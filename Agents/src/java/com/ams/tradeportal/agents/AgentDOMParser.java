package com.ams.tradeportal.agents;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;

import com.amsinc.ecsg.frame.Logger;

/*
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AgentDOMParser implements AgentDOMParserWrapper, ErrorHandler {

    org.apache.xerces.parsers.DOMParser parser;

    private Logger logger = Logger.getLogger();
    private String fatalErrorFlag = "N";
    private String errorFlag = "N";
    private String warningFlag = "N";

    private static String loggerCategory;
    private String agentID = "";

    static {
        PropertyResourceBundle properties = (PropertyResourceBundle) ResourceBundle.getBundle("AgentConfiguration");
        loggerCategory = properties.getString("loggerServerName");
    }

    public AgentDOMParser() {
        parser = new org.apache.xerces.parsers.DOMParser();

        try {
            parser.setFeature("http://xml.org/sax/features/validation", true);
        } catch (SAXException _ex) {
            System.out.println("AgentDOMParser: Error in setting up parser feature");
            logger.log(loggerCategory, this.agentID, "AgentDOMParser: Error in setting up parser feature", 5);
        }
        parser.setErrorHandler(this);
    }

    @Override
    public boolean isXmlValidAgainstDtd() {
        if (fatalErrorFlag.equals("Y") || errorFlag.equals("Y") || warningFlag.equals("Y")) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void error(SAXParseException saxparseexception) {
        System.out.println("AgentDOMParser: [Error] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage());
        logger.log(loggerCategory, this.agentID, "AgentDOMParser: [Error] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage(), 5);
        errorFlag = "Y";
    }

    @Override
    public void fatalError(SAXParseException saxparseexception) throws SAXException {
        System.out.println("AgentDOMParser: [Fatal Error] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage());
        logger.log(loggerCategory, this.agentID, "AgentDOMParser: [Fatal Error] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage(), 5);
        fatalErrorFlag = "Y";
        throw saxparseexception;
    }

    private String getLocationString(SAXParseException saxparseexception) {
        StringBuilder sBuilder = new StringBuilder();
        String s = saxparseexception.getSystemId();
        if (s != null) {
            int i = s.lastIndexOf(47);
            if (i != -1) {
                s = s.substring(i + 1);
            }
            sBuilder.append(s);
        }
        sBuilder.append(':');
        sBuilder.append(saxparseexception.getLineNumber());
        sBuilder.append(':');
        sBuilder.append(saxparseexception.getColumnNumber());
        return sBuilder.toString();
    }

    @Override
    public Document parse(String s) throws Exception {

        parser.parse(s);
        return parser.getDocument();
    }

    @Override
    public Document parse(InputSource inputSource) throws Exception {
        System.out.println("AgentDOMParser: [Agent ID]: " + this.agentID);
        parser.parse(inputSource);
        return parser.getDocument();
    }

    @Override
    public void setFeature(String s, boolean flag) throws SAXNotRecognizedException, SAXNotSupportedException {
        parser.setFeature(s, flag);
    }

    @Override
    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    @Override
    public void warning(SAXParseException saxparseexception) {
        System.out.println("AgentDOMParser: [Warning] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage());
        logger.log(loggerCategory, this.agentID, "AgentDOMParser: [Warning] " + getLocationString(saxparseexception) + ": " + saxparseexception.getMessage(), 5);
        warningFlag = "Y";
    }

}
