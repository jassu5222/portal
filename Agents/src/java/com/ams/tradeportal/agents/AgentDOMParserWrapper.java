package com.ams.tradeportal.agents;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

/*
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AgentDOMParserWrapper {

    Document parse(String s) throws Exception;

    Document parse(InputSource inputSource) throws Exception;

    boolean isXmlValidAgainstDtd();

    void setFeature(String s, boolean flag) throws SAXNotRecognizedException, SAXNotSupportedException;

    void setAgentID(String agentID);
}
