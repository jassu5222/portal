package com.ams.util;

import org.bouncycastle.util.encoders.*;
import java.util.*;
import java.io.*;

import org.bouncycastle.util.encoders.Base64;

/**
 *  Utility class that contains static methods for working with base64
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class Base64Utils
 {
    /**
     * Converts an array of bytes into a string using base64 encoding.
     * base64 is an algorithm for representing a set of bytes as a string.
     * The official name is Base64 Content-Transfer-Encoding and more information
     * on its details can be found in RFC-2045 at
     *       http://www.oac.uci.edu/indiv/ehood/MIME/2045/rfc2045.html
     *
     *
     * @param bytes array of bytes to convert
     * @return string representing the bytes in the above format
     */
    public static String bytesToBase64String(byte[] rawBytes)
    {
       String base64 = null;

       try 
        {
          // Encode the bytes, which returns an array of bytes.  Then, convert those
          // bytes into ASCII
          base64 = new String(Base64.encode(rawBytes), "ISO8859_1");

          // The ! character is not actually part of the base64 specification 
          // When a '/' character is included in a URL, it is interpreted to mean 
          // a ! character.  Therefore, when encoding, we should treat the !
          // character as being identical to the '/' character.   
          // Replace all instances of the '/' character with '!'
          base64 = base64.replace('/','!');
       }
       catch( UnsupportedEncodingException uee)
        {
          // This should never happen - "ASCII" is a valid encoding
        }              
     
        // The algorithm places a newline at the end of each base64 string
        // Strip it off here
        return base64.trim();
    }

   /**
    * Converts a base64 string into bytes.
    * base64 is an algorithm for representing a set of bytes as a string.
    * The official name is Base64 Content-Transfer-Encoding and more information
    * on its details can be found in RFC-2045 at
    *       http://www.oac.uci.edu/indiv/ehood/MIME/2045/rfc2045.html
    *
    *
    * @param str base64-encoded String to convert
    * @return array of bytes
    */
   public static byte[] base64StringToBytes(String str)
    {
      byte[] decodedBytes = null;

      // The space character is not actually part of the base64 specification
      // When a '+' character is included in a URL, it is interpreted to mean 
      // a space character.  Therefore, when decoding, we should treat the space
      // character as being identical to the '+' character.   
      // Replace all instances of the ' ' character with '+'
      str = str.replace(' ','+');
      // The ! character is not actually part of the base64 specification
      // When a '/' character is included in a URL, it is interpreted to mean 
      // a ! character.  Therefore, when decoding, we should treat the !
      // character as being identical to the '/' character.   
      // Replace all instances of the '!' character with '/'
      str = str.replace('!','/');
          
      // First, convert the string passed in to bytes so that it can be decoded
      try 
       {
          decodedBytes = Base64.decode(str.getBytes("ISO8859_1"));
       }
      catch( UnsupportedEncodingException uee)
       {
          // This should never happen - "ASCII" is a valid encoding
       }

      return decodedBytes;     
    }
 }






