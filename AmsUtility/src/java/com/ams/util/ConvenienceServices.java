package com.ams.util;

import java.util.*;
import java.io.*;

/**
 *  Utility class that contains static methods to perform ConvenienceServices
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public class ConvenienceServices
 {
   /**
    * Returns true if the given value is null or a blank string
    *
    * @return boolean - true: string is blank or null
    * @param value java.lang.String - value to test
    */
    public static boolean isBlank(String value) {
       if (value == null) return true;
   	 if (value.equals("")) return true;
	    return false;
    }
  

   /**
    * Returns true if the given value is NOT null or a blank string
    *
    * @return boolean - string is non blank
    * @param value java.lang.String - value to test
    */
    public static boolean isNotBlank(String value) {
       return !isBlank(value);
    }
 }






