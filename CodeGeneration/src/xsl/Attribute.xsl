<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates the code for the regsiterAttributes() method in the business object.  This
*  method is only added if there are actually attributes to be registered.
-->



dddd



<xsl:template name="ATTRIBUTE_REGISTRATION">
<xsl:param name="Dynamic">False</xsl:param>
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:param name="ObjectName"></xsl:param>
<xsl:param name="Ancestor">BusinessObject</xsl:param>
<xsl:variable name="RegistrationCode">
   <xsl:if test="$Dynamic='False'">
	   <xsl:call-template name="ATTRIBUTES"><xsl:with-param name="ObjectName" select="$ObjectName"/><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="Dynamic" select="$Dynamic" /></xsl:call-template>
   </xsl:if>
   <xsl:call-template name="ASSOCIATIONS"><xsl:with-param name="ObjectName" select="$ObjectName"/><xsl:with-param name="ObjectType" select="$ObjectType"/> </xsl:call-template>
</xsl:variable>
<xsl:if test="$ObjectType='Bean'"><xsl:variable name="Scope">protected</xsl:variable></xsl:if>
<xsl:if test="not($ObjectType='Bean')"><xsl:variable name="Scope">public</xsl:variable></xsl:if>
<!--<xsl:if test="not($RegistrationCode='')">-->
   <xsl:if test="$ObjectType='Bean'">
  /* 
   * Register the attributes and associations of the business object
   */
   </xsl:if> 
   <xsl:value-of select="$Scope"/> void registerAttributes()<xsl:if test="$ObjectType='Bean'"> throws AmsException</xsl:if>
   {  <xsl:if test="not($Ancestor='BusinessObject' or $Ancestor='com.amsinc.ecsg.web.AmsEntity')">
      <xsl:if test="$ObjectType='Bean'">

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      </xsl:if>
      <xsl:if test="not($ObjectType='Bean')">
      super.registerAttributes();</xsl:if></xsl:if>
      <xsl:value-of select="$RegistrationCode"/>

	<xsl:if test="$Dynamic !='False'">
	registerDynamicAttributes();
	</xsl:if>
   }
   <xsl:if test="($Dynamic!='False') and ($ObjectType='Bean')">
     /**
      * This method is overriden in the actual Bean class, where it 
      * handles registration of dynamic attributes 
      */
     protected void registerDynamicAttributes() throws AmsException
      {
	
      }
   </xsl:if>

   <xsl:if test="($Dynamic!='False') and ($ObjectType!='Bean')">
     /**
      * This method is overriden in the actual Bean class, where it 
      * handles registration of dynamic attributes 
      */
     protected void registerDynamicAttributes() 
      {
	
      }
   </xsl:if>

   <xsl:if test="($Dynamic!='False') and ($ObjectType='Bean')">
   /**
    *  Static inner class to define factory methods for the various attributes.
    *
    */
   public static class Attributes
    {  <xsl:call-template name="ATTRIBUTES"><xsl:with-param name="ObjectName" select="$ObjectName"/><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="Dynamic" select="$Dynamic" /></xsl:call-template>
    }
   </xsl:if>   
<!--</xsl:if>-->
</xsl:template>

<!--
*  Loops through each attribute for the object specified in the object model and calls the "ATTRIBUTE" template to
*  register it with the attributeManager service object.  Note: Associations, Component Pointers, and Parent Pointers
*  are not registered as "Attributes" in the model.  Separate templates are used to process these element of the
*  object model.  
-->
<xsl:template name="ATTRIBUTES">
<xsl:param name="ObjectName" />
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:param name="Dynamic">False</xsl:param>
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Attribute">
   <xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

   <xsl:if test="not($Generate='False')" >
      <xsl:call-template name="ATTRIBUTE"><xsl:with-param name="ObjectName" select="$ObjectName"/><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="Dynamic" select="$Dynamic" /></xsl:call-template>
   </xsl:if>
</xsl:for-each>
</xsl:template>



<!--
*  Performs the registration of an Attribute with the attributeManager service object.  Most attributes are 
*  registered using the "registerAttribute()" method, however, ReferenceAttributes are registered by calling
*  "registerReferenceAttribute()".
-->
<xsl:template name="ATTRIBUTE">
<xsl:param name="ObjectName"/>
<xsl:param name="ObjectType"/>
<xsl:param name="Dynamic">False</xsl:param>
<xsl:variable name="LogicalName"   select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.DataType/@xmi.idref"/>
<xsl:if test="not($AttributeLink=null)"> 
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:if test="$AttributeLink=null">
   <xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.Class/@xmi.idref"/>
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:if test="$Dynamic!='False'">
	<xsl:variable name="Alias" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Alias']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
	<xsl:if test="$Alias=null or $Alias=''"><xsl:variable name="hasAlias">False</xsl:variable></xsl:if>
	<xsl:if test="$Alias!=null and $Alias!=''"><xsl:variable name="hasAlias">True</xsl:variable></xsl:if>
      <xsl:variable name="NullsOK" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:NullsOK']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
      <xsl:if test="$NullsOK=null"><xsl:variable name="NullsOK" select="$NullsOKDefault"/></xsl:if>

	<xsl:variable name="AliasResource">"TermsBeanAlias.<xsl:value-of select="$LogicalName" />"</xsl:variable>

      /**
        * Attribute factory method for <xsl:value-of select="$LogicalName" />
        *
        * <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">8</xsl:with-param></xsl:call-template>
        */
	public static Attribute create_<xsl:value-of select="$LogicalName" />()
	 {<xsl:if test="($AttributeType = 'TextAttribute') or ($AttributeType = 'ClobAttribute')">
		<xsl:variable name="RevisedAttributeType">Attribute</xsl:variable>
	</xsl:if>
	<xsl:if test="($AttributeType != 'TextAttribute') and ($AttributeType != 'ClobAttribute')">
		<xsl:variable name="RevisedAttributeType" select="$AttributeType" />
	</xsl:if>  return (Attribute) new <xsl:value-of select="$RevisedAttributeType" />("<xsl:value-of select="$LogicalName" />", "<xsl:call-template name="PHYSICAL_NAME"><xsl:with-param name="LogicalName" select="$LogicalName"/></xsl:call-template>"<xsl:if test="$AttributeType='ReferenceAttribute'">, "<xsl:value-of select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:ValueSet']/Foundation.Extension_Mechanisms.TaggedValue.value"/>"</xsl:if><xsl:if test="$NullsOK!='False' and $hasAlias='True'">, false, <xsl:value-of select="$AliasResource" /></xsl:if><xsl:if test="$NullsOK='False' and $hasAlias='False'">, true, "<xsl:value-of select="$LogicalName" />"</xsl:if><xsl:if test="$NullsOK='False' and $hasAlias='True'">, true, "<xsl:value-of select="$AliasResource" />"</xsl:if>);
	 }
</xsl:if>
<xsl:if test="$ObjectType='Bean' and $Dynamic='False'">
      /* <xsl:value-of select="$LogicalName"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
      attributeMgr.register<xsl:if test="$AttributeType='ReferenceAttribute'">Reference</xsl:if>Attribute("<xsl:value-of select="$LogicalName"/>", "<xsl:call-template name="PHYSICAL_NAME"><xsl:with-param name="LogicalName" select="$LogicalName"/></xsl:call-template>"<xsl:call-template name="TYPE"><xsl:with-param name="AttributeType" select="$AttributeType"/></xsl:call-template>
  	<!-- Aliases -->
	<xsl:variable name="Alias" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Alias']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
	<xsl:if test="$Alias=null or $Alias=''"></xsl:if>
	<xsl:if test="$Alias!=null and $Alias!=''">
      attributeMgr.registerAlias("<xsl:value-of select="$LogicalName"/>", getResourceManager().getText("<xsl:value-of select="$ObjectName" />BeanAlias.<xsl:value-of select="$LogicalName" />", TradePortalConstants.TEXT_BUNDLE));</xsl:if>
      <xsl:text>
      </xsl:text>
</xsl:if>
<xsl:if test="$ObjectType='WebBean'">
<xsl:text>&#xa;</xsl:text><xsl:text>&#xa;</xsl:text>
       /* <xsl:value-of select="$LogicalName"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
       registerAttribute("<xsl:value-of select="$LogicalName"/>", "<xsl:call-template name="PHYSICAL_NAME"><xsl:with-param name="LogicalName" select="$LogicalName"/></xsl:call-template>"<xsl:call-template name="TYPE"><xsl:with-param name="AttributeType" select="$AttributeType"/><xsl:with-param name="ObjectType" select="$ObjectType"/></xsl:call-template>
</xsl:if>
</xsl:template>


<!--
*  Determines the Physical column name of the Attribute and specifies this and the second parameter to the
*  "registerAttribute()" method.  If a separate physical name for the attribute has not been specified, the Logical
*  name of the attribute is used. 
-->
<xsl:template name="PHYSICAL_NAME">
   <xsl:param name="LogicalName"/>
   <xsl:variable name="PhysicalName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:ColumnName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:choose>
      <xsl:when test="$PhysicalName=null"><xsl:value-of select="$LogicalName"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="$PhysicalName"/></xsl:otherwise>
   </xsl:choose>
</xsl:template>


<!--
*  Determines the Type of the Attribute.  If the Attribute is just a 'normal' string, we do not need
*  to specify its type when we register it.  If the Attribute is something other than a simple string,
*  we need to specify its type as the third parameter to the "registerAttribute()" method.  If the
*  Attribute is a Reference Attribute, the logical table name used to validate the attribute's value
*  will be speficied as the third parameter.
-->
<xsl:template name="TYPE">
<xsl:param name="AttributeType"/>
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:choose>
   <xsl:when test="$AttributeType='TextAttribute'">);</xsl:when>
   <xsl:when test="$AttributeType='ClobAttribute'">);</xsl:when>
   <xsl:when test="$AttributeType='TradePortalDecimalAttribute' and $ObjectType='Bean'">, "<xsl:value-of select="$AttributeType"/>", "com.ams.tradeportal.common");</xsl:when>
   <xsl:when test="$AttributeType='TradePortalSignedDecimalAttribute' and $ObjectType='Bean'">, "<xsl:value-of select="$AttributeType"/>", "com.ams.tradeportal.common");</xsl:when>
   <xsl:when test="$AttributeType=null">);</xsl:when>
   <xsl:when test="$AttributeType='ReferenceAttribute' and $ObjectType='Bean'">, "<xsl:value-of select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:ValueSet']/Foundation.Extension_Mechanisms.TaggedValue.value"/>");</xsl:when>
   <xsl:when test="$AttributeType='ReferenceAttribute' and $ObjectType='WebBean'">);</xsl:when>
   <xsl:otherwise>, "<xsl:value-of select="$AttributeType"/>");</xsl:otherwise>
</xsl:choose>
<xsl:if test="$ObjectType='Bean'">
   <xsl:call-template name="REQUIRED"><xsl:with-param name="AttributeType" select="$AttributeType"/></xsl:call-template>
</xsl:if>
</xsl:template>


<!-- 
*  Determines whether the Attribute is required or not.  If the Attribute has been designated as required, then
*  we need to call the "requiredAttribute()" method on attributeManager to register it as such.  Several types
*  of attributes do not need to be registered as required since they are set internally by the framework.  The
*  "ObjectIDAttribute" is an example of this. 
-->
<xsl:template name="REQUIRED">
<xsl:param name="AttributeType">.</xsl:param>
<xsl:choose>
   <xsl:when test="$AttributeType='ObjectIDAttribute'"></xsl:when>
   <xsl:otherwise>
      <xsl:variable name="LogicalName" select="Foundation.Core.ModelElement.name"/>
      <xsl:variable name="NullsOK" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:NullsOK']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
      <xsl:if test="$NullsOK=null"><xsl:variable name="NullsOK" select="$NullsOKDefault"/></xsl:if>
      <xsl:if test="$NullsOK='False'">
      attributeMgr.requiredAttribute("<xsl:value-of select="$LogicalName"/>");</xsl:if>





   </xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>