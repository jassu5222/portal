<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<xsl:param name="DescriptorDirectory"/>
<!--
  *  This is the template generates a consolidated build script for the serialized deployment descriptors needed
  *  by the application. 
-->

<xsl:template match="XMI">
<xsl:if test="$SerAndStubDirectory=null"><xsl:variable name="OutputDirectory" select="$DescriptorDirectory"/></xsl:if>
<xsl:if test="not($SerAndStubDirectory=null)"><xsl:variable name="OutputDirectory" select="$SerAndStubDirectory"/></xsl:if>
<xsl:for-each select="XMI.content">
  <xsl:for-each select="Model_Management.Model">time /t<xsl:call-template name="PROCESS_PACKAGE"><xsl:with-param name="OutputDirectory" select="$OutputDirectory"/></xsl:call-template>
time /t
  </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Loops through each Class (e.g., Business Object) in the current package and calls the PROCESS_CLASS template
  *  to generate the required code for the object.  Also recursively calls itself (the PROCESS_PACKAGE) template
  *  to process packages that are contained with this package.  Note: The first time that this template is called
  *  it is processing at the highest level within the Model and is not within a package.
-->
<xsl:template name="PROCESS_PACKAGE">
<xsl:param name="OutputDirectory"/>
<xsl:for-each select="Foundation.Core.Namespace.ownedElement">
   <xsl:for-each select="Foundation.Core.Class">
      <xsl:call-template name="PROCESS_CLASS"><xsl:with-param name="OutputDirectory" select="$OutputDirectory"/></xsl:call-template>
   </xsl:for-each>
   <xsl:for-each select="Model_Management.Package">
      <xsl:call-template name="PROCESS_PACKAGE"><xsl:with-param name="OutputDirectory" select="$OutputDirectory"/></xsl:call-template>
   </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Generates an entry in the Serialized Deployment Descriptor build script for the class.
-->
<xsl:template name="PROCESS_CLASS">
<xsl:param name="PackageName"/>
<xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not($Generate='False')" >
java weblogic.ejb.utils.DDCreator -dir <xsl:value-of select="$SerAndStubDirectory"/> -outputfile <xsl:value-of select="$ClassName"/>DD.ser <xsl:value-of select="$DescriptorDirectory"/>\<xsl:value-of select="$ClassName"/>DD.txt</xsl:if>
</xsl:template>


<xsl:include href="Preferences.xsl"/>

</xsl:stylesheet>