<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<!--
  *  This template generates the Enterprise Java Bean for a Business Object defined within the object
  *  model.  The code generated includes:
  *
  *  Registration of Attributes and Associations
  *  Registration of Component Objects
  *  Specification of Attributes as Required
  *  Setting default values for Attributes
  *  Stubbing out Methods
  *  
  *  The logic to generate the items listed above has been modularized into separate templates.  This template
  *  really just serves as a skeleton / coordinator for calling the other templates.
-->
<xsl:template name="BUSINESS_OBJECT">
<xsl:param name="PackageName"/> 
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="Ancestor"><xsl:call-template name="ANCESTOR"/></xsl:variable> 
-------------------- START: <xsl:value-of  select="$ClassName"/>Bean_Base.java --------------------
<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package <xsl:value-of select="$PackageName"/><xsl:value-of select="$BusObjPackage"/>;
<xsl:call-template name="IMPORTS"></xsl:call-template>

/*
 * <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/></xsl:call-template>
 *<xsl:call-template name="COPYRIGHT"> <xsl:with-param name="ClassName" select="$ClassName"/> </xsl:call-template>*/
public class <xsl:value-of select="$ClassName"/>Bean_Base extends <xsl:value-of select="$Ancestor"/>Bean
{

  <xsl:variable name="dynamicAttributes" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:DynamicAttributeRegistration']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

   <xsl:if test="not($dynamicAttributes='TRUE')">
     <xsl:call-template name="ATTRIBUTE_REGISTRATION">
	 <xsl:with-param name="ObjectName" select="$ClassName" />
	 <xsl:with-param name="Dynamic">False</xsl:with-param>
       <xsl:with-param name="Ancestor" select="$Ancestor"/>
     </xsl:call-template>
  </xsl:if>

   <xsl:if test="$dynamicAttributes='TRUE'">
     <xsl:call-template name="ATTRIBUTE_REGISTRATION">
	 <xsl:with-param name="Dynamic">True</xsl:with-param>
       <xsl:with-param name="Ancestor" select="$Ancestor"/>
     </xsl:call-template>
  </xsl:if>

   <xsl:call-template name="COMPONENT_REGISTRATION">
      <xsl:with-param name="Ancestor" select="$Ancestor"/>
   </xsl:call-template>
 
 <!-- Took this out... no methods in the _Base class
   <xsl:call-template name="NEW_OBJECT_PROCESSING">
      <xsl:with-param name="Ancestor" select="$Ancestor"/>
   </xsl:call-template>
 -->
 
   <!-- Took this out... no methods in the _Base class
     <xsl:call-template name="METHOD_DECLARATIONS"/>-->
}
-------------------- FINISH: <xsl:value-of select="$ClassName"/>Bean_Base.java --------------------
</xsl:template>

<xsl:include href="Copyright.xsl"/>
<xsl:include href="Imports.xsl"/>
<xsl:include href="Attribute.xsl"/>
<xsl:include href="Component.xsl"/>
<xsl:include href="NewObject.xsl"/>
<xsl:include href="Method.xsl"/>
<xsl:include href="Ancestor.xsl"/>
<xsl:include href="Comment.xsl"/>

</xsl:stylesheet>