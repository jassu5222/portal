<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<xsl:param name="GenBean">Y</xsl:param>
<xsl:param name="GenRemote">Y</xsl:param> 
<xsl:param name="GenHome">Y</xsl:param>
<xsl:param name="GenWebBean">Y</xsl:param>
<xsl:param name="GenDeploy">Y</xsl:param>
<xsl:param name="GenDDL">Y</xsl:param>
<xsl:param name="GenTester">Y</xsl:param>
<xsl:param name="ObjectToGenerate"/>
<!--
  *  This is the Main / Initial template for the AMS Enterprise Application Framework code generation process.  The
  *  stylesheets called by this template transform an XML document which has been exported from an Object Modeling
  *  tool in XMI (eXtensible Metadata Interchange) format into Enterprise Java Beans and their corresponding com-
  *  ponents (Home Interface, Remote Interface, Deployment Descriptor, etc.)
  *
  *  This template initiates the process by calling the PROCESS_PACKAGE template which will generate code for all
  *  business objects defined at the highest level within the model (not actually in a package).  The PROCESS_PACKAGE
  *  template will then loop through each package and call itself recursively to process the business objects and
  *  packages which reside inside of those packages.  Cool stuff, huh?
  *
  *  The name of the default package to place objects in is hard-coded and passed in to the PROCESS_PACKAGE template
  *  as a parameter.  Objects not residing in any package will be placed in this package.  The name of the default
  *  package will also be pre-pended to the name of any packages specified within the model.  Projects can modify
  *  the value of the "TopLevelPackage" variable as needed.
  *
  *  Note:  The "for-each" loops in this template should not be necessary given the structure of the XML document.
  *  However, eliminating them and placing all of their nodes in the "Match" clause for the template results is a 
  *  lot of "garbage" being inserted at the beginning and end of the generated code.  This appears to be the result
  *  of a bug in the XML parser. 
-->
<xsl:template match="XMI">
<xsl:for-each select="XMI.content">
  <xsl:for-each select="Model_Management.Model">
    <xsl:call-template name="PROCESS_PACKAGE"><xsl:with-param name="AncestorPackage"><xsl:value-of select="$PackagePrefix"/></xsl:with-param></xsl:call-template>
  </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Loops through each Class (e.g., Business Object) in the current package and calls the PROCESS_CLASS template
  *  to generate the required code for the object.  Also recursively calls itself (the PROCESS_PACKAGE) template
  *  to process packages that are contained with this package.  Note: The first time that this template is called
  *  it is processing at the highest level within the Model and is not within a package.
  *
  *  The name of the current Package is constructed by concatenating together the name of each package encountered
  *  in the package hierarchy.  This information is eventually passed to the PROCESS_CLASS template as a parameter. 
-->
<xsl:template name="PROCESS_PACKAGE">
<xsl:param    name="AncestorPackage"/>
<xsl:variable name="ThisPackage" select="../Model_Management.Package/Foundation.Core.ModelElement.name"/>
<xsl:choose>
  <xsl:when test="$ThisPackage=null or $AncestorPackage=null"><xsl:variable name="PackageName" select="concat($AncestorPackage, $ThisPackage)"/></xsl:when>
  <xsl:otherwise><xsl:variable name="PackageName" select="concat($AncestorPackage, '.', $ThisPackage)"/></xsl:otherwise>
</xsl:choose>
<xsl:for-each select="Foundation.Core.Namespace.ownedElement">
   <xsl:for-each select="Foundation.Core.Class">
      <xsl:call-template name="PROCESS_CLASS"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
   </xsl:for-each>
   <xsl:for-each select="Model_Management.Package">
      <xsl:call-template name="PROCESS_PACKAGE"><xsl:with-param name="AncestorPackage" select="$PackageName"/></xsl:call-template>
   </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Calls templates which will generate objects which are appropriate for the type of class being processed.  Valid
  *  class types (defined by a Stereotype within the object model) include "BusinessObject"s and "Mediator"s.
-->
<xsl:template name="PROCESS_CLASS">
<xsl:param name="PackageName"/>
<xsl:variable name="Stereotype"><xsl:call-template name="STEREOTYPE"/></xsl:variable>
<xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not($ObjectToGenerate=null)">
   <xsl:if test="not($ObjectToGenerate=$ClassName)">
      <xsl:variable name="Generate">False</xsl:variable>
   </xsl:if>
</xsl:if>
<xsl:if test="not($Generate='False')" >
   <xsl:choose>
      <xsl:when test="$Stereotype='BusinessObject'">
         <xsl:call-template name="PROCESS_BUSINESSOBJECT"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
      </xsl:when>
      <xsl:when test="$Stereotype='Mediator'">
         <xsl:call-template name="PROCESS_MEDIATOR"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
      </xsl:when>
   </xsl:choose>
</xsl:if>
</xsl:template>


<!--
  *  Determines the Stereotype of the class being processed.  That is, is the class a Business Object, Mediator,
  *  Service, etc.  The Stereotype of the class will determine which objects are generated for it and which templates
  *  are used in the generation process.
-->
<xsl:template name="STEREOTYPE">
<xsl:variable name="StereotypeId" select="Foundation.Core.ModelElement.stereotype/Foundation.Extension_Mechanisms.Stereotype/@xmi.idref"/>
<xsl:variable name="StereotypeName" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Extension_Mechanisms.Stereotype[@xmi.id=$StereotypeId]/Foundation.Core.ModelElement.name"/>
<xsl:if test="$StereotypeName=null">BusinessObject</xsl:if>
<xsl:if test="not($StereotypeName=null)"><xsl:value-of select="$StereotypeName"/></xsl:if>
</xsl:template>


<!--
  *  Calls templates to generate the components which are required to implement and deploy the Business Object
  *  being processed.  These components include the Enterprise Java Bean, the Home Interface, the Remote Interface,
  *  the Deployment Descriptor, and the Web Bean.  The name of the Package in which the class resides is passed to
  *  the template as a paramter so that it will not have to be "re-computed".
-->
<xsl:template name="PROCESS_BUSINESSOBJECT">
<xsl:param name="PackageName"/>
<xsl:if test="$GenBean='Y'">
   <xsl:call-template name="BUSINESS_OBJECT"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenRemote='Y'">
   <xsl:call-template name="REMOTE_INTERFACE"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenHome='Y'">
   <xsl:call-template name="HOME_INTERFACE"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenDeploy='Y'">
   <xsl:call-template name="DEPLOYMENT_DESCRIPTOR"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenWebBean='Y'">
   <xsl:call-template name="WEB_BEAN"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenDDL='Y'">
   <xsl:call-template name="TABLE_DEFINITION"></xsl:call-template>
</xsl:if>
<xsl:if test="$GenTester='Y'">
   <xsl:call-template name="TESTER"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
</xsl:template>


<!--
  *  Calls templates to generate the components which are required to implement and deploy the Mediator being
  *  processed.  These components include the Enterprise Java Bean, the Home Interface, the Remote Interface,
  *  and  the Deployment Descriptor, and the Web Bean.
-->
<xsl:template name="PROCESS_MEDIATOR">
<xsl:param name="PackageName"/>
<xsl:if test="$GenBean='Y'">
   <xsl:call-template name="MEDIATOR"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenRemote='Y'">
   <xsl:call-template name="REMOTE_INTERFACE">
      <xsl:with-param name="PackageName" select="$PackageName"/>
      <xsl:with-param name="DefaultAncestorClass">Mediator</xsl:with-param>
    </xsl:call-template>
</xsl:if>
<xsl:if test="$GenHome='Y'">
   <xsl:call-template name="HOME_INTERFACE"><xsl:with-param name="PackageName" select="$PackageName"/></xsl:call-template>
</xsl:if>
<xsl:if test="$GenDeploy='Y'">
   <xsl:call-template name="DEPLOYMENT_DESCRIPTOR">
      <xsl:with-param name="PackageName" select="$PackageName"/>
      <xsl:with-param name="ObjectType">Mediator</xsl:with-param>
   </xsl:call-template>
</xsl:if>
</xsl:template>

<xsl:include href="BusinessObject.xsl"/>
<xsl:include href="Mediator.xsl"/>
<xsl:include href="HomeInterface.xsl"/>
<xsl:include href="RemoteInterface.xsl"/>
<xsl:include href="DeploymentDescriptor.xsl"/>
<xsl:include href="WebBean.xsl"/>
<xsl:include href="TableDefinition.xsl"/>
<xsl:include href="Tester.xsl"/>

</xsl:stylesheet>