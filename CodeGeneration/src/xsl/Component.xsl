<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates the code for the regsiterComponents() method in the business object.  This
*  method is only added if there are actually components to be registered.
-->
<xsl:template name="COMPONENT_REGISTRATION">
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:param name="Ancestor">BusinessObject</xsl:param>
<xsl:variable name="RegistrationCode">
   <xsl:call-template name="COMPONENTS"><xsl:with-param name="ObjectType" select="$ObjectType"/>
   </xsl:call-template>
</xsl:variable>
<xsl:if test="$ObjectType='Bean'"><xsl:variable name="Scope">protected</xsl:variable></xsl:if>
<xsl:if test="not($ObjectType='Bean')"><xsl:variable name="Scope">public</xsl:variable></xsl:if>
<xsl:if test="not($RegistrationCode='')">
   <xsl:if test="$ObjectType='Bean'">
  /* 
   * Register the components of the business object
   */
   </xsl:if>
   <xsl:value-of select="$Scope"/> void registerComponents()<xsl:if test="$ObjectType='Bean'"> throws RemoteException, AmsException</xsl:if>
   {  <xsl:if test="not($Ancestor='BusinessObject' or $Ancestor='com.amsinc.ecsg.web.AmsEntity')">
      <xsl:if test="$ObjectType='Bean'">
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      </xsl:if>
      <xsl:if test="not($ObjectType='Bean')">
      super.registerComponents();</xsl:if></xsl:if>
      <xsl:value-of select="$RegistrationCode"/>   }
</xsl:if>
</xsl:template>


<!--
*  Loops through each Component specified for the object in the object model and calls the "COMPONENT" template
*  to register it with the compoentManager service object.
-->
<xsl:template name="COMPONENTS">
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:for-each select="Foundation.Core.Classifier.associationEnd/Foundation.Core.AssociationEnd">
  <xsl:variable name="AssociationEndLink" select="@xmi.idref"/>
  <xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Association/Foundation.Core.Association.connection/Foundation.Core.AssociationEnd[@xmi.id=$AssociationEndLink]">
     <xsl:call-template name="COMPONENT_END"><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="AssociationEndLink" select="$AssociationEndLink"/></xsl:call-template>
  </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
*  Called by the COMPONENTS template to identify which AssociationEnd in the Connection points to the Object being
*  processed and which AssociationEnd in the Connection points to the associated object.  Once this is determined,
*  the COMPONENT template is called (passing it this info) to process the actual component.
-->
<xsl:template name="COMPONENT_END">
<xsl:param name="AssociationEndLink"/>
<xsl:param name="ObjectType"/>
<xsl:if test="not(Foundation.Core.AssociationEnd.aggregation/@xmi.value='none')">
  <xsl:choose>
    <xsl:when test="../Foundation.Core.AssociationEnd[1]/@xmi.id=$AssociationEndLink">
      <xsl:call-template name="COMPONENT"><xsl:with-param name="ThisObject">1</xsl:with-param><xsl:with-param name="ComponentObject">2</xsl:with-param><xsl:with-param name="ObjectType" select="$ObjectType"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="COMPONENT"><xsl:with-param name="ThisObject">2</xsl:with-param><xsl:with-param name="ComponentObject">1</xsl:with-param><xsl:with-param name="ObjectType" select="$ObjectType"/></xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:if>
</xsl:template>

<!--
*  Determines whether the component is a One-To-One relationship (Parent contains only one instance of the component
*  class) or a One-To-Many relationship (Parent contains a list of the component class) and registers it with the
*  componentManager service object.
-->
<xsl:template name="COMPONENT"> <xsl:param name="ThisObject">0</xsl:param> <xsl:param name="ComponentObject">0</xsl:param> <xsl:param name="ObjectType"/>
<xsl:variable name="ComponentIDRef" select="../Foundation.Core.AssociationEnd[position()=$ComponentObject]/Foundation.Core.AssociationEnd.type/Foundation.Core.Class/@xmi.idref"/>
<xsl:variable name="ComponentClass" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$ComponentIDRef]/Foundation.Core.ModelElement.name"/>
<xsl:variable name="ClassIsGenerated" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$ComponentIDRef]/Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:if test="not($ClassIsGenerated='False')">
<xsl:variable name="Cardinality" select="../Foundation.Core.AssociationEnd[position()=$ComponentObject]/Foundation.Core.AssociationEnd.multiplicity"/>

<xsl:variable name="AssocName">
    <xsl:call-template name="ASSOC_NAME">
      <xsl:with-param name="ClassName" select="$ComponentClass"/>
      <xsl:with-param name="AssociatedObject" select="$ComponentObject"/>
    </xsl:call-template>
</xsl:variable>

<xsl:if test="not($Cardinality='1..1' or $Cardinality='0..1' or $Cardinality='1')">
<xsl:if test="not ($AssocName = '')">
	<xsl:variable name="FirstParam" select="$AssocName" />
</xsl:if>
<xsl:if test="$AssocName = ''">
	<xsl:variable name="FirstParam"><xsl:value-of select="$ComponentClass" />List</xsl:variable>
</xsl:if>
   <xsl:if test="$ObjectType='Bean'">
      /* <xsl:value-of select="$FirstParam"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */</xsl:if>
      registerOneToManyComponent("<xsl:value-of select="$FirstParam" />","<xsl:value-of select="$ComponentClass"/>List");
</xsl:if>
<xsl:if test="$Cardinality='1..1' or $Cardinality='0..1' or $Cardinality='1'">
   <xsl:variable name="LinkageAttribute">
      <xsl:call-template name="LINKAGE_ATTRIBUTE">
        <xsl:with-param name="ClassName" select="$ComponentClass"/>
        <xsl:with-param name="AssociatedObject" select="$ComponentObject"/>
      </xsl:call-template>
   </xsl:variable>

	<xsl:if test="not ($AssocName = '')">
		<xsl:variable name="FirstParam" select="$AssocName" />
	</xsl:if>
	<xsl:if test="$AssocName = ''">
		<xsl:variable name="FirstParam" select="$ComponentClass" />
	</xsl:if>	

   <xsl:if test="$ObjectType='Bean'">
      /* <xsl:value-of select="$FirstParam" /> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */</xsl:if>
      registerOneToOneComponent("<xsl:value-of select="$FirstParam" />", "<xsl:value-of select="$ComponentClass"/>", "<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$FirstParam" />");
   </xsl:if>
</xsl:if>
</xsl:template>
</xsl:stylesheet>