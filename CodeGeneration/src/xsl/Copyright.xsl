<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<!--
*  Generates a standard Copyright notifcation which can be placed at the beginning of each software module
*  generated.  This notice can be easily modified to meet the specific needs of each project.  The name of the
*  object is dynamicaly substituted into the text of the copyright statement, but the rest if just "hard-coded".
-->
<xsl:template name="COPYRIGHT">
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
</xsl:template>
</xsl:stylesheet>