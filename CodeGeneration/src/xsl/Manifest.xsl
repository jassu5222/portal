<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<xsl:param name="DescriptorDirectory"/>
<!--
  *  Creates a Manifest file for the application. 
-->
<xsl:template match="XMI">
<xsl:for-each select="XMI.content">
  <xsl:for-each select="Model_Management.Model">Manifest-Version: 1.0
Created-By: 1.2.2 (Sun Microsystems Inc.)
     <xsl:call-template name="PROCESS_PACKAGE"/>
  </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Loops through each Class (e.g., Business Object) in the current package and calls the PROCESS_CLASS template
  *  to generate the required code for the object.  Also recursively calls itself (the PROCESS_PACKAGE) template
  *  to process packages that are contained with this package.  Note: The first time that this template is called
  *  it is processing at the highest level within the Model and is not within a package.
-->
<xsl:template name="PROCESS_PACKAGE">
<xsl:for-each select="Foundation.Core.Namespace.ownedElement">
   <xsl:for-each select="Foundation.Core.Class">
      <xsl:call-template name="PROCESS_CLASS"/>
   </xsl:for-each>
   <xsl:for-each select="Model_Management.Package">
      <xsl:call-template name="PROCESS_PACKAGE"/>
   </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
  *  Generates an entry in the Manifest file for the class.
-->
<xsl:template name="PROCESS_CLASS">
<xsl:param name="PackageName"/>
<xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not($Generate='False')" >
Name: <xsl:value-of select="$ClassName"/>DD.ser
Enterprise-Bean: True
</xsl:if>
</xsl:template>


<xsl:include href="Preferences.xsl"/>

</xsl:stylesheet>