<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates the code for the userNewObject() method in the business object.  This method
*  is typically used to default the values of attributes when a new instance of a class is
*  created.  If an "Initial Value" for an attribute has been specified in the object model,
*  a setAttribute() will be generated for it within the userNewObject() method.
*
*  Note:  If the class does not have any attributes which need to be assigned default values,
*  then the code for the userNewObject() method will not be generated at all.
-->
<xsl:template name="NEW_OBJECT_PROCESSING">
<xsl:param name="Ancestor">BusinessObject</xsl:param>
<xsl:variable name="RegistrationCode"><xsl:call-template name="NEW_OBJECT"/></xsl:variable>
<xsl:if test="not($RegistrationCode='')">
  /* 
   * Perform processing for a New instance of the business object
   */
   protected void userNewObject() throws AmsException, RemoteException
   {  <xsl:if test="not($Ancestor='BusinessObject')">
      /* Perform any New Object processing defined in the Ancestor class */
      super.userNewObject();
      </xsl:if>
      <xsl:value-of select="$RegistrationCode"/>   }
</xsl:if>
</xsl:template>


<!--
*  Loops through each attribute for the object and calls the "DEFAULT_VALUE" template to determine whether or
*  not the attribute has an Initial (e.g., default) value specified for it.
-->
<xsl:template name="NEW_OBJECT">
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Attribute">
   <xsl:call-template name="DEFAULT_VALUE"></xsl:call-template>
</xsl:for-each>
</xsl:template>


<!--
*  If an Initial Value has been specified for the attribute, the setAttribute() method is called to initialize
*  the attribute to the default value.
-->
<xsl:template name="DEFAULT_VALUE">
<xsl:variable name="DefaultValue" select="Foundation.Core.Attribute.initialValue/Foundation.Data_Types.Expression/Foundation.Data_Types.Expression.body"/>
<xsl:if test="not($DefaultValue=null)">
      <xsl:variable name="LogicalName"   select="Foundation.Core.ModelElement.name"/>
      /* Default value for <xsl:value-of select="$LogicalName"/> */
      this.setAttribute("<xsl:value-of select="$LogicalName"/>", "<xsl:value-of select="$DefaultValue"/>");
</xsl:if>
</xsl:template>

</xsl:stylesheet>