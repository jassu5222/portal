package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.frame.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
 
//IR DUM050356373 - IValavala - 08/10/2012

public class AgentLockException extends AmsException {
   
    String agentId;
    String agentType;
  	String lockedbyAgentId;
    
    public AgentLockException(){super("This agent is not able to acquire the reservation");}
    public AgentLockException(String agentId, String agentType, String lockedbyAgentId){
        this.agentId = agentId;
        this.lockedbyAgentId = lockedbyAgentId;
        this.agentType = agentType;
    }
    
    public String AgentId()
    {
        return agentId;   
    }
    public String lockedbyAgentId()
    {
        return lockedbyAgentId;   
    }
    public String agentType()
    {
        return agentType;   
    }
  
}