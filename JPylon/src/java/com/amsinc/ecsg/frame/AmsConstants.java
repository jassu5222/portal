/*
 * @(#)AmsConstants
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;
/**
 * Various constants used throughout the framework are defined here to make
 * them centrally located. Use of these constants reduces the need to "hard
 * code" values during development.
 */
public class AmsConstants
{
    // Framework Object Classes
    public static final String BUSINESS_OBJECT = "BusinessObject";
    public static final String LISTING_OBJECT = "ListingObject";
    public static final String STANDARD_LIST = "STANDARD_LIST";
    // Framework Processing Codes
    public static final int SAVE = 1;
    public static final int VALIDATE = 2;
    public static final int DELETE = 3;
    public static final int HAS_CHANGES_PENDING = 4;
    // Attribute Classes
    public static final String STANDARD_ATTRIBUTE = "Attribute";
    // FRAME Error Code definitions
    public static final String INVALID_ATTRIBUTE = "ATT01";
    public static final String REQUIRED_ATTRIBUTE = "ATT02";
    public static final String INVALID_NUMBER_ATTRIBUTE = "ATT03";
    public static final String INVALID_DATE_ATTRIBUTE = "ATT04";
    public static final String IVALID_ASSOCIATION_ATTRIBUTE = "ATT05";
    public static final String SQL_ERROR = "ATT06";

    public static final String COMPONENT_SAVE_ERROR = "FRW01";
    // Debugging Codes
    public static final String DEBUG_SYSTEM_OUT = "1";
    public static final String DEBUG_DEBUG_FILE = "2";
    public static final String DEBUG_OFF = "0";
    // Encryption Algorithms
    public static final String NUMERIC_ENCRYPTION = "NumericEncryption";
    public static final String ALPHA_NUMERIC_ENCRYPTION = "AlphaNumericEncryption";
    // Unique ID Types
    public static final String OID = "ECSG";
    public static final String MESSAGE = "MESSAGE";
    public static final String BANKQUERY = "BANKQUERY";

    // Encoding
    //public static final String ISO_ENCODING = "ISO8859_1";
    public static final String UTF8_ENCODING = "UTF-8";

    public static final String ROW_LOCKED = "ROW_LOCKED"; //MDB Rel6.1 IR# PDUL012842185 1/28/11
}
