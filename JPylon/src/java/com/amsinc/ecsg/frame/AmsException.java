/*
 * @(#)AmsException
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

/**
 * Special subclass of the <code>Exception</code> class used for generic
 * framework specific exceptions.
 */
public class AmsException extends Exception
{
    private IssuedError issuedError;

    /**
     * Constructs an <code>AmsException</code> with no specified detail message.
     */
    public AmsException ()
    {
        super ();
    }
    
    //cquinton 10/27/2011 Rel 7.1 start
    /**
     * Constructs an <code>AmsException</code> with no specified detail message and a cause.
     * @param cause - chained exception
     */
    public AmsException (Throwable cause)
    {        
        super (cause);
    }
    //cquinton 10/27/2011 Rel 7.1 end

    /**
     * Constructs an <code>AmsException</code> with the specified detail
     * message.
     *
     * @param s - the detail message.
     */
    public AmsException (String s)
    {
        super(s);
    }
    
    //cquinton 10/27/2011 Rel 7.1 start
    /**
     * Constructs an <code>AmsException</code> with the specified detail
     * message and cause.
     *
     * @param s - the detail message.
     * @param cause - chained exception
     */    
    public AmsException(String s, Throwable cause) {
        super(s,cause);        
    }
    //cquinton 10/27/2011 Rel 7.1 end
    
    /**
     * Constructs an <code>AmsException</code> with the specified issued
     * error.
     *
     * @param error - the issued error.
     */
    public AmsException (IssuedError error)
    {
		issuedError = error;
    }
    /**
     * Getter for the issued error.
     *
     * @return The issued error.
     */
    public IssuedError getIssuedError ()
    {
        return issuedError;
    }
    /**
     * Setter for the issued error.
     *
     * @param error Value to set the issued error to.
     */
    public void setIssuedError (IssuedError error)
    {
       issuedError = error;
    }
}
