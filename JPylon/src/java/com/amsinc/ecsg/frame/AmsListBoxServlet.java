/*
 * @(#)AmsListBoxServlet
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import com.amsinc.ecsg.util.*;

public class AmsListBoxServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doTheWork(req, res);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doTheWork(req, res);
    }

    public void doTheWork(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");

        // Get a handle to the output stream
        PrintWriter out = res.getWriter();
        HttpSession session = req.getSession(true);
        ResourceManager resMgr = (ResourceManager) session.getAttribute("resMgr");

        // Determine what reference data table we are creating a list box from
        String tableType = req.getParameter("tableType");
        if (tableType == null) {
            throw new ServletException("Need to send the tableType parameter");
        }

        //Validation to check Cross Site Scripting	  
        tableType = StringFunction.xssCharsToHtml(tableType);

        // Determine what parameter name to use for this list box from
        String listBoxParmName = req.getParameter("listBoxParmName");
        if (listBoxParmName == null) {
            throw new ServletException("Need to send the listBoxParmName parameter");
        }

  //Validation to check Cross Site Scripting	  
        //Rel 9.2 XSS - CID 10030
        listBoxParmName = StringFunction.xssCharsToHtml(listBoxParmName);

        // Get the reference data and iterate through it to create the list box html
        Hashtable refData;
        try {
            if (resMgr != null) {
        //If a the locale has been set in the resMgr, use the locale to get back the appropriate
                //data from the reference data manager
                if (resMgr.getResourceLocale() != null) {
                    refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(tableType, resMgr.getResourceLocale());
                } else {
                    refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(tableType);
                }
            } else {
                refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(tableType);
            }
        } catch (AmsException ex) {
            throw new ServletException(ex.getMessage());
        }

        out.write("<select name=\"" + listBoxParmName + "\" size=\"1\">\n");

        for (Object o : refData.keySet()) {
            String code = (String) o;
            String descr = (String) refData.get(code);
            out.write("<option value=\"" + code + "\">" + descr + "</option>\n");
        }

        out.write("</select>\n");
        out.flush();
    }
}
