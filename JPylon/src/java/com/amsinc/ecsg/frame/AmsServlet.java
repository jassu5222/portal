package com.amsinc.ecsg.frame;

/*
 * @(#)AmsServlet
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.*;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class AmsServlet extends HttpServlet {
	private static final Logger LOG = LoggerFactory.getLogger(AmsServlet.class);

	/**
	 * The EJB server location (IP:port)
	 */
	private String serverLocation;
	/**
	 * The location of the XSL style sheet files
	 */
	private String xslFileLocation;
	/**
	 * The debug mode setting for the web server framework
	 */
	private int debugMode;
	/**
	 * An indicator stating whether the jsp:forward is being used. If yes, then the BeanManager is informed of the next page, and
	 * the JSP does a jsp:forward. If no, then this servlet does a sendRedirect method call.
	 */
	private boolean jspForwardOn = true;
	/**
	 * A count of the number of subsequent pages navigation information should be stored for
	 */
	private int pageExpirationCount;
	/**
	 * The EJB Vendor
	 */
	private String EJBVendor;

	/**
	 * Load Perf Stat Config indicator
	 */
	private String loadPerfStatConfig;

	/**
	 * This method implements the standard servlet API for GET requests. It invokes the doTheWork method to do as the name says.
	 *
	 * @param req
	 *            the servlet request object
	 * @param res
	 *            the servlet response object
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doTheWork(req, res);
	}

	/**
	 * This method implements the standard servlet API for POST requests. It invokes the doTheWork method to do as the name says.
	 *
	 * @param req
	 *            the servlet request object
	 * @param res
	 *            the servlet response object
	 */
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doTheWork(req, res);
	}

	/**
	 * This method does the primary work of the servlet. Both get's and post's are processed by this method.
	 *
	 * @param req
	 *            the servlet request object
	 * @param res
	 *            the servlet response object
	 */
	public void doTheWork(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// Insure that navigation manager is active
		NavigationManager nif = NavigationManager.getNavMan();
		if (nif == null) {
			nif = NavigationManager.getNavMan(this.getDebugMode());
		}

		// Obtain a handle to the user's session and the Bean Manager for their
		// session. The BeanManager will have the servlet invocation information.
		// Otherwise, we will look at the http request parameters to get the information
		HttpSession session = req.getSession(true);
		AmsServletInvocation reqInfo = null;
		BeanManager beanMgr = (BeanManager) session.getAttribute("beanMgr");
		FormManager formMgr = (FormManager) session.getAttribute("formMgr");
		ResourceManager resMgr = (ResourceManager) session.getAttribute("resMgr");
		if (formMgr == null) {
			LOG.warn("AmsServlet WARNING: The Form Manager could not be obtained from the session object with id formMgr");
			LOG.warn("AmsServlet Add a jsp:useBean tag for FormManager to your JSP page calling AmsServlet");
			// Create a servlet invocation info object from the http parameters
			reqInfo = this.getServletInvocationParms(req, new AmsServletInvocation());
		} else {
			// There may be more than one form on the page, so check for the
			// navigation id parameter to indicate what form was submitted.
			// First, check for an override from the application. This would happen
			// in the case of a next form.
			String navId = null;
			if (formMgr.isNextFormNumber()) {
				navId = formMgr.getNextFormNumber();
			} else {
				navId = req.getParameter("ni");
			}
			if (navId != null) {
				try {
					reqInfo = this.getServletInvocationParms(req, formMgr.getServletInvocation(navId, req));
				} catch (AmsException e) {
					LOG.error("Exception caught in AMSServlet ServletInvocation: ", e);					
				}

			} else {
				// Create a servlet invocation info object from the http parameters
				reqInfo = this.getServletInvocationParms(req, new AmsServletInvocation());
			}
		}

		// Derive correct web action. There may be multiple possibilities based on what submit button the user clicked.
		reqInfo.setMediator(this.getMediator(req, reqInfo));

		// Write servlet parameters to debug if it is turned on

		LOG.debug("AmsServlet Parameters are: {}", reqInfo);

		try {
			XmlTransport transport = new XmlTransport(jspForwardOn);

			// Get the input and output parameter information for this particular transaction
			MediatorInfoFactory fact = MediatorInfoFactory.getMediatorInfoFactory();
			MediatorInfo medInfo = fact.getMediatorInfo(reqInfo.getMediator());
			Hashtable inputParmMap = medInfo.getInputParmMap().getParameterXmlPairs();

			DocumentHandler returnDoc = transport.callEjbComponent(reqInfo, beanMgr, formMgr, resMgr, getServerLocation(), req, res,
					inputParmMap);

			// Check if there are web beans we should populate with the data
			// returned from the transaction. This allows us to populate web beans
			// outside of using AmsListWebBean or AmsEntityWebBean.
			DocumentHandler outputSectionOfDoc = returnDoc.getComponent("/Out");
			if (reqInfo.isCallEjb()) {
				transferDataToBeans(outputSectionOfDoc, reqInfo, beanMgr);
			}

		} catch (Exception e) {
			LOG.error("Exception caught in AMSServlet: ", e);
			throw new ServletException(e.getMessage());
		}

	}

	/**
	 * This method returns the debug mode configured for the servlet Values are: 0 - turned off, 1 - system out, 2 - log files
	 *
	 * @return the configured debug mode
	 */
	public int getDebugMode() {
		return debugMode;
	}

	/**
	 * This method returns the EJB Vendor string.
	 *
	 * @return the EJB Vendor
	 */
	public String getEJBVendor() {
		return EJBVendor;
	}

	/**
	 * This method parses the mediator parameter. The mediator parameter can take two forms: 1) MediatorName1 2)
	 * form:MediatorName1/MediatorName2/...
	 *
	 * The first format is straightforward, simply the name of the mediator. The second looks for parameters with the given mediator
	 * names concatenated together by '/' characters. This is used when mulitple submit buttons on the same form can call different
	 * mediators.
	 *
	 * @param req
	 *            the Servlet request object containing the http parameters
	 * @param res
	 *            the infrastructure request information object
	 * @return the web action derived from the request object
	 */
	protected String getMediator(HttpServletRequest req, AmsServletInvocation reqInfo) {
		String mediator = reqInfo.getMediator();
		if (mediator == null) {
			return null;
		}
		if (mediator.startsWith("form:")) {
			StringTokenizer tokenizer = new StringTokenizer(mediator.substring(5), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				String value = req.getParameter(token);
				if (value != null) {
					return token;
				}
			}
			return null;
		}
		return mediator;
	}

	/**
	 * This method returns the page expiration count. This is the number of pages serverd for which the framework will support the
	 * browser 'back' button. A reasonable number * for this is setting is less than 10. If you do not wish the user to be able to
	 * use the browser 'back' button to navigate successfully, set this value to 0.
	 *
	 * @return the page expiration count
	 */
	public int getPageExpirationCount() {
		return pageExpirationCount;
	}

	/**
	 * This method returns the EJB server location (ip:port)
	 *
	 * @return the EJB server location (ip:port)
	 */
	public String getServerLocation() {
		return serverLocation;
	}

	/**
	 * This method gets the http parameters for AmsServlet from the servlet request object. This method is used when the values for
	 * these parameters have not already been supplied to the BeanManager in a Java Server Page.
	 *
	 * @param req
	 *            the Servlet request object containing the http parameters
	 * @param reqInfo
	 *            the infrastructure request information object
	 * @return object filled with AmsServlet parameter values
	 */
	protected AmsServletInvocation getServletInvocationParms(HttpServletRequest req, AmsServletInvocation reqInfo) {
		if (reqInfo == null) {
			reqInfo = new AmsServletInvocation();
		}
		String mediator = req.getParameter("mediator");
		if (mediator != null) {
			reqInfo.setMediator(mediator);
		}

		String preMedWebAction = req.getParameter("preMedWebAction");
		if (preMedWebAction != null) {
			reqInfo.setPreMedWebAction(preMedWebAction);
		}
		String postMedWebAction = req.getParameter("postMedWebAction");
		if (postMedWebAction != null) {
			reqInfo.setMediator(postMedWebAction);
		}

		String mapFiles = req.getParameter("mapFiles");
		if (mapFiles != null) {
			reqInfo.setMapFiles(mapFiles);
		}
		String xslErrorSheet = req.getParameter("xslErrorSheet");
		if (xslErrorSheet != null) {
			reqInfo.setXslErrorSheet(xslErrorSheet);
		}
		String xslStyleSheet = req.getParameter("xslStyleSheet");
		if (xslStyleSheet != null) {
			reqInfo.setXslStyleSheet(xslStyleSheet);
		}
		String nextPage = req.getParameter("nextPage");
		if (nextPage != null) {
			reqInfo.setNextPage(nextPage);
		}
		String errorPage = req.getParameter("errorPage");
		if (errorPage != null) {
			reqInfo.setErrorPage(req.getParameter("errorPage"));
		}
		String addToInputCache = req.getParameter("addToInputCache");
		if (addToInputCache != null) {
			if (addToInputCache.equalsIgnoreCase("yes")) {
				reqInfo.setAddToInputCache(true);
			}
		}
		String beansToPopulate = req.getParameter("beansToPopulate");
		if (beansToPopulate != null) {
			reqInfo.setBeansToPopulate(beansToPopulate);
		}
		String beansAsInput = req.getParameter("beansAsInput");
		if (beansAsInput != null) {
			reqInfo.setBeansAsInput(beansAsInput);
		}
		String docCacheAsInput = req.getParameter("docCacheAsInput");
		if (docCacheAsInput != null) {
			reqInfo.setDocCacheAsInput(docCacheAsInput);
		}
		String docCacheName = req.getParameter("docCacheName");
		if (docCacheName != null) {
			reqInfo.setDocCacheName(docCacheName);
		}
		String multipleObjects = req.getParameter("multipleObjects");
		if (multipleObjects != null) {
			if (multipleObjects.equalsIgnoreCase("yes")) {
				reqInfo.setMultipleObjects(true);
			}
		}
		String strNumberOfMultipleObjects = req.getParameter("numberOfMultipleObjects");
		if (strNumberOfMultipleObjects != null) {
			int numberOfMultipleObjects = 0;
			try {
				numberOfMultipleObjects = Integer.parseInt(strNumberOfMultipleObjects);
			} catch (NumberFormatException e) {
				numberOfMultipleObjects = 0;
			}
			reqInfo.setNumberOfMultipleObjects(numberOfMultipleObjects);
		}

		// Vshah - 10/19/2010 - IR#VIUK101252424 - [BEGIN]
		String strNumberOfMultipleObjects1 = req.getParameter("numberOfMultipleObjects1");
		if (strNumberOfMultipleObjects1 != null) {
			int numberOfMultipleObjects1 = 0;
			try {
				numberOfMultipleObjects1 = Integer.parseInt(strNumberOfMultipleObjects1);
			} catch (NumberFormatException e) {
				numberOfMultipleObjects1 = 0;
			}
			reqInfo.setNumberOfMultipleObjects1(numberOfMultipleObjects1);
		}
		// Vshah - 10/19/2010 - IR#VIUK101252424 - [END]

		// Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		String strNumberOfMultipleObjects2 = req.getParameter("numberOfMultipleObjects2");
		if (strNumberOfMultipleObjects2 != null) {
			int numberOfMultipleObjects2 = 0;
			try {
				numberOfMultipleObjects2 = Integer.parseInt(strNumberOfMultipleObjects2);
			} catch (NumberFormatException e) {
				numberOfMultipleObjects2 = 0;
			}
			reqInfo.setNumberOfMultipleObjects2(numberOfMultipleObjects2);
		}
		// Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

		// Kyriba CR 268 Start - Added to get count of External Bank's
		String strNumberOfMultipleObjects3 = req.getParameter("numberOfMultipleObjects3");
		if (strNumberOfMultipleObjects3 != null) {
			int numberOfMultipleObjects3 = 0;
			try {
				numberOfMultipleObjects3 = Integer.parseInt(strNumberOfMultipleObjects3);
			} catch (NumberFormatException e) {
				numberOfMultipleObjects3 = 0;
			}
			reqInfo.setNumberOfMultipleObjects3(numberOfMultipleObjects3);
		}
		// Kyriba CR 268 End

		// Rel9.0 - numberOfMultipleObjects4 for Address fields
		String strNumberOfMultipleObjects4 = req.getParameter("numberOfMultipleObjects4");
		if (strNumberOfMultipleObjects4 != null) {
			int numberOfMultipleObjects4 = 0;
			try {
				numberOfMultipleObjects4 = Integer.parseInt(strNumberOfMultipleObjects4);
			} catch (NumberFormatException e) {
				numberOfMultipleObjects4 = 0;
			}
			reqInfo.setNumberOfMultipleObjects4(numberOfMultipleObjects4);
		}
		return reqInfo;
	}

	/**
	 * This method returns the location where the xsl style sheet files are stored.
	 *
	 * @return the location of the xsl style sheet files
	 */
	public String getXslFileLocation() {
		return xslFileLocation;
	}

	/**
	 * This method can be overridden by subclasses specific to an application server platform. If it is desired to create web page
	 * content from an application specific servlet component.
	 *

	 * @param request
	 *            the servlet request object
	 * @param response
	 *            the servlet response object
	 * @param doc
	 *            the output XML document
	 */
	public String handleOutputData(HttpServletRequest request, HttpServletResponse response, DocumentHandler outputDocument)
			throws ServletException, IOException {
		return null;
	}

	/**
	 * This method initializes the servlet. The intialization parameters are read and stored as properties of the servlet. The web
	 * server calls this method before it processes any requests with the servlet.
	 *
	 * @param ServletConfig
	 *            the configuration object for the servlet
	 * @return void
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		try {
			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			serverLocation = jPylonProperties.getString("serverLocation");

			try {
				xslFileLocation = jPylonProperties.getString("xslFileLocation");
			} catch (AmsException amse) {
				xslFileLocation = "";
			}

			String strPageExpirationCount = jPylonProperties.getString("pageExpirationCount");
			if (strPageExpirationCount == null) {
				pageExpirationCount = 0;
			} else {
				try {
					pageExpirationCount = Integer.parseInt(strPageExpirationCount);
				} catch (Exception e) {
					pageExpirationCount = 0;
				}
			}

			String strDebugMode = jPylonProperties.getString("debugMode");
			if (strDebugMode == null) {
				debugMode = 0;
			} else {
				try {
					debugMode = Integer.valueOf(strDebugMode).intValue();
				} catch (Exception e) {
					debugMode = 0;
				}
			}

			try {
				EJBVendor = jPylonProperties.getString("EJBVendor");
			} catch (AmsException amse) {
				// Default to Weblogic's
				EJBVendor = "Weblogic";
			}
			try {
				loadPerfStatConfig = jPylonProperties.getString("loadPerfStatConfig");
			} catch (AmsException amse) {
				// Default to Weblogic's
				loadPerfStatConfig = "N";
			}
			LOG.info("-------------------------------");
			LOG.info("AmsServlet configuration values");
			LOG.info("-------------------------------");
			LOG.info("Debug mode is {}" , strDebugMode);
			LOG.info("Using server location {}" , serverLocation);
			LOG.info("JSP Forwarding On: {}" , jspForwardOn);
			LOG.info("Page expiration Count: {}" , pageExpirationCount);
			LOG.info("-------------------------------");
		} catch (Exception e) {
		}

		// To read the config files, XMLConfigFileCache needs the ServletContext. Pass it in here
		XmlConfigFileCache.init(this.getServletContext());

		initializeResources(serverLocation);
	}

	/**
	 *
	 * @param serverLocation
	 *            java.lang.String
	 */
	public void initializeResources() {
		initializeResources(serverLocation);
	}

	/**
	 *
	 * @param serverLocation
	 *            java.lang.String
	 */
	public void initializeResources(String newServerLocation) {
		ReferenceDataManager.getRefDataMgr(newServerLocation);
		if ("Y".equalsIgnoreCase(loadPerfStatConfig)) {
			PerfStatConfigManager.getPerfStatConfigMgr(newServerLocation);
		}
	}

	/**
	 * This method returns true if jsp:forward is being used. This means that the servlet itself does not do the redirect, it sets
	 * the next page on the BeanManager and the jsp:forward does the redirect. If false, this servlet does the redirect.
	 *
	 * @return boolean An indication of jsp:forward being turned on
	 */
	public boolean isJspForwardOn() {
		return jspForwardOn;
	}

	/**
	 * This method looks for parameters that designate web beans that should be populated from the output data of this transaction.
	 * Is notified of the update.
	 *
	 * @param doc
	 *            the xml document returned from the mediator
	 * @param reqInfo
	 *            the infrastructure request information object
	 * @param beanMgr
	 *            a reference to the BeanManager infrastructure component
	 * @return void
	 */
	protected void transferDataToBeans(DocumentHandler doc, AmsServletInvocation reqInfo, BeanManager beanMgr) {
		if (reqInfo.getBeansToPopulate() == null) {
			return;
		}
		try {
			if (beanMgr == null) {
				return;
			}

			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getBeansToPopulate(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("AmsServlet transfer data to bean {}", token);
				try {
					AmsWebBean wb = (AmsWebBean) beanMgr.getBean(token);
					if (wb instanceof AmsListWebBean) {
						if (!(wb.populateFromXmlDoc(doc))) {
							wb.populateFromXmlDoc(doc, "/ResultSetRecord/");
						}
					} else if (!(wb.populateFromXmlDoc(doc))) {
						wb.populateFromXmlDoc(doc, "/");
					}
				} catch (Exception ex) {
				}
			}
		} catch (Exception e) {
		}
	}
}
