/*
 * @(#)Attribute
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

public class Attribute implements java.io.Serializable, Cloneable
{
 
   //---------------------------------------------------//
   // Properties
   //---------------------------------------------------//
 
   /**
    * The logical name of the attribute
    */
   private String attributeName;
   /**
    * The physical name of the attribute (usually a DBMS column name)
    */
   protected String physicalName;
   /**
    * The attribute class type (a valid descendant of the Attribute class).
    */
   protected String attributeType;
   /**
    * The current value of the attribute
    */
   protected String attributeValue = "";
   /**
    * Boolean indicator as to if the attribute is required
    */
    
   protected String alias;
   private transient boolean ibRequired = false;
   private String localeName = null;

   //---------------------------------------------------//
   // Object Methods
   //---------------------------------------------------//
 
	public Attribute ()
	{
	}

	public Attribute (String name, String physicalName)
	{
	      this.attributeName = name;
	      this.physicalName = physicalName;
	}

	public Attribute (String name, String physicalName, boolean required, String alias)
	{
	      this(name, physicalName);
	      this.ibRequired = required;
	      this.alias = alias;
	}

    public String getAlias ()
    {
          return this.alias; 
    }
   
    public void setAlias (String alias)
    {
          this.alias = alias; 
    }
   
   /**
    * Validates the value of the attribute.
    * 
    * This method is typically implemented in descendants of the 
    * standard attribute object.
    * 
    * @param attributeValue The value of the attribute being validated.
    */
   public void validate (String attributeValue)
          throws AmsException
   {
      // Return Success
   }

   //---------------------------------------------------//
   // Getter Methods
   //---------------------------------------------------//
 
   /**
    * Gets the current value of the attribute.
    * 
    * @return The value of the attribute
    */
   public String getAttributeValue ()
   { 
      return attributeValue; 
   }

   /**
    * Gets the logical attribute name as it was registered in the 
    * AttributeManager.
    * 
    * @return The logical name of the attribute
    */
   public String getAttributeName ()
   { 
      return attributeName;
   }

   /**
    * Gets the name of the Attribute Class that represents the attribute.
    * 
    * @return The name of the Attribute Class.
    */
   public String getAttributeType ()
   { 
      return attributeType;
   }

   /**
    * Gets the physical (usually DBMS column name) of the attribute as it
    * was registered in the AttributeManager.
    * 
    * @return The physical name of the attribute
    */
   public String getPhysicalName ()
   { 
      return physicalName; 
   }

   //---------------------------------------------------//
   // Setter Methods
   //---------------------------------------------------//
 
   /**
    * Sets the value of an attribute.  If validation is required, attribute
    * validation is performed.
    *
    * @param attributeValue The value to be set on the attribute.
    * @param validateValue Indicates if the value is to be validated.

    */
   public void setAttributeValue (String attributeValue, boolean validateValue)
          throws AmsException
   {
      // If the value being set is null, set it to an empty string
      if (attributeValue == null || attributeValue.trim().length() < 1) //Narayan-LIUK031741938
      {
         attributeValue = "";
      }
      // If validation is required, validate and then set the attribute
      if (validateValue)
      {
         this.validate (attributeValue);
         this.attributeValue = attributeValue;
      }
      else
      {
         this.attributeValue = attributeValue;
      }
   }
    
   /**
    * Sets the logical name of the attribute.
    * 
    * @param attributeName String The physical name of the attribute
    */
   public void setAttributeName (String attributeName)
   {
      this.attributeName = attributeName;
   }

   /**
    * Sets the physical name of the attribute, usually a DBMS 
    * column name.
    * 
    * @param physicalName String The physical name of the attribute
    */
   public void setPhysicalName (String physicalName)
   {
      this.physicalName = physicalName;
   }

   /**
    * Sets the attribute's Attribute Class.
    * 
    * The attribute type or class is used to dynamically create the 
    * correct Attribute descendant object when the attribute is
    * registered with the Attribute Manager.
    * 
    * @param attributeType String The name of the Attribute descendant class.
    */
   public void setAttributeType (String attributeType)
   {
      this.attributeType = attributeType;
   }

   /**
    * Identifies the attribute as required.
    */
   public void setRequired ()
   {
	//BSL IR BIUM050341259 05/10/2012 Rel 8.0 BEGIN
//      this.ibRequired = true;
//   }
	   setRequired(true);
   }
 
   /**
    * Identifies the attribute as required or not required, depending on the parameter
    * 
    * @param isRequired boolean
    */
   public void setRequired(boolean isRequired)
   {
      this.ibRequired = isRequired;
   }
   //BSL IR BIUM050341259 05/10/2012 Rel 8.0 END

   /**
    * Determines if the attribute is required.
    */
   public boolean isRequired ()
   {
      return this.ibRequired;
   }

   public String getLocaleName()
   {
      return this.localeName;
   }
   
   public void setLocaleName(String localeName)
   {
      this.localeName = localeName;
   }
}
