package com.amsinc.ecsg.frame;

/*
 * @(#)AttributeManager
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.rmi.*;

import java.util.*;

public class AttributeManager implements java.io.Serializable {
	/**
	 * The list of all the attributes registered within this attribute manager, keyed by logical name
	 */
	private Hashtable<String, Attribute> htRegisteredAttributes = new Hashtable<>();
	/**
	 * Contains the pysical name (key) and the logical name (element) attribute mappings
	 */
	private final Hashtable<String, String> htPhysicalLogical = new Hashtable<>();
	/**
	 * List of all the attributes that have had their value modified, keyed by physical name. These are attributes whose value has
	 * been modified since the last time the business object that owns these attributes was saved.
	 */
	private final Hashtable<String, Attribute> htModifiedAttributes = new Hashtable<>();

	/**
	 * List of the original values of all attributes that have been modified This will contain the value from the database
	 */
	private final Hashtable<String, String> htOriginalValues = new Hashtable<>();

	/** The business object related to this attribute manager */
	private BusinessObjectBean serviceRequestor = null;
	private String localeName = null;

	public AttributeManager(BusinessObjectBean requestorObject) {
		this.serviceRequestor = requestorObject;
	}

	/**
	 * Sets the locale
	 *
	 * @param localeName
	 *            The locale attribute.
	 */
	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	/**
	 * Retrieves an alias name for a registered attribute. The alias name is used in the error processing messages that are issued
	 * by the framework. Alias attributes provide a more "user-friendly" attribute name for error messages.
	 *
	 * 
	 * @param logicalAttributeName
	 *            The logical name of the attribute from the registerAttribute method.
	 * @return the Alias attribute name if one exists, otherwise <code>null
	 * </code>
	 */
	public String getAlias(String logicalAttributeName) {
		// Determine if the attribute has an alias
		if (!this.htRegisteredAttributes.containsKey(logicalAttributeName)) {
			return null;
		} else {
			Attribute tmpAttribute = this.htRegisteredAttributes.get(logicalAttributeName);
			return tmpAttribute.getAlias();
		}
	}

	/**
	 * Returns the Attribute manager's attribute hashtable. This is the list of all the registered attributes keyed by logical name.
	 *
	 * This method was provided to simply BusinessObject/DataObject communications in regards to Attribute manipulation.
	 * 
	 * @return The registered attribute Hashtable
	 */
	public Hashtable<String, Attribute> getAttributeHashtable() {
		return this.htRegisteredAttributes;
	}

	/**
	 * Returns the value for a requested registered attribute.
	 *
	 * This method first obtains a handle to the registered object using the logical attribute named passed in. Once the attribute
	 * is located, its value is returned.
	 *
	 * 
	 * @param attributeName
	 *            The logical name of the attribute.
	 * @return The String value of the attribute.
	 */
	public String getAttributeValue(String attributeName) throws AmsException, RemoteException {
		Attribute attribute = htRegisteredAttributes.get(attributeName);
		// If the attribute is not found, throw a processing exception
		if (attribute == null) {
			throw new AttributeNotRegisteredException(attributeName);
		}
		// Return the attribute's value
		if (attribute instanceof DoubleDispatchAttribute) {
			// Perform Double-Dispatch Processing
			return serviceRequestor.getAttribute(attribute.getPhysicalName());
		} else if (attribute instanceof ComputedAttribute) {
			// Call hook on the Business Object to compute the value of the
			// attribute
			return serviceRequestor.userGetAttribute(attribute.getPhysicalName());
		} else {
			return attribute.getAttributeValue();
		}
	}

	/**
	 * Returns the original value for a requested registered attribute. The original value is the first value assigned to the
	 * attribute when getting data from the database.
	 *
	 * This method first obtains a handle to the registered object using the logical attribute named passed in. Once the attribute
	 * is located, its value is returned.
	 *
	 * 
	 * @param attributeName
	 *            The logical name of the attribute.
	 * @return The String value of the attribute.
	 */
	public String getOriginalAttributeValue(String attributeName) throws AmsException, RemoteException {
		Attribute attribute = htRegisteredAttributes.get(attributeName);
		// If the attribute is not found, throw a processing exception
		if (attribute == null) {
			throw new AttributeNotRegisteredException(attributeName);
		}
		// Return the attribute's value
		if (attribute instanceof DoubleDispatchAttribute) {
			// Perform Double-Dispatch Processing
			return serviceRequestor.getAttributeOriginalValue(attribute.getPhysicalName());
		} else {
			// Look for original value in list of attributes that changed
			String originalValue = htOriginalValues.get(attribute.getPhysicalName());

			if (originalValue != null)
				return originalValue;
			else
				// Value hasn't changed, so just return the attribute's real value as the original
				return attribute.getAttributeValue();
		}
	}

	/**
	 * Returns the values for a requested registered attribute.
	 *
	 * This method first obtains a handle to the registered object using the logical attribute named passed in. Once the attribute
	 * is located, its value is returned.
	 *
	 * @param attributeNames
	 *            The list of logical attribute names to find the values of.
	 * @return An array of string values for the requested attributes.
	 * 
	 */
	public Hashtable<String, String> getAttributeValues(String[] attributeNames) throws AmsException, RemoteException {

		int count = attributeNames.length;

		Hashtable<String, String> nameValuePair = new Hashtable<>(count);

		for (String attributeName : attributeNames) {
			nameValuePair.put(attributeName, getAttributeValue(attributeName));
		}
		
		return nameValuePair;
	}

	/**
	 * Returns the AttributeManager's Hashtable of modified attributes.
	 *
	 * @return The modified attribute Hashtable.
	 * 
	 */
	public Hashtable<String, Attribute> getChanges() {
		return this.htModifiedAttributes;
	}

	/**
	 * Gets the ObjectIDAttribute that has been registered with the manager.
	 *
	 * This method returns a pointer to the actual attribute object so that attribute object methods can be called. The user of the
	 * method may be interested in one or more properties of the attribute object.
	 * 
	 * 
	 * @return Reference to the attrbiute registered as type ObjectIDAttribute.
	 * @exception AmsException
	 *                If there is no attribute of type ObjectIDAttribute registered.
	 */
	public Attribute getIDAttribute()

	{

		Enumeration<Attribute> attributes = htRegisteredAttributes.elements();
		// Find the attribute that has been registered as an "ObjectIDAttribute"
		while (attributes.hasMoreElements()) {
			Attribute attribute = attributes.nextElement();
			if (attribute instanceof ObjectIDAttribute) {
				return attribute;
			}
		}
		// The ObjectIDAttribute wasn't found, return an exception
		// W Zhu 8/27/12 Rel 8.1 PPX-173 do not throw exception. An object could have no ObjectIDAttribute defined. It cannot use
		// open() but otherwise is ok.
		// throw new AmsException ("No ObjectIDAttribute registered");
		return null;
	}

	/**
	 * Returns the number of attributes that have been modified since the last call of the attribute manager's resetModified method.
	 *
	 * 
	 * @return The number of modified attributes.
	 */
	public int getModifiedCount() {
		return htModifiedAttributes.size();
	}

	public Attribute getOptimisticLockAttribute() {

		Enumeration<Attribute> attributes = htRegisteredAttributes.elements();
		// Find the attribute that has been registered as an "OptimisticLockAttribute"
		while (attributes.hasMoreElements()) {
			Attribute attribute = attributes.nextElement();
			if (attribute instanceof OptimisticLockAttribute) {
				return attribute;
			}
		}
		// If OptimisticLockAttribute wasn't found then return null
		return null;
	}

	/**
	 * Gets the ParentIDAttribute that has been registered with the manager.
	 *
	 * This method returns a pointer to the actual attribute object so that attribute object methods can be called. The user of the
	 * method may be interested in one or more properties of the attribute object.
	 * 
	 * 
	 * @return Reference to the attribute registered as type ParentIDAttribute.
	 * @exception AmsException
	 *                If there is no attribute of type ParentIDAttribute registered.
	 */
	public Attribute getParentIDAttribute() throws AmsException {
		// Get the Attributes
		Enumeration<Attribute> attributes = htRegisteredAttributes.elements();
		// Find the attribute that has been registered as an "ParentIDAttribute"
		while (attributes.hasMoreElements()) {
			Attribute attribute = attributes.nextElement();
			if (attribute instanceof ParentIDAttribute) {
				return attribute;
			}
		}
		// The ParentIDAttribute wasn't found, return an exception
		throw new AmsException("No ParentIDAttribute registered");
	}

	/**
	 * Returns the physical attribute name based on the logical attribute name.
	 *
	 * @param logicalAttributeName
	 *            The logical name of the attribute to find the physical name for.
	 * @return The physical attribute name.
	 * @exception AmsException
	 *                If there is no attribute registered with the indicated logical name.
	 */
	public String getPhysicalName(String logicalAttributeName) throws AmsException {
		Attribute attribute = this.htRegisteredAttributes.get(logicalAttributeName);
		if (attribute != null) {
			return attribute.getPhysicalName();
		} else {
			throw new AmsException("The attribute: " + logicalAttributeName + " is not registered.");
		}
	}

	/**
	 * Gets the logical names of all attributes registered.
	 * 
	 * @return An array of logical attribute names
	 * 
	 */
	public String[] getRegisteredAttributes() {
		String names[] = new String[htRegisteredAttributes.size()];
		int i = 0;
		// Store the keys as an enumeration
		Enumeration<Attribute> attributes = htRegisteredAttributes.elements();
		// For each key, get the name and value
		while (attributes.hasMoreElements()) {
			Attribute attribute = attributes.nextElement();
			names[i] = attribute.getAttributeName();
			i++;
		}
		// Return the string array
		return names;
	}

	/**
	 * Returns the number of attributes that have been registered with the attribute manager.
	 *
	 * This method interogates the size of the attribute manager's RegisteredAttributes Hashtable and returns the value.
	 * 
	 * @return The number of registered attributes.
	 * 
	 */
	public int getRegisteredCount() {
		return htRegisteredAttributes.size();
	}

	/**
	 * Registers an alias name for a registered attribute. The alias name is used in the error processing messages that are issued
	 * by the framework. Alias attributes provide a more "user-friendly" attribute name for error messages.
	 *
	 * 
	 * @param logicalAttributeName
	 *            The logical name of the attribute to register an attribute for.
	 * @param aliasName
	 *            The attribute alias name.
	 * @exception AmsException
	 *                If there is no registered attribute with the specified logical name.
	 */
	public void registerAlias(String logicalAttributeName, String aliasName) throws AmsException {
		// Determine if the attribute is valid
		Attribute tmpAttribute = this.htRegisteredAttributes.get(logicalAttributeName);
		if (tmpAttribute == null) {
			throw new AmsException(
					"An alias cannot be registered for " + logicalAttributeName + " because the attribute is not registered");
		} else {
			// Add the Alias
			tmpAttribute.setAlias(aliasName);
		}
	}

	/**
	 * Registers the attribute as an association to another business object. Attributes registered as associations will ensure that
	 * the value being set for the attribute value relates to an existing instance of the specified object.
	 * 
	 * @param attributeName
	 *            The name of the attribute being registered
	 * @param physicalName
	 *            The physical (database column) name of the attribute
	 * @param associationObjectName
	 *            The name of the business object that is being associated to
	 * 
	 */
	public void registerAssociation(String attributeName, String physicalName, String associationObjectName) throws AmsException {
		this.registerAttribute(attributeName, physicalName, "AssociationAttribute");
		// Get the Attribute object and set it's
		AssociationAttribute attribute = (AssociationAttribute) htRegisteredAttributes.get(attributeName);
		// Set the Association Object Name
		attribute.setAssociationObjectName(associationObjectName);
	}

	public void registerAttribute(Attribute attribute) {
		htRegisteredAttributes.put(attribute.getAttributeName(), attribute);
		htPhysicalLogical.put(attribute.getPhysicalName(), attribute.getAttributeName());
	}

	/**
	 * Registers the attribute as a standard attribute (no type requried).
	 * 
	 * @param attributeName
	 *            The name of the attribute being registered
	 * @param physicalName
	 *            The physical (database column) name of the attribute
	 * 
	 */
	public void registerAttribute(String attributeName, String physicalName) throws AmsException {
		// Default the registration to a standard attribute
		this.registerAttribute(attributeName, physicalName, AmsConstants.STANDARD_ATTRIBUTE);
	}
	// ---------------------------------------------------//
	// Attribute registration Methods
	// ---------------------------------------------------//

	/**
	 * Registers an attribute with the attribute manager.
	 *
	 * This method creates a new entry in the RegisteredAttributes Hashtable and initializes its properties based on the arguments
	 * passed into the method.
	 *
	 * @param attributeName
	 *            The logical name of the attribute
	 * @param physicalName
	 *            The physicalName (database column) of the attribute
	 * @param attributeType
	 *            The type of attribute (STRING, Integer, etc..)
	 * 
	 */
	public void registerAttribute(String attributeName, String physicalName, String attributeType) throws AmsException {
		// Register Attribute from the default package name
		this.registerAttribute(attributeName, physicalName, attributeType, "com.amsinc.ecsg.frame.");
	}
	// ---------------------------------------------------//
	// Attribute registration Methods
	// ---------------------------------------------------//

	/**
	 * Registers an attribute with the attribute manager.
	 *
	 * This method creates a new entry in the RegisteredAttributes Hashtable and initializes its properties based on the arguments
	 * passed into the method.
	 *
	 * @param attributeName
	 *            The logical name of the attribute
	 * @param physicalName
	 *            The physicalName (database column) of the attribute
	 * @param attributeType
	 *            The type of attribute (STRING, Integer, etc..)
	 * @param packageName
	 *            Name of the java package the class representing the attribute is contained in.
	 * 
	 */
	public void registerAttribute(String attributeName, String physicalName, String attributeType, String packageName)
			throws AmsException {
		Object obj;

		// Create the attribute using the Attribute Factory
		try {
			if (packageName == null) {
				packageName = "com.amsinc.ecsg.frame.";
			}
			if (!packageName.endsWith(".")) {
				packageName = packageName.concat(".");
			}
			Class attributeClass = Class.forName(packageName + attributeType);
			obj = attributeClass.newInstance();
		} catch (Exception e) {
         throw new AmsException (
            "Attribute registration has failed for: LogicalName: " +
            attributeName + " PhysicalName: " + physicalName +
            " AttributeType: " + attributeType +
            ".  Ensure that the attributeType is a valid attribute class.");
		}
		// Type cast the object into an attribute
		Attribute attribute = (Attribute) obj;
		// Set the attribute's logical name
		attribute.setAttributeName(attributeName);
		// Set the attribute's physical name
		attribute.setPhysicalName(physicalName);
		// Set the attribute type (eventuall this will be objectified)
		attribute.setAttributeType(attributeType);
		// Make the appropriate entries to the Attribute Hash Tables
		attribute.setLocaleName(localeName);
		htRegisteredAttributes.put(attributeName, attribute);
		htPhysicalLogical.put(physicalName, attributeName);
	}

	/**
	 * Registers the attribute as a reference to a valid value within the frame's ReferenceDataManager. Attributes registered as
	 * referenceAttributes will ensure that the value being passed to the attribute is a valid entry within the application's
	 * reference data, based on the code value and type parameters passed to this method.
	 *
	 * @param attributeName
	 *            The logical name of the attribute
	 * @param physicalName
	 *            The DBMS name of the attribute
	 * @param referenceType
	 *            The name of the reference type (i.e., CustType)
	 * @exception AmsException
	 *                If there is no registered attribute with the specified logical name.
	 * 
	 */
	public void registerReferenceAttribute(String attributeName, String physicalName, String referenceType) throws AmsException {
		this.registerAttribute(attributeName, physicalName, "ReferenceAttribute");
		// Get the Attribute object and set it's association object name
		ReferenceAttribute attribute = (ReferenceAttribute) htRegisteredAttributes.get(attributeName);
		// Set the Association Object Name
		attribute.setReferenceTypeName(referenceType);
	}
	// ---------------------------------------------------//
	// Attribute Validation methods
	// ---------------------------------------------------//

	/**
	 * Identifies an attribute as a required attribute.
	 *
	 * If an attribute is identified as "required", when the object is saved, the attribute must have a value set. If a value is not
	 * set, the attribute will not pass validation and the object will not be saved.
	 *
	 * 
	 * @param attributeName
	 *            The logical name of the attribute.
	 * @see #validateRequired()
	 */
	public void requiredAttribute(String attributeName) throws AmsException {
		requiredAttribute(attributeName, true);
	}

	/**
	 * Identifies an attribute as required or not required.
	 *
	 * If an attribute is identified as "required", when the object is saved, the attribute must have a value set. If a value is not
	 * set, the attribute will not pass validation and the object will not be saved.
	 *
	 * @param attributeName
	 *            The logical name of the attribute.
	 * @param isRequired
	 *            boolean
	 * @see #validateRequired()
	 */
	public void requiredAttribute(String attributeName, boolean isRequired) throws AmsException {
		// Ensure that the attribute has been registered
		if (!htRegisteredAttributes.containsKey(attributeName)) {
			throw new AttributeNotRegisteredException(attributeName);
		}
		// Get a handle to the attribute and set it to required
		Attribute attributeObject = htRegisteredAttributes.get(attributeName);
		attributeObject.setRequired(isRequired);
	}

	/**
	 * Populates a list of attribute names that have been identified as "required" and DO NOT have a value.
	 *
	 * This method is used by business objects that have been deactivated and thus should not have validations performed.
	 *
	 * @return A list of required attribute logical names that have no value.
	 */
	public List<String> getRequired() {
		List<String> requiredAttributes = new ArrayList<>();
		// For each attribute, determine if it has been registered as required
		Collection<Attribute> registeredAttributes = htRegisteredAttributes.values();
		for (Attribute attributeObject : registeredAttributes) {
			if (attributeObject.isRequired()) {
				// If the attribute is required and does not have a value, place
				// it in the requiredAttributes list
				if (attributeObject.getAttributeValue().length() < 1) {
					requiredAttributes.add(attributeObject.getAttributeName());
				}
			}
		}

		return requiredAttributes;
	}

	/**
	 * Set required attributes without values to not required so they will not produce validation errors.
	 */
	public void clearRequired() throws AmsException {
		List<String> requiredAttributes = getRequired();
		for (String attribute : requiredAttributes) {
			requiredAttribute(attribute, false);
		}
	}
	// BSL IR BIUM050341259 05/10/2012 Rel 8.0 END

	/**
	 * This method clears the attribute managers Hashtable of modified attributes. This is typically called once an object has saved
	 * it's current attribute set successfully.
	 *
	 * 
	 */
	public void resetModified() {
		this.htModifiedAttributes.clear();
		this.htOriginalValues.clear();
	}

	/**
	 * Allows "mass" setting of the list of attributes for this attribute manager. Rather than adding attributes one at a time, sets
	 * the entire list all at once.
	 *
	 * @param htAttributes
	 *            The list of attributes to assign to the attribute manager.
	 */
	public void setAttributeHashtable(Hashtable<String, Attribute> htAttributes) {
		this.htRegisteredAttributes = htAttributes;
	}
	// ---------------------------------------------------//
	// Mutator (Setter) Methods
	// ---------------------------------------------------//

	/**
	 * Sets the value of a specified attribute name.
	 *
	 * If the attribute value passes validation, the attribute is added to the "Modified" hashtable.
	 *
	 * 
	 * @param attributeName
	 *            The logical name of the attribute.
	 * @param attributeValue
	 *            The value that correspond to the specified attribute name.
	 * @exception AttributeNotRegisteredException
	 *                If there is no registered attribute with the specified logical name.
	 * @exception InvalidObjectAssociationException
	 */
	public void setAttributeValue(String attributeName, String attributeValue) throws RemoteException, AmsException {
		Attribute attribute = htRegisteredAttributes.get(attributeName);
		// If the attribute is not valid, throw an exception
		if (attribute == null) {
			throw new AttributeNotRegisteredException(attributeName);
		}
		// If the attribute is an ObjectIdentifier, throw an exception
		// if (attribute instanceof ObjectIDAttribute)
		// {
		// throw new ObjectIdentifierAttributeException (attributeName);
		// }
		// If the attribute is a double-dispatched attribute (relating to a
		// component) object, perform the necessary processing to set the
		// attribute
		if (attribute instanceof DoubleDispatchAttribute) {
			serviceRequestor.setAttribute(attribute.getPhysicalName(), attributeValue);
			return;
		} else if (attribute instanceof ComputedAttribute) {
			// Computed Attributes cannot be set!
			return;
		} else if (attribute instanceof LocalAttribute) {
			// Set the attribute value, but don't add to modified list
			attribute.setAttributeValue(attributeValue, false);
			return;
		} else {
			// Don't perform any processing for attribute values that haven't
			// really changed from their present state. This change was added to
			// facilitate HTML processing.
			if (attribute.getAttributeValue().equals(attributeValue)) {
				// No processing necessary, exit processing
				return;
			}
		}
		// If the attribute is an assocation, perform association processing
		if (attribute instanceof AssociationAttribute) {
			if ((attributeValue != null) && (attributeValue.length() != 0)) {
				AssociationAttribute association = (AssociationAttribute) attribute;
				if (!serviceRequestor.checkObjectAssociation(association.getAssocationObjectName(),
						Long.parseLong(attributeValue))) {
					throw new InvalidObjectAssociationException(attributeName, attributeValue);
				}
			}
		}

		// Add the attribute to the list of modified attributes if it is not
		// already there
		if (!htModifiedAttributes.containsKey(attribute.getPhysicalName())) {
			htModifiedAttributes.put(attribute.getPhysicalName(), attribute);
			// Store the original value also
			htOriginalValues.put(attribute.getPhysicalName(), attribute.getAttributeValue());
		}

		// Set the attribute value
		attribute.setAttributeValue(attributeValue, true);

	}

	/**
	 * Checks to see if the attribute has been registered
	 * 
	 * @param attributeName
	 *            The logical name of the attribute being examined.
	 * @return True/False based on whether or not the attribute has been registered.
	 */
	public boolean isAttributeRegistered(String attributeName) {
		return htRegisteredAttributes.containsKey(attributeName);
	}

	/**
	 * Checks to see if the attribute has been modified
	 * 
	 * @param attributeName
	 *            The name of the attribute being examined.
	 * @return True/False based on whether or not the attribute has been modified.
	 */
	public boolean isAttributeModified(String attributeName) throws AmsException {
		return htModifiedAttributes.containsKey(this.getPhysicalName(attributeName));
	}
	// ---------------------------------------------------//
	// DataObject synchronization methods
	// ---------------------------------------------------//

	/**
	 * This method is used for DataObject synchronization. The DataObject uses this method to load the attribute manager's
	 * attribute's with their corresponding values. Using the physicalNames/Values string arrays, each registered attribute's value
	 * is set.
	 *
	 * @param physicalNames
	 *            List of the physical names of the attributes to set value for.
	 * @param values
	 *            List of the values to assign to the attributes.
	 * 
	 */
	public void setData(String physicalNames[], String values[]) throws AmsException {
		int attributeCount = physicalNames.length;
		for (int i = 0; i < attributeCount; i++) {
			// Get the logical attribute name based on the physical name provided
			String logicalName = htPhysicalLogical.get(physicalNames[i]);
			// Get a handle to the attribute and set its value
			Attribute attribute = htRegisteredAttributes.get(logicalName);
			attribute.setAttributeValue(values[i], false);
		}
	}

	/**
	 * Populates a vector of attribute names that have been identified as "required" and DO NOT have a value.
	 *
	 * This method is used by the BusinessObjectBean to interrogate the values of required attributes. If a required attribute does
	 * not have a value, an exception in thrown in the business object's save method.
	 *
	 * @return A vector of required attribute logical names that have no value. Note that if the attribute has an alias registered
	 *         the alias is returned instead of the logical name.
	 * 
	 */
	public List<String> validateRequired() {
		List<String> requiredAttributes = new ArrayList<>();
		// For each attribute, determine if it has been registered as required
		Enumeration<Attribute> registeredAttributes = this.htRegisteredAttributes.elements();

		while (registeredAttributes.hasMoreElements()) {
			// For each required attribute, ensure that a value has been set
			Attribute attributeObject = registeredAttributes.nextElement();
			if (attributeObject.isRequired()) {
				// If the attribute is required and does not have a value, place
				// it in the requiredAttribute Vector
				if (attributeObject.getAttributeValue().length() < 1) {
					String aliasName = this.getAlias(attributeObject.getAttributeName());
					// If an alias exists for the attribute, use the alias name
					if (aliasName != null) {
						requiredAttributes.add(aliasName);
					} else // otherwise use the logical name
					{
						requiredAttributes.add(attributeObject.getAttributeName());
					}
				}
			}
		}
		// Return the vector of required attributes
		return requiredAttributes;
	}
}
