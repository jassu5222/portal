/*
 * @(#)BlobAttribute
 *
 * Copyright � 1999 American Management Systems, Inc. ,
 * 4000 Legato Road, Fairfax, Virginia 22033, U.S.A.
 * All Rights Reserved.
 *
 * Permission is granted to use this software as specified by the AMS 
 * COMMERCIAL LICENSE AGREEMENT.  You may use and modify this software only for
 * commercial purposes, as specified in the details of the license.
 * AMERICAN MANAGEMENT SYSTEMS SHALL NOT BE LIABLE FOR ANY  DAMAGES SUFFERED BY
 * THE LICENSEE AS A RESULT OF USING OR MODIFYING THIS SOFTWARE IN ANY WAY.
 *
 * YOU MAY NOT DISTRIBUTE ANY SOURCE CODE OR OBJECT CODE FROM THE ECSG
 * FRAMEWORK AT ANY TIME. VIOLATORS WILL BE PROSECUTED TO THE FULLEST EXTENT
 * OF UNITED STATES LAW.
 *
 * @version 1.0, 04/27/2000

 */
package com.amsinc.ecsg.frame;

/**
 *
 */
public class BlobAttribute extends Attribute{
    
	public BlobAttribute ()
	 {
     }

	public BlobAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public BlobAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }    
}   