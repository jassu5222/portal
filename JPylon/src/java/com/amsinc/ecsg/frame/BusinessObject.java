/*
 * @(#)BusinessObject
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.math.BigDecimal;
import java.rmi.*;
import javax.ejb.*;
import java.util.*;
import com.amsinc.ecsg.util.*;

public interface BusinessObject extends EJBObject
{
  // Object Properties
  public EJBObject createServerEJB (String objectName)
       throws AmsException, RemoteException;
  public EJBObject createServerEJB (String objectName, long oid)
       throws AmsException, RemoteException;
  public void setToComponent()
       throws RemoteException;
  public boolean isComponent()
       throws RemoteException;
  public boolean isNewObject()
       throws RemoteException;
  public String getDataTableName()
       throws RemoteException;
  public boolean isAttributeModified (String attributeName)
       throws RemoteException, AmsException;
  public String[] getRegisteredAttributes()
       throws RemoteException;
  public String getAttribute(String attributeName) 
       throws RemoteException, AmsException;
  public String getAttributeOriginalValue(String attributeName) 
       throws RemoteException, AmsException;
  public byte[] getAttributeBytes (String attributePath)
        throws RemoteException, AmsException;
  public long getAttributeLong(String attributeName)
       throws RemoteException, AmsException;
  public int getAttributeInteger(String attributeName)
       throws RemoteException, AmsException;
  public Date getAttributeDate(String attributeName)
       throws RemoteException, AmsException;
  public Date getAttributeDateTime(String attributePath) 
       throws AmsException, RemoteException;
  public BigDecimal getAttributeDecimal(String attributePath) 
       throws AmsException, RemoteException;
  public Hashtable<String,String> getAttributes(String[] attributeNames)
       throws RemoteException, AmsException;
  public void setAttribute(String attributeName, String attributeValue)
       throws RemoteException, AmsException;
  public void setAttributeBytes(String attributeName, byte[] attributeValue)
       throws RemoteException, AmsException;
  public void setAttributes(String[] attributeNames, String[] attributeValues)
       throws RemoteException, AmsException;
  public Hashtable<String, Attribute> getAttributeHash()
       throws RemoteException;
  public String getIDAttributeName()
       throws RemoteException, AmsException;
  public String getParentIDAttributeName(boolean physicalName)
       throws RemoteException, AmsException;
  // Object persistence methods
  public boolean hasChangesPending()
       throws RemoteException;
  public int save() 
      throws RemoteException, AmsException;      
  public int save(boolean shouldValidate) 
      throws RemoteException, AmsException;
  public void getData(long objectID)
       throws RemoteException, AmsException;
  public void find(String attributeName, String attributeValue) 
       throws AmsException, RemoteException;
  public long newObject()
       throws RemoteException, AmsException;
  public int delete()
       throws RemoteException, AmsException;
  public int preDelete()
       throws RemoteException, AmsException;
  public int validate()
       throws RemoteException, AmsException;
  // jgadela R8.3 CR 501 04/15/2013 begin
  public int validate(boolean callPreSave)
  throws RemoteException, AmsException;
  //jgadela R8.3 CR 501 04/15/2013 begin
  // Component Management Methods
  public void registerComponent(String componentIdentifier, String componentName, String componentType, String idAttribute)
       throws RemoteException, AmsException;
  public long newComponent(String componentIdentifier)
       throws RemoteException, AmsException;
  public int deleteComponent(String componentIdentifier)
       throws RemoteException, AmsException;
  public EJBObject getComponentHandle(String componentIdentifier)
       throws RemoteException, AmsException;
  public EJBObject getComponentHandle(String componentIdentifier, long objectId)
       throws RemoteException, AmsException;
  public String getComponentType(String componentIdentifier)
       throws RemoteException, AmsException;
  // List Processing Methods
  public String getListCriteria(String listTypeName)
       throws RemoteException;
  public void populateObjectFromList(long objectID)
       throws RemoteException, AmsException;
  // Document Manipulation Methods
  public DocumentHandler populateXmlDoc(DocumentHandler doc, String componentPath)
       throws RemoteException, AmsException;
  public DocumentHandler populateXmlDoc(DocumentHandler doc)
       throws RemoteException, AmsException;
  public void populateFromXmlDoc(DocumentHandler doc, String componentPath, XMLTagTranslator contentMap)
       throws RemoteException, AmsException;
  public void populateFromXmlDoc(DocumentHandler doc, String componentPath)
       throws RemoteException, AmsException;
  public void populateFromXmlDoc(DocumentHandler doc)
       throws RemoteException, AmsException;
  // Error Management Methods  
  public ErrorManager getErrorManager()
       throws RemoteException; 
  public ResourceManager getResourceMgr ()   
       throws RemoteException;  
  public List<IssuedError> getIssuedErrors()
       throws RemoteException;
  // Object Association Methods
  public boolean checkAssociation(long objectID)
       throws RemoteException, AmsException;
  public void setClientServerDataBridge(ClientServerDataBridge csdb)
       throws RemoteException;    
  public boolean isAttributeRegistered (String attributeName)
       throws RemoteException, AmsException;
  //IAZ 02/02/09 optLockRetry
  public void setRetryOptLock(boolean retryOptLockOnEx)
       throws RemoteException, AmsException;
  public boolean getRetryOptLock()
       throws RemoteException, AmsException;
  //IAZ 02/02/09 optLockRetry
  //IR#T36000019323
  public int getMaxErrorSeverity()
	       throws RemoteException, AmsException;
}