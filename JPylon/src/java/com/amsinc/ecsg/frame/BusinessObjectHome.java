/*
 * @(#)BusinessObjectHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;

public interface BusinessObjectHome extends EJBHome
{
  public BusinessObject create()
    throws RemoteException, CreateException, AmsException;
  public BusinessObject create(ClientServerDataBridge csdb)
    throws RemoteException, CreateException, AmsException;
}

