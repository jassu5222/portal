/*
 * @(#)ClientServerDataBridge
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.*;

public class ClientServerDataBridge implements java.io.Serializable  {

    //Hashtable to store values 
    public Hashtable htCSDBValues;
    
    //Create a new hashtable
    public ClientServerDataBridge() {
        htCSDBValues = new Hashtable();
    }

    public ClientServerDataBridge(ClientServerDataBridge csdb)
     {
        htCSDBValues = new Hashtable();

        if(csdb != null)
         {
	        Enumeration enumer = csdb.htCSDBValues.keys();

	        while(enumer.hasMoreElements())
	         {
	            String key = (String) enumer.nextElement();
	
	            this.setCSDBValue(key, csdb.getCSDBValue(key));
	         }
          }
     }

    
    //Return the value from htCSDBValues keyed by csdbKey
    public String getCSDBValue (String csdbKey) {
        return (String) this.htCSDBValues.get(csdbKey);
    }
    
    //Set a value in htCSDBValues keyed by csdbKey.  If an object already
    //exists with the key we will over right the value
    public void setCSDBValue (String csdbKey, String csdbValue) {
        Object tempObject = this.htCSDBValues.get("csdbKey");
        if (tempObject != null) {
            this.htCSDBValues.remove("csdbKey");
        }
        this.htCSDBValues.put(csdbKey, csdbValue);
    }

    //Left in for legacy purposes    
    public String getLocaleName() {
        return this.getCSDBValue("localeName");
    }
    
    //Left in for legacy purposes    
    public void setLocaleName(String localeName) {      
        this.setCSDBValue("localeName", localeName);
    }


}