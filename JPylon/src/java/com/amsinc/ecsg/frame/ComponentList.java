/*
 * @(#)ComponentList
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;
import java.util.*;
import com.amsinc.ecsg.util.DocumentHandler;

public interface ComponentList extends GenericList
{  
    public void setAttribute(String attributePath, String attributeValue) 
         throws RemoteException, AmsException;
    public String getListAttribute(String attributeName)
         throws RemoteException, AmsException;
    public Hashtable getListAttributes(String[] attributeNames, Long objectID)
         throws RemoteException, AmsException;
    public int save()
         throws RemoteException, AmsException;
    public int validate()
         throws RemoteException, AmsException;
    public long newComponent(String componentPath)
         throws RemoteException, AmsException;
    public void setParentObjectID(String objectID)
         throws RemoteException;
    public int preDelete()
         throws RemoteException, AmsException;
    public int delete()
         throws RemoteException, AmsException;
    public List<IssuedError> getIssuedErrors()
         throws RemoteException;
	public void scrollToObjectByIndexReadOnly (int index)
		   throws RemoteException, AmsException;
	public String getAttributeReadOnly (String attributeName)
		   throws RemoteException, AmsException;
    public BusinessObject getComponentObject(long objectID)
         throws RemoteException, AmsException;
    public boolean hasChangesPending()
       throws RemoteException, AmsException;    
   // Document Manipulation Methods
    public DocumentHandler populateXmlDoc(DocumentHandler doc, String componentPath)
         throws RemoteException, AmsException;
    public DocumentHandler populateXmlDoc(DocumentHandler doc)
         throws RemoteException, AmsException;
    public void populateFromXmlDoc(DocumentHandler doc, String componentPath)
         throws RemoteException, AmsException;
    public void populateFromXmlDoc(DocumentHandler doc)
         throws RemoteException, AmsException;
    public void setClientServerDataBridge(ClientServerDataBridge csdb)
         throws RemoteException;     
}
