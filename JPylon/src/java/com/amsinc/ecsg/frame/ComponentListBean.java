/*
 * @(#)ComponentListBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import javax.ejb.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

/**
 * The ComponentListBean is a subclass of the GenericListBean which provides basic listing services for standard business objects.
 * 
 * The ComponentListBean provides functionality for processing Listing objects as business object components within a business
 * object's component management service. ComponentListBean's are updatable, whereas the GenericListBean is used only for data
 * retrieval/interrogation.
 *

 * @version 1.0
 */
public class ComponentListBean extends GenericListBean {
	private static final Logger LOG = LoggerFactory.getLogger(ComponentListBean.class);
	// Object Properties
	protected transient BusinessObject currentBusinessObject = null;
	protected transient Hashtable<Long, BusinessObject> htInstantiatedComponents = new Hashtable<>();
	protected transient Hashtable<Integer, Long> htNewComponents = new Hashtable<>();
	protected transient String parentObjectID;

	// -------------------------------------------------//
	// Overridden EJB Methods //
	// -------------------------------------------------//

	public void ejbRemove() {
		// Clean-up all EJB components created
		Enumeration<BusinessObject> businessObjects = htInstantiatedComponents.elements();
		while (businessObjects.hasMoreElements()) {
			BusinessObject boInterface = businessObjects.nextElement();
			// If the business object has been instantiated, remove it's
			// reference
			if (boInterface != null) {
				try {
					boInterface.remove();
				} catch (RemoteException | RemoveException re) {
					LOG.error("Errors on remove from list", re);
				}
			}
			// Nullify the BO Interface
			boInterface = null;
		}
		// Clear out the business object from the Hash
		this.htInstantiatedComponents.clear();
		// Clear out the new components hash
		this.htNewComponents.clear();
	}

	/**
	 * Sets the ListingObject's parentObjectID.
	 * 
	 * @param objectID
	 */
	public void setParentObjectID(String objectID) {
		this.parentObjectID = objectID;
	}

	/**
	 * Creates a new instance of the component in which the ComponentList manages and returns the newly generated objectID.
	 *
	 * The ComponentList manages one or more instances of a particular business object. This method creates a new instances of the
	 * particular business object and returns the newly generated object id.
	 * 

	 * @param componentPath
	 *            The path of the component to be created
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public long newComponent(String componentPath) throws RemoteException, AmsException {
		long newComponentOID;
		// Determine the top-level and remaining component paths
		String remainingComponentPath = StringFunction.removeFirstComponentEntry(componentPath);
		// If further component processing is necessary, process accordingly
		if (remainingComponentPath.length() > 0) {
			// Get the top-level object
			long objectID = StringFunction.parseIDFromComponentPath(StringFunction.getFirstComponentEntry(componentPath));
			BusinessObject objectInterface = this.getComponentObject(objectID);
			// Call the object's newComponent method with the remaining component path
			newComponentOID = objectInterface.newComponent(remainingComponentPath);
		} else {
			// No further process is necessary, create the component
			BusinessObject objectInterface = (BusinessObject) EJBObjectFactory.createServerEJB(this.myContext, this.objectName, this.csdb);
			// Call the object's newObject method
			newComponentOID = objectInterface.newObject();
			// Mark the object as a component
			objectInterface.setToComponent();
			// Set the object's parent relationship
			objectInterface.setAttribute(objectInterface.getParentIDAttributeName(false), this.parentObjectID);
			// Place the component object in the Hash
			htInstantiatedComponents.put(newComponentOID, objectInterface);
			// Place an entry into the New Components Hash. Place the index and its newComponentID value into the hash.
			htNewComponents.put(new Integer(this.getObjectCount()), newComponentOID);
		}
		// Return the new component's OID
		return newComponentOID;
	}

	/**
	 * Based on the objectID provided, this method returns a remoteInterface to a particular instance of the ComponentList's
	 * businessobject.
	 * 
	 * @exception AmsException
	 * @exception RemoteException
	 * @param objectID
	 *            The component object's ObjectID
	 */
	public BusinessObject getComponentObject(long objectID) throws RemoteException, AmsException {
		// If the component doesn't exist, create it
		if (!htInstantiatedComponents.containsKey(new Long(objectID))) {
			this.htInstantiatedComponents.put(new Long(objectID), this.instantiateBusinessObject(objectID));
		}

		// Return a handle to the component
		return htInstantiatedComponents.get(new Long(objectID));
	}

	// -------------------------------------------------//
	// Overridden Ancestor Methods //
	// -------------------------------------------------//

	/**
	 * Based on the row index passed in, this method scrolls the current object pointer to the row representing that object.
	 *
	 * @param index
	 *            The row index being scrolled to
	 * @exception AmsException
	 */
	public void scrollToObjectByIndex(int index) throws AmsException {
		long oid = 0;

		// If the index equates to a newComponent object, initialize the currentBusinessObject variable and scroll to it
		if (this.htNewComponents.containsKey(new Integer(index))) {
			// Get the objectID for the new component
			oid = this.htNewComponents.get(new Integer(index)).longValue();
		} else {
			// Requesting an object in the list that is NOT a new component
			// Need to determine OID for object based on the index passed in
			// The superclass stores this mapping in objectRowIndex (key is OID, value is index)
			Enumeration<Long> enumer = objectRowIndex.keys();
			while (enumer.hasMoreElements()) {
				// Loop through the list to find the OID that matches the index
				Long theObjectID = enumer.nextElement();
				Integer objectIndex = (Integer) objectRowIndex.get(theObjectID);
				if (objectIndex.intValue() == index) {
					oid = theObjectID.longValue();
					break;
				}
			}
		}

		// Once we have the objectID, use getComponentObject to look it up
		try {
			// Set the current object pointer to the BO interface
			this.currentBusinessObject = this.getComponentObject(oid);
		} catch (RemoteException reEx) {
			throw new AmsException("Errors scrolling to Object by Index", reEx);
		}
	}

	/**
	 * Based on the row index passed in, this method scrolls the current record pointer to the row representing that object. This
	 * method does not instantiate the object for the data it scrolls to. This essentially just invokes
	 * GenericList.scrollToObjectByIndex().
	 *
	 * @param index
	 *            The row index being scrolled to
	 * @exception AmsException
	 */
	public void scrollToObjectByIndexReadOnly(int index) throws AmsException {
		super.scrollToObjectByIndex(index);
	}

	/**
	 * Based on the unique object id passed in, this method scrolls the current object pointer to the row representing that object.
	 *
	 * @param objectID
	 *            The unique ObjectID being scrolled to
	 * @exception AmsException
	 */
	public void scrollToObject(long objectID) throws AmsException {
		// If the index equates to a newComponent object, initialize the
		// currentBusinessObject variable and scroll to it
		if (this.htNewComponents.contains(new Long(objectID))) {
			try {
				// Set the current object pointer to the BO interface
				this.currentBusinessObject = this.getComponentObject(objectID);
			} catch (RemoteException reEx) {
				throw new AmsException("Errors scrolling to Object by Index", reEx);
			}
		} else {
			super.scrollToObject(objectID);
		}
	}

	/**
	 * Returns the number entries found within the list once the getData method has been called. This method overrides the ancestor
	 * method. This method includes any components that were created internally via the newComponent() method.
	 *
	 * @exception AmsException
	 */
	public int getObjectCount() throws AmsException {
		// Return entries in the list and new components that have been created
		return super.getObjectCount() + this.htNewComponents.size();
	}

	/**
	 * This method indicates if any changes are pending for any of the component on the ComponentList.
	 *
	 * @return True if changes are pending, False if no changes are pending.
	 *
	 */
	public boolean hasChangesPending() throws RemoteException, AmsException {
		if (processComponent(AmsConstants.HAS_CHANGES_PENDING) < 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the attributevalue for a specified attribute.
	 * <p>
	 * <p>
	 * NOTE: Based on the form of the AttributePath, the ComponentList processes the getAttribute method differently. If a fully
	 * qualified path (i.e., /Account(436)/balance) is provided, then the Component instantiates the individual business object and
	 * returns the value of the requested attribute.
	 * <p>
	 * It is also valid for the getBusinessObject method in the GenericListBean class to call this method with just an attribute
	 * name (the name of the object's identifying attribute). In this case we also instantiate the individual business object and
	 * return the value of the identifying attribute.
	 * 
	 * @param attributePath
	 *            The attributePath for the requested attribute.
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public String getAttribute(String attributePath) throws RemoteException, AmsException {
		// Component process is required
		long objectID;
		String objectNameAndID;
		String newAttributePath;
		BusinessObject objectInterface = null;
		// Store the first component/id path
		objectNameAndID = StringFunction.getFirstComponentEntry(attributePath);
		// Check for fully qualified path by looking for a parenthesis. If we
		// do not find one, we assume we were called by getBusinessObject as
		// described in the method level comment.
		if (objectNameAndID.indexOf('(') > 0) {
			objectID = StringFunction.parseIDFromComponentPath(objectNameAndID);
			// If the component name and ID could not be determined, throw a
			// processing exception
			if (objectNameAndID.isEmpty() || objectID == 0) {
				throw new ComponentProcessingException("Component path and ID could not be determined from path: " + attributePath);
			}
			// Get a handle to the business object component
			objectInterface = this.getComponentObject(StringFunction.parseIDFromComponentPath(objectNameAndID));
			// Remove the First Component Entry path from the full attribute
			// path
			newAttributePath = StringFunction.removeFirstComponentEntry(attributePath);
		} else {
			// Get a handle to the business object component
			objectInterface = this.getComponentObject(Long.parseLong(getListAttribute(attributePath)));
			newAttributePath = attributePath;
		}
		// Return the value of the attribute
		return objectInterface.getAttribute(newAttributePath);
	}

	/**
	 * The getAttributes method has been overridden to return a null Hash. This has been done to override the functionality in
	 * generic list which is NOT applicable for ComponentListing objects.
	 * 
	 * @param attributeName
	 *            The attributes being requested
	 */
	public Hashtable<String, String> getAttributes(String[] attributeName) throws RemoteException, AmsException {
		Hashtable<String, String> nullHT = null;
		return nullHT;
	}

	/**
	 * Gets the value of an attribute for the object pointed by the current record pointer. <br>
	 * We do not instantiate the individual business object here. This essentially just invokes GenericList.getAttribute().
	 * 
	 * @param attributePath
	 *            The attributePath for the requested attribute.
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public String getAttributeReadOnly(String attributeName) throws RemoteException, AmsException {
		return super.getAttribute(attributeName);
	}

	/**
	 * Gets the value of an attribute for the current object pointed to in the list. This method uses the superclass method
	 * getAttribute. This method should only be called for read-only purposes since the component object is not instantiated.
	 *
	 * @param attributeName
	 *            The attributeName being requested
	 * @exception ComponentProcessingException
	 */
	public String getListAttribute(String attributeName) throws AmsException, RemoteException {
		// If the currentBusinessObject pointer is valid, get the attributes
		// from the business object interface, otherwise, call the ancestor
		// method
		if (this.currentBusinessObject != null) {
			return currentBusinessObject.getAttribute(attributeName);
		} else {
			return super.getAttribute(attributeName);
		}
	}

	/**
	 * Gets multiple attributes for the current object pointed to in the list
	 *
	 * @param attributeNames
	 *            The attributeNames being requested
	 * @exception ComponentProcessingException
	 * @exception RemoteException
	 */
	public Hashtable<String, String> getListAttributes(String[] attributeNames, Long objectID)
			throws AmsException, RemoteException {
		// If the currentBusinessObject pointer is valid, get the attributes
		// from the business object interface, otherwise, call the ancestor
		// method
		if ((this.currentBusinessObject != null) && htInstantiatedComponents.containsKey(objectID)) {
			return currentBusinessObject.getAttributes(attributeNames);
		} else {
			return super.getAttributes(attributeNames);
		}
	}

	/**
	 * Based on the attributePath and attributeValue, the ComponentList's businessobject's attribute is set.
	 * 
	 * @param attributePath
	 *            The attributePath for the requested attribute.
	 * @param attributeValue
	 *            The value that corresponds to the attribuePath.
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public void setAttribute(String attributePath, String attributeValue) throws RemoteException, AmsException {
		long objectID;
		String objectNameAndID;
		String newAttributePath;
		// Store the first component/id path
		objectNameAndID = StringFunction.getFirstComponentEntry(attributePath);
		objectID = StringFunction.parseIDFromComponentPath(objectNameAndID);
		// If the component name and ID could not be determined, throw a
		// processing exception
		if (objectNameAndID.isEmpty() || objectID == 0) {
			throw new ComponentProcessingException("Component path and ID could not be determined from path: " + attributePath
					+ " in method setComponentAttribute.");
		}
		// Get a handle to the business object component
		BusinessObject objectInterface = this.getComponentObject(StringFunction.parseIDFromComponentPath(objectNameAndID));
		// Remove the First Component Entry path from the full attribute path
		newAttributePath = StringFunction.removeFirstComponentEntry(attributePath);
		// Set the value of the attribute on the component object
		objectInterface.setAttribute(newAttributePath, attributeValue);
	}

	/**
	 * This method initiates save logic for all components that have been modified in the ComponentList. This method loops through
	 * all instantiated and modified business objects that are part of the component list and initiates the BusinessObject.save()
	 * method.
	 * 
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public int save() throws RemoteException, AmsException {
		return this.processComponent(AmsConstants.SAVE);
	}

	/**
	 * This method initiates validate logic for all components that have been modified in the ComponentList. This method loops
	 * through all instantiated and modified business objects that are part of the component list and initiates the
	 * BusinessObject.validate() method.
	 * 
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public int validate() throws RemoteException, AmsException {
		return this.processComponent(AmsConstants.VALIDATE);
	}

	/**
	 * This method initiates validate/save logic (depending on the processing type specified) for all components that have been
	 * modified in the ComponentList. This method loops through all instantiated and modified business objects that are part of the
	 * component list and initiates the appropriate BusinessObject method.
	 * 
	 * @see #validate()
	 * @see #save()
	 * @return 1 for Success, -1 for Failure
	 * @exception AmsException
	 * @exception RemoteException
	 */
	protected int processComponent(int processType) throws RemoteException, AmsException {
		boolean errorsFound = false;
		// Loop through each business object. If changes are pending, save
		// the changes to the object
		Enumeration<BusinessObject> businessObjects = htInstantiatedComponents.elements();
		while (businessObjects.hasMoreElements()) {
			// Get the Component Interface
			BusinessObject componentInterface = businessObjects.nextElement();

			if (processType == AmsConstants.SAVE) {
				boolean tempErrorsFound = (componentInterface.save() == -1);
				if (tempErrorsFound) {
					errorsFound = true;
					// Return here. Otherwise if multiple components
					// fail and all try to do setRollbackOnly (), it throws uncaught exception.
					return -1;
				}
			} else if (processType == AmsConstants.VALIDATE) {
				boolean tempErrorsFound = (componentInterface.validate() == -1);
				if (tempErrorsFound) {
					errorsFound = true;
				}
			} else if (processType == AmsConstants.HAS_CHANGES_PENDING) {
				if (componentInterface.hasChangesPending()) {
					errorsFound = true; // not really an error but indicating there is change pending.
				}

			}

		}
		// If errors were encountered, return failure
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * This method initiates preDelete logic for all components that are being deleted that are maintained within the ComponentList.
	 * 
	 * @see #delete()
	 * @return 1 for Success, -1 for Failure
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public int preDelete() throws RemoteException, AmsException {
		boolean errorsFound = false;
		int i = 0;
		// Get the data for this listing object (if necessary)
		if (!this.isDataRetrieved()) {
			this.getData();
		}
		// Instantiate and initial preDelete logic for each business object in
		// the ComponentList.
		int objectCount = this.getObjectCount();
		for (i = 0; i < objectCount; i++) {
			this.scrollToObjectByIndex(i);
			Long objectID = new Long(this.getListAttribute(this.IDAttributeName));
			// Get a handle to the business object component
			BusinessObject objectInterface = this.getComponentObject(objectID.longValue());
			// Delete the object
			errorsFound = (objectInterface.preDelete() == -1);
		}
		// If errors are found, return failure, otherwise return success
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * This method initiates delete logic for all components that are maintained within the ComponentList. NOTE: The component
	 * manger ensures that preDelete processing is initiated before the delete logic is called. If errors are encountered within
	 * preDelete, this method will not be called.
	 * 
	 * @see #preDelete()
	 * @return 1 for Success, -1 for Failure
	 * @exception AmsException
	 * @exception RemoteException
	 */
	public int delete() throws RemoteException, AmsException {
		boolean errorsFound = false;
		int i = 0;
		// Get the data for this listing object (if necessary)
		if (!this.isDataRetrieved()) {
			this.getData();
		}
		int objectCount = this.getObjectCount();
		// Instantiate each object and call the delete method
		for (i = 0; i < objectCount; i++) {
			this.scrollToObjectByIndex(i);
			Long objectID = new Long(this.getListAttribute(this.IDAttributeName));
			// Get a handle to the business object component
			BusinessObject objectInterface = this.getComponentObject(objectID.longValue());
			// Delete the object
			errorsFound = (objectInterface.delete() == -1);
		}
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * This method obtains all business object errors that have been encountered for all business objects that are maintained by the
	 * ComponentList.
	 * 
	 * @return Vector of error objects.
	 * @exception RemoteException
	 */
	public List<IssuedError> getIssuedErrors() throws RemoteException {
		// Create a vector to store all the component object's errors
		List<IssuedError> componentErrors = new ArrayList<>();
		// For each instantiated component, get the errors and add them
		// to the componentErrors Vector
		Enumeration<BusinessObject> components = this.htInstantiatedComponents.elements();
		while (components.hasMoreElements()) {
			BusinessObject componentObject = components.nextElement();
			List<IssuedError> errorList = componentObject.getIssuedErrors();
			componentErrors.addAll(errorList);

		}
		// Return the Error vector
		return componentErrors;
	}

	// -------------------------------------------------//
	// Document Integration Methods //
	// -------------------------------------------------//

	/**
	 * This method takes a document and puts this business object's attributes into the document starting at the specified component
	 * path. Currently, it will also put any component data into the document also.
	 *
	 * @return DocumentHandler
	 */
	public DocumentHandler populateXmlDoc(DocumentHandler doc) throws RemoteException, AmsException {
		return (this.populateXmlDoc(doc, ""));
	}

	public DocumentHandler populateXmlDoc(DocumentHandler doc, String componentPath) throws RemoteException, AmsException {
		if (!(componentPath.endsWith("/"))) {
			String ecsgObjectName = "";

			javax.naming.Context ctx = null;

			try {
				ctx = GenericInitialContextFactory.getInitialContext();
			} catch (AmsException amsEx) {
			}

			try {
				// Get the ecsg object name (obtained from the Deployment Descriptor)
				ecsgObjectName = (String) ctx.lookup("java:/comp/env/ecsgObjectName");
			} catch (javax.naming.NamingException e) {
			}

			StringBuilder addlPath = new StringBuilder(componentPath);
			addlPath.append('/');
			addlPath.append(ecsgObjectName);
			addlPath.append('/');
			componentPath = addlPath.toString();
		}
		MessageFormat msgFormat = new MessageFormat(componentPath.substring(0, componentPath.length() - 1) + "({0})/");
		Object[] args = { "" };

		// For each instantiated component, put the contents into the XML document
		Enumeration<BusinessObject> components = this.htInstantiatedComponents.elements();
		while (components.hasMoreElements()) {
			BusinessObject componentObject = components.nextElement();
			args[0] = new Integer(componentObject.getAttributeInteger(componentObject.getIDAttributeName()));
			doc = componentObject.populateXmlDoc(doc, msgFormat.format(args));
		}

		return doc;
	}

	/**
	 * This method takes a document and populates this business object's attributes from the values in the document starting at the
	 * specified component path.
	 *
	 */
	public void populateFromXmlDoc(DocumentHandler doc) throws RemoteException, AmsException {
		this.populateFromXmlDoc(doc, "");
	}

	public void populateFromXmlDoc(DocumentHandler doc, String componentPath) throws RemoteException, AmsException {
		String ecsgObjectName = "";

		javax.naming.Context ctx = null;

		try {
			ctx = GenericInitialContextFactory.getInitialContext();
		} catch (AmsException amsEx) {
		}

		try {
			// Get the ecsg object name (obtained from the Deployment Descriptor)
			if (ctx != null)
				ecsgObjectName = (String) ctx.lookup("java:/comp/env/ecsgObjectName");
		} catch (javax.naming.NamingException e) {
		}

		if (!(componentPath.endsWith("/"))) {
			StringBuilder addlPath = new StringBuilder(componentPath);
			addlPath.append('/');
			addlPath.append(ecsgObjectName);
			componentPath = addlPath.toString();
		}
		List<DocumentHandler> docList = doc.getFragmentsList(componentPath);
		for (DocumentHandler tempDoc : docList) {
			// Get the object identifier from the document
			String strObjectId = tempDoc.getAttribute("/".concat(this.IDAttributeName));
			try {
				long objectId = Integer.valueOf(strObjectId).intValue();
				// Get a handle to the appropriate business object
				BusinessObject busObj = this.getComponentObject(objectId);
				// Populate this business object from the document
				busObj.populateFromXmlDoc(tempDoc, "/");
			} catch (Exception e) {
			}
		}
	}

	public void setClientServerDataBridge(ClientServerDataBridge csdb) throws RemoteException {
		this.csdb = csdb;
	}

	/**
	 * This method returns the remote interface of an instantiated business object based on the object that is currently active
	 * within the list.
	 * 
	 * The active object is determined based on a call to the scrollToObject() or scrollToObjectByIndex() method.
	 *
	 * @return The remote interface of the Buisness Object being requested.
	 * @exception AmsException
	 */
	public BusinessObject getBusinessObject() throws AmsException, RemoteException {
		// Obtain the active object's objectID
		Long objectID = new Long(this.getAttribute(this.getIDAttributeName()));
		// Get the business object
		return getComponentObject(objectID.longValue());
	}

	/**
	 * This method returns the remote interface of an instantiated business object based on the objectID passed to the method.
	 *
	 * @param objectID
	 *            The objectID of the business object being requested.
	 * @return The remote interface of the Buisness Object being requested.
	 * @exception AmsException
	 */
	public BusinessObject getBusinessObject(long objectID) throws AmsException, RemoteException {
		return getComponentObject(objectID);
	}

}
