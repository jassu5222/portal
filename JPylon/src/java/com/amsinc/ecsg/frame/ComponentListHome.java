/*
 * @(#)ComponentListHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;

public interface ComponentListHome extends EJBHome
{
  public ComponentList create()
    throws CreateException, RemoteException;
  public ComponentList create(ClientServerDataBridge csdb)
    throws CreateException, RemoteException;
}

