package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */ 
class ComponentNoRelationshipException extends ComponentProcessingException{
    public ComponentNoRelationshipException(){super();}
    public ComponentNoRelationshipException(String s){super(s);}
}