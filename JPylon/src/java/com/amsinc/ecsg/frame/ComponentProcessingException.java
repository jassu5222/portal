package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ComponentProcessingException extends AmsException{
    public ComponentProcessingException(){super();}
    public ComponentProcessingException(String s){super(s);}
}