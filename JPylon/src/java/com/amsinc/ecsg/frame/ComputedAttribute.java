/*
 * @(#)ComputedAttribute
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

/**
 * The ComputedAttribute class is used to designate attributes whose
 * values are not stored in the database, but must be derived, computed,
 * or calculated.  If a requrest to get a ComputedAttribute is received
 * by the AttributeManager, it calls the userGetAttribute() method on the
 * Business Object where business logic must be implemented to compute the
 * attribute's value.
 *
 * ComputedAttributes do not have any special processing at the 
 * attribute level, however this sub-classing aids the AttributeManager
 * service object for attribute processing.
 *

 */
public class ComputedAttribute extends Attribute{
    
	public ComputedAttribute ()
	 {
     }

	public ComputedAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public ComputedAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
    
}   