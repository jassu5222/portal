package com.amsinc.ecsg.frame;

/*
 * @(#)DataSourceFactory
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import javax.naming.*;
import javax.sql.*;
import java.util.PropertyResourceBundle;
import java.util.Hashtable;
import com.amsinc.ecsg.frame.GenericInitialContextFactory;

/**
 * The DataSourceFactory is a factory class that caches DataSources.
 * Caching DataSources will boost performance.  Only one of these
 * classes should appear per VM.
 *

 * @version 1.0 
 */
public class DataSourceFactory
{
    //Hashtable that will hold many datasources eached keyed by
    //dbmsParm which is roughly the same as the JNDI name the DataSources
    //would be normally looked up.  JNDI is sometimes slow and can be 
    //quickly replaced by this factory class.
    private static Hashtable dataSourceCache = new Hashtable();

    /**
     * The default creator is not available to the user as we only want
     * users calling the static methods.
     */
    private DataSourceFactory ()
    {
    }
        
    /**
     * This method empties the dataSourceHashtable. If this method
     * is called the hashtable will be cleared. This method should
     * be called when datasource properties change and the server is
     * not reset.
     *

     */
    public static synchronized void refreshCachedValues ()
    {
        dataSourceCache = new Hashtable ();
    }

    /**
     * This synchronized methods retrieves the datasource keyed by dbmsparm
     * from the static hashtable kept by this class. If the datasource is not
     * found in the hashtable. It is retrieved from a JNDI lookup and placed
     * into the hashtable.  
     *
     * @param dbmsParm Key to the dataSourceCache hashtable
     * @return The datasource that was found in the hashtable or retrieved
     * from JNDI.
     *

     */
    public static synchronized DataSource getDataSource (String dbmsParm)
           throws AmsException
    {
        return getDataSource(dbmsParm,true);
    }
    
    /**
     * This synchronized methods retrieves the datasource keyed by dbmsparm
     * from the static hashtable kept by this class. If the datasource is not
     * found in the hashtable. It is retrieved from a JNDI lookup and placed
     * into the hashtable.  
     *
     * @param dbmsParm Key to the dataSourceCache hashtable
     * @param onServer indicates whether or not the data source is retrieved 
     *                 from the client or the server
     * @return The datasource that was found in the hashtable or retrieved
     * from JNDI.
     *

     */
    public static synchronized DataSource getDataSource (String dbmsParm, boolean onServer)
           throws AmsException
    {
        DataSource dataSource;

        dataSource = (DataSource)dataSourceCache.get (dbmsParm);
        if (dataSource == null)
        {
            dataSource = lookupDataSource (dbmsParm, onServer);
        }
        return dataSource;        
    }
    
    /**
     * This synchronized methods retrieves the datasource from JNDI
     * and places the datasource into the dataSourceCache hashtable.
     *
     * @param dbmsParm Key to the dataSourceCache hashtable
     * @param onServer indicates whether or not the data source is retrieved
                       from the client or the server
     * @return The datasource that was retrieved from JNDI.
     *

     */
    private static synchronized DataSource lookupDataSource (String dbmsParm, boolean onServer)
            throws AmsException
    {
        DataSource dataSource;
        
        Context ctx = GenericInitialContextFactory.getInitialContext (onServer);
        try
        {
            dataSource = (DataSource)ctx.lookup (dbmsParm);
            if (dataSource != null)
            {
                dataSourceCache.put (dbmsParm, dataSource);
            }
        }
        catch (NamingException nEx)
        {
            throw new AmsException (
                "Naming Exception thrown: Unable to create DataSource: " +
                nEx.getMessage ());
        }
        return dataSource;
    }
}
