package com.amsinc.ecsg.frame;

/*
 * DatabaseQueryBean
 *
 *     Copyright  2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.util.*;
import java.sql.*;

import com.amsinc.ecsg.util.*;

import javax.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DatabaseQueryBean is a query service that can be used to retreive the number of rows for any SQL statement.
 *
 * @version 1.0
 */
public class DatabaseQueryBean {

	private static final Logger LOG = LoggerFactory.getLogger(DatabaseQueryBean.class);

	private static String dbmsParm;

	static {
		try {
			dbmsParm = JPylonProperties.getInstance().getString("dataSourceName");

		} catch (Exception e) {
			LOG.error("Exception getting dataSourceName", e);
		}
	}

	/**
	 * Default Constructor
	 */
	private DatabaseQueryBean() {
	}

	/**
	 * Internal private method that connects to the database and holds the connection in a static hashtable.
	 *
	 * @param onServer
	 *            If calling from the server side this value should be true otherwise false.
	 */
	public static Connection connect(boolean onServer) throws AmsException {
		return connect(onServer, dbmsParm);
	}

	/**
	 * Internal private method that connects to the database and holds the connection in a static hashtable.
	 *
	 * @param onServer
	 *            If calling from the server side this value should be true otherwise false.
	 * @param dbmsParameter
	 *            Specifies the data source name
	 */
	private static Connection connect(boolean onServer, String dbmsParameter) throws AmsException {
		Connection dbmsConnection;
		try {
			DataSource ds = DataSourceFactory.getDataSource(dbmsParameter, onServer);
			dbmsConnection = ds.getConnection();

		} catch (SQLException e) {
			throw new AmsException("Error on connecting to Datasource in DatabaseCountBean",e);
		}
		return dbmsConnection;
	}

	/**
	 * Static method that selects multiple rows from the tableName specified according to the whereClause. The update statement is
	 * then used to update the rows The Vector returned is a list of the OIDs of the objects returned
	 *
	 * @param tableName
	 *            The name of the table from which the data is selected.
	 * @param objectIdField
	 *            The physical name of the objectIdField n the table specified
	 * @param whereClause
	 *            The where clause that will cause the query to return at most one row from the DB
	 * @param updateStatement
	 *            update statement to run to update the data
	 * @wherSqlParams
	 * @updateSqlParams
	 */
	public static long selectForUpdate(String tableName, String objectIdField, String whereClause, String updateStatement,
			boolean onServer, boolean noWait, Object[] whereSqlParams, Object[] updateSqlParams) throws AmsException {

		long objectIdValue = 0;
		String sqlStatement = "";

		try (Connection dbmsConnection = connect(onServer, dbmsParm)) {

			dbmsConnection.setAutoCommit(false);

			sqlStatement = buildSelectStatement(tableName, objectIdField, whereClause, noWait);
			if (sqlStatement != null) {
				LOG.debug("In selectRowsForUpdate(), Executing selectSQL ==> {} ", sqlStatement);

				try (PreparedStatement statement = dbmsConnection.prepareStatement(sqlStatement)) {

					if (whereSqlParams != null) {
						for (int i = 0; i < whereSqlParams.length; i++) {
							if (whereSqlParams[i] != null) {
								statement.setObject(i + 1, whereSqlParams[i]);
							} else {
								statement.setNull(i + 1, Types.NULL);
							}
						}
					}
					try (ResultSet results = statement.executeQuery()) {

						if (results.next()) {
							objectIdValue = results.getLong(objectIdField);
							String objectIdValueStr = Long.toString(objectIdValue);

							sqlStatement = buildUpdateStatement(tableName, objectIdField, updateStatement, objectIdValueStr);
							LOG.debug("In selectRowsForUpdate(), Executing updateSQL ==> {} ", updateStatement);
							try (PreparedStatement statement1 = dbmsConnection.prepareStatement(sqlStatement)) {
								if (updateSqlParams != null) {
									for (int i = 0; i < updateSqlParams.length; i++) {
										if (updateSqlParams[i] != null) {
											statement1.setObject(i + 1, updateSqlParams[i]);
										} else {
											statement1.setNull(i + 1, Types.NULL);
										}
									}
								}
								statement1.executeUpdate();
								dbmsConnection.commit();
							}
						}
					}
				}
			}
			// MDB Rel6.1 IR# PDUL012842185 1/28/11 - Begin
		} catch (SQLException e) {
			if ((noWait) && (e.getErrorCode() == 54)) {
				throw new AmsException(AmsConstants.ROW_LOCKED);
			}
			LOG.error("Exception in  selectForUpdate() - SQL: {} ", sqlStatement, e);
			// MDB Rel6.1 IR# PDUL012842185 1/28/11 - End
		} catch (Exception e) {
			LOG.error("Exception in  selectForUpdate() - SQL: {} ", sqlStatement, e);

		}

		return objectIdValue;
	}

	/**
	 * Static method that returns a DocumentHandler containing the result set of the query specified in the sqlStatement.
	 *
	 * @param sql
	 *            A valid SQL statement
	 * @param onServer
	 *            Specifies whether or not this statement is being called from the application server. If it is being called from a
	 *            JSP onServer should be false.
	 * @param paramList
	 *            sql query parameters.
	 */
	public static long selectForUpdate(String tableName, String objectIdField, String whereClause, String updateStatement,
			boolean onServer, boolean noWait, List<Object> whereSqlParamList, List<Object> updateSqlParams) throws AmsException {

		Object[] whereParams = null;
		Object[] updateParams = null;

		if (whereSqlParamList != null) {
			whereParams = whereSqlParamList.toArray();
		}

		if (updateSqlParams != null) {
			updateParams = updateSqlParams.toArray();
		}
		return selectForUpdate(tableName, objectIdField, whereClause, updateStatement, onServer, noWait, whereParams, updateParams);
	}

	/**
	 * Static method that selects multiple rows from the tableName specified according to the whereClause. whereClause. The update
	 * statement is then used to update the rows The Vector returned is a list of the OIDs of the objects returned
	 *
	 * @param tableName
	 *            The name of the table from which the data is selected.
	 * @param objectIdField
	 *            The physical name of the objectIdField n the table specified
	 * @param whereClause
	 *            The where clause that will cause the query to return at most one row from the DB
	 * @param updateStatement
	 *            update statement to run to update the data
	 */
	public static Vector<String> selectRowsForUpdate(String tableName, String objectIdField, String whereClause,
			String updateStatement, boolean onServer, Object[] whereSqlParams, Object[] updateSqlParams) throws AmsException {

		String sqlStatement ="";
		Vector<String> returnValue = new Vector<>(1, 10);

		try (Connection dbmsConnection = connect(onServer, dbmsParm)) {

			dbmsConnection.setAutoCommit(false);

			sqlStatement = buildSelectStatement(tableName, objectIdField, whereClause, false);
			if (sqlStatement != null) {
				LOG.debug("In selectRowsForUpdate(), Executing selectSQL ==> {} ", sqlStatement);

				try (PreparedStatement statement = dbmsConnection.prepareStatement(sqlStatement)) {

					if (whereSqlParams != null) {
						for (int i = 0; i < whereSqlParams.length; i++) {
							if (whereSqlParams[i] != null) {
								statement.setObject(i + 1, whereSqlParams[i]);
							} else {
								statement.setNull(i + 1, Types.NULL);
							}
						}
					}
					try (ResultSet results = statement.executeQuery()) {
						boolean rowsFound = false;

						// Add the selected values to the vector to be returned
						while (results.next()) {
							rowsFound = true;
							String oid = results.getString(objectIdField.toUpperCase());
							returnValue.addElement(oid);
						}

						if (rowsFound) {
							// Run the update statement to free up the rows that were selected for update
							LOG.debug("In selectRowsForUpdate(), Executing updateSQL ==> {} ", updateStatement);

							try (PreparedStatement statement1 = dbmsConnection.prepareStatement(updateStatement)) {

								if (updateSqlParams != null) {
									for (int i = 0; i < updateSqlParams.length; i++) {
										if (updateSqlParams[i] != null) {
											statement1.setObject(i + 1, updateSqlParams[i]);
										} else {
											statement1.setNull(i + 1, Types.NULL);
										}
									}
								}
								statement1.executeUpdate();
								dbmsConnection.commit();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Exception in  selectRowsForUpdate() - SQL: {})", sqlStatement);
			LOG.error("Exception in  selectRowsForUpdate() - updateSQL: ()", updateStatement, e);
		}

		return returnValue;

	}

	public static Vector<String> selectRowsForUpdate(String tableName, String objectIdField, String whereClause,
			String updateStatement, boolean onServer, List<Object> whereSqlParams, List<Object> updateSqlParams)
					throws AmsException {

		Object[] whereParams = null;
		Object[] updateParams = null;

		if (whereSqlParams != null) {
			whereParams = whereSqlParams.toArray();
		}

		if (updateSqlParams != null) {
			updateParams = updateSqlParams.toArray();
		}

		return selectRowsForUpdate(tableName, objectIdField, whereClause, updateStatement, false, whereParams, updateParams);
	}

	/**
	 *
	 */
	public static String buildUpdateStatement(String tableName, String objectIdField, String updateStatement, String objectIdValue) {

		if (StringFunction.isBlank(tableName) || StringFunction.isBlank(updateStatement)) {
			return null;
		}

		StringBuilder strBuffer = new StringBuilder("UPDATE ");
		strBuffer.append(tableName);
		strBuffer.append(" SET ");
		strBuffer.append(updateStatement);
		strBuffer.append(" WHERE ");
		strBuffer.append(objectIdField);
		strBuffer.append(" = ");
		strBuffer.append(objectIdValue);

		return strBuffer.toString();

	}

	/**
	 *
	 */
	public static String buildSelectStatement(String tableName, String objectIdField, String whereClause, boolean noWait) {// MDB Rel6.1 IR# PDUL012842185 1/28/11  - added noWait

		if (StringFunction.isBlank(objectIdField) || StringFunction.isBlank(tableName)) {
			return null;
		}

		StringBuilder strBuffer = new StringBuilder("SELECT ");

		strBuffer.append(objectIdField);
		strBuffer.append(" FROM ");
		strBuffer.append(tableName);
		if (StringFunction.isNotBlank(whereClause)) {
			strBuffer.append(" WHERE ");
			strBuffer.append(whereClause);
			strBuffer.append(" FOR UPDATE");
			// MDB Rel6.1 IR# PDUL012842185 1/28/11 - Begin
			if (noWait) {
				strBuffer.append(" NOWAIT");
			}
			// MDB Rel6.1 IR# PDUL012842185 1/28/11 - End
		}

		return strBuffer.toString();

	}

	/**
	 * Method added to fix sqlinjection Static method that returns a DocumentHandler containing the result set of the query
	 * specified in the sqlStatement.
	 *
	 * @param sql
	 *            A valid SQL statement
	 * @param onServer
	 *            Specifies whether or not this statement is being called from the application server. If it is being called from a
	 *            JSP onServer should be false.
	 * @param params
	 *            sql query parameters.
	 */
	public static DocumentHandler getXmlResultSet(String sql, boolean onServer, Object... params) throws AmsException {

		DocumentHandler returnDoc = null;

		LOG.debug("In getXmlResultSet(), Executing SQL ==> {} ", sql);

		try (Connection dbmsConnection = connect(onServer, dbmsParm);
				PreparedStatement statement = dbmsConnection.prepareStatement(sql)) {

			// now set the inputs!
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					if (params[i] != null) {
						statement.setObject(i + 1, params[i]);
					} else {
						statement.setNull(i + 1, Types.NULL);
					}
				}
			}

			try (ResultSet results = statement.executeQuery()) {
				if (results.next()) {
					returnDoc = resultSetAsXml(results);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception in  getXmlResultSet() - SQL : {}", sql, e);
		}
		return returnDoc;
	}

	/**
	 * Static method that returns a DocumentHandler containing the result set of the query specified in the sqlStatement.
	 *
	 * @param sql
	 *            A valid SQL statement
	 * @param onServer
	 *            Specifies whether or not this statement is being called from the application server. If it is being called from a
	 *            JSP onServer should be false.
	 * @param paramList
	 *            sql query parameters.
	 */
	public static DocumentHandler getXmlResultSet(String sql, boolean onServer, List<Object> paramList) throws AmsException {

		Object[] params = null;
		if (paramList != null) {
			params = paramList.toArray();
		}
		return getXmlResultSet(sql, onServer, params);
	}

	/**
	 *
	 */
	private static DocumentHandler resultSetAsXml(ResultSet results) throws SQLException {

		ResultSetMetaData resultsMetaData = results.getMetaData();
		int numberOfColumns = resultsMetaData.getColumnCount();
		String[] columnNames = new String[numberOfColumns];

		StringBuilder sbuff = new StringBuilder("<DocRoot>");
		char[] srsr = "<ResultSetRecord ID=\"".toCharArray();
		char[] ersr = "</ResultSetRecord>".toCharArray();

		for (int i = 1; i <= numberOfColumns; i++) {
			columnNames[i - 1] = resultsMetaData.getColumnLabel(i).toUpperCase();
		}

		try {
			int rowIndex = 0;
			boolean containsRecords = true;
			while (containsRecords) {
				sbuff.append(srsr);
				sbuff.append(rowIndex);
				sbuff.append('"');
				sbuff.append('>');
				for (int columnIndex = 0; columnIndex < numberOfColumns; columnIndex++) {
					sbuff.append('<');
					sbuff.append(columnNames[columnIndex]);
					sbuff.append('>');

					// resultsMetaData = results.getMetaData();
					int sqlType = resultsMetaData.getColumnType(columnIndex + 1);
					Object rawColumnData = results.getObject(columnIndex + 1);

					if (sqlType == Types.DATE && rawColumnData != null) {
						String date = results.getDate(columnIndex + 1).toString();
						String time = results.getTime(columnIndex + 1).toString();
						rawColumnData = date + ' ' + time + ".0";
					}
					String columnData;
					if (rawColumnData == null) {
						columnData = "";
					} else {
						columnData = rawColumnData.toString();
						columnData = StringService.change(columnData, "&", "&amp;");
						columnData = StringService.change(columnData, ">", "&gt;");
						columnData = StringService.change(columnData, "<", "&lt;");
					}
					sbuff.append(columnData);
					sbuff.append('<');
					sbuff.append('/');
					sbuff.append(columnNames[columnIndex]);
					sbuff.append('>');
				}
				sbuff.append(ersr);

				rowIndex++;
				containsRecords = results.next();
			}
			sbuff.append("</DocRoot>");

			return new DocumentHandler(sbuff.toString(), false);
		} catch (Exception e) {
			LOG.error("Exception in  resultSetAsXml() ", e);
			return null;
		}
	}

	/**
	 * Static method that returns the number of rows in the table "tableName" with the unique column "columnName" with the
	 * whereClause "whereClause" i.e. select count(columnName) from tableName;
	 *
	 * @param columnName
	 *            Unique column name to increase the search speed
	 * @param tableName
	 *            Table that is being searched.
	 * @param whereClause
	 *            Where clause for the select statement
	 * @param onServer
	 *            Specifies whether or not this method is being called from a remote client.
	 */
	public static int getCount(String columnName, String tableName, String whereClause, boolean onServer, List<Object> paramList)  throws AmsException {

		Object[] params = null;
		if (paramList != null) {
			params = paramList.toArray();
		}
		return getCount(columnName, tableName, whereClause, onServer, params);
	}

	public static int getCount(String columnName, String tableName, String whereClause, boolean onServer, Object... sqlVars)  throws AmsException {

		int rowCount = 0;

		if (StringFunction.isBlank(columnName) || StringFunction.isBlank(tableName)) {
			return rowCount;
		}

		StringBuilder strbuf = new StringBuilder("SELECT ");
		StringBuilder select = new StringBuilder("COUNT(");
		select.append(columnName);
		select.append(')');
		strbuf.append(select.toString());
		strbuf.append(" FROM ");
		strbuf.append(tableName);
		if (whereClause != null) {
			strbuf.append(" WHERE ");
			strbuf.append(whereClause);
		}

		LOG.debug("In getCount(), Executing SQL ==> {} ", strbuf);

		try (Connection dbmsConnection = connect(onServer, dbmsParm);
				PreparedStatement statement = dbmsConnection.prepareStatement(strbuf.toString())) {

			// now set the inputs!
			if (sqlVars != null) {
				for (int i = 0; i < sqlVars.length; i++) {
					if (sqlVars[i] != null) {
						statement.setObject(i + 1, sqlVars[i]);
					} else {
						statement.setNull(i + 1, Types.NULL);
					}
				}
			}

			try (ResultSet results = statement.executeQuery()) {
				results.next();
				rowCount = results.getInt(select.toString());
			}
		} catch (Exception e) {
			LOG.error("Exception in  getCount() - SQL: {}", strbuf, e);
		}

		return rowCount;
	}

	public static double getSum(String columnName, String tableName, String whereClause, boolean onServer, List<Object> paramList)  throws AmsException {

		Object[] params = null;
		if (paramList != null) {
			params = paramList.toArray();
		}
		return getSum(columnName, tableName, whereClause, onServer, params);
	}

	public static double getSum(String columnName, String tableName, String whereClause, boolean onServer, Object... sqlVars)  throws AmsException {

		double sumValue = 0;

		if (StringFunction.isBlank(columnName) || StringFunction.isBlank(tableName)) {
			return sumValue;
		}

		StringBuilder strbuf = new StringBuilder("SELECT ");
		StringBuilder select = new StringBuilder("SUM(");
		select.append(columnName);
		select.append(')');
		strbuf.append(select.toString());
		strbuf.append(" FROM ");
		strbuf.append(tableName);
		if (whereClause != null) {
			strbuf.append(" WHERE ");
			strbuf.append(whereClause);
		}

		LOG.debug("In getSum(), Executing SQL ==> {} ", strbuf);

		try (Connection dbmsConnection = connect(onServer, dbmsParm);
				PreparedStatement statement = dbmsConnection.prepareStatement(strbuf.toString())) {
			// now set the inputs!
			if (sqlVars != null) {
				for (int i = 0; i < sqlVars.length; i++) {
					if (sqlVars[i] != null) {
						statement.setObject(i + 1, sqlVars[i]);
					} else {
						statement.setNull(i + 1, Types.NULL);
					}
				}
			}

			try (ResultSet results = statement.executeQuery()) {
				results.next();
				sumValue = results.getDouble(select.toString());
			}
		} catch (Exception e) {
			LOG.error("Exception in  getSum() - SQL: {}", strbuf,  e);
		}
		return sumValue;
	}

	public static int executeUpdate(String sqlStatement, boolean onServer, List<Object> paramList)   throws AmsException, SQLException {

		Object[] params = null;
		if (paramList != null) {
			params = paramList.toArray();
		}

		return executeUpdate(sqlStatement, onServer, params);
	}

	/**
	 * ANYONE CALLING THIS METHOD MUST HAVE A GOOD REASON FOR DOING SO AS IT CAN CAUSE ALL KINDS OF TRANSACTIONAL COMMIT PROBLEMS.
	 * DO NOT USE THIS METHOD WITHOUT A GOOD REASON.
	 *
	 * Static method that executes a SQL INSERT, UPDATE, or DELETE statement. The result is the number of rows affected.
	 *
	 * If the SQL statement is not valid, a SQLException is thrown.
	 *
	 * @param sqlStatement
	 *            A valid SQL statement
	 * @param onServer
	 *            Specifies whether or not this statement is being called from the application server. If it is being called from a
	 *            JSP onServer should be false.
	 * @return int - Number of rows affected as returned by Statement.executeUpdate
	 * @exception AmsException
	 * @exception SQLException
	 */
	public static int executeUpdate(String sqlStatement, boolean onServer, Object... sqlVars) throws SQLException, AmsException {

		int resultCount = 0;

		if (StringFunction.isBlank(sqlStatement)) {
			return resultCount;
		}

		LOG.debug("In executeUpdate(), Executing SQL ==> {} ", sqlStatement);

		try (Connection dbmsConnection = connect(onServer, dbmsParm);
				PreparedStatement statement = dbmsConnection.prepareStatement(sqlStatement)) {

			// now set the inputs!
			if (sqlVars != null) {
				for (int i = 0; i < sqlVars.length; i++) {
					if (sqlVars[i] != null) {
						statement.setObject(i + 1, sqlVars[i]);
					} else {
						statement.setNull(i + 1, Types.NULL);
					}
				}
			}
			resultCount = statement.executeUpdate();

		} catch (SQLException e) {
			LOG.error("Exception in DatabaseQueryBean.executeUpdate: SQL ==> {} ", sqlStatement, e);
			throw new SQLException(e.getMessage());
		}
		return resultCount;
	}

	public static String getObjectID(String tableName, String objectIdField, String whereClause, List<Object> paramList) {

		Object[] params = null;
		if (paramList != null) {
			params = paramList.toArray();
		}
		return getObjectID(tableName, objectIdField, whereClause, params);
	}

	public static String getObjectID(String tableName, String objectIdField, String whereClause, Object... sqlVars) {

		String sqlStr = "SELECT " + objectIdField + " FROM " + tableName + " WHERE " + whereClause;
		String objectID = null;

		try {
			LOG.debug("In getObjectID(), Executing SQL ==> {} ", sqlStr);
			DocumentHandler resultsDocXML = DatabaseQueryBean.getXmlResultSet(sqlStr, false, sqlVars);

			if (resultsDocXML != null) {
				List<DocumentHandler> ve = resultsDocXML.getFragmentsList("/ResultSetRecord");

				if (ve != null && ve.size() == 1) {
					objectID = resultsDocXML.getAttribute("/ResultSetRecord/" + objectIdField);
				}
			}
		} catch (AmsException e) {
			LOG.error("Exception in  getObjectID: SQL ==> {} ", sqlStr, e);
		}
		return objectID;
	}

}
