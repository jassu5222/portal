package com.amsinc.ecsg.frame;

/*
 * @(#)DateAttribute
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DateTimeUtility;
import java.text.SimpleDateFormat;

/**
 * The DateAttribute class performs all validation required for DBMS Date attributes. The current implementation of the
 * DateAttribute class validates that the attribute value is a valid SQL date.
 *
 */
public class DateAttribute extends Attribute {
	private static final Logger LOG = LoggerFactory.getLogger(DateAttribute.class);

	public DateAttribute() {
	}

	public DateAttribute(String name, String physicalName) {
		super(name, physicalName);
	}

	public DateAttribute(String name, String physicalName, boolean required, String alias) {
		super(name, physicalName, required, alias);
	}

	/**
	 * Override the ancestor's getAttributeValue method so that the SQL Timestamp is converted to the standard Date format of
	 * "MM/dd/yyyy".
	 *
	 * @return String The date value in the format "MM/dd/yyyy"
	 */
	public String getAttributeValue() {
		String superValue = super.getAttributeValue();
		// If the value is a pre-existing value (retrieved from the DB) then perfrom the necessary processing to convert
		// the value from a Timestamp to the standard framework date format "MM/dd/yyyy".
		if (superValue.length() > 10) {
			String value = "";
			try {
				value = DateTimeUtility.convertTimestampToDateString(super.getAttributeValue());
			} catch (Exception e) {
				LOG.error("Exception in getAttributeValue()", e);
			}

			return value;
		} else {
			// return the unprocessed value
			return superValue;
		}
	}

	/**
	 * Override the ancestor's setAttributeValue method so that null/emtpy values are processed correctly.
	 *
	 * @param String
	 *            attributeValue The value being set
	 * @param boolean
	 *            validateValue Boolean indicator as to if the value will validate.
	 *
	 */
	public void setAttributeValue(String attributeValue, boolean validateValue) throws AmsException {
		// If the attribute value is empty/null, default the value to "0".
		if ((attributeValue == null) || (attributeValue.isEmpty())) {
			super.setAttributeValue("", false);
		} else {
			// Simply set the attribute Value using the super class method
			super.setAttributeValue(attributeValue, validateValue);
		}
	}

	public void validate(String attributeValue) throws AmsException {
		// Try to process the date-string as a SQL Date

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date parsedDate = null;
		String newDate = "";

		try {
			// Parse the value and then format it again.
			parsedDate = formatter.parse(attributeValue);
			newDate = formatter.format(parsedDate).trim();
		} catch (Exception e) {
			throw new InvalidAttributeValueException(AmsConstants.INVALID_DATE_ATTRIBUTE);
		}

		// If the given value does not match the parsed/formatted value, we have a bad date.
		if (!attributeValue.equals(newDate)) {
			throw new InvalidAttributeValueException(AmsConstants.INVALID_DATE_ATTRIBUTE);
		}
	}
}
