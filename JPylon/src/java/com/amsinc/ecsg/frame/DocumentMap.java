/*
 * @(#)DocumentMap
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;
import java.util.*;
import java.io.*;

public class DocumentMap {
  
   private Hashtable xmlpathParameterMap;
   
   public DocumentMap(String fileName) {
     DocumentHandler doc = null; 
     try {
       doc = new DocumentHandler(fileName);
       setHashtableFromDoc(doc);     
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
   }
   
   public DocumentMap(DocumentHandler doc) {
     if (doc == null) {
        xmlpathParameterMap = new Hashtable();
     } else {
       setHashtableFromDoc(doc);
     }
   }
   
   public DocumentMap(Hashtable hashtable) {
     xmlpathParameterMap = hashtable;
   }

   private void setHashtableFromDoc(DocumentHandler doc) {
     xmlpathParameterMap = new Hashtable();
     
     Vector parameterDocs = doc.getFragments("/Parameter");
     Enumeration enumer = parameterDocs.elements();
     while (enumer.hasMoreElements())
     {
        DocumentHandler parmDoc = (DocumentHandler) enumer.nextElement();
        xmlpathParameterMap.put(parmDoc.getAttribute("/Name"),
                                new DocumentMapRecord(parmDoc.getAttribute("/Xmlpath"),
                                                      parmDoc.getAttribute("/EncryptionAlgorithm")));
     }     
   
   }
   
   public Hashtable getParameterXmlPairs() {
     return xmlpathParameterMap;
   }

   public String toString() {
		
		StringBuilder buf = new StringBuilder();
		
		Enumeration keys = xmlpathParameterMap.keys();
		while (keys.hasMoreElements()){
			String key = (String)keys.nextElement();
			buf.append(key).append(":").append(((DocumentMapRecord) xmlpathParameterMap.get(key)).getDocumentPath()).append("<BR>");
		}
		
		return buf.toString();
		
   }
}

