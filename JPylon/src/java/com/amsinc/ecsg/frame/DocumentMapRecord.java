package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;
import java.util.*;
import java.io.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class DocumentMapRecord {
    private String documentPath;
    private String encryptionType;
    
    public DocumentMapRecord(String documentPath, String encryptionType)
    {
      this.documentPath = documentPath;
      this.encryptionType = encryptionType;
    }

    public DocumentMapRecord(String documentPath)
    {
      this.documentPath = documentPath;
      this.encryptionType = null;
    }
    
    public String getDocumentPath() {
        return this.documentPath;
    }
    
    public String getEncryptionType() {
        return this.encryptionType;
    }
}
