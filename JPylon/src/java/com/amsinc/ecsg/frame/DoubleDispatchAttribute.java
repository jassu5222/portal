package com.amsinc.ecsg.frame;

/*
 * @(#)DoubleDispatchAttribute
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
/**
 * The DoubleDispatchAttribute class is used to designate attributes that
 * exist on a component object of the business object in which they are
 * registered.
 *
 * DoubleDispatchAttributes do not have any special processing at the 
 * attribute level, however this sub-classing aids the AttributeManager
 * service object for attribute processing.
 *

 */
public class DoubleDispatchAttribute extends Attribute  {

	public DoubleDispatchAttribute ()
	 {
     }

	public DoubleDispatchAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public DoubleDispatchAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
    
}
