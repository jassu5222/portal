package com.amsinc.ecsg.frame;

import java.util.Hashtable;
import javax.naming.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import java.rmi.*;
import java.lang.reflect.*;
import java.util.PropertyResourceBundle;
import com.amsinc.ecsg.util.JPylonProperties;

/**
 * The EJBObjectFactory provides asimplified mechanism to create and use 
 * EJB components from both client applications or other remote (server-side) 
 * EJB components.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 * @version 1.0 
 */
public class EJBObjectFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(EJBObjectFactory.class);
    private static String EJB_SECURITY_MESSAGE = 
      ". This exception is typically thrown when an EJB is created with " + 
      "invalid security priveledges. Ensure that the userid/Password are " +
      "correct for the EJB. Nested exception: ";

    //  The jndiFactory name will vary by Application Server.  This value is read
    //  from a properties file and is stored in this static variable.
    private static String jndiFactoryName;
          
    //  A cache of home interface that can be reused to avoid uneccessary lookups 
    private static Hashtable homeInterfaces = new Hashtable();
    
    static {
           //  Look up the property file containing environment properties
           JPylonProperties jPylonProperties = null;

           try
            {
               jPylonProperties = JPylonProperties.getInstance();

               //  Get the environment property jndiFactoryName from jPylon.properties           
               jndiFactoryName = jPylonProperties.getString ("jndiFactoryName"); 
            }
           catch (Exception e)
            {
               // Default to WebLogic's JNDI factory if not available
               jndiFactoryName = "weblogic.jndi.WLInitialContextFactory";               
            }      
      }
    
    private EJBObjectFactory ()
    {
    }
        
     /**
     * This method refreshes the cached static valuesrity
     *

     * @version 1.0
     */
    public static synchronized void refreshCachedValues() 
           throws RemoteException, AmsException
    {
        homeInterfaces = new Hashtable();        
    }


    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter.  The EJB component will 
     * be created using EJB security (user_id/password) to enforce the server's 
     * security controls.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param userID The userID used for EJB security
     * @param password The password used for EJB security
     *

     * @version 1.0
     */
    public static EJBObject createSecureClientEJB (String serverLocation, 
                                                   String objectName, 
                                                   String userID, 
                                                   String password)
           throws RemoteException, AmsException
    {
        return createEJB (null, serverLocation, objectName, null, userID, password);
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter.  The EJB component will 
     * be created using EJB security (user_id/password) to enforce the server's 
     * security controls. Additionally, this method will instantiate the business object 
     * based on the objectID parameter.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param objectID The unique object identifier
     * @param userID The userID used for EJB security
     * @param password The password used for EJB security
     *

     * @version 1.0
     */
    public static EJBObject createSecureClientEJB (String serverLocation, 
                                                   String objectName, 
                                                   long objectID, 
                                                   String userID, 
                                                   String password)
           throws RemoteException, AmsException
    {
        // Create the EJB
        EJBObject objectInterface = createEJB (null, serverLocation, objectName, null, userID, password);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter.  The EJB 
     * component will be created using EJB security (user_id/password) to 
     * enforce the server's security controls.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param csdb The client server data bridge being used for the EJB
     * @param userID The userID used for EJB security
     * @param password The password used for EJB security
     *

     * @version 1.0
     */
    public static EJBObject createSecureClientEJB (String serverLocation, 
                                                   String objectName, 
                                                   ClientServerDataBridge csdb,
                                                   String userID, 
                                                   String password)
           throws RemoteException, AmsException
    {
        // Create the EJB
        return createEJB (null, serverLocation, objectName, csdb, userID, password);
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter.  The EJB 
     * component will be created using EJB security (user_id/password) to 
     * enforce the server's security controls. Additionally, this method will 
     * instantiate the business object based on the objectID parameter.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param objectID The unique object identifier
     * @param csdb The client server data bridge being used for the EJB
     * @param userID The userID used for EJB security
     * @param password The password used for EJB security
     *

     * @version 1.0
     */
    public static EJBObject createSecureClientEJB (String serverLocation,
                                                   String objectName,
                                                   long objectID,
                                                   ClientServerDataBridge csdb,
                                                   String userID,
                                                   String password)
           throws RemoteException, AmsException
    {
        // Create the EJB
        EJBObject objectInterface = createEJB (null, serverLocation, objectName, csdb, userID, password);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) 
     * for the component specified by the componentName parameter.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param String The name of the object as specified within the EJB
     * container's Deployment
     *

     * @version 1.0 
     */
    public static EJBObject createClientEJB (String serverLocation, String objectName)
           throws RemoteException, AmsException
    {
        return createEJB (null, serverLocation, objectName, null, null, null);
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter and instantiates 
     * the business object based on the objectID parameter.
     *
     * This method has been provided to reduce the number of remote method calls
     * when using BusinessObject EJB components.  The develop DOES NOT need to
     * call the BusinessObject's getData() method when this method is used to
     * create business objects.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param objectID The objectID that will be used in a getData call. If
     * the objectID is "0", then the getData() method will not be called.
     *

     * @version 1.0 
     */
    public static EJBObject createClientEJB (String serverLocation, String objectName, long objectID) 
           throws RemoteException, AmsException
    {
        // Create the object
        EJBObject objectInterface = createEJB (
           null, serverLocation, objectName, null, null, null);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified using a server-side EJB's context.
     *
     * This method has been provided so that server-side EJB components can
     * create other EJB components and pass along the current context. This
     * provides all the necessary context information (i.e. should the child
     * component create a transaction) for the new EJB component that is
     * created.
     *
     * @param SessionContext The Session Context of the EJB calling this method
     * (REQUIRED)
     * @param String The name of the object as specified within the EJB
     * container's Deployment
     *

     * @version 1.0 
     */
    public static EJBObject createServerEJB (SessionContext ejbContext, String objectName)
           throws AmsException
    {
        return createEJB (ejbContext, null, objectName, null, null, null);
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified using a server-side EJB's context and
     * instantiatesthe business object based on the objectID parameter supplied.
     *
     * This method has been provided so that server-side EJB components can
     * create other EJB components and pass along the current context. This
     * provides all the necessary context information (i.e. should the child
     * component create a transaction) for the new EJB component that is
     * created.
     *
     * @param SessionContext The Session Context of the EJB calling this method
     * (REQUIRED)
     * @param String The name of the object as specified within the EJB
     * container's Deployment
     * @param long objectID The unique object identifier of the business object
     *

     * @version 1.0 
     */
    public static EJBObject createServerEJB (SessionContext ejbContext, String objectName, long objectID)
           throws AmsException, RemoteException
    {
        // Create the EJB Component
        EJBObject objectInterface =
           createEJB (ejbContext, null, objectName, null, null, null);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }
    
    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param csdb A shared data structure which contains initialization values
     * for the EJB
     *

     * @version 1.0 
     */    
    public static EJBObject createClientEJB (String serverLocation, String objectName, ClientServerDataBridge csdb)
           throws RemoteException, AmsException
    {
        return createEJB (null, serverLocation, objectName, csdb, null, null);
    }

    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified by the componentName parameter and instantiates 
     * the business object based on the objectID parameter.
     *
     * This method has been provided to reduce the number of remote method calls
     * when using BusinessObject EJB components.  The develop DOES NOT need to
     * call the BusinessObject's getData() method when this method is used to
     * create business objects.
     *
     * @param serverLocation The name and location of the server (i.e.
     * "ECSGDynamo:8860")
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param objectID The objectID that will be used in a getData call. If
     * the objectID is "0", then the getData() method will not be called.
     * @param csdb A shared data structure which contains initialization values
     * for the EJB
     *

     * @version 1.0 
     */
    public static EJBObject createClientEJB (String serverLocation, String objectName, long objectID, ClientServerDataBridge csdb)
           throws RemoteException, AmsException
    {
        // Create the object
        EJBObject objectInterface =
           createEJB (null, serverLocation, objectName, csdb, null, null);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }

    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified using a server-side EJB's context.
     *
     * This method has been provided so that server-side EJB components can
     * create other EJB components and pass along the current context. This
     * provides all the necessary context information (i.e. should the child
     * component create a transaction) for the new EJB component that is
     * created.
     *
     * @param ejbContext The Session Context of the EJB calling this method
     * (REQUIRED)
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param csdb A shared data structure which contains initialization values
     * for the EJB
     *

     * @version 1.0 
     */
    public static EJBObject createServerEJB (SessionContext ejbContext, String objectName, ClientServerDataBridge csdb)
           throws AmsException
    {
        return createEJB(ejbContext,null,objectName,csdb, null, null);
    }

    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified using a server-side EJB's context and
     * instantiates the business object based on the objectID parameter
     * supplied.
     *
     * This method has been provided so that server-side EJB components can
     * create other EJB components and pass along the current context. This
     * provides all the necessary context information (i.e. should the child
     * component create a transaction) for the new EJB component that is
     * created.
     *
     * @param ejbContext The Session Context of the EJB calling this method
     * (REQUIRED)
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param objectID objectID The unique object identifier of the business
     * object
     * @param csdb A shared data structure which contains
     * initialization values for the EJB
     *

     * @version 1.0 
     */
    public static EJBObject createServerEJB (SessionContext ejbContext, String objectName, long objectID, ClientServerDataBridge csdb)
           throws AmsException, RemoteException
    {
        // Create the EJB Component
        EJBObject objectInterface =
           createEJB (ejbContext, null, objectName, csdb, null, null);
        // Call the business object's getData() method
        BusinessObject boInterface = (BusinessObject)objectInterface;
        boInterface.getData (objectID);
        // Return the EJB RemoteInterface
        return objectInterface;
    }

    /**
     * This method create's the remote interface (a descendant of EJBObject) for
     * the component specified.
     *
     * If the SessionContext has been provided, create the component as a 
     * server-side component.  If the server location has been provided,
     * create the component using the corresponding server location parameter.
     *
     * @param ejbContext The Session Context of the EJB calling this method
     * (REQUIRED)
     * @param serverLocation The location and port of the EJB Server
     * @param objectName The name of the object as specified within the EJB
     * container's Deployment
     * @param csdb A shared data structure which contains initialization values
     * for the EJB
     * @param userID The userID used for EJB Security (may be null)
     * @param password The password used for EJB Security (may be null)
     *

     * @version 1.0 
     */
     
    private static synchronized void addHomeInterfaceToCache(String objectName,
                                    Object homeInterface)
    {
           homeInterfaces.put(objectName, homeInterface);
    }
    
    protected static EJBObject createEJB (SessionContext ejbContext, 
                                          String serverLocation, 
                                          String objectName, 
                                          ClientServerDataBridge csdb,
                                          String userID,
                                          String password)
           throws AmsException
    {
        Context ctx = null;
        // Create the EJB object
        EJBObject objectInterface = null;
        try
        {
            // If the EJB Context exists, use it, otherwise create a new context
            if (ejbContext == null)
            {
                // Set the USERID and Password if they have been specified
                if ((userID != null) && (password != null))
                {
                    Hashtable props = new Hashtable ();
                    props.put(Context.SECURITY_PRINCIPAL, userID);
                    props.put(Context.SECURITY_CREDENTIALS, password);
                    props.put (Context.INITIAL_CONTEXT_FACTORY,
                               jndiFactoryName);
                    props.put (Context.PROVIDER_URL,serverLocation);
                    ctx = new InitialContext (props);
                }
                else 
                 {
                    ctx = GenericInitialContextFactory.getInitialContext (false, serverLocation);
                 }
            }
            else
            {
                ctx = GenericInitialContextFactory.getInitialContext (true); 
            }
            // Get the Home Interface    
            Object homeInterface = homeInterfaces.get(objectName);
            // Get a handle to the EJB's Home interface
            if (homeInterface == null)
            {
                homeInterface = ctx.lookup (objectName);
                homeInterface = javax.rmi.PortableRemoteObject.narrow(homeInterface,homeInterface.getClass());
                addHomeInterfaceToCache(objectName,homeInterface);
            }
            // Ensure that the home interface has been found
            if (homeInterface == null)
            {
                throw new AmsException (
                   "Unable to create Server EJB Object: " + objectName);
            }
            // Dynamically call the create method to get the Remote Interface
            Class componentClass = homeInterface.getClass ();
            Method create = null;
            if ((!((EJBHome)homeInterface).getEJBMetaData ().isStatelessSession ()) &&
                (csdb != null))
            {
                Class[] parameterTypes = new Class[1];
                parameterTypes[0] = Class.forName(
                    "com.amsinc.ecsg.frame.ClientServerDataBridge");
                Object[] arguments = new Object [1];
                arguments[0] = csdb;
                create = componentClass.getMethod ("create", parameterTypes);
                objectInterface =
                   (EJBObject)create.invoke (homeInterface, arguments);                
            } else {
                Class[] parameterTypes = new Class[0];
                Object[] arguments = new Object[0];
                create = componentClass.getMethod ("create", parameterTypes);
                objectInterface =
                   (EJBObject)create.invoke (homeInterface, arguments);                
            }            
        }
        catch (RemoteException rEx)
        {
           LOG.error("Exception occured in createEJB() " , rEx);
           throw new AmsException ("Statefullness of EJB cannot be determined",rEx);
        }
        catch (NoSuchMethodException nsmEx)
        {
        	LOG.error("Exception occured in createEJB()" , nsmEx);
            throw new AmsException ("NoSuchMethodException thrown: Unable to create Server EJB Object: " + objectName +
               ". Nested exception: " + nsmEx.getMessage (), nsmEx);
        }
        catch (IllegalAccessException iaEx)
        {
        	LOG.error("Exception occured in createEJB()" , iaEx);
            throw new AmsException ("IllegalAccessException thrown: Unable to create Server EJB Object: " + objectName +
               EJB_SECURITY_MESSAGE + iaEx.getMessage(), iaEx);
        }
        catch (InvocationTargetException itEx)
        {
        	LOG.error("Exception occured in createEJB()" , itEx);
            throw new AmsException("InvocationTargetException thrown: Unable to create Server EJB Object: " + objectName +
               itEx.getMessage (),itEx);
        }
        catch (NamingException nEx)
        {
        	LOG.error("Exception occured in createEJB()" , nEx);
            throw new AmsException ("Naming Exception thrown: Unable to create Server EJB Object: " + objectName +
                nEx.getMessage (),nEx);
        }
        catch (ClassNotFoundException cnEx)
        {
        	LOG.error("Exception occured in createEJB()" , cnEx);
            throw new AmsException ("ClassNotFoundException thrown: Unable to create Server EJB Object: " + objectName +
               ". Nested exception: " + cnEx.getMessage (), cnEx);
        } 
        // Return the EJB object's remote interface
        return objectInterface;
    }
}
