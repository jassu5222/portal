package com.amsinc.ecsg.frame;

/*
 * @(#)GenericList
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.rmi.*;
import javax.ejb.*;
import java.util.Hashtable;
import com.amsinc.ecsg.util.*;

public interface GenericList extends EJBObject
{
  public void prepareList(String objectName)
	  throws RemoteException, AmsException;  
  public void prepareList(String objectName, String listTypeName)
	  throws RemoteException, AmsException;  
  public void prepareList(String objectName, String listTypeName, String[] listParameters)
	  throws RemoteException, AmsException;  
  public void prepareList(String objectName, String listTypeName, String[] listParameters, boolean wrapArguments)
	  throws AmsException, RemoteException;  
  public void getData ()
	  throws RemoteException, AmsException;  
  public void getData (int maxRows)
	  throws RemoteException, AmsException;  
  public void getData (int firstRow, int maxRows)
	  throws RemoteException, AmsException;  
  public int getObjectIndex(long objectID)
	  throws RemoteException, AmsException;  
  public String getAttribute(String attributeName)
	  throws RemoteException, AmsException;  
  public Hashtable getAttributes(String[] attributeNames)
	  throws RemoteException, AmsException;  
  public int getObjectCount()
	  throws RemoteException, AmsException;  
  public void scrollToObject(long objectID)
	  throws RemoteException, AmsException;  
  public void scrollToObjectByIndex(int index)
	  throws RemoteException, AmsException;  
  public boolean isDataRetrieved()
	  throws RemoteException;  
  public void getObjectInformation() 
	  throws AmsException, RemoteException;  
  public DocumentHandler getXmlResultSet()
	  throws AmsException, RemoteException;  
  public BusinessObject getBusinessObject(long objectID)
	  throws AmsException, RemoteException;  
  public BusinessObject getBusinessObject()
	  throws AmsException, RemoteException;  
  public String getIDAttributeName()
	  throws AmsException, RemoteException;  
}
