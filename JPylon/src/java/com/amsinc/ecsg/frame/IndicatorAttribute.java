/**
 * IndicatorAttribute object.  This class is a descendant of the
 * Business Object framework Attribute class.  It performs validation
 * on the attribute value to ensure the value is a valid Indicator type.
 * (i.e. "Y"-yes or "N"-no).
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 * @version 1.0
 */
package com.amsinc.ecsg.frame;

public class IndicatorAttribute extends Attribute
{
	private static final String INDICATOR_T = "T";

	public IndicatorAttribute ()
	 {
     }

	public IndicatorAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public IndicatorAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }

    // Perform attribute validation
    public void validate(String attributeValue) throws AmsException{
        // Ensure that the value beting set is Yes or No
        if (!attributeValue.isEmpty())
        {
          if(!attributeValue.equals("Y") && !attributeValue.equals("N") && !attributeValue.equals("X") && !attributeValue.equals(INDICATOR_T)){ //RKAZI CR709 Rel 8.2 Added Indicator "T"
             throw new InvalidAttributeValueException();
          }
        }
    }
}