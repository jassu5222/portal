package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvalidObjectAssociationException extends AmsException{
    protected String associationObjectName;
        
    public InvalidObjectAssociationException(){super("The ObjectIdentifier specified is not valid");}
    public InvalidObjectAssociationException(String associationName, String objectID){
        // Initialize the exception's association name
        super(objectID + " is not a valid association ObjectID for association attribute:" + associationName);
        this.associationObjectName = associationName;
    }
    
    // Method used to obtain the association object Name
    public String getAssociationObjectName(){
        return this.associationObjectName;
    }
    
    // Method used to obtain the association object Name
    public String getErrorID(){
        return AmsConstants.IVALID_ASSOCIATION_ATTRIBUTE;
    }
}