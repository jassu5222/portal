package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvalidObjectIdentifierException extends AmsException{
    String objectIdentifier;
    
    public InvalidObjectIdentifierException(){super("The ObjectIdentifier specified is not valid");}
    public InvalidObjectIdentifierException(String objectIdentifer){
        super("The ObjectIdentifier: "+objectIdentifer+ " is not valid");
        this.objectIdentifier = objectIdentifer;
    }
    
    public String getObjectId() {
        return objectIdentifier;
    }
}