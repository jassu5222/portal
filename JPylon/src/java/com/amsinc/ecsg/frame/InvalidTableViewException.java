package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class InvalidTableViewException extends DataObjectException{
    public InvalidTableViewException(){
        super("Table or View not valid in DataObject Processing");
        this.vendorErrorCode = 942;
    }
}
