/*
 * @(#)IssuedError
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

/** 
* This class represents an error that was issued from a business object. The
* ErrorManager class maintains a Vector of these objects throughout the
* lifetime of a transaction. It is basically a data object corresponding to
* the Error Code table. Basic get and set methods are implemented for this
* class.
*/
public class IssuedError implements java.io.Serializable
{
    /** Short identifier of the error code */
    private String errorCode;
    /** Descriptive textual string describing the error code */
    private String errorText;
    /** Severity level of the error. Higher values are more severe. Valid
    values are defined in the ErrorManager class and are INFO_ERROR_SEVERITY,
    WARNING_ERROR_SEVERITY, ERROR_SEVERITY, SEVERE_ERROR_SEVERITY, and
    FATAL_ERROR_SEVERITY. */
    private int severity;
    /** Indicates if the error should be logged ('Y') or not ('N'). */
    private String logInd;

    /**
     * Creates an <code>IssuedError</code> instance populated with default
     * values. Default values are blank error code, error text, and log
     * indicator and a 0 for severity.
     */
    public IssuedError()
    {
        errorCode = " ";
        errorText = " ";
        severity  = 0;
        logInd    = " ";
    }
    /**
     * Creates an <code>IssuedError</code> instance populated with the
     * specified information.
     *
     * @param err_code The error code as stored in the error code database
     * table.
     * @param err_text The error text as stored in the error code database
     * table.
     * @param err_severity The error severity as stored in the error code
     * database table.
     * @param log Indicates if logging is required for this error. Also stored
     * in the error code database table.
     */
    public IssuedError (String err_code, String err_text,
                        int err_severity, String log)
    {
        errorCode = err_code;
        errorText = err_text;
        severity  = err_severity;
        logInd    = log;
    }
    /**
     * This is a copy constructor. It creates a new <code>IssuedError</code>
     * instance with the same values as the specified one.
     *
     * @param issuedError The instance to make a copy of.
     */
    public IssuedError (IssuedError issuedError)
    {
        errorCode = issuedError.getErrorCode ();
        errorText = issuedError.getErrorText ();
        severity  = issuedError.getSeverity ();
        logInd    = issuedError.getLogInd ();
    }
    /**
     * Getter for the error code string.
     *
     * @return The error code string.
     */
    public String getErrorCode()
    {
        return errorCode;
    }
    /**
     * Setter for the error code string.
     *
     * @param errorCode Value to set the error code string to.
     */
    public void setErrorCode (String errorCode) 
    {
        this.errorCode = errorCode;
    }

    /**
     * Getter for the error text string.
     *
     * @return The error text string.
     */
    public String getErrorText ()
    {
        return errorText;
    }
    /**
     * Setter for the error code text.
     *
     * @param errorText Value to set the error code text to.
     */
    public void setErrorText (String errorText) 
    {
        this.errorText = errorText;
    } 
    /**
     * Getter for the error severity level.
     *
     * @return The error severity level.
     */
    public int getSeverity ()
    {
        return severity;
    }
    /**
     * Setter for the error severity level.
     *
     * @param severity Value to set the error severity level to.
     */
    public void setSeverity (int severity) 
    {
        this.severity = severity;
    } 
    /**
     * Getter for the error logging indicator.
     *
     * @return The error logging indicator.
     */
    public String getLogInd ()
    {
        return logInd;
    }
    /**
     * Setter for the error logging indicator.
     *
     * @param logInd Value to set the error logging indicator to.
     */
    public void setlogInd (String logInd) 
    {
        this.logInd = logInd;
    }
} // end "public class IssuedError"
