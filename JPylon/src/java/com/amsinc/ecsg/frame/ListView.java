/*
 * @(#)ListView
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import java.sql.Clob;

import javax.ejb.*;
import java.util.Hashtable;
import com.amsinc.ecsg.util.*;

public interface ListView extends EJBObject
{
  public void getRecords(String[] sqlArguments)
      throws RemoteException, AmsException;
  public void getRecords(String[] sqlArguments, int maxRows)
      throws RemoteException, AmsException;
  public void getRecords(String[] sqlArguments, int firstRow, int maxRows)
      throws RemoteException, AmsException;
  public void getRecords()
      throws RemoteException, AmsException;
  public void getRecords(int maxRows)
      throws RemoteException, AmsException;
  public void getRecords(int firstRow, int maxRows)
      throws RemoteException, AmsException;
  public void getRecords(String[] arguments, boolean wrapArguments) 
      throws RemoteException, AmsException;
  public void getRecords(String[] arguments, boolean wrapArguments, int maxRows) 
      throws RemoteException, AmsException;
  public void getRecords(String[] arguments, boolean wrapArguments, int firstRow, int maxRows)
      throws RemoteException, AmsException;
  public int getRecordCount()
      throws RemoteException, AmsException;
  public String getRecordValue(String columnName)
      throws RemoteException, AmsException;
  public void scrollToRow(int rowID)
      throws RemoteException, AmsException;
  public DocumentHandler getXmlResultSet()
      throws AmsException, RemoteException;
  public DocumentHandler getRecordsAsXml (String[] sqlArguments)
      throws RemoteException, AmsException;
  public DocumentHandler getRecordsAsXml (String[] sqlArguments, int maxRows)
      throws RemoteException, AmsException;
  public DocumentHandler getRecordsAsXml (String[] sqlArguments, int firstRow, int maxRows)
      throws RemoteException, AmsException;
  public DocumentHandler getRecordsAsXml ()
      throws RemoteException, AmsException;
  public DocumentHandler getRecordsAsXml (int maxRows)
      throws RemoteException, AmsException;
  public DocumentHandler getRecordsAsXml (int firstRow, int maxRows)
      throws RemoteException, AmsException;
  public int getReturnCount ()
      throws RemoteException, AmsException;
  
  //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
  public String getRecordValueAsClob (String columnName)
     throws RemoteException, AmsException;
  //trudden CR-564 End   
}
