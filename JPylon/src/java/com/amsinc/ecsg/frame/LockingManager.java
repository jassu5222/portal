/*
 * @(#)LockingManager
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.JPylonProperties;

/**
 * The LockingManager is a with several static methods that can be used to lock instruments. Developers are responsible for using
 * this class to lock, unlock, and check for locks on instruments.
 *
 * @version 1.0
 */
public class LockingManager implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(LockingManager.class);
	private static String dbmsParm;

	static {
		try {
			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			dbmsParm = jPylonProperties.getString("dataSourceName");
		} catch (Exception e) {
			dbmsParm = "";
		}
	}

	private LockingManager() {
	}

	/**
	 * This synchronized methods retrieves a database connection from the data source factory.
	 *
	 * @param onServer
	 *            If this class is being used off the server this value must be set to false for a connection to be established.
	 * @return The connection that was retrieved from the datasource factory.
	 *
	 */
	private static Connection connect(boolean onServer) throws AmsException {
		try {
			DataSource ds = DataSourceFactory.getDataSource(dbmsParm, onServer);
			Connection dbmsConnection = ds.getConnection();
			return dbmsConnection;
		} catch (SQLException e) {
			throw new AmsException("Error on connecting to Datasource in DataObject using DBMS " + "parms: " + dbmsParm
					+ " Nested Exception: " + e.getMessage(), e);
		}
	}

	/**
	 * Close the established connection to the database data source factory.
	 *
	 * @param Connection
	 *            The esablished database connection
	 *

	 */
	private static void disconnect(Connection dbmsConnection) throws AmsException {
		try {
			if (dbmsConnection != null)
				dbmsConnection.close();
		} catch (SQLException e) {
			throw new AmsException("Error on disconnecting DataObject " + e.getMessage(), e);
		}
	}

	/**
	 * This synchronized method determines if the instrument specified is locked by the user specified. It returns true or false
	 * depending on what it finds
	 *
	 * @param instrumentOid
	 *            The unique identifier of the instrument being processed
	 * @param userOid
	 *            The unique identifier of the user being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 * @return True if the instrument is locked by the user specified, otherwise false.
	 *
	 */
	public static boolean isLocked(long instrumentOid, long userOid, boolean onServer) throws AmsException {
		// Checks to see if an instrument is locked by a user
		try (Connection dbmsConnection = connect(onServer);
				PreparedStatement statement = dbmsConnection.prepareStatement(
						"SELECT INSTRUMENT_OID, USER_OID FROM INSTRUMENT_LOCK WHERE INSTRUMENT_OID = ? AND USER_OID = ?");) {
			statement.setLong(1, instrumentOid);
			statement.setLong(2, userOid);
			// Attempt to select the row(s) from the database
			try (ResultSet results = statement.executeQuery()) {
				if (!results.next())
					return false;
				else
					return true;
			} catch (SQLException sqlEx) {
				return false;
			}
		} catch (SQLException sqlEx) {
			return false;
		}

	}

	/**
	 * This synchronized method determines if the instrument specified is locked. It returns true or false depending on what it
	 * finds
	 *
	 * @param instrumentOid
	 *            The unique identifier of the instrument being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 * @return True if the instrument is locked, otherwise false.
	 *
	 */
	public static boolean isLocked(long instrumentOid, boolean onServer) throws AmsException {
		// Checks to see if an instrument is locked by a user
		try (Connection dbmsConnection = connect(onServer);
				PreparedStatement statement = dbmsConnection
						.prepareStatement("SELECT INSTRUMENT_OID FROM INSTRUMENT_LOCK WHERE INSTRUMENT_OID = ?");) {
			statement.setLong(1, instrumentOid);
			// Attempt to select the row(s) from the database
			try (ResultSet results = statement.executeQuery()) {
				if (!results.next())
					return false;
				else
					return true;
			}
		} catch (SQLException sqlEx) {
			return false;
		}

	}

	/**
	 * This synchronized method locks the instrument by the user specified. If the instrument cannot be locked for any reason it
	 * throws an InstrumentLockException
	 *
	 * @param instrumentOid
	 *            The unique identifier of the instrument being processed
	 * @param userOid
	 *            The unique identifier of the user being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static void lockBusinessObject(long instrumentOid, long userOid, boolean onServer)
			throws AmsException, InstrumentLockException {
		long userLockedOid;
		String firstName;
		String lastName;
		String instrumentNumber;
		Connection dbmsConnection = null;
		PreparedStatement statement = null;

		// Attempt to insert a row into the instrument lock table. If there is already a row
		// in the table, another user must already have the table locked.
		try {
			dbmsConnection = connect(onServer);
			// PPX-208 - Ravindra B - 03/24/2011 - Start.
			statement = dbmsConnection.prepareStatement("INSERT INTO INSTRUMENT_LOCK(INSTRUMENT_OID,USER_OID) VALUES (?,?)");
			// PPX-208 - Ravindra B - 03/24/2011 - End.
			statement.setLong(1, instrumentOid);
			statement.setLong(2, userOid);
			// Attempt to insert the new row
			statement.executeUpdate();
		} catch (SQLException sqlEx) {
			// If a user already has the table locked select the row from the table and look
			// up the user name. Throw an error with the username so the calling process can
			// issue an error with the username.
			ResultSet results = null;
			try {
				if (statement != null)
					statement.close();
				// Attempt to select the user_oid from the lock table
				statement = dbmsConnection.prepareStatement("SELECT USER_OID FROM INSTRUMENT_LOCK WHERE INSTRUMENT_OID = ?");
				statement.setLong(1, instrumentOid);
				results = statement.executeQuery();
				if (results.next()) {
					userLockedOid = results.getLong("USER_OID");
				} else {
					throw new InstrumentLockException();
				}
			} catch (SQLException sqlEx2) {
				// If an error was caught, we couldn't select the user_oid from the lock table.
				// If this occurs, throw the base InstrumentLockException
				throw new InstrumentLockException();
			}
			try {
				// If we have successfully gotten the locked user's oid get the user's login_name
				// from the database
				statement.close();
				statement = dbmsConnection.prepareStatement("SELECT FIRST_NAME,LAST_NAME FROM USERS WHERE USER_OID = ?");
				statement.setLong(1, userLockedOid);
				// Execute the query to get the user's login id using the user_oid
				results = statement.executeQuery();
				if (results.next()) {
					// Save off the firstName and lastName if the select was successful
					firstName = results.getString("FIRST_NAME");
					lastName = results.getString("LAST_NAME");
				} else {
					throw new InstrumentLockException();
				}

				statement.close();
				statement = dbmsConnection
						.prepareStatement("SELECT COMPLETE_INSTRUMENT_ID FROM INSTRUMENT WHERE INSTRUMENT_OID = ?");
				statement.setLong(1, instrumentOid);
				// Execute the query to get the user's login id using the user_oid
				results = statement.executeQuery();
				if (results.next()) {
					// Save off the firstName and lastName if the select was successful
					instrumentNumber = results.getString("COMPLETE_INSTRUMENT_ID");
				} else {
					throw new InstrumentLockException();
				}
			} catch (SQLException sqlEx2) {
				// If an error was caught, we couldn't select the user_identifier from the users table.
				// If this occurs, throw the base InstrumentLockException
				throw new InstrumentLockException();
			}
			throw new InstrumentLockException(firstName, lastName, instrumentNumber, userLockedOid);
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					LOG.error("Exception closing statement", e);
				}
			}
			disconnect(dbmsConnection);
		}
	}

	// PPX-208 - Ravindra B - 03/18/2011 - Start.
	/**
	 * This synchronized method locks the instrument by the agent specified. If the instrument cannot be locked for any reason it
	 * throws an InstrumentLockException
	 *
	 * @param instrumentOid
	 *            The unique identifier of the instrument being processed
	 * @param agent
	 *            The unique identifier of the user being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static void lockBusinessObject(long instrumentOid, String agentId, boolean onServer)
			throws AmsException, InstrumentLockException {
		String instrumentNumber;
		Connection dbmsConnection = null;
		PreparedStatement statement = null;

		// Attempt to insert a row into the instrument lock table. If there is already a row
		// in the table, another user must already have the table locked.
		try {
			dbmsConnection = connect(onServer);
			statement = dbmsConnection.prepareStatement("INSERT INTO INSTRUMENT_LOCK(INSTRUMENT_OID,AGENT_ID) VALUES (?,?)");
			statement.setLong(1, instrumentOid);
			statement.setString(2, agentId);
			// Attempt to insert the new row
			statement.executeUpdate();
		} catch (SQLException sqlEx) {
			// If agent already has the table locked select the row from the table and look
			// up the agent name. Throw an error with the agent so the calling process can
			// issue an error with the agent.
			ResultSet results = null;
			try {
				statement.close();
				// Attempt to select the agentId from the lock table
				statement = dbmsConnection.prepareStatement("SELECT AGENT_ID FROM INSTRUMENT_LOCK WHERE INSTRUMENT_OID = ?");
				statement.setLong(1, instrumentOid);
				results = statement.executeQuery();
				if (results.next()) {
					agentId = results.getString("AGENT_ID");
				} else {
					throw new InstrumentLockException();
				}
			} catch (SQLException sqlEx2) {
				// If an error was caught, we couldn't select the agentId from the lock table.
				// If this occurs, throw the base InstrumentLockException
				throw new InstrumentLockException();
			}
			try {

				statement.close();
				statement = dbmsConnection
						.prepareStatement("SELECT COMPLETE_INSTRUMENT_ID FROM INSTRUMENT WHERE INSTRUMENT_OID = ?");
				statement.setLong(1, instrumentOid);
				// Execute the query to get the user's login id using the user_oid
				results = statement.executeQuery();
				if (results.next()) {
					// Save off the firstName and lastName if the select was successful
					instrumentNumber = results.getString("COMPLETE_INSTRUMENT_ID");
				} else {
					throw new InstrumentLockException();
				}
			} catch (SQLException sqlEx2) {
				// If an error was caught, we couldn't select the user_identifier from the users table.
				// If this occurs, throw the base InstrumentLockException
				throw new InstrumentLockException();
			}
			throw new InstrumentLockException(instrumentNumber, agentId);
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LOG.error("Exception closing statement", e);
			}
			disconnect(dbmsConnection);

		}
	}

	// PPX-208 - Ravindra B - 03/18/2011 - End.
	/**
	 * This synchronized method unlocks the instrument by the instrumentOid specified. If the instrument cannot be unlocked for any
	 * reason it throws an InstrumentLockException
	 *
	 * @param instrumentOid
	 *            The unique identifier of the instrument being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static void unlockBusinessObjectByInstrument(long instrumentOid, boolean onServer)
			throws AmsException, InstrumentLockException {
		// Delete a row from the lock table by instrument oid

		// Attempt to delete a row into the instrument lock table.
		try (Connection dbmsConnection = connect(onServer);
				PreparedStatement statement = dbmsConnection.prepareStatement("DELETE INSTRUMENT_LOCK WHERE INSTRUMENT_OID = ?")) {

			statement.setLong(1, instrumentOid);
			// Attempt to delete the row
			statement.executeUpdate();
		} catch (SQLException sqlEx) {
			// If an exception occurs something has gone wrong.
			throw new InstrumentLockException();
		}

	}

	/**
	 * This synchronized method unlocks the instrument by the userOid specified. If the instrument cannot be unlocked for any reason
	 * it throws an InstrumentLockException
	 *
	 * @param userOid
	 *            The unique identifier of the user being processed
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static void unlockBusinessObjectByUser(long userOid, boolean onServer) throws AmsException, InstrumentLockException {
		// Delete a row from the lock table by instrument oid

		// Attempt to delete a row(s) from the instrument lock table.
		try (Connection dbmsConnection = connect(onServer);
				PreparedStatement statement = dbmsConnection.prepareStatement("DELETE INSTRUMENT_LOCK WHERE USER_OID = ?")) {

			statement.setLong(1, userOid);
			// Attempt to delete the row
			statement.executeUpdate();
		} catch (SQLException sqlEx) {
			LOG.error("SQLException while unlocking instruments for a user...", sqlEx);

			// If an exception occurs something has gone wrong.
			throw new InstrumentLockException();
		}

	}

	// IR DUM050356373 - IValavala - 08/10/2012 - Start.
	/**
	 * This adds an entry in agent lock table with agent ID and agent type. Only one agent can add a lock at any point of time for a
	 * particular agent type
	 *
	 * @param agentID
	 *            The unique identifier of the agent trying to acquire the lock
	 * @param agentType
	 *            The unique identifier for the type of agent. Example: PAYUPLOAD
	 * @param The
	 *            unique identifier for the agent who currently holds the lock.
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static boolean lockAgent(String agentId, String agentType, boolean onServer) throws AmsException, SQLException {

		Connection dbmsConnection = null;
		PreparedStatement statement = null;
		String lockedbyAgent;

		// Attempt to insert a row into the agent lock table. If there is already a row
		// in the table, another agent must already have the table locked.
		try {
			dbmsConnection = connect(onServer);
			statement = dbmsConnection.prepareStatement("INSERT INTO AGENT_LOCK(AGENT_ID, AGENT_TYPE) VALUES (?,?)");
			statement.setString(1, agentId);
			statement.setString(2, agentType);
			// Attempt to insert the new row
			statement.executeUpdate();
			return true;
		} catch (SQLException sqlEx) {
			// If agent already has the table locked select the row from the table and look
			// up the agent name. Throw an error with the agent so the calling process can
			// issue an error with the agent.
			ResultSet results = null;
			try {
				if (statement != null)
					statement.close();
				// Attempt to select the agentId from the lock table
				statement = dbmsConnection.prepareStatement("SELECT AGENT_ID FROM AGENT_LOCK WHERE AGENT_TYPE = ?");
				statement.setString(1, agentType);
				results = statement.executeQuery();
				if (results.next()) {
					lockedbyAgent = results.getString("AGENT_ID");
					throw new AgentLockException(agentId, agentType, lockedbyAgent);
				}
			} catch (SQLException sqlEx2) {
				// If an error was caught, we couldn't select the agentId from the lock table.
				throw sqlEx2;
			}
			return false;

		} finally {
			if (statement != null) {
				statement.close();
			}
			disconnect(dbmsConnection);
		}
	}

	// IR DUM050356373 - IValavala - 08/10/2012 - Start.
	/**
	 * This clears an entry in agent lock table with agent ID and agent type.
	 *
	 * @param agentID
	 *            The unique identifier of the agent trying to acquire the lock
	 * @param agentType
	 *            The unique identifier for the type of agent. Example: PAYUPLOAD
	 * @param The
	 *            unique identifier for the agent who currently holds the lock.
	 * @param onServer
	 *            This value should be set to true if being called from either a business object or a mediator. Otherwise, this
	 *            value should be set to false
	 */
	public static void unlockAgent(String agentId, String agentType, boolean onServer) throws AmsException, SQLException {

		// Attempt to insert a row into the agent lock table. If there is already a row
		// in the table, another agent must already have the table locked.
		try (Connection dbmsConnection = connect(onServer);
				PreparedStatement statement = dbmsConnection
						.prepareStatement("DELETE FROM AGENT_LOCK WHERE AGENT_ID=? AND AGENT_TYPE=? ")) {
			statement.setString(1, agentId);
			statement.setString(2, agentType);
			statement.executeUpdate();

		} catch (SQLException sqlEx) {
			throw sqlEx;
		}

	}

}
