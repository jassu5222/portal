/*
 * @(#)Logger
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;


/**
 * The Logger class handles messages logging to files as well as other targets
 * in the future. There are 4 types of log files: Informational, Warning,
 * Error, and Debug. Error codes that are specified to be logged are written
 * to the appropriate file. Debug statements are written to the debug file.
 * This logging component starts a seperate thread that acts as a socket server
 * to read incoming messages and write them to the appropriate log file.
 * This allows the calling program to continue processing because log messages
 * can be written in less than absolute real time (i.e. a few seconds later).
 * This speeds up transaction time by allowing the process to continue without
 * having to wait for the message to be written to the file.
 * The file name format is &lt;logType&gt;&lt;dateTime&gt;.txt and all log files
 * are located in a log file directory.
 */
public class Logger
{
    /**
     * Singleton instance variable
     */
    private static Logger mInstance = null;
    /**
     * Server location of logging server
     */
    private String serverLocation = null;
    /**
     * Port number of logging server
     */
    private int port;
    /** Number of times to retry making a client socket connection when
        a connect fails */
    private int retryCount;
    /**
     * Internet address of logging server
     */
    private InetAddress addr;

	/*
     * indicates whether log is sent to Logging server
     */
    private boolean useLoggingServer = true;

    /*
     * indicates whtether log is also written to STD Out
     */
    private boolean sendToSTDOut = true;

    /*
     * Log level
     */
    private int logLevel = 1;


	/**
     * Constants for different levels of logging
     */

    public static final int DEBUG_LOG_SEVERITY = 2;
    public static final int ERROR_LOG_SEVERITY = 5;
    public static final int WARN_LOG_SEVERITY = 3;
    public static final int INFO_LOG_SEVERITY = 1;

    /**
     * Default Constructor
     */
    public Logger ()
    {
        mInstance = this;
        // Initialize the server location
        serverLocation = "localhost";
        port = 4000;
        retryCount = 0;
        try
        {
          addr = InetAddress.getByName (serverLocation);
        }
        catch (UnknownHostException e)
        {
          System.out.println("Logger: Unknown Host " + serverLocation);
        }
    }

    /**
     * Log the given message with the given severity level
     *
     * @param   category Object the log message is associated with. This is
     * used to determine which file the log message is written to.
     * @param   processName - Adding this parm to facilitate separate logs for each process
     * @param   message Message to log
     * @param   severity severity level of log message
     * @param   retryCount number of times we have tried to establish a socket
     * connection

     */
    //IValavala IR POUM080661913
    public void logMessage (String category, String processName, String message, int severity,
                            int retryCount)
    {
      if (severity < logLevel) {
    	  return;
      }

      if (sendToSTDOut) {
    	  System.out.println(message);
      }
      if (!useLoggingServer){
    	  return;
      }

      try(Socket clientSock = new Socket (addr, port);
          OutputStream stream = clientSock.getOutputStream ();
          PrintWriter writer = new PrintWriter (stream))
      {
          // Create a client socket
          
          // Create the string to be written to the log file
          StringBuilder logMessageBuffer = new StringBuilder(category);
          logMessageBuffer.append ("$$");
          logMessageBuffer.append ("" + severity);
          logMessageBuffer.append ("$$");
          //IValavala IR POUM080661913. Add processName
          logMessageBuffer.append(processName);
          logMessageBuffer.append("$$");
          logMessageBuffer.append (message);
          // Write the message to the socket
          // The format is <objectname>$$<severity>$$<message>
          writer.println (logMessageBuffer.toString ());
          writer.flush ();
          
      }
      catch (Exception e)
      {
          if (retryCount < this.retryCount)
          {
             logMessage (category, processName, message, severity, retryCount + 1);
          }
          else
          {
             System.out.println ("Exception caught in Logger::logMessage");
             System.out.println (e);
          }
      }

    }


    /**
     * Log the given message with the given severity level
     *
     * @param   category Object the log message is associated with. This is
     * used to determine which file the log message is written to.
     * @param   message Message to log
     * @param   severity severity level of log message
     * @param   retryCount number of times we have tried to establish a socket
     * connection

     */
    //IValavala IR POUM080661913. Making this private so only the logMessage method with processName
    //is available to other classes
    private void logMessage (String category, String message, int severity,
                            int retryCount)
    {
      if (severity < logLevel) {
    	  return;
      }

      if (sendToSTDOut) {
    	  System.out.println(message);
      }
      if (!useLoggingServer){
    	  return;
      }

      try(Socket clientSock = new Socket (addr, port);
          OutputStream stream = clientSock.getOutputStream ();
          PrintWriter writer = new PrintWriter (stream))
      {
          // Create a client socket
          
          // Create the string to be written to the log file
          StringBuilder logMessageBuffer = new StringBuilder(category);
          logMessageBuffer.append ("$$");
          logMessageBuffer.append ("" + severity);
          logMessageBuffer.append ("$$");
          //IValavala IR POUM080661913. If processName was not provided use default.
          logMessageBuffer.append("default");
          logMessageBuffer.append("$$");
          logMessageBuffer.append (message);
          // Write the message to the socket
          // The format is <objectname>$$<severity>$$<message>
          writer.println (logMessageBuffer.toString ());
          writer.flush ();
          
      }
      catch (Exception e)
      {
          if (retryCount < this.retryCount)
          {
             logMessage (category, message, severity, retryCount + 1);
          }
          else
          {
             System.out.println ("Exception caught in Logger::logMessage");
             System.out.println (e);
          }
      }

    }

    /**
     * Log a defined message in the error code table.
     * This method is used for convenience for messages that have no
     * substitution values.
     *
     * @param category Debugging category this message is for. This is
     * used to determine which file the log message is written to.
     * @param messageCode message code (Same as error code in error code table)

     */
    public static void log (String category, String messageCode)
    {
        Logger.log (category, messageCode, new String[0]);
    }

    /**
     * Log a defined message in the error code table.
     * This method is used for convenience for messages that have one
     * substitution value.
     *
     * @param category Debugging category this message is for. This is
     * used to determine which file the log message is written to.
     * @param messageCode message code (Same as error code in error code table)
     * @param sub1 substitution value #1

     */
    public static void log (String category, String messageCode, String sub1)
    {
        String[] subs = {sub1};
        Logger.log (category, messageCode, subs);
    }

  //IVAlavala IR POUM080661913
    public static void log (String category, String processName, String message, int severity)
    {

    	//SHR fixed null pointer exception- Check for null value
		if (processName==null || processName.isEmpty()) 
			processName  = "default";
    	 Logger.getLogger ().logMessage (category, processName, message, severity, 0);


    }

    public static void log (String category, String processName, String message, Exception e, int severity)
    {
    	StringBuilder errorStack=new StringBuilder(message);
    	errorStack.append("- Exception Message: " + e.getMessage() + "- Stack Trace:\r\n" );
    	for (StackTraceElement ste : e.getStackTrace()) { 
            errorStack.append(ste+ "\r\n");
        }

        Logger.getLogger ().logMessage (category, processName, errorStack.toString(), severity, 0);


    }
    
    /**
     * Log a defined message in the error code table.
     * This method is used for convenience for messages that have two
     * substitution values.
     *
     * @param category Debugging category this message is for. This is
     * used to determine which file the log message is written to.
     * @param messageCode message code (Same as error code in error code table)
     * @param sub1 substitution value #1
     * @param sub2 substitution value #2

     */
    public static void log (String category, String messageCode, String sub1, String sub2)
    {
        String[] subs = {sub1,sub2};
        Logger.log (category, messageCode, subs);
    }

    /**
     * Log a defined message in the error code table.
     * This method is used for convenience for messages that have three
     * substitution values.
     *
     * @param category Debugging category this message is for. This is
     * used to determine which file the log message is written to.
     * @param messageCode message code (Same as error code in error code table)
     * @param sub1 substitution value #1
     * @param sub2 substitution value #2
     * @param sub3 substitution value #3

     */
    public static void log (String category, String messageCode, String sub1, String sub2, String sub3)
    {
        String[] subs = {sub1,sub2,sub3};
        Logger.log (category, messageCode, subs);
    }

    /**
     * Log a defined message in the error code table.
     * Insert the substitution values if necessary into the message text.
     *
     * @param category Debugging category this message is for. This is
     * used to determine which file the log message is written to.
     * @param messageCode message code (Same as error code in error code table)
     * @param substitutionValues collection of strings to be inserted into
     * message text

     */
    public static void log (String category, String messageCode, String[] substitutionValues)
    {
        // Look the message up in the catalog
        IssuedError error =
            ErrorManager.findErrorCode (messageCode, "");
        // check that error is not null, otherwise create a default error code
        if (error == null)
            return;
        // Get the message text
        String message = error.getErrorText ();

        // put substitution values into error message text
        Logger.getLogger().logMessage (
            category, MessageFormat.format (message,substitutionValues),
            error.getSeverity (), 0);
    }

    /**
     * Static accessor to the singleton Logger instance.
     * This method implements the singleton pattern to ensure that only
     * one Logger is running per server environment.
     *

     */
    public static Logger getLogger ()
    {
    	if (mInstance == null)
    	{
    		synchronized (Logger.class)
    		{
    			if (mInstance == null)
    			{
    				String propValue = null;
    				PropertyResourceBundle serverProps =
    					(PropertyResourceBundle)PropertyResourceBundle.getBundle (
    							"LoggerServer");

    				mInstance = new Logger ();

    				try
    				{
    					propValue = serverProps.getString ("useLoggingServer");
    					mInstance.setUseLoggingServer("true".equals(propValue));
    				}
    				catch (MissingResourceException mrEx)
    				{
    				}

    				try
    				{
    					propValue = serverProps.getString ("sendToSTDOut");
    					mInstance.setSendToSTDOut("true".equals(propValue));
    				}
    				catch (MissingResourceException mrEx)
    				{
    				}

    				try
    				{
    					propValue = serverProps.getString ("logLevel");
    					int i = Integer.parseInt (propValue);
    					if (i >= INFO_LOG_SEVERITY && i <= ERROR_LOG_SEVERITY) {
    						mInstance.setLogLevel(Integer.parseInt (propValue));
    					}
    					else {
    						System.out.println("Logger: invalid logLevel, valid values are 1 to 5, defaulting to logLevel 1");
    					}

    				}
    				catch (NumberFormatException nfEx)
					{
    					System.out.println("Logger: invalid logLevel, defaulting to logLevel 1");
					}
    				catch (MissingResourceException mrEx)
    				{
    				}

    				try
    				{
    					propValue = serverProps.getString ("loggingServer");
    					mInstance.setServerLocation (propValue);
    				}
    				catch (MissingResourceException mrEx)
    				{
    				}
    				try
    				{
    					propValue = serverProps.getString ("loggingPort");
    					if (propValue != null)
    					{
    						try
    						{
    							mInstance.setServerPort (Integer.parseInt (propValue));
    						}
    						catch (NumberFormatException nfEx)
    						{
    						}
    					}
    				}
    				catch (MissingResourceException mrEx2)
    				{
    				}
    				try
    				{
    					propValue = serverProps.getString ("loggingClientRetryCount");
    					if (propValue != null)
    					{
    						try
    						{
    							mInstance.setRetryCount (Integer.parseInt (propValue));
    						}
    						catch (NumberFormatException nfEx)
    						{
    						}
    					}
    				}
    				catch (MissingResourceException mrEx2)
    				{
    				}
    			}
    			return mInstance;
    		}
    	}
    	return mInstance;
    }

    /**
     * This method sets the application server location.
     * This is where the logging server is running.
     *

     */
    public synchronized void setServerLocation (String serverLocation)
    {
       this.serverLocation = serverLocation;
       int index = serverLocation.indexOf (serverLocation);
       try
       {
          if (index == -1)
          {
            addr = InetAddress.getByName (serverLocation);
          }
          else
          {
            addr = InetAddress.getByName (serverLocation.substring (0, index));
          }
       }
       catch (UnknownHostException e)
       {
          System.out.println ("Logger: Unknown Host " + serverLocation);
       }
    }

    public void setUseLoggingServer(boolean useLoggingServer) {
		this.useLoggingServer = useLoggingServer;
	}

	public void setSendToSTDOut(boolean sendToSTDOut) {
		this.sendToSTDOut = sendToSTDOut;
	}

	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

    /**
     * This method sets the application server location.
     * This is where the logging server is running.
     *

     */
    public synchronized void setServerPort (int port)
    {
       this.port = port;
    }

    /**
     * This method sets the socket connect retry count.
     *

     */
    public synchronized void setRetryCount (int retryCount)
    {
       this.retryCount = retryCount;
    }

    /** Nar IR-T36000032592 Rel9.2 09/28/2014 Add - Begin 
     * 
     * Main method that allows a user to stop the logger server process
     * The message sent to the socket will cause it to shut down.
     *
    public static void main(String argc[])
     {
         if((argc.length > 0) && (argc[0].equalsIgnoreCase("stop")))
          {
             Logger.getLogger ().logMessage (LoggerServerLogic.STOP, LoggerServerLogic.STOP, 0, 0);
          }
     }
     Nar IR-T36000032592 Rel9.2 09/28/2014 Add - End */
}
