/*
 * @(#)MediatorHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

public interface MediatorHome extends javax.ejb.EJBHome
{
  public Mediator create()
        throws CreateException, RemoteException;
}
