/*
 * @(#)MediatorInfo
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.*;

public class MediatorInfo {

    // Class variables
    private DocumentMap inputParmMap;
    private DocumentMap outputParmMap;
    
    public MediatorInfo() {
    }

    public DocumentMap getInputParmMap() {
      return inputParmMap;
    }
    public DocumentMap getOutputParmMap() {
      return outputParmMap;
    }
    public void setInputParmMap(DocumentMap docMap) {
      inputParmMap = docMap;
    }
    public void setOutputParmMap(DocumentMap docMap) {
      outputParmMap = docMap;
    }
}
    
    