/*
 * @(#)MediatorInfoFactory
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

/**
 * MediatorInfoFactory is used to hold XML parameter mapping information for each Mediator and its additional map files. This class
 * acts as a singleton that reads the map files and then caches them for faster access.
 */
public class MediatorInfoFactory {
	private static final Logger LOG = LoggerFactory.getLogger(MediatorInfoFactory.class);

	/**
	 * The reference to the singleton static instance of the factory class
	 */
	private static MediatorInfoFactory instance = null;
	/**
	 * The cached collection of mediator info objects
	 */
	private Hashtable mediatorInfoList;

	/**
	 * This method provides static access to the mediator info factory instance. This implements the singleton pattern. NOTE: This
	 * getter should not be used on the very first access to the NavigationManager.
	 *
	 * @return NavigationManager the singleton instance
	 */
	public static MediatorInfoFactory getMediatorInfoFactory() {
		if (instance == null) {
			synchronized (MediatorInfoFactory.class) {
				if (instance == null)
					instance = new MediatorInfoFactory();
			}
		}
		return instance;
	}

	/**
	 * Default constructor
	 *
	 */
	private MediatorInfoFactory() {
		mediatorInfoList = new Hashtable();
	}

	/**
	 * This method is the primary access method. It returns the mediator info object for the specified map file.
	 *
	 * @param String
	 *            mediatorName the name of the mediator, or map file, to access
	 * @return MediatorInfo the mediator info object
	 */
	public MediatorInfo getMediatorInfo(String mediatorName) {
		// If mediator is null, return empty parameter maps
		if (mediatorName == null) {
			MediatorInfo medInfo = new MediatorInfo();
			medInfo.setInputParmMap(new DocumentMap(new Hashtable()));
			return medInfo;
		}

		LOG.debug("MediatorInfoFactory getMediatorInfo looking for {}", mediatorName);
		MediatorInfo medInfo = (MediatorInfo) mediatorInfoList.get(mediatorName);
		if (medInfo == null) {
			// If not found, this is the first time so we must create it.
			// The input and output files are based on the following naming convention
			// <mediatorName>.xml.
			// This xml document should have entries representing the input parameters.
			medInfo = new MediatorInfo();

			DocumentHandler doc = XmlConfigFileCache.readConfigFile("/mapfiles/".concat(mediatorName).concat(".xml"));
			medInfo.setInputParmMap(new DocumentMap(doc));

			// Store in hashtable for future client requests
			mediatorInfoList.put(mediatorName, medInfo);
		}
		return medInfo;
	}

}
