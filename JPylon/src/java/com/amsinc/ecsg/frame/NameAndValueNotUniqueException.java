package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NameAndValueNotUniqueException extends AmsException{
    public NameAndValueNotUniqueException(){super("The attribute name/value pair is not unique.");}
    public NameAndValueNotUniqueException(String attributeName, String attributeValue){
        super("The attribute value: "+ attributeValue + " is not unique for attribute: " + attributeName);
    }
}