package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ObjectNotFoundException extends AmsException{
    public ObjectNotFoundException(){super("The object could not be found.");}
    public ObjectNotFoundException(String attributeName, String attributeValue){
        super("The object could not be found using the following parameters: " + " AttributeName: " + attributeName + " AttributeValue: " + attributeValue);
    }
}
