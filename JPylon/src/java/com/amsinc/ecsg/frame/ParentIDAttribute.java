package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ParentIDAttribute extends NumberAttribute {
	public ParentIDAttribute ()
	 {
     }
	public ParentIDAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public ParentIDAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
}
