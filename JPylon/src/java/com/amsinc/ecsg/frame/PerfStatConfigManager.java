package com.amsinc.ecsg.frame;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.frame.ServerDeploy;

public class PerfStatConfigManager {
	private static final Logger LOG = LoggerFactory.getLogger(PerfStatConfigManager.class);
    /**
     * The reference to the one singleton instance of this class.
     */
    private static PerfStatConfigManager mInstance = null;
    /**
     * The server location of the EJB server
     */
	private String serverLocation = null;
    /**
     * The collection of Perf Stat Config data stored in memory for faster access.
     */
    private Hashtable htOverallPerfStatConfig;

    /**
     * Indicator to state whether the configuration information was read correctly
     */
    private boolean bConfigurationValid;


	/**
     * Static accessor to Perf Stat Config Manager functionality.
     * This method implements the singleton pattern to ensure that only
     * one PerfStatConfigManager is running per server environment.
     *

     */
	public static PerfStatConfigManager getPerfStatConfigMgr()
	{
        if (mInstance == null) {

          synchronized(PerfStatConfigManager.class) {

            if (mInstance == null) {
              mInstance = new PerfStatConfigManager();
              return mInstance;
            }
          }
        }

        return mInstance;
    }

    /**
     * Static accessor to Perf Stat Config manager functionality.
     * This method implements the singleton pattern to ensure that only
     * one PerfStatConfigManager is running per server environment.
     *

     */
	public static PerfStatConfigManager getPerfStatConfigMgr(String serverLocation)
	{
        if (mInstance == null) {

          synchronized(PerfStatConfigManager.class) {

            if (mInstance == null) {
              mInstance = new PerfStatConfigManager(serverLocation);
              return mInstance;
            }
          }
        }

        return mInstance;
    }

    /**
     * Default constructor.  This will load all Perf Stat Config in the database into memory.
     *

     */
    private PerfStatConfigManager()
	{
	    mInstance = this;
	    refresh();
	}

    /**
     * Constructor that specifies the location of the EJB server.
     * This will load all Perf Stat Config in the database into memory.
     *

     */
    private PerfStatConfigManager(String serverLocation)
	{
	    mInstance = this;
	    this.serverLocation = serverLocation;
	    refresh();
	}

	/**
     * This method selects reference data from the database and loads it into a hashtable.
     *

     */
	public synchronized void refresh() {
        // Get a handle to the ServerDeploy EJB component to get the configuration information
        try {
          if (serverLocation == null) {
            System.out.println("ServerDeploy: No server location was specified for the EJB server");
            throw new AmsException("No EJB server location specified");
          }
          ServerDeploy serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(serverLocation,
                                                                                      "ServerDeploy");
          bConfigurationValid = true;

          QueryListView listView = (QueryListView) EJBObjectFactory.createClientEJB(serverLocation,
                                                                                    "QueryListView");
          String sql = "Select * from PERF_STAT_CONFIG order by client_bank";
          listView.setSQL(sql);

          int listViewRecordCount;

          try
           {
             listView.getRecords();
             listViewRecordCount = listView.getRecordCount();

           }
          catch(AmsException sqle)
           {
              System.out.println("Could not retrieve data from perf stat config table named perf_stat_config");
              listViewRecordCount = 0;
           }



  	      // Empty the collection
          // construct new object with local variable instead of instance varialbe to be thread-safe.
          Hashtable newHtOverallPerfStatConfig = new Hashtable();

          // Iterate through the result set
          String currentClientBank = null;
          String clientBank = null;
          String pageName = null;
          String active = null;

          Hashtable htCurrent = null;

          boolean bRecordFound = false;
          int recordCount = 0;
          while (recordCount < listViewRecordCount)
          {
            listView.scrollToRow(recordCount);
            // Set the indicator to say we found a record
            bRecordFound = true;

            // For a given client bank, create a hashtable of all
            // the entries.  This hashtable will be the entry in
            // the overall hashtable based on client bank.
            clientBank = listView.getRecordValue("client_bank");
            if (currentClientBank == null)
               currentClientBank = clientBank;

            if (htCurrent == null)
               htCurrent = new Hashtable();


            pageName = listView.getRecordValue("page_name");
            if(pageName==null)
            pageName="";
            active = listView.getRecordValue("active");
            if(active==null)
            {
             active="";
            }

            if ((currentClientBank.equals(clientBank))) {
               htCurrent.put(pageName, active);
            } else {
               newHtOverallPerfStatConfig.put(currentClientBank, htCurrent);
               currentClientBank = clientBank;
               if(currentClientBank==null)
                  currentClientBank="";
               htCurrent = new Hashtable();

               htCurrent.put(pageName, active);
            }
            recordCount++;
          }

          if (bRecordFound) {
             // Put last collection of reference data from query into overall hashtable
             newHtOverallPerfStatConfig.put(currentClientBank, htCurrent);
          }

          // Free the list view component
          listView.remove();
          htOverallPerfStatConfig = newHtOverallPerfStatConfig;

        } catch (Exception e) {
          bConfigurationValid = false;
          LOG.error("Exception occured in refresh()",e);
        }
	}


	
	/**
	 * @param pageName
	 * @param clientBank
	 * @param defaultValue
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public String getPerfStatConfig(String pageName, String clientBank, String defaultValue) 
	throws AmsException, RemoteException
	{
		Object tempObj = htOverallPerfStatConfig.get(clientBank);
		if (tempObj == null)
	           return defaultValue;
        Hashtable perfStatConfig = (Hashtable) tempObj;
        Object settingValue = perfStatConfig.get(pageName);
		if (settingValue == null || "".equals(settingValue)) {
			settingValue = defaultValue;
		}			
		return (String)settingValue;

	}

	/**
	 * @param clientBank
	 * @param pageName
	 * @param active
	 */
	public void updateCode(String clientBank, String pageName, String active)
     {
         Hashtable tableData = (Hashtable) htOverallPerfStatConfig.get(clientBank);
         tableData.put(pageName, active);
     }

    /**
     * @param clientBank
     * @param pageName
     */
    public void removeCode(String clientBank, String pageName)
     {
            Hashtable tableData = (Hashtable) htOverallPerfStatConfig.get(clientBank);
            tableData.remove(pageName);
     }

}
