/*
 * @(#)QueryListViewBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.List;

public class QueryListViewBean extends ListViewBean{
    // Supply the listview witht the SQL statement that will be used to
    // populate the list view
    public void setSQL(String sqlStatement) {
        this.SQLStatement = sqlStatement;
    }
   
    public void setSQL(String sqlStatement, Object... params ) {
    	this.SQLStatement = sqlStatement;
        this.sqlParams = params;
    }
    public void setSQL(String sqlStatement, List<Object> params) {
    	this.SQLStatement = sqlStatement;
        if(params != null)
    		this.sqlParams = params.toArray();
    }
}
    
  

