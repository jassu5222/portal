package com.amsinc.ecsg.frame;

/*
 * @(#)QueryListViewHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.rmi.*;
import javax.ejb.*;

public interface QueryListViewHome extends EJBHome
{
  public QueryListView create()
	throws CreateException, RemoteException, AmsException;
}
