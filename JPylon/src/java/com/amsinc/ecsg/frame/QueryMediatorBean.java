package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;

/**
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class QueryMediatorBean extends MediatorBean {

    // Business methods
    @Override
    public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
            throws RemoteException, AmsException {

        boolean bWrapArguments = true;
        String objectName = inputDoc.getAttribute("/Query/objectname");
        if (objectName == null) {
            throw new AmsException("Need to send the business object name parameter.");
        }

        String query = inputDoc.getAttribute("/Query/querylisttype");
        List<String> parmList = inputDoc.getAttributes("/Query/parameters");

        int numberOfParms = parmList.size();
        String parmValues[] = new String[numberOfParms];
        if (numberOfParms > 0) {
            if (query == null) {
                throw new AmsException("Need to send the list type parameter if query parameters are specified.");
            }

            int i = 0;
            for (String parmName : parmList) {
                if (parmName.length() == 0) {
                    parmValues[i] = parmName;
                } else {
                    if (parmName.charAt(0) == '/') {
                        parmValues[i] = inputDoc.getAttribute(parmName);
                    } else {
                        parmValues[i] = parmName;
                    }
                }
                i++;
            }

            // Determine if we should wrap substitution values with ' marks
            // Sometimes users wish to do this themselves in the query definition
            String wrapArgs = inputDoc.getAttribute("/Query/wrapargs");
            if (wrapArgs != null) {
                bWrapArguments = false;
            }
        }

        // Create a new instance of the object
        GenericList list
                = (GenericList) mediatorServices.createServerEJB("GenericList");

        // Run the query and put the result set into the return XML document
        if (query == null) {
            list.prepareList(objectName);
        } else if (numberOfParms == 0) {
            list.prepareList(objectName, query);
        } else {
            list.prepareList(objectName, query, parmValues, bWrapArguments);
        }

        list.getData();
        return list.getXmlResultSet();

    }
}
