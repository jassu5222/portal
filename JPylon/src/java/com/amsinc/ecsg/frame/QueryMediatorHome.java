package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface QueryMediatorHome extends javax.ejb.EJBHome
{
  public QueryMediator create()
        throws CreateException, RemoteException;
}