package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceAttribute extends Attribute  {
	protected String referenceTypeName;
	
	public ReferenceAttribute ()
	 {
     }

	public ReferenceAttribute (String name, String physicalName, String referenceType)
	 {
	    super(name, physicalName);
   	    this.setReferenceTypeName(referenceType);
     }

 	public ReferenceAttribute (String name, String physicalName, String referenceType, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
   	    this.setReferenceTypeName(referenceType);
	 }

	// Gets the reference Type name
	public String getReferenceTypeName(){
		return this.referenceTypeName;
	}
	// Sets the reference Type name
	public void setReferenceTypeName(String referenceType){
		this.referenceTypeName = referenceType;
	}
	// Validate the attribute value
	public void validate(String attributeValue) throws AmsException{
        if (!attributeValue.isEmpty())
        {
		   ReferenceDataManager refMgr = ReferenceDataManager.getRefDataMgr();
		   if(!refMgr.checkCode(this.getReferenceTypeName(), attributeValue)){
			  throw new InvalidAttributeValueException();
		   }
		}
	}
}
