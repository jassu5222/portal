/*
 * @(#)ReferenceDataManager
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

/**
 * The Reference Data Manager class handles static reference data of the code/description format for an application. It assumes that
 * this data is stored in a 'table of tables' with the following format:
 *
 * TABLE_TYPE VARCHAR(8) The type of reference data CODE VARCHAR(8) Code value DESCR VARCHAR(30) The description of the code
 *
 * For example, states may be represented as:\
 *
 * TABLE_TYPE CODE DESCR ---------- ---- ----- STATE VA Virginia STATE IL Illinois
 */
public class ReferenceDataManager {
	private static final Logger LOG = LoggerFactory.getLogger(ReferenceDataManager.class);

	/**
	 * The reference to the one singleton instance of this class.
	 */
	private static ReferenceDataManager mInstance = null;
	/**
	 * The server location of the EJB server
	 */
	private String serverLocation = null;
	/**
	 * The collection of reference data stored in memory for faster access.
	 */
	private Hashtable htOverallRefData;
	/**
	 * The collection of "additional value" data.
	 */
	private Hashtable htAdditionalValueOverallRefData;
	/**
	 * The name of the reference data table. This is configurable.
	 */
	private String tableName = "";
	/**
	 * Indicator to state whether the configuration information was read correctly
	 */
	private boolean bConfigurationValid;

	/**
	 * Default constructor. This will load all reference data in the database into memory.
	 *
	 */
	public ReferenceDataManager() {
		mInstance = this;
		refresh();
	}

	/**
	 * Constructor that specifies the location of the EJB server. This will load all reference data in the database into memory.
	 *
	 */
	public ReferenceDataManager(String serverLocation) {
		mInstance = this;
		this.serverLocation = serverLocation;
		refresh();
	}

	/**
	 * This method selects reference data from the database and loads it into a hashtable.
	 *
	 */
	public synchronized void refresh() {
		// Get a handle to the ServerDeploy EJB component to get the configuration information
		try {
			if (serverLocation == null) {
				LOG.error("ServerDeploy: No server location was specified for the EJB server");
				throw new AmsException("No EJB server location specified");
			}
			ServerDeploy serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(serverLocation, "ServerDeploy");
			this.setRefDataTableName(serverDeploy.getInitializationParameter("refDataTableName"));
			bConfigurationValid = true;

			QueryListView listView = (QueryListView) EJBObjectFactory.createClientEJB(serverLocation, "QueryListView");
			String sql = "Select * from ".concat(tableName).concat(" order by table_type,locale_name");
			listView.setSQL(sql);

			int listViewRecordCount;

			try {
				listView.getRecords();
				listViewRecordCount = listView.getRecordCount();

			} catch (AmsException sqle) {
				LOG.error("Could not retrieve data from reference data table named " + tableName, sqle);
				listViewRecordCount = 0;
			}

			// Empty the collection
			// W Zhu NCUH121502535 2/11/08 BEGIN
			// construct new object with local variable instead of instance varialbe to be thread-safe.
			Hashtable newHtOverallRefData = new Hashtable();
			Hashtable newHtAdditionalValueOverallRefData = new Hashtable();
			// W Zhu NCUH121502535 2/11/08 END

			// Iterate through the result set
			String currentTableType = null;
			String tableType = null;
			String code = null;
			String descr = null;
			String currentLocaleName = null;
			String localeName = null;
			String additionalValue = null;

			Hashtable htCurrent = null;
			Hashtable htAdditionalValueCurrent = null;

			boolean bRecordFound = false;
			int recordCount = 0;
			while (recordCount < listViewRecordCount) {
				listView.scrollToRow(recordCount);
				// Set the indicator to say we found a record
				bRecordFound = true;

				// For a given table type, create a hashtable of all
				// the entries. This hashtable will be the entry in
				// the overall hashtable based on tabletype.
				tableType = listView.getRecordValue("table_type");
				localeName = listView.getRecordValue("locale_name");
				if (localeName == null) {
					localeName = "";
				}
				if (currentTableType == null) {
					currentTableType = tableType;
				}
				if (currentLocaleName == null) {
					currentLocaleName = localeName;
				}

				if (htCurrent == null) {
					htCurrent = new Hashtable();
				}

				if (htAdditionalValueCurrent == null) {
					htAdditionalValueCurrent = new Hashtable();
				}

				code = listView.getRecordValue("code");
				// Krishna PPx-047 Begin
				if (code == null) {
					code = "";
				}
				// Krishna PPx-047 End
				descr = listView.getRecordValue("descr");
				// Krishna IR-GYUH060564111 12/01/2007 Begin.
				if (descr == null) {
					descr = "";
				}
				// Krishna IR-GYUH060564111 12/01/2007 End.
				additionalValue = listView.getRecordValue("addl_value");

				if (additionalValue == null) {
					additionalValue = "";
				}

				if ((currentTableType.equals(tableType)) && (currentLocaleName.equals(localeName))) {
					htCurrent.put(code, descr);
					htAdditionalValueCurrent.put(code, additionalValue);
				} else {
					newHtOverallRefData.put(currentTableType.concat(currentLocaleName), htCurrent);
					newHtAdditionalValueOverallRefData.put(currentTableType.concat(currentLocaleName), htAdditionalValueCurrent);
					currentTableType = tableType;
					currentLocaleName = localeName;
					if (currentTableType == null) {
						currentTableType = "";
					}

					htCurrent = new Hashtable();
					htAdditionalValueCurrent = new Hashtable();

					htCurrent.put(code, descr);
					htAdditionalValueCurrent.put(code, additionalValue);
				}
				recordCount++;
			}

			if (bRecordFound) {
				// Put last collection of reference data from query into overall hashtable
				newHtOverallRefData.put(currentTableType, htCurrent);
				newHtAdditionalValueOverallRefData.put(currentTableType, htAdditionalValueCurrent);
			}

			// Free the list view component
			listView.remove();
			// W Zhu NCUH121502535 2/11/08 BEGIN
			htOverallRefData = newHtOverallRefData;
			htAdditionalValueOverallRefData = newHtAdditionalValueOverallRefData;
			// W Zhu NCUH121502535 2/11/08 END

		} catch (Exception e) {
			bConfigurationValid = false;
			LOG.error("Exception occured in refresh()", e);
		}
	}

	/**
	 * This method gets a description for a given code and reference data table type.
	 *
	 */
	public String getDescr(String tableType, String code) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType);
		if (refData == null) {
			throw new AmsException("Invalid reference data table type: " + tableType);
		}
		String descr = (String) refData.get(code);
		if (descr == null) {
			throw new AmsException("Code " + code + " not found for table type " + tableType);
		}
		return descr;
	}

	/**
	 * This method gets a description for a given code, reference data table type and locale name.
	 *

	 */
	public String getDescr(String tableType, String code, String localeName) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType.concat(localeName));
		if (refData == null) {
			refData = (Hashtable) htOverallRefData.get(tableType);
			if (refData == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
		}
		String descr = (String) refData.get(code);
		// Krishna IR-GYUH060564111 12/01/2007 Begin.
		// When the entry for current locale name is not found fetch the default locale data.
		if (descr == null) {
			refData = (Hashtable) htOverallRefData.get(tableType);
			if (refData == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
			descr = (String) refData.get(code);

		}
		// Krishna IR-GYUH060564111 12/01/2007 End
		return descr;
	}

	/**
	 * This method gets the "additional value" for a given code and table type
	 *
	 */
	public String getAdditionalValue(String tableType, String code) throws AmsException {
		Hashtable additionalValues = (Hashtable) htAdditionalValueOverallRefData.get(tableType);
		if (additionalValues == null) {
			throw new AmsException("Invalid reference data table type: " + tableType);
		}
		String additionalValue = (String) additionalValues.get(code);
		if (additionalValue == null) {
			throw new AmsException("Code " + code + " not found for table type " + tableType);
		}
		return additionalValue;
	}

	/**
	 * This method gets the "additional value" for a given code, reference data table type and locale name.
	 *
	 */
	public String getAdditionalValue(String tableType, String code, String localeName) throws AmsException {
		Hashtable additionalValues = (Hashtable) htAdditionalValueOverallRefData.get(tableType.concat(localeName));
		if (additionalValues == null) {
			additionalValues = (Hashtable) htAdditionalValueOverallRefData.get(tableType);
			if (additionalValues == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
		}
		String additionalValue = (String) additionalValues.get(code);
		if (additionalValue == null) {
			throw new AmsException("Code " + code + " not found for table type " + tableType);
		}
		return additionalValue;
	}

	/**
	 * This method checks if a code is valid for a given reference data table type.
	 *

	 */
	public boolean checkCode(String tableType, String code) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType);
		if (refData == null) {
			throw new AmsException("Invalid reference data table type: " + tableType);
		}
		String descr = (String) refData.get(code);
		if (descr == null) {
			return false;
		}
		return true;
	}

	/**
	 * This method checks if a code is valid for a given reference data table type and locale name.
	 *

	 */
	public boolean checkCode(String tableType, String code, String localeName) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType.concat(localeName));
		if (refData == null) {
			refData = (Hashtable) htOverallRefData.get(tableType);
			if (refData == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
		}
		String descr = (String) refData.get(code);
		if (descr == null) {
			return false;
		}
		return true;
	}

	/**
	 * This method returns the list of descriptions for a given reference data table type.
	 *

	 */
	public Enumeration getAllDescr(String tableType) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType);
		if (refData == null) {
			throw new AmsException("Invalid reference data table type: " + tableType);
		}
		return (refData.elements());
	}

	/**
	 * This method returns the list of descriptions for a given reference data table type and locale name.
	 *
	 */
	public Enumeration getAllDescr(String tableType, String localeName) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType.concat(localeName));
		if (refData == null) {
			refData = (Hashtable) htOverallRefData.get(tableType);
			if (refData == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
		}
		return (refData.elements());
	}

	/**
	 * This method returns the a hashtable of all the codes and descriptions for a given reference data table type.
	 *
	 */
	public Hashtable getAllCodeAndDescr(String tableType) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType);
		if (refData == null) {
			throw new AmsException("Invalid reference data table type: " + tableType);
		}
		return (refData);
	}

	/**
	 * This method returns the a hashtable of all the codes and descriptions for a given reference data table type and locale name.
	 *
	 */
	public Hashtable getAllCodeAndDescr(String tableType, String localeName) throws AmsException {
		Hashtable refData = (Hashtable) htOverallRefData.get(tableType.concat(localeName));

		// IR - PHUK020871475 - BEGIN.
		if (refData != null) {
			Hashtable defaultRefData = (Hashtable) htOverallRefData.get(tableType);
			if (defaultRefData != null && (refData.size() < defaultRefData.size())) {
				String name = null;
				Enumeration keys;
				keys = defaultRefData.keys();

				int i = 0;
				while (keys.hasMoreElements()) {
					name = (String) keys.nextElement();
					if (!refData.containsKey(name)) {
						refData.put(name, defaultRefData.get(name));
					}
					i++;
				}
			}
		} // IR - PHUK020871475 - END.
		else {
			refData = (Hashtable) htOverallRefData.get(tableType);
			if (refData == null) {
				throw new AmsException("Invalid reference data table type: " + tableType);
			}
		}
		return (refData);
	}

	/**
	 * Static accessor to reference data manager functionality. This method implements the singleton pattern to ensure that only one
	 * ReferenceDataManager is running per server environment.
	 *
	 */
	public static ReferenceDataManager getRefDataMgr() {
		if (mInstance == null) {

			synchronized (ReferenceDataManager.class) {

				if (mInstance == null) {
					mInstance = new ReferenceDataManager();
					return mInstance;
				}
			}
		}

		return mInstance;
	}

	/**
	 * Static accessor to reference data manager functionality. This method implements the singleton pattern to ensure that only one
	 * ReferenceDataManager is running per server environment.
	 *
	 */
	public static ReferenceDataManager getRefDataMgr(String serverLocation) {
		if (mInstance == null) {

			synchronized (ReferenceDataManager.class) {

				if (mInstance == null) {
					mInstance = new ReferenceDataManager(serverLocation);
					return mInstance;
				}
			}
		}

		return mInstance;
	}

	/**
	 * This method sets the name of the reference data table.
	 *
	 * @param String
	 *            the name of the reference data table
	 * @return void
	 */
	public void setRefDataTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Removes data for a table type and code combination from ReferenceDataManager's hashtables. Used to update refdata without
	 * bringing down the server.
	 *
	 * Data for all locales is removed.
	 *
	 * @param tableType
	 *            - the table type of the code being removed
	 * @param code
	 *            - the code identifying the code being removed
	 *
	 */
	public void removeCode(String tableType, String code) {
		Hashtable supportedLocales = ResourceManager.getSupportedLocales();

		Enumeration locales = supportedLocales.keys();

		// Loop through the locales, removing data for each one
		while (locales.hasMoreElements()) {
			String localeName = (String) locales.nextElement();

			Hashtable tableData = (Hashtable) htOverallRefData.get(tableType + localeName);
			tableData.remove(code);

			Hashtable addlValueTableData = (Hashtable) htAdditionalValueOverallRefData.get(tableType + localeName);
			addlValueTableData.remove(code);
		}
	}

	/**
	 * Updates the description and additional value data for a table type, code, and description combination. Used to update refdata
	 * without bringing down the server.
	 *
	 * @param tableType
	 *            - table type of the data being updated
	 * @param code
	 *            - code identifying the data to be updated
	 * @param descr
	 *            - new description for the code
	 * @param addlValue
	 *            - new additional value for the code
	 * @param localeName
	 *            - locale identifying the data to be updated.
	 */
	public void updateCode(String tableType, String code, String descr, String addlValue, String localeName) {
		Hashtable tableData = (Hashtable) htOverallRefData.get(tableType + localeName);
		tableData.put(code, descr);

		Hashtable addlValueTableData = (Hashtable) htAdditionalValueOverallRefData.get(tableType + localeName);
		addlValueTableData.put(code, addlValue);
	}

}
