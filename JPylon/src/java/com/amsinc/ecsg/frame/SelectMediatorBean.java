package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SelectMediatorBean extends MediatorBean {
  
  // Business methods
  @Override
  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
     throws RemoteException, AmsException {
    
    // Declare business object variables
    BusinessObject busObj = null;

      // Get input arguments
    String objectName = inputDoc.getAttribute("/Select/beanName");
    String oidAttributeName = inputDoc.getAttribute("/Select/oidFieldName");
    String notFoundError = inputDoc.getAttribute("/Select/notFoundError");
    List<String> componentList = inputDoc.getAttributes("/Select/componentList");
    
    if (objectName == null)
       throw new AmsException("Need to send the objectName parameter.");
       
    if (oidAttributeName == null)
       throw new AmsException("Need to send the oidAttributeName parameter.");
       
    if (notFoundError == null)
       notFoundError = "INF01";
       
    StringBuilder oidAttributeXmlPath = new StringBuilder();
    oidAttributeXmlPath.append('/');
    oidAttributeXmlPath.append(objectName);
    oidAttributeXmlPath.append('/');
    oidAttributeXmlPath.append(oidAttributeName);
    
    long lOid = inputDoc.getAttributeLong(oidAttributeXmlPath.toString());

    try
    {
      // Create a new instance of the object
      busObj = (BusinessObject)mediatorServices.createServerEJB (objectName);
      busObj.getData(lOid);

        ComponentList component;
        for (String name : componentList) {
        try {
          String type = busObj.getComponentType(name);
          if (type.equals(AmsConstants.BUSINESS_OBJECT)) {
             busObj.getComponentHandle(name);
          } else if (type.equals(AmsConstants.LISTING_OBJECT)) {
             component = (ComponentList) busObj.getComponentHandle(name);
             // Instantiate each object in the list
             for(int index=0; index < component.getObjectCount(); index++) {      
                 component.scrollToObjectByIndex(index);
                 component.getComponentObject(
                        Long.parseLong(component.getListAttribute(component.getIDAttributeName())));
             }
          }
        } catch (ComponentNoRelationshipException ex) {
           // do nothing, this means the data was not found
        } catch (AmsException e) {
           mediatorServices.debug (e.toString ());
        }
      }
            
    } catch (InvalidObjectIdentifierException ex) {
        
      // If the object does not exist, issue an error
      mediatorServices.getErrorManager().issueError (
         getClass ().getName (), notFoundError, ex.getObjectId());
        
    } finally {
        
      // Populate output document
       outputDoc = busObj.populateXmlDoc(outputDoc);

      // Get error information from business object and put into
      // document to be returned to client
      mediatorServices.addErrorInfo (busObj.getIssuedErrors ());
    
    }
    return outputDoc;
  }  
}
