/*
 * @(#)ServerDeployBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.ejb.*;
import javax.naming.*;
import java.rmi.*;
import java.util.*;
import java.io.FileNotFoundException;
import com.amsinc.ecsg.util.*;
import java.lang.reflect.*;

/**
 * ServerDeploy is used as an initialization or '.ini' file.
 * Parameters that are needed to deploy the framework are put into the deployment descriptor
 * of this EJB component, and the corresponding framework components use this bean to
 * lookup those intialization parameters.  
 */
public class ServerDeployBean implements SessionBean {
  
  // EJB Contect variable
  protected SessionContext mContext;
  
  // Class variables
  transient Properties props;
  
  // Required callbacks by EJB
  public void ejbActivate() {};
  public void ejbPassivate() {};
  public void ejbRemove() {};
  public void setSessionContext(SessionContext context) {
    mContext = context;
  }
  public void ejbCreate() {
  }

  /**
   * This method retrieves the value of a specified initilization parameter.  The
   * parameter name should be the exact same name as that specified in the deployment
   * descriptor.  If the parameter was not specified in the deployment descriptor,
   * a null value is returned.
   *
   * @param   String The name of the initialization parameter

   * @return  String
   */
  public String getInitializationParameter(String parmName) throws RemoteException {
    String parmValue = null;
    try {
        InitialContext ctx = new InitialContext();
        parmValue = (String) ctx.lookup("java:/comp/env/".concat(parmName));
    } catch (Exception e) {
        e.printStackTrace();
    }
    return parmValue;
  }
  
  /**
   * This method forces  the reference data manager to reload
   * its data (which is cached) from the database.  This method is invoked by the
   * RefDataRefresh utility.
   *

   * @return  void
   */
  public void reloadReferenceData() throws RemoteException {
    ReferenceDataManager.getRefDataMgr().refresh();
  }

  /**
   * This method initializes the reference data component.
   * It should be invoked before any access to either component is done.
   * The BeanManager component in the web server infrastructure invokes this method
   * when it is initialized.  If the web server framework is not used in conjunction
   * with the framework business objects, this method should be called in an
   * initialization program run when the application server is started.
   *

   * @return  void
   */
   public void init(String serverLocation) throws RemoteException {
      PropertyResourceBundle myProps = (PropertyResourceBundle)PropertyResourceBundle.getBundle("jPylon");

    ReferenceDataManager.getRefDataMgr(serverLocation);
  }

  /**
   * Several classes cache data and provide static methods to update that data.
   * This method provides a way for the static update method to be called in 
   * the server JVM, thus allowing the cached data to be updated.
   * A fully qualified class name is passed in, and a static method named update() is
   * called on it.   
   *
   * @param updateableClass - fully qualified name of a class with a static update() method
   * @return Object - the object returned by the update method (usually a string with error information)
   */
  public Object updateCache(String updateableClass)
            throws RemoteException
   {   
     try
      {
        // Get a handle to the class
        Class theClass = Class.forName(updateableClass);

        // Get a handle to the update method (no parameters)
        Method theMethod = theClass.getMethod("update", new Class[] {});

        // Invoke the method, returning the result
        // First parameter (new Object()) is ignored since the method is static
        return theMethod.invoke(new Object(), new Object[] {});
       }
      catch(ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ex)
       {
         ex.printStackTrace();
       }

      return null;
   }

  /**
   * Returns the GMT date and time on the server on which this EJB is deployed
   */ 
  public Date getServerDate() throws RemoteException, AmsException
   {
      return this.getServerDate(true);
   }

  /**
   * Returns the GMT date and time on the server on which this EJB is deployed
   *
   * @param override - if true, the GMTUtility will look in a file to override the date if it is set up that way
   */
  public Date getServerDate(boolean override) throws RemoteException, AmsException
   {
     return GMTUtility.getGMTDateTime(true, override);
   }
  
}







