/*
 * @(#)ServerDeployHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

public interface ServerDeployHome extends javax.ejb.EJBHome
{
  public ServerDeploy create()
        throws CreateException, RemoteException;
}