/*
 * @(#)TableListViewHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;

public interface TableListViewHome extends EJBHome
{
  public TableListView create()
    throws RemoteException, CreateException, AmsException;
}
