package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class UniqueConstraintViolatedException extends DataObjectException{
    public UniqueConstraintViolatedException(){
        super("Unique Constraint Violated in DataObject Processing");
        this.vendorErrorCode = 1;
    }
}