package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UpdateMediatorHome extends javax.ejb.EJBHome
{
  public UpdateMediator create()
        throws CreateException, RemoteException;
}