package com.amsinc.ecsg.frame;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.amsinc.ecsg.util.*;

/**
 *  The class facilitate the reading of XML config files.   The first time
 *  a file is requested, it is read from the servlet context.  This allows 
 *  the XML to be located within the structure of a Web Application Resource 
 *  (WAR) file.  After the first request, the XML config files are read from 
 *  a cache stored within this class.
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
//cquinton 5/19/2011 refactor reading of the file to XmlConfigFile subclass
public class XmlConfigFileCache extends XmlConfigFile {
    // The stored XML config files
    private static HashMap configFiles = new HashMap(50);

    /**
     * This method sets the servlet context of the cache.   The servlet
     * context is used to look up the configuration files.   This must 
     * be set prior to making any other method calls.
     *
     * @param ServletContext theContext - the servlet context to read from
     */
    public static void init(ServletContext theContext) {
        XmlConfigFile.init(theContext);               
    }

    /**
     * Obtains data from a configuration file.   The file may have been cached
     * already.   If it is not already cached, the file is read from the servlet
     * context.  
     *
     * The init() method must have already been called prior to calling this method.
     *
     *
     * @param path String - the path to the file
     * @return DocumentHandler - the XML data requested
     */
    public static DocumentHandler readConfigFile( String path ) {
        // Attempt to read the data from the cache
        DocumentHandler xml = (DocumentHandler) configFiles.get(path);

        if(xml != null) {
            // Data is in the cache.  Use it.
            return xml;
        }
        else
        {
            // Data is not in the cache.  
            //cquinton 5/19/2011 - refactor reading of the config file
            // to XmlConfigFile subclass
            xml = XmlConfigFile.readConfigFile( path );
            // Store it in the cache for later use
            configFiles.put(path, xml);

            return xml;
        }
    }

}