/*
 * @(#)ListBox
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.html;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

import java.util.*;
import javax.servlet.*;
import javax.crypto.SecretKey;

public class ListBox
{
   /**
    * Creates a drop down list box populated with the specified data from the
    * reference data table.
    *
    * @param tableType The table type to get reference data for.
    * @param listBoxParmName The name to assign to the HTML list box widget.
    * @return HTML statement to create the list widget.
    */
   public static String createFromRefData (String tableType,
                                           String listBoxParmName)
          throws ServletException
   {
      return ListBox.createFromRefData (tableType, listBoxParmName, null, null, false);
   }
   /**
    * Creates a drop down list box populated with the specified data from the
    * reference data table.
    *
    * @param tableType The table type to get reference data for.
    * @param listBoxParmName The name to assign to the HTML list box widget.
    * @param encryptValue A boolean that specifies whether or not to encrypt the dropdown
    *                     value.
    * @return HTML statement to create the list widget.
    */
   public static String createFromRefData (String tableType,
                                           String listBoxParmName,
                                           boolean encryptValue)
          throws ServletException
   {
      return ListBox.createFromRefData (tableType, listBoxParmName, null, null, encryptValue);
   }
   /**
    * Creates a drop down list box populated with the specified data from the
    * reference data table.
    *
    * @param tableType The table type to get reference data for.
    * @param listBoxParmName The name to assign to the HTML list box widget.
    * @param selectedOption Value to make the seleted item in the list box.
    * @param localeName
    * @param encryptValue A boolean that specifies whether or not to encrypt the dropdown
    *                     value.
    * @return HTML statement to create the list box widget.
    */
   public static String createFromRefData (String tableType,
                                           String listBoxParmName,
                                           String selectedOption,
                                           String localeName,
                                           boolean encryptValue)
          throws ServletException
   {
      StringBuilder out = new StringBuilder();

      try
      {
         // Determine what list box to create
         if (tableType == null) 
            throw new AmsException ("Need to send the tableType parameter");

         // Determine what parameter name to use for this list box from
         if (listBoxParmName == null)
            throw new AmsException (
               "Need to send the listBoxParmName parameter");

         // Get the reference data and iterate through it to create the list
         // box html
         Hashtable refData;
         if (localeName == null)
         {
            refData = ReferenceDataManager.getRefDataMgr ().getAllCodeAndDescr (
               tableType);
         }
         else
         {
            refData =
               ReferenceDataManager.getRefDataMgr ().getAllCodeAndDescr (
                  tableType.concat (localeName));
         }
         out.append("<select name=\"").append(listBoxParmName).append ("\" size=\"1\">\n");

         Enumeration enumer = refData.keys ();
         while (enumer.hasMoreElements ())
         {
            String code = (String)enumer.nextElement ();
            String descr = (String)refData.get (code);
            if (selectedOption == null)
            {
               out.append("<option value=\"").append(code).append("\">").append(descr).append ("</option>\n");
            }
            else
            { 
               if (code.equals (selectedOption))
               {
                  out.append("<option selected value=\"").append(code).append("\">").append(descr).append ("</option>\n");
               }
               else
               {
                  out.append("<option value=\"").append(code).append("\">").append(descr).append ("</option>\n");
               }
            }
         }
         out.append ("</select>\n");
      }
      catch (AmsException e)
      {
         throw new ServletException (e.getMessage ());
      }
      return out.toString ();
   }

   /**
    * Creates a drop down list box populated with the specified data from the
    * reference data table.
    *
    * @param tableType The table type to get reference data for.
    * @param listBoxParmName The name to assign to the HTML list box widget.
    * @param localeName
    * @return HTML statement to create the list box widget.
    */
   public static String createFromRefData (String tableType,
                                           String listBoxParmName,
                                           String localeName)
          throws ServletException
   {
        return ListBox.createFromRefData (tableType, listBoxParmName, localeName, false) ;
   }

   /**
    * Creates a drop down list box populated with the specified data from the
    * reference data table.
    *
    * @param tableType The table type to get reference data for.
    * @param listBoxParmName The name to assign to the HTML list box widget.
    * @param localeName
    * @param encryptValue A boolean that specifies whether or not to encrypt the dropdown
    *                     value.
    * @return HTML statement to create the list box widget.
    */
   public static String createFromRefData (String tableType,
                                           String listBoxParmName,
                                           String localeName, 
                                           boolean encryptValue)
          throws ServletException
   {
      StringBuilder out = new StringBuilder();

      try
      {
         // Determine what list box to create
         if (tableType == null) 
            throw new AmsException ("Need to send the tableType parameter");

         // Determine what parameter name to use for this list box from
         if (listBoxParmName == null)
            throw new AmsException (
               "Need to send the listBoxParmName parameter");

         if (localeName == null)
            throw new AmsException ("Need to send the localeName parameter");

         // Get the reference data and iterate through it to create the list
         // box html
         Hashtable refData;
         refData = ReferenceDataManager.getRefDataMgr ().getAllCodeAndDescr (
            tableType.concat (localeName));

         out.append("<select name=\"").append(listBoxParmName).append ("\" size=\"1\">\n");

         Enumeration enumer = refData.keys ();
         while (enumer.hasMoreElements ())
         {
            String code = (String)enumer.nextElement ();
            String descr = (String)refData.get (code);
            out.append("<option value=\"").append(code).append("\">").append(descr).append ("</option>\n");
         }
    
         out.append ("</select>\n");
      }
      catch (AmsException e)
      {
          throw new ServletException (e.getMessage ());
      }
      return out.toString ();
   }
   /**
    * This method returns an HTML SELECT statement based on the inputDoc and
    * the various input parameters.
    *
    * See createFromDoc(DocumentHandler, String, String, String) for a more
    * detailed explanation.
    *
    * This version of the method automatically selects the first option
    *

    * @date 06/01/2000
    * 
    * @param doc The document to read values from 
    * @param listBoxParmName The name of the select tag (NAME=)
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option
    * @return The HTML Select statement
    */
   public static String createFromDoc (DocumentHandler doc,
                                       String listBoxParmName,
                                       String valueAttribute,
                                       String textAttribute)
          throws ServletException
   {
      return createFromDoc (
         doc, listBoxParmName, valueAttribute, textAttribute, null, null);
   }          
   /**
    * This method returns an HTML SELECT statement based on the inputDoc and
    * the various input parameters.
    *
    * See createFromDoc(DocumentHandler, String, String, String) for a more
    * detailed explanation.
    *
    * This version of the method automatically selects the first option
    *

    * @date 06/01/2000
    * 
    * @param doc The document to read values from 
    * @param listBoxParmName The name of the select tag (NAME=)
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option
    * @return The HTML Select statement
    */
   public static String createFromDoc (DocumentHandler doc,
                                       String listBoxParmName,
                                       String valueAttribute,
                                       String textAttribute,
                                       SecretKey secretKey)
          throws ServletException
   {
      return createFromDoc (
         doc, listBoxParmName, valueAttribute, textAttribute, null, secretKey);
   }          
   /**
    * This method returns an HTML SELECT statement based on the inputDoc and
    * the various input parameters.<p>
    * <p>
    * The statement is of the form &lt;SELECT NAME=listBoxParmName SIZE=1&gt;.
    * Each option is of the form &lt;OPTION VALUE=value1&gt;text<p>
    * <p>
    * The values used for the options are taken from the input doc.  This
    * XML document is assumed to be a set of /ResultSetRecords.  The
    * valueAttribute and textAttribute are the names of the attributes to
    * retrieve from each result set record for the OPTION's value and text.<p>
    * <p>
    * If a selectedOption is provided (i.e., not null), then as the OPTION
    * tags are created, if a match of the value is made with the selectedOption,
    * the SELECTED tag is added as in &lt;OPTION SELECTED VALUE=value&gt;text
    *
    * @param doc The document to read values from 
    * @param listBoxParmName The name of the select tag &lt;NAME=&gt;
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option
    * @param selectedOption The value of the option to default as selected.
    * If null, the first value defaults to selected
    * @return The HTML Select statement
    */
   public static String createFromDoc (DocumentHandler inputDoc, 
                                       String listBoxParmName, 
                                       String valueAttribute, 
                                       String textAttribute,
                                       String selectedOption,
                                       SecretKey secretKey)
          throws ServletException
   {
      StringBuilder out = new StringBuilder();
      String    value;
      String    text;

      try
      {
         // Determine what parameter name to use for this list box from
         if (listBoxParmName == null)
            throw new AmsException (
               "Need to send the listBoxParmName parameter");

         // First build the <select name="x" size="1"> tag.
         out.append("<select name=\"").append(listBoxParmName).append ("\" size=\"1\">\n");

         Vector v = inputDoc.getFragments ("/ResultSetRecord");
         int numItems = v.size ();

         for (int i = 0;i < numItems;i++)
         {
            DocumentHandler doc = (DocumentHandler)v.elementAt (i);
            // Extract the value and text attributes from the document.
            try
            {
               value = doc.getAttribute ("/" + valueAttribute);
            }
            catch (Exception e)
            {
               value = "0";
            }
            try
            {
               text = doc.getAttribute ("/" + textAttribute);
            }
            catch (Exception e)
            {
               text = "";
            }
      	   // W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.            
            if (selectedOption == null)
            {
               if (secretKey != null)
                 value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
               out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
            }
            else
            {
               if (value.equals (selectedOption))
               {
                  if (secretKey != null)
                    value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
                  out.append("<option selected value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
               else
               {
                 if (secretKey != null)
                   value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
                 out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
            }
         }
         out.append ("</select>\n");
      }
      catch (AmsException e)
      {
         throw new ServletException (e.getMessage ());
      }
      return out.toString ();
   }                        
   /**
    * This method returns an HTML SELECT statement based on the inputDoc and
    * the various input parameters.<p>
    * <p>
    * The statement is of the form &lt;SELECT NAME=listBoxParmName SIZE=1&gt;.
    * Each option is of the form &lt;OPTION VALUE=value&gt;text<p>
    * <p>
    * The values used for the options are taken from the input doc. This
    * XML document is assumed to be a set of /ResultSetRecords. The
    * valueAttribute and textAttribute are the names of the attributes to
    * retrieve from each result set record for the OPTION's value and text.<p>
    * <p>
    * If a selectedOption is provided (i.e., not null), then as the OPTION
    * tags are created, if a match of the value is made with the selectedOption,
    * the SELECTED tag is added as in &lt;OPTION SELECTED VALUE=value&gt;text<p>
    *< p>
    * This version also adds a OPTION as the first option.  The value and text 
    * are taken from the passed in defaultValue and defaultText.
    *
    * @param doc The document to read values from 
    * @param listBoxParmName The name of the select tag &lt;NAME=&gt;
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option
    * @param selectedOption The value of the option to default as selected.
    * If null, the first value defaults to selected
    * @param defaultValue The value of the first option. (Used to provide
    * a blank or null value in the list.)
    * @param defaultText The text for the first option. (Used to provide
    * a blank or null value in the list.)
    * @return The HTML Select statement
    */
   public static String createFromDocWithBlank (
          DocumentHandler inputDoc,
          String listBoxParmName,
          String valueAttribute, 
          String textAttribute,
          String selectedOption,
          String defaultValue,
          String defaultText)
          throws ServletException
   {
      StringBuilder out = new StringBuilder();
      String  value;
      String  text;

      try
      {
         // Determine what parameter name to use for this list box from
         if (listBoxParmName == null)
             throw new AmsException (
                "Need to send the listBoxParmName parameter");
         // First build the <select name="x" size="1"> tag.
         out.append("<select name=\"").append(listBoxParmName).append ("\" size=\"1\">\n");

         // We need to add a default as the first option.  Use the values
         // that were passed in.
         out.append("<option value=\"").append(defaultValue).append("\">").append(defaultText).append ("</option>\n");

         Vector v = inputDoc.getFragments ("/ResultSetRecord");
         int numItems = v.size ();

         for (int i = 0; i < numItems; i++)
         {
            DocumentHandler doc = (DocumentHandler)v.elementAt (i);
            // Extract the value and text attributes from the document.
            try
            {
               value = doc.getAttribute ("/" + valueAttribute);
            }
            catch (Exception e)
            {
               value = "0";
            }
            try
            {
               text = doc.getAttribute ("/" + textAttribute);
            }
            catch (Exception e)
            {
               text = "";
            }
            if (selectedOption == null)
            {
               out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
            }
            else
            {
               if (value.equals (selectedOption))
               {
                  out.append("<option selected value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
               else
               {
                  out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
            }

         }
         out.append ("</select>\n");
      }
      catch (AmsException e)
      {
         throw new ServletException (e.getMessage ());
      }
      return out.toString ();
   }
   /**
    * This method returns the OPTION tags that can be incorporated into an
    * an HTML SELECT statement. The OPTION values are based on the inputDoc
    * and the other input parameters.<p>
    * <p>
    * Each option is of the form &lt;OPTION VALUE=value&gt;text<p>
    * <p>
    * The values used for the options are taken from the input doc.  This
    * XML document is assumed to be a set of /ResultSetRecords.  The
    * valueAttribute and textAttribute are the names of the attributes to
    * retrieve from each result set record for the OPTION's value and text.<p>
    * <p>
    * If a selectedOption is provided (i.e., not null), then as the OPTION
    * tags are created, if a match of the value is made with the selectedOption,
    * the SELECTED tag is added as in &lt;OPTION SELECTED VALUE=value&gt;text<p>
    * <p>
    * This variation does not create the SELECT tag to allow the caller
    * to define the SELECT himself (e.g., to use style sheet, set width, etc.)
    * 
    * @return The HTML Select statement
    * @param The document to read values from 
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option.
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option.
    * @param selectedOption The value of the option to default as selected.
    * If null, the first value defaults to selected.
    */
   public static String createOptionList (DocumentHandler inputDoc,
                                          String valueAttribute, 
                                          String textAttribute,
                                          String selectedOption)
          throws ServletException
   {
        return ListBox.createOptionList (inputDoc,valueAttribute,
                                         textAttribute,selectedOption,
                                         null);
   }
   /**
    * This method returns the OPTION tags that can be incorporated into an
    * an HTML SELECT statement. The OPTION values are based on the inputDoc
    * and the other input parameters.<p>
    * <p>
    * Each option is of the form &lt;OPTION VALUE=value&gt;text<p>
    * <p>
    * The values used for the options are taken from the input doc.  This
    * XML document is assumed to be a set of /ResultSetRecords.  The
    * valueAttribute and textAttribute are the names of the attributes to
    * retrieve from each result set record for the OPTION's value and text.<p>
    * <p>
    * If a selectedOption is provided (i.e., not null), then as the OPTION
    * tags are created, if a match of the value is made with the selectedOption,
    * the SELECTED tag is added as in &lt;OPTION SELECTED VALUE=value&gt;text<p>
    * <p>
    * This variation does not create the SELECT tag to allow the caller
    * to define the SELECT himself (e.g., to use style sheet, set width, etc.)
    * 
    * @return The HTML Select statement
    * @param The document to read values from 
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option.
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option.
    * @param selectedOption The value of the option to default as selected.
    * If null, the first value defaults to selected.
    */
   public static String createOptionList (DocumentHandler inputDoc,
                                          String valueAttribute, 
                                          String textAttribute,
                                          String selectedOption,
                                          SecretKey secretKey)
          throws ServletException
   {
      StringBuilder out = new StringBuilder();
      String  value;
      String  text;

      // WUUK082559086 Check null inputDoc to avoid NullPointerException
      if (inputDoc == null) {
          return "";
      }
      
      try
      {
         Vector v = inputDoc.getFragments ("/ResultSetRecord");
         int numItems = v.size ();

         for (int i = 0; i < numItems; i++)
         {
            DocumentHandler doc = (DocumentHandler)v.elementAt (i);
            // Extract the value and text attributes from the document.
            try
            {
               value = doc.getAttribute ("/" + valueAttribute);
            }
            catch (Exception e)
            {
               value = "0";
            }
            try
            {
               text = doc.getAttribute ("/" + textAttribute);
            // NShrestha - 07/09/2009 - IR#RRUJ061739225 - Begin --
               text = StringFunction.xssCharsToHtml(text); 
            // NShrestha - 07/09/2009 - IR#RRUJ061739225 - End --
            }
            catch (Exception e)
            {
               text = "";
            }
      	   // W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.            
            if (selectedOption == null)
            {
               if (secretKey != null)
                  value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
               out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
            }
            else
            {
               if (value.equals (selectedOption))
               {
                  if (secretKey != null)
                     value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
                  out.append("<option selected value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
               else
               {
                  if (secretKey != null)
                     value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
                  out.append("<option value=\"").append(value).append("\">").append(text).append ("</option>\n");
               }
            }
         }
      }
      catch (Exception e)
      {
         System.out.println ("Error in ListBox.createOptionList " +
                             e.toString ());
      }
      return out.toString ();
   }
   /** Added for CR 821 - Rel 8.3
    * This method is used to return Panel Groups based on Instrument Type.
    * This method returns the OPTION tags that can be incorporated into an
    * an HTML SELECT statement. The OPTION values are based on the inputDoc
    * and the other input parameters.<p>
    * <p>
    * Each option is of the form &lt;OPTION VALUE=value&gt;text<p>
    * <p>
    * The values used for the options are taken from the input doc.  This
    * XML document is assumed to be a set of /ResultSetRecords.  The
    * valueAttribute and textAttribute are the names of the attributes to
    * retrieve from each result set record for the OPTION's value and text.<p>
    * <p>
    * If a selectedOption is provided (i.e., not null), then as the OPTION
    * tags are created, if a match of the value is made with the selectedOption,
    * the SELECTED tag is added as in &lt;OPTION SELECTED VALUE=value&gt;text<p>
    * <p>
    * This variation does not create the SELECT tag to allow the caller
    * to define the SELECT himself (e.g., to use style sheet, set width, etc.)
    * 
    * @return The HTML Select statement
    * @param The document to read values from 
    * @param valueAttribute The name of the attribute in the input document
    * that represents the "value" for each option.
    * @param textAttribute The name of the attribute in the input document
    * that represents the "text" for each option.
    * @param instrumentInd Indicates the instrumentType for which Panel Groups are to be shown.
    * @param selectedOption The value of the option to default as selected.
    * If null, the first value defaults to selected.
    */
	public static String createPanelGroupOptionList(DocumentHandler inputDoc,
			String valueAttribute, String textAttribute, String instrumentInd, String selectedOption) throws ServletException {
		StringBuilder out = new StringBuilder();
		String value = "0";
		String text = "";

		// WUUK082559086 Check null inputDoc to avoid NullPointerException
		if (inputDoc == null) {
			return "";
		}

		try {
			Vector v = inputDoc.getFragments("/ResultSetRecord");
			int numItems = v.size();

			for (int i = 0; i < numItems; i++) {
				DocumentHandler doc = (DocumentHandler) v.elementAt(i);
				// Extract the value and text attributes from the document.
				try {
					if(doc.getAttribute("/" + instrumentInd).equals("Y"))
						value = doc.getAttribute("/" + valueAttribute);
				} catch (Exception e) {
					value = "0";
				}
				try {
					if(doc.getAttribute("/" + instrumentInd).equals("Y"))
					{
						text = doc.getAttribute("/" + textAttribute);
						// NShrestha - 07/09/2009 - IR#RRUJ061739225 - Begin --
						text = StringFunction.xssCharsToHtml(text);
						// NShrestha - 07/09/2009 - IR#RRUJ061739225 - End --
					}else{
						text="";
					}
					
				} catch (Exception e) {
					text = "";
				}
				if (StringFunction.isBlank(selectedOption)) {
					if(StringFunction.isNotBlank(text))
					out.append("<option value=\"").append(value).append("\">").append(text).append("</option>\n");
				} else {
					if (value.equals(selectedOption)) {
						if(StringFunction.isNotBlank(text))
						out.append("<option selected value=\"").append(value).append("\">").append(text).append("</option>\n");
					} else {
						if(StringFunction.isNotBlank(text))
						out.append("<option value=\"").append(value).append("\">").append(text).append("</option>\n");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error in ListBox.createPanelGroupOptionList "
					+ e.toString());
		}
		return out.toString();
	}
}
