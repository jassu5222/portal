/*
 * @(#)LoggerServerLogic
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.logging;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.*;

/**
 * Special threaded class to handle each client that connects to the
 * socket the error logger listens to. An instance of this class is created
 * each time a client connects to the error logger socket port. This
 * instance reads data from the client's connection and processes it. When
 * the client closes their socket connection the thread terminates.
 */
public class LoggerServerLogic extends Thread
{
   public static SimpleDateFormat _format = new SimpleDateFormat ("yyyy.MM.dd HH:mm:ss");
   Socket         _clientSocket;
   LoggerServer   _logger;
   int            _threadNumber;
   public static final String STOP = "%%STOP%%";
   
   //IValavala IR POUM080661913
   String 		   callingProcess;

   /**
    * Create a new logger thread, which means setting method variables to
    * the specified values.
    *
    * @param logger The LoggerServer class instance that started the thread.
    * @param threadNumber Which thread number in the thread pool this
    * thread is.

    */
   public LoggerServerLogic (LoggerServer logger, int threadNumber)
   {
      _clientSocket = null;
      _threadNumber = threadNumber;
      _logger = logger;
   }

   /**
    * Sets the client socket being listened to to the specified socket.
    *
    * @param socket The client socket to listen to.
    */
   public synchronized void setClientSocket (Socket socket)
   {
      _clientSocket = socket;
      notify ();
   }

   /**
    * Performs the actual reading and processing of data from the client
    * socket. This involves parsing the information read from the socket and
    * calling another method to write the information to a log.
    *

    */
   public synchronized void run ()
   {
      BufferedReader reader = null;
      String logString = null,
             preLogString = null,       
             message = null,
             category = null;
      int    severity = 0,
             firstDelimiter = 0,
             secondDelimiter = 0,
             thirdDelimiter = 0; 

      while (true)
      {
         while (_clientSocket == null)
         {
            try
            {
               wait ();
            }
            catch (InterruptedException iEx)
            {  // Should never happen
               continue;
            }
         }
         synchronized (this)
         {
            try
            {
               reader = new BufferedReader (new InputStreamReader (
                  _clientSocket.getInputStream ()));
            }
            catch (IOException ioEx)
            {
               System.out.println (
                  "Could not open socket for input: " + ioEx.toString ());
               _clientSocket = null;
               _logger.releaseLoggerThread (_threadNumber);
               continue;
            }
            do
            {
               try
               {  
                  /*
                  First, line is supposed to be in the following format:
                  category$$severity$$message
                  */
                  logString = reader.readLine(); 
                
                  /*
                  Following is processing in a case when 'message' contains 'new
                  line' or 'return' characters ('\n' and '\r'). Those characters
                  are not read by readLine(), but serve as a sign for a next line.
                  To restore an original message, its separate pieces must be 
                  combined. Note that readLine() removes '\n' and '\r'. 
                  */
                  
                  while (true){
                    preLogString = reader.readLine();
                    
                    if (preLogString != null){
                    	// W Zhu 4/4/12 Rel 8.0 Sys Test troubleshooting Add line return to log string
                        logString = logString + "\r\n" + preLogString;
                        continue;
                    }
                    break;                 
                  }
                 
                  
                  if (logString == null)
                  {
                     _clientSocket = null;
                     _logger.releaseLoggerThread (_threadNumber);
                     continue;
                  }

		  // If the string sent in starts with the STOP string, stop the
                  // logger server process
                  if(logString.startsWith(STOP))
                     System.exit(2);

                  firstDelimiter =
                     logString.indexOf (LoggerServer.MESSAGE_DELIMITER);
                  if (firstDelimiter < 1)
                  {
                     System.out.println (
                        "Invalidly formatted log message read, no delimiter " +
                        "found: " + logString);
                     _clientSocket = null;
                     _logger.releaseLoggerThread (_threadNumber);
                     continue;
                  }
                  secondDelimiter =
                     logString.indexOf (
                        LoggerServer.MESSAGE_DELIMITER, firstDelimiter + 1);
                  if (secondDelimiter < 1)
                  {
                     System.out.println (
                        "Invalidly formatted log message read, only one " +
                        "delimiter found: " + logString);
                     _clientSocket = null;
                     _logger.releaseLoggerThread (_threadNumber);
                     continue;
                  }
                  category = logString.substring (0, firstDelimiter);
                  try
                  {
                     severity = Integer.parseInt (logString.substring (
                        firstDelimiter +
                           LoggerServer.MESSAGE_DELIMITER.length (),
                        secondDelimiter));
                  }
                  catch (NumberFormatException nfEx)
                  {
                     System.out.println (
                        "Could not determine severity of log message: " +
                        logString);
                     _clientSocket = null;
                     _logger.releaseLoggerThread (_threadNumber);
                     continue;
                  }
                //IValavala. IR POUM080661913. Added third delimiter. Retrieve callingProcess
                  thirdDelimiter =
                          logString.indexOf (
                             LoggerServer.MESSAGE_DELIMITER, secondDelimiter + 1);
                  callingProcess =  logString.substring (secondDelimiter + LoggerServer.MESSAGE_DELIMITER.length (),   thirdDelimiter);
                  
                  message = logString.substring (
                     secondDelimiter + LoggerServer.MESSAGE_DELIMITER.length (),
                     logString.length ());
                  processLogMessage (category, severity, message);
               }
               catch (IOException ioEx)
               {
                  System.out.println (
                     "Could not read from client socket" + ioEx.toString ());
               }
            } while (logString != null);
            try
            {
               if (reader != null)
                  reader.close ();
               if (_clientSocket != null)
                  _clientSocket.close ();
            }
            catch (IOException ioEx)
            {
            }
         }
         // Don't release twice if we stopped reading the socket abnormally
         if (logString != null)
         {
            _logger.releaseLoggerThread (_threadNumber);
            _clientSocket = null;
         }
      }
   }

   /**
    * Processes the specified log message. This involves the following
    * steps:<p>
    * 1) Find the LoggerCategoryInfo class instance associated with the
    * category name specified. If no instance is found, get the default
    * category's class instance.<p>
    * 2) Find the logical file name for the severity being logged. If
    * no severity is found, go back to step one, but look for just the
    * default category's class instance.<p>
    * 3) Get the file handle for the logical file name.<p>
    * 4) Write the message out to the file handle.<p>
    *
    * @param categoryName Name of the category to log for.
    * @param severity Severity of the message being logged.
    * @param message The actual message being logged.

    */
   private void processLogMessage (String categoryName,
                                   int severity,
                                   String message)
   {
      LoggerCategoryInfo logCategory = null;
      PrintWriter writer = null;
      DateFormat format = null;
      String   fileName = null;
      Calendar rightNow = null;
      
      

      logCategory = _logger.getLoggerCategoryForCategoryName (categoryName);
      // Make sure we found an instance. If we did not then we will try
      // again with the default category.
      if (logCategory != null)
      {
    	  fileName = logCategory.getLogFileNameForSeverity (severity);
          
         // Make sure we found a file name. If we did not then we will try
         // again with the default category.
         if (fileName != null)
         {
        	//IValavala IR POUM080661913
            // writer = _logger.getWriterForLogicalName (fileName);
        	 writer = _logger.getWriterForLogicalName (fileName, this.callingProcess);
            // If we found a writer then use it, otherwise we are done as
            // we are not logging this category/severity combination
            if (writer != null)
            {
               rightNow = Calendar.getInstance ();
               writer.println (
                  LoggerServerLogic._format.format (
                     rightNow.getTime ()) + "::" + categoryName + "::" +
                     severity + "::" + message);
               //IValavala Oct 6 2012. IR #MMUM092039389 . Added callingProcess
               verifyMaxFileSize (fileName, this.callingProcess, writer);
            }
            return;
         }
      }
      logCategory = _logger.getLoggerCategoryForCategoryName (
         LoggerServer.DEFAULT_CATEGORYNAME);
      // Make sure we found an instance. If we did not then we will print
      // an error as the default category should always exist
      if (logCategory != null)
      {
         fileName = logCategory.getLogFileNameForSeverity (severity);
         // Make sure we found an instance. If we did not then we are done
         // as we are not logging this category/severity combination
         if (fileName != null)
         {
            writer = _logger.getWriterForLogicalName (fileName,this.callingProcess );
            // If we found a writer then use it, otherwise we are done as
            // we are not logging this category/severity combination
            if (writer != null)
            {
               rightNow = Calendar.getInstance ();
               writer.println (
                  _format.format (rightNow.getTime ()) + "::" + categoryName +
                                  "::" + severity + "::" + message);
             //IValavala Oct 6 2012. IR#. Added callingProcess
               verifyMaxFileSize (fileName, this.callingProcess, writer);
            }
         }
         return;
      }
      System.out.println (
         "Could not determine where to send message for category: " +
         categoryName + " with severity: " + severity);
   }

   /**
    * Verifies that a log file has not exceeded it maximum size. If it has then
    * it is "rolled". This involves renaming the oversized file and opening a
    * new file handle to a file with the oversized file's original name. If
    * an error occurs in the process we do our best to make sure the logger
    * remains in a usable state, which may result in continuing to add data
    * to the oversized file.
    *
    * @param logicalName The logical name of the file we are checking the size
    * of.
    * @param currentWriter The PrintWriter currently associated with the
    * file we are checking the size of.

    */
 //IValavala Oct 6 2012. IR #MMUM092039389 . Added processName
   private synchronized void verifyMaxFileSize (String logicalName, String processName, 
                                                PrintWriter currentWriter)
   {
      SimpleDateFormat format = null;
      PrintWriter newWriter = null;
      Calendar rightNow = null;
      String   physicalName = null,
               backupName = null;
      int      maxFileLength = 0;
      File     currentFile = null,
               backupFile = null;
      
      physicalName = _logger.getPhysicalForLogicalName(logicalName)+ "_"+processName + ".log";

      if (physicalName.equals (LoggerServer.DO_NOT_LOG))
         return;
      maxFileLength = _logger.getMaxFileLength ();
      currentFile = new File (physicalName);
      if (currentFile.length () > maxFileLength)
      {  // We have exceeded our maximum size, create the suffix for the
         // rolled file's name
         rightNow = Calendar.getInstance ();
         format = new SimpleDateFormat ("yyyyMMddHHmmss");
         backupName = physicalName + "." + format.format (rightNow.getTime ());
         backupFile = new File (backupName);
         // Have to close the existing file or we cannot rename it
         currentWriter.close ();
         if (!currentFile.renameTo (backupFile))
         {  // If the rename fails, try to reopen the original file
            System.out.println (
               "Unable to roll file: " + physicalName + " to new log file: " +
               backupName);
            System.out.println (
               "This is typically caused when the file is in use, make sure" +
               " no other applications are using it");
            // Now we have to reestablish our connection to the old file so
            // we may continue logging
            try
            {
               newWriter =
                  new PrintWriter (new FileWriter (physicalName, true), true);
               System.out.println (
                  "Logging server will continue to use old, oversized file");
            }
            catch (IOException ioEx)
            {
               System.out.println (
                  "Logging server is corrupt for: " + physicalName);
               System.out.println ("Could not reopen it after log roll failed");
               System.out.println (ioEx.toString ());
            }
          //IValavala Oct 6 2012. IR#MMUM092039389. Added callingProcess
            _logger.replaceWriterForLogicalName (logicalName, processName, newWriter);
            return;
         }
         try
         {
            // Create the PrintWriter with autoflush on
            newWriter = new PrintWriter (new FileWriter (physicalName), true);
          //IValavala Oct 6 2012. IR#MMUM092039389. Added callingProcess
            _logger.replaceWriterForLogicalName (logicalName,processName, newWriter);
         }
         catch (IOException ioEx2)
         {
            System.out.println (
              "Unable to roll to new log file: " + ioEx2.toString ());
            System.out.println (
               "Logging server is corrupt for: " + physicalName);
            System.out.println ("Could not reopen it after log roll");
         }
      }
   }
}
