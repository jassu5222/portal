package com.amsinc.ecsg.util;

/*
 * @(#)DocumentNode
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */


/**
 * A DocumentNode is an element of the DocumentHandler Tree.
 * 
 *

 * @version 1.0, 05/20/2001
 * @since   JDK1.2.2
 */

public class DocumentNode implements java.io.Serializable
{
    //Tag Name holds the value of the XML tag for this node.  A  DocumentNode 
    //is not valid without a tagName.
    //Example: tagName for <Address>111 Bryce Court</Address> is "Address"
    private String          tagName;
    //ID Name holds the value of the ID tag for this node.  A  DocumentNode 
    //may or may not contain an ID.
    //Example: idName for <Address ID="MyAddress">111 Bryce Court</Address> is 
    //"MyAddress"
    private String          idName;
    
    //ShilpaR CR710 packaging task start
    //attr holds the attribute of an element tag
    //ex:  <Amount Ccy="USD">  attr holds Ccy=USD here
    private String          attr;
   
	//ShilpaR CR710 packaging task end 
    
    //Node Value holds the data value for this node.  A  DocumentNode 
    //may or may not contain a node value.
    //Example: nodeValue for <Address ID="MyAddress">111 Bryce Court</Address>
    //is "111 Bryce Court"
    private String          nodeValue;
    //parentNode holds a handle to the nodes parent.  This value will be null
    //if the node does not reference a parent.
    private DocumentNode    parentNode;
    //leftChildNode holds a handle to the nodes leftmost child.  This value will 
    //be null if the node is a leaf node and has no children.
    private DocumentNode    leftChildNode;
    //rightSiblingNode holds a handle to the nodes right sibling.  This value will
    //be null when the node was the last child node inserted for the parent.
    private DocumentNode    rightSiblingNode;

   /**  
    * Construct a DocumentNode from a tagName, idName and nodeValue.
    *
    * @param  tagName Value of the XML tag
    * @param  idName Value of the idName
    * @param  nodeValue Value of the nodeValue
    * @return A new DocumentHandler instance.
    */       
   public DocumentNode(String tagName, String idName, String nodeValue, boolean containsEscapeCharacters )
   {
        this.tagName = tagName;
        this.idName = idName;    
        this.setNodeValue(nodeValue, containsEscapeCharacters);
   }
   
   /**
    * @param tagName Value of the XML tag
    * @param idName Value of the idName
    * @param attribute name & value of attribute
    * @param nodeValue Value of the nodeValue
    * @param containsEscapeCharacters
    */
   public DocumentNode(String tagName, String idName,String attribute, String nodeValue, boolean containsEscapeCharacters )
   {
        this.tagName = tagName;
        this.idName = idName;    
        this.attr = attribute;    
        this.setNodeValue(nodeValue, containsEscapeCharacters);
   }
 
   /**  
    * Construct a DocumentNode from a tagName and nodeValue.
    *
    * @param  tagName Value of the XML tag
    * @param  nodeValue Value of the nodeValue
    * @return A new DocumentHandler instance.
    */       
   public DocumentNode(String tagName, String nodeValue, boolean containsEscapeCharacters)
   {
        this.tagName = tagName;
        this.setNodeValue(nodeValue, containsEscapeCharacters);
   }

   /**  
    * Construct a DocumentNode from a tagName and nodeValue.
    *
    * @param  nodeValue Value of the nodeValue
    * @return A new DocumentHandler instance.
    */       
   public DocumentNode(String tagName)
   {
        this.tagName = tagName;
   }
   
   
   /**  
    * Add a node as a child node to the current node.
    *
    * @param  DocumentNode A DocumentNode to be added as a child of this
    * node.
    * @return void
    */       
   public void appendChildToRight(DocumentNode childNode)
   {
        //if the node passed in is not null
        if (childNode != null)
        {            
           //Temporarily point to the leftmost child of childNode
            DocumentNode tempNode = leftChildNode;
            //If the current node has children, navigate to the right
            //most node.  This preserves the order in which nodes are inserted
            //into the document.
            if (tempNode != null)
            {
              while (tempNode.getRightSiblingNode() != null)
              {
                tempNode = tempNode.getRightSiblingNode();
              }
            }
            //Add the currentNode as the parent of the node to be inserted.
            childNode.setParentNode(this);
            
            //If the leftmost child is null, the current node has no children.
            //If the node has no children add the new node as the left most
            //child.
            if (leftChildNode == null) 
            {
                this.setLeftChildNode(childNode);
            }
            //Else add the node as the right most sibling of this nodes 
            //children.
            else if (tempNode != null)
            {
                tempNode.setRightSiblingNode(childNode);
            }
        }
   }
   
   /**  
    * Add a node as a child node to the current node.
    *
    * @param  DocumentNode A DocumentNode to be added as a child of this
    * node.
    * @return void
    */       
   public void appendChildToLeft(DocumentNode childNode)
   {
        //if the node passed in is not null
        if (childNode != null)
        {            
            childNode.setParentNode(this);
            childNode.setRightSiblingNode(leftChildNode);
            this.setLeftChildNode(childNode);
        }
   }
   /**  
    * Remove a child node from the document tree.
    *
    * @param  DocumentNode The childNode to be removed from the document tree.
    * @return void   
    */       
    
   public void removeChild(DocumentNode childNode)
   {
        DocumentNode tempNode = getLeftChildNode();
        //Start by finding the node that points to the node we want to remove.
        if (tempNode != childNode)
        {
            while ((tempNode != null) && (tempNode.getRightSiblingNode() != childNode))
            {
             tempNode = tempNode.getRightSiblingNode();
            }
            //If we've found the node, set the rightsibling to the node to be removed
            //right sibling.
            if (tempNode != null)
            {
               tempNode.setRightSiblingNode(childNode.getRightSiblingNode());
            }
        }        
        else if (tempNode == childNode)
        {
            //if the tempnode = childnode then the node we're looking for is the left
            //most child.
            setLeftChildNode(childNode.getRightSiblingNode());
        }
   }
   
   /**
    * @return attribute
    */
   public String getAttr() {
		return attr;
	}

	/**
	 * @param attr- attibute name and value
	 */
	public void setAttr(String attr) {
		this.attr = attr;
	}
   
   /**  
    * Setter Method
    *
    * @param  idName The idName to be set
    * @return void
    */       
   public void setIdName(String idName)
   {
        this.idName = idName;
   }

   /**  
    * Setter Method
    *
    * @param  nodeValue The nodeValue to be set
    * @return void
    */       
   public void setNodeValue(String nodeValue, boolean containsEscapeCharacters)
   {
        if (containsEscapeCharacters)
        {
            if ((nodeValue != null)&&(nodeValue != ""))
            {
                this.nodeValue = this.replaceEscapeCharacters(nodeValue);        
            }
        }
        else
        {
            this.nodeValue = nodeValue;
        }
   }

   /**  
    * Setter Method
    *
    * @param  parentNode The parentNode to be set
    * @return void
    */       
   public void setParentNode(DocumentNode parentNode)
   {
        this.parentNode = parentNode;
   }

   /**  
    * Setter Method
    *
    * @param  leftChildNode The leftChildNode to be set
    * @return void
    */       
   public void setLeftChildNode(DocumentNode leftChildNode)
   {
        this.leftChildNode = leftChildNode;
   }

   /**  
    * Setter Method
    *
    * @param  rightSiblingNode The rightSiblingNode to be set
    * @return void
    */       
   public void setRightSiblingNode(DocumentNode rightSiblingNode)
   {
        this.rightSiblingNode = rightSiblingNode;
   }

   /**  
    * Getter Method
    *
    * @return String tagName()
    */       
   public String getTagName()
   {
        return this.tagName;
   }

   /**  
    * Getter Method
    *
    * @return String Idname()
    */       
   public String getIdName()
   {
        return this.idName;
   }

   /**  
    * Getter Method
    *
    * @return String nodeValue()
    */       
   public String getNodeValue(boolean containsEscapeCharacters)
   {
        if (this.nodeValue != null)
            if (containsEscapeCharacters)
                return this.replaceWithEscapeCharacters(this.nodeValue);
            else
                return this.nodeValue;
        else
            return "";
   }

   /**  
    * Getter Method
    *
    * @return String parentNode()
    */       
   public DocumentNode getParentNode()
   {
        return this.parentNode;
   }

   /**  
    * Getter Method
    *
    * @return String leftChildNode()
    */       
   public DocumentNode getLeftChildNode()
   {
        return this.leftChildNode;
   }

   /**  
    * Getter Method
    *
    * @return String rightSiblingNode()
    */       
   public DocumentNode getRightSiblingNode()
   {
        return this.rightSiblingNode;
   }
   
   public String replaceWithEscapeCharacters(String nodeValue)
   {
       char[] nodeValueArray = nodeValue.toCharArray();
       char[] copyCharArray = new char[nodeValueArray.length*6];
       int j = 0;
       for (char aNodeValueArray : nodeValueArray) {
           if ((aNodeValueArray == '&') || (aNodeValueArray == '<') || (aNodeValueArray == '>') ||
                   (aNodeValueArray == '"') || (aNodeValueArray == '\'')) {
               if (aNodeValueArray == '&') {
                   copyCharArray[j++] = '&';
                   copyCharArray[j++] = 'a';
                   copyCharArray[j++] = 'm';
                   copyCharArray[j++] = 'p';
                   copyCharArray[j++] = ';';
               } else if (aNodeValueArray == '<') {
                   copyCharArray[j++] = '&';
                   copyCharArray[j++] = 'l';
                   copyCharArray[j++] = 't';
                   copyCharArray[j++] = ';';
               } else if (aNodeValueArray == '>') {
                   copyCharArray[j++] = '&';
                   copyCharArray[j++] = 'g';
                   copyCharArray[j++] = 't';
                   copyCharArray[j++] = ';';
               } else if (aNodeValueArray == '"') {
                   copyCharArray[j++] = '&';
                   copyCharArray[j++] = 'q';
                   copyCharArray[j++] = 'u';
                   copyCharArray[j++] = 'o';
                   copyCharArray[j++] = 't';
                   copyCharArray[j++] = ';';
               } else if (aNodeValueArray == '\'') {
                   copyCharArray[j++] = '&';
                   copyCharArray[j++] = 'a';
                   copyCharArray[j++] = 'p';
                   copyCharArray[j++] = 'o';
                   copyCharArray[j++] = 's';
                   copyCharArray[j++] = ';';
               }
           } else {
               copyCharArray[j++] = aNodeValueArray;
           }
       }
       
       return new String(copyCharArray,0,j);
       
   }
   
   public String replaceEscapeCharacters(String nodeValue)
   {
       char[] nodeValueArray = nodeValue.toCharArray();
       char[] copyCharArray = new char[nodeValueArray.length];
       int j = 0;
       for (int i=0;i<nodeValueArray.length;i++)
       {
         if (nodeValueArray[i] == '&')
         {
           i++;
           if (nodeValueArray[i] == 'a')
           {
             i++;
             if (nodeValueArray[i] == 'm')
             {
               copyCharArray[j++] = '&';
               i=i+2;
             }
             else
             {
               copyCharArray[j++] = '\'';
               i=i+3;
             }
           }
           else if (nodeValueArray[i] == 'l')
           {
               copyCharArray[j++] = '<';
               i=i+2;
           }
           else if (nodeValueArray[i] == 'g')
           {
               copyCharArray[j++] = '>';
               i=i+2;
           }
           else if (nodeValueArray[i] == 'q')
           {
               copyCharArray[j++] = '"';
               i=i+4;
           }
         }
         else
         {
           copyCharArray[j++] = nodeValueArray[i];
         }
       }
       return new String(copyCharArray,0,j);
   }  
}