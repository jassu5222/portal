package com.amsinc.ecsg.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.PropertyResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;

import com.amsinc.ecsg.frame.AmsConstants;

/**
 *  Utility class that contains static methods for use in encrypting and decrypting data.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class EncryptDecrypt
 {
    // Constants to define algorithms

    // Use SHA-1 20-byte hashing
    private static final String SHA1_ALGORITHM = "SHA1";

    // Use Triple DES with CBC (Cipher Block Chaining) and PKCS5 padding
    private static final String DES3_ALGORITHM = "DESede/CBC/PKCS5Padding";

    // RSA Encryption Algorithm - used for private / public key encryption
    // Use RSA with PKCS#1 padding - we should be able to use "RSA/PKCS1" here,
    // but that gives an error.   Using the numerical version instead.
    private static final String RSA_ALGORITHM = "2.5.8.1.1";

    // Constant for provider name
    private static final String PROVIDER_NAME = "BC";

    private static final String SHA1_WITH_RSA_SIGNATURE = "SHA1withRSA";

    //cquinton 7/21/2011 Rel 7.1.0 ppx240
    private static final String HMACMD5_ALGORITHM = "HmacMD5";


    // Static class variables

    // The initialization vector must be set in order to use CBC mode
    // The value for the initialization vector must always be the same when
    // encrypting.  Changing the IV changes the result of the encryption.
    private static final IvParameterSpec initializationVector;

    // TripleDES key
    private static final SecretKey commonDes3Key;


    // The blank is used as a workaround for a particular situation.   When
    // this was first developed, the TripleDES encryption was mistakenly set up to
    // use a key of an invalid size.  Instead of giving an error, the old JCE
    // provider simply used a blank key (but did use a secret initialization vector).
    // To be able to still use the old encrypted data, this is provided as a workaround
    // Any encryption done after using the new JCE provider will be done using the
    // proper key from Encryption.properties
    private static final SecretKey blankKey;

    // Create the cipher for TripleDES
    private static Cipher des3cipher;

    // Create the cipher for TripleDES
    private static Cipher rsaCipher;

    // MessageDigest used to perform SHA-1 hashes
    private static MessageDigest sha1;

    private static Signature rsaSignature;

  //TRUDDEN IR RUJ041658830 Add Begin
    // Use MD5 24-byte hashing
    private static final String MD5_ALGORITHM = "MD5";

    // MessageDigest used to perform SHA-1 hashes
    private static MessageDigest md5Digest;
  //TRUDDEN IR RUJ041658830 End

    // Static initializer
    // This is only called once during the life of the JVM
    static
     {
        // Add BouncyCastle as the primary Security Provider
        Security.addProvider(new BouncyCastleProvider());

        PropertyResourceBundle encryptionProperties =
                (PropertyResourceBundle)PropertyResourceBundle.getBundle("Encryption");

	// Get the key and the IV from the Encryption.properties file
	byte[] ivBytes = base64StringToBytes(encryptionProperties.getString("TripleDESInitializationVector"));

        initializationVector = new IvParameterSpec(ivBytes);

        byte[] keyBytes = base64StringToBytes(encryptionProperties.getString("TripleDESKey"));

        commonDes3Key = new SecretKeySpec(keyBytes, DES3_ALGORITHM);

        byte[] blankKeyBytes = new byte[24];

        blankKey = new SecretKeySpec(blankKeyBytes, DES3_ALGORITHM);

        try
         {
            // Create the cipher for TripleDES
            des3cipher = Cipher.getInstance(DES3_ALGORITHM, PROVIDER_NAME);

            // Get an instance of the SHA1 algorithm
            sha1 = MessageDigest.getInstance(SHA1_ALGORITHM);

            // Create the cipher for RSA
            rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);

            // Create the signature engine for creating and verifying digital signatures.
            rsaSignature = Signature.getInstance(SHA1_WITH_RSA_SIGNATURE);

            //TRUDDEN IR RUJ041658830 Add Begin
            md5Digest = MessageDigest.getInstance(MD5_ALGORITHM);
            //TRUDDEN IR RUJ041658830 End
         }
        catch (NoSuchProviderException nspe)
         {
            nspe.printStackTrace();
         }
        catch (NoSuchAlgorithmException nsae)
         {
            nsae.printStackTrace();
         }
	catch (Exception e)
	 {
	    e.printStackTrace();
	 }
     }


    /**
     * Encrypt a string using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @return a string of digits representing the encrypted input bytes
     */
    public static String encryptStringUsingTripleDes(String input)
     {
       return encryptStringUsingTripleDes(input, null, true);
     }

    /**
     * Encrypt a String using the TripleDES and the specified secret key.  This key can be different for each user session.
     * @param input
     * @param key
     * @return
     */
	// W Zhu 8/17/2012 Rel 8.1 T36000004579 add key parameter.
    public static String encryptStringUsingTripleDes(String input, SecretKey key)
    {
      return encryptStringUsingTripleDes(input, key, true);
    }

    /**
     * Encrypt a string using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @param replaceSlash - indicates whether base64 encoding should replace / with !.  This should be true
     *                       if the encoded value is being placed into a URL
     * @return a string of digits representing the encrypted input bytes
     */
    public static String encryptStringUsingTripleDes(String input, SecretKey key, boolean replaceSlash)
     {
         if (key == null) {
        	key = commonDes3Key;
        }

         byte[] inputBytes = null;
         try
         {
            // Convert the input string into bytes using Java's getBytes method
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing base64 strings.
            inputBytes = input.getBytes(AmsConstants.UTF8_ENCODING);
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        // Convert encrypted bytes into our string format so that
        // it can be included in URLs if necessary
        return bytesToBase64String(encryptUsingDes3(inputBytes, key, false), replaceSlash);
     }

    /**
     * Decrypt a string using TripleDES encryption and the common static TripleDES key.
     * @param encryptedString
     * @return
     */
    public static String decryptStringUsingTripleDes(String encryptedString) {
    	return decryptStringUsingTripleDes(encryptedString, null);
    }


    /**
     * Decrypt a string using TripleDES encryption and the key.
     *
     * @param encryptedString - a string of digits representing the encrypted input bytes
     * @param key - a TripleDES key.  The key could be different for each user session.
     * @return the string that was originally encrypted
     */
   // W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter
    // W Zhu 8/28/2012 Rel 8.1 T36000004915 check for empty encryptedString
    public static String decryptStringUsingTripleDes(String encryptedString, SecretKey key)
     {
    	//Added trim()
    	if (encryptedString == null || "".equals(encryptedString.trim())) {
    		return encryptedString;
    	}

    	if (key == null) {
    		key = commonDes3Key;
    	}
        // Convert from a string of numbers to an array of bytes using
        // our conversion method and then decrypt those bytes
        byte[] decryptedBytes = decryptUsingDes3(base64StringToBytes(encryptedString), key, false);

        // Convert decrypted bytes to a string (the original value that was encrypted) using Java's method
        String returnString = null;
        try
         {
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
           returnString = new String( decryptedBytes, AmsConstants.UTF8_ENCODING );
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return returnString;
     }

    /**
     * Creates a SHA-1 hash of the input.  The output is always 20 bytes.
     *
     *
     * @param valueToHash - the input to the hash algorithm
     * @return the hashed value
     */
    public static synchronized byte[] createSha1Hash(byte[] valueToHash)
     {
        // Tell the alogorithm what value to hash
        sha1.update(valueToHash);

        // call digest() to produce the hash.
        return sha1.digest();
     }


    /**
     * Encrypt an array of bytes using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @return an array of bytes representing the encrypted value
     */
    public static synchronized byte[] encryptUsingDes3(byte[] input)
     {
         return encryptUsingDes3(input, commonDes3Key, false);
     }

    public static synchronized byte[] encryptUsingDes3SessionKey(byte[] input)
    {
    	return encryptUsingDes3(input, null, false);
    }

    /**
     * Encrypt an array of bytes using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @return an array of bytes representing the encrypted value
     */
    private static synchronized byte[] encryptUsingDes3(byte[] input, SecretKey key, boolean useBlankKey)
     {
        try
         {
            // Set up cipher with the key, the IV and indicate encrypt mode
            if(useBlankKey)
               // The blank is used as a workaround for a particular situation.   When
               // this was first developed, the TripleDES encryption was mistakenly set up to
               // use a key of an invalid size.  Instead of giving an error, the old JCE
               // provider simply used a blank key (but did use a secret initialization vector).
               // To be able to still use the old encrypted data, this is provided as a workaround
	       des3cipher.init(Cipher.ENCRYPT_MODE, blankKey, initializationVector);
            else
               // Use the proper key from Encryption.properties
	       des3cipher.init(Cipher.ENCRYPT_MODE, key, initializationVector);

            // Call doFinal() to perform the encryption
	    return des3cipher.doFinal(input);

	 }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                des3cipher = Cipher.getInstance(DES3_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                des3cipher = Cipher.getInstance(DES3_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
     }
  //Ravindra - 10/01/2011 - CR-541 - Start
    /*
     * Decrypt string value using AES encryption.
     */
  //AAlubala IR SGUM102560422 Rel 8.1 CMA - [BEGIN]
    //Changes from Steven Tang 
    //10/26/2012
    //The key is in HEX format, so, the AES decrypt / encrypt modified to reflect this
    public static String decryptUsingAESKey(String encrypted, String AESKey) {
    	try {

            int NumberChars = AESKey.length();

            ByteArrayOutputStream bas = new ByteArrayOutputStream();

            for(int i=0; i<NumberChars; i+=2){
                int b = Integer.parseInt(AESKey.substring(i, i + 2), 16);
                bas.write(b);
            }

            byte[] raw = bas.toByteArray();


    		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    		//AAlubala IR SGUM102560422 Rel 8.1 CMA - [BEGIN]
    		Cipher cipher = Cipher.getInstance("AES");
    		// IR SGUM102560422 [END]
    		cipher.init(Cipher.DECRYPT_MODE, skeySpec);

    		byte[] decodedValue = new Base64().decode(encrypted);
    		byte[] original = cipher.doFinal(decodedValue);

    		return new String(original);
    	} catch (Exception exc) {
    		exc.printStackTrace();
    	}
          return null;
    }

    /*
     * Encrypt string value using AES encryption.
     */

    public static String encryptUsingAESKey(String text, String AESKey) {
    	
    	try {
           int NumberChars = AESKey.length();

           ByteArrayOutputStream bas = new ByteArrayOutputStream();

           for(int i=0; i<NumberChars; i+=2){
               int b = Integer.parseInt(AESKey.substring(i, i + 2), 16);
               bas.write(b);
           }

           byte[] raw = bas.toByteArray();

           int byteLength = (raw.length)*8;

           System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
           System.out.println("!!!!!!!!!encryptUsingAESKey!!!!!!!!!!!!!!!!!");
           System.out.println("!! Length of key = [" + byteLength +  "]");
           System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
           System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


    		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    		Cipher cipher = Cipher.getInstance("AES");
    		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
    		byte[] original = Base64.encode(cipher.doFinal(text.getBytes()));
    		return new String(original);
    	} catch (Exception exc) {
    		exc.printStackTrace();
    	}
          return null;
	}
  //Ravindra - 10/01/2011 - CR-541 - End
    
  //AAlubala IR SGUM102560422 Rel 8.1 CMA - [END]
    /**
     * Decrypt an array of bytes using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @return an array of bytes representing the encrypted value
     */
    public static synchronized byte[] decryptUsingDes3(byte[] input)
     {
         return decryptUsingDes3(input, commonDes3Key, false);
     }

    public static synchronized byte[] decryptUsingDes3(byte[] input, boolean useBlankKey)
    {
        return decryptUsingDes3(input, commonDes3Key, useBlankKey);
    }

    /**
     * Decrypt an array of bytes using TripleDES encryption.
     *
     * @param input - an array of bytes to encrypt
     * @return an array of bytes representing the encrypted value
     */
    public static synchronized byte[] decryptUsingDes3(byte[] input, SecretKey key, boolean useBlankKey)
     {

        try
         {
            // Set up cipher with the key, the IV and indicate decrypt mode
            if(useBlankKey)
               // The blank is used as a workaround for a particular situation.   When
               // this was first developed, the TripleDES encryption was mistakenly set up to
               // use a key of an invalid size.  Instead of giving an error, the old JCE
               // provider simply used a blank key (but did use a secret initialization vector).
               // To be able to still use the old encrypted data, this is provided as a workaround
	       des3cipher.init(Cipher.DECRYPT_MODE, blankKey, initializationVector);
            else
               // Use the proper key from Encryption.properties
	       des3cipher.init(Cipher.DECRYPT_MODE, key, initializationVector);

            return des3cipher.doFinal(input);
         }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") >= 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                des3cipher = Cipher.getInstance(DES3_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            	System.out.println("des3 cipher init exception 1: "+ex);
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                des3cipher = Cipher.getInstance(DES3_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            	System.out.println("des3 cipher init exception 2: "+ex);
            }
            return null;
         }
     }


    /**
     * Encrypt a string using a public key
     *
     * @param encryptThis - string to encrypt
     * @param key - public key
     * @return base64 representation of the encrypted value
     */
    public static String encryptUsingPublicKey(String encryptThis, PublicKey key)
     {
         return encryptUsingPublicKey(encryptThis, key, true);
     }

    /**
     * Encrypt a string using a public key
     *
     * @param encryptThis - string to encrypt
     * @param key - public key
     * @param replaceSlash - indicates whether base64 encoding should replace / with !.  This should be true
     *                       if the encoded value is being placed into a URL
     * @return base64 representation of the encrypted value
     */
    public static String encryptUsingPublicKey(String encryptThis, PublicKey key, boolean replaceSlash)
     {
        byte[] bytesToEncrypt = null;

        try
         {
            // Convert the input string into bytes using Java's getBytes method
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
            bytesToEncrypt = encryptThis.getBytes(AmsConstants.UTF8_ENCODING);
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return bytesToBase64String(encryptUsingPublicKey(bytesToEncrypt, key), replaceSlash);
     }

    /**
     * Encrypt a string using a private key
     *
     * @param encryptThis - string to encrypt
     * @param key - private key
     * @return base64 representation of the encrypted value
     */
    public static String encryptUsingPrivateKey(String encryptThis, PrivateKey key)
     {
        return encryptUsingPrivateKey(encryptThis, key, true);
     }

    /**
     * Encrypt a string using a private key
     *
     * @param encryptThis - string to encrypt
     * @param key - private key
     * @param replaceSlash - indicates whether base64 encoding should replace / with !.  This should be true
     *                       if the encoded value is being placed into a URL
     * @return base64 representation of the encrypted value
     */
    public static String encryptUsingPrivateKey(String encryptThis, PrivateKey key, boolean replaceSlash)
     {
        byte[] bytesToEncrypt = null;

        try
         {
            // Convert the input string into bytes using Java's getBytes method
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
            bytesToEncrypt = encryptThis.getBytes(AmsConstants.UTF8_ENCODING);
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return bytesToBase64String(encryptUsingPrivateKey(bytesToEncrypt, key), replaceSlash);
     }

    /**
     * Decrypt a string (in base64 format) using a public key
     *
     * @param decryptThis - base64 representation of encrypted string to decrypt
     * @param key - public key
     * @return the decrypted value as a string
     */
    public static String decryptUsingPublicKey(String decryptThis, PublicKey key)
     {
        byte[] decryptedBytes = decryptUsingPublicKey(base64StringToBytes(decryptThis), key);

        // Convert decrypted bytes to a string (the original value that was encrypted) using Java's method
        String returnString = null;
        try
         {
           // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
           //returnString = new String( decryptedBytes, AmsConstants.ISO_ENCODING );
           returnString = new String( decryptedBytes, AmsConstants.UTF8_ENCODING );
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return returnString;
     }

    /**
     * Decrypt a string (in base64 format) using a private key
     *
     * @param decryptThis - base64 representation of encrypted string to decrypt
     * @param key - private key
     * @return  the decrypted value as a string
     */
    public static String decryptUsingPrivateKey(String decryptThis, PrivateKey key)
     {
        byte[] decryptedBytes = decryptUsingPrivateKey(base64StringToBytes(decryptThis), key);

        // Convert decrypted bytes to a string (the original value that was encrypted) using Java's method
        String returnString = null;
        try
         {
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
           returnString = new String( decryptedBytes, AmsConstants.UTF8_ENCODING );
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return returnString;
     }

    /**
     * Create a base64 encoded digital signature using a plaintext string to sign and a
     * base64 encoded private key.
     *
     * @param stringToSign - plaintext string to digitally sign
     * @param privateKey -  private key
     * @return base64 representation of the digital signature
     */
    public static String createDigitialSignature(String stringToSign, PrivateKey privateKey)
     {
        return createDigitialSignature(stringToSign, privateKey, true);
     }

    /**
     * Create a base64 encoded digital signature using a plaintext string to sign and a
     * base64 encoded private key.
     *
     * @param stringToSign - plaintext string to digitally sign
     * @param privateKey -  private key
     * @param replaceSlash - indicates whether base64 encoding should replace / with !.  This should be true
     *                       if the encoded value is being placed into a URL
     * @return base64 representation of the digital signature
     */
    public static String createDigitialSignature(String stringToSign, PrivateKey privateKey, boolean replaceSlash)
     {
        byte[] bytesToSign = null;

        try
         {
            // Convert the input string into bytes using Java's getBytes method
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
            bytesToSign = stringToSign.getBytes(AmsConstants.UTF8_ENCODING);
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }
       
        return bytesToBase64String(createDigitalSignature(bytesToSign, privateKey), replaceSlash);
     }

     /**
      * Returns a boolean indicating whether or not the digital signature is valid for the message
      * passed in and the public key.  Accepts a plaintext message, a base64 encoded signature, and a
      * base64 encoded public key.
      *
      * @param message - the plaintext message that was digitally signed
      * @param signature - base64 encoded digital signature
      * @param publicKey - public key
      */
     public static boolean verifyDigitalSignature(String message, String signature, PublicKey publicKey)
      {
        byte[] messageBytes = null;

        try
         {
            // Convert the input string into bytes using Java's getBytes method
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
            messageBytes = message.getBytes(AmsConstants.UTF8_ENCODING);
         }
        catch( UnsupportedEncodingException uee)
         {
            // This should never happen - "ASCII" is a valid encoding
         }

        return verifyDigitalSignature(messageBytes, base64StringToBytes(signature), publicKey);
      }



    /**
     * Encrypt an array of bytes using RSA encryption given a public key
     *
     * @param input - an array of bytes to encrypt
     * @param keyBytes - PublicKey object representing the public key
     * @return an array of bytes representing the encrypted value
     */
    private static synchronized byte[] encryptUsingPublicKey(byte[] input, PublicKey key)
     {
        try
         {
            // Set up cipher with the key and indicate encrypt mode
	    rsaCipher.init(Cipher.ENCRYPT_MODE, key);

            // Perform the encryption
            return rsaCipher.doFinal(input);
	 }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
     }

    /**
     * Encrypt an array of bytes using RSA encryption given a private key
     *
     * @param input - an array of bytes to encrypt
     * @param keyBytes - PrivateKey object representing the private key
     * @return an array of bytes representing the encrypted value
     */
    public static synchronized byte[] encryptUsingPrivateKey(byte[] input, PrivateKey key)
     {
        try
         {
            // Set up cipher with the key and indicate encrypt mode
	    rsaCipher.init(Cipher.ENCRYPT_MODE, key);

            // Perform the encryption
            return rsaCipher.doFinal(input);
	  }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
     }

    /**
     * Decrypt an array of bytes using RSA encryption given a public key
     *
     * @param input - an array of bytes to decrypt
     * @param keyBytes - PublicKey object representing the public key
     * @return an array of bytes representing the decrypted value
     */
    public static synchronized byte[] decryptUsingPublicKey(byte[] input, PublicKey key)
     {
        try
         {
            // Set up cipher with the key and indicate decrypt mode
	    rsaCipher.init(Cipher.DECRYPT_MODE, key);

            // Perform the decryption
            return rsaCipher.doFinal(input);
	 }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
     }


    /**
     * Decrypt an array of bytes using RSA encryption given a private key
     *
     * @param input - an array of bytes to decrypt
     * @param keyBytes - PrivateKey object representing the private key
     * @return an array of bytes representing the decrypted value
     */
    private static synchronized byte[] decryptUsingPrivateKey(byte[] input, PrivateKey key)
     {
        try
         {
            // Set up cipher with the key and indicate decrypt mode
	    rsaCipher.init(Cipher.DECRYPT_MODE, key);

            // Perform the decryption
            return rsaCipher.doFinal(input);
	 }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Sometimes when there is an exception the cipher goes into an unusable state
                // Get a new cipher to prevent this.
                rsaCipher = Cipher.getInstance(RSA_ALGORITHM, PROVIDER_NAME);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
     }


     /**
      * Returns a digital signature for the bytes passed in.   The digital signature is created
      * using a private key.
      *
      * @param bytesToSign - the bytes that comprise the message to be signed
      * @param privateKey - private key
      * @return the digital signature
      */
     private static synchronized byte[] createDigitalSignature(byte[] bytesToSign, PrivateKey key)
      {
        try
         {
            // Initialize the signature with the key
            rsaSignature.initSign(key);

            // Initialize the signature with the message to be signed
            rsaSignature.update(bytesToSign);

            // Create the digital signature
            return rsaSignature.sign();
	  }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Renew the signature - sometimes if there is a problem signing
                // the Signature object needs to be created again
                rsaSignature = Signature.getInstance(SHA1_WITH_RSA_SIGNATURE);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Renew the signature - sometimes if there is a problem signing
                // the Signature object needs to be created again
                rsaSignature = Signature.getInstance(SHA1_WITH_RSA_SIGNATURE);
            }
            catch (Exception ex)
            {
            }
            return null;
         }
      }

     /**
      * Returns a boolean to indicate if the digital signature is accurate for the message passed
      * in and the public key.
      *
      * @param messageBytes - bytes representing the message that was signed
      * @param signature - the digital signature
      * @param publicKey - the public key used to verify the signature
      * @return true if the signature is valid, false otherwise
      */
     private static synchronized boolean verifyDigitalSignature(byte[] messageBytes, byte[] signature, PublicKey key)
      {
        try
         {
            // Initialize the signature with the key
            rsaSignature.initVerify(key);

            // Initialize the signature with the message that was signed
            rsaSignature.update(messageBytes);

            // Verify the signature
            return rsaSignature.verify(signature);
	  }
        catch (SecurityException e)
         {
            e.printStackTrace();

            if(e.getMessage().indexOf("Unsupported keysize") > 0)
             {
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!! Unsupported keysize exceptions are often caused by not having the ");
               System.out.println("!! unlimited keysize encryption policy files located in the jre/lib/security");
               System.out.println("!! directory of both the JDK and of the JRE_* directory under the BEA home directory");
               System.out.println("!! Download the proper files and overwrite local_policy.jar and US_export_policy.jar");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
             }

            try
            {
                // Renew the signature - sometimes if there is a problem signing
                // the Signature object needs to be created again
                rsaSignature = Signature.getInstance(SHA1_WITH_RSA_SIGNATURE);
            }
            catch (Exception ex)
            {
            }
            return false;
         }
        catch (Exception e)
         {
            e.printStackTrace();
            try
            {
                // Renew the signature - sometimes if there is a problem signing
                // the Signature object needs to be created again
                rsaSignature = Signature.getInstance(SHA1_WITH_RSA_SIGNATURE);
            }
            catch (Exception ex)
            {
            }
            return false;
         }

      }


    /**
     * Converts an array of bytes into a string using base64 encoding.
     * base64 is an algorithm for representing a set of bytes as a string.
     * The official name is Base64 Content-Transfer-Encoding and more information
     * on its details can be found in RFC-2045 at
     *       http://www.oac.uci.edu/indiv/ehood/MIME/2045/rfc2045.html
     *
     *
     * @param bytes array of bytes to convert
     * @return string representing the bytes in the above format
     */
    public static String bytesToBase64String(byte[] rawBytes)
     {
        return bytesToBase64String(rawBytes, true);
     }


    /**
     * Converts an array of bytes into a string using base64 encoding.
     * base64 is an algorithm for representing a set of bytes as a string.
     * The official name is Base64 Content-Transfer-Encoding and more information
     * on its details can be found in RFC-2045 at
     *       http://www.oac.uci.edu/indiv/ehood/MIME/2045/rfc2045.html
     *
     *
     * @param bytes array of bytes to convert
     * @param replaceSlash - indicates whether or not to replace / with !
     * @return string representing the bytes in the above format
     */
    public static String bytesToBase64String(byte[] rawBytes, boolean replaceSlash)
    {
       String base64 = null;

       try
        {
          // Encode the bytes, which returns an array of bytes.  Then, convert those
          // bytes into ASCII
           // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
          //base64 = new String(Base64.encode(rawBytes), AmsConstants.ISO_ENCODING);
          base64 = new String(Base64.encode(rawBytes), AmsConstants.UTF8_ENCODING);

          // The ! character is not actually part of the base64 specification
          // When a '/' character is included in a URL, it is interpreted to mean
          // a ! character.  Therefore, when encoding, we should treat the !
          // character as being identical to the '/' character.
          // Replace all instances of the '/' character with '!'
          if(replaceSlash)
             base64 = base64.replace('/','!');
       }
       catch( UnsupportedEncodingException uee)
        {
          // This should never happen - "ASCII" is a valid encoding
        }

        // The algorithm places a newline at the end of each base64 string
        // Strip it off here
        return base64.trim();
    }


    /**
     * Given a file path, this method reads the file at that path as a
     * certificate and then gets the public key off of it.  It is assumed
     * that the file is in PEM format (very similar to base64)
     *
     * @param certificateFilePath - path to the certificate file
     * @return PublicKey representing the public key read from the certificate
     */
    public static PublicKey getPublicKeyFromCertificate(String certificateFilePath)
     {
       try(PEMReader pemReader = new PEMReader(new InputStreamReader(new FileInputStream(certificateFilePath)), null))
         {
            // Create a certificate from those bytes, assuming PEM format
           
            X509Certificate cert = (X509Certificate) pemReader.readObject();
            
            // Get public key from certificate
            return cert.getPublicKey();
         }
        catch(FileNotFoundException fnfe)
         {
            fnfe.printStackTrace();
            return null;
         }
        catch(IOException ioe)
         {
            // Do nothing, just return null
            return null;
         }
        catch(Exception e)
         {
            e.printStackTrace();
            return null;
         }
     }


    /**
     * Given a file path, this method reads the file at that path as a
     * RSA private key and then gets the PrivateKey object.  It is assumed
     * that the file is in PEM format (very similar to base64)
     *
     * @param certificateFilePath - path to the certificate file
     * @return PublicKey representing the public key read from the certificate
     */
    public static PrivateKey getPrivateKeyFromFile(String filePath)
     {
            try
             {
                 // The format must be PEM with no password protection
                 String privateKeyFileContents = "";

    	         File f = new File(filePath);

	         if(f.exists())
               {
	        	 try(FileReader fr = new FileReader(f)){

	  	    	// Read in characters from file
	  	    	char[] c = new char[(int)f.length()];
	 	    	fr.read(c,0,(int)f.length());

	 	    	privateKeyFileContents = new String(c);
	  	    	}
               }

                 // Some of the files containing our private keys have "BEGIN CERTIFICATE" instead
                 // of "BEGIN RSA PRIVATE KEY" like they should.  To make sure that the files will
                 // work, change the text as its read in
                 if( (privateKeyFileContents.indexOf("-----BEGIN CERTIFICATE-----") >= 0) &&
                     (privateKeyFileContents.indexOf("-----END CERTIFICATE-----") >= 0) )
                  {
                    privateKeyFileContents = StringService.change(privateKeyFileContents, "-----BEGIN CERTIFICATE-----",
                                                                                          "-----BEGIN RSA PRIVATE KEY-----");
                    privateKeyFileContents = StringService.change(privateKeyFileContents, "-----END CERTIFICATE-----",
                                                                                          "-----END RSA PRIVATE KEY-----");
                  }

				try (PEMReader pemReader = new PEMReader(new StringReader(privateKeyFileContents), null)) {
					KeyPair keyPair = (KeyPair) pemReader.readObject();
					if (keyPair == null) {
						return null;
					}
					return keyPair.getPrivate();
				}
             }
            catch(IOException ioe)
             {
                return null;
             }
            catch(Exception e)
             {
                e.printStackTrace();
                return null;
             }
     }



   /**
    * Converts a base64 string into bytes.
    * base64 is an algorithm for representing a set of bytes as a string.
    * The official name is Base64 Content-Transfer-Encoding and more information
    * on its details can be found in RFC-2045 at
    *       http://www.oac.uci.edu/indiv/ehood/MIME/2045/rfc2045.html
    *
    *
    * @param str base64-encoded String to convert
    * @return array of bytes
    */
   public static byte[] base64StringToBytes(String str)
    {
		return base64StringToBytes(str, true);
   }
   public static byte[] base64StringToBytes(String str, boolean replaceFlag)
    {

        // The space character is not actually part of the base64 specification
      // When a '+' character is included in a URL, it is interpreted to mean
      // a space character.  Therefore, when decoding, we should treat the space
      // character as being identical to the '+' character.
      // Replace all instances of the ' ' character with '+'
      str = str.replace(' ','+');
      // The ! character is not actually part of the base64 specification
      // When a '/' character is included in a URL, it is interpreted to mean
      // a ! character.  Therefore, when decoding, we should treat the !
      // character as being identical to the '/' character.
      // Replace all instances of the '!' character with '/'
      if (replaceFlag)
      str = str.replace('!','/');

      // In case there are any newline characters, remove them.
      str = StringService.change(str, "\n", "");
      str = StringService.change(str, "\r", "");

      // First, convert the string passed in to bytes so that it can be decoded
        byte[] decodedBytes = null;
        try
       {
          // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing encrypted strings.
          decodedBytes = Base64.decode(str.getBytes(AmsConstants.UTF8_ENCODING));
       }
      catch( UnsupportedEncodingException uee)
       {
          // This should never happen - "ASCII" is a valid encoding
       }

      return decodedBytes;
    }

   /**
    * Encodes a plaintext string into base64 encoding.
    *
    * @param str plaintext string to convert to base 64
    * @return base 64 string
    */
   public static String stringToBase64String(String str)
      {
        if(str.isEmpty())
            return str;

        try
         {
            // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing base64 strings.
            return bytesToBase64String(str.getBytes(AmsConstants.UTF8_ENCODING));
         }
        catch(UnsupportedEncodingException ce)
         {
            ce.printStackTrace();
            return null;
         }
      }

   /**
    * Decodes a base 64 string into a plaintext string
    *
    * @param str base 64 encoded string
    * @return plaintext string
    */
   public static String base64StringToString(String str)
      {
        if(str.isEmpty())
            return str;

        try
         {
           // CR-486 Use UTF-8 Encoding.  UTF-8 is superset of ISO 8859-1 so we are safe with any existing base64 strings.
           return new String(base64StringToBytes(str), AmsConstants.UTF8_ENCODING);
         }
        catch(UnsupportedEncodingException ce)
         {
            ce.printStackTrace();
            return null;
         }
      }

   //cquinton 7/21/2011 Rel 7.1.0 ppx240 start
   /**
    * Converts a byte array to a hex string
    */
   public static String bytesToHexString( byte[] bytes ) {

      //the following converts the given byte array to a hex string
      //it doubles the length using the left shift operator
      //because each byte is represented by 2 characters
      BigInteger bi = new BigInteger(1, bytes );
      String hexString = String.format("%0" + (bytes.length << 1) + "X", bi);
      return hexString;
   }

   /**
    * Generates a message authentication code hash value from
    * the given data and key using the HmacMD5 algorithm.
    * The MD5 algorithm result will always be 128bits (16 bytes).
    *
    * Note the class name misnomer -- this is cryptography, not encryption,
    * but we still include here becuase of similarities...
    *
    * @param data
    * @param keyBytes
    * @return 16 byte hash if successful or null if not
    */
   public static byte[] createHmacMD5Hash( byte[] dataBytes, byte[] keyBytes ) {
      byte[] hashValueBytes;
      Key key = new SecretKeySpec(keyBytes, HMACMD5_ALGORITHM );
      try {
         Mac mac = Mac.getInstance( HMACMD5_ALGORITHM );
         mac.init(key);

         hashValueBytes = mac.doFinal( dataBytes );
      } catch( Exception ex ) {
         System.err.println("EncryptDecrypt.createHmacMD5Hash: Problem creating hash: " + ex.toString() );
         ex.printStackTrace();
         hashValueBytes=null;
      }

      return hashValueBytes;
   }
   //cquinton 7/21/2011 Rel 7.1.0 ppx240 end

   //TRUDDEN IR RUJ041658830 Add Begin
   /**
    * Creates a MD5 hash of the input.
    *
    *
	
    * @param valueToHash - the input to the hash algorithm
    * @return the hashed value
    */
	
	/**
	 *@param byte array
   * @return hexidecimal value
   */
  public static String bytesTohex(byte[] b)
    {
       String hs = "";
       String stmp = "";

       for (int n = 0; n < b.length; n++)
       {
          stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));

          if (stmp.length() == 1)
          {
             hs = hs + "0" + stmp;
          }
          else
          {
             hs = hs + stmp;
          }

          if (n < b.length - 1)
          {
             hs = hs + "";
          }
       }

       return hs;
    }
   public static synchronized byte[] createMD5Hash(byte[] valueToHash)
    {

       // Tell the alogorithm what value to hash
       md5Digest.update(valueToHash);

       // call digest() to produce the hash.
       return 	md5Digest.digest();
    }
   //TRUDDEN IR RUJ041658830 End

   // CR-473 begin
   /**
    * Get X.509 Certificate from bytes.
    *
    * @parm certificateBytes - certificate data in bytes.
    * @return certificate
    *
    */
   public static X509Certificate getCertificate(byte[] certificateBytes) {
       try {
           CertificateFactory cf = CertificateFactory.getInstance("X.509");
           weblogic.security.PEMInputStream pemIs = new weblogic.security.PEMInputStream(new ByteArrayInputStream(certificateBytes));
           BufferedInputStream bufis = new BufferedInputStream(pemIs);
           X509Certificate certificate = (X509Certificate)cf.generateCertificate(bufis);
           return certificate;

           // Note an alternative is to use the bouncy castle's PEMReader.  PEMReader requires the certificate
           // starts with "-----BEGIN CERTIFICATE-----\r\n" and ends with "\r\n-----END CERTIFICATE-----".
           // weblogic.security.PEMInputStream can read with or without the prefix.
           //   PEMReader pemReader = new PEMReader(new InputStreamReader(new java.io.ByteArrayInputStream(certificateBytes)), null);
           //   X509Certificate cert = (X509Certificate) pemReader.readObject();
           // OR
           // CertificateFactory cf = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);
           // X509Certificate certificate = (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(certificateBytes));

       }
       catch (Exception e) {
           e.printStackTrace();
           return null;
       }

   }
   // CR-473 end

 }






