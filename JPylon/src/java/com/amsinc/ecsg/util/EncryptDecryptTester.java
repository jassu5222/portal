package com.amsinc.ecsg.util;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EncryptDecryptTester
{ 
 public static void main (String[] argv)
 {
  if (argv.length == 0) {
     System.out.println("Usage: java com.amsinc.ecsg.util.EncryptDecryptTester [testString]");
     System.out.println("testString is a required parameter.");
  } else {
    String testString = argv[0];
    for (int i = 1; i<10000; i++)
    {
       String encryptedString = EncryptDecrypt.encryptStringUsingTripleDes(testString);        
       String decryptedString = EncryptDecrypt.decryptStringUsingTripleDes(encryptedString);
    }
    String encryptedString = EncryptDecrypt.encryptStringUsingTripleDes(testString);

    System.out.println("Here is your string encrypted: "+ encryptedString);

    String decryptedString = EncryptDecrypt.decryptStringUsingTripleDes(encryptedString);
    
    System.out.println("Here is your string decrypted: "+decryptedString);
  }
    
 }
}
