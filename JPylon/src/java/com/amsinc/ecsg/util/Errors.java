package com.amsinc.ecsg.util;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
  
/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class Errors implements ErrorHandler, java.io.Serializable {
        public Errors() {}
        
        public void warning(SAXParseException ex) {
            System.err.println(ex);
        }
        public void error(SAXParseException ex) {
            System.err.println(ex);
        }
        public void fatalError(SAXParseException ex) throws SAXException {
            System.err.println(ex);
        }
    }