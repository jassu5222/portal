package com.amsinc.ecsg.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsServlet;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ExecutionTimer
{
	private static final Logger LOG = LoggerFactory.getLogger(ExecutionTimer.class);
    // Properties
    double computeTime = 0;
    long startTime = 0;
    long endTime = 0;
    
    public void setStart(){
        this.startTime = System.currentTimeMillis();
    }
    
    public void setEnd(){
        this.endTime = System.currentTimeMillis();
    }
    
    public double getTime(){
        computeTime = endTime - startTime;
        computeTime = computeTime/1000;
        LOG.debug("Processing Time in Seconds: {}" , computeTime);
        // Reset the Start Time and End Time for reuse
        startTime = 0;
        endTime = 0;
        // Return the Total Time
        return computeTime;
    }
}
  
    