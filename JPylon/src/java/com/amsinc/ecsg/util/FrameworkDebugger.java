/*
 * @(#)FrameworkDebugger
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.util;

import com.amsinc.ecsg.frame.Logger;

/**
 * The FrameworkDebugger class offers a way to log debug statements during
 * development and production. The debug mode can be used to turn on or off the
 * actual writing of the debug statements to a file or to system out. Normally,
 * in production, the debug mode is set to off. If a problem occurs, the debug
 * mode can be set to either output target to help determine what the problem
 * might be. The debug modes are:<br>
 * 0 - Debug Off, 1 - Debug to system out, 2 - Debug to log file.
 */
public class FrameworkDebugger implements java.io.Serializable
{
   protected int debugMode = 0;

   /**
    * Write a debug message for the given debug mode. The debug modes are:<br>
    * 0 - Debug Off, 1 - Debug to system out, 2 - Debug to log file
    *
    * @param category Debugging category the debug message is for.
    * @param debugMode debug mode for message
    * @param debugMessage Message to log

    */
   public void debug (String category, int debugMode, String debugMessage)
   {
      if (debugMode == 0)
         return;
      if (debugMode == 1)
      {
         debugSystemOut (debugMessage);
      }
      else
      {
         debugFile (category, debugMessage);
      }
   }

   /**
    * Write a debug message for the overall debug mode currently set by
    * the FrameworkDebugger class.
    *
    * @param category Debugging category the debug message is for.
    * @param debugMessage Message to log

    */
   public void debug (String category, String debugMessage)
   {
      debug (category, debugMode, debugMessage);
   }

   /**
    * Writes the debug message to system out. This typically results in the
    * message being written to the screen.
    *
    * @param debugMessage The debug message to be written to system out.
    */
   protected void debugSystemOut (String debugMessage)
   {
      System.out.println (debugMessage);
      System.out.flush();
   }

   /**
    * Writes the debug message to a file. This may or may not actually result
    * in the message being written to a file, depending on how the logging
    * server is configured.
    *
    * @param category Debugging category the debug message is for. This is
    * used to determine which file the log message is written to.
    * @param debugMessage The debug message to be written to system out.
    */
   protected void debugFile (String category, String debugMessage)
   {
      Logger.log (category, "default", debugMessage, Logger.DEBUG_LOG_SEVERITY);
   }

   /**
    * This method sets the overall debug mode currently set by
    * the FrameworkDebugger class.  The debug modes are:<br>
    * 0 - Debug Off, 1 - Debug to system out, 2 - Debug to log file
    *
    * @param debugMode debug mode for message

    */
   public void setDebugMode (int debugMode)
   {
      this.debugMode = debugMode;
   }
}
