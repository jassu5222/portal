package com.amsinc.ecsg.util;

import java.util.*;

/**
 *    Parses an XML file that contains key/value pairs.  Returns a
 *    hashtable containing the key / value mapping.
 *
 *    Sample file format:
 * 
 *    <settings>
 *       <setting>
 *          <key>maxUsers</key>
 *          <value>42</value>
 *       </setting>
 *    </settings>         
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class KeyValueXmlParser
 {

     /**
      * Parses an XML key-value file into a Map. 
      *
      * @param path - the full path to the XML file being read
      * @return Map - Map containing mapping of key/value pairs
      */
     public static Map<String, String> parse(String path)
      {
         Map<String, String> settings = new HashMap<>(10);

         // Read in the file using DocHandler
         DocumentHandler xml = new DocumentHandler(path);

         // Get a list of the settings
         List<DocumentHandler> enumer = xml.getComponent("/").getFragmentsList("/setting");
   
         for (DocumentHandler settingXml: enumer)
          {

              // Extract the key and value
              String key = settingXml.getAttribute("/key");
              String value = settingXml.getAttribute("/value");

              // Put them in the hashtable
              settings.put(key, value);           
          }

         return settings;
      }
 }