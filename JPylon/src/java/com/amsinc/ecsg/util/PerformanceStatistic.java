package com.amsinc.ecsg.util;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;

/**
 *  This class keeps performance data related to the actions that 
 *  are supported by the jPylon framework: link actions and form
 *  submissions.  
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerformanceStatistic extends PerformanceTimer implements java.io.Serializable
 { 
	private static final Logger LOG = LoggerFactory.getLogger(PerformanceStatistic.class);
    // Indicates whether this timer represents a form submission
    // or just a link
    private boolean isFormSubmit = false;
    
    // Types of actions
    public static final String SUBMIT_ACTION = "SUBMIT";
    public static final String LINK_ACTION = "LINK";
        
    // The action that this timer represents
    // For links, this is the logical page to which the user is being sent
    // For submits, this is the name of the logical form name that defines the form
    // being submitted
    private String action = "";
    
    // Additional information about the action that the timer represents
    // For links, this may be set in an application, but is never set by jPylon
    // For submits, this is the mediator that is called
    private String context = "";
    
    // The organization to which the user completing the action belongs
    private String organization = "";

    // The action type
    private String actionType = "";

    public void setActionType (String actionType){
    	this.actionType = actionType;
    }
    public void setTimerStart(Date timerStart){
    	this.timerStart = timerStart;
    }

    public void setTimerStop(Date timerStop){
    	this.timerStop = timerStop;
    }

    /**
	 * @return the navId
	 */
	public String getNavId() {
		return navId;
	}

	/**
	 * @param navId the navId to set
	 */
	public void setNavId(String navId) {
		this.navId = navId;
	}

	/**
	 * @return the userOid
	 */
	public String getUserOid() {
		return userOid;
	}

	/**
	 * @param userOid the userOid to set
	 */
	public void setUserOid(String userOid) {
		this.userOid = userOid;
	}

	/**
	 * @return the timeType
	 */
	public String getTimeType() {
		return timeType;
	}

	/**
	 * @param timeType the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * @return the fromPage
	 */
	public String getFromPage() {
		return fromPage;
	}

	/**
	 * @param fromPage the fromPage to set
	 */
	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	/**
	 * @return the toPage
	 */
	public String getToPage() {
		return toPage;
	}

	/**
	 * @param toPage the toPage to set
	 */
	public void setToPage(String toPage) {
		this.toPage = toPage;
	}

	/**
	 * @return the aObjectOid
	 */
	public String getaObjectOid() {
		return aObjectOid;
	}

	/**
	 * @param aObjectOid the aObjectOid to set
	 */
	public void setaObjectOid(String aObjectOid) {
		this.aObjectOid = aObjectOid;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	public void setTimerStopGMTTimeStamp(Date timerStopGMTTimeStamp){
		this.timerStopGMTTimeStamp = timerStopGMTTimeStamp;
	}
	
	// The navId of the page being accessed
    private String navId = "";

    // The useroid of the logged in user
    private String userOid = "";

    // The time type being logged - will always be SERVER
    private String timeType = "";

    // The page on which action started 
    private String fromPage = "";

    // The response page which is shown  
    private String toPage = "";

    // The aObjectOid of the transaction opened
    private String aObjectOid = "";

    // The serverName of the logged in user
    private String serverName = "";

    /**
     * Returns true if this statistic represents a submit action
     *
     * @return true if this statistic represents a submit action
     */
    public boolean isSubmitAction()
     {
        return isFormSubmit;
     }

    /**
     * Returns true if this statistic represents a link action
     *
     * @return true if this statistic represents a link action
     */   
    public boolean isLinkAction()
     {
        return !isFormSubmit;
     }    

    /**
     * Setter for action type
     */
    public void setAsLinkAction()
     {
        isFormSubmit = false;
     }

    /**
     * Setter for action type
     */
    public void setAsSubmitAction()
     {
        isFormSubmit = true;
     }

     /**
      * @return the action
      */
     public String getAction() {
         return action;
     }

     /**
     * Sets the action value for this performance statistic.
     *
     * @param action - String - the action value for this performance statistic
     */
    public void setAction(String action)
     {
        this.action = action;
     }

     /**
      * @return the context
      */
     public String getContext() {
         return context;
     }

     /**
     * Sets the context value for this performance statistic.
     *
     * @param context - String - the context value for this performance statistic
     */    
    public void setContext(String context)
     {
         this.context = context;            
     }     
        
    /**
     * Sets the organization code value for this performance statistic.
     *
     * @param organizationCode - String - the organization code value for this performance statistic
     */    
    public void setOrganizationCode(String organizationCode)
     {
         this.organization = organizationCode;          
     }       

    /**
     * Determines whether or not this performance statistic will be saved
     * to the database.  This allows for filtering of statistic data
     *
     * @return true if this object will be added to the cache to be saved
     */
    protected boolean shouldAddToCache()
     { 
        if(!isFormSubmit && !PerformanceStatisticFilter.isLinkActionMonitored(action, context))
         {
             return false;              
         } 
            
        if(isFormSubmit && !PerformanceStatisticFilter.isSubmitActionMonitored(action, context))
         {               
             return false;
         }              
        
        return true;
     }
     
    /**
     * Creates the insert statement used to save this performance statistic.
     *
     * @return SQL to insert this performance statistic into the database
     */
    protected String createInsertStatement(List<Object> sqlParams)
     {
        StringBuilder sql = new StringBuilder();
        
        
        
            
        sql.append("insert into PERFORMANCE_STAT (TIMESTAMP, SYSTEM_NAME, ELAPSED_TIME, ACTION_TYPE, ACTION, CONTEXT, CLIENT_BANK, UOID, NAVID, USER_OID, TIME_TYPE, FROM_PAGE, TO_PAGE, A_OBJECT_OID, SERVER_NAME) values ");
        sql.append("(to_date(?, 'MM/DD/YYYY HH24:MI:SS'), ?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        
        
        sqlParams.add(DateTimeUtility.convertDateToDateTimeString(timerStopGMTTimeStamp));
        sqlParams.add(PerformanceStatisticFilter.getSystemName());
        sqlParams.add(this.getElapsedTime());
        
        if(isFormSubmit)
        	sqlParams.add(("".equals(actionType) ? SUBMIT_ACTION : actionType));
        else
        	sqlParams.add(("".equals(actionType) ? LINK_ACTION : actionType));
        
        sqlParams.add(action);
        sqlParams.add(context);
        sqlParams.add(organization);
        
        // Generate an object ID to use as the UOID column 
	// This is required since OTL objects are being used to access this table
        long uoidColumnValue;
        try
         {
            uoidColumnValue = ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID();
         }
        catch(AmsException amse)
         {
            uoidColumnValue = 0;
         }
        sqlParams.add(uoidColumnValue);
        sqlParams.add(this.getNavId());
        sqlParams.add(this.getUserOid());
        
        sqlParams.add(this.getTimeType());
        sqlParams.add(this.getFromPage());
        sqlParams.add(this.getToPage());
        sqlParams.add(this.getaObjectOid());
        sqlParams.add(this.getServerName());

        return sql.toString();
     }

    /**
     * Purges data that has been in the performance_stat table for a certain 
     * period of time.
     *
     */
    public void purge() 
     {
        // Get the number of days that data should be around before being deleted
        int statisticLifetime = PerformanceStatisticFilter.getStatisticLifetime();

        // Only purge if a valid purge period has been set
        if(statisticLifetime > 0)
         {
                // Create the SQL to purge everything older than a certain number of days
	        String str = "delete from performance_stat where timestamp < (sysdate - ? )";
	
	        try
		 {
		    // Run the SQL to purge
		    DatabaseQueryBean.executeUpdate(str, false, statisticLifetime);
		 }
	        catch(Exception e)
		 {
		     LOG.error("Exception caught while purging performance statistic data",e);   
		 }   
         }
     }
        
    /**
     * Creates a textual representation of this object's data.
     * Used for debugging.
     *
     * @return string textual representation of this object
     */    
    public String toString()
     {
        StringBuilder str = new StringBuilder();
            
        str.append("Timer Start: "+timerStart+"\n");
        str.append("Timer Stop: "+timerStop+"\n");
           
        str.append("Elapsed Time: "+getElapsedTime()+"\n");
                      
        if(isFormSubmit)
         {
            str.append("Action Type: "+SUBMIT_ACTION+"\n");
         }
        else
         {
            str.append("Action Type: "+LINK_ACTION+"\n");               
         }    
           
        str.append("Action: "+ this.action+"\n");
        str.append("Context: "+ this.context+"\n");              
        str.append("Organization: "+this.organization+"\n");
        str.append("System Name: "+PerformanceStatisticFilter.getSystemName()+"\n");
           
        return str.toString();
     }                 
}
