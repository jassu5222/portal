package com.amsinc.ecsg.util;

import java.util.*;

/**
 *  This class deals with filtering the performance statistic 
 *  collection process.   
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerformanceStatisticFilter
{
   // Stores a list of actions to be monitored
   private static TreeSet monitoredLinkActions;    
   private static TreeSet monitoredSubmitActions;
   
   // The system name to be stored for each performance statistic - read from file
   private static String systemName;
   
   // The last time that data was read from the configuration XML file
   private static long xmlLastUpdateTime;

   // The number of days to keep a performance statistic before purging
   private static int statisticLifetime = -1;
   
   // Delimiter to be used when constructing string to store in list
   private static final String DELIMITER = "!@#";

   static
   {   
      loadFilterData();
   }
   
   /** 
    * Populates the list of actions to monitor and the system name
    * from the XML file that is cached in the PerformanceTimerCache.
    *
    * This method is called either in the static initializer or
    * when it is detected that the data cached in this class is stale.
    *
    */
   private static synchronized void loadFilterData()
    {
        monitoredLinkActions = new TreeSet();
        monitoredSubmitActions = new TreeSet();

        // Get the XML file from the cache and update the timestamp      
        DocumentHandler configXml = PerformanceTimerCache.getConfigXml();
        xmlLastUpdateTime = System.currentTimeMillis();

        // Read the system name
        systemName = configXml.getAttribute("/PerformanceStatistic/SystemName");

        if(systemName == null)
           systemName = "";

        // Read the statistic lifetime - how many days before purge
        String statisticLifetimeString = configXml.getAttribute("/PerformanceStatistic/StatisticLifetime");

        if(statisticLifetimeString == null)
           statisticLifetime = -1;
        else
           statisticLifetime = Integer.parseInt(statisticLifetimeString);
            
        // Get list of things to monitor from the XML
        Vector linksToMonitor = configXml.getComponent("/PerformanceStatistic").getFragments("/Link");
        Vector submitsToMonitor = configXml.getComponent("/PerformanceStatistic").getFragments("/Submit");
      
        // Store that list in memory
        populateActionList(monitoredLinkActions, linksToMonitor);
        populateActionList(monitoredSubmitActions, submitsToMonitor);      
    }
   
   /** 
    * Populates the in-memory storage of the list of actions to monitor.
    *
    * @param listToPopulate - the object representing the list to populate
    * @param listFromXml - the XML representation of the list (from file)
    */
   private static void populateActionList(TreeSet listToPopulate, Vector listFromXml)
   {
      Enumeration enumer = listFromXml.elements();
      
      // Loop through each of the items in the XML
      while(enumer.hasMoreElements())
      {
           DocumentHandler element = (DocumentHandler) enumer.nextElement();
           
           String action = element.getAttribute("/Action");
           String context = element.getAttribute("/Context");
       
           // Each action is stored in the list as the action + DELIMITER + context
           // Add the empty string if action or context is null    
           String actionToAdd = "";
           if(action != null)
               actionToAdd += action;
           
           actionToAdd += DELIMITER;
           
           if(context != null)
               actionToAdd += context;
           
           // Add to the list in memory
           listToPopulate.add(actionToAdd);
      }       
   }
  
   /** 
    * Determines if the configuration file is requesting that an action and
    * context combination be monitored.  The cached list of items to be 
    * monitored is stored as action+DELIMITER+context.  This makes it quick 
    * to look up.   The logic in this method also deals with the fact that
    * sometimes we only want to monitor on an action or context, but not
    * both.
    *
    * @param action - the action 
    * @param context - the context
    */
   public static boolean isLinkActionMonitored(String action, String context)
   {      
       // If data is stale, load the filter data again
       if(PerformanceTimerCache.getXmlLastUpdateTime() > xmlLastUpdateTime)
           loadFilterData();
       
       // Check to see if the combination passed in is present in the list
       return monitoredLinkActions.contains(action+DELIMITER+context) ||
              monitoredLinkActions.contains(action+DELIMITER) ||
              monitoredLinkActions.contains(DELIMITER+context);
   }
   
   /** 
    * Determines if the configuration file is requesting that an action and
    * context combination be monitored.  The cached list of items to be 
    * monitored is stored as action+DELIMITER+context.  This makes it quick 
    * to look up.   The logic in this method also deals with the fact that
    * sometimes we only want to monitor on an action or context, but not
    * both.
    *
    * @param action - the action 
    * @param context - the context
    */
   public static boolean isSubmitActionMonitored(String action, String context)
   {
       // If data is stale, load the filter data again
       if(PerformanceTimerCache.getXmlLastUpdateTime() > xmlLastUpdateTime)
           loadFilterData();       
       
       // Check to see if the combination passed in is present in the list
       return monitoredSubmitActions.contains(action+DELIMITER+context) ||
              monitoredSubmitActions.contains(action+DELIMITER) ||
              monitoredSubmitActions.contains(DELIMITER+context);       
   }
   
   /** 
    * Returns the system name that was read from the XML file.
    *
    */  
   public static String getSystemName()
   {
      return systemName;   
   }

   /** 
    * Returns the number of days before purging a performance statistic from the database
    *
    */  
   public static int getStatisticLifetime()
   {
      return statisticLifetime;   
   }
}
