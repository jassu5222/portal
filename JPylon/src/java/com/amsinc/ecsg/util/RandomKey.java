package com.amsinc.ecsg.util;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.math.*;
 
/**
 * Used to generate keys.
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
public class RandomKey    
 {
    // Random number generator
    private static SecureRandom prngRandom;
    
    static {
            // Random number generator
            prngRandom = new SecureRandom();        
    }
    
    /**
     * Generates a random array of bytes using a SHA1PRNG algorithm.
     * 
     * @param size the number of bits that the random array will contain (this 
     *        number is divided by 8 first to determine the number of bytes
     * @return array of random bytes
     *
     */
    public static byte[] getRandomBytes(int size)
      {
        // Create the seed.  Make sure the length of seed > 0
	    byte[] seed = prngRandom.generateSeed(size/8 + 1);

	    prngRandom.setSeed(seed);

        // Create an array of bytes to hold the number of bits required
        // Divide num. of bits by 8 to get the number of bytes
        byte[] result =	new byte[size / 8];

        // Generate the random number
	    prngRandom.nextBytes(result);        
        
        return result;
      }
    
 }