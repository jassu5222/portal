package com.amsinc.ecsg.util;

/**
 * @(#)ResourceManager.java
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.XmlConfigFileCache;

/**
 * The ResourceManager contains mechanisms for retrieving and formatting data based on user specific preferences. It uses the
 * ClientServerDataBridge (CSDB) object to store user specific preferences and contains methods which utilize the CSDB to determine
 * string, number and date formatting. Currently, the only user specific preference maintained by the CSDB is locale. Therefore, the
 * implemented formatting routines depend solely on locale.
 */
public class ResourceManager implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(ResourceManager.class);
	/**
	 * The csdb holds user specific preferences for the current session, including locale.
	 */
	private ClientServerDataBridge csdb;

	/**
	 * Indicates if MissingResource Exception logging is enabled or disabled By default it is enabled.
	 */
	private static boolean logError = true; // RKAZI Rel 91. T36000033243 10/24/2014 - Add

	private static String defaultLocaleName;
	/**
	 * The hashtable contains the list of supported locales where the key is a string representation of a locale and the value is
	 * the actual locale object. This hashtable is populated using the keys and values found in SupportedLocales.xml.
	 */
	private static Hashtable htSupportedLocalesByString = new Hashtable();
	/**
	 * The hashtable contains the list of supported locales where the key is a locale object and the value is a string
	 * representation of a locale. This hashtable is populated using the keys and values found in SupportedLocales.xml.
	 */
	private static Hashtable htSupportedLocalesByLocale = new Hashtable();
	/**
	 * This hashtable is keyed by the logical name of the locale. Its data is the Oracle Sorting Linguistic Definition appropriate
	 * for this locale. The sorting linguistic definition ensures that sorting is done properly for the locale's language. Populated
	 * from SupportedLocales.xml (this value is optional).
	 */
	private static Hashtable htSupportedLocalesSortingLinguisticDef = new Hashtable();

	static {
		try {

			loadSupportedLocales();
			// RKAZI Rel 91. T36000033243 10/24/2014 - Start
			String logMissingResourceError = JPylonProperties.getInstance().getString("logMissingResourceError");

			if ("N".equals(logMissingResourceError)) {
				logError = false;
			}
			// RKAZI Rel 91. T36000033243 10/24/2014 - End
		} catch (Exception e) {
			LOG.error("Error initializing ResourceManager ", e);
		}
	}

	/**
	 * Default constructor for ResourceManager. This method instantiates each of the instance variables.
	 *
	 */
	public ResourceManager() {
		this.csdb = new ClientServerDataBridge();
		this.setResourceLocale(defaultLocaleName);
	}

	/**
	 * Constructor for ResourceManager. This method instantiates each of the instance variables.
	 *
	 */
	public ResourceManager(ClientServerDataBridge csdb) {
		if (csdb != null) {
			this.csdb = csdb;
			if (csdb.getLocaleName() == null)
				this.setResourceLocale(defaultLocaleName);
		} else {
			this.csdb = new ClientServerDataBridge();
			this.setResourceLocale(defaultLocaleName);
		}
	}

	/**
	 * Getter for csdb
	 *
	 * @return ClientServerDataBridge
	 */
	public ClientServerDataBridge getCSDB() {
		return csdb;
	}

	/**
	 * This method returns a NumberFormat object suitable for formatting currencies based on the locale found in the CSDB.
	 *
	 * @return NumberFormat
	 */
	public NumberFormat getCurrencyFormatter() {
		NumberFormat format = NumberFormat.getCurrencyInstance(getResourceLocaleValue());
		return format;
	}

	/**
	 * This method transforms a date in framework format into new format based on the dateFormatType and the locale found in the
	 * CSDB.
	 *
	 * @param frameworkDate
	 *            A date in framework format represented as a string
	 * @param dateFormatType
	 *            An integer representing the desired date formatting style (ex. SHORT = M/d/yy in en_US locale)
	 * @return A date localized and converted to the desired formatting style
	 */
	public String getFormattedDate(String frameworkDate, int dateFormatType) {
		Date tempDate = new Date();
		try {
			tempDate = DateTimeUtility.convertStringDateToDate(frameworkDate);
		} catch (AmsException amsex) {
		}
		DateFormat format = DateFormat.getDateInstance(dateFormatType, getResourceLocaleValue());
		return format.format(tempDate);
	}

	/**
	 * This method transforms a date in ISO format into new format based on the dateFormatType and the locale found in the CSDB.
	 *
	 * @param isoDate
	 *            A date in ISO (YYYY-MM-DD) format represented as a string
	 * @param dateFormatType
	 *            An integer representing the desired date formatting style (ex. SHORT = M/d/yy in en_US locale)
	 * @return A date localized and converted to the desired formatting style
	 */
	public String getFormattedDateFromISO(String isoDate, int dateFormatType) {
		Date tempDate = new Date();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			tempDate = formatter.parse(isoDate);
		} catch (Exception e) {
		}
		DateFormat format = DateFormat.getDateInstance(dateFormatType, getResourceLocaleValue());
		return format.format(tempDate);
	}

	/**
	 * This method transforms a date/time in framework format into new format based on the dateFormatType/timeFormatType and the
	 * locale found in the CSDB.
	 *
	 * @param frameworkDateTime
	 *            A date/time in framework format represented as a string
	 * @param dateFormatType
	 *            An integer representing the desired date formatting style (ex. SHORT = M/d/yy in en_US locale)
	 * @param timeformatType
	 *            An integer representing the desired time formatting style (ex. SHORT = h:mm a in en_US locale)
	 * @return A date/time localized and converted to the desired formatting style
	 */
	public String getFormattedDateTime(String frameworkDateTime, int dateFormatType, int timeFormatType) {
		Date tempDateTime = new Date();
		try {
			tempDateTime = DateTimeUtility.convertStringDateTimeToDate(frameworkDateTime);
		} catch (AmsException amsex) {
		}
		DateFormat format = DateFormat.getDateTimeInstance(dateFormatType, timeFormatType, getResourceLocaleValue());
		return format.format(tempDateTime);
	}

	/**
	 * This method returns a NumberFormat object suitable for formatting decimal numbers based on the locale found in the CSDB.
	 *
	 * @return NumberFormat
	 */
	public NumberFormat getNumberFormatter() {
		NumberFormat format = NumberFormat.getNumberInstance(getResourceLocaleValue());
		return format;
	}

	/**
	 * This method returns a NumberFormat object suitable for formatting percentages based on the locale found in the CSDB.
	 *
	 * @return NumberFormat
	 */
	public NumberFormat getPercentFormatter() {
		NumberFormat format = NumberFormat.getPercentInstance(getResourceLocaleValue());
		return format;
	}

	/**
	 * This method returns a PropertyResourceBundle based on the passed logicalResourceName and the locale found in the CSDB.
	 *
	 * @param logicalResourceName
	 *            Name of the resource we're looking for
	 */
	public PropertyResourceBundle getResource(String logicalResourceName) {
		return (getResource(logicalResourceName, getResourceLocaleValue()));
	}

	public PropertyResourceBundle getResource(String logicalResourceName, Locale localeName) {
		return (PropertyResourceBundle) PropertyResourceBundle.getBundle(logicalResourceName, localeName);
	}

	/**
	 * This method gets the locale name from the CSDB
	 *
	 * @return Logical name of the chosen locale
	 */
	public String getResourceLocale() {
		return csdb.getLocaleName();
	}

	/**
	 * This method gets the locale object from the CSDB
	 *
	 * @return Locale name cooresponding to the logical locale stored in the CSDB
	 */
	public Locale getResourceLocaleValue() {
		if (csdb.getLocaleName() != null) {
			return (Locale) htSupportedLocalesByString.get(csdb.getLocaleName());
		} else {
			return null;
		}
	}

	/**
	 * This static method gets the Locale for a logical name
	 *
	 * @param String
	 *            - the logical locale name
	 * @return Locale name cooresponding to the logical locale passed in
	 */
	public static Locale getLocaleByLogicalName(String logicalName) {
		return (Locale) htSupportedLocalesByString.get(logicalName);
	}

	/**
	 * Looks up in a given resource bundle for the specified key. The value for that key is returned. If the key doesn't exist in
	 * the bundle an error is printed to the console and the key is returned.
	 * 
	 * @return java.lang.String - The translated value from the bundle
	 * @param key
	 *            java.lang.String - The key to lookup in the bundle
	 * @param bundle
	 *            java.lang.String - The bundle name to use
	 */
	public String getText(String key, String bundle) {
		return getText(key, bundle, null);
	}

	/**
	 * Looks up in a given resource bundle for the specified key. The value for that key is returned. If the key doesn't exist in
	 * the bundle an error is printed to the console and the key is returned.
	 * 
	 * @return java.lang.String - The translated value from the bundle
	 * @param key
	 *            java.lang.String - The key to lookup in the bundle
	 * @param bundle
	 *            java.lang.String - The bundle name to use
	 * @param localeName
	 *            java.lang.String - logical name of locale.
	 */
	public String getText(String key, String bundle, String localeName) {
		String translation = getTextIncludingEmptyString(key, bundle, localeName);
		if (StringFunction.isBlank(translation)) {
			translation = key;
		}

		return translation;
	}

	/**
	 * @param key
	 * @param bundle
	 * @return
	 */
	public String getTextIncludingEmptyString(String key, String bundle) {
		return getTextIncludingEmptyString(key, bundle, null);
	}

	/**
	 * @param key
	 * @param bundle
	 * @param localeName
	 * @return
	 */
	public String getTextIncludingEmptyString(String key, String bundle, String localeName) {
		String translation = null;

		try {
			if (StringFunction.isBlank(localeName)) {
				translation = this.getResource(bundle).getString(key);
			} else {
				translation = this.getResource(bundle, getLocaleByLogicalName(localeName)).getString(key);
			}

		} catch (MissingResourceException e) {
			if (logError) {
				LOG.trace("Missing Resource Exception with error:  " + e.toString());
			}
			translation = key;
		}

		return translation;

	}

	/**
	 * Looks up in a given resource bundle for the specified key. The value for that key is returned. If the key doesn't exist in
	 * the bundle an error is printed to the console and the key is returned.
	 * 
	 * @return java.lang.String - The translated value from the bundle
	 * @param key
	 *            java.lang.String - The key to lookup in the bundle
	 * @param bundle
	 *            java.lang.String - The bundle name to use
	 */
	public String getTextEscapedJS(String key, String bundle) {
		String translation = getText(key, bundle);

		if (translation != null) {
			translation = StringService.change(translation, "'", "\\'");
			translation = StringService.change(translation, "\"", "\\\"");
		}

		return translation;

	}

	/**
	 * This method loads each of the supported locales from the SupportedLocales.xml file. The supported locales are loaded into two
	 * hashtables. The default locale specified in the XML file is used to "default" the locale in the CSDB.
	 *
	 * @return void
	 */
	public static synchronized void loadSupportedLocales() {

		try {
			DocumentHandler doc = XmlConfigFileCache.readConfigFile("/SupportedLocales.xml");
			String defaultLocale = doc.getAttribute("/defaultLocale");
			if (defaultLocale == null) {
				LOG.warn("Locale Info: NO DEFAULT LOCALE SPECIFIED in SupportedLocales.xml ");
			}
			List<DocumentHandler> localeDocs = doc.getFragmentsList("/LocaleName");
			for (DocumentHandler locDoc : localeDocs) {
				String logicalName = locDoc.getAttribute("/logicalName");
				String language = locDoc.getAttribute("/language");
				String country = locDoc.getAttribute("/country");
				String variant = locDoc.getAttribute("/variant");
				String sortingLinguisticDef = locDoc.getAttribute("/oracleSortingLinguisticDefinition");

				if (logicalName == null) {
					LOG.debug("Encountered an error. No logical name specified. Ignoring this entry.");
				} else {
					if (language == null) {
						LOG.debug("Encountered an error.  No logical name specified.  Ignoring this entry.");
					} else {
						if (country == null) {
							country = "";
						}
						if (variant == null) {
							variant = "";
						}
						Locale tempLocale = new Locale(language, country, variant);
						htSupportedLocalesByString.put(logicalName, tempLocale);
						htSupportedLocalesByLocale.put(tempLocale, logicalName);

						if ((sortingLinguisticDef != null) && !sortingLinguisticDef.isEmpty()) {
							htSupportedLocalesSortingLinguisticDef.put(logicalName, sortingLinguisticDef);
						}
					}
				}
			}
			defaultLocaleName = defaultLocale;
		} catch (Exception e) {
			LOG.error("Error occured loading supported locales", e);
		}
	}

	/**
	 * This method localizes a currency in string format according to the locale found in the CSDB.
	 *
	 * @param numberString
	 *            A decimal number represented as a string to be localized
	 * @return A currency represented as a string after being localized
	 */
	public String localizeNumberStringAsCurrencyString(String numberString) {
		double numberValue = Double.parseDouble(numberString);
		return getCurrencyFormatter().format(numberValue);
	}

	/**
	 * This method localizes a decimal number in string format according to the locale found in the CSDB.
	 *
	 * @param numberString
	 *            A decimal number represented as a string to be localized
	 * @return A decimal number represented as a string after being localized
	 */
	public String localizeNumberStringAsNumberString(String numberString) {
		double numberValue = Double.parseDouble(numberString);
		return getNumberFormatter().format(numberValue);
	}

	/**
	 * This method localizes a percentage in string format according to the locale found in the CSDB.
	 *
	 * @param numberString
	 *            A decimal number represented as a string to be localized
	 * @return A percentage represented as a string after being localized
	 */
	public String localizeNumberStringAsPercentString(String numberString) {
		double numberValue = Double.parseDouble(numberString);
		return getPercentFormatter().format(numberValue);
	}

	/**
	 * Setter for csdb
	 *
	 * @param csdb
	 * @return void
	 */
	public void setCSDB(ClientServerDataBridge csdb) {
		this.csdb = new ClientServerDataBridge(csdb);
	}

	/**
	 * This method sets the locale in the CSDB based on the user's browser settings. It loops through the list of user's locales and
	 * compares each locale to the hash of supported locales If a locale match is made, the locale in the CSDB is set.
	 *
	 * @param req
	 *            The user's preferred locales are found in the servlet request is found
	 * @return void
	 */
	public void setLocaleWithClientBrowserSettings(HttpServletRequest req) {
		Locale currentLocale = null;
		String localeNames = req.getHeader("Accept-Language");
		String language = null;
		String country = null;
		String variant = null;
		String delimiterString = null;
		String tokenString = null;
		StringTokenizer localeString = null;
		StringTokenizer languageHeader = new StringTokenizer(localeNames, ",;", true);

		while (languageHeader.hasMoreTokens()) {
			tokenString = languageHeader.nextToken();
			localeString = new StringTokenizer(tokenString, "-");
			language = localeString.nextToken().toLowerCase();
			if (localeString.hasMoreTokens()) {
				country = localeString.nextToken().toUpperCase();
				if (localeString.hasMoreTokens()) {
					variant = localeString.nextToken();
					currentLocale = new Locale(language, country, variant);
				} else {
					currentLocale = new Locale(language, country);
				}
			} else {
				currentLocale = new Locale(language, "");
			}
			String localeNameString = (String) htSupportedLocalesByLocale.get(currentLocale);
			if (localeNameString != null) {
				setResourceLocale(localeNameString);
				break;
			}
			if (languageHeader.hasMoreTokens()) {
				delimiterString = languageHeader.nextToken();
				while ((delimiterString.equals(";")) && languageHeader.hasMoreTokens()) {
					String trashString = languageHeader.nextToken();
					if (languageHeader.hasMoreTokens()) {
						delimiterString = languageHeader.nextToken();
					}
				}
			}
		}
	}

	/**
	 * This method sets the locale name in the CSDB to the passed value
	 *
	 * @param resourceLocale
	 *            Logical name of the chosen locale
	 * @return void
	 */
	public void setResourceLocale(String resourceLocale) {
		Locale loc = (Locale) htSupportedLocalesByString.get(resourceLocale);
		if (loc != null) {
			csdb.setLocaleName(resourceLocale);
		} else {
			LOG.info("Encountered an error: Locale name '{}' is name not valid; can't set in csdb.", resourceLocale);
		}
	}

	/**
	 * Adds localization settings (if appropriate) to an order by clause. The linguistic definition is passed to Oracle by using the
	 * Oracle nlssort SQL function. For example, to sort based on the 'name' column based on spanish sorting, the order by clause
	 * must appear as follows: order by nlssort(name, 'nls_sort=XSpanish')
	 *
	 * This method accepts the column to be sorted on as a parameter and returns a string to represent that column in the order by
	 * clause. If no linguistic definition exists for the user's locale, the column name is returned unchanged. Otherwise, the
	 * column name is wrapped by a call to Oracle's nlssort.
	 *
	 * @param column
	 *            Logical name of the chosen locale
	 * @return String - column name wrapped by call to Oracle's nlssort function if linguistic definition exists
	 */

	public String localizeOrderBy(String column) {
		// Determine the linguistic defintion for the current locale
		String linguisticDefinition = (String) htSupportedLocalesSortingLinguisticDef.get(getResourceLocale());

		if (linguisticDefinition == null) {
			// No linguistic definition defined for this locale
			// Don't specify anything to Oracle about locale... use Oracle's default of English sorting
			return column;
		} else {
			// Wrap the column name with a call to Oracle's nlssort function
			// and pass in linguistic definition
			StringBuilder localized = new StringBuilder();
			localized.append("nlssort(");
			localized.append(column);
			localized.append(", 'NLS_SORT=");
			localized.append(linguisticDefinition);
			localized.append("')");

			return localized.toString();
		}
	}

	public static Hashtable getSupportedLocales() {
		return htSupportedLocalesByString;
	}

}
