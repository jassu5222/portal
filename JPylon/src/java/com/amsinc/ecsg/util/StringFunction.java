/*
 * @(#)StringFunction
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
package com.amsinc.ecsg.util;

import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amsinc.ecsg.frame.AmsException;

public final class StringFunction
{
	/**
	 * The regular expression pattern "(&#)(\\d+)(;)" defines 3 groups: "$#",
	 * "\\d+", and ";".  The second group is an integer that specifies the
	 * code point of the Unicode character that the numeric character
	 * reference represents.
	 */
	private static final Pattern NUMERIC_CHAR_REF = Pattern.compile("(&#)(\\d+)(;)");
	private static final Map<String,String> HTML_TO_CHAR = getHTMLToCharMap();
	private static final Map<String,String> CHAR_TO_HTML = getCharToHTMLMap();
	public static final Pattern VALID_DATETIME_CHARS = Pattern.compile("^[0-9-\\/:\\s\\.]+$");//example value: 2004-03-05 00:00:00.0

	public static String subString(String str, char startChar, char endChar){
        //  str             <String>
        //  startStr        <String>
        //  endStr          <String>
        //  Retrieves a substring from "str" where the start char is startChar
        //  and end char is endChar
        //  e.g.  StringFunction.subString("(hellow)there", '(',')') returns "hello".

        int startIndex = str.indexOf(startChar);
	    int endIndex = str.indexOf(endChar);
	    String subStr = "";
	    if(startIndex+1 > endIndex){
	        return subStr;
	    }
	    subStr = str.substring(startIndex+1, endIndex);
	    return subStr;
    }

    public static List<String> separateStringIntoList(String inputStr, String separator){
        //  inputString     <String>
        //  separator       <String>
        //  ^               <List with: <String>>
        //  Parse the input String (inputStr) and separate the string using the separator "/" and put all
        //  the tokens into a vector that gets returned.
        //  e.g.  StringFunction.separateStringIntoVector("/parse/this/string", "/")
        //  return a Vector containing the following strings: parse, this, string

        StringTokenizer parser = new StringTokenizer(inputStr, separator);
        List<String> inputList = new ArrayList<>();
        while (parser.hasMoreTokens()){
            inputList.add(parser.nextToken());
        }
        return inputList;
    }

    public static String[] separateStringIntoArray(String inputStr, String separator){
        //  inputString     <String>
        //  separator       <String>
        //  ^               <Array with: <String>>
        //  Parse the input String (inputStr) and separate the string using the separator "/" and put all
        //  the tokens into an array that gets returned.
        //  e.g.  StringFunction.separateStringIntoArray("/parse/this/string", "/")
        //  return an array containing the following strings: parse, this, string

        List<String> inputList = separateStringIntoList(inputStr, separator);

        String[] stringArray = new String[inputList.size()];
        inputList.toArray(stringArray);

        return stringArray;
    }

    /**
     * This method determines if component processing is necessary for attribute path
     * string suppled.
     *
     * @param attributePath String the un-processed attribute path string
     * @return True if the name needs component processing, false if not
     */
    public static boolean requiresComponentParsing(String attributePath){
        return StringService.occurrencesOf(attributePath, "/", 0) > 0;
    }

    /**
     * This method processes the attribute path string and returns the
     * first objectname-objectid(if applicable) in a single string.
     *
     * @param attributePath String the un-processed attribute path string
     * @return The componentName/ID string
     */
    public static String getFirstComponentEntry(String attributePath){
        String[] entries = separateStringIntoArray(attributePath, "/");
        return entries[0];
    }

    /**
     * This method processes the attribute path string and returns the
     * attribute name element.
     *
     * The attribute name is always the final element within an
     * attributePath string.
     *
     * @param attributePath String the un-processed attribute path string
     * @return The attribute specified in the attributePath string
     */
    public static String getComponentAttributeName(String attributePath){
        String[] entries = separateStringIntoArray(attributePath, "/");
        return entries[entries.length - 1];
    }

    /**
     * This method removes the first objectname-objectid path specification
     * from the attributePath string.
     *
     * @param attributePath String the un-processed attribute path string
     * @return The attributePath without the first objectname-objectid path
     */
    public static String removeFirstComponentEntry(String attributePath){
        if(!requiresComponentParsing(attributePath)){
            return "";
        }
        String firstComponentEntry = getFirstComponentEntry(attributePath);
        // If there is only one more component name to parse, remove only the
        // "/" prefix from the path
        if(StringService.occurrencesOf(attributePath,"/",0) != 2){
            return StringService.change(attributePath,"/"+firstComponentEntry,"");
        }else{
            // Remove both the "/" prefix and suffix from the component Path
            return StringService.change(attributePath,"/"+firstComponentEntry+"/","");
        }
    }

    /**
     * This method extracts the objectID from an objectname-objectid path
     * specification.  The method returns the objectID as a long dataType.
     *
     * @param attributePath An objectName/objectID component path string
     * @return long The objectID that was extracted from the path string
     */
    public static long parseIDFromComponentPath(String componentPath) throws AmsException{
        String idString = subString(componentPath,'(',')');
        if(!StringService.isDigit(idString)){
            throw new AmsException("Unable to parse ID from component path: "+componentPath);
        }
        return Long.parseLong(subString(componentPath, '(', ')'));
    }

    /**
     * This method extracts the component object name from an objectname-objectid
     * path specification.  The method returns the componentName as a String.
     *
     * @param attributePath An objectName/objectID component path string
     * @return String The componentName that was extracted from the path string
     */
    public static String parseNameFromComponentPath(String componentPath){
        String componentName;
        // If an object identifier exists, extract the name before the ID

        if(componentPath.indexOf('(') >= 0){
            int index = componentPath.indexOf('(');
            componentName = StringService.remove(componentPath, index, componentPath.length()-index);
        }else{
            // No object identifier exists, simply extract the first component name
            componentName = getFirstComponentEntry(componentPath);
        }
        return componentName;
    }

    /**
     * This method indicates as to if a componentPath string is a valid
     * component path.
     *
     * @param componentPath A component path string
     * @return boolean True if the componentPath is valid, false if it is invalid
     */
    public static boolean validateComponentPath(String componentPath){
        return componentPath.indexOf("//") < 0;
    }

    /**
     * This method wraps (prepends/appends) a given string with a specified
     * string.
     *
     * @param String sourceString The string that is to be wrapped
     * @param String wrapString The string that is prepened/appened to the source string
     * @return String The wrapped String
     */
    public static String wrapString(String sourceString, String wrapString){
        StringBuilder buffer = new StringBuilder(wrapString);
        buffer.append(sourceString);
        buffer.append(wrapString);
        return buffer.toString();
    }

    
  
     /**
      * This methods escapes single quotes and double quotes in given string.
      * This method should be used when string is used in JavaScript functions.
     * @param str
     * @return
     */
    public static String escapeQuotesforJS(String str) 
    {
    	if (isBlank(str)) return "";

    	int len = str.length();
    	StringBuilder output = new StringBuilder(len);

    	for (int i = 0; i < len; i++) {
    		char c = str.charAt(i);
    		switch (c) {
    		case '\'':
    			output.append("\\u0027");
    			break;
    		case '"':
    			output.append("\\u0022");
    			break;
    		case '\\':
    			output.append("\\u005C");
    			break;
    		case '\n':
    			output.append("\\u000A");
    			break;
    		case '\r':
    			output.append("\\u000D");
    			break;

    		default:
    			output.append(c);
    			break;
    		}
    	}
    	
    	str = output.toString();
    	str = StringService.change(str, "&#34;", "\\&#34;");
   		str = StringService.change(str, "&#39;", "\\&#39;");
    	
    	return str;

    }
    


     /**
    * Returns true if the given value is null or a blank string
    *
    * @return boolean - true: string is blank or null
    * @param value java.lang.String - value to test
    */
    public static boolean isBlank(String value) {
        if (value == null) return true;
		
        return value.trim().length() < 1;
    }


     /**
    * Returns true if the given value is NOT null or a blank string
    *
    * @return boolean - string is non blank
    * @param value java.lang.String - value to test
    */
    public static boolean isNotBlank(String value) {
        return !isBlank(value);
    }
    
    
    /**
     * [PPX-050] This method will convert '&#60;', '&#62;', '&#38;', '&#34;', '&#39;', '&#40;',
     * and '&#41;' to their literal equivalents ('<', '>', '&', '"', ''', '(', and ')') to
     * defend against Cross Site Scripting (XSS) attacks.
     *
     * @param value HTML encoded value
     * @return HTML decoded value
     */
    public static String xssHtmlToChars(String value) {
    	if (value == null) return null; 


 		for (String charAsHtml : HTML_TO_CHAR.keySet()) {
 			String charAsLiteral = HTML_TO_CHAR.get(charAsHtml);
 			while (value.indexOf(charAsHtml) != -1) {
 				value = value.substring(0, value.indexOf(charAsHtml))
 						+ charAsLiteral
 						+ value.substring(value.indexOf(charAsHtml)+charAsHtml.length());
 			}
 		}
 		return value;
 	}

	private static Map<String,String> getHTMLToCharMap() {
		Map<String,String> map = new HashMap<>();
		map.put("&#60;", "<");
		map.put("&#62;", ">");
		map.put("&#38;", "&");
		map.put("&#34;", "\"");
		map.put("&#39;", "'");
		map.put("&#40;", "(");
		map.put("&#41;", ")");
		map.put("&#47;", "/");  
		return map;
	}

	/**
     * [PPX-050] This method will convert '<', '>', '&', '"', ''', '(', and ')'
     * to their HTML encoded equivalents.  This is to defend against Cross Site
     * Scripting (XSS) attacks.
     *
     * @param originalValue HTML decoded value
     * @return HTML encoded value
     */
	public static String xssCharsToHtml(String originalValue) {
	
		return xssCharsToHtml(originalValue,false);
	}
	
	public static String xssCharsToHtml(String originalValue, Boolean ignoreForwardSlash) {
    	if (originalValue == null) return null; 


		StringBuilder encodedValue = new StringBuilder();
		for(int i=0; i<originalValue.length(); i++) {
			String oneCharString = originalValue.substring(i, i+1);
			//ignore forward slash for date fields
			if(ignoreForwardSlash && "/".equals(oneCharString)){
				continue;
			}
			if(oneCharString.equals("&")
					&& (i+4 < originalValue.length())
					&& (CHAR_TO_HTML.containsValue(originalValue.substring(i, i + 5)))) {
				encodedValue.append(originalValue.substring(i, i+5));
				i = i+4;
			}
            // W Zhu 4/30/09 RAUJ043049008 do not convert the "&" in "&#" in order to accomodate
            // legitimate numerical representation of unicode, such as &#23494;&#30721;
            else if (oneCharString.equals("&")
                    && (i+1 < originalValue.length())
                    && ("#".equals(originalValue.substring(i+1, i+2)))) {
                encodedValue.append(oneCharString);
            }
	    else if (oneCharString.equals("&")
                    && (i+5 < originalValue.length())
                    && ("&nbsp;".equals(originalValue.substring(i, i+6)))) {
                encodedValue.append(oneCharString);
            }
			else if(CHAR_TO_HTML.containsKey(oneCharString)) {
				encodedValue.append(CHAR_TO_HTML.get(oneCharString));
			}
			else {
				encodedValue.append(oneCharString);
			}
		} 

		return encodedValue.toString();
	}

	private static Map<String,String> getCharToHTMLMap() {
		Map<String,String> map = new HashMap<>();

		map.put("<", "&#60;");
		map.put(">", "&#62;");
		map.put("&", "&#38;");
		map.put("\"", "&#34;");
		map.put("'", "&#39;");
		map.put("(", "&#40;");
		map.put(")", "&#41;");
	    map.put("/", "&#47;"); 

		return map;
	}


	/**
     * transform unicode string to Oracle SQL's unistr() escape string.
     *
     * @param value
     * @return unistr string
     */
    public static String toUnistr(String value) {
        char[] buf = value.toCharArray();
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
            if (buf[i] <= '\u007f') {
                out.append(buf[i]);
            }
            else {
                out.append('\\');
                String hex = Integer.toHexString(buf[i]);
                for (int j = 0; j < 4 - hex.length(); j++) {
                    out.append('0');
                }
                out.append(hex);
            }
        }
        return out.toString();
    }

    /**
     * Transforms numeric character references of the form &#999; to
     * the unicode characters they represent.
     * 
     * @param value
     * @return
     */
    public static String asciiToUnicode(String value) {
    	if(isBlank(value)) 
    		return "";
    	Matcher matcher = NUMERIC_CHAR_REF.matcher(value);
    	StringBuffer output = new StringBuffer();

    	while (matcher.find()) {
    		// The regular expression pattern "(&#)(\\d+)(;)" defines 3 
    		// groups: "$#", "\\d+", and ";".  We need to filter out the first
    		// and third groups to leave only the second group, which is an
    		// integer that specifies the code point of the Unicode character
    		// that the numeric character reference represents.
    		// The Matcher.group method uses a 1-based index (group zero
    		// denotes the entire pattern).
    		int codePoint = Integer.parseInt(matcher.group(2));
    		matcher.appendReplacement(output,
    				new String(Character.toChars(codePoint)));
    	}
    	matcher.appendTail(output);

    	return output.toString();
    }

    /*
	 *  checks to see if given string can be encoded in "windows-1252"
	 */
	public static boolean canEncodeIn_WIN1252(String str) {
		return canEncodeString(str, "windows-1252");
	}

	/*
	 *  This methods returns true/false if given string can be successfully encoded in given encoding.
	 */
	public static boolean canEncodeString(String str, String encode) {
		boolean isSuccess = true;
		try {

			Charset.forName(encode).newEncoder().encode(CharBuffer.wrap(str.toCharArray()));
		}
		catch (CharacterCodingException e) {
			isSuccess = false;
		}

		return isSuccess;
	}

	/*
	 *  This methods produces SQL string from the source which can be used for IN, NOT IN Clause. For example = ('A', 'B', 'C')
	 */
	public static String toSQLString(Collection c) {
		StringBuilder _SQLString = new StringBuilder();
		int counter = 0;
		if (c.size() > 0) {
			for (Iterator itr = c.iterator(); itr.hasNext();) {
				if (counter > 0)
					_SQLString.append(",");
				_SQLString.append("'").append((String) itr.next()).append("'");
				counter++;
			}
		}

		_SQLString.insert(0, "(");
		_SQLString.insert(_SQLString.length(), ")");
		return _SQLString.toString();
	}
	
	
	
	/**
	 * This method pads given number of spaces on right side
	 * @param str
	 * @param size
	 * @return
	 */
	public static String padRight(String str, int size){
		return String.format("%1$-" + size + "s", str);
	}
	
	
	
	/**
	 * This method pads given number of spaces on left side
	 * @param str
	 * @param num
	 * @return
	 */
	public static String padLeft(String str, int size){
		return String.format("%1$#" + size + "s", str);
	}
	
	
	
	/**
	 * This method duplicates passed in string given number of times
	 * @param str
	 * @param nTimes
	 * @return
	 */
	public static String repeat(String str, int nTimes) {
	    if (str == null) return str;
	    
	    StringBuilder sb = new StringBuilder(str);
	    for (int i = 1 ; i < nTimes ; i ++) {
	      sb.append(str);
	    }
	    return sb.toString();
	  
	}
	
	/**
	 * this method returns Map keys into String format.
	 * @param map
	 * @return Map keys in String
	 */
	public static String getMapKeyToStringFormat(Map map){
		String panelApprovalsList = "";
		for(Object obj : map.keySet()){
			panelApprovalsList = panelApprovalsList + obj;
		}
		return 	panelApprovalsList;	
	}
	
	/**RP
	 * This method will validate the file path
	 * @param path
	 * @return path or null
	 */
	
	public static String validateFilePath(String filePath){
		String regex = ".*[.][\\]\\[!\"\\#$%&'()*+,/:;<=>?@\\^`{|}~].*" ;   
		if (! filePath.matches(regex)) {  
			   
		} else {  
			filePath = null;
		}
		return filePath;
	}
	
	
	public static String validatedDateOutputString(String dateString){
		Matcher matcher = VALID_DATETIME_CHARS.matcher(dateString);            	
		  if(matcher.matches()){
			  return dateString;	//For Date, ignore forward slash;  
		  }else{
			  return xssCharsToHtml(dateString);
		  }
			
	}
}