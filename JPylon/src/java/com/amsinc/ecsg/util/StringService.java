package com.amsinc.ecsg.util;

/**
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public final class StringService implements java.io.Serializable
{

   //cquinton 5/24/2011 Rel 7.0.0 - do not instantiate StringService
   private StringService() {
   }

   /**
    * Returns the number of occurrences of the specified string.
    * You can specify that case should be ignored.
    *
    * @param      s         the string to be inspected
    * @param      c         the string to search for
    * @param      start     the starting position for the search
    *
    * @return     the number  of occurrences of the specified string.
    *             If the string is not found -1 is returned.
    *
    */
   public static   int occurrencesOf(String s, String c, int start)
   {
      int lc = c.length();
      int i;
      int n = 0;

      if (start > s.length()-lc) {
         return 0;
      }

      if (lc > 0) {
          while ( (i = s.indexOf(c, start)) >= 0 )
          {
             n++;
             start = i+lc;
          }
      }
      return n;
   }


      /**
   * Changes occurrences of a specified pattern to a replacement string.
   * The maximum number of changes and a starting position can be specified.
   * You can specify that case should be ignored.
   *
   * @param      s          the input string
   * @param      oldp       the pattern to be replaced
   * @param      newp       the replacement string
   *
   * @return        a string with the specified patterns changed.
   */
   public static    String change(String s, String oldp, String newp)
   {
      int start=0;
      int maxChanges = Integer.MAX_VALUE;
      int lo = oldp.length();
      int ln = newp.length();
      int ls = s.length();
      int newlen = ls;
      int lastPos;
      int lastDest;
      int fpos;
      int nchg = 0;

      if (lo == 0 || start >= ls) {
         return s;
      }


      lastPos = start;

      if (ln > lo) {
          int n = occurrencesOf(s, oldp, start);
         newlen = ls+n*(ln-lo);
      }

      char[] buf = new char[newlen];

      s.getChars(0, start, buf, 0);
      lastDest = start;

      while ( (fpos = s.indexOf(oldp, lastPos)) >= 0 && nchg++ < maxChanges)
      {
         s.getChars(lastPos, fpos, buf, lastDest);
         lastDest += fpos-lastPos;
         newp.getChars(0, ln, buf, lastDest);
         lastPos = fpos+lo;
         lastDest += ln;
      }

      s.getChars(lastPos, ls, buf, lastDest);

      return new String(buf, 0, lastDest-lastPos+ls);
   }

     /**
   * Returns true if the string consists of digits.
   * @param      s          the input string
   *
   * @return     true if the string consists only of digits.
   *
   */
   public static  boolean isDigit(String s)
   {
      int ls = s.length();

      for (int i = 0; i < ls; i++)
      {
         if (!Character.isDigit( s.charAt(i) )) {
            return false;
         }
      }

      return true;
   }

     /**
   * Removes the specified number of characters from the string, starting
   * at the specified position.
   *
   * @param      s         the original string
   * @param      start     the starting position
   * @param      numChars  the number of characters to be removed
   *
   * @return     a string with the specified number of characters removed.
   */
   public static  String remove(String s, int start, int numChars)
   {
      int ls = s.length();

      if (start >= ls || start < 0 || numChars < 1) {
         return s;
      }

      if (start > ls-numChars) {
         numChars = ls-start;
      }

      int newlen = ls-numChars;

      char[] buf = new char[newlen];

      s.getChars(0, start, buf, 0);
      s.getChars(start+numChars, ls, buf, start);

      return new String(buf);
   }

 // T36000041885 RKAZI REL 9.3 07/22/2015 - Start
   /**
   *
   *
   * @param sqlParam - the string to be filtered
   * @return the filtered string
   */
   public static String filterSQLParam(String sqlParam)
   {
       if (sqlParam==null) return null;
      // If string contains the tick character ('), change it to double tick ('') so
      // that Oracle will interpet it as a single tick

      // Do an indexOf first so that the code doesn't go into the change() method
      // unless it needs to
      if( sqlParam.indexOf('\'') >= 0)
       {
          sqlParam = StringService.change(sqlParam, "'", "''");
       }

      // If string contains the escape sequence for the tick character, %27, change
      // it to double tick ('')  so that Oracle will interpet it as a single tick

      // Do an indexOf first so that the code doesn't go into the change() method
      // unless it is likely to need to do so
      if( sqlParam.indexOf('%') >= 0)
       {
          sqlParam = StringService.change(sqlParam, "%27", "''");
       }

      return sqlParam;
   }
// T36000041885 RKAZI REL 9.3 07/22/2015 - END
}
