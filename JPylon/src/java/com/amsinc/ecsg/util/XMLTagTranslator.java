/**
 * This is an abstract "call-back" class used in BusinessObject.populateFromXmlDoc.  
 * It should be subclassed with translateTag() implemented.
 *
 *     Copyright  � 2008
 *     CGI, Incorporated
 *     All rights reserved
 */

package com.amsinc.ecsg.util;

public abstract class XMLTagTranslator {

    protected String attributeName;
    protected String attributeValue;

    /**
     * When populating a business object with XML file, an implementation of this
     * method translates the tag name and tag value in XML file to appropriate 
     * attribute/component name and value.
     * 
     * The method should set the instance variable attributeName and attributeValue.
     * If attributeName is left as null, this node will be ignored.  Keep in mind one
     * instance of this object is used throughout the processing of the whole XML file,
     * so attributeName and attributeValue would contain  
     * 
     * @param tagName
     * @param tagValue
     */
    public abstract void translateTag(String tagName, String tagValue) ;
    
    public String getAttributeName(){
        return attributeName;
    }
    
    public String getAttributeValue() {
        return attributeValue;
    }

}
