package com.amsinc.ecsg.web;

/*
 * @(#)AmsListWebBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.util.DocumentHandler;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

/**
 * AmsListWebBean manages lists of web beans on the web server. It has methods to prepare list queries to fill the list, both
 * generic lists (lists of objects) and list views (handwritten SQL queries). Once populated, the list web bean maintains the list,
 * handles notifications of updates, provides access to web beans by their id values. Most commonly, a list web bean is registered
 * with the BeanManager, the list is prepared using one of the given methods, and the getObjects method is called to iterate through
 * the list so that each item can be displayed using some HTML in the Java Server Page.
 *
 */
public class AmsListWebBean extends AmsWebBean {
	private static final Logger LOG = LoggerFactory.getLogger(AmsListWebBean.class);

	/**
	 * The collection of objects (web beans) in the list.
	 */
	protected Hashtable<String, AmsEntityWebBean> htObjectList;
	/**
	 * This collection is used to preserve the knowledge of the order of items in the list.
	 */
	protected Vector vObjectList;
	/**
	 * The class object for the web beans in the list. We store this so it is efficient to create new instances of the list items.
	 */
	private Class beanClass;
	/**
	 * The object identifier field name for the web beans in the list.
	 */
	private String oidFieldName;
	/**
	 * The name of the object to fill the list from in generic list queries. Normally this is the same as the name of the web beans
	 * being stored in the list, but it can be different if the prepareList method is invoked which specifies a different object
	 * name.
	 */
	private String queryObjectName;
	/**
	 * The name of the query to be executed to populate the list. This could be either the name of a generic list type or the name
	 * of the EJB list view component.
	 */
	private String queryName;
	/**
	 * A collection of arguments to be sent to either the generic list query or the list view query.
	 */
	private List<String> queryParameters;
	/**
	 * An indicator of whether the query to execute is a view list query. If false, the query is a generic list query.
	 */
	private boolean bUseView;
	/**
	 * An indicator to determine whether to wrap the arguments to a query with tic marks
	 */
	private boolean bWrapArguments;
	/**
	 * The name of the list component this list corresponds to. This is the logical name that the list was registered under. This
	 * will be applicable only to list web beans.
	 */
	private String ecsgListName;

	public AmsListWebBean() {
		super();
		htObjectList = new Hashtable();
		vObjectList = new Vector();
		beanClass = null;
		queryName = null;
		queryParameters = new ArrayList<>();
		bUseView = false;
		bWrapArguments = true;
	}

	/**
	 * This method gets the data from the application server using the query bean.
	 *
	 */
	public void getDataFromAppServer() {
		QueryBean qbean = (QueryBean) getBeanManager().getBean("QueryBean");
		DocumentHandler doc;
		if (isUseView()) {
			doc = qbean.runViewQuery(queryName, queryParameters);
		} else {
			qbean.setWrapArguments(bWrapArguments);
			doc = qbean.runQuery(queryObjectName, queryName, queryParameters);
		}
		populateFromXmlDoc(doc, "/ResultSetRecord/");
		// Set data valid flag back to true
	}

	/**
	 * This method sets the object name and also does some preparation so the list object can create new instances of the class
	 * later on.
	 *
	 */
	@Override
	public void setEcsgObjectName(String ecsgObjectName) {
		this.ecsgObjectName = ecsgObjectName;
		String className = getBeanManager().getClassName(ecsgObjectName);
		try {
			beanClass = Class.forName(className);
			AmsEntityWebBean tempBean = (AmsEntityWebBean) beanClass.newInstance();
			oidFieldName = tempBean.getOidFieldName();
			queryObjectName = ecsgObjectName;
		} catch (Exception e) {
			LOG.error("Exceptoin in setEcsgObjectName()",e);
		}
		// Erase the collection of objects
		htObjectList.clear();
		vObjectList.removeAllElements();
	}

	/**
	 * Prepares the listing object so that all instances of the specified object will be available to the listing object.
	 *
	 * This is the most basic form of list preparation. No search criteria or special list processing will be performed on the list
	 * retrieval using this list preperation method.
	 *
	 */
	public void prepareList() {
		queryName = null;
		queryParameters = new ArrayList<>();
		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(false);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object.
	 *
	 * This form of list prepartaion allows some special processing, without runtime criteria to occur during list retrieval. Based
	 * on the listtype name provided, the business object will be interrogated as to what objects will appear in the list.
	 *
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 */
	public void prepareList(String listTypeName) {
		queryName = listTypeName;
		queryParameters = new ArrayList<>();
		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(false);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object based on a listtype and criteria parameters provided.
	 *
	 * This form of list prepartaion allows the most specialization. Using both a specified list name and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 * @param String[]
	 *            An array of parameter values using for list processing.
	 */
	public void prepareList(String listTypeName, String[] listParameters) {
		queryName = listTypeName;
		queryParameters = new ArrayList<>();
		queryParameters.addAll(Arrays.asList(listParameters));

		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(false);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object based on a listtype and criteria parameters provided.
	 *
	 * This form of list prepartaion allows the most specialization. Using both a specified list name and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepareList(String listTypeName, String parm1) {
		String parms[] = new String[1];
		parms[0] = parm1;
		prepareList(listTypeName, parms);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object based on a listtype and criteria parameters provided.
	 *
	 * This form of list preparation allows the most specialization. Using both a specified list name and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepareList(String listTypeName, String parm1, String parm2) {
		String parms[] = new String[2];
		parms[0] = parm1;
		parms[1] = parm2;
		prepareList(listTypeName, parms);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object based on a listtype and criteria parameters provided. NOTE: This method should be used when the business object to be
	 * put into the list is different than the name of the registered list items.
	 *
	 * This form of list prepartaion allows the most specialization. Using both a specified list name and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the business object to populate the list
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepareListForAbstractWebBean(String queryObjectName, String listTypeName, String parm1, String parm2) {
		this.queryObjectName = queryObjectName;
		prepareList(listTypeName, parm1, parm2);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object based on a listtype and criteria parameters provided. NOTE: This method should be used when the business object to be
	 * put into the list is different than the name of the registered list items.
	 *
	 * This form of list prepartaion allows the most specialization. Using both a specified list name and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the business object to populate the list
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepareListForAbstractWebBean(String queryObjectName, String listTypeName, String parm1) {
		this.queryObjectName = queryObjectName;
		prepareList(listTypeName, parm1);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified object will be available to the listing
	 * object. NOTE: This method should be used when the business object to be put into the list is different than the name of the
	 * registered list items.
	 *
	 * This form of list prepartaion allows some special processing, without runtime criteria to occur during list retrieval. Based
	 * on the listtype name provided, the business object will be interrogated as to what objects will appear in the list.
	 *
	 * @param String
	 *            The name of the business object to populate the list
	 * @param String
	 *            The name of the listType that will be used for list processing.
	 */
	public void prepareListForAbstractWebBean(String queryObjectName, String listTypeName) {
		this.queryObjectName = queryObjectName;
		prepareList(listTypeName);
	}

	/**
	 * Prepares the listing object so that all instances of the specified object will be available to the listing object. NOTE: This
	 * method should be used when the business object to be put into the list is different than the name of the registered list
	 * items.
	 *
	 * This is the most basic form of list preparation. No search criteria or special list processing will be performed on the list
	 * retrieval using this list preperation method.
	 *
	 * @param String
	 *            The name of the business object to populate the list
	 */
	public void prepareListForAbstractWebBean(String queryObjectName) {
		this.queryObjectName = queryObjectName;
		queryParameters = new ArrayList<>();
		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(false);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified view will be available to the listing
	 * object.
	 *
	 *
	 * @param String
	 *            The name of the view (EJB List View component) that will be used for list processing.
	 */
	public void prepareView(String viewName) {
		queryName = viewName;
		queryParameters = new ArrayList<>();
		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(true);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified view will be available to the listing
	 * object based on the parameters provided.
	 *
	 * This form of list preparation allows the most specialization. Using both a specified list view and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the view (EJB List View component) that will be used for list processing.
	 * @param String[]
	 *            An array of parameter values using for list processing.
	 */
	public void prepareView(String viewName, String[] listParameters) {
		queryName = viewName;
		queryParameters = new ArrayList<>();
		queryParameters.addAll(Arrays.asList(listParameters));

		htObjectList.clear();
		vObjectList.removeAllElements();
		setUseView(true);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified view will be available to the listing
	 * object based on the parameters provided.
	 *
	 * This form of list preparation allows the most specialization. Using both a specified list view and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the view (EJB List View component) that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepareView(String viewName, String parm1) {
		String parms[] = new String[1];
		parms[0] = parm1;
		prepareView(viewName, parms);
	}

	/**
	 * Prepares the listing object so that a specified number of instances of the specified view will be available to the listing
	 * object based on the parameters provided.
	 *
	 * This form of list preparation allows the most specialization. Using both a specified list view and an array of supplied
	 * criteria parameters, the instances of objects made available to this list will be determined at runtime.
	 *
	 * @param String
	 *            The name of the view (EJB List View component) that will be used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 * @param String
	 *            A parameter value used for list processing.
	 */
	public void prepare(String viewName, String parm1, String parm2) {
		String parms[] = new String[2];
		parms[0] = parm1;
		parms[1] = parm2;
		prepareView(viewName, parms);
	}

	/**
	 * This method returns the list of objects as an enumeration.
	 *
	 * @return Enumeration
	 */
	public Enumeration getObjectEnumeration() {
		return vObjectList.elements();
	}

	/**
	 * This method returns the list of objects as a vector.
	 *
	 * @return Vector
	 */
	public Vector getObjects() {
		return vObjectList;
	}

	/**
	 * This method takes an object identifier and returns the appropriate bean.
	 *
	 * @return void
	 */
	public AmsEntityWebBean getById(String oid) {
		AmsEntityWebBean wb = htObjectList.get(oid);
		return wb;
	}

	/**
	 * This method returns the count of objects in the list.
	 *
	 * @return int
	 */
	public int getObjectCount() {
		return htObjectList.size();
	}

	/**
	 * This method returns the type of object being stored in the list.
	 *
	 * @return String
	 */
	public String getObjectType() {
		return queryObjectName;
	}

	/**
	 * This method takes an XML document and puts all of the objects in this list bean into the XML document. into the document.
	 *
	 * @param DocumentHandler
	 *            The XML document object
	 */
	@Override
	public void populateXmlDoc(DocumentHandler doc) {
		populateXmlDoc(doc, "");
	}

	/**
	 * This method takes a document and puts all of the objects in this list into the document starting at the specified component
	 * path. If a '/' character terminates the component path, that will be the absolute path name used, otherwise, the eCSG object
	 * name will be appended.
	 *
	 * @param DocumentHandler
	 *            The XML document object
	 * @param String
	 *            The path in the XML document to put the data
	 */
	@Override
	public void populateXmlDoc(DocumentHandler doc, String componentPath) {
		componentPath = createXmlPath(componentPath);
		MessageFormat msgFormat = new MessageFormat(componentPath.substring(0, componentPath.length() - 1) + "({0})/");
		int count = 0;
		Object[] args = { "" };
		Enumeration enumer = this.getObjectEnumeration();
		while (enumer.hasMoreElements()) {
			AmsEntityWebBean wb = (AmsEntityWebBean) enumer.nextElement();
			args[0] = count++;
			wb.populateXmlDoc(doc, msgFormat.format(args));
		}
	}

	/**
	 * This method takes an XML document and returns a vector of the web beans from the values in the document starting at the root
	 * level of the document.
	 *
	 * @return boolean true if object was found in document, otherwise false
	 */
	@Override
	public boolean populateFromXmlDoc(DocumentHandler doc) {
		return (populateFromXmlDoc(doc, ""));
	}

	/**
	 * This method takes an XML document and returns a vector of the web beans from the values in the document starting at the
	 * specified component path. If a '/' character terminates the component path, that will be the absolute path name used,
	 * otherwise, the eCSG object name will be appended.
	 *
	 * @return boolean true if object was found in document, otherwise false
	 */
	@Override
	public boolean populateFromXmlDoc(DocumentHandler doc, String componentPath) {
		boolean bDataFound = true;
		componentPath = createXmlPath(componentPath);
		LOG.debug("AmsListWebBean In populateFromXmlDoc - looking at {}", componentPath);

		List<DocumentHandler> docList = doc.getFragmentsList(componentPath);
		int size = docList.size();
		Vector populatedOids;
		if (size < 1) {
			bDataFound = false;
			populatedOids = new Vector(1);
		} else {
			populatedOids = new Vector(size);
		}
		for (DocumentHandler tempDoc : docList) {
			try {
				AmsEntityWebBean bean = null;
				String oid = null;
				if (oidFieldName != null) {
					oid = tempDoc.getAttribute(oidFieldName);
				}
				if (oid != null) {
					bean = htObjectList.get(oid);
				}
				if (bean == null) {
					bean = (AmsEntityWebBean) beanClass.newInstance();
					bean.init(beanMgr);
					bean.setServerLocation(this.getServerLocation());
					bean.populateFromXmlDoc(tempDoc, "/");
					bean.setEcsgObjectName(queryObjectName);
					htObjectList.put(bean.getOid(), bean);
					vObjectList.addElement(bean);
					populatedOids.addElement(oid);
				} else {
					bean.populateFromXmlDoc(tempDoc, "/");
					populatedOids.addElement(oid);
				}
			} catch (Exception e) {
				LOG.error("Exceptoin in populateFromXmlDoc()",e);
			}
		}
		// Remove any items from the list that were not populated from this document
		// We waited to do this so that we could reuse the existing instances in the list
		// rather than wiping out the whole list and having to instantiate a whole new
		// list of objects
		Enumeration keyList = htObjectList.keys();
		List<String> removeList = new ArrayList<>();
		while (keyList.hasMoreElements()) {
			String key = (String) keyList.nextElement();
			if (!(populatedOids.contains(key))) {
				removeList.add(key);
			}
		}
		for (String key : removeList) {
			AmsEntityWebBean wb = htObjectList.get(key);
			htObjectList.remove(key);
			vObjectList.removeElement(wb);
		}

		return bDataFound;
	}

	/**
	 * This method returns the indicator stating whether the query to be run is a list view. If false, the query is a generic list.
	 *
	 * @return boolean
	 */
	public boolean isUseView() {
		return bUseView;
	}

	/**
	 * This method sets the indicator stating whether the query to be run is a list view. If false, the query is a generic list.
	 *
	 * @param boolean
	 *            The indicator stating whether the query is a list view
	 */
	public void setUseView(boolean bUseView) {
		this.bUseView = bUseView;
	}

	/**
	 * This method sets the wrap arguments indicator.
	 *
	 *
	 * @param boolean
	 *            the wrap arguments indicator
	 */
	public void setWrapArguments(boolean bWrapArguments) {
		this.bWrapArguments = bWrapArguments;
	}

	/**
	 * This method creates the XML path location for the object in a document based on if the user specified a location, if the path
	 * ends with a '/', and if the object name should be added on.
	 *
	 * @param String
	 *            A predefined xml path location for the object
	 * @return boolean
	 */
	protected String createXmlPath(String componentPath) {
		if (!(componentPath.endsWith("/"))) {
			StringBuilder addlPath = new StringBuilder(componentPath);
			addlPath.append('/');
			addlPath.append(getEcsgListName());
			addlPath.append('/');
			componentPath = addlPath.toString();
		}
		return (componentPath);
	}

	/**
	 * This method adds a web bean to the list.
	 *
	 * @param AmsEntityWebBean
	 *            a web bean
	 */
	public void addBean(AmsEntityWebBean webBean) {
		htObjectList.put(webBean.getOid(), webBean);
		vObjectList.addElement(webBean);
	}

	/**
	 * This method removes a web bean from the list.
	 *
	 * @param String
	 *            id
	 */
	public void removeBean(String id) {
		AmsEntityWebBean wb = htObjectList.get(id);
		htObjectList.remove(id);
		vObjectList.removeElement(wb);
	}

	/**
	 * This method returns the name of this list component. This name should be the same as the component list object of the
	 * corresponding business object, if there is one.
	 *
	 * @return String
	 */
	@Override
	public String getEcsgListName() {
		return ecsgListName;
	}

	/**
	 * This method sets the location of the object in the output XML document. This will only be set if the location will be
	 * different than the ecsgObjectName. This will also be used for list web beans if the items in this list correspond to a list
	 * component of a parent business object. This name should be the same as that parent business object. This name is used to look
	 * for items in an XML document if it has a value.
	 *
	 * @param String
	 *            the location of the object in the output XML document
	 */
	@Override
	public void setEcsgListName(String ecsgListName) {
		this.ecsgListName = ecsgListName;
	}

}
