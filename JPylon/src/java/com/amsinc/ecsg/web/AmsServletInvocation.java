package com.amsinc.ecsg.web;

/*
 * @(#)ServletInvocation
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.frame.AmsException;
import java.util.Hashtable;
import java.util.Vector;

/**
 * The AmsServletInvocation object stores parameters for a specific invocation
 * of AmsServlet. These parameters are stored on the Bean Manager for security,
 * rather than being hidden input parameters on HTML forms. The Bean Manager
 * is particular to each user's session and is typically available from every
 * Java Server Page, so it is a good place to store this information. All of
 * the properties of this object are parameters to AmsServlet.
 */
public class AmsServletInvocation implements java.io.Serializable
{

    /**
     * The transaction to invoke.  The name of the Mediator to call.
     */
    private String mediator;
    /**
     * Additional input configuration files to use to look for input HTTP
     * parameters.
     */
    private String mapFiles;
    /**
     * A complete URL of an XSL style sheet to use to apply to the output data
     */
    private String xslStyleSheet;
    /**
     * A complete URL of an XSL style sheet to use to apply to the error data
     * if the transaction fails
     */
    private String xslErrorSheet;
    /**
     * The URL (can be relative) to redirect to if the transaction is successful
     */
    private String nextPage;
    /**
     * The URL (can be relative) to redirect to if the transaction fails
     */
    private String errorPage;
    /**
     * An indicator of whether the input data should be added to the existing
     * data in the document cache. By default, the input data is wiped clean
     * for the start of a new transaction. This is sometimes used on multi-part
     * forms to accumulate data in the XML document until it is ready to be
     * sent to the application service
     */
    private boolean addToInputCache;
    /**
     * A list of web beans (seperated by a '/') that should be put into the
     * input XML document for the transaction.
     */
    private String beansAsInput;
    /**
     * A list of document cache names (seperated by a '/') that should be put
     * into the input XML document for the transaction.
     */
    private String docCacheAsInput;
    /**
     * A list of web beans (seperated by a '/') that should be populated either
     * from the input data (if call EJB is false) or from the output XML data
     * (if call EJB is true)
     */
    private String beansToPopulate;
    /**
     * An indicator of whether there are multiple object instances to look for
     * in the input data.
     */
    private boolean multipleObjects;
    /**
     * The number of multiple object occurrences to look for.
     */
    private int numberOfMultipleObjects;
    //Vshah - 10/19/2010 - IR#VIUK101252424 - [BEGIN]
    /**
     * The number of multiple object occurrences (Part 1) to look for.
     */
    private int numberOfMultipleObjects1;
    //Vshah - 10/19/2010 - IR#VIUK101252424 - [END]
  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
    /**
     * The number of multiple object occurrences (Part 2) to look for.
     */
    private int numberOfMultipleObjects2;
    //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
    
    /**
     * Kyriba CR 268 - The number of multiple object occurrences in External Bank Section.
     */
    private int numberOfMultipleObjects3;
    
    /**
     * //Rel9.0 - numberOfMultipleObjects4 for Address fields
     */
    private int numberOfMultipleObjects4;
    /**
     * The name of the XML document to use for this transaction.  By default,
     * it is 'default.doc'
     */
    private String docCacheName;
    /**
     * A hashtable of additional parameters, usually application specific, to
     * send to the AmsServlet call.
     */
    private Hashtable addlParms;
    /**
     * The name of WebAction modules to execute before the mediator is invoked.
     */
    private String preMedWebAction;
    /**
     * The name of WebAction modules to execute after the mediator is invoked.
     */
    private String postMedWebAction;
    
    private boolean advanceCurrentPage;

    /**
     * Default constructor
     */
    public AmsServletInvocation ()
    {
        // Assign default values, if any
        mediator = null;
        mapFiles = null;
        xslStyleSheet = null;
        xslErrorSheet = null;
        nextPage = null;
        errorPage = null;
        addToInputCache = false;
        beansAsInput = null;
        docCacheAsInput = null;
        beansToPopulate = null;
        multipleObjects = false;
        numberOfMultipleObjects = 0;
        numberOfMultipleObjects1 = 0; //Vshah - IR#VIUK101252424
        numberOfMultipleObjects2 = 0; //Leelavthi - 10thDec2012 - Rel8200 CR-737
        numberOfMultipleObjects3 = 0; //Kyriba CR 268
        numberOfMultipleObjects4 = 0;//Rel9.0 - numberOfMultipleObjects4 for Address fields
        docCacheName = "default.doc";
        addlParms = null;
        preMedWebAction = null;
        postMedWebAction = null;
        advanceCurrentPage = true;
    }

    public String getMediator ()
    {
        return mediator;
    }

    public void setMediator (String mediator)
    {
        this.mediator = mediator;
    }

    public String getMapFiles ()
    {
        return mapFiles;
    }

    public void setMapFiles (String mapFiles)
    {
        this.mapFiles = mapFiles;
    }

    public String getXslStyleSheet ()
    {
        return xslStyleSheet;
    }

    public void setXslStyleSheet (String xslStyleSheet)
    {
        this.xslStyleSheet = xslStyleSheet;
    }

    public String getXslErrorSheet ()
    {
        return xslErrorSheet;
    }

    public void setXslErrorSheet (String xslErrorSheet)
    {
        this.xslErrorSheet = xslErrorSheet;
    }

    public String getNextPage ()
    {
        return nextPage;
    }

    public void setNextPage (String nextPage)
    {
        this.nextPage = nextPage;
    }

    public String getErrorPage ()
    {
        return errorPage;
    }

    public void setErrorPage (String errorPage)
    {
        this.errorPage = errorPage;
    }

    public boolean isCallEjb ()
    {
        if (mediator == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean isAddToInputCache ()
    {
        return addToInputCache;
    }

    public void setAddToInputCache (boolean addToInputCache)
    {
        this.addToInputCache = addToInputCache;
    }

    public String getBeansAsInput ()
    {
        return beansAsInput;
    }

    public String getDocCacheAsInput ()
    {
        return docCacheAsInput;
    }

    public void setBeansAsInput (String beansAsInput)
    {
        this.beansAsInput = beansAsInput;
    }

    public void setDocCacheAsInput (String docCacheAsInput)
    {
        this.docCacheAsInput = docCacheAsInput;
    }

    public String getBeansToPopulate ()
    {
        return beansToPopulate;
    }

    public void setBeansToPopulate (String beansToPopulate)
    {
        this.beansToPopulate = beansToPopulate;
    }

    public boolean isMultipleObjects ()
    {
        return multipleObjects;
    }

    public void setMultipleObjects (boolean multipleObjects)
    {
        this.multipleObjects = multipleObjects;
    }

    public int getNumberOfMultipleObjects ()
    {
        return numberOfMultipleObjects;
    }

    public void setNumberOfMultipleObjects (int numberOfMultipleObjects)
    {
        this.numberOfMultipleObjects = numberOfMultipleObjects;
    }

    //Vshah - 10/19/2010 - IR#VIUK101252424 - [BEGIN]
    public int getNumberOfMultipleObjects1 ()
    {
        return numberOfMultipleObjects1;
    }

    public void setNumberOfMultipleObjects1 (int numberOfMultipleObjects1)
    {
        this.numberOfMultipleObjects1 = numberOfMultipleObjects1;
    }
    //Vshah - 10/19/2010 - IR#VIUK101252424 - [END]

  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
    public int getNumberOfMultipleObjects2 ()
    {
        return numberOfMultipleObjects2;
    }

    public void setNumberOfMultipleObjects2 (int numberOfMultipleObjects2)
    {
        this.numberOfMultipleObjects2 = numberOfMultipleObjects2;
    }
    //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
    
    //Kyriba CR 268 - External Bank
    public int getNumberOfMultipleObjects3 ()
    {
        return numberOfMultipleObjects3;
    }

    public void setNumberOfMultipleObjects3 (int numberOfMultipleObjects3)
    {
        this.numberOfMultipleObjects3 = numberOfMultipleObjects3;
    }
    
  //Rel9.0 - numberOfMultipleObjects4 for Address fields
    public int getNumberOfMultipleObjects4() {
		return numberOfMultipleObjects4;
	}

	public void setNumberOfMultipleObjects4(int numberOfMultipleObjects4) {
		this.numberOfMultipleObjects4 = numberOfMultipleObjects4;
	}

	public String getDocCacheName ()
    {
        return docCacheName;
    }

    public void setDocCacheName (String docCacheName)
    {
        this.docCacheName = docCacheName;
    }

    public String getAddlParm (String parmName) throws AmsException
    {
        Object obj = getAddlParms ().get (parmName);
        if (obj == null)
        {
          return null;
        }
        if (obj instanceof Vector)
        {
            throw new AmsException (
               "AmsServletInvocation: There are multiple values for " +
               parmName);
        }
        return ((String)obj);
    }

    public Vector getAddlParms (String parmName)
    {
        Object obj = getAddlParms ().get (parmName);
        if (obj == null)
        {
          return null;
        }
        if (obj instanceof String)
        {
            Vector v = new Vector ();
            v.addElement (obj);
            return v;
        }
        return ((Vector)obj);
    }

    public void setAddlParm (String parmName, String parmValue)
    {
        getAddlParms ().put (parmName, parmValue);
    }

    public void setAddlParms (String parmName, String [] parmValues)
    {
        Vector parmValuesVector = new Vector ();
        for (String parmValue : parmValues) {
            parmValuesVector.addElement(parmValue);
        }
        getAddlParms ().put (parmName, parmValuesVector);
    }

    public void setAddlParms (Hashtable addlParms)
    {
        this.addlParms = addlParms;
    }

    public Hashtable getAddlParms ()
    {
        if (addlParms == null)
        {
            addlParms = new Hashtable ();
        }
        return addlParms;
    }

    public String getPreMedWebAction ()
    {
        return preMedWebAction;
    }

    public void setPreMedWebAction (String preMedWebAction)
    {
        this.preMedWebAction = preMedWebAction;
    }

    public String getPostMedWebAction ()
    {
        return postMedWebAction;
    }

    public void setPostMedWebAction (String postMedWebAction)
    {
        this.postMedWebAction = postMedWebAction;
    }

    public boolean getAdvanceCurrentPage ()
    {
        return advanceCurrentPage;
    }

    public void setAdvanceCurrentPage (boolean advanceCurrentPage)
    {
        this.advanceCurrentPage = advanceCurrentPage;
    }
    
    public String toString ()
    {
        StringBuilder output = new StringBuilder();
        if (mediator != null)
        {
          output.append ("mediator:");
          output.append (mediator);
        }
        if (mapFiles != null)
        {
          output.append (" mapFiles:");
          output.append (mapFiles);
        }
        if (beansToPopulate != null)
        {
          output.append (" beansToPopulate:");
          output.append (beansToPopulate);
        }
        if (beansAsInput != null)
        {
          output.append (" beansAsInput:");
          output.append (beansAsInput);
        }
        if (docCacheAsInput != null)
        {
          output.append (" docCacheAsInput:");
          output.append (docCacheAsInput);
        }
        if (nextPage != null)
        {
          output.append (" nextPage:");
          output.append (nextPage);
        }
        if (errorPage != null)
        {
          output.append (" errorPage:");
          output.append (errorPage);
        }
        if (xslStyleSheet != null)
        {
          output.append (" xslStyleSheet:");
          output.append (xslStyleSheet);
        }
        if (xslErrorSheet != null)
        {
          output.append (" xslErrorSheet:");
          output.append (xslErrorSheet);
        }
        if (preMedWebAction != null)
        {
          output.append (" preMedWebAction:");
          output.append (preMedWebAction);
        }
        if (postMedWebAction != null)
        {
          output.append (" postMedWebAction:");
          output.append (postMedWebAction);
        }

        output.append (" addToInputCache:");
        output.append (addToInputCache);
        output.append (" multipleObjects:");
        output.append (multipleObjects);
        output.append (" numberOfMultipleObjects:");
        output.append (numberOfMultipleObjects);
        output.append (" numberOfMultipleObjects1:");
        output.append (numberOfMultipleObjects1);
      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
        output.append (" numberOfMultipleObjects2:");
        output.append (numberOfMultipleObjects2);
        //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
        //Kyriba CR 268 - ExternalBank - Start
        output.append (" numberOfMultipleObjects3:");
        output.append (numberOfMultipleObjects3);
        //Kyriba End
      //Rel9.0 - numberOfMultipleObjects4 for Address fields
        output.append (" numberOfMultipleObjects4:");
        output.append (numberOfMultipleObjects4);
        output.append (" docCacheName:");
        output.append (docCacheName);
        output.append (" advanceCurrentPage:");
        output.append (advanceCurrentPage);

        java.util.Enumeration keys = getAddlParms ().keys ();
        while (keys.hasMoreElements ())
        {
            String key = (String)keys.nextElement ();
            output.append(" ").append(key).append(":").append(getAddlParms().get(key));
        }

        return (output.toString ());
    }	
}
