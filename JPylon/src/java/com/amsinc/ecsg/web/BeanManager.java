package com.amsinc.ecsg.web;

import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @(#)BeanManager
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.JPylonProperties;

/**
 * BeanManager keeps track of all the web bean components used in an application. Web bean components are registered with the
 * BeanManager, which causes the object to be instantiated and stored as a part of the user's session. It provides access to the web
 * beans and notifies them of any updates that occur to the corresponding objects on the application server. This class should be
 * initialized with the component location of AmsServlet. This is used to get the EJB server location from the servlet
 * initialization parameter. Otherwise, localhost:8860 is the location used for the EJB server by default.
 */
public class BeanManager implements java.io.Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(BeanManager.class);

	/**
	 * A map between the logical object names and their corresponding id variable names. The id variable name is the identifier used
	 * to store web bean objects in the session object. This map provides access to beans through their logical name.
	 */
	private Hashtable beanList = null;
	/**
	 * A map between the logical object names and the java class name.
	 */
	private Hashtable classList = null;
	/**
	 * A map between logical object names and the collection of ListWebBeans that store this object type. This mapping is needed to
	 * determine what lists may have been affected by an update to an object on the application server.
	 */
	private Hashtable updateMap = null;
	/**
	 * The Enterprise Java Bean server location
	 */
	private String serverLocation;
	/**
	 * The location of AmsServlet
	 */
	private String servletLocation;
	/**
	 * The location of AmsServlet
	 */
	private int generatedBeanId;
	/**
	 * The location of AmsServlet
	 */
	private static String BEAN_MANAGER = "BeanManager";

	/**
	 * A collection of parameter objects for calls to AmsServlet. These are stored on the Bean Manager for security, rather than
	 * being hidden input parameters on HTML forms. The Bean Manager is particular to each user's session and is typically available
	 * from every Java Server Page, so it is good place to store these parameters. AmsServlet will use the getter method to retrieve
	 * the appropriate form parameters. This needs to be a collection because a given JSP could have more than one form, so a
	 * parameter is sent with each form, i.e., a numeric identifier for the form. This identifier corresponds with the index in the
	 * list of the parameter object.
	 */
	// private Vector servletInvocationParmList;

	/**
	 * A map between bean names and the actual beans
	 */
	// added to remove dependance on session.
	private Hashtable beans = new Hashtable();
	private UserLoginManager _userLoginMgr = null;

	/**
	 * The default constructor
	 *
	 */
	public BeanManager() {

	}
	// BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
	/*
	 * This method is used to apply XSL style sheets in a Java Server Page. It takes the name of a style sheet, an XML document, and
	 * the servlet output stream. This is a utility method placed on the BeanManager for developer convenience since the BeanManager
	 * is available from every JSP.
	 *
	 * @return void
	 */
   /*public void applyXsl (String styleSheet, DocumentHandler doc, Writer out)
   {
      PrintWriter printWriter = new PrintWriter (out);
      QueryBean qB = (QueryBean)getBean ("QueryBean");
      qB.setXmlDoc (doc);
      qB.applyXslToResultSet (styleSheet, printWriter);
      return;
   }*/
	/*
	 * This method is used to apply XSL style sheets in a Java Server Page. It takes the name of a style sheet, an XML document, and
	 * the servlet output stream. This is a utility method placed on the BeanManager for developer convenience since the BeanManager
	 * is available from every JSP.
	 *
	 */
   /*public void applyXsl (String styleSheet,
                         DocumentHandler doc,
                         ServletOutputStream outStream)
   {
      QueryBean qB = (QueryBean)getBean ("QueryBean");
      qB.setXmlDoc (doc);
      qB.applyXsl (styleSheet, outStream);
      return;        
   }*/
   //BSL Cocoon Upgrade 09/28/11 Rel 7.0 END

	/**
	 * This method returns the bean from the user's session based on the object name.
	 * 
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 */
	public Object getBean(String objectName) {
		String id = (String) beanList.get(objectName);
		if (id == null) {
			LOG.debug("BeanManager Didn't find {} in BeanManager registry.", objectName);
			return null;
		}
		return (beans.get(id));
	}

	/**
	 * This method returns the bean from the user's session based on the id, or Java Server Page variable name. This method can be
	 * used as an alternative to the getBean method, although, it must be used when there are two different web beans which
	 * represent different instances of the same logical object. In this case, the object name would be the same, and the
	 * BeanManager would not know which instance to return to the calling method.
	 * 
	 * @param id
	 *            The id of the object represented, typically the same as the JSP variable name
	 */
	public Object getBeanById(String id) {
		Object obj = beans.get(id);
		if (obj == null) {
			LOG.debug("BeanManager Didn't find the id {} in the BeanManager registry.", id);
			return null;
		}
		return obj;
	}

	/**
	 * This method returns the class name of the bean based on the object name.
	 * 
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 */
	public String getClassName(String objectName) {
		String className = (String) classList.get(objectName);
		if (className == null) {
			LOG.debug("BeanManager Didn't find {} in BeanManager registry.", objectName);
		}
		return className;
	}

	/**
	 * This method generates an id value to use to store beans in the sessioni object.
	 *
	 * @return generated id
	 */
	private String getNextBeanId() {
		String generatedId = BeanManager.BEAN_MANAGER.concat(Integer.toString(generatedBeanId));
		generatedBeanId++;
		return generatedId;
	}

	/**
	 * This method returns the Enterprise Java Bean server location as a string. This location is used for all interaction with EJB
	 * components.
	 *
	 * @return EJB server location
	 */
	public String getServerLocation() {
		return serverLocation;
	}

	/**
	 * Retrieves the user's login information from the form manager's session information.
	 *
	 * @return The user's login information object or <code>null</code> if no login information exists in the session.
	 */
	public UserLoginManager getUserLoginInfo() {
		return _userLoginMgr;
	}

	/**
	 * This method initializes the bean manager. This method should only be called to start a new session. It will refresh all of
	 * the internal mappings stored in this class. A call to this method is almost always followed by registerBean() method calls.
	 * It also registers infrastructure web beans such as the query bean. The method invocation from a Java Server Page almost
	 * always looks like this: beanMgr.init(request.getSession(true), this.getServletConfig().getServletContext()); The servlet
	 * location paramter is used to gather initialization parameters from the servlet for use with web beans (i.e. the EJB server
	 * location, debug mode, etc.)
	 *
	 * @param session
	 *            The servlet session object
	 * @param serletLocation
	 *            The component location of AmsServet on the web server
	 */
	public void init(HttpSession session, String servletLocation) {
		int debugMode;

		beans = new Hashtable();
		beanList = new Hashtable();
		classList = new Hashtable();
		updateMap = new Hashtable();
		JPylonProperties jPylonProperties = null;
		String xslFileLocation = null;
		generatedBeanId = 0;

		try {
			jPylonProperties = JPylonProperties.getInstance();
		} catch (Exception e) {
		}

		// try to get the EJB server location from the AmsServlet properties file
		try {
			if (servletLocation != null) {
				this.setServerLocation(jPylonProperties.getString("serverLocation"));

				try {
					xslFileLocation = jPylonProperties.getString("xslFileLocation");
				} catch (AmsException amse) {
					xslFileLocation = "";
				}
			}
		} catch (Exception e) {
			LOG.error("BeanManager Couldn't get server location from AmsServlet", e);
		}

		// Initialize application support systems
		try {
			// Initialize the server side components
			ServerDeploy serverDeploy = null;

			if (getUserLoginInfo() == null) {
				serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(getServerLocation(), "ServerDeploy");
			} else // we are using EJB security
			{
				serverDeploy = (ServerDeploy) EJBObjectFactory.createSecureClientEJB(getServerLocation(), "ServerDeploy",
						getUserLoginInfo().getLoginName(), getUserLoginInfo().getLoginPassword());
			}
			serverDeploy.init(getServerLocation());
			serverDeploy.remove();
			// Initialize the web server components
			ReferenceDataManager.getRefDataMgr(getServerLocation());
		} catch (Exception e) {
		}

		// Register infrastructure beans
		registerBean("com.amsinc.ecsg.web.QueryBean", "QueryBean", "qbean");
		QueryBean qb = (QueryBean) getBean("QueryBean");
		// BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
		/*
		 * if (xslFileLocation != null) { qb.setXslFileLocation (xslFileLocation); }
		 */
		// BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	}

	/**
	 * This method tells whether a given bean is registered.
	 * 
	 * @param objectName
	 *            The logical name of the web bean
	 */
	public boolean isBeanRegistered(String objectName) {
		String id = (String) beanList.get(objectName);
		if (id == null) {
			return false;
		}
		return true;
	}

	/**
	 * The simple API to register an individual web bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 */
	public void registerBean(String className, String objectName) {
		// Generate an id to store this bean in the session
		String id = this.getNextBeanId();
		// Register the bean
		registerBean(className, objectName, id, null, null);
	}

	/**
	 * The simple API to register an individual web bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the
	 * @param ClientServerDataBridge
	 *            The name of the object represented, typically the same as the component name
	 */
	public void registerBean(String className, String objectName, ClientServerDataBridge csdb) {
		// Generate an id to store this bean in the session
		String id = this.getNextBeanId();
		// Register the bean
		registerBean(className, objectName, id, null, csdb);
	}

	/**
	 * Registers a bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param id
	 *            The id (or parameter name) used to refer to this object in Java Server Pages
	 */
	public void registerBean(String className, String objectName, String id) {
		registerBean(className, objectName, id, null, null);
	}

	/**
	 * Registers a bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param id
	 *            The id (or parameter name) used to refer to this object in Java Server Pages
	 */
	public void registerBean(String className, String objectName, String id, ClientServerDataBridge csdb) {
		registerBean(className, objectName, id, null, csdb);
	}
	
	public <T extends AmsWebBean> T createBean(Class<T> clazz, String objectName) {
		return createBean(clazz, objectName, null);
	}
	
	 public <T extends AmsWebBean> T createBean(Class<T> clazz, String objectName , ClientServerDataBridge csdb) {
		   T newObj = null;
		   try
		      {
			     newObj = clazz.newInstance();
		         newObj.setClientServerDataBridge(csdb);
		         newObj.init (this);
		         newObj.setServerLocation (this.getServerLocation());
		         newObj.setEcsgObjectName (objectName);
		      }
		      catch (Exception e)
		      {
		    	  LOG.error("BeanManager Couldn't create the class {}", clazz.getName(), e);
		      } 
		      return newObj;
	   }

	/**
	 * Registers a bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param id
	 *            The id (or parameter name) used to refer to this object in Java Server Pages
	 * @param bLazyRefresh
	 *            Indicator to determine whether this bean should do a lazy refresh
	 * @param listObjectName
	 *            If this bean is a list bean, this is the name of the objects in the list
	 * @deprecated Use the registerListBean method
	 */
	public void registerBean(String className, String objectName, String id, String listObjectName, ClientServerDataBridge csdb) {
		LOG.debug("BeanManager Registering {} as {}", className, objectName);
		// Create an instance of the bean
		AmsWebBean newObj = null;
		try {
			newObj = (AmsWebBean) Class.forName(className).newInstance();
			newObj.setClientServerDataBridge(csdb);
			newObj.init(this);
			newObj.setServerLocation(this.getServerLocation());
			if (listObjectName == null) {
				newObj.setEcsgObjectName(objectName);
			} else {
				newObj.setEcsgListName(objectName);
				newObj.setEcsgObjectName(listObjectName);
				Vector catalogObjects = (Vector) updateMap.get(objectName);
				if (catalogObjects == null) {
					catalogObjects = new Vector();
					updateMap.put(listObjectName, catalogObjects);
				}
				catalogObjects.addElement(objectName);
			}
		} catch (Exception e) {
			LOG.error("BeanManager Couldn't create the class {}", className, e);
		}

		// Store the bean as a part of the user's session.
		// This will allow us to access it from Java Server Pages.
		beans.put(id, newObj);

		// We also need to store the objects based on object name.
		// This is our connection from the business object side.
		beanList.put(objectName, id);

		// We also need to store the class names so we can construct
		// instances of the classes.
		classList.put(objectName, className);
	}

	/**
	 * The simple API to register a list web bean with the bean manager. It is recommended that this API be used rather than the
	 * other registerListBean() methods.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param listObjectName
	 *            The logical name of the entity web bean objects in the list
	 * @param listObjectClassName
	 *            The class name of the entity web bean objects in the list
	 */
	public void registerListBean(String className, String objectName, String listObjectName, String listObjectClassName) {
		// Store the class name of the entity web beans in the list so we can
		// construct instances of the class later on.
		classList.put(listObjectName, listObjectClassName);
		// Generate an id to store this bean in the session
		String id = this.getNextBeanId();
		// Register the bean
		registerBean(className, objectName, id, listObjectName, null);
	}

	/**
	 * The simple API to register a list web bean with the bean manager. It is recommended that this API be used rather than the
	 * other registerListBean() methods.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param listObjectName
	 *            The logical name of the entity web bean objects in the list
	 * @param listObjectClassName
	 *            The class name of the entity web bean
	 * @param csdb
	 *            The client server data bridge objects in the list
	 */
	public void registerListBean(String className, String objectName, String listObjectName, String listObjectClassName,
			ClientServerDataBridge csdb) {
		// Store the class name of the entity web beans in the list so we can
		// construct instances of the class later on.
		classList.put(listObjectName, listObjectClassName);
		// Generate an id to store this bean in the session
		String id = this.getNextBeanId();
		// Register the bean
		registerBean(className, objectName, id, listObjectName, csdb);
	}

	/**
	 * Registers a list web bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param id
	 *            The id (or parameter name) used to refer to this object in Java Server Pages
	 * @param bLazyRefresh
	 *            Indicator to determine whether this bean should do a lazy refresh
	 * @param listObjectName
	 *            The logical name of the entity web bean objects in the list
	 * @param listObjectClassName
	 *            The class name of the entity web bean objects in the list
	 */
	public void registerListBean(String className, String objectName, String id, String listObjectName,
			String listObjectClassName) {
		// Store the class name of the entity web beans in the list so we can
		// construct instances of the class later on.
		classList.put(listObjectName, listObjectClassName);
		// Register the bean
		registerBean(className, objectName, id, listObjectName, null);
	}

	/**
	 * Registers a list web bean with the bean manager.
	 * 
	 * @param className
	 *            The class name of java bean being registered
	 * @param objectName
	 *            The name of the object represented, typically the same as the component name
	 * @param id
	 *            The id (or parameter name) used to refer to this object in Java Server Pages
	 * @param bLazyRefresh
	 *            Indicator to determine whether this bean should do a lazy refresh
	 * @param listObjectName
	 *            The logical name of the entity web bean objects in the list
	 * @param listObjectClassName
	 *            The class name of the entity web bean objects in the list
	 */
	public void registerListBean(String className, String objectName, String id, String listObjectName, String listObjectClassName,
			ClientServerDataBridge csdb) {
		// Store the class name of the entity web beans in the list so we can
		// construct instances of the class later on.
		classList.put(listObjectName, listObjectClassName);
		// Register the bean
		registerBean(className, objectName, id, listObjectName, csdb);
	}

	/**
	 * This method sets the Enterprise Java Bean server location as a string. This location is used for all interaction with EJB
	 * components.
	 *
	 * @param serverLociation
	 *            The EJB server location (IP:port)
	 */
	public void setServerLocation(String serverLocation) {
		this.serverLocation = serverLocation;
	}

	/**
	 * Unregister a web bean with the bean manager.
	 * 
	 * @param String
	 *            The logical name of the web bean
	 */
	public void unregisterBean(String objectName) {
		String id = (String) beanList.get(objectName);
		beans.remove(id);
		beanList.remove(objectName);
		classList.remove(objectName);
		updateMap.remove(objectName);
	}

	/**
	 * Updates the user login information stored in this form manager's session. If there us no login information stored there then
	 * it is created.
	 *
	 * @param name
	 *            The user's login name.
	 * @param password
	 *            The user's login password.
	 */
	public void updateUserLoginInfo(String name, String password) {
		// Check if we already have one. If so update it, otherwise create one
		if (_userLoginMgr == null) {
			_userLoginMgr = new UserLoginManager(name, password);
		}
	}
}
