package com.amsinc.ecsg.web;

/*
 * @(#)CurrPageStorage
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.util.*;
import com.amsinc.ecsg.util.*;

public class CurrPageStorage implements java.io.Serializable {

    // Class variables
    private Vector firstPage;
    private Vector secondPage;
    private boolean onFirst;
    
    /**
     * A reference to the framework debugger class
     */
    private transient FrameworkDebugger debugger;    
    
    public CurrPageStorage() {
        this.firstPage = null;
        this.secondPage = null;
        this.onFirst = true;
    }

    public void nextPage(Hashtable _forms) {
        Vector tempVector = null;
        if (onFirst) {
            tempVector = this.getSecondPage();
            //this.firstPage = firstPage;
            // reset indicator
            onFirst = false;
        } else {
            tempVector = this.getFirstPage();
            //this.secondPage = secondPage;
            // reset indicator
            onFirst = true;
        }
        // Remove old navigation contexts
        for (int loop = 0; loop < tempVector.size(); loop++) {
            String tempNavId = (String) tempVector.elementAt(loop);
            this.debug("About to remove " + tempNavId);
            _forms.remove(tempNavId);
        }
        
    }

    public Vector getCurrentPage() {
        if (onFirst) {
          return getFirstPage();
        }
        return getSecondPage();
    }

    public void addElement(String navId) {
        Vector tempVector = this.getCurrentPage();
        tempVector.addElement(navId);
    }

    //
    // Private methods
    //
    private Vector getFirstPage() {
        if (firstPage == null) {
            firstPage = new Vector();
        }
        return firstPage;
    }

    private Vector getSecondPage() {
        if (secondPage == null) {
            secondPage = new Vector();
        }
        return secondPage;
    }
    
   /**
    * This method writes a message to the debugger, which based on the
    * debug mode which was configured through AmsServlet, will be written
    * to system out, the debug log file, or not at all.
    *
    * @param   message debug message to be logged     

    */
   public void debug (String message)
   {
       getDebugger ().debug (this.getClass ().getName (), message);
   }    
   
   /**
    * Retrieves the debugger to be used by this class instance. If one has not
    * yet been created, then one is created now.
    *
    * @return The framework debugger
    */
   public FrameworkDebugger getDebugger ()
   {
      if (debugger == null)
      {
         debugger = new FrameworkDebugger ();
      }
      return debugger;
   }
}
