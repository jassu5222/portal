package com.amsinc.ecsg.web;

/**
 * @(#)FormManager.java
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import java.util.*;
import java.text.*;
import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FormManager simplifies the process of setting up forms within a JSP. It stores the form parameters internally and uses a
 * navigation id to associate the specific form instace. The form manager uses an XML configuration file, FormManager.xml, to
 * determine the generic parameters for the specific type of form being used. The FormManager.xml file should be stored in the
 * mapFiles directory specified in the AmsServletConfig.properties file.<BR/>
 *
 * Note that FormManager must be initialized through the init() method before any of its methods can be used. It is safe to put the
 * following at the top of all of your JSPs:
 * <P/>
 * &lt;jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"&gt; <BR/>
 * &lt;% <BR/>
 * formMgr.init(request.getSession(true), this.getServletConfig().getServletContext(), "AmsServlet"); <BR/>
 * %&gt; <BR/>
 * &lt;/jsp:useBean&gt;
 * <P/>
 *
 * <H3>Use FormManager methods to do the following in your JSP:</H3>
 * <H4>Page Navigation</H4> To create navigational links, use either getLinkAsHref() or getLinkAsUrl(). getLinkAsHref() returns the
 * entire anchor tag string while getLinkAsUrl() returns only the URL portion which you can then put into an HTML anchor tag. These
 * methods are overloaded to handle navigation to frame windows and adding secure or unsecure additional parameters (either put into
 * the URL or stored in FormManager on the server side).
 *
 * <H4>HTML Forms: Linking form on JSP to FormManager.xml Entry</H4> To link a form in your JSP to its form definition in the
 * configuration file, FormManager.xml, use the getFormInstanceAsInputField() method. This method is also overloaded to allow for
 * cases where additional secure parameters. The most commonly used forms are: <BR/>
 * &lt;%= formMgr.getFormInstanceAsInputField (formName, userAction) %&gt; <BR/>
 * &lt;%= formMgr.getFormInstanceAsInputField (formName, formParms, userAction) %&gt;
 * <P/>
 * 
 * Note that you must put these method calls in a JSP expression because they result in an HTML hidden input field on the form that
 * carries the navigation id value, which links the form submission to the server side context.
 *
 * <H4>Easily Access the XML Document Cache</H4> To get a document from the document cache, use formMgr.getFromDocCache(). To store
 * in the document cache, use formMgr.storeInDocCache().
 *
 * <H4>Apply XSL Style Sheets in a JSP</H4> To apply a style sheet to an XML document, use the applyXsl() method. A common example
 * is to use this method to apply an error style sheet to the document cache to display the errors from the previous transaction to
 * the user. This might look like: <BR/>
 * &lt%; formMgr.applyXsl ("auctionerrors.xsl", formMgr.getFromDocCache (), out); %&gt;
 * 
 *
 * @version 1.0 Tue Jan 11 16:52:48 CST 2000
 */

public class FormManager implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(FormManager.class);

	private static Hashtable _formTemplates = new Hashtable();
	private static Hashtable _formAddlParms = new Hashtable();

	private static final Hashtable _noFormParms = new Hashtable();
	private static final String _noJavaScript = "";

	public static final int CURR_PAGE_PLUS_ACTION_NAVIGATION = 1;
	public static final int ACTION_BASED_NAVIGATION = 2;
	public static final int CURR_PAGE_BASED_NAVIGATION = 3;

	public static final int TIME_BASED_EXPIRATION = 1;
	public static final int CURRENT_PAGE_BASED_EXPIRATION = 2;

	private Hashtable _docCache = new Hashtable();
	private String _servletLocation;
	private boolean _isPerformanceTestingEnabled = false;

	// The context root for the web application in which this is running Must be passed into init()
	// Used to build links
	private String contextRoot;

	private boolean cocoonMessageHandlerSetup = false;

	// These next messages are static to take them out of the heap.
	private static MessageFormat _formInstanceAsInputField = new MessageFormat("<INPUT TYPE=HIDDEN NAME=\"ni\" VALUE=\"{0}\">");
	private static MessageFormat _formInstanceAsHRef = new MessageFormat("<A HREF=\"{3}/Dispatcher.jsp?ni={0}{2}>{1}</A>\"");
	private static MessageFormat _formInstanceAsURL = new MessageFormat("{1}/Dispatcher.jsp?ni={0}");
	private static MessageFormat _linkAsHref = new MessageFormat("<A HREF=\"{0}\">{1}</A>");
	private static MessageFormat _linkAsHrefwFrame = new MessageFormat("<A HREF=\"{0}\" TARGET=\"{2}\">{1}</A>");
	private static MessageFormat _amsServletLink = new MessageFormat("{3}/Dispatcher.jsp?ni={0}{1}{2}");
	// BSL Cocoon Upgrade 09/21/11 Rel 7.0 BEGIN
	// private static MessageFormat _documentLink = new MessageFormat
	// ("{4}/Document.xml?producer={0}&transaction={1}&locale={2}&bank_prefix={3}&report=document.pdf");
	private static MessageFormat _documentLink = new MessageFormat("/documents/{0}?transaction={1}&locale={2}&bank_prefix={3}&report=document.pdf");
	// BSL Cocoon Upgrade 09/21/11 Rel 7.0 END

	private static DocumentHandler _configFile = null;
	private Hashtable _forms = null;
	private boolean atgDynamo = false;
	public static final String ADDL_PARM_DELIMETER = "!#!";
	private String serverLocation = null;
	private String mNavId = null;
	/**
	 * A reference to the framework debugger class
	 */
	// private transient FrameworkDebugger debugger;
	/*
	 * An instance of an XSL parser (lotusxsl) used to display XML result sets if desired.
	 */
	// private transient XSLTProcessor processor;//BSL Cocoon Upgrade 09/28/11 Rel 7.0 DELETE
	/**
	 * The location of the XSL style sheet files
	 */
	private String xslFileLocation;

	//
	// Navigation properties
	//

	/**
	 * The web page (logical name) the user last navigated to (used by the NavigationManager) and set as the CurrentPage base.
	 */
	private String currPage;

	/**
	 * The web page (logical name) the user last navigated to (used by the Dispatcher.jsp)
	 */
	private String navigateToPage;
	/**
	 * The form number to execute next
	 */
	private int nextFormNumber;
	/**
	 * The original page name
	 */
	private String originalPage;
	/**
	 * A counter for the number of pages served, used to determine when to erase old page request info objects.
	 */
	private int pageNumberServed;
	/**
	 * A counter for the number of request info objects for this page.
	 */
	private int requestInfoCount;
	/**
	 * The expiration number for all client request info objects. This will be set to the configured value plus one (for the current
	 * page)
	 */
	private int requestInfoExpiration;
	/**
	 * The expiration count for all client request info objects
	 */
	private int requestInfoExpirationCount;
	/**
	 * The array of Vectors storing nav Ids
	 */
	private Vector reqInfoStorageList;
	/**
	 * An indicator determining whether links created will expire or not
	 */
	private boolean bLinksExpire;
	/**
	 * An indicator determining what type of navigation to use
	 */
	private int navigationType;
	/**
	 * An indicator of page expiration modality
	 */
	private int pageExpirationModality;
	/**
	 * The array of Vectors storing nav Ids
	 */
	private Hashtable currPageBasedExpireList;

	/**
	 * Default constructor for FormManager.
	 *

	 */
	public FormManager() {
		this.nextFormNumber = -1;

	}
	// BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
	/*
	 * This method is used to apply XSL style sheets in a Java Server Page. It takes the name of a style sheet, an XML document, and
	 * the servlet output stream. This is a utility method placed on the FormManager for developer convenience since the FormManager
	 * is available from every JSP.
	 *
	 */
   /*public void applyXsl (String styleSheet, DocumentHandler doc, Writer out)
   {
	  try {
		FileReader fileReader = new FileReader(xslFileLocation.concat (styleSheet));
		InputSource is = new InputSource (fileReader);
		XSLTInputSource xsltStyleSheet = new XSLTInputSource (is);
		XSLTInputSource xmlDoc = new XSLTInputSource (doc.toString());
		XSLTResultTarget resultTarget = new XSLTResultTarget (out);
		getXslProcessor ().process (xmlDoc, xsltStyleSheet, resultTarget);
	  }
	  catch (Exception e)
	  {
		 LOG ("Error: ",e);
	  }

	  return;
   }*/
   //BSL Cocoon Upgrade 09/28/11 Rel 7.0 END

	/**
	 * This method clears all registered forms and links. This should be done to remove any permanent navigation links, however,
	 * note that all form and link contexts will be erased, so this should be used with extreme caution. The browser back button
	 * will not be supported immediately after a call to this method.
	 * 
	 */
	public void clearForms() {
		if (_forms != null) {
			_forms.clear();
		}
	}

	/**
	 * This method clears the servlet invocation objects unless the next request is directed back to the servlet.
	 * 
	 * @deprecated This method is no longer needed due to the pageExpirationCount logic
	 */
	public void clearFormsUnlessServletRedirect() {
	}

	/**
	 * This method writes a message to the debugger, which based on the debug mode which was configured through AmsServlet, will be
	 * written to system out, the debug log file, or not at all.
	 *
	 * @param message
	 *            debug message to be logged
	 */
	public void debug(String message) {
		LOG.debug(message);
	}

	/**
	 * This method returns the logical name of the current web page (used by NavigationManager). If there is no current context, the
	 * default page in the Navigation configuration is returned.
	 *
	 */
	public String getCurrPage() {
		if (currPage == null) {
			try {
				currPage = NavigationManager.getNavMan().getDefaultPage();
			} catch (Exception e) {
			}
		}
		return currPage;
	}

	/**
	 * This method returns the logical name of the current web page (used by NavigationManager). If there is no current context, the
	 * default page in the Navigation configuration is returned.
	 *
	 */
	public String getNavigateToPage() {
		if (navigateToPage == null) {
			return getCurrPage();
		}
		return navigateToPage;
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 */
	private synchronized Hashtable getFormAddlParms(String formName) {
		// If the form template is not already stored, create a new one using
		// the XML configuration file in the configuration directory
		if (!_formAddlParms.containsKey(formName)) {
			Hashtable addlParmsHash = new Hashtable();
			Vector addlParmsList = getFormTemplate(formName).getFragments("/addlParms");
			int addlParmsListSize = addlParmsList.size();
			for (int loop = 0; loop < addlParmsListSize; loop++) {
				DocumentHandler tempDoc = (DocumentHandler) addlParmsList.elementAt(loop);
				String addlParmName = tempDoc.getAttribute("/name");
				String addlParmValue = tempDoc.getAttribute("/value");
				if (addlParmName == null) {
					LOG.debug("FormManager.getFormAddlParms ERROR: Parameter name missing in addlParms in FormManager.xml");
				}
				if (addlParmValue == null) {
					LOG.debug("FormManager.getFormAddlParms ERROR: Parameter value missing for {} in addlParms in FormManager.xml", addlParmName);
				}
				if (addlParmsHash.containsKey(addlParmName)) {
					StringBuilder tempBuffer = new StringBuilder((String) addlParmsHash.get(addlParmName));
					// Append some wacky string to delimit multiple values. XmlTransport will use this delimiter
					tempBuffer.append(FormManager.ADDL_PARM_DELIMETER);
					tempBuffer.append(addlParmValue);
					String newValue = tempBuffer.toString();
					addlParmsHash.put(addlParmName, newValue);
				} else {
					addlParmsHash.put(addlParmName, addlParmValue);
				}
			}
			_formAddlParms.put(formName, addlParmsHash);
		}

		return (Hashtable) _formAddlParms.get(formName);
	}

	/**
	 * This method is typically invoked through getFormInstanceAsInputField (). Use these methods instead unless you have carefully
	 * read the documentation and understand how to use the navigation id returned.
	 *
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 */
	public int getFormInstance(String formName, Hashtable formParms) {
		return this.getFormInstance(formName, formParms, null);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 * @param userAction
	 *            The logical action that occurs when this form's submit button is pressed. Matches string defined in the
	 *            Navigation.xml file.
	 */
	public int getFormInstance(String formName, Hashtable formParms, String userAction) {
		// Create a new client request info object
		if (userAction == null) {
			userAction = getFormTemplate(formName).getAttribute("/userAction");
		}

		// Use navigation manager to get navigation info for the client
		// request object
		ClientRequestInfo reqInfo = null;
		NavigationInfo navInfo = null;
		try {
			switch (this.navigationType) {
			case CURR_PAGE_PLUS_ACTION_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(currPage, userAction);
				break;
			case ACTION_BASED_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(null, userAction);
				break;
			case CURR_PAGE_BASED_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(currPage, null);
				break;
			default:
				LOG.debug("FormManager ERROR: Invalid navigation type {}" , navigationType);
			}
			
				reqInfo = new ClientRequestInfo(navInfo.getNextPage(), navInfo.getErrorPage(), formName, navInfo.getAdvanceCurrentPage());
				// Add the servlet invocation parameters defined for this specific instance of the form
				Enumeration en = formParms.keys();
				while (en.hasMoreElements()) {
					String key = (String) en.nextElement();
					reqInfo.setAddlParm(key, (String) formParms.get(key));
				}
			

		} catch (Exception e) {
			LOG.error("FormManager Exception caught in FormManager.getFormInstance", e);

		}

		// This registers the uniquely identifying code from bean manager and returns a uniquely identifying form number
		int navId = registerServletInvocation(reqInfo);
//		LOG.trace("FormManager.getFormInstance: Created ni {}{} + {} + {} -> {}", ""+navId, currPage, formName, userAction, navInfo);
		LOG.trace("FormManager.getFormInstance: Created ni " + navId  + currPage + "+" + formName + "+" + userAction + "->" + navInfo.toString ());
		return navId;

	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param linkName
	 */
	public String getFormInstanceAsHRef(String formName, String linkName) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getFormInstanceAsHRef(formName, linkName, _noJavaScript, _noFormParms);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param linkName
	 * @param javaScript
	 * @return String
	 */
	public String getFormInstanceAsHRef(String formName, String linkName, String javaScript) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getFormInstanceAsHRef(formName, linkName, javaScript, _noFormParms);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param linkName
	 * @param javaScript
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 */
	public String getFormInstanceAsHRef(String formName, String linkName, String javaScript, Hashtable formParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String[] formatArgs = { Integer.toString(getFormInstance(formName, formParms)), linkName, javaScript, contextRoot };

		return _formInstanceAsHRef.format(formatArgs);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param linkName
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 */
	public String getFormInstanceAsHRef(String formName, String linkName, Hashtable formParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getFormInstanceAsHRef(formName, linkName, _noJavaScript, formParms);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @return String representation of the hidden input field that contains the form's navigation information.
	 */
	public String getFormInstanceAsInputField(String formName) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form as an INPUT TYPE=HIDDEN
		return getFormInstanceAsInputField(formName, _noFormParms, null);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param userAction
	 *            The logical action that occurs when this form's submit button is pressed. Matches string defined in the
	 *            Navigation.xml file.
	 * @return String representation of the hidden input field that contains the form's navigation information.
	 */
	public String getFormInstanceAsInputField(String formName, String userAction) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form as an INPUT TYPE=HIDDEN
		return getFormInstanceAsInputField(formName, _noFormParms, userAction);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 * @return String representation of the hidden input field that contains the form's navigation information.
	 */
	public String getFormInstanceAsInputField(String formName, Hashtable formParms) {
		return this.getFormInstanceAsInputField(formName, formParms, null);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 * @param userAction
	 *            The logical action that occurs when this form's submit button is pressed. Matches string defined in the
	 *            Navigation.xml file.
	 * @return String representation of the hidden input field that contains the form's navigation information.
	 */
	public String getFormInstanceAsInputField(String formName, Hashtable formParms, String userAction) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form as an INPUT TYPE=HIDDEN
		String[] formatArgs = { Integer.toString(getFormInstance(formName, formParms, userAction)) };
		return _formInstanceAsInputField.format(formatArgs);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @return String
	 */
	public String getFormInstanceAsURL(String formName) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getFormInstanceAsURL(formName, _noFormParms);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @param formParms
	 *            Additional parameters to be supplied during form submission. These are treated the same as HTML field values in
	 *            that their values are mapped as defined in any mapfiles and sent on to the mediator. As a "side effect" these are
	 *            also placed into the same space as a link's secure parameters, and can be accessed with a getSecurePageParm method
	 *            call from the next JSP.
	 * @return String
	 */
	public String getFormInstanceAsURL(String formName, Hashtable formParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String[] formatArgs = { Integer.toString(getFormInstance(formName, formParms)), contextRoot };
		return _formInstanceAsURL.format(formatArgs);
	}

	/**
	 * 
	 * @param formName
	 *            The name of the form as defined to the FormManager.xml file
	 * @return
	 */
	private synchronized DocumentHandler getFormTemplate(String formName) {
		// If the form template is not already stored, create a new one using the XML configuration file in the configuration directory

		if (!_formTemplates.containsKey(formName)) {
			DocumentHandler doc = XmlConfigFileCache.readConfigFile("/FormManager.xml")
					.getComponent("/Forms/Form(" + formName + ")");
			if (doc == null) {
				LOG.debug("FormManager.getFormTemplate Form " + formName + " not found in FormManager.xml file.");
			}
			_formTemplates.put(formName, doc);
		}

		return (DocumentHandler) _formTemplates.get(formName);
	}

	/**
	 * This method is used to retrieve the default XML document from the document cache.
	 *
	 * @return The document from the document cache
	 */
	public DocumentHandler getFromDocCache() {
		return this.getFromDocCache("default.doc");
	}

	/**
	 * This method is used to retrieve XML documents from the document cache.
	 *
	 * @param docCacheName
	 *            the document name
	 * @return The document from the document cache
	 */
	public DocumentHandler getFromDocCache(String docCacheName) {
		DocumentHandler doc = (DocumentHandler) _docCache.get(docCacheName);
		return doc;
	}

	/**
	 * This method is used to create a navigational link to a specific document with secure parameters.
	 *
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param response
	 *            The built in response object that is part of every jsp. page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String representation of the HTML HREF link code.
	 */
	/*
   /* W Zhu 8/17/2012 T36000004579 comment out since this is not used and if this needed we should use session-specific key from userSession.
   public String getDocumentLinkAsHref (String linkText, String templateName,
										String key, String locale, String bankPrefix,
										HttpServletResponse response)
   {      
	  String[] formatArgs = { templateName, EncryptDecrypt.encryptAlphaNumericString(key), locale, bankPrefix, contextRoot };
	  String url = _documentLink.format (formatArgs);
	  formatArgs[0] = response.encodeRedirectURL (url);
	  formatArgs[1] = linkText;
	  return _linkAsHref.format (formatArgs);
   }  
   */    

	/**
	 * Creates a URL to fill the "ACTION" attribute of the <FORM> tag. Calling encodeURL adds the session ID to the URL if cookies
	 * are not enabled.
	 *
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return the form action
	 */
	public String getSubmitAction(HttpServletResponse response) {
		return response.encodeURL(contextRoot + "/Dispatcher.jsp");
	}

	/**
	 * This method is used to create a navigational link. For form submissions, use the getFormInstanceXXX set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, String addlParms, HttpServletResponse response) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String[] formatArgs = { Integer.toString(getLinkInstance(userAction)), addlParms, "", contextRoot };

		if (_isPerformanceTestingEnabled) {
			formatArgs[2] = "&userAction=" + userAction;
		}
		String url = _amsServletLink.format(formatArgs);
		formatArgs[0] = response.encodeRedirectURL(url);
		formatArgs[1] = linkText;
		return _linkAsHref.format(formatArgs);
	}

	/**
	 * This method is used to create a navigational link to a specific frame. For form submissions, use the getFormInstanceXXX set
	 * of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param frameName
	 *            The HTML name of the target frame (the "name" tag's value within the "frame" tag).
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, String addlParms, HttpServletResponse response,
			String frameName) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String[] formatArgs = { Integer.toString(getLinkInstance(userAction)), addlParms, frameName, contextRoot };
		String url = _amsServletLink.format(formatArgs);
		formatArgs[0] = response.encodeRedirectURL(url);
		formatArgs[1] = linkText;
		return _linkAsHrefwFrame.format(formatArgs);
	}

	/**
	 * This method is used to create a navigational link to a specific frame with secure parameters. For form submissions, use the
	 * getFormInstanceXXX set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param frameName
	 *            The HTML name of the target frame (the "name" tag's value within the "frame" tag).
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, String addlParms, HttpServletResponse response,
			String frameName, Hashtable secureParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String strNavId = Integer.toString(getLinkInstance(userAction));
		// Put secure parms in ClientRequestInfo
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(strNavId);
		Enumeration en = secureParms.keys();
		while (en.hasMoreElements()) {
			String key = (String) en.nextElement();
			reqInfo.setAddlParm(key, (String) secureParms.get(key));
		}

		String[] formatArgs = { strNavId, addlParms, frameName, contextRoot };
		String url = _amsServletLink.format(formatArgs);
		formatArgs[0] = response.encodeRedirectURL(url);
		formatArgs[1] = linkText;
		return _linkAsHrefwFrame.format(formatArgs);
	}

	/**
	 * This method is used to create a navigational link with secure parameters. For form submissions, use the getFormInstanceXXX
	 * set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These are included in the link's text.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, String addlParms, HttpServletResponse response,
			Hashtable secureParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		String strNavId = Integer.toString(getLinkInstance(userAction));
		// Put secure parms in ClientRequestInfo
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(strNavId);
		Enumeration en = secureParms.keys();
		while (en.hasMoreElements()) {
			String key = (String) en.nextElement();
			reqInfo.setAddlParm(key, (String) secureParms.get(key));
		}

		String[] formatArgs = { strNavId, addlParms, "", contextRoot };
		if (_isPerformanceTestingEnabled) {
			formatArgs[2] = "&userAction=" + userAction;
		}

		String url = _amsServletLink.format(formatArgs);
		formatArgs[0] = response.encodeRedirectURL(url);
		formatArgs[1] = linkText;
		return _linkAsHref.format(formatArgs);
	}

	/**
	 * This method is used to create a navigational link. For form submissions, use the getFormInstanceXXX set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, HttpServletResponse response) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getLinkAsHref(userAction, linkText, "", response);
	}

	/**
	 * This method is used to create a navigational link to a specific frame. For form submissions, use the getFormInstanceXXX set
	 * of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param frameName
	 *            The HTML name of the target frame (the "name" tag's value within the "frame" tag).
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, HttpServletResponse response, String frameName) {
		return getLinkAsHref(userAction, linkText, "", response, frameName);
	}

	/**
	 * This method is used to create a navigational link to a specific frame. For form submissions, use the getFormInstanceXXX set
	 * of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param frameName
	 *            The HTML name of the target frame (the "name" tag's value within the "frame" tag).
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, HttpServletResponse response, String frameName,
			Hashtable secureParms) {
		return getLinkAsHref(userAction, linkText, "", response, frameName, secureParms);
	}

	/**
	 * This method is used to create a navigational link with secure parameters. For form submissions, use the getFormInstanceXXX
	 * set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param linkText
	 *            Text to display in the HTML to the user for the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String representation of the HTML HREF link code.
	 */
	public String getLinkAsHref(String userAction, String linkText, HttpServletResponse response, Hashtable secureParms) {
		// Return a string that represents the call to servlet necessary to submit a request ot the form
		return getLinkAsHref(userAction, linkText, "", response, secureParms);
	}

	/**
	 * This method is used to create the url for a navigational link For form submissions, use the getFormInstanceXXX set of methods
	 * instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return String
	 */
	public String getLinkAsUrl(String userAction, String addlParms, HttpServletResponse response) {
		// Return a string that represents the navigational url
		String[] formatArgs = { Integer.toString(getLinkInstance(userAction)), addlParms, "", contextRoot };
		if (_isPerformanceTestingEnabled) {

			formatArgs[2] = "&userAction=" + userAction;
		}

		String url = _amsServletLink.format(formatArgs);
		return response.encodeRedirectURL(url);
	}

	/**
	 * This method is used to create the url for a navigational link with secure parameters. For form submissions, use the
	 * getFormInstanceXXX set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String
	 */
	public String getLinkAsUrl(String userAction, String addlParms, HttpServletResponse response, Hashtable secureParms) {
		// Return a string that represents the navigational url
		String strNavId = Integer.toString(getLinkInstance(userAction));
		// Put secure parms in ClientRequestInfo
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(strNavId);
		Enumeration en = secureParms.keys();
		while (en.hasMoreElements()) {
			String key = (String) en.nextElement();
			reqInfo.setAddlParm(key, (String) secureParms.get(key));
		}

		String[] formatArgs = { strNavId, addlParms, "", contextRoot };
		if (_isPerformanceTestingEnabled) {

			formatArgs[2] = "&userAction=" + userAction;
		}

		String url = _amsServletLink.format(formatArgs);
		return response.encodeRedirectURL(url);
	}

	/**
	 * This method is used to create the url for a navigational link For form submissions, use the getFormInstanceXXX set of methods
	 * instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return String
	 */
	public String getLinkAsUrl(String userAction, HttpServletResponse response) {
		return getLinkAsUrl(userAction, "", response);
	}

	/**
	 * This method is used to create the url for a navigational link with secure parameters. For form submissions, use the
	 * getFormInstanceXXX set of methods instead.
	 *
	 * @param userAction
	 *            The logical action that results from selecting the link. Matches string defined in the Navigation.xml file.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @param secureParms
	 *            Additional parameters (data) to be passed to the page invoked by the link. These parameters are kept on the
	 *            server, they are not included in the links text. The getSecurePageParm method must be called to later access these
	 *            parameters.
	 * @return String
	 */
	public String getLinkAsUrl(String userAction, HttpServletResponse response, Hashtable secureParms) {
		return getLinkAsUrl(userAction, "", response, secureParms);
	}

	/**
	 * This method creates and registers a client request info object for a navigational link. This method is typically invoked
	 * through getLinkAsHref() or getLinkAsUrl(). Use these methods instead unless you have carefully read the documentation and
	 * understand how to use the navigation id returned.
	 *
	 * @param userAction
	 *            The logical action taken for this link Matches string defined in the Navigation.xml file.
	 * @return The unique identifying form number.
	 */
	public int getLinkInstance(String userAction) {
		// Use navigation manager to get navigation info for the client request object
		ClientRequestInfo reqInfo = null;
		NavigationInfo navInfo = null;
		try {
			switch (this.navigationType) {
			case CURR_PAGE_PLUS_ACTION_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(currPage, userAction);
				break;
			case ACTION_BASED_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(null, userAction);
				break;
			case CURR_PAGE_BASED_NAVIGATION:
				navInfo = NavigationManager.getNavMan().getNavInfo(currPage, null);
				break;
			default:
				LOG.error("FormManager ERROR: Invalid navigation type {}" , navigationType);
			}

			if (navInfo != null)
				reqInfo = new ClientRequestInfo(navInfo.getNextPage(), navInfo.getErrorPage(), null,
						navInfo.getAdvanceCurrentPage());
		} catch (Exception e) {
			LOG.error("FormManager Exception caught in FormManager.getLinkInstance", e);

		}

		// This registers the uniquely identifying code from bean manager and returns a uniquely identifying form number
		int navId = registerServletInvocation(reqInfo);
//		LOG.trace("FormManager.getLinkInstance: Created ni {} - {}+{}->{}", ""+navId, currPage, userAction, navInfo);
	    LOG.trace("FormManager.getLinkInstance: Created ni " + navId + " - " + currPage + "+" + userAction + "->" + navInfo.toString());
		return navId;
	}

	/**
	 * This method returns the next form number to execute.
	 *
	 * @return The next form number
	 */
	public String getNextFormNumber() {
		int temp = nextFormNumber;
		this.nextFormNumber = -1;
		return Integer.toString(temp);
	}

	/**
	 * This method is used to create the url for a navigational link when the next page is already known For form submissions, use
	 * the getFormInstanceXXX set of methods instead.
	 *
	 * @param nextPage
	 *            The next page the user will be forwarded to Matches string defined in the Navigation.xml file.
	 * @param addlParms
	 *            Additional parameters (data) to be passed to the page invoked by the link.
	 * @param response
	 *            The built in response object that is part of every jsp.
	 * @return String
	 */
	public String getUrlForLogicalPage(String logicalPage, String addlParms, HttpServletResponse response) {
		// Return a string that represents the navigational url
		ClientRequestInfo reqInfo = null;
		reqInfo = new ClientRequestInfo(logicalPage, null, null, true);
		int navId = registerServletInvocation(reqInfo);
		String[] formatArgs = { Integer.toString(navId), addlParms, "", contextRoot };
		String url = _amsServletLink.format(formatArgs);
		return response.encodeRedirectURL(url);
	}

	/**
	 * This method returns the original web page (excludes subsequent redirects back to AmsServlet).
	 *
	 * @return String
	 */
	public String getOriginalPage() {
		if (originalPage == null) {
			try {
				originalPage = NavigationManager.getNavMan().getDefaultPage();
			} catch (Exception e) {
			}
		}
		return originalPage;
	}

	/**
	 * This method gets a secure page parameter. These parameters are created with the option available to the getLinkXXX methods
	 * for parameters that you do not wish to be a part of the URL visible to the end user.
	 *
	 * @param parmName
	 *            secure parameter name
	 * @return The parameter value
	 */
	public String getSecurePageParm(String parmName) {
		if (this.mNavId == null) {
			return null;
		}
		// Get a handle to the client request object and then create the servlet invocation object
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(this.mNavId);

		// Handle the case where the navId is not found in the hashtable
		// This should only happen for an expired page or a user trying to
		// fudge the navId
		if (reqInfo == null) {
			LOG.debug("FormManager.getSecurePageParm WARNING: The navId " + this.mNavId + " was not found in the session.");
			return null;
		}

		return ((String) reqInfo.getAddlParms().get(parmName));
	}

	/**
	 * Get a secure page parameter for the given navigation id. This version of this method allows retrieval of secure page parms
	 * created on other pages. For example a child window can retrieve data from the parent using this mechanism.
	 *
	 * @param navId
	 *            - the specific navigation id for the form.
	 * @param parmName
	 *            secure parameter name
	 * @return The parameter value
	 */
	public String getSecurePageParm(String navId, String parmName) {
		if (navId == null) {
			return null;
		}
		// Get a handle to the client request object and then create the servlet invocation object
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(navId);

		// Handle the case where the navId is not found in the hashtable
		// This should only happen for an expired page or a user trying to fudge the navId
		if (reqInfo == null) {
			LOG.debug("FormManager.getSecurePageParm WARNING: The navId {} was not found in the session.", this.mNavId);
			return null;
		}

		return ((String) reqInfo.getAddlParms().get(parmName));
	}

	/**
	 * This method returns the Enterprise Java Bean server location as a string. This location is used for all interaction with EJB
	 * components.
	 *
	 * @return EJB server location
	 */
	public String getServerLocation() {
		return serverLocation;
	}

	public String determineAlternateFormName(String formName, HttpServletRequest req) {
		DocumentHandler doc = getFormTemplate(formName).getComponent("/alternateForms");
		if (doc != null) {
			String requestButtonName = req.getParameter("buttonName");
			if (requestButtonName == null)
				requestButtonName = "";

			Vector alternateFormDocs = doc.getFragments("/alternateForm");
			Enumeration enumer = alternateFormDocs.elements();
			while (enumer.hasMoreElements()) {
				DocumentHandler alternateFormDoc = (DocumentHandler) enumer.nextElement();
				String buttonName = alternateFormDoc.getAttribute("/buttonName");
				if (requestButtonName.equals(buttonName)) {
					return alternateFormDoc.getAttribute("/formName");
				} else {
					String value = req.getParameter(buttonName);
					if (value != null) {
						return alternateFormDoc.getAttribute("/formName");
					} else {
						value = req.getParameter(buttonName + ".x");
						if (value != null) {
							return alternateFormDoc.getAttribute("/formName");
						}
					}
				}
			}
		}

		return formName;
	}

	/**
	 * This method returns a servlet invocation object which was previously stored in the Form Manager for an upcoming form
	 * submission. The object is indentified by a form number. This is a zero based index number representing the order that the
	 * servlet parms were entered using the registerServletInvocation method.
	 * 
	 * @return AmsServletInvocation or null if object not found
	 */
	public AmsServletInvocation getServletInvocation(String navId, HttpServletRequest req) throws AmsException{
		LOG.debug("FormManager.getServletInvocation with navId {}", navId);
		this.mNavId = navId;
		if (_forms == null) {
			LOG.debug("FormManager.getServletInvocation WARNING: Need to initialize form manager");
			AmsServletInvocation servletInvocation = new AmsServletInvocation();
			servletInvocation.setNextPage(NavigationManager.getNavMan().getDefaultPage());
			return servletInvocation;
		}

		// Get the performance statistic object from the HTTP session
		// It should have been placed there by Dispatcher.jsp
		PerformanceStatistic perfStat = null;
		HttpSession theSession = req.getSession(false);
		perfStat = (PerformanceStatistic) theSession.getAttribute("performanceStatistic");

		// Get a handle to the client request object and then create the servlet invocation object
		ClientRequestInfo reqInfo = (ClientRequestInfo) _forms.get(navId);

		// Handle the case where the navId is not found in the hashtable
		// This should only happen for an expired page or a user trying to fudge the navId
		if (reqInfo == null) {
			LOG.debug("FormManager.getServletInvocation WARNING: The navId {} was not found in the session.", navId);
			if("UploadsHome".equalsIgnoreCase(this.originalPage)) throw new AmsException(); //if uploads servlets (i.e. non AMSServlet) dont have a valid ni, then throw exception
			AmsServletInvocation servletInvocation = new AmsServletInvocation();
			if (!this.atgDynamo) {
				servletInvocation.setNextPage("/AutoReroute.jsp");
			} else {
				try {
					servletInvocation.setNextPage(NavigationManager.getNavMan().getExceptionPage(this.originalPage));
				} catch (AmsException e) {
					LOG.error("Error occured setting nextpage",e);
				}
			}
			return servletInvocation;
		}

		String formName = reqInfo.getFormName();

		// Create a new servlet invocation
		AmsServletInvocation servletInvocation = new AmsServletInvocation();

		// Set the navigation information from the client request
		// NOTE: This will be overriden if it was specified in the FormManager.xml file
		servletInvocation.setNextPage(reqInfo.getNextPage());
		servletInvocation.setErrorPage(reqInfo.getErrorPage());
		servletInvocation.setAdvanceCurrentPage(reqInfo.getAdvanceCurrentPage());
		// If this is only navigation, there will not be a form name, so skip
		// getting the form template parameter values. We only need the navigational info.
		if (formName == null) {
			// Save the input data; we only erase it by default before the
			// next form submission, not on a navigational link
			servletInvocation.setAddToInputCache(true);

			if (perfStat != null) {
				// This is a link action, so set the performance statistic as such
				// and set the action to be the logical page to which the user is going
				perfStat.setAsLinkAction();
				perfStat.setAction(servletInvocation.getNextPage());
			}

			return servletInvocation;
		} else {
			formName = this.determineAlternateFormName(formName, req);
		}

		// Set the servlet invocation parameters from the form template
		servletInvocation.setMediator(getFormTemplate(formName).getAttribute("/mediator"));
		servletInvocation.setMapFiles(getFormTemplate(formName).getAttribute("/mapFiles"));

		if (perfStat != null) {
			// This is a form submit action, so set the performance statistic as such
			// and set the action to be the form name and the mediator name as the context
			perfStat.setAsSubmitAction();
			perfStat.setAction(formName);
			perfStat.setContext(servletInvocation.getMediator());
		}

		String nextPage = getFormTemplate(formName).getAttribute("/nextPage");
		if (nextPage != null) {
			servletInvocation.setNextPage(nextPage);
		}
		String errorPage = getFormTemplate(formName).getAttribute("/errorPage");
		if (errorPage != null) {
			servletInvocation.setErrorPage(errorPage);
		}
		servletInvocation.setBeansAsInput(getFormTemplate(formName).getAttribute("/beansAsInput"));
		servletInvocation.setDocCacheAsInput(getFormTemplate(formName).getAttribute("/docCacheAsInput"));
		servletInvocation.setBeansToPopulate(getFormTemplate(formName).getAttribute("/beansToPopulate"));
		servletInvocation.setXslStyleSheet(getFormTemplate(formName).getAttribute("/xslStyleSheet"));
		servletInvocation.setXslErrorSheet(getFormTemplate(formName).getAttribute("/xslErrorSheet"));
		String docCacheName = getFormTemplate(formName).getAttribute("/docCacheName");
		if (docCacheName != null) {
			servletInvocation.setDocCacheName(docCacheName);
		}
		String addToInputCache = getFormTemplate(formName).getAttribute("/addToInputCache");
		if (addToInputCache != null) {
			if (addToInputCache.equalsIgnoreCase("Yes")) {
				servletInvocation.setAddToInputCache(true);
			}
		}
		String multipleObjects = getFormTemplate(formName).getAttribute("/multipleObjects");
		if (multipleObjects != null) {
			if (multipleObjects.equalsIgnoreCase("Yes")) {
				servletInvocation.setMultipleObjects(true);
			}
		}
		String numberOfMultipleObjects = getFormTemplate(formName).getAttribute("/numberOfMultipleObjects");
		if (numberOfMultipleObjects != null) {
			int iNumber = 0;
			try {
				iNumber = Integer.parseInt(numberOfMultipleObjects);
				servletInvocation.setNumberOfMultipleObjects(iNumber);
			} catch (Exception e) {
				LOG.error("FormManager Exception caught in getFormInstance", e);
			}
		}

		// Add the servlet invocation parameters defined for this specific instance of the form
		servletInvocation.setAddlParms(reqInfo.getAddlParms());

		// Include any predefined additional parameters from the configuration file
		Hashtable addlParmsList = getFormAddlParms(formName);
		Enumeration enumer = addlParmsList.keys();
		while (enumer.hasMoreElements()) {
			String addlParmName = (String) enumer.nextElement();
			if (!(servletInvocation.getAddlParms().containsKey(addlParmName))) {
				String addlParmValue = (String) addlParmsList.get(addlParmName);
				servletInvocation.setAddlParm(addlParmName, addlParmValue);
			}
		}

		// Set extensible WebAction module information
		servletInvocation.setPreMedWebAction(getFormTemplate(formName).getAttribute("/preMedWebAction"));
		servletInvocation.setPostMedWebAction(getFormTemplate(formName).getAttribute("/postMedWebAction"));

		// If there will be a next form, store the form number of the next
		// form in the session so that the forwarded request to AmsServlet
		// will know what form is the next to execute. We do this instead of
		// trying to change the formNumber request parameter on the forwarded
		// request because JSP 1.0 does not support jsp:param on a jsp:forward yet

		// If a next form was specified, replace the next page parameter with a new form
		if (hasNextForm(formName)) {
			int iNavId = getFormInstance(getFormTemplate(formName).getAttribute("/nextForm"), reqInfo.getAddlParms());
			// Reset the navigation info on this newly created client request info
			ClientRequestInfo cri = (ClientRequestInfo) _forms.get(Integer.toString(iNavId));
			if (cri == null) {
				LOG.debug("FormManager.getServletInvocation Major problem...can't find the client request info just created");
			} else {
				cri.setNextPage(servletInvocation.getNextPage());
				cri.setErrorPage(servletInvocation.getErrorPage());
				this.setNextFormNumber(iNavId);
			}
			servletInvocation.setNextPage(_servletLocation.concat(".jsp"));
		}

		return servletInvocation;
	}

   //BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
   /*
	* This method returns the instance of the XSL processor used by this bean.
	* 

	* @return  XSLProcessor
	*/
   /*private XSLTProcessor getXslProcessor ()
   {
	  if (processor == null)
	  {
		 try
		 {
			processor = XSLTProcessorFactory.getProcessorUsingLiaisonName("org.apache.xalan.xpath.xdom.XercesLiaison");
		 }
		 catch (Exception e)
		 {
			LOG.error("",e);
		 }
	  }
	  return processor;
   }*/
   //BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * 
	 * @param
	 * @return
	 */
	private boolean hasNextForm(String formName) {
		return getFormTemplate(formName).getAttribute("/nextForm") != null;
	}

	public void init(HttpSession session, String servletLocation) {
		init(session, servletLocation, "");
	}

	/**
	 * This method must be called method before any other FormManager methods can be used. Note you can call this method a second
	 * time, but that it will erase any links or forms currently stored. Normally this only occurs on login situation.
	 * 
	 * It is safe to put the following at the top of all of your JSPs:
	 * <P/>
	 * &lt;jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"&gt; <BR/>
	 * &lt% <BR/>
	 * formMgr.init(request.getSession(true), this.getServletConfig().getServletContext(), "AmsServlet"); <BR/>
	 * %&gt <BR/>
	 * &lt;/jsp:useBean&gt;
	 * <P/>
	 *
	 * @param HttpSession
	 *            session
	 * @param String
	 *            servletLocation
	 * @param String
	 *            contextRoot - the context root of the web application in which this is running
	 * @return String
	 */
	public void init(HttpSession session, String servletLocation, String contextRoot) {
		JPylonProperties jPylonProperties = null;

		// Set the context root
		this.contextRoot = contextRoot;

		try {
			jPylonProperties = JPylonProperties.getInstance();
		} catch (Exception e) {
		}

		this._servletLocation = servletLocation;
		this._forms = new Hashtable();

		// Initialize the navigation manager
		try {
			int debugMode;
			String strDebugMode = jPylonProperties.getString("debugMode");
			if (strDebugMode == null) {
				debugMode = 0;
			} else {
				try {
					debugMode = Integer.parseInt(strDebugMode);
				} catch (Exception e) {
					debugMode = 0;
				}
			}

			String isPerformanceTestingEnabled;

			try {
				isPerformanceTestingEnabled = jPylonProperties.getString("isPerformanceTestingEnabled");
			} catch (AmsException amse) {
				isPerformanceTestingEnabled = "n";
			}

			if (isPerformanceTestingEnabled.equals("y")) {
				_isPerformanceTestingEnabled = true;
			} else {
				_isPerformanceTestingEnabled = false;
			}

			NavigationManager.getNavMan(debugMode);

			int pageExpirationCount;
			String strPageExpirationCount = jPylonProperties.getString("pageExpirationCount");
			if (strPageExpirationCount == null) {
				pageExpirationCount = 0;
			} else {
				try {
					pageExpirationCount = Integer.parseInt(strPageExpirationCount);
				} catch (Exception e) {
					pageExpirationCount = 0;
				}
			}
			requestInfoExpiration = pageExpirationCount + 1;
			// Adjust links for vendors that don't support JSP 1.0
			String EJBVendor;

			try {
				EJBVendor = jPylonProperties.getString("EJBVendor");
			} catch (AmsException amse) {
				EJBVendor = null;
			}

			if (EJBVendor != null) {
				if (EJBVendor.equalsIgnoreCase("ATG")) {
					this.atgDynamo = true;
					this._formInstanceAsHRef = new MessageFormat(
							"<A HREF=/".concat(_servletLocation).concat("?ni={0} {2}>{1}</A>"));
					this._formInstanceAsURL = new MessageFormat("/".concat(_servletLocation).concat("?ni={0}"));
					this._linkAsHref = new MessageFormat("<A HREF={0}>{1}</A>");
					this._amsServletLink = new MessageFormat("/AmsServlet?ni={0}{1}{2}");
				}
			}
			// Initialize the server side components
			serverLocation = jPylonProperties.getString("serverLocation");
			ServerDeploy serverDeploy = null;
			BeanManager beanMgr = (BeanManager) session.getAttribute("beanMgr");

			if (beanMgr == null || beanMgr.getUserLoginInfo() == null) {
				serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(serverLocation, "ServerDeploy");
			} else // we are using EJB security
			{
				serverDeploy = (ServerDeploy) EJBObjectFactory.createSecureClientEJB(serverLocation, "ServerDeploy",
						beanMgr.getUserLoginInfo().getLoginName(), beanMgr.getUserLoginInfo().getLoginPassword());
			}
			serverDeploy.init(serverLocation);
			serverDeploy.remove();
			// Initialize the web server components
			ReferenceDataManager.getRefDataMgr(serverLocation);
			// Initialize XSL stuff
			// this.xslFileLocation = amsServlet.getXslFileLocation ();
			try {
				this.xslFileLocation = jPylonProperties.getString("xslFileLocation");
			} catch (AmsException amse) {
				this.xslFileLocation = "";
			}
		} catch (Exception e) {
			LOG.error("Exception occured in FormManger", e);
		}

		// Initialize other navigation stuff
		// this.processor = null; //BSL Cocoon Upgrade 09/28/11 Rel 7.0 DELETE
		this.bLinksExpire = true;
		this.navigationType = CURR_PAGE_PLUS_ACTION_NAVIGATION;
		this.pageExpirationModality = this.TIME_BASED_EXPIRATION;
		this.currPageBasedExpireList = null;

		this.requestInfoExpirationCount = 0;
		this.getCurrPage();
		this.pageNumberServed = 0;
		this.requestInfoCount = 0;
		this.reqInfoStorageList = new Vector(requestInfoExpiration);
		// Initialize each vector
		for (int count = 0; count < requestInfoExpiration; count++) {
			reqInfoStorageList.addElement(new Vector());
		}
	}

	/**
	 * This method indicates whether there is a next form number.
	 *
	 * @return String form number
	 */
	public boolean isNextFormNumber() {
		return (!(nextFormNumber == -1));
	}

	/**
	 * This method sets a servlet invocation object for an upcoming form submission. The servlet invocation object is indentified by
	 * a form number, in case there are multiple forms on an upcoming page, this can be used to differentiate. The form number is
	 * assigned based on the order the method was called. The first setServletParms method call assigns a form number of 0, the next
	 * a form number of 1, etc.
	 * 
	 * @return int the newly assigned form number this servlet invocation
	 */
	public int registerServletInvocation(ClientRequestInfo reqInfo) {
		if (_forms == null) {
			LOG.warn("FormManager.registerServletInvocation WARNING: Need to initialize form manager");
			return 0;
		}

		// Generate unique navId
		StringBuilder buffer = new StringBuilder();
		buffer.append(requestInfoCount++);
		buffer.append(currPage);
		buffer.append(System.currentTimeMillis());
		int navId = buffer.toString().hashCode();

		// Store in hashtable
		_forms.put(String.valueOf(navId), reqInfo);
		// Store in navigation id tables for removal after page expiration
		if (this.pageExpirationModality == this.TIME_BASED_EXPIRATION) {
			if (this.bLinksExpire) {
				Vector navIdList = null;
				navIdList = (Vector) reqInfoStorageList.elementAt(requestInfoExpirationCount);
				navIdList.addElement(String.valueOf(navId));
			}
		} else {
			CurrPageStorage cps = (CurrPageStorage) this.currPageBasedExpireList.get(this.currPage);
			if (cps == null) {
				cps = new CurrPageStorage();
				this.currPageBasedExpireList.put(this.currPage, cps);
			}
			cps.addElement(String.valueOf(navId));
		}

		// Return the index of the last element added (the passed servlet Invocation)
		return navId;
	}

	public void setCurrPage(String currPage) {
		setCurrPage(currPage, true);
	}

	/**
	 * This method sets the current web page (used by NavigationManager). This method can be used in the case of a web site with
	 * multiple entry points (i.e. there may be more than one default page). In these cases, this method should be used at the top
	 * of the JSP to set the context for the page. Note that this is only relevant with navigation using current page as part of the
	 * criteria.
	 *
	 * @return void
	 * @param currPage
	 *            - String the current page
	 * @param advanceCurrentPage
	 *            - boolean whether to set the current page to currPage. There are actually two variables for the current page:
	 *            currPage and navigateToPage. currPage is the current page for further navigation. navigateToPage is the current
	 *            page that may and may not be used for further navigation. An example is ViewPDF which opens up a separate browser
	 *            window and there is no further navigation.
	 */
	public void setCurrPage(String currPage, boolean advanceCurrentPage) {
		// If this is a redirect to another form, save the original page name
		// for navigation later
		if (currPage != null) {
			if (!advanceCurrentPage) {
				this.navigateToPage = currPage;
			} else {
				if ((!(currPage.endsWith("Dispatcher.jsp"))) && (!(currPage.endsWith("AutoReroute.jsp")))
						&& (!(currPage.endsWith("IntermediatePage.jsp")))) {
					this.originalPage = currPage;
					this.pageNumberServed++;
					this.requestInfoCount = 0;
					// Erase client request info objects that are beyond n pages old
					// Increment the expiration count and reset if beyond size
					requestInfoExpirationCount++;
					if (requestInfoExpirationCount >= requestInfoExpiration) {
						requestInfoExpirationCount = 0;
					}
					// Erase any nav Ids that have expired
					if (_forms != null) {
						if (this.pageExpirationModality == this.TIME_BASED_EXPIRATION) {
							Vector navIdList = (Vector) reqInfoStorageList.elementAt(requestInfoExpirationCount);
							int navIdListSize = navIdList.size();
							for (int loop = 0; loop < navIdListSize; loop++) {
								String tempNavId = (String) navIdList.elementAt(loop);
								_forms.remove(tempNavId);
								LOG.trace("FormManager.setCurrPage: Removed ni {}", tempNavId);
							}
							navIdList.removeAllElements();
						} else {
							CurrPageStorage cps = (CurrPageStorage) this.currPageBasedExpireList.get(currPage);
							if (cps == null) {
								cps = new CurrPageStorage();
								this.currPageBasedExpireList.put(currPage, cps);
							}
							cps.nextPage(_forms);
						}
					}
				}
				this.currPage = currPage;
				this.navigateToPage = currPage;
				LOG.trace("FormManager.setCurrPage: Now currPage = {}", currPage);
			}
		}
	}

	/**
	 * This method sets the link expiration indicator. The default is true for memory conservation purposes.
	 *
	 * @param bLinksExpire
	 *            expiration indicator, true - links expire, false - links are permanent for session
	 * @return void
	 */
	public void setLinksExpire(boolean bLinksExpire) {
		this.bLinksExpire = bLinksExpire;
	}

	/**
	 * This method sets the navigation type. Valid values are static FormManager values CURR_PAGE_PLUS_ACTION_NAVIGATION,
	 * ACTION_BASED_NAVIGATION, and CURR_PAGE_BASED_NAVIGATION. The default is CURR_PAGE_PLUS_ACTION_NAVIGATION.
	 *
	 * @param navigationType
	 *            navigation type
	 */
	public void setNavigationType(int navigationType) {
		this.navigationType = navigationType;
	}

	/**
	 * This method sets the page expiration modality. Valid values are static FormManager values TIME_BASED_EXPIRATION and
	 * CURRENT_PAGE_BASED_EXPIRATION. Time based expiration uses the page expiration count and pages expire after the configured
	 * number of page requests. Current page based expiration means that only the most current instance of a given page in stored in
	 * memory. Thus, if you hit the back button to a previous instance of the same page, it will be expired. The default is
	 * TIME_BASED_EXPIRATION.
	 *
	 * @param pageExpirationModality
	 *            navigation type
	 * @return void
	 */
	public void setPageExpirationModality(int pageExpirationModality) {
		this.pageExpirationModality = pageExpirationModality;
		if (this.pageExpirationModality == CURRENT_PAGE_BASED_EXPIRATION) {
			if (this.currPageBasedExpireList == null) {
				this.currPageBasedExpireList = new Hashtable();
			}
		}
	}

	/**
	 * This method sets the next form number to execute
	 *
	 * @param nextFormNumber
	 *            form number
	 * @return void
	 */
	public void setNextFormNumber(int nextFormNumber) {
		this.nextFormNumber = nextFormNumber;
	}

	/**
	 * This method is used to store an XML document in the document cache.
	 *
	 * @param docCacheName
	 *            the document name
	 * @param doc
	 * @return DocumentHandler the document from the document cache
	 */
	public DocumentHandler storeInDocCache(String docCacheName, DocumentHandler doc) {
		_docCache.put(docCacheName, doc);
		return doc;
	}

	/**
	 * This method is used to remove an XML document from the document cache.
	 *
	 * @param docCacheName
	 *            the document name
	 */
	public void removeFromDocCache(String docCacheName) {
		_docCache.remove(docCacheName);
	}

	/**
	 * This method creates a navigational link to a specific document with secure parameters.
	 *
	 * The link is based on the private _documentLink variable and expects the following link arguments: the producer that will
	 * create the xml data document, the encoded transaction oid, the user's locale, and the bank prefix.
	 *
	 * 
	 * @return java.lang.String - The Html Href
	 * @param linkText
	 *            java.lang.String - The text to display in the link
	 * @param frameName
	 *            java.lang.String - The frame the link opens to
	 * @param linkArgs
	 *            java.lang.String[] - Arguments for the link, see above.
	 * @param response
	 *            javax.servlet.http.HttpServletResponse - The Http response
	 */
	public String getDocumentLinkAsHrefInFrame(String linkText, String frameName, String[] linkArgs, HttpServletResponse response) {

		// Rkazi CR-710 Rel 8.0 02/10/2012 modified to get URL from generic method - Start
		String url = getDocumentLinkAsURL(linkText, frameName, linkArgs, response);
		// Rkazi CR-710 Rel 8.0 02/10/2012 modified to get URL from generic method - End

		String formatArgs[] = { response.encodeRedirectURL(url), linkText, frameName };

		return _linkAsHrefwFrame.format(formatArgs);
	}

   //BSL Cocoon Upgrade 10/03/11 Rel 7.0 BEGIN
   /*
    * Inner class used to capture output messages from the PDF generation
    * process.  If this message listener did not exist, then the messages
    * would just be output to stdout.  
    *
    */
   /*public class jPylonMessageListener implements MessageListener
    {
	public void processMessage(MessageEvent messageevent)
         {
		// For some strange reason, the Apache FOP classes create
		// an event containing an error that is only a newline
		// To avoid these blank lines to be printed, ignore them.
		if( (messageevent.getMessageType() == MessageEvent.ERROR) &&
                    (!messageevent.getMessage().startsWith("\n") ) )
	                    LOG.debug(messageevent.getMessage());
		else
 			    LOG.debug(messageevent.getMessage());
		
		return;
         }
     }*/
   //BSL Cocoon Upgrade 10/03/11 Rel 7.0 END

	/**
	 * This method creates a navigational link to a specific document with secure parameters.
	 *
	 * The link is based on the private _documentLink variable and expects the following link arguments: the producer that will
	 * create the xml data document, the encoded transaction oid, the user's locale, and the bank prefix.
	 *
	 * Added as part of CR-710 Rel 8.0
	 * 
	 * @return java.lang.String - The Html Href
	 * @param linkText
	 *            java.lang.String - The text to display in the link
	 * @param frameName
	 *            java.lang.String - The frame the link opens to
	 * @param linkArgs
	 *            java.lang.String[] - Arguments for the link, see above.
	 * @param response
	 *            javax.servlet.http.HttpServletResponse - The Http response
	 */
	public String getDocumentLinkAsURLInFrame(String linkText, String frameName, String[] linkArgs, HttpServletResponse response) {

		String url = getDocumentLinkAsURL(linkText, frameName, linkArgs, response);

		return response.encodeRedirectURL(url);
	}

	/**
	 * This method is used to format the passed in param array into link url. This type of link is useful to pass information to the
	 * cocoon based producers used to generate PDF's. Added as part of CR-710 Rel 8.0
	 * 
	 * @param linkText
	 * @param frameName
	 * @param linkArgs
	 * @param response
	 * @return link as URL string.
	 */
	private String getDocumentLinkAsURL(String linkText, String frameName, String[] linkArgs, HttpServletResponse response) {
		String newLinkArgs[] = new String[5];

		newLinkArgs[0] = linkArgs[0];
		newLinkArgs[1] = linkArgs[1];
		newLinkArgs[2] = linkArgs[2];
		newLinkArgs[3] = linkArgs[3];
		newLinkArgs[4] = contextRoot;

		String url = _documentLink.format(newLinkArgs);

		return url;
	}

}