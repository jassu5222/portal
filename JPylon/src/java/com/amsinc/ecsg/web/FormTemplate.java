package com.amsinc.ecsg.web;

import java.util.*;

/**
 * FormTemplate is a representation of a specific form as specified in the
 * FormatManager.xml.  Previously we stored the FormManager.xml in the
 * XmlConfigFileCache in addition to a copy of each form specific xml in the
 * FormManager's internal form templates cache -- alternatively, here we
 * read the form manager file once and cache instances of FormTemplate class
 * in the FormManager form template cache -- this saves memory and reduces 
 * need to parse that xml continuously.
 */
public class FormTemplate implements java.io.Serializable
{
   private String id = null;
   private String mediator = null;
   private boolean addToInputCache = false;
   private String mapFiles = null;
   private String userAction = null;
   private String nextPage = null;
   private String errorPage = null;
   private boolean multipleObjects = false;
   private int numberOfMultipleObjects = -1; //-1 means not defined
   private String preMedWebAction = null;
   private String postMedWebAction = null;
   private String nextForm = null; //is this used?
   
   private List alternateFormList = null;
   private List addlParmList = null;
   
   public class AlternateForm {
       private String buttonName = null;
       private String formName = null;
       
       public AlternateForm() {      
       }
       
       public String getButtonName() { return buttonName; }
       public void setButtonName(String x) { buttonName=x; }
       public String getFormName() { return formName; }
       public void setFormName(String x) { formName=x; }
   }

   public class AddlParm {
       private String name = null;
       private String value = null;
       
       public AddlParm() {      
       }
       
       public String getName() { return name; }
       public void setName(String x) { name=x; }
       public String getValue() { return value; }
       public void setValue(String x) { value=x; }
   }

   /**
	* Default constructor for FormTemplate.
	*/
   public FormTemplate() {
   }
   
   public String getId() { return id; }
   public void setId(String x) { id=x; }
   public String getMediator() { return mediator; }
   public void setMediator(String x) { mediator=x; }
   public boolean isAddToInputCache() { return addToInputCache; }
   public void setAddToInputCache(boolean x) { addToInputCache=x; }
   public String getMapFiles() { return mapFiles; }
   public void setMapFiles(String x) { mapFiles=x; }
   public String getUserAction() { return userAction; }
   public void setUserAction(String x) { userAction=x; }
   public String getNextPage() { return nextPage; }
   public void setNextPage(String x) { nextPage=x; }
   public String getErrorPage() { return errorPage; }
   public void setErrorPage(String x) { errorPage=x; }
   public boolean isMultipleObjects() { return multipleObjects; }
   public void setMultipleObjects(boolean x) { multipleObjects=x; }
   public boolean isNumberOfMultipleObjectsDefined() { 
       if (numberOfMultipleObjects<0) { 
           return true; 
       } else { 
           return false; 
       } 
   }
   public int getNumberOfMultipleObjects() { return numberOfMultipleObjects; }
   public void setNumberOfMultipleObjects(int x) { numberOfMultipleObjects=x; }
   public String getPreMedWebAction() { return preMedWebAction; }
   public void setPreMedWebAction(String x) { preMedWebAction=x; }
   public String getPostMedWebAction() { return postMedWebAction; }
   public void setPostMedWebAction(String x) { postMedWebAction=x; }
   public String getNextForm() { return nextForm; }
   public void setNextForm(String x) { nextForm=x; }

   /**
    * Get the alternate form list.
    * 
    * @return
    */
   public List getAlternateFormList() { 
       if ( alternateFormList == null ) {
           alternateFormList = new ArrayList();
       }
       return alternateFormList; 
   }
   /**
    * Add an alternate form to the alternate form list.
    * 
    * @param buttonName
    * @param formName
    */
   public void addAlternateForm(String buttonName, String formName) {
       if ( alternateFormList == null ) {
           alternateFormList = new ArrayList();
       }
       AlternateForm af = new AlternateForm();
       af.setButtonName(buttonName);
       af.setFormName(formName);
       alternateFormList.add(af);
   }

   /**
    * Get the additional parameter list.
    * 
    * @return
    */
   public List getAddlParmList() { 
       if ( addlParmList == null ) {
           addlParmList = new ArrayList();
       }
       return addlParmList; 
   }
   /**
    * Add an additional parameter to the list.
    * 
    * @param name
    * @param value
    */
   public void addAddlParm(String name, String value) {
       if ( addlParmList == null ) {
           addlParmList = new ArrayList();
       }
       AddlParm ap = new AddlParm();
       ap.setName(name);
       ap.setValue(value);
       addlParmList.add(ap);
   }
}