package com.amsinc.ecsg.web;

/*
 * @(#)WebAction
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import javax.servlet.http.*;
import java.util.*;


public interface WebAction
{
  // Object Properties
  public void act(AmsServletInvocation reqInfo,
                  BeanManager beanMgr,
                  FormManager formMgr,
                  ResourceManager resMgr,
                  String serverLocation,
                  HttpServletRequest request,
                  HttpServletResponse response,
                  Hashtable inputParmMap,
                  DocumentHandler inputDoc)
       throws AmsException;   
}
