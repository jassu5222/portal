<%--
*******************************************************************************
                               Account Balance Page

  Description:  **ToDo**
  
  

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
 String            cashSelection                   = null;
 StringBuffer      newLink                          = new StringBuffer();
 String selectedOrg    = ""; 
 String searchListViewName = "";
 StringBuffer dynamicWhereClause = new StringBuffer();
 
	final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations",
                                                                   TradePortalConstants.TEXT_BUNDLE);
 if (current2ndNav.equals(TradePortalConstants.NAV__ACCT_BALANCE))
   {
      cashSelection = request.getParameter("cashSelection");

      if (cashSelection == null)
      {
         cashSelection = (String) session.getAttribute("cashSelection");

         if (cashSelection == null)
         {
            cashSelection = TradePortalConstants.NAV__ACCT_BALANCE;
         }
      }
   }   
   
   session.setAttribute("cashSelection",     cashSelection);
   
   
  //IAZ CM-451 12/26/08 Begin 
  //check if this is a bank user: if so, use CustAccountListView to display all accounts
  //for this customer.  if not, use AccountListView to display accounts for this user only 
  
  //IAZ CM-451 02/25/09 Begin
  //Use userSession.hasSavedUserSession() to determine if bank/subsidiary user
  
  /*String sqlString = "select name from client_bank, users where user_oid = " + userSession.getUserOid() + " and p_owner_org_oid = organization_oid";
  DocumentHandler bankUser = DatabaseQueryBean.getXmlResultSet(sqlString, false);
  dynamicWhereClause = new StringBuffer();
  
  if (bankUser == null)
  {
   	searchListViewName = "AccountListView.xml";
    dynamicWhereClause.append("and b.p_user_oid = ");
    dynamicWhereClause.append(userOid);   	
  }
  else
  {
   	searchListViewName = "CustAccountListView.xml";
    dynamicWhereClause.append("and a.p_owner_oid = ");
    dynamicWhereClause.append(userOrgOid);   	
  }*/  
 
   //RKAZI HRUL062032745 06/24/2011 Start
  	String clientBankOid = userSession.getClientBankOid();
	QueryListView queryListView   = null;
	StringBuffer sqlQuery = new StringBuffer();

	sqlQuery.append("select allow_pay_by_another_accnt");
	sqlQuery.append(" from client_bank");
	sqlQuery.append(" where organization_oid = ");
	sqlQuery.append(clientBankOid);

	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
	//out.println("QUERY: "+sqlQuery.toString());
	queryListView.setSQL(sqlQuery.toString());
	queryListView.getRecords();

	DocumentHandler result = queryListView.getXmlResultSet();
	String allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
  
  //RKAZI HRUL062032745 06/25/2011 End
  
  if (userSession.hasSavedUserSession())
  {
    //out.println("uooid " + userOrgOid); 
    searchListViewName = "CustAccountListView.xml";
    dynamicWhereClause.append("and a.p_owner_oid = ");
    dynamicWhereClause.append(userOrgOid);  
    //RKAZI HRUL062032745 06/24/2011 Start
    if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
	    dynamicWhereClause.append(" and a.othercorp_customer_indicator != '");
	    dynamicWhereClause.append(TradePortalConstants.INDICATOR_YES);
	    dynamicWhereClause.append("' ");
    }
    //RKAZI HRUL062032745 06/24/2011 End
  }
  else 
  {
    searchListViewName = "AccountListView.xml";
    dynamicWhereClause.append("and b.p_user_oid = ");
    dynamicWhereClause.append(userOid);   
    //RKAZI XXX 06/24/2011 Start
    if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
	    dynamicWhereClause.append(" and a.othercorp_customer_indicator != '");
	    dynamicWhereClause.append(TradePortalConstants.INDICATOR_YES);
	    dynamicWhereClause.append("' ");
	//RKAZI XXX 06/24/2011 End
    }
  }
  //IAZ CM-451 12/26/08 and 02/25/09 End   
    
    selectedOrg = request.getParameter("historyOrg");
	      if (selectedOrg == null)
	      {
	         selectedOrg = (String) session.getAttribute("historyOrg");
	         if (selectedOrg == null)
	         {
	            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
	         }
	      }

      	 session.setAttribute("historyOrg", selectedOrg);

      	 selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
   
 
   //dynamicWhereClause = new StringBuffer();
   //dynamicWhereClause.append("and b.p_user_oid = ");
   //dynamicWhereClause.append(userOid);   

	
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
     <tr>
    <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
      <p class="ListTextWhite">
      <%=resMgr.getText("Accounts.Header1", TradePortalConstants.TEXT_BUNDLE)%> 
      </p>
      </td>
          <th rowspan=2 valign="middle">
          
          <%
         newLink.append(formMgr.getFormInstanceAsURL("cashManagementViaIntermediatePageForm"));
         newLink.append("&current2ndNav=");
         newLink.append(TradePortalConstants.NAV__ACCT_BALANCE);
        %>
         <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="RefreshBalanceButton" />
            <jsp:param name="image" value="common.RefreshBalanceImg" />
            <jsp:param name="text"  value="common.RefreshBalanceText" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="140" />

         </jsp:include>
          
          
          </th>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    <tr> 
    <td width="15" nowrap>&nbsp;</td>
      
      <td  nowrap colspan =2>
      <p class="ListTextWhite">
      <%=resMgr.getText("Accounts.Header2", TradePortalConstants.TEXT_BUNDLE)%>
      </p>
      </td>
    </tr>
  </table>
     
        
        
   <table> 
 
   <tr>
   <td class="ColorBeige" valign="top"><jsp:include page="/cashmanagement/CashMgmtTab.jsp"/></td>

   <td width ="100%" valign="top">  
    <jsp:include page="/common/TradePortalListView.jsp">
      <%-- IAZ 12/26/08 Replace with dynamic list jsp:param name="listView" value="AccountListView.xml"/ --%>
      <jsp:param name="listView" value="<%=searchListViewName%>"/>       
       <jsp:param name="whereClause2"   value='<%= dynamicWhereClause.toString() %>' />
      <jsp:param name="userType"       value='<%= userSecurityType %>' />
      <jsp:param name="userTimezone"       value='<%= userSession.getTimeZone()%>' />
      <jsp:param name="maxScrollHeight" value="316"/> 	
    </jsp:include>
   </td>
   </tr>
   </table>


  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td width="15" nowrap>&nbsp;</td>
          <td width="100%" height="30" valign="middle">&nbsp;</td>
    </tr>
  </table>
  
 

  
