<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%
   String invoiceDateDay = null;
   String invoiceDateMonth = null;
   String invoiceDateYear = null; 
   String payment_date = null;
   DocumentHandler  doc = formMgr.getFromDocCache();  
   String globalNavigationTab = userSession.getCurrentPrimaryNavigation();
   String formName = "ApplyPaymentDateToSelectionOnDetailForm";
   //BSL CR710 02/16/2012 Rel8.0 BEGIN - update for grouping
   String fromInvoiceGroupDetail = (String)session.getAttribute("fromInvoiceGroupDetail");
   String currentTab = (String)session.getAttribute("currentInvoiceMgmtTab");
   if (!TradePortalConstants.INDICATOR_YES.equals(fromInvoiceGroupDetail) &&
		   TradePortalConstants.INV_GROUP_TAB.equals(currentTab)) {
      formName = "ApplyPaymentDateToGroupSelectionOnDetailForm";
   }
   //BSL CR710 02/16/2012 Rel8.0 END
   String cancelLink=formMgr.getLinkAsUrl("goToInvoiceManagement", response); 
%>

<!--script -->
<script LANGUAGE="JavaScript">
  
   function updateINVPaymentDate() {
		
	var index = document.forms[0].invoiceDateDay.selectedIndex;
	var day = document.forms[0].invoiceDateDay.options[index].value;
	
	index = document.forms[0].invoiceDateMonth.selectedIndex;
	var month = document.forms[0].invoiceDateMonth.options[index].value;
	
	index = document.forms[0].invoiceDateYear.selectedIndex;
	var year = document.forms[0].invoiceDateYear.options[index].value;    
	if (day != '-1' || month != '-1' || year != '-1') 
	{
             document.forms[0].payment_date.value = month + '/' + day + '/' + year;
	}
  }
 </script> 


<%-- check for security --%>


<%-- display the header with the navigation bar when coming from the listview, without otherwise --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
   		value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<!-- script LANGUAGE="JavaScript"-->

  <%-- Display Invoice Header --%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr> 
      <td width="15" class="BankColor" nowrap height="25"><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
      <td class="BankColor" valign="middle" align="left" width="302" nowrap height="25"> 
      <p class="ControlLabelWhite"><%=resMgr.getText("UploadInvoice.Heading", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td class="BankColor" width="600" height="25" align="center" valign="middle"> 
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25"></td>
      <td width="15" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>

<form method="post" name="<%=formName%>" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <input type=hidden value="<%=payment_date%>" name="payment_date">
 <%= formMgr.getFormInstanceAsInputField(formName) %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td colspan=2>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <p class="ListText"><%=resMgr.getText("UploadInvoice.AssignPaymenttoSeleted", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
           <%=TPDateTimeUtility.getDayDropDown("invoiceDateDay", "",
             	resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE), "onChange='updateINVPaymentDate();'")%> 
           <img src="/portal/images/Blank_15.gif" width="15" height="15">
           <%=TPDateTimeUtility.getMonthDropDown("invoiceDateMonth", "", "en_US", 
                resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE), "onChange='updateINVPaymentDate();'")%> 
           <img src="/portal/images/Blank_15.gif" width="15" height="15">
           <%=TPDateTimeUtility.getYearDropDown("invoiceDateYear", "",
                resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE), "onChange='updateINVPaymentDate();'")%> 
           <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan=3>&nbsp;</td>
    </tr>
  </table>
 
  <%-- Display the apply payment date and Cancel banner --%>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
   <tr> 
    <td class="BankColor" width="100%">&nbsp;</td>
    <td> 
     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
     <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="15" nowrap>&nbsp;</td>
            <td>
                 <jsp:include page="/common/RolloverButtonSubmit.jsp"  >
			   <jsp:param name="showButton" value="true" />
                  <jsp:param name="name" value="<%=TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE%>" />
                  <jsp:param name="image" value='common.UploadInvoiceApplyPaymentDateImg' />
                  <jsp:param name="text" value='UploadInvoiceAction.UploadInvoiceApplyPaymentDateText' />
                  <jsp:param name="width" value="96" />
                  <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE%>" />
			  </jsp:include>
		  </td>	  
		  <td width="15" nowrap>&nbsp;</td>
            <td>
                 <jsp:include page="/common/RolloverButtonLink.jsp">
			   <jsp:param name="showButton" value="true" />
                  <jsp:param name="name" value="CancelButton" />
                  <jsp:param name="image" value='common.CancelImg' />
                  <jsp:param name="text" value='common.CancelText' />
                  <jsp:param name="width" value="96" />
                  <jsp:param name="link" value="<%=java.net.URLEncoder.encode(cancelLink)%>" />
			  </jsp:include>
		  </td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
     </table>
   </td>
  </tr>
</table>

</form>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>

