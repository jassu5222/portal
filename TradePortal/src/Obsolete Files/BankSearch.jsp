
`<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
				 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
Debug.debug("***START********************BANK*SEARCH**************************START***");
/******************************************************************************
 * BankSearch.jsp
 *
 * Filtering:
 * This page originally displays a listview containing all possible Bank/Branch Code and branch address for a given bank,
 * but user may also choose to narrow down the results by searching by City, Province, Bank Name, or Branch Name 
 * which will filter the existing results and link that redisplays the page with the filter text passed as a url parameter.
 * JavaScript is used to dynamically build that link.
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String returnAction     = null;
String onLoad           = "document.FilterForm.bankName.focus();";
String city		    = null;
String province		    = null;
String bankName		    = null;
String branchName	    = null;
String bankBranchCode   = null; //Vasavi CR 573 05/19/10 Add
String accCountry	    = "";
String bankType 	    = "";

String paymentMethodCode    = "";
String paymentMethodType    = "";

StringBuffer where      		 = new StringBuffer();
StringBuffer searchCriteria      	 = new StringBuffer();
StringBuffer sql 			 = new StringBuffer();
StringBuffer countrySQL			 = new StringBuffer();

Hashtable secureParams = new Hashtable();

DocumentHandler doc;

// Get the document from the cache.  We'll use it to repopulate the screen if the
// user was entering data with errors.
doc = formMgr.getFromDocCache();

Debug.debug("doc from cache is " + doc.toString());

returnAction    	= doc.getAttribute("/In/BankSearchInfo/ReturnAction");
paymentMethodCode   	= doc.getAttribute("/In/DomesticPayment/payment_method_type");
bankType		= doc.getAttribute("/In//BankSearchInfo/Type");

sql.append("SELECT ADDL_VALUE FROM REFDATA WHERE TABLE_TYPE = '").append(TradePortalConstants.PMT_METHOD).append("'"); 
sql.append(" AND CODE = '").append(SQLParamFilter.filter(paymentMethodCode)).append("'");

DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql.toString(), false);
if (result != null)
	paymentMethodType = result.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");

AccountWebBean account = (AccountWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.AccountWebBean", "Account");
if (TradePortalConstants.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethodCode))
	account.setAttribute("account_oid", doc.getAttribute("/In/Terms/credit_account_oid"));
else
	account.setAttribute("account_oid", doc.getAttribute("/In/Terms/debit_account_oid"));

account.getDataFromAppServer();
    
countrySQL.append("SELECT ADDL_VALUE FROM REFDATA WHERE CODE = '").append(account.getAttribute("bank_country_code")).append("'");
countrySQL.append(" AND TABLE_TYPE = '").append(TradePortalConstants.COUNTRY_ISO3116_3).append("'");
    
DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(countrySQL.toString(), false);
if (countryDoc != null) 
	accCountry = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");	

//IAZ/VShah IR SHUK011146898 and 01/20/10 - Begin
if (TradePortalConstants.ORDERING_PARTY_BANK.equals(bankType))	
{
    String comparInd = "<>";
	if (TradePortalConstants.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethodCode))
		comparInd = "=";
		
	//where.append(" where bank_branch_rule.address_country = '").append(SQLParamFilter.filter(accCountry)).append("'");
	where.append(" where bank_branch_rule.PAYMENT_METHOD ").append(comparInd).append( "'")
					.append(SQLParamFilter.filter(TradePortalConstants.DIRECT_DEBIT_INSTRUCTION)).append("'");	
	
}
else
{
	where.append(" where bank_branch_rule.PAYMENT_METHOD = '").append(SQLParamFilter.filter(paymentMethodCode)).append("'");
}

//if (paymentMethodType.equals(TradePortalConstants.PAYMENT_METHOD_TYPE_ELLECTR) || paymentMethodType.equals(TradePortalConstants.PAYMENT_METHOD_TYPE_CHEQUE) || paymentMethodType.equals(TradePortalConstants.PAYMENT_METHOD_TYPE_COLLECTN))
if (!TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethodCode))		//IAZ IR- POUK020758766 02/08/10 CHG
{
		where.append(" and bank_branch_rule.address_country = '").append(SQLParamFilter.filter(accCountry)).append("'");
}

// CR-486 only beneficiary bank and order party bank can use bank branches that have non-win1252 unicode characters.
if (!TradePortalConstants.ORDERING_PARTY_BANK.equals(bankType) && !TradePortalConstants.BENEFICIARY_BANK.equals(bankType))	
{
	where.append(" and (unicode_indicator = 'N' or unicode_indicator is null)");
}


//IAZ/VShah IR SHUK011146898 and 01/20/10 - End

//Debug.debug("where is " + where.toString());

city			= doc.getAttribute("/In/BankSearchInfo/City");
province		= doc.getAttribute("/In/BankSearchInfo/Province");
bankName		= doc.getAttribute("/In/BankSearchInfo/BankName");
branchName		= doc.getAttribute("/In/BankSearchInfo/BranchName");
bankBranchCode 	= doc.getAttribute("/In/BankSearchInfo/BankBranchCode"); 					//Vasavi/IAZ CR 573 05/19/10 Add

// Get user entered search criteria
// For performance reason, only search if at least one search field is not empty.
if (!InstrumentServices.isBlank(bankName)
        || !InstrumentServices.isBlank(bankBranchCode)										//Vasavi CR 573 05/19/10 Add
        || !InstrumentServices.isBlank(branchName)
        || !InstrumentServices.isBlank(city)
        || !InstrumentServices.isBlank(province)) {
        
	if (!InstrumentServices.isBlank(bankName) )
	{
		if (!searchCriteria.toString().equals("")) searchCriteria.append(" AND");			//Vasavi CR 573 05/19/10 Chg
		searchCriteria.append("upper(bank_branch_rule.bank_name) like unistr('")
		              .append(StringFunction.toUnistr(SQLParamFilter.filter(bankName.toUpperCase()))).append("%')");
	}
	  //Vasavi CR573 Begin
	if (!InstrumentServices.isBlank(bankBranchCode) )
	{
		if (!searchCriteria.toString().equals("")) searchCriteria.append(" AND ");
		searchCriteria.append("upper(bank_branch_rule.bank_branch_code) like '")
		              .append(SQLParamFilter.filter(bankBranchCode.toUpperCase())).append("%'");
	}
	 //Vasavi CR573 End
	if (!InstrumentServices.isBlank(branchName))
	{
		if (!searchCriteria.toString().equals("")) searchCriteria.append(" AND");			//Vasavi CR 573 05/19/10 Chg
	    searchCriteria.append(" upper(bank_branch_rule.branch_name) like unistr('")
	                  .append(StringFunction.toUnistr(SQLParamFilter.filter(branchName.toUpperCase()))).append("%')");
	}
	if (!InstrumentServices.isBlank(city))
	{
		if (!searchCriteria.toString().equals("")) searchCriteria.append(" AND");			//Vasavi CR 573 05/19/10 Chg
		searchCriteria.append(" upper(bank_branch_rule.address_city) like unistr('")
		              .append(StringFunction.toUnistr(SQLParamFilter.filter(city.toUpperCase()))).append("%')");
	}
	if (!InstrumentServices.isBlank(province))
	{
		if (!searchCriteria.toString().equals("")) searchCriteria.append(" AND");			//Vasavi CR 573 05/19/10 Chg
		searchCriteria.append(" upper(bank_branch_rule.address_state_province) like unistr('")
		              .append(StringFunction.toUnistr(SQLParamFilter.filter(province.toUpperCase()))).append("%')");
	}	
}

if(!searchCriteria.toString().equals(""))
{
		where.append(" and ( ").append(searchCriteria).append(" )");
}

 
%>
<div class="pageHeader">
<div class="pageContent">
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<script src="<%=userSession.getdojoJsPath()%>/datagrid.js" type="text/javascript"></script>
<!-- <script src="<%=userSession.getdojoJsPath()%>/common.js" type="text/javascript"></script> -->
<script LANGUAGE="JavaScript">

  function getCheckedRadioButton()
  {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>
    numElements = document.ChooseBankForm.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.ChooseBankForm.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.ChooseBankForm.selection.length;
        <%--
        //Handle the case where there is only one radio button
        //Handle the case where there are two or more radio buttons
        --%>
        for (i=0; i<size; i++)
        {
            if (document.ChooseBankForm.selection[i].checked)
            {
                return document.ChooseBankForm.selection[i].value;
            }
        }
        if (document.ChooseBankForm.selection.checked)
        {
            return document.ChooseBankForm.selection.value;
        }

    }
    return 0;
  }

  function confirmSelection()
  {
    if (getCheckedRadioButton()==0)
    {
        alert('<%=resMgr.getText("BankSearch.SelectBankWarning",TradePortalConstants.TEXT_BUNDLE)%>');
        return false;
    }
    return true;
  }

  //IAZ IR-PPUK012164630 02/16.2010 Begin	   
  function resetStartRow()
  {
  	document.forms.FilterForm.NewSearch.value = 'Y'
  	return true;
  }
  //IAZ IR-PPUK012164630 02/16.2010 End	
  
  
</script>

<%
/*************************************************************
 * FilterForm
 * This form contains the filter text field and submit button.
 *************************************************************/
%>


<div clas="pageMain">
<div class="pageContent">
<jsp:include page="/common/PageHeader.jsp">
	              <jsp:param name="titleKey" value="Bank/Branch Search"/>
	              
	        </jsp:include>

			<div class="subHeaderDivider"></div>
<form method="post" name="FilterForm" action="<%=formMgr.getSubmitAction(response)%>">
<div class="formContentNoSidebar"> <!-- Form Content Area starts here -->
<%-- //IAZ IR-PPUK012164630 02/16.2010 Begin --%>
<input type=hidden name=NewSearch value="N">

<%
if ((request.getParameter("NewSearch") != null)&&(request.getParameter("NewSearch").equals("Y")))
{
 	session.removeAttribute("BankSearchListView.xml");
}

%>
<%-- //IAZ IR-PPUK012164630 02/16.2010 End --%>

  <input type=hidden value="" name=buttonName>
  <div>
<%= widgetFactory.createLabel("", "BankName:", false, false, false, "inline") %>
<%= widgetFactory.createTextField("BankName", "", bankName, "120", false, false, false, " width=10 ", "", "inline") %>

 <%= widgetFactory.createLabel("", "BankBranchCode:", false, false, false, "inline") %>
 <%= widgetFactory.createTextField("BankBranchCode", "", bankBranchCode, "16", false, false, false, " width=10 ", "", "inline") %>
 
 <%= widgetFactory.createLabel("", "BranchName:", false, false, false, "inline") %>
<%= widgetFactory.createTextField("BranchName", "", branchName, "70", false, false, false, " width=10 ", "", "inline") %>
    
 <div align="right">
               <button data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
			Search
    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			searchBank();return false;
    	</script>
  		</button>
          </div>    
          <div style="clear:both;"></div>
     </div>
	 <div>
	 <%= widgetFactory.createLabel("", "City:", false, false, false, "inline") %>
	 <%= widgetFactory.createTextField("City", "", city, "35", false, false, false, " width=10 ", "", "inline") %>
	 
	 <%= widgetFactory.createLabel("", "Province:", false, false, false, "inline") %>     
	 <%= widgetFactory.createTextField("Province", "", province, "16", false, false, false, " width=10 ", "", "inline") %>
     
	 
	 <div style="clear:both;"></div>
	 </div>

       
       <%= formMgr.getFormInstanceAsInputField("BankSearchFilterForm", secureParams) %>
        </div>
       
       <%= formMgr.getFormInstanceAsInputField("BankSearchFilterForm", secureParams) %>
<!--        </div> -->
</form>
<form method="post" name="ChooseBankForm" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>

	<jsp:include page="/common/dataGrid.jsp">
		<jsp:param name="dataGridId"   value="bankSearchGrid"/>
  		<jsp:param name="dataGrid" value="BankSearchListView" />
				
	</jsp:include>

 <%= formMgr.getFormInstanceAsInputField("ChooseBankForm", secureParams) %>
    
<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("bankSearchGrid","BankSearchDataGrid",null);
  String gridLayout = dgFactory.createGridLayout("BankSearchDataGrid");  
  
%>
<%= gridHtml %>

<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var initSearchParms = "bankType=<%=bankType%>&paymentMethodCode=<%=paymentMethodCode%>" +
	"&accCountry=<%=accCountry%>" ;
console.log("initSearchParms="+initSearchParms);

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var bankSearchGrid = 
    createDataGrid("bankSearchGrid", "BankSearchDataView",
                    gridLayout, initSearchParms);

  
  
  
  <%--provide a search function that collects the necessary search parameters--%>
  function chooseBank() {
    require(["dojo/dom"],
      function(dom){
    	var rowKeys = getSelectedGridRowKeys("bankSearchGrid");
    	var formName = 'ChooseBankForm';
        var buttonName = 'Search';
        submitFormWithParms(formName, buttonName, "selection", rowKeys);
        
      });
  }

  
  function searchBank() {
      require(["dojo/dom"],
        function(dom){
    	  var searchParms = initSearchParms;

          var bankName = dom.byId("BankName").value;
          var bankBranchCode = dom.byId("BankBranchCode").value;
          var branchName = dom.byId("BranchName").value;
          var city = dom.byId("City").value;
          var province = dom.byId("Province").value;
          
          if (bankName != "")
        	  searchParms=searchParms+"&BankName="+bankName+"&";
          if (bankBranchCode != "")
        	  searchParms=searchParms+"&BankBranchCode="+bankBranchCode+"&";
          if (branchName != "")
        	  searchParms=searchParms+"&BranchName="+branchName+"&";
          if (city != "")
        	  searchParms=searchParms+"&City="+city+"&";
          if (province != "")
        	  searchParms=searchParms+"&Province="+province+"&";
          
          console.log("searchParams= "+searchParms);
          searchDataGrid("bankSearchGrid", "BankSearchDataView",
                         searchParms);
        });
    }

</script>

</form>

</div>
</div>

</body>
</html>      	