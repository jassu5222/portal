<%--
*******************************************************************************
                               Cash Mgmt Payment File Upload List View

  Description:  **ToDo**
  
  

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<SCRIPT LANGUAGE="JavaScript">



	var blink_speed=200;
	var i=0;
	
function restrictAttach(form) {
  var extArr = new Array(".txt");
  var allowUpload = true; // CR 597
  var file = form.PaymentDataFilePath.value
  if (!file) return false;
  
  // manohar - CR 597 starts- The file does not need to have a .txt extension, but it must always be a plain text file
    //while (file.indexOf("\\") != -1)
     // file = file.slice(file.indexOf("\\") + 1);
      //ext = file.slice(file.indexOf(".")).toLowerCase();
      //for (var i = 0; i < extArr.length; i++) {
      //if (extArr[i] == ext) {
      //	allowUpload = true; 
      //	break;
      //}
    //} manohar - CR 597 ends
   if (allowUpload) {
	   form.action ="<%= response.encodeURL(request.getContextPath()+ "/transactions/PaymentFileUploadServlet.jsp") %>";
	   form.enctype="multipart/form-data";
	   form.encoding="multipart/form-data";
	   document.getElementById('UploadButtonDiv').style.visibility = 'hidden';
	   document.getElementById('UploadButtonDiv').style.display = 'none';	
	   document.getElementById('UploadMessage').style.visibility = 'visible';
			document.getElementById('UploadMessage').style.display = 'block';
	   Blink('UploadMessage');	
	   return true;
   }
   else {
       alert('<%= resMgr.getTextEscapedJS("PaymentUploadLogDetail.ValidFileExtionsionText", TradePortalConstants.TEXT_BUNDLE) %>');
       return false;
   }
}

function Blink(layerName){

		
			if(i%2==0) {
				eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="visible"');
			} else {
 				eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="hidden"');
			}
		 
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}

</script>

     
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="100%" height="40">&nbsp;</td>
      <td>
<%
         newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
         newLink.append("&currentTab=");
         newLink.append(TradePortalConstants.PAYMENT_FILE_UPLOAD);
         newLink.append("&workflow=");
         newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
%>
         <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="RefreshThisPageButton" />
            <jsp:param name="image" value="common.RefreshThisPageImg" />
            <jsp:param name="text"  value="common.RefreshThisPageText" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="140" />
         </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">
    <tr>
        <td width="5" nowrap>&nbsp;</td>
        <td nowrap align="left" valign="middle" nowrap>
          <p class="ListText">
            <%= resMgr.getText("PaymentFileUpload.InstructionsText", TradePortalConstants.TEXT_BUNDLE) %>&nbsp;&nbsp;
          </p>
      </td>
    </tr>
  </table>
  
  <div id="UploadButtonDiv" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td nowrap align="left" valign="bottom"> 
          <p class="ControlLabel">
            <%= resMgr.getText("LocateUploadPaymentFile.PaymentDataFileLocation", TradePortalConstants.TEXT_BUNDLE) %>
 	  </p>
        </td>
      </tr>
      <tr>
        <td width="15" nowrap>&nbsp;</td>
        <td align="left" valign="bottom"> 
           
            <%=InputField.createFileField("PaymentDataFilePath", "70", "ListText", false) %>
            &nbsp;&nbsp;&nbsp;
           <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton"   value="true" />
              <jsp:param name="name"         value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
              <jsp:param name="image"        value='common.UploadFileImg' />
              <jsp:param name="text"         value='common.UploadFileText' />
              <jsp:param name="width"        value="96" />
              <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
              <jsp:param name="imageName"    value="UploadFileButton" />
              <jsp:param name="extraTags"    value="return restrictAttach(document.forms[0])" />
           </jsp:include>
           
       		  
       </td>
       
      </tr>
    
    

      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    </div>
    <div id="UploadMessage" style="display: none;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
      </tr>
      <tr>
                  <td>&nbsp;</td>
                  <td>
           		  
                  			    <p class="ControlLabel">
                  				   <font color="blue">
                  				      <%= resMgr.getText("PaymentUploadLogDetail.Uploading", TradePortalConstants.TEXT_BUNDLE) %>.....
                  				   </font>
                  				</p>
    			  </td>
            </tr>
            <tr>
	            <td>&nbsp;</td>
	            <td>&nbsp;</td>
      </tr>
      <tr>
      	            <td>&nbsp;</td>
      	            <td>&nbsp;</td>
      </tr>
            </table>
            </div>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle" class="ListHeaderTextWhite">
          <%= resMgr.getText("PaymentFileUpload.Show", TradePortalConstants.TEXT_BUNDLE) %>
      </td>
      <td align="left" valign="middle" class="ListText">
<%
        StringBuffer prependText = new StringBuffer();
        prependText.append(resMgr.getText("PaymentFileUpload.WorkFor", 
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(ALL_WORK);
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        extraTags.append("onchange=\"location='");
        extraTags.append(formMgr.getLinkAsUrl("goToCashManagement", response));
        extraTags.append("&amp;currentTab=");
        extraTags.append(TradePortalConstants.PAYMENT_FILE_UPLOAD);
        extraTags.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

        String defaultValue = "";
        String defaultText  = "";

        if(!userSession.hasSavedUserSession())
         {
           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
           defaultText = MY_WORK;
         }

        out.print(InputField.createSelectField("Workflow", 
                                               defaultValue, 
                                               defaultText, dropdownOptions.toString(), 
                                               "ListText", false, extraTags.toString()));
%>
      </td>
      <td width="100%" height="40">&nbsp;</td>
    </tr>
  </table>

  <jsp:include page="/common/TradePortalListView.jsp">
      <jsp:param name="listView"     value="CashMgmtPaymentFileUploadListView.xml"/>
      <jsp:param name="whereClause2" value='<%= dynamicWhereClause.toString() %>' />
      <jsp:param name="userType"     value='<%= userSecurityType %>' />
      <jsp:param name="userTimezone" value='<%=userSession.getTimeZone()%>' />
      <jsp:param name="maxScrollHeight" value="316"/>
  </jsp:include>

  <%--  The table below displays the Delete buttons --%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="BankColor">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="37" nowrap align="left" valign="top">
        <img src="/portal/images/UpIndicatorArrow.gif" width="37" height="27">
      </td>
      <td width="5" nowrap>
        <img src="/portal/images/Blank_4x4.gif" width="4" height="4">
      </td>
      <td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
<table>
<tr>
      <td width="15" nowrap>&nbsp;</td>
      <td width="264"  valign=bottom>           

<%
        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.INSTRUMENT_DELETE))
        {
%>
          <jsp:include page="/common/RolloverButtonSubmit.jsp">
             <jsp:param name="name"         value="Delete" />
             <jsp:param name="image"        value='common.DeleteRejectedItemsImg' />
             <jsp:param name="text"         value='common.DeleteRejectedItemsText' />
             <jsp:param name="extraTags"    value="return confirmDelete()" />
             <jsp:param name="width"        value="152" />
             <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_DELETE_PAYMENT_UPLOAD%>" />
          </jsp:include>
<%
        }
        else
        {
           out.print("&nbsp;");
        }
%>
      </td>

      </tr></table></td>
      <td width="100%">&nbsp; </td>
    </tr>
  </table>
  </td>
   </tr>
   </table>
