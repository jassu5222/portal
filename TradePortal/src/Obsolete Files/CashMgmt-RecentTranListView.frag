<%--
*******************************************************************************
                               Cash Mgmt Authorized List View

  Description:  **ToDo**
  
  

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%
  String xmlStr = "<DocRoot><ResultSetRecord ID=\"0\"><ACCTTRNID>9000007</ACCTTRNID><DATEPOSTED>2007-12-31</DATEPOSTED><DESCRIPTION>check 3 deposit</DESCRIPTION><RUNNINGBAL>23123.55</RUNNINGBAL><AMOUNT>903</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord><ResultSetRecord ID=\"1\"><ACCTTRNID>9000008</ACCTTRNID><DATEPOSTED>2007-11-31</DATEPOSTED><DESCRIPTION>check 2 deposit</DESCRIPTION><RUNNINGBAL>22</RUNNINGBAL><AMOUNT>802.11</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord><ResultSetRecord ID=\"2\"><ACCTTRNID>9000009</ACCTTRNID><DATEPOSTED>2007-10-31</DATEPOSTED><DESCRIPTION>check 1 deposit</DESCRIPTION><RUNNINGBAL>21</RUNNINGBAL><AMOUNT>901</AMOUNT><CURRENCYCODE>USD</CURRENCYCODE></ResultSetRecord></DocRoot>";
%>
<%
   String currency111 = "CAD";
   String account111 = "123423annn444"; 
%>
    
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
<%
      // Display Organizations dropdown or the single organization.

//      if ((SecurityAccess.hasRights(userSecurityRights,
//                                    SecurityAccess.VIEW_CHILD_ORG_WORK)))
//      {
%>
        <td nowrap align="left" valign="middle">
          <p class="ListHeaderTextWhite">
            <%= resMgr.getText("CMRecentTransactions.Text",
                               TradePortalConstants.TEXT_BUNDLE) %>
            <%= currency111 %>
            <%= account111 %>
          </p>
        </td>
        <td width="100%" height="30">&nbsp;</td>
        <td width="20" nowrap valign="bottom">&nbsp;</td>
<%
//      }

%>
      <td>
		&nbsp;
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

  <jsp:include page="/common/TradePortalListView.jsp">
      <jsp:param name="listView"     	value="RecentTransactionListView.xml"/>
      <jsp:param name="documentContent" value='<%= xmlStr %>' />
      <jsp:param name="userType"     	value='<%= userSecurityType %>' />
  </jsp:include>


