<%--
*******************************************************************************
                            Cash Management - Transaction Page

  Description: **ToDo**
     
 

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
		final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", 
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", 
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  
                                                                   TradePortalConstants.TEXT_BUNDLE);
/*        // NSX - PRUK080636392 - 08/16/10 - Begin
   final String      STATUS_ALL                   = resMgr.getText("common.StatusAll",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_STARTED               = resMgr.getText("common.StatusStarted",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_READY                 = resMgr.getText("common.StatusReady",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_PARTIAL               = resMgr.getText("common.StatusPartial",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_AUTH_FAILED           = resMgr.getText("common.StatusAuthFailed",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_REJECTED_BY_BANK      = resMgr.getText("common.StatusRejectedByBank",
                                                                  TradePortalConstants.TEXT_BUNDLE);
   //final String      STATUS_REQ_AUTHENTCN         = resMgr.getText("common.StatusReqAuthentication",	//IAZ CM-451 01/22/09
   //                                                                TradePortalConstants.TEXT_BUNDLE);	//IAZ CM-451 01/22/09
                                                                   

   final String      STATUS_ACTIVE                = resMgr.getText("common.StatusActive",
                                                                   TradePortalConstants.TEXT_BUNDLE);
   final String      STATUS_INACTIVE              = resMgr.getText("common.StatusInactive",
                                                                   TradePortalConstants.TEXT_BUNDLE);
*/         // NSX - PRUK080636392 - 08/16/10 - End
    //Vshah - 12/08/2008 - Cash Management History Tab - Begin

   String			 instrumentId 		   = "";
   String			 refNum			   = "";
   String			 amountFrom     	   = "";
   String			 amountTo                  = "";
   String 			 currency                  = "";
   String			 searchCondition 	   = TradePortalConstants.SEARCH_ALL_INSTRUMENTS; 
   String 			 searchListViewName        = "CashMgmtTransactionHistoryListView.xml";
   String			 searchType		   = null;
   String 			 loginLocale 		   = userSession.getUserLocale();
   String 			 instrumentType		   = null;
   Vector            		 instrumentTypes           = null;
   String 			 options                   = "";
   String 			 otherParty                = "";
   String 			 link 			   = null;
   String 			 linkText 		   = null;   
   StringBuffer 		 newSearchCriteria         = new StringBuffer();                                                          
   //Vshah - 12/08/2008 - Cash Management History Tab - End

   DocumentHandler   hierarchyDoc                 = null;
   StringBuffer      dynamicWhereClause           = new StringBuffer();
   StringBuffer      dropdownOptions              = new StringBuffer();
   int               totalOrganizations           = 0;
   StringBuffer      extraTags                    = new StringBuffer();
   StringBuffer      newLink                      = new StringBuffer();
   StringBuffer      newUploadLink                = new StringBuffer();
         
   String            userDefaultWipView           = null; 
   String            selectedWorkflow             = null;
   String            selectedStatus               = "";
   
   String            tabPendingOn                 = TradePortalConstants.INDICATOR_NO;
   String linkParms   = null;   
   StringBuffer dynamicWhere = dynamicWhereClause;
   String tabAuthorizedOn = null;
   String            selectedOrg                  = null;
   String loginRights = userSecurityRights; // PPRAKASH IR NGUJ013070808 

   userSession.setGlobalNavigationTab(resMgr.getText("NavigationBar.CashManagement", TradePortalConstants.TEXT_BUNDLE));
   
   userDefaultWipView           = userSession.getDefaultWipView();
   
   currentTab = request.getParameter("currentTab");
   
   if (currentTab == null)
   {
      currentTab = (String) session.getAttribute("currentCashMgmtTab");      

      if (currentTab == null)
      {
       currentTab = TradePortalConstants.CASH_MGMT_TRANSACTION;
      }
   }
    // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   StringBuffer sqlQuery = new StringBuffer();
   sqlQuery.append("select organization_oid, name");
   sqlQuery.append(" from corporate_org");
   sqlQuery.append(" where activation_status = '");
   sqlQuery.append(TradePortalConstants.ACTIVE);
   sqlQuery.append("' start with organization_oid = ");
   sqlQuery.append(userOrgOid);
   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
   sqlQuery.append(" order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false);
	  
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
    

   if((perfStat != null) && (perfStat.isLinkAction()))
           perfStat.setContext(currentTab);

   // ***********************************************************************
   // Data setup for the Pending Transaction tab
   // ***********************************************************************
   if (currentTab.equals(TradePortalConstants.CASH_MGMT_TRANSACTION))
   {
      tabPendingOn = TradePortalConstants.INDICATOR_YES;
      formName     = "CashMgmt-TransactionForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;

      selectedStatus = request.getParameter("transStatusType");

      if (selectedStatus == null) 
      {
         selectedStatus = (String) session.getAttribute("transStatusType");
      }

      if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_PENDING);
      }
      else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
      }
      else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
      }
      else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
      }
      else if (TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK);
      }    
      //MDB CR-564 12/16/10 Rel 6.1 Begin         
      else if (TradePortalConstants.TRANS_STATUS_VERIFIED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED);
      }    
      else if (TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX);
      }    
      //MDB CR-564 12/16/10 Rel 6.1 End
      // DK CR-640 Rel7.1 Begins
      else if (TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED);
      } 
      else if (TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(selectedStatus) )
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE);
      }  
   	  // DK CR-640 Rel7.1 Ends
      else // default is ALL
      {
         statuses.addElement(TradePortalConstants.TRANS_STATUS_STARTED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTHORIZE_PENDING);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE);
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN);   //IAZ CM-451 01/22/09         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK);         
         //MDB CR-564 12/16/10 Rel 6.1 Begin         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED);         
         statuses.addElement(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX);         
         //MDB CR-564 12/16/10 Rel 6.1 End
		 // DK CR-640 Rel7.1 Begins
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED); 
		 statuses.addElement(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE);
		 // DK CR-640 Rel7.1 Ends
         selectedStatus = TradePortalConstants.STATUS_ALL;
      }

      session.setAttribute("transStatusType", selectedStatus);

      // Build a where clause using the vector of statuses we just set up.

      dynamicWhereClause.append(" and a.transaction_status in (");

      numStatuses = statuses.size();

      for (int i=0; i<numStatuses; i++) {
          dynamicWhereClause.append("'");
          dynamicWhereClause.append( (String) statuses.elementAt(i) );
          dynamicWhereClause.append("'");

          if (i < numStatuses - 1) {
            dynamicWhereClause.append(", ");
          }
      }

      dynamicWhereClause.append(") ");


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      if (selectedWorkflow.equals(MY_WORK))
      {
         dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);
      }
      else if (selectedWorkflow.equals(ALL_WORK))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedWorkflow);
      }
      
      //CR-586 Vshah [BEGIN]
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
       	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
       	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
      }
      //CR-586 Vshah [END]

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/pend_trans.htm",  resMgr, userSession);

      onLoad.append("document.TransactionListForm.Workflow.focus();");
%>

      <%-- **************** JavaScript for Pending tab only *********************  --%>

      <script language="JavaScript">

     
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) // Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

      

      </script>

<%
   } // End data setup for Pending tab
   
   else // ****************************************************************************
   // Data setup for the Future Value Date Transaction tab   VS CR 609 11/23/10 Begin
   // *********************************************************************************
   if (currentTab.equals(TradePortalConstants.FUTURE_TRANSACTIONS))
   {
      tabPendingOn = TradePortalConstants.INDICATOR_YES;
      formName     = "CashMgmt-TransactionForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      if (selectedWorkflow.equals(MY_WORK))
      {
         dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);
      }
      else if (selectedWorkflow.equals(ALL_WORK))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedWorkflow);
      }
      
      //CR-586 Vshah [BEGIN]
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
       	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
       	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
      }
      //CR-586 Vshah [END]

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/futureValueTransaction.htm",  resMgr, userSession);

      onLoad.append("document.TransactionListForm.Workflow.focus();");
%>

      <%-- **************** JavaScript for Future tab only *********************  --%>

      <script language="JavaScript">

    
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("FutureValueTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) // Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

    

      </script>

<%
   } // End data setup for Future Value tab
   //VS CR 609 11/23/10 End
   
   else if (currentTab.equals(TradePortalConstants.AUTHORIZED_TRANSACTIONS))
   {
      formName = "CashMgmt-TransactionForm";
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
      // The organization dropdown displays dynamically based on security and the
      // number of orgs.  If it will display, set focus to that field.
      if ((SecurityAccess.hasRights(userSecurityRights, 
                                    SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1)) {
        onLoad.append("document.TransactionListForm.Organization.focus();");
      }

      selectedOrg = request.getParameter("org");

      if (selectedOrg == null)
      {
         selectedOrg = (String) session.getAttribute("org");

         if (selectedOrg == null)
         {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }

      session.setAttribute("org", selectedOrg);

      selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());

      if (selectedOrg.equals(ALL_ORGANIZATIONS))
      {
         dynamicWhereClause.append("and b.a_corp_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and b.a_corp_org_oid = ");
         dynamicWhereClause.append(selectedOrg);
      }
      
      //CR-586 Vshah [BEGIN]--- 
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
      	 dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
      	 dynamicWhereClause.append(" t.confidential_indicator is null) ");
      } 	
      //CR-586 Vshah [END]
	  
      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/auth_trans.htm",  resMgr, userSession);
   }
   else // ****************************************************************************
   // Data setup for the Payment Upload File tab     MDB CR-564 Rel6.1 12/7/10 Begin
   // *********************************************************************************
   if (currentTab.equals(TradePortalConstants.PAYMENT_FILE_UPLOAD))
   {
      tabPendingOn = TradePortalConstants.INDICATOR_YES;
      formName     = "CashMgmt-TransactionForm";

      // Based on the statusType dropdown, build the where clause to include one or more
      // statuses.
      Vector statuses = new Vector();
      int numStatuses = 0;


      // Based on the workflow dropdown, build the where clause to include transactions at
      // various levels of the organization.
      selectedWorkflow = request.getParameter("workflow");

      if (selectedWorkflow == null)
      {
         selectedWorkflow = (String) session.getAttribute("workflow");

         if (selectedWorkflow == null)
         {
            if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
            {
               selectedWorkflow = userOrgOid;
            }
            else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
            {
               selectedWorkflow = ALL_WORK;
            }
            else
            {
               selectedWorkflow = MY_WORK;
            }
            selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
         }
      }

      session.setAttribute("workflow", selectedWorkflow);

      selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

      if (selectedWorkflow.equals(MY_WORK))
      {
         dynamicWhereClause.append("and a_assigned_to_user_oid = "); //Rkazi - 05/24/2011 IR POUL051854748 changed
								     // a_user_oid to a_assigned_to_user_oid.	
         dynamicWhereClause.append(userOid);
      }
      else if (selectedWorkflow.equals(ALL_WORK))
      {
         dynamicWhereClause.append("and a_owner_org_oid in (");
         dynamicWhereClause.append(" select organization_oid");
         dynamicWhereClause.append(" from corporate_org");
         dynamicWhereClause.append(" where activation_status = '");
         dynamicWhereClause.append(TradePortalConstants.ACTIVE);
         dynamicWhereClause.append("' start with organization_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
      }
      else
      {
         dynamicWhereClause.append("and a_owner_org_oid  = ");
         dynamicWhereClause.append(selectedWorkflow);
      }
      
      //CR-586 Vshah [BEGIN]
      //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
      //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
      if (TradePortalConstants.INDICATOR_NO.equals(confInd))
      {
       	 dynamicWhereClause.append(" and ( confidential_indicator = 'N' OR ");
       	 dynamicWhereClause.append(" confidential_indicator is null) ");
      }
      //CR-586 Vshah [END]

      helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/futureValueTransaction.htm",  resMgr, userSession);

      onLoad.append("document.TransactionListForm.Workflow.focus();");
%>

      <%-- **************** JavaScript for Future tab only *********************  --%>

      <script language="JavaScript">

      
         <%--
         // This function is used to display a popup message confirming
         // that the user wishes to delete the selected items in the
         // listview.
         --%>
         function confirmDelete()
         {
           var   confirmMessage = "<%=resMgr.getText("FutureValueTransactions.PopupMessage", 
                                                     TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage)) // Modified by LOGANATHAN - 10/10/2005 - IR LNUF092749769
             {
                formSubmitted = false;
                return false;
             }
           else
           {
              return true;
           }
        }

    

      </script>

<%
   } // End data setup for Payment File Upload Tab
   //MDB CR-564 Rel 6.1 12/7/10 End

   //Vshah - 12/08/2008 - Cash Management History Tab - Begin
   else if (currentTab.equals(TradePortalConstants.INSTRUMENT_HISTORY))
   {
      
       formName     = "CashMgmt-TransactionForm";
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
       
       
       // Determine the organization to select in the dropdown
       selectedOrg = request.getParameter("historyOrg");
       if (selectedOrg == null)
       {
       	  selectedOrg = (String) session.getAttribute("historyOrg");
       	  if (selectedOrg == null)
          {
            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
            
          }
        }

      	session.setAttribute("historyOrg", selectedOrg);
	selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
       
   	String newSearch = request.getParameter("NewSearch");
	String newDropdownSearch = request.getParameter("NewDropdownSearch");
    	Debug.debug("New Search is " + newSearch);
      	Debug.debug("New Dropdown Search is " + newDropdownSearch);
   	searchType   = request.getParameter("SearchType");
   		
   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
      	{
         	session.removeAttribute(searchListViewName);

         	// Default search type for instrument status on the transactions history page is ACTIVE.
         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
     	}
   		
   		if (searchType == null) 
	    {
   			searchType = ListViewHandler.getSearchTypeFromState(searchListViewName, session);
        	if (searchType == null)
        	{
          		  searchType = TradePortalConstants.SIMPLE;
        	}
   			
   			 if ((newDropdownSearch != null) && (newDropdownSearch.equals(TradePortalConstants.INDICATOR_YES)))
	        {
             	  session.removeAttribute(searchListViewName);
         	}
      	}	
      	  else
     	{
         	// this is probably a new search type clicked from the link, so remove
         	// the old search type
         	session.removeAttribute(searchListViewName);
        }
        
        linkParms = "";
      	linkText  = "";
        
         if (searchType.equals(TradePortalConstants.ADVANCED))
      	{
         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
         	link       = formMgr.getLinkAsUrl("goToCashManagement", linkParms, response);
         	linkText   = resMgr.getText("common.search.basic", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
      	}
      	else
      	{
        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
         	link       = formMgr.getLinkAsUrl("goToCashManagement", linkParms, response);
         	linkText   = resMgr.getText("common.search.advanced", TradePortalConstants.TEXT_BUNDLE);

         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
     	 }

	 // Determine the organizations to select for listview
     	 if (selectedOrg.equals(ALL_ORGANIZATIONS))
     	 {
        	 // Build a comma separated list of the orgs
		 DocumentHandler orgDoc = null;
        	 StringBuffer orgList = new StringBuffer();

	         for (int i = 0; i < totalOrganizations; i++)
	         {
	    	        orgDoc = (DocumentHandler) orgListDoc.get(i);
        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
            		if (i < (totalOrganizations - 1) )
            		{
	             		 orgList.append(", ");
    	       		}
        	 }

         	 dynamicWhere.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
      	 }
      	 else
     	 {
        	 dynamicWhere.append(" and i.a_corp_org_oid = "+  selectedOrg);
      	 }
     	 
     	 // Based on the statusType dropdown, build the where clause to include one or more
     	 // instrument statuses.
     	 Vector statuses = new Vector();
     	 int numStatuses = 0;

      	selectedStatus = request.getParameter("instrStatusType");

     	 if (selectedStatus == null) 
     	 {
       		  selectedStatus = (String) session.getAttribute("instrStatusType");
     	 }

      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
      	{
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
      	}

      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
      	{
       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      	}

      	else // default is ALL (actually not all since DELeted instruments
           // never show up (handled by the SQL in the listview XML)
      	{
        	 selectedStatus = TradePortalConstants.STATUS_ALL;
      	}

     	session.setAttribute("instrStatusType", selectedStatus);
     	
     	
     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
     	{
        	 dynamicWhere.append(" and i.instrument_status in (");

	         numStatuses = statuses.size();

    	     for (int i=0; i<numStatuses; i++)
    	     {
        	     dynamicWhere.append("'");
            	 dynamicWhere.append( (String) statuses.elementAt(i) );
             	 dynamicWhere.append("'");

	             if (i < numStatuses - 1)
	             {
               		dynamicWhere.append(", ");
             	 }
         	 }

         	 dynamicWhere.append(") ");
      	}
      	
      	//CR-586 Vshah [BEGIN]---
      	//For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
	//on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
	if (TradePortalConstants.INDICATOR_NO.equals(confInd))
	{
	    //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
		dynamicWhereClause.append(" and ( i.confidential_indicator = 'N' OR ");
		dynamicWhereClause.append(" i.confidential_indicator is null) ");
	}
  	//CR-586 Vshah [END]
%>
		<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-TransType.frag" %>
		<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-SearchParms.frag" %>
		
		<%-- JavaScript for Instrument History tab to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
      <script LANGUAGE="JavaScript">
     
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
     
      </script>

<%      	
     }
     
   Debug.debug("form " + formName);
   
   //Vshah - 12/08/2008 - Cash Management History Tab - End   
%>

<%-- ********************* HTML for page begins here *********************  --%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td width="15" nowrap>&nbsp;</td>
          <td width="100%" height="30" valign="middle"><p class="ListTextWhite">
          <%=resMgr.getText("CashMgmtPendingTransactions.PendingTransactionText", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p></td>
    </tr>
  </table>
      

  
  <%
   if ( (currentTab.equals(TradePortalConstants.CASH_MGMT_TRANSACTION)) ||
        (currentTab.equals(TradePortalConstants.FUTURE_TRANSACTIONS)) || //MDB IR RAUK120855907 12/10/10 
        (currentTab.equals(TradePortalConstants.PAYMENT_FILE_UPLOAD)) ) //MDB CR-564 Rel6.1 12/21/10
   {
      // Store the user's oid and security rights in a secure hashtable for the form
      secureParms.put("SecurityRights", userSecurityRights);
      secureParms.put("UserOid",        userOid);
   }
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> &nbsp; </tr>
    <tr>
      <td class=ListText width="20" nowrap>&nbsp;</td>
      <%                    
        //MDB CR-564 Rel 6.1 12/16/10 R6.1 Begin
	CorporateOrganizationWebBean corpOrg  = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
	corpOrg.getById(userOrgOid);

	if ( (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_UPLOAD_BULK_FILE)) &&
	     (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_domestic_payments"))) &&
	     (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_payment_file_upload"))) &&
	     ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_AUTHORIZE)) ||
	      (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_CREATE_MODIFY)) ||
	      (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_DELETE)) ||
	      (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_ROUTE))) )
   	{
		 currentTabLink = "currentTab=" + TradePortalConstants.PAYMENT_FILE_UPLOAD;
	         if (currentTab.equals(TradePortalConstants.PAYMENT_FILE_UPLOAD)) {
	            tabOn = TradePortalConstants.INDICATOR_YES;
	         } else {
	            tabOn = TradePortalConstants.INDICATOR_NO;
	         }
      %>
      <jsp:include page="/common/Tab.jsp">
          <jsp:param name="tabOn"       value="<%= tabOn %>" />
          <jsp:param name="linkTextKey" value="PaymentFileUpload.TabName" />
          <jsp:param name="action"      value="goToCashManagement" />
          <jsp:param name="linkParms"   value="<%= currentTabLink %>" />
      </jsp:include>
      <%
        //MDB CR-564 Rel 6.1 12/16/10 R6.1 End
   	}
        currentTabLink = "currentTab=" + TradePortalConstants.CASH_MGMT_TRANSACTION;
         
      %>
      <%
         currentTabLink = "currentTab=" + TradePortalConstants.CASH_MGMT_TRANSACTION;
         if (currentTab.equals(TradePortalConstants.CASH_MGMT_TRANSACTION)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
      <jsp:include page="/common/Tab.jsp">
          <jsp:param name="tabOn"       value="<%= tabOn %>" />
          <jsp:param name="linkTextKey" value="PendingTransactions.TabName" />
          <jsp:param name="action"      value="goToCashManagement" />
          <jsp:param name="linkParms"   value="<%= currentTabLink %>" />
      </jsp:include>
      <%                    
      
        currentTabLink = "currentTab=" + TradePortalConstants.FUTURE_TRANSACTIONS;
         
      %>
      <%
         currentTabLink = "currentTab=" + TradePortalConstants.FUTURE_TRANSACTIONS;
         if (currentTab.equals(TradePortalConstants.FUTURE_TRANSACTIONS)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
      <jsp:include page="/common/Tab.jsp">
          <jsp:param name="tabOn"       value="<%= tabOn %>" />
          <jsp:param name="linkTextKey" value="FutureTransactions.TabName" />
          <jsp:param name="action"      value="goToCashManagement" />
          <jsp:param name="linkParms"   value="<%= currentTabLink %>" />
      </jsp:include>      
      
      <%
         //currentTabLink = "currentTab=" + TradePortalConstants.AUTHORIZED_TRANSACTIONS;
         currentTabLink = "currentTab=" + TradePortalConstants.AUTHORIZED_TRANSACTIONS;
         if (currentTab.equals(TradePortalConstants.AUTHORIZED_TRANSACTIONS)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
      <jsp:include page="/common/Tab.jsp">
          <jsp:param name="tabOn"       value="<%= tabOn %>" />
          <jsp:param name="linkTextKey" value="AuthorizedTransactions.TabName" />
          <jsp:param name="action"      value="goToCashManagement" />
          <jsp:param name="linkParms"   value="<%= currentTabLink %>" />
      </jsp:include>
      <%
         currentTabLink = "currentTab=" + TradePortalConstants.INSTRUMENT_HISTORY;
         if (currentTab.equals(TradePortalConstants.INSTRUMENT_HISTORY)) {
            tabOn = TradePortalConstants.INDICATOR_YES;
         } else {
            tabOn = TradePortalConstants.INDICATOR_NO;
         }
      %>
      <jsp:include page="/common/Tab.jsp">
          <jsp:param name="tabOn"       value="<%= tabOn %>" />
          <jsp:param name="linkTextKey" value="CachMgmtTransactionHistory.TabName" />
          <jsp:param name="action"      value="goToCashManagement" />
          <jsp:param name="linkParms"   value="<%= currentTabLink %>" />
      </jsp:include>
      <td class=ListText width="100%">&nbsp;</td>
      <td class=ListText >
        &nbsp;
      </td>
      <td class=ListText width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

<%
  // Based on the current tab, display the appropriate HTML for that tab
   if (currentTab.equals(TradePortalConstants.PAYMENT_FILE_UPLOAD))
  {
%>
    <%@ include file="CashMgmt-PaymentFileUploadListView.frag" %>

<%
  } else if (currentTab.equals(TradePortalConstants.CASH_MGMT_TRANSACTION))
  {
%>
   
     <%@ include file="CashMgmt-Transaction-PendingListView.frag" %> 
<%
  } else if (currentTab.equals(TradePortalConstants.FUTURE_TRANSACTIONS))
  {
%>
    <%@ include file="CashMgmt-FutureValue-ListView.frag" %>

<%
  } else if (currentTab.equals(TradePortalConstants.AUTHORIZED_TRANSACTIONS))
  {
%>
    <%@ include file="CashMgmt-AuthorizedListView.frag" %>
<%
  }
  else if (currentTab.equals(TradePortalConstants.INSTRUMENT_HISTORY)) 
  {
%>
    <%@ include file="CashMgmt-HistoryListView.frag" %>
<%
  }
%>
   




