<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
		 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************PARTY*SEARCH**************************START***");
/******************************************************************************
 * CorporateCustomerAddressList.jsp
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/

String returnAction     = null;
String partyType        = null;
String partyTypeCode    = null;
//String onLoad           = null;
String corpCustOid  = null;
String corpCustName = null;
String parentOrgID      = userSession.getOwnerOrgOid();
String bogID            = userSession.getBogOid();
String clientBankID     = userSession.getClientBankOid();
String globalID         = userSession.getGlobalOrgOid();
String ownershipLevel   = userSession.getOwnershipLevel();

String userSecurityRights = null;

Hashtable secureParams = new Hashtable();

DocumentHandler doc;

// Get the document from the cache.  
doc = formMgr.getFromDocCache();

Debug.debug("doc from cache is " + doc.toString());

// Get corporate customer name and oid.
// When we visit the page for the first time from corporate customer detail, 
// get the info from /In/CorporateOrganization.  
// When we access the page again from addres page, the information is stored 
// in /In/Address
// Do not want to keep the information in /In/CorporateOrganization to avoid
// error in corp customer page.
corpCustName = doc.getAttribute("/In/CorporateOrganization/name");
if (corpCustName == null) {
    corpCustName = doc.getAttribute("/In/Address/corp_org_name");
}

corpCustOid = doc.getAttribute("/In/CorporateOrganization/organization_oid");
if (corpCustOid == null) {
   corpCustOid = doc.getAttribute("/In/Address/corp_org_oid");
}
    
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="" />
</jsp:include>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
      <td width="1" class="BankColor" nowrap >&nbsp;</td>
      <td class="BankColor" nowrap > 
        <p class="ControlLabelWhite">
          <span class="Links">
            <a href="<%=formMgr.getLinkAsUrl("goToOrganizationsHome", response) %>" class="Links">
              <%= resMgr.getText("CorpCust.CorporateCustomers", TradePortalConstants.TEXT_BUNDLE) %> 
            </a>
          </span>&gt; 
          <span class="Links">
            <a href="<%=formMgr.getLinkAsUrl("goToCorporateCustomerDetail", response) %>" class="Links">
              <%= corpCustName %> 
            </a>
          </span>&gt; 
              <%= resMgr.getText("CorpCust.ManageAddressesText", TradePortalConstants.TEXT_BUNDLE) %>          
        </p>
      </td>
    <td width="*" class="BankColor" align="left" valign="middle" nowrap>&nbsp;</td>
    <td width="255" class="BankColor" align="left" valign="middle" nowrap>
  <table width="4" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign=middle>
    <td class="BankColor" align="left" valign="middle" nowrap>
    <%
      String link = formMgr.getLinkAsUrl("goToCorpCustAddress", response);
    %>
        <jsp:include page="/common/RolloverButtonLink.jsp">
        <jsp:param name="name"  value="CreateNewButton" />
		<jsp:param name="image" value="CorpCust.AddAddressImg" />
		<jsp:param name="text"  value="CorpCust.AddAddressText" />
		<jsp:param name="link"  value="<%=java.net.URLEncoder.encode(link)%>" />
        </jsp:include>
    </td>
        <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
    <td class="BankColor" align="left" valign="middle" nowrap>
    <%
        link = formMgr.getLinkAsUrl("goToCorporateCustomerDetail", response);
    %>
		<jsp:include page="/common/RolloverButtonLink.jsp">
		<jsp:param name="showButton" value="true" />
		<jsp:param name="name" value="CloseButton" />
		<jsp:param name="image" value='common.CloseImg' />
		<jsp:param name="text" value='common.CloseText' />
		<jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
		</jsp:include>    
	</td>
    <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
	</tr>
	</table>
	<td width="15" class="BankColor" nowrap>
	<%= OnlineHelp.createContextSensitiveLink("/admin/corp_customer_manageaddresses.htm",resMgr, userSession) %>
	</td>
	<td width="20" class="BankColor" nowrap>&nbsp;
    </td>
</tr>
</table>
<%
/*********************************************************************
 * Corporate Customer Address List View
 * This form contains the list view.  The user can click on an address
 * in the list view and go to that address.  
 *********************************************************************/
%>

<%
    /***********************************************************************
     * Begin list view for Parties List View Page
     ***********************************************************************/

%>
     <jsp:include page="/common/TradePortalListView.jsp">
         <jsp:param name="listView" value="CorpCustAddressListView.xml"/>
         <jsp:param name="whereClause2" value='<%="where p_corp_org_oid="+corpCustOid%>' />
         <jsp:param name="userType" value='<%=userSession.getSecurityType()%>' />
         </jsp:include>
<%
    Debug.debug("Listview complete");
    /***********************************************************************
     * End list view for Parties List View Page
     ***********************************************************************/
%>
</body>

</html>

<%
   // Reset the cached document 
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   
   // Set up parameter for address page
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/In/Address/corp_org_name", corpCustName);
   doc.setAttribute("/In/Address/corp_org_oid", corpCustOid);
   
%>
