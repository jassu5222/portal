<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
		 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************ADDRESS*SEARCH**************************START***");
/******************************************************************************
 * CorporateCustomerAddressList.jsp
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/

String partyType        = null;
String partyTypeCode    = null;
//String onLoad           = null;
String corporateOrgOid  = null;
String parentOrgID      = userSession.getOwnerOrgOid();
String bogID            = userSession.getBogOid();
String clientBankID     = userSession.getClientBankOid();
String globalID         = userSession.getGlobalOrgOid();
String ownershipLevel   = userSession.getOwnershipLevel();

String userSecurityRights = null;

Hashtable secureParams = new Hashtable();

DocumentHandler doc;

// Get the document from the cache.  We'll use it to repopulate the screen if the
// user was entering data with errors.
doc = formMgr.getFromDocCache();

Debug.debug("doc from cache is " + doc.toString());

corporateOrgOid = doc.getAttribute("/In/Instrument/corp_org_oid");
    


%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="" />
</jsp:include>

<script LANGUAGE="JavaScript">

  function getCheckedRadioButton()
  {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.ChooseAddressForm.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.ChooseAddressForm.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.ChooseAddressForm.selection.length;
        <%--
        //Handle the case where there is only one radio button
        //Handle the case where there are two or more radio buttons
        --%>
        for (i=0; i<size; i++)
        {
            if (document.ChooseAddressForm.selection[i].checked)
            {
                return document.ChooseAddressForm.selection[i].value;
            }
        }
        if (document.ChooseAddressForm.selection.checked)
        {
            return document.ChooseAddressForm.selection.value;
        }

    }
    return 0;
  }

  function confirmSelection()
  {
    if (getCheckedRadioButton()==0)
    {
        alert('<%=resMgr.getText("AddressSearch.SelectBlankWarning",TradePortalConstants.TEXT_BUNDLE)%>');
        return false;
    }
    return true;
  }
  
  function setPrimary()
  {
     document.ChooseAddressForm.ChoosePrimary.value = "PRIMARY";
     
  }

</script>


<%
/*********************************************************************
 * ChooseAddressForm
 * This form contains the list view and the choose and cancel buttons.
 *********************************************************************/
%>

<form method="post" name="ChooseAddressForm" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
  <input type=hidden value="NONE" name=ChoosePrimary>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr>
      <td width="15" class="BankColor" nowrap height="25">
        <img src="/portal/images/Blank_15.gif" width="15" height="15">
      </td>
      <td class="BankColor" valign="middle" align="left" width="302" nowrap height="25">
        <p class="ControlLabelWhite">
          <%=resMgr.getText("AddressSearch.TabHeading",TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td class="BankColor" width="622" height="25" align="center" valign="middle">
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25">
	    <%= OnlineHelp.createContextSensitiveLink("customer/address_search.htm",resMgr,userSession) %>
      </td>
      <td class="BankColor" width="1" height="25" nowrap>&nbsp;</td>
      <td class="BankColor" width="1" height="25" nowrap>&nbsp; </td>
      <td width="1" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>
  
  
<%
    /***********************************************************************
     * Begin list view for Addresses List View Page
     ***********************************************************************/

%>
     <jsp:include page="/common/TradePortalListView.jsp">
         <jsp:param name="listView" value="CorpCustAddressSearchListView.xml"/>
         <jsp:param name="whereClause2" value="<%=\"where p_corp_org_oid = \"+corporateOrgOid%>" />
         <jsp:param name="userType" value='<%=userSession.getSecurityType()%>' />
         </jsp:include>
<%
    Debug.debug("Listview complete");
    /***********************************************************************
     * End list view for Parties List View Page
     ***********************************************************************/
%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="37" nowrap align="left" valign="middle">
        <img src="/portal/images/UpIndicatorArrow.gif" width="37">
      </td>
      <td width="5" nowrap><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
      <td nowrap valign="middle">
        <p class="ListTextwhite">
          <%=resMgr.getText("AddressSearch.SelectText",TradePortalConstants.TEXT_BUNDLE)%></a>
        </p>
      </td>
      <%
      /*******************************************
       * START <CHOOSE ADDRESS> AND <CANCEL> BUTTONS
           onClick="return confirmSelection()">
       *******************************************/
      %>
      <td class="BankColor" width="100%" align="right">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
          <tr>
            <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
          </tr>
          <tr>
            <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15">
            <td align=right>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="showSave" />
                <jsp:param name="name" value="ChooseButton" />
                <jsp:param name="image" value='AddressSearch.ChooseSelectedAddressImg' />
                <jsp:param name="text" value='AddressSearch.ChooseSelectedAddressText' />
                <jsp:param name="extraTags" value="return confirmSelection();" />
                <jsp:param name="width" value="230" />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SEARCH%>" />
              </jsp:include>
            </td>
            <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15">
            <td align=right>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="showSave" />
                <jsp:param name="name" value="ChoosePrimaryAddress" />
                <jsp:param name="image" value='AddressSearch.ChoosePrimaryAddressImg' />
                <jsp:param name="text" value='AddressSearch.ChoosePrimaryAddressText' />
                <jsp:param name="extraTags" value="return setPrimary();" />
                <jsp:param name="width" value="230" />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SEARCH%>" />
              </jsp:include>
            </td>
            <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15">
            <td>
            <% 
            String link = java.net.URLEncoder.encode(formMgr.getLinkAsUrl("goToInstrumentNavigator", response));
            %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                 <jsp:param name="showButton" value="true" />
                 <jsp:param name="name" value="CloseButton" />
                 <jsp:param name="image" value='common.CloseImg' />
		 <jsp:param name="text" value='common.CloseText' />
                 <jsp:param name="link" value="<%=link%>" />
              </jsp:include>
           </td>
           <%
             /*****************************************
             * END <CHOOSE ADDRESS> AND <CANCEL> BUTTONS
             *****************************************/
           %>
           <td width="20" nowrap>&nbsp;</td>
         </tr>
        </table>
         </td>
       </tr>
    </table>
    <%= formMgr.getFormInstanceAsInputField("AddressSearchForm", secureParams) %>
  </form>
</body>

</html>


