package com.ams.tradeportal.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.html.WidgetFactory;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.html.ListBox;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

public class CorpCustOpBankOidGeneratorServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doTheWork(request, response);

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doTheWork(request, response);

	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	    //cquinton 12/1/2012 this is now obsolete.
	    // all logic is handled on corp cust detail page
	    
		DocumentHandler operationalBankOrgsDoc = null;
		StringBuilder orgOidHtml = new StringBuilder();
		StringBuilder sqlQuery = new StringBuilder();
		String dropdownOptions = null; 
		String clientBankOid = null;
		QueryListView queryListView = null;
		SessionWebBean userSession = null;
		ResourceManager resMgr = null;
		FormManager formMgr = null;
		boolean otherOwnerB = false;
		
	
		try {
			long lOid = 0;
			// Retrieve the form manager and resource manager from the session
			HttpSession session = request.getSession(true);
			// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
	        javax.crypto.SecretKey secretKey = null;
			if (session!=null) {
	            resMgr = (ResourceManager) session.getAttribute("resMgr");
	            userSession = (SessionWebBean) session.getAttribute("userSession");
	            formMgr = (FormManager) session.getAttribute("formMgr");
	      	    secretKey = userSession.getSecretKey();
		    }
			clientBankOid = userSession.getClientBankOid();

			WidgetFactory widgetFactory = new WidgetFactory(resMgr);
			String corporateOrgOid = request.getParameter("corporateOrgOid");
			String displayCOOBOid = request.getParameter("displayCOOBOid");
			String opBankOrgOid = request.getParameter("opBankOrgOid");
			String readOnly = request.getParameter("readOnly");
			String otherCorpCustInd = request.getParameter("otherCorpCustInd");
			//Validation to check Cross Site Scripting	  
			if(opBankOrgOid != null){
				opBankOrgOid = StringFunction.xssCharsToHtml(opBankOrgOid);
			  }

			boolean readOnlyB = Boolean.parseBoolean(readOnly);

			if(!StringFunction.isBlank(otherCorpCustInd) && otherCorpCustInd.equals(TradePortalConstants.INDICATOR_YES)){
				otherOwnerB = true;	//IAZ IR-ALUL051728806 05/17/11 Add
			}

			queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
			sqlQuery.append("select organization_oid, name");
			sqlQuery.append(" from operational_bank_org");
			sqlQuery.append(" where p_owner_bank_oid = ");
			sqlQuery.append(clientBankOid);
			sqlQuery.append(" and activation_status = '");
			sqlQuery.append(TradePortalConstants.ACTIVE);
			sqlQuery.append("' order by ");
			sqlQuery.append(resMgr.localizeOrderBy("name"));

			queryListView.setSQL(sqlQuery.toString());
			queryListView.getRecords();

			operationalBankOrgsDoc = queryListView.getXmlResultSet();

            if (corporateOrgOid.equals("0")) {
				String displayCOOBOidName = null;
				// donot display anything when creating if it is new customer
				//orgOidHtml.append("<INPUT TYPE=hidden NAME='opBankOrgIdEdit' VALUE='"+ EncryptDecrypt.encryptStringUsingTripleDes(displayCOOBOid, secretKey)+ "'>");
				// Start Rkazi, IAZ - IR#LOULO42232737 - 05/23/2011- Display
				// empty dropdown disabled for new customers.

				
				if (StringFunction.isBlank(displayCOOBOid)){
					displayCOOBOid = "";
					displayCOOBOidName = "----------";
				}
				else {
					sqlQuery = new StringBuilder();
					sqlQuery.append("select organization_oid, name from operational_bank_org where organization_oid = '"+ displayCOOBOid + "'");
					queryListView.setSQL(sqlQuery.toString());
					queryListView.getRecords();
					operationalBankOrgsDoc = queryListView.getXmlResultSet();
					Vector thisOprOrgNameSet = operationalBankOrgsDoc.getFragments("/ResultSetRecord");
					try {
						displayCOOBOidName = ((DocumentHandler) thisOprOrgNameSet.elementAt(0)).getAttribute("/NAME");
					} catch (Exception any_e) {
						any_e.printStackTrace();
						// displayCOOBOid is not set
					}
				}
				

				
				orgOidHtml.append(widgetFactory.createSelectField("opBankOrgIdEdit", "CorpCust.opBankOrgId", "", "<option value=\""+ EncryptDecrypt.encryptStringUsingTripleDes(displayCOOBOid, secretKey) + "\">" + displayCOOBOidName, readOnlyB, false,false, " class=\"char30\"", "disable:true", ""));
				// End Rkazi, IAZ - IR#LOULO42232737 - 05/23/2011- Display empty
				// dropdown disabled for new customers.
			} else {

				// Maheswar CR 610 Bug fix End 11/04/2011
				sqlQuery = new StringBuilder();

				// account belongs to the subsidiary
				// acct bank op org should be a protected field and not dropdown
				if (!otherOwnerB) {
					sqlQuery.append("select organization_oid, name from operational_bank_org where organization_oid in (select a_first_op_bank_org from corporate_org where organization_oid = ");
					sqlQuery.append(corporateOrgOid);
					sqlQuery.append(" and a_first_op_bank_org is not NULL union select a_second_op_bank_org from corporate_org where organization_oid=");
					sqlQuery.append(corporateOrgOid);
					sqlQuery.append(" union select a_third_op_bank_org from corporate_org where organization_oid=");
					sqlQuery.append(corporateOrgOid);
					sqlQuery.append(" union select a_fourth_op_bank_org from corporate_org where organization_oid=");
					sqlQuery.append(corporateOrgOid);
					sqlQuery.append(")");
				} else {
                    String unencryptedOpBankOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(opBankOrgOid, secretKey);
                    sqlQuery.append("select organization_oid, name from operational_bank_org where organization_oid = '"+ unencryptedOpBankOrgOid + "'");
				}

				queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
				queryListView.setSQL(sqlQuery.toString());
				queryListView.getRecords();
				operationalBankOrgsDoc = queryListView.getXmlResultSet();
						
				String  value;
		        String  text;
				// for first time entry get the first entry of op_bank_org_oid
				// value from the default corporate customer
				if (!otherOwnerB) {
				 	 
			        // WUUK082559086 Check null inputDoc to avoid NullPointerException
			        if (operationalBankOrgsDoc != null) {
			            Vector ocv = operationalBankOrgsDoc.getFragments ("/ResultSetRecord");
			            int items = ocv.size ();
			            for (int i = 0; i < items; i++)
			            {
			                DocumentHandler docO = (DocumentHandler)ocv.elementAt (i);
			                // Extract the value and text attributes from the document.
			                try
			                {
			                    value = docO.getAttribute ("/ORGANIZATION_OID");
			                }
			                catch (Exception e){
			                    value = "0";
			                }
			                try {
			                    text = docO.getAttribute ("/NAME");
			                    text = StringFunction.xssCharsToHtml(text); 
			                } catch (Exception e){
			                    text = "";
			                }
			          	    // W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.            
		                    
	                        value = EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey);
						    
			            }
					}
					         
					//************************************************************************************************************************
							
                    dropdownOptions = "<option selected value=\"\"\"=\"></option>";
					dropdownOptions += ListBox.createOptionList(operationalBankOrgsDoc, "ORGANIZATION_OID", "NAME", opBankOrgOid, secretKey);
					
					orgOidHtml.append(widgetFactory.createSelectField("opBankOrgIdEdit", "CorpCust.opBankOrgId", "", dropdownOptions, readOnlyB, false, false, " class=\"char30\"", "", ""));
					String opBankOrgOidValue = "";

					if (StringFunction.isNotBlank(opBankOrgOid)){
						opBankOrgOidValue = EncryptDecrypt.encryptStringUsingTripleDes(opBankOrgOid, secretKey);
					}
					orgOidHtml.append("<input type=hidden name=\"opBankOrgIdHid\"  value=\""+ opBankOrgOidValue + "\">");
				} else { //other account owner
					Vector thisOprOrgNameSet = operationalBankOrgsDoc.getFragments("/ResultSetRecord");
					String opOrgName = "";
					try {
						opOrgName = ((DocumentHandler) thisOprOrgNameSet.elementAt(0)).getAttribute("/NAME");
					} catch (Exception any_e) {
						any_e.printStackTrace();
						// opOrgName is not set
					}
					

					orgOidHtml.append(widgetFactory.createTextField("opBankOrgIdD", "CorpCust.opBankOrgId", opOrgName,"", readOnlyB, false, false, " width=\"30\" disabled=\"disabled\"", "", ""));

					
					orgOidHtml.append("<INPUT TYPE=hidden NAME='opBankOrgIdEdit'  VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(opBankOrgOid, secretKey)+ "'>");

				}
			}
		} catch (AmsException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}

		PrintWriter pw = response.getWriter();
		
		pw.print(orgOidHtml.toString());
		
		pw.flush();
	}
}





