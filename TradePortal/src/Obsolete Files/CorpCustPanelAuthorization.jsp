<%--
**********************************************************************************
                              Corporate Customer Panel Level Authrorization

  Description:
     This page is used to maintain all foreign exchange rates.  It supports 
     insert, update, and delete.  Errors found during a save are redisplayed on 
     the same page. Successful updates return the user to the ref data home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
DocumentHandler                xmlDoc                          = null;
//PanelAuthorizationRangeWebBean   panelAuthRange                    = null;
CorporateOrganizationWebBean corporateOrg 					   = null;
Hashtable                      secureParms                     = new Hashtable();
boolean                        getDataFromXmlDocFlag           = false;
boolean                        showDeleteButtonFlag            = true;
boolean                        showSaveButtonFlag              = true;
boolean                        retrieveFromDBFlag              = false;
boolean                        isClientBankUser                = false;
boolean                        insertModeFlag                  = true;
boolean                        isReadOnly                      = false;
boolean                        errorFlag                       = false;
String                         addressCountry             = null;
String                         addressName                = null;
String                         addressOid                 = null;
String                         corporateOrgName                = null;
String                         corporateOrgOid                = null;
String                         userSecurityRights              = null;
String                         defaultText                     = null;
String                         userOrgOid                      = null;
String                         userLocale                      = null;
String                         userType                        = null;
String                         onLoad                          = null;
String                         sValue                          = null;
String                         clientBankOid                   = null;
QueryListView                  queryListView                   = null;

Vector                         vVector;
boolean 					 transferBtwAcctIndicator = false;
boolean 				     domesticPaymtIndicator = false;
boolean 					 intlPaymentIndicator = false;
String 						allowTBAIndicator	= null;
String						allowDomesticPmtIndicator	= null;
String 						allowInternationalPmtInd	= null;

String panelRangeMinAmount = null;
String panelRangeMaxAmount = null;
String baseCurrency = "";
int panelRangeForAllInstTypeCount = 18;
int maxInstrumentTypeRows = 6;

List paneAuthRangeList = new ArrayList(panelRangeForAllInstTypeCount);
List transferBtwAcctsList = new ArrayList(maxInstrumentTypeRows);
List domesticPymtsList = new ArrayList(maxInstrumentTypeRows);
List intlPaymentList = new ArrayList(maxInstrumentTypeRows);


   beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.PanelAuthorizationRangeWebBean", "PanelAuthRange");
   beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
   corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");
   
   xmlDoc = formMgr.getFromDocCache();
     
   // Retrieve the user's locale, security rights, and organization oid
   userSecurityRights = userSession.getSecurityRights();
   userLocale         = userSession.getUserLocale();
   userOrgOid         = userSession.getOwnerOrgOid();
   clientBankOid      = userSession.getClientBankOid();
   baseCurrency		  = xmlDoc.getAttribute("/In/CorporateOrganization/base_currency_code");
   // Retrieve the type of user so that we can get the appropriate read only rights; we can only be on this 
   // page if we're a client bank user or a BOG user
   userType = userSession.getOwnershipLevel();

   if (userType.equals(TradePortalConstants.OWNER_BANK))
   {
      isClientBankUser = true;

      isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_CB);
   }
   else
   {
      isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_BG);
   }

   corporateOrgName = xmlDoc.getAttribute("/In/CorporateOrganization/name");
   
   if (corporateOrgName == null) {
      corporateOrgName = "New Customer";
   }
   corporateOrgOid = xmlDoc.getAttribute("/In/CorporateOrganization/organization_oid");    
   
   if (corporateOrgOid.equals("0")) {  // If this is a new corp Org, the oid is in /Out/CorporateOrganization
       corporateOrgOid = xmlDoc.getAttribute("/Out/CorporateOrganization/organization_oid");
      
   }
   corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");
   
   if (xmlDoc.getAttribute("/In/CorporateOrganization/PanelAuthorizationRangeList") == null) {
	   
	  
	      // We have a good oid so we can retrieve.
	      getDataFromXmlDocFlag = false;
	      retrieveFromDBFlag    = true;
	      insertModeFlag        = false;
	      corporateOrg.getById(corporateOrgOid);
	      
	     
	   
	   
   } else {
			// We've returned from a save/update/delete but have errors.
	      // We will display data from the cache.  Determine if we're
	      // in insertMode by looking for a '0' oid in the input section
	      // of the doc.
	   retrieveFromDBFlag    = false;
	   getDataFromXmlDocFlag = true;
	   errorFlag             = true;
	   insertModeFlag		 = false;
   }
  
    PanelAuthorizationRangeWebBean panelAuthRange[] = new PanelAuthorizationRangeWebBean[18];
	for (int iLoop=0; iLoop<panelRangeForAllInstTypeCount; iLoop++)
	{
		panelAuthRange[iLoop] = (PanelAuthorizationRangeWebBean)beanMgr.createBean("com.ams.tradeportal.busobj.webbean.PanelAuthorizationRangeWebBean", "PanelAuthorizationRange");
	}
    if (getDataFromXmlDocFlag) {
    	try
        {
           corporateOrg.populateFromXmlDoc(xmlDoc.getFragment("/In"));
           //out.println("HI");
        }
        catch (Exception e)
        {
           System.out.println("Contact Administrator: Unable to get CorporateOrganization attributes " + 
                              "for oid: " + corporateOrgOid + " " + e.toString());
        }
    	vVector = xmlDoc.getFragments("/In/CorporateOrganization/PanelAuthorizationRangeList");  
   		int numItems = vVector.size();   		   		
   		Debug.debug("Vector Size = "+numItems);
   		Debug.debug(xmlDoc.toString());
   		int i;
		DocumentHandler panelAuthDoc = null; 
		for(i =0; i < numItems; i++) {
			panelAuthDoc = (DocumentHandler) vVector.elementAt(i);
			sValue = panelAuthDoc.getAttribute("/instrument_type_code");				  
			if(TradePortalConstants.XFER_BET_ACCTS.equals(sValue)) {
				  transferBtwAcctsList.add(panelAuthDoc);
			} else if(TradePortalConstants.DOMESTIC_PMT.equals(sValue)) {
				  domesticPymtsList.add(panelAuthDoc);
			} else {
				  intlPaymentList.add(panelAuthDoc);
			}
		}
		// Chandrakanth IR PYUI121166801 12/17/2008 Begin
		
		  // filling empty objects to display the elements in the appropriate section in UI.
		  for(i = transferBtwAcctsList.size(); i < maxInstrumentTypeRows; i++) {
			  transferBtwAcctsList.add(new DocumentHandler());
		  }
		  for(i = domesticPymtsList.size(); i < maxInstrumentTypeRows; i++) {
			  domesticPymtsList.add(new DocumentHandler());
		  }
		  for(i = intlPaymentList.size(); i < maxInstrumentTypeRows; i++) {
			  intlPaymentList.add(new DocumentHandler());
		  }
		  paneAuthRangeList.addAll(transferBtwAcctsList);
		  paneAuthRangeList.addAll(domesticPymtsList);
		  paneAuthRangeList.addAll(intlPaymentList);
		  
	 	for (int iLoop=0; iLoop<paneAuthRangeList.size(); iLoop++) {
	 		
	 		 DocumentHandler panelAuthFromCacheDoc = (DocumentHandler) paneAuthRangeList.get(iLoop); 
			 sValue = panelAuthFromCacheDoc.getAttribute("/panel_auth_range_oid");
			 panelAuthRange[iLoop].setAttribute("panel_auth_range_oid", sValue);
			 
			 
			 sValue = panelAuthFromCacheDoc.getAttribute("/instrument_type_code");		 
			 panelAuthRange[iLoop].setAttribute("instrument_type_code", sValue);
			 
			
			 sValue = panelAuthFromCacheDoc.getAttribute("/panel_auth_range_max_amt");		 
			 panelAuthRange[iLoop].setAttribute("panel_auth_range_max_amt", sValue);
			 
			
			 sValue = panelAuthFromCacheDoc.getAttribute("/panel_auth_range_min_amt");		 
			 panelAuthRange[iLoop].setAttribute("panel_auth_range_min_amt", sValue);
			 
			 for(i = 0; i < 5; i++) {
			   	 sValue = panelAuthFromCacheDoc.getAttribute("/num_of_panel_A_users_group"+(i+1));			     
				 panelAuthRange[iLoop].setAttribute("num_of_panel_A_users_group"+(i+1), sValue);
				    
			     sValue = panelAuthFromCacheDoc.getAttribute("/num_of_panel_B_users_group"+(i+1));					     
				 panelAuthRange[iLoop].setAttribute("num_of_panel_B_users_group"+(i+1), sValue);
				   
			     sValue = panelAuthFromCacheDoc.getAttribute("/num_of_panel_C_users_group"+(i+1)); 					     
				 panelAuthRange[iLoop].setAttribute("num_of_panel_C_users_group"+(i+1), sValue);
	  
			 }
	 	}
	    
    } 
    
    if (!insertModeFlag || errorFlag)
    {
    	allowTBAIndicator = corporateOrg.getAttribute("allow_xfer_btwn_accts_panel");
    	allowDomesticPmtIndicator = corporateOrg.getAttribute("allow_domestic_payments_panel");
    	allowInternationalPmtInd = corporateOrg.getAttribute("allow_funds_transfer_panel");
    }
		StringBuffer sql = new StringBuffer();				
		queryListView = null;
		
	// Retrieve Panel ranges for all the Instrument Types	
	if(retrieveFromDBFlag) {
		//try {
		insertModeFlag = false;	
		 
		  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		  sql.append("select * from panel_auth_range");		  
		  sql.append(" where p_corp_org_oid = " + corporateOrgOid);
		  //sql.append(" and instrument_type_code = '");
		 // sql.append(TradePortalConstants.XFER_BET_ACCTS);
		  sql.append(" order by ");
		  sql.append("panel_auth_range_min_amt");
		  
		  
		  Debug.debug(sql.toString());
		
		  queryListView.setSQL(sql.toString());
		  queryListView.getRecords();
		
		  DocumentHandler panelAuthRangeList = new DocumentHandler();
		  panelAuthRangeList = queryListView.getXmlResultSet();
		  Debug.debug(panelAuthRange.toString());
		  
		 
		  vVector = panelAuthRangeList.getFragments("/ResultSetRecord");
		  int numOfItems = vVector.size();
		  Debug.debug("Record Count for Transfer Between Accounts "+numOfItems);
		  
		  int i;
		  DocumentHandler panelAuthDoc = null; 
		  for(i =0; i < numOfItems; i++) {
			  panelAuthDoc = (DocumentHandler) vVector.elementAt(i);
			  sValue = panelAuthDoc.getAttribute("/INSTRUMENT_TYPE_CODE");	
			  
			  if(TradePortalConstants.XFER_BET_ACCTS.equals(sValue)) {
				  transferBtwAcctsList.add(panelAuthDoc);
			  } else if(TradePortalConstants.DOMESTIC_PMT.equals(sValue)) {
				  domesticPymtsList.add(panelAuthDoc);
			  } else {
				  intlPaymentList.add(panelAuthDoc);
			  }
		  }
		
		  // filling empty objects to display the elements in the appropriate section in UI.
		  for(i = transferBtwAcctsList.size(); i < maxInstrumentTypeRows; i++) {
			  transferBtwAcctsList.add(new DocumentHandler());
		  }
		  for(i = domesticPymtsList.size(); i < maxInstrumentTypeRows; i++) {
			  domesticPymtsList.add(new DocumentHandler());
		  }
		  for(i = intlPaymentList.size(); i < maxInstrumentTypeRows; i++) {
			  intlPaymentList.add(new DocumentHandler());
		  }
		  paneAuthRangeList.addAll(transferBtwAcctsList);
		  paneAuthRangeList.addAll(domesticPymtsList);
		  paneAuthRangeList.addAll(intlPaymentList);
		 
		  
		  
		  for(int jLoop = 0; jLoop < paneAuthRangeList.size(); jLoop++) {
			  DocumentHandler acctDoc = (DocumentHandler) paneAuthRangeList.get(jLoop);
		    	
			    sValue = acctDoc.getAttribute("/PANEL_AUTH_RANGE_OID"); 				    
			    
			    panelAuthRange[jLoop].setAttribute("panel_auth_range_oid", sValue);
			
			    sValue = acctDoc.getAttribute("/INSTRUMENT_TYPE_CODE");			    
			    panelAuthRange[jLoop].setAttribute("instrument_type_code", sValue);
			   
			    sValue = acctDoc.getAttribute("/PANEL_AUTH_RANGE_MIN_AMT");			    
			    panelAuthRange[jLoop].setAttribute("panel_auth_range_min_amt", sValue);
			    
			    sValue = acctDoc.getAttribute("/PANEL_AUTH_RANGE_MAX_AMT");			    
			    panelAuthRange[jLoop].setAttribute("panel_auth_range_max_amt", sValue);
			   				    
			    			    
			    for(i = 0; i < 5; i++) {
			    	sValue = acctDoc.getAttribute("/NUM_OF_PANEL_A_USERS_GROUP"+(i+1)); 
			       			    		 			    	
				    panelAuthRange[jLoop].setAttribute("num_of_panel_A_users_group"+(i+1), sValue);
				    
				    sValue = acctDoc.getAttribute("/NUM_OF_PANEL_B_USERS_GROUP"+(i+1));
		    	 		
		    	 	panelAuthRange[jLoop].setAttribute("num_of_panel_B_users_group"+(i+1), sValue);
				    
		    	 	sValue = acctDoc.getAttribute("/NUM_OF_PANEL_C_USERS_GROUP"+(i+1)); 
		    	 	
		    	 	panelAuthRange[jLoop].setAttribute("num_of_panel_C_users_group"+(i+1), sValue);
		    	 	
		    	 	
			    }  			   
		  }
		//} catch (Exception e) { e.printStackTrace(); }
		/*finally {
		try {
		  if (queryListView != null) {
		    queryListView.remove();
		  }
		} catch (Exception e) { System.out.println("Error removing queryListView in CorpCustPanelAuthorization.jsp"); }
		}*/
		if (queryListView != null) {
		    queryListView.remove();
		  }
	}
		
	
 
%>
<script language="JavaScript">

function setInstrumentTypeCheckBox(loopIndex)
{
	
	if(loopIndex < 6) {
		 document.forms[0].transferBtwAcctsPanelIndicator.checked = true;
	} else if(loopIndex < 12) {
		 document.forms[0].domesticPmtPanelIndicator.checked = true;
	} else {
		 document.forms[0].IntlPmtPanelIndicator.checked = true;
	}
	  
}

function clearPanelFieldsForTransfer() {
	
	if(document.forms[0].transferBtwAcctsPanelIndicator.checked == false) {
		
		clearPanelFields(0,6);
		
	}
	
}

function clearPanelFieldsForDomesticPmt() {
	
	if(document.forms[0].domesticPmtPanelIndicator.checked == false) {
		
		clearPanelFields(6,12);
		
	}
	
}
function clearPanelFieldsForIntlPmt() {
	if(document.forms[0].IntlPmtPanelIndicator.checked == false) {
		clearPanelFields(12,18);
	}
}

function clearPanelFields(startIndex,endIndex) {
	for(startIndex;startIndex < endIndex; startIndex++) {
		
		document.getElementById("panelAuthRangeMinAmt"+startIndex).value = "";
		document.getElementById("panelAuthRangeMaxAmt"+startIndex).value = "";
		//document.getElementById("InstrumentTypeCode"+startIndex).value = "";
		for(i = 0; i < 5; i++) {
			document.getElementById("numOfPanelAUsersGroup"+(i+1)+startIndex).value = "";
			document.getElementById("numOfPanelBUsersGroup"+(i+1)+startIndex).value = "";
			document.getElementById("numOfPanelCUsersGroup"+(i+1)+startIndex).value = "";
		}
		
	}
}


</script>
<%-- ********************* HTML for page begins here *********************  --%>
<jsp:include page="/common/ButtonPrep.jsp" />


<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="" />
   <jsp:param name="autoSaveFlag"             value="true" />
</jsp:include>

<form name="CorpCustPanelAuthForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
  <input type=hidden value="" name=buttonName>
<%
   // Store the Corporate Org oid (if there is one) and other necessary info in a secure hashtable for the form
   //secureParms.put("AddressOid", addressOid);
   secureParms.put("UserOid",                  userSession.getUserOid());
   secureParms.put("login_oid",                userSession.getUserOid());
   secureParms.put("login_rights",             userSession.getSecurityRights());
   secureParms.put("ownership_level",userSession.getOwnershipLevel());
   secureParms.put("name", corporateOrgName);
   secureParms.put("CorporateOrganizationOid", corporateOrgOid);
   
   secureParms.put("OptimisticLockId", corporateOrg.getAttribute("opt_lock"));
   if (insertModeFlag)
   {
      secureParms.put("ClientBankOid", clientBankOid);

      if (userType.equals(TradePortalConstants.OWNER_BOG))
      {
         secureParms.put("BankOrgGroupOid", EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()) );
      }
   }
   else
   {
      //secureParms.put("opt_lock", address.getAttribute("opt_lock"));
   }
%>

<%= formMgr.getFormInstanceAsInputField("CorpCustPanelAuthForm", secureParms) %>


  <table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tr> 
      <td width="1" class="BankColor" nowrap >&nbsp;</td>
      <td class="BankColor" nowrap > 
        <p class="ControlLabelWhite">
          <span class="Links">
            <a href="<%=formMgr.getLinkAsUrl("goToOrganizationsHome", response) %>" class="Links">
              <%= resMgr.getText("CorpCust.CorporateCustomers", TradePortalConstants.TEXT_BUNDLE) %> 
            </a>
          </span>&gt; 
          <span class="Links">
            <a href="<%=formMgr.getLinkAsUrl("goToCorporateCustomerDetail", response) %>" class="Links">
              <%= corporateOrgName %> 
            </a>
          </span>&gt; 
              <%= resMgr.getText("CorpCust.PanelDefinition", TradePortalConstants.TEXT_BUNDLE) %>          
        </p>
      </td>
      <td width="100%" class="BankColor">&nbsp; </td>
      <td colspan=5 class="BankColor" nowrap> 
        <jsp:include page="/common/SaveDeleteClose.jsp">
           <jsp:param name="showSaveButton"   value="true" />
           <jsp:param name="showDeleteButton" value="false" />
           <jsp:param name="showCloseButton"  value="true" />
           <jsp:param name="showHelpButton"   value="true" />
   	     <jsp:param name="helpLink"         value="/admin/corp_customer_address.htm" />
           <jsp:param name="cancelAction"     value="goToCorporateCustomerDetail" />
        </jsp:include>
      </td>
    </tr>
  </table>
  <table width=100% border=0 cellspacing=0 cellpadding=0 class="ColorBeige">
    <tr>
      <td width=20 nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="AsterixText">
          <span class="Asterix">*</span>
            <%=resMgr.getText("common.Required",
                              TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
    </tr>
  </table>
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorBeige">
    <tr> 
      <td width="30" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ListText">
          <%= resMgr.getText("CorpCust.PanelAuthorizationRangeText", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
    </tr>
  </table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
	  
      <td width="50" nowrap>&nbsp;</td>
      <td width="24" nowrap class="ColorGrey"> 
        
		<%
		// Chandrakanth IR PYUI121166801 12/17/2008 Begin
		if((!insertModeFlag || errorFlag) && TradePortalConstants.INDICATOR_YES.equals(allowTBAIndicator)) {
			transferBtwAcctIndicator = true;
		   } else {
			   transferBtwAcctIndicator = false;
		   }
				
			out.print(InputField.createCheckboxField("transferBtwAcctsPanelIndicator", TradePortalConstants.INDICATOR_YES, 
                                                   "", transferBtwAcctIndicator, "ListText", "ListText", isReadOnly, 
                                                   "onClick=\"clearPanelFieldsForTransfer()\""));
			
			// Chandrakanth IR PYUI121166801 12/17/2008 End.
        %>
      </td>
      <td width="1" nowrap class="ColorGrey">&nbsp;</td>
      <td nowrap colspan="2" class="ColorGrey"> 
        <p class="ListHeaderText">
          <%= resMgr.getText("CorpCust.TransferBtwAccts", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
  </tr>
  </table>
    

<br>


<table border="0" cellspacing="0" cellpadding="0">  
  <tr>
    <td width="50" nowrap>&nbsp;</td>
   	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
	<td width="50" class="ControlLabel" valign="bottom" align="right"  nowrap>&nbsp;</td>
    <td class="ControlLabel" valign="bottom" width="100" height="46" align="left"><%= resMgr.getText("CorpCust.UsersFromPanelLevelA", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td>&nbsp;</td>
    <td class="ControlLabel" valign="bottom" width="100" height="46" align="left"><%= resMgr.getText("CorpCust.UsersFromPanelLevelB", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td>&nbsp;</td>
    <td class="ControlLabel" valign="bottom" width="100" height="46" align="left" ><%= resMgr.getText("CorpCust.UsersFromPanelLevelC", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td class="ControlLabel" valign="bottom" width="100" height="46" align="center" ><%= resMgr.getText("CorpCust.TotalNumOfAuthorizers", TradePortalConstants.TEXT_BUNDLE) %></td></tr>
  
 <%
 	int resetOloop = 0;
 	for(int oLoop =0; oLoop < panelRangeForAllInstTypeCount; oLoop++) { 
	 	for(int iLoop = 0; iLoop < 5; iLoop++) {
	 	
 	%> 
	<tr>
	<td width="50" nowrap>&nbsp;</td>
<% if(iLoop==0) {
	// Chandrakanth IR AAUI120247248 12/08/2008 Begin
	if (retrieveFromDBFlag) {
	  
		if(!InstrumentServices.isBlank(panelAuthRange[oLoop].getAttribute("panel_auth_range_min_amt"))) {
			panelRangeMinAmount = TPCurrencyUtility.getDisplayAmount(
					panelAuthRange[oLoop].getAttribute("panel_auth_range_min_amt"), baseCurrency, userLocale);			
			
		} else {
			panelRangeMinAmount = panelAuthRange[oLoop].getAttribute("panel_auth_range_min_amt");
		}
		if(!InstrumentServices.isBlank(panelAuthRange[oLoop].getAttribute("panel_auth_range_max_amt"))) {
			panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmount(
					panelAuthRange[oLoop].getAttribute("panel_auth_range_max_amt"), baseCurrency, userLocale);	
		} else {
			panelRangeMaxAmount = panelAuthRange[oLoop].getAttribute("panel_auth_range_max_amt");
		}
		
	} else {
		panelRangeMinAmount = panelAuthRange[oLoop].getAttribute("panel_auth_range_min_amt");
		panelRangeMaxAmount = panelAuthRange[oLoop].getAttribute("panel_auth_range_max_amt");
	}
	// Chandrakanth IR AAUI120247248 12/08/2008 End.
	%>
	
    <td width="30" class="ControlLabel" valign="bottom" align="center"  nowrap><br><%= ++resetOloop  %></td>
    <td class="ControlLabel" width="150" valign="bottom" align="left" nowrap><%= resMgr.getText("CorpCust.PanelAmountRange", TradePortalConstants.TEXT_BUNDLE) %><br>
    <%= InputField.createTextField("panelAuthRangeMinAmt"+oLoop, panelRangeMinAmount, "30", "30", "ListTextRight", isReadOnly,"id=\"panelRangeMinAmount"+oLoop+"\" onChange=\"setInstrumentTypeCheckBox("+oLoop+")\"")%></td>
    <td width="20" class="ControlLabel" valign="bottom" align="center" nowrap><%= resMgr.getText("CorpCust.To", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td width="150" valign="bottom" align="left" nowrap><br>
    <%= InputField.createTextField("panelAuthRangeMaxAmt"+oLoop, panelRangeMaxAmount, "30", "30", "ListTextRight", isReadOnly,"id=\"panelAuthRangeMaxAmt"+oLoop+"\"")%></td>
    </td>
    <td></td>

<% 
		if(oLoop < 6) {	
			out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCode" + oLoop + "' VALUE='" + TradePortalConstants.XFER_BET_ACCTS + "' ID='InstrumentTypeCode"+oLoop +"' >");
		} else if (oLoop < 12) {
			out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCode" + oLoop + "' VALUE='" + TradePortalConstants.DOMESTIC_PMT + "' ID='InstrumentTypeCode"+oLoop +"'>");
		} else {
			out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCode" + oLoop + "' VALUE='" + TradePortalConstants.FUNDS_XFER + "' ID='InstrumentTypeCode"+oLoop +"'>");
		}

	if(panelAuthRange[oLoop].getAttribute("panel_auth_range_oid") != null) {
		out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOid" + oLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthRange[oLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>");	
	} else {
		out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOid" + oLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey()) + "'>");
	}
 
%> 
    <%} else { %>
<td colspan=5></td>
<%} %>
	
    <td width="10%" valign="bottom" align="left" nowrap><%= InputField.createTextField("numOfPanelAUsersGroup"+(iLoop+1)+oLoop, panelAuthRange[oLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)), "5", "5", "ListTextRight", isReadOnly,"id=\"numOfPanelAUsersGroup"+oLoop+"\"")%></td></td>
    <td width="5%" class="ListText" valign="bottom" align="left" nowrap><%= resMgr.getText("CorpCust.And", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td width="10%" valign="bottom" align="left" nowrap><%= InputField.createTextField("numOfPanelBUsersGroup"+(iLoop+1)+oLoop, panelAuthRange[oLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)), "5", "5", "ListTextRight", isReadOnly, "id=\"numOfPanelBUsersGroup"+oLoop+"\"")%></td>
    <td width="5%" class="ListText" valign="bottom" align="left" nowrap><%= resMgr.getText("CorpCust.And", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td width="10%" valign="bottom" align="left" nowrap><%= InputField.createTextField("numOfPanelCUsersGroup"+(iLoop+1)+oLoop, panelAuthRange[oLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)), "5", "5", "ListTextRight", isReadOnly, "id=\"numOfPanelCUsersGroup"+oLoop+"\"")%></td>
    <% 
    // Chandrakanth IR PRUI120461417 01/09/2008 Begin
    int panelA = 0;
    int panelB = 0;
    int panelC = 0;
    int totalNumOfAuthGroup = 0;
    try {
   	 	 panelA = Integer.parseInt(panelAuthRange[oLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)));			     	     
    } catch(NumberFormatException nfe) {
    	panelA = 0;
    }
    try {
   	     panelB = Integer.parseInt(panelAuthRange[oLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)));				     	    
    } catch(NumberFormatException nfe) {
    	panelB = 0;
    }
    try {
   	 	panelC = Integer.parseInt(panelAuthRange[oLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1))); 					     
		
    } catch(NumberFormatException nfe) {
    	panelC = 0;
    }
    totalNumOfAuthGroup = panelA + panelB + panelC;
   // Chandrakanth IR PRUI120461417 01/09/2008 End.
    %>
	<td width="10%" valign="bottom" align="center" nowrap><%= totalNumOfAuthGroup%> </td>
   <%-- <td><%= InputField.createTextField("totalNumOfAuthGroup"+(iLoop+1)+oLoop, "", "5", "5", "ListText", isReadOnly)%></td> --%>
</tr>
<% if(iLoop == 4) {
     break;
	}
%>
  <tr>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="ControlLabel" width="5%" valign="bottom" align="right" nowrap><%= resMgr.getText("CorpCust.Or", TradePortalConstants.TEXT_BUNDLE) %></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
<% } %>
<tr><td> &nbsp;</td></tr>	
<%

if(oLoop == 5) {
	resetOloop = 0;
%>
<tr><td colspan=11>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
	  
      <td width="50" nowrap>&nbsp;</td>
      <td width="24" nowrap class="ColorGrey"> 
        
		<%
		// Chandrakanth IR PYUI121166801 12/17/2008 Begin
		if((!insertModeFlag || errorFlag) && TradePortalConstants.INDICATOR_YES.equals(allowDomesticPmtIndicator)) {
				domesticPaymtIndicator = true;
		} else {
			   domesticPaymtIndicator = false;
		}
		// Chandrakanth IR PYUI121166801 12/17/2008 End.
			out.print(InputField.createCheckboxField("domesticPmtPanelIndicator", TradePortalConstants.INDICATOR_YES, 
                                                    "", domesticPaymtIndicator, "ListText", "ListText", isReadOnly, 
                                                    "onClick=\"clearPanelFieldsForDomesticPmt()\""));
        %>
      </td>
 <%--
 
<% out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOid" + oLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthRange[oLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>"); %>
--%>
      <td width="1" nowrap class="ColorGrey">&nbsp;</td>
      <td nowrap colspan="2" class="ColorGrey"> 
        <p class="ListHeaderText">
          <%= resMgr.getText("CorpCust.DomesticPayment", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
  </tr>
  </table>

 <% } %>
  </td><tr>
  
  <tr><td> &nbsp;</td></tr>	
<%
if(oLoop == 11) {
	resetOloop = 0;
%>
<tr><td colspan=11>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
	  
      <td width="50" nowrap>&nbsp;</td>
      <td width="24" nowrap class="ColorGrey"> 
        
		<%
		// Chandrakanth IR PYUI121166801 12/17/2008 Begin
		if((!insertModeFlag || errorFlag) && TradePortalConstants.INDICATOR_YES.equals(allowInternationalPmtInd)) {
			intlPaymentIndicator = true;
		} else {
			intlPaymentIndicator = false;
		}
		// Chandrakanth IR PYUI121166801 12/17/2008 End
			out.print(InputField.createCheckboxField("IntlPmtPanelIndicator", TradePortalConstants.INDICATOR_YES, 
                                                    "", intlPaymentIndicator, "ListText", "ListText", isReadOnly, 
                                                    "onClick=\"clearPanelFieldsForIntlPmt()\""));
			
        %>
      </td>
<%-- 
<% out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOid" + oLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthRange[oLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>"); %>
--%>
     
      <td width="1" nowrap class="ColorGrey">&nbsp;</td>
      <td nowrap colspan="2" class="ColorGrey"> 
        <p class="ListHeaderText">
          <%= resMgr.getText("CorpCust.FundsTransferRequests", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
  </tr>
  </table>
 <% } %>
  </td><tr>
<%} %>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="80%" class="BankColor" >&nbsp;</td>
      <td width="9">&nbsp;</td>
      <td colspan="5" class="BankColor" nowrap> 
        <jsp:include page="/common/SaveDeleteClose.jsp">
           <jsp:param name="showSaveButton"   value="true" />
           <jsp:param name="showDeleteButton" value="false" />
           <jsp:param name="showCloseButton"  value="true" />
           <jsp:param name="showHelpButton"   value="false" />
           <jsp:param name="cancelAction"     value="goToCorporateCustomerDetail" />
        </jsp:include>
      </td>
    </tr>
  </table>
<%-- Chandrakanth IR PYUI121166801 12/17/2008 Begin --%>
<input type="hidden" name="saveFromPanelPage" value="Y"/>
<%-- Chandrakanth IR PYUI121166801 12/17/2008 End --%>

</form>
</body>

</html>

<%
   // Unregister the Corporate Org web bean that was used in this page
   //beanMgr.unregisterBean("CorporateOrganization");
   
	// Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
  
 	  // formMgr.storeInDocCache("default.doc", new DocumentHandler());
 	   //xmlDoc = formMgr.getFromDocCache();
 	   
 	   //xmlDoc.setAttribute("/In/CorporateOrganization/name", corporateOrgName);
 	  //xmlDoc.setAttribute("/In/CorporateOrganization/organization_oid", corporateOrgOid);
 	   //xmlDoc.setAttribute("/In/Update/SaveFromPanelPage",null);
 	   xmlDoc.setAttribute("/In/CorporateOrganization/allow_xfer_btwn_accts_panel", null);
 	   xmlDoc.setAttribute("/In/CorporateOrganization/allow_domestic_payments_panel", null);
 	   xmlDoc.setAttribute("/In/CorporateOrganization/allow_funds_transfer_panel", null);

%>