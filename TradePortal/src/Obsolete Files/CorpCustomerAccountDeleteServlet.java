package com.ams.tradeportal.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.NavigationManager;

public class CorpCustomerAccountDeleteServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}
		String delete=request.getParameter("delete");
		String corpOrgOid=request.getParameter("corp_org_oid");
		String acctOid=request.getParameter("acctOid");
		int rowsAffected=0;

		try{
			if(delete!=null && !delete.equals("")){
				//jgadela R91 IR T36000026319 - SQL INJECTION FIX
				String sqlQuery = "delete from ACCOUNT where P_OWNER_OID = ? and ACCOUNT_OID = ?";
				rowsAffected=DatabaseQueryBean.executeUpdate(sqlQuery, false, corpOrgOid, acctOid);
			}
		}catch (AmsException ae) {
			ae.printStackTrace();
		}
		catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		response.getWriter().println(rowsAffected);

	}

}




