<%--
*******************************************************************************
                              Current Terms Detail Page

  Description:
    This is the main driver for the Current Terms Detail page.  It handles data
  retrieval of terms and terms parties and creates the html page to display all
  the data for the transaction.  The Instrument's Oid is retrieved from the url
  which was passed in as a secureParameter (and encrypted).  Once we have the
  instrument Oid which should always be there - if not then this page will
  propogate errors- The Transaction oid is derived from the Active_transaction_oid
  which is set by OTL.  This jsp is designed to reuse the Terms Summary.jsp
  which is also called by the Transaction Terms Details.jsp.

  This jsp is designed to require the instrumentOid to be passed in.  It takes
  care of anything else it may need.
*******************************************************************************
--%>



<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   Debug.debug("***START******************** Transaction - Current Terms Detail **************************START***");
    /*********\
    * GLOBALS *
    \*********/
    WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
    boolean isReadOnly         = true;   //is the page read only
    boolean showSave           = false;  //do we show the save button
    boolean showDelete         = false;  //do we show the delete button
    boolean showTemplateSave   = false;
    boolean showTemplateDelete = false;
    boolean isTemplate	       = false;
    boolean isFromCurrentTerms = true;
    boolean isSummaryPage = false;

    String date 	       = "";   //universal variable to display a date.
    String currencyCode;
    String attribute;
    String displayText;

    String goToInstrumentNavigator;
    String goToInstrumentCloseNavigator;

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";
  String transactionOid = "";
  String instrumentType;
  String instrumentStatus;
  String transactionType;
  String transactionStatus;
  String termsPartyOid;

  //Variables used to pass parameters in the url - encrypted.
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String urlParm;

  String loginLocale = userSession.getUserLocale(); 	//need this for the formatting of dates
  String loginRights = userSession.getSecurityRights();


  // These are the beans used on the page.

  instrumentOid = (String)session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID);

  //Please note : the CSDB in the Transaction is required for the creation of the Terms WebBean -
  //When it makes the call to RegisterBankReleasedTerms()

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.TransactionWebBean",
                           "Transaction", resMgr.getCSDB());

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.InstrumentWebBean",
                           "Instrument");


  TransactionWebBean transaction 	  = (TransactionWebBean) beanMgr.getBean("Transaction");

  InstrumentWebBean instrument 		  = (InstrumentWebBean) beanMgr.getBean("Instrument");

  TermsWebBean terms             	  = null;
  TermsWebBean termsForLoan        	  = null; //PUUH062751147

  TermsPartyWebBean termsPartyApplicant   = (TermsPartyWebBean) beanMgr.createBean(
                             	    	    "com.ams.tradeportal.busobj.webbean.TermsPartyWebBean",
                             		    "TermsParty");
  TermsPartyWebBean termsPartyBeneficiary = (TermsPartyWebBean) beanMgr.createBean(
                             	 	    "com.ams.tradeportal.busobj.webbean.TermsPartyWebBean",
                             		    "TermsParty");

  String customerEnteredTerms = transaction.getAttribute("c_CustomerEnteredTerms");
  boolean hasCustEnteredTerms = InstrumentServices.isNotBlank( customerEnteredTerms );

  Debug.debug("populating beans from database");
  // We will perform a retrieval from the database.

//INSTRUMENT
  if (!InstrumentServices.isBlank(instrumentOid) ) {
	instrument.setAttribute("instrument_oid", instrumentOid);
	instrument.getDataFromAppServer();
  }else{
	System.out.println("Creation of Instrument WebBean Failed : instrumentOid from Instrument Summary was (in the url) was blank.");
  }

//MOST RECENT AMENDMENT TRANSACTION DATA
// Retrieve the transaction_oid of the most recent amendment/change/issuance transaction that was processed by bank.
// The most recent amendment transaction is the one that has the latest/highest transaction status date.
// This transaction contains the most recent applicant and bene information.  Cannot use active_transaction_oid here.
//
  StringBuffer amdTransactionSQL = new StringBuffer();

  amdTransactionSQL.append("select max(transaction_oid) as max_amd_transaction_oid ")
                   .append("from transaction ")
                   .append("where p_instrument_oid = ").append(instrumentOid)
                   .append(" and (transaction_type_code = '").append(TradePortalConstants.AMEND).append("' or transaction_type_code = '").append(TradePortalConstants.CHANGE).append("')")
                   .append(" and transaction_status = '").append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).append("'")
                   .append(" and transaction_status_date = (select max(transaction_status_date)")
                   .append("                               from transaction")
                   .append("                               where p_instrument_oid = ").append(instrumentOid)
                   .append("                               and (transaction_type_code = '").append(TradePortalConstants.AMEND).append("' or transaction_type_code = '").append(TradePortalConstants.CHANGE).append("')")
                   .append("                               and transaction_status = '").append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).append("')");

  DocumentHandler amdTransactionResult = DatabaseQueryBean.getXmlResultSet(amdTransactionSQL.toString());
  if (amdTransactionResult != null) {
     transactionOid = amdTransactionResult.getAttribute("/ResultSetRecord(0)/MAX_AMD_TRANSACTION_OID");

		 // GGAYLE - 12/16/2004 - IR NOUE110264473
	   if (InstrumentServices.isBlank( transactionOid ) ) {
				 transactionOid = instrument.getAttribute("original_transaction_oid");	// Use the original transaction oid instead
		 }
		 // GGAYLE - 12/16/2004 - IR NOUE110264473
  }

  if (!InstrumentServices.isBlank( transactionOid ) ) {
        transaction.setAttribute("transaction_oid", transactionOid);
        transaction.getDataFromAppServer();
  }else{
	System.out.println("Creation of Transaction WebBean Failed : the transaction oid could not be retrieved.");
  }

  terms = transaction.registerBankReleasedTerms();
  termsForLoan = terms; //PUUH062751147
%>

   <%@ include file="fragments/LoadTermsDetailsParties.frag" %>
<%
//ACTIVE TRANSACTION DATA
  transactionOid = instrument.getAttribute("active_transaction_oid");
  if (!InstrumentServices.isBlank( transactionOid ) ) {
        transaction.setAttribute("transaction_oid", transactionOid);
        transaction.getDataFromAppServer();
  }else{
	System.out.println("Creation of Transaction WebBean Failed : the active_transaction_oid on the Instrument was blank.");
  }

  terms = transaction.registerBankReleasedTerms();

  // Now load the termsPartyApplicant and termsPartyBeneficiary
  // webbeans.
%>


<%
  transactionType   = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");

  instrumentType    = instrument.getAttribute("instrument_type_code");
  instrumentStatus  = instrument.getAttribute("instrument_status");

  Debug.debug("Instrument Type "    + instrumentType);
  Debug.debug("Instrument Status "  + instrumentStatus);
  Debug.debug("Transaction Type "   + transactionType);
  Debug.debug("Transaction Status " + transactionStatus);

  transactionOid = transaction.getAttribute("transaction_oid");
  instrumentOid  = instrument.getAttribute("instrument_oid");
  encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes( transactionOid , userSession.getSecretKey()); 	//Transaction_Oid
  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( instrumentOid , userSession.getSecretKey());	//Instrument_Oid
  encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( "true" , userSession.getSecretKey());		//From Transaction-TermsDetails.jsp flag

  urlParm = "&oid=" + encryptVal1 + "&instrument_oid=" + encryptVal2 + "&isFromTransTermsFlag=" + encryptVal3;


  // Teh summary section displays differently based on the instrument type.
  boolean isFundsXfer = false;
  boolean isLoanRqst = false;
  boolean isRFInstrument = false;//IR - RIUJ021980931 Vshah

  // Certain fields display based on instrument types of funds transfer or loan request.
  if (instrumentType.equals(TradePortalConstants.FUNDS_XFER)) isFundsXfer = true;
  if (instrumentType.equals(TradePortalConstants.LOAN_RQST)) isLoanRqst = true;
  if (instrumentType.equals(TradePortalConstants.RECEIVABLES_FINANCE_INSTRUMENT)) isRFInstrument = true; //IR - RIUJ021980931 Vshah
%>

<%-- Body tag included as part of common header --%>
<%-- HTML Code Begins here			--%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<form name="AuthorizationErrorsDetail" method="post" action="<%=formMgr.getSubmitAction(response)%>">

<%
  // The breadcrumb bar is slightly different from the standard breadcrumb bar so
  // we'll write our own instead of using the common one.
  StringBuffer parms = new StringBuffer();

  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToInstrumentSummary", userSession.getSecretKey()));
  parms.append("&instrument_oid=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrument.getAttribute("instrument_oid"), userSession.getSecretKey()));

  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  StringBuffer breadCrumb = new StringBuffer();

  breadCrumb.append(instrument.getAttribute("complete_instrument_id"));
  breadCrumb.append("&nbsp;&nbsp;");
  breadCrumb.append(refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                                     instrument.getAttribute("instrument_type_code"),
                                     loginLocale));
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr class="BankColor">
      <td width="15" nowrap>&nbsp;</td>
      <td class="BankColor" nowrap height="34">
        <span class="Tabtext">
          <span class="Links">
            <a href=<%=formMgr.getLinkAsUrl("goToInstrumentCloseNavigator",
                                            parms.toString(),
                                            response) %>
               class="Links">
               <%=breadCrumb%>
            </a>
          </span>
          <span class="ControlLabelWhite">
            &gt; <%=resMgr.getText("TransactionTerms.CurrentTermDetails",
                                   TradePortalConstants.TEXT_BUNDLE)%>
          </span>
        </span>
      </td>
      <td width="100%" class="BankColor" height="34">&nbsp;</td>
      <td width="15" nowrap>
        <%=OnlineHelp.createContextSensitiveLink("customer/current_terms.htm",
                                                 resMgr, userSession)%>
      </td>
      <td width="15" nowrap>
        <img src="/portal/images/Blank_15.gif" width="15" height="15">
      </td>
    </tr>
  </table>


<%-- *************** Calls to the other jsp(s) goes here *************** --%>

  <%@ include file="fragments/Transaction-TermsDetails-ActionButtons.frag" %>

  <%@ include file="fragments/Transaction-Terms-Summary.frag" %>

  <%@ include file="fragments/Transaction-TermsDetails-ActionButtons.frag" %>

</body>
</html>
<%
  /**********
   * CLEAN UP
   **********/

   Debug.debug("***END******************** Transaction - Current Terms Detail **************************END***");
%>

