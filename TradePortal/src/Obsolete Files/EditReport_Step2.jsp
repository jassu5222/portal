<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.businessobjects.rebean.wi.*,com.crystaldecisions.sdk.framework.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%--commented by Kajal Bhatt
<%WISession webiSession = null; %>--%>

<%
Debug.debug("***START*********************EDIT*REPORT*STEP2***************************START***"); 
//Set current page 
//Since this page is directly acessed form the applet 
//set the current page in Navigation manager
formMgr.setCurrPage("goToEditReportStep2");


String              storageToken        = null;
StringBuffer        newLink             = new StringBuffer();
//commented by kajal bhatt
//we require DocumentInstance instead of WIDocument
//WIDocument          webiDocument        = null;
DocumentInstance    webiDocument        = null;
//commented by kajal bhatt
//WIOutput            webiOutput          = null;
HTMLView            webiOutput          = null;
String              currRepCat          = null;
String              mode                = TradePortalConstants.REPORT_EDIT;
String              editReportName      = null;
String              reportMode          = null;
MediatorServices    mediatorServices        = null;
DocumentHandler     xmlDoc                  = null;
String              physicalPage            = null;

storageToken = request.getParameter("entry");

reportMode = request.getParameter("reportMode");
Debug.debug("REPORT MODE IN EDIT REPORT STEP2#####################"+reportMode+"#############");



if (storageToken == null || storageToken.equals(""))
    storageToken = (String) session.getAttribute("storageToken");

%>

<%@ include file="fragments/wistartpage.frag" %>

<%
    

// retrieve the document
if (storageToken != null)
{
    webiDocument = reportEngine.getDocumentFromStorageToken(storageToken);
    webiOutput = (HTMLView) webiDocument.getView(OutputFormatType.HTML);
    editReportName = webiDocument.getProperties().getProperty(PropertiesType.NAME); 
}
else
{
    System.out.println("STORAGE TOKEN IS NULL IN EDIT REPORT STEP2 ##################");
}


%>


<%--*******************************HTML for page begins here **************--%>

<%-- Begin HTML header decleration --%>    

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%-- End HTML header decleration --%>    


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="" bgcolor="#FFFFFF"><form method="post" action="../FullHTML/Replace"100%" border="0" cellspacing="0" cellpadding="0" height="34">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" nowrap height="35"> 
      <%
      if (reportMode.equals(TradePortalConstants.NEW_STANDARD_REPORTS) ||
          reportMode.equals(TradePortalConstants.NEW_CUSTOM_REPORTS))
      {
      %>
        <p class="ControlLabelWhite"><%=resMgr.getText("Reports.NewStep2Text", TradePortalConstants.TEXT_BUNDLE) %><img src="/portal/images/Blank_15.gif" width="50" height="15"></p>
      <%
      }
      
      if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM ) ||
          reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD ))
      {
      %>
        <p class="ControlLabelWhite"><%=resMgr.getText("Reports.EditStep2Text", TradePortalConstants.TEXT_BUNDLE) %><img src="/portal/images/Blank_15.gif" width="50" height="15"></p>
      <%
      }
      %>
      
      </td>
      <td width="90%" class="BankColor" height="35">
        <p class="ControlLabelWhite"><%=resMgr.getText("Reports.Step2Heading", TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td class="BankColor" height="35">&nbsp;</td>
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/report_step2.htm",resMgr,userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  
   
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr valign=bottom> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%">
        <p class="ListText"><%=resMgr.getText("Reports.Step2", TradePortalConstants.TEXT_BUNDLE) %></p>
      
      </td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
                      
            <%
            //Reports previous step button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToNewReportStep1", response));
            newLink.append("&storageToken=" + storageToken);
            newLink.append("&reportMode=" + reportMode);
            %>
                      
            <td height="45">
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="ReportsStep1" />
                <jsp:param name="image" value="Reports.PreviousImg" />
                <jsp:param name="text"  value="Reports.PreviousStep" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
           <td width="15" nowrap>&nbsp;</td>
           
            <%
            //Reports next step button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToEditReportStep3", response));
            newLink.append("&storageToken=" + storageToken);
            newLink.append("&reportMode=" + reportMode);
            newLink.append("&editReportName=" + editReportName);
            %>
      
            <td height="45">
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="EditReportNextStep" />
                <jsp:param name="image" value="Reports.EditReportNextStepImg" />
                <jsp:param name="text"  value="Reports.EditReportNextStep" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            
            
            <%
            //Reports home button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
            %>
            
            <td height="45">
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="CancelReportSave" />
                <jsp:param name="image" value="common.CancelImg" />
                <jsp:param name="text"  value="common.CancelText" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
            <td width="20" nowrap>&nbsp;</td>
            
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr align="center"> 
      <td>
        
        <jsp:include page="/reports/ReportsDetail.jsp">
        <jsp:param name="storageToken" value="<%= storageToken %>" />
        </jsp:include>
        
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="40">
    <tr valign=bottom> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="BankColor" width="100%"><p class="ListText">&nbsp;</p></td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45">
          <tr height="45"> 
                      
            <%
            //Reports previous step button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToNewReportStep1", response));
            newLink.append("&storageToken=" + storageToken);
            newLink.append("&reportMode=" + reportMode);
            %>
                      
            <td>
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="ReportsStep1" />
                <jsp:param name="image" value="Reports.PreviousImg" />
                <jsp:param name="text"  value="Reports.PreviousStep" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
           <td width="15" nowrap>&nbsp;</td>
           
            <%
            //Reports next step button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToEditReportStep3", response));
            newLink.append("&storageToken=" + storageToken);
            newLink.append("&reportMode=" + reportMode);
            newLink.append("&editReportName=" + editReportName);
            %>
      
            <td>
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="EditReportNextStep" />
                <jsp:param name="image" value="Reports.EditReportNextStepImg" />
                <jsp:param name="text"  value="Reports.EditReportNextStep" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            
            
            <%
            //Reports home button
            newLink = new StringBuffer();
            newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
            %>
            
            <td>
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="CancelReportSave" />
                <jsp:param name="image" value="common.CancelImg" />
                <jsp:param name="text"  value="common.CancelText" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="96" />
                </jsp:include>
            </td>
            <td width="20" nowrap>&nbsp;</td>
            
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>
</body>

</html>

<%--<%
session.setAttribute("storageToken", storageToken);
%> --%>

<%
Debug.debug("***END*********************EDIT*REPORT*STEP2***************************END***"); 
%>