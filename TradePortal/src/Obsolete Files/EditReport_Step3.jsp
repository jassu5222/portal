<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,com.ams.tradeportal.common.cache.Cache,com.ams.tradeportal.common.cache.TPCacheManager" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%--commented by Kajal Bhatt
we require EnterpriseSession instead of WISession
<%WISession webiSession = null; %>--%>

<%
Debug.debug("***START*********************EDIT*REPORT*STEP3***************************START***"); 

DocumentHandler     standardDocHdr              = null;
DocumentHandler     customDocHdr                = null;
StringBuffer        standardDropdownOptions     = new StringBuffer();
StringBuffer        customDropdownOptions       = new StringBuffer();
String              storageToken                = null;
StringBuffer        newLink                     = new StringBuffer();
boolean             standardReportSelected      = false;
boolean             publishSelected             = false;
DocumentHandler     xmlDoc                      = null;
String              hasErrors                   = null;
String              reportName                  = null;
String              repName                     = null;
String              reportDesc                  = null;
String              reportType                  = null;
String              reportMode                  = null;
String              editReportName              = null;
//commented by kajal bhatt
//WIDocument          webiDocument                = null;
DocumentInstance    webiDocument                = null;
String              getOwnershipLevel           = null;
MediatorServices    mediatorServices            = null;
String              physicalPage                = null;


Debug.debug("REPORT MODE IN EDIT REPORT STEP3##########################"+reportMode+"#####################");

storageToken     = request.getParameter("storageToken");
reportMode       = request.getParameter("reportMode");
editReportName   = request.getParameter("editReportName");
reportDesc = request.getParameter("reportDesc");

//rbhaduri - 29th Jul 09 - PPX-051 - Add Begin 
if (editReportName == null || editReportName.equals("null")){
    editReportName = "";
}

if (reportDesc == null || reportDesc.equals("null")){
    reportDesc = "";
}
//rbhaduri - 29th Jul 09 - PPX-051 - Add End




//Get ownership level
getOwnershipLevel = userSession.getOwnershipLevel();

Debug.debug("Ownership level in EditReport_Step3  ########"+getOwnershipLevel);

//Process for Copy this report mode
if (reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) )
{
   reportName = resMgr.getText("Reports.CopyOf", TradePortalConstants.TEXT_BUNDLE);
   
}

%>

<%@ include file="fragments/wistartpage.frag" %>

<%

if (storageToken != null)
{
    //Retrieve report from repository using storage token
    webiDocument = reportEngine.getDocumentFromStorageToken(storageToken);
    repName = webiDocument.getProperties().getProperty(PropertiesType.NAME);
    reportDesc = webiDocument.getProperties().getProperty(PropertiesType.DESCRIPTION); 
    
    
     //rbhaduri - 29th Jul 09 - PPX-051 
    if (reportDesc == null || reportDesc.equals("null")){
        reportDesc = "";
    }
}
else
{
     //Catch all exception
     System.out.println("storagrToken is null in Edit Report Step 3 ######################");
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}


if( boSession != null )
{
    //commented by kajal bhatt, we have to change the method parameter of RepUtility.getCategories()
	//Retrieve Standard and Custom category list from the repository
    standardDocHdr = RepUtility.getCategories(boSession, TradePortalConstants.STANDARD_REPORTS,beanMgr, userSession.getUserOid());
    customDocHdr = RepUtility.getCategories(boSession, TradePortalConstants.CUSTOM_REPORTS, beanMgr, userSession.getUserOid());
}
else
{
     //Catch all exception
     System.out.println("webiSession is null in Edit Report Step 3 ######################");
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%   
}

//Append report name with "Copy of" if report mode is REPORT_COPY_CUSTOM or 
//REPORT_COPY_STANDARD
Debug.debug("REPORT NAME in edit report step 3###################"+reportName);
if (reportName != null)
{
    repName = reportName+repName;
}

Debug.debug("REPORT NAME in edit report step 3###################"+repName);

//Process for Save as Standard Report mode
//commented by kajal bhatt
/* In XIR2 there is no way to detach the category, the only way is while saving
the document pass the category as null*/
/*if (reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM) ||
    reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM))
{
    String [] categories;
    if ( (categories = webiDocument.getCategoryList()) != null )

    for (int i=0;i<categories.length;i++)
	    webiDocument.detachCategory(categories[i]);
}*/
xmlDoc = formMgr.getFromDocCache();
hasErrors = xmlDoc.getAttribute("/In/HasErrors");

if ((hasErrors != null) && (hasErrors.equals(TradePortalConstants.INDICATOR_YES)))
{
    editReportName = xmlDoc.getAttribute("/In/ReportName");
    reportDesc = xmlDoc.getAttribute("/In/ReportDesc");
    reportType = xmlDoc.getAttribute("/In/ReportType");
    //String standardReport  = xmlDoc.getAttribute("/In/ReportType");
    
      //rbhaduri - 29th Jul 09 - PPX-051 
    if (reportDesc == null || reportDesc.equals("null")){
        reportDesc = "";
    }
        
}
%>



<%--*******************************HTML for page begins here **************--%>

<!-- Begin HTML header decleration -->    

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<!-- End HTML header decleration -->    

<script LANGUAGE="JavaScript">
  function pickUser(selectUser) {
    // When the user drop down box has been clicked,
    // set the first StandardReport to 'Y' and the second to 'N' for the radio buttons
    //var size=document.saveReport.StandardReports.length;
    
    document.saveReport.StandardReport[0].checked = selectUser;
    document.saveReport.StandardReport[1].checked = !selectUser;    
    
   
  }
</script>

<script LANGUAGE="JavaScript">
  function pickCat(selectUser) {
    // When the user drop down box has been clicked,
    // set the first StandardReport to 'Y' and the second to 'N' for the radio buttons
    //var size=document.saveReport.StandardReports.length;
    document.saveReport.StandardReport.checked = selectUser;
       
  }
</script>



<form name="saveReport" method="POST" action="<%= formMgr.getSubmitAction(response) %>">

  <input type=hidden value="" name=buttonName>

<%= formMgr.getFormInstanceAsInputField("saveReport") %>

 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" nowrap height="35"> 
        <%
        if (reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD))
        {
        %>
            <p class="ControlLabelWhite"><%=resMgr.getText("Reports.SaveasStandardReportText", TradePortalConstants.TEXT_BUNDLE) %><img src="/portal/images/Blank_15.gif" width="50" height="15"></p>
        <%
        }
        else if (reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
                 reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM))
        {
        %>
            <p class="ControlLabelWhite"><%=resMgr.getText("common.ReportCopyText", TradePortalConstants.TEXT_BUNDLE) %><img src="/portal/images/Blank_15.gif" width="50" height="15"></p>
        <%    
        }
        else
        {
        %>
            <p class="ControlLabelWhite"><%=resMgr.getText("Reports.NewStep3Text", TradePortalConstants.TEXT_BUNDLE) %><img src="/portal/images/Blank_15.gif" width="50" height="15"></p>
            
        <%
        }
        %>
      </td>
      <td width="90%" class="BankColor" height="35">
      <%
      if (!reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) &&
          !reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) &&
          !reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM))
      {
      %>
          <p class="ControlLabelWhite"><%=resMgr.getText("Reports.Step3Heading", TradePortalConstants.TEXT_BUNDLE) %></p>
      <%
      }
      else
      {
        %>
        &nbsp;
        <%
      }
      %>
      </td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td class="BankColor" height="35">&nbsp;</td>
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/report_step3.htm",resMgr,userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%">&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> 
        <p class="ListText"><%=resMgr.getText("Reports.Step3", TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <p class="ControlLabel"><%=resMgr.getText("Reports.Info", TradePortalConstants.TEXT_BUNDLE) %>  </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="40" nowrap>&nbsp;</td>
      <td>
        <p class="ControlLabel"><%=resMgr.getText("Reports.ReportName", TradePortalConstants.TEXT_BUNDLE) %>   </p>
      </td>
      
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="ListText" nowrap> 
         
         <%
         //Display the report name if report mode is EDIT or SaveAS
         if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM) ||  
             reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD) ||
             reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD))
         {
                   
         %>
            <input type="hidden" name="ReportName"    value="<%= editReportName %>" />
         <%
            
            //rbhaduri -PPX-051 - 1st Jul 09 - made this input box editable by chaning the last argument from false(read only) to true
            out.print(InputField.createTextField("ReportNameEdit", editReportName, "35", "36", "ListText", false)); 
         }
         else if (reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
                  reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM)  )
         {
            out.print(InputField.createTextField("ReportName", repName, "35", "36", "ListText", false)); 
            
         }
         else         
         {
            editReportName = " ";
            out.print(InputField.createTextField("ReportName", editReportName, "35", "36", "ListText", false)); 
            
         }
         %> 
          
        
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <p class="ControlLabel"><%=resMgr.getText("Reports.Desc", TradePortalConstants.TEXT_BUNDLE) %> </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="ListText"> 
        <input type="text" name="ReportDesc" size="35" maxlength="35" value="<%=reportDesc%>" class="ListText">
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr> 
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel"><%=resMgr.getText("Reports.SpecifyType", TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    
    <tr>
        <td>&nbsp;</td>
        <td class="ListText" nowrap>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    
  </table>
      
  <%
  if (!reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM))
  {
  %>
        
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="60" nowrap>&nbsp;</td>
        <td > 
            <%
            if (reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) ||
                reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
            {
            %>
                <input type="hidden" name="StandardReport" value="Y" />
                &nbsp;
            <%
            }
            else
            {
            %>
	            <%=InputField.createRadioButtonField("StandardReport", "Y", "", standardReportSelected, "ListText", "", false)%>
	        <%
	        }
	        %>
        </td>
        <td width="5" nowrap>&nbsp;</td>
        <td nowrap>
            <p class="ListText"><%=resMgr.getText("Reports.StandardReportsCategory", TradePortalConstants.TEXT_BUNDLE) %></p>
        </td>  
        <td width="100%">&nbsp;</td>
    </tr>
  </table>
            
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>   
        <td width="100" nowrap>&nbsp;</td>
        <td nowrap>
            <p class="ListText"><%=resMgr.getText("Reports.StandardCategory", TradePortalConstants.TEXT_BUNDLE) %> <br>
            <%
            //Narayan CR-603 06/27/2011 while editing reprot include All Category only if report category indicator is off.
                  String includeAllCategories = TradePortalConstants.INDICATOR_YES;
		          UserWebBean thisUser = (UserWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.UserWebBean","User");      
	              thisUser.setAttribute("user_oid", userSession.getUserOid());
     	          thisUser.getDataFromAppServer();  
                  String clientBankOid = thisUser.getAttribute("client_bank_oid");         
     	          
     	          if (clientBankOid != null && !clientBankOid.equals("")) {
     		          Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
     		          DocumentHandler CBCResult = (DocumentHandler)CBCache.get(clientBankOid);
     		          includeAllCategories = CBCResult.getAttribute("/ResultSetRecord(0)/REPORT_CATEGORIES_IND");
     	           }
                    
            // W Zhu CR-603 4/27/2011 Consider user categories.
                    //standardDropdownOptions.append(Dropdown.getUnsortedOptions(standardDocHdr, "CATEGORY", "CATEGORY", "", false));
      				//Ravindra - Rel7000 - IR# RUUL070741274  - 07/11/2011 - Start
      				//ANZ -> CR -603 -Report Categories  -  Admin user should see only  the category associated with the Corporate Customer.
      				//Ravindra - Rel7000 - IR# DVUL070864037  - 07/12/2011 - Start
                    standardDropdownOptions.append(RepUtility.getUserCategoryOptions(standardDocHdr, "", resMgr.getResourceLocale(), beanMgr, userSession.getUserOid(),includeAllCategories,userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType()));
                  //Ravindra - Rel7000 - IR# DVUL070864037  - 07/12/2011 - End
      				//Ravindra - Rel7000 - IR# RUUL070741274  - 07/11/2011 - End
                    
                    if (reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) &&
                        reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
                        out.print(InputField.createSelectField("StandardCategory", "", "", standardDropdownOptions.toString(), "ListText", false, ""));
                    else
                        out.print(InputField.createSelectField("StandardCategory", "", "", standardDropdownOptions.toString(), "ListText", false, "onClick=\"pickUser(true)\""));
                       
            %>     
            </p>
        </td>
        <td width="100%">&nbsp;</td>
    </tr>
           
    <%
    //Display selective publishing options only for BOG and Bank users
    if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK) ||
        getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
    {
    %>
        <tr>
            <td width="100" nowrap>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="100%">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p class="ListText"><%=resMgr.getText("Reports.PublishText", TradePortalConstants.TEXT_BUNDLE) %></p>
            </td>
        </tr>
        <tr>
            <td width="100" nowrap>&nbsp;</td>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="5" nowrap>&nbsp;</td>
                        <td>
                            <%=InputField.createRadioButtonField("Publish", "Y", "", publishSelected, "ListText", "", false)%> 
                        </td>
                        <td width="5" nowrap>&nbsp;</td>
                        <td nowrap>
                        <%
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK))
                        {
                        %>
                            <p class="ListText"><%=resMgr.getText("Reports.ClientBankPublishAll", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
                                
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
                        {
                        %>
                            <p class="ListText"><%=resMgr.getText("Reports.BOGPublishAll", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
                        %>
                        </td>  
                        <td width="100%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <%=InputField.createRadioButtonField("Publish", "N", "", publishSelected, "ListText", "", false)%>  
                        </td>
                        <td width="5" nowrap>&nbsp;</td>
                        <td nowrap>
                        <%
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK))
                        {
                        %>
                            <p class="ListText"><%=resMgr.getText("Reports.ClientBankSelect", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
                                    
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
                        {
                        %>
                            <p class="ListText"><%=resMgr.getText("Reports.BOGSelect", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
                        %>
                        </td>  
                        <td width="100%">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="100%">&nbsp;</td>
        </tr>
        <%
        } //end if
        %> 
             
  </table>

  <%
  } //end if
  %>

    
  <%
  if (!reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) &&
      !reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
  {
  %>
            
        <br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="60" nowrap>&nbsp;</td>
                <td >
                    <%
                    if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM))
                    {
                    %>
                        <input type="hidden" name="StandardReport" value="N" />
                        &nbsp;
                    <%
                    }
                    else
                    {
                    %>
	                    <%=InputField.createRadioButtonField("StandardReport", "N", "", standardReportSelected, "ListText", "", false)%>
	                <%
	                }
        	        %>
                </td>                
                <td width="5" nowrap>&nbsp;</td>
                <td nowrap>
                    <p class="ListText"><%=resMgr.getText("Reports.CustomReportsCategory", TradePortalConstants.TEXT_BUNDLE) %> </p>
                </td>  
                <td width="100%">&nbsp;</td>
                    </tr>
        </table>
                
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>   
                <td width="100" nowrap>&nbsp;</td>
                <td nowrap>
                    <p class="ListText"><%=resMgr.getText("Reports.CustomCategory", TradePortalConstants.TEXT_BUNDLE) %> <br>
                    <%
                        
                        
                        customDropdownOptions.append(Dropdown.getUnsortedOptions(customDocHdr, "CATEGORY", "CATEGORY", "", false));
                       
                        if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM))
                            out.print(InputField.createSelectField("CustomCategory", "", "", customDropdownOptions.toString(), "ListText", false, ""));
                        else
                            out.print(InputField.createSelectField("CustomCategory", "", "", customDropdownOptions.toString(), "ListText", false, "onClick=\"pickUser(false)\""));

                    %>     
                    </p>
                </td>
                <td width="100%">&nbsp;</td>
            </tr>
        </table> 
        
   <%
   }
   %>
  
    
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  
  <input type="hidden" name="storageToken"    value="<%= storageToken %>" />
  <input type="hidden" name="reportMode"    value="<%= reportMode %>" />
  
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45" class="BankColor">
    <tr valign=bottom> 
      <td width="100%">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      
      <%
    
      //Previous Step
      if (!reportMode.equals(TradePortalConstants.REPORT_SAVEAS_STANDARD) &&
          !reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) &&
          !reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM))
      {
        
           
        newLink = new StringBuffer();
        newLink.append(formMgr.getLinkAsUrl("goToEditReportStep2", response));
        newLink.append("&entry=" + storageToken);
        newLink.append("&reportMode=" + reportMode);
                          
      %>
                
        <td>
            <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="ReportsStep2" />
            <jsp:param name="image" value="Reports.PreviousImg" />
            <jsp:param name="text"  value="Reports.PreviousStep" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="96" />
            </jsp:include>
        </td>
        <td width="15" nowrap>&nbsp;</td>
          
      <%
      }
      %>    
      
      <td class="BankColor" align="left" valign="bottom">
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
          <jsp:param name="showButton" value="true" />
          <jsp:param name="name"       value="SaveReport" />
          <jsp:param name="image"      value='Reports.SaveImg' />
          <jsp:param name="text"       value='Reports.SaveText' />
          <jsp:param name="width"        value="96" />
          <jsp:param name="submitButton" value="<%=TradePortalConstants.CUSTOM_REPORTS%>" />
        </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      
           
      <%
         
      newLink = new StringBuffer();
      newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
                      
      %>
            
      <td>
        <jsp:include page="/common/RolloverButtonLink.jsp">
        <jsp:param name="name"  value="CancelReportSave" />
        <jsp:param name="image" value="common.CancelImg" />
        <jsp:param name="text"  value="common.CancelText" />
        <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
        <jsp:param name="width" value="96" />
        </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>
</body>

</html>
 
<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%
Debug.debug("***END*********************EDIT*REPORT*STEP3***************************END***"); 
%>