<%--
*****************************************************************************************************
                  Direct Debit Transaction History Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>

<input type="hidden" name="NewSearch" value="Y">
	<div id="advanceDirectDebitFilter" style="padding-top: 5px;">
		<div>
			<div class="inline">
				<%=widgetFactory.createLabel("","Transaction ID:",false, false, false, "inline")%>
				<%= widgetFactory.createTextField("InstrumentId","",instrumentId, "30", false, false, false, "width=11",  "", "inline")%>
	
				<%=widgetFactory.createLabel("","Credit Account No:",false, false, false, "inline")%>
				<%= widgetFactory.createTextField("CreditAcct","",creditAcct, "30", false, false, false,"width=11",  "", "inline")%>
	
				<%=widgetFactory.createLabel("","Reference No:",false, false, false, "inline")%>									
				<%= widgetFactory.createTextField("Reference","",refNum, "30", false, false, false, "width=11",  "", "inline")%>
			</div>
			<div class="title-right inline">							
				<button data-dojo-type="dijit.form.Button" type="button">
					<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	    			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					searchDirectDebitHistory();return false;
    			</script>
	    		</button>
	    		</br>
				<a href="<%=linkHref%>"><%=linkName%></a>	
			</div>
			<div style="clear:both"></div>
		</div>

		<div>
			<%=widgetFactory.createLabel("","Execution Date:",false, false, false, "inline")%>
			<%=widgetFactory.createSearchDateField("FromDate", "From", "inline")%>
			<%=widgetFactory.createSearchDateField("ToDate", "To", "inline")%>
			<%= widgetFactory.createPartyClearButton("ClearDateFields", "resetDates();",false,"")%>
		</div>
		
		<div>
			<%=widgetFactory.createLabel("","Amount:",false, false, false, "inline")%>
			<%=widgetFactory.createSearchAmountField("AmountFrom", "From","","inline")%>
			<%=widgetFactory.createSearchAmountField("AmountTo", "To","","inline")%>
			<div style="clear:both"></div>
		</div>
	</div>