<%--
*******************************************************************************
                    Direct Debit Transaction History Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Direct Debit Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">
	
	<div id="basicDirectDebitFilter" style="padding-top: 5px;">
		<%//=widgetFactory.createSearchTextField("InstrumentId","Transaction ID","30","width='10'")%>
		<%//=widgetFactory.createSearchTextField("CreditAcct","Credit Account No","30","")%>
		<%//=widgetFactory.createSearchTextField("Reference","Reference No:","30","")%>
		<div class="inline">
			<%=widgetFactory.createLabel("","Transaction ID",false, false, false, "inline")%>
			<%=widgetFactory.createTextField("InstrumentId","", instrumentId, "30", false, false, false, "width=11",  "", "inline")%>
			
			<%=widgetFactory.createLabel("","Credit Account No",false, false, false, "inline")%>
			<%=widgetFactory.createTextField("CreditAcct","",creditAcct, "30", false, false, false, "width=11",  "", "inline")%>
			
			<%=widgetFactory.createLabel("","Reference No",false, false, false, "inline")%>									
			<%=widgetFactory.createTextField("Reference","",refNum, "30", false, false, false, "width=11",  "", "inline")%>
		</div>						
		<div class="title-right inline">
			<button data-dojo-type="dijit.form.Button" type="button">
				<%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				searchDirectDebitHistory();return false;
    		</script>
			</button>
			</br>
			<a href="<%=linkHref%>"> <%=linkName%></a>	
		</div>
		<div style="clear: both;"></div>
	</div>

	<script>
		function disableOtherSearchFields(type) {
			console.log('type='+type);
			var ca = dijit.byId('CreditAcct');
			var inst = dijit.byId('InstrumentId');
			var re = dijit.byId('Reference');
			if (type=='I'){
				inst.attr('enabled','true');
				ca.attr('disabled','true');
				re.attr('disabled','true');
			} 
			if (type=='C'){
			ca.attr('enabled','true');
			inst.attr('disabled','true');
			re.attr('disabled','true');
			} 
			if (type=='R'){
				re.attr('enabled','true');
				ca.attr('disabled','true');
				inst.attr('disabled','true');
			} 
		}
	</script>