<!DOCTYPE HTML>
<%--
**********************************************************************************
  Direct Debit Transactions Home

  Description:  
     This page is used as the main Direct Debits page.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 
<%
   StringBuffer      dynamicWhereClause = new StringBuffer();
   StringBuffer      onLoad             = new StringBuffer();
   Hashtable         secureParms        = new Hashtable();
   String            helpSensitiveLink  = null;
   String            userSecurityRights = null;
   String            userSecurityType   = null;
   String            current2ndNav      = null;
   String            userOrgOid         = null;
   String            userOid            = null;
   String            formName           = "";
   StringBuffer      newLink            = new StringBuffer();
   StringBuffer orgList = new StringBuffer();
   boolean           canViewDirectDebitInstruments = false;  // NSX CR-509 12/07/2009 
   WidgetFactory widgetFactory = new WidgetFactory (resMgr);
   boolean orgListDropDown = false;

   //rbhaduri - CR-374 - 14th Oct 08
   String 	     loginLocale 		  = null;
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
                                         
   StringBuffer newSearchReceivableCriteria = new StringBuffer();//PPramey - CR-434 - 22th Nov 08                                         

   //set the return actions
   session.setAttribute("PAYMENTREMITDETAIL","goToReceivablesManagement");
   session.setAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_CLOSE_ACTION, "goToDirectDebitTransactionsHome");
   session.setAttribute(TradePortalConstants.INSTRUMENT_CLOSE_ACTION,         "goToDirectDebitTransactionsHome"); // NSX CR-509 12/07/2009 

   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();

   //IR - SAUJ122835498 - Start
   CorporateOrganizationWebBean org = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
   org.getById(userOrgOid);
   //IR - SAUJ122835498 - End
   
   canViewDirectDebitInstruments = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_DIRECTDEBIT_AREA); // NSX CR-509 12/07/2009 
   
   // PKUK012160488 End
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");



	   formName     = "DirectDebit-TransactionForm";
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);
       %>
       <%@ include file="/receivables/fragments/DirectDebit-TranSearchErrorParms.frag" %>
 <% 
  // NSX CR-509 12/07/2009 ADD END
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

<% //cr498 reauthenticate if necessary
    String certAuthURL = ""; //needed for openReauth frag
    
    Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
    
    String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
    
    boolean matchRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE);
    boolean approveDiscountRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT);
    boolean closeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE);
    boolean financeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE);
    boolean disputeInvoiceRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE);
    boolean ddiRequireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,InstrumentAuthentication.TRAN_AUTH__DDI_ISS);
    
    String authorizeLink = "";
    String approveDiscountLink = "";
    String closeInvoiceLink = "";
    String financeInvoiceLink = "";
    String disputeInvoiceLink = "";
    String ddiLink = "";
    
    if ( matchRequireAuth||approveDiscountRequireAuth||
         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth||
         ddiRequireAuth ) {
%>   
  <%@ include file="/logon/fragments/openReauthenticationWindow.frag" %>
<%
        authorizeLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'PayRemit')";
        approveDiscountLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
	    "'" + formName + "','ApproveDiscountAuthorize'," +
            "'PayRemit')";
        closeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS + "'," +
            "'Invoice')";
        financeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS + "'," +
            "'Invoice')";
        disputeInvoiceLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS + "'," +
            "'Invoice')";
        ddiLink = "javascript:openReauthenticationWindowForList('" + certAuthURL + "'," +
            "'" + formName + "','" + TradePortalConstants.BUTTON_AUTHORIZE + "'," +
            "'Transaction')";
    }
    //cr498 end
%>

	<div class="pageMainNoSidebar">	
		<div class="pageContent">
		
			<div class="secondaryNav">	
			  	<div class="secondaryNavTitle">
			   	 	<%=resMgr.getText("NewInstrumentsMenu.ExistingDirectDebitSearch.DirectDebitSearch", TradePortalConstants.TEXT_BUNDLE)%>
			  	</div>
			
			  	<a href="<%=formMgr.getLinkAsUrl("goToTradePortalHome", response)%>" class="title-right" name="CloseThisPageButton">
					<img src="/portal/themes/default/images/cancel_icon_default.png" alt="Close Page"/>
				</a>
			
			  	<div style="clear:both;"></div>
			</div>
			
			<form id="ExistingDirectDebitsForm" name="ExistingDirectDebitsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
  				<div class="formContentNoSidebar">
  					<jsp:include page="/common/ErrorSection.jsp" />
  					
	  				<input type=hidden value="" name=buttonName>

					<%  
						//cr498 add reauthentication hidden fields
					    if ( matchRequireAuth||approveDiscountRequireAuth||
					         closeInvoiceRequireAuth||financeInvoiceRequireAuth||disputeInvoiceRequireAuth || ddiRequireAuth) {
					%> 

				      <input type=hidden name="reCertification" value="<%=TradePortalConstants.INDICATOR_NO%>">
				      <input type=hidden name="reCertOK">
				      <input type=hidden name="logonResponse">
				      <input type=hidden name="logonCertificate">
				
					<%
					    }
					%>
							
							
							<%-- ************** Data retrieval page setup begins here ****************  --%>
							
							<%
							   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", 
							                                                                   TradePortalConstants.TEXT_BUNDLE);
							   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", 
							                                                                   TradePortalConstants.TEXT_BUNDLE);
							   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  
							                                                                   TradePortalConstants.TEXT_BUNDLE);
							   String			 instrumentId 		   = "";
							   String			 refNum			       = "";
							   String			 amountFrom     	   = "";
							   String			 amountTo              = "";
							   String 			 currency              = "";
							   String 			 dayFrom 			   = "";
							   String 			 monthFrom 			   = "";
							   String 			 yearFrom 			   = "";
							   String 			 dayTo 				   = "";
							   String 			 monthTo 			   = "";
							   String 			 yearTo 			   = "";
							   String 			 searchListViewName    = "DirectDebitTransactionHistoryListView.xml";
							   String			 searchType		       = null;
							   String 			 creditAcct		       = null;
							   Vector            instrumentTypes       = null;
							   String 			 link 			       = null;
							   String 			 linkText 		       = "";   
							   StringBuffer 	 newSearchCriteria     = new StringBuffer();                                                          
							
							   DocumentHandler   hierarchyDoc                 = null;
							   StringBuffer      dropdownOptions              = new StringBuffer();
							   int               totalOrganizations           = 0;
							   StringBuffer      extraTags                    = new StringBuffer();
							            
							   String            userDefaultWipView           = null; 
							   String            selectedWorkflow             = null;
							   String            selectedStatus               = "";
							   String            selectedOrg                  = null;  
							   String            linkParms                    = "";   
							   DataGridFactory dgFac = new DataGridFactory(resMgr, formMgr, response);
							   String gridHtml = "", gridLayout = "";
							   userDefaultWipView = userSession.getDefaultWipView();
							   loginLocale = userSession.getUserLocale();
							   
							   // Construct the Corporate Organization
							   CorporateOrganizationWebBean corpOrg  = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
							   corpOrg.getById(userOrgOid);
							   
							   // Store the user's oid and security rights in a secure hashtable for the form
							   secureParms.put("instrumentType",TradePortalConstants.DIRECT_DEBIT_INSTRUCTION);
							   secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
							   
							   //VShah/IAZ IR-SLUK012866351 02/02/10 Begin 
							   //secureParms.put("userOid",userSession.getUserOid());
							   //secureParms.put("securityRights",userSecurityRights); 
							   secureParms.put("UserOid",userSession.getUserOid());
							   secureParms.put("SecurityRights",userSecurityRights); 
							   //VShah/IAZ IR-SLUK012866351 02/02/10 End
							   secureParms.put("clientBankOid",userSession.getClientBankOid()); 
							   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
							
							   
							   // This is the query used for populating the workflow drop down list; it retrieves
							   // all active child organizations that belong to the user's current org.
							   StringBuffer sqlQuery = new StringBuffer();
							   sqlQuery.append("select organization_oid, name");
							   sqlQuery.append(" from corporate_org");
							   sqlQuery.append(" where activation_status = '");
							   sqlQuery.append(TradePortalConstants.ACTIVE);
							   sqlQuery.append("' start with organization_oid = ");
							   sqlQuery.append(userOrgOid);
							   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
							   sqlQuery.append(" order by ");
							   sqlQuery.append(resMgr.localizeOrderBy("name"));
							
							   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false);
								  
							   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
							   totalOrganizations = orgListDoc.size();
							   
							   // Now perform data setup for whichever is the current tab.  Each block of
							   // code sets tab specific parameters (for data retrieval or dropdowns) as
							   // well as setting common things like the appropriate help link and formName
							
							
							       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
							       
							       // Determine the organization to select in the dropdown
							       selectedOrg = request.getParameter("historyOrg");
							       if (selectedOrg == null)
							       {
							       	  selectedOrg = (String) session.getAttribute("historyOrg");
							       	  if (selectedOrg == null)
							          {
							            selectedOrg = EncryptDecrypt.encryptAlphaNumericString(userOrgOid);
							          }
							       }
							
							   	  session.setAttribute("historyOrg", selectedOrg);
								  selectedOrg = EncryptDecrypt.decryptAlphaNumericString(selectedOrg);
							       
							   	  String newSearch = request.getParameter("NewSearch");
								  String newDropdownSearch = request.getParameter("NewDropdownSearch");
							      Debug.debug("New Search is " + newSearch);
							      Debug.debug("New Dropdown Search is " + newDropdownSearch);
							   	  searchType   = request.getParameter("SearchType");
							   		
							   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
							      	{
							         	session.removeAttribute(searchListViewName);
							         	// Default search type for instrument status on the transactions history page is ACTIVE.
							         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
							     	}
							   		
							   		if (searchType == null) 
								    {
							   			searchType = ListViewHandler.getSearchTypeFromState(searchListViewName, session);
							        	if (searchType == null)
							        	{
							          		  searchType = TradePortalConstants.SIMPLE;
							        	}
							   			 if ((newDropdownSearch != null) && (newDropdownSearch.equals(TradePortalConstants.INDICATOR_YES)))
								        {
							             	  session.removeAttribute(searchListViewName);
							         	}
							      	}	
							      	  else
							     	{
							         	// this is probably a new search type clicked from the link, so remove
							         	// the old search type
							         	session.removeAttribute(searchListViewName);
							        }
							        
							         if (searchType.equals(TradePortalConstants.ADVANCED))
							      	{
							         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
							         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
							         	linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);
							
							         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
							      	}
							      	else
							      	{
							        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
							         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
							         	linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);
							
							         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
							     	 }
							
								 // Determine the organizations to select for listview
							     DocumentHandler orgDoc = null;
							        	 
							
								         for (int i = 0; i < totalOrganizations; i++)
								         {
								    	    orgDoc = (DocumentHandler) orgListDoc.get(i);
							        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
							            	if (i < (totalOrganizations - 1) )
							            		{
								             		 orgList.append(",");
							    	       		}
							        	 }
								 
								 
								 if (selectedOrg.equals(ALL_ORGANIZATIONS))
							     	 {
							        	 // Build a comma separated list of the orgs
									     dynamicWhereClause.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
							      	 }
							      	 else
							     	 {
							        	 dynamicWhereClause.append(" and i.a_corp_org_oid = "+  selectedOrg);
							      	 }
							     	 
							     	 // Based on the statusType dropdown, build the where clause to include one or more
							     	 // instrument statuses.
							     	 Vector statuses = new Vector();
							     	 int numStatuses = 0;
							
							      	selectedStatus = request.getParameter("instrStatusType");
							        if (selectedStatus == null) 
							     	 {
							       		  selectedStatus = (String) session.getAttribute("instrStatusType");
							     	 }
							
							      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
							      	{
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
							      	}
							
							      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
							      	{
							       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
							         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
							      	}
							
							      	else // default is ALL (actually not all since DELeted instruments
							           // never show up (handled by the SQL in the listview XML)
							      	{
							        	 selectedStatus = TradePortalConstants.STATUS_ALL;
							      	}
							
							     	session.setAttribute("instrStatusType", selectedStatus);
							     	
							     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
							     	{
							        	 dynamicWhereClause.append(" and i.instrument_status in (");
								         numStatuses = statuses.size();
							    	     for (int i=0; i<numStatuses; i++)
							    	     {
							        	     dynamicWhereClause.append("'");
							            	 dynamicWhereClause.append( (String) statuses.elementAt(i) );
							             	 dynamicWhereClause.append("'");
								             if (i < numStatuses - 1)
								             {
							               		dynamicWhereClause.append(", ");
							             	 }
							         	 }
							         	 dynamicWhereClause.append(") ");
							      	}
							%>

<%-- <%@ include file="/receivables/fragments/DirectDebit-TranSearch-SearchParms.frag" %> --%>

<%-- JavaScript for Instrument History link to enable form submission by enter.
           event.which works for NetScape and event.keyCode works for IE  --%>
<script LANGUAGE="JavaScript" type="text/javascript">
      <!--
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
  
      //-->
      </script>

<%      	
     
     
   Debug.debug("form " + formName);
%>


<%@ include file="/transactions/fragments/ExistingDirectDebitSearch.frag"%>


<%= formMgr.getFormInstanceAsInputField("ExistingDirectDebitsForm", secureParms) %>

  				</div><!-- end of formContentNoSidebar div -->
			</form>
		</div><!-- end of pageContent div -->
	</div><!-- end of pageMainNoSidebar div -->

	<jsp:include page="/common/Footer.jsp">
	   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
	
	<%@ include file="/transactions/fragments/ExistingDirectDebitSearchFooter.frag"%>
	
	</body>
</html>

<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
