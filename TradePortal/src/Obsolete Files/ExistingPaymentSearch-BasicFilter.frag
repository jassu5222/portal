<%--
*******************************************************************************
                    Cash Management Transaction Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">

	<div id="basicPaymentFilter" style="padding-top: 5px;"> 
	 	<div class="inline">
			<%=widgetFactory.createLabel("","CashMgmtTransactionSearch.TransactionID",false, false, false, "inline")%>
			<%=widgetFactory.createTextField("InstrumentId","", instrumentId, "16", false, false, false, "onClick='selectRadio();' width=11", "", "inline")%>
			
			<%=widgetFactory.createLabel("","CashMgmtTransactionSearch.PrimaryRefNum",false, false, false, "inline")%>
			<%=widgetFactory.createTextField("RefNum","","", "30", false, false, false, "onClick='selectRadio();' width=11", "", "inline")%>

	        <% 
	        	options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );
	        %>
	        <%=widgetFactory.createLabel("","CashMgmtTransactionSearch.TransactionType",false, false, false, "inline")%>
	    	<%=widgetFactory.createSelectField("InstrumentType",""," ", options, false,false,false,"onChange='filterPayments(\"Basic\");'","","inline")%>  
	    </div>
	    	
    	<div class="title-right inline">
	    	<button data-dojo-type="dijit.form.Button" type="button">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	         		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  filterPayments("Basic");return false;
	      		</script>
	        </button><br>
			<a href="<%=linkHref%>" ><%=linkName%></a>
		</div>
		<div style="clear: both;"></div>
	</div>   
      