<!DOCTYPE HTML>
<%--
*******************************************************************************
  Existing Payments Search Page

  Description:  
     This page is used as the payments search grid page.
   
*******************************************************************************
--%>

<%--
 *	   Dev Owner: Sandeep
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
   
	StringBuffer      onLoad                = new StringBuffer();
   Hashtable         secureParms           = new Hashtable();
   String            helpSensitiveLink     = null;
   String            current2ndNav      = null;
   String            formName              = null;

   String            userOrgOid            = null;
   String            userOid               = null;
   String            userSecurityRights    = null;
   String            userSecurityType      = null;

   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //set the return actions
   session.setAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_CLOSE_ACTION,   "goToPaymentTransactionsHome");
   // Chandrakanth IR PBUI123160678 02/01/2009 Begin
   session.setAttribute(TradePortalConstants.INSTRUMENT_CLOSE_ACTION,           "goToPaymentTransactionsHome");
   // Chandrakanth IR PBUI123160678 02/01/2009 End.
   //
   // Retrieve the user's security rights, security type, organization oid, and user oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   
   //CR-586 Vshah [BEGIN]
   UserWebBean thisUser = (UserWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.UserWebBean", "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();
   
   String confInd = "";
   if (userSession.getSavedUserSession() == null)
       confInd = thisUser.getAttribute("confidential_indicator");
   else
   {
       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
           confInd = TradePortalConstants.INDICATOR_YES;
       else
           confInd = thisUser.getAttribute("subsid_confidential_indicator");  
   }
   if (InstrumentServices.isBlank(confInd))
      confInd = TradePortalConstants.INDICATOR_NO;
   //CR-586 Vshah [END]

 
   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   
   
   // Now perform data setup for whichever is the current secondary navigation.  Each block of
   // code sets page specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current page as its context
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   //if((perfStat != null) && (perfStat.isLinkAction()))
           //perfStat.setContext();

   // ***********************************************************************
   // Data setup for the Cash Mgmt. Pending Transactions page
   // ***********************************************************************
   
   	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/payment_trans_history.htm",  resMgr, userSession);
   	Debug.debug("*** form " + formName);
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad.toString()%>" />
</jsp:include>

	<div class="pageMainNoSidebar">	
		<div class="pageContent">
			
			<div class="secondaryNav">	
			  	<div class="secondaryNavTitle">
			   	 	<%=resMgr.getText("NewInstrumentsMenu.ExistingPaymentSearch.PaymentSearch", TradePortalConstants.TEXT_BUNDLE)%>
			  	</div>
			
			  	<a href="<%=formMgr.getLinkAsUrl("goToTradePortalHome", response)%>" class="title-right" name="CloseThisPageButton">
					<img src="/portal/themes/default/images/cancel_icon_default.png" alt="Close Page"/>
				</a>
			
			  	<div style="clear:both;"></div>
			</div>
			
			<form id="ExistingPaymentsForm" name="ExistingPaymentsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
				<div class="formContentNoSidebar">
					<jsp:include page="/common/ErrorSection.jsp" />
					
					<input type=hidden value="" name=buttonName>

					<%
					  // Based on the current selected, display the appropriate HTML 
					
					//ctq - todo - this should be in the navigation to the page, not here!!!!  
					//   if (TradePortalConstants.NAV__PENDING_TRANSACTIONS.equals(current2ndNav) ||
					//         	   TradePortalConstants.NAV__FUTURE_TRANSACTIONS.equals(current2ndNav) ||
					//		   TradePortalConstants.NAV__AUTHORIZED_TRANSACTIONS.equals(current2ndNav) ||
					//		   TradePortalConstants.NAV__INQUIRIES.equals(current2ndNav)) {
					%>
					
					<%-- ************** Data retrieval page setup begins here ****************  --%>
					
					<%
					   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", 
					                                                                   TradePortalConstants.TEXT_BUNDLE);
					   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", 
					                                                                   TradePortalConstants.TEXT_BUNDLE);
					   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  
					                                                                   TradePortalConstants.TEXT_BUNDLE);
					   String			 instrumentId 		   = "";
					   String			 refNum			   = "";
					   String			 amountFrom     	   = "";
					   String			 amountTo                  = "";
					   String 			 currency                  = "";
					   String			 searchCondition 	   = TradePortalConstants.SEARCH_ALL_INSTRUMENTS; 
					   String 			 searchListViewName        = "CashMgmtTransactionHistoryListView.xml";
					   String			 searchType		   = null;
					   String 			 loginLocale 		   = userSession.getUserLocale();
					   String 			 instrumentType		   = null;
					   Vector            		 instrumentTypes           = null;
					   String 			 options                   = "";
					   String 			 otherParty                = "";
					   String 			 link 			   = null;
					   String 			 linkText 		   = null;   
					   StringBuffer 		 newSearchCriteria         = new StringBuffer();                                                          
					   //Vshah - 12/08/2008 - Cash Management History Page - End
					
					   DocumentHandler   hierarchyDoc                 = null;
					   StringBuffer      dynamicWhereClause           = new StringBuffer();
					   StringBuffer      dropdownOptions              = new StringBuffer();
					   int               totalOrganizations           = 0;
					   StringBuffer      extraTags                    = new StringBuffer();
					   StringBuffer      newLink                      = new StringBuffer();
					   StringBuffer      newUploadLink                = new StringBuffer();
					         
					   String            userDefaultWipView           = null; 
					   String            selectedWorkflow             = null;
					   String            selectedStatus               = "";
					   
					   String linkParms   = null;   
					   StringBuffer dynamicWhere = dynamicWhereClause;
					   String            selectedOrg                  = null;
					   String loginRights = userSecurityRights; // PPRAKASH IR NGUJ013070808 
					
					   userDefaultWipView           = userSession.getDefaultWipView();
					   
					   StringBuffer sqlQuery = new StringBuffer();
					   sqlQuery.append("select organization_oid, name");
					   sqlQuery.append(" from corporate_org");
					   sqlQuery.append(" where activation_status = '");
					   sqlQuery.append(TradePortalConstants.ACTIVE);
					   sqlQuery.append("' start with organization_oid = ");
					   sqlQuery.append(userOrgOid);
					   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
					   sqlQuery.append(" order by ");
					   sqlQuery.append(resMgr.localizeOrderBy("name"));
					
					   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false);
						  
					   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
					   totalOrganizations = orgListDoc.size();
					   
					   // Now perform data setup for whichever is the current secondary navigation.  Each block of
					   // code sets specific parameters (for data retrieval or dropdowns) as
					   // well as setting common things like the appropriate help link and formName
					
					
					   // Set the performance statistic object to have the current secondary navigation as its context
					
					      
					       formName     = "ExistingPaymentsForm";
					       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
					       
					       
					       // Determine the organization to select in the dropdown
					       selectedOrg = request.getParameter("historyOrg");
					       if (selectedOrg == null)
					       {
					       	  selectedOrg = (String) session.getAttribute("historyOrg");
					       	  if (selectedOrg == null)
					          {
					            selectedOrg = EncryptDecrypt.encryptAlphaNumericString(userOrgOid);
					            
					          }
					        }
					
					      	session.setAttribute("historyOrg", selectedOrg);
						selectedOrg = EncryptDecrypt.decryptAlphaNumericString(selectedOrg);
					       
					   	String newSearch = request.getParameter("NewSearch");
						String newDropdownSearch = request.getParameter("NewDropdownSearch");
					    	Debug.debug("New Search is " + newSearch);
					      	Debug.debug("New Dropdown Search is " + newDropdownSearch);
					   	searchType   = request.getParameter("SearchType");
					   		
					   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
					      	{
					         	session.removeAttribute(searchListViewName);
					
					         	// Default search type for instrument status on the transactions history page is ACTIVE.
					         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
					     	}
					   		
					   		if (searchType == null) 
						    {
					   			searchType = ListViewHandler.getSearchTypeFromState(searchListViewName, session);
					        	if (searchType == null)
					        	{
					          		  searchType = TradePortalConstants.SIMPLE;
					        	}
					   			
					   			 if ((newDropdownSearch != null) && (newDropdownSearch.equals(TradePortalConstants.INDICATOR_YES)))
						        {
					             	  session.removeAttribute(searchListViewName);
					         	}
					      	}	
					      	  else
					     	{
					         	// this is probably a new search type clicked from the link, so remove
					         	// the old search type
					         	session.removeAttribute(searchListViewName);
					        }
					     
					         	//helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
					         	//helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
					     	
						 // Determine the organizations to select for listview
					     	 if (selectedOrg.equals(ALL_ORGANIZATIONS))
					     	 {
					        	 // Build a comma separated list of the orgs
							 DocumentHandler orgDoc = null;
					        	 StringBuffer orgList = new StringBuffer();
					
						         for (int i = 0; i < totalOrganizations; i++)
						         {
						    	        orgDoc = (DocumentHandler) orgListDoc.get(i);
					        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
					            		if (i < (totalOrganizations - 1) )
					            		{
						             		 orgList.append(", ");
					    	       		}
					        	 }
					
					         	 dynamicWhere.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
					      	 }
					      	 else
					     	 {
					        	 dynamicWhere.append(" and i.a_corp_org_oid = "+  selectedOrg);
					      	 }
					     	 
					     	 // Based on the statusType dropdown, build the where clause to include one or more
					     	 // instrument statuses.
					     	 Vector statuses = new Vector();
					     	 int numStatuses = 0;
					
					      	selectedStatus = request.getParameter("instrStatusType");
					
					     	 if (selectedStatus == null) 
					     	 {
					       		  selectedStatus = (String) session.getAttribute("instrStatusType");
					     	 }
					
					      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
					      	{
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
					      	}
					
					      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
					      	{
					       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
					         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
					      	}
					
					      	else // default is ALL (actually not all since DELeted instruments
					           // never show up (handled by the SQL in the listview XML)
					      	{
					        	 selectedStatus = TradePortalConstants.STATUS_ALL;
					      	}
					
					     	session.setAttribute("instrStatusType", selectedStatus);
					     	
					     	
					     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
					     	{
					        	 dynamicWhere.append(" and i.instrument_status in (");
					
						         numStatuses = statuses.size();
					
					    	     for (int i=0; i<numStatuses; i++)
					    	     {
					        	     dynamicWhere.append("'");
					            	 dynamicWhere.append( (String) statuses.elementAt(i) );
					             	 dynamicWhere.append("'");
					
						             if (i < numStatuses - 1)
						             {
					               		dynamicWhere.append(", ");
					             	 }
					         	 }
					
					         	 dynamicWhere.append(") ");
					      	}
					      	
					      	//CR-586 Vshah [BEGIN]---
					      	//For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No), 
						//on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
						if (TradePortalConstants.INDICATOR_NO.equals(confInd))
						{
						    //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
							dynamicWhereClause.append(" and ( i.confidential_indicator = 'N' OR ");
							dynamicWhereClause.append(" i.confidential_indicator is null) ");
						}
					  	//CR-586 Vshah [END]
					%>
		
					<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-TransType.frag" %>
					<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-SearchParms.frag" %>
		
					<%-- JavaScript for Instrument History to enable form submission by enter.
			           event.which works for NetScape and event.keyCode works for IE  --%>
			      <script LANGUAGE="JavaScript">
			      <!--
			         function enterSubmit(event, myform) {
			            if (event && event.which == 13) {
			               setButtonPressed('null', 0);
			               myform.submit();
			            }
			            else if (event && event.keyCode == 13) {
			               setButtonPressed('null', 0);
			               myform.submit();
			            }
			            else {
			               return true;
			            }
			         }
			  
			      //-->
			      </script>

				<%      	
				    
				   Debug.debug("form " + formName);
				   
				   //Vshah - 12/08/2008 - Cash Management History Page - End   
				%>
				
				<%-- ********************* HTML for page begins here *********************  --%>
				

	
				  <%
				   CorporateOrganizationWebBean corpOrg  = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
				   corpOrg.getById(userOrgOid);
				  %>
				
				
				    <%@ include file="/transactions/fragments/ExistingPaymentSearch.frag" %>

					<%
						  secureParms.put("login_oid", userSession.getUserOid());
						  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
						  secureParms.put("login_rights", loginRights);

					    //secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
					    secureParms.put("userOid",userSession.getUserOid());
					    secureParms.put("securityRights",userSession.getSecurityRights()); 
					    secureParms.put("clientBankOid",userSession.getClientBankOid()); 
					    secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
					      
					    //VS RVU062382912 Adding New parameter value for transactions
					      if (userSession.getSavedUserSession() == null) {
					        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES); 
					      } 
					%>
					
					<%//= formMgr.getFormInstanceAsInputField("ExistingPaymentsForm", secureParms) %>
  				</div><!-- end of formContentNoSidebar div -->
			</form>
		</div><!-- end of pageContent div -->
	</div><!-- end of pageMainNoSidebar div -->

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<%	String gridLayout = dgFactory.createGridLayout("ExistingPaymentSearchDataGrid"); %>

<script type="text/javascript">
//require("dojo/query", function(query){
//	dojo.query("#Organization").on('change'){filterPayments();}
//});

	
	var gridLayout = <%= gridLayout %>,//ALL_ORGANIZATIONS
		initSearchParms = "userOrgOid=<%=userOrgOid%>&confInd=<%=confInd%>";
	
	createDataGrid("existingPaymentSearchDataGridId", "ExistingPaymentSearchDataView",gridLayout, initSearchParms);

	console.log("initSearchParms="+initSearchParms);
	
  	function filterPayments() {filterPayments(null)}
  	
  	function filterPayments(buttonId) {
  		console.log("Inside filterPayments()");
  		
	    require(["dojo/dom"],
	      function(dom){
	    	var organization = dijit.byId("Organization").attr('value'),
	        	status = dijit.byId("instrStatusType").attr('value');
	        
	        var searchParms="userOrgOid="+organization;
		       	searchParms+="&confInd=<%=confInd%>";
		        searchParms+="&status="+status;
	        
	        if(buttonId=="Advanced"){
	        	var instrumentType = dijit.byId("InstrumentType").attr('value'),
		        	currency = dijit.byId("Currency").attr('value'),
		        	amountFrom = dijit.byId("AmountFrom").value,
		        	amountTo = dijit.byId("AmountTo").value,
		        	otherParty = dijit.byId("OtherParty").value;
		        
		        if(amountFrom.toString()=="NaN")amountFrom = "";
		        if(amountTo.toString()=="NaN")amountTo = "";
		        
		        searchParms+="&instrumentType="+instrumentType;
				searchParms+="&currency="+currency;
				searchParms+="&amountFrom="+amountFrom;
				searchParms+="&amountTo="+amountTo;
				searchParms+="&otherParty="+otherParty;
				searchParms+="&searchType=Advanced";
	        }else if(buttonId=="Basic"){
	        	var instrumentType = dijit.byId("InstrumentType").attr('value'),
	        		instrumentId = dijit.byId("InstrumentId").value,
	 	        	refNum = dijit.byId("RefNum").value;
	 	        
	 	        searchParms+="&instrumentType="+instrumentType;
	 	        searchParms+="&instrumentId="+instrumentId;
				searchParms+="&refNum="+refNum;
				searchParms+="&searchType=Basic";
	        }
		        searchParms+="&encrypted=true";
		        searchParms+="&loginLocale=<%=loginLocale%>";
		        searchParms+="&resourceLocale=<%=resMgr.getResourceLocale()%>";
		        searchParms+="&fromDocCache=<%=formMgr.getFromDocCache()%>";

	        console.log("searchParms="+searchParms);

			searchDataGrid("existingPaymentSearchDataGridId", "ExistingPaymentSearchDataView", searchParms);
			
			console.log("Query fired");	        
		});
	}
  	
  	function selectRadio(){
  		console.log("inside selectRadio()");
  		
  	}
 
  	var formName = 'NewPaymentForm',
		buttonName = '<%=TradePortalConstants.BUTTON_SELECT%>',
		rowKeys = "",
		instType = "";
	
  	function selectPayment(bankBranchArray, newInstrumentType){
  		
		<%--get array of rowkeys from the grid--%>
		rowKeys = getSelectedGridRowKeys("existingPaymentSearchDataGridId");
		instType= newInstrumentType;
		
		if(rowKeys == ""){
			alert("Please pick a record and click 'Select'");
			return;
		}
		
  		require(["t360/dialog"], function(dialog ) {
  	      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
  	                  'bankBranchSelectorDialog.jsp',
  	                  null, null, //parameters
  	                  'select', this.createNewPaymentBankBranchSelected);
  	    });
  	}
  	
  	function createNewPaymentBankBranchSelected(bankBranchOid) {
  		console.log("inside createNewPaymentBankBranchSelected() - bankBranchOid: "+bankBranchOid);
  		
  		if ( document.getElementsByName('NewPaymentForm').length > 0 ) {
  	        var theForm = document.getElementsByName('NewPaymentForm')[0];
  	        theForm.mode.value='CREATE_NEW_INSTRUMENT';
  	        theForm.copyType.value='Blank';
  	      	theForm.copyInstrumentOid.value=rowKeys;
  	      	theForm.bankBranch.value=bankBranchOid;
          	
  	        if ( instType ) {
  	        	console.log("newInstrumentType: "+instType);
  	        
  	          	theForm.instrumentType.value=instType;
  	        } else {
  	          	alert('Problem in createNewInstrument: instrumentType not defined');
  	        }
  	    	
  	      	console.log("theForm.mode.value: "+theForm.mode.value);
  	      	console.log("theForm.copyType.value: "+theForm.copyType.value);
  	    	console.log("theForm.copyInstrumentOid.value: "+theForm.copyInstrumentOid.value);
  	  		console.log("theForm.bankBranch.value: "+theForm.bankBranch.value);
  	  		console.log("theForm.instrumentType.value: "+theForm.instrumentType.value);
  	    		  
  	      	theForm.submit();
  	      } else {
  	        	alert('Problem in createNewInstrument: cannot find NewPaymentForm');
  	      }
    }  	
  	
</script>

<div>

  <div id="bankBranchSelectorDialog" ></div>

<%  //include a hidden form for new instrument submission
    // this is used for all of the new instrument types available from the menu
    Hashtable newInstrSecParms = new Hashtable();
    newInstrSecParms.put("UserOid", userSession.getUserOid());
    newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
    newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
    newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
    newInstrSecParms.put("login_oid", userSession.getUserOid());
    newInstrSecParms.put("owner_org_oid", userSession.getOwnerOrgOid());
    newInstrSecParms.put("login_rights", loginRights);
%>
  <form method="post" name="NewPaymentForm" id="NewPaymentForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" value="NEW_INSTRUMENT"/>
    <input type="hidden" name="copyType" value="Blank"/>
    <input type="hidden" name="bankBranch" />
    <input type="hidden" name="instrumentType" />
    <input type="hidden" name="copyInstrumentOid" />
<!--    <input type="hidden" name="TemplateName" value="NEW_INSTRUMENT"/>-->
<!--    <input type="hidden" name="copyType" value="Blank"/>-->
<!--    <input type="hidden" name="PayeeBankCode" />-->
<!--    <input type="hidden" name="instrument_type_code" />-->
<!--    <input type="hidden" name="instrument_oid" />-->
    
    <%= formMgr.getFormInstanceAsInputField("NewPaymentForm",newInstrSecParms) %>
  </form>
</div>
</body>
</html>

<%   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   // Store the current secondary navigation in the session.
   session.setAttribute("paymentTransactions2ndNav", current2ndNav);
%>

