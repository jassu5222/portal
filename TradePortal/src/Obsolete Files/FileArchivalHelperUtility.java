package com.ams.tradeportal.devtool;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author adarsha.ks
 *
 */
public class FileArchivalHelperUtility {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out
					.println("Usage: FileArchivalHelperUtility <PropertyFile>");
			return;
		}
		XMLUtils xmlUtils = new XMLUtils(args[0]);

		FileArchivalHelperUtility utility = new FileArchivalHelperUtility();
		Connection conn = utility.getConnection(xmlUtils);
		utility.archiveUnprocessedFiles(conn, xmlUtils);
		utility.closeConnection(conn);
	}

	public Connection getConnection(XMLUtils xmlUtils) {
		Connection conn = null;

		String dbHost = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/dbHost");
		String dbPort = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/dbPort");
		String dbInstance = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/dbInstance");
		String dbUserName = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/dbUserName");
		String dbPassword = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/dbPassword");
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			String url = "jdbc:oracle:thin:@" + dbHost + ":" + dbPort + ":"
					+ dbInstance;
			conn = DriverManager.getConnection(url, dbUserName, dbPassword);

		} catch (ClassNotFoundException ex) {
			System.err.println(ex.getMessage());
		} catch (IllegalAccessException ex) {
			System.err.println(ex.getMessage());
		} catch (InstantiationException ex) {
			System.err.println(ex.getMessage());
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return conn;
	}

	/**
	 *
	 * @param conn
	 */
	public void closeConnection(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	/**
	 *
	 * @param conn
	 * @param xmlUtils
	 */
	private void archiveUnprocessedFiles(Connection conn, XMLUtils xmlUtils) {
		String query = "SELECT PAYMENT_FILE_NAME FROM PAYMENT_FILE_UPLOAD WHERE PAYMENT_FILE_NAME=?";
		String inputFolder = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/inputFolder");
		String archivalFolderPath = xmlUtils
				.getValueAsStringForXmlPath("/FileArchive/archivedFilesPath");
		File dir = new File(inputFolder);
		if (dir == null) {

			// Either dir does not exist or is not a directory
		} else {
			File[] children = dir.listFiles();
			for (int i = 0; i < children.length; i++) {
				// Get filename of file or directory
				if (children[i].isFile()) {
					try {
						PreparedStatement st = conn.prepareStatement(query);
						st.setObject(1, children[i].getName());
						ResultSet rs = st.executeQuery();
						if (!rs.next()) {
							// String fileName = rs.getString("PAYMENT_FILE_NAME");
							copyFile(children[i], archivalFolderPath + "/"
									+ children[i].getName());

						}
					} catch (SQLException ex) {
						System.err.println(ex.getMessage());
					}
				}
			}
		}

	}

	/**
	 *
	 * @param srFile
	 * @param dtFilePath
	 */
	private void copyFile(File srFile, String dtFilePath) {
		try {
			// File f1 = new File(srFile);
			File dtFile = new File(dtFilePath);
			InputStream in = new FileInputStream(srFile);

			// For Overwrite the file.
			OutputStream out = new FileOutputStream(dtFile);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			srFile.delete();
			System.out.println("File copied.");
		} catch (FileNotFoundException ex) {
			System.out
					.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 * @author adarsha.ks
	 *
	 */
	static class XMLUtils {
		String sFilePath = null;

		public XMLUtils(String sFilePath) {
			this.sFilePath = sFilePath;
		}

		/**
		 * creates a document for xml
		 *
		 * @param xml
		 *
		 * @return Document
		 */
		public Document createDocumentForXml(String xml) {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();

			Document doc = null;

			try {
				DocumentBuilder parser = factory.newDocumentBuilder();

				StringBuffer stringBuffer = new StringBuffer(xml);
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
						stringBuffer.toString().getBytes("UTF-8"));

				doc = parser.parse(byteArrayInputStream);
			} catch (ParserConfigurationException e) {
				System.err.println(e.getMessage());
			} catch (SAXException e) {
				System.err.println(e.getMessage());
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

			return doc;
		}

		/**
		 * Get the List of nodes having xpath sXmlPath from xml doc
		 *
		 * @param sXmlPath
		 * @param doc
		 *
		 * @return List
		 */
		public List getValueForXmlPath(String sXmlPath, Document doc) {
			List valueList = new ArrayList();
			XPathFactory xpF = XPathFactory.newInstance();
			XPath xpath = xpF.newXPath();
			XPathExpression compiled = null;
			NodeList nodeList = null;

			try {
				compiled = xpath.compile(sXmlPath);
				nodeList = (NodeList) compiled.evaluate(doc
						.getDocumentElement(), XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				valueList.add(node.getTextContent());
			}

			return valueList;
		}

		/**
		 * Get the List of nodes having xpath sXmlPath from file
		 *
		 * @param sXmlPath
		 *
		 * @return
		 */
		public List getValueForXmlPath(String sXmlPath) {
			Document doc = readXMLFile();

			return getValueForXmlPath(sXmlPath, doc);
		}

		public String getValueAsStringForXmlPath(String sXmlPath) {
			List lstOfValues = getValueForXmlPath(sXmlPath);

			if ((lstOfValues != null) && (lstOfValues.size() != 0)) {
				return (String) lstOfValues.get(0);
			}

			return null;
		}

		/**
		 * Read an XML file and parse it into a DOM Document.
		 *
		 * @return Document Document object representing the XML
		 */
		public Document readXMLFile() {
			InputStream istr = null;
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder parser = null;

			try {
				parser = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}

			try {
				istr = new FileInputStream(sFilePath);
			} catch (Exception x) {
				x.printStackTrace();
			}

			try {
				InputSource isrc = new InputSource(istr);
				isrc.setSystemId(sFilePath);

				Document d = parser.parse(isrc);

				return d;
			} catch (Exception x3) {
				x3.printStackTrace();
			}

			return null;
		}
	}

}
