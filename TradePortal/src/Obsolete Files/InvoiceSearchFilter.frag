<%--
*******************************************************************************
                  Instrument Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  IMPORTANT NOTE: This page may currently come from the following:
  
  Create Transaction Step 1 Page: for an existing instrument or copying an
  			instrument
  Export LC Issue Transfer: Search Instrument button
  MessageHome: Search Instrument
  
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.
  
  
  
  <%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
  <input type="hidden" name="NewSearch" value="Y">



<%
  
  String financeInstrument = null;
  String dateType = null;
  String amountType = null;
  String partyFilterText = null;
  String fromDate = null;
  String toDate = null;
  
  invoiceID = request.getParameter("InvoiceID");
    
  amountType = request.getParameter("AmountType");
  amountFrom = request.getParameter("AmountFrom");
  amountTo = request.getParameter("AmountTo");
  
  partyFilterText = request.getParameter("PartyFilterText");
  
  financeInstrument = request.getParameter("FinanceInstrument");
  
  poNumber = request.getParameter("PONumber");
  currency = request.getParameter("Currency");
  
  dateType = request.getParameter("DateType");
  
  dayFrom   = request.getParameter("DayFrom");
  monthFrom = request.getParameter("MonthFrom");
  yearFrom  = request.getParameter("YearFrom");
  
  dayTo   = request.getParameter("DayTo");
  monthTo = request.getParameter("MonthTo");
  yearTo  = request.getParameter("YearTo");
  
  if (invoiceID == null) {
      		invoiceID = "";
  	}
  	
     
%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">
    <tr> 
      <td align="right" nowrap width="100"> 
        <p class="ControlLabel">
          <%=resMgr.getText("InvoiceSearch.InvoiceID", 
                            TradePortalConstants.TEXT_BUNDLE)%>
         
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
     <td nowrap align="left" class="ListText"> 
        <p>
          <%=InputField.createTextField("InvoiceID", invoiceID,
                                      "27", "20", "ListText", false)%>
       </p>
     </td>
     <td width="10" nowrap>&nbsp;</td>
     <td align="right" nowrap width="70"> 
        <p class="ControlLabel">
          <%=resMgr.getText("InvoiceSearch.PONumber", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          
        </p>
      </td>
       <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p>
          <%=InputField.createTextField("PONumber", poNumber,
                                      "27", "20", "ListText", false)%>
       </p>
     </td>
     <td width="10" nowrap>&nbsp;</td>
      <td align="right" nowrap width="80"> 
        <p class="ControlLabel">
          <%=resMgr.getText("InvoiceSearch.BuyerIdentifier", 
                            TradePortalConstants.TEXT_BUNDLE)%>
         
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
       <td  align="left" valign="middle" nowrap width="200">
        <p class="ListText">
          <%=resMgr.getText("PartySearch.ToFilter",TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td  valign="middle" align="left" nowrap> <span class="ListText">
        <%
        /********************************
         * START FILTER INPUT FIELD
         ********************************/
        Debug.debug("Filter Field");
        %>
        <%=resMgr.getText("PartySearch.NameStartsWithExample",TradePortalConstants.TEXT_BUNDLE)%><br>
        </span>
        <%= InputField.createTextField("PartyFilterText", partyFilterText, "30", "30", "ListText", false)%>
        <%
        /********************************
         * END FILTER INPUT FIELD
         ********************************/%>
      </td>
  </tr>
    
  <tr>
        <td align="right" nowrap width="100"> 
        <p class="ControlLabel">
          <%=resMgr.getText("InvoiceSearch.FinanceInstrument", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
       <td nowrap class="ListText"> 
        <p>
          <%=InputField.createTextField("FinanceInstrument", financeInstrument,
                                      "27", "20", "ListText", false)%>
       </p>
     </td>
     <td width="20" nowrap>&nbsp;</td>
      <td align="right" nowrap width="30"> 
        <p class="ControlLabel">
          <%=resMgr.getText("InvoiceSearch.Financed", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        
        </p>
      </td>
       <td nowrap class="ListText" align="left" width="30"> 
        <p align="left">

		<%=InputField.createCheckboxField("Financed", TradePortalConstants.INDICATOR_NO, "", false, "ListText", "", false)%>

                                                                      
       </p>   	
     </td>
    <td width="100%">&nbsp;</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">

   <tr>
       <td align="right" nowrap width="100"> 
        <p class="ControlLabel">
         <br>
          <%=resMgr.getText("InvoiceSearch.AmountType", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td  nowrap class="ListText"> 
        <p>
          <br>
<%
          options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, "", loginLocale);
          out.println(InputField.createSelectField("AmountType", amountType,
                      " ", options, "ListText", false, "onkeypress=\"return enterSubmit(event, this.form)\""));
%>
       </p>

      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td valign="middle" align="right" nowrap width="100"> 
        <p class="ControlLabel">
          <br>
          <%=resMgr.getText("InvoiceSearch.AmountRange",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.From", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("InvoiceSearch", amountFrom,
                                      "27", "20", "ListText", false)%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.To", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("AmountTo", amountTo,
                                      "27", "20", "ListText", false)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.Currency", 
                            TradePortalConstants.TEXT_BUNDLE)%>
         <br>
<%
          options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);
          out.println(InputField.createSelectField("Currency", "",
                      " ", options, "ListText", false, "onkeypress=\"return enterSubmit(event, this.form)\""));
%>
	</p>
      </td>
     <td width="100%">&nbsp;</td>
     <td width="100%">&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">
 
<tr>
       
     

<td align="right" nowrap width="100"> 
        <p class="ControlLabel">
	 <br>
          <%=resMgr.getText("InvoiceSearch.DateType", 
                            TradePortalConstants.TEXT_BUNDLE)%>
         
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
     <td nowrap align="left" class="ListText"> 
        <p>
            <br>
<%
          options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, "", loginLocale);
          out.println(InputField.createSelectField("DateType", dateType,
                      " ", options, "ListText", false, "onkeypress=\"return enterSubmit(event, this.form)\""));
%>
       </p>
     </td>
       <td width="10" nowrap>&nbsp;</td>
       <td nowrap class="ListText">
        <p class="ListText">
         <br>
          <%=resMgr.getText("InvoiceSearch.From", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.Day", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=TPDateTimeUtility.getDayDropDown("DayFrom", dayFrom, "", "onkeypress=\"return enterSubmit(event, this.form)\"")%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
       <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.Month", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
           <%=TPDateTimeUtility.getMonthDropDown("MonthFrom", monthFrom, 
                                                loginLocale, "")%>
        </p>
      </td>
     <td width="10" nowrap>&nbsp;</td>
      <td class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InstSearch.Year", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=TPDateTimeUtility.getYearDropDown("YearFrom", yearFrom, "", -5, "onkeypress=\"return enterSubmit(event, this.form)\"")%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText">
        <p class="ListText">
          <br>
          <%=resMgr.getText("InvoiceSearch.To", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
  
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.Day", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=TPDateTimeUtility.getDayDropDown("DayTo", dayTo, "", "onkeypress=\"return enterSubmit(event, this.form)\"")%>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
       <td nowrap class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InvoiceSearch.Month", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
           <%=TPDateTimeUtility.getMonthDropDown("MonthTo", monthTo, 
                                                loginLocale, "")%>
        </p>
      </td>
     <td width="10" nowrap>&nbsp;</td>
      <td class="ListText"> 
        <p class="ListText">
          <%=resMgr.getText("InstSearch.Year", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=TPDateTimeUtility.getYearDropDown("YearTo", yearTo, "", -5, "onkeypress=\"return enterSubmit(event, this.form)\"")%>
        </p>
      </td>
       <td width="10" nowrap>&nbsp;</td>
	<td align="left" valign="bottom">
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
          <jsp:param name="showButton" value="true" />
          <jsp:param name="name" value="SearchButton" />
          <jsp:param name="image" value='common.SearchGreyImg' />
          <jsp:param name="text" value='common.SearchGreyText' />
	  <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_FILTER%>" />
        </jsp:include>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
   <tr height="7">
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
     </tr>
</table>

  
<%

	if (invoiceID == null) {
      		invoiceID = "";
  	}
  	else {
      		invoiceID = invoiceID.toUpperCase();
  	}
        

	// Search by the invoice ID
  
  	if (!invoiceID.equals("")) {
      		dynamicWhereClause.append(" where upper(invoice_reference_id) like '");
      		dynamicWhereClause.append(invoiceID);
      		dynamicWhereClause.append("%'");
      		dynamicWhereClause.append(" and ");
  	}
  	
  	                 
        System.out.println("DateType1 " + dateType); 
        
        if (dateType == "DDT"){
 		dateType = "invoice_due_date";
 	}
 	else if (dateType == "IDT"){
 		dateType = "invoice_issue_datetime";
 	}
 	else {
 		dateType = "";
 	}  
 	
 	if (!dateType.equals("")){
 	          	

        	if (TPDateTimeUtility.isGoodDate(yearFrom, monthFrom, dayFrom)) {
           		fromDate = TPDateTimeUtility.formatOracleDate(yearFrom,
                                                           monthFrom, dayFrom);
           	if (!fromDate.equals("")) {
              	dynamicWhere.append(" and " + dateType + " >= '");
              	dynamicWhere.append(fromDate);
              	dynamicWhere.append("'");
              	dynamicWhereClause.append(" and ");
           	}
        	} else {
           		/*medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_FROM_DATE); */       	
        	}

        	       
        	if (TPDateTimeUtility.isGoodDate(yearTo, monthTo, dayTo)) {
           		toDate = TPDateTimeUtility.formatOracleDate(yearTo,
                                                        monthTo, dayTo);
           	if (!toDate.equals("")) {
             	dynamicWhere.append(" and " + dateType + " <= '");
             	dynamicWhere.append(toDate);
             	dynamicWhere.append("'");
             	dynamicWhereClause.append(" and ");
           	}
        	} else {
           	/*medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_TO_DATE);*/
          	        	}
	}
	
        currency = request.getParameter("Currency");
        if (currency != null) {
           if (!currency.equals("")) {
              dynamicWhere.append(" and currency_code='" + currency + "'");
              dynamicWhereClause.append(" and ");
           }
        } 
        
        
               
        if(amountType == "INA"){
        	amountType = "invoice_total_amount";
        }
        else if (amountType == "DSA"){
        	amountType = "discount_amount";
        }
        else if (amountType == "CNA"){
        	amountType = "credit_note_amount";
        }
        else if (amountType == "FNA"){
        	amountType = "finance_amount";
        }
        else { 
        	amountType = "";
        }
        
        if(amountType != "") {
                	
        	if (amountFrom != null) {
           	amountFrom = amountFrom.trim();
           	try {
              	String amount =
                 	NumberValidator.getNonInternationalizedValue(amountFrom,
                                                              loginLocale);
              	if (!amount.equals("")) {
                 	dynamicWhere.append(" and " + amountType + " >= ");
                 	dynamicWhere.append(amount);
                 	dynamicWhereClause.append(" and ");
              		}
           	} catch (InvalidAttributeValueException e) {
              		/*medService.getErrorManager().issueError(
                                 TradePortalConstants.ERR_CAT_1,
                                 TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                 amountFrom);   */           		
           		}	
        	} 
        	
        	
        	if (amountTo != null) {
           	amountTo = amountTo.trim();
           	try {
              	String amount =
                 	NumberValidator.getNonInternationalizedValue(amountTo,
                                                              loginLocale);
              	if (!amount.equals("")) {
                 	dynamicWhere.append(" and " + amountType + " <= ");
                 	dynamicWhere.append(amount);
                 	dynamicWhereClause.append(" and ");
             	 }
          	 } catch (InvalidAttributeValueException e) {
              	/*medService.getErrorManager().issueError(
                                   TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                   amountTo);     */    
           	}
        	} 
        }
        
        partyFilterText = request.getParameter("PartyFilterText");
        if ( partyFilterText != null) {          
           partyFilterText = otherParty.trim().toUpperCase();
           if (!partyFilterText.equals("")) {
              dynamicWhere.append(" and upper(buyer_identifier) like '");
              dynamicWhere.append(SQLParamFilter.filter(partyFilterText));
              dynamicWhere.append("%'");
              dynamicWhereClause.append(" and ");
           }
        } 
        
        if (!poNumber.equals("")) {
      		dynamicWhereClause.append(" and invoice.uoid in (select p_invoice_oid from invoice_goods where upper(po_reference_id) like '");
      		dynamicWhereClause.append(poNumber);
      		dynamicWhereClause.append("%'))");
      		dynamicWhereClause.append(" and ");
  	}
  	
  	Debug.debug("*** where0-> " +dynamicWhereClause);
  	
  	
  	
  	String whereClause = null;
  	whereClause = dynamicWhereClause.toString();
  	
  	if (whereClause != null && !whereClause.equals("")){
  	        whereClause = dynamicWhereClause.substring(0, dynamicWhereClause.lastIndexOf("and"));
  		if (whereClause != null && !whereClause.equals("")){
  			dynamicWhereClause = new StringBuffer(whereClause);
  		}
  	}
  	
  	Debug.debug("*** where1-> " +dynamicWhereClause);

%>


