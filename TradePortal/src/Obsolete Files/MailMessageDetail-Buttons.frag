<%    /*****************************************************
      * Action Buttons For Mail Message Detail page 
      *******************************************************/                 
%>

      <td width="100%">&nbsp;</td>

<%-- ------------ --%>
<%-- Route Button --%>
<%-- ------------ --%>
<%--		If the Message Status is NOT sent to bank AND either:			--%>
<%--		it's not a discrepancy notice and we have the right to route a message, --%>
<%--		Otherwise it is a discrepancy and the user has the right to route a discrepancy message. --%>

<% if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) &&
      ((!isDiscrepancy && hasRouteRights ) || (isDiscrepancy && hasRouteDiscrepRights )) ) {
   
	String routeButtonName;

	if(isReadOnly){ 	//Just route to the next page: Route/RouteItems
		routeButtonName = "Route";

	}else{			//Route to: RouteAndSave/RouteItems
		routeButtonName = "RouteAndSave";
	}  
%>
      <td>	 <%-- This lays in the common route button logic --%>
         <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="name"         value="<%=routeButtonName%>" />
            <jsp:param name="image"        value='common.RouteImg' />
            <jsp:param name="text"         value='common.RouteText' />
            <jsp:param name="width"        value="96" />
            <jsp:param name="submitButton" value="<%=routeButtonName%>" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
<% } %>


<%-- ----------- --%>
<%-- Save Button --%>
<%-- ----------- --%>
<%--		If this page is Read Only, then we dont need to display the save as Draft button.	--%>
<%--		We alsways call the save function :'Save as Draft' per webpage definition.		--%>

<% if( !isReadOnly ) {
%>
      <td>
         <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="name"         value="SaveAsDraft" />
            <jsp:param name="image"        value='common.SaveasDraftAndCloseImg' />
            <jsp:param name="text"         value='common.SaveasDraftAndCloseText' />
            <jsp:param name="width"        value="140" />
            <jsp:param name="submitButton" value="SaveAsDraft" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
<% } %>


<%-- ------------------- --%>
<%-- Send to Bank Button --%>
<%-- ------------------- --%>
<%--		If the user has the security right to send a message and we're Not in read only mode	--%>
<%-- 		then the Send to Bank button is displayed.						--%>

<% if( messageCanSendToBank && hasSendRights ) {
%>
      <td>
         <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="name"         value="SendToBank" />
            <jsp:param name="image"        value='common.SendToBankImg' />
            <jsp:param name="text"         value='common.SendToBankText' />
            <jsp:param name="width"        value="96" />
            <jsp:param name="submitButton" value="SendToBank" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
<% } %>     


<%-- -------------------- --%>
<%-- Reply to Bank Button --%>
<%-- -------------------- --%>
<%--		If the message is a Discrepancy Notice then the has to have a security right to create/	--%>
<%--		modify Discrepancy(s).  If this message is a Mail Message than the user needs the right --%>
<%--		to create / edit Mail Messages.  Either way this has to be a read only page to start.	--%>

<%-- 		It should be noted that Replying to Bank is either done by Submission or by a link...   --%>
<%--		If we are dealing with a discrepancy then the button is a submit button, other wise	--%>
<%--		we are dealing with a link that brings the user back to this jsp in a different mode.	--%>


<%    

//Added following if condition not to show ReplyToBank button to the Parent org if the
// child mail message is accessed directly and not through Subsidiary Access
if ((!isUsersOrgOid) && (!userSession.hasSavedUserSession()))
{
	displayDiscrepancyReplyToBankButton = false;
}


	if( isMsgFromBank && isDiscrepancy && hasEditDiscrepRights && displayDiscrepancyReplyToBankButton) {
%>
         <td>
         <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="name"         value="ReplyToBank" />
            <jsp:param name="image"        value='common.ReplytoBankImg' />
            <jsp:param name="text"         value='common.ReplytoBankText' />
            <jsp:param name="width"        value="96" />
            <jsp:param name="submitButton" value="ReplyToBank" />
         </jsp:include>
         </td>
         <td width="15" nowrap>&nbsp;</td>

<%    }else if(isMsgFromBank && !isDiscrepancy && hasEditRights) {

	  urlParm = "&isReply=true" + "&relatedInstID=" + completeInstId + "&subject=" + java.net.URLEncoder.encode(subject) + 
		    "&sequenceNumber=" + sequenceNumber;
%>
         <td>
          <jsp:include page="../common/RolloverButtonLink.jsp">
            <jsp:param name="showButton" value="true" />
            <jsp:param name="name" value="ReplyToBank" />
            <jsp:param name="image" value='common.ReplytoBankImg' />
            <jsp:param name="text" value='common.ReplytoBankText' />
            <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )) %>" />
            <jsp:param name="width" value="96" />
          </jsp:include>
          </td>
	    <td width="15" nowrap>&nbsp;</td>

<%    }  %>

<%-- ------------------- --%>
<%-- Edit Message Button --%>
<%-- ------------------- --%>
<%-- 		This is a simple Link to bring the user back to this jsp in a different mode.		--%>
<%-- 		The user needs to access this same message, except in edit mode - not readOnly mode.	--%>

<% if( isReadOnly && !isCurrentUser && hasEditRights && !isMsgFromBank) {

	  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
	  urlParm = "&isEdit=true" + "&mailMessageOid=" + encryptVal2;
%>
      <td>
          <jsp:include page="../common/RolloverButtonLink.jsp">
            <jsp:param name="showButton" value="true" />
            <jsp:param name="name" value="Edit" />
            <jsp:param name="image" value='common.EditMessageImg' />
            <jsp:param name="text" value='common.EditMessageText' />
            <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )) %>" />
            <jsp:param name="width" value="96" />
          </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
<% } %>


<%-- ------------- --%>
<%-- Delete Button --%>
<%-- ------------- --%>
<%-- 		This is a submit button that will call the delete Mediator functionality.	--%>

<% if( (!isMessageOidNull && hasDeleteRights) ||
       (isDiscrepancy && hasDeleteDiscrepRights)) {
%>
      <td>
         <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="name"         value="Delete" />
            <jsp:param name="image"        value='common.DeleteImg' />
            <jsp:param name="text"         value='common.DeleteText' />
            <jsp:param name="width"        value="96" />
            <jsp:param name="extraTags"    value="return confirmDelete()" />
            <jsp:param name="submitButton" value="Delete" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
<% } %>   

<%-- --------------------- --%>
<%-- Cancel / Close Button --%>
<%-- --------------------- --%>
<%-- 		The buttone text is determined by the cancelFlag variable - otherwise the button always --%>
<%-- 		performs the same action regardless of the text. 					--%>
<%-- 		This is a link that will route the user to how they got to this page in the first place --%>
<%--		by looking at the fromPage variable.							--%>

      <td>
	<%-- This lays in the common close button logic --%>
          <jsp:include page="../common/RolloverButtonLink.jsp">
            <jsp:param name="showButton" value="true" />
            <jsp:param name="name" value="Close" />
            <jsp:param name="image" value="<%=cancelImg%>" />
            <jsp:param name="text" value="<%=cancelFlag%>" />
            <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( fromPage, response )) %>" />
            <jsp:param name="width" value="<%=cancelWidth%>" />
          </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
