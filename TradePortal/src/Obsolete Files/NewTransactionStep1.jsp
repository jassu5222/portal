<%--
*******************************************************************************
                                New Transaction Step 1

  Description: Allows the user to initiate a new transaction.  It can be used
  to create a new transaction or a new instrument (based on existing instrument,
  template, blank form, or export dlc.

  Clicking the Next Step button calls the CreateTransactionMediator to do
  validation before forwarding to the next page (if more data required).

*******************************************************************************
--%>


<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, 
                 com.ams.tradeportal.common.cache.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
    Debug.debug("****START**************NEW TRANSACTION STEP 1*****************START***");
    userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

    /**************************************************************************
     * THIS SCRIPTLET SETS THE GLOBALS FOR THE PAGE AND REFRESHES THE PAGE FROM
     * THE DOC CACHE IF WE'RE RETURNING TO THE PAGE AFTER AN ERROR
     **************************************************************************/

    String options            = null;
    String outerRadioVal      = null;
    String innerRadioVal      = null;
    String selectedInstrType  = null;
    String selectedBankBranch = null;
    String loginLocale        = userSession.getUserLocale();
    String loginRights        = userSession.getSecurityRights();
    String startPage          = request.getParameter("startPage");

    if(startPage != null)
     {
        startPage =  EncryptDecrypt.decryptStringUsingTripleDes(startPage, userSession.getSecretKey());
     }

    String ownerOrg           = userSession.getOwnerOrgOid();

    boolean getDataFromDoc    = false;
    boolean showDefaultMsg    = true;

    int oboCount = 0;

    DocumentHandler queryDoc  = null;
    DocumentHandler queryDoc2 = null;

    Hashtable secParms        = null;

    HttpSession theSession    = request.getSession(false);

    /***************************************************
     * START DATA RETRIEVAL FOR THE BANK BRANCH DROPDOWN
     ***************************************************/

    //Corporate Org Web Bean
    CorporateOrganizationWebBean corpOrg = (CorporateOrganizationWebBean)beanMgr.createBean ("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
    corpOrg.getById(ownerOrg);

    //The the oids of the allowed opBankOrgs
    String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                          corpOrg.getAttribute("second_op_bank_org"),
                          corpOrg.getAttribute("third_op_bank_org"),
                          corpOrg.getAttribute("fourth_op_bank_org")};

    StringBuffer in = new StringBuffer(opBankOid[0]);

    for (oboCount=1; oboCount<4; oboCount++)
    {
        if (InstrumentServices.isBlank(opBankOid[oboCount]))
            break;
        else
            in.append(", ").append(opBankOid[oboCount]);
    }
    Debug.debug("in -> " + in.toString());
    Debug.debug("oboCount -> " + oboCount);

    if (startPage == null) //cancelAction will only be null if we are returning from an error
        getDataFromDoc = true;

    // Retrieve the data used to populate some of the dropdowns.
    // This data will be used in the bank branch dropdown
    // Typical sql: select organization_oid, name from operational_bank_org
    //              where organization_oid in ( 1, 2, 3, 4);

    StringBuffer sql = null;

    try {
      // Get a list of the bank branches for the client bank
      sql = new StringBuffer();
      sql.append("select ORGANIZATION_OID, NAME");
      sql.append(" from OPERATIONAL_BANK_ORG");
      sql.append(" where ORGANIZATION_OID in (").append(in).append(")");
      Debug.debug("sql -> " + sql.toString());

      queryDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true);
      Debug.debug("queryDoc --> " + queryDoc.toString());

    } catch (AmsException e) {
      Debug.debug("queryListView threw an exception");
      e.printStackTrace();
    }

    /***************************************************
     * END DATA RETRIEVAL FOR THE BANK BRANCH DROPDOWN
     ***************************************************/

    if (!getDataFromDoc)
    {
        //Set a session indicator to let us know we are in the new transaction area
        theSession.setAttribute("inNewTransactionArea", TradePortalConstants.INDICATOR_YES);
        //Store the start page on the session so future pages know where to go on <cancel>
        theSession.setAttribute("startPage", startPage);
    }
    else
    //HANDLE THE ERROR CASE
    {
        DocumentHandler doc = formMgr.getFromDocCache();

        outerRadioVal       = doc.getAttribute("/In/mode");
        innerRadioVal       = doc.getAttribute("/In/copyType");
        selectedInstrType   = doc.getAttribute("/In/instrumentType");
        selectedBankBranch  = doc.getAttribute("/In/bankBranch");
        startPage           = (String) theSession.getAttribute("startPage");
    }

    //SECURE PARAMETERS
    secParms = new Hashtable();
    secParms.put("userOid", userSession.getUserOid());
    secParms.put("securityRights", loginRights);
    secParms.put("clientBankOid", userSession.getClientBankOid());
    secParms.put("ownerOrg", userSession.getOwnerOrgOid());

    //IAZ IR-IZUK102110110 10/21/10 Begin
    //If Client Bank allows usage of template groups, check if any assigned to this user.
    Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
    DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
    String canViewPaymentTemplateGroups = TradePortalConstants.INDICATOR_NO;
    int templateGroupCount = 0;
    if ((CBCResult != null)&&(InstrumentServices.isNotBlank(userSession.getClientBankOid())))
    {
    	canViewPaymentTemplateGroups = CBCResult.getAttribute("/ResultSetRecord(0)/TEMPLATE_GROUPS_IND");
    	if (TradePortalConstants.INDICATOR_YES.equals(canViewPaymentTemplateGroups))
    	{
    	    String whereClause = "P_USER_OID = '" + userSession.getUserOid() + "' ";
    		templateGroupCount = DatabaseQueryBean.getCount("AUTHORIZED_TEMPLATE_GROUP_OID", "USER_AUTHORIZED_TEMPLATE_GROUP", whereClause.toString());
    	}
    }
    //out.println("tgc1 " + templateGroupCount);
    //IAZ IR-IZUK102110110 10/21/10 End

%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<form method="post" name="NewTransStep1Form" 
      action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr>
      <td width="20" class="BankColor" nowrap height="25">
        <img src="/portal/images/Blank_15.gif" width="15" height="15">
      </td>
      <td class="BankColor" valign="middle" align="left" nowrap height="25">
        <p class="ControlLabelWhite">
          <%= resMgr.getText("NewTransaction1.HeadingStep1", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td class="BankColor" width="100%" height="25" align="center" valign="middle">
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25">
        <%= OnlineHelp.createContextSensitiveLink("customer/new_transaction_no_context.htm",resMgr, userSession) %>
      </td>
      <td class="BankColor" width="20" height="25" nowrap>&nbsp;</td>
      <td class="BankColor" width="1" height="25" nowrap>&nbsp; </td>
      <td width="1" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%= resMgr.getText("NewTransaction1.Create", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td nowrap>
        <input type="radio" name="radioButtonOuter" 
               value="<%= TradePortalConstants.NEW_INSTR %>">
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%= resMgr.getText("NewTransaction1.NewInstrument", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td nowrap>
        <p class="ListText">
          <%= resMgr.getText("NewTransaction1.SpecifyBankBranch", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="70" nowrap>&nbsp;</td>
      <td class="ListText" valign="bottom">
        <%
        /***********************************
         * Start Bank Branch Dropdown Box
         ***********************************/
        Debug.debug("Bank Branch Dropdown");
        options = Dropdown.createSortedOptions(queryDoc,"ORGANIZATION_OID", "NAME",
                                               selectedBankBranch, resMgr.getResourceLocale());
        Debug.debug(options);
        %>
        <select name="bankBranch" class="ListText" 
                onChange="radioButtonOuter[0].checked='true'">
<%
          // If there is more than one op. bank org, add a "select" option.
          if (oboCount > 1) {
%>
            <option value="" selected>
                <%= resMgr.getText("NewTransaction1.SelectBankBranch", 
                                   TradePortalConstants.TEXT_BUNDLE) %>
            </option>
<%
          }
%>
          <%= options %>
        </select>
        <%
        /***********************************
         * End Bank Branch Dropdown Box
         ***********************************/
        %>
      </td>
      <td width="100%" height="30" valign="bottom">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td>
        <p class="ListText">
          <%= resMgr.getText("NewTransaction1.Specify", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="30" nowrap>&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="70" nowrap>&nbsp;</td>
      <td>
        <%
        /****************************
         * START INNNER RADIO BUTTONS
         ****************************/
        %>
        <input type="radio" name="radioButtonInner" 
               value="<%= TradePortalConstants.FROM_INSTR %>"
               onClick="radioButtonOuter[0].checked='true'">
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap height="30">
        <p class="ListText">
          <%= resMgr.getText("NewTransaction1.CopyFromInstrument", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <input type="radio" name="radioButtonInner" 
               value="<%= TradePortalConstants.FROM_TEMPL %>"
               onClick="radioButtonOuter[0].checked='true'">
      </td>
      <td>&nbsp;</td>
      <td>
        <p class="ListText">
          <%= resMgr.getText("NewTransaction1.CopyFromTemplate", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td height="30">&nbsp;</td>
    </tr>
<%
      //IAZ IR-IZUK102110110 10/21/10 Begin Changes
      //Make This Option only available when user has no template groups associated with.
      if (templateGroupCount == 0)
      {
%>

    <tr>
      <td>&nbsp;</td>
      <td>
        <input type="radio" name="radioButtonInner" 
               value="<%= TradePortalConstants.FROM_BLANK %>"
               onClick="radioButtonOuter[0].checked='true'">

      </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText">
        <p class="ListText">
          <%= resMgr.getText("NewTransaction1.UseBlankForm", 
                             TradePortalConstants.TEXT_BUNDLE) %>

<%
      
          /************************************
           * Start Instrument Type Dropdown Box
           ************************************/
          Debug.debug("Instrument Type Dropdown Box");

          //Get the instrument type vector
          Vector instrumentTypes = Dropdown.getEditableInstrumentTypes(formMgr,
                                        Long.valueOf(ownerOrg).longValue(),
                                        loginRights, 
                                        userSession.getSecurityType(),
                                        userSession.getOwnershipLevel());

          // Export LC should not appear in this dropdown
          if(instrumentTypes.contains(TradePortalConstants.EXPORT_DLC)) {
              instrumentTypes.removeElement(TradePortalConstants.EXPORT_DLC);
          }

	  // Transfer Between Acct should not appear in this dropdown
          if(instrumentTypes.contains(TradePortalConstants.XFER_BET_ACCTS)) {
              instrumentTypes.removeElement(TradePortalConstants.XFER_BET_ACCTS);
          } 	

	  // Domestic Payment should not appear in this dropdown
          if(instrumentTypes.contains(TradePortalConstants.DOMESTIC_PMT)) {
              instrumentTypes.removeElement(TradePortalConstants.DOMESTIC_PMT);
          }
          
          // IR - SRUK011185791 - Add Below condition to exclude DDI from drop down
          // Direct Debit should not appear in this dropdown
	  if(instrumentTypes.contains(TradePortalConstants.DIRECT_DEBIT_INSTRUCTION )) {
              instrumentTypes.removeElement(TradePortalConstants.DIRECT_DEBIT_INSTRUCTION );
          }  
          
          
         // CRhodes - 4/10/2009 - PYUJ031764918 - Added use of filterAllCashMgmtTrans flag to determine if FUNDS_XFER (International Payments)
         // should be removed from the dropdown list or not.          
         boolean filterAllCashMgmtTrans = false;

         if(corpOrg.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)){
           boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);   
           boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);   
           filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(loginRights);
         } else {
           boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
           boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);     
           filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(loginRights);
         }
          
         //Trudden IR PHUJ012854472 Add Begin
         //When Cash Mgt and Instruments tabs are both available, FUND_XFER should be excluded from this dropdown
	  if(instrumentTypes.contains(TradePortalConstants.FUNDS_XFER)) {
	       // if (TradePortalConstants.INSTRUMENT_CASH_TAB_VISIBLE.equals(corpOrg.tabVisible()))
         if (filterAllCashMgmtTrans == true) {
	            instrumentTypes.removeElement(TradePortalConstants.FUNDS_XFER);
	       }
          }
          //Trudden IR PHUJ012854472 End
		
          //Build the option tags for the dropdown box.
          options = Dropdown.getInstrumentList(selectedInstrType, 
                                               loginLocale, 
                                               instrumentTypes );
          Debug.debug(options);
%>
          <select name="instrumentType" class="ListText" 
                  onChange="radioButtonInner[2].checked='true'; radioButtonOuter[0].checked='true'">
            <option value="" selected>
              <%= resMgr.getText("NewTransaction1.SelectInstrumentType", 
                                 TradePortalConstants.TEXT_BUNDLE) %>
            </option>
            <%= options %>
          </select>
          <%
          /***********************************
           * End Instrument Type Dropdown Box
           ***********************************/
          %>
        </p>
      </td>
      <td height="30">&nbsp;</td>
    </tr>
<%    
      }
      else
      {
         out.println("<input type=hidden name=instrumentType value=''>");      
      }
      //IAZ IR-IZUK102110110 10/21/10 End Changes
%>      
    <tr>
      <td>&nbsp;</td>
      <td>
<%
        //export DLC Radio Button
        if (SecurityAccess.hasRights(loginRights,SecurityAccess.EXPORT_LC_CREATE_MODIFY)) {
           String allowExportLC = corpOrg.getAttribute("allow_export_LC");
           if (allowExportLC != null && allowExportLC.equals(TradePortalConstants.INDICATOR_YES)) 
           {
%>
               <input type="radio" name="radioButtonInner" 
                      value="<%= TradePortalConstants.TRANSFER_ELC %>"
                      onClick="radioButtonOuter[0].checked='true'">
               </td>
               <td>&nbsp;</td>
               <td nowrap>
                 <p class="ListText">
                   <%= resMgr.getText("NewTransaction1.TransferExportLC", 
                                      TradePortalConstants.TEXT_BUNDLE) %>
                 </p>
<%
           } // end allowExportLC = Y
        } /* end if has rights for export lc */
%>
      </td>
      <td>&nbsp;</td>
      <td height="30">&nbsp;</td>
    </tr>
    <%
    /****************************
     * END INNNER RADIO BUTTONS
     ****************************/
    %>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td nowrap>&nbsp; </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <% /* RADIO BUTTON */%>
          <input type="radio" name="radioButtonOuter" 
                 value="<%= TradePortalConstants.EXISTING_INSTR %>">
        <% /* RADIO BUTTON */%>
      </td>
      <td>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel">
          <%= resMgr.getText("NewTransaction1.NewTrans", 
                             TradePortalConstants.TEXT_BUNDLE) %></span>
          <input type="hidden" name="NewSearch" value="Y">
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" 
         class="BankColor" height="17">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td class="BankColor" width="100%">&nbsp;</td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr>
            <td>&nbsp;</td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
            <%
            /*******************
             * START NEXT BUTTON
             *******************/
            %>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                <jsp:param name="showButton" value="true" />
                <jsp:param name="name" value="NextButton" />
                <jsp:param name="image" value='common.NextStepImg' />
                <jsp:param name="text" value='common.NextStepText' />
                <jsp:param name="submitButton"
                           value="<%=TradePortalConstants.BUTTON_SAVE %>" />
              </jsp:include>

            <%
            /*******************
             * END NEXT BUTTON
             *******************/
            %>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
            <%
            /*********************
             * START CANCEL BUTTON
             *********************/
             String cancelLink = formMgr.getLinkAsUrl("cleanUpDoc", "", response, secParms);

            %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name" value="CancelButton" />
                <jsp:param name="image" value='common.CancelImg' />
                <jsp:param name="text" value='common.CancelText' />
                <jsp:param name="link" value="<%= java.net.URLEncoder.encode(cancelLink) %>" />
              </jsp:include>

            <%
            /*******************
             * END CANCEL BUTTON
             *******************/
            %>
            </td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <%= formMgr.getFormInstanceAsInputField("NewTransStep1Form", secParms) %>

<%
  if (getDataFromDoc)
  //REFRESH RADIO BUTTON SELECTIONS FROM THE DOC CACHE
  {
        if (outerRadioVal != null)
        {
            if (outerRadioVal.equals(TradePortalConstants.NEW_INSTR))
            { %>
                <script LANGUAGE="JavaScript">
                    document.NewTransStep1Form.radioButtonOuter[0].checked='true';
                </script>
            <% }
            else if (outerRadioVal.equals(TradePortalConstants.EXISTING_INSTR))
            { %>
                <script LANGUAGE="JavaScript">
                    var size=document.NewTransStep1Form.radioButtonOuter.length;
                    if (size==2)
                    {
                        document.NewTransStep1Form.radioButtonOuter[1].checked='true';
                    }
                    else
                    {
                        document.NewTransStep1Form.radioButtonOuter[2].checked='true';
                    }
                </script>
            <% }
        }

        if (innerRadioVal != null)
        {
            if (innerRadioVal.equals(TradePortalConstants.FROM_INSTR))
            { %>
                <script LANGUAGE="JavaScript">
                    document.NewTransStep1Form.radioButtonInner[0].checked='true';
                </script>
            <% }
            else if (innerRadioVal.equals(TradePortalConstants.FROM_TEMPL))
            { %>
                <script LANGUAGE="JavaScript">
                    document.NewTransStep1Form.radioButtonInner[1].checked='true';
                </script>
            <% }
            else if (innerRadioVal.equals(TradePortalConstants.FROM_BLANK))
            { %>
                <script LANGUAGE="JavaScript">
                    document.NewTransStep1Form.radioButtonInner[2].checked='true';
                </script>
            <% }
            else if (innerRadioVal.equals(TradePortalConstants.TRANSFER_ELC))
            { %>
                <script LANGUAGE="JavaScript">
                    document.NewTransStep1Form.radioButtonInner[3].checked='true';
                </script>
            <% }
        }
  }
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
  %>
  </form>
</body>

</html>
