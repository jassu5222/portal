<%--
*******************************************************************************
                         Rollover Button Submit

  Description:
    This includable JSP create HTML for a party search button.  It is a 
  basically a pass through to the RolloverButtonSubmit.jsp.  However, the
  button name is always PartySearch (while the image name takes on the name
  that was passed in).  The partyType parameter is used to build a method
  call for the button is clicked.  The resulting HTML supports rollover logic
  and submits the page for doing a party search.

  This is some sample HTML produced:
  
     <a href="javascript:document.forms[0].submit()" name="PartySearch"
        onMouseOut="changeImage('SearchAppButton', 
                                '/portal/images/Button_SearchforApplicant_off.gif')"
        onMouseOver="changeImage('SearchAppButton', 
                                 '/portal/images/Button_SearchforApplicant_ro.gif')"
        onClick="setButtonPressed('PartySearch'); return setPartySearchFields('APP');">
       <img border="0" name="SearchAppButton" width=140 height=17
            src="/portal/images/Button_SearchforApplicant_off.gif"
            alt="Search"></a>

  The button is ALWAYS called PartySearch.  (Thus all party search buttons submit
  to the same location.)

  To use this jsp, include an entry similar to the following in your jsp.

        <jsp:include page="/common/PartySearchButton.jsp">
           <jsp:param name="showButton" value="<%=!isReadOnly%>" />
           <jsp:param name="name" value="SearchBenButton" />
           <jsp:param name="image" value='common.SearchBeneficiaryImg' />
           <jsp:param name="text" value='common.SearchText' />
           <jsp:param name="width" value="140" />
           <jsp:param name="partyType" 
                      value="<%=TradePortalConstants.BENEFICIARY%>" />
        </jsp:include>

  See below for a definition of the parameters.

  Parameters:
    showButton - true/false: indicates if <input> tag or nbsp; is returned
    name       - logical 'name' of the button, used for determing the mouse
                 over variable names -- see above.  This is NOT the name of 
                 the button, the button is always called PartySearchButton.
    image      - resource bundle key for the image in normal state
    text       - resource bundle key for the alternate text for the image
    partyType  - the party type the button represents, will map to a field
                 that is passed to the Party Search page
    width      - width of the button.  Default value is 96

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.util.*, com.amsinc.ecsg.util.StringFunction" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<%
  boolean showButton = true;
  String name;
  String partyType;

  String value = request.getParameter("showButton");
  if ((value != null) && value.equals("false")) showButton = false;

  name  = request.getParameter("name");
partyType = StringFunction.xssCharsToHtml(request.getParameter("partyType"));
//Validation to check Cross Site Scripting
  if(partyType != null){
	  partyType = StringFunction.xssCharsToHtml(partyType);
  }
  
  
  // CR-486 05/24/10 - Begin
  String unicodeInd = request.getParameter("displayUnicode");
  if (InstrumentServices.isBlank(unicodeInd)) {
	  unicodeInd = TradePortalConstants.INDICATOR_NO;
  }
  //CR-486 05/24/10 - End
  
  // String used to construct an onClick tag for the party search buttons.
  //String clicktag = "return setPartySearchFields('" + partyType + "');";   // CR-486 05/24/10
  String clicktag = "return setPartySearchFields('" + partyType + "','" + unicodeInd + "');";    // CR-486 05/24/10

  if (showButton) {
%>
      <a name=<%=partyType%>></a>
      <jsp:include page="/common/RolloverButtonSubmit.jsp">
        <jsp:param name="showButton" value='true' />
        <jsp:param name="name" value="PartySearch" />
        <jsp:param name="image" value='<%=request.getParameter("image")%>' />
        <jsp:param name="text" value='<%=request.getParameter("text")%>' />
        <jsp:param name="width" value='<%=request.getParameter("width")%>' />
        <jsp:param name="extraTags" value="<%=clicktag%>" />
        <jsp:param name="submitButton"
                   value="<%=TradePortalConstants.BUTTON_PARTYSEARCH%>" />
        <jsp:param name="imageName" 
                   value='<%=request.getParameter("name")%>' />
      </jsp:include>
<% } else {
       out.println("&nbsp;");
   }
%>
