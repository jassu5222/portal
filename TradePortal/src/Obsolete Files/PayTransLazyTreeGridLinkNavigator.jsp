
<%--
**********************************************************************************
  LazyTreeGridLinkNavigator.jsp
	This navigates thelink action in Payables Credit Notes Lazytree Grid by determining
	the incoming oid to the exact target page. 
  
**********************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, 
                 com.ams.tradeportal.busobj.*, java.math.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
	String closeLink = "goToPayableTransactionsHome";
	String inCommingOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("upload_invoice_oid"), userSession.getSecretKey());
	boolean isUploadInvOid = false;
	boolean isUploadCNOid = false;
	String uploadCNOidSQL = "Select * from Credit_Notes where UPLOAD_CREDIT_NOTE_OID = ?";
	String uploadInvOidSQL = "Select * from invoices_summary_data where UPLOAD_INVOICE_OID = ?";
    DocumentHandler uploadInvOidResults = DatabaseQueryBean.getXmlResultSet(uploadInvOidSQL, false, new Object[]{inCommingOid});
    DocumentHandler uploadCNOidResults = DatabaseQueryBean.getXmlResultSet(uploadCNOidSQL, false, new Object[]{inCommingOid});
    
     if(uploadInvOidResults != null){
    	 isUploadInvOid = true;
     }else if(uploadCNOidResults != null){
    	 isUploadCNOid = true;
     }
     
     if(isUploadInvOid){
%>     
		<%@ include file="../invoicemanagement/fragments/UploadInvoiceDetails.frag"%>
<%
     }
     if(isUploadCNOid){
%>
    	<%@ include file="../invoicemanagement/fragments/UploadCreditNoteDetail.frag"%>
<%
     }
%>    	