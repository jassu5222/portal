<%--
*******************************************************************************
                        Upload Payment Data File Page

  Description:  The Upload Payment Data File page is used for uploading
                a payment data file from the user's computer to the
                Trade Portal with multiple payees for a Domestic Payment 
                Transaction. Once uploaded the data will be displayed on the 
                Domestic Payment Screen in the Payee List Section where the user
                will be able to modify any data.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   String onLoad = null;
   
   // Set the focus to the Purchase Order File Location field
   onLoad = "document.forms[0].PaymentDataFilePath.focus();";
%>


<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>

<%--
   Note: The following form MUST have the 'enctype' attribute specified as "multipart/form-data"
   in order for the contents of the file selected by the user to be uploaded. It also must go 
   through the Payment File Upload Servlet, as this servlet handles the 
   requests and forwards the user to the appropriate page afterwards.
--%>
<form name="UploadPaymentFileDataForm" method="POST" enctype="multipart/form-data"
      action="<%= response.encodeURL(request.getContextPath()+ "/transactions/PaymentFileUploadServlet.jsp") %>">

  <input type=hidden name="buttonName" value="">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr> 
      <td width="20" class="BankColor" nowrap height="25">
        <img src="/portal/images/Blank_15.gif" width="15" height="15">
      </td>
      <td class="BankColor" valign="middle" align="left" nowrap height="25"> 
        <p class="ControlLabelWhite">
          <%= resMgr.getText("LocateUploadPaymentFile.Header", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td class="BankColor" width="100%" height="25" align="center" valign="middle"> 
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25">
        <%= OnlineHelp.createContextSensitiveLink("customer/Payment_File_Upload.htm", resMgr, userSession) %>
      </td>
      <td class="BankColor" width="20" height="25" nowrap>&nbsp;</td>
      <td class="BankColor" width="1" height="25" nowrap>&nbsp; </td>
      <td width="1" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%= resMgr.getText("LocateUploadPaymentFile.ToUploadData", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td nowrap align="left" valign="bottom"> 
        <p class="ControlLabel">
          <%= resMgr.getText("LocateUploadPaymentFile.PaymentDataFileLocation", TradePortalConstants.TEXT_BUNDLE) %>
          <br>
          <%=InputField.createFileField("PaymentDataFilePath", "70", "ListText", false) %>
        </p>
      </td>
      <td width="10" nowrap>&nbsp;</td>
      <td align="left" valign="bottom">&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%= resMgr.getText("LocateUploadPaymentFile.ToUploadDataNote", TradePortalConstants.TEXT_BUNDLE) %>
        </p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td height="18">&nbsp;</td>
      <td height="18">&nbsp;</td>
      <td height="18">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor" height="48">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                 <jsp:param name="showButton"   value="true" />
                 <jsp:param name="name"         value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
                 <jsp:param name="image"        value='common.UploadFileImg' />
                 <jsp:param name="text"         value='common.UploadFileText' />
                 <jsp:param name="width"        value="96" />
                 <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
                 <jsp:param name="imageName"    value="UploadFileButton" />
              </jsp:include>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td>
              <%
                 String cancelLink=formMgr.getLinkAsUrl("goToInstrumentNavigator", response);
              %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="showButton" value="true" />
                  <jsp:param name="name"  value="CancelButton" />
                  <jsp:param name="image" value="common.CancelImg" />
                  <jsp:param name="text"  value="common.CancelText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(cancelLink)%>" />
                  <jsp:param name="width" value="96" />
              </jsp:include>
            </td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
