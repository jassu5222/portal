<%--
*******************************************************************************
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<div>

<input type="hidden" name="NewSearch" value="Y">

<script language="Javascript">




  function doSearch(sort, startRow, sortColumn, sortOrder){
    var getstr = getFormData(document.getElementById("InvoiceSearchForm"));
    if(sort == true){
      getstr += "redisplayList=true&"
      getstr += "listViewToSort=PaymentMatchInvoiceSearchListView.xml&"
      if(startRow != null){
        getstr += "startRow=" + startRow + "&";
      }
      if(sortColumn != null){
        getstr += "sortColumn=" + sortColumn + "&"; 
      }
      if(sortOrder != null){
        getstr += "sortOrder=" + sortOrder + "&";
      }
    }
    
    executeInvoiceSearch(getstr);    
	
  }
  
  
  function getFormData(obj) {
    var getstr = "";
    for (i=0; i<obj.getElementsByTagName("input").length; i++) {
      if (obj.getElementsByTagName("input")[i].type == "text" || obj.getElementsByTagName("input")[i].type == "hidden" || obj.getElementsByTagName("input")[i].type == "HIDDEN") {
        getstr += obj.getElementsByTagName("input")[i].getAttribute("name") + "=" + 
            obj.getElementsByTagName("input")[i].value + "&";
      }
      if (obj.getElementsByTagName("input")[i].type == "checkbox") {
        if (obj.getElementsByTagName("input")[i].checked) {
          getstr += obj.getElementsByTagName("input")[i].getAttribute("name") + "=" + 
              obj.getElementsByTagName("input")[i].value + "&";
        } else {
          getstr += obj.getElementsByTagName("input")[i].getAttribute("name") + "=&";
        }
      }
      if (obj.getElementsByTagName("input")[i].type == "radio") {
        if (obj.getElementsByTagName("input")[i].checked) {
          getstr += obj.getElementsByTagName("input")[i].getAttribute("name") + "=" + 
              obj.getElementsByTagName("input")[i].value + "&";
        }
      }  

    }
    for(i = 0; i < obj.getElementsByTagName("select").length; i++){
      var sel = obj.getElementsByTagName("select")[i];
      getstr += sel.getAttribute("name") + "=" + sel.options[sel.selectedIndex].value + "&";
    }

    return getstr;
  }
  

</script>




<%

  String userSecurityType   = userSession.getSecurityType();
  String invoiceID          = request.getParameter("InvoiceID") == null         ? "" : request.getParameter("InvoiceID");
  String yearFrom           = request.getParameter("YearFrom") == null          ? "" : request.getParameter("YearFrom");
  String yearTo             = request.getParameter("YearTo") == null            ? "" : request.getParameter("YearTo");
  String monthTo            = request.getParameter("MonthTo") == null           ? "" : request.getParameter("MonthTo");
  String monthFrom          = request.getParameter("MonthFrom") == null         ? "" : request.getParameter("MonthFrom");
  String dayTo              = request.getParameter("DayTo") == null             ? "" : request.getParameter("DayTo");
  String dayFrom            = request.getParameter("DayFrom") == null           ? "" : request.getParameter("DayFrom");
  String currency           = request.getParameter("Currency") == null          ? "" : request.getParameter("Currency");
  String amountType         = request.getParameter("AmountType") == null        ? "" : request.getParameter("AmountType");
  String amountFrom         = request.getParameter("AmountFrom") == null        ? "" : request.getParameter("AmountFrom");
  String amountTo           = request.getParameter("AmountTo") == null          ? "" : request.getParameter("AmountTo");
  String partyFilterText    = request.getParameter("PartyFilterText") == null   ? "" : request.getParameter("PartyFilterText");
  //String financeInstrument  = request.getParameter("FinanceInstrument") == null ? "" : request.getParameter("FinanceInstrument");
  String poNumber           = request.getParameter("PONumber") == null          ? "" : request.getParameter("PONumber");
  String dateType           = request.getParameter("DateType") == null          ? "" : request.getParameter("DateType");
  String financeChecked		= request.getParameter("Financed") == null          ? "" : request.getParameter("Financed");
  boolean isChecked 		= InstrumentServices.isNotBlank(financeChecked);
  String sortColumn         = request.getParameter("sortColumn");
  String sortOrder          = request.getParameter("sortOrder");
  
  String otherParty = "";
  String fromDate = null;
  String toDate = null;
  String options = null;  
  
 
  
  
   
	String gridLayout= "";	
	
	String titleKey ="";
	String helpUrl="";
   String selectedstatus="";
  
  
  
  
  StringBuffer newSearchCriteria = new StringBuffer();   
   

%>
<%

  List conditions = new ArrayList();
  
  if (invoiceID == null) {
    invoiceID = "";
  } else {
    invoiceID = invoiceID.toUpperCase();
  }
        
  // Search by the invoice ID
  
  if (!invoiceID.equals("")) {
    conditions.add(" upper(invoice_reference_id) like '" +invoiceID + "%'");
  }
                
  if ("DDT".equals(dateType)){
    dateType = "invoice_due_date";
  }
  else if ("IDT".equals(dateType)){
    dateType = "invoice_issue_datetime";
  }
  else {
    dateType = "";
  }  
  
  if (!dateType.equals("")){              
    if (TPDateTimeUtility.isGoodDate(yearFrom, monthFrom, dayFrom)) {
      fromDate = TPDateTimeUtility.formatOracleDate(yearFrom, monthFrom, dayFrom);
      if (fromDate != null && !fromDate.equals("")) {
        conditions.add( dateType + " >= '" + fromDate + "'");
      }
    } else {
      /*medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                           TradePortalConstants.INVALID_FROM_DATE); */        
    }

                 
    if (TPDateTimeUtility.isGoodDate(yearTo, monthTo, dayTo)) {
        toDate = TPDateTimeUtility.formatOracleDate(yearTo, monthTo, dayTo);
      if (toDate != null && !toDate.equals("")) {
        conditions.add( dateType + " <= '" + toDate + "'");
      }
    } else {
      /*medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                             TradePortalConstants.INVALID_TO_DATE);*/
    }
  }
  
  
  
  currency = request.getParameter("Currency");
  if (currency != null && !"".equals(currency)) {
    conditions.add( "currency_code='" + currency + "'");
  }     

/*  financeInstrument = request.getParameter("FinanceInstrument");
  if (InstrumentServices.isNotBlank(financeInstrument)) {
  StringBuffer financeSql = new StringBuffer(" i.a_finance_instrument_oid in (select instrument_oid from instrument where complete_instrument_id = '");
    financeSql.append(financeInstrument.toUpperCase());
    financeSql.append("')");
    conditions.add( financeSql.toString());
  }
 
*/  
  // Search by Finance
  financeChecked = request.getParameter("Financed");
  if (InstrumentServices.isNotBlank(financeChecked)  ) {
    conditions.add( "finance_status in ('FIN','AFF') ");
  }
  
  
  

  if("INA".equals(amountType)){
    amountType = "invoice_total_amount";
  }
  else if ("DSA".equals(amountType)){
    amountType = "invoice_discount_amount";
  }
  else if ("CNA".equals(amountType)){
    amountType = "invoice_credit_note_amount";
  }
  else if ("FNA".equals(amountType)){
    amountType = "finance_amount";
  }
  else { 
    amountType = "";
  }
        
  if(amountType != null && !"".equals(amountType)) {

    if (amountFrom != null && !"".equals(amountFrom)) {
      amountFrom = amountFrom.trim();
      try {
        String amount = NumberValidator.getNonInternationalizedValue(amountFrom,
                                                      loginLocale);
        if (!amount.equals("")) {
          conditions.add( amountType + " >= " + amount);
        }
      } catch (InvalidAttributeValueException e) {
            /*medService.getErrorManager().issueError(
                           TradePortalConstants.ERR_CAT_1,
                           TradePortalConstants.INVALID_CURRENCY_FORMAT,
                           amountFrom);   */              
      } 
    } 
    
          
    if (amountTo != null && amountTo != "") {
      amountTo = amountTo.trim();
      try {
        String amount =
        NumberValidator.getNonInternationalizedValue(amountTo,
                                                  loginLocale);
        if (!amount.equals("")) {
          conditions.add( amountType + " <= " + amount);
        }
      } catch (InvalidAttributeValueException e) {
      /*medService.getErrorManager().issueError(
                       TradePortalConstants.ERR_CAT_1,
                       TradePortalConstants.INVALID_CURRENCY_FORMAT,
                       amountTo);     */    
      }
    } 
  }
        
  partyFilterText = request.getParameter("PartyFilterText");
  if ( partyFilterText != null && !partyFilterText.equals("")) {          
    partyFilterText = partyFilterText.trim().toUpperCase();
    if (!partyFilterText.equals("")) {
      conditions.add( "upper(buyer_party_identifier) like '" + SQLParamFilter.filter(partyFilterText) + "%'");
    }
  } 

  if (poNumber != null && !poNumber.equals("")) {
    conditions.add("invoice_oid in (select p_invoice_oid from invoice_goods where upper(po_reference_id) like '" + poNumber.toUpperCase()+ "%')");
  }
    
  
  //Assemble where clause
  StringBuffer dynamicWhereClause = null;
  if(conditions.size() == 0 && emptySearchFilter != null){
    conditions.add(emptySearchFilter);
  }

  dynamicWhereClause = new StringBuffer();    
  for(Iterator i = conditions.iterator(); i.hasNext(); ){
    String condition = (String)i.next();
    dynamicWhereClause.append(" and ").append(condition);
  }

  //IR - MUUJ032477002 - VShah - BEGIN
  // W Zhu 4/24/09 MMUJ041745035 no subsidiary.
  dynamicWhereClause.append(" and INVOICE.a_corp_org_oid = ");
  dynamicWhereClause.append(userOrgOid);
  //IR - MUUJ032477002 - VShah - END
  
  Debug.debug("*** where1-> " +dynamicWhereClause);
%>


<form style="margin-bottom: 0px;" id="InvoiceSearchForm" name="InvoiceSearchForm" method="POST">

  <div class="formContentNoSidebar">
<table>
		<tr>
		<td>
		</td>
			<%= widgetFactory.createLabel("InvoiceSearch.FinanceStatus","InvoiceSearch.FinanceStatus", true, false, false,"inline") %>
			<td>
				<%-- <% if (TradePortalConstants.INDICATOR_YES.equals(finance)) { %> --%>
					  <%=widgetFactory.createCheckboxField("Financed", "InvoiceSearch.Financed", false, false, false, "", "", "") %>
					  
				<%-- <% } else { %> --%>
					  <%-- <%=InputField.createCheckboxField("Financed", TradePortalConstants.INDICATOR_NO, "", false, "ListText", "", false, "onChange=\"setFinanceCheck();\"")%> --%>
				<%-- <% } %> --%>
			</td>
			<td></td>
			<td></td>
		</tr>
	</table>

<div>
  			<%= widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID","20","inline")%>
  		
  	
  			<%= widgetFactory.createSearchTextField("BuyerIdentifier", "InvoiceSearch.BuyerIdentifier","20","inline")%>
  		
  		
  			
  			<%= widgetFactory.createSearchTextField("PONumber", "InvoiceSearch.PONumber","20","inline")%>
  		
  		
  			<button data-dojo-type="dijit.form.Button" type="button" id="searchParty">Search
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchMatchInvoices();</script>
		  	</button>
  		

  	
  			<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("AmountType", "InvoiceSearch.AmountType", " ", options,"inline"));
  			%>
  		
  			<%=widgetFactory.createSearchTextField("AmountFrom", "InvoiceSearch.From","20","inline") %>
  			
  	
  			<%=widgetFactory.createSearchTextField("AmountTo", "InvoiceSearch.To", "20","inline") %>
  	
  		
  			<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, dateType, loginLocale);
  			out.println(widgetFactory.createSearchSelectField("DateType", "InvoiceSearch.DateType", " ", options, "inline"));
  			%>
  		
  		
  			<%=widgetFactory.createSearchDateField("DateFrom", "InvoiceSearch.From", "inline") %>
  		
  			<%=widgetFactory.createSearchDateField("DateTo", "InvoiceSearch.To", "inline") %>
  		
  	</div>
  
  
 
 <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
String	gridHtml = dgFactory.createDataGrid("MatchInvoicesDataGridId","MatchInvoicesDataGrid",null);
// out.println("gridHtml="+gridHtml);

%>

<%=gridHtml%>  


<%------
<input type=hidden name=SearchType value=<%=TradePortalConstants.SIMPLE%>>
  
  <jsp:include page="/common/TradePortalListView.jsp">
    <jsp:param name="listView"       value='PaymentMatchInvoiceSearchListView.xml' /> 
    <jsp:param name="whereClause2"   value='<%= dynamicWhereClause.toString() %>' />
    <jsp:param name="userType"       value='<%= userSecurityType %>' />
    <jsp:param name="searchCriteria" value='<%= newSearchCriteria.toString() %>' />
    <jsp:param name="alternateRedisplayAction" value="doSearch"/>
    <jsp:param name="absoluteScrollHeight"   value="316"/>
  </jsp:include> 
 ------%>
 
 
  
 </div>
 
</form>
<%


	// This boolean and MediatorServices provides error handling for invalid
    // search criteria fields.
  	boolean errorsFound = false;
  	MediatorServices medService = new MediatorServices();
 	medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
 	
 	// AmountFrom is not numeric then error needs to incorporated
 	
 	String errorVariable = request.getParameter("AmountFrom");
	boolean amountTypeValue =  InstrumentServices.isBlank(request.getParameter("AmountType"));
	boolean amountTypeError = false;
 	
        if (!InstrumentServices.isBlank(errorVariable)) {
           try {
                 NumberValidator.getNonInternationalizedValue(errorVariable.trim(),
                                                              loginLocale);
                  amountTypeError = true;
                  
                                                              
           } catch (InvalidAttributeValueException e) {
              medService.getErrorManager().issueError(
                                 TradePortalConstants.ERR_CAT_1,
                                 TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                 errorVariable);
              errorsFound = true;
           }
      }

      errorVariable = request.getParameter("AmountTo");
 	  if (!InstrumentServices.isBlank(errorVariable)) {
           try {
                 NumberValidator.getNonInternationalizedValue(errorVariable.trim(),
                                                              loginLocale);
				amountTypeError = true;                                                              
           } catch (InvalidAttributeValueException e) {
              medService.getErrorManager().issueError(
                                 TradePortalConstants.ERR_CAT_1,
                                 TradePortalConstants.INVALID_CURRENCY_FORMAT,
                                 errorVariable);
              errorsFound = true;
           }
      }


      if (amountTypeError && amountTypeValue){
      
      medService.getErrorManager().issueError(
                                 TradePortalConstants.ERR_CAT_1,
                                 TradePortalConstants.INVALID_AMOUNT_TYPE);
              errorsFound = true;
      }
      
 
  if (!TPDateTimeUtility.isGoodDate(request.getParameter("YearTo"),request.getParameter("MonthTo"),request.getParameter("DayTo")))
  {         medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_TO_DATE);
           errorsFound = true;
  }
  
  if (!TPDateTimeUtility.isGoodDate(request.getParameter("YearFrom"),request.getParameter("MonthFrom"),request.getParameter("DayFrom")))
  {         medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_FROM_DATE);
           errorsFound = true;
  }
  
  boolean  dateTypeValue = InstrumentServices.isBlank(request.getParameter("DateType"));

  if (((TPDateTimeUtility.isGoodDate(request.getParameter("YearTo"),request.getParameter("MonthTo"),request.getParameter("DayTo"))&& (request.getParameter("DayTo")!= null) && (!request.getParameter("DayTo").equals("-1"))) ||
 	   (TPDateTimeUtility.isGoodDate(request.getParameter("YearFrom"),request.getParameter("MonthFrom"),request.getParameter("DayFrom")) && (request.getParameter("DayFrom") != null) && (!request.getParameter("DayFrom").equals("-1"))))
 	   && dateTypeValue)
 	   
   {         medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                   TradePortalConstants.INVALID_DATE_TYPE);
           	 errorsFound = true;
   }
     if (errorsFound) {
    // Any errors found while editing the date and amount fields need to be
    // placed in the doc cache.
    DocumentHandler errDoc = formMgr.getFromDocCache();
    medService.addErrorInfo();
    errDoc.setComponent("/Error", medService.getErrorDoc());
    //formMgr.storeInDocCache("default.doc", errDoc);
  


%>
<jsp:include page="/common/ErrorSection.jsp">
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%
//IR - MJUJ022054796 Pramey Begin
errDoc.removeComponent("/Error");
formMgr.storeInDocCache("default.doc", errDoc);
//IR - MJUJ022054796 Pramey End
}
%>





  
<%

// Once the items have been returned populate the javascript invoices object with the values from the objects.
  List invoiceList = new ArrayList();
  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  sqlQuery = new StringBuffer();
  sqlQuery.append("select invoice_oid");
  sqlQuery.append("  from  invoice where INVOICE.INVOICE_STATUS NOT IN ('PAD','CLD','CFN','MTH')");
  sqlQuery.append(dynamicWhereClause);  
  
  Debug.debug("The sqlquery is "  + sqlQuery);
  queryListView.setSQL(sqlQuery.toString());
  queryListView.getRecords();
  
  xmlDoc = queryListView.getXmlResultSet();
  listviewVector = xmlDoc.getFragments("/ResultSetRecord");
  
  for(int i = 0; i < listviewVector.size(); i++){
    DocumentHandler resultRow = (DocumentHandler)listviewVector.elementAt(i);
    InvoiceWebBean obj = (InvoiceWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.InvoiceWebBean", "Invoice");
    String oid = resultRow.getAttribute("/INVOICE_OID");
    obj.getById(oid);
    invoiceList.add(obj);
  }
%>

<script language="Javascript" id="InvoiceSearchFilterAJAXScript">



 
<% if(invoiceList.size() > 0){ %>  
  
    matchContext.clearInvoiceSearchList();
    var invoice = null;
<% 
  
  for(Iterator i = invoiceList.iterator(); i.hasNext(); ){
    InvoiceWebBean bean = (InvoiceWebBean)i.next();
    String oid = bean.getAttribute("invoice_oid");
    String hashedOid = EncryptDecrypt.encryptStringUsingTripleDes(oid, userSession.getSecretKey());
    String referenceId = bean.getAttribute("invoice_reference_id");
    String dueDate = bean.getAttribute("invoice_due_date");
    
    dueDate = TPDateTimeUtility.convertGMTDateForTimezoneAndFormat(dueDate, TPDateTimeUtility.SHORT, resMgr.getResourceLocale(), userSession.getTimeZone());
    String totalAmount = bean.getAttribute("invoice_total_amount");
    String outstandingAmount = bean.getAttribute("invoice_outstanding_amount");
    String financeStatus = bean.getAttribute("finance_status");
    String financePercentage = bean.getAttribute("finance_percentage");
%>
        invoice = new Invoice("<%=hashedOid%>", "<%=referenceId%>", "<%=dueDate%>", "<%=totalAmount%>", "<%=outstandingAmount%>","<%=financeStatus%>","<%=financePercentage%>");
    
    var newOid = "NewMatch" + matchContext.getNewMatchesList().getElements().length;
    <%--var matchListItem = new MatchListItem(newOid,<%=oid%>,matchContext.getSelectedPayRemitInvoiceOid(),true,"MMD",invoice.getOutstandingAmount(),"Y");--%>
    
    matchContext.getInvoiceSearchList().addItem(invoice);

<%}%>

<%}%>

</script>
