

<%--
*******************************************************************************
  Invoices Search Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
 
<%
	 dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	 gridLayout = dgFactory.createGridLayout("MatchInvoicesDataGridId", "MatchInvoicesDataGrid");
%>


<script type="text/javascript">

var initSearchParms="";
 var loginLocale="";


 <%--get grid layout from the data grid factory--%>
 var gridLayout = <%=gridLayout%>;

 <%--set the initial search parms--%>
 
 
	 
 
 initSearchParms = "userOrgOid=<%=userOrgOid%>";
 console.log("initSearchParms: " + initSearchParms);
 
 
 
 <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
 var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("MatchInvoicesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%> 
 createDataGrid("MatchInvoicesDataGridId", viewName,gridLayout,initSearchParms);
 	        

 <%--provide a search function that collects the necessary search parameters--%>
   function searchMatchInvoices() {
     require(["dojo/dom"],
       function(dom){
     	
    	 var finance="";
     		if(dom.byId("Financed").checked)
     			finance="Y";
     		else
     			finance="N";
     	
     	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
     	var partyFilterText=(dom.byId("BuyerIdentifier").value).toUpperCase();
     	var poNumber=(dom.byId("PONumber").value).toUpperCase();
     	
     	
     	var amountType=(dijit.byId("AmountType").value).toUpperCase();
     	var amountFrom=(dom.byId("AmountFrom").value).toUpperCase();
     	var amountTo=(dom.byId("AmountTo").value).toUpperCase();
     	
     	var dateType=(dijit.byId("DateType").value).toUpperCase();
     	var fromDate=(dom.byId("DateFrom").value).toUpperCase();
     	var toDate=(dom.byId("DateTo").value).toUpperCase();
     	
     	
     	
     	
         
         //var filterText=filterTextCS.toUpperCase();
  var searchParms = initSearchParms+"&finance="+finance+"&invoiceID="+invoiceID+"&partyFilterText="+partyFilterText+"&poNumber="
         +poNumber+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&dateType="+dateType+"&fromDate="+fromDate
         +"&toDate="+toDate; 
         
         console.log("SearchParms: " + searchParms);
         searchDataGrid("MatchInvoicesDataGridId", "MatchInvoicesDataView",
                        searchParms);
       });
   } 
 </script>
