 <%--
 *	   Dev By Sandeep
 *     Copyright  � 2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
<%	
	Debug.debug("*** Before Including Listview"); 
	Debug.debug("*** <<reportFlag>>: "+TradePortalConstants.REPORTFLAG);
	Debug.debug("*** <<categories>>: "+selectedCategorySQL);
	Debug.debug("*** <<reportType>>: "+reportType);
%>
<jsp:include page="/common/TradePortalListView.jsp">
    <jsp:param name="listView"   value="StandardReportListView.xml"/>
    <jsp:param name="reportFlag" value="<%=TradePortalConstants.REPORTFLAG%>"/>
    <jsp:param name="categories" value="<%= selectedCategorySQL %>"/>
    <jsp:param name="reportType" value="<%= reportType %>"/>
    <jsp:param name="whereClause2" value='<%=where%>' />
</jsp:include>
<%	Debug.debug("*** After Including Listview"); %>