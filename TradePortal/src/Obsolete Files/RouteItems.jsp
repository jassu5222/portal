<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%
   // note: no check to security since there may be multiple transactions selected
   // route is based on transaction type.
   //Chandrakanth 11/27/2008 BR-451 Begin
   String currentPrimaryNavigation = userSession.getCurrentPrimaryNavigation();   
   if ("NavigationBar.CashManagement".equals(currentPrimaryNavigation) ||
    		"goToCashManagement".equals(session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_CLOSE_ACTION))) {
	   userSession.setCurrentPrimaryNavigation("NavigationBar.CashManagement");
   } else {
	   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");   
   }
    	
 //Chandrakanth 11/27/2008 BR-451 End
   
%>

<%

   QueryListView queryListView = null;
   CorporateOrganizationWebBean corpOrg = null;
   InstrumentWebBean instrument = null;
   TemplateWebBean template = null;
   boolean fromListView = false;
   boolean fromMessages = false;
   boolean fromPayRemit = false;
   boolean multipleRelatedOrgs = false;
   boolean routeToUserIsSelected = false;
   boolean routeToOrgIsSelected = false;
   DocumentHandler doc;
   StringBuffer query = new StringBuffer();
   DocumentHandler orgListDoc = null;
   DocumentHandler userListDoc = null;
   String topLevelOrgOid = null;
   String orgDropdownOptions = null;
   String userDropdownOptions = null;
   String cancelLink = null;
   String routeSelectionErrors = null;
   String userOidSelected = null;
   String orgOidSelected = null;
   String helpSensitiveLink = null;
   String formName = null;
   int orgCount = 0;

   // this page may originate from either the transactions listview,
   // transactions detail page, message listview or the messages detail page.  
   // If originating from the transactions listview or messages listview,
   // the fromListview parameter is populated
   // If originating from messages, the fromMessages parameter is populated.
   // Summary of possible pages arriving to this page are:
   // 						  fromListView	fromMessages	FormName			CancelButtonLink
   // Transactions Listview		Y						RouteTransactionsForm	
   // Transactions Detail								RouteTransactionsForm
   // Messages Listview			Y				Y		RouteMessagesForm
   // Messages Detail							Y		RouteMessagesForm
   
   
   Debug.debug(formMgr.getFromDocCache().toString()); 	
   doc = formMgr.getFromDocCache();

   
   String fromListViewValue = doc.getAttribute("/In/Route/fromListView");
   Debug.debug("Route listview " + fromListViewValue);
   
   if (fromListViewValue != null && fromListViewValue.equals(TradePortalConstants.INDICATOR_YES))
   {
   	  fromListView = true;
   }
   
   String fromMessagesValue = doc.getAttribute("/In/Route/fromMessages");
   Debug.debug("From Message value: " + fromMessagesValue);

   String fromPayRemitValue = doc.getAttribute("/In/Route/fromPayRemit");
   Debug.debug("From PayRemit value: " + fromPayRemitValue);

   if (fromMessagesValue != null && fromMessagesValue.equals(TradePortalConstants.INDICATOR_YES))
   {
   		fromMessages = true;
   	   userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
        formName = "RouteMessagesForm";
	    if (fromListView) 
	    {
			cancelLink=formMgr.getLinkAsUrl("goToMessagesHome", response);	    	
	    }
	    else 
	    {
		    //The output section's message_oid is defined when we have a brand new
		    //  message defined (meaning the user pressed 'Create MailMessage'
		    //  That new oid needed to be passed to the RouteDeleteMessagesMediator
		    //  and the only way to really accomplish that is to move it from the 
		    //  passed in output section to the In section so that the Mediator will 
		    //  have access to it.  The outputDoc that is passed in is null, so this 
		    //  was the only viable solution.
		    String mesgOid = doc.getAttribute("/Out/message_oid");

		    if( InstrumentServices.isNotBlank( mesgOid ) ) {
		    	doc.setAttribute("/In/MailMessage/message_oid", mesgOid );
		    }


		  	// from messages detail page - should contain messageOid
		    String mailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes(
				doc.getAttribute("/In/MessageList/Message/messageOid"));
		    cancelLink = formMgr.getLinkAsUrl("goToMailMessageDetail", 
				response);
			cancelLink = cancelLink + "&mailMessageOid=" + mailMessageOid;	    	
	    }
   }
   else if (fromPayRemitValue != null && fromPayRemitValue.equals(TradePortalConstants.INDICATOR_YES))
   {
	   fromPayRemit = true;
	   userSession.setCurrentPrimaryNavigation("NavigationBar.ReceivablesManagement");
	   if (fromListView) {
		   formName = "RoutePayRemitsForm";
		   cancelLink = formMgr.getLinkAsUrl("goToReceivablesManagement", response);
	   }
	   else {
		   formName = "RoutePayRemitDetailForm";
		   cancelLink = formMgr.getLinkAsUrl("goToPayRemitMatchResponse", response);
	   }
   }
   else {
	    formName = "RouteTransactionsForm";	   
	  //Chandrakanth 11/27/2008 BR-451 Begin
	    if ("NavigationBar.CashManagement".equals(currentPrimaryNavigation)) {
	   	 	userSession.setCurrentPrimaryNavigation("NavigationBar.CashManagement");	
	    } else if ("NavigationBar.ReceivablesManagement".equals(currentPrimaryNavigation)) {     //NSX 12/07/2009 CR-509
		   	 	userSession.setCurrentPrimaryNavigation("NavigationBar.ReceivablesManagement");  //NSX 12/07/2009 CR-509
	    } else {
	    	userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");	
	    }
	  //Chandrakanth 11/27/2008 BR-451 End
	   
	    // from listview, the cancel link returns to the listview, 
	    // otherwise, it returns to the instrument close navigator page
		if (fromListView)
		{
			//Chandrakanth 11/27/2008 BR-451 Begin
			if ("NavigationBar.CashManagement".equals(currentPrimaryNavigation)) {
				cancelLink = formMgr.getLinkAsUrl("goToCashManagement", response);
			} else if ("NavigationBar.ReceivablesManagement".equals(currentPrimaryNavigation)) {    //NSX 12/07/2009 CR-509
				cancelLink = formMgr.getLinkAsUrl("goToReceivablesManagement", response);	                                                     //NSX 12/07/2009 CR-509
			}
			else {
				cancelLink=formMgr.getLinkAsUrl("goToTransactionsHome", response);	
			}
			//Chandrakanth 11/27/2008 BR-451 End
		}
        else
		{
	   	    cancelLink=formMgr.getLinkAsUrl("goToInstrumentNavigator", response);
		}
   }


   // if this page is returned due to an error, get previously selected information
   // to populate in radio button and dropdowns
   routeSelectionErrors = doc.getAttribute("/Out/Route/RouteSelectionErrors");
   if (routeSelectionErrors != null && 
       routeSelectionErrors.equals(TradePortalConstants.INDICATOR_YES))
   {
     routeToUserIsSelected = doc.getAttribute("/In/Route/routeToUser") != null &&
	 	doc.getAttribute("/In/Route/routeToUser").equals(TradePortalConstants.INDICATOR_YES);
     routeToOrgIsSelected = doc.getAttribute("/In/Route/routeToUser") != null &&
	 	doc.getAttribute("/In/Route/routeToUser").equals(TradePortalConstants.INDICATOR_NO);
	 userOidSelected = doc.getAttribute("/In/Route/userOid");
	 orgOidSelected = doc.getAttribute("/In/Route/corporateOrgOid");
	 Debug.debug("Route To Org Is Selected " + routeToOrgIsSelected );
   }
   
   Debug.debug("Cancel Link is: " + cancelLink);   
   try
   {
      // determine the rows to populate organization drop down:
      // find only ACTIVE organizations 
	 
      queryListView = (QueryListView) 
	 	EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");


      corpOrg = (CorporateOrganizationWebBean) beanMgr.createBean(
                             		    "com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean",
                             		    "CorporateOrganization");

      corpOrg.getById(userSession.getOwnerOrgOid());

      String routingSetting = corpOrg.getAttribute("route_within_hierarchy");

      // Based on corporate customer setting, determine the list of orgs that can be routed to
      if(routingSetting.equals(TradePortalConstants.ROUTE_ALL))
       {
         //  This query retrieves the list of organizations across the entire hierarchy, even "sibling" 
         //  and "cousin" organizations.
         query.append("select organization_oid, name");
	 query.append(" from corporate_org");
	 query.append(" where activation_status='" + TradePortalConstants.ACTIVE + "'");
	 query.append("  start with organization_oid =");
	 query.append("   (select organization_oid ");
	 query.append("    from corporate_org");
	 query.append("    where (p_parent_corp_org_oid is null or p_parent_corp_org_oid = '') ");
	 query.append("    start with organization_oid = ");
	 query.append(     userSession.getOwnerOrgOid());
	 query.append("    connect by prior p_parent_corp_org_oid = organization_oid) ");
	 query.append(" connect by prior organization_oid = p_parent_corp_org_oid"); 
       }
      else
       {
         // Retrieve the user's organizations and all of its children and grandchildren
	 query.append("select organization_oid, name");
	 query.append(" from corporate_org");
	 query.append(" where activation_status='" + TradePortalConstants.ACTIVE + "'");
	 query.append("  start with organization_oid =");
         query.append(     userSession.getOwnerOrgOid());
	 query.append(" connect by prior organization_oid = p_parent_corp_org_oid"); 

         // We also want the direct parent organization of the user's organization, so
         // union that in
         query.append(" union ");
	 query.append("select organization_oid, name");
	 query.append(" from corporate_org");
	 query.append(" where activation_status='" + TradePortalConstants.ACTIVE + "'");
         query.append(" and organization_oid = ");
         query.append("   (select p_parent_corp_org_oid ");
         query.append("    from corporate_org ");
         query.append("    where organization_oid = ");
         query.append(     userSession.getOwnerOrgOid());
         query.append(")");
       }

     Debug.debug("Query is : " + query);	 
	 
	 queryListView.setSQL(query.toString());
	 queryListView.getRecords();
	 
	 orgCount = queryListView.getRecordCount();
	 
	 // if there is more than one organization row returned, display the Organization dropdown list
	 // otherwise, display only the user list since this would be a standalone dropdown
	 if (orgCount > 1)
	 {
	   multipleRelatedOrgs = true;
	   
	   // create the drop down list of related organizations with 
	   // the default set to empty
       orgListDoc = queryListView.getXmlResultSet();
       orgDropdownOptions = ListBox.createOptionList(orgListDoc, "ORGANIZATION_OID", "NAME", orgOidSelected, userSession.getSecretKey());
     Debug.debug("ORG DROP DOWNS " + orgDropdownOptions);
	 }
	 
	 // select the users associated with the list of selected organizations
	 // use a different SQL for single organization and multiple related organizations
	 query.delete(0, query.length());

	 if (multipleRelatedOrgs)
	 {
	   // for multiple related organizations, append the organization name and selct
	   // the users from the list of organizations previously retrieved
	   query.append("select last_name || ', ' || first_name || ' ' || middle_initial || ");
	   query.append("' - '|| user_identifier || ' - ' || name user_descr, user_oid ");

	   query.append("from users, corporate_org where p_owner_org_oid in (");
	   
	   // append all organization oids
	   for (int i = 0; i < orgCount; i++)
	   {
	   	 queryListView.scrollToRow(i);
	   	 query.append(queryListView.getRecordValue("ORGANIZATION_OID"));
		 query.append(", ");
	   }
	   
	   query.setCharAt( query.length() -2,')');
	   query.append(" and organization_oid = p_owner_org_oid"); 
	   query.append(" and users.activation_status='" + TradePortalConstants.ACTIVE + "'"); 
	 
	 }
	 else
	 {
	   // for stand alone, select the users belonging to the single organization
	   query.append("select last_name || ', ' || first_name || ' ' || middle_initial || ");
	   query.append("' - '|| user_identifier user_descr, user_oid ");
	   query.append("from users where p_owner_org_oid = ");
	   query.append( userSession.getOwnerOrgOid());
	   query.append(" and users.activation_status='" + TradePortalConstants.ACTIVE + "'"); 
	 
	 }

	 Debug.debug("User query is: " + query.toString());		
	 // create the User drop down options with a blank default value
	 queryListView.setSQL(query.toString());
	 queryListView.getRecords();
	 userListDoc = queryListView.getXmlResultSet();
	 userDropdownOptions = ListBox.createOptionList(userListDoc, "USER_OID", "USER_DESCR", userOidSelected, userSession.getSecretKey());
     Debug.debug("USER DROP DOWNS " + userDropdownOptions);
     queryListView.remove();
   }
   catch (Exception e)
   {
     e.printStackTrace();
   }
   
   // determine context sensitive help link based on existence of multipleRelated orgs
   if (multipleRelatedOrgs)
     helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/route_with_parent.htm", "route_with_parent", resMgr, userSession);
   else
     helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/route_no_parent.htm", "route_no_parent",resMgr, userSession);

%>


<%-- check for security --%>


<%-- display the header with the navigation bar when coming from the listview, without otherwise --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
   		value="<%=fromListView?TradePortalConstants.INDICATOR_YES:TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script LANGUAGE="JavaScript">
  function pickUser(selectUser) {
    <%-- When the user drop down box has been clicked, --%>
    <%-- set the first RouteToUser to 'Y' and the second to 'N' for the radio buttons --%>
    document.RouteTransactionSelection.RouteToUser[0].checked = selectUser;
    document.RouteTransactionSelection.RouteToUser[1].checked = !selectUser;
  }

</script>
  
  <%-- Display Route Items Header --%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="21">
    <tr> 
      <td width="15" class="BankColor" nowrap height="25"><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
      <td class="BankColor" valign="middle" align="left" width="302" nowrap height="25"> 
        <p class="ControlLabelWhite"><%=resMgr.getText("RouteItem.Heading", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td class="BankColor" width="600" height="25" align="center" valign="middle"> 
        <p class="ControlLabelWhite">&nbsp;</p>
      </td>
      <td class="BankColor" width="15" height="25"><%=helpSensitiveLink%></td>
      <td width="15" class="BankColor" nowrap height="25">&nbsp;</td>
    </tr>
  </table>


<form method="post" name="RouteTransactionSelection" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>  
  <input type=hidden value='<%=fromListView?"Y":"N"%>' name=fromListView>
  <input type=hidden value='<%=fromMessages?"Y":"N"%>' name=fromMessages>
  <input type=hidden value=<%=TradePortalConstants.INDICATOR_YES%> name=routeAction>

<%= formMgr.getFormInstanceAsInputField(formName) %>
  
  
<%
if (multipleRelatedOrgs)
{
%>
  <input type="hidden" name="multipleOrgs" value="Y">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td>
        <p class="ListText"><%=resMgr.getText("RouteItem.RouteItemOrganization", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td colspan=2>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30" nowrap>&nbsp;</td>
      <td nowrap> 
	      <%=InputField.createRadioButtonField("RouteToUser", "Y", "", 
							routeToUserIsSelected, "ListText", "", false)%>

<%--        <input type="radio" name="radiobutton" value="radiobutton"> --%>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap>
        <p class="ControlLabel"><%=resMgr.getText("RouteItem.RouteItemToAPerson", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td colspan=5>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="40" nowrap>&nbsp;</td>
      <td align="left" valign="bottom" class="ListText"> 
        <p class="ListText"><%=resMgr.getText("RouteItem.RouteItemPerson", 
                              TradePortalConstants.TEXT_BUNDLE) %><br>
							<%=resMgr.getText("RouteItem.NameIDOrganization", 
                              TradePortalConstants.TEXT_BUNDLE) %><br>
          <% out.print(InputField.createSelectField("RouteUserOid", "", " ", userDropdownOptions, 
		  										   "ListText", false, "onchange=\"pickUser(true)\""));  %>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan=3>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="30" nowrap>&nbsp;</td>
      <td nowrap> 
	      <%=InputField.createRadioButtonField("RouteToUser", "N", "", 
							routeToOrgIsSelected, "ListText", "", false)%>	  
<%--        <input type="radio" name="radiobutton" value="radiobutton"> --%>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel"><%=resMgr.getText("RouteItem.RelatedOrganization", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan=5>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="40" nowrap>&nbsp;</td>
      <td align="left" valign="bottom" class="ListText"> 
        <p class="ListText"><%=resMgr.getText("RouteItem.OrganizationName", 
                              TradePortalConstants.TEXT_BUNDLE) %> <br>
		  <% out.println(InputField.createSelectField("RouteCorporateOrgOid", "", " ", orgDropdownOptions,
		  											 "ListText", false, "onchange=\"pickUser(false)\""));
		  %>
        </p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan=3>&nbsp;</td>
    </tr>
  </table>
<%
}
else
{
%>

 <input type="hidden" name="multipleOrgs" value="N">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td colspan=2>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <p class="ListText"><%=resMgr.getText("RouteItem.RouteItemPerson", 
                              TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
       <%    out.print(InputField.createSelectField("RouteUserOid", "", " ", userDropdownOptions, 
                                                  "ListText", false)); %>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan=3>&nbsp;</td>
    </tr>
  </table>
  
<%
}
%>

  <%-- Display the Route and Cancel banner --%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td class="BankColor" width="100%">&nbsp;</td>
      <td> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
            <td width="15" nowrap>&nbsp;</td>
            <td><jsp:include page="/common/RolloverButtonSubmit.jsp"  >
			      <jsp:param name="showButton" value="true" />
                  <jsp:param name="name" value="RouteButton" />
                  <jsp:param name="image" value='common.RouteItem(s)Img' />
                  <jsp:param name="text" value='common.RouteItem(s)Text' />
                  <jsp:param name="width" value="96" />
                  <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_ROUTE%>" />
			  </jsp:include><td width="15" nowrap>&nbsp;</td>
            <td><jsp:include page="/common/RolloverButtonLink.jsp">
			      <jsp:param name="showButton" value="true" />
                  <jsp:param name="name" value="CancelButton" />
                  <jsp:param name="image" value='common.CancelImg' />
                  <jsp:param name="text" value='common.CancelText' />
                  <jsp:param name="width" value="96" />
                  <jsp:param name="link" value="<%=java.net.URLEncoder.encode(cancelLink)%>" />
			  </jsp:include></td>
            <td width="20" nowrap>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</form>

</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>



