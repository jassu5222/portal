<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*" %>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>




<%

/******************************************************************************************
 * THIS JSP WILL BUILD SAVE, DELETE, CLOSE, AND HELP BUTTONS IN THAT ORDER.
 *
 * SPECIAL NOTES:
 *      ButtonPrep.jsp must be included in the jsp from which this is called for this jsp
 *          to work properly.  And it must be included prior to the point at which this jsp
 *          is called
 *      This jsp must be included INSIDE an HTML table cell
 *
 * REQUIRED PARAMETERS:
 *      boolean showSave
 *      boolean showDelete
 *      boolean showClose
 *      boolean showHelp
 *      String  cancelAction
 *      String  extraSaveTags (optional, added to the onClick attribute)
 *      String  helpLink
 ******************************************************************************************/


    boolean showSave   = false;
    boolean showDelete = false;
    boolean showClose  = false;
    boolean showHelp   = false;

    if(request.getParameter("showSaveButton").equals("true"))
        showSave = true;
    if(request.getParameter("showDeleteButton").equals("true"))
        showDelete = true;
    if(request.getParameter("showCloseButton").equals("true"))
        showClose = true;
    if(request.getParameter("showHelpButton").equals("true"))
        showHelp = true;

    String extraSaveTags = request.getParameter("extraSaveTags");
    if (extraSaveTags == null) extraSaveTags = "";

    String helpLink = request.getParameter("helpLink");
  //Validation to check Cross Site Scripting
    if(helpLink != null){
    	helpLink = StringFunction.xssCharsToHtml(helpLink);
    }



%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr valign=middle>
        <td>
<%
          if (showSave && showDelete) {
%>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="showSave" />
              <jsp:param name="name" value="SaveCloseButton" />
              <jsp:param name="image" value='common.SaveCloseImg' />
              <jsp:param name="text" value='common.SaveCloseText' />
              <jsp:param name="extraTags" value="<%=extraSaveTags%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_SAVE%>" />
            </jsp:include>
<%
          } else {
%>
            <img src="/portal/images/Blank_96.gif" width="96" height="1">
<%
          } /*end else*/
%>
        </td>
        <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
        <td>
<% 
          if (showDelete) {
%>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="showDelete" />
              <jsp:param name="name" value="DeleteButton" />
              <jsp:param name="image" value='common.DeleteImg' />
              <jsp:param name="text" value='common.DeleteText' />
              <jsp:param name="extraTags" value="return confirmDelete()" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_DELETE%>" />
            </jsp:include>
<%
          } else if (showSave) {
%>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="showSave" />
              <jsp:param name="name" value="SaveCloseButton" />
              <jsp:param name="image" value='common.SaveCloseImg' />
              <jsp:param name="text" value='common.SaveCloseText' />
              <jsp:param name="extraTags" value="<%=extraSaveTags%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_SAVE%>" />
            </jsp:include>
<%
          } else {
%>
             <img src="/portal/images/Blank_96.gif" width="96" height="1">
<% 
          } /*end else*/
%>
        </td>
        <td width="15" nowrap><img src="/portal/images/Blank_15.gif" width="15" height="15"></td>
        <td>
<%
          if (showClose) {
            String link=formMgr.getLinkAsUrl(request.getParameter("cancelAction"),
                                             response);
%>
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="showButton" value="showClose" />
              <jsp:param name="name" value="CloseButton" />
              <jsp:param name="image" value='common.CloseImg' />
              <jsp:param name="text" value='common.CloseText' />
              <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
            </jsp:include>
<%
          } else {
%>
                <img src="/portal/images/Blank_96.gif" width="96" height="1">
<%
          } /*end else*/
%>
        </td>
        <td width="15" nowrap>
          <img src="/portal/images/Blank_15.gif" width="15" height="15">
        </td>
        <td width="15" nowrap>

<%
          if (showHelp) {
		  if(helpLink == null)
			helpLink = "";
%> 
	    <%= OnlineHelp.createContextSensitiveLink(helpLink,resMgr, userSession) %>
<%
          } else {
%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15">
<%
          } /*end else*/
%>
        </td>
        <td width="15" nowrap>
          <img src="/portal/images/Blank_15.gif" width="15" height="15">
        </td>
      </tr>
    </table>
