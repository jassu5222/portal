<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.businessobjects.rebean.wi.*,java.util.*,com.ams.tradeportal.busobj.util.SecurityAccess" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
Debug.debug("***START********************STANDARD*REPORTS*PROMPT*PAGE3**************************START***");  

// first try to retrieve the session by using the WebIntelligence Cookie
// if not switch back to default page

String              	redirectURI             = null;
StringBuffer        	newLink                 = new StringBuffer();
String              	reportTab               = null;
String              	repName                 = null;
String              	cell                    = null;
String              	sAction                 = null;
//commented by Kajal Bhatt
//We do not require these variables
//WIDocument          	webiDocument            = null;
//WIOutput            	webiOutput              = null;
//WISession           	webiSession             = null;
//ends here
String              	reportAuthor            = null;
String              	userName                = null;
MediatorServices    	mediatorServices        = null;
DocumentHandler     	xmlDoc                  = null;
String              	physicalPage            = null;
DocumentInstance 	widDoc 			= null;

PromptType 		widPromptType 		= null;
int 			totalNoPages 		= 0;
int 			prevPage 		= 0;
int 			nextPage 		= 0;
String 			pageCall 		= null; 
int 			goToPage 		= 0;
PageNavigation 		pgn 			= null;
String 			pageNum 		= null;
String 			pageToken 		= null;
String 			viewMode 		= null;
Prompt 			webiPrompt		= null;
	
String  		formString		= null;;
String [] 		LOV 			= null;
String [] 		listOfValues		= null;
int 			iPromptCount 		= 0;
HTMLView		repView 		= null;
Reports 		widReports 		= null;
Report 			widReport 		= null;
String 			pageMode 		= null;
String 			locale			= null;


%>

<%@ include file="fragments/wistartpage.frag" %>

<%

//The pageToken variable is set if the request is coming from
//the same page. If the request is coming for the first time, them the
//storageToken variable is set

String storageToken = request.getParameter("storageToken");
if(request.getParameter("pageToken")!=null)
{
	pageToken = request.getParameter("pageToken");
}

//Kmajji- Rel 7.1.0.0 IR DDUK112454152 12/23/2011 Begin
String repOwn = null;
if(request.getParameter("repOwn")!=null)
{
	repOwn = request.getParameter("repOwn");
}
//Kmajji- Rel 7.1.0.0 IR DDUK112454152 12/23/2011 End


PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
pageMode = portalProperties.getString("pageMode");
String securityRights = userSession.getSecurityRights();

if( pageMode == null)
{
    pageMode = "n";

}

//Locale to set the date prompt
locale = portalProperties.getString("locale");

if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
	if (request.getParameter("pageCall")!=null)
	{
		pageCall = request.getParameter("pageCall");
	}
	else
	{
		pageCall ="N";
	}

	if (request.getParameter("goToPage")!=null) 
	{
   		pageNum = request.getParameter("goToPage");
	   	goToPage = (new Integer(pageNum).intValue());
	}
	else 
	{
   		goToPage = 1;
	}
}
else
{
	pageCall ="N";
}
try
{
	if (pageCall.equals("Y"))
	{
		//Request coming from the same page
		widDoc = reportEngine.getDocumentFromStorageToken(pageToken);
	}
	else
	{
		//Request coming for the first time
		widDoc =  reportEngine.getDocumentFromStorageToken(storageToken);
		widDoc.refresh();
	}	
}catch (REException rex) // have changed the REException class
{
     //Catch all exception
     System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}

//retrieve the report attributes
repName = widDoc.getProperties().getProperty(PropertiesType.NAME);
reportAuthor = widDoc.getProperties().getProperty(PropertiesType.AUTHOR);

// Place the reporting user suffix on to the end of the OID
// The suffix is used to distinguish between organizations being processed
// by different ASPs that might be on the same reports server.  

userName = userSession.getOwnerOrgOid()+userSession.getReportingUserSuffix();

Debug.debug("storageToken in Standard Reports Prompt page 3 #########"+storageToken);
Debug.debug("repName in Standard Reports Prompt page 3 #########"+repName);
Debug.debug("report Author in Standard Reports Prompt page 3 #########"+reportAuthor);
Debug.debug("user name in Standard Reports Prompt page 3 #########"+userName);
Debug.debug("Inside Standard prompt page #######");
        
if (pageCall.equals("N"))
{ 
	try
	{
		Prompts  webiPromptList = widDoc.getPrompts();
		iPromptCount = webiPromptList.getCount();
		for (int i=0;i<iPromptCount;i++)
		{
			webiPrompt = webiPromptList.getItem(i);
			// the Request.Form contains the values selected in previous page
			// it can contained multiple values for a single variable in a string separated by commas
			formString = "ListOfValues"+i;
			widPromptType = webiPrompt.getType();
			if (webiPrompt.getType() == PromptType.Mono)
			{
				//case for an input text
				String  repPrompt = request.getParameter(formString);
				StringTokenizer strTokenizer = new StringTokenizer(repPrompt, ";");
				int iValueCount = strTokenizer.countTokens();
				String[] astrValues = new String[iValueCount];
				PromptType pt = webiPrompt.getType();
				int iValueIndex = 0;
				for (iValueIndex = 0; iValueIndex < iValueCount; iValueIndex++)
				{
					astrValues[iValueIndex] = strTokenizer.nextToken();
					//Date parsing logic				
					if (webiPrompt.getObjectType() == ObjectType.DATE)
					{
						if(locale.equals("en_GB"))
						{ 
							String  retDt = astrValues[iValueIndex];
							int intFirstVal = retDt.indexOf("/");
							String dtFirsrVal = retDt.substring(0,intFirstVal);
							int intSecVal = retDt.lastIndexOf("/");
							String dtSecVal = retDt.substring(intFirstVal+1,intSecVal);
							int dtlen = retDt.length();
							String dtThirdVal = retDt.substring(intSecVal+1,dtlen);
							if((intSecVal == 5 ) && (intFirstVal == 2) )
							{
								astrValues[iValueIndex] = dtSecVal + "/"+ dtFirsrVal + "/" + dtThirdVal;
							}	
						}
					}
					webiPrompt.enterValues(astrValues);
				}
			}
			else
			{

				LOV = request.getParameterValues(formString);
				webiPrompt.enterValues(LOV);
			}
		}

		widDoc.setPrompts();
		widDoc.applyFormat();

		storageToken = widDoc.getStorageToken();
		pageToken = widDoc.getStorageToken();
		
	}
	catch (REException rex) // have changed the REException class
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page1 ######################"+rex.getErrorCode()+" "+rex.getMessage());
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("StandardReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}
	catch (IndexOutOfBoundsException ex) // Check for null dates
	{
		 //Catch all exception
		 System.out.println("EXCEPTION in Standard Reports Prompt Page3 ######################");
		 xmlDoc = formMgr.getFromDocCache();
		 mediatorServices = new MediatorServices();
		 mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
		 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_INVALID_DATE_ERROR);
		 mediatorServices.addErrorInfo();
		 xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
		 xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
		 formMgr.storeInDocCache("default.doc", xmlDoc);
		 physicalPage = NavigationManager.getNavMan().getPhysicalPage("StandardReportPrompt1", request);
		 %>
		 <jsp:forward page='<%= physicalPage %>' />
		 <%
	}	
}
              
//Retrieve the reports
widReports = widDoc.getReports();
widReport = widReports.getItem(0);
          
//Set the page navigation mode to page mode
widReport.setPaginationMode(PaginationMode.QuickDisplay);
pgn = widReport.getPageNavigation();
           
//Set the report navigation to the last page for computing total number of pages
pgn.last();
totalNoPages = pgn.getCurrent();
if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
{
    pgn.setTo(goToPage);
    repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
}
%>

<%--*******************************HTML for page begins here **************--%>
<%-- Begin HTML header decleration --%>    
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- End HTML header decleration --%>    

<script LANGUAGE="JavaScript">
function confirmDelete() 
{
	// Display a popup asking if the user wants to delete.  If OK is pressed,
	// true is returned.  If Cancel is pressed, false it returned.
	var msg = "<%=resMgr.getText("confirm.delete",TradePortalConstants.TEXT_BUNDLE)%>"
	if (!confirm(msg))
	{
		formSubmitted = false;
		return false;
	}
	return true;
}

function openURL(url) 
{
	widgets = 'toolbar=yes,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes';
	openWin = window.open(url, 'ViewPDF', widgets);
	openWin.focus();
}

function openURLXLS(url) 
{
	widgets = 'toolbar=yes,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes';
	openWin = window.open(url, 'ViewPDF', widgets);
	openWin.focus();
}
 </script>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="" bgcolor="#FFFFFF">
<form method="post" action="../FullHTML/Replace"100%" border="0" cellspacing="0" cellpadding="0" height="34">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" nowrap height="35"> 
        <p><span class="Links"><a href="#" class="Links"><span class="Links"><%=resMgr.getText("StandardReports.TabName", TradePortalConstants.TEXT_BUNDLE) %> </span></a></span> <span class="ControlLabelWhite">&gt; <%=repName%></span></p>
      </td>
      <td width="90%" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <%
      newLink = new StringBuffer();
      newLink.append(formMgr.getLinkAsUrl("goToEditReportStep3", response));
      newLink.append("&storageToken=" + storageToken);
      newLink.append("&reportMode=" + TradePortalConstants.REPORT_COPY_STANDARD);
      %>
      <td class="BankColor">  
        <jsp:include page="/common/RolloverButtonLink.jsp">
        <jsp:param name="name"  value="CreateNewReportButton" />
        <jsp:param name="image" value="common.CreateCopyofReportImg" />
        <jsp:param name="text"  value="common.CreateCopyofReportText" />
        <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
        <jsp:param name="width" value="230" />
        </jsp:include>
      </td>
      <td class="BankColor" width="15" nowrap >&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" height="35" nowrap>
       <%= OnlineHelp.createContextSensitiveLink("customer/report.htm",resMgr,userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="45" class="ColorGrey">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      <%
         //Download report button
         newLink = new StringBuffer();
		 newLink.append(request.getContextPath());
         // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
         newLink.append("/common/TradePortalDownloadServlet?");
         newLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
         newLink.append("=");
         newLink.append(TradePortalConstants.REPORT_DOWNLOAD);
         newLink.append("&storageToken=" + pageToken);
      %>
      <td>
        <jsp:include page="/common/RolloverButtonLink.jsp">
          <jsp:param name="name"  value="DownloadReport" />
          <jsp:param name="image" value="common.ReportDownloadImg" />
          <jsp:param name="text"  value="common.ReportDownloadText" />
          <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
          <jsp:param name="width" value="96" />
        </jsp:include>
      </td>  
      <td width="15" nowrap >&nbsp;</td>
      <%
    //Narayan- Rel 7.0.0.0 IR DEUL070782979 07/12/2011 Begin
    // User has maintain rights then only 'Delete' and 'Edit Report Design' Button should be display
     if (SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT)){
      //Kmajji- Rel 7.1.0.0 IR DDUK112454152 12/23/2011 added repOwn
	  if (userName.equals(repOwn))
      {    
			Debug.debug("userName in custom report page#######################"+userName);
			Debug.debug("reportAuthor in custom report page#######################"+reportAuthor);
            
			//Edit reports design method
			newLink = new StringBuffer();
			newLink.append(formMgr.getLinkAsUrl("goToNewReportStep1", response));      
			newLink.append("&storageToken=" + pageToken);
			newLink.append("&reportMode=" + TradePortalConstants.REPORT_EDIT_STANDARD);
                   
            %>
            <td >  
                <jsp:include page="/common/RolloverButtonLink.jsp">
                <jsp:param name="name"  value="EditReportDesign" />
                <jsp:param name="image" value="CustomReports.EditReportImgGrey" />
                <jsp:param name="text"  value="CustomReports.EditReportText" />
                <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
                <jsp:param name="width" value="140" />
                </jsp:include>
            </td>
            <td width="15" nowrap >&nbsp;</td>
			<%
			}
			%>
			<%
			//Delete Reports
			//Kmajji- Rel 7.1.0.0 IR DDUK112454152 12/23/2011 added repOwn
			if (userName.equals(repOwn))
			{
				reportTab = TradePortalConstants.STANDARD_REPORTS;
				newLink = new StringBuffer();
				newLink.append(formMgr.getLinkAsUrl("goToReportsDelete", response));
				newLink.append("&storageToken=" + pageToken);
				newLink.append("&reportTab=" + reportTab);
				%>
				<td>
					<jsp:include page="/common/RolloverButtonLink.jsp">
					<jsp:param name="name"  value="Close" />
					<jsp:param name="image" value="common.DeleteImg" />
					<jsp:param name="text"  value="common.DeleteText" />
					<jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
					<jsp:param name="extraTags" value="return confirmDelete()" />
					<jsp:param name="width" value="97" />
					</jsp:include>
				</td>
				<td width="15" nowrap>&nbsp;</td>
				<%
			}
		 } //Narayan- Rel 7.0.0.0 IR DEUL070782979 07/12/2011 End
			%>
			<%
			  newLink = new StringBuffer();
			  newLink.append("javascript:openURL('");
			  newLink.append("/portal/reports/viewPDF.jsp?");
			  newLink.append("&storageToken=" + pageToken);
			  newLink.append("')");
			%>
				<td>
					<jsp:include page="/common/RolloverButtonLink.jsp">
					<jsp:param name="name"  value="ViewPDF" />
					<jsp:param name="image" value="Reports.ViewPDF" />
					<jsp:param name="text"  value="Reports.ViewPDFText" />
					<jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
					<jsp:param name="width" value="96" />
					</jsp:include>
				</td>
				<td width="20" nowrap>&nbsp;</td>
              
              <%
				newLink = new StringBuffer();
				newLink.append("javascript:openURLXLS('");
				newLink.append("/portal/reports/viewEXCEL.jsp?");
				newLink.append("&storageToken=" + pageToken);
				newLink.append("')");
				%>
			<td>
	          <jsp:include page="/common/RolloverButtonLink.jsp">
	          <jsp:param name="name"  value="ViewPDF" />
	          <jsp:param name="image" value="Reports.ViewXLS" />
	          <jsp:param name="text"  value="Reports.ViewXLSText" />
	          <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
	          <jsp:param name="width" value="96" />
	          </jsp:include>
	      </td>
	      <td width="20" nowrap>&nbsp;</td>
      <%
      newLink = new StringBuffer();
      newLink.append(formMgr.getLinkAsUrl("goToReportsHome", response));
      %>
            
      <td>
        <jsp:include page="/common/RolloverButtonLink.jsp">
        <jsp:param name="name"  value="Close" />
        <jsp:param name="image" value="Reports.CloseGreyImg" />
        <jsp:param name="text"  value="Reports.CloseText" />
        <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
        <jsp:param name="width" value="96" />
        </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
 
<%
	if(pageMode.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
	{
		//Retrieve the reports in HTML format and print the body
		if(pgn.getCurrent()==1)
		{
			prevPage = 1;
		}
		else
		{
			prevPage = pgn.getCurrent()-1;
		}


		if(pgn.getCurrent() == totalNoPages)
		{
		  nextPage = totalNoPages ;
		}
		else
		{
		  nextPage = pgn.getCurrent()+1;
		}

		if (totalNoPages != 1)
		{
%>

			<Table align="center" >
				<TR align="center">
					<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=1&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">First Page</a></TD>
					<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=prevPage%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Previous Page</a></TD>
					<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=nextPage%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Next Page</a></TD>
					<TD><a class="ListHeaderText" href="/portal/reports/StandardReportPrompt3.jsp?goToPage=<%=totalNoPages%>&pageCall=Y&pageToken=<%=pageToken%>&storageToken=<%=storageToken%>">Last Page</a></TD>
				</TR>
			</Table>
<%
		}
%>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td width="20" nowrap>&nbsp;</td>
	      <td width="100%" class="ListText"> 
	      </td>
	      <td width="20" nowrap>&nbsp;</td>
	    </tr>
	</table>
	<%
	repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 
	
	if(repView!=null)
	{
			
		String strHeight = "height:50%";
		String strWidth = "width:100%";
		String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
		String strDivEnd = "\">";
		String strHead = repView.getStringPart("head",false);
		out.println(strHead);
		String strBody = repView.getStringPart("body",false);
		int iDiv1 = strBody.indexOf("<div");
		int iDiv2 = strBody.indexOf("<div",iDiv1+1);
		String strDiv = strBody.substring(iDiv1,iDiv2);
		StringTokenizer stk = new StringTokenizer(strDiv,";");

		while(stk.hasMoreTokens())
		{
			String strToken = stk.nextToken();
			if(strToken.startsWith("width"))
			{
				strWidth = strToken;
			}
			else if(strToken.startsWith("height"))
			{
				strHeight = strToken;
			}
		}
		while(strBody.indexOf("overflow:hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
		}

		while(strBody.indexOf("overflow: hidden;")>-1){
			int istrBodylength = strBody.length();
			int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");
			String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);
			String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);
			strBody = strBodyPart1 + strBodyPart2;
		}
		
		while(strBody.indexOf("ro:style")>-1)
		{
			strBody = strBody.replaceAll("ro:style","style");
		}
		if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
			strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
		}

		
		
     %>
	 <DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  
    <%
	out.println(strBody);
	%>
	</DIV>
	<%
		out.flush();
	}
	%>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td>&nbsp;</td>
	    </tr>
	  </table>
	<%
	}
	else
	{

		for(int pgcount = 1;pgcount<=totalNoPages;pgcount++){
			pgn.setTo(pgcount);
			repView = (HTMLView) widReport.getView(OutputFormatType.HTML); 

			String strHeight = "height:50%";
			String strWidth = "width:100%";
			String strDivStart = "<DIV id = WebiContent style=\"postion:relative";
			String strDivEnd = "\">";
			String strHead = repView.getStringPart("head",false);
			out.println(strHead);
			String strBody = repView.getStringPart("body",false);
			int iDiv1 = strBody.indexOf("<div");
			int iDiv2 = strBody.indexOf("<div",iDiv1+1);
			String strDiv = strBody.substring(iDiv1,iDiv2);
			StringTokenizer stk = new StringTokenizer(strDiv,";");

			while(stk.hasMoreTokens())
			{
				String strToken = stk.nextToken();

				if(strToken.startsWith("width"))
				{
					strWidth = strToken;
				}
				else if(strToken.startsWith("height"))
				{
					strHeight = strToken;
				}
			}


			while(strBody.indexOf("overflow:hidden;")>-1){
		 		
		 				 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow:hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+16 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;
				
		
 			}


			while(strBody.indexOf("overflow: hidden;")>-1){
		 		
		 				 		
				int istrBodylength = strBody.length();
				int istrBodybeginOverFlow = strBody.indexOf("overflow: hidden;");

				String strBodyPart1 = strBody.substring(0,istrBodybeginOverFlow-1);

				String strBodyPart2 = strBody.substring(istrBodybeginOverFlow+17 ,istrBodylength);

				strBody = strBodyPart1 + strBodyPart2;
				
		
 			}

			while(strBody.indexOf("ro:style")>-1)
			{
				strBody = strBody.replaceAll("ro:style","style");
			}
			
		       if(strBody.indexOf("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;")> -1){			
				strBody = strBody.replace("<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:124px;","<div class=\"s1 ovh\" style=\"position: absolute;left:76px;top:48px;");
			}



		       %>
		       <DIV ID="webIContent" style=" position:relative;<%=strWidth%>;<%=strHeight%>">  



		       <%

			out.println(strBody);


			%>

			</DIV>

			<%

			out.flush();


		}

	}
%>

 	
  

</form>

<%--cquinton 3/16/2012 Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton 3/16/2012 Portal Refresh - end--%>

</body>

</html>
<%


//Garbage Collection

mediatorServices        = null;
xmlDoc                  = null;
widDoc 			= null;
widPromptType 		= null;
pgn 			= null;
webiPrompt		= null;
repView 		= null;
widReports 		= null;
widReport 		= null;

%>
<%
Debug.debug("***END********************STANDARD*REPORTS*PROMPT*PAGE3**************************END***"); 
%>