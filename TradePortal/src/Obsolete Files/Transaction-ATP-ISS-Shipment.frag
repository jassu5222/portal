<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                     Approval to Pay Issue Page - Shipment section

  Description:
    Contains HTML to create the Approval to Pay Issue Shipment section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-ATP-ISS-Shipment.jsp" %>
*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("ApprovalToPayIssue.Shipment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="Shipment"></a>
        </p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="right" valign="middle" width="8" height="17">&nbsp;</td>
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
                <a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
                </a>
            </td>
            <td align="left" valign="middle" width="8" height="17">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class=BankColor align=right valign=middle width=15 height=35 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/issue_approval_to_pay.htm", "shipment", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width=20 nowrap>&nbsp;</td>
      <td class="ListText" nowrap> 
        <p class="ListText">
          <%=InputField.createCheckboxField("TranshipAllowed",
                           TradePortalConstants.INDICATOR_YES, "", 
                           terms.getAttribute("transshipment_allowed").
                                   equals(TradePortalConstants.INDICATOR_YES),
                           "ListText", "", isReadOnly || isFromExpress, isFromExpress, "")%>
          &nbsp;
          <%=resMgr.getText("ApprovalToPayIssue.TranshipAllowed", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createCheckboxField("PartialAllowed",
                           TradePortalConstants.INDICATOR_YES, "", 
                           terms.getAttribute("partial_shipment_allowed").
                                   equals(TradePortalConstants.INDICATOR_YES),
                           "ListText", "", isReadOnly || isFromExpress, isFromExpress, "")%>
          &nbsp;
          <%=resMgr.getText("ApprovalToPayIssue.PartialAllowed", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
        </p>

      </td>
      <td width=20 nowrap>&nbsp;</td>
      <td class=ControlLabel align=left nowrap> 
          <%=resMgr.getText("ApprovalToPayIssue.LastestShipDate", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <span class="ListText">
<%
            if (isReadOnly) {
              out.println(TPDateTimeUtility.formatDate(
                                  terms.getAttribute("latest_shipment_date"),
                                                     TPDateTimeUtility.LONG,
                                                     loginLocale));
            } else {
%> 
              <%=TPDateTimeUtility.getDayDropDown("ShipmentDay", shipDay,
                    resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
              <%=TPDateTimeUtility.getMonthDropDown("ShipmentMonth", 
                    shipMonth, loginLocale, 
                    resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
              <%=TPDateTimeUtility.getYearDropDown("ShipmentYear", shipYear,
                    resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
<%
          }
%>
          </span>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="ControlLabel" align="left" valign="bottom"> 
          <%=resMgr.getText("ApprovalToPayIssue.ShippingTerm", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
	<span class=ListText>
<%
          options = Dropdown.createSortedRefDataOptions( TradePortalConstants.INCOTERM,
                                 terms.getAttribute("incoterm"),
                                 loginLocale);
          defaultText = " ";
          out.println(InputField.createSelectField("Incoterm", "",
                               defaultText, options, "ListText", 
                               isReadOnly || isFromExpress));
%>
	</span>
      </td>
      <td>&nbsp;</td>
      <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <%=resMgr.getText("ApprovalToPayIssue.ShipTermLocation", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createTextField("IncotermLocation",
                                        terms.getAttribute("incoterm_location"),
                                        "30", "30", "ListText", 
                                        isReadOnly || isFromExpress)%>
      </td>
      <td height="55">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr> 
          <td width=20 nowrap>&nbsp;</td>
      <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("ApprovalToPayIssue.From", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="ListText">
            <%=resMgr.getText("ApprovalToPayIssue.FromPort", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createTextField("ShipFromPort",
                                        terms.getAttribute("shipment_from"),
                                        "65", "65", "ListText", 
                                        isReadOnly || isFromExpress)%>
      </td>
      <td class="ListText" align="left" valign="bottom" nowrap> 
      <p class="ListText">
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <span class="ControlLabel">
            <%=resMgr.getText("ApprovalToPayIssue.To", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <%=resMgr.getText("ApprovalToPayIssue.ToDischarge", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createTextField("ShipToDischarge",
                                        terms.getAttribute("shipment_to_discharge"),
                                        "65", "65", "ListText", 
                                        isReadOnly || isFromExpress)%>
        </p>
      </td>
         <td height="55">&nbsp;</td>
    </tr>
    <tr> 
            <td width=20 nowrap>&nbsp;</td>
      <td class="ControlLabel" align="left" valign="bottom" nowrap> 
      <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("ApprovalToPayIssue.From", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <span class="ListText">
            <%=resMgr.getText("ApprovalToPayIssue.FromLoad", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createTextField("ShipFromLoad",
                                        terms.getAttribute("shipment_from_loading"),
                                        "65", "65", "ListText", 
                                        isReadOnly || isFromExpress)%>
      </td>
      <td class="ListText" align="left" valign="bottom" nowrap> 
        <p class="ListText">
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <span class="ControlLabel">
            <%=resMgr.getText("ApprovalToPayIssue.To", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <%=resMgr.getText("ApprovalToPayIssue.ToPort", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=getExpressIndicator(isExpressTemplate)%>
          <br>
          <%=InputField.createTextField("ShipToPort",
                                        terms.getAttribute("shipment_to"),
                                        "65", "65", "ListText", 
                                        isReadOnly || isFromExpress)%>
        </p>
      </td>
          <td height="55">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom" colspan="3">
        <%=resMgr.getText("ApprovalToPayIssue.PresentDocs", 
                          TradePortalConstants.TEXT_BUNDLE)%>
        <%=InputField.createTextField("PresentDocsDays",
                                      terms.getAttribute("present_docs_within_days"),
                                      "3", "3", "ListText", isReadOnly)%>
        <%=resMgr.getText("ApprovalToPayIssue.DayOfShipment", 
                          TradePortalConstants.TEXT_BUNDLE)%>
      </td>
      <td height="40">&nbsp;</td>
    </tr>
  </table>

  <br>

   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">
    <tr align="left" valign="middle"> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="BodyText" width="100%"> 
        <p class="ControlLabel">
          <%=resMgr.getText("ApprovalToPayIssue.GoodsDesc", TradePortalConstants.TEXT_BUNDLE)%>
          <%=getRequiredIndicator(!isTemplate)%>
        </p>
      </td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ControlLabel">
          <%=resMgr.getText("ApprovalToPayIssue.GoodsText", TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom"> 
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.AddAPhrase", TradePortalConstants.TEXT_BUNDLE);

              out.println(InputField.createSelectField("GoodsPhraseItem", "", defaultText, options, "ListText", 
                          isReadOnly, 
                          "onChange=" + PhraseUtility.getPhraseChange("/Terms/goods_description", "GoodsDescText", 
                                                                      "6500")));
        %>
              <jsp:include page="/common/PhraseLookupButton.jsp">
                 <jsp:param name="imageName" value='GoodDescButton' />
              </jsp:include>
        <%
           }
        %>
        <br>
	</td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr class=FixedSize valign=top>
      <td width="20" nowrap>&nbsp;</td>
      <td class="FixedSize" width="25" nowrap>&nbsp;</td>
	<td class="FixedSize" valign=top>
        <%=InputField.createTextArea("GoodsDescText", terms.getAttribute("goods_description"), "65", "3", "FixedSize", 
                                     isReadOnly)%>
        <%
           userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();

           userCustomerAccessIndicator  = userSession.getCustomerAccessIndicator();
           transactionOid               = transaction.getAttribute("transaction_oid");

           if (((userOrgAutoATPCreateIndicator != null) && 
                (userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && 
               ((SecurityAccess.hasRights(loginRights, SecurityAccess.APPROVAL_TO_PAY_PROCESS_PO)) ||
               ((userCustomerAccessIndicator != null) && 
                (userCustomerAccessIndicator.equals(TradePortalConstants.INDICATOR_YES)))))
           {
              // Compose the SQL to retrieve PO Line Items that are currently assigned to 
              // this transaction
              sqlQuery = new StringBuffer();
              sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency");
              sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
              //jgadela  R90 IR T36000026319 - SQL FIX
			  Object sqlParamsT[] = new Object[1];
			  sqlParamsT[0] = transactionOid;

              // Retrieve any PO line items that satisfy the SQL query that was just constructed
              poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParamsT);

              if (poLineItemsDoc != null)
              {
                 hasPOLineItems = true;

                 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
                 poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
                 poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
                 poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/BEN_NAME");
                 poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
              }

        %>
      </td>
      <td width="100%">
        <p>&nbsp;</p>
      </td>
    </tr>
  </table>
  <table height=30 border="0" cellspacing="0" cellpadding="0">
	  <%
              if (!hasPOLineItems && (!(isReadOnly || isFromExpress)))
              {
        %>
    <tr valign=top>
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
	  <td class="ListText">

                 <table height=60 width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr align="right" align="bottom"> 
                     <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name" value="AddPOLineItemsButton" />
                                <jsp:param name="image" value='common.AddPOLineItemsImg' />
                                <jsp:param name="text" value='common.AddPOLineItemsText' />
                                <jsp:param name="submitButton"
                                           value="AddPOLineItemsButton" />
                                <jsp:param name="imageName" 
                                           value='AddPOLineItemsButton' />
                              </jsp:include>
                     </td>
                   </tr>
                 </table>

      </td>
      <td width="100%">
        <p>&nbsp;</p>
      </td>
    </tr>
        <%
              }
           }
        %>
    <tr> 
      <%-- This spacer row ensures readonly mode looks good --%>
      <td width="20" height=1 nowrap></td>
      <td class="ListText" width="25" nowrap></td>
      <td class="ListText"> 
        <img src="/portal/images/Blank_517.gif" width="517" height="1">
      </td>
      <td width="100%"></td>
    </tr>
  </table>
  <%
     // It is possible that after a transaction has been processed by bank that it has
     // no POs assigned to it because these POs have been moved to amendments.  However,
     // we still want to display the PO information in the PO line items field.
     if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK) &&
           !InstrumentServices.isBlank(terms.getAttribute("po_line_items")))
               hasPOLineItems = true;

     if (((userOrgAutoLCCreateIndicator != null) && 
          (userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && hasPOLineItems)
     {
  %>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="20" nowrap>&nbsp;</td>
            <td nowrap width="15">&nbsp; </td>
            <td>&nbsp;</td>
            <td nowrap class="ListText" colspan="5"> 
              <p class="ControlLabel">
                <%=resMgr.getText("ApprovalToPayIssue.POLineItems", TradePortalConstants.TEXT_BUNDLE)%>
              </p>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td class="ListText">&nbsp; </td>
            <td width="100%">&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr valign=top> 
            <td width="20" nowrap>&nbsp;</td>
            <td class="ListText" width="25" nowrap>&nbsp;</td>
            <td class="FixedSize" align="left" valign="top"> 
             <%--  Suresh Penke IR-MJUL032264649 08/11/2011 added oncontextmenu to disable mouse right click --%>
              <%=InputField.createTextArea("POLineItems", terms.getAttribute("po_line_items"), "65", "6", 
                                           "FixedSize", isReadOnly, true, "oncontextmenu='return false;' onselectstart='return false;'")%>
             <%--  Suresh Penke IR-MJUL032264649 08/11/2011 End  --%>
            </td>
            <td width="100%"> 
              <p>&nbsp;</p>
            </td>
          </tr>
	    <tr>
		<td class=ListText>
		  &nbsp;
		</td>
	    </tr>
              <%
                 if ((SecurityAccess.hasRights(loginRights, SecurityAccess.APPROVAL_TO_PAY_PROCESS_PO)) && 
                     (!(isReadOnly || isFromExpress)))
                 {
                    secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
                    secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
                    secureParms.put("addPO-currency", poLineItemCurrency);
              %>
          <tr valign=bottom> 
            <td width="20" nowrap>&nbsp;</td>
            <td class="ListText" width="25" nowrap>&nbsp;</td>
            <td class="ListText" align="left" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="right">
                        <td width="100%">&nbsp;</td>
                        <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name" value="AddPOLineItemsButton" />
                                <jsp:param name="image" value='common.AddPOLineItemsImg' />
                                <jsp:param name="text" value='common.AddPOLineItemsText' />
                                <jsp:param name="submitButton"
                                           value="AddPOLineItemsButton" />
                                <jsp:param name="imageName" 
                                           value='AddPOLineItemsButton' />
                              </jsp:include>
                        </td>
                        <td width="10" nowrap>&nbsp;</td>
                        <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name"  value="RemovePOLineItemsButton" />
                                <jsp:param name="image" value='common.RemovePOLineItemsImg' />
                                <jsp:param name="text"  value='common.RemovePOLineItemsText' />
                                <jsp:param name="submitButton"
                                           value="RemovePOLineItemsButton" />
                                <jsp:param name="imageName" 
                                           value='RemovePOLineItemsButton' />
                              </jsp:include>
                        </td>
                      </tr>
                    </table>
            </td>
            <td width="100%"> 
              <p>&nbsp;</p>
            </td>
          </tr>
              <%
                 }
              %>
        </table>
  <%
     }
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
