<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                           Transaction Page - Add/Modify Party
  Description:
	<insert description>

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-AddModifyParty.frag" %>
*******************************************************************************
--%>

<%
String deletePayee = TradePortalConstants.BUTTON_DELETE_PAYEE;
String modifyPayee = TradePortalConstants.BUTTON_MODIFY_PAYEE;

String addPayeeAction;
if (isTemplate)
{

	addPayeeAction = TradePortalConstants.BUTTON_ADD_PAYEE;
}
else
{
	//addPayeeAction = TradePortalConstants.BUTTON_SAVETRANS; 
	 addPayeeAction = TradePortalConstants.BUTTON_ADDSAVETRANS;
}
%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="32">
      <tr>
        <td width="45" class="BankColor" nowrap>&nbsp;
<% 
if (radioSelectSection)
{
%>        
        <img src="/portal/images/UpIndicatorArrow.gif" width="37">
<%
}
else
{
%>
		<img src="/portal/images/Blank_15.gif" width="37" height="15"> 
<%
}
String mainPartyType = "Beneficiary";
if (TradePortalConstants.DIRECT_DEBIT_INSTRUCTION.equals(instrumentType))
	mainPartyType = "Payer";
String deleteImg = "common.Delete" + mainPartyType +"Img";
String deleteText = "common.Delete" + mainPartyType;
String addUpdateImg = "common.AddUpdate" + mainPartyType +"Img";
String addUpdateText = "common.AddUpdate" + mainPartyType;
%>
        </td>

        <td width="60%" class="BankColor" height="32">
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="name" value="DeletePayee" />
              <jsp:param name="image" value='<%=deleteImg%>' />
              <jsp:param name="text" value='<%=deleteText%>' />
              <jsp:param name="extraTags" value="setButtonPressed('Delete', 0); return confirmDelete()" />
              <jsp:param name="submitButton"
                         value="<%=deletePayee%>" />
            </jsp:include>
            &nbsp;&nbsp;&nbsp;
<%
//reserved for possible future use
if (!payeeAutoReload)
{
%>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="name" value="ModifyButton" />
              <jsp:param name="image" value='common.EditDataGreyImg' />
              <jsp:param name="text" value='common.SaveText' />
              <jsp:param name="submitButton"
                         value="<%=modifyPayee%>" />
            </jsp:include>
<%
}
%>
        </td> 
        <td width="55%" class="BankColor" height="34" nowrap align=right>
<%
	//VShah - CR564
	if (!(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))) 
	{
	//VShah - CR564
%>	     
            
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="name" value="AddUpdatePayee" />
              <jsp:param name="image" value='<%=addUpdateImg%>' />
              <jsp:param name="text" value='<%=addUpdateText%>' />
              <jsp:param name="submitButton"
                         value="<%=addPayeeAction%>" />
            </jsp:include>
<%
	 } //VShah - CR564
%>        
        </td>

        <td width="45" class="BankColor" nowrap>&nbsp;
        </td>        
      </tr>
    </table>