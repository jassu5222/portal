<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Domestic Payment Request Page - Debit/Credit Details section

  Description:
    Contains HTML to create Domestic Payment Request Debit/Credit Details section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-FTDP-ISS-CreditDebit.frag" %>
*******************************************************************************
--%>

<%
	// Define Variables
	DocumentHandler enteredDomPmtDoc = null;
	QueryListView queryListView = null;
	StringBuffer updateText = null;
	StringBuffer sqlQuery = new StringBuffer();
	boolean errorFlag = false;
	String corporateOrgOid = null;
	Vector enteredDomPmtList = null;
	String classAttribute = null;
	String defaultValue = null;
	String userLocale = null;
	int numberOfDomPmts = 0;
	String dropdownOptions = null;
	String fundingCurrency = "";
	String displayPaymentAmount = "0.00";
	String displayFundingAmount = "0.00";

	corporateOrgOid = userSession.getOwnerOrgOid();

	// IAZ: Retrive all domestic payments to be displayed as a table at the bottom 
	// of the screen

	boolean sqlPayeeError = false;
	try {
		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(),"QueryListView");

		sqlQuery.append("select dp.domestic_payment_oid, dp.payee_account_number as payee_account, ");
		sqlQuery.append("dp.payee_name as payee_name, dp.amount as payee_amount, ");
		sqlQuery.append("dp.amount_currency_code as currency_code, ");
		sqlQuery.append("dp.payee_bank_code, dp.payee_bank_name, dp.payee_description ");
		sqlQuery.append(", dp.payment_method_type ");
		sqlQuery.append(", dp.payee_address_line_1, dp.payee_address_line_2, dp.payee_address_line_3, dp.payee_address_line_4, dp.country ");
		sqlQuery.append(", dp.value_date, customer_reference ");											//IAZ CR-509 01/18/10, 02/03/10 Add
		sqlQuery.append("from domestic_payment dp ");
		sqlQuery.append("where dp.p_transaction_oid = ");
		sqlQuery.append(transaction.getAttribute("transaction_oid"));
		sqlQuery.append(" order by payee_account");

		queryListView.setSQL(sqlQuery.toString());
		queryListView.getRecords();
		numberOfDomPmts = queryListView.getRecordCount();
		DocumentHandler enteredDomPmtsDoc = queryListView.getXmlResultSet();
		enteredDomPmtList = enteredDomPmtsDoc.getFragments("/ResultSetRecord");

	} catch (Exception e) {
		sqlPayeeError = true;
		e.printStackTrace();
	}
	finally {
		try {
			if (queryListView != null) {
				queryListView.remove();
			}
		} catch (Exception e) {
			System.out.println("Error removing QueryListView in -CreditDebitDetails.frag!");

		}
	}
	//End Retrive all domestic payments list 

	//make sure to load a Payee to a Credit section upon successfull Verify so screen 
	//does not appear empty
	if ((numberOfDomPmts >= 1) && (isReadOnly)) {
		if (InstrumentServices.isBlank(currentDPOid)) {
			enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList
					.elementAt(0);
			currentDPOid = enteredDomPmtDoc
					.getAttribute("/DOMESTIC_PAYMENT_OID");
			currentDomesticPayment.setAttribute("domestic_payment_oid",
					currentDPOid);
			currentDomesticPayment.getDataFromAppServer();
		}
	}

	// IAZ: We retrive this amount to handle "corner cases"
	//                  such as no domestic payments are entered
	//                  However, this value will be recalculated to
	//                  be equal the sum of all dom pmt amounts

	String fundingAmount = terms.getAttribute("funding_amount");
	if ((fundingAmount == null) || (fundingAmount.equals(""))) {
		fundingAmount = "0";
	}

	// Always send the terms party oids
	secureParms.put("PayerParty_terms_party_oid", payerParty
			.getAttribute("terms_party_oid"));
	secureParms.put("ApplicantParty_terms_party_oid", applicantParty
			.getAttribute("terms_party_oid"));

	//IAZ: get payee accout number from current payee, if loaded     
	payeeAcctNum = currentDomesticPayment
			.getAttribute("payee_account_number");
	if (payeeAcctNum == null) {
		payeeAcctNum = "";
	}

	String bankChargesType = currentDomesticPayment
			.getAttribute("bank_charges_type");

	//Calculate amounts and determine accounts/currencies to be used later

	// Using the acct_choices xml from the payer party,build the dropdown and 
	// select the one matching debit_account_oid
	
	acctOid = StringFunction.xssHtmlToChars(terms.getAttribute("credit_account_oid"));

	Vector availabilityChecks = new Vector(2);
	availabilityChecks.add("!deactivate_indicator");
	availabilityChecks.add("available_for_direct_debit");

	String userCheck = null;
	if (isBankUser) {
		userCheck = null;
	} else {
		userCheck = userSession.getUserOid();
	}
	payerParty.loadAcctChoices(corporateOrgOid, formMgr
					.getServerLocation(), resMgr, availabilityChecks,
					userCheck);
	payerAcctList = StringFunction.xssHtmlToChars(payerParty.getAttribute("acct_choices"));

	DocumentHandler acctOptions = new DocumentHandler(payerAcctList, true);

	// IAZ: We will get Funding Currency from the Details of the Funding Account 
	// selected from the Available Account's drop-down list.
	// Selected Payee's payment amount and total amount comes from the Domestic Payment data and it
	// will be formated using transaction/payment currency which is the same 
	// for all payees (but can be different form the funding currency) 

	if (acctOid != null && !acctOid.equals("")) {
		Vector vector = acctOptions
				.getFragments("/DocRoot/ResultSetRecord");
		int numItems = vector.size();

		for (int i = 0; i < numItems; i++) {
			DocumentHandler document = (DocumentHandler) vector
					.elementAt(i);
			String oid = document.getAttribute("/ACCOUNT_OID");
			if (acctOid.equals(oid)) {
				fundingCurrency = document.getAttribute("/CURRENCY");
				break;
			}
		}
	}

	String transactionCCY = terms.getAttribute("amount_currency_code");
	String transactionCCYDisplay = transactionCCY;
	String transactionAmount = terms.getAttribute("amount");
	String paymentAmount = currentDomesticPayment
			.getAttribute("amount");
	String paymentCurrency = currentDomesticPayment
			.getAttribute("amount_currency_code");

	displayPaymentAmount = TPCurrencyUtility.getDisplayAmount(
			paymentAmount, transactionCCY, loginLocale);
	if (getDataFromDoc
			&& maxError != null
			&& maxError.compareTo(String
					.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0)
		displayPaymentAmount = paymentAmount;
	String displayTransactionAmount = TPCurrencyUtility
			.getDisplayAmount(transactionAmount, transactionCCY,
					loginLocale);
	displayFundingAmount = TPCurrencyUtility.getDisplayAmount(
			fundingAmount, fundingCurrency, loginLocale);
%>

<%-- Header line --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	height="34">
	<tr>
		<td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
		<td class="BankColor" width="170" nowrap height="35">
		<p class="ControlLabelWhite"><span class="ControlLabelWhite">
		<%=resMgr.getText(
							"DirectDebitInstruction.CreditDetailsText",
							TradePortalConstants.TEXT_BUNDLE)%> </span></p>
		</td>
		<td width="100%" class="BankColor" height="35" valign="top">&nbsp;</td>
		<td width="1" class="BankColor" height="35">&nbsp;</td>
		<td width="1" class="BankColor" height="35">&nbsp;</td>
		<td width="90" class="BankColor" height="35" nowrap>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="17">
			<tr>
				<td align="center" valign="middle" height="17">
				<p class="ButtonLink"></p>
				</td>
			</tr>
		</table>
		</td>
		<td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
		<td class="BankColor" align="right" valign="middle" width="15"
			height="35" nowrap><%=OnlineHelp.createContextSensitiveLink(
							"customer/issue_funds_trans_request.htm",
							"funding_details", resMgr, userSession)%></td>
		<td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
	</tr>
</table>

<%
	if (!isTemplate) {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="ColorBeige">
	<tr>
		<td width="20" nowrap>&nbsp;</td>
		<td width="100%">
		<p class="AsterixText"><%=getRequiredIndicator(!isTemplate)%> <%=resMgr.getText("common.Required",
								TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
	</tr>
</table>
<%
	}
%>

<br>

<%-- IAZ: we need to preserve at least one attribute of the payee besides oid on the page
     otherise mediator will remove payer reference from the terms record 
            we also want to preserve at leats one attribute of payer which was originally 
     created form template --%>

<input type=hidden name=ApplicantName value='<%=applicantParty.getAttribute("name")%>'>
<input type=hidden name=PayerPartyType value=<%=TradePortalConstants.PAYER %>>

<%
	String firstPartyOid = terms.getAttribute("c_FirstTermsParty");

	//	In case of a single Payer, store this Payers' name into the Terms object
	//	If no payers, no Payer data should be stored with Terms (case of an error when first payee
	//  is being entered is also covered)
	//	If there are more then 1 Payer, store constant MULTIPLE_PAYERS_ENTERED to Terms
	
	//	Note: by the time of Authorize, this value will ALWAYS be set to either a single Payer name
	//	or MULTIPLE_PAYERS_ENTERED constant value because DDI transaction cannot be verified
	//	if there is less then 1 Payer.  This will also ensure a creation of FirstTermsParty (Payer)
	//	record in the database

	if (numberOfDomPmts > 1) {
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName"
	value='<%=resMgr.getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)%>'>
<input type="hidden" name="PayeeTermsPartyName"
	value='<%=resMgr.getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)%>'>	
<input type="hidden" name="FirstTermsPartyAddress1" value="">
<input type="hidden" name="FirstTermsPartyAddress2" value="">
<input type="hidden" name="FirstTermsPartyAddress3" value="">
<input type="hidden" name="FirstTermsPartyAddress4" value="">
<input type="hidden" name="FirstTermsPartyCountry" value="">
<% terms.setAttribute("reference_number", resMgr.getText("Common.MultReferences",
														TradePortalConstants.TEXT_BUNDLE)); %>

<%
	}
	
	if (numberOfDomPmts == 1) {
		String payeeNameToUse, payeeAdd1, payeeAdd2, payeeAdd3, payeeAdd4, payeeCountry, payeeRefNum;
		if (getDataFromDoc) {
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeAdd4 = currentDomesticPayment
					.getAttribute("payee_address_line_4");
			payeeCountry = currentDomesticPayment
			        .getAttribute("country");
			payeeRefNum = currentDomesticPayment.getAttribute("customer_reference");        
		} else {
			payeeNameToUse = ((DocumentHandler) enteredDomPmtList
					.elementAt(0)).getAttribute("/PAYEE_NAME");
			payeeAdd1 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_1");
			payeeAdd2 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_2");
			payeeAdd3 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_3");
			payeeAdd4 = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/PAYEE_ADDRESS_LINE_4");
			payeeCountry = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/COUNTRY");
			payeeRefNum = ((DocumentHandler) enteredDomPmtList
					.elementAt(0))
					.getAttribute("/CUSTOMER_REFERENCE");
		}

		payeeNameToUse = StringFunction.xssCharsToHtml(payeeNameToUse);
		if ((InstrumentServices.isNotBlank(payeeNameToUse))&&(payeeNameToUse.length() > 35))
            payeeNameToUse = payeeNameToUse.substring(0,35);		
		
		payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
		payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
		payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
		payeeAdd4 = StringFunction.xssCharsToHtml(payeeAdd4);
		payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);
		terms.setAttribute("reference_number", StringFunction.xssCharsToHtml(payeeRefNum));        
		
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName" value="<%=payeeNameToUse%>">
<input type="hidden" name="PayeeTermsPartyName"
	value="<%=payeeNameToUse%>">
<input type="hidden" name="FirstTermsPartyAddress1" value="<%=payeeAdd1%>">
<input type="hidden" name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>">
<input type="hidden" name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
<input type="hidden" name="FirstTermsPartyAddress4" value="<%=payeeAdd4%>">
<input type="hidden" name="FirstTermsPartyCountry" value="<%=payeeCountry%>">
<%
	}
	
	if (numberOfDomPmts == 0) {
		String payeeNameToUse, payeeAdd1, payeeAdd2, payeeAdd3, payeeAdd4, payeeCountry;
		if (getDataFromDoc) {
			payeeNameToUse = currentDomesticPayment
					.getAttribute("payee_name");
			payeeAdd1 = currentDomesticPayment
					.getAttribute("payee_address_line_1");
			payeeAdd2 = currentDomesticPayment
					.getAttribute("payee_address_line_2");
			payeeAdd3 = currentDomesticPayment
					.getAttribute("payee_address_line_3");
			payeeAdd4 = currentDomesticPayment
			     .getAttribute("payee_address_line_4");
			payeeCountry = currentDomesticPayment
 		         .getAttribute("country");
			payeeNameToUse = StringFunction
					.xssCharsToHtml(payeeNameToUse);
			payeeAdd1 = StringFunction.xssCharsToHtml(payeeAdd1);
			payeeAdd2 = StringFunction.xssCharsToHtml(payeeAdd2);
			payeeAdd3 = StringFunction.xssCharsToHtml(payeeAdd3);
			payeeAdd4 = StringFunction.xssCharsToHtml(payeeAdd4);
			payeeCountry = StringFunction.xssCharsToHtml(payeeCountry);
			terms.setAttribute("reference_number", StringFunction.xssCharsToHtml(currentDomesticPayment.getAttribute("customer_reference"))); 
		} else {
			payeeNameToUse = "";
			payeeAdd1 = "";
			payeeAdd2 = "";
			payeeAdd3 = "";
			payeeAdd4 = "";
			payeeCountry = "";
			terms.setAttribute("reference_number", "");
		}
        if ((InstrumentServices.isNotBlank(payeeNameToUse))&&(payeeNameToUse.length() > 35))
           	payeeNameToUse = payeeNameToUse.substring(0,35); 		
%>
<input type="hidden" name="testDebugForstPartyOid"
	value="<%=firstPartyOid%>">
<input type="hidden" name="PayerPartyContactName" value="<%=payeeNameToUse%>">
<input type="hidden" name="PayeeTermsPartyName"
	value="<%=payeeNameToUse%>">
<input type="hidden" name="FirstTermsPartyAddress1" value="<%=payeeAdd1%>">
<input type="hidden" name="FirstTermsPartyAddress2" value="<%=payeeAdd2%>">
<input type="hidden" name="FirstTermsPartyAddress3" value="<%=payeeAdd3%>">
<input type="hidden" name="FirstTermsPartyAddress4" value="<%=payeeAdd4%>">
<input type="hidden" name="FirstTermsPartyCountry" value="<%=payeeCountry%>">
<%
	}
%>

<%-- IAZ CR-483B 08/13/09 Begin: Various sections on DP Transaction and Template screens are moved per CR-483 design --%>
<%-- Template DropDown and Payment Amount Summary Table --%>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
		<th width="45%" class="ColorGrey" align="left">
		<%
			if ((!isTemplate) && (!isReadOnly)) {
		%>
		<p class="ControlLabel"><%=resMgr.getText("DomesticPaymentRequest.Template",
								TradePortalConstants.TEXT_BUNDLE)%></p>
		<%
			}
		%>
		</th>
		<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
		<th colspan="2" width="26%" class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.TotalAmount",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</th>
		<th width="16%" class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.NoOfDebits",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</th>
		<td width="5%"></td>
	</tr>

	<tr>
		<td class="ColorWhite" nowrap>&nbsp;</td>
		<td class="ListText">&nbsp <br>
		<%
			if ((!isTemplate) && (!isReadOnly)) {
				defaultText = resMgr.getText(
						"TransferBetweenAccounts.SelectTemplate",
						TradePortalConstants.TEXT_BUNDLE);
				DropdownOptions templateOptions = new DropdownOptions();
				templateOptions.setSelected(defaultText);

				DocumentHandler templateEntry = null;
				for (int j = 0; j < numberOfTemplates; j++) {
					templateEntry = (DocumentHandler) templateList.elementAt(j);
					templateOptions.addOption(templateEntry
							.getAttribute("/ORIGINAL_TRANSACTION_OID"),
							templateEntry.getAttribute("/TEMPLATENAME"));
				}
				String strTemplateOptions = templateOptions
						.createSortedOptionListInHtml(loginLocale);
		%> <%=resMgr.getText(
								"TransferBetweenAccounts.TemplateText",
								TradePortalConstants.TEXT_BUNDLE)%><br>
		<%
			out.println(InputField.createSelectField("templateSelection",
						"", defaultText, strTemplateOptions, "ListText",
						isReadOnly));
		%> <a href="javascript:document.forms[0].submit()" name="templateList"
			onMouseOut="changeImage('selectTemplate', '/portal/images/AddIcon_off.gif')"
			onMouseOver="changeImage('selectTemplate', '/portal/images/AddIcon_ro.gif')"
			onClick="setButtonPressed('SelectTemplate', 0);"> <img border="0"
			name="templateSelection" width="15" height="15"
			src="/portal/images/AddIcon_off.gif"
			alt='<%=resMgr.getText("common.PhraseLookupText",
								TradePortalConstants.TEXT_BUNDLE)%>'></a>
		<%
			}
		%>
		</td>
		<td class="ColorWhite" nowrap>&nbsp;</td>

		<td width="13%" class="ListText" align="left" nowrap><img
			src="/portal/images/Blank_15.gif" width="20" height="1" alt="">
		<%=resMgr.getText("TransferBetweenAccounts.Currency",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
		<img src="/portal/images/Blank_15.gif" width="20" height="1" alt="">
		<%
			options = Dropdown.createSortedCurrencyCodeOptions(terms
					.getAttribute("amount_currency_code"), loginLocale);
			out.println(InputField.createSelectField("TransactionCurrencyDD",
					"", " ", options, "ListText", isReadOnly,
					"onChange='updatePaymentCCY();'"));
		%>
		</td>

		<td width="14%" class="ListText" align="left" nowrap><%=resMgr.getText("TransferBetweenAccounts.Amount",
							TradePortalConstants.TEXT_BUNDLE)%><br>
		<input type="hidden" value="<%=fundingAmount%>" name="FundingAmount"
			size="20" maxlength="20" class="ListText"> <%
 	if (!isReadOnly) {
 %> <input type="text" value="<%=displayTransactionAmount%>"
			name="displayTransactionAmount" size="20" maxlength="20"
			class="ListText" disabled> <%
 	} else {
 %> <%=displayTransactionAmount%> <%
 	}
 %>
		</td>
		<td class="ListText" align="left"><br>
		<b><img src="/portal/images/Blank_15.gif" width="20" height="1"
			alt=""><img src="/portal/images/Blank_15.gif" width="20"
			height="1" alt=""><%=numberOfDomPmts%></b> <%-- IAZ CM-451 01/29/09 Add: send the total number of current Payees to the server 
          	so we can set Terms First Party data correctly --%> <input
			type="hidden" name="numberOfPayees" value="<%=numberOfDomPmts%>">
		</td>
	</tr>
</table>
<br>

<table width="95%" border="0" cellspacing="0" cellpadding="0">

	<tr>
		<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
		<td width="45%" class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.ExecutionDate",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%></p>
		</td>
		<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
		<%-- Vasavi NIUK01055165 Added required indicator --%>
		<td colspan="3" width="42%" class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.CreditToAccount",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%></p>
		</td>
		<td width="15%"></td>
	</tr>

	<tr>
		<td class="ColorWhite" nowrap>&nbsp;</td>
		<td class="ListText">&nbsp; <img
			src="/portal/images/Blank_15.gif" width="20" height="15" alt="">
		<input type="hidden" name="payment_date" value="<%=displayDPDate%>">
		<%
			if (isReadOnly) {
				String dateToFormat;
				if ((transaction.getAttribute("payment_date").indexOf("/") != -1)
						&& (InstrumentServices.isNotBlank(formatableDate))) {
					dateToFormat = formatableDate;
					Debug.debug("IAZDEbug::formatedDate in readOnly:: "
							+ dateToFormat);
				} else {
					dateToFormat = transaction.getAttribute("payment_date");

				}
				out.println(TPDateTimeUtility.formatDate(dateToFormat,
						TPDateTimeUtility.LONG, loginLocale));
			} else {
		%> <%=TPDateTimeUtility.getDayDropDown("PaymentDateDay",
								paymentDateDay, resMgr.getText("common.Day",
										TradePortalConstants.TEXT_BUNDLE),
								"onChange='updateDMPaymentDate();'")%> <img
			src="/portal/images/Blank_15.gif" width="15" height="15" alt="">
		<%=TPDateTimeUtility.getMonthDropDown(
								"PaymentDateMonth", paymentDateMonth,
								loginLocale, resMgr.getText("common.Month",
										TradePortalConstants.TEXT_BUNDLE),
								"onChange='updateDMPaymentDate();'")%> <img
			src="/portal/images/Blank_15.gif" width="15" height="15" alt="">
		<%=TPDateTimeUtility.getYearDropDown("PaymentDateYear",
								paymentDateYear, resMgr.getText("common.Year",
										TradePortalConstants.TEXT_BUNDLE),
								"onChange='updateDMPaymentDate();'")%> <img
			src="/portal/images/Blank_15.gif" width="15" height="15" alt="">
		<%
			}
		%>
		</td>
		<td class="ColorWhite" nowrap>&nbsp;</td>
		<%-- Vasavi NIUK01055165 Removed required indicator --%>
		<td class="ListText" valign="bottom"><%=resMgr
									.getText(
											"DomesticPaymentRequest.AvailableFundsShownForAccounts",
											TradePortalConstants.TEXT_BUNDLE)%>
		<br>
		<%
			defaultText = resMgr.getText("UserDetail.selectAccountNumberCCY",
					TradePortalConstants.TEXT_BUNDLE);

			options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid,
					loginLocale);
			out.println(InputField.createSelectField("CreditAccount",
					"", defaultText, options, "ListText", isReadOnly,
					"onChange='updatePayerAccCurrency();'"));
		%>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>


	<tr>
		 <%-- Vshah CR509 Begin --%>
		 <input type=hidden name=c_OrderingParty value="<%=terms.getAttribute("c_OrderingParty")%>">
         <input type=hidden name=OrderingPartyOid value="<%=orderingParty.getAttribute("payment_party_oid")%>">        
         <input type=hidden name=c_OrderingPartyBank value="<%=terms.getAttribute("c_OrderingPartyBank")%>">
         <input type=hidden name=OrderingPartyBankOid value="<%=orderingPartyBank.getAttribute("payment_party_oid")%>">
         <%-- Vshah CR509 End --%> 
		<td class="ColorWhite" nowrap>&nbsp;</td>
		<td class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.OrderingParty",
							TradePortalConstants.TEXT_BUNDLE)%></p>

		<td class="ColorWhite" nowrap>&nbsp;</td>

		<td class="ColorGrey" align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.OrderPartyBank",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
		</td>
	</tr>


	<tr>
		<td>&nbsp;</td>
		<td class="ListText" align="left" nowrap><br>
		<jsp:include page="/common/PartySearchButton.jsp">
			<jsp:param name="showButton" value="<%=!isReadOnly%>" />
			<jsp:param name="name" value="SearchOrderingParty" />
			<jsp:param name="image" value='common.SearchOrderingPartyImg' />
			<jsp:param name="text" value='common.SearchOrderingPartyText' />
			<jsp:param name="width" value="140" />
			<jsp:param name="partyType"	value="<%=TradePortalConstants.ORDERING_PARTY%>" />
		</jsp:include>&nbsp;&nbsp;</td>
		<td>&nbsp;</td>
		<td align="left"><br>
		<%-- Vshah CR509 Begin --%>
        <%-- /* IR-PMUK012737723 02/03/10 CHG 
		<jsp:include page="/common/BankSearchButton.jsp">
			<jsp:param name="showButton" value="<%=!isReadOnly%>" />
			<jsp:param name="name" value="SearchOrderingPartyBank" />
			<jsp:param name="image" value='common.SearchOrderingPartyBankImg' />
			<jsp:param name="text" value='common.SearchOrderingPartyBankText' />
			<jsp:param name="width" value="230" />
			<jsp:param name="bankType" value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>" />
		</jsp:include>
	    */ --%>				
          <jsp:include page="/common/PartySearchButton.jsp">
             <jsp:param name="showButton" 
                        value="<%=!(isReadOnly)%>" />
             <jsp:param name="name" value="SearchPayeeBankButton" />
             <jsp:param name="image" value='common.SearchOrderingPartyBankImg' />
             <jsp:param name="text" value='common.SearchOrderingPartyBankText' />
             <jsp:param name="width" value="140" />
             <jsp:param name="partyType" 
                        value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>" />
          </jsp:include>	          	  
	        <%-- Vshah CR507 End --%>  		
	  </td>
	</tr> 	  				  
	<tr>
		<td>&nbsp;</td>
		<td class="ControlLabel" align="left" nowrap><%=resMgr.getText("DirectDebitInstruction.OrderPartyName",
							TradePortalConstants.TEXT_BUNDLE)%></td>
		<td>&nbsp;</td>
		<td class="ControlLabel" align="left"><%=resMgr.getText(
							"DirectDebitInstruction.OrderPartyBankBranchCode",
							TradePortalConstants.TEXT_BUNDLE)%></td>

	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="ListText" align="left" nowrap><%=InputField.createTextField("OrderingPartyName",
				orderingParty.getAttribute("bank_name"),
							"35", "40", "ListText",
							isReadOnly || isFromExpress,
							"onChange='updatePayeePartyName();'")%></td>
		<td>&nbsp;</td>
		<td align="left"><%=InputField.createTextField("OrderingPartyBankCode",
				orderingPartyBank.getAttribute("bank_branch_code"),
							"35", "40", "ListText",
							isReadOnly || isFromExpress)%></td>
		<%-- IAZ IR-PMUK012737723 02/03/10 DEL input type=hidden name="OrderingPartyBankCode" 
					value="<%=orderingPartyBank.getAttribute("bank_branch_code")%>" --%> <%-- Vshah CR509 --%>	
	</tr>

	<tr>
		<%-- Second Line: Payee Name and Address, Bank Address and Central Bank Label only --%>
		<%-- plus Payee Terms and OTL Payee --%>
		<td width="2%" nowrap>&nbsp; <input type="hidden"
			name='OrderingPartyType' value="<%=TradePortalConstants.ORDERING_PARTY%>">
			<input type="hidden"
			name='OrderingPartyBankPartyType' value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>">
			<input type="hidden"
			name='ApplicantPartyType' value="<%=TradePortalConstants.APPLICANT%>">
			
		<input type="hidden" name="PayerPartyOTLCustomerId"
			value='<%=payerParty.getAttribute("OTL_customer_id")%>'>
			</td>
		<td class="ControlLabel" width="42%" valign="top">

		<p class="ControlLabel"><%=resMgr.getText(
							"DirectDebitInstruction.OrderPartyAddress",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
					
		<%=InputField.createTextField("OrderingPartyAddress1",orderingParty.getAttribute("address_line_1"), "35", "40", "ListText", isReadOnly,"")%> <br>
		<%=InputField.createTextField("OrderingPartyAddress2",orderingParty.getAttribute("address_line_2"),"35", "40", "ListText", isReadOnly,"")%> <br>
		<%=InputField.createTextField("OrderingPartyAddress3",orderingParty.getAttribute("address_line_3"),"32", "40", "ListText", isReadOnly,"")%> <br>
		<%=InputField.createTextField("OrderingPartyAddress4",orderingParty.getAttribute("address_line_4"),"35", "40", "ListText", isReadOnly,"")%><br>                                                               
      
      		<%=resMgr.getText("DirectDebitInstruction.Country",TradePortalConstants.TEXT_BUNDLE)%> <br>
      <%
        String countryCode = "";
        String countryDesc = "";
        /* IAZ IR-SEUK012660805 Rework Begin: For Release 5.2 we do not enable Country Fireld for OPY
           Vshah IR-SLUK021033331 04/28/10 For Release 6.0, we now enable Country Field for OPY */ 
        try
        {
        	countryCode = orderingParty.getAttribute("country");
        	if (InstrumentServices.isNotBlank(countryCode))
        		countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        	if (InstrumentServices.isBlank(countryDesc))
        	{
        		countryDesc = countryCode;
        		countryCode = "";
        	}
        }
        catch (Exception e)
        {
        	countryCode = "";
        	countryDesc = "";
        }
        
        if (InstrumentServices.isBlank(countryCode))
        	countryCode = " ";
		options = Dropdown.createSortedRefDataOptions(
		                        "COUNTRY",
		                        countryCode,
		                        loginLocale);
		out.println(InputField.createSelectField("OrderingPartyCountry", "",
		                        " ", options, "ListText", isReadOnly));
		                        
		/* IAZ IR-SEUK012660805 Rework End
		   Vshah IR-SLUK021033331 End  */                           
		  %>        
		<input type=hidden name="OrderingPartyType" value="<%=TradePortalConstants.ORDERING_PARTY%>"></p>  
		
		</td>
		<td width="4%" nowrap>&nbsp;</td>
		<td class="ControlLabel" width="44%" valign="top"><%=resMgr.getText(
							"DirectDebitInstruction.OrderPartyBankNameAddress",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
        <%
        countryCode = "";
        countryDesc = "";
        try
        {
        	countryCode = orderingPartyBank.getAttribute("country");
        	if (InstrumentServices.isNotBlank(countryCode))
        		countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        	if (InstrumentServices.isBlank(countryDesc))
        	{
        		countryDesc = countryCode;
        		countryCode = "";
        	}
        }
        catch (Exception e)
        {
        	countryCode = "";
        	countryDesc = "";
        }
        %>          							
		<textarea name='PayeeBankData' disabled="true" wrap="PHYSICAL"
			onFocus='this.blur();' class="ListText" cols="45" rows="5"><%=buildPaymentBankDisplayData(
					orderingPartyBank.getAttribute("bank_name"),
					orderingPartyBank.getAttribute("branch_name"),
					orderingPartyBank.getAttribute("address_line_1"),
 					orderingPartyBank.getAttribute("address_line_2"),
					orderingPartyBank.getAttribute("address_line_3"),
					countryDesc, isReadOnly)%></textarea>
				<br><br>
				<%-- Vshah CR509 End --%>
				<%
					String clearReqExtraTag = "return setBankPartyToClear('" + TradePortalConstants.ORDERING_PARTY_BANK + "');"; //IAZ 01/30/10 Add
				%>				
				
				<%-- Vshah ClearParty Start--%> 
			    <jsp:include page="/common/RolloverButtonSubmit.jsp">
				    <jsp:param name="showButton" value='<%=!isReadOnly%>' />
		        	<jsp:param name="name" value="ClearOrderingPartyBankButton" />
				    <jsp:param name="image" value='common.ClearOrderingBankImg' />
		        	<jsp:param name="text" value='common.ClearOrderingBankText' />
				    <jsp:param name="width" value="230" />
		        	<jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_CLEAR%>" />
					<jsp:param name="extraTags" value="<%=clearReqExtraTag%>" />  
				</jsp:include>
				<%-- Vshah ClearParty End --%>
		  <%-- Vshah CR509 Begin --%>							
		  <input type=hidden name="OrderingPartyBankName" value="<%=orderingPartyBank.getAttribute("bank_name")%>">
          <input type=hidden name="OrderingPartyBankAddressLine1" value="<%=orderingPartyBank.getAttribute("address_line_1")%>">
          <input type=hidden name="OrderingPartyBankAddressLine2" value="<%=orderingPartyBank.getAttribute("address_line_2")%>">
          <input type=hidden name="OrderingPartyBankAddressLine3" value="<%=orderingPartyBank.getAttribute("address_line_3")%>">
          <input type=hidden name="OrderingPartyBankBranchName" value="<%=orderingPartyBank.getAttribute("branch_name")%>">
          <input type=hidden name="OrderingPartyBankType" value="<%=TradePortalConstants.ORDERING_PARTY_BANK%>">
          <input type=hidden name="OrderingPartyBankCountry" value="<%=countryCode%>">  
          <%-- Vshah CR509 End --%>
    	 <br>
		<img src="/portal/images/Blank_256pix.gif" width="256" height="1"
			alt=""> <br>

		</td>

	</tr>

	<tr>
		<td>&nbsp;</td>
		<td class="ListText" align="left" nowrap></td>
		<td>&nbsp;</td>
		<td align="left"></td>

	</tr>

</table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	height="34">
	<tr>
		<td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
		<td class="BankColor" width="170" nowrap height="35">
		<p class="ControlLabelWhite"><span class="ControlLabelWhite">
		<%=resMgr.getText("DirectDebitInstruction.details",
							TradePortalConstants.TEXT_BUNDLE)%> </span></p>
		</td>
		<td width="100%" class="BankColor" height="35" valign="top">&nbsp;</td>
		<td width="1" class="BankColor" height="35">&nbsp;</td>
		<td width="1" class="BankColor" height="35">&nbsp;</td>
		<td width="90" class="BankColor" height="35" nowrap>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="17">
			<tr>
				<td align="center" valign="middle" height="17">
				<p class="ButtonLink"></p>
				</td>
			</tr>
		</table>
		</td>
		<td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
		<td class="BankColor" align="right" valign="middle" width="15"
			height="35" nowrap></td>
		<td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
	</tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0">

	<tr>
		<td width="2%">&nbsp;</td>
		<td colspan="7" width="98%" class="ColorBeige" align="left">
		<p class="ControlLabel">&nbsp;&nbsp;&nbsp;<%=resMgr.getText(
							"DirectDebitInstruction.EnterReviewCreditData",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>
	</tr>
	<tr>
		<th colspan="8" width="100%" cellspacing="0" cellpadding="0">
		<table border="0" width="100%">
			<tr>
				<td class="ColorWhite" nowrap>&nbsp;</td>
			</tr>
			<%-- Header Line: Payment Method Control Label, Payment Details Control Label --%>
			<tr>
				<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
				<th width="20%" class="ColorGrey" align="left">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.ChargeDetails",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%></p>
				</th>
				<td width="2%" nowrap>&nbsp;</td>
				<th width="25%" class="ColorGrey" align="left">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.PaymentDetails",
							TradePortalConstants.TEXT_BUNDLE)%></p>
				</th>
				<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
				<th colspan="3" width="40%" class="ColorGrey" align="left">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.DebitDetails",
							TradePortalConstants.TEXT_BUNDLE)%></p>
				</th>
			</tr>
			<%-- First Line: Payment Method, Curency (protected), Amount, Value Date --%>
			<%-- plus hidden Bank Terms and OTL Bank --%>
			<tr>
				<td width="2%" nowrap rowspan="2">&nbsp;
				<input type=hidden name=PaymentMethodType value='<%=TradePortalConstants.DIRECT_DEBIT_INSTRUCTION %>'></td>
				<td class="ControlLabel" width="25%" nowrap rowspan="2">&nbsp;&nbsp;<%=resMgr.getText("DirectDebitInstruction.Charges",
							TradePortalConstants.TEXT_BUNDLE)%> <br>

				<%=InputField.createRadioButtonField(
											"BankChargesType",
											TradePortalConstants.CHARGE_OUR,
											"",
											bankChargesType
													.equals(TradePortalConstants.CHARGE_OUR),
											"ListText", "", isReadOnly)%> <%=resMgr.getText("DirectDebitInstruction.OursCharges",
							TradePortalConstants.TEXT_BUNDLE)%> <br>

				<%=InputField
									.createRadioButtonField(
											"BankChargesType",
											TradePortalConstants.CHARGE_PAYER,
											"",
											bankChargesType
													.equals(TradePortalConstants.CHARGE_PAYER),
											"ListText", "", isReadOnly)%> <%=resMgr.getText("DirectDebitInstruction.Payer",
							TradePortalConstants.TEXT_BUNDLE)%> <br>

				<%=InputField.createRadioButtonField("BankChargesType",
							TradePortalConstants.CHARGE_SHARED, "",
							bankChargesType
									.equals(TradePortalConstants.CHARGE_SHARED),
							"ListText", "", isReadOnly)%> <%=resMgr.getText("DirectDebitInstruction.SharedCharges",
							TradePortalConstants.TEXT_BUNDLE)%></td>
				<td width="2%" nowrap>&nbsp;</td>
				<td class="ListText" width="25%" nowrap rowspan="2">
				<%
					String paymentDetailText = currentDomesticPayment
							.getAttribute("payee_description");
					if (!InstrumentServices.isBlank(paymentDetailText)
							&& (paymentDetailText.equals("null")))
						paymentDetailText = "";
					out.println(InputField.createTextArea("PaymentDetail",
							paymentDetailText, "50", "3", "ListText", isReadOnly
									|| isFromExpress));
				%>
				</td>

				<td rowspan="2" width="2%" nowrap><input type="hidden"
					name='PayeeBankTermsPartyType'
					value="<%=TradePortalConstants.PAYEE_BANK%>">
				</td>
				<td class="ControlLabel" width="7%" align="left" valign="bottom"
					nowrap><%=resMgr.getText("FundsTransferRequest.Currency",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%>
				<br>
				<span class="ListText"> <%
 	if (!isReadOnly) {
 %> <input type="text" value="<%=transactionCCYDisplay%>"
					name="TransactionCurrencyDisplay" size="5" maxlength="5"
					class="ListText" disabled> <%
 	} else {
 %> <%=transactionCCYDisplay%> <%
 	}
 %> </span></td>
				<td class="ControlLabel" width="21%" align="left" valign="bottom"
					nowrap><%=resMgr.getText("FundsTransferRequest.Amount",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%>
				<br>
				<%=InputField.createTextField("DomesticPaymentAmount",
							displayPaymentAmount, "22", "22", "ListTextRight",
							isReadOnly)%></td>
				<td class="ControlLabel" width="12%" align="left" valign="top"
					nowrap><%=resMgr.getText("DirectDebitInstruction.ValueDate",
							TradePortalConstants.TEXT_BUNDLE)%> 
            <br>              
            <%-- IAZ 01/18/10 Add Value Date to the screen --%>
            <span class="ListText" id="ValueDate">
            	<%=TPDateTimeUtility.formatDate(currentDomesticPayment.getAttribute("value_date"), 
                             TPDateTimeUtility.LONG,
                             userLocale) %></span>                 
            <br>							
				</td>
			</tr>

			<tr>
				<td width="2%" nowrap>&nbsp;</td>

				<td class="ControlLabel" colspan="3" width="40%" align="left"
					valign="top" nowrap><%=resMgr.getText("DirectDebitInstruction.BOReference",
							TradePortalConstants.TEXT_BUNDLE)%> <br>

				<%=InputField.createTextField("PayerReference",
							currentDomesticPayment
									.getAttribute("customer_reference"),
							"30", "40", "ListText", isReadOnly,
							"onChange='updateTermsDescription();'")%> <br>

				</td>

			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="34">
			<tr>
				<td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
				<td class="BankColor" width="170" nowrap height="35">
				<p class="ControlLabelWhite"><span class="ControlLabelWhite">
				<%=resMgr.getText("DirectDebitInstruction.PartyInformation",
							TradePortalConstants.TEXT_BUNDLE)%> </span></p>
				</td>
				<td width="100%" class="BankColor" height="35" valign="top">&nbsp;</td>
				<td width="1" class="BankColor" height="35">&nbsp;</td>
				<td width="1" class="BankColor" height="35">&nbsp;</td>
				<td width="90" class="BankColor" height="35" nowrap>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					height="17">
					<tr>
						<td align="center" valign="middle" height="17">
						<p class="ButtonLink"></p>
						</td>
					</tr>
				</table>
				</td>
				<td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
				<td class="BankColor" align="right" valign="middle" width="15"
					height="35" nowrap></td>
				<td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
			</tr>
		</table>


		<%-- Inner Table for Credit Parties CR-507 --%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<%-- Credit Parties Details Header Line --%>

			<tr>
				<td width="2%">&nbsp;</td>
				<td colspan="3" width="98%" class="ColorBeige" align="left">
				<p class="ControlLabel">&nbsp;&nbsp;&nbsp;<%=resMgr
									.getText(
											"DirectDebitInstruction.EnterReviewDirectPartyData",
											TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
			</tr>
			<tr>
				<td class="ColorWhite" nowrap>&nbsp;</td>
			</tr>
			<tr>
				<%-- Bene Columns Sub-Header Line --%>
				<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
				<th width="45%" class="ColorGrey" align="left">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.Payer",
							TradePortalConstants.TEXT_BUNDLE)%></p>
				</th>
				<td width="2%" class="ColorWhite" nowrap>&nbsp;</td>
				<th colspan="3" width="40%" class="ColorGrey" align="left">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.PayerBank",
							TradePortalConstants.TEXT_BUNDLE)%></p>
				</th>
			</tr>
			<tr>
				<%-- First Line: Bene Acc No & Search Button, Bene Bank Name/Branch Code & Search Button --%>
				<td nowrap>&nbsp;</td>
				<td class="ControlLabel" width="42%"><jsp:include
					page="/common/PartySearchButton.jsp">
					<jsp:param name="showButton" value="<%=!isReadOnly%>" />
					<jsp:param name="name" value="SearchPyrButton" />
					<jsp:param name="image" value='common.SearchPayerImg' />
					<jsp:param name="text" value='common.SearchPayerText' />
					<jsp:param name="width" value="230" />
					<jsp:param name="partyType" value="<%=TradePortalConstants.PAYER%>" />
				</jsp:include> <br>
				<%=resMgr.getText(
							"DirectDebitInstruction.PayerAccountNumber",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%>
				<br>
				<%
				  String extTag="";  
				  if (displayMultipleAccounts) {
					out.println(InputField.createRadioButtonField("AccountSelectType", TradePortalConstants.USER_ENTERED_ACCT, "",!accountSet.contains(payeeAcctNum), "ListText", "", isReadOnly,"onClick=\"javascript:setAccount('EnteredAccount');\"\""));
					extTag = "onChange=\"javascript:setAccountSelectType('EnteredAccount','0');\"\"";
				  } 
   	              else {
		        	   extTag = "onChange=\"javascript:setAccount('EnteredAccount');\"\"";
		           }
		           
		           if (accountSet != null && accountSet.contains(payeeAcctNum)) {
		        	   acctNo = "";
		           }
		           else {
		        	   acctNo = payeeAcctNum;
		           }  
                 %>
				<%=InputField.createTextField("EnteredAccount", acctNo, "34", "40", "ListText", isReadOnly,extTag)%> <br>
			  <%if (displayMultipleAccounts) {
	                defaultText = resMgr.getText("DomesticPaymentRequest.selectAccountNumber",  TradePortalConstants.TEXT_BUNDLE);
				    out.println(InputField.createRadioButtonField("AccountSelectType", "NOT"+TradePortalConstants.USER_ENTERED_ACCT, "",accountSet.contains(payeeAcctNum), "ListText", "", isReadOnly,"onClick=\"javascript:setAccount('SelectedAccount');\"\""));
	        	    acctNo = (accountSet.contains(payeeAcctNum))? payeeAcctNum : "";
                    options = ListBox.createOptionList(accountDoc, "ACCOUNT_NUMBER", "ACCOUNT_NUMBER",acctNo);
		            out.println(InputField.createSelectField("SelectedAccount", "", defaultText, options, "ListText", isReadOnly,"onChange=\"javascript:setAccountSelectType('SelectedAccount','1');\"\""));
                }
		        %>
		         <input type=hidden name=beneficiaryPartyOid value=<%=currentDomesticPayment.getAttribute("beneficiary_party_oid")%>>
		         <input type=hidden name=PayeeAccount value=<%=currentDomesticPayment.getAttribute("payee_account_number")%>>
				</td>
				<td width="4%" nowrap>&nbsp;</td>
				<td class="ControlLabel" width="42%" valign="top">
				<%if (TradePortalConstants.NON_ADMIN.equals(userSession.getSecurityType())) {
 			     %>
				<%-- Vshah CR509 Begin --%>
					<jsp:include page="/common/BankSearchButton.jsp">
						<jsp:param name="showButton" value="<%=!isReadOnly%>" />
						<jsp:param name="name" value="SearchPayerBank" />	
						<jsp:param name="image" value='common.SearchPayerBankImg' />
						<jsp:param name="text" value='common.SearchPayerBankText' />
						<jsp:param name="width" value="230" />
						<jsp:param name="bankType" value="<%=TradePortalConstants.PAYER_BANK%>" />
					</jsp:include>
				<%-- Vshah CR509 End --%>	
			    <%}%>
				<br>
				<%=resMgr.getText("DirectDebitInstruction.PayerBankBranch",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%>
				<br>
				<%=InputField.createTextField("PayeeBankBranchCode",
							currentDomesticPayment
									.getAttribute("payee_bank_code"), "20",
							"35", "ListText", isReadOnly, "disabled")%> <br>
				<input type="hidden" name="PayeeBankCode" value="<%=currentDomesticPayment.getAttribute("payee_bank_code")%>">	<%-- Vshah CR509 --%>		
				</td>
			</tr>
			<tr>
				<%-- Second Line: Payee Name and Address, Bank Address and Central Bank Label only --%>
				<%-- plus Payee Terms and OTL Payee --%>
				<td width="2%" nowrap>&nbsp; <input type="hidden"
					name='PayeeTermsPartyType' value="<%=TradePortalConstants.PAYEE%>">
				</td>
				<td class="ControlLabel" width="42%"><br>
				<%=resMgr.getText("DirectDebitInstruction.PayerName",
							TradePortalConstants.TEXT_BUNDLE)%> <%=getRequiredIndicator(!isTemplate)%>
				<br>
				<%=InputField.createTextField("PayeeName",
							currentDomesticPayment.getAttribute("payee_name"),
							"140", "40", "ListText",
							isReadOnly || isFromExpress,
							"onChange='updatePayeePartyName();'")%> <br>
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.PayerAddress",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
				<%=InputField.createTextField("PayeeAddrL1",
							currentDomesticPayment
									.getAttribute("payee_address_line_1"),
							"35", "40", "ListText", isReadOnly,
							"onChange='updatePayeeAddressLine();'")%> <br>
				<%=InputField.createTextField("PayeeAddrL2",
							currentDomesticPayment
									.getAttribute("payee_address_line_2"),
							"35", "40", "ListText", isReadOnly,
							"onChange='updatePayeeAddressLine();'")%> <br>
				<%=InputField.createTextField("PayeeAddrL3",
							currentDomesticPayment
									.getAttribute("payee_address_line_3"),
							"32", "40", "ListText", isReadOnly,
							"onChange='updatePayeeAddressLine();'")%> <br>
				<%=InputField.createTextField("PayeeAddrL4",
							currentDomesticPayment
									.getAttribute("payee_address_line_4"),
							"35", "40", "ListText", isReadOnly,
							"onChange='updatePayeeAddressLine();'")%></p>

				</td>
				<%-- IAZ CR-483B 08/13/09 End Payee Adress --%>
				<td width="4%" nowrap>&nbsp;</td>
				<td class="ControlLabel" width="44%" valign="top"><br>
				<%
        		countryCode = "";
        		countryDesc = "";
        		try
        		{
        			countryCode = currentDomesticPayment.getAttribute("payee_bank_country");
        			if (InstrumentServices.isNotBlank(countryCode))
        				countryDesc = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY, countryCode, 
        															resMgr.getCSDB().getLocaleName());
        			if (InstrumentServices.isBlank(countryDesc))
        			{
        				countryDesc = countryCode;
        				countryCode = "";
        			}
        		}
        		catch (Exception e)
        		{
        			countryCode = "";
        			countryDesc = "";
        		}
        		%>          							
				
				<%=resMgr.getText("DirectDebitInstruction.PayerBankAddress",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
				<textarea name='PayeeBankData' disabled="true" wrap="PHYSICAL"
					onFocus='this.blur();' class="ListText" cols="45" rows="5"><%=buildPaymentBankDisplayData(
									currentDomesticPayment
											.getAttribute("payee_bank_name"),
									currentDomesticPayment
											.getAttribute("payee_branch_name"),
									currentDomesticPayment
											.getAttribute("address_line_1"),
									currentDomesticPayment
											.getAttribute("address_line_2"),
									currentDomesticPayment
											.getAttribute("address_line_3"),
									countryDesc, isReadOnly)%></textarea>
				<br><br>
				<%
					clearReqExtraTag = "return setBankPartyToClear('" + TradePortalConstants.PAYER_BANK + "');"; //IAZ 01/30/10 Add
					if (TradePortalConstants.NON_ADMIN.equals(userSession.getSecurityType())) {
				%>				
				<%-- Vshah ClearParty Start--%> 
	        	    <jsp:include page="/common/RolloverButtonSubmit.jsp">
		            	<jsp:param name="showButton" value='<%=!isReadOnly%>' />
	        		    <jsp:param name="name" value="ClearPayerBankButton" />
			            <jsp:param name="image" value='common.ClearPayerBankImg' />
        			    <jsp:param name="text" value='common.ClearPayerBankText' />
		    	        <jsp:param name="width" value="230" />
        			    <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_CLEAR%>" />
						<jsp:param name="extraTags" value="<%=clearReqExtraTag%>" />  
				    </jsp:include>
			    <%-- Vshah ClearParty End--%>
			     <%}%>
				<input type="hidden" name="PayeeBankName"	value='<%=currentDomesticPayment.getAttribute("payee_bank_name")%>'>
				<input type="hidden" name="PayeeBankAddressLine1" value='<%=currentDomesticPayment.getAttribute("address_line_1")%>'>
				<input type="hidden" name="PayeeBankAddressLine2" value='<%=currentDomesticPayment.getAttribute("address_line_2")%>'>
				<input type="hidden" name="PayeeBankAddressLine3" value='<%=currentDomesticPayment.getAttribute("address_line_3")%>'>
				<input type="hidden" name="PayeeBankCountry" value='<%=countryCode%>'>
				<input type=hidden name="PayeeBankBranchName" value="<%=currentDomesticPayment.getAttribute("payee_branch_name")%>"> 	<%-- Vshah CR509 --%>
				<br>
				<img src="/portal/images/Blank_256pix.gif" width="256" height="1"
					alt=""> <br>
				<br>
				</td>

			</tr>
			<tr>
				<%-- Third Line: Country, Fax, Email, Instructions Number and Centrtal Bank Data 121009 --%>
				<%-- plus Payee Terms and OTL Payee --%>
				<td width="2%" nowrap>&nbsp; <input type="hidden"
					name='PayeeTermsPartyType' value="<%=TradePortalConstants.PAYEE%>">
				<input type="hidden" name="PayeeOTLCustomerId"
					value='<%=payerParty.getAttribute("OTL_customer_id")%>'>
				</td>
				<td class="ControlLabel" width="42%">
				<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.Country",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
				<%
					String defaultCntry = currentDomesticPayment
							.getAttribute("country");
					options = Dropdown.createSortedRefDataOptions("COUNTRY",
							defaultCntry, loginLocale);
					out.println(InputField.createSelectField("Country", "", " ",
							options, "ListText", isReadOnly));
				%> <br>
				<%=resMgr.getText("DirectDebitInstruction.PayerFax",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
				<%=InputField.createTextField("payeeFaxNumber",
							currentDomesticPayment
									.getAttribute("payee_fax_number"), "15",
							"40", "ListText", isReadOnly)%> <br>
				<%=resMgr.getText("DirectDebitInstruction.PayerEmail",
							TradePortalConstants.TEXT_BUNDLE)%> <br>
				<%=InputField.createTextField("payeeEmail",
							currentDomesticPayment.getAttribute("payee_email"),
							"255", "40", "ListText", isReadOnly)%> <br>

				</p>
				</td>
				<%-- IAZ CR-483B 08/13/09 End Payee Adress --%>
				<td width="4%" nowrap>&nbsp;</td>
				<td class="ControlLabel" width="44%" valign="top"><br>
				</td>
			</tr>



			<tr></tr>

		</table>
		</th>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<%-- IAZ: Data Table Ends --%>

<%-- IAZ CM-451 11/25/08 Add/Modify/Delete Payee should be available in !Read-Only Mode --%>
<%
	if (!isReadOnly) {
		radioSelectSection = false;
%>

<%@ include file="Transaction-AddModifyParty.frag"%><%-- SAUK012141953 - Changed from Transaction-AddModifyPayee.frag to Transaction-AddModifyParty.frag --%>
<p>
<%
	}

	if (isTemplate) {
%> <%@ include file="Transaction-Footer.frag"%>
<%
	}
%> <br>
<br>
<p>
<div
	style="left: 100px; top: 100px; width: 1000; height: 200px; background-color: #ffffff; overflow: auto;">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="" nowrap>&nbsp;</td>
		<td colspan="6" class="ColorGrey" nowrap align="left">
		<p class="ControlLabel"><%=resMgr.getText("DirectDebitInstruction.PayerList",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>


	</tr>
	<tr>
		<td width="14" nowrap>&nbsp;</td>
		<td colspan="6" class="ColorBeige" nowrap align="left">
		<p class="ListText"><%=resMgr.getText("DirectDebitInstruction.PayerListDesc",
							TradePortalConstants.TEXT_BUNDLE)%></p>
		</td>

	</tr>
</table>

<%
	BigDecimal totalPaymentsAmount = new BigDecimal(0); 
	String totalPaymentsAmountStr = "0.00";
	if (!sqlPayeeError) {
%> <%-- A List of All the Payees --%>

<table width="100%" border="0" cellspacing="4" cellpadding="2">
	<tr>
		<td width="15" nowrap>&nbsp;</td>
		<td width="15%" class="ColorBeige" nowrap height="20" align="left"
			valign="middle"><span class="ControlLabel"> <%=resMgr.getText("FundsTransferRequest.AccountNumber",
								TradePortalConstants.TEXT_BUNDLE)%> </span></td>
		<td width="19%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText("DirectDebitInstruction.PayerName",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
		<%-- IAZ CR-483B 08/13/09 Begin: New Currency and Payment Method Columns per CR-483 design --%>
		<td width="10%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText("FundsTransferRequest.Currency",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
		<td width="10%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText("FundsTransferRequest.Amount",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
		<td width="18%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText(
								"DirectDebitInstruction.PayerBankName",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
		<td width="10%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText(
								"DirectDebitInstruction.PayerBankBranch",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
		<td width="19%" class="ColorBeige" nowrap align="left" valign="middle"><span
			class="displaytext"> <span class="ControlLabel"> <%=resMgr.getText("DirectDebitInstruction.ValueDate",
								TradePortalConstants.TEXT_BUNDLE)%> </span> </span></td>
	</tr>

	<%
		for (int j = 0; j < numberOfDomPmts; j++) {
				enteredDomPmtDoc = (DocumentHandler) enteredDomPmtList
						.elementAt(j);
				boolean isPayeeSelected = enteredDomPmtDoc.getAttribute(
						"/DOMESTIC_PAYMENT_OID").equals(
						currentDomesticPayment
								.getAttribute("domestic_payment_oid"));

				if ((j % 2) == 0) {
					classAttribute = "";
				} else {
					classAttribute = "class='ColorGrey'";
				}
	%>
	<tr>
		<td width="1"><%=InputField
											.createRadioButtonField(
													"DomesticPaymentOID",
													enteredDomPmtDoc
															.getAttribute("/DOMESTIC_PAYMENT_OID"),
													"", isPayeeSelected,
													"ListText", "", false,
													"onClick='setButtonPressed(\"ModifyPayee\", 0);document.forms[0].submit()'")%>
		</td>
		<td <%=classAttribute%> align="left" valign="middle">
		<p class="ListText">
		<%
					defaultValue = enteredDomPmtDoc
							.getAttribute("/PAYEE_ACCOUNT");
					defaultValue = StringFunction.xssCharsToHtml(defaultValue);
					out.println(defaultValue);
		%>
		</p>
		</td>

		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_NAME");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}
 			out.println(defaultValue);
 %> </span></td>

		<%-- IAZ CR-483B 08/13/09 Begin: New Currency and Pmt Method Columns per CR-483 design --%>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	out.println(transactionCCY);
 %> </span></td>

		<td <%=classAttribute%> align="right" valign="middle"><span
			class="ListText"> <%
 	{
 				String currentCurrency = enteredDomPmtDoc
 						.getAttribute("/CURRENCY_CODE");
 				defaultValue = TPCurrencyUtility.getDisplayAmount(
 						enteredDomPmtDoc.getAttribute("/PAYEE_AMOUNT"),
 						currentCurrency, loginLocale);;
 			}

 			out.print(defaultValue);

 			try {
 				BigDecimal checkPmtValue = new BigDecimal(
 						enteredDomPmtDoc.getAttribute("/PAYEE_AMOUNT"));
 				totalPaymentsAmount = totalPaymentsAmount
 						.add(checkPmtValue);
 			} catch (Exception any_e) {
 				//skip this till next
 			}
 %> </span></td>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_BANK_NAME");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);

 				out.print(defaultValue); 			
 	}

 %> </span></td>
		<%-- IAZ CR-483B 08/13/09 End: New Currency and Pmt Method Columns per CR-483 design --%>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%
 	{
 				defaultValue = enteredDomPmtDoc
 						.getAttribute("/PAYEE_BANK_CODE");
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}

 			out.print(defaultValue);
 %> </span></td>
		<td <%=classAttribute%> align="left" valign="middle"><span
			class="ListText"> <%

 	{
 				//Add Value date
 				defaultValue = TPDateTimeUtility.formatDate(enteredDomPmtDoc.getAttribute("/VALUE_DATE"), 
                             									TPDateTimeUtility.LONG,
                             									userLocale);
 				defaultValue = StringFunction
 						.xssCharsToHtml(defaultValue);
 			}

 			out.print(defaultValue);			
 %> </span></td>
	</tr>
	<%
		}

			totalPaymentsAmountStr = TPCurrencyUtility.getDisplayAmount(
					totalPaymentsAmount.toString(),
					transactionCCY, loginLocale); 

			DocumentHandler checkError = doc.getComponent("/Error");
			if (checkError != null) {
				String checkErrorStr = checkError.toString();
				if ((checkErrorStr != null)
						&& (checkErrorStr
								.indexOf(TradePortalConstants.INVALID_CURRENCY_FORMAT)) != -1) {
					totalPaymentsAmountStr = totalPaymentsAmount.toString();
				}
			}
		} else {
	%>
	<br>
	<img src="/portal/images/Error.gif" alt="">
	System Error: List of Payees for thes Payment Transaction Cannot Be
	Displayed.
	<br>
	<img src="/portal/images/Error.gif" alt="">
	System Error: Please Contact Technical Support.
	<%
		}
	%>
</table>

</div>


<%-- IAZ: Store various hidden fields to be send to the server --%> 
<%--      Some of these fields are needed by both Terms and Domestic Payments --%>

<input type="hidden" name="DisplayTotalPaymentsAmount"
	value="<%=totalPaymentsAmountStr%>"> 
<input type="hidden"
	name="TotalPaymentsAmount" value="<%=totalPaymentsAmount%>"> 
	
<input type="hidden" name="PayerRefNum"
	value='<%=terms.getAttribute("reference_number")%>'> 
				   
<input type="hidden"
	name="TransactionCurrency" value="<%=transactionCCY%>"> 

<input
	type="hidden" name="TransactionAmount"
	value="<%=totalPaymentsAmount.toString()%>"> 

<%-- IAZ CM-451 11/25/08 Regulary, Transaction Detail Page does not have a delete
                         button so confimrDelete() is not there.  Add for our specific
                         Domenstic Payment Payee case --%> 
<script	LANGUAGE="JavaScript" type="text/javascript">
<%if (!isTemplate) 
{
	out.println("function confirmDelete()");
	out.println("{ var msg = 'Are you sure you want to delete this item?'");
	out.println("if(!confirm(msg)) {formSubmitted = false; return false;}");
	out.println("return true;}");

}%>
</script> 

<script LANGUAGE="JavaScript" type="text/javascript">
  
   function updateDMPaymentDate() {

	var index = document.forms[0].PaymentDateDay.selectedIndex;
	var day = document.forms[0].PaymentDateDay.options[index].value;
	
	index = document.forms[0].PaymentDateMonth.selectedIndex;
	var month = document.forms[0].PaymentDateMonth.options[index].value;
	
	index = document.forms[0].PaymentDateYear.selectedIndex;
	var year = document.forms[0].PaymentDateYear.options[index].value;
		    
	if (day != '-1' || month != '-1' || year != '-1') 
	{
             document.forms[0].payment_date.value = month + '/' + day + '/' + year + ' 00:00:00';
	}
	document.getElementById("ValueDate").innerHTML = ""; 
  }
	
  function updatePaymentCCY()
  {
  
    document.forms[0].TransactionCurrencyDisplay.value = document.forms[0].TransactionCurrencyDD.value;
    document.forms[0].TransactionCurrency.value = document.forms[0].TransactionCurrencyDD.value;
    document.getElementById("ValueDate").innerHTML = ""; 
  }
  
  function updatePayerAccCurrency() {
    var account = document.forms[0].CreditAccount.options[document.forms[0].CreditAccount.selectedIndex].text
    var i = 0;
    var currency ="";
    var aindex = account.indexOf("(");
    if (aindex != -1)
    	currency = account.substring(aindex+1, aindex+4);    
    var aTC = document.forms[0].TransactionCurrencyDD
    if (aTC.selectedIndex == 0)
    {
        document.forms[0].TransactionCurrencyDisplay.value = currency;
        document.forms[0].TransactionCurrency.value = currency;

    	for (i = 0; i < aTC.options.length; i++)
    	{
    	    if (aTC.options[i].value == currency)
    	    {
    			aTC.options[i].selected = true
    		}
    		else
    		{
    			aTC.options[i].selected = false    	
    		}
    	}
    }	
    document.getElementById("ValueDate").innerHTML = ""; 
  }    
    

  function updateTotalAmount() {
  
      var displayTranAmtfromDB = document.forms[0].displayTransactionAmount.value;
      var calculatedTranAmt = document.forms[0].DisplayTotalPaymentsAmount.value;
      if (displayTranAmtfromDB != calculatedTranAmt)
      {
      	document.forms[0].TransactionAmount.value = calculatedTranAmt;
      	document.forms[0].displayTransactionAmount.value = calculatedTranAmt;
      	document.forms[0].FundingAmount.value = calculatedTranAmt;
      }      
  }
    
  function updateTermsDescription() {
  
    document.forms[0].PayerRefNum.value =    	  
    	  document.forms[0].PayerReference.value;
  }

function updatePayeePartyName()
{
	document.forms[0].PayeeTermsPartyName.value = 
          document.forms[0].PayeeName.value.substring(0,35);
        document.forms[0].PayerPartyContactName.value = 
          document.forms[0].PayeeName.value.substring(0,35);
}
function updatePayeeAddressLine()
{
	document.forms[0].FirstTermsPartyAddress1.value = 
          document.forms[0].PayeeAddrL1.value;
    document.forms[0].FirstTermsPartyAddress2.value = 
          document.forms[0].PayeeAddrL2.value;
    document.forms[0].FirstTermsPartyAddress3.value = 
          document.forms[0].PayeeAddrL3.value;
    document.forms[0].FirstTermsPartyAddress4.value = 
          document.forms[0].PayeeAddrL4.value;
    document.forms[0].FirstTermsPartyCountry.value = 
          document.forms[0].Country.value;

}

function selectOtherCharges()
{
  var size = document.forms[0].BankChargesType.length;
  var counter = size - 1
  while(counter >=0)
  {
  	if (document.forms[0].BankChargesType[counter].value == '<%=TradePortalConstants.CHARGE_OTHER%>')
  	{
  		document.forms[0].BankChargesType[counter].checked = true;
  	    break;
  	}
  	counter--;	
  }
}
function updateValueDate()
{
	  document.getElementById("ValueDate").innerHTML = "";        
}
</script></p>
</p>
<tr></tr>
<table></table>
<th></th>
<tr></tr>
<tr></tr>
<table></table>