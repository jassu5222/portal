<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                           Transaction Page - Footer
  Description:
    Contains logic to display the appropriate button bar footer depending on
  whether the pag eis for a template or a transaction.  Relies heavily on the
  use of variable declared by the page and the Transaction-PageMode.jsp

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-General.jsp" %>
*******************************************************************************
--%>
<%
  if (isTemplate) {
%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
      <tr>
        <td width="15" class="BankColor" nowrap>&nbsp;</td>
        <td width="100%" class="BankColor" height="34">&nbsp;</td>
        <td colspan=5 class="BankColor" height="34" nowrap width="96"> 
          <jsp:include page="/common/SaveDeleteClose.jsp">
             <jsp:param name="showSaveButton"
                        value="<%=showTemplateSave%>" />
             <jsp:param name="showDeleteButton" 
                        value="<%=showTemplateDelete%>" />
             <jsp:param name="showCloseButton" value="true" />
             <jsp:param name="showHelpButton" value="false" />
             <jsp:param name="cancelAction" 
                        value="goToInstrumentCloseNavigator" />
             <jsp:param name="saveButtonName" value="SaveTemplateButton" />
             <jsp:param name="deleteButtonName" value="DeleteTemplateButton" />
          </jsp:include>
        </td>
      </tr>
    </table>
<%
  } else {
%>
    <jsp:include page="/common/TransactionButtonBar.jsp">
      <jsp:param name="instrumentType" value="<%=instrumentType%>" />
      <jsp:param name="transactionStatus" value="<%=transactionStatus%>" />
      <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
      <%--cr498 add certAuthURL parm--%>
      <jsp:param name="certAuthURL" value="<%=certAuthURL%>" />
    </jsp:include>
<%
  }
%>

