<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Import DLC Amend Page - all sections

  Description:
    Contains HTML to create the Import DLC Amend page.  All data retrieval 
  is handled in the Transaction-IMP_DLC-AMD.jsp.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-IMP_DLC-ISS-BankDefined.jsp" %>
*******************************************************************************

--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#General">
            <%=resMgr.getText("ImportDLCAmend.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#Shipment">
            <%=resMgr.getText("ImportDLCAmend.Shipment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="14" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#OtherConditions">
            <%=resMgr.getText("ImportDLCAmend.OtherConditions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
          <a href="#BankInstructions">
            <%=resMgr.getText("ImportDLCAmend.ClientBankInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </a>
        </p>
      </td>

<%
	//Add Issue Application Form PDF Link if not in admin template mode
	//and the transaction is not started and not deleted...
	if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) && 
		(!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK)) ) {
%>
	  <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle"> 
        <p class="ControlLabel">
	       <%
	         linkArgs[0] = TradePortalConstants.PDF_DLC_AMEND_APPLICATION;
	         linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);//Use common key for encryption of parameter to doc producer
	         linkArgs[2] = "en_US";
	         //loginLocale;
	         linkArgs[3] = userSession.getBrandingDirectory(); 
	       %>
	       <%= formMgr.getDocumentLinkAsHrefInFrame(resMgr.getText("ImportDLCAmend.AmendApplicationForm",
	      							TradePortalConstants.TEXT_BUNDLE),
	      							TradePortalConstants.PDF_DLC_AMEND_APPLICATION,
	      							linkArgs,
	      							response) %>
	    </p>
	  </td>
<%
  }
%>  
      <td width="100%" height="30">&nbsp;</td>
    </tr>
  </table>

  <br>


<% 
  if (rejectionIndicator.equals(TradePortalConstants.INDICATOR_YES)
  	|| rejectionIndicator.equals(TradePortalConstants.INDICATOR_X))
  {
%>
  <%@ include file="Transaction-RejectionReason.frag" %>
  <%
  } 
%>
<% // [BEGIN] IR-YVUH032343792 - jkok %>
  <%@ include file="Transaction-Documents.frag" %>
<% // [END] IR-YVUH032343792 - jkok %>
<%--
*******************************************************************************
                     Import DLC Issue Page - General section
*******************************************************************************
--%>

  <table width=100% border=0 cellspacing=0 cellpadding=0 height=34
         class="BankColor">
    <tr class="BankColor"> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("ImportDLCAmend.General", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="General"></a>
        </p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td class=BankColor align=right valign=middle width="5" height="35" nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/amend_import_dlc.htm", "general", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap rowspan="2">&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("transaction.YourRefNumber", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td nowrap width="50">&nbsp;</td>
      <td nowrap>
         <p class="ControlLabel">
          <%=resMgr.getText("ImportDLCAmend.ApplicantName", 
                             TradePortalConstants.TEXT_BUNDLE)%>
	     </p>
      </td>
      <td width="100%">
    </tr>
    <tr>
      <td nowrap> 
        <p class="ListText">
 		  <%=instrument.getAttribute("copy_of_ref_num")%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td nowrap>
         <p class="ListText">
          <%=applicantName%>
	     </p>
      </td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <%
     if(!isProcessedByBank)
      {
   %>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("ImportDLCAmend.LCAmount", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
        </p>
      </td>
      <td width="5" nowrap>&nbsp;</td>
      <td nowrap align="right"> 
        <p class="ListText">
          <%=TPCurrencyUtility.getDisplayAmount(originalAmount.toString(), 
                                              currencyCode, loginLocale)%>
        </p>
      </td>
      <td width="25" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
<%
	}
%>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap>
        <span class="ListText">
<%
        options = Dropdown.createIncreaseDecreaseOptions(resMgr, 
                                                   incrDecrValue, loginLocale);
        defaultText = "";
        out.println(InputField.createSelectField("IncreaseDecreaseFlag", "", 
                               defaultText, options, "ListText", isReadOnly));
%>
        </span>
      </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td class="ListText" align="right"> 
        <%=InputField.createTextField("TransactionAmount",
                                      displayAmount,
                                      "30", "30", "ListTextRight", isReadOnly)%>
      </td>
      <td>&nbsp;</td>
      <td>
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
          <jsp:param name="showButton" value="<%=!isReadOnly%>" />
          <jsp:param name="name" value="CalcNewAmountButton" />
          <jsp:param name="image" value='common.CalcNewAmountImg' />
          <jsp:param name="text" value='common.CalcNewAmountText' />
          <jsp:param name="width" value="140" />
          <jsp:param name="submitButton"
                     value="<%=TradePortalConstants.BUTTON_CALCULATE%>" />
        </jsp:include>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
    <tr> 
      <td height=10></td>
    </tr>
   <%
     if(!isProcessedByBank)
      {
   %>
    <tr> 
      <td height=2></td>
      <td colspan="7" height=2></td>
      <td height=2></td>
    </tr>
<%
	}
%>
    <tr> 
      <td height=10></td>
    </tr>
   <%
     if(!isProcessedByBank)
      {
   %>
    <tr> 
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ControlLabel">
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCAmend.LCAmount", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
          <%=currencyCode%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td align="right"> 
        <p class="ListText">
<%
          if (!badAmountFound) {
             out.println(TPCurrencyUtility.getDisplayAmount(calculatedTotal.toString(), 
                                                  currencyCode, loginLocale));
          }
%>
        </p>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
<%
	}
%>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap class=ControlLabel> 
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.AmtTolerance", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=resMgr.getText("transaction.Plus", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=InputField.createTextField("AmountTolerancePlus",
                                      terms.getAttribute("amt_tolerance_pos"),
                                      "3", "3", "ListText", isReadOnly)%>
          <%=resMgr.getText("transaction.Percent", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <img src="/portal/images/Blank_15.gif" width="15" height="15">
          <%=resMgr.getText("transaction.Minus", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=InputField.createTextField("AmountToleranceMinus",
                                      terms.getAttribute("amt_tolerance_neg"),
                                      "3", "3", "ListText", isReadOnly)%>
          <%=resMgr.getText("transaction.Percent", 
                            TradePortalConstants.TEXT_BUNDLE)%>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <span class="ControlLabel">
          <%=resMgr.getText("ImportDLCAmend.CurrentExpiryDate", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      <td nowrap width="50">&nbsp;</td>
      <td nowrap> 
        <span class="ControlLabel">
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.ExpiryDate", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap> 
        <span class="ListText">
          <%=currentExpiryDate%>
        </span>
      <td nowrap width="50">&nbsp;</td>
      <td nowrap> 
        <span class="ListText">
<%
          if (isReadOnly) {
            out.println(TPDateTimeUtility.formatDate(
                                  terms.getAttribute("expiry_date"),
                                                     TPDateTimeUtility.LONG,
                                                     loginLocale));
          } else {
%>
            <%=TPDateTimeUtility.getDayDropDown("ExpiryDay", expiryDay,
                  resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE))%>
            <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
            <%=TPDateTimeUtility.getMonthDropDown("ExpiryMonth", 
                  expiryMonth, loginLocale, 
                  resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE))%>
            <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
            <%=TPDateTimeUtility.getYearDropDown("ExpiryYear", expiryYear,
                  resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE))%>
            <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
<%
          }
%>
        </span>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <br>

<%--
*******************************************************************************
                  Import DLC Amend Page - Shipment section

*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("ImportDLCAmend.Shipment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="Shipment"></a>
        </p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="right" valign="middle" width="8" height="17">&nbsp;</td>
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
                <a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
                </a>
            </td>
            <td align="left" valign="middle" width="8" height="17">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class=BankColor align=right valign=middle width=15 height=35 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/amend_import_dlc.htm", "shipment", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%" align="left" valign="middle" height="35"> 
        <p class="ListText">
          <%=resMgr.getText("ImportDLCAmend.UseGoodsDescDetails", TradePortalConstants.TEXT_BUNDLE)%>
          <a href="#GoodsDescriptionShipmentDetails">
          <%=resMgr.getText("ImportDLCAmend.GoodsDescLink", TradePortalConstants.TEXT_BUNDLE)%>
          </a>
          <%=resMgr.getText("ImportDLCAmend.AmendShipmentDetails", TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width=20 nowrap>&nbsp;</td>
      <td class="ControlLabel" align=left nowrap> 
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.LastestShipDate", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <span class="ListText">
<%
            if (isReadOnly) {
              out.println(TPDateTimeUtility.formatDate(
                                  terms.getFirstShipment().getAttribute("latest_shipment_date"),
                                                     TPDateTimeUtility.LONG,
                                                     loginLocale));
            } else {
%> 
              <%=TPDateTimeUtility.getDayDropDown("ShipmentDay", shipDay,
                    resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
              <%=TPDateTimeUtility.getMonthDropDown("ShipmentMonth", 
                    shipMonth, loginLocale, 
                    resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
              <%=TPDateTimeUtility.getYearDropDown("ShipmentYear", shipYear,
                    resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE))%>
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
<%
          }
%>
          </span>
      </td>
      <td width=20 nowrap>&nbsp;</td>
      <td nowrap>&nbsp;</td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="ControlLabel" align="left" valign="bottom"> 
          <br>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr><td nowrap class="ControlLabel">
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.ShippingTerm", 
                            TradePortalConstants.TEXT_BUNDLE)%>
            </td>
                 <td><img src="/portal/images/Blank_4x4.gif" width="50" height="4"></td>

         </tr>
          </table>
          <span class=ListText>
<%

            options = Dropdown.createSortedRefDataOptions(TradePortalConstants.INCOTERM,
                                   terms.getFirstShipment().getAttribute("incoterm"),
                                   loginLocale);
            defaultText = " ";
            out.println(InputField.createSelectField("Incoterm", "",
                                 defaultText, options, "ListText", 
                                 isReadOnly));
%>
        </span>
      </td>
      <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <table border="0" cellspacing="0" cellpadding="0">
            <tr><td nowrap class="ControlLabel">
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.ShipTermLocation", 
                            TradePortalConstants.TEXT_BUNDLE)%>
            </td></tr>
          </table>
          <%=InputField.createTextField("IncotermLocation",
                                        terms.getFirstShipment().getAttribute("incoterm_location"),
                                        "30", "30", "ListText", 
                                        isReadOnly)%>
      </td>
      <td width=100%>&nbsp;</td>
    </tr>
  </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr> 
          <td width=20 nowrap>&nbsp;</td>
          <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.FromPort", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("ShipFromPort",
                                        terms.getFirstShipment().getAttribute("shipment_from"),
                                        "65", "65", "ListText", 
                                        isReadOnly)%>
      </td>
       <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.ToDischarge", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("ShipToDischarge",
                                        terms.getFirstShipment().getAttribute("shipment_to_discharge"),
                                        "65", "65", "ListText", 
                                        isReadOnly)%>
      </td>
         <td height="55">&nbsp;</td>
    </tr>
    <tr> 
         <td width=20 nowrap>&nbsp;</td>
  	<td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.FromLoad", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("ShipFromLoad",
                                        terms.getFirstShipment().getAttribute("shipment_from_loading"),
                                        "65", "65", "ListText", 
                                        isReadOnly)%>
      </td>
         <td class="ControlLabel" align="left" valign="bottom" nowrap> 
          <img src="/portal/images/Blank_256pix.gif" width="256" height="1">
          <br>
          <%=resMgr.getText("transaction.New", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <%=resMgr.getText("ImportDLCIssue.ToPort", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=InputField.createTextField("ShipToPort",
                                        terms.getFirstShipment().getAttribute("shipment_to"),
                                        "65", "65", "ListText", 
                                        isReadOnly)%>
      </td>
          <td height="55">&nbsp;</td>
    </tr>
  </table>
  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ControlLabel">
          <%=resMgr.getText("ImportDLCAmend.NewGoodsDescription", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <a name="GoodsDescriptionShipmentDetails"></a>
        </p>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ControlLabel">
          <%=resMgr.getText("ImportDLCAmend.ChangesToGoodsDescription", TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p>
        <%=resMgr.getText("ImportDLCAmend.RemindPOChanged", TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5">&nbsp; 
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td class="ListText">&nbsp; </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
      <td class="ListText" align="left" valign="bottom"> 
        <%
           if (isReadOnly)
           {
              out.println("&nbsp;");
           }
           else
           {
              options = ListBox.createOptionList(goodsDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());

              defaultText = resMgr.getText("transaction.SelectPhrase", TradePortalConstants.TEXT_BUNDLE);

              out.println(InputField.createSelectField("GoodsPhraseItem", "", defaultText, options, "ListText", 
                                                       isReadOnly, "onChange=" + 
                                                       PhraseUtility.getPhraseChange("/Terms/ShipmentTermsList/goods_description", 
                                                       "GoodsDescText", "6500")));
        %>
              <jsp:include page="/common/PhraseLookupButton.jsp">
                <jsp:param name="imageName" value='GoodsDescButton' />
              </jsp:include>
        <%
           }
        %>
        <br>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr valign=top>
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
	<td class="FixedSize">
        <%=InputField.createTextArea("GoodsDescText", terms.getFirstShipment().getAttribute("goods_description"), "65", "10", "FixedSize", 
                                     isReadOnly)%>
      </td>
      <td width="100%">
        <p>&nbsp;</p>
      </td>
    </tr>
    <tr valign=bottom>
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
	<td class="ListText">
        <%
           userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();
           transactionOid               = transaction.getAttribute("transaction_oid");

           if (((userOrgAutoLCCreateIndicator != null) && 
                (userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && 
               ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) ||
               (userSession.hasSavedUserSession() && 
                (userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN)))))
           {
              // Compose the SQL to retrieve PO Line Items that are currently assigned to 
              // this transaction
              sqlQuery = new StringBuffer();
              sqlQuery.append("select a_source_upload_definition_oid, ben_name, currency");
              sqlQuery.append(" from po_line_item where a_assigned_to_trans_oid = ?");
			  //jgadela  R90 IR T36000026319 - SQL FIX
			  Object[] sqlParamsRepoCnt = new Object[1];
			  sqlParamsRepoCnt[0] =  transactionOid;
              // Retrieve any PO line items that satisfy the SQL query that was just constructed
              poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParamsRepoCnt);

              if (poLineItemsDoc != null)
              {
                 hasPOLineItems = true;

                 poLineItems                   = poLineItemsDoc.getFragments("/ResultSetRecord");
                 poLineItemDoc                 = (DocumentHandler) poLineItems.elementAt(0);
                 poLineItemUploadDefinitionOid = poLineItemDoc.getAttribute("/A_SOURCE_UPLOAD_DEFINITION_OID");
                 poLineItemBeneficiaryName     = poLineItemDoc.getAttribute("/BEN_NAME");
                 poLineItemCurrency            = poLineItemDoc.getAttribute("/CURRENCY");
              }

              if (!hasPOLineItems && (!(isReadOnly || isFromExpress)))
              {
        %>
                 <br>
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr align="right"> 
                     <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name" value="AddPOLineItems" />
                                <jsp:param name="image" value='common.AddPOLineItemsImg' />
                                <jsp:param name="text" value='common.AddPOLineItemsText' />
                                <jsp:param name="submitButton" value="AddPOLineItems" />
                                <jsp:param name="imageName" value='AddPOLineItemsButton' />
                                            </jsp:include>
                          <%
                               // Include currency code so that POs will be filtered
                               secureParms.put("addPO-currency", currencyCode);
                          %>
                     </td>
                   </tr>
                 </table>
        <%
              }
           }
        %>

      </td>
      <td width="100%">
        <p>&nbsp;</p>
      </td>
    </tr>
    <tr> 
      <%-- This spacer row ensures readonly mode looks good --%>
      <td width="20" height=1 nowrap></td>
      <td class="ListText" width="25" nowrap></td>
      <td class="ListText"> 
        <img src="/portal/images/Blank_517.gif" width="517" height="1">
      </td>
      <td width="100%"></td>
    </tr>
  </table>
  <%
     // It is possible that after a transaction has been processed by bank that it has
     // no POs assigned to it because these POs have been moved to amendments.  However,
     // we still want to display the PO information in the PO line items field.
     if(transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK) &&
           !InstrumentServices.isBlank(terms.getFirstShipment().getAttribute("po_line_items")))
               hasPOLineItems = true;

     if (((userOrgAutoLCCreateIndicator != null) && 
          (userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES))) && hasPOLineItems)
     {
  %>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>  
            <td width="20" nowrap>&nbsp;</td>
            <td nowrap width="15">&nbsp; </td>
            <td>&nbsp;</td>
            <td nowrap class="ListText" colspan="5"> 
              <p class="ControlLabel">
                <%=resMgr.getText("ImportDLCAmend.POLineItems", TradePortalConstants.TEXT_BUNDLE)%>
              </p>
            </td>
            <td width="15" nowrap>&nbsp;</td>
            <td class="ListText">&nbsp; </td>
            <td width="100%">&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr valign=top> 
            <td width="20" nowrap>&nbsp;</td>
            <td class="ListText" width="25" nowrap>&nbsp;</td>
            <td class="FixedSize" align="left" valign="top"> 
            <%--  Suresh Penke IR-MJUL032264649 08/11/2011 added oncontextmenu to disable mouse right click --%> 
              <%=InputField.createTextArea("POLineItems", terms.getFirstShipment().getAttribute("po_line_items"), "65", "6", 
                                           "FixedSize", isReadOnly, true, "onFocus='this.blur();' oncontextmenu='return false;' onselectstart='return false;'", "OFF")%>
	    <%--  Suresh Penke IR-MJUL032264649 08/11/2011 End  --%>
            </td>
            <td width="100%"> 
              <p>&nbsp;</p>
            </td>
          </tr>
              <%
                 if ((SecurityAccess.hasRights(loginRights, SecurityAccess.DLC_PROCESS_PO)) && 
                     (!(isReadOnly || isFromExpress)))
                 {
                   secureParms.put("addPO-uploadDefinitionOid", poLineItemUploadDefinitionOid);
                   secureParms.put("addPO-beneficiaryName", poLineItemBeneficiaryName);
                   secureParms.put("addPO-currency", poLineItemCurrency);
                  secureParms.put("addPO-shipment_oid",  terms.getFirstShipment().getAttribute("shipment_oid"));
              %>
	    <tr><td class=ListText>&nbsp;</td></tr>
          <tr> 
            <td width="20" nowrap>&nbsp;</td>
            <td class="ListText" width="25" nowrap>&nbsp;</td>
            <td align="left" valign="bottom"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="right">
                        <td width="100%">&nbsp;</td>
                        <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name" value="AddPOLineItems" />
                                <jsp:param name="image" value='common.AddPOLineItemsImg' />
                                <jsp:param name="text" value='common.AddPOLineItemsText' />
                                <jsp:param name="submitButton" value="AddPOLineItems" />
                                <jsp:param name="imageName" value='AddPOLineItemsButton' />
                              </jsp:include>
                        </td>
                        <td width="10" nowrap>&nbsp;</td>
                        <td>
                              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                                <jsp:param name="showButton" value='true' />
                                <jsp:param name="name"  value="RemovePOLineItemsButton" />
                                <jsp:param name="image" value='common.RemovePOLineItemsImg' />
                                <jsp:param name="text"  value='common.RemovePOLineItemsText' />
                                <jsp:param name="submitButton"
                                           value="RemovePOLineItemsButton" />
                                <jsp:param name="imageName" 
                                           value='RemovePOLineItemsButton' />
                              </jsp:include>
                        </td>
                      </tr>
                    </table>
            </td>
            <td width="100%"> 
              <p>&nbsp;</p>
            </td>
          </tr>
              <%
                 }
              %>
        </table>
  <%
     }
  %>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>

<%--
*******************************************************************************
                  Import DLC Amend Page - Other Conditions section

*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("ImportDLCAmend.OtherConditions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="OtherConditions"></a>
        </p>
      </td>
      <td width="90%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="right" valign="middle" width="8" height="17">&nbsp;</td>
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
                <a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
                </a>
            </td>
            <td align="left" valign="middle" width="8" height="17">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class=BankColor align=right valign=middle width=15 height=35 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/amend_import_dlc.htm", "other_cond", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
          <%=resMgr.getText("ImportDLCIssue.AddlConditions", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(addlCondDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.println(InputField.createSelectField("AddlDocsPhraseItem", "", 
                      defaultText, options, "ListText", 
                      isReadOnly,
                      "onChange=" + PhraseUtility.getPhraseChange(
                                                  "/Terms/additional_conditions",
                                                  "AddlConditionsText", "1000")));
%>
          <jsp:include page="/common/PhraseLookupButton.jsp">
            <jsp:param name="imageName" value='AddlConditionsButton' />
          </jsp:include>
<%
        }
%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
      <td width=512 class="ListText"> 
        <%=InputField.createTextArea("AddlConditionsText", 
                                     terms.getAttribute("additional_conditions"), 
                                     "100", "3", "ListText", 
                                     isReadOnly)%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <%-- This spacer row ensures readonly mode looks good --%>
      <td width="20" height=1 nowrap></td>
      <td class="ListText" width="25" nowrap></td>
      <td class="ListText"> 
        <img src="/portal/images/Blank_517.gif" width="517" height="1">
      </td>
      <td width="100%"></td>
    </tr>

  </table>

  <br>

<%--
*******************************************************************************
               Import DLC Amend Page - Bank Instructions section

*******************************************************************************
--%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class="BankColor" width="170" nowrap height="35"> 
        <p class="ControlLabelWhite">
          <span class="ControlLabelWhite">
            <%=resMgr.getText("ImportDLCAmend.ClientBankInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <a name="BankInstructions"></a>
        </p>
      </td>
      <td width="100%" class="BankColor" height="35" valign="top">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="1" class="BankColor" height="35">&nbsp;</td>
      <td width="90" class="BankColor" height="35" nowrap> 
        <table width=100% border=0 cellspacing=0 cellpadding=0 height=17>
          <tr> 
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink">
                <a href="#top" class="LinksSmall">
                  <%=resMgr.getText("common.TopOfPage", 
                                    TradePortalConstants.TEXT_BUNDLE)%>
                </a>
              </p>
            </td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap height="35">&nbsp;</td>
      <td class=BankColor align=right valign=middle width=15 height=35 nowrap>
        <%= OnlineHelp.createContextSensitiveLink("customer/amend_import_dlc.htm", "instr_to_bank", resMgr, userSession) %>
      </td>
      <td width="20" class="BankColor" nowrap height="35">&nbsp;</td>
    </tr>
  </table>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td nowrap width="15">&nbsp; </td>
      <td>&nbsp;</td>
      <td nowrap class="ListText" colspan="5"> 
        <p class="ListText">
          <%=resMgr.getText("ImportDLCIssue.InstructionsText", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap class="ListText"> 
<%
        if (isReadOnly) {
          out.println("&nbsp;");
        } else {
          options = ListBox.createOptionList(spclInstrDocList, "PHRASE_OID", "NAME", "", userSession.getSecretKey());
          defaultText = resMgr.getText("transaction.SelectPhrase",
                                       TradePortalConstants.TEXT_BUNDLE);
          out.println(InputField.createSelectField("CommInvPhraseItem", "", 
                   defaultText, options, "ListText", 
                   isReadOnly,
                   "onChange=" + PhraseUtility.getPhraseChange(
                                              "/Terms/special_bank_instructions",
                                              "SpclBankInstructions",
                                              "100")));
%>
          <jsp:include page="/common/PhraseLookupButton.jsp">
            <jsp:param name="imageName" value='SpclInstructionsButton' />
          </jsp:include>
<%
        }
%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ListText" width="25" nowrap>&nbsp;</td>
      <td class="ListText"> 
        <%=InputField.createTextArea("SpclBankInstructions", 
                                     terms.getAttribute("special_bank_instructions"), 
                                     "100", "3", "ListText", 
                                     isReadOnly)%>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <%-- This spacer row ensures readonly mode looks good --%>
      <td width="20" height=1 nowrap></td>
      <td class="ListText" width="25" nowrap></td>
      <td class="ListText"> 
        <img src="/portal/images/Blank_517.gif" width="517" height="1">
      </td>
      <td width="100%"></td>
    </tr>
  </table>

  <br>

