<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                 Loan Request Page - Payment Instructions section

  Description:
    Contains HTML to create the Loan Request Payment Instructions section.  

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transaction-LRQ-ISS-PaymentInstructions.jsp" %>
*******************************************************************************
--%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="15" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" width="170" nowrap> 
        <p class="ControlLabelWhite">
		  <span class="ControlLabelWhite">
		    <%=resMgr.getText("LoanRequest.PaymentInstructions", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  </span>
		  <a name="PaymentInstructions"></a>
		</p>
      </td>
      <td width="90%" class="BankColor" valign="top">&nbsp;</td>
      <td width="1" class="BankColor">&nbsp;</td>
      <td width="1" class="BankColor">&nbsp;</td>
      <td width="90" class="BankColor" nowrap> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td align="right" valign="middle" width="8" height="17">&nbsp;</td>
            <td align="center" valign="middle" height="17"> 
              <p class="ButtonLink"><a href="#top" class="LinksSmall">Top of Page</a></p>
            </td>
            <td align="left" valign="middle" width="8" height="17">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" align="right" valign="middle" width="15" nowrap>
		<%= OnlineHelp.createContextSensitiveLink("customer/issue_loan_request.htm", "payment_instructions", resMgr, userSession) %>
	  </td>
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="ColorGrey">
        <p class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.LoanProceeds", 
                            TradePortalConstants.TEXT_BUNDLE)%>
		</p>
      </td>
      <td class="ColorGrey">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td>
	    <span class="ControlLabel">
	      <%=resMgr.getText("LoanRequest.CcyOfPayment", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  <%=getRequiredIndicator(!isTemplate)%>
		</span>
        <br>
<%
               options = Dropdown.createSortedCurrencyCodeOptions(
                      loanProceedsPmtCurrency, loginLocale);
               out.println(InputField.createSelectField("LoanProceedsPmtCurr", "",
                      " ", options, "ListText", isReadOnly));
%>
        <br>
        <img src="images/2x2_blank.gif" width="120" height="1"> </td>
      <td width="895">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.LoanProceedsAmount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<span class="ListText"> 
        </span>
		<br>
        <%=InputField.createTextField("LoanProceedsPmtAmount",
                                      displayLoanProceedsPmtAmount,
                                      "22", "22", "ListTextRight", isReadOnly)%>
        <br>
        <img src="images/2x2_blank.gif" width="256" height="1"> </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="2" nowrap>
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.ApplyLoanProceedsTo", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  <%=getRequiredIndicator(!isTemplate)%>
		</span>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td width="20" nowrap> 
              <%=InputField.createRadioButtonField("LoanProceedsCreditType",
                     TradePortalConstants.CREDIT_REL_INST, "", 
                     loanProceedsCreditType.equals(TradePortalConstants.CREDIT_REL_INST),
                     "ListText", "", isReadOnly)%>
      </td>
      <td class="ListText">
	    <%=resMgr.getText("LoanRequest.RelatedInstruments", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="ListText">
	    <img src="images/2x2_blank.gif" width="400" height="10">
		<br>
        <%=resMgr.getText("LoanRequest.ForASingleRelatedInstrument", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="ControlLabel">
	    <%=resMgr.getText("LoanRequest.RelatedInstrumentID", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
          <%
             if (InstrumentServices.isBlank(instrumentID))
             {
                instrumentID = terms.getAttribute("related_instrument_id");
             }
	  %>
          <%=InputField.createTextField("RelatedInstrumentID", instrumentID, "16", 
                                        "16", "ListText", isReadOnly, "onChange='pickRelatedInstrumentRadio();'")%>
										
        <img src="/portal/images/2x2_blank.gif" width="8" height="8"> 
        <%
           if (!isReadOnly)
           {
        %>
              <jsp:include page="/common/RolloverButtonSubmit.jsp">
                 <jsp:param name="showButton"   value="<%=!isReadOnly%>" />
                 <jsp:param name="name"         value="<%=TradePortalConstants.BUTTON_INSTRUMENT_SEARCH%>" />
                 <jsp:param name="image"        value='common.SearchInstrumentsImg' />
                 <jsp:param name="text"         value='common.SearchInstrumentsText' />
                 <jsp:param name="width"        value="230" />
                 <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_INSTRUMENT_SEARCH%>" />
                 <jsp:param name="imageName"    value="SearchInstrumentsButton" />
              </jsp:include>
        <%
           }
        %>
	  </td>	
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="ListText">
	    <img src="images/2x2_blank.gif" width="400" height="10">
		<br>
        <%=resMgr.getText("LoanRequest.ForMultipleInstruments", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
        <%=InputField.createTextArea("MultipleRelatedInstruments", 
                                     terms.getAttribute("multiple_related_instruments"), 
                                     "100", "3", "ListText", isReadOnly, "onChange='pickRelatedInstrumentRadio();'")%>
        <br>
        <img src="images/2x2_blank.gif" width="400" height="10"> </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td> 
        <%=InputField.createRadioButtonField("LoanProceedsCreditType",
                     TradePortalConstants.CREDIT_OUR_ACCT, "", 
                     loanProceedsCreditType.equals(TradePortalConstants.CREDIT_OUR_ACCT),
                     "ListText", "", isReadOnly)%>
      </td>
      <td class="ListText">
	    <%=resMgr.getText("LoanRequest.OurAccount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="ControlLabel">
	    <%=resMgr.getText("LoanRequest.AccountNumber", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
<%
        // Using the acct_choices xml from the app party,build the dropdown and 
        // select the one matching loan_proceeds_credit_acct
        acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("loan_proceeds_credit_acct"));

        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
        options = Dropdown.createSortedAcctOptions(acctOptions, acctNum, loginLocale);
        out.println(InputField.createSelectField("LoanProceedsCreditAccount", "", " ", 
                                                 options, "ListText", isReadOnly, "onChange='pickOurAccountRadio();'")); 
%>        
        <br>
        <img src="images/2x2_blank.gif" width="400" height="10"> </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td> 
        <%=InputField.createRadioButtonField("LoanProceedsCreditType",
                     TradePortalConstants.CREDIT_BEN_ACCT, "", 
                     loanProceedsCreditType.equals(TradePortalConstants.CREDIT_BEN_ACCT),
                     "ListText", "", isReadOnly)%>
      </td>
      <td class="ListText">
	    <%=resMgr.getText("LoanRequest.TheFollowingBeneficiary", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td nowrap>&nbsp;</td>
            <td class="ColorGrey" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.Beneficiary", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  </p>
            </td>
            <td width="120" nowrap class="ColorGrey">&nbsp;</td>
            <td align="center" class="ColorGrey" colspan="2">&nbsp;</td>
            <td width="100%" class="ColorGrey">&nbsp;</td>
          </tr>
<%
    if (!(isReadOnly)) {
%>		  
          <tr> 
            <td nowrap>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td width="120" nowrap><img src="images/2x2_blank.gif" width="53" height="8"></td>
            <td align="center" colspan="2">&nbsp;</td>
            <td width="100%">&nbsp;</td>
          </tr>
          <tr> 
            <td nowrap>&nbsp;</td>
            <td colspan="2">
          <jsp:include page="/common/PartySearchButton.jsp">
             <jsp:param name="showButton" 
                        value="<%=!(isReadOnly)%>" />
             <jsp:param name="name" value="SearchBenButton" />
             <jsp:param name="image" value='common.SearchBeneficiaryImg' />
             <jsp:param name="text" value='common.SearchText' />
             <jsp:param name="width" value="140" />
             <jsp:param name="partyType" 
                        value="<%=TradePortalConstants.BENEFICIARY%>" />
          </jsp:include>
        </td>
        <td width="120" nowrap>&nbsp;</td>
        <td align="left" colspan="2">
          <jsp:include page="/common/RolloverButtonSubmit.jsp">
            <jsp:param name="showButton" value="<%=!isReadOnly%>" />
            <jsp:param name="name" value="ClearBenButton" />
            <jsp:param name="image" value='common.ClearBeneficiaryImg' />
            <jsp:param name="text" value='common.ClearBeneficiaryText' />
            <jsp:param name="width" value="152" />
            <jsp:param name="submitButton"
                       value="<%=TradePortalConstants.BUTTON_CLEAR%>" />
            </jsp:include>
          </td>			
          <td width="100%">&nbsp;</td>
          </tr>
<%
    }

    // Always send the ben terms party oid
    secureParms.put("ben_terms_party_oid", 
                    termsPartyBen.getAttribute("terms_party_oid"));
%>
          <tr> 
            <td>&nbsp;</td>
            <td class="ControlLabel" colspan="2">
			  <input type=hidden name='BenTermsPartyType' 
                value=<%=TradePortalConstants.BENEFICIARY%>>
              <input type=hidden name="BenOTLCustomerId"
                value="<%=termsPartyBen.getAttribute("OTL_customer_id")%>">
              <input type=hidden name="VendorId"
                value="<%=termsPartyBen.getAttribute("vendor_id")%>">
              <img src="/portal/images/images/Blank_256pix.gif" width="256" height="1">
              <%=resMgr.getText("LoanRequest.BeneName", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>
				<%=InputField.createTextField("BenName",
											  termsPartyBen.getAttribute("name"),
											  "35", "35", "ListText",
											  isReadOnly, "onBlur='checkBeneficiaryName(\"" + 
											  StringFunction.escapeQuotesforJS(termsPartyBen.getAttribute("name")) + "\"); pickTheFollowingBeneficiaryRadio();'")%>
            </td>
            <td>&nbsp;</td>
            <td colspan="2" class="ListText"> 
              <p class="ControlLabel">
			    <img src="/portal/images/images/Blank_256pix.gif" width="256" height="1">
              </p>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td class="ListText" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneAddressLine1", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
				  <%=InputField.createTextField("BenAddressLine1",
										  termsPartyBen.getAttribute("address_line_1"),
										  "35", "35", "ListText",
										  isReadOnly, "onBlur='pickTheFollowingBeneficiaryRadio();'")%>
              </p>
            </td>
            <td>&nbsp;</td>
            <td class="ListText" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneAddressLine2", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
				  <%=InputField.createTextField("BenAddressLine2",
										  termsPartyBen.getAttribute("address_line_2"),
										  "35", "35", "ListText", 
										  isReadOnly)%>
              </p>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td class="ListText"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneCity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
				  <%=InputField.createTextField("BenCity",
										  termsPartyBen.getAttribute("address_city"),
										  "23", "23", "ListText", 
										  isReadOnly, "onBlur='pickTheFollowingBeneficiaryRadio();'")%>
              </p>
            </td>
            <td class="ControlLabel">
			  <%=resMgr.getText("LoanRequest.BeneProvinceState", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>
				<%=InputField.createTextField("BenStateProvince",
								  termsPartyBen.getAttribute("address_state_province"),
								  "8", "8", "ListText", isReadOnly, "onBlur='pickTheFollowingBeneficiaryRadio();'")%>
            </td>
            <td>&nbsp;</td>
            <td class="ListText"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneCountry", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
<%
          options = Dropdown.createSortedRefDataOptions(
                      TradePortalConstants.COUNTRY, 
                      termsPartyBen.getAttribute("address_country"), 
                      loginLocale);
          out.println(InputField.createSelectField("BenCountry", "",
                      " ", options, "ListText", isReadOnly, "onBlur='pickTheFollowingBeneficiaryRadio();'"));
%>
              </p>
            </td>
            <td class="ControlLabel" nowrap> &nbsp;&nbsp;
			  <%=resMgr.getText("LoanRequest.BenePostalCode", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>
			    <%-- IR FAUH110933221 Krishna 11/15/2007 added 2 blank spaces(&nbsp)--%>
			    &nbsp;&nbsp;
				<%=InputField.createTextField("BenPostalCode",
									termsPartyBen.getAttribute("address_postal_code"),
									"15", "15", "ListText", 
									isReadOnly, "onBlur='pickTheFollowingBeneficiaryRadio();'")%>
            </td>
            <td>&nbsp;</td>
          </tr>
		  <tr> 
            <td nowrap>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td width="120" nowrap><img src="images/2x2_blank.gif" width="53" height="8"></td>
            <td align="center" colspan="2">&nbsp;</td>
            <td width="100%">&nbsp;</td>
          </tr>

       </table>
		
<%
  // Logic to handle beneficiary account list.  If no beneficiary has been searched, there is
  // only an enterable account number field.  Otherwise, we display the set of accounts defined
  // for the beneficiary (when the party is selected, we get the set of accounts for that party 
  // and save it on the TermsParty record -- if the party's accounts change later, we do not update
  // the set on the TermsParty record.) as well as an enterable account number field.  The account 
  // number selected by the user will be one of the beneficiary's predefined accounts or the 
  // entered account number.

  benAcctList = StringFunction.xssHtmlToChars(termsPartyBen.getAttribute("acct_choices"));
  DocumentHandler acctData = new DocumentHandler(benAcctList, true);
  DocumentHandler acct;
  boolean foundAcctMatch = false;	// indicates if ANY predefined acct num matches ben. acct
					//     number from business object
  boolean foundMatch = false;		// indicates if a given acct num matches ben. acct
					//     number from business object
  int numAccts, i;
  Vector accounts = acctData.getFragments("/DocRoot/ResultSetRecord");
  numAccts = accounts.size();

  Debug.debug("Account Choices for Ben Party: " + benAcctList);
  Debug.debug("Number of account in list is " + numAccts);

  // Set the showBenAccts flag that determines if a list of accounts with radio buttons
  // appears.  They appear if we have a set of accounts (i.e., it's been preloaded into
  // a benAcctList variable).  However, in readonly mode, we do not display the list.

  if (numAccts > 0) showBenAccts = true;
  if (isReadOnly) showBenAccts = false;

  Debug.debug("showBenAccts flag is " + showBenAccts);

  // Store the set of accounts for the beneficiary in a hidden field.  This gets written to the
  // TermsParty record so we always have the set of accounts at the time the party was selected.
	if(null != benAcctList && !"".equals(benAcctList)){
	%>
  		<input type=hidden value='<%=benAcctList%>' name=BenAcctChoices>
  	<%} %>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
    // Peform selective logic throughout that determines how this section is displayed -- as
    // a set of radio buttons and accounts, or just a single enterable account.
    if (showBenAccts) {
%>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="4">
        <span class="ListText">
          <%=resMgr.getText("LoanRequest.CreditAccountText", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </span>
        <%=getRequiredIndicator(!isTemplate)%>
      </td>
    </tr>
<%
    }
%>
    <tr> 
    <td width="20" nowrap>&nbsp;</td> 
      <%=showBenAccts?"<td width=24 nowrap>&nbsp;</td>":""%>
      <td class="ControlLabel">
        <%=resMgr.getText("LoanRequest.BeneficiaryAccountNumber", 
                          TradePortalConstants.TEXT_BUNDLE)%>
      </td>
      <td width="95" class="ControlLabel">&nbsp;</td>
      <td width="100%" class="ControlLabel">&nbsp;</td>
    </tr>

<%
    if (showBenAccts) {
      // This loop prints the set of accounts previously determined when the party was
      // selected.
      for (i = 0; i<numAccts; i++) {
        acct = (DocumentHandler) accounts.elementAt(i);
%>
    
        <tr> 
          <td width="20" nowrap>&nbsp;</td>
<%
            // Display a radio button using the "account number/currency" from the
            // account list (for whichever row we're on).  Select the correct
            // radio button if we match on the current value of "acct_num" on the
            // beneficiary party.
            out.println("<td width=24 nowrap>");

            // (Don't know why, but the first "get" from the doc must be without the /,
            // succeeding ones must be with a /.  This is different from similar logic
            // elsewhere.  Believed to be a symptom of flattening a DocHandler to a string
            // and then creating another DocHandler from the string.)

            acctNum = acct.getAttribute("ACCOUNT_NUMBER");       // no "slash"
            acctCcy = acct.getAttribute("/CURRENCY");            // get with "slash"

            foundMatch = acctNum.equals(termsPartyBen.getAttribute("acct_num"));

            if (!foundAcctMatch) {
              foundAcctMatch = foundAcctMatch | foundMatch;
            }
            out.println(InputField.createRadioButtonField("SelectedAccount",
                         acctNum + TradePortalConstants.DELIMITER_CHAR + acctCcy, "", 
                         foundMatch, "ListText", "", isReadOnly, "onClick='pickTheFollowingBeneficiaryRadio();'"));
            out.println("</td>");
%>
          <td> 
            <p class="ListText">
<%
		  if (acctCcy.equals("")) {
%>
              <%=acctNum%>
<%		  
		  } else {
%>
              <%=acctNum + " (" + acctCcy + ")"%>
<%
		  }
%>
            </p>
          </td>
          <td width="95" class="ListText">&nbsp;</td>
          <td width="100%" class="ListText">&nbsp;</td>
        </tr>
<%
      // End of for loop
      }
    } // end if showBenAccts
%>
    <tr> 
      <td width="3" nowrap>&nbsp;</td>
<%
      // This IF sets up acctNum and acctCcy fields with values if necessary and uses them
      // for displaying the enterable account field.

      if (showBenAccts) {
        // Display radio button for selection of the enterable account.  If we haven't found
        // an account match yet, then we might match on the enterable value.  If they match,
        // turn on the radio button.
        if (foundAcctMatch) {
          acctNum = "";
          acctCcy = "";
          foundMatch = false;
        } else {
          acctNum = termsPartyBen.getAttribute("acct_num");
          acctCcy = termsPartyBen.getAttribute("acct_currency");
          if (acctNum == null) acctNum = "";
          foundMatch = acctNum.equals(termsPartyBen.getAttribute("acct_num"));
        }
%>
        <td width="24"> 
          <%=InputField.createRadioButtonField("SelectedAccount",
                       TradePortalConstants.USER_ENTERED_ACCT, "", 
                       foundMatch, "ListText", "", isReadOnly, "onClick='pickTheFollowingBeneficiaryRadio();'")%>
        </td>
<%
      } else {
        // We're not displaying the account list, so populate the account field with the
        // account from the beneficiary party record.
        acctNum = termsPartyBen.getAttribute("acct_num");
        acctCcy = termsPartyBen.getAttribute("acct_currency");
        if (acctNum == null) acctNum = "";
      }
%>
      <td> 
        <%=InputField.createTextField("EnteredAccount",
                                      acctNum,
                                      "30", "30", "ListText", isReadOnly, "onChange='pickTheFollowingBeneficiaryRadio();'")%> 

      </td>
      <td width="95" class="ListText"> 
<%
//          options = Dropdown.createSortedCurrencyCodeOptions(
//                      acctCcy, loginLocale);
//          out.println(InputField.createSelectField("EnteredCurrency", "",
//                      " ", options, "ListText", isReadOnly, "onChange='pickTheFollowingBeneficiaryRadio();'"));
         out.println("&nbsp;");
%>
      </td>
      <td width="100%" class="ListText">&nbsp;</td>
    </tr>

    <tr>
      <%-- Spacer to keep table aligned in read-only mode --%>
      <td width="3" nowrap>&nbsp;</td>
      <%=showBenAccts?"<td width=24>&nbsp;</td>":""%>
      <td>
        <img src="images/Blank_256pix.gif" width="217" height="8">
      </td>
      <td width="95" class="ListText">
        <img src="images/Blank_256pix.gif" width="95" height="1">
      </td>
      <td width="100%" class="ListText">
        <img src="images/Blank_256pix.gif" width="256" height="1">
      </td>
    </tr>
  </table>

      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
            <td nowrap>&nbsp;</td>
            <td class="ColorGrey" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BankBeneficiary", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  </p>
            </td>
            <td width="120" nowrap class="ColorGrey">&nbsp;</td>
            <td align="center" class="ColorGrey" colspan="2">&nbsp;</td>
            <td width="100%" class="ColorGrey">&nbsp;</td>
          </tr>
		  <tr> 
            <td nowrap>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td width="120" nowrap><img src="images/2x2_blank.gif" width="53" height="8"></td>
            <td align="center" colspan="2">&nbsp;</td>
            <td width="100%">&nbsp;</td>
          </tr>
<%
    if (!(isReadOnly)) {
%>
          <tr> 
            <td nowrap>&nbsp;</td>
            <td colspan="2">
			  <jsp:include page="/common/PartySearchButton.jsp">
				 <jsp:param name="showButton" 
							value="<%=!(isReadOnly)%>" />
				 <jsp:param name="name" value="SearchBbkButton" />
				 <jsp:param name="image" value='common.SearchBeneficiaryBankImg' />
				 <jsp:param name="text" value='common.SearchText' />
				 <jsp:param name="width" value="140" />
				 <jsp:param name="partyType" 
							value="<%=TradePortalConstants.BENEFICIARY_BANK%>" />
			  </jsp:include>
		  	</td>            
			<td width="120" nowrap>
			  <img src="images/2x2_blank.gif" width="53" height="8">
			</td>
            <td colspan="2">&nbsp;</td>
            <td width="100%">&nbsp;</td>
          </tr>
<%
    }

    // Always send the bbk terms party oid
    secureParms.put("bbk_terms_party_oid", 
                    termsPartyBbk.getAttribute("terms_party_oid"));
%>
          <tr> 
            <td nowrap>&nbsp;</td>
			  <input type=hidden name='BbkTermsPartyType' 
                value=<%=TradePortalConstants.BENEFICIARY_BANK%>>
              <input type=hidden name="BbkOTLCustomerId"
                value="<%=termsPartyBbk.getAttribute("OTL_customer_id")%>">
            <td colspan="2">
              <p class="ControlLabel">
			        <%=resMgr.getText("LoanRequest.BeneBankName", 
                          TradePortalConstants.TEXT_BUNDLE)%>
        			<br>
        			<%=InputField.createTextField("BbkName",
                                      termsPartyBbk.getAttribute("name"),
                                      "35", "35", "ListText",
                                      isReadOnly || isFromExpress, "onBlur='checkBeneficiaryBankName(\"" + 
                                      termsPartyBbk.getAttribute("name") + "\")'")%>
			  </p>
			</td>
            <td width="120" nowrap>&nbsp;</td>
            <td align="center" colspan="2">&nbsp;</td>
            <td width="100%">&nbsp;</td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td class="ListText" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneBankAddressLine1", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
				  <%=InputField.createTextField("BbkAddressLine1",
										  termsPartyBbk.getAttribute("address_line_1"),
										  "35", "35", "ListText",
										  isReadOnly)%>
              </p>
            </td>
            <td>&nbsp;</td>
            <td class="ListText" colspan="2"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneBankAddressLine2", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
				  <%=InputField.createTextField("BbkAddressLine2",
										  termsPartyBbk.getAttribute("address_line_2"),
										  "35", "35", "ListText", 
										  isReadOnly)%>
              </p>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            
		  <td>&nbsp;</td>
            
          <td class="ListText">
		    <p class="ControlLabel">
			  <%=resMgr.getText("LoanRequest.BeneBankCity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>
			  <%=InputField.createTextField("BbkCity",
									  termsPartyBbk.getAttribute("address_city"),
									  "23", "23", "ListText", 
									  isReadOnly)%>
            </p>
            </td>
            <td class="ControlLabel">
			  <%=resMgr.getText("LoanRequest.BeneBankProvinceState", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>
				<%=InputField.createTextField("BbkStateProvince",
								  termsPartyBbk.getAttribute("address_state_province"),
								  "8", "8", "ListText", isReadOnly)%>
            </td>
            <td>&nbsp;</td>
            <td class="ListText"> 
              <p class="ControlLabel">
			    <%=resMgr.getText("LoanRequest.BeneBankCountry", 
                              TradePortalConstants.TEXT_BUNDLE)%>
				<br>
		<%
					options = Dropdown.createSortedRefDataOptions(
								TradePortalConstants.COUNTRY, 
								termsPartyBbk.getAttribute("address_country"), 
								loginLocale);
					out.println(InputField.createSelectField("BbkCountry", "",
								" ", options, "ListText", isReadOnly));
		%>
              </p>
            </td>
            <td class="ControlLabel" nowrap> &nbsp;&nbsp;
			  <%=resMgr.getText("LoanRequest.BeneBankPostalCode", 
                              TradePortalConstants.TEXT_BUNDLE)%>
			  <br>	    
			    <%-- IR FAUH110933221 Krishna 11/15/2007 added 2 blank spaces(&nbsp)--%>
			    &nbsp;&nbsp;
				<%=InputField.createTextField("BbkPostalCode",
									termsPartyBbk.getAttribute("address_postal_code"),
									"15", "15", "ListText", 
									isReadOnly)%>
            </td>
            <td>&nbsp;</td>
          </tr>
		<tr> 
		  <%-- Spacer to keep table aligned in read-only mode --%>
		  <td>&nbsp;</td>
		  <td class="ControlLabel" colspan="2">
			<img src="/portal/images/Blank_256pix.gif" width="256" height="1">
		  </td>
		  <td>&nbsp;</td>
		  <td class="ControlLabel" colspan="2" valign=bottom>
			<img src="/portal/images/Blank_256pix.gif" width="256" height="1">
		  </td>
		  <td>&nbsp;</td>
		</tr>  
        </table>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td> 
        <%=InputField.createRadioButtonField("LoanProceedsCreditType",
                     TradePortalConstants.CREDIT_OTHER_ACCT, "", 
                     loanProceedsCreditType.equals(TradePortalConstants.CREDIT_OTHER_ACCT),
                     "ListText", "", isReadOnly)%>
      </td>
      <td class="ListText">
	    <%=resMgr.getText("LoanRequest.Other1", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td> 
        <%=InputField.createTextArea("LoanProceedsCreditOtherText", 
                                     terms.getAttribute("loan_proceeds_credit_other_txt"), 
                                     "100", "3", "ListText", isReadOnly, "onChange='pickApplyProceedsToOtherRadio();'")%>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr valign="bottom"> 
      <td width="20"><img src="images/2x2_blank.gif" width="8" height="30"></td>
      <td colspan="2">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.ExchangeRateDetails", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
	  </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (proceedsBankToBook.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("ProceedsBankToBook",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>
	  </td>
      <td width="255" class="ListText">
	    <%=resMgr.getText("LoanRequest.BankToBook", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (proceedsUseFEC.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("ProceedsUseFEC",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>
      </td>
      <td width="255" class="ListText">
	    <%=resMgr.getText("LoanRequest.FEC", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td width="255">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECCovered", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
          <%=InputField.createTextField("ProceedsCoveredByFECNumber",
                                      terms.getAttribute("covered_by_fec_number"),
                                      "30", "14", "ListText", isReadOnly, "onChange='pickLoanProceedsFECCheckbox();'")%>
      </td>
      <td width="27">&nbsp;</td>
      <td width="648">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECAmount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	    </span>
		<span class="ListText">
		  <%=resMgr.getText("LoanRequest.FECSettlement", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
              <%=InputField.createTextField("SettlementFECAmount",
                                      displaySettlementFECAmount,
                                      "22", "22", "ListTextRight", isReadOnly, "onChange='pickLoanProceedsFECCheckbox();'")%>
      </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td width="255">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECRate", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
              <%=InputField.createTextField("SettlementFECRate",
                                      displaySettlementFECRate,
                                      "22", "13", "ListTextRight", isReadOnly, "onChange='pickLoanProceedsFECCheckbox();'")%>
        <br>
        <img src="images/Blank_256pix.gif" width="256" height="1"> </td>
      <td width="27">&nbsp;</td>
      <td width="648">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECMaturity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
<%
          if (isReadOnly) {
            out.println(TPDateTimeUtility.formatDate(
                                  terms.getAttribute("fec_maturity_date"),
                                                     TPDateTimeUtility.LONG,
                                                     loginLocale));
          } else {
%>
              <%=TPDateTimeUtility.getDayDropDown("FECMaturityDay", fecMaturityDay,
                  resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE), "onChange='pickLoanProceedsFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15">
			  <%=TPDateTimeUtility.getMonthDropDown("FECMaturityMonth", 
                  fecMaturityMonth, loginLocale, 
                  resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE), "onChange='pickLoanProceedsFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15">
			  <%=TPDateTimeUtility.getYearDropDown("FECMaturityYear", fecMaturityYear,
                  resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE), "onChange='pickLoanProceedsFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
<%
          }
%>
        <br>
        <img src="images/Blank_256pix.gif" width="256" height="1"> </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (proceedsUseOther.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("ProceedsUseOther",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>
	  </td>
      <td colspan="3" class="ListText">
	    <%=resMgr.getText("LoanRequest.Other2", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td colspan="3"> 
        <%=InputField.createTextArea("UseOtherText", 
                                     terms.getAttribute("use_other_text"), 
                                     "100", "3", "ListText", isReadOnly, "onChange='pickLoanProceedsFECOtherCheckBox();'")%>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1" nowrap>&nbsp;</td>
      <td colspan="3">&nbsp;</td>
      <td nowrap width="1">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
      <td width="33">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td colspan="4" nowrap class="ColorGrey"> 
        <p class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.LoanMaturity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</p>
      </td>
      <td nowrap class="ColorGrey" width="448">&nbsp;</td>
      <td class="ColorGrey" colspan="2">&nbsp;</td>
      <td width="72" class="ColorGrey">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1">&nbsp;</td>
      <td valign="top" colspan="3">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.AtLoanMaturity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  <%=getRequiredIndicator(!isTemplate)%>
		</span>
      </td>
      <td width="1">&nbsp;</td>
      <td nowrap class="ListText" colspan="2">&nbsp;</td>
      <td width="33">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1">&nbsp;</td>
      <td valign="top" width="24"> 
              <%=InputField.createRadioButtonField("LoanMaturityDebitType",
                     TradePortalConstants.DEBIT_APP, "", 
                     loanMaturityDebitType.equals(TradePortalConstants.DEBIT_APP),
                     "ListText", "", isReadOnly)%>
	  </td>
      <td nowrap colspan="2" class="ListText">
	    <%=resMgr.getText("LoanRequest.DebitTheApplicantsAccount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="1">&nbsp;</td>
      <td nowrap class="ListText" colspan="2">&nbsp;</td>
      <td width="33">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1" height="48">&nbsp;</td>
      <td valign="top" width="24" height="48">&nbsp;</td>
      <td nowrap width="305" valign="top" height="48">
	    <span class="ControlLabel">
	      <%=resMgr.getText("LoanRequest.AccountNumber", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
<%
        // Using the acct_choices xml from the app party,build the dropdown and 
        // select the one matching loan_maturity_debit_acct
        acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("loan_maturity_debit_acct"));

        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
        acctOptions = new DocumentHandler(appAcctList, true);
        options = Dropdown.createSortedAcctOptions(acctOptions, acctNum, loginLocale);
        out.println(InputField.createSelectField("LoanMaturityDebitAccount", "", " ", 
                                                 options, "ListText", isReadOnly, "onChange='pickDebitApplicantsAccountRadio();'"));
%>              </td>
      <td nowrap width="2" height="48">&nbsp;</td>
      <td width="1" height="48">&nbsp;</td>
      <td nowrap class="ListText" colspan="2" height="48">&nbsp;</td>
      <td width="33" height="48">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1">&nbsp;</td>
      <td width="24"> 
              <%=InputField.createRadioButtonField("LoanMaturityDebitType",
                     TradePortalConstants.DEBIT_OTHER, "", 
                     loanMaturityDebitType.equals(TradePortalConstants.DEBIT_OTHER),
                     "ListText", "", isReadOnly)%>
      </td>
      <td nowrap align="left" colspan="2" class="ListText">
	      <%=resMgr.getText("LoanRequest.LoanMaturityOther", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="1">&nbsp;</td>
      <td nowrap class="ListText" colspan="2">&nbsp;</td>
      <td width="33">&nbsp;</td>
    </tr>
    <tr> 
      <td width="1" height="63">&nbsp;</td>
      <td valign="top" width="24" height="63">&nbsp; </td>
      <td nowrap align="left" valign="top" colspan="5" height="63"> 
        <p class="ListText"> 
        <%=InputField.createTextArea("LoanMaturityDebitOtherText", 
                                     terms.getAttribute("loan_maturity_debit_other_txt"), 
                                     "100", "3", "ListText", isReadOnly, "onChange='pickDebitOtherRadio();'")%>
        </p>
      </td>
      <td width="33" height="63">&nbsp;</td>
    </tr>
  
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr valign="bottom"> 
      <td width="20"><img src="images/2x2_blank.gif" width="8" height="30"></td>
      <td colspan="2">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.ExchangeRateDetails", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
      </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (maturityBankToBook.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("MaturityBankToBook",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>
	  </td>
      <td width="255" class="ListText">
	    <%=resMgr.getText("LoanRequest.BankToBook", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (maturityUseFEC.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("MaturityUseFEC",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>
	  </td>
      <td width="255" class="ListText">
	    <%=resMgr.getText("LoanRequest.FEC", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
      <td width="27">&nbsp;</td>
      <td width="648">&nbsp;</td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td width="255">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECCovered", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
          <%=InputField.createTextField("MaturityCoveredByFECNumber",
                                      terms.getAttribute("maturity_covered_by_fec_number"),
                                      "30", "14", "ListText", isReadOnly, "onChange='pickMaturityFECCheckbox();'")%>
      </td>
      <td width="27">&nbsp;</td>
      <td width="648">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECAmount", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<span class="ListText">
		  <%=resMgr.getText("LoanRequest.FECLoan", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
              <%=InputField.createTextField("LoanFECAmount",
                                      displayLoanFECAmount,
                                      "22", "22", "ListTextRight", isReadOnly, "onChange='pickMaturityFECCheckbox();'")%>
      </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td width="255">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECRate", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
              <%=InputField.createTextField("MaturityFECRate",
                                      displayMaturityFECRate,
                                      "22", "13", "ListTextRight", isReadOnly, "onChange='pickMaturityFECCheckbox();'")%>
        <br>
        <img src="images/Blank_256pix.gif" width="256" height="1"> </td>
      <td width="27">&nbsp;</td>
      <td width="648">
	    <span class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.FECMaturity", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</span>
		<br>
<%
          if (isReadOnly) {
            out.println(TPDateTimeUtility.formatDate(
                                  terms.getAttribute("maturity_fec_maturity_date"),
                                                     TPDateTimeUtility.LONG,
                                                     loginLocale));
          } else {
%>
              <%=TPDateTimeUtility.getDayDropDown("MaturityFECMaturityDay", maturityFECMaturityDay,
                  resMgr.getText("common.Day", TradePortalConstants.TEXT_BUNDLE), "onChange='pickMaturityFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15">
			  <%=TPDateTimeUtility.getMonthDropDown("MaturityFECMaturityMonth", 
                  maturityFECMaturityMonth, loginLocale, 
                  resMgr.getText("common.Month", TradePortalConstants.TEXT_BUNDLE), "onChange='pickMaturityFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15">
			  <%=TPDateTimeUtility.getYearDropDown("MaturityFECMaturityYear", maturityFECMaturityYear,
                  resMgr.getText("common.Year", TradePortalConstants.TEXT_BUNDLE), "onChange='pickMaturityFECCheckbox();'")%> 
              <img src="/portal/images/Blank_15.gif" width="15" height="15"> 
<%
          }
%>
        <br>
        <img src="images/Blank_256pix.gif" width="256" height="1"> </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24"> 
	  <%
             if (maturityUseOther.equals(TradePortalConstants.INDICATOR_YES))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  
			 out.print(InputField.createCheckboxField("MaturityUseOther",
													  TradePortalConstants.INDICATOR_YES, "", defaultCheckBoxValue,
													  "ListText", "ListText",isReadOnly));  
	  %>      </td>
      <td colspan="3" class="ListText">
	    <%=resMgr.getText("LoanRequest.Other2", 
                              TradePortalConstants.TEXT_BUNDLE)%>
	  </td>
    </tr>
    <tr> 
      <td width="20">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td colspan="3"> 
        <%=InputField.createTextArea("MaturityUseOtherText", 
                                     terms.getAttribute("maturity_use_other_text"), 
                                     "100", "3", "ListText", isReadOnly, "onChange='pickMaturityFECOtherCheckBox();'")%>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td class="ColorGrey" nowrap> 
        <p class="ControlLabel">
		  <%=resMgr.getText("LoanRequest.ChargesAndInterest", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		</p>
      </td>
      <td width="100%" class="ColorGrey">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="ListText" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td align="left" valign="bottom" nowrap> 
        <p class="ControlLabel">
		  <img src="images/Blank_256pix.gif" width="256" height="1">
		  <br>
          <%=resMgr.getText("LoanRequest.ApplicantsDebitAccountForCharges", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  <%=getRequiredIndicator(!isTemplate)%>
		  <br>
<%
        // Using the acct_choices xml from the app party,build the dropdown and 
        // select the one matching coms_chrgs_our_account_num
        acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("coms_chrgs_our_account_num"));

        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
        acctOptions = new DocumentHandler(appAcctList, true);
        options = Dropdown.createSortedAcctOptions(acctOptions, acctNum, loginLocale);
        out.println(InputField.createSelectField("ComsChargesOurAccountNum", "", " ", 
                                                 options, "ListText", isReadOnly));
%>        
        </p>
      </td>
      <td width="40" nowrap>&nbsp;</td>
      <td nowrap colspan="2" class="ListText" align="left" valign="bottom"> 
        <p class="ControlLabel">
		  <img src="images/Blank_256pix.gif" width="256" height="1">
		  <br>
          <%=resMgr.getText("LoanRequest.ApplicantsDebitAccountForInterest", 
                              TradePortalConstants.TEXT_BUNDLE)%>
		  <%=getRequiredIndicator(!isTemplate)%>
		  <br>
<%
        // Using the acct_choices xml from the app party,build the dropdown and 
        // select the one matching interest_debit_acct_num
        acctNum = StringFunction.xssHtmlToChars(terms.getAttribute("interest_debit_acct_num"));

        appAcctList = StringFunction.xssHtmlToChars(termsPartyApp.getAttribute("acct_choices"));

        //DocumentHandler acctOptions = new DocumentHandler(appAcctList, true);
        acctOptions = new DocumentHandler(appAcctList, true);
        options = Dropdown.createSortedAcctOptions(acctOptions, acctNum, loginLocale);
        out.println(InputField.createSelectField("InterestDebitAccountNum", "", " ", 
                                                 options, "ListText", isReadOnly));

%>                </p>
      </td>
      <td nowrap class="ListText"> 
        <p class="ControlLabel">&nbsp;</p>
      </td>
      <td width="100%">&nbsp;</td>
    </tr>
  </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>&nbsp;</td>
    </tr>
	<input type=hidden value='<%=appAcctList%>' name=AppAcctChoices>
  </table>
  
<script LANGUAGE="JavaScript">

	function clearBeneficiaryBank() {
		document.TransactionLRQ.BbkName.value = "";
		document.TransactionLRQ.BbkAddressLine1.value = "";
		document.TransactionLRQ.BbkAddressLine2.value = "";
		document.TransactionLRQ.BbkCity.value = "";
		document.TransactionLRQ.BbkStateProvince.value = "";
		document.TransactionLRQ.BbkCountry.value = "";
		document.TransactionLRQ.BbkPostalCode.value = "";
		document.TransactionLRQ.BbkPhoneNumber.value = "";
		document.TransactionLRQ.BbkOTLCustomerId.value = "";
	}
	
	function clearBeneficiary() {
		document.TransactionLRQ.BenName.value = "";
		document.TransactionLRQ.BenAddressLine1.value = "";
		document.TransactionLRQ.BenAddressLine2.value = "";
		document.TransactionLRQ.BenCity.value = "";
		document.TransactionLRQ.BenStateProvince.value = "";
		document.TransactionLRQ.BenCountry.value = "";
		document.TransactionLRQ.BenPostalCode.value = "";
		document.TransactionLRQ.BenPhoneNumber.value = "";
		document.TransactionLRQ.BenOTLCustomerId.value = "";
	}
	
	function checkBeneficiaryName(originalName) {
	
		if (document.forms[0].BenName.value != originalName)
		{
			document.forms[0].BenOTLCustomerId.value = "";
		}
	}
	
	function checkBeneficiaryBankName(originalName) {
	
		if (document.forms[0].BbkName.value != originalName)
		{
			document.forms[0].BbkOTLCustomerId.value = "";
		}
	}
	
	<%-- PICK RELATED INSTRUMENT RADIO IF RELATED INSTRUMENT ID IS ENTERED--%>
	function pickRelatedInstrumentRadio() {
	    
		if (document.forms[0].RelatedInstrumentID.value > '' || document.forms[0].MultipleRelatedInstruments.value > '' ) {
			document.forms[0].LoanProceedsCreditType[0].checked = true;
		}
	}

	<%-- PICK OUR ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
	function pickOurAccountRadio() {
	    
		var index = document.forms[0].LoanProceedsCreditAccount.selectedIndex;
    	var acct  = document.forms[0].LoanProceedsCreditAccount.options[index].value;

		if (acct > '') {
			document.forms[0].LoanProceedsCreditType[1].checked = true;
		}
	}

	<%-- PICK THE FOLLOWING BENEFICIARY RADIO IF A BENEFICIARY IS SELECTED --%>
	function pickTheFollowingBeneficiaryRadio() {
	    
		if (document.TransactionLRQ.BenName.value > ''			||
			document.TransactionLRQ.BenAddressLine1.value > ''	||
			document.TransactionLRQ.BenAddressLine2.value > ''	||
			document.TransactionLRQ.BenStateProvince.value > ''	||
			document.TransactionLRQ.BenCity.value > ''			||
			document.TransactionLRQ.BenPostalCode.value > ''	||
			document.TransactionLRQ.BenPhoneNumber.value > ''	||
			document.TransactionLRQ.BenOTLCustomerId.value > ''	||
			document.TransactionLRQ.BenCountry.value > '') {
				document.forms[0].LoanProceedsCreditType[2].checked = true;
		}
	}

	<%-- PICK OTHER (ENTER ...) RADIO IF APPLY LOAN PROCEEDS TO OTHER TEXT BOX IS FILLED OUT --%>
	function pickApplyProceedsToOtherRadio() {
	    
		if (document.forms[0].LoanProceedsCreditOtherText.value > '') {
			document.forms[0].LoanProceedsCreditType[3].checked = true;
		}
	}
	
	<%-- CHECK LOAN PROCEEDS FEC CHECKBOX IF NUMBER, AMOUNT, RATE, OR DATE IS ENTERED --%>
	function pickLoanProceedsFECCheckbox() {
	
	    var index = document.forms[0].FECMaturityDay.selectedIndex;
		var day = document.forms[0].FECMaturityDay.options[index].value;
	
		index = document.forms[0].FECMaturityMonth.selectedIndex;
		var month = document.forms[0].FECMaturityMonth.options[index].value;
	
		index = document.forms[0].FECMaturityYear.selectedIndex;
		var year = document.forms[0].FECMaturityYear.options[index].value;

		if (document.forms[0].ProceedsCoveredByFECNumber.value > ''		||
			document.forms[0].SettlementFECAmount.value > ''			||
			document.forms[0].SettlementFECRate.value > ''				||
			day != '-1' || month != '-1' || year != '-1') {
				document.forms[0].ProceedsUseFEC.checked = true;
		}
	}
	
	<%-- PICK LOAN PROCEEDS FEC OTHER CHECKBOX IF APPLY LOAN PROCEEDS FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickLoanProceedsFECOtherCheckBox() {
	    
		if (document.forms[0].UseOtherText.value > '') {
			document.forms[0].ProceedsUseOther.checked = true;
		}
	}
	
	<%-- PICK DEBIT THE APPLICANTS ACCOUNT RADIO IF AN ACCOUNT NUMBER IS SELECTED --%>
	function pickDebitApplicantsAccountRadio() {
	    
		var index = document.forms[0].LoanMaturityDebitAccount.selectedIndex;
    	var acct  = document.forms[0].LoanMaturityDebitAccount.options[index].value;

		if (acct > '') {
			document.forms[0].LoanMaturityDebitType[0].checked = true;
		}
	}

	<%-- PICK DEBIT OTHER RADIO IF TEXT IS ENTERED INTO THE OTHER (ADVISE ...) TEXT BOX --%>
	function pickDebitOtherRadio() {
	    
		if (document.forms[0].LoanMaturityDebitOtherText.value > '') {
			document.forms[0].LoanMaturityDebitType[1].checked = true;
		}
	}
	
	<%-- CHECK LOAN MATURITY FEC CHECKBOX IF NUMBER, AMOUNT, RATE, OR DATE IS ENTERED --%>
	function pickMaturityFECCheckbox() {
	
	    var index = document.forms[0].MaturityFECMaturityDay.selectedIndex;
		var day = document.forms[0].MaturityFECMaturityDay.options[index].value;
	
		index = document.forms[0].MaturityFECMaturityMonth.selectedIndex;
		var month = document.forms[0].MaturityFECMaturityMonth.options[index].value;
	
		index = document.forms[0].MaturityFECMaturityYear.selectedIndex;
		var year = document.forms[0].MaturityFECMaturityYear.options[index].value;

		if (document.forms[0].MaturityCoveredByFECNumber.value > ''		||
			document.forms[0].LoanFECAmount.value > ''			||
			document.forms[0].MaturityFECRate.value > ''				||
			day != '-1' || month != '-1' || year != '-1') {
				document.forms[0].MaturityUseFEC.checked = true;
		}
	}
	
	<%-- PICK MATURITY FEC OTHER CHECKBOX IF APPLY MATURITY FEC OTHER TEXT BOX IS FILLED OUT --%>
	function pickMaturityFECOtherCheckBox() {
	    
		if (document.forms[0].MaturityUseOtherText.value > '') {
			document.forms[0].MaturityUseOther.checked = true;
		}
	}

</script>
