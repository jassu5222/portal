<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
                    Transaction-TermsDetail-Action Buttons Page

  Description:
	This jsp just lays in the action Buttons for the Transaction Terms
  Details page.  Because this logic needs to be called twice, This was 
  localized into it's own jsp.
*******************************************************************************
--%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="6"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="ColorGrey">
    <tr> 
    <%
     /****************************
      * START Action Buttons
      ***************************/                 Debug.debug("*** START Action Buttons ***");

      goToInstrumentNavigator = "goToInstrumentNavigator";
      goToInstrumentCloseNavigator = "goToInstrumentCloseNavigator";
    %>
      <td width="100%" class="ColorGrey">&nbsp;</td>
      <td class="ColorGrey">&nbsp;</td>
      <td width="15" nowrap class="ColorGrey">&nbsp;</td>
      <td class="ColorGrey">&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="15" nowrap>&nbsp;</td>
      <td>
	<%-- This lays in the view terms as entered button logic --%>
<%
	if( (isFromCurrentTerms == false) && hasCustEnteredTerms ) {
%>
          <jsp:include page="../common/RolloverButtonLink.jsp">
            <jsp:param name="showButton" value="true" />
            <jsp:param name="name" value="ViewTermsButtonTOP" />
            <jsp:param name="image" value='common.ViewTermsAsEnteredImg' /> 
            <jsp:param name="text" value='common.ViewTermsAsEnteredText' />
            <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( goToInstrumentNavigator, urlParm, response )) %>" />
            <jsp:param name="width" value="140" />
          </jsp:include>
<%
	}else{
%>		&nbsp;
<%	}
%>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>

	<%-- This lays in the common close button logic --%>
          <jsp:include page="../common/RolloverButtonLink.jsp">
            <jsp:param name="showButton" value="true" />
            <jsp:param name="name" value="CloseButtonTOP" />
            <jsp:param name="image" value='common.CloseImg' />
            <jsp:param name="text" value='common.CloseText' />
            <jsp:param name="link" value="<%=java.net.URLEncoder.encode(formMgr.getLinkAsUrl( goToInstrumentCloseNavigator, urlParm, response )) %>" />
            <jsp:param name="width" value="" />
          </jsp:include>

      </td>
      <td width="20" nowrap>&nbsp;</td>
    <%
     /****************************
      * END Action Buttons
      ***************************/                 Debug.debug("*** END Action Buttons ***");
    %>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ColorGrey">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
