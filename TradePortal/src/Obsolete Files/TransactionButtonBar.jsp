<%--
*******************************************************************************
                             TransactionButtonBar

  Description:
     Creates the transaction button bar.  The Save, Save&Close, Route, Verify,
  Edit, Authorise, and Close buttons may display.  All of these buttons display
  conditionally.

  Parameters:
     None.  The assumption is that 'instrument' and 'transaction' are webbeans
  available to this JSP
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.common.cache.*,
                 com.amsinc.ecsg.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>



<%
  TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");
  TemplateWebBean template        = (TemplateWebBean)
                                      beanMgr.getBean("Template");

  boolean showSave       = false;
  boolean showSaveClose  = false;
  boolean showRoute      = false;
  boolean saveBeforeRoute = false;
  boolean showVerify     = false;
  boolean showVerifyFX	 = false; //VShah - CR564
  boolean showEdit       = false;
  boolean showAuthorize  = false;
  boolean showProxyAuthorize = false; //AAlubala CR-711 Rel 8.0 10/14/2011
  boolean showReCertificate = false; //IAZ 01/03/09
  boolean showAuthorizeDelete  = false;
  boolean showClose      = false;
  boolean showUploadPayment = false;
  boolean canCreateMod   = false;
  boolean canRoute       = false;
  boolean canAuthorize   = false;
  boolean canProxyAuthorize = false; //AAlubala CR-711 Rel 8.0 10/14/2011
  boolean isEditable     = false;
  boolean isReadOnly     = false;
  boolean canShowUploadPayment = false;
  boolean showUploadDirectDebit = false;
  boolean canShowUploadDirectDebit = false; 
  boolean isVascoOTP = false; 
  StringBuffer      sqlQuery   = new StringBuffer(); //Vshah - IR#DWUM043037276


  StringBuffer extraTags;
  StringBuffer downloadLink; 

  String userRights = userSession.getSecurityRights();

  // Get the ownership level of the user.  If this user is within customer access or subsidiary access,
  // the true ownership level is store on the savedUserSession.
  String userSecurityType = null;
  boolean adminCustAccess = false;

  if (userSession.hasSavedUserSession()) 
  {
    userSecurityType = userSession.getSavedUserSessionSecurityType();
    if (userSecurityType.equals(TradePortalConstants.ADMIN)) 
      adminCustAccess = true;
      
    // IAZ 03/15/09 CM-451 Begin
    // Note that panelEnabled and parentCorpOrgOid attributes are stored to the request object by the CashMgmt.jsp
    // that reads the corresponding values from the CorpOrgWebBean it creates.
    else
    {
        // This is not an admin access so this is subsidiary access
        // Check if subsidiary itself has panel authorization enabled. if so, disable authroize as it is disallowed in
        // the subsidiary access mode.
    	if (TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")))
    	{
	      adminCustAccess = true;
	      //out.println("TBBar: " + (String)request.getAttribute("panelEnabled"));
    	}
    }
    //IAZ 03/15/09 CM-451 End
    
  }

  String transactionStatus = transaction.getAttribute("transaction_status");
  String instrumentType    = instrument.getAttribute("instrument_type_code");
  String transactionType   = transaction.getAttribute("transaction_type_code");
  String SlcGuaranteeForm  = transaction.getAttribute("standby_using_guarantee_form");

  String bankReleasedTerms	= transaction.getAttribute("c_BankReleasedTerms");
  boolean hasBankReleasedTerms  = InstrumentServices.isNotBlank( bankReleasedTerms );

  isReadOnly = new Boolean(request.getParameter("isReadOnly")).booleanValue();

  Debug.debug("transStatus " + transactionStatus);
  Debug.debug("instType " + instrumentType);

  canCreateMod = SecurityAccess.canCreateModInstrument(userRights, instrumentType, transactionType);
  canRoute     = SecurityAccess.canRouteInstrument(userRights, instrumentType, transactionType);
  
  canAuthorize = SecurityAccess.canAuthorizeInstrument(userRights, instrumentType, transactionType);
  //AAlubala - CR-711 Rel8.0 - 10/14/2011 - Proxy Authorize check - start
  canProxyAuthorize = SecurityAccess.canProxyAuthorizeInstrument(userRights, instrumentType, transactionType);
  //CR-711 - end
  canShowUploadPayment = SecurityAccess.canProcessUploadBulkFile(userRights, instrumentType, transactionType);
  canShowUploadDirectDebit = SecurityAccess.canProcessUploadDirectDebitFile(userRights,instrumentType);

  isEditable = InstrumentServices.isEditableTransStatus(transactionStatus);
  Debug.debug("isEditable " + isEditable);
  Debug.debug("canCreateMod " + canCreateMod);
  Debug.debug("canRoute " + canRoute);
  Debug.debug("canAuthorize " + canAuthorize);
  Debug.debug("canShowUploadPayment " + canShowUploadPayment);

  if (isEditable && canCreateMod && !isReadOnly) showSave = true;
  if (isEditable && canCreateMod && !isReadOnly) showSaveClose = true;

  if (isEditable && canRoute) {
     showRoute = true;
	 saveBeforeRoute = true;
  }

  if (!isEditable && canRoute &&
     (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FVD_AUTHORIZED) //VS CR 609 11/23/10
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN) //IAZ 01/03/09    
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED)))
        showRoute = true;

  if (isEditable && canCreateMod && !isReadOnly) showVerify = true;

  //VShah - CR564 - 03/01/2011 - Begin
  if (isEditable && canCreateMod && !isReadOnly) {
     if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX))
  	showVerifyFX = true;
  }	   
  //VShah - CR564 - 03/01/2011 - End


  if (!isEditable && canCreateMod 
  		&& (userSession.getOwnerOrgOid().equals(instrument.getAttribute("corp_org_oid")))) {	//IAZ IR-LKUK052747265 Add
    if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FVD_AUTHORIZED) //VS CR 609 11/23/10
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN) //IAZ 01/03/09         
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED) )
      showEdit = true;
  }

  // Do not display Authorize button if this is an admin user in customer access
  // DK CR-640 Rel7.1
  if (!isEditable && canAuthorize && !adminCustAccess) {
    if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN)          
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED) 
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED) 
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE))
      showAuthorize = true;
     
    //IAZ 01/03/09 Begin
    //cr498 change certNewLink to certAuthURL to support multiple button actions
    String certAuthURL = (String)request.getParameter("certAuthURL"); 
    if (InstrumentServices.isNotBlank(certAuthURL)) 
       showReCertificate = true;
    //IAZ 01/03/09 End   
		  //IR-PNUI061351319 of CR-419 Krishna Begin
		  //When the user has the Authorization rights as well as the rights to delete
		  //Discrepancy/ATP Approval Notices from the Mail Message Inbox and the associated Mail message notice is 
		  //not yet deleted then display the "Authorize&Delete" Button
		  if ((transactionType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE)||
				  transactionType.equals(TradePortalConstants.DISCREPANCY))&&
				  (SecurityAccess.hasRights(userRights, SecurityAccess.DELETE_DISCREPANCY_MSG)))
		  {
			  StringBuffer msgStatusQuery = new StringBuffer();
			  msgStatusQuery.append("select message_status ");
			  msgStatusQuery.append("from mail_message ");
			  msgStatusQuery.append("where response_transaction_oid = " + transaction.getAttribute("transaction_oid") );
			  String messageStatus = "";
			  Vector veFrags  = null;
			  DocumentHandler resultsDocXML = DatabaseQueryBean.getXmlResultSet(msgStatusQuery.toString());
			  //Fetch the existing records 
			  if(resultsDocXML != null)
				  veFrags = resultsDocXML.getFragments("/ResultSetRecord");
			  if(veFrags != null)
			  {  
				  messageStatus = resultsDocXML.getAttribute("/ResultSetRecord(0)/MESSAGE_STATUS");
			  } 
			  //If the user can authorize as well the associated Mail message to this transaction is not deleted 
			  //then display the Authorize&Delete Button
			  if((showAuthorize && !messageStatus.equals(TradePortalConstants.REC_DELETED)))
			  {
				  showAuthorizeDelete = true;  
			  }
		  }
		  //IR-PNUI061351319 of CR-419 Krishna End
  }
  
  ////AAlubala - IR#DNUM062245488 -  This will check if a user is  set to use OTP Vasco type
  //
  //
  QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
  //sqlQuery.append("select token_type,auth2fa_subtype from users where user_oid = '");
  sqlQuery.append("select auth2fa_subtype from users where user_oid = '");
  sqlQuery.append(userSession.getUserOid());
  sqlQuery.append("'");
   
  queryListView.setSQL(sqlQuery.toString());
  queryListView.getRecords();
     
  DocumentHandler result = queryListView.getXmlResultSet();
  //String tokenType = result.getAttribute("/ResultSetRecord(0)/TOKEN_TYPE");
  String authSubtype = result.getAttribute("/ResultSetRecord(0)/AUTH2FA_SUBTYPE");
  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(authSubtype)) isVascoOTP = true;
 
  //  //AAlubala - IR#DNUM062245488 -  This will check if a user is  set to use OTP Vasco type

  
  //AAlubala - CR-711 Rel8.0 - The proxy authorize - 10/14/2011 - START --
  // Do not display Proxy Authorize button if this is an admin user in customer access

////AAlubala - IR#DNUM062245488 -  This will check if a user is NOT set to use OTP Vasco type
  if (!isEditable && canProxyAuthorize && !adminCustAccess && !isVascoOTP) {	  
    if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN)          
	 || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED)
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED) //Vshah - IR#MMUM061784253 - Rel8.0
     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE) ) //Vshah - IR#MMUM061784253 - Rel8.0
	 {
	 	//Vshah - IR#MMUM060640963 - Rel8.0 - 06/06/2012 - <BEGIN>
	 	//Offline authorise button should appear only for transactions 
	 	//that are flagged for token authorization at the Bank ASP level.
	 	Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
		DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
		String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
		boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, instrumentType+"_"+transactionType);
	 	if(requireAuth) 
	 	//Vshah - IR#MMUM060640963 - Rel8.0 - 06/06/2012 - <END>
	 	   showProxyAuthorize = true;
      }

    String certAuthURL = (String)request.getParameter("certAuthURL"); 
    if (InstrumentServices.isNotBlank(certAuthURL)) 
       showReCertificate = true;
  }
  //CR-711 - END

  if (isEditable && canShowUploadPayment) {
     showUploadPayment = true;
  }

  if (isEditable && canShowUploadDirectDebit) {
      showUploadDirectDebit = true;
  }
  
  showClose = true;

  long instrumentOid = Long.valueOf(instrument.getAttribute("instrument_oid")).longValue();
  long userOid = Long.valueOf(userSession.getUserOid()).longValue();

  // If it isn't already locked by the user, don't allow them to do anything.
  // (We only lock the instrument if it is editable.)
  if (isEditable && !LockingManager.isLocked(instrumentOid, userOid, false)) {
    Debug.debug("User doesn't have a lock so can't do anything");
    showSave       = false;
    showSaveClose  = false;
    showRoute      = false;
    showVerify     = false;
    showEdit       = false;
    showAuthorize  = false;
    showUploadPayment = false;
    showUploadDirectDebit = false;
  }
  
  // TLE - 06/27/07 - IR-NNUH062163493 - Add Begin
  // Added code to check if we need to display the 'Delete a Document' button
  StringBuffer query = new StringBuffer();
  boolean showDeleteAttachDocButton = false;

  query.append("select doc_image_oid, image_id, doc_name ");
  query.append("from document_image ");
  query.append("where p_transaction_oid = " + transaction.getAttribute("transaction_oid") );
  query.append(" and form_type = '"+TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED+"'");
  query.append(" and mail_message_oid is null");

  Debug.debug("Query is : " + query);
  DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false);

  Vector listviewVector = null;
  if(dbQuerydoc != null) {
      if (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0) {
          showDeleteAttachDocButton = true;
      }
  }
  // TLE - 06/27/07 - IR-NNUH062163493 - Add End
  
%>

  <table width=100% border=0 cellspacing=0 cellpadding=0 class=ColorGrey>
    <tr>
         <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
    <tr align=middle>
      <td nowrap class="ColorGrey" width="20">&nbsp;</td>
      <td nowrap class="ColorGrey">
        <%
           if ((instrumentType.equals(TradePortalConstants.IMPORT_DLC)) && 
               (transactionType.equals(TradePortalConstants.ISSUE)))
           {
              downloadLink = new StringBuffer();

              downloadLink.append(request.getContextPath());

             // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
              downloadLink.append("/common/TradePortalDownloadServlet?");
              downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
              downloadLink.append("=");
              downloadLink.append(TradePortalConstants.DOWNLOAD_IMPORT_LC_ISSUE_DATA);
        %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name"  value="DownloadSavedDataButton" />
                  <jsp:param name="image" value="common.DownLoadSavedDataGreyImg" />
                  <jsp:param name="text"  value="common.DownLoadLCTermsText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(downloadLink.toString())%>" />
                  <jsp:param name="width" value="140" />
              </jsp:include>
        <%
           }
           //rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
           else if ((instrumentType.equals(TradePortalConstants.APPROVAL_TO_PAY)) && 
               (transactionType.equals(TradePortalConstants.ISSUE)))
           {
              downloadLink = new StringBuffer();

              downloadLink.append(request.getContextPath());

              // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
              downloadLink.append("/common/TradePortalDownloadServlet?");
              downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
              downloadLink.append("=");
              downloadLink.append(TradePortalConstants.DOWNLOAD_ATP_ISSUE_DATA);
        %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name"  value="DownloadSavedDataButton" />
                  <jsp:param name="image" value="common.DownLoadSavedDataGreyImg" />
                  <jsp:param name="text"  value="common.DownLoadATPTermsText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(downloadLink.toString())%>" />
                  <jsp:param name="width" value="140" />
              </jsp:include>
        <%
           }
           //rkrishna CR 375-D ATP-ISS 07/21/2007 End
		   else if ((instrumentType.equals(TradePortalConstants.REQUEST_ADVISE)) && 
               (transactionType.equals(TradePortalConstants.ISSUE)))
           {
              downloadLink = new StringBuffer();

              downloadLink.append(request.getContextPath());

             // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
              downloadLink.append("/common/TradePortalDownloadServlet?");
              downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
              downloadLink.append("=");
              downloadLink.append(TradePortalConstants.DOWNLOAD_IMPORT_LC_ISSUE_DATA);
        %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name"  value="DownloadSavedDataButton" />
                  <jsp:param name="image" value="common.DownLoadSavedDataGreyImg" />
                  <jsp:param name="text"  value="common.DownLoadLCTermsText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(downloadLink.toString())%>" />
                  <jsp:param name="width" value="140" />
              </jsp:include>
        <%
           }
           else if((instrumentType.equals(TradePortalConstants.STANDBY_LC)) && 
                   (transactionType.equals(TradePortalConstants.ISSUE))     &&
                   (!SlcGuaranteeForm.equals(TradePortalConstants.INDICATOR_YES)) )
           {
              //This section is specific to the Standy LC download data...
              downloadLink = new StringBuffer();

              downloadLink.append(request.getContextPath());
            // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
              downloadLink.append("/common/TradePortalDownloadServlet?");
              downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
              downloadLink.append("=");
              downloadLink.append(TradePortalConstants.DOWNLOAD_OUTGOING_STANDBY);
        %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name"  value="DownloadSavedDataButton" />
                  <jsp:param name="image" value="common.DownLoadSavedDataGreyImg" />
                  <jsp:param name="text"  value="common.DownLoadLCTermsText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(downloadLink.toString())%>" />
                  <jsp:param name="width" value="140" />
              </jsp:include>
        <%

	   }
            //Krishna IR-GTUE041257440 - Adding OR condition for StandBy LC to have "Download saved data" button option 
           //for Detailed StandBy LC also.
           else if((instrumentType.equals(TradePortalConstants.GUARANTEE))  && 
          transactionType.equals(TradePortalConstants.ISSUE) ||
         (instrumentType.equals(TradePortalConstants.STANDBY_LC) && 
          transactionType.equals(TradePortalConstants.ISSUE)     &&
          SlcGuaranteeForm.equals(TradePortalConstants.INDICATOR_YES)))  
           {
              //This section is specific to the Standy LC download data...
              downloadLink = new StringBuffer();

              downloadLink.append(request.getContextPath());
         // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
              downloadLink.append("/common/TradePortalDownloadServlet?");
              downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
              downloadLink.append("=");
              downloadLink.append(TradePortalConstants.DOWNLOAD_GUARANTEE_ISSUE_DATA);
        %>
              <jsp:include page="/common/RolloverButtonLink.jsp">
                  <jsp:param name="name"  value="DownloadSavedDataButton" />
                  <jsp:param name="image" value="common.DownLoadSavedDataGreyImg" />
                  <jsp:param name="text"  value="common.DownLoadLCTermsText" />
                  <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(downloadLink.toString())%>" />
                  <jsp:param name="width" value="140" />
              </jsp:include>
        <%

	   }
           else
           {
              out.print("&nbsp;");
           }
        %>
      </td>
<% 
// [START] CR-186 - jkok - "Attach a Document" button
if ((isEditable) && (!isReadOnly) && (SecurityAccess.hasRights(userRights, SecurityAccess.ATTACH_DOC_TRANSACTION))) {
   session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
   session.setAttribute("documentImageFileUploadOid", transaction.getAttribute("transaction_oid"));
%>
      <td width="15" class="ColorGrey">&nbsp;</td>
      <td nowrap class="ColorGrey">
      <%
// [BEGIN] IR-YYUH032242088 - jkok
      // Attach a Document
      extraTags = new StringBuffer();
      %>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="DocumentImageFileUploadData" />
              <jsp:param name="image" value='common.AttachADocumentImg' />
              <jsp:param name="text" value='common.AttachADocumentText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_ATTACH_DOCUMENTS%>" />
            </jsp:include>
      </td>
      <td width="30" nowrap>&nbsp;</td>
<%
// [END] IR-YYUH032242088 - jkok
} // if ((isEditable) && (!isReadOnly) && (SecurityAccess.hasRights(userRights, SecurityAccess.ATTACH_DOC_TRANSACTION))) {
// [END] CR-186 - jkok
%>
<%
// [START] CR-186 - jkok - "Delete a Document" button
if ((isEditable) && (!isReadOnly) && (SecurityAccess.hasRights(userRights, SecurityAccess.DELETE_DOC_TRANSACTION))) {
%>
<script language="JavaScript">

   <%--
   // This function is used to display a popup message confirming
   // that the user wishes to delete the selected items in the
   // listview.
   --%>
   function confirmDocumentDelete()
   {
      var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", 
                                               TradePortalConstants.TEXT_BUNDLE) %>";
      
      <%-- Find any checked checkbox and flip a flag if one is found --%>
      var isAtLeastOneDocumentChecked = new Boolean(false);      
      if (document.forms[0].AttachedDocument != null) {
         if (document.forms[0].AttachedDocument.length != null) {
            for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
               if (document.forms[0].AttachedDocument[i].checked==true) {
                  isAtLeastOneDocumentChecked = true;
               }
            }
         }
         else if (document.forms[0].AttachedDocument.checked==true) {
            isAtLeastOneDocumentChecked = true;
         }
      }
      
      if (isAtLeastOneDocumentChecked==true) {
         if (confirm(confirmMessage)==false)
         {
            <%-- Uncheck any checked checkboxes --%>
            if (document.forms[0].AttachedDocument != null) {
               if (document.forms[0].AttachedDocument.length != null) {
                  for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
                     if (document.forms[0].AttachedDocument[i].checked==true) {
                     document.forms[0].AttachedDocument[i].checked = false;
                     }
                  }
               }
               else if (document.forms[0].AttachedDocument.checked==true) {
                  document.forms[0].AttachedDocument.checked = false;
               }
            }            
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = false;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return false;
         }
         else
         {
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = true;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return true;
         }
      }
      else {
         <%-- [BEGIN] IR-YVUH032339608 - jkok --%>
         alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE) %>");
         <%-- [END] IR-YVUH032339608 - jkok --%>
         <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
         formSubmitted = false;
         <%-- [END] IR-YRUH032953136 - jkok --%>
         return false;
      }
  }


</script>
<%
extraTags = new StringBuffer();
// [BEGIN] IR-YRUH032953136 - jkok
extraTags.append("return confirmDocumentDelete()");
// [END] IR-YRUH032953136 - jkok
%>

<%-- TLE - 06/27/07 IR-NNUH062163493 - Add Begin --%>
<td width="15" class="ColorGrey">&nbsp;</td>
<% 
    if (showDeleteAttachDocButton) {
%>
      <td nowrap class="ColorGrey">
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="DeleteADocumentButton" />
              <jsp:param name="image" value='common.DeleteADocumentImg' />
              <jsp:param name="text" value='common.DeleteADocumentText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>" />
            </jsp:include>
      </td>
<%
    }
%>
<%-- TLE - 06/27/07 IR-NNUH062163493 - Add End --%>     
          
      
<%
} // if ((isEditable) && (!isReadOnly) && (SecurityAccess.hasRights(userRights, SecurityAccess.DELETE_DOC_TRANSACTION))) {
// [END] CR-186 - jkok
%>
      <td class="ColorGrey">&nbsp;</td>
<%
	//MDB CR-564 Rel6.1 1/6/11 - Don't display Payment Upload Button - left rest of code in, just set display to false
        showUploadPayment = false;
        if (showUploadPayment) {
         // This is the link for the Upload PO Data button
         StringBuffer newLink = new StringBuffer("");
         newLink.append(formMgr.getLinkAsUrl("goToUploadPaymentFile", response));
%>
          <td class="ColorGrey">
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="name"  value="common.UploadPaymentFileText" />
              <jsp:param name="image" value="common.UploadPaymentFileImg" />
              <jsp:param name="text"  value="common.UploadPaymentFileText" />
              <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
              <jsp:param name="width" value="95" />
              </jsp:include>
          </td>         
 
         
<%
        }
%>
      <td class="ColorGrey">&nbsp;</td>
<%
        if (showUploadDirectDebit) {
         // This is the link for the Upload Direct Debit Data button
         StringBuffer newLink = new StringBuffer("");
         newLink.append(formMgr.getLinkAsUrl("goToUploadDirectDebitFile", response));
%>
          <td class="ColorGrey">
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="name"  value="common.UploadDirectDebitFileText" />
              <jsp:param name="image" value="common.UploadDirectDebitFileImg" />
              <jsp:param name="text"  value="common.UploadDirectDebitFileText" />
              <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
              <jsp:param name="width" value="95" />
              </jsp:include>
          </td>         
 
         
<%
        }
%>

 <td width="100%" class="ColorGrey"> &nbsp;</td> 
<% 
        if (showSave) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraSaveTags"));
%>
          <td class="ColorGrey">
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="SaveButton" />
              <jsp:param name="image" value='common.SaveGreyImg' />
              <jsp:param name="text" value='common.SaveText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_SAVETRANS%>" />
            </jsp:include>
          </td>

          <td width="15" nowrap class="ColorGrey">&nbsp;</td>
<%
        }

        if (showSaveClose) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraSaveCloseTags"));
%>
          <td class="ColorGrey">
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="SaveCloseButton" />
              <jsp:param name="image" value='common.SaveCloseGreyImg' />
              <jsp:param name="text" value='common.SaveCloseText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_SAVECLOSETRANS%>" />
            </jsp:include>
          </td>

          <td width="15" nowrap>&nbsp;</td>
<%
        }

        if (showRoute) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraRouteTags"));
		  String submitValue = saveBeforeRoute?
		  		TradePortalConstants.BUTTON_SAVEROUTETRANS: TradePortalConstants.BUTTON_ROUTE;
%>
          <td>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="RouteButton" />
              <jsp:param name="image" value='common.RouteGreyImg' />
              <jsp:param name="text" value='common.RouteText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="width" value="140" />
              <jsp:param name="submitButton"
                         value="<%=submitValue%>" />
            </jsp:include>
          </td>

          <td width="15" nowrap>&nbsp;</td>
<%
        }

		//VShah - CR564
	if (showVerifyFX) {
	   extraTags = new StringBuffer();
           extraTags.append(request.getParameter("extraVerifyTags"));
%>		
	  <td>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="VerifyFXButton" />
              <jsp:param name="image" value='common.VerifyFXImg' />
              <jsp:param name="text" value='common.VerifyFXText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_VERIFY_FX%>" />
            </jsp:include>
          </td>

          <td width="15" nowrap>&nbsp;</td>
<%
	}
	else {	
	//VShah - CR564
             if (showVerify) {
	          extraTags = new StringBuffer();
        	  extraTags.append(request.getParameter("extraVerifyTags"));
%>
 	        <td>
        	    <jsp:include page="/common/RolloverButtonSubmit.jsp">
	              <jsp:param name="showButton" value="true" />
        	      <jsp:param name="name" value="VerifyButton" />
	              <jsp:param name="image" value='common.VerifyGreyImg' />
        	      <jsp:param name="text" value='common.VerifyText' />
	              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
        	      <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_VERIFY%>" />
	            </jsp:include>
	        </td>

	        <td width="15" nowrap>&nbsp;</td>
<%
  	      }
	}

        if (showEdit) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraEditTags"));
%>
          <td>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="EditButton" />
              <jsp:param name="image" value='common.EditDataGreyImg' />
              <jsp:param name="text" value='common.EditDataText' />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_EDIT%>" />
            </jsp:include>
          </td>

          <td width="15" nowrap>&nbsp;</td>
<%
        }

        if (showAuthorize) {
        	if (!showReCertificate) {  
          
          		extraTags = new StringBuffer();
          		extraTags.append(request.getParameter("extraAuthorizeTags"));
%>
          		<td>
          		  <jsp:include page="/common/RolloverButtonSubmit.jsp">
          		    <jsp:param name="showButton" value="true" />
          		    <jsp:param name="name" value="AuthorizeButton" />
          		    <jsp:param name="image" value='common.AuthorizeGreyImg' />
          		    <jsp:param name="text" value='common.AuthorizeText' />
          		    <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
          		    <jsp:param name="submitButton"
          		               value="<%=TradePortalConstants.BUTTON_AUTHORIZE%>" />
          		  </jsp:include>
          		</td>

          		<td width="15" nowrap>&nbsp;</td>
<%
			}
        	else {
        	  String certAuthURL = (String)request.getParameter("certAuthURL"); 
                  String authLink = "javascript:openReauthenticationWindow(" +
                      "'" + certAuthURL + "','Authorize')";
%>
          		<td>
          		  <jsp:include page="/common/RolloverButtonLink.jsp">
          		    <jsp:param name="showButton" value="true" />
          		    <jsp:param name="name" value="Authorize" />
          		    <jsp:param name="image" value='common.AuthorizeGreyImg' />
          		    <jsp:param name="text" value='common.AuthorizeText' />
          		    <jsp:param name="link" value="<%=authLink%>" />
          		  </jsp:include>
          		</td>

          		<td width="15" nowrap>&nbsp;</td>
<%
			}
        }
        //IAZ 01/03/09 End

%>

<%--//AAlubala - CR-711 Rel8.0 - The proxy authorize - 10/14/2011 - START --%>


          <td width="15" nowrap>&nbsp;</td>
<%
        

        if (showProxyAuthorize) {
        	if (!showReCertificate) {  
          
          		extraTags = new StringBuffer();
          		extraTags.append(request.getParameter("extraAuthorizeTags"));
%>
				<%--
				//No need to show proxy button if recertification is not required
          		<td>
          		  <jsp:include page="/common/RolloverButtonSubmit.jsp">
          		    <jsp:param name="showButton" value="true" />
          		    <jsp:param name="name" value="ProxyAuthorizeButton" />
          		    <jsp:param name="image" value='common.ProxyAuthorizeGreyImg' />
          		    <jsp:param name="text" value='common.ProxyAuthorizeText' />
          		    <jsp:param name="extraTags" value="<%=extraTags.toString()" />
          		    <jsp:param name="submitButton"
          		               value="<%=TradePortalConstants.BUTTON_PROXY_AUTHORIZE%>" />
          		  </jsp:include>
          		</td>

          		<td width="15" nowrap>&nbsp;</td>
          		--%>
<%
			}
        	else {
        	  String certAuthURL = (String)request.getParameter("certAuthURL"); 
                  String authLink = "javascript:openReauthenticationWindow(" +
                      "'" + certAuthURL + "','ProxyAuthorize')";
%>
          		<td>
          		  <jsp:include page="/common/RolloverButtonLink.jsp">
          		    <jsp:param name="showButton" value="true" />
          		    <jsp:param name="name" value="ProxyAuthorize" />
          		    <jsp:param name="image" value='common.ProxyAuthorizeGreyImg' />
          		    <jsp:param name="text" value='common.ProxyAuthorizeText' />
          		    <jsp:param name="link" value="<%=authLink%>" />
          		  </jsp:include>
          		</td>

          		<td width="15" nowrap>&nbsp;</td>
<%
			}
        }
       


//CR-711 Rel8.0 - END
       
        //CR-419 Krishna Begin
          if (showAuthorizeDelete) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraAuthorizeTags"));
          System.out.println("extraTags:"+extraTags.toString());
%>
          <td>
<%            //cr498 add reauth link to Authorize & Delete
              if (!showReCertificate) {  
%>
            <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="AuthorizeAndDeleteButton" />
              <jsp:param name="image" value='common.AuthorizeDeleteGreyImg' />
              <jsp:param name="text" value='common.AuthorizeDeleteText' />
              <jsp:param name="extraTags" value="return confirmAuthorizeDelete()" />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_AUTHORIZEDELETE%>" />
            </jsp:include>
<%
              } else {
        	  String certAuthURL = (String)request.getParameter("certAuthURL"); 
                  String authAndDeleteLink = "javascript:openReauthenticationWindow(" +
                      "'" + certAuthURL + "','AuthorizeAndDelete')"; // Narayan IR-CUUK113047292
%>
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="showButton" value="true" />
              <jsp:param name="name" value="AuthorizeAndDeleteButton" />
              <jsp:param name="image" value='common.AuthorizeDeleteGreyImg' />
              <jsp:param name="text" value='common.AuthorizeDeleteText' />
              <jsp:param name="extraTags" value="return confirmAuthorizeDelete()" />
	      <jsp:param name="link" value="<%=authAndDeleteLink%>" />
            </jsp:include>
<%
              } //end cr498
%>
          </td>

          <td width="15" nowrap>&nbsp;</td>
       <%
       }
       //CR-419 Krishna End
        if (showClose) {
          extraTags = new StringBuffer();
          extraTags.append(request.getParameter("extraCloseTags"));
  	  String link = formMgr.getLinkAsUrl("transactionClose", response);
  		//rkazi IR RSUL020973906 02/11/2011 START added 2 Transaction statuses to condition
	  if ((transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK ) || 
			  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT ) ||
			  transactionStatus.equals( TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT )) &&
	      hasBankReleasedTerms ) {
		//rkazi IR RSUL020973906 02/11/2011 END		  
	    link = formMgr.getLinkAsUrl("goToTransactionTermsDetails", response);
	  }
%>
          <td>
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="showButton" value="showClose" />
              <jsp:param name="name" value="CloseButton" />
              <jsp:param name="image" value='common.CloseGreyImg' />
              <jsp:param name="text" value='common.CloseText' />
              <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
              <jsp:param name="extraTags" value="<%=extraTags.toString()%>" />
            </jsp:include>
          </td>

          
<%
        }
%>

      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
<%
//VS RVU062382912 Adding New parameter value for authorizing transactions
  if (userSession.getSavedUserSession() == null) {
%>
        <input type="hidden" name="AuthorizeAtParentViewOrSubs" value="Y">
<%
   }
%>