<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Transactions Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%> 

<%
   StringBuffer statusOptions = new StringBuffer();
   StringBuffer statusExtraTags = new StringBuffer();

   // Build the status dropdown options (hardcoded in ACTIVE (default), INACTIVE, ALL order), 
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");

   // Upon changing the status selection, automatically go back to the Transactions Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToTransactionsHome", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");

%>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="15" nowrap>&nbsp;</td>
      <td nowrap align="left" valign="middle">
        <span class="ListHeaderTextWhite">
          <%= resMgr.getText("InstrumentHistory.Show", 
                             TradePortalConstants.TEXT_BUNDLE) %>
        </span>
        &nbsp;
<%
        out.print(InputField.createSelectField("instrStatusType", "", "",
                                               statusOptions.toString(), "ListText", 
                                               false, statusExtraTags.toString()));
%>
        &nbsp;
        <span class="ListHeaderTextWhite">
          <%= resMgr.getText("InvoiceSearch.Invoices", 
                             TradePortalConstants.TEXT_BUNDLE) %>&nbsp;
        </span>
      </td>
<%
      // When the Instrument History tab is selected, display 
      // Organizations dropdown or the single organization.
      if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
           && (totalOrganizations > 1))
      {
%>
        <td nowrap align="left" valign="middle" class="ListText">
          <span class="ListHeaderTextWhite">
            <%= resMgr.getText("InstrumentHistory.For", 
                               TradePortalConstants.TEXT_BUNDLE) %>
          </span>
          &nbsp;
<%
          dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                             "ORGANIZATION_OID", "NAME", 
                                                             selectedOrg, userSession.getSecretKey()));
          dropdownOptions.append("<option value=\"");
          dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGANIZATIONS, userSession.getSecretKey()));
          dropdownOptions.append("\"");

          if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
             dropdownOptions.append(" selected");
          }

          dropdownOptions.append(">");
          dropdownOptions.append(ALL_ORGANIZATIONS);
          dropdownOptions.append("</option>");

          extraTags = new StringBuffer("");
          extraTags.append("onchange=\"location='");
          extraTags.append(formMgr.getLinkAsUrl("goToTransactionsHome", response));
          extraTags.append("&amp;current2ndNav=");
          extraTags.append(current2ndNav);
          extraTags.append("&NewDropdownSearch=Y");
          extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");

          out.print(InputField.createSelectField("Organization", "", "", 
                                                 dropdownOptions.toString(), 
                                                 "ListText", false, 
                                                 extraTags.toString()));
%>
        </td>
<%
      }
%>
      <td width="100%" height="30">&nbsp;</td>
      <td>

           <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="name"  value="RefreshThisPageButton" />
              <jsp:param name="image" value="common.RefreshThisPageImg" />
              <jsp:param name="text"  value="common.common.RefreshThisPageText" />
              <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
              <jsp:param name="width" value="140" />
           </jsp:include>


      </td>
      <td width="20" nowrap>
        <img src="/portal/images/Blank_4x4.gif" width="4" height="4">
      </td>
    </tr>
  </table>

<%
System.out.println(searchType + "Rajdeep-Invoice");

searchListViewName = "InvoicesListView.xml";

%>
      
  <%@ include file="/transactions/fragments/InvoiceSearchFilter.frag" %>



   <input type=hidden name=SearchType value=<%=searchType%>>
    
  
   <jsp:include page="/common/TradePortalListView.jsp">
      <jsp:param name="listView"       value='<%= searchListViewName %>' /> 
      <jsp:param name="whereClause2"   value='<%= dynamicWhereClause.toString() %>' />
      <jsp:param name="userType"       value='<%= userSecurityType %>' />
      <jsp:param name="searchCriteria" value='<%= newSearchCriteria.toString() %>' />
   </jsp:include>
 

 </table>

  
 <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="37" nowrap align="left" valign="top">
        <img src="/portal/images/UpIndicatorArrow.gif" width="37">
      </td>
      <td width="5" nowrap>
        <img src="/portal/images/Blank_4x4.gif" width="4" height="4">
      </td>
      <td valign=bottom>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table><tr><td>
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
           <jsp:param name="name"         value="Delete" />
           <jsp:param name="image"        value='common.DeleteSelectedItemsImg' />
           <jsp:param name="text"         value='common.DisputeUndisputeSelectedItems' />
           <jsp:param name="extraTags"    value="return confirmDelete()" />
           <jsp:param name="width"        value="152" />
           <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS%>" />
        </jsp:include>
        </td></tr></table>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td width="264">&nbsp;</td>
      <td width="100%">&nbsp;</td>
      <td width="264" valign=bottom>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table>
        <tr>
      
        <%-- TLE - 07/29/07 - CR-375 - Add Begin --%>
        <td>       
	<% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY) && canProcessPOForATP && userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) { %>
	            <jsp:include page="/common/RolloverButtonSubmit.jsp">
	               <jsp:param name="showButton"   value="true" />
	               <jsp:param name="name"         value="CreateATPs" />
	               <jsp:param name="image"        value='common.CreateATPsUsingRulesImg' />
	               <jsp:param name="text"         value='common.CloseSelectedItems' />
	               <jsp:param name="width"        value="140" />
	               <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS%>" />
	            </jsp:include>
        <% } else { %>
	            &nbsp;
	<% } %>
	</td>
        <td width="15" nowrap>&nbsp;</td>
        <%-- TLE - 07/29/07 - CR-375 - Add End --%>
        
        <td>
      <% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DLC_CREATE_MODIFY) && canProcessPOForDLC && userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) { %>
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
           <jsp:param name="showButton"   value="true" />
           <jsp:param name="name"         value="CreateLCs" />
           <jsp:param name="image"        value='common.CreateLCsUsingRulesImg' />
           <jsp:param name="text"         value='common.FinanceSelectedItems' />
           <jsp:param name="width"        value="140" />
           <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS%>" />
        </jsp:include>
      <% } else { %>
        &nbsp;
      <% } %>
        </td></tr></table>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
