<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                      Transactions Home Purchase Order Tab

  Description:
    Contains HTML to create the Purchase Orders tab for the Transactions Home 
  page.  Actions on this page include: linking to the Purchase Order Upload Log
  page, refreshing the page, linking to the Upload Purchase Order Data page,
  deleting selecting purchase order line items, and executing the create lc
  from po line items process.  Additionally, clicking on a po line item displays
  the detail for that line item.

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-PurchaseOrdersListView.jsp" %>
*******************************************************************************
--%> 

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%" nowrap>
        <p class="ListHeaderTextWhite">
          <%=resMgr.getText("PurchaseOrders.NotAssignedLine1", 
                            TradePortalConstants.TEXT_BUNDLE)%>
          <br>
          <%=resMgr.getText("PurchaseOrders.NotAssignedLine2", 
                            TradePortalConstants.TEXT_BUNDLE)%>
        </p>
      </td>
      <td width="50" height="48">&nbsp;</td>
      <td>
<%
         // This is the link for the View Log button
         newLink.append(formMgr.getLinkAsUrl("goToViewPOUploadLog", response));
         newLink.append("&current2nvNav=");
         newLink.append(TradePortalConstants.NAV__PURCHASE_ORDERS);
%>
         <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="ViewLog" />
            <jsp:param name="image" value="common.ViewLogImg" />
            <jsp:param name="text"  value="common.ViewLogText" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="95" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>
<%
         // This is the link for the Refresh This Page button
         newLink = new StringBuffer("");
         newLink.append(formMgr.getLinkAsUrl("goToTransactionsHome", response));
         newLink.append("&current2nvNav=");
         newLink.append(TradePortalConstants.NAV__PURCHASE_ORDERS);
%>
         <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="RefreshThisPageButton" />
            <jsp:param name="image" value="common.RefreshThisPageImg" />
            <jsp:param name="text"  value="common.RefreshThisPageText" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="140" />
         </jsp:include>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td>
<%
         // This is the link for the Upload PO Data button
         newLink = new StringBuffer("");
         newLink.append(formMgr.getLinkAsUrl("goToUploadPOData", response));
         newLink.append("&current2nvNav=");
         newLink.append(TradePortalConstants.NAV__PURCHASE_ORDERS);
%>
         <jsp:include page="/common/RolloverButtonLink.jsp">
            <jsp:param name="name"  value="UploadPOData" />
            <jsp:param name="image" value="common.UploadPODataImg" />
            <jsp:param name="text"  value="common.UploadPODataText" />
            <jsp:param name="link"  value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
            <jsp:param name="width" value="95" />
         </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

  <%--TLE - 09/28/06 - CR381 - Add Begin --%>
  <%@ include file="/transactions/fragments/POSearch.frag" %>
  <%--TLE - 09/28/06 - CR381 - Add End --%>

  <jsp:include page="/common/TradePortalListView.jsp">
      <jsp:param name="listView"     value="PurchaseOrdersListView.xml"/>
      <jsp:param name="whereClause2" value='<%= dynamicWhereClause.toString() %>' />
      <jsp:param name="userType"     value='<%= userSecurityType %>' />  
      <jsp:param name="userTimezone" value='<%=userSession.getTimeZone()%>' />
  </jsp:include>

  <br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="37" nowrap align="left" valign="top">
        <img src="/portal/images/UpIndicatorArrow.gif" width="37">
      </td>
      <td width="5" nowrap>
        <img src="/portal/images/Blank_4x4.gif" width="4" height="4">
      </td>
      <td valign=bottom>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table><tr><td>
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
           <jsp:param name="name"         value="Delete" />
           <jsp:param name="image"        value='common.DeleteSelectedItemsImg' />
           <jsp:param name="text"         value='common.DeleteSelectedItemsText' />
           <jsp:param name="extraTags"    value="return confirmDelete()" />
           <jsp:param name="width"        value="152" />
           <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_DELETE_PO%>" />
        </jsp:include>
        </td></tr></table>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td width="264">&nbsp;</td>
      <td width="100%">&nbsp;</td>
      <td width="264" valign=bottom>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">  
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
        <table>
        <tr>
      
        <%-- TLE - 07/29/07 - CR-375 - Add Begin --%>
        <td>       
	<% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY) && canProcessPOForATP && userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) { %>
	            <jsp:include page="/common/RolloverButtonSubmit.jsp">
	               <jsp:param name="showButton"   value="true" />
	               <jsp:param name="name"         value="CreateATPs" />
	               <jsp:param name="image"        value='common.CreateATPsUsingRulesImg' />
	               <jsp:param name="text"         value='common.CreateATPsUsingRulesText' />
	               <jsp:param name="width"        value="140" />
	               <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_CREATE_ATP_FROM_PO%>" />
	            </jsp:include>
        <% } else { %>
	            &nbsp;
	<% } %>
	</td>
        <td width="15" nowrap>&nbsp;</td>
        <%-- TLE - 07/29/07 - CR-375 - Add End --%>
        
        <td>
      <% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DLC_CREATE_MODIFY) && canProcessPOForDLC && userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) { %>
        <jsp:include page="/common/RolloverButtonSubmit.jsp">
           <jsp:param name="showButton"   value="true" />
           <jsp:param name="name"         value="CreateLCs" />
           <jsp:param name="image"        value='common.CreateLCsUsingRulesImg' />
           <jsp:param name="text"         value='common.CreateLCsUsingRulesText' />
           <jsp:param name="width"        value="140" />
           <jsp:param name="submitButton" value="<%=TradePortalConstants.BUTTON_CREATE_LC_FROM_PO%>" />
        </jsp:include>
      <% } else { %>
        &nbsp;
      <% } %>
        </td></tr></table>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
