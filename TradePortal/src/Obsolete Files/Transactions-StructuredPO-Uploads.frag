<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        PO Management Tab

  Description:
    Contains HTML to create the PO Uploads tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-StructuredPO_Uploads.frag" %>
*******************************************************************************
--%>

<%
	boolean readOnlyATP = false;
	boolean readOnlyDLC = false;
	
	//ShilpaR IR - BOUM032835703 Strat
	
	// taking sortOrder parameter from request if user is manually pressing link on column header on list view to 
	// sort data in either Ascending  or descending. bydefault file upload list view needed to be in descending order.
	String sortOrder =  request.getParameter("sortOrder");
	if(InstrumentServices.isBlank(sortOrder)){
	     sortOrder = "D";
	 }
    // Shilpa IR - BOUM032835703  End
    
	// Replaced canProcessPOForATP and canProcessPOForDLC
	if ((userOrgAutoATPCreateIndicator != null && !userOrgAutoATPCreateIndicator
			.equals(TradePortalConstants.INDICATOR_YES))
			|| !canProcessPOForATP) {
		readOnlyATP = true;
	}

	if ((userOrgAutoLCCreateIndicator != null && !userOrgAutoLCCreateIndicator
			.equals(TradePortalConstants.INDICATOR_YES))
			|| !canProcessPOForDLC) {
		readOnlyDLC = true;
	}

	String path = corpOrg.getAttribute("restricted_po_upload_dir");
	if(path!=null){
	path = path.replaceAll("\\\\", "\\\\\\\\");
	}
	else{
		path ="";
	}
	String newPath = null;
	String clicktag = "return restrictAttach(document.forms[0],'"
			+ path + "');";
%>
<SCRIPT LANGUAGE="JavaScript">

var blink_speed=200;
var i=0;

function restrictAttach(form,path) {

var allowUpload = true; 

var file = form.PaymentDataFilePath.value;
if (!file) {
	// BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.NoPOFileError",
					TradePortalConstants.TEXT_BUNDLE))%>');
	return false;
}
//ShilpaR IR SHUM041138201 Start
var filedef = form.PODefinitionName.value;

if (!filedef) {
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.NoPOFileDefError",
					TradePortalConstants.TEXT_BUNDLE))%>');
	return false;
}
//ShilpaR IR SHUM041138201 End

if (path){
var delimiter = "\\";
	delimiter = (file.lastIndexOf("//") != -1)?"//":"\\";
	delimiter = (file.lastIndexOf("/") != -1)?"/":"\\";
			
if (file.lastIndexOf(delimiter) != -1){
 var slashIndex = file.lastIndexOf(delimiter) ;
 file = file.substring(0,slashIndex);
 newPath = file;

//if path is not same as restricted path the show up the error
if(newPath != path){
	// BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.RestrictFilePathErrorText",
					TradePortalConstants.TEXT_BUNDLE))%>');
return false;
	 }
	}
  }
  //Purchase Order Data validation

  if (document.forms[0].POUploadDataOption[1].checked == true) {
   var instrOptionlist = document.getElementsByName("POUploadInstrOption");
   var insOptionChecked =false;
   for(var i = 0; i < instrOptionlist.length; i++) {
    if (instrOptionlist[i].type == 'radio' && instrOptionlist[i].checked == true) {
	   insOptionChecked = true;
	   break;
	 }
   }
   if (!insOptionChecked) {
	  alert('<%=StringFunction.asciiToUnicode(resMgr.getText("StructuredPOUpload.instrTypeMissing",TradePortalConstants.TEXT_BUNDLE))%>');
	  return false;
   }                  
  

  if (document.forms[0].POUploadGroupingOption[0].checked == false && document.forms[0].POUploadGroupingOption[1].checked == false) {
	// BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText("StructuredPOUpload.instrTypeMissing",TradePortalConstants.TEXT_BUNDLE))%>');
	return false;
  }
}


   if (allowUpload) {
	   form.action ="<%=response.encodeURL(request.getContextPath()
					+ "/transactions/POStructuredFileUploadServlet.jsp")%>";
	   form.enctype="multipart/form-data";
	   form.encoding="multipart/form-data";
	   document.getElementById('UploadButtonDiv').style.visibility = 'hidden';
	   document.getElementById('UploadButtonDiv').style.display = 'none';	
	   document.getElementById('UploadMessage').style.visibility = 'visible';
			document.getElementById('UploadMessage').style.display = 'block';
	   Blink('UploadMessage');	
	   return true;
   }
   else {
       // BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode
       alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.ValidFileExtionsionText",
					TradePortalConstants.TEXT_BUNDLE))%>');
       return false;
   }
}

function Blink(layerName){
		
	if(i%2==0) {
		eval("document.all"+'["'+layerName+'"]'+
				'.style.visibility="visible"');
	} else {
 			eval("document.all"+'["'+layerName+'"]'+
			  '.style.visibility="hidden"');
		}
		 
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}
	
function confirmDelete()
{
  <%-- BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
  var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("CashMgmtFutureValue.PopupMessage",
					TradePortalConstants.TEXT_BUNDLE))%>";

   if (!confirm(confirmMessage)) 
    {
       formSubmitted = false;
       return false;
    }
  else
  {
     return true;
  }
}


<%--
//This function unchecks the PO Items grouping option if the second PO data 
//option is selected and checkes the first PO Items grouping option only if 
//the user is changing to the first PO data option (does nothing if it's 
//already selected.
--%>
function setPOUploadGroupingOption()
{
if (document.forms[0].POUploadDataOption[0].checked == true)
{
   var instrOptionlist = document.getElementsByName("POUploadInstrOption");
   for(var i = 0; i < instrOptionlist.length; i++) {
     instrOptionlist[i].checked = false
    }                  
   document.forms[0].POUploadGroupingOption[0].checked = false;
   document.forms[0].POUploadGroupingOption[1].checked = false;
   document.forms[0].POUploadDataOption[1].checked = false;
}
else
{
   if (document.forms[0].POUploadDataOption[1].checked == true) 
   {
         document.forms[0].POUploadDataOption[0].checked = false;
    }
}
}

//This function checks the first PO data option if it hasn't already been 
//selected. This function gets called when the user selects either of the 
//PO Items grouping options.
function setPOUploadDataOption()
{
if (document.forms[0].POUploadDataOption[1].checked == false)
{
   document.forms[0].POUploadDataOption[1].checked = true;
}
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="100%" height="40">&nbsp;</td>
		<td>
		<%
			userDefaultWipView = userSession.getDefaultWipView();
			selectedWorkflow = request.getParameter("workflow");

			if (selectedWorkflow == null) {
				selectedWorkflow = (String) session.getAttribute("workflow");

				if (selectedWorkflow == null) {
					if (userDefaultWipView
							.equals(TradePortalConstants.WIP_VIEW_MY_ORG)) {
						selectedWorkflow = userOrgOid;
					} else if (userDefaultWipView
							.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN)) {
						selectedWorkflow = ALL_WORK;
					} else {
						selectedWorkflow = MY_WORK;
					}
					selectedWorkflow = EncryptDecrypt
							.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
				}
			}

			session.setAttribute("workflow", selectedWorkflow);

			selectedWorkflow = EncryptDecrypt
					.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
			if (selectedWorkflow.equals(MY_WORK)) {
				dynamicWhereClause
						.append("and p.a_user_oid = ");
				dynamicWhereClause.append(userOid);
			} else if (selectedWorkflow.equals(ALL_WORK)) {
				dynamicWhereClause
						.append("and p.a_owner_org_oid in (");
				dynamicWhereClause.append(" select organization_oid");
				dynamicWhereClause.append(" from corporate_org");
				dynamicWhereClause.append(" where activation_status = '");
				dynamicWhereClause.append(TradePortalConstants.ACTIVE);
				dynamicWhereClause.append("' start with organization_oid = ");
				dynamicWhereClause.append(userOrgOid);
				dynamicWhereClause
						.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
			} else {
				dynamicWhereClause
						.append("and p.a_owner_org_oid  = ");
				dynamicWhereClause.append(selectedWorkflow);
			}
			//
			newLink.append(formMgr.getLinkAsUrl("goToTransactionsHome",
					response));
			newLink.append("&currentTab=");
			newLink.append(TradePortalConstants.STRUCTURED_PO);
			newLink.append("&workflow=");
			newLink.append(EncryptDecrypt
					.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
		%> <jsp:include page="/common/RolloverButtonLink.jsp">
			<jsp:param name="name" value="RefreshThisPageButton" />
			<jsp:param name="image" value="common.RefreshThisPageImg" />
			<jsp:param name="text" value="common.RefreshThisPageText" />
			<jsp:param name="link"
				value="<%=java.net.URLEncoder.encode(newLink.toString())%>" />
			<jsp:param name="width" value="140" />
		</jsp:include></td>
		<td width="20" nowrap>&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="ColorGrey">
	<tr>
		<td width="5" nowrap>&nbsp;</td>
		<td nowrap align="left" valign="middle" nowrap>
		<p class="ControlLabel"><%=resMgr.getText("LocateUploadPOFile.InstructionsText",
					TradePortalConstants.TEXT_BUNDLE)%>&nbsp;&nbsp;</p>
		</td>
	</tr>
</table>

<div id="UploadButtonDiv">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="15">&nbsp;</td>

		<%-- 1st td start --%>
		<td nowrap align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
				<p class="ControlLabel"><%=resMgr.getText(
					"LocateUploadPOFile.PODataFileLocation",
					TradePortalConstants.TEXT_BUNDLE)%><span class="Asterix">*</span></p>
				</td>
			</tr>
			<tr>
				<td>
				<p class="ListText"><%=resMgr.getText("LocateUploadPOFile.BrowseLocation",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
			</tr>
			<tr>
				<td align="left"><%=InputField.createFileField("PaymentDataFilePath", "50",
					"ListText", false)%></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td nowrap align="left" valign="bottom">
				<p class="ControlLabel"><%=resMgr.getText(
					"LocateUploadPOFile.PODefinition",
					TradePortalConstants.TEXT_BUNDLE)%><span class="Asterix">*</span></p>
				</td>
			</tr>

			<tr>
				<td nowrap align="left" valign="bottom">
				<p class="ListText"><%=resMgr.getText("LocateUploadPOFile.SelectFileDef",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
			</tr>
			<tr>
				<td align="left" valign="bottom">
				<%
				// Get the current xml doc from the doc cache and check for errors
				  DocumentHandler xmlDoc = formMgr.getFromDocCache();
				String poDefinitionOid ="";
				String hasErrors = xmlDoc.getAttribute("/In/HasErrors");

				   /*if ((hasErrors != null) && (hasErrors.equals(TradePortalConstants.INDICATOR_YES)))
				   {
					   poDefinitionOid = xmlDoc.getAttribute("/In/PODefinitionName");
				   }
				      else
				      {*/
				      //jgadela  R90 IR T36000026319 - SQL FIX
						Object[] sqlParamsDefaltDef = new Object[2];
						sqlParamsDefaltDef[0] =  "Y";
						sqlParamsDefaltDef[1] =  userSession.getOwnerOrgOid();
				         // Default the PO Upload Definition dropdown to be the default definition for this corporate org
				         String defaultDefinitionSql = "select PURCHASE_ORDER_DEFINITION_OID from PURCHASE_ORDER_DEFINITION where default_flag = ? and a_owner_org_oid = ?";
				         DocumentHandler results = DatabaseQueryBean.getXmlResultSet(defaultDefinitionSql, false, sqlParamsDefaltDef);

				         if(results != null)
				          {
				        	 poDefinitionOid = results.getAttribute("/ResultSetRecord(0)/PURCHASE_ORDER_DEFINITION_OID");
				          }
				      //}
					// This is the query used for populating the Purchase Order Definition dropdown list;
					// it retrieves all Purchase Order Definitions that belong to the corporate customer 
					// user's org that are used for uploading 
					//jgadela  R90 IR T36000026319 - SQL FIX
      				java.util.List<Object> sqlParamList = new java.util.ArrayList();
					
					StringBuffer sqlQuery1 = new StringBuffer();
					sqlQuery1.append("select PURCHASE_ORDER_DEFINITION_OID, NAME");
					sqlQuery1.append(" from PURCHASE_ORDER_DEFINITION");
					sqlQuery1.append(" where a_owner_org_oid in (?");
					sqlParamList.add(userSession.getOwnerOrgOid());

					// Also include purchase order definitions from the user's actual organization if using subsidiary access
					if (userSession.showOrgDataUnderSubAccess()) {
						sqlQuery1.append(",?");
						sqlParamList.add(userSession.getSavedUserSession().getOwnerOrgOid());
					}

					sqlQuery1.append(") order by ");
					sqlQuery1.append(resMgr.localizeOrderBy("name"));

					DocumentHandler poDefinitionDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery1.toString(), false, sqlParamList);

					if (poDefinitionDoc == null)
						poDefinitionDoc = new DocumentHandler();
					if(poDefinitionOid== null)
						poDefinitionOid="";
					
					StringBuffer poDefOptions = new StringBuffer();
					poDefOptions
							.append(Dropdown.createSortedOptions(
									poDefinitionDoc,
									"PURCHASE_ORDER_DEFINITION_OID", "NAME",
									poDefinitionOid, userSession.getSecretKey(), resMgr.getResourceLocale()));
					out.print(InputField.createSelectField("PODefinitionName", "",resMgr.getText("LocateUploadPO.SelectUploadDefinition", 
                            TradePortalConstants.TEXT_BUNDLE), poDefOptions.toString(), "ListText", false));
				%>
				</td>
			</tr>
		</table>
		</td>
		<%-- 1st td end --%>

		<%-- 2nd td start --%>
		<td nowrap align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ControlLabel"><%=resMgr.getText("LocateUploadPOFile.POData",
					TradePortalConstants.TEXT_BUNDLE)%><span class="Asterix">*</span></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
		</table>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="60" nowrap>&nbsp;</td>
				<td><%=InputField.createRadioButtonField("POUploadDataOption",
					TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS, "", true,
					"ListText", "ListText", false,
					"onClick=\"setPOUploadGroupingOption()\"")%></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText("LocateUploadPOFile.MakePOAvailable",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
		</table>

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="60" nowrap>&nbsp;</td>
				<td><%=InputField.createRadioButtonField("POUploadDataOption",
					TradePortalConstants.GROUP_PO_LINE_ITEMS, "", false,
					"ListText", "ListText", false,
					"onClick=\"setPOUploadGroupingOption()\"")%></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText("LocateUploadPOFile.GroupUploadedPO",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<%
				//Show only if user has the security right to opt for Import LC for PO file upload.
				if (!readOnlyDLC) {
			%>
			<tr>
				<td width="100" nowrap>&nbsp;</td>
				<td><%=InputField.createRadioButtonField(
						"POUploadInstrOption",
						TradePortalConstants.AUTO_LC_PO_UPLOAD, "", false,
						"ListText", "ListText", readOnlyDLC,
						"onClick=\"setPOUploadDataOption()\"")%></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText("LocateUploadPO.ImportLC",
						TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
			<%
				} //if(!readOnlyDLC)
					//Show only if user has the security right to opt for Approval to Pay for PO file upload. 
				if (!readOnlyATP) {
			%>
			<tr>
				<td width="100" nowrap>&nbsp;</td>
				<td><%=InputField.createRadioButtonField(
						"POUploadInstrOption",
						TradePortalConstants.AUTO_ATP_PO_UPLOAD, "", false,
						"ListText", "ListText", readOnlyATP,
						"onClick=\"setPOUploadDataOption()\"")%></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText("LocateUploadPO.ApprovalToPay",
						TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
			<%
				} //if(!readOnlyATP)
			%>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="60" nowrap>&nbsp;</td>
				<td width="30" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText(
					"LocateUploadPOFile.DataUserWantsToInclude",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="100" nowrap>&nbsp;</td>
				<td><%=InputField.createRadioButtonField(
					"POUploadGroupingOption",
					TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS, "", false,
					"ListText", "ListText", false,
					"onClick=\"setPOUploadDataOption()\"")%></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>
				<p class="ListText"><%=resMgr.getText(
					"LocateUploadPOFile.IncludePODataInTheFile",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td width="100%">&nbsp;</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><%=InputField.createRadioButtonField(
					"POUploadGroupingOption",
					TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS, "",
					false, "ListText", "ListText", false,
					"onClick=\"setPOUploadDataOption()\"")%></td>
				<td>&nbsp;</td>
				<td>
				<p class="ListText"><%=resMgr.getText("LocateUploadPOFile.UseOnlyPO",
					TradePortalConstants.TEXT_BUNDLE)%></p>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>

</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="90%">&nbsp;</td>
		<td>&nbsp;</td>

		<td align="left" valign="bottom"><jsp:include
			page="/common/RolloverButtonSubmit.jsp">
			<jsp:param name="showButton" value="true" />
			<jsp:param name="name"
				value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
			<jsp:param name="image" value='common.UploadFileImg' />
			<jsp:param name="text" value='common.UploadFileText' />
			<jsp:param name="width" value="96" />
			<jsp:param name="submitButton"
				value="<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>" />
			<jsp:param name="imageName" value="UploadFileButton" />
			<jsp:param name="extraTags" value='<%=clicktag%>' />
		</jsp:include></td>
	</tr>
</table>
</div>

<div id="UploadMessage" style="display: none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>

		<p class="ControlLabel"><font color="blue"> <%=resMgr.getText("PaymentUploadLogDetail.Uploading",
					TradePortalConstants.TEXT_BUNDLE)%>..... </font></p>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="15" nowrap>&nbsp;</td>
		<td nowrap align="left" valign="middle" class="ListHeaderTextWhite">
		<%=resMgr.getText("PaymentFileUpload.Show",
					TradePortalConstants.TEXT_BUNDLE)%></td>
		<td align="left" valign="middle" class="ListText">
		<%
			StringBuffer prependText = new StringBuffer();
			prependText.append(resMgr.getText("PaymentFileUpload.WorkFor",
					TradePortalConstants.TEXT_BUNDLE));
			prependText.append(" ");

			// If the user has rights to view child organization work, retrieve the 
			// list of dropdown options from the query listview results doc (containing
			// an option for each child org) otherwise, simply use the user's org 
			// option to the dropdown list (in addition to the default 'My Work' option).

			if (SecurityAccess.hasRights(userSecurityRights,
					SecurityAccess.VIEW_CHILD_ORG_WORK)) {
				dropdownOptions.append(Dropdown.getUnsortedOptions(
						hierarchyDoc, "ORGANIZATION_OID", "NAME",
						prependText.toString(), selectedWorkflow, userSession.getSecretKey()));

				// Only display the 'All Work' option if the user's org has child organizations
				if (totalOrganizations > 1) {
					dropdownOptions.append("<option value=\"");
					dropdownOptions.append(EncryptDecrypt
							.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
					dropdownOptions.append("\"");

					if (selectedWorkflow.equals(ALL_WORK)) {
						dropdownOptions.append(" selected");
					}

					dropdownOptions.append(">");
					dropdownOptions.append(ALL_WORK);
					dropdownOptions.append("</option>");
				}
			} else {
				dropdownOptions.append("<option value=\"");
				dropdownOptions.append(EncryptDecrypt
						.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
				dropdownOptions.append("\"");

				if (selectedWorkflow.equals(userOrgOid)) {
					dropdownOptions.append(" selected");
				}

				dropdownOptions.append(">");
				dropdownOptions.append(prependText.toString());
				dropdownOptions.append(userSession.getOrganizationName());
				dropdownOptions.append("</option>");
			}

			extraTags.append("onchange=\"location='");
			extraTags.append(formMgr.getLinkAsUrl("goToTransactionsHome",
					response));
			extraTags.append("&amp;currentTab=");
			extraTags.append(TradePortalConstants.STRUCTURED_PO);
			extraTags
					.append("&amp;workflow='+this.options[this.selectedIndex].value\"");

			String defaultValue = "";
			String defaultText = "";

			if (!userSession.hasSavedUserSession()) {
				defaultValue = EncryptDecrypt
						.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
				defaultText = MY_WORK;
			}

			out.print(InputField.createSelectField("Workflow", defaultValue,
					defaultText, dropdownOptions.toString(), "ListText", false,
					extraTags.toString()));
		%>
		</td>
		<td width="100%" height="40">&nbsp;</td>
	</tr>
</table>

<jsp:include page="/common/TradePortalListView.jsp">
	<jsp:param name="listView" value="POFileUploadListView.xml" />
	<jsp:param name="whereClause2"
		value='<%= dynamicWhereClause.toString() %>' />
	<%-- ShilpaR IR - BOUM032835703  Begin passing sortOrder to default as desending order and also redisplayList as it is required else sort order won't work--%>
	<jsp:param name="sortOrder" value='<%= sortOrder %>' />
	<jsp:param name="redisplayList" value="N" />
	<%-- ShilpaR IR - BOUM032835703  End --%>
	<jsp:param name="userType" value='<%= userSecurityType %>' />
	<jsp:param name="userTimezone" value='<%=userSession.getTimeZone()%>' />
	<jsp:param name="maxScrollHeight" value="316" />
</jsp:include>

<%--  The table below displays the Delete buttons --%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="BankColor">
	<tr>
		<td width="20" nowrap>&nbsp;</td>
		<td width="37" nowrap align="left" valign="top"><img
			src="/portal/images/UpIndicatorArrow.gif" width="37" height="27">
		</td>
		<td width="5" nowrap><img src="/portal/images/Blank_4x4.gif"
			width="4" height="4"></td>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="BankColor">
			<tr>
				<td><img src="/portal/images/Blank_4x4.gif" width="4"
					height="4"></td>
			</tr>
		</table>
		<table>
			<tr>
				<td width="15" nowrap>&nbsp;</td>
				<td width="264" valign=bottom>
				<%
					if (SecurityAccess.hasRights(userSecurityRights,
							SecurityAccess.INSTRUMENT_DELETE)) {
				%> <jsp:include page="/common/RolloverButtonSubmit.jsp">
					<jsp:param name="name" value="Delete" />
					<jsp:param name="image"
						value='InvoiceMgmtUploads.RemoveFailedItemsImg' />
					<jsp:param name="text"
						value='InvoiceMgmtUploads.RemoveFailedItemsText' />
					<jsp:param name="extraTags" value="return confirmDelete()" />
					<jsp:param name="width" value="152" />
					<jsp:param name="submitButton"
						value="<%=TradePortalConstants.BUTTON_DELETE_PO_UPLOAD%>" />
				</jsp:include> <%
 	} else {
 		out.print("&nbsp;");
 	}
 %>
				</td>

			</tr>
		</table>
		</td>
		<td width="100%">&nbsp;</td>
	</tr>
</table>
</td>
</tr>
</table>


