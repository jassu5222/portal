<%--
*******************************************************************************
                                  Reset User Account

  Description:
     Displays a page where a user's account can be reset after being locked.
     This page is accessed by first going to the Locked Out User listview.  
     An encrypted OID must be passed to this page from that listview to determine
     which user's account to reset.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.ams.tradeportal.busobj.util.*,
                 com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"/>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"/>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%
   	// Get the document from the cache.  This is used to set the radio buttons back to the
   	// way the user had previously set them if there was an error.
	DocumentHandler doc = formMgr.getFromDocCache();   

    boolean changingPasswordFlag;
    String onLoad;

    String changingPasswordFlagFromXml = doc.getAttribute("/In/changingPasswordFlag");
    if(!InstrumentServices.isBlank(changingPasswordFlagFromXml) &&
        changingPasswordFlagFromXml.equals("true"))
     {
            changingPasswordFlag = true;
            onLoad = "document.ResetUserAccount.changingPasswordFlag[1].focus();";
     }
    else
     {
            // Default the radio button setting and the focus to "don't change password"
            // if not coming from an error situation
            changingPasswordFlag = false;
            onLoad = "document.ResetUserAccount.changingPasswordFlag[0].focus();";
     }
%>

<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>


<form name="ResetUserAccount" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  <%
	Hashtable addlParms = new Hashtable();

    // The OID of the user being reset must be sent to the mediator
    // It either comes from the XML if there were errors or from the URL parameters
    String userOID;
    if(doc.getAttribute("/In/userOID") != null)
        userOID = doc.getAttribute("/In/userOID");
    else
        userOID = EncryptDecrypt.decryptAlphaNumericString(request.getParameter("selection"));
    
    // Pass the user OID to the user
	addlParms.put("userOID", userOID);

    // Flag to indicate that this user should be reset
    addlParms.put("resettingUserFlag", "true");

    // If the password is being changed, force the user to change it again
    // once they log in
    addlParms.put("force", "true");

    // If the password is being changed, indicate that there is no validation
    // that the user entered their current password
    addlParms.put("checkCurrentPasswordFlag", "false");
  %>

  <%= formMgr.getFormInstanceAsInputField("UnlockUserAccount", addlParms) %>

 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="34">
    <tr> 
      <td width="20" class="BankColor" nowrap>&nbsp;</td>
      <td class="BankColor" align="left" valign="middle" nowrap><span class="HeadingTextWhite"><%=resMgr.getText("ResetUserAccount.ResetUserAccount", TradePortalConstants.TEXT_BUNDLE)%></span></td>
      <td class="BankColor" width="90%">&nbsp;</td>
      <td class="BankColor"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
          <tr> 
            <td width="8" height="17"><%= OnlineHelp.createContextSensitiveLink("/admin/reset_user_account.htm", resMgr, userSession)%></td>
          </tr>
        </table>
      </td>
      <td width="10" class="BankColor" nowrap>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="45" nowrap>&nbsp;</td>
      <td width="100%"> 
        <p class="ListText"><%=resMgr.getText("ResetUserAccount.Text", TradePortalConstants.TEXT_BUNDLE)%></p>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="65" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <table>
          <tr>
            <td>
              <%=InputField.createRadioButtonField("changingPasswordFlag", "false", "", 
                           !changingPasswordFlag, "ListText", "", false, "")%>
            </td>
            <td class="ListText" nowrap align="left">
              <%=resMgr.getText("ResetUserAccount.ResetOnlyLoginCount", TradePortalConstants.TEXT_BUNDLE) %>
            </td>
            <td width="50" nowrap>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>
              <%=InputField.createRadioButtonField("changingPasswordFlag", "true", "", 
                           changingPasswordFlag, "ListText", "", false, "")%>
            </td>
            <td class="ListText" nowrap align="left">
              <%=resMgr.getText("ResetUserAccount.ResetCountAndPassword", TradePortalConstants.TEXT_BUNDLE) %>
            </td>
            <td width="50" nowrap>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td class="ControlLabel" nowrap align="left" valign="bottom">
	           <%= resMgr.getText("ResetUserAccount.NewPassword", TradePortalConstants.TEXT_BUNDLE) %><br>
               <%= InputField.createPasswordField("newPassword", "", "32", "32", "ListText", false, "onChange=\"document.ResetUserAccount.changingPasswordFlag[1].checked = true\"") %></td>
            <td width="50" nowrap>&nbsp;</td>
            <td class="ControlLabel" nowrap align="left" valign="bottom">
               <%=resMgr.getText("ResetUserAccount.RetypeNewPassword", TradePortalConstants.TEXT_BUNDLE) %><br>
               <%= InputField.createPasswordField("retypePassword", "", "32", "32","ListText", false, "onChange=\"document.ResetUserAccount.changingPasswordFlag[1].checked = true\"") %></td>
         </tr>
        </table>
       </td>
       <td width="100%">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr>
       <td><img src="/portal/images/Blank_4x4.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="BankColor">
    <tr valign=middle> 
      <td width="20" nowrap>&nbsp;</td>
      <td width="100%" class="BankColor">&nbsp;</td>
      <td width="9">
          <jsp:include page="/common/RolloverButtonSubmit.jsp">
              <jsp:param name="name" value="ResetAccountButton" />
              <jsp:param name="extraTags" value="return confirmSelection();" />
              <jsp:param name="image" value='common.ResetAccountImg' />
              <jsp:param name="text" value='common.ResetAccountText' />
              <jsp:param name="submitButton"
                         value="<%=TradePortalConstants.BUTTON_RESET_ACCOUNT%>" />
            </jsp:include>
      <td width="14">&nbsp;</td>
      <td nowrap>
<%
            String link = formMgr.getLinkAsUrl("goToAdminRefDataHome", response);
%>
            <jsp:include page="/common/RolloverButtonLink.jsp">
              <jsp:param name="name" value="CancelButton" />
              <jsp:param name="image" value='common.CancelImg' />
              <jsp:param name="text" value='common.CancelText' />
              <jsp:param name="link" value="<%=java.net.URLEncoder.encode(link)%>" />
            </jsp:include>
      </td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>

</form>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>





