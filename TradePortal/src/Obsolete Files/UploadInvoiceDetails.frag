<%--
*******************************************************************************
                        Invoice Detail

  Description:
     This page is used to display the detail for an invoice.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  String oid = "";
  String groupOID = "";
  boolean showDelete = true;
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  DocumentHandler doc;
  String invoicestatus = null;
  String addressLine1 = "";
  String addressLine2 = "";
  String addressCity = "";
  String addressCtry = "";
  String currency = null;
  String useCurrency = null;	
  String listViewToSort = null;
  DocumentHandler invoiceSummaryGoodsResult         = null;
  DocumentHandler invoiceListDetailResult           = null;
  DocumentHandler invoiceTradingPartnerDetailResult = null;
  String linkArgs[] = {"","","","","",""};
  String LogGridHtml = "";
  String logGridLayout = "";
  String upload_invoice_oid = "";
  String linkString = "";
  ArMatchingRuleWebBean_Base rule = null;
  String userOrgOid         = userSession.getOwnerOrgOid();
//MEer Rel 8.3 T36000021992 
  boolean ignoreDefaultPanelValue = false;
  CorporateOrganizationWebBean corpOrg = (CorporateOrganizationWebBean) beanMgr.createBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
  corpOrg.getById(userOrgOid);
  
  InvoicesSummaryDataWebBean_Base InvoicesSummaryData = (InvoicesSummaryDataWebBean_Base) beanMgr.createBean(
               			  "com.ams.tradeportal.busobj.webbean.InvoicesSummaryDataWebBean_Base",
                       		  "InvoicesSummaryData");
				//RKAZI CR709 Rel 8.2 01/25/2013 -Start
  InvoicePaymentInstructionsWebBean_Base InvoicePaymentInstructions = (InvoicePaymentInstructionsWebBean_Base) beanMgr.createBean(
			  "com.ams.tradeportal.busobj.webbean.InvoicePaymentInstructionsWebBean_Base",
       		  "InvoicePaymentInstructions");
				//RKAZI CR709 Rel 8.2 01/25/2013 -End                       		                    
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale        = userSession.getUserLocale();
  String userSecurityType   = userSession.getSecurityType();
  String userTimeZone       = userSession.getTimeZone();
  String corpOrgOid = userSession.getOwnerOrgOid();
     // Get the listview to sort if the user clicked on a listview header link; we only want to sort the listview the
   // user clicked on, NOT both listviews on the page
   listViewToSort = request.getParameter("listView");
  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/In/InvoicesSummaryData") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item)
     
     groupOID = request.getParameter("a_invoice_group_oid");
     oid = request.getParameter("upload_invoice_oid");          
     if (oid != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
     }
     if (oid == null) {
       oid = "0";
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a delete that was successful
        // We shouldn't be here.  Forward to the ReceivablesManagementHome page.
        formMgr.setCurrPage("InvoiceManagementHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("InvoiceManagementHome", request);

%>
<jsp:forward page='<%=physicalPage%>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        // Use oid from input doc.
        oid = doc.getAttribute("/In/InvoicesSummaryData/upload_invoice_oid");
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Display error.

     getDataFromDoc = false;

     InvoicesSummaryData.setAttribute("upload_invoice_oid", oid);
     InvoicesSummaryData.getDataFromAppServer();
     
     if (InvoicesSummaryData.getAttribute("upload_invoice_oid").equals("")) {
       oid = "0";
     }
     
     
	 //jgadela  R92 IR - SQL FIX
     String invoiceSummaryGoodsSQL = "select * from invoice_summary_goods where p_invoice_summary_data_oid = ? ";
     invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryGoodsSQL, false, new Object[]{oid});
     //IR T36000015769 Start    
     InvoicesSummaryDataWebBean inv = (InvoicesSummaryDataWebBean) beanMgr.createBean(
 			  "com.ams.tradeportal.busobj.webbean.InvoicesSummaryDataWebBean",
         		  "InvoicesSummaryData");
    inv.setAttribute("upload_invoice_oid", oid);
    inv.getDataFromAppServer();
    String ruleOid = inv.getMatchingRule();
    rule = (ArMatchingRuleWebBean_Base) beanMgr.createBean(
 			  "com.ams.tradeportal.busobj.webbean.ArMatchingRuleWebBean_Base",
         		  "ArMatchingRule");
    rule.setAttribute("ar_matching_rule_oid", ruleOid);
    rule.getDataFromAppServer();
    //IR T36000015769 End
    
				//RKAZI CR709 Rel 8.2 01/25/2013 -Start   
  	if (oid != null & !"0".equals(oid)){
  		//jgadela  R92 IR - SQL FIX
  		String invPayInstSQL = "SELECT INV_PAY_INST_OID from INVOICE_PAYMENT_INSTRUCTIONS WHERE P_INVOICE_SUMMARY_DATA_OID = ? ";
  		 
  		DocumentHandler invPayInstOIDDoc = DatabaseQueryBean.getXmlResultSet(invPayInstSQL, false, new Object[]{oid});
  		if (invPayInstOIDDoc != null){
  			String invPayInstOid = invPayInstOIDDoc.getAttribute("/ResultSetRecord(0)/INV_PAY_INST_OID");
  			if (invPayInstOid != null){
  				InvoicePaymentInstructions.setAttribute("inv_pay_inst_oid",invPayInstOid);
  				InvoicePaymentInstructions.getDataFromAppServer();
  				out.println("invPayInstSQL " +InvoicePaymentInstructions.getAttribute("ben_acct_num"));

  			}
  		}
  	}
  } 
				//RKAZI CR709 Rel 8.2 01/25/2013 -End
    
   currency = InvoicesSummaryData.getAttribute("currency");
  if (!InstrumentServices.isBlank(currency))
  	useCurrency = currency;
  else useCurrency = "2";
  

  if ((!userSession.getSecurityType().equals(TradePortalConstants.ADMIN))) {
	      linkArgs[0] = TradePortalConstants.PDF_INVOICE_DETAILS;
	      linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(InvoicesSummaryData.getAttribute("upload_invoice_oid"));
	      linkArgs[2] = loginLocale;
	      linkArgs[3] = userSession.getBrandingDirectory();
	      linkString =  formMgr.getDocumentLinkAsURLInFrame(resMgr.getText("UploadInvoiceDetail.PrintInvoiceDetails",
					TradePortalConstants.TEXT_BUNDLE),
					TradePortalConstants.PDF_INVOICE_DETAILS,
					linkArgs,
					response);
   }


%>


<%-- ********************* HTML for page begins here *********************  --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <div class="secondaryNav">
      <div class="secondaryNavTitle"><%=resMgr.getText("InvoiceDetail.InvDetailDesc",
                      TradePortalConstants.TEXT_BUNDLE)%></div>

      <div id="secondaryNavHelp" class="secondaryNavHelp"><%= OnlineHelp.createContextSensitiveLink("customer/invoice_details.htm",resMgr,userSession) %>
      </div>
		<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
      <div style="clear: both;"></div>
    </div>

    <%--cquinton 10/31/2012 ir#7015 replace return with close button--%>
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem"><%=resMgr.getText("UploadInvoiceDetail.InvoiceNumber",TradePortalConstants.TEXT_BUNDLE)%></span>
      <span class="pageSubHeaderItem"><%out.print(":"); %> <%=InvoicesSummaryData.getAttribute("invoice_id")%></span>
      <span class="pageSubHeader-right">
       <%-- SHR T36000007952- Use DOJO button as below <a href="<%=linkString%>" target="<%=TradePortalConstants.PDF_INVOICE_DETAILS%>">
          <button name="print" id="print" type="button"><%=resMgr.getText("common.PrintText", TradePortalConstants.TEXT_BUNDLE)%></button>
        </a>--%>
        <button data-dojo-type="dijit.form.Button" name="print" id="print" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.PrintText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
           window.open( "<%=linkString%>");
		  </script>
        </button> 
        <%=widgetFactory.createHoverHelp("print", "PrintHoverText") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt"> 
			document.location.href = '<%=formMgr.getLinkAsUrl(closeLink, response)%>';
          </script>
        </button> 
		<%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>


<form name="UploadInvoiceDetail" method="post" id="UploadInvoiceDetail"
	data-dojo-type="dijit.form.Form"
	action="<%=formMgr.getSubmitAction(response)%>"><input
	type=hidden value="" name=buttonName> <%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());

  secureParms.put("upload_invoice_oid", oid);
%> <%= formMgr.getFormInstanceAsInputField("InvoicesForm", secureParms) %>

<div class="formMainHeader">
<%
        String link=formMgr.getLinkAsUrl("goToInvoiceManagement", response);
%>
</div>
<div class="formContentNoSidebar">
 <%         
                 // UploadInvoiceDetail.jsp page is used in both group and list invoices. if request is coming from list, then go to management tab else
                 // go to invoice group detail page.
                 StringBuffer newLink = new StringBuffer();
                 if(groupOID != null){
                      newLink.append(formMgr.getLinkAsUrl("goToInvoiceGroupDetail", response));
                      newLink.append("&invoice_group_oid=" + groupOID);
                 }
                 else{
                      newLink.append(formMgr.getLinkAsUrl("goToInvoiceManagement", response));
                 }    
%>



<table>
	<tr valign="baseline">
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.IssueDate") %>
		<br>
		<b><%=TPDateTimeUtility.formatDate(InvoicesSummaryData.getAttribute("issue_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b> 
         </p>
		</td>
<td>&nbsp;</td>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.DueDate") %>
		<br>
		<b><%=TPDateTimeUtility.formatDate(InvoicesSummaryData.getAttribute("due_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b> </p>
		</td>
<td>&nbsp;</td>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.PaymentDate") %>
		<br>
		 <% if(!InstrumentServices.isBlank(InvoicesSummaryData.getAttribute("payment_date"))) {  %>
		<b><%=TPDateTimeUtility.formatDate(InvoicesSummaryData.getAttribute("payment_date"),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b> </p>
      
		<%-- SHR T36000007952 prefix currency to amount--%>
		<% } else { %> &nbsp; <% } %> 
		
		</td>
<td>&nbsp;</td>
		<%-- SHR T36000007952 Comment <td >
		<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CurrencyCode") %>
		<br>
		<b><%=InvoicesSummaryData.getAttribute("currency")%></b>
		</p>
		</td>--%>
		
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.InvoiceAmount") %>
		 <br>
		<% if(!InstrumentServices.isBlank(InvoicesSummaryData.getAttribute("amount"))) {%>
		<%-- SHR T36000007952 prefix currency to amount--%>
		<b><%=currency%></b>&nbsp;
		<b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("amount"), useCurrency, loginLocale)%></b>
		<% } else { %> &nbsp; <% } %></p>
		</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.FinancedAmount") %>
		 <br>
		<% if(!InstrumentServices.isBlank(InvoicesSummaryData.getAttribute("invoice_finance_amount"))) { %>
		<%-- SHR T36000007952 prefix currency to amount--%>
		<b><%=currency%></b>&nbsp;
		<b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("invoice_finance_amount"), useCurrency, loginLocale)%></b>
		<% } else { %> &nbsp; <% } %> </p>
		</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.EstimatedInterest") %>
		<br>
		<% if(!InstrumentServices.isBlank(InvoicesSummaryData.getAttribute("estimated_interest_amount"))) {  %>
		<%-- SHR T36000007952 prefix currency to amount--%>
		<b><%=currency%></b>&nbsp;
		<b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("estimated_interest_amount"), useCurrency, loginLocale)%></b>
		<% } else { %> &nbsp; <% } %> </p>
		</td>
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
<td>&nbsp;</td>	
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.NetFinanceAmount") %>
		 <br>
		 <% if(!InstrumentServices.isBlank(InvoicesSummaryData.getAttribute("net_invoice_finance_amount"))) {%>
		 <%-- SHR T36000007952 prefix currency to amount--%>
		 <b><%=currency%></b>&nbsp;
		<b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("net_invoice_finance_amount"), useCurrency, loginLocale)%></b>
		<% } else { %> &nbsp; <% } %> </p>
		</td>

	</tr>
	</table>
	<table>
	<tr>	
<%-- Rel 9.0 CR 913 START --%>	
	<td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.EarlyPaymentAmount") %>
    <br>
    <% if(StringFunction.isNotBlank(InvoicesSummaryData.getAttribute("payment_amount"))) {%>
    <b><%=currency%></b>&nbsp;
        <b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("payment_amount"), useCurrency, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>
    <td>&nbsp;</td>
       
<%-- Rel 9.0 CR 913 END --%>

    <%if(TradePortalConstants.PAYABLES_MGMT.equals(InvoicesSummaryData.getAttribute("linked_to_instrument_type"))){%>
    <td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoices.CreditNoteAppliedAmt") %>
    <br>
    <% if(StringFunction.isNotBlank(InvoicesSummaryData.getAttribute("total_credit_note_amount"))) {%>
    <b><%=currency%></b>&nbsp;
        <b><%=TPCurrencyUtility.getDisplayAmount(InvoicesSummaryData.getAttribute("total_credit_note_amount"), useCurrency, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>
    <td>&nbsp;</td>
    
    <td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoices.NetInvoiceAmount") %>
    <br>
    <% BigDecimal amt = StringFunction.isNotBlank(InvoicesSummaryData.getAttribute("payment_amount"))?  new BigDecimal(InvoicesSummaryData.getAttribute("payment_amount")):
    		new BigDecimal(InvoicesSummaryData.getAttribute("amount"));
    		BigDecimal totalCRAmt = new BigDecimal("0.00");
    		if(StringFunction.isNotBlank(InvoicesSummaryData.getAttribute("total_credit_note_amount"))){
    		totalCRAmt=new BigDecimal(InvoicesSummaryData.getAttribute("total_credit_note_amount"));
    		}
    		
    amt = amt.subtract(totalCRAmt.abs());
    if(StringFunction.isNotBlank(amt.toPlainString())) {  %>
    <b><%=currency%></b>&nbsp;
        <b><%=TPCurrencyUtility.getDisplayAmount(amt.toPlainString(), useCurrency, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>
    <td>&nbsp;</td>
 <%}%>
 	
	 <td >
    <p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.SendToSuppDate") %>
    <br>
    <% if(StringFunction.isNotBlank(InvoicesSummaryData.getAttribute("send_to_supplier_date"))) {%>
    
    <b><%=TPDateTimeUtility.formatDate(InvoicesSummaryData.getAttribute("send_to_supplier_date"),
                TPDateTimeUtility.SHORT, loginLocale)%></b>
    <% } else { %> &nbsp; <% } %> </p>
    </td>
	<td>&nbsp;</td>
	<td >
        <p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.InvoiceStatus") %>
        <br>
         <%
  invoicestatus = InvoicesSummaryData.getAttribute("invoice_status");
  if (InstrumentServices.isBlank(invoicestatus)) {
	  invoicestatus="";
  } else {
    invoicestatus = invoicestatus.trim();
   invoicestatus = ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.UPLOAD_INVOICE_STATUS, 
				       invoicestatus, loginLocale);
   
  }
%> 
		<b><%=invoicestatus %></b>
</p>
		</td>
<td>&nbsp;</td>		
		<td >
		<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.LinkedToInstType") %>
		 <br>
		 <%
		 String linkedToInsttype = ReferenceDataManager.getRefDataMgr().getDescr(
				      TradePortalConstants.UPLOAD_INV_LINKED_INSTR_TY, 
				       InvoicesSummaryData.getAttribute("linked_to_instrument_type"), loginLocale);
				linkedToInsttype = (linkedToInsttype == null) ? "" : linkedToInsttype;       
		 %>
		<b><%=linkedToInsttype%></b>
		</p>
		</td>
	</tr>
</table>
<%--AAlubala Rel8.2 CR-741 02/15/2013 Credit Note entries - 
Credit Note when amount is negative
Start --%>
<%
String amount = InvoicesSummaryData.getAttribute("amount"); 
if(InstrumentServices.isNotBlank(amount) && (Double.parseDouble(amount) < 0)){
%>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoiceDetail.CreditNote") %>
<div class="columnLeftNoBorder">

<div class="formItem">
<br />

<table>
	<tr>	
		<td >
		 <%=widgetFactory.createSubLabel("UploadInvoiceDetail.DiscountCodeDescription") %>
	
		
         
		</td>
<td>
<%
String code = InvoicesSummaryData.getAttribute("cred_discount_code");
String desc = "";
String theDisplay = "";

//String discSQL = "SELECT DISCOUNT_DESCRIPTION FROM CUSTOMER_DISCOUNT_CODE WHERE DISCOUNT_CODE = '" + code+"'";
//Note - As per requirements, No duplicates expected
//jgadela  R92 IR - SQL FIX
String discSQL = "SELECT DISCOUNT_DESCRIPTION as DISCOUNT_DESCRIPTION FROM CUSTOMER_DISCOUNT_CODE WHERE DISCOUNT_CODE = ? union select descr as DISCOUNT_DESCRIPTION from bankrefdata where code = ? ";
DocumentHandler discCodeDoc = DatabaseQueryBean.getXmlResultSet(discSQL, false, code, code);
if (discCodeDoc != null){
	desc = discCodeDoc.getAttribute("/ResultSetRecord(0)/DISCOUNT_DESCRIPTION");
}

if(StringFunction.isNotBlank(desc)){
	theDisplay = code +" - "+desc;
}else{
	theDisplay = code;
}
%>
<b><%=StringFunction.xssCharsToHtml(theDisplay)%>



</b> </td>
</tr><tr>
		<td >
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.DiscountGLCodes") %>
	

		</td>
<td>		<b><%=InvoicesSummaryData.getAttribute("cred_gl_code")%></b> </td>
</tr>
<tr>
		<td >
	<%=widgetFactory.createSubLabel("UploadInvoiceDetail.DiscountComments") %>
	

		</td>
<td>		<b><%=InvoicesSummaryData.getAttribute("cred_comments")%></b> </td>		

</tr>
</table>
<div style="clear: both"></div>
</div>
</div>
<div style="clear: both"></div>
</div>
<%}%>
<%-- CR-741  End --%>


<div class="formContentNoBorder">
<div class="columnLeftNoBorder">
<%if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(InvoicesSummaryData.getAttribute("invoice_classification"))){%>
<%=widgetFactory.createSubsectionHeader("UploadInvoiceDetail.BuyerInfo")%>
<%}else { %>
<%=widgetFactory.createSubsectionHeader("InvoicesOfferedDetail.Seller")%>
<%}%>
<div class="formItem">

<% 
String buyerId= InvoicesSummaryData.getAttribute("buyer_name");
//if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(InvoicesSummaryData.getAttribute("invoice_classification"))){
	buyerId= corpOrg.getAttribute("name");
	addressLine1 = corpOrg.getAttribute("address_line_1");
    addressLine2 = corpOrg.getAttribute("address_line_2");
    addressCity = corpOrg.getAttribute("address_city"); 
    addressCtry = corpOrg.getAttribute("address_country"); 
//}else if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(InvoicesSummaryData.getAttribute("invoice_classification"))){
	//buyerId= InstrumentServices.isBlank(buyerId)?InvoicesSummaryData.getAttribute("buyer_id"):buyerId;

/*if( rule != null ) {
                              
	                             addressLine1 = rule.getAttribute("address_line_1");
	                             addressLine2 = rule.getAttribute("address_line_2");
	                             addressCity = rule.getAttribute("address_city"); 
	                             addressCtry = rule.getAttribute("address_country"); 
	                          }  }*/
                             %>
<b><%=buyerId%></b>
<br />
<b><%=addressLine1%></b> <%if(!InstrumentServices.isBlank(addressLine2)) {%>
<br>
<b><%=addressLine2%></b> <% } %> <br>
<b><%=addressCity%></b>
<div style="clear: both"></div>
</div>
</div>
<div class="columnRightNoBorder"><%=widgetFactory.createSubsectionHeader("UploadInvoiceDetail.TradingPartnerInfo")%>
<div class="formItem">

<% if( rule != null ) {
	%>
	<b><%=rule.getAttribute("buyer_name")%></b><br />  
	<%
	                             addressLine1 = rule.getAttribute("address_line_1");
	                             addressLine2 = rule.getAttribute("address_line_2");
	                             addressCity = rule.getAttribute("address_city"); 
	                             addressCtry = rule.getAttribute("address_country"); 
	                          }  
                             %> <b><%=addressLine1%></b> <%if(!InstrumentServices.isBlank(addressLine2)) {%>
<br>
<b><%=addressLine2%></b> <% } %> <br>
<b><%=addressCity%></b>
<b><%=addressCtry%></b></div>
</div>
<div style="clear: both"></div>
</div>


<div style="clear: both"></div>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoiceDetail.InvoiceSummaryDetail") %>

<% if( invoiceSummaryGoodsResult != null ) {
     Vector summaryGoodsVector = invoiceSummaryGoodsResult.getFragments("/ResultSetRecord");
     int totalSummaryGoods = summaryGoodsVector.size();
     //BSL IR NLUM040346023 04/09/2012 BEGIN
     if (totalSummaryGoods > 0) {
    	 // Only display the first Goods Summary
    	 totalSummaryGoods = 1;
     }
     //BSL IR NLUM040346023 04/09/2012 END
     for(int goods = 0; goods < totalSummaryGoods; goods++) {	      
   %>
<div class="columnLeftNoBorder">
<div class="formItem">
<table class="poContent">
<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
<% if(!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/GOODS_DESCRIPTION")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/INCOTERM")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/COUNTRY_OF_LOADING")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/COUNTRY_OF_DISCHARGE")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/VESSEL")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/CARRIER")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/ACTUAL_SHIP_DATE")) ||
		!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/PURCHASE_ORDER_ID"))){%>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.GoodsDescr")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/GOODS_DESCRIPTION"))%></b>
		
		</td>
	</tr>

	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Incoterm")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/INCOTERM"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PortofLoading")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/COUNTRY_OF_LOADING"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PortofDischarge")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/COUNTRY_OF_DISCHARGE"))%></b>
		
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Vessel")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/VESSEL"))%></b>
		
		</td>
	</tr>
	<%-- BSL IR NLUM040346023 04/11/2012 BEGIN --%>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.Carrier")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/CARRIER"))%></b>
		
		</td>
	</tr>
	<%-- BSL IR NLUM040346023 04/11/2012 END --%>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.ActualShpDate")%>
		
		</td>
		<td >
		<b><%=TPDateTimeUtility.formatDate(StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/ACTUAL_SHIP_DATE")),
                                          TPDateTimeUtility.SHORT,
                                          loginLocale)%></b>
		</td>
	</tr>
	<tr>
		<td width="2%" nowrap>&nbsp;</td>
		<td nowrap>
		<%=widgetFactory.createSubLabel("UploadInvoiceDetail.PurchaseOrderId")%>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/PURCHASE_ORDER_ID"))%></b>
		
		</td>
	</tr>
<%} %>
</table>
</div>
</div>
<div class="columnRightNoBorder">
<div class="formItem">
<table>
	<% for (int i = 1; i<= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i++) {
        if(!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/BUYER_USERS_DEF" + i + "_LABEL"))){%>
	<tr>
		<td nowrap>
		<%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/BUYER_USERS_DEF" + i + "_LABEL"))%>
		
		</td>
		<td nowrap>&nbsp;</td>
		<td>
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/BUYER_USERS_DEF" + i + "_VALUE"))%></b>
		
		</td>
	</tr>
	<%}}%>
	<% for (int i = 1; i<= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i++) {
        if(!InstrumentServices.isBlank(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/SELLER_USERS_DEF" + i + "_LABEL"))){%>
	<tr>
		<td nowrap>
		<%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/SELLER_USERS_DEF" + i + "_LABEL"))%>
		
		</td>
		<td nowrap>&nbsp;</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)summaryGoodsVector.get(goods)).getAttribute("/SELLER_USERS_DEF" + i + "_VALUE"))%></b>
		
		</td>
	</tr>
	<%}}%>
</table>

</div>
</div>
<%} %>
<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
</div>
<div style="clear: both"></div>

<%
				//RKAZI CR709 Rel 8.2 01/25/2013 -Start
	if (!InstrumentServices.isBlankOrWhiteSpace(InvoicesSummaryData.getAttribute("pay_method"))){
%>
<div style="font-style:italic;"></div>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoiceDetail.PaymentDetails") %><br>

<div class="columnLeft33NoBorder">
<div class="formItem">
<%-- Kiran  05/24/2013  Rel8.2 IR-T36000016477  Start --%>
<%-- Modified the table width for all the tables and adjusted the table data --%>
<table class="poContent" style="width:110%;" cellspacing="2">
			<tr>	
				<td >
				<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.PaymentMethod") %> :      <br/><b><%=InvoicesSummaryData.getAttribute("pay_method")%></b> 
		         </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAcctNumber") %> :      <br/><b><%=InvoicesSummaryData.getAttribute("ben_acct_num")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr1") %> :     <br/><b><%=InvoicesSummaryData.getAttribute("ben_add1")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr2") %> :      <br/><b><%=InvoicesSummaryData.getAttribute("ben_add2")%></b> </p>
				</td>
			</tr>
			<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenAddr3") %> :      <br/><b><%=InvoicesSummaryData.getAttribute("ben_add3")%></b> </p>
				</td>
			</tr>
			<tr>
				
				<td >
				<p> <%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankName") %> :       <br/><b><%=InvoicesSummaryData.getAttribute("ben_bank_name")%></b>  </p>
				</td>
			</tr>
			

</table>
</div>
</div>
 
 <div class="columnLeft33NoBorder">
<div class="formItem">
<table class="poContent" style="width:110%;" cellspacing="2">
		<tr>		
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenCountry") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_country")%></b> </p>
				</td>
			</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("InvoiceUploadRequest.ben_email_addr") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_email_addr")%></b> </p>
				</td>
		
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchCode") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_branch_code")%></b> </p>
				</td>
		
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("InvoiceUploadRequest.ben_bank_sort_code") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_bank_sort_code")%></b> </p>
				</td>
		
		</tr>		
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchAddr1") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_branch_add1")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankBranchAddr2") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_branch_add2")%></b> </p>
				</td>
		</tr>
				
		
</table>
</div>
</div>

 <div class="columnLeft33NoBorder">
<div class="formItem">
<table class="poContent" style="width:110%;" cellspacing="2">
		<tr>			
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.Charges") %> : <br/><b><%=StringFunction.isBlank(InvoicesSummaryData.getAttribute("charges")) ?"" : InvoicesSummaryData.getAttribute("charges")%></b> </p>
				</td>
			</tr>	 
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankCity") %> : <br/><b><%=InvoicesSummaryData.getAttribute("ben_bank_city")%></b> </p>
				</td>
		
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.BenBankProvinceCountry") %> : 
				<br/><b>
					<%=StringFunction.isBlank(InvoicesSummaryData.getAttribute("ben_bank_province")) ? "" :InvoicesSummaryData.getAttribute("ben_bank_province")%></b>  <b><%=InvoicesSummaryData.getAttribute("ben_branch_country")%></b></p>
				</td>
		
		</tr>
		
		<%-- DK IR T36000016477 Rel8.2 05/02/2013 Removed extra row --%>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting1") %> : <br/><b><%=InvoicesSummaryData.getAttribute("central_bank_rep1")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting2") %> : <br/><b><%=InvoicesSummaryData.getAttribute("central_bank_rep2")%></b> </p>
				</td>
		</tr>
		<tr>
				<td >
				<p><%=widgetFactory.createSubLabel("UploadInvoiceDetail.CentralReporting3") %> : <br/><b><%=InvoicesSummaryData.getAttribute("central_bank_rep3")%></b> </p>
				</td>
		</tr>

</table>
<%-- Kiran  05/24/2013  Rel8.2 IR-T36000016477  End --%>
</div>
</div>
 </div>  
  <%} 
 				//RKAZI CR709 Rel 8.2 01/25/2013 -End
 %> 
   		
<div style="clear: both"></div>
<%--DK IR T36000014551 Rel8.2 03/12/2013 --%>
<div style="font-style:italic;">
<%=widgetFactory.createWideSubsectionHeader("InvoiceDefinition.InvoiceLineItemDetail") %><br>
       <%= widgetFactory.createLabel("","UploadInvoiceDetail.ViewLineItemDetails",false,false,false,"")%>		
<%-- BSL IR NLUM040346023 04/09/2012 END --%> <%}%> <%
    	 upload_invoice_oid = InvoicesSummaryData.getAttribute("upload_invoice_oid"); 
  %>
  </div>

<%-- 914A start--%>

<div style="clear: both"></div>
<div class="formContentNoBorder">
<%=widgetFactory.createWideSubsectionHeader("UploadInvoices.CreditNoteAppliedDetails") %>
   
<div class="columnLeftNoBorder">
<div class="formItem">
<table class="poContent">
   <tr>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoices.CreditNoteId") %>
		
		</td>
		<td >
		<p><%=widgetFactory.createSubLabel("UploadInvoices.AppliedAmt") %>
		
		</td>
	</tr>


<% if(TradePortalConstants.PAYABLES_MGMT.equals(InvoicesSummaryData.getAttribute("linked_to_instrument_type"))){
     //jgadela  R92 IR - SQL FIX
     StringBuilder creditNoteSQL = new StringBuilder();
     creditNoteSQL.append("select r.credit_note_applied_amount, c.invoice_id from INVOICES_CREDIT_NOTES_RELATION r,CREDIT_NOTES c")
	.append(" where r.a_upload_credit_note_oid=c.upload_credit_note_oid and  r.a_upload_invoice_oid = ? ");
     invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(creditNoteSQL.toString(), false, new Object[]{oid});
if( invoiceSummaryGoodsResult != null && invoiceSummaryGoodsResult.getFragments("/ResultSetRecord").size()>0 ) {
     Vector creditVector = invoiceSummaryGoodsResult.getFragments("/ResultSetRecord");
        
   %>

<%for(int i = 0; i < creditVector.size(); i++) {	      
   %>
	<tr>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)creditVector.get(i)).getAttribute("/INVOICE_ID"))%></b>
		
		</td>
		<td >
		<b><%=StringFunction.xssCharsToHtml(((DocumentHandler)creditVector.get(i)).getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT"))%></b>
		
		</td>
	</tr>
	<% } %>
	
<% }} %>
</table>
</div>
</div>

</div>
<%--CR 914A end--%>

   <%

   
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  LogGridHtml = dgFactory.createDataGrid("invoiceHistoryLogGrid","InvoiceHistoryLogDataGrid",null);
  logGridLayout = dgFactory.createGridLayout("InvoiceHistoryLogDataGrid");
  %>
   
<div class="formItem">
	<div class="GridHeader" >
	<span class="invGridHeaderTitle" style="border-top:1px solid #DBDBDB;">
	<%=resMgr.getText("UploadInvoiceSummary.TransactionLog",TradePortalConstants.TEXT_BUNDLE)%>
      </span>
	</div>
	<div style="clear: both"></div>
		 <%=LogGridHtml %>
		 </div>
	</div>
</form>
</div>
<%-- IR T36000021686 Rel 8.3  --%>
<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>

<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<script type="text/javascript" src="/portal/js/datagrid.js"></script>
<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var logGridLayout = <%= logGridLayout %>;
  <%--set the initial search parms--%>
  <%-- MEer IR-36419 encrypt uploaded invoice oid --%>
  var init='upload_invoice_oid=<%=EncryptDecrypt.encryptStringUsingTripleDes(upload_invoice_oid, userSession.getSecretKey())%>';

  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoiceHistoryLogDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  var invoiceHistoryLogGrid = createDataGrid("invoiceHistoryLogGrid", viewName, logGridLayout, init);
  
//IR T36000021686 Rel 8.3 - For performance improvement
  function panelLevelAliasesFormatter(columnValues){
	  var panelLevel = columnValues[0];
	  var panelAlias="";
	  if(panelLevel != ""){
		  panelAlias = panelStore.get(panelLevel).name;
	  }
	  return panelAlias;
	  
  }
      
</script>
</div>
</body>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
 if(!"goToViewInvoices".equals(closeLink) && !"goToViewTransInvoices".equals(closeLink))
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
