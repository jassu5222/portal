//configure dojo - this is executed before dojo is loaded to get the
// modules defined correctly.

var dojoConfig = {     
  baseUrl: "/portal/js/dojo19",
  packages: [
    // register packages with identical name and location
    // More verbose declaration, for reference: { name: 'app', location: 'app', packageMap: {} }
    "dojo",
    "dijit",
    "dojox",
    "t360",
    "widget"
    //commented out additionals will want later
    //'xstyle',
    //'put-selector',
    //'dgrid',
  ],
  xdWaitSeconds: 5
};

