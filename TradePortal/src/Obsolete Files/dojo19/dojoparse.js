//load all of the common dojo modules, and specifically those that are parsed
require(["dojo/parser", "dojo/dom-class",
         "dojo/query", "dojo/NodeList-dom",
         "dojo/_base/declare",
         "dojo/_base/lang","dojo/_base/array","dojo/domReady!",
         "dojo/domReady","dijit/form/Form","dijit/form/Button",
         "dijit/form/TextBox","dijit/form/ValidationTextBox",
	 "dijit/form/CurrencyTextBox","dijit/form/NumberTextBox",
	 "dijit/form/NumberSpinner","dijit/form/DateTextBox",
	 "dijit/form/CheckBox","dijit/form/Textarea",
	 "dijit/form/FilteringSelect","t360/widget/FilteringSelect",
	 "dijit/form/RadioButton",
	 "dijit/Tooltip","dojox/layout/FloatingPane","t360/widget/Tooltip",
	 "dijit/form/DropDownButton","dijit/MenuBar","dijit/PopupMenuBarItem",
	 "dijit/MenuBarItem","dijit/MenuItem","t360/widget/MenuBar",
	 "dijit/Dialog","dojox/grid/DataGrid",
	 "dojo/data/ItemFileReadStore","dojo/data/ItemFileWriteStore",
	 "dojo/store/Memory","dojo/data/ObjectStore",
	 "dojox/grid/_CheckBoxSelector","dojox/grid/_RadioSelector",
	 "dojox/data/QueryReadStore","dijit/layout/ContentPane",
	 "dijit/TitlePane","widget/FormSidebar"
	], function(parser, domClass, query){
  parser.parse();

  //after parsing, add the 'loaded' class to the body
  //this removes the page flicker for declarative content.
  //note that in the stylesheet we make specific sections
  // visible or not, rather than the whole page.
  // this leaves header and menu on the page to
  // avoid whole page whiteflash - 
  // making page transitions cleaner
  domClass.add(document.body, 'loaded');
});

