define([
  "dojo/_base/lang", "dojo/_base/array",
  "dojo/dom", "dojo/on", "dojo/aspect",
  "dojo/dom-construct",
  "dijit/registry",
  "dojo/_base/xhr",
  "dojo/store/Memory",
  "dojo/data/ObjectStore",
  "t360/widget/DataGrid",
  "dojox/data/QueryReadStore",
  "dojox/grid/LazyTreeGrid",
  "dojox/grid/LazyTreeGridStoreModel",
  "dojo/data/ItemFileWriteStore",
  "dojox/grid/_CheckBoxSelector", "dojox/grid/_RadioSelector"
  
], function(lang, array, dom, on, aspect, domConstruct, registry, xhr, 
            Memory, ObjectStore, DataGrid, QueryReadStore,
            LazyTreeGrid,LazyTreeGridStoreModel,ItemFileWriteStore) {

  //cquinton 8/16/2012 Rel portal refresh start
  //!!!!!!NOTE!!!!!!!! the following grid formatters code has been temporarily 
  // moved to datagridformatters include until all grids use t360/datagrid
  // to create the grid.  with that this global variable cannot be found
  //!!!!!!END NOTE!!!!
  //create a global variable that holds formatters
  // this is not ideal, but removes need for grid repaints to figure out
  // the variable reference of the t360/datagrid returned object

  //todo: check to see if the variable exists, if so don't recreate

  //!!!!!!!!!ADD ALL COMMON GRID FORMATTERS HERE!!!!!!!!!!
  //t360gridFormatters = {
  //  formatGridLink: function(columnValues) {
  //    var gridLink = "";
  //    if ( columnValues.length >= 2 ) {
  //      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
  //    }
  //    else if ( columnValues.length == 1 ) {
  //      //no url parameters, just return the display
  //      gridLink = columnValues[0];
  //    }
  //    return gridLink;
  //  },
  //  
  //  addressFormatter: function(fields) {
  //    var formattedAddress = "";
  //    var formattedAddress1 = "";
  //    var formattedAddress2 = "";
  //    if ( fields.length >= 2 ) {
  //      for (i=0; i<fields.length; i++) {
  //        if(i<=1 && !formattedAddress1=="")
  //          formattedAddress1=formattedAddress1+","+fields[i];
  //        else if(i<=1 &&formattedAddress1=="")
  //          formattedAddress1=fields[i];
  //      		
  //        if(i>1 && !formattedAddress2=="")
  //          formattedAddress2=formattedAddress2+","+fields[i];
  //        else if(i>1 && formattedAddress2=="")
  //          formattedAddress2=fields[i];
  //      }
  //      formattedAddress=formattedAddress1+"\n"+formattedAddress2;
  //    }
  //    else if ( fields.length == 1 ) {
  //        
  //        formattedAddress = fields[0];
  //    }
  //    return formattedAddress;
  //  }
  //};
  //cquinton 8/16/2012 Rel portal refresh start

  var datagrid = {
    //todo: create grids using object oriented approach rather than procedural

    createDataGrid: function(dataGridId, dataViewName, gridLayout, searchParms, sortInfo, notFoundMessage) {

      var myGrid;

      //cquinton 3/8/2013 to avoid unsafe data in get url, 
      // use post instead of get. parse the parameters and create a javascript object to pass
      var queryObj = this.createQueryObj(searchParms);

      var gridDataStore = new dojox.data.QueryReadStore({
          url:"/portal/getViewData?vName=" + dataViewName,
          requestMethod: "post"
        });

      //cquinton 9/20/2012 take in noDataMessage optionally
      //01/28/2013 Prateep Gedupudi assigning global variable because we need to assign text resources property to js file variable. So nodatafoundglobal is a global variable in footer.jsp
      var myNoDataMessage = nodatafoundglobal;
      if ( typeof notFoundMessage != 'undefined' ) {
        myNoDataMessage = notFoundMessage;
      }

      myGrid=new DataGrid({
          structure:gridLayout, 
          store:gridDataStore,
          //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) Start
          //prateep 1/31/2013 commenting following as we are encoding html entities at server side at abstractdataview. By default escapeHTMLInData is true to datagrid
          //escapeHTMLInData:false,
          //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) End
          //cquinton 8/10/2012 Rel portal refresh start
          //add reordering ability
          columnReordering:true,
          //cquinton 8/10/2012 Rel portal refresh end
          //cquinton 9/6/2012 Rel portal refresh start
          //add no data message. 
          //todo: i18n. see dojo i18n capabilities
          noDataMessage: "<span class='dojoxGridNoData'>" + myNoDataMessage + "</span>",
          //cquinton 10/16/2012 ir#6448 set default to 20
          autoHeight: 20,
          //cquinton 9/6/2012 Rel portal refresh end
          rowsPerPage: 25,
          //cquinton 3/8/2013 move search parameters to query
          query: queryObj
        },
        dom.byId(dataGridId)
      );
      //cquinton 9/11/2012 Rel portal refresh ir#t36000004008 start
      if ( typeof sortInfo == 'undefined' ||
           sortInfo == null ||
           sortInfo == ''  ) {
        //when no sort info is specific, 
        // do the default, which is to 
        // sort on the first non-hidden column ascending
        var defaultSortIndex = 0; //default it
        //identify the first non-hidden column
        var gridColumns = this.getGridColumns(gridLayout);
        for (var i in gridColumns) {
          var myHidden = gridColumns[i].hidden;
          if (myHidden) {
            //do nothing
          }
          else {
            defaultSortIndex = parseInt(i); //this is necessary becuase i is a string
              //and for some reason setSortIndex really only works with numbers 
            break;
          }
        }
        myGrid.setSortIndex(defaultSortIndex);
      }
      else {
        //if sortInfo is specified attempt it
        // if sortInfo is invalid for any reason
        // we just won't sort
        if ( isNaN(sortInfo) ) {
          //assume its a column id
          //translate into numeric sortInfo
          var sortDirection = '';
          var sortColId = sortInfo;
          if ( sortInfo.indexOf('-')==0 ) {
            sortColId = sortInfo.substring(1);
            sortDirection = '-';
          }
          var sortColIdx = this.getColumnIndex( gridLayout, sortColId);
          if ( sortColIdx >= 0 ) {
            sortInfo = sortDirection + (sortColIdx+1);
            myGrid.setSortInfo(sortInfo);
          }
        }
        else {
          if (sortInfo=='0') { //a safety net
            //fudge to first column
            sortInfo='1';
          }
          myGrid.setSortInfo(sortInfo);
        }
      }
      //cquinton 9/11/2012 Rel portal refresh ir#36000004008 end
      myGrid.startup();	


      //define callback handlers

      //updateGridSelectedCount updates the specific grids selection count
      //div with the # of current selections
      //Arguments:
      //  node - this is the "_selCount" dom node
      //  this - the grid!
      function updateGridSelectedCount(node) {  
        var items = this.selection.getSelected();  
        var itemCount = 0;
        if ( items.length ) {
          itemCount = items.length;
	}
        node.innerHTML = itemCount; 
      }

      //hitch adds the "_selCount" dom node to the callback 
      // plus ensures the function parameters remain in scope
      var mySelCountNode = dom.byId(dataGridId+"_selCount");
      if ( mySelCountNode ) { //the page has a selected count dom node
        var selCountCallback = lang.hitch(
          myGrid, updateGridSelectedCount, mySelCountNode);
        //register the callback on the grid's selectionChanged event
        on(myGrid, "selectionChanged", selCountCallback);
      }

      //updateGridTotalCount updates the specific grids total count
      //div with the total # of records
      //Arguments:
      //  node - this is the _totalCount dom node
      //  this - the grid!
      function updateGridTotalCount(node) {
        //_numRows is direct insight into the store's numRows json attribute
        var mynumrows = this.store._numRows;
        node.innerHTML = mynumrows;
      }

      //hitch adds the "_totalCount" dom node to the callback
      // plus ensures the function parameters remain in scope
      var myTotalCountNode = dom.byId(dataGridId+"_totalCount");
      if ( myTotalCountNode ) { //the page has a total count dom node
        var totCountCallback = lang.hitch(
          myGrid, updateGridTotalCount, myTotalCountNode);
        aspect.after(myGrid, "_onFetchComplete", totCountCallback);
      }

      return myGrid;
    },

    searchDataGrid: function(dataGridId, dataViewName, searchParms){
      var grid = registry.byId(dataGridId);

      if (grid) {
        //cquinton 3/8/2013 reset the query object 
        var queryObj = this.createQueryObj(searchParms);

        //<<BEGIN - VSHAH>> - ADDED TO OVERCOME THE LIMITATION OF LazyTreeGrid 
        //WHERE SOMETIMES IT DISPLAYS DATA IN FORM OF THREE DOTS [LIKE (...)]        
        if (grid instanceof dojox.grid.LazyTreeGrid) {
        	grid._size=grid.rowsPerPage;
        }
        //<<END>>
        // clear any previous selections when doing new search
        grid.selection.clear();
        //cquinton 3/8/2013 set the query
        grid.setQuery(queryObj);
      }
    },

    createLazyTreeDataGrid: function(dataGridId,dataViewName,gridLayout, searchParms,sortIndex) {

      //cquinton 3/8/2013 to avoid unsafe data in get url, 
      // use post instead of get. parse the parameters and create a javascript object to pass
      var queryObj = this.createQueryObj(searchParms);

      var store = new QueryReadStore({
          url: "/portal/getViewData?vName=" + dataViewName,
          requestMethod: "post"
        });
      var model = new LazyTreeGridStoreModel({
          store: store,
          serverStore: true,
          childrenAttrs: ["CHILDREN"]
        });
  	  	
      //cquinton 9/20/2012 take in noDataMessage optionally
      var myNoDataMessage = "No data found";

      var grid = new LazyTreeGrid({
        treeModel: model,
        structure: gridLayout,
        //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) Start
        //prateep 1/31/2013 commenting following as we are encoding html entities at server side at abstractdataview. By default escapeHTMLInData is true to datagrid
        escapeHTMLInData:false,
        //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) End
        rowSelector:true,
        selectionMode:'single',
        //add no data message. 
        //todo: i18n. see dojo i18n capabilities
        noDataMessage: "<span class='dojoxGridNoData'>" + myNoDataMessage + "</span>",
        //cquinton 10/16/2012 ir#6448 set default to 20
        autoHeight: 20,
        //for lazy tree grid we set rows per page higher because
        //dynamically changing causes problems
        rowsPerPage: 35,
        //cquinton 3/8/2013 move search parameters to query
        query: queryObj
      }, dataGridId);
      grid.set("query",queryObj); //setting query sometimes does not work on create

      function updateGridSelectedCount(node) {  
        var items = this.selection.getSelected();  
        var itemCount = 0;
        if ( items.length ) {
          itemCount = items.length;
        }
        node.innerHTML = itemCount; 
      }

      //hitch adds the "_selCount" dom node to the callback 
      // plus ensures the function parameters remain in scope
      var mySelCountNode = dom.byId(dataGridId+"_selCount");
      if ( mySelCountNode ) { //the page has a selected count dom node
        var selCountCallback = lang.hitch(
          grid, updateGridSelectedCount, mySelCountNode);
        //register the callback on the grid's selectionChanged event
        on(grid, "selectionChanged", selCountCallback);
      }

      function updateGridTotalCount(node) {
        //_numRows is direct insight into the store's numRows json attribute
        var mynumrows = this.store._numRows;
        node.innerHTML = mynumrows;
      }

      var myTotalCountNode = dom.byId(dataGridId+"_totalCount");
      if ( myTotalCountNode ) { //the page has a total count dom node
        var totCountCallback = lang.hitch(
          grid, updateGridTotalCount, myTotalCountNode);
        aspect.after(grid, "_onFetchComplete", totCountCallback);
      }

      //Added for default sorting.
      if(sortIndex!=null && sortIndex!=""){
        if (!isNaN(sortIndex)) {
          // Is a number
          grid.setSortIndex(sortIndex);
        }
        //IR 16481 start
        else {
            //assume its a column id
            //translate into numeric sortInfo
            var sortDirection = '';
            var sortColId = sortIndex;
            if ( sortIndex.indexOf('-')==0 ) {
              sortColId = sortIndex.substring(1);
              sortDirection = '-';
            }
            var sortColIdx = this.getColumnIndex( gridLayout, sortColId);
            if ( sortColIdx >= 0 ) {
            	sortIndex = sortDirection + (sortColIdx+1);
              grid.setSortInfo(sortIndex);
            }
          }
        //IR 16481 end
      }

      grid.startup();

      return grid;
    },

    //cquinton 3/11/2013 make lazy tree grid post is not as simple --
    // the query object works fine on initial, but on subsequent
    // search calls, the grid will not resize correctly
    // to work around this, we have searchLazyTreeDataGrid recreate the grid
    // we retain sorting and sizing info
    searchLazyTreeDataGrid: function(dataGridId, dataViewName, gridLayout, searchParms) {

      //first destroy the existing
      var grid = registry.byId(dataGridId);
      var sortInfo = "0"; //default
      var myAutoHeight = 20; //default
      var myRowsPerPage = 35; //default
      if ( grid ) {
        sortInfo = grid.get('sortInfo');
        myAutoHeight = grid.get('autoHeight');
        myRowsPerPage = grid.get('rowsPerPage');
        var gridNode = grid.domNode;
        grid.destroyRecursive(true);
        domConstruct.empty(gridNode);
      }

      //cquinton 3/8/2013 to avoid unsafe data in get url, 
      // use post instead of get. parse the parameters and create a javascript object to pass
      var queryObj = this.createQueryObj(searchParms);

      var store = new QueryReadStore({
          url: "/portal/getViewData?vName=" + dataViewName,
          requestMethod: "post"
        });
      var model = new LazyTreeGridStoreModel({
          store: store,
          serverStore: true,
          childrenAttrs: ["CHILDREN"]
        });
  	  	
      //cquinton 9/20/2012 take in noDataMessage optionally
      var myNoDataMessage = "No data found";

      grid = new LazyTreeGrid({
        treeModel: model,
        structure: gridLayout,
        rowSelector:true,
        selectionMode:'single',
        noDataMessage: "<span class='dojoxGridNoData'>" + myNoDataMessage + "</span>",
        autoHeight: myAutoHeight,
        rowsPerPage: myRowsPerPage,
        query: queryObj
      }, dataGridId);
      grid.set("query",queryObj); //setting query sometimes does not work on create

      function updateGridSelectedCount(node) {  
        var items = this.selection.getSelected();  
        var itemCount = 0;
        if ( items.length ) {
          itemCount = items.length;
        }
        node.innerHTML = itemCount; 
      }

      //hitch adds the "_selCount" dom node to the callback 
      // plus ensures the function parameters remain in scope
      var mySelCountNode = dom.byId(dataGridId+"_selCount");
      if ( mySelCountNode ) { //the page has a selected count dom node
        var selCountCallback = lang.hitch(
          grid, updateGridSelectedCount, mySelCountNode);
        //register the callback on the grid's selectionChanged event
        on(grid, "selectionChanged", selCountCallback);
      }

      function updateGridTotalCount(node) {
        //_numRows is direct insight into the store's numRows json attribute
        var mynumrows = this.store._numRows;
        node.innerHTML = mynumrows;
      }

      var myTotalCountNode = dom.byId(dataGridId+"_totalCount");
      if ( myTotalCountNode ) { //the page has a total count dom node
        var totCountCallback = lang.hitch(
          grid, updateGridTotalCount, myTotalCountNode);
        aspect.after(grid, "_onFetchComplete", totCountCallback);
      }

      grid.setSortInfo(sortInfo);

      grid.startup();

      return grid;
    },
 
    createMemoryDataGrid: function(dataGridId, gridData, gridLayout) {
      var myStore = new Memory({data: gridData});
      //now create the object store for the datagrid
      var objStore = new ObjectStore({ objectStore: myStore });

      //cquinton 9/20/2012 take in noDataMessage optionally
      var myNoDataMessage = "No data found";
      if ( typeof notFoundMessage != 'undefined' ) {
        myNoDataMessage = notFoundMessage;
      }

      var myGrid = new DataGrid({
        store: objStore,
      //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) Start
        escapeHTMLInData:false,
      //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) End
        autoHeight: true, //todo: some limit over which should scroll!
        structure: gridLayout,
        //add no data message. 
        //todo: i18n. see dojo i18n capabilities
        noDataMessage: "<span class='dojoxGridNoData'>" + myNoDataMessage + "</span>"
      }, dataGridId);
      myGrid.startup();
      return myGrid;
    },

    startMemoryDataGrid: function(dataGridId, myStore, gridLayout) {
      //now create the object store for the datagrid
      var objStore = new ObjectStore({ objectStore: myStore });
      var myGrid = new DataGrid({
        store: objStore,
      //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) Start
        escapeHTMLInData:false,
      //prateep 9/12/2012 XSS attacks (Decoding XSS characters added in StringFunction ) End
        autoHeight: true, //todo: some limit over which should scroll!
        structure: gridLayout
      }, dataGridId);
      myGrid.startup();
    },


    getSelectedGridRowKeys: function(dataGridId){
      var rowKeys = [];
        var grid = registry.byId(dataGridId);
        if (grid) {
          var items = grid.selection.getSelected();
          if (items.length) {
            // Iterate through the list of selected items.
            // The current item is available in the variable
            // 'selectedItem' within the following function:
            var rowKeyIdx = 0;
            array.forEach(items, function(selectedItem){
              if (selectedItem !== null) {
                rowKeys[rowKeyIdx] = grid.store.getValue(selectedItem,'rowKey');
    	    rowKeyIdx++;
              }
            });
          }
        }
      return rowKeys;
    },

    getSelectedGridItems: function(dataGridId){
      var items = null;
      var grid = registry.byId(dataGridId);
      if (grid) {
        items = grid.selection.getSelected();
      }
      return items;
    },


    enableFooterItemOnSelectionEquals: function(theGrid, footerItemId, equalsCount) {
      var items = theGrid.selection.getSelected();
      var itemCount = 0;
      if ( items.length ) {
        itemCount = items.length;
      }
      var footerItem = registry.byId(footerItemId);
      if ( footerItem ) {
        if ( itemCount == equalsCount ) {
          footerItem.set('disabled',false);
        } else {
          footerItem.set('disabled',true);
        }
      }
    },

    enableFooterItemOnSelectionGreaterThan: function(theGrid, footerItemId, greaterThanCount) { 
      var items = theGrid.selection.getSelected();
      var itemCount = 0;
      if ( items.length ) {
        itemCount = items.length;
      }
      var footerItem = registry.byId(footerItemId);
      if ( footerItem ) {
        if ( itemCount > greaterThanCount ) {
          footerItem.set('disabled',false);
        } else {
          footerItem.set('disabled',true);
        }
      }
    },


    //get grid columns for the customization popup
    // this is in a specific simplified format becuase it is
    // passed to an ajax call, so we want it to be small
    //format is <columnId>,<columnId>:off, where
    // off means the columns is not visible
    getColumnsForCustomization: function(gridId) {
      var columns = "";
      var theGrid = registry.byId(gridId);
      //structure is column layout, but order could be different due to reordering
      var theStructure = theGrid.get('structure');
      var gridColumns;
      //see if have fields or more advanced structure
      if ( theStructure[0].field ) {
        gridColumns = theStructure;
      }
      else {
        //assumes first item is selection object, 2nd is columns
        gridColumns = theStructure[1];
      }
      //loop through cell names
      var columnIdx=0;
      for (var i = 0; i<gridColumns.length; i++) {
        var theCell = theGrid.getCell(i);
        var cellName = theGrid.getCellName(theCell);
        //find the column in the structure
        for (var j in gridColumns) {
          var columnId = gridColumns[j].field;
          if (columnId == cellName) {
            if ( columnIdx > 0 ) {
              columns += ",";
            }
            columns += columnId;
            //if hidden, include just set to :off
            //we want the hidden fields because they are still in the grid ordered
            // this allows user to check/uncheck fields that they want and 
            // it staying in place on the popup rather than moving around
            var myHidden = gridColumns[j].hidden;
            if (myHidden) {
              columns += ":off";
            }
            columnIdx++;
            break;
          }
        }
      }
      return columns;
    },

    //hide a grid column
    //this modifies the grid layout and
    // then resets the layout on the grid
    // so the grid repaints
    hideColumn: function(gridId, columnId) {
      var theGrid = registry.byId(gridId);
      //this is the grids internal layout structure
      // not the layout we set
      //we create a new layout becuase trying to 
      // modify the existing layout causes issues
      var theStructure = theGrid.get('structure');
      var gridColumns = theStructure;
      var newLayout = [];
      var newColumns = newLayout;
      //see if have fields or more advanced structure
      if ( !theStructure[0].field ) {
        //assumes first item is selection object, 2nd is columns
        gridColumns = theStructure[1];
        newLayout[0] = theStructure[0];
        newLayout[1] = [];
        newColumns = newLayout[1];
      }
      var newColumnsIdx = 0;
      for (var i = 0; i<gridColumns.length; i++) {
        var newLayoutCol = new Object();
        this.copyColumnLayout(gridColumns[i],newLayoutCol);
        //now hide the field!!!
        if ( columnId == gridColumns[i].field ) {
          newLayoutCol.hidden = "true";
        }
        newColumns[newColumnsIdx] = newLayoutCol;
        newColumnsIdx++;
      }

      theGrid.setStructure(newLayout);
    },

    //show a grid column
    // of course assumes the grid column is hidden
    //this modifies the grid layout and
    // then resets the layout on the grid
    // so the grid repaints
    showColumn: function(gridId, columnId) {
      var theGrid = registry.byId(gridId);
      //this is the grids internal layout structure
      // not the layout we set
      //we create a new layout becuase trying to 
      // modify the existing layout causes issues
      var theStructure = theGrid.get('structure');
      var gridColumns = theStructure;
      var newLayout = [];
      var newColumns = newLayout;
      //see if have fields or more advanced structure
      if ( !theStructure[0].field ) {
        //assumes first item is selection object, 2nd is columns
        gridColumns = theStructure[1];
        newLayout[0] = theStructure[0];
        newLayout[1] = [];
        newColumns = newLayout[1];
      }
      var newColumnsIdx = 0;
      for (var i = 0; i<gridColumns.length; i++) {
        var newLayoutCol = new Object();
        this.copyColumnLayout(gridColumns[i],newLayoutCol);
        //now unhide the field!!!
        if ( columnId == gridColumns[i].field ) {
          newLayoutCol.hidden = false;
        }
        newColumns[newColumnsIdx] = newLayoutCol;
        newColumnsIdx++;
      }

      theGrid.setStructure(newLayout);
    },

    copyColumnLayout: function(fromColumn, toColumn) {
      var layoutName = fromColumn.name;
      if ( layoutName ) {
        toColumn.name = layoutName;
      }
      var layoutField = fromColumn.field;
      if ( layoutField ) {
        toColumn.field = layoutField;
      }
      var layoutFields = fromColumn.fields;
      if ( layoutFields ) {
        toColumn.fields = layoutFields;
      }
      var layoutFormatter = fromColumn.formatter;
      if ( layoutFormatter ) {
        toColumn.formatter = layoutFormatter;
      }
      var layoutWidth = fromColumn.width;
      if ( layoutWidth ) {
        toColumn.width = layoutWidth;
      }
      var layoutCellClasses = fromColumn.cellClasses;
      if ( layoutCellClasses ) {
        toColumn.cellClasses = layoutCellClasses;
      }
      var layoutHidden = fromColumn.hidden;
      if ( layoutHidden ) {
        toColumn.hidden = true;
      }
    },

    saveGridLayout: function(gridId,gridName) {
      var urlParms = "action=save&gridId="+gridId+"&gridName="+gridName;
      //spin through the grid creating appropriate url
      // parms for each visible column
      var theGrid = registry.byId(gridId);

      // <START - rajendra - Dec 12, 2012 - FIX FOR CUSTOM VIEW COLUMN REORDERING AND SAVE>
      // The following block of code is customization of code sample given by SitePen for Ticket #21873
      // ANY MODIFICATION TO THE FOLLOWING CODE PLEASE CONTACT RAJENDRA KUMAR / CLAY QUINTON
      
  	// Filter down to just views of type _View (to avoid row selectors etc)
	  	var views = array.filter(theGrid.views.views, function(view){
	  		return view.declaredClass === "dojox.grid._View";
	  	});
	
	  	// Get the cell structure out of each one
	  	var allCells = array.map(views, function(view){
	  		// For simple views, the first field in cells is the only one we care
	  		// about. If you have more complex views, you may need to do something
	  		// more here, digging out each sub-item under view.structure.cells
	  		return view.structure.cells[0];
	  	});
	
	  	// Take a look at the cells
	  	// console.log("All Cells:", allCells);
	
	  	// Get widths and order, using array.forEach
	  	// for easier-to-read code
	  	var colIdx = 0;
	  	array.forEach(allCells, function(cells){
	  		array.forEach(cells, function(cell){
	  			var layoutHidden = cell.hidden;
//	  			console.log("Cell " + cell.field + ":", cell.unitWidth + "Layout Hidden = " + layoutHidden);
	  	        if ( !layoutHidden ) {
	  	          urlParms+="&columnId"+colIdx+"="+cell.field;
	  	          urlParms+="&width"+colIdx+"="+cell.unitWidth;
	  	          colIdx++;
	  	        }
	  		});
	  	});
	

      //</END - rajendra - Dec 12, 2012 - FIX FOR CUSTOM VIEW COLUMN REORDERING AND SAVE>
      
      //this is the grids internal layout structure
      // not the layout we set
/*  <START - rajendra - Dec 12, 2012 - FIX FOR CUSTOM VIEW COLUMN REORDERING AND SAVE>
 * 	THE FOLLOWING BLOCK OF CODE IS COMMENTED AS THE PUERPOSE OF THIS CODE IS TAKEN CARE BY THE CODE BLOCK ABOVE.
      var theStructure = theGrid.get('structure');
      var gridColumns = theStructure;
      //see if have fields or more advanced structure
      if ( !theStructure[0].field ) {
        //assumes first item is selection object, 2nd is columns
        gridColumns = theStructure[1];
      }
      var colIdx = 0;
      for (var i = 0; i<gridColumns.length; i++) {
        var layoutField = gridColumns[i].field;
        var layoutWidth = gridColumns[i].width;
        var layoutHidden = gridColumns[i].hidden;
        if ( !layoutHidden ) {
          urlParms+="&columnId"+colIdx+"="+layoutField;
          urlParms+="&width"+colIdx+"="+layoutWidth;
          colIdx++;
        }
      }
      
//</END - rajendra - Dec 12, 2012 - FIX FOR CUSTOM VIEW COLUMN REORDERING AND SAVE>
*/
      
      var remoteUrl = "/portal/customizeGrid?" + urlParms;
      //ajax call to save grid columns
      //include grid id and all columns that are checked
      xhr.get({
        url: remoteUrl,
        load: function(data) {
          if ( data.indexOf("SUCCESS")>=0 ) {
            //do nothing
          }
          else {
            var errMsg = "Unknown error saving grid layout.";
            if ( data.indexOf("<errorMessage>")>=0 ) {
              errMsg = data.substring( data.indexOf('<errorMessage>')+14 );
              errMsg = errMsg.substring( 0, errMsg.indexOf('</errorMessage>') );
            }
            alert(errMsg);
          }
        },
        error: function(err) {
          //todo: make a custom dialog for errors
          //would be nice to display this in a nice fashion, 
          // probably also allow the user to avoid the error in future
          // an alternative would be to have some kind of error display at bottom of the page???
          alert('Error retrieving remote content:'+err);
        }
      });
    },

    resetGridLayout: function(gridId,gridName) {
      //ajax call to reset grid columns
      //only grid id is needed
      var urlParms = "action=reset&gridId="+gridId+"&gridName="+gridName;
      var remoteUrl = "/portal/customizeGrid?" + urlParms;
      xhr.get({
        url: remoteUrl,
        load: function(data) {
          if ( data.indexOf("SUCCESS")>=0 ) {
            //pull gridLayout out of the response
            var gridLayoutStr = data.substring( data.indexOf('<gridLayout>')+12 );
            gridLayoutStr = gridLayoutStr.substring( 0, gridLayoutStr.indexOf('</gridLayout>') );
            //need to eval the string to make it an array
            //todo: eval is slow, can this be optimized?
            var gridLayout = eval(gridLayoutStr);
            //refresh the grid with the new details
            var theGrid = registry.byId(gridId);
            theGrid.setStructure(gridLayout);
          }
          else {
            var errMsg = "Unknown error resetting grid layout.";
            if ( data.indexOf("<errorMessage>")>=0 ) {
              errMsg = data.substring( data.indexOf('<errorMessage>')+14 );
              errMsg = errMsg.substring( 0, errMsg.indexOf('</errorMessage>') );
            }
            alert(errMsg);
          }
        },
        error: function(err) {
          //todo: make a custom dialog for errors
          //would be nice to display this in a nice fashion, 
          // probably also allow the user to avoid the error in future
          // an alternative would be to have some kind of error display at bottom of the page???
          alert('Error retrieving remote content:'+err);
        }
      });
    },

    //this helper function returns the grid columns array
    // for a given layout
    getGridColumns: function(gridLayout) {
      var gridColumns;
      //see if have fields or more advanced structure
      if ( gridLayout[0].field ) {
        gridColumns = gridLayout;
      }
      else {
        //assumes first item is selection object, 2nd is columns
        gridColumns = gridLayout[1];
      }
      return gridColumns;
    },
    
    //cquinton 9/11/2012 Rel portal refresh ir#t36000004008 start
    //return the column index for a given layout and column id
    // return -1 if not found
    getColumnIndex: function(gridLayout,columnId) {
      var colIdx = -1;
      var gridColumns = this.getGridColumns(gridLayout);
      for (var i = 0; i<gridColumns.length; i++) {
        if ( columnId == gridColumns[i].field ) {
          colIdx = parseInt(i);
          break;
        }
      }
      return colIdx;
    },
    //cquinton 9/11/2012 Rel portal refresh ir#t36000004008 end

    //cquinton 3/8/2013 add createQueryObj()
    //helper method that creates query object
    createQueryObj: function(searchParms) {
      var queryObj = {};
      var arrURLParams = searchParms.split("&");      
      var arrParamNames = new Array(arrURLParams.length);
      var arrParamValues = new Array(arrURLParams.length);     
      for (var i=0;i<arrURLParams.length;i++) {
        if (arrURLParams[i] && arrURLParams[i].length>0) {
          var opIndex =  arrURLParams[i].indexOf("=");
          var key = arrURLParams[i].substring(0,opIndex);
          var value = arrURLParams[i].substring(opIndex+1);
          if (value && value.length>0) {
            queryObj[key] = value;
          }
        }
      }
      return queryObj;
    },
    
//MEerupula Rel 8.3 IR-T36000021433/20624 utility method to get Column Index with given Column Name- starts 
    
    getColumnIndexWithName: function(dataGridId, columnName){    	
        var index = -1;  
        var grid = registry.byId(dataGridId);
          if (grid) {
        	  array.forEach(grid.layout.cells, function(cell,idx) {    	
              	if (cell.field == columnName) {
              	 	index = idx +1;  
              	 	return index;
              	}
          	});
          }
         return index;
    }
    
 //MEerupula Rel 8.3 IR-T36000021433/20624 utility method to get Column Index with given Column Name- ends
    
    

  };

  return datagrid;
});

