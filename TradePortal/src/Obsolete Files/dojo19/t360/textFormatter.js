define([
], function() {
  var textFormatter = {

    //standard address formatting - that typically used for text areas
    formatAddressLine3: function(city, stateProvince, postalcode) {
      var addrLine3 = "";
      if(city && city.length>0) {
      	addrLine3 = city;
      }
      if(stateProvince && stateProvince.length>0) {
        if ( addrLine3.length>0 ) {
          addrLine3 += " ";
        }
     	addrLine3 += stateProvince;
      }
      if(postalcode && postalcode.length>0) {
        if ( addrLine3.length>0 ) {
          addrLine3 += " ";
        }
     	addrLine3 += postalcode;
      }
      return addrLine3;
    },
   
    //use this version of format address when you have a line3 
    formatAddressLines: function( addrLine1, addrLine2, addrLine3, country ) {
      var addr = "";
      if(addrLine1 && addrLine1.length>0) {
        addr = addrLine1;
      }
      if(addrLine2 && addrLine2.length>0) {
        if ( addr.length>0 ) {
          addr += "\n";
        }
     	addr += addrLine2;
      }
      //append country to address line 3
      var addr3AndCountry = "";
      if(addrLine3 && addrLine3.length>0) {
     	addr3AndCountry += addrLine3;
      }
      if(country && country.length>0) {
        if ( addr3AndCountry.length>0 ) {
          addr3AndCountry += " ";
        }
     	addr3AndCountry += country;
      }
      if(addr3AndCountry && addr3AndCountry.length>0) {
        if ( addr.length>0 ) {
          addr += "\n";
        }
     	addr += addr3AndCountry;
      }
      return addr;
    },

    //use this version of format address when you have full address (no line3)
    formatAddress: function( addrLine1, addrLine2, city, stateProvince, postalcode, country ) {
      var addrLine3 = this.formatAddressLine3( city, stateProvince, postalcode);
      var addr = this.formatAddressLines( addrLine1, addrLine2, addrLine3, country );
      return addr;
    },
    
    //use this version when you have party name and address lines
    formatPartyAddressLines: function(partyName, addrLine1, addrLine2, addrLine3, country ) {
      var partyAddr = "";
      if(partyName && partyName.length>0) {
        partyAddr = partyName;
      }
      var addr = this.formatAddressLines( addrLine1, addrLine2, addrLine3, country );
      if(addr && addr.length>0) {
        if ( partyAddr.length>0 ) {
          partyAddr += "\n";
        }
     	partyAddr += addr;
      }
      return partyAddr;
    },

    //use this version when you have party name and full address (no line3)
    formatPartyAddress: function(partyName, addrLine1, addrLine2, city, stateProvince, postalcode, country ) {
      var partyAddr = "";
      if(partyName && partyName.length>0) {
        partyAddr = partyName;
      }
      var addr = this.formatAddress( addrLine1, addrLine2, city, stateProvince, postalcode, country );
      if(addr && addr.length>0) {
        if ( partyAddr.length>0 ) {
          partyAddr += "\n";
        }
     	partyAddr += addr;
      }
      return partyAddr;
    },

    //use this version when you have party name and full address (no line3) with a phone
    formatPartyAddressWithPhone: function(partyName, addrLine1, addrLine2, city, stateProvince, postalcode, country, phone ) {
      var partyAddr = "";
      if(partyName && partyName.length>0) {
        partyAddr = partyName;
      }
      var addr = this.formatAddress( addrLine1, addrLine2, city, stateProvince, postalcode, country );
      if(addr && addr.length>0) {
        if ( partyAddr.length>0 ) {
          partyAddr += "\n";
        }
     	partyAddr += addr;
      }
      if(phone && phone.length>0) {
        if ( partyAddr.length>0 ) {
          partyAddr += "\n";
        }
     	partyAddr += phone;
      }
      return partyAddr;
    },

    //use this version when you have bank names and address lines
    formatBankAddressLines: function(bankName, bankBranchName, addrLine1, addrLine2, addrLine3, country) {
      var bankAddr = "";
      if(bankName && bankName.length>0) {
        bankAddr = bankName;
      }
      if(bankBranchName && bankBranchName.length>0) {
        if ( bankAddr.length>0 ) {
          bankAddr += "\n";
        }
        bankAddr += bankBranchName;
      }
      var addr = this.formatAddressLines( addrLine1, addrLine2, addrLine3, country );
      if(addr && addr.length>0) {
        if ( bankAddr.length>0 ) {
          bankAddr += "\n";
        }
     	bankAddr += addr;
      }
      return bankAddr;
    },

    //use this version when you have bank names and full address (no line3)
    formatBankAddress: function(bankName, bankBranchName, addrLine1, addrLine2, city, stateProvince, postalcode, country ) {
      var bankAddr = "";
      if(bankName && bankName.length>0) {
        bankAddr = bankName;
      }
      if(bankBranchName && bankBranchName.length>0) {
        if ( bankAddr.length>0 ) {
          bankAddr += "\n";
        }
        bankAddr += bankBranchName;
      }
      var addr = this.formatAddress( addrLine1, addrLine2, city, stateProvince, postalcode, country );
      if(addr && addr.length>0) {
        if ( bankAddr.length>0 ) {
          bankAddr += "\n";
        }
     	bankAddr += addr;
      }
      return bankAddr;
    }

  };

  return textFormatter;
});
