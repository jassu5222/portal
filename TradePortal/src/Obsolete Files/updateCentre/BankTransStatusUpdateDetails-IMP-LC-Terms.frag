	<%--
	*
	*     Copyright  � 2001                         
	*     American Management Systems, Incorporated 
	*     All rights reserved
	--%>
	<%--
	*******************************************************************************
		Air Waybill Release and Shipping Guarantee Issue  Page - General section

		Description:
		Contains HTML to create the Air Waybill Release and Shipping Guarantee
		Issue General section.

		This is not a standalone JSP.  It MUST be included using the following tag:
		<%@ include file="Transaction-AIR-REL_SHP-ISS-General.jsp" %>
		*******************************************************************************
	--%>
		
		<div class="columnLeft">
	<%	
		String identifier ="FreightForwarderName,FreightForwarderAddressLine1,FreightForwarderAddressLine2,FreightForwarderCity,FreightForwarderStateProvince,FreightForwarderPostalCode,FreightForwarderCountry,FreightForwarderFaxNumber,FreightForwarderAttentionOf,FreightForwarderOTLCustomerId";
		String section = "freightSection";
		if (instrumentType.equals(TradePortalConstants.AIR_WAYBILL))
		{
	
				
		String frightFwdSearchHtml = widgetFactory.createPartySearchButton(identifier,section,false,TradePortalConstants.FREIGHT_FORWARD,false);
		out.print(widgetFactory.createSubsectionHeader( "AWB_SGTEE.FreightForwarderCarrier",false, false,false,frightFwdSearchHtml));
		}
		else
		{
			String frightFwdSearchHtml = widgetFactory.createPartySearchButton(identifier,section,false,TradePortalConstants.FREIGHT_FORWARD,false);
			out.print(widgetFactory.createSubsectionHeader( "AWB_SGTEE.FreightForwarder",false, false,false,frightFwdSearchHtml));
		}
	%>
			
		<%
		secureParms.put("FreightForwarderTermsPartyOid", termsPartyFreightForwarder.getAttribute("terms_party_oid"));
		%>
			<div style="display:none">
				<input type=hidden name="FreightForwarderTermsPartyType" value=<%=TradePortalConstants.FREIGHT_FORWARD%>>
				<input type=hidden name="FreightForwarderOTLCustomerId" value="<%=termsPartyFreightForwarder.getAttribute("OTL_customer_id")%>">
				<input type=hidden name="VendorId" value="<%=termsPartyFreightForwarder.getAttribute("vendor_id")%>">
			</div>
			<%= 
				widgetFactory.createTextField( "FreightForwarderName", "AWB_SGTEE.Name", termsPartyFreightForwarder.getAttribute("name"), 
						"35", false, !false, false, 
						"class='char35' onBlur='checkFreightForwarderName(\"" + StringFunction.escapeQuotesforJS(termsPartyFreightForwarder.getAttribute("name")) + "\")';", "", "" ) 
			%>
			<%= 
				widgetFactory.createTextField( "FreightForwarderAddressLine1", "AWB_SGTEE.AddressLine1", termsPartyFreightForwarder.getAttribute("address_line_1"), "35", false, false, false, "class='char35'", "", "")
			%>
			<%= 
				widgetFactory.createTextField( "FreightForwarderAddressLine2", "AWB_SGTEE.AddressLine2", termsPartyFreightForwarder.getAttribute("address_line_2"), "35", false, false, false, "class='char35'", "", "") 
			%>
			<%= widgetFactory.createTextField( "FreightForwarderCity", "AWB_SGTEE.City", termsPartyFreightForwarder.getAttribute("address_city"), "23", false, false, false, "class='char35'", "", "") %>
			<%= 
				widgetFactory.createTextField( "FreightForwarderStateProvince", "AWB_SGTEE.ProvinceState", termsPartyFreightForwarder.getAttribute("address_state_province"), "8", false, false, false, "", "", "inline" ) 
			%>
			<%= 
				widgetFactory.createTextField( "FreightForwarderPostalCode", "AWB_SGTEE.PostalCode", termsPartyFreightForwarder.getAttribute("address_postal_code"), "8", false, false, false, "", "", "inline" ) 
			%>
			<div style="clear: both"></div>
			<%
				options = Dropdown.createSortedRefDataOptions(
				 TradePortalConstants.COUNTRY, 
				 termsPartyFreightForwarder.getAttribute("address_country"), 
				 loginLocale);
			%> 
			<%= widgetFactory.createSelectField( "FreightForwarderCountry", "AWB_SGTEE.Country", " ", options, false, false, false, "class='char35'", "", "") %>
			<%= widgetFactory.createTextField( "FreightForwarderFaxNumber", "AWB_SGTEE.FaxNumber", terms.getAttribute("fax_number"), "15", false, false, false, "class='char25'", "", "") %>
			<%= widgetFactory.createTextField( "FreightForwarderAttentionOf", "AWB_SGTEE.AttentionOf", terms.getAttribute("attention_of"), "35", false, false, false, "class='char35'", "", "") %>
			<%
			String identifie1 ="ApplicantTermsPartyOid,ApplicantName,ApplicantAddressLine1,ApplicantAddressLine2,ApplicantCity,ApplicantStateProvince,ApplicantCountry,ApplicantPostalCode,Applicant";
			String sectio1 ="applicantoutgoing";
			String applicantSearchHtml = widgetFactory.createPartySearchButton(identifie1,sectio1,false,TradePortalConstants.APPLICANT,false);
			  /* Rel 8400 @ KMehta IR-T36000023613 Start */
			String applicantClearHtml = "";
			if (!false && false) {
				applicantClearHtml = widgetFactory.createPartyClearButton("ClearApplicantButton", "clearApplicant()", false,"");
			}
			%>
			<%= widgetFactory.createSubsectionHeader( "AWB_SGTEE.Applicant",false, !false, false, (applicantSearchHtml+applicantClearHtml))  %>
			<%--  Rel 8400 @ KMehta IR-T36000023613 End --%>
			 <%
			 	String addressSearchIndicator = null;
			 boolean isFromExpress = false;
			    addressSearchIndicator = termsPartyApplicant.getAttribute("address_search_indicator");
				if (!(false || isFromExpress) && !addressSearchIndicator.equals("N"))
			    {
			    	if(!false && corpOrgHasMultipleAddresses)
			    	{ %>
			    	<div class = "formItem">
			    	   <button data-dojo-type="dijit.form.Button"  name="AddressSearch" id="AddressSearch" type="button">
	                   <%=resMgr.getText("common.searchAddressButton",TradePortalConstants.TEXT_BUNDLE)%>
	                   <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		                   var corpOrgOid = <%=corpOrgOid%>;
							require(["t360/common", "t360/partySearch"], function(common, partySearch){
								partySearch.setPartySearchFields('<%=TradePortalConstants.APPLICANT%>');
	                  			common.openCorpCustAddressSearchDialog(corpOrgOid);
	                  		});
	                  </script>
	                   </button>

					 </div>  
			    	<% 
			    	}
			    }
				%>
					<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - start - changing the textarea to autoresize --%>	
					<%--	
					<%= widgetFactory.createTextArea( "Applicant", "",termsPartyApplicant.buildAddress(false, resMgr), true,true, false, "rows='4';", "", "" ) %>
					--%>
				<%= widgetFactory.createAutoResizeTextArea("Applicant", "", termsPartyApplicant.buildAddress(false, resMgr), true, true,false, "class='autoHeightTxtAreaQtrWidthNoSidebar' style='overflow-x: hidden;' rows='4' cols='45'", "", "")%>
					<%-- Srinivasu_D  T36000019515 Rel8.4 01/23/2014 - End --%>

					<input type=hidden name="ApplicantTermsPartyOid" value="<%=termsPartyApplicant.getAttribute("terms_party_oid")%>"> 
					<input type=hidden name="ApplicantName"	value="<%=termsPartyApplicant.getAttribute("name")%>"> 
					<input type=hidden name="ApplicantAddressLine1"	value="<%=termsPartyApplicant.getAttribute("address_line_1")%>">
					<input type=hidden name="ApplicantAddressLine2"	value="<%=termsPartyApplicant.getAttribute("address_line_2")%>">
					<input type=hidden name="ApplicantCity"	value="<%=termsPartyApplicant.getAttribute("address_city")%>">
					<input type=hidden name="ApplicantStateProvince" value="<%=termsPartyApplicant.getAttribute("address_state_province")%>">
					<input type=hidden name="ApplicantCountry" value="<%=termsPartyApplicant.getAttribute("address_country")%>">
					<input type=hidden name="ApplicantPostalCode" value="<%=termsPartyApplicant.getAttribute("address_postal_code")%>">
					<input type=hidden name="ApplicantOTLCustomerId" value="<%=termsPartyApplicant.getAttribute("OTL_customer_id")%>">
					<input type=hidden name="ApplicantAddressSeqNum" value="<%=termsPartyApplicant.getAttribute("address_seq_num")%>">
					<input type=hidden name="AppAddressSearchIndicator"	value="<%=termsPartyApplicant.getAttribute("address_search_indicator")%>">
					<input type=hidden name="ApplicantTermsPartyType"  value="<%=TradePortalConstants.APPLICANT%>">
				<%= widgetFactory.createTextField( "ApplicantReferenceNumber", "AWB_SGTEE.YourRefNumber", terms.getAttribute("reference_number"), "30", false, false, false, "class='char35'", "", "") %>
			<%
				if (InstrumentServices.isBlank(instrumentId))
				{
					instrumentId = terms.getAttribute("related_instrument_id");
				}
			%>
			<%= widgetFactory.createTextField( "ApplicantRelatedInstrumentID", "AWB_SGTEE.RelatedInstrumentID", instrumentId, "16", false, false, false, "", "", "inline" ) %>
			<br>
<%--	Naveen 11-Oct-2012 IR-T36000006190. Added two string variables which will passed as parameters to createInstrumentSearchButton method	--%>			
			<%  
				String ItemId_inst="ApplicantRelatedInstrumentID";
				String section_inst="InstrumentId";
			%>
			<%= widgetFactory.createInstrumentSearchButton(ItemId_inst,section_inst,false,TradePortalConstants.IMPORTS_ONLY,false) %>
				<input type="hidden" name="SearchInstrumentType" value=<%=TradePortalConstants.IMPORTS_ONLY%>>
	</div>
	<div class="columnRight">
			<%
			// Only display the search buttons in edit mode
			identifier ="ReleaseToPartyName,ReleaseToPartyAddressLine1,ReleaseToPartyAddressLine2,ReleaseToPartyCity,ReleaseToPartyStateProvince,ReleaseToPartyPostalCode,ReleaseToPartyCountry,ReleaseToPartyOTLCustomerId";
			section = "releasetopartySection";
			%>
		
			<%	String releaseSearchHtml = widgetFactory.createPartySearchButton(identifier,section,false,TradePortalConstants.RELEASE_TO_PARTY,false);
				out.print(widgetFactory.createSubsectionHeader( "AWB_SGTEE.ReleaseToParty",false, false,false,releaseSearchHtml));
				secureParms.put("ReleaseToPartyTermsPartyOid", termsPartyReleaseToParty.getAttribute("terms_party_oid"));
				String   amount        = terms.getAttribute("amount");
				String   currency      = terms.getAttribute("amount_currency_code");
				String displayAmount;
				if(!getDataFromDoc)
					displayAmount = TPCurrencyUtility.getDisplayAmount(amount, currency, loginLocale);          
				else
					displayAmount = amount;       

			%><div style="display:none">
				<input type=hidden name="ReleaseToPartyTermsPartyType" value="<%=TradePortalConstants.RELEASE_TO_PARTY%>">
				<input type=hidden name="ReleaseToPartyOTLCustomerId" value="<%=termsPartyReleaseToParty.getAttribute("OTL_customer_id")%>">
			</div>
				<%= widgetFactory.createTextField( "ReleaseToPartyName", "AWB_SGTEE.RTPName", termsPartyReleaseToParty.getAttribute("name"), "35", false, !false, false,"class='char35'; onBlur='checkReleaseToPartyName(\"" + 
				StringFunction.escapeQuotesforJS(termsPartyReleaseToParty.getAttribute("name")) + "\")'", "", "" ) %>
				<%= widgetFactory.createTextField( "ReleaseToPartyAddressLine1", "AWB_SGTEE.RTPAddressLine1", termsPartyReleaseToParty.getAttribute("address_line_1"), "35", false, false, false, "class='char35'", "", "") %>                              
				<%= widgetFactory.createTextField( "ReleaseToPartyAddressLine2", "AWB_SGTEE.RTPAddressLine2", termsPartyReleaseToParty.getAttribute("address_line_2"), "35", false, false, false, "class='char35'", "", "") %>
				<%= widgetFactory.createTextField( "ReleaseToPartyCity", "AWB_SGTEE.RTPCity", termsPartyReleaseToParty.getAttribute("address_city"), "23", false, false, false, "class='char35'", "", "") %>
				<%= widgetFactory.createTextField( "ReleaseToPartyStateProvince", "AWB_SGTEE.RTPProvinceState", termsPartyReleaseToParty.getAttribute("address_state_province"), "8", false, false, false, "", "", "inline" ) %>
				<%= widgetFactory.createTextField( "ReleaseToPartyPostalCode", "AWB_SGTEE.RTPPostalCode", termsPartyReleaseToParty.getAttribute("address_postal_code"), "8", false, false, false, "", "", "inline") %>
		<div style="clear: both"></div>
			<%	
				options = Dropdown.createSortedRefDataOptions(
				 TradePortalConstants.COUNTRY, 
				 termsPartyReleaseToParty.getAttribute("address_country"), 
				 loginLocale);
			%>
				<%= widgetFactory.createSelectField( "ReleaseToPartyCountry", "AWB_SGTEE.RTPCountry", " ", options, false, false, false, "class='char35'", "", "") %>
				<br>
				<%=widgetFactory.createSubsectionHeader("AWB_SGTEE.AmountDetails",false,false,false,"")%>
				<%
				options = Dropdown.createSortedCurrencyCodeOptions(currency, loginLocale);
				%>
				<%=widgetFactory.createCurrencySelectField("TransactionCurrency","AWB_SGTEE.Currency",options,
						"TransactionAmount",false,!false,false)%>
				<%		
				 String tmp_displayAmount;
				 String   tmp_amount        = terms.getAttribute("amount");
				String   tmp_currency      = terms.getAttribute("amount_currency_code");

					if(!getDataFromDoc)
						tmp_displayAmount = TPCurrencyUtility.getDisplayAmount(tmp_amount, tmp_currency, loginLocale);
					else
						tmp_displayAmount = amount;
				%>	
				
				<%= widgetFactory.createAmountField( "TransactionAmount", "AWB_SGTEE.Amount", tmp_displayAmount, tmp_currency, false, !false, false, "class='char15';", "", "inline") %> 

	</div><%--DivColRight--%>
	<div style="clear: both;"></div>
<%--JavaScript function--%>	
<script type="text/javaScript">
		function checkFreightForwarderName(originalName)
		{
			//Srinivasu_d IR#T36000039814 Rel9.3 - Clearing OTL Id - start
			/*
			if (document.forms[0].FreightForwarderName.value != originalName)
			{
				document.forms[0].FreightForwarderOTLCustomerId.value = "";
			}
			*/
			if(dijit.byId('FreightForwarderName')){
    	  		if(dijit.byId('FreightForwarderName').value != originalName){
    	  			if(null != document.getElementById('FreightForwarderOTLCustomerId'))
    	  				document.getElementById('FreightForwarderOTLCustomerId').value = "";
    	  		}
    	  	}
				//Srinivasu_d IR#T36000039814 Rel9.3 - End
		}

		function checkReleaseToPartyName(originalName)
		{
			//Srinivasu_d IR#T36000039814 Rel9.3 - Clearing OTL Id - start
			/*
			if (document.forms[0].ReleaseToPartyName.value != originalName)
			{
				document.forms[0].ReleaseToPartyOTLCustomerId.value = "";
			}
			*/
			if(dijit.byId('ReleaseToPartyName')){
    	  		if(dijit.byId('ReleaseToPartyName').value != originalName){
    	  			if(null != document.getElementById('ReleaseToPartyOTLCustomerId'))
    	  				document.getElementById('ReleaseToPartyOTLCustomerId').value = "";
    	  		}
    	  	}
			//Srinivasu_d IR#T36000039814 Rel9.3 - Clearing OTL Id - End
		} 
		
</script>
<%-- /* Rel 8400 @ KMehta IR-T36000023613 Start*/ --%>	
<script LANGUAGE="JavaScript">
	function clearApplicant() 
	  {
	    document.forms[0].Applicant.value = "";
	    document.forms[0].ApplicantName.value = "";
	    document.forms[0].ApplicantAddressLine1.value = "";
	    document.forms[0].ApplicantAddressLine2.value = "";
	    document.forms[0].ApplicantCity.value = "";
	    document.forms[0].ApplicantStateProvince.value = "";
	    document.forms[0].ApplicantCountry.value = "";
	    document.forms[0].ApplicantPostalCode.value = "";
	    document.forms[0].ApplicantOTLCustomerId.value = "";
	  }
</script>
<%-- /* Rel 8400 @ KMehta IR-T36000023613 End*/ --%>
	