/**************************************************************************/
/* This SQL Script File change Trade Portal Database for Portal 9.4.0.0   */
/* For more information on how this script should be maintained, refer to */
/* the documents titled "PPX DB Change Control Procedure"                 */
/*                                                                        */
/* QUICK NOTES                                                            */
/* 1. Wrap changes in @util/CHANGE_BEGIN and @util/CHANGE_END             */
/* 2. Use EXECUTE IMMEDIATE for DDL                                       */
/* 3. Separate DDL and DML                                                */
/* 4.                                                                     */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*    To test the script, in Command Window under the directory of the    */
/*    script, run the following on local DB                               */
/*      SQLPLUS tpuser/tpuser  @TradePortalChanges.sql                    */
/*    Then examine the output on command window.  Check db_change table   */
/*    for your change.  Make sure its status = PRC.  Check DB for your    */
/*    specific change.  Make sure your change has taken effect            */
/* 5. Use CHAR for the following cases.  Use VARCHAR2 for all other       */
/*    character datatypes.  Do not use VARCHAR.                           */
/*     - CHAR(10) for UOID                                                */
/*     - CHAR(1)                                                          */
/*     - If the value in the column is always full-length (or null)       */
/*     - To match a referenced existing column.                           */
/* 6. Specify NOT NULL on the Primary key column explicitly. Othrwise the */
/*    primary key is not guaranteed to be not null.                       */
/* 7. All the primary keys should be named.  For example:                 */
/*       CONSTRAINT PK_XXX PRIMARY KEY (UOID)                             */
/* 8. Use default value where applicable, especially for Y/N indicators.  */
/*    NULL value should not take on the meaning of a default value.       */
/* 9. Think about index for performance.  Parent pointer (p_xxx) may need */
/*    one.                                                                */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

/* Do not display the substitution variables for less cluttered results.  */
SET VERIFY OFF 
/* Do not display feedback like 'PL/SQL procedure successfully completed'.*/
/* SET FEEDBACK OFF */
/* Allow DBMS_OUTPUT.PUTLINE being display on stdout */
SET SERVEROUTPUT ON

/* Get timing info to track how long each script takes */
SET TIMING ON

/* spool output to file */
column systime new_value spooltimestamp
select to_char(sysdate,'YYYYMMDDHH24MISS') systime from dual;
spool TradePortalChanges.&spooltimestamp..lst
select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') begintime from dual;

/***********************************************************/
/* Create the DB change table if it does not already exist */
/***********************************************************/
select decode(count(*), 0, 'No                   ', 'Yes                  ') 
       as does_db_change_exists
from user_tables
where table_name = 'DB_CHANGE';

DECLARE
  db_change_exists INTEGER;
BEGIN
  SELECT count(*) 
  INTO   db_change_exists
  FROM   user_tables
  where  table_name = 'DB_CHANGE';
  
  IF db_change_exists = 0 THEN
     @util/CreateDBChange.sql
  END IF;

END;
/

/*******ADD YOUR CHANGE AFTER THIS LINE **************************************/

@util/CHANGE_BEGIN 'JMM IR-50231 08-09-2016' 'Update REFDATA table'
update refdata set locale_name = 'vi_VN' where locale_name = 'vi' and table_type = 'TRANSACTION_STATUS'; 
update refdata set locale_name = 'ja_JP' where locale_name = 'ja' and table_type = 'TRANSACTION_STATUS'; 
update refdata set locale_name = 'th_TH' where locale_name = 'th' and table_type = 'TRANSACTION_STATUS'; 
COMMIT;
@util/CHANGE_END 

@util/CHANGE_BEGIN 'SG CR48784 07-13-2016 P1' 'alter REFDATA table' 
 EXECUTE IMMEDIATE 
 'ALTER TABLE REFDATA MODIFY (CODE VARCHAR2(40))'; 
 COMMIT;
@util/CHANGE_END


@util/CHANGE_BEGIN 'SG CR-48784 07-12-2016 P2' 'Added MESSAGE_SUBJECT in refdata'

INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Approval To Pay Notice', null, 'Approval To Pay Notice', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Funding Notice', null, 'Funding Notice', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Discrepancy Notice', null, 'Discrepancy Notice', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Payables Funding Notification', null, 'Payables Funding Notification', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Settlement Instruction Request', null, 'Settlement Instruction Request', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Free Format', null, 'Free Format', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Correspondence ', null, 'Correspondence ', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_SUBJECT', 'Consolidated PCM Preadvise', null, 'Consolidated PCM Preadvise', null);

COMMIT;
@util/CHANGE_END

@util/CHANGE_BEGIN 'SG CR-48784 07-13-2016 P3' 'Added MESSAGE_PRODUCT in refdata'

INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Advance', null, 'Advance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Advance', null, 'Import Advance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Advance', null, 'Inward Advance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Advance', null, 'Export Advance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Advance', null, 'Outward Advance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Buyer Approval To Pay', null, 'Buyer Approval To Pay', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Billing', null, 'Billing', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Cargo Release', null, 'Cargo Release', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Clean BA', null, 'Clean BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Documentary BA', null, 'Import Documentary BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Documentary BA', null, 'Export Documentary BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward  Documentary BA', null, 'Inward  Documentary BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Documentary BA', null, 'Outward Documentary BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Collection', null, 'Outward Collection', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Doc Collection', null, 'Outward Doc Collection', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Doc Collection', null, 'Inward Doc Collection', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Collection', null, 'Inward Collection', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Deferred Paymnt', null, 'Deferred Paymnt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Deferred Paymnt', null, 'Import Deferred Paymnt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Deferred Paymnt', null, 'Inward Deferred Paymnt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Deferred Paymnt', null, 'Export Deferred Paymnt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Deferred Paymnt', null, 'Outward Deferred Paymnt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Direct Send', null, 'Outward Direct Send', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Documentary LC', null, 'Import Documentary LC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Documentary LC', null, 'Export Documentary LC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Finance', null, 'Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Buyer Finance', null, 'Buyer Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Finance', null, 'Export Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Seller Finance', null, 'Seller Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Payer Finance', null, 'Payer Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Finance', null, 'Import Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Finance', null, 'Outward Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Receivables Finance', null, 'Receivables Finance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Guarantee', null, 'Inward Guarantee', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Guarantee', null, 'Outward Guarantee', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Indemnity', null, 'Export Indemnity', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Payer Open Acct Pay', null, 'Payer Open Acct Pay', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Part Bought DLC', null, 'Part Bought DLC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Part Bought SLC', null, 'Part Bought SLC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Refinance BA', null, 'Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Buyer Refinance BA', null, 'Buyer Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Refinance BA', null, 'Export Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Seller Refinance BA', null, 'Seller Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Payer Refinance BA', null, 'Payer Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Refinance BA', null, 'Import Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Refinance BA', null, 'Inward Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Refinance BA', null, 'Outward Refinance BA', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Refinance', null, 'Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Buyer Refinance', null, 'Buyer Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Refinance', null, 'Export Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Seller Refinance', null, 'Seller Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Payer Refinance', null, 'Payer Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Import Refinance', null, 'Import Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Refinance', null, 'Inward Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Refinance', null, 'Outward Refinance', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Payables Rec Pay Mgmt', null, 'Payables Rec Pay Mgmt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Receivables Rec Pay Mgmt', null, 'Receivables Rec Pay Mgmt', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Synd Bought DLC', null, 'Synd Bought DLC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Synd Bought SLC', null, 'Synd Bought SLC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Standby LC', null, 'Inward Standby LC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Standby LC', null, 'Outward Standby LC', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Inward Trade Accept', null, 'Inward Trade Accept', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Outward Trade Accept', null, 'Outward Trade Accept', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE_PRODUCT', 'Export Standby LC', null, 'Export Standby LC', null);
COMMIT;
@util/CHANGE_END

@util/CHANGE_BEGIN 'SG CR-48784 07-13-2016 P4' 'Added MESSAGE in refdata'

INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE', 'MESSAGE', null, 'Please refer to the related document (s).', null);
INSERT INTO REFDATA (TABLE_TYPE, CODE, LOCALE_NAME, DESCR, ADDL_VALUE) VALUES ('MESSAGE', 'SETTLE', null, 'Please provide instructions for settlement of the related item (if this was for a Settlement Instruction Request)', null);

COMMIT;
@util/CHANGE_END


@util/CHANGE_BEGIN 'Pavani IR-50332 CR927B 08/09' 'alter NOTIFICATION_RULE_CRITERION table'
EXECUTE IMMEDIATE
 'ALTER TABLE NOTIFICATION_RULE_CRITERION ADD (NOTIFICATION_USER_IDS VARCHAR2(300), CLEAR_TRAN_IND VARCHAR2(1))'; 
COMMIT;
@util/CHANGE_END 

@util/CHANGE_BEGIN 'JM PPX CR9930 07-27-2016' 'alter TRANSACTION table'
EXECUTE IMMEDIATE
 'ALTER TABLE TRANSACTION ADD UNREAD_FLAG VARCHAR2(1)'; 
COMMIT;
@util/CHANGE_END  

/*******ADD YOUR CHANGE BEFORE THIS LINE **************************************/



/******************************************************************************/
/* Queries db_change table and list any errors (exception)                    */
/* thrown during the execution of this script file.                           */
/* Syntax errors go to stdout                                                 */
/******************************************************************************/
SET LINESIZE 300
@util/Show_DB_Change.sql

select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') endtime from dual;

quit;

