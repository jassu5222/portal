PROMPT &1 &2

DECLARE
  db_change_id db_change.id%TYPE; 
  db_change_comments db_change.comments%TYPE;
  db_change_return_value INTEGER;
BEGIN
  BEGIN 
     db_change_id := '&1' ;
     db_change_comments := '&2';
  EXCEPTION
    WHEN OTHERS THEN
      if not ChangeIsDone('&1') then
         db_change_return_value := RegisterFailedChange('&1', 'ID or comments of the change too long.  The change has not been applied.');
      end if;
      return;
  END;
  IF OKToStartChange(db_change_id, db_change_comments) > 0 then /*Mark the script as syntax error at first in case there is syntax error in the next PL/SQL blocks.*/
         db_change_return_value := RegisterFailedChange('&1', 'Syntax Error.  Please view the script execution log file for details.');
  END IF;
END;
/

DECLARE
  db_change_id db_change.id%TYPE; 
  db_change_comments db_change.comments%TYPE;
  db_change_my_sqlerrm db_change.error_message%TYPE;
  db_change_return_value INTEGER;
BEGIN
  BEGIN 
     db_change_id := '&1' ;
     db_change_comments := '&2';
  EXCEPTION
    WHEN OTHERS THEN  /*Already handled*/
      return;
  END;
  IF OKToStartChange(db_change_id, db_change_comments) > 0 then 
