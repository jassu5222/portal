DECLARE
  db_change_id db_change.id%TYPE; 
  db_change_comments db_change.comments%TYPE;
  db_change_my_sqlerrm db_change.error_message%TYPE;
  db_change_return_value INTEGER;
BEGIN
  BEGIN 
     db_change_id := '&1' ;
     db_change_comments := '&2';
  EXCEPTION
    WHEN OTHERS THEN
      db_change_return_value := RegisterFailedChange('&1', 'ID or comments of the change too long.  The change has not been applied.');
      return;
  END;

  IF TRUE then 
