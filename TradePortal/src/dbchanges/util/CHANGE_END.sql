
     IF RegisterSuccessfulChange(db_change_id) <= 0 then
        db_change_my_Sqlerrm := 'Change is processded, but cannot update the change status in db_change: ' || SQLERRM;
        db_change_return_value := RegisterFailedChange(db_change_id, db_change_my_sqlerrm);  
     END IF;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
     db_change_my_Sqlerrm := db_change_my_sqlerrm || SQLERRM;
     db_change_return_value := RegisterFailedChange(db_change_id, db_change_my_sqlerrm);  
END;
/
