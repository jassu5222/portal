
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeInput">
 *   &lt;complexContent>
 *     &lt;extension base="{http://soa.bk.mufg.jp/data/header/}ServiceInput">
 *       &lt;sequence>
 *         &lt;element name="otpVerifySignatureRequestmessage" type="{http://soa.bk.mufg.jp/ebz/otpverifysignature}otpVerifySignatureRequestmessage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeInput", propOrder = {
    "otpVerifySignatureRequestmessage"
})
public class ExecuteInput
    extends ServiceInput
{

    protected OtpVerifySignatureRequestmessage otpVerifySignatureRequestmessage;

    /**
     * Gets the value of the otpVerifySignatureRequestmessage property.
     * 
     * @return
     *     possible object is
     *     {@link OtpVerifySignatureRequestmessage }
     *     
     */
    public OtpVerifySignatureRequestmessage getOtpVerifySignatureRequestmessage() {
        return otpVerifySignatureRequestmessage;
    }

    /**
     * Sets the value of the otpVerifySignatureRequestmessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtpVerifySignatureRequestmessage }
     *     
     */
    public void setOtpVerifySignatureRequestmessage(OtpVerifySignatureRequestmessage value) {
        this.otpVerifySignatureRequestmessage = value;
    }

}
