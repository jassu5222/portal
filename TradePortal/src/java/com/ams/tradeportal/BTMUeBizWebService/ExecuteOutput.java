
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeOutput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeOutput">
 *   &lt;complexContent>
 *     &lt;extension base="{http://soa.bk.mufg.jp/data/header/}ServiceOutput">
 *       &lt;sequence>
 *         &lt;element name="otpVerifySignatureResponsemessage" type="{http://soa.bk.mufg.jp/ebz/otpverifysignature}otpVerifySignatureResponsemessage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeOutput", propOrder = {
    "otpVerifySignatureResponsemessage"
})
public class ExecuteOutput
    extends ServiceOutput
{

    protected OtpVerifySignatureResponsemessage otpVerifySignatureResponsemessage;

    /**
     * Gets the value of the otpVerifySignatureResponsemessage property.
     * 
     * @return
     *     possible object is
     *     {@link OtpVerifySignatureResponsemessage }
     *     
     */
    public OtpVerifySignatureResponsemessage getOtpVerifySignatureResponsemessage() {
        return otpVerifySignatureResponsemessage;
    }

    /**
     * Sets the value of the otpVerifySignatureResponsemessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtpVerifySignatureResponsemessage }
     *     
     */
    public void setOtpVerifySignatureResponsemessage(OtpVerifySignatureResponsemessage value) {
        this.otpVerifySignatureResponsemessage = value;
    }

}
