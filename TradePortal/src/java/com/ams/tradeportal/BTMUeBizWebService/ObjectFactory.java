
package com.ams.tradeportal.BTMUeBizWebService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ams.tradeportal.BTMUeBizWebService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OtpVerifySignatureRequestmessage_QNAME = new QName("http://soa.bk.mufg.jp/ebz/otpverifysignature", "otpVerifySignatureRequestmessage");
    private final static QName _ExecuteInput_QNAME = new QName("http://soa.bk.mufg.jp/ebz/otpverifysignature", "executeInput");
    private final static QName _OtpVerifySignatureResponsemessage_QNAME = new QName("http://soa.bk.mufg.jp/ebz/otpverifysignature", "otpVerifySignatureResponsemessage");
    private final static QName _ExecuteOutput_QNAME = new QName("http://soa.bk.mufg.jp/ebz/otpverifysignature", "executeOutput");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ams.tradeportal.BTMUeBizWebService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OtpVerifySignatureResponsemessage }
     * 
     */
    public OtpVerifySignatureResponsemessage createOtpVerifySignatureResponsemessage() {
        return new OtpVerifySignatureResponsemessage();
    }

    /**
     * Create an instance of {@link ExecuteOutput }
     * 
     */
    public ExecuteOutput createExecuteOutput() {
        return new ExecuteOutput();
    }

    /**
     * Create an instance of {@link ExecuteInput }
     * 
     */
    public ExecuteInput createExecuteInput() {
        return new ExecuteInput();
    }

    /**
     * Create an instance of {@link OtpVerifySignatureRequestmessage }
     * 
     */
    public OtpVerifySignatureRequestmessage createOtpVerifySignatureRequestmessage() {
        return new OtpVerifySignatureRequestmessage();
    }

    /**
     * Create an instance of {@link DeviceInformation }
     * 
     */
    public DeviceInformation createDeviceInformation() {
        return new DeviceInformation();
    }

    /**
     * Create an instance of {@link AuthenticationInformation }
     * 
     */
    public AuthenticationInformation createAuthenticationInformation() {
        return new AuthenticationInformation();
    }

    /**
     * Create an instance of {@link ServiceOutput }
     * 
     */
    public ServiceOutput createServiceOutput() {
        return new ServiceOutput();
    }

    /**
     * Create an instance of {@link Soakyoutsuuheader }
     * 
     */
    public Soakyoutsuuheader createSoakyoutsuuheader() {
        return new Soakyoutsuuheader();
    }

    /**
     * Create an instance of {@link ServiceInput }
     * 
     */
    public ServiceInput createServiceInput() {
        return new ServiceInput();
    }

    /**
     * Create an instance of {@link ErrorJouhou }
     * 
     */
    public ErrorJouhou createErrorJouhou() {
        return new ErrorJouhou();
    }

    /**
     * Create an instance of {@link Soakyoutsuuerrorjouhou }
     * 
     */
    public Soakyoutsuuerrorjouhou createSoakyoutsuuerrorjouhou() {
        return new Soakyoutsuuerrorjouhou();
    }

    /**
     * Create an instance of {@link KakuchouErrorJouhou }
     * 
     */
    public KakuchouErrorJouhou createKakuchouErrorJouhou() {
        return new KakuchouErrorJouhou();
    }

    /**
     * Create an instance of {@link CommonInformation }
     * 
     */
    public CommonInformation createCommonInformation() {
        return new CommonInformation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtpVerifySignatureRequestmessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.bk.mufg.jp/ebz/otpverifysignature", name = "otpVerifySignatureRequestmessage")
    public JAXBElement<OtpVerifySignatureRequestmessage> createOtpVerifySignatureRequestmessage(OtpVerifySignatureRequestmessage value) {
        return new JAXBElement<OtpVerifySignatureRequestmessage>(_OtpVerifySignatureRequestmessage_QNAME, OtpVerifySignatureRequestmessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.bk.mufg.jp/ebz/otpverifysignature", name = "executeInput")
    public JAXBElement<ExecuteInput> createExecuteInput(ExecuteInput value) {
        return new JAXBElement<ExecuteInput>(_ExecuteInput_QNAME, ExecuteInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtpVerifySignatureResponsemessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.bk.mufg.jp/ebz/otpverifysignature", name = "otpVerifySignatureResponsemessage")
    public JAXBElement<OtpVerifySignatureResponsemessage> createOtpVerifySignatureResponsemessage(OtpVerifySignatureResponsemessage value) {
        return new JAXBElement<OtpVerifySignatureResponsemessage>(_OtpVerifySignatureResponsemessage_QNAME, OtpVerifySignatureResponsemessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.bk.mufg.jp/ebz/otpverifysignature", name = "executeOutput")
    public JAXBElement<ExecuteOutput> createExecuteOutput(ExecuteOutput value) {
        return new JAXBElement<ExecuteOutput>(_ExecuteOutput_QNAME, ExecuteOutput.class, null, value);
    }

}
