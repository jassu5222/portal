
package com.ams.tradeportal.BTMUeBizWebService;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceOutput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceOutput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="soakyoutsuuheader" type="{http://soa.bk.mufg.jp/data/header/}Soakyoutsuuheader"/>
 *         &lt;element name="soakyoutsuuerrorjouhou" type="{http://soa.bk.mufg.jp/data/error/}Soakyoutsuuerrorjouhou" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceOutput", namespace = "http://soa.bk.mufg.jp/data/header/", propOrder = {
    "soakyoutsuuheader",
    "soakyoutsuuerrorjouhou"
})
@XmlSeeAlso({
    ExecuteOutput.class
})
public class ServiceOutput {

    @XmlElement(required = true, nillable = true)
    protected Soakyoutsuuheader soakyoutsuuheader;
    @XmlElement(nillable = true)
    protected List<Soakyoutsuuerrorjouhou> soakyoutsuuerrorjouhou;

    /**
     * Gets the value of the soakyoutsuuheader property.
     * 
     * @return
     *     possible object is
     *     {@link Soakyoutsuuheader }
     *     
     */
    public Soakyoutsuuheader getSoakyoutsuuheader() {
        return soakyoutsuuheader;
    }

    /**
     * Sets the value of the soakyoutsuuheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link Soakyoutsuuheader }
     *     
     */
    public void setSoakyoutsuuheader(Soakyoutsuuheader value) {
        this.soakyoutsuuheader = value;
    }

    /**
     * Gets the value of the soakyoutsuuerrorjouhou property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the soakyoutsuuerrorjouhou property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSoakyoutsuuerrorjouhou().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Soakyoutsuuerrorjouhou }
     * 
     * 
     */
    public List<Soakyoutsuuerrorjouhou> getSoakyoutsuuerrorjouhou() {
        if (soakyoutsuuerrorjouhou == null) {
            soakyoutsuuerrorjouhou = new ArrayList<Soakyoutsuuerrorjouhou>();
        }
        return this.soakyoutsuuerrorjouhou;
    }

}
