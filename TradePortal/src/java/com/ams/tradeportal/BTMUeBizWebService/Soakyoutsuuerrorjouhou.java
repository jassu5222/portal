
package com.ams.tradeportal.BTMUeBizWebService;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Soakyoutsuuerrorjouhou complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Soakyoutsuuerrorjouhou">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseSystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceMei" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="operationMei" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceshorikekka" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorJouhou" type="{http://soa.bk.mufg.jp/data/error/}ErrorJouhou" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="kakuchouErrorJouhou" type="{http://soa.bk.mufg.jp/data/error/}KakuchouErrorJouhou" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Soakyoutsuuerrorjouhou", namespace = "http://soa.bk.mufg.jp/data/error/", propOrder = {
    "responseSystemId",
    "serviceMei",
    "operationMei",
    "serviceshorikekka",
    "errorJouhou",
    "kakuchouErrorJouhou"
})
public class Soakyoutsuuerrorjouhou {

    @XmlElement(required = true, nillable = true)
    protected String responseSystemId;
    @XmlElement(required = true, nillable = true)
    protected String serviceMei;
    @XmlElement(required = true, nillable = true)
    protected String operationMei;
    @XmlElement(required = true, nillable = true)
    protected String serviceshorikekka;
    @XmlElement(nillable = true)
    protected List<ErrorJouhou> errorJouhou;
    @XmlElement(nillable = true)
    protected List<KakuchouErrorJouhou> kakuchouErrorJouhou;

    /**
     * Gets the value of the responseSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseSystemId() {
        return responseSystemId;
    }

    /**
     * Sets the value of the responseSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseSystemId(String value) {
        this.responseSystemId = value;
    }

    /**
     * Gets the value of the serviceMei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceMei() {
        return serviceMei;
    }

    /**
     * Sets the value of the serviceMei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceMei(String value) {
        this.serviceMei = value;
    }

    /**
     * Gets the value of the operationMei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationMei() {
        return operationMei;
    }

    /**
     * Sets the value of the operationMei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationMei(String value) {
        this.operationMei = value;
    }

    /**
     * Gets the value of the serviceshorikekka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceshorikekka() {
        return serviceshorikekka;
    }

    /**
     * Sets the value of the serviceshorikekka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceshorikekka(String value) {
        this.serviceshorikekka = value;
    }

    /**
     * Gets the value of the errorJouhou property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorJouhou property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorJouhou().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorJouhou }
     * 
     * 
     */
    public List<ErrorJouhou> getErrorJouhou() {
        if (errorJouhou == null) {
            errorJouhou = new ArrayList<ErrorJouhou>();
        }
        return this.errorJouhou;
    }

    /**
     * Gets the value of the kakuchouErrorJouhou property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kakuchouErrorJouhou property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKakuchouErrorJouhou().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KakuchouErrorJouhou }
     * 
     * 
     */
    public List<KakuchouErrorJouhou> getKakuchouErrorJouhou() {
        if (kakuchouErrorJouhou == null) {
            kakuchouErrorJouhou = new ArrayList<KakuchouErrorJouhou>();
        }
        return this.kakuchouErrorJouhou;
    }

}
