package com.ams.tradeportal.agents;


import java.rmi.RemoteException;
import java.util.Date;
import java.util.Map;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.webbean.AccountWebBean;
import com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean;
import com.ams.tradeportal.busobj.webbean.InstrumentWebBean;
import com.ams.tradeportal.busobj.webbean.OperationalBankOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.busobj.webbean.TransactionWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.web.BeanManager;

/**
 * This is abtract class for packaging/unpackaging FX-online query messages.
 *
 */
public abstract class AbstractFXQuery extends BankQuery{
//private static final Logger LOG = LoggerFactory.getLogger(AbstractFXQuery.class);

	public static final String FX_GETRATE_RESPONSE = "FX_RESP";
	public static final String GETRATE_REQ_ID = "/GetRequestID";
	public static final String GETRATE_QUOTE_ID = "/ForExQuoteID";


    protected BankOrganizationGroupWebBean bankOrg = null;
    protected OperationalBankOrganizationWebBean opOrg  = null;
    protected AccountWebBean debitAcct = null;
    protected AccountWebBean creditAcct = null;
    protected TransactionWebBean transaction = null;
    protected TermsWebBean terms = null;
    protected InstrumentWebBean instrument = null;


    /**
	 * Constructor
	 */
	protected AbstractFXQuery(String messageType, String agent, String requestDTD, String responseDTD) {
		super (messageType, agent,requestDTD,responseDTD);

	}

	/**
	 *
	 *
	 * @param user_oid
	 * @param transactionOID
	 * @param clientBank
	 * @param corpOrg
	 * @param object
	 * @param beanMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected Object query(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, Object object, BeanManager beanMgr) throws RemoteException, AmsException {
		init(user_oid,transactionOID,beanMgr);
		Object returnObj = super.query(user_oid, transactionOID, clientBank, corpOrg, object);
		if (returnObj == null) {
			throw new AmsException(getNoResponseError());
		}
		return returnObj;
	}


	protected IssuedError getNoResponseError() {
		return ErrorManager.findErrorCode(getNoResponseErrorCode(),transaction.getClientServerDataBridge().getLocaleName());
	}

	abstract String getNoResponseErrorCode();


	/**
	 * @param user_oid
	 * @param transactionOID
	 * @param beanMgr
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected void init(String user_oid, String transactionOID,BeanManager beanMgr) throws RemoteException, AmsException {

		transaction = beanMgr.createBean(TransactionWebBean.class, "Transaction");
		transaction.getById(transactionOID);

		terms = transaction.registerCustomerEnteredTerms();

		instrument = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
		instrument.getById(transaction.getAttribute("instrument_oid"));


		creditAcct = null;
		if (InstrumentType.XFER_BET_ACCTS.equals(transaction.getAttribute("copy_of_instr_type_code"))) {
			creditAcct = transaction.getBeanManager().createBean(AccountWebBean.class,  "Account");
			creditAcct.getById(terms.getAttribute("credit_account_oid"));
		}

		debitAcct =  beanMgr.createBean(AccountWebBean.class, "Account");
		debitAcct.getById(terms.getAttribute("debit_account_oid"));


		opOrg =  beanMgr.createBean(OperationalBankOrganizationWebBean.class, "OperationalBankOrganization");
		opOrg.getById(debitAcct.getAttribute("op_bank_org_oid"));


		bankOrg = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
		bankOrg.getById(opOrg.getAttribute("bank_org_group_oid"));

	}


	/**
	 * This method packages the header section of request xml
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected void packageHeader(Date sentDate, String messageID, ClientBank clientBank, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		super.packageHeader(sentDate, messageID, clientBank, corpOrg, opBankOrg, outputDoc, object);
		setAttribute(outputDoc,UniversalMessage.headerPath+"/OperationOrganizationID", bankOrg.getAttribute("Proponix_id"));
	}



	/**
	 * @param transaction
	 * @param debitAcct
	 * @param creditAcct
	 * @return
	 */
	protected String getContraCurCode(TransactionWebBean transaction, AccountWebBean debitAcct, AccountWebBean creditAcct) {
		String instrumentType = transaction.getAttribute("copy_of_instr_type_code");
		String paymentCur = transaction.getAttribute("copy_of_currency_code");
		String debitCur = debitAcct.getAttribute("currency");

		String val = null;

		if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType)) {
			String creditCur = creditAcct.getAttribute("currency");
			if (paymentCur.equals(debitCur)){
				val = creditCur;
			}else if (paymentCur.equals(creditCur)){
				val = debitCur;
			}
		}

		if (InstrumentType.DOM_PMT.equals(instrumentType) ||
				InstrumentType.FUNDS_XFER.equals(instrumentType)) {
			val = debitCur;
		}

		return val;

	}


	/**
	 * @param transaction
	 * @param debitAcct
	 * @param creditAcct
	 * @return
	 */
	protected String getBaseCurrencyCode(TransactionWebBean transaction, AccountWebBean debitAcct, AccountWebBean creditAcct) {
		String instrumentType = transaction.getAttribute("copy_of_instr_type_code");
		String paymentCur = transaction.getAttribute("copy_of_currency_code");
		String debitCur = debitAcct.getAttribute("currency");
		String val = null;

		if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType)) {
			String creditCur = creditAcct.getAttribute("currency");
			if (paymentCur.equals(debitCur)){
				val = debitCur;
			}else if (paymentCur.equals(creditCur)){
				val = creditCur;
			}
		}

		if (InstrumentType.DOM_PMT.equals(instrumentType) ||
				InstrumentType.FUNDS_XFER.equals(instrumentType)) {
			val = paymentCur;
		}

		return val;

	}


	/**
	 * @param transaction
	 * @param debitAcct
	 * @return
	 */
	protected String getBuySellIndicator(TransactionWebBean transaction, AccountWebBean debitAcct) {
		String instrumentType = transaction.getAttribute("copy_of_instr_type_code");
		String paymentCur = transaction.getAttribute("copy_of_currency_code");
		String debitCur = debitAcct.getAttribute("currency");
		String val = null;

		if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType)) {
			if (paymentCur.equals(debitCur)){
				val = TradePortalConstants.SELL_INDICATOR;       //NSX IR# PBUM010949226 Rel. 7.1.0. - 01/11/12 - 
			}else {
				val = TradePortalConstants.BUY_INDICATOR;        //NSX IR# PBUM010949226 Rel. 7.1.0. - 01/11/12 - 
			}
		}

		if (InstrumentType.DOM_PMT.equals(instrumentType) || 	InstrumentType.FUNDS_XFER.equals(instrumentType)) {
			val = TradePortalConstants.BUY_INDICATOR;        //NSX IR# PBUM010949226 Rel. 7.1.0. - 01/11/12 - 
		}

		return val;

	}

	/**
	 * This method packages the subheader section of request xml
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected void packageSubHeader(String user_oid, CorporateOrganization corpOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		super.packageSubHeader(user_oid, corpOrg, outputDoc, object);
		outputDoc.setAttribute(UniversalMessage.subHeaderPath+"/Instrument", instrument.getAttribute("complete_instrument_id"));
	}


	abstract protected Map<String,String> getErrorMap();



	protected IssuedError getError(String statusCode) {
		IssuedError err = ErrorManager.findErrorCode(getErrorMap().get(statusCode),transaction.getClientServerDataBridge().getLocaleName());
		return err;
	}

}
