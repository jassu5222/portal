package com.ams.tradeportal.agents;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class packages account balance query request and puts the message
 * in input queue. It also unpackages the response and updates the account
 * object with the response data.
 *
 */
public class AcctBalanceQuery extends BankQuery{
private static final Logger LOG = LoggerFactory.getLogger(AcctBalanceQuery.class);

	protected static final String AGENT_ID             = "balInq";
	protected static final String BAL_TYPE_CURRENT     = "Current";
	protected static final String BAL_TYPE_AVAIL       = "Avail";
	protected static final String ACCOUNT_LIST         = "AccountList";
	protected static final String MESSGE_TYPE          = MessageType.ACCT_BALANCE_REQUEST;
	static protected final String REQUEST_DTD          = getDTD(DTD_PATH+"acctBalanceOutbound.dtd");
    static protected final String RESPONSE_DTD         = getDTD(DTD_PATH+"acctBalanceInbound.dtd");


	/**
	 * Contructor
	 */

	public AcctBalanceQuery() {
		this(MESSGE_TYPE,AGENT_ID,REQUEST_DTD,RESPONSE_DTD);
	}

	private AcctBalanceQuery(String messageType, String agent, String requestDTD, String responseDTD) {
		super (messageType, agent,requestDTD,responseDTD);
	}
	
	/**
	 * This method querys account balance and populates the accounts with response data. It takes the
	 * user_oid, ClientBank, CorporateOrganization and list of AccountWebBeans as input.
	 * Those accounts which failed account balance query is returned in map with status.
	 * If the returned map is empty, that means all account query was successful.
	 * If the reutrned map is null, it means some error (dtd validation etc.) occurred.
	 *
	 * @param String user_oid
	 * @param ClientBank clientBank
	 * @param CorporateOrganization corpOrg
	 * @param List Accounts
	 * @return Map failed Accounts
	 * @throws AmsException 
	 * @throws RemoteException 
	 * 
	 *
	 */
	public Map queryAcctBalance(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, List accounts) throws RemoteException, AmsException {
		Map aMap = new HashMap(accounts.size());
		aMap.put(ACCOUNT_LIST, accounts);
		return (Map)query(user_oid, transactionOID, clientBank, corpOrg, aMap);
	}


	/**
	 * This method packages the body section of request xml
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
        @Override
	protected void packageBody(String messageID, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException

	{
        Map map = (Map) object;
		List accounts = (List)map.get(ACCOUNT_LIST);
		map.remove(ACCOUNT_LIST);
		Account acct = null;
		Iterator it = accounts.iterator();
		DocumentHandler doc = null;
		int i=1;
		String rqUID = null;
		String opBankOrgID= opBankOrg.getAttribute("Proponix_id");
		String deactivateInd = "";
		while (it.hasNext()) {
			acct = (Account)it.next();
			deactivateInd = acct.getAttribute("deactivate_indicator");
			if (!deactivateInd.equals("Y")){
				doc = new DocumentHandler();
				rqUID = messageID+"-"+i++;
				doc.setAttribute("/AcctInqRq/RqUID",rqUID);
				setAttribute(doc,"/AcctInqRq/MsgRqHdr/SignonRq/ClientDt",getCurrentDateTime(corpOrg));
				setAttribute(doc,"/AcctInqRq/MsgRqHdr/SignonRq/ClientApp/Org",opBankOrgID);  
				doc.setAttribute("/AcctInqRq/MsgRqHdr/SignonRq/ClientApp/Name",TradePortalConstants.PORTAL);
				doc.setAttribute("/AcctInqRq/MsgRqHdr/SignonRq/ClientApp/Version",TradePortalConstants.APP_VERSION);
	
				setAttribute(doc,"/AcctInqRq/AcctSel/AcctId",acct.getAttribute("account_number"));
				setAttribute(doc,"/AcctInqRq/AcctSel/AcctCurCode",acct.getAttribute("currency"));
				setAttribute(doc,"/AcctInqRq/AcctSel/FIIdent/BranchIdent",acct.getAttribute("source_system_branch"));
				setAttribute(doc,"/AcctInqRq/AcctSel/FIIdent/Country",acct.getAttribute("bank_country_code"));
				setAttribute(doc,"/AcctInqRq/AcctSel/AcctType",acct.getAttribute("account_type"));
				doc.setAttribute("/AcctInqRq/IncBal","1");
				outputDoc.addComponent(UniversalMessage.acctInqRqPath, doc);
				map.put(rqUID, acct);
			}
		}
	}


	/**
	 * This method unpackages the Account balance query response and updates the corresponding accountWebBean.
	 * Those accounts that failed the query is returned in map.
	 * @throws AmsException 
	 * @throws RemoteException 
	 *
	 */
        @Override
	protected Object unPackageResponse(DocumentHandler doc, Object object) throws RemoteException, AmsException {
		Map aMap = (Map) object;
        Account acct = null;
		Map failedAccts = new HashMap();

		Date gmtDate = GMTUtility.getGMTDateTime();
		String sDate=DateTimeUtility.convertDateToDateTimeString(gmtDate);
		List<DocumentHandler> acctResps = doc.getFragmentsList("/Body/AcctInqRs");
		for (DocumentHandler acctRs: acctResps) {
			String statusCode = acctRs.getAttribute("/Status/StatusCode");
			String rqUID = acctRs.getAttribute("/RqUID");
			acct = (Account)aMap.get(rqUID);
			if (acct == null) {
				LOG.info("unknown account rqUID returned, skipping...");
				continue;
			}
			if (StringFunction.isBlank(statusCode) || SUCCESS_STATUS.equals(statusCode)) {
				acct.setAttribute("last_retrieved_from_bank_date", sDate);  
				acct.setAttribute("interest_rate", acctRs.getAttribute("/AcctRec/AcctInfo/IntRateData/Rate"));
				List<DocumentHandler> acctEnvrNodeList = acctRs.getFragmentsList("/AcctRec/AcctEnvr");
                for (DocumentHandler acctEnvrNode : acctEnvrNodeList) {
					List<DocumentHandler> acctBalNodeList = acctEnvrNode.getFragmentsList("/AcctBal");
					for (DocumentHandler acctBalNode : acctBalNodeList) {
						String balType = acctBalNode.getAttribute("/BalType");
						if (BAL_TYPE_CURRENT.equals(balType)) {
							acct.setAttribute("current_balance",acctBalNode.getAttribute("/CurAmt/Amt"));
						}
						else if (BAL_TYPE_AVAIL.equals(balType)) {
							acct.setAttribute("available_funds",acctBalNode.getAttribute("/CurAmt/Amt"));
						}
					}
				}
			}
			else {
				String acctID = acct.getAttribute("account_number");
				String currency = acct.getAttribute("currency");
				LOG.warn("Account not updated, error returned for AcctId: {}  currency: {} statusCode: " + statusCode, acctID, currency );
				LOG.debug("Error message: " + acctRs.getAttribute("/Status/StatusDesc"));
				failedAccts.put(acctID+"|"+currency, acctRs.getFragment("/Status/"));
			}
			acct.setAttribute("status_code",statusCode);
		}

		return failedAccts;
	}
}
