 package com.ams.tradeportal.agents;


import com.amsinc.ecsg.frame.*;
import javax.ejb.*;
import java.rmi.*;
import java.util.*;

import com.amsinc.ecsg.util.*;
import com.ams.util.*;
import com.ams.tradeportal.busobj.*;


public class EmailSenderAgent extends TradePortalAgent
 {
//private static final Logger LOG = LoggerFactory.getLogger(EmailSenderAgent.class);

	private static final String TRY_AGAIN = "R";
	private static final String INVALID_DATA = "E";
	private static final String STATUS_STARTED = "S";
	
	  
	/**
	 * Performs the work of the agent.
	 *
	 *
	 */
	protected int lookForWorkAndDoIt() throws RemoteException, RemoveException
	{
		long rowCount = 0; //<Amit IR GNUF020156507 - 05/25/2005>

		   try
		{
		  // Orphans are rows that have already been accessed by this agent, but
		  // where processing was not successful.

		  if (isOrphan == true)
		   {
			// Look for orphans (rows that already have an AGENT_ID)
			String selectOrphanSQL = "select EMAIL_TRIGGER_OID from EMAIL_TRIGGER where agent_id='" + AGENT_ID + "'" +
						 " and status <> '" + INVALID_DATA + "'";
 
			query.setSQL(selectOrphanSQL);
			query.getRecords();

			//<Amit IR GNUF020156507 - 05/25/2005>
			rowCount = query.getRecordCount();
			if(rowCount > 0)
				logInfo( rowCount + " Orphan record(s) found!");
			//</Amit IR GNUF020156507 - 05/25/2005>

			// Process each row that was found
			for(int i=0; i < rowCount; i++)
			 {
				query.scrollToRow(i);
				boolean shouldContinue = processOutgoingEmail(query.getRecordValue("EMAIL_TRIGGER_OID"));

				// If there is no SMTP server available, don't continue through the rest of the list
				if(!shouldContinue)
					break;
			 }

			 //<Amit IR GNUF020156507 - 05/25/2005 >
			 isOrphan = false;
			 //</Amit IR GNUF020156507 - 05/25/2005 >
			}
		 }
		catch(AmsException e)
		 {
			e.printStackTrace();
			logError("Could not retrieve orphan rows from Email Trigger table", e);
			return AGENT_FAILURE;
		 }

		try
		 {
				// Access the available emails to be sent and use "select for update" to
				// mark them as being processed by this agent (setting the AGENT_ID).
				// They must be marked as being "owned by" an instance of this agent
				// so that there will not be problems if we try to run multiple agents in the future
			List<Object> sqlUpdateParams = new ArrayList<Object>();
			sqlUpdateParams.add(AGENT_ID);
			sqlUpdateParams.add(STATUS_STARTED);
			sqlUpdateParams.add(STATUS_ERROR);
			
			List<Object> whereSqlParams = new ArrayList<Object>();
			whereSqlParams.add(INVALID_DATA);
			
			Vector outgoingEmailOids = DatabaseQueryBean.selectRowsForUpdate( "EMAIL_TRIGGER",
											 "EMAIL_TRIGGER_OID",
											 "agent_id is NULL and status <> ? ",
											 "update EMAIL_TRIGGER set agent_id = ?, status = ? where agent_id is NULL and status <> ? and rownum <= 100",
											 false,whereSqlParams, sqlUpdateParams);
			Enumeration enumer = outgoingEmailOids.elements();

			//<Amit IR GNUF020156507 - 05/23/2005>
			long processedRows = 0;
			rowCount = outgoingEmailOids.size();
			if(rowCount > 0)
				logInfo(rowCount + " New record(s) found."); //
			//</Amit IR GNUF020156507 - 05/23/2005>

			// Process each row that was found
			while(enumer.hasMoreElements())
			 {
				 //<Amit IR GNUF020156507 - 05/25/2005>
				 processedRows++;
				 //</Amit IR GNUF020156507 - 05/25/2005>
			boolean shouldContinue = processOutgoingEmail((String) enumer.nextElement());

			// If there is no SMTP server available, don't continue through the rest of the list
			if(!shouldContinue)
				break;
			 }

	        // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
	        /** Since this agent does bulk processing, always return
	         * no more records to process, which ensures the thread will sleep before the next call 
	         * */   
	          return AGENT_NO_RECORDS_TO_PROCESS;
	       // CR-451 TRudden 01/28/2009 End  
		 }
		catch(AmsException e)
		 {
			e.printStackTrace();
			logError("Could not retrieve rows from Email Trigger table", e);
			return AGENT_FAILURE;
		 }

	}





  private boolean processOutgoingEmail(String outgoingEmailOid)
   {
	DocumentHandler emailXml = null;
	EmailTrigger emailTrigger = null;
	String newStatus = null;
	boolean success=true;

	// Variables to hold content of email
	String toAddress = null;
	String ccAddress = null;
	String bccAddress = null ;
	String fromAddress = null;
	String fromName = null;
	String subject = null;
	String content = null;
	byte[] attachmentBytes = null;
	String attachmentName = null; //SHILPAR CR-597

		// Create the EmailTrigger object
	try
	 {
		emailTrigger = (EmailTrigger) EJBObjectFactory.createClientEJB(ejbServerLocation, "EmailTrigger", Long.parseLong(outgoingEmailOid));
		 }
		catch(Exception e)
		 {
			// If an exception occurs at this point, the EJB server is probably down
		// log the error, but try to send it again
			logError("Error while reading from EmailTrigger object "+outgoingEmailOid+". Will try again", e);

		// We can't do anything else at this point, so just return.
		return false;
		 }

		// Extract data for the e-mail
		if(success)
		 {
		try
		 {

				// Create DocHandler from XML in EmailTrigger
			emailXml = new DocumentHandler(emailTrigger.getAttribute("email_data"), false, true);

			// Extract data from the XML to get fields for the e-mail
				// The XML will be of this format:
			//		<Email>
			//			<To>somebody@somewhere.com, somebody@aaa.com</To>
			//			<Cc>ccaddress@aaa.com</Cc>
			//			<Bcc>bccaddress@aaa.com</Bcc>
			//			<From>
			//				<SenderAddress>sender@sender.com</SenderAddress>
			//				<SenderName>Sam Sender</SenderName>
			//			</From>
			//			<Subject>Hello</Subject>
			//			<Content>This is a message to demonstrate
			//			the format of an XML
			//			message that triggers an e-mail.</Content>
			//		</Email>

			toAddress = emailXml.getAttribute("/To");
			ccAddress = emailXml.getAttribute("/Cc");
			bccAddress = emailXml.getAttribute("/Bcc");
			fromAddress = emailXml.getAttribute("/From/SenderAddress");
			fromName = emailXml.getAttribute("/From/SenderName");
			subject = emailXml.getAttribute("/Subject");
			content = emailXml.getAttribute("/Content");
			attachmentBytes = emailTrigger.getAttributeBytes("attachment_data");

			attachmentName = emailXml.getAttribute("/AttachmentName"); 
				
		 }
		catch(Exception e)
		 {
				e.printStackTrace();
					// If there's a problem at this point, mark the email trigger as having bad data
					// Meaning the system will not try to send the email again
				logError("While preparing to send e-mail from EmailTrigger object "+outgoingEmailOid+". Marked as unsendable", e);
			success=false;
			newStatus = INVALID_DATA;
		 }
		 }


	// If everything's good so far, try to send the e-mail
		if(success)
		 {
		try
		 {
			// Send the e-mail
			EmailSender sender = EmailSender.getInstance();
			sender.sendEmail(toAddress, ccAddress, bccAddress,
					fromAddress, fromName, subject, content,
					attachmentName, attachmentBytes);
			logInfo("Successfully sent e-mail for EmailTrigger "+outgoingEmailOid+". Sent to "+toAddress+" with Subject:  "+subject);
		 }
		catch(Exception e)
		 {
			// If we're here in the code, we couldn't send the e-mail
			String message = e.getMessage();
			success = false;

			// If message could not be sent because there aren't any SMTP servers available,
						// then mark the message as being able to send again.
						if((message != null) && message.equals(EmailSender.SMTP_CONNECTION_ERROR))
						 {
						   newStatus = TRY_AGAIN;
					   logError("No SMTP servers available while attempting to send e-mail from EmailTrigger object "+outgoingEmailOid+". Will try again", e);
						 }
						else
						 {
			   // Any other error will cause the message to be marked as unsendable
						   newStatus = INVALID_DATA;
					   logError("Problem with sending e-mail for EmailTrigger object "+outgoingEmailOid+". Marked as unsendable", e);
						 }
		 }
		 }

	   boolean returnValue = true;

	   try
		{
		   // Clean up the EmailTrigger object - either delete it or set the appropriate status
		   if(success)
			{
			emailTrigger.delete();
			}
		   else
			{
			emailTrigger.setAttribute("status", newStatus);

						// Get the current send attempt count (if there is one)
						String sendAttemptCountStr = emailTrigger.getAttribute("send_attempt_count");
						int sendAttemptCount = 0;
						if(!ConvenienceServices.isBlank(sendAttemptCountStr))
						   sendAttemptCount = Integer.parseInt(sendAttemptCountStr);

						// Increment the send attempt count
						emailTrigger.setAttribute("send_attempt_count", String.valueOf(++sendAttemptCount));

			emailTrigger.save();

			// If the row failed because either the EJB server or the SMTP server were down
			// don't continue through the list.
			if(newStatus.equals(TRY_AGAIN))
				returnValue = false;
			}
		}
	   catch(Exception e)
		{
		logError("Error with updating EmailTrigger "+outgoingEmailOid+". THIS MAY RESULT IN EMAILS BEING SENT TWICE!!", e);
		}

	   try
		{
	   emailTrigger.remove();
		}
	   catch(Exception e)
		{
	   // We can't remove the EJB. It's a problem, but there's nothing we can do it about here.
		}

	   return returnValue;
   }

  @Override
  protected  boolean connectToMQ() {
   return false;	
  }

}
