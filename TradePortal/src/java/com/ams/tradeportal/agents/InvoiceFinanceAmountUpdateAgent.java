package com.ams.tradeportal.agents;


import com.ams.tradeportal.busobj.InvoiceFinanceAmountQueue;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.RemoveException;

import com.ams.tradeportal.common.InvoiceFinanceAmountUpdateProcessor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;


/**
 *   Here goes detailed description of InvoiceFinanceAmountUpdateAgent functionality
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceFinanceAmountUpdateAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(InvoiceFinanceAmountUpdateAgent.class);

     private String queueStatusToProcess = TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_PENDING;   

    @Override
       public void setAgentId(String agentID)
        {
	   super.setAgentId(agentID);
            // W Zhu 5/7/2013 T36000016798 use configurable InvoiceFinAmountQueueStatusToProcess    
            try {
                queueStatusToProcess = properties.getString("InvoiceFinAmountQueueStatusToProcess").trim();
                System.out.println("InvoiceFinAmountQueueStatusToProcess="+queueStatusToProcess);
            }
            catch (java.util.MissingResourceException e) {

            }
        }

	/**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
	 *    This method is called every 10 seconds or so. Actual frequency is softcoded through
	 *    agentTimerInterval parameter in AgentConfiguration.properties file.
	 *    It returns -1 if exception occurred during processing, or MQ Series became unaccessible,
	 *    or some other problem occurred. It returns 1 if everything went successfully.
	 *    @return int
	 */
     @Override
	protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {


		
		try {
			////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
			if (isOrphan == true) {

				String selectOrphanSQL = "Select INVOICE_FIN_AMOUNT_QUEUE_OID from (Select INVOICE_FIN_AMOUNT_QUEUE_OID from INVOICE_FIN_AMOUNT_QUEUE where agent_id=? and " +
				"(status=?) order by creation_timestamp) where rownum = 1";
				query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_INPROGRESS});
				query.getRecords();

				////////////////////////////////////////////////
				// BEGIN PROCESSING OF:    ORPHAN FOUND       //
				////////////////////////////////////////////////
				if (query.getRecordCount() == 1){
					isOrphan = true;
					long oidLong = Long.parseLong(query.getRecordValue("INVOICE_FIN_AMOUNT_QUEUE_OID"));
					logInfo("Orphan found! Start processing orphan " + oidLong + "...");
					ClientServerDataBridge csdb = new ClientServerDataBridge();
					csdb.setLocaleName(LOCALE_NAME);

					InvoiceFinanceAmountUpdateProcessor processor = new InvoiceFinanceAmountUpdateProcessor(AGENT_ID,
							oidLong, ejbServerLocation, csdb);
					processor.processOrphan();
				}
           
			}
            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                return AGENT_SUCCESS;
            }


        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
	        
        try {

			int processSuccess = AGENT_NO_RECORDS_TO_PROCESS;
			List<Object> selectSQLParams = new ArrayList<Object>();
        	selectSQLParams.add(queueStatusToProcess);
        	List<Object> updateSQLParams = new ArrayList<Object>();
        	updateSQLParams.add(AGENT_ID);
			
			long oidLong = DatabaseQueryBean.selectForUpdate( "INVOICE_FIN_AMOUNT_QUEUE",
					"INVOICE_FIN_AMOUNT_QUEUE_OID",
					"INVOICE_FIN_AMOUNT_QUEUE_OID = (select INVOICE_FIN_AMOUNT_QUEUE_OID from (" +
					"select INVOICE_FIN_AMOUNT_QUEUE_OID from INVOICE_FIN_AMOUNT_QUEUE where agent_id is NULL " +
					"and status = ? order by creation_timestamp) where rownum = 1)",
					"agent_id = ? ",
					false, true, selectSQLParams, updateSQLParams); 


			////////////////////////////////////////////////
			// BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
			////////////////////////////////////////////////

			if(oidLong != 0){
				logInfo("New message found. Start processing new message..." + oidLong);

				long startMessage = System.currentTimeMillis();

				ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName(LOCALE_NAME);
                                InvoiceFinanceAmountQueue invoiceFinanceAmountQueue = (InvoiceFinanceAmountQueue) EJBObjectFactory
                                                .createClientEJB(ejbServerLocation,
                                                                "InvoiceFinanceAmountQueue",
                                                                oidLong);
                                invoiceFinanceAmountQueue.setClientServerDataBridge(csdb);
                                invoiceFinanceAmountQueue.setAttribute("status",
                                                TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_INPROGRESS);
                                invoiceFinanceAmountQueue.save();
                                
				InvoiceFinanceAmountUpdateProcessor processor = new InvoiceFinanceAmountUpdateProcessor(AGENT_ID,oidLong,ejbServerLocation,csdb);
				
				boolean return1 = processor.processInterestDiscountUpdate();
                                boolean return2 = processor.processSupplierPortalDiscountUpdate();
                                
                                
                                if (return1 & return2 ) {
                                    invoiceFinanceAmountQueue.setAttribute("status",
					TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_COMPLETED);
                                }
                                else {
                                    invoiceFinanceAmountQueue.setAttribute("status",
                                                    TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_FAILED);
                                }
                                invoiceFinanceAmountQueue.save();
                                invoiceFinanceAmountQueue.remove();
                                

				logInfo("Finished processing new message " +  oidLong );
				logInfo("Complete time = " + (System.currentTimeMillis()-startMessage)
						+"\tmilliseconds");
				return AGENT_SUCCESS;
			}
		}
		catch(AmsException e){
			if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED))
			{
				logInfo("Contention issue between agents - next agent will try and process the next message");
				return AGENT_SUCCESS;
			}

			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
		catch(NumberFormatException | IOException e){
			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
         ////////////////////////////////////////////////
         // END PROCESSING OF: NEW MESSAGE FOUND       //
         ////////////////////////////////////////////////

		////////////////////////////////////////////////
		// END PROCESSING OF: NEW MESSAGE FOUND       //
		////////////////////////////////////////////////

		///////////////////////////////////////////////////
		// NEED Archive process similar to OutboundAgent//
		//////////////////////////////////////////////////

		return AGENT_NO_RECORDS_TO_PROCESS;

	}

	
     @Override
     protected  boolean connectToMQ() {
      return false;	
     }

}