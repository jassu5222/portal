package com.ams.tradeportal.agents;


import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.RemoveException;

import com.ams.tradeportal.common.InvoiceUploadFileProcessor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;


/**
 *   Here goes detailed description of InvoiceUploadAgent functionality
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceUploadAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(InvoiceUploadAgent.class);
	 
	/**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
	 *    This method is called every 10 seconds or so. Actual frequency is softcoded through
	 *    agentTimerInterval parameter in AgentConfiguration.properties file.
	 *    It returns -1 if exception occured during processing, or MQ Series became unaccessible,
	 *    or some other problem occured. It returns 1 if everything went successfully.
	 *    @return int
	 */
    @Override
	protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {
		
		
		try {
			////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
			if (isOrphan == true) {

				String selectOrphanSQL = "Select INVOICE_FILE_UPLOAD_OID from (Select INVOICE_FILE_UPLOAD_OID from INVOICE_FILE_UPLOADS where agent_id=? and " +
				"(validation_status=?) order by creation_timestamp) where rownum = 1";
				query.setSQL(selectOrphanSQL, AGENT_ID, TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS);
				query.getRecords();

				////////////////////////////////////////////////
				// BEGIN PROCESSING OF:    ORPHAN FOUND       //
				////////////////////////////////////////////////
				if (query.getRecordCount() == 1){
					isOrphan = true;
					String s_oid = query.getRecordValue("INVOICE_FILE_UPLOAD_OID");
					logInfo("Orphan found! Start processing orphan " + s_oid + "...");
					ClientServerDataBridge csdb = new ClientServerDataBridge();
					csdb.setLocaleName(LOCALE_NAME);
					InvoiceUploadFileProcessor processor = new InvoiceUploadFileProcessor(AGENT_ID,Long.parseLong(s_oid),ejbServerLocation,csdb);
					processor.processOrphan();
				}
           
			}
            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                return AGENT_SUCCESS;
            }


        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
	        
        try {

			int processSuccess = AGENT_NO_RECORDS_TO_PROCESS;
			List<Object> selectSQLParams = new ArrayList<Object>();
        	selectSQLParams.add(TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
        	selectSQLParams.add(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS);
        	List<Object> updateSQLParams = new ArrayList<Object>();
        	updateSQLParams.add(AGENT_ID);
			
			long oidLong = DatabaseQueryBean.selectForUpdate( "INVOICE_FILE_UPLOADS",
					"INVOICE_FILE_UPLOAD_OID",
					"INVOICE_FILE_UPLOAD_OID = (select INVOICE_FILE_UPLOAD_OID from (select INVOICE_FILE_UPLOAD_OID from INVOICE_FILE_UPLOADS " +
					"where agent_id is NULL and validation_status=? " +
					" and a_owner_org_oid not in (select distinct a_owner_org_oid from INVOICE_FILE_UPLOADS where validation_status=?)" +
					" order by creation_timestamp) where rownum = 1)",
					"agent_id = ?",
					 false, true, selectSQLParams, updateSQLParams); 

			////////////////////////////////////////////////
			// BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
			////////////////////////////////////////////////

			if(oidLong != 0){
				logInfo("New message found. Start processing new message..." + oidLong);

				long startMessage = System.currentTimeMillis();

				ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName(LOCALE_NAME);
				InvoiceUploadFileProcessor processor = new InvoiceUploadFileProcessor(AGENT_ID,oidLong,ejbServerLocation,csdb);
				
				processor.processInvoiceFile();

				logInfo("Finished processing new message " +  oidLong );
				logInfo("Complete time = " + (System.currentTimeMillis()-startMessage)
						+"\tmilliseconds");
				return AGENT_SUCCESS;
			}
		}
		catch(AmsException e){
			if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED))
			{
				logInfo("Contention issue between agents - next agent will try and process the next message");
				return AGENT_SUCCESS;
			}
			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
		catch(NumberFormatException | IOException e){
			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
        ////////////////////////////////////////////////
        // END PROCESSING OF: NEW MESSAGE FOUND       //
        ////////////////////////////////////////////////
        
		////////////////////////////////////////////////
		// END PROCESSING OF: NEW MESSAGE FOUND       //
		////////////////////////////////////////////////

		///////////////////////////////////////////////////
		// NEED Archive process similar to OutboundAgent//
		//////////////////////////////////////////////////

		return AGENT_NO_RECORDS_TO_PROCESS;

	}

	
    @Override
    protected  boolean connectToMQ() {
     return false;	
    }

}