package com.ams.tradeportal.agents;


import com.ibm.mq.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import com.ams.tradeportal.busobj.IncomingInterfaceQueue;

/*
 *     Copyright  ?2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MQAgent extends TradePortalAgent{
//private static final Logger LOG = LoggerFactory.getLogger(MQAgent.class);
    
    private String msg_text;
    
    @Override
    public int lookForWorkAndDoIt(){

        /*  Read with a syncpoint off the MQ Input Queue. */
        msg_text = mqWrapper.attemptToGetMessageFromQueue();
            
        if (msg_text != null && !msg_text.equals("")){
        	// W Zhu 12/20/07 WAUH121261057 #22 BEGIN 
        	// Log the header and subheader or at most 500 characters.
        	String displayMsgText;
        	int displayMsgTextLen = msg_text.indexOf("</SubHeader>");
        	if (displayMsgTextLen < 0) {
        		displayMsgTextLen = msg_text.length();
        		if (displayMsgTextLen > 500) displayMsgTextLen = 500;
        	}
        	else {
        		displayMsgTextLen += 12; // Include </SubHeader> tag
        	}
        	if (displayMsgTextLen < msg_text.length()) {
        		displayMsgText = msg_text.substring(0, displayMsgTextLen) + " ..."; 
        	}
        	else {
        		displayMsgText = msg_text;
        	}
            logInfo("New message found on MQ Input Queue ("+ msg_text.length() + " Bytes): " + displayMsgText);
        	// W Zhu 12/20/07 WAUH121261057 #22 END            
            boolean saveSuccess = saveMessageToDB(msg_text);
            if (saveSuccess == true){
                try{mqWrapper.commit();}  catch(MQException mqe){logError("MQ Manager failed to commit",  mqe);}
            }
            else{
                try{mqWrapper.backout();} catch(MQException mqe){logError("MQ Manager failed to backout", mqe);}
            }
            logInfo("Finished processing new message found on MQ Input Queue");
        }
        else{

        	 // CR-451 TRudden 01/28/2009 Begin -- Changes to skip sleep period when more work exists
            return AGENT_NO_RECORDS_TO_PROCESS;
            // CR-451 TRudden 01/28/2009 End
        }
        return AGENT_SUCCESS;
    }
    
    
    
    protected boolean saveMessageToDB(String msg_text){
        
        /* Set date_received to current GMT time */
        String date_received;
        try{
            date_received = DateTimeUtility.getGMTDateTime(false);            
        }
        catch (AmsException e){
            logError("While obtaining GMT date and time using JPylon DateTimeUtility", e);
            return false;
        }   
        
        int saveSuccess;
        
        // W Zhu 2/20/08 WEUH121436595 BEGIN
        // Set message type so that we could use different agents to process different msg types.
        int messageTypeBeginIndex; 
        int messageTypeEndIndex;
        String messageType = null;
        messageTypeBeginIndex = msg_text.indexOf("<MessageType>");
        messageTypeEndIndex = msg_text.indexOf("</MessageType>");
        if (messageTypeBeginIndex > -1 && messageTypeEndIndex > -1 && messageTypeEndIndex > messageTypeBeginIndex) {
        	messageType = msg_text.substring(messageTypeBeginIndex + "<MessageType>".length(), messageTypeEndIndex);
        }
        // W Zhu 2/20/08 WEUH121436595 END
        try{
            incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue");
            incoming_queue.newObject();
            incoming_queue.setAttribute("date_received",   date_received);
            incoming_queue.setAttribute("msg_text",        msg_text);
            incoming_queue.setAttribute("status",          STATUS_RECEIVED);
            incoming_queue.setAttribute("msg_type",        messageType);// W Zhu 2/20/08 WEUH121436595 BEGIN
            
            saveSuccess = incoming_queue.save();
            logInfo("Saved new message queue_oid="+incoming_queue.getAttribute("queue_oid")+", msg_type="+messageType);

            if (saveSuccess < 0) {
                logError("Error is occured While saving date_received, msg_text, status and msg_type to incoming_queue table");
            }            
			//Srinivasu_D IR#T36000040732 Rel9.3 07/07/2015 - causing ejbDeleteException due to removal of private instance of the same obejct
            //incoming_queue.remove();
        }
        catch(Exception e){
            logError("While saving date_received, msg_text, status and msg_type to incoming_queue table", e);
            e.printStackTrace();
            return false;
        }
        finally{
            //Should I check error section or what? Ask Ken.
        }
        return true;    
    } 
    
    @Override
    protected  boolean connectToMQ() {
     return true;	
    }
    
}// end of MQAgent class