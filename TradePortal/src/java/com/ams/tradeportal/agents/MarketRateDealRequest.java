package com.ams.tradeportal.agents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

/**
 * This class handles Market Rate Deal query request/response. It puts request message
 * in output queue and waits for response. The response received is then unpackaged
 * and an object is returned.
 *
 */
public class MarketRateDealRequest extends AbstractFXQuery{
private static final Logger LOG = LoggerFactory.getLogger(MarketRateDealRequest.class);

	protected static final String AGENT_ID             = "fxDealInq";
	protected static final String MESSGE_TYPE          = MessageType.FX_ONLINE_DEAL_REQUEST;
	protected static final String REQUEST_DTD          = getDTD(DTD_PATH+"FXDEALOT.dtd");
	protected static final String RESPONSE_DTD         = getDTD(DTD_PATH+"FXDEALIN.dtd");
	protected static final String INDIRECT_CONVERT_RULE  = "Indirect";        //IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 - 

    protected static final Map<String,String> ERROR_MAP = initErrorMap();


	/**
	 * Constructor
	 */

	public MarketRateDealRequest() {
		this(MESSGE_TYPE,AGENT_ID,REQUEST_DTD,RESPONSE_DTD);
	}

	private MarketRateDealRequest(String messageType, String agent, String requestDTD, String responseDTD) {
		super (messageType, agent,requestDTD,responseDTD);
	}


	/**
	 * @param user_oid
	 * @param transactionOID
	 * @param clientBank
	 * @param corpOrg
	 * @param beanMgr
	 * @param resp
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public DocumentHandler queryMarketDeal(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, BeanManager beanMgr, DocumentHandler resp) throws RemoteException, AmsException {
		return (DocumentHandler)query(user_oid, transactionOID, clientBank, corpOrg, resp,beanMgr);
	}



	/**
	 * This method packages the subheader section of request xml
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected void packageSubHeader(String user_oid, CorporateOrganization corpOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException
	{
		super.packageSubHeader(user_oid, corpOrg, outputDoc, object);

		DocumentHandler respDoc = (DocumentHandler) object;
		outputDoc.setAttribute(UniversalMessage.subHeaderPath+"/GetRequestID", respDoc.getAttribute(GETRATE_REQ_ID));    //compute requestid from gx
	}

	/**
	 * This method packages the body section of request xml
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected void packageBody(String messageID, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException

	{

		DocumentHandler respDoc = (DocumentHandler) object;
		DocumentHandler doc = new DocumentHandler();
		String rqUID = messageID+"-01";

		doc.setAttribute("/RqUID",rqUID);

		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/NetworkOwner","Other");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/TerminalId","TRNSACTV");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/Name",TradePortalConstants.PORTAL);
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/BankIdType","BSB");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/BankId","000000");

		doc.setAttribute("/MsgRqHdr/SignonRq/ClientDt",getCurrentDateTime(corpOrg));

		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Org",bankOrg.getAttribute("domain_org_url"));
		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Name","CMO");
		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Version","7.0");

		doc.setAttribute("/PartyRef/IssuedIdent/OrgIdentType","FXIdentifer");
		String fx_onlin_acct_id = corpOrg.getAttribute("fx_online_acct_id");
		if (StringFunction.isBlank(fx_onlin_acct_id)) {
			fx_onlin_acct_id = bankOrg.getAttribute("deflt_fx_online_acct_id");
		}

		String trimed_fx_onlin_acct_id = fx_onlin_acct_id;
		if (fx_onlin_acct_id.length() > 30) {
			trimed_fx_onlin_acct_id=fx_onlin_acct_id.substring(0, 30);
		}

		doc.setAttribute("/PartyRef/IssuedIdent/IdentValue",trimed_fx_onlin_acct_id);

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteId",respDoc.getAttribute(GETRATE_QUOTE_ID));  //compute  Quote Identifier provided by FX Online in ForExQuoteAdd response in the <ForExQuoteIdent> tag
		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/ForExQuoteType","FX Trade");
		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/BuySellIndicator",getBuySellIndicator(transaction, debitAcct));

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/BaseCurCode/Value",getBaseCurrencyCode(transaction, debitAcct, creditAcct));

		//doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/BaseCurAmt",transaction.getAttribute("copy_of_amount"));   //compute

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/ContraCurCode/Value",getContraCurCode(transaction, debitAcct, creditAcct));

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/ForExValDtType","Cash");

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/TranDt",getCurrentDateTime(corpOrg,"yyyy-MM-dd"));

		doc.setAttribute("/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteStatus/ForExQuoteStatusCode","Pending");   //compute Echoed from Quote Response Message Value = Pending


		outputDoc.addComponent("/Proponix/Body/ForExDealAddRq", doc);
	}


	/**
	 * This method unpackages the response.
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected Object unPackageResponse(DocumentHandler doc, Object object) throws RemoteException, AmsException {

		DocumentHandler result= new DocumentHandler();
		DocumentHandler rDoc = doc.getFragment("/Body/ForExDealAddRs");
		String messageID = doc.getAttribute("/SubHeader/ReplytoMessageID");
		String statusCode = rDoc.getAttribute("/Status/StatusCode");
		Date gmtDate = GMTUtility.getGMTDateTime();
		String sDate=DateTimeUtility.convertDateToDateTimeString(gmtDate);

		if (StringFunction.isBlank(statusCode) || SUCCESS_STATUS.equals(statusCode)) {	      // NSX  IR# PRUL121276804 Rel. 7.1.0  12/12/11 
			  result.setAttribute("/ForEXDealId",rDoc.getAttribute("/ForExDealRec/ForExDealId"));
			  result.setAttribute("/ExchRate",rDoc.getAttribute("/ForExDealRec/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteEnvr/ForExRate/ExchRate"));
			  result.setAttribute("/EquivalentAmt",rDoc.getAttribute("/ForExDealRec/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/ContraCurAmt"));            // IR# PDUL103138323 Rel. 7.1.0
			  result.setAttribute("/EquivalentAmtCCY",rDoc.getAttribute("/ForExDealRec/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteInfo/ContraCurCode/Value"));  // IR# PDUL103138323 Rel. 7.1.0
			  result.setAttribute("/DealDateTime",sDate);
			  //IR# DAUM011860866 Rel. 7.1.0   - 01/19/12 - Begin -  	
			  String convertRule = rDoc.getAttribute("/ForExDealRec/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteEnvr/ForExRate/ExchConvertRule");
			  if (StringFunction.isBlank(convertRule)) {
				  convertRule = rDoc.getAttribute("/ForExDealRec/ForExDealInfo/ForExQuoteRef/ForExQuoteRec/ForExQuoteEnvr/ForExRate/ExchMktConvertRule");
			  }
			  if (INDIRECT_CONVERT_RULE.equals(convertRule)) {
				  result.setAttribute("/CalcMethod",TradePortalConstants.DIVIDE);
			  }else {
				  result.setAttribute("/CalcMethod",TradePortalConstants.MULTIPLY);
			  }
		}
		else {
			LOG.warn("Error returned for Market Rate Deal Request,  messageID: {} statusCode: {}" ,messageID, statusCode );
			throw new AmsException(getError(statusCode));
		}

		return result;
	}

        @Override
	protected Map<String,String> getErrorMap() {
		return MarketRateDealRequest.ERROR_MAP;
	}


        @Override
   protected String getNoResponseErrorCode() {

		return TradePortalConstants.FX_GET_DEAL_NO_RESPONSE;
	}


	protected static Map<String,String> initErrorMap() {
		Map<String,String> map = new HashMap<>();
		map.put("100", TradePortalConstants.FX_GET_DEAL_GENERAL_ERR);
		map.put("200", TradePortalConstants.FX_GET_DEAL_GENERAL_ERR);
		map.put("300", TradePortalConstants.FX_GET_DEAL_FX_ONLINE_TIMEDOUT_ERR);
		map.put("700", TradePortalConstants.FX_GET_DEAL_GENERAL_ERR);
		map.put("1020", TradePortalConstants.FX_GET_DEAL_GENERAL_ERR);
		map.put("1800", TradePortalConstants.FX_GET_DEAL_SERVICE_NOT_ENABLED_ERR);
		map.put("3860", TradePortalConstants.FX_GET_DEAL_EXPIRED);
		return map;
	}
}
