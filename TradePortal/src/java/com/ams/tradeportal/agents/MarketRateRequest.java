package com.ams.tradeportal.agents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

/**
 * This class handles Market Rate request query request/response messages. It puts request message
 * in output queue and waits for response. The response received is then unpackaged
 * and an object is returned.
 *
 */
public class MarketRateRequest extends AbstractFXQuery{
private static final Logger LOG = LoggerFactory.getLogger(MarketRateRequest.class);

	protected static final String AGENT_ID             = "fxRateInq";
	protected static final String MESSGE_TYPE          = MessageType.FX_ONLINE_RATE_REQUEST;
	protected static final String REQUEST_DTD          = getDTD(DTD_PATH+"FXQUOTOT.dtd");
	protected static final String RESPONSE_DTD         = getDTD(DTD_PATH+"FXQUOTIN.dtd");

    protected static final Map<String,String> ERROR_MAP = initErrorMap();


	/**
	 * Constructor
	 */

	public MarketRateRequest() {
		this(MESSGE_TYPE,AGENT_ID,REQUEST_DTD,RESPONSE_DTD);
	}

	private MarketRateRequest(String messageType, String agent, String requestDTD, String responseDTD) {
		super (messageType, agent,requestDTD,responseDTD);
	}



	/**
	 * @param user_oid
	 * @param transactionOID
	 * @param clientBank
	 * @param corpOrg
	 * @param beanMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public DocumentHandler queryMarketRate(String user_oid, String transactionOID, ClientBank clientBank, CorporateOrganization corpOrg, BeanManager beanMgr) throws RemoteException, AmsException {
		   return (DocumentHandler)query(user_oid, transactionOID, clientBank, corpOrg, null,beanMgr);
	}



	/* (non-Javadoc)
	 * @see com.ams.tradeportal.agents.AbstractFXQuery#init(java.lang.String, java.lang.String, com.amsinc.ecsg.web.BeanManager)
	 */
        @Override
	protected void init(String user_oid, String transactionOID,BeanManager beanMgr) throws RemoteException, AmsException {

		super.init(user_oid, transactionOID, beanMgr);

		if (!TradePortalConstants.INDICATOR_YES.equals(bankOrg.getAttribute("fx_online_avail_ind"))) {
		    throw new AmsException(getError(TradePortalConstants.FX_GET_RATE_NOT_AVAIL_FOR_BRANCH));
		}

	}




	/**
	 * This method packages the body section of request xml
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected void packageBody(String messageID, CorporateOrganization corpOrg, OperationalBankOrganization opBankOrg, DocumentHandler outputDoc, Object object) throws RemoteException, AmsException

	{


		DocumentHandler doc = new DocumentHandler();
		String rqUID = messageID+"-01";

		doc.setAttribute("/RqUID",rqUID);

		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/NetworkOwner","Other");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/TerminalId","TRNSACTV");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/Name",TradePortalConstants.PORTAL);
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/BankIdType","BSB");
		doc.setAttribute("/MsgRqHdr/NetworkTrnInfo/BankId","000000");

		doc.setAttribute("/MsgRqHdr/SignonRq/ClientDt",getCurrentDateTime(corpOrg));

		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Org",bankOrg.getAttribute("domain_org_url"));

		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Name","CMO");
		doc.setAttribute("/MsgRqHdr/SignonRq/ClientApp/Version","7.0");

		doc.setAttribute("/PartyRef/IssuedIdent/OrgIdentType","FXIdentifer");

		String fx_onlin_acct_id = corpOrg.getAttribute("fx_online_acct_id");

		if (StringFunction.isBlank(fx_onlin_acct_id)) {
			fx_onlin_acct_id = bankOrg.getAttribute("deflt_fx_online_acct_id");
		}

		String trimed_fx_onlin_acct_id = fx_onlin_acct_id;
		if (fx_onlin_acct_id.length() > 30) {
			trimed_fx_onlin_acct_id=fx_onlin_acct_id.substring(0, 30);
		}

		doc.setAttribute("/PartyRef/IssuedIdent/IdentValue",trimed_fx_onlin_acct_id);

		doc.setAttribute("/ForExQuoteInfo/ForExQuoteType","FX Trade");

		doc.setAttribute("/ForExQuoteInfo/BuySellIndicator",getBuySellIndicator(transaction, debitAcct));

		doc.setAttribute("/ForExQuoteInfo/BaseCurCode/Value",getBaseCurrencyCode(transaction, debitAcct, creditAcct));

		doc.setAttribute("/ForExQuoteInfo/BaseCurAmt",transaction.getAttribute("copy_of_amount"));

		doc.setAttribute("/ForExQuoteInfo/ContraCurCode/Value",getContraCurCode(transaction, debitAcct, creditAcct));

		doc.setAttribute("/ForExQuoteInfo/ForExValDtType","Cash");

		doc.setAttribute("/ForExQuoteInfo/RelatedForExDealRef/ForExDealRec/ForExDealId","");

		doc.setAttribute("/ForExQuoteInfo/RelatedForExDealRef/ForExDealRec/ForExDealEnvr/ForExDealIdent",fx_onlin_acct_id);

		doc.setAttribute("/ForExQuoteInfo/RelatedForExDealRef/ForExDealRec/ForExDealStatus/ForExDealStatusCode","");

		doc.setAttribute("/ForExQuoteInfo/DepAcctRef/AcctRec/AcctId","");

		doc.setAttribute("/ForExQuoteInfo/DepAcctRef/AcctRec/AcctInfo/CurCode/Value","");

		doc.setAttribute("/ForExQuoteInfo/DepAcctRef/AcctRec/AcctInfo/FIIdent/Country","AU");

		doc.setAttribute("/ForExQuoteInfo/DepAcctRef/AcctRec/AcctStatus/AcctStatusCode","");

		outputDoc.addComponent("/Proponix/Body/ForExQuoteAddRq", doc);

	}


	/**
	 * This method unpackages the Account balance query response and updates the corresponding accountWebBean.
	 * Those accounts that failed the query is returned in map.
	 * @throws AmsException
	 * @throws RemoteException
	 *
	 */
        @Override
	protected Object unPackageResponse(DocumentHandler doc, Object object) throws RemoteException, AmsException {

		DocumentHandler result= new DocumentHandler();
		DocumentHandler rDoc = doc.getFragment("/Body/ForExQuoteAddRs");
		String messageID = doc.getAttribute("/SubHeader/ReplytoMessageID");
		String statusCode = rDoc.getAttribute("/Status/StatusCode");

		if (StringFunction.isBlank(statusCode) || SUCCESS_STATUS.equals(statusCode)) {        // NSX  IR# PRUL121276804 Rel. 7.1.0  12/12/11
			result.setAttribute("/payAmount",transaction.getAttribute("copy_of_amount"));
			result.setAttribute("/payCurrencyCode",transaction.getAttribute("copy_of_currency_code"));
			result.setAttribute("/equivalentAmount",rDoc.getAttribute("/ForExQuoteRec/ForExQuoteInfo/ContraCurAmt"));
			result.setAttribute("/equivalentCurrencyCode",rDoc.getAttribute("/ForExQuoteRec/ForExQuoteInfo/ContraCurCode/Value"));
			result.setAttribute("/FXRate",rDoc.getAttribute("/ForExQuoteRec/ForExQuoteEnvr/ForExRate/ExchRate"));
			result.setAttribute("/ForExQuoteID",rDoc.getAttribute("/ForExQuoteRec/ForExQuoteEnvr/ForExQuoteIdent"));
			result.setAttribute(GETRATE_REQ_ID,messageID);

		}
		else {

			LOG.warn("Error returned for Market Rate Request,  messageID: {} statusCode: {}" ,messageID, statusCode );
			throw new AmsException(getError(statusCode));
		}

		return result;
	}

        @Override
	protected Map<String,String> getErrorMap() {
		return MarketRateRequest.ERROR_MAP;
	}

        @Override
	protected String getNoResponseErrorCode() {

		return TradePortalConstants.FX_GET_RATE_NO_RESPONSE;
	}

	protected static Map<String,String> initErrorMap() {
		Map<String,String> map = new HashMap<>();
		map.put("100", TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
		map.put("200", TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
		map.put("700", TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
		map.put("1020", TradePortalConstants.FX_GET_RATE_GENERAL_ERR);
		map.put("1045", TradePortalConstants.FX_GET_RATE_REQUEST_DENIED_ERR);
		map.put("1800", TradePortalConstants.FX_GET_RATE_SERVICE_NOT_ENABLED_ERR);
		return map;
	}

}
