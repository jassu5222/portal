package com.ams.tradeportal.agents;


import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.common.PaymentBeneficiaryEmailProcessor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;



public class PaymentBeneficiaryEmailAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(PaymentBeneficiaryEmailAgent.class);

	/**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
	 *    This method is called every 10 seconds or so. Actual frequency is softcoded through
	 *    agentTimerInterval parameter in AgentConfiguration.properties file.
	 *    It returns -1 if exception occured during processing, or MQ Series became unaccessible,
	 *    or some other problem occured. It returns 1 if everything went successfully.
	 *    @return int
	 */
	@Override
	protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {
		
		
		try {
			////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
			if (isOrphan == true) {

				String selectOrphanSQL = 
					"SELECT PAYMENT_BEN_EMAIL_QUEUE_OID, A_TRANSACTION_OID, REQUIRED_STATUS FROM " +
						"(SELECT PAYMENT_BEN_EMAIL_QUEUE_OID, A_TRANSACTION_OID, REQUIRED_STATUS " + 
						"FROM PAYMENT_BEN_EMAIL_QUEUE " + 
						"WHERE "+ 
							"AGENT_ID=? AND " + 
							" STATUS = ? " + 
						"ORDER BY CREATION_TIMESTAMP) " + 
					"WHERE ROWNUM = 1";	
								
				query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, TradePortalConstants.PYMT_BEN_EMAIL_IN_PROGRESS});
				query.getRecords();				
				
				////////////////////////////////////////////////
				// BEGIN PROCESSING OF:    ORPHAN FOUND       //
				////////////////////////////////////////////////
				if (query.getRecordCount() == 1){
					isOrphan = true;
					String s_oid = query.getRecordValue("PAYMENT_BEN_EMAIL_QUEUE_OID");
					String transactionOID = query.getRecordValue("A_TRANSACTION_OID");
					String requiredStatus = query.getRecordValue("REQUIRED_STATUS");
					logInfo("Orphan found! Start processing orphan " + s_oid + "...");
					ClientServerDataBridge csdb = new ClientServerDataBridge();
					csdb.setLocaleName(LOCALE_NAME);
					
					// Send the Emails
					invokeEmailProcessor(csdb, transactionOID, requiredStatus);
					
					// The item has been processed successfully.  Delete the queue record.
					String deleteSQL = "DELETE PAYMENT_BEN_EMAIL_QUEUE WHERE PAYMENT_BEN_EMAIL_QUEUE_OID = ?";
					DatabaseQueryBean.executeUpdate(deleteSQL, false, new Object[]{s_oid});					
				}           
			}
            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                return AGENT_SUCCESS;
            }


        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        catch(SQLException sqle){
            logError("Error deleting queue record for orpha.", sqle);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
	        
        try {

			int processSuccess = AGENT_NO_RECORDS_TO_PROCESS;
			List<Object> whereSqlParams = new ArrayList<Object>();
			whereSqlParams.add(TradePortalConstants.PYMT_BEN_EMAIL_PENDING);
			whereSqlParams.add(TradePortalConstants.PYMT_BEN_EMAIL_IN_PROGRESS);
			
			
			List<Object> updateSqlParams = new ArrayList<Object>();
			updateSqlParams.add(AGENT_ID);
			updateSqlParams.add(TradePortalConstants.PYMT_BEN_EMAIL_IN_PROGRESS);
			
					

			long oidLong = DatabaseQueryBean.selectForUpdate( "PAYMENT_BEN_EMAIL_QUEUE",
					"PAYMENT_BEN_EMAIL_QUEUE_OID",
					"PAYMENT_BEN_EMAIL_QUEUE_OID = (SELECT PAYMENT_BEN_EMAIL_QUEUE_OID FROM " + 
					"(SELECT PAYMENT_BEN_EMAIL_QUEUE_OID FROM PAYMENT_BEN_EMAIL_QUEUE WHERE " + 
					"AGENT_ID IS NULL AND STATUS =? " +
					" AND A_TRANSACTION_OID NOT IN (SELECT A_TRANSACTION_OID FROM PAYMENT_BEN_EMAIL_QUEUE WHERE STATUS = ?) " +					
					" ORDER BY CREATION_TIMESTAMP) WHERE ROWNUM = 1)",
					" AGENT_ID = ?, STATUS=?",
					false, true, whereSqlParams, updateSqlParams);
	
			////////////////////////////////////////////////
			// BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
			////////////////////////////////////////////////

			if(oidLong != 0){
				logInfo("New message found. Start processing new message..." + oidLong);
				
				String selectDomPmtOidSQL = "select A_TRANSACTION_OID, REQUIRED_STATUS from PAYMENT_BEN_EMAIL_QUEUE where PAYMENT_BEN_EMAIL_QUEUE_OID = ? ";
				DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(selectDomPmtOidSQL, false, new Object[]{oidLong});
				if ( resultSet != null && !StringFunction.isBlank(resultSet.getAttribute("/ResultSetRecord/A_TRANSACTION_OID"))) {
					long startMessage = System.currentTimeMillis();
					String transactionOid = resultSet.getAttribute("/ResultSetRecord/A_TRANSACTION_OID");
					String requiredStatus = resultSet.getAttribute("/ResultSetRecord/REQUIRED_STATUS");
					
	
					ClientServerDataBridge csdb = new ClientServerDataBridge();
					csdb.setLocaleName(LOCALE_NAME);
					
					// Send the Emails
					invokeEmailProcessor(csdb, transactionOid, requiredStatus);

					// The item has been processed successfully.  Delete the queue record.
					String deleteSQL = "DELETE PAYMENT_BEN_EMAIL_QUEUE WHERE PAYMENT_BEN_EMAIL_QUEUE_OID = ?";
					DatabaseQueryBean.executeUpdate(deleteSQL, false, new Object[]{oidLong});		
					
					
					logInfo("Finished processing new message");

					logInfo("Complete time = " + (System.currentTimeMillis()-startMessage)
							+"\tmilliseconds");
				}
				return AGENT_SUCCESS;
			}
		}
		catch(AmsException e){
			//MDB Rel6.1 IR# PDUL012842185 1/28/11 - Begin
			if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED))
			{
				logInfo("Contention issue between agents - next agent will try and process the next message");
				return AGENT_SUCCESS;
			}
			//MDB Rel6.1 IR# PDUL012842185 1/28/11 - End

			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
		catch(NumberFormatException e){
			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
		catch(SQLException sqle){
            logError("Error deleting Queue record for new message.", sqle);
            return AGENT_FAILURE;
        }
		////////////////////////////////////////////////
		// END PROCESSING OF: NEW MESSAGE FOUND       //
		////////////////////////////////////////////////

		///////////////////////////////////////////////////
		// NEED Archive process similar to OutboundAgent//
		//////////////////////////////////////////////////

		return AGENT_NO_RECORDS_TO_PROCESS;

	}// end of lookForWorkAndDoIt()

	
	protected void invokeEmailProcessor(ClientServerDataBridge csdb, String transactionOID, String requiredStatus) throws RemoteException, AmsException, RemoveException{
		
		logInfo("TransactionOID [" + transactionOID + "] - Looking for DomesticPayments with PAYMENT_STATUS =" + requiredStatus);
		
		String sqlStatement = "SELECT DOMESTIC_PAYMENT_OID from DOMESTIC_PAYMENT WHERE p_transaction_oid = ? AND payment_ben_email_sent_flag='N' AND PAYMENT_STATUS = ? AND payee_email is not null";
			
		
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, transactionOID, requiredStatus);
		
		if(resultXML != null){
			int currentIndex = 0;
			String domesticPaymentOid = resultXML.getAttribute("/ResultSetRecord(" + currentIndex + ")/DOMESTIC_PAYMENT_OID");
			
			// CJR 5/21/2011 Begin - Moved ejb creation to Agent instead of processor.
			Transaction transaction = (Transaction) EJBObjectFactory
					.createClientEJB(ejbServerLocation, "Transaction", Long.parseLong(transactionOID));
			String instrumentOid = transaction.getAttribute("instrument_oid");
			Instrument instrument = (Instrument) EJBObjectFactory
					.createClientEJB(ejbServerLocation, "Instrument", Long.parseLong(instrumentOid));
			
			String corpOrgOid = instrument.getAttribute("corp_org_oid");
			CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory
					.createClientEJB(ejbServerLocation, "CorporateOrganization", Long.parseLong(corpOrgOid));			
		
			String bankOrgGroupOid = corpOrg.getAttribute("bank_org_group_oid");
			
			BankOrganizationGroup bankOrg = (BankOrganizationGroup) EJBObjectFactory.createClientEJB(
					ejbServerLocation, "BankOrganizationGroup", Long.parseLong(bankOrgGroupOid));
			// CJR 5/21/2011 End - Moved ejb creation to Agent instead of processor.
			
			// SL - 10/29/2014 R9.1 IR T36000030417  [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation 
			String termsOid  = transaction.getAttribute("c_CustomerEnteredTerms");
			String sqlQry = "select BANK_NAME from PAYMENT_PARTY where payment_party_oid in (select c_ordering_party_oid from terms where terms_oid = ? )";
			
			DocumentHandler resultXMLOrderingParty = DatabaseQueryBean.getXmlResultSet(sqlQry, false, new Object[]{termsOid});
			//Null Check is added 
			      String orderingPartyName = null;
			         if(resultXMLOrderingParty != null)
			        	 orderingPartyName = resultXMLOrderingParty.getAttribute("/ResultSetRecord(" + currentIndex + ")/BANK_NAME"); 
			         
			// SL - 10/29/2014 R9.1 IR T36000030417  [END]
			
			while(!StringFunction.isBlank(domesticPaymentOid)){
				logInfo("Invoking PaymentBeneficiaryEmailProcessor for TermsOID [" + termsOid + "]");
				logInfo("Invoking PaymentBeneficiaryEmailProcessor for orderingPartyName [" + orderingPartyName + "]");
				logInfo("Invoking PaymentBeneficiaryEmailProcessor for TransactionOID [" + transactionOID + "] DomesticPayment [" + domesticPaymentOid + "]");
				try{
					PaymentBeneficiaryEmailProcessor processor = new PaymentBeneficiaryEmailProcessor(transaction, instrument,
							corpOrg, bankOrg, Long.parseLong(domesticPaymentOid), ejbServerLocation, csdb, orderingPartyName);
					processor.processDomesticPayment();
				}
				catch (Exception e)
				{
					logError("Error invoking PaymentBeneficiaryEmailProcessor", e);
					e.printStackTrace();
				}
				currentIndex++;
				domesticPaymentOid = resultXML.getAttribute("/ResultSetRecord(" + currentIndex + ")/DOMESTIC_PAYMENT_OID");
			}
			
			if(currentIndex == 0){
				logInfo("TransactionOID [" + transactionOID + "] - No DomesticPayments retrieved to be processed.");
			}else{
				logInfo("TransactionOID [" + transactionOID + "] Total Domestic Payments processed = " + currentIndex );
			}
			transaction.remove();
			instrument.remove();
			corpOrg.remove();
			bankOrg.remove();			
		}else{
			logInfo("TransactionOID [" + transactionOID + "] - No DomesticPayments retrieved to be processed.");			
		}
		
	}
	
	@Override
    protected  boolean connectToMQ() {
     return false;	
    }		

}