package com.ams.tradeportal.agents;


import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.RemoveException;

import com.ams.tradeportal.common.PurchaseOrderFileProcessor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;


/**
 *   Here goes detailed description of PurchaseOrderUploadAgent functionality
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PurchaseOrderUploadAgent extends TradePortalAgent
{
//private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderUploadAgent.class);
    
	/**   This method is an actual implementation if TradePortalAgent's lookForWorkAndDoIt().
	 *    This method is called every 10 seconds or so. Actual frequency is softcoded through
	 *    agentTimerInterval parameter in AgentConfiguration.properties file.
	 *    It returns -1 if exception occured during processing, or MQ Series became unaccessible,
	 *    or some other problem occured. It returns 1 if everything went successfully.
	 *    @return int
	 */
    @Override
	protected int lookForWorkAndDoIt() throws RemoteException, RemoveException {
		
		
		try {
			////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    isOrphan == TRUE   //
            ////////////////////////////////////////////////
			if (isOrphan == true) {
				
				
				String selectOrphanSQL = "Select po_file_upload_oid from (Select po_file_upload_oid from PO_FILE_UPLOADS where agent_id=? and " +
				"(validation_status=?) order by creation_timestamp) where rownum = 1";
				query.setSQL(selectOrphanSQL, new Object[]{AGENT_ID, TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS});
				query.getRecords();
				
				////////////////////////////////////////////////
				// BEGIN PROCESSING OF:    ORPHAN FOUND       //
				////////////////////////////////////////////////
				if (query.getRecordCount() == 1){
					isOrphan = true;
					String s_oid = query.getRecordValue("po_file_upload_oid");
					logInfo("Orphan found! Start processing orphan " + s_oid + "...");
					ClientServerDataBridge csdb = new ClientServerDataBridge();
					csdb.setLocaleName(LOCALE_NAME);

					PurchaseOrderFileProcessor processor = new PurchaseOrderFileProcessor(AGENT_ID,Long.parseLong(s_oid),ejbServerLocation,csdb);
					processor.processOrphan();
				}
           
			}
            ////////////////////////////////////////////////
            // BEGIN PROCESSING OF:    ORPHAN NOT FOUND   //
            ////////////////////////////////////////////////
            else{
                logInfo("No orphans found");
                isOrphan = false;
                return AGENT_SUCCESS;
            }


        }
        catch(AmsException e){
            logError("While processing orphan", e);
            return AGENT_FAILURE;
        }
        ////////////////////////////////////////////////
        // END PROCESSING OF:   isOrphan == TRUE      //
        ////////////////////////////////////////////////



        ////////////////////////////////////////////////
        // BEGIN PROCESSING OF: isOrphan == FALSE     //
        ////////////////////////////////////////////////
	        
        try {
			int processSuccess = AGENT_NO_RECORDS_TO_PROCESS;
			
			List<Object> whereSqlParams = new ArrayList<Object>();
			whereSqlParams.add(TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
			whereSqlParams.add(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS);
			
			List<Object> updateSqlParams = new ArrayList<Object>();
			updateSqlParams.add(AGENT_ID);

			long oidLong = DatabaseQueryBean.selectForUpdate( "PO_FILE_UPLOADS",
					"po_file_upload_oid",
					"po_file_upload_oid = (select po_file_upload_oid from (select po_file_upload_oid from PO_FILE_UPLOADS " +
					"where agent_id is NULL and validation_status=? " +
					" and a_owner_org_oid not in (select distinct a_owner_org_oid from po_file_uploads where validation_status=?)" +		//IR# NSUM042659866
					" order by creation_timestamp) where rownum = 1)",
					"agent_id = ? ",
					false, true, whereSqlParams, updateSqlParams); 
			////////////////////////////////////////////////
			// BEGIN PROCESSING OF: NEW MESSAGE FOUND     //
			////////////////////////////////////////////////

			if(oidLong != 0){
				logInfo("New message found. Start processing new message..." + oidLong);

				long startMessage = System.currentTimeMillis();

				ClientServerDataBridge csdb = new ClientServerDataBridge();
				csdb.setLocaleName(LOCALE_NAME);
				PurchaseOrderFileProcessor processor = new PurchaseOrderFileProcessor(AGENT_ID,oidLong,ejbServerLocation,csdb);
				
				processor.processPurchaseOrderFile();

				logInfo("Finished processing new message " +  oidLong );
				logInfo("Complete time = " + (System.currentTimeMillis()-startMessage)
						+"\tmilliseconds");
				return AGENT_SUCCESS;
			}
		}
		catch(AmsException e){
			if (e.getMessage().equalsIgnoreCase(TradePortalConstants.ROW_LOCKED))
			{
				logInfo("Contention issue between agents - next agent will try and process the next message");
				return AGENT_SUCCESS;
			}

			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
		catch(NumberFormatException | IOException e){
			logError("While processing new message", e);
			return AGENT_FAILURE;
		}
        ////////////////////////////////////////////////
        // END PROCESSING OF: NEW MESSAGE FOUND       //
        ////////////////////////////////////////////////

		////////////////////////////////////////////////
		// END PROCESSING OF: NEW MESSAGE FOUND       //
		////////////////////////////////////////////////

		///////////////////////////////////////////////////
		// NEED Archive process similar to OutboundAgent//
		//////////////////////////////////////////////////

		return AGENT_NO_RECORDS_TO_PROCESS;

	}// end of lookForWorkAndDoIt()

	
    @Override
    protected  boolean connectToMQ() {
     return false;	
    }	

}