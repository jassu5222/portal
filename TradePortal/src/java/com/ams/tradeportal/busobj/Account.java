



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface Account extends TradePortalBusinessObject
{
	public String getBankBranchCountryInISO() throws RemoteException, AmsException;      	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - 

    //MDB PIUL102167761 Rel7.1 10/21/11 Begin
	public String getBaseCurrencyOfAccount() throws RemoteException, AmsException;
	public String getClientBankOfAccount() throws RemoteException, AmsException;
	public String getOwnerOfAccount() throws RemoteException, AmsException;
    //MDB PIUL102167761 Rel7.1 10/21/11 End
}
