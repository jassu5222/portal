
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActiveInstrumentPartyBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ActiveInstrumentPartyBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* active_party_oid - Unique Identifier */
      attributeMgr.registerAttribute("active_party_oid", "active_party_oid", "ObjectIDAttribute");
      
      /* terms_party_type - The type of terms party.  This information is also stored on the associated
         TermsParty but is replicated here for performance. */
      attributeMgr.registerReferenceAttribute("terms_party_type", "terms_party_type", "TERMS_PARTY_TYPE");
      attributeMgr.requiredAttribute("terms_party_type");
      
      /* a_terms_party_oid - Association to terms party.  But cannot register as association since the
         terms party may not have been saved to the database yet and thus would not
         pass association validation. */
      attributeMgr.registerAttribute("a_terms_party_oid", "a_terms_party_oid", "NumberAttribute");
      
        /* Pointer to the parent Instrument */
      attributeMgr.registerAttribute("instrument_oid", "p_instrument_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
