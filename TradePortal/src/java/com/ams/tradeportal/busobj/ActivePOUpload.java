package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * When a user initiates an Auto LC Create operation by uploading a file, a
 * row is placed into this table.   This row contains data about the current
 * process.  Prior to starting an Auto LC Create operation, the presence of
 * a row in this table is checked for.  If there is a row, the user is not
 * allowed to move forward with the process.
 * 
 * The AutoLCAgent looks for rows in this table in order to kick off the actual
 * process of creating LCs from purchase orders.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ActivePOUpload extends TradePortalBusinessObject
{   
}
