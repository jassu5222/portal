 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActivePOUploadBean extends ActivePOUploadBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ActivePOUploadBean.class);
   /**
    * This method is used to initialize several attributes for a new corporate
    * organization. Specifically, it sets the creation date for a new active 
    * PO upload.
    */
   protected void userNewObject() throws RemoteException, AmsException
   {
      super.userNewObject();

      this.setAttribute("creation_timestamp", DateTimeUtility.getGMTDateTime());
   }
 }
