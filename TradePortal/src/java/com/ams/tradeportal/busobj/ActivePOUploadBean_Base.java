
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user initiates an Auto LC Create operation by uploading a file, a
 * row is placed into this table.   This row contains data about the current
 * process.  Prior to starting an Auto LC Create operation, the presence of
 * a row in this table is checked for.  If there is a row, the user is not
 * allowed to move forward with the process.
 * 
 * The AutoLCAgent looks for rows in this table in order to kick off the actual
 * process of creating LCs from purchase orders.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActivePOUploadBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ActivePOUploadBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* active_po_upload_oid - Unique identifier */
      attributeMgr.registerAttribute("active_po_upload_oid", "active_po_upload_oid", "ObjectIDAttribute");
      
      /* creation_timestamp - Timestamp (in GMT) of when this business object instance was created */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* status - The status of the upload process. */
      attributeMgr.registerReferenceAttribute("status", "status", "PO_UPLOAD_STATUS");
      
      /* po_data_file - The contents of the file that was uploaded by the user for this Auto LC
         Create process. */
      attributeMgr.registerAttribute("po_data_file", "po_data_file");
      
      /* po_upload_parameters - An XML document containing information about what the user wants to happen
         with the data they just uploaded. */
      attributeMgr.registerAttribute("po_upload_parameters", "po_upload_parameters");
      
      /* agent_id - The ID of the agent that picked up this row for processing. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* corp_org_oid - The corporate organiztaion that is currently performing an upload of purchase
         orders. */
      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");
      
   }
   
 
   
 
 
   
}
