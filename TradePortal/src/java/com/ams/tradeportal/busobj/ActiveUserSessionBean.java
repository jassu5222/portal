 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActiveUserSessionBean extends ActiveUserSessionBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ActiveUserSessionBean.class);

   
  /*
   * 
   * @exception java.rmi.RemoteException 
   * @exception com.amsinc.ecsg.frame.AmsException 
   */
   protected void userNewObject() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
    {
       this.setAttribute("creation_timestamp", DateTimeUtility.getGMTDateTime(true, false));

       PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

       this.setAttribute("server_instance_name", portalProperties.getString("serverName") );
    }
   
   
   public String getListCriteria(String type) 
   {
	  // Retrieve the user by certificate
	  if (type.equals("byServer")) {
		 return "server_instance_name = {0}";
	  }
	  
	  return "";
   }       
   

}
