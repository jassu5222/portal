
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AddressBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(AddressBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* address_oid - Unique Identifier */
      attributeMgr.registerAttribute("address_oid", "address_oid", "ObjectIDAttribute");
      
      /* city - City */
      attributeMgr.registerAttribute("city", "city");
      attributeMgr.requiredAttribute("city");
      attributeMgr.registerAlias("city", getResourceManager().getText("AddressBeanAlias.city", TradePortalConstants.TEXT_BUNDLE));
      
      /* country - Country */
      attributeMgr.registerReferenceAttribute("country", "country", "COUNTRY");
      attributeMgr.requiredAttribute("country");
      attributeMgr.registerAlias("country", getResourceManager().getText("AddressBeanAlias.country", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_1 - Address Line 1 */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");
      attributeMgr.requiredAttribute("address_line_1");
      attributeMgr.registerAlias("address_line_1", getResourceManager().getText("AddressBeanAlias.address_line_1", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_2 - Address Line 2 */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");
      attributeMgr.registerAlias("address_line_2", getResourceManager().getText("AddressBeanAlias.address_line_2", TradePortalConstants.TEXT_BUNDLE));
      
      /* state_province - State or Province */
      attributeMgr.registerAttribute("state_province", "state_province");
      
      /* postal_code - Postal Code */
      attributeMgr.registerAttribute("postal_code", "postal_code");
      
      /* address_seq_num - Sequence Number of Address (2-99) */
      attributeMgr.registerAttribute("address_seq_num", "address_seq_num", "NumberAttribute");
      attributeMgr.requiredAttribute("address_seq_num");
      attributeMgr.registerAlias("address_seq_num", getResourceManager().getText("AddressBeanAlias.address_seq_num", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
      
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
      //added attributes for Portal only Bank Additional address user defined fields
      attributeMgr.registerAttribute("user_defined_field_1", "user_defined_field_1");
      attributeMgr.registerAttribute("user_defined_field_2", "user_defined_field_2");
      attributeMgr.registerAttribute("user_defined_field_3", "user_defined_field_3");
      attributeMgr.registerAttribute("user_defined_field_4", "user_defined_field_4");
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
   
   }
   
 
   
 
 
   
}
