
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AddressHome extends EJBHome
{
   public Address create()
      throws RemoteException, CreateException, AmsException;

   public Address create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
