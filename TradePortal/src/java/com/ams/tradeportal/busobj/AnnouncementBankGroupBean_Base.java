

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Bank group association with an announcement.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AnnouncementBankGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementBankGroupBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* announcement_bank_org_group_oid - Unique identifier */
      attributeMgr.registerAttribute("announcement_bank_group_oid", "announcement_bank_group_oid", "ObjectIDAttribute");

      /* Pointer to the parent Announcement */
      attributeMgr.registerAttribute("announcement_oid", "p_announcement_oid", "ParentIDAttribute");

      /* bog_oid - The bank org group association. */
      attributeMgr.registerAssociation("bog_oid", "a_bog_oid", "BankOrganizationGroup");
   }






}
