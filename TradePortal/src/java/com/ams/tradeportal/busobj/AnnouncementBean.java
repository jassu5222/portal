 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.StringFunction;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AnnouncementBean extends AnnouncementBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementBean.class);

	public void userValidate()
			throws RemoteException, AmsException {
		
		// if start_date is greater than end_date, then throw back exception
		String startdate = this.getAttribute("start_date");
		String enddate = this.getAttribute("end_date");
		

		
		if (!StringFunction.isBlank(startdate) && !StringFunction.isBlank(enddate)) {
			Date start_Date = new Date (startdate);
			Date end_Date = new Date (enddate);
			if (start_Date.after(end_Date)) {
				this.getErrorManager().issueError(
	  					TradePortalConstants.ERR_CAT_1,
	  					TradePortalConstants.MUST_BE_LESS_THAN,
	  					"Start Date", "End Date");
			}
		}
			
		
	}
  
}
