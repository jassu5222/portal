
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;



/*
 * An Announcement.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AnnouncementBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(AnnouncementBean_Base.class);
  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* announcement_oid - Unique identifier */
      attributeMgr.registerAttribute("announcement_oid", "announcement_oid", "ObjectIDAttribute");
      
      /* title - a short blurb that provides the essence of the announcement. */
      attributeMgr.registerAttribute("title", "title");
      attributeMgr.requiredAttribute("title");
      
      /* subject - the text of the announcement. */
      attributeMgr.registerAttribute("subject", "subject");
      attributeMgr.requiredAttribute("subject");
      
      /* status - announcement status. */
      attributeMgr.registerReferenceAttribute("announcement_status", "announcement_status", "ANNOUNCEMENT_STATUS");
      
      /* start_date - the date the announcement should first be displayed to users */
      attributeMgr.registerAttribute("start_date", "start_date", "DateAttribute");
      attributeMgr.requiredAttribute("start_date");
      
      /* end_date - the last day the announcement should be displayed to users */
      attributeMgr.registerAttribute("end_date", "end_date", "DateAttribute");
      attributeMgr.requiredAttribute("end_date");

      /* critical - indicates whether critical or non-critical announcement */
      attributeMgr.registerAttribute("critical_ind", "critical_ind", "IndicatorAttribute");

      /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   
      /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer */
      attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");
      attributeMgr.requiredAttribute("ownership_level");

      /* does the announcement apply to all bank org groups?  this only 
       * applies when the ownership_level=client bank */
      attributeMgr.registerAttribute("all_bogs_ind", "all_bogs_ind", "IndicatorAttribute");

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on hRow this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
   }
   
  /*
   * Register the components of the business object
   */
   
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
    super.registerComponents();

      /* BankGroupList - the bank groups the announcement is applicable for.
       * This only applies when the ownership_level=client bank.
       * note that an announcement owns an association to a bank org group,
       * not the bank org group itself */
      registerOneToManyComponent("AnnouncementBankGroupList","AnnouncementBankGroupList");
   }

 
}
