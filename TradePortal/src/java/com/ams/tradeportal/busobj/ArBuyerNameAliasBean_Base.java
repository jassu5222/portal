

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * The aliases for the AR Buyer Name in an ARMatchingRule.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ArBuyerNameAliasBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ArBuyerNameAliasBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* ar_buyer_name_alias_oid - Unique Identifier */
      attributeMgr.registerAttribute("ar_buyer_name_alias_oid", "ar_buyer_name_alias_oid", "ObjectIDAttribute");

      /* buyer_name_alias - Buyer Name Alias */
      attributeMgr.registerAttribute("buyer_name_alias", "buyer_name_alias");
      attributeMgr.registerAlias("buyer_name_alias", getResourceManager().getText("ArBuyerNameAliasBeanAlias.buyer_name_alias", TradePortalConstants.TEXT_BUNDLE));

        /* Pointer to the parent ArMatchingRule */
      attributeMgr.registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");

   }






}
