

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * This is the rule that a corporate customer can set up to match AR payment
 * and invoices.  This rule is sent to OTL.  It is not used in TP otherwise.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ArMatchingRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ArMatchingRuleBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* ar_matching_rule_oid -  */
      attributeMgr.registerAttribute("ar_matching_rule_oid", "ar_matching_rule_oid", "ObjectIDAttribute");

      /* buyer_id - Buyer ID */
      attributeMgr.registerAttribute("buyer_id", "buyer_id");
      attributeMgr.requiredAttribute("buyer_id");
      attributeMgr.registerAlias("buyer_id", getResourceManager().getText("ArMatchingRuleDetail.BuyerId", TradePortalConstants.TEXT_BUNDLE));

      /* buyer_name - Buyer Name */
      attributeMgr.registerAttribute("buyer_name", "buyer_name");
      attributeMgr.requiredAttribute("buyer_name");
      attributeMgr.registerAlias("buyer_name", getResourceManager().getText("ArMatchingRuleDetail.BuyerName", TradePortalConstants.TEXT_BUNDLE));

		attributeMgr.registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");
      /* tolerance_percent_indicator - Whether tolerance percent is used in matching. */
      attributeMgr.registerAttribute("tolerance_percent_indicator", "tolerance_percent_indicator", "IndicatorAttribute");

      attributeMgr.registerAlias("tolerance_percent_indicator", getResourceManager().getText("ArMatchingRuleBeanAlias.tolerance_percent_indicator", TradePortalConstants.TEXT_BUNDLE));

      /* tolerance_percent_plus - Plus percentage of matching tolerance */
      attributeMgr.registerAttribute("tolerance_percent_plus", "tolerance_percent_plus", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      attributeMgr.registerAlias("tolerance_percent_plus", getResourceManager().getText("ArMatchingRuleBeanAlias.tolerance_percent_plus", TradePortalConstants.TEXT_BUNDLE));

      /* tolerance_percent_minus - Minus percent of matching tolerance. */
      attributeMgr.registerAttribute("tolerance_percent_minus", "tolerance_percent_minus", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      attributeMgr.registerAlias("tolerance_percent_minus", getResourceManager().getText("ArMatchingRuleBeanAlias.tolerance_percent_minus", TradePortalConstants.TEXT_BUNDLE));

      /* tolerance_amount_indicator - Whether tolerance amount is used in matching. */
      attributeMgr.registerAttribute("tolerance_amount_indicator", "tolerance_amount_indicator", "IndicatorAttribute");

      attributeMgr.registerAlias("tolerance_amount_indicator", getResourceManager().getText("ArMatchingRuleBeanAlias.tolerance_amount_indicator", TradePortalConstants.TEXT_BUNDLE));

      /* tolerance_amount_plus - Plus amount used in matching tolerance. */
      attributeMgr.registerAttribute("tolerance_amount_plus", "tolerance_amount_plus", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      attributeMgr.registerAlias("tolerance_amount_plus", getResourceManager().getText("ArMatchingRuleBeanAlias.tolerance_amount_plus", TradePortalConstants.TEXT_BUNDLE));

      /* tolerance_amount_minus - Minus amount used in matching tolerance. */
      attributeMgr.registerAttribute("tolerance_amount_minus", "tolerance_amount_minus", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      /* no_tolerance_indicator - Y = No tolerance is used in matching. */
      attributeMgr.registerAttribute("no_tolerance_indicator", "no_tolerance_indicator", "IndicatorAttribute");

      attributeMgr.registerAlias("no_tolerance_indicator", getResourceManager().getText("ArMatchingRuleBeanAlias.no_tolerance_indicator", TradePortalConstants.TEXT_BUNDLE));

      /* partial_pay_to_portal_indicator - Whether partially paid invoice is sent to portal. */
      attributeMgr.registerAttribute("partial_pay_to_portal_indicator", "partial_pay_to_portal_ind", "IndicatorAttribute");

      attributeMgr.registerAlias("partial_pay_to_portal_indicator", getResourceManager().getText("ArMatchingRuleBeanAlias.partial_pay_to_portal_indicator", TradePortalConstants.TEXT_BUNDLE));

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");

	/* address_line_1 - First line of the address */
	attributeMgr.registerAttribute("address_line_1", "address_line_1");
	 attributeMgr.requiredAttribute("address_line_1");
	attributeMgr.registerAlias("address_line_1", getResourceManager().getText("ArMatchingRuleBeanAlias.address_line_1", TradePortalConstants.TEXT_BUNDLE));

/* address_line_2 - First line of the address */
	attributeMgr.registerAttribute("address_line_2", "address_line_2");

	//attributeMgr.registerAlias("address_line_2", getResourceManager().getText("TradingPartnerDetails.TradingAddress2", TradePortalConstants.TEXT_BUNDLE));


	/* address_city - The city in which the party is located */
	attributeMgr.registerAttribute("address_city", "address_city");	
	attributeMgr.requiredAttribute("address_city");
	attributeMgr.registerAlias("address_city", getResourceManager().getText("ArMatchingRuleBeanAlias.address_city", TradePortalConstants.TEXT_BUNDLE));

/* address_city - The city in which the party is located */
	attributeMgr.registerAttribute("address_state", "address_state");	
	//attributeMgr.registerAlias("address_state", getResourceManager().getText("TradingPartnerDetails.TradingProvince", TradePortalConstants.TEXT_BUNDLE));


	/* address_country - The country in which the party is located */
	attributeMgr.registerAttribute("address_country", "address_country");
	attributeMgr.requiredAttribute("address_country");
	attributeMgr.registerAlias("address_country", getResourceManager().getText("ArMatchingRuleBeanAlias.address_country", TradePortalConstants.TEXT_BUNDLE));

/* address_city - The city in which the party is located */
	attributeMgr.registerAttribute("postalcode", "postalcode");	
	//attributeMgr.registerAlias("postalcode", getResourceManager().getText("TradingPartnerDetails.TradingPostalCode", TradePortalConstants.TEXT_BUNDLE));


	/* creation_date_time - Timestamp (in GMT) of when the MAtching Rule was created. */
      attributeMgr.registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");

	attributeMgr.registerAlias("creation_date_time", getResourceManager().getText("ArMatchingRuleBeanAlias.creation_date_time", TradePortalConstants.TEXT_BUNDLE));
	
        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 register aliases for proper error messages - Start 
		/* payment_day_allow - The city in which the party is located */
		 attributeMgr.registerAttribute("payment_day_allow", "payment_day_allow");
                 attributeMgr.registerAlias("payment_day_allow", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesPayment", TradePortalConstants.TEXT_BUNDLE));

		/* days_before - The city in which the party is located */
		attributeMgr.registerAttribute("days_before", "days_before","NumberAttribute");
                attributeMgr.registerAlias("days_before", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesDaysBefore", TradePortalConstants.TEXT_BUNDLE));

		/* days_after - The city in which the party is located */
		attributeMgr.registerAttribute("days_after", "days_after","NumberAttribute");
                attributeMgr.registerAlias("days_after", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesDaysAfter", TradePortalConstants.TEXT_BUNDLE));
		
		/* invoice_value_finance - The city in which the party is located */
		attributeMgr.registerAttribute("invoice_value_finance", "invoice_value_finance","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
                attributeMgr.registerAlias("invoice_value_finance", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesInvoiceValue", TradePortalConstants.TEXT_BUNDLE));

		/* days_finance_of_payment - The city in which the party is located */
		attributeMgr.registerAttribute("days_finance_of_payment", "days_finance_of_payment","NumberAttribute");
                attributeMgr.registerAlias("days_finance_of_payment", getResourceManager().getText("TradingPartnerDetails.BankDefinedRulesDaysAllow", TradePortalConstants.TEXT_BUNDLE));
        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 register aliases for proper error messages - Start 
                
		/* receivable_invoice - The city in which the party is located */
		attributeMgr.registerAttribute("receivable_invoice", "receivable_invoice", "IndicatorAttribute");
	 
		/* receivable_authorise - The city in which the party is located */
		attributeMgr.registerReferenceAttribute("receivable_authorise", "receivable_authorise", "DUAL_AUTH_REQ");
	
		/* credit_note - The city in which the party is located */
		 attributeMgr.registerAttribute("credit_note", "credit_note", "IndicatorAttribute");
	 
		/* credit_authorise - The city in which the party is located */
		attributeMgr.registerReferenceAttribute("credit_authorise", "credit_authorise", "DUAL_AUTH_REQ"); 

		/* instrument_type - The city in which the party is located */
	//	 attributeMgr.registerAttribute("instrument_type", "instrument_type");//SHR CR-708 Rel8.1.1
		// attributeMgr.registerAlias("instrument_type", getResourceManager().getText("TradingPartnerDetails.InvoicesInstrumentType", TradePortalConstants.TEXT_BUNDLE));

        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 made this a IndicatorAttribute - Start
		/* individual_invoice_allowed - The city in which the party is located */
		 attributeMgr.registerAttribute("individual_invoice_allowed", "individual_invoice_allowed",  "IndicatorAttribute");
        //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 made this a IndicatorAttribute - End

		//SHR CR-708 Rel8.1.1 Start

		/* Set of Payables instrument types */
		attributeMgr.registerAttribute("pay_instrument_type", "pay_instrument_type");
		/* Set of Receivables instrument types */
		attributeMgr.registerAttribute("rec_instrument_type", "rec_instrument_type");

		//SHR CR-708 Rel8.1.1 End


		//IValavala Rel 8.2.0.0 CR-741
		attributeMgr.registerAttribute("payment_gl_code", "payment_gl_code");
		attributeMgr.registerAttribute("over_payment_gl_code", "over_payment_gl_code");
		attributeMgr.registerAttribute("discount_gl_code", "discount_gl_code");

		//IValavala End Rel 8.2.0.0 CR-741

		// DK CR 709 Rel8.2 Start
		attributeMgr.registerAttribute("rec_loan_type", "rec_loan_type");
		attributeMgr.registerAttribute("pay_loan_type", "pay_loan_type");
		// DK CR 709 Rel8.2 End

		//Ravindra - CR-708B - Start
		attributeMgr.registerAttribute("tps_customer_id", "tps_customer_id");
		//Ravindra - CR-708B - End
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
   {

      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* ArBuyerNameAliasList - Each AR Matching Rule has a number of Buyer Name Aliases. */
      registerOneToManyComponent("ArBuyerNameAliasList","ArBuyerNameAliasList");

      /* ArBuyerIdAliasList - Each AR Matching Rule has a number of Buyer ID Aliases. */
      registerOneToManyComponent("ArBuyerIdAliasList","ArBuyerIdAliasList");

	   /* TradePartnerMarginRulesList - Each AR Matching Rule has a number of Trader Matching Rules Aliases. */
      registerOneToManyComponent("TradePartnerMarginRulesList","TradePartnerMarginRulesList");
      
      //Narayan CR913 Rel9.0.0.0 23-Jan-2014 - Start
      /* TradePartnerMarginRulesList - Each AR Matching Rule has a number of Trader Matching Rules Aliases. */
      registerOneToManyComponent("PayableInvPayInstList","PayableInvPayInstList");
     //Narayan CR913 Rel9.0.0.0 23-Jan-2014 - End
   }






}
