
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This is the rule that a corporate customer can set up to match AR payment
 * and invoices.  This rule is sent to OTL.  It is not used in TP otherwise.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ArMatchingRuleHome extends EJBHome
{
   public ArMatchingRule create()
      throws RemoteException, CreateException, AmsException;

   public ArMatchingRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
