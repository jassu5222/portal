
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * An AuditLog record is created whenever a piece of reference data is inserted,
 * updated or deleted.   It is used for reporting purposes to track changes
 * to reference data.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AuditLogBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(AuditLogBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* audit_log_oid - Object Identifier of the AuditLog record */
      attributeMgr.registerAttribute("audit_log_oid", "audit_log_oid", "ObjectIDAttribute");
      
      /* class_name - The type of reference data that was deleted.  The EJB Name(for example:
         User, CorporateOrganization) of the data that was added, deleted, or changed. */
      attributeMgr.registerAttribute("class_name", "class_name");
      
      /* owner_org_oid - A pointer to the organization (CorporateOrganization, BankOrganizationGroup,
         ClientBank, or GlobalOrganization) that owns or owned the reference data
         that this AuditLog record corresponds to. */
      attributeMgr.registerAttribute("owner_org_oid", "owner_org_oid", "NumberAttribute");
      
      /* change_type - Indicates the type of change (Create, Update, Delete) that occured on the
         reference data that the AuditLog record corresponds to. */
      attributeMgr.registerReferenceAttribute("change_type", "change_type", "CHANGE_TYPE");
      
      /* changed_object_oid - The OID of the object that this AuditLog record corresponds to.   The OID
         may not actually correspond to an OID that exists in the database if the
         reference data object has been deleted. */
      attributeMgr.registerAttribute("changed_object_oid", "changed_object_oid", "NumberAttribute");
      
      /* object_description - A description of the object to which this AuditLog record corresponds. 
         This is usually populated with the name attribute of the reference data.
         If no'name' attribute exists, an analogous one is chosen.    Object_description
         must exist so that the corresponding  reference data can still be identified
         after it has been deleted. */
      attributeMgr.registerAttribute("object_description", "object_description");
      
      /* date_time_stamp - The date and time (stored in the database in the GMT time zone) at which
         this record was inserted into the database. */
      attributeMgr.registerAttribute("date_time_stamp", "date_time_stamp", "DateTimeAttribute");
      
      /* change_user_oid - The OID of the user that performed the actioin that resulted in this AuditLog
         record being created.  Since users are never deleted from the database (they
         are deactivated), this can be used to get the name of the user */
      attributeMgr.registerAttribute("change_user_oid", "change_user_oid", "NumberAttribute");
      
      
      // jgadela R 8.3 CR 501
      /* approver_user_oid - The OID of the user that approved the change to reference data that requires dual control.
       *  The action is recorded in this AuditLog */
      attributeMgr.registerAttribute("approver_user_oid", "approver_user_oid", "NumberAttribute");
      
   }
   
   
}
