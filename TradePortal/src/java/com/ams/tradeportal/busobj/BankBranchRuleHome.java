
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Bank/Branch with the Bank/Branch code and address information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface BankBranchRuleHome extends EJBHome
{
   public BankBranchRule create()
      throws RemoteException, CreateException, AmsException;

   public BankBranchRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
