package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;

/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 * Copyright � 2015 CGI, Incorporated All rights reserved
 */
public class BankGrpForRestrictRuleBean_Base extends
		TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(BankGrpForRestrictRuleBean_Base.class);

	/*
	 * Register the attributes and associations of the business object
	 */
	protected void registerAttributes() throws AmsException {

		/* Register attributes defined in the Ancestor class */
		super.registerAttributes();

		/* bank_grp_for_restrict_rls_oid - Unique Identifier */
		attributeMgr.registerAttribute("bank_grp_for_restrict_rls_oid",
				"BANK_GRP_FOR_RESTRICT_RLS_OID", "ObjectIDAttribute");

		/* Pointer to the parent User */
		attributeMgr.registerAttribute("bank_grp_restrict_rules_oid",
				"P_BANK_GRP_RESTRICT_RULES_OID", "ParentIDAttribute");

		/* bank_organization_group_oid - */
		attributeMgr.registerAssociation("bank_organization_group_oid",
				"BANK_ORGANIZATION_GROUP_OID", "BankOrganizationGroup");
	}

}
