package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Business object to store commonly used BankGrpRestrictRule 
 *
 *     Copyright  � 2015                         
 *     CGI, Incorporated 
 *     All rights reserved
 */
public interface BankGrpRestrictRule extends ReferenceData
{   
}
