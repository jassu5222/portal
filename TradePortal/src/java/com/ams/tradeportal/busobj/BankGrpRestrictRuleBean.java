 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;
import java.nio.file.attribute.UserDefinedFileAttributeView;

import javax.ejb.*;

import com.ams.tradeportal.common.*;




/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 *     Copyright  � 2015                         
 *     CGI, Incorporated 
 *     All rights reserved
 */
public class BankGrpRestrictRuleBean extends BankGrpRestrictRuleBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(BankGrpRestrictRuleBean.class);
    public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
    {
    	this.validateName();
    	
    	BusinessObject busobj = null;
    	String bnkGrpOid=null;
    	ComponentList bnkGrpForRestictRlsList = (ComponentList)this.getComponentHandle("BankGrpForRestrictRuleList");
    	int count = bnkGrpForRestictRlsList.getObjectCount();
    	if(count==0){
    		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.ONE_BANK_GROUP_MUST_SELECTED);
    	}

    	Set bnkGrpFrRestrictedRlsset = new HashSet();
    	for (int i = 0; i < count; i++) {
    		bnkGrpForRestictRlsList.scrollToObjectByIndex(i);
    		busobj = bnkGrpForRestictRlsList.getBusinessObject();
    		bnkGrpOid = busobj.getAttribute("bank_organization_group_oid");
    		if(!bnkGrpFrRestrictedRlsset.contains(bnkGrpOid)) {
    			bnkGrpFrRestrictedRlsset.add(bnkGrpOid);
    		}
    		else if(StringFunction.isNotBlank(bnkGrpOid)){
    			BankOrganizationGroup bankOrgGroup = (BankOrganizationGroup)this.createServerEJB("BankOrganizationGroup",Long.parseLong(bnkGrpOid));
    			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.BANK_GROUP_SELECTED_ONCE,
                        bankOrgGroup.getAttribute("name"));
    			
    		}
    	}

    }
    
    public void userDelete() throws RemoteException, AmsException{
    	String bankGrpRestrictRuleOid = this.getAttribute("bank_grp_restrict_rules_oid");
    	List<Object> sqlParams = new ArrayList();
    	sqlParams.add(bankGrpRestrictRuleOid);
    	int usersCount = DatabaseQueryBean.getCount("*", "USERS", " A_BANK_GRP_RESTRICT_RULE_OID in (?) ", false, sqlParams);
    	if(usersCount>0){
    		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.BANK_GROUP_RESTRICT_RULE_NOT_DELETED,
                    this.getAttribute("name"));
    	}
		
    }
    
    /**
	    * This method checks if the Rule Name name is already exist in the db and throws error if duplicated 
	    * IR#T36000041435
	    * @throws java.rmi.RemoteException
	    * @throws com.amsinc.ecsg.frame.AmsException
	    */
	public  void validateName() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
				String name = this.getAttribute("name");
				String where = " and P_CLIENT_BANK_OID= '" + this.getAttribute("client_bank_oid") + "'";
				if (!isUnique("name", name, where)) {
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ALREADY_EXISTS,
						name,
						attributeMgr.getAlias("name"));
				}
				
	 }
	


}
