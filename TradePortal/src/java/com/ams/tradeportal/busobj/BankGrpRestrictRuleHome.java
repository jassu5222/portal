package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 *     Copyright  � 2015                         
 *     CGI, Incorporated 
 *     All rights reserved
 */
public interface BankGrpRestrictRuleHome extends EJBHome
{
   public BankGrpRestrictRule create()
      throws RemoteException, CreateException, AmsException;

   public BankGrpRestrictRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
