package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *     Copyright  � 2001                       
 *     CGI, Incorporated 
 *     All rights reserved
 */
public class BankMailMessageBean extends BankMailMessageBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(BankMailMessageBean.class);


	/**
	 * Attempts to set the related_instrument_oid attribute.  Performs
	 * validations and possibly issues errors.
	 *
	 * @param completeInstrumentId - the complete instrument ID of the instrument to which the message is being associated
	 * @param ownerOrgOid - the organization that owns the message
	 */
	public void setRelatedInstrument(String completeInstrumentId, String ownerOrgOid) throws RemoteException, AmsException
	{
		//Always check the passed in Complete Instrument ID is in the DB and that it's there for the
		//Current users Corporate Org.  We need to set the Related instrument Oid which we cant count
		//on being passed in, and it may not always be a reliable oid if the users changes the complete
		//Instrument Id in the editable text field. 
		// The instrument must exist for the user's corporate org, or for a corporate org related to it
		// (children, parents, siblings, etc)      
		if( !StringFunction.isBlank(completeInstrumentId) ) 
		{
			StringBuffer sql = new StringBuffer(); 
			sql.append("select instrument_oid from instrument ");
			sql.append("where complete_instrument_id = ?");
			sql.append(" AND a_corp_org_oid in ( " );

			// The inner query retrieves all corporate orgs related to the user's 
			// corporate org.   In the inner part of this query, it walks up the 
			// tree to determine the highest corporate org in the hierarchy.
			// The outer part of this query retrieves the highest point and all of 
			// its children.  The final result is all of the orgs in the hierarchy.
			StringBuffer innerQuery = new StringBuffer();
			innerQuery.append("select organization_oid ");
			innerQuery.append("from corporate_org ");
			innerQuery.append("start with organization_oid = ");
			innerQuery.append("  (select organization_oid ");
			innerQuery.append("   from corporate_org ");
			innerQuery.append("   where p_parent_corp_org_oid is null ");
			innerQuery.append("   start with organization_oid =  ? ");
			innerQuery.append("   connect by organization_oid = prior p_parent_corp_org_oid) ");
			innerQuery.append("connect by prior organization_oid = p_parent_corp_org_oid ");

			sql.append(innerQuery.toString());
			sql.append(" ) ");


			DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, completeInstrumentId, ownerOrgOid);

			if(null == results){
				getErrorManager ().issueError ( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ENTER_VALID_INSTRUMENT);
			}else{
				if(null != results.getFragments("/ResultSetRecord/")){
					if(results.getFragments("/ResultSetRecord/").size() == 0){
						getErrorManager ().issueError ( TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.ENTER_VALID_INSTRUMENT);
					}
				}
			}
			
			//There is no attribute called related_instrument_oid in bank mail message.

			/*	Vector resultsVector = results.getFragments("/ResultSetRecord/");

				if( ((DocumentHandler)resultsVector.elementAt(0)) != null )
				{
					DocumentHandler myDoc = ((DocumentHandler)resultsVector.elementAt(0));
					this.setAttribute("related_instrument_oid", myDoc.getAttribute("/INSTRUMENT_OID"));
				}
				else
				{
					this.setAttribute("related_instrument_oid", ""); 
					getErrorManager ().issueError (TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ENTER_VALID_INSTRUMENT);
				}
			}
			else
			{
				this.setAttribute("related_instrument_oid", "");
				getErrorManager ().issueError ( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ENTER_VALID_INSTRUMENT);
			}*/
		}
		
	}


	/** Given the securityRights and userOid who initiates the deletion, 
	 * this method checks the security, deletes the message from the database
	 * or sets the message status to deleted.
	 * 
	 * @param  String securityRights  	- the security rights of the user initiating the delete
	 * @param  String userOid  		- user who initiates the delete
	 * @return boolean		if deletion is successful
	 */
	public boolean deleteMessage(String securityRights, String userOid)
			throws AmsException, RemoteException

	{


		String messageSource = null;
		String messageStatus = null;

		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.BANK_MAIL_DELETE)) {
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
					getAttribute("message_subject"),	
					getResourceManager().getText("MessageAction.Delete",
							TradePortalConstants.TEXT_BUNDLE));
			return false;
		}

		messageSource = getAttribute("message_source_type");
		messageStatus = getAttribute("message_status");

		// if the message has previously been deleted, issue error
		// that it's been deleted
		if (messageStatus.equals(TradePortalConstants.SENT_TO_CUST_DELETED) ||
				messageStatus.equals(TradePortalConstants.REC_DELETED)) {
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ITEM_DELETED);
			return false;
		}

		// for Outgoing messages
		if (messageSource.equals(TradePortalConstants.BANK)) {
			// message originates from here (Outgoing)
			// if has been sent, marked as Sent and Deleted
			// otherwise, delete the message from the database.
			if (messageStatus.equals(TradePortalConstants.DRAFT) ||
					messageStatus.equals(TradePortalConstants.SENT_ASSIGNED) || messageStatus.equals(TradePortalConstants.SENT_TO_CUST)) {
				this.delete();
				// if delete is unsuccessful, errors are issued - if there are
				// errors, return false, otherwise return true
				if (this.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY){
					return false;
				} else {
					return true;
				}
			} else {
				setAttribute("message_status", TradePortalConstants.SENT_TO_CUST_DELETED);
				setAttribute("last_update_date", DateTimeUtility.getGMTDateTime());

				this.save();
				if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY){
					return false;
				} else {
					return true;
				}
			}
		} else {
			// message is sent from BANK (Incoming)
			setAttribute("message_status", TradePortalConstants.REC_DELETED);
			this.save();
			if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY){
				return false;
			} else {
				return true;
			}
		}
	}  

	/**
	 *  This is a convience method that provides an easy way to 
	 *  determine if the message object has a status of 'deleted'.
	 *  
	 *  @return boolean     - Y/N it has been flagged as deleted.
	 *  @throws AmsException
	 *  @throws RemoteException

	 */

	public boolean isDeleted () throws AmsException, RemoteException
	{
		try{
			String messageStatus = this.getAttribute("message_status");   
			if( messageStatus.equals(TradePortalConstants.SENT_TO_CUST_DELETED) ||
					messageStatus.equals(TradePortalConstants.NOT_SENT_DELETED) ||
					messageStatus.equals(TradePortalConstants.REC_DELETED) )
				return true;

		}catch(NullPointerException e) {
			return false;
		}
		return false;
	}


}
