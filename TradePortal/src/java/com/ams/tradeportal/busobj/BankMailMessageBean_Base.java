
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Business object to store a message or discrepancy notice.   These messages
 * can be created in the back end system and placed into this table by the
 * middleware or they can be created by portal users and sent to the back end
 * system via the middleware.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankMailMessageBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(BankMailMessageBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* message_oid - Unique identifier */
      attributeMgr.registerAttribute("bank_mail_message_oid", "bank_mail_message_oid", "ObjectIDAttribute");
      
      /* message_text - Text that comprises the body of the message */
      attributeMgr.registerAttribute("message_text", "message_text");
      
      /* message_subject - Text that comprises the subject of the mail message */
      attributeMgr.registerAttribute("message_subject", "message_subject");
      attributeMgr.requiredAttribute("message_subject");
      attributeMgr.registerAlias("message_subject", getResourceManager().getText("MailMessageBeanAlias.message_subject", TradePortalConstants.TEXT_BUNDLE));
      
          
      /* message_source_type - Indicates whether the message was created on the back end system or created
         on the Trade Portal */
      attributeMgr.registerReferenceAttribute("message_source_type", "message_source_type", "MESSAGE_SOURCE");
      attributeMgr.requiredAttribute("message_source_type");
      
      /* message_status - The status in the lifecycle of a mail message.   Statuses can include Received,
         Draft, Sent to Bank, etc. */
      attributeMgr.registerReferenceAttribute("message_status", "message_status", "MESSAGE_STATUS");
      attributeMgr.requiredAttribute("message_status");
      
      /* unread_flag - Indicates whether or not the message has been read by its intended recipient.
         Yes = message has not been read */
      attributeMgr.registerAttribute("unread_flag", "unread_flag", "IndicatorAttribute");
      attributeMgr.requiredAttribute("unread_flag");
      
      /* last_update_date - Timestamp in GMT of when any part of this message (including status) was
         updated. */
      attributeMgr.registerAttribute("last_update_date", "last_update_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("last_update_date");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      
          
      /* is_reply - Inidcates whether or not this message is a reply to another message.   A
         Trade Portal user can create a message by pressing the 'Reply' button. 
         If this is done, this flag is set.  Yes = the message is a reply. */
      attributeMgr.registerAttribute("is_reply", "is_reply", "IndicatorAttribute");
      attributeMgr.requiredAttribute("is_reply");
      
       
      /* complete_instrument_id - The ID of the related instrument.  Used only if the mail message is received
         before its related instrument is created.  That could happen if the messages
         get out of order in the middleware.  When the UNV message later comes in
         and creates the instrument, proper a_related_instrument_oid will be set. */
      attributeMgr.registerAttribute("complete_instrument_id", "complete_instrument_id");
      
           
      /* assigned_to_corp_org_oid - The corporate organization to which a mail message is assigned.    This
         will never be empty.  When a mail message is assigned to a user, it is also
         still assigned to the corporate organization. */
      attributeMgr.registerAssociation("assigned_to_corp_org_oid", "a_assigned_to_corp_org_oid", "CorporateOrganization");
      
          /* assigned_to_user_oid - The user to which a mail message is currently assigned. */
      attributeMgr.registerAssociation("assigned_to_user_oid", "a_assigned_to_user_oid", "User");
      
    
      
      attributeMgr.registerAttribute("bank_instrument_id","bank_instrument_id");
   }
   
}
