
package com.ams.tradeportal.busobj;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.SupervisorCommandUtility;
import com.ams.tradeportal.common.EmailValidator;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;




/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class BankOrganizationGroupBean extends BankOrganizationGroupBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(BankOrganizationGroupBean.class);

  /**
   * Descendent implemented method that identifies listing object
   * criteria.
   *
   * Used to identify one or more listTypes and list processing criteria.
   *
   * @param listTypeName The name of the listtype (as specified by the
   * developer)
   * @return The qsuedo SQL used for specifying the list criteria.
   *
   * Acceptable listTypes are:
   * byActiveClientBank
   */
  public String getListCriteria(String type)
  {

     if (type.equals("byActiveClientBank"))
      {

	  return "p_client_bank_oid = {0} and activation_status = {1}";

      }
	  // CR-581/640 Rel. 7.1.0 - 09/21/11 - Begin- 
	  else if (type.equals("activeByBankOrgGrp"))
		{

			return "a_cross_rate_calc_oid = {0}";

		}
	  // CR-581/640 Rel. 7.1.0 - 09/21/11 - End -	
      return "";

  }

  /**
   * Performs any special "validate" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method verifies that bank group name is unique
   * for the Client Bank.
   *
   * @see #validate()
   */

  protected void userValidate() throws RemoteException, AmsException
  {

      // verify that name is unique
      String name = getAttribute("name");
      if (!isUnique("name", name, " and p_client_bank_oid = " +
      getAttribute("client_bank_oid")))
      {
	    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	           TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
      }

	// The support information text is limited to 2000 characters.
	String supportInfo = getAttribute("support_information");

	if (supportInfo.length() > 2000) {
		String endText = supportInfo.substring(1980, 2000);
		this.getErrorManager().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					attributeMgr.getAlias("support_information"),
					endText);
	}
	// AAlubala - IR#AAUL08462630 and IR#NEUL012137189 - Rel8.0 - 09/21/11 - Start 
	//Validate Email Address(es)
	this.performEmailValidation(this.getAttribute("sender_email_address"));	
	this.performEmailValidation(this.getAttribute("sender_email_address_2"));

      // Verify that email language attributes contain different values
      String language1 = this.getAttribute("email_language");
      String language2 = this.getAttribute("email_language_2");

      if (language1.equals(language2))
      {
        if (StringFunction.isBlank(language1) && StringFunction.isBlank(language2))
        {
          this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                            TradePortalConstants.EMAIL_LANGUAGE_REQUIRED);
        }
        else
        {
          String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",
					                                      language1,
					                                      getResourceManager().getResourceLocale());

          this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                            TradePortalConstants.ALREADY_EXISTS,
                                            languageDesc,
                                            attributeMgr.getAlias("email_language"));
        }
      }

      // If an email language has been specified require the user to enter at least one email detail
      String senderName;
      String senderEmailAddress;
      String systemName;
      String bankUrl;
      String incldBnkURL;

      if (!StringFunction.isBlank(language1))
      {
        senderName         = this.getAttribute("sender_name");
        senderEmailAddress = this.getAttribute("sender_email_address");
        systemName         = this.getAttribute("system_name");
        bankUrl            = this.getAttribute("bank_url");
        incldBnkURL            = this.getAttribute("incld_bnk_url_trade");

        if (StringFunction.isBlank(senderName)         &&
            StringFunction.isBlank(senderEmailAddress) &&
            StringFunction.isBlank(systemName)         &&
            StringFunction.isBlank(bankUrl))
        {
          this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                            TradePortalConstants.EMAIL_DETAILS_REQUIRED,
                                            "1");
        }
        /*PGedupudi Rel 9.5 CR-1123*/
        if(TradePortalConstants.INDICATOR_YES.equals(incldBnkURL) && StringFunction.isBlank(bankUrl)){
        	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.URL_REQD_TRD);
        	
        }
      }

      if (!StringFunction.isBlank(language2))
      {
        senderName         = this.getAttribute("sender_name_2");
        senderEmailAddress = this.getAttribute("sender_email_address_2");
        systemName         = this.getAttribute("system_name_2");
        bankUrl            = this.getAttribute("bank_url_2");

        if (StringFunction.isBlank(senderName)         &&
            StringFunction.isBlank(senderEmailAddress) &&
            StringFunction.isBlank(systemName)         &&
            StringFunction.isBlank(bankUrl))
        {
          this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                            TradePortalConstants.EMAIL_DETAILS_REQUIRED,
                                            "2");
        }
      }

      //DK CR-640 Rel7.1 BEGINS
      String fxOnlineAvailable = this.getAttribute("fx_online_avail_ind");
      String defaultFxOnlineAcctId = this.getAttribute("deflt_fx_online_acct_id");
      String domainOrgUrl = this.getAttribute("domain_org_url");

      if(TradePortalConstants.INDICATOR_YES.equals(fxOnlineAvailable)){
    	  if(StringFunction.isBlank(defaultFxOnlineAcctId) &&  StringFunction.isBlank(domainOrgUrl)){
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.DEFAULT_FX_ONLINE_ACCT_ID_REQUIRED);
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.DOMAIN_ORG_URL_REQUIRED);
    	  }
    	  else if(StringFunction.isBlank(defaultFxOnlineAcctId)){
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.DEFAULT_FX_ONLINE_ACCT_ID_REQUIRED);
    	  }else if(StringFunction.isBlank(domainOrgUrl)){
    		  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.DOMAIN_ORG_URL_REQUIRED);
    	  }

      }

      //DK CR-640 Rel7.1 ENDS
     
    //SSikhakolli - Rel 9.3.5 CR 1029 - Begin
  	//If "Enable Update center or Enable Session Sync HTML is enabled at any Bank Group's under this Client Bank, we should not allow to off the Enable Admin Update Center setting
  	String newAdminUpdateCenterVal  = this.getAttribute("enable_admin_update_centre");
  	String oldAdminUpdateCenterVal = DatabaseQueryBean.getObjectID("BANK_ORGANIZATION_GROUP", "ENABLE_ADMIN_UPDATE_CENTRE" ,"organization_oid = ? ", new Object[]{this.getAttribute("organization_oid")});
  	
  	if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(newAdminUpdateCenterVal) && 
  			TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(oldAdminUpdateCenterVal)) { 
  		int count = DatabaseQueryBean.getCount( "ORGANIZATION_OID", "corporate_org", 
  				"A_BANK_ORG_GROUP_OID = ? and CUST_IS_NOT_INTG_TPS  = ? and ACTIVATION_STATUS = ?", 
  				true, new Object[]{this.getAttribute("organization_oid"), TradePortalConstants.INDICATOR_YES, TradePortalConstants.ACTIVE});
  		if(count > 0) {
  			this.getErrorManager().issueError(
  				TradePortalConstants.ERR_CAT_1,
  				TradePortalConstants.CANNOT_UNCHECK_ENABLE_ADMIN_UPDATE_CENTRE,
  				this.getResourceManager().getText("BankGroups.EnableAdministrativeUpdateCentre", TradePortalConstants.TEXT_BUNDLE));
  	    }
  	}
  	//SSikhakolli - Rel 9.3.5 CR 1029 - End
  	
  	/*PGedupudi Rel 9.5 CR 1123 validations*/
  	String incldBnkUrlPmntBen = this.getAttribute("incld_bnk_url_pmnt_ben");
    String incldBnkUrlReprPanel = this.getAttribute("incld_bnk_url_repr_panel");
    String incldBnkUrlPayableMgmt = this.getAttribute("incld_bnk_url_payable_mgmt");
    String beneBankUrl = this.getAttribute("bene_bank_url");
    String repairBankUrl = this.getAttribute("repair_bank_url");
    String payableBankUrl = this.getAttribute("payable_bank_url");
    
    if(TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlPmntBen) && StringFunction.isBlank(beneBankUrl)){
    	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.URL_REQD_PMNT_BEN);
    	
    }
    if(TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlReprPanel) && StringFunction.isBlank(repairBankUrl)){
    	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.URL_REQD_REPR_PANEL);
    	
    }
    if(TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlPayableMgmt) && StringFunction.isBlank(payableBankUrl)){
    	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.URL_REQD_PAYABLE_MGMT);
    	
    }
    
     
  }

  /**
   * This method is called in the business object
   * method delete.
   *
   * Any processing that is necessary before an object is deleted should
   * be placed within this method.  If business object errors are issued
   * within this method, delete processing will not continue and the
   * delete transaction will be rolled back.
   *
   * If the Bank Group has any active operational bank organizations,
   * active corporate customers or active users, issue error and
   * do not delete
   *
   * @see #delete()
   */
  protected void userDeactivate() throws RemoteException, AmsException
  {
	// check if there any active operational bank organizations


        if (isAssociated("OperationalBankOrganization", "activeByBankOrgOp",
		new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE }))
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.ACTIVE_OP_BANK_ORGS, getAttribute("name") );

	// check if there are any active corporate customers

	if (isAssociated("CorporateOrganization", "activeByBankOrgOp",
	        new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE}))
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.ACTIVE_CORP_CUSTOMERS, getAttribute("name"));


        // check if there are any active users

	if (isAssociated("User", "activeByBankOrgOp",
		new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE}))
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.ACTIVE_USERS, getAttribute("name"));

  }

  /**
   * Performs any special "save" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method is optinally coded in descendant objects as desired.
   * Any pre-save processing should be coded within this method on
   * descendants of BusinessObject. An example of this would be the
   * setting of a last_updated attribute on the business object.
   *

   * @see #save()
   */
  protected void preSave ()
            throws AmsException
  {
	if (isNew)
	{
     	    // copy the Client Bank's branding prefix to this new bank org group
     	    //Following code inserts rows in SUPERVISOR_COMMAND table to create users in
     	    //Supervisor module when a new BOG is created


	    try
	    {

            SupervisorCommand   supCommand1         = null;
	        String              orgOID             = null;
	        String              pclientbankOID    = null;

	        supCommand1 = (SupervisorCommand) createServerEJB("SupervisorCommand");
	        supCommand1.newObject();
            

 	    //rbhaduri - 10th Jul 08 - Modify Begin - M And T Bank Change - get reporting
            //suffix from client bank table instead of global organization table

	    // Get the reporting user suffix from the Global Org to which this org belongs
           

            String reportingUserSuffixSql = "select c.reporting_user_suffix from client_bank c where c.organization_oid = ?";
                  
	    //rbhaduri - 10th Jul 08 - Modify End
          //LSuresh R91 IR T36000026319 - SQLINJECTION FIX
            DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false,new Object[]{this.getAttribute("client_bank_oid")});

            String reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");

            // Place the reporting user suffix on to the end of the OID
            // the suffix is used to distinguish between organizations being processed
            // by different ASPs that might be on the same reports server.
            
            orgOID = getAttribute("organization_oid")+reportingUserSuffix;
            pclientbankOID = getAttribute("client_bank_oid")+reportingUserSuffix;


            //Insert the Supervisor command
            //Please refer to Supervisor admin guide for detail description
            supCommand1.setAttribute("command", SupervisorCommandUtility.getBankGroupFirstCommand(pclientbankOID, orgOID));
            supCommand1.save();

            

	    }
	    catch (RemoteException e)
	    {
		    throw new AmsException(e.getMessage());
	    }
	}

  }
  
	// AAlubala - IR#AAUL08462630 and IR#NEUL012137189 - Rel8.0 - 09/21/11 - Start 
	// Validate Email Address Format
	/**
	 *  Validatesemail address
	 * @throws AmsException 
	 * @throws RemoteException 
	 */
	protected void performEmailValidation(String emailList) throws RemoteException, AmsException {
		if (StringFunction.isBlank(emailList)) return;
		
	    if (!EmailValidator.validateEmailList(emailList))	{
	    	getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_EMAIL,
					"");
	    }
	}


}
