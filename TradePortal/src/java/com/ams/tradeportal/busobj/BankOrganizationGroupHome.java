
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A Bank Organization Group (BOG) represents a grouping of corporate customers
 * underneath a Client Bank.    Each BOG has its own branding directory and
 * can own reference data.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface BankOrganizationGroupHome extends EJBHome
{
   public BankOrganizationGroup create()
      throws RemoteException, CreateException, AmsException;

   public BankOrganizationGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
