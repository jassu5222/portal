
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;


/*
 * Details for the various actions performed on the Cash Management transactions
 * by a user
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankTransactionHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(BankTransactionHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* bank_transaction_history_oid - Unique Identifier */
      attributeMgr.registerAttribute("bank_transaction_history_oid", "bank_transaction_history_oid", "ObjectIDAttribute");
      
      /* action_datetime - Timestamp (in GMT) when the action is taken. */
      attributeMgr.registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");
      
      /* action - Action. */
      attributeMgr.registerReferenceAttribute("action", "action", "BANK_ADMIN_UPDATE_ACTION");
      
      /* bank_update_status - Lifecycle status of a transaction */
      attributeMgr.registerReferenceAttribute("bank_update_status", "bank_update_status", "BANK_TRANS_H_UPDATE_STATUS");
      
        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("bank_update_transaction_oid", "p_bank_update_transaction_oid", "ParentIDAttribute");
   
      /* user_oid - The user who the updates the transaction. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /* repair_reason - repair reason to sedn transaction for repair. */
      attributeMgr.registerAttribute("repair_reason", "repair_reason");
      
      /*bank_transaction_status - REFDATA table Type - BANK_TRANSACTION_STATUS*/
      attributeMgr.registerReferenceAttribute("bank_transaction_status", "bank_transaction_status", "BANK_TRANSACTION_STATUS");
      
   }
   
 
   
 
 
   
}
