package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * 
 */
public class BankTransactionUpdateBean extends BankTransactionUpdateBean_Base{
private static final Logger LOG = LoggerFactory.getLogger(BankTransactionUpdateBean.class);

	public int save (boolean shouldValidate) throws RemoteException, AmsException
    {
		int val = super.save(shouldValidate);
		if (val == 1) { 
			val = postSave();
		}
		return val;
    }

	private int postSave() throws RemoteException, AmsException {
	
	    String action = this.getClientServerDataBridge().getCSDBValue(TradePortalConstants.TRAN_ACTION);
		BankTransactionHistory bankTranHistory = (BankTransactionHistory) createServerEJB("BankTransactionHistory");
		bankTranHistory.newObject();
		bankTranHistory.setAttribute( "action_datetime", DateTimeUtility.getGMTDateTime() );
		bankTranHistory.setAttribute( "action", action );
		bankTranHistory.setAttribute( "user_oid", this.getAttribute("bank_user_oid") );
		bankTranHistory.setAttribute( "bank_transaction_status", this.getAttribute("bank_transaction_status") );
		if ( TradePortalConstants.BANK_ADMIN_ACTION_APPROVE.equals(action) ) {
		    if ( TransactionStatus.PROCESSED_BY_BANK.equals(this.getAttribute("bank_transaction_status")) ) {
			    bankTranHistory.setAttribute( "bank_update_status", TradePortalConstants.BANK_TRANS_H_UPDATE_STATUS_APPROVED_PBB );
		    } else {
			    bankTranHistory.setAttribute( "bank_update_status", TradePortalConstants.BANK_TRANS_H_UPDATE_STATUS_APPROVED_RBB );
		    }
		} else {
			bankTranHistory.setAttribute( "bank_update_status", this.getAttribute("bank_update_status") );
		}
		bankTranHistory.setAttribute( "bank_update_transaction_oid", this.getAttribute("bank_transaction_update_oid") );
		if(TradePortalConstants.BANK_ADMIN_ACTION_SENT_FOR_REPAIR.equals(action)){
			String repairReason = this.getClientServerDataBridge().getCSDBValue("RepairReason");
			if(StringFunction.isNotBlank(repairReason)){
				bankTranHistory.setAttribute("repair_reason", repairReason);
			}			
		}
		int success = bankTranHistory.save(false);
		if ( success!= 1 ) {
			LOG.info("Error occured while creating Bank Transaction History Log...");
		}
		return success;
	}
	
}
