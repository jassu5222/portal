package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A client bank that has chosen to use the Trade Portal for their customers'
 * trade processing.   
 * 
 * Each bank has a separate instrument number (ID) range.  When instruments
 * are created, this range is used to determine the valid range of instrument
 * numbers.
 * 
 * Information about instrument ID ranges, security, branding, and default
 * templates are stored in a ClientBank record.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ClientBankHome extends EJBHome
{
   public ClientBank create()
      throws RemoteException, CreateException, AmsException;

   public ClientBank create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
