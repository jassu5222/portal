
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The configuration of the application.  These configuration entries used
 * to reside on the properties files.  They are moved to database for easier
 * management.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ConfigSettingBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ConfigSettingBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* application_name - The application that this configuration entry is for.  The value could be:
         PORTAL_APP - Portal application on Weblogic
         PORTAL_AGENT - Portal Agent */
      attributeMgr.registerReferenceAttribute("application_name", "application_name", "CONFIG_APPLICATION_NAME");
      attributeMgr.requiredAttribute("application_name");
      
      /* class_name - The class name that this config_setting entry is for.  This is corresponding
         to the name of the properties file where the configuration used to be stored.
         The values are:
         PORTAL - ConfigSetting used by TradePortal code.  Used to be stored in TradePortal.properties
         AGENT - ConfigSetting used by Agent code.  Used to be stored in AgentConfigurations.properties.
         
         Note ConfigSetting currently do not support the configuration used by JPylon.
         Since ConfigSetting implementation depends on a lot of code only in TradePortal,
         it is not implemented in JPylon and therefore cannot be used in JPylon.
 */
      attributeMgr.registerReferenceAttribute("class_name", "class_name", "CONFIG_CLASS_NAME");
      attributeMgr.requiredAttribute("class_name");
      
      /* setting_name - The configuration setting name. */
      attributeMgr.registerAttribute("setting_name", "setting_name");
      attributeMgr.requiredAttribute("setting_name");
      
      /* setting_value - Config setting value. */
      attributeMgr.registerAttribute("setting_value", "setting_value");
      
      /* description - The description of the config_setting entry. */
      attributeMgr.registerAttribute("description", "description");
      
   }
   
 
   
 
 
   
}
