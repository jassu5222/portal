
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The configuration of the application.  These configuration entries used
 * to reside on the properties files.  They are moved to database for easier
 * management.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ConfigSettingHome extends EJBHome
{
   public ConfigSetting create()
      throws RemoteException, CreateException, AmsException;

   public ConfigSetting create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
