

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;


/**
 * This business object represents a business organization that is a customer
 * of the client banks.   Corporate organizations apply for instruments and,
 * by authorizing them, send them to the bank.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CorporateOrganization extends ReferenceDataOwner
{   
	public String getDualAuthIndicator(java.lang.String instrumentType, java.lang.String transactionType) throws RemoteException, AmsException;

	public String getNotificationRuleSettingOrFreq(java.lang.String notifRuleOid, java.lang.String instrumentCategory, java.lang.String transactionType, java.lang.String criterionType) throws RemoteException, AmsException;

	public String getNotificationRuleAttribute(java.lang.String attributeName) throws RemoteException, AmsException;

	public void createEmailMessage(java.lang.String instrumentOid, java.lang.String transactionOid, java.lang.String discrepancyCurrency, java.lang.String discrepancyAmount, java.lang.String emailType) throws RemoteException, AmsException;

	public void touch() throws RemoteException, AmsException;
	
	public boolean isInstrumentTypeEnabled(java.lang.String instrumentType) throws RemoteException, AmsException;    // NSX 28/07/10 - PYUK071981074
	
    public boolean isStraightThroughAuthorize() throws RemoteException, AmsException;
    
    public boolean isPanelAuthEnabled(java.lang.String instrumentType) throws RemoteException, AmsException;
    
    public boolean isPanelAuthEnabled(java.lang.String instrumentType, java.lang.String transactionType) throws RemoteException, AmsException;// Nar IR-T36000019930
    
    public boolean isCustNotIntegratedWihTPS() throws RemoteException, AmsException; // Nar CR-1029 06/11/2015
    
    public String fetchEmailAddresses(String instrumentType, String transactionType, String notifyRuleOid) throws RemoteException, AmsException;//Rel9.5 CR-927B
    
    public String fetchNotificationRule() throws RemoteException, AmsException;//Rel9.5 CR-927B
    
}
