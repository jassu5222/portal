package com.ams.tradeportal.busobj;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.busobj.util.SupervisorCommandUtility;
import com.ams.tradeportal.common.EmailValidator;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.GenericList;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.StringService;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CorporateOrganizationBean extends CorporateOrganizationBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrganizationBean.class);

	/**
	 *
	 *
	 * @param  instrumentType
	 * @param  transactionType
	 * @return String
	 */
	
	public String getDualAuthIndicator(String instrumentType, String transactionType)
		throws RemoteException, AmsException {
		// Check to see if the transactionType is a Discrepancy...
                             //Krishna IR-RIUH101733973 10/29/2007
		//Inserted '||transactionType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE)'
		if (transactionType.equals(TransactionType.DISCREPANCY)||transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE)) {
			// Check to see if the discrepancy dual authentication is not Instrument Defaults and not Blank
			String discDualAuthValue = attributeMgr.getAttributeValue("dual_auth_disc_response");
			if (!(discDualAuthValue.equals(TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS)) &&
				!(discDualAuthValue.equals(""))) {
				// Return discrepancy Authorization Value and override instrument authorization defaults
				return discDualAuthValue;
			}
		} 
		// CR-818 Adding condition for SIM and SIT transaction
		if ( transactionType.equals(TransactionType.SIM)||transactionType.equals(TransactionType.SIR) ) {
			String discDualAuthValue = attributeMgr.getAttributeValue("dual_settle_instruction");
			if ( StringFunction.isNotBlank(discDualAuthValue) && 
					!TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(discDualAuthValue) ) {
				return discDualAuthValue;
			}
		}
		
		// Check transaction's instrument authorization requirements
		if (instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || 
				instrumentType.equals(InstrumentType.DEFERRED_PAY) ) {
			return attributeMgr.getAttributeValue("dual_auth_import_DLC");
		} else if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			return attributeMgr.getAttributeValue("dual_auth_SLC");
		} else if (instrumentType.equals(InstrumentType.EXPORT_DLC)) {
			return attributeMgr.getAttributeValue("dual_auth_export_LC");
		} else if (instrumentType.equals(InstrumentType.GUARANTEE)) {
			return attributeMgr.getAttributeValue("dual_auth_guarantee");
		} else if (instrumentType.equals(InstrumentType.AIR_WAYBILL)) {
			return attributeMgr.getAttributeValue("dual_auth_airway_bill");
		} else if (instrumentType.equals(InstrumentType.SHIP_GUAR)) {
			return attributeMgr.getAttributeValue("dual_auth_shipping_guar");
		} else if (instrumentType.equals(InstrumentType.FUNDS_XFER)) {
			return attributeMgr.getAttributeValue("dual_auth_funds_transfer");
		} else if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
			return attributeMgr.getAttributeValue("dual_auth_loan_request");
		} else if (instrumentType.equals(InstrumentType.EXPORT_COL)) {
			return attributeMgr.getAttributeValue("dual_auth_export_coll");
		} else if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) {
			return attributeMgr.getAttributeValue("dual_auth_new_export_coll");
		} else if (instrumentType.equals(InstrumentType.REQUEST_ADVISE)) {
			return attributeMgr.getAttributeValue("dual_auth_request_advise");
		} else if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS)) {
			return attributeMgr.getAttributeValue("dual_xfer_btwn_accts");
		} else if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) {
			return attributeMgr.getAttributeValue("dual_auth_direct_debit");
		} else if (instrumentType.equals(InstrumentType.DOM_PMT)) {
			return attributeMgr.getAttributeValue("dual_domestic_payments");
        } else if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			return attributeMgr.getAttributeValue("dual_auth_approval_to_pay");
		} else if (instrumentType.equals(InstrumentType.IMPORT_COL)) {
			return attributeMgr.getAttributeValue("dual_imp_col");
		} else
			return "";
	}

	/**
	 * Descendent implemented method that identifies listing object
	 * criteria.
	 *
	 * Used to identify one or more listTypes and list processing criteria.
	 *
	 * @param listTypeName The name of the listtype (as specified by the
	 * developer)
	 * @return The qsuedo SQL used for specifying the list criteria.
	 *
	 * Acceptable listTypes are:
	 * activeByBankOrgOp - search active corporate organizations
	 * 			 with the given bank organizaion group oid
	 * activeByParentCorporateOrg - search for active corporate orgs that
	 *             have a parent corporate org matching the oid passed in
	 *
	 */
	public String getListCriteria(String type) {
		if (type.equals("activeByBankOrgOp")) {
			return "a_bank_org_group_oid = {0} and activation_status = {1} ";
		} else if (type.equals("activeByParentCorporateOrg")) {
			return "p_parent_corp_org_oid = {0} and activation_status = {1} ";
		} else if (
			type.equals(
				"activeOpBankOrgAssoc")) //called from OperationalBankOrgBean
			{
			return "activation_status = {1} and (a_first_op_bank_org = {0} or a_second_op_bank_org = {0} or "
				+ "a_third_op_bank_org = {0} or a_fourth_op_bank_org = {0})";
		} else if (type.equals("byNotificationRule")) {
			return "a_notify_rule_oid = {0} and activation_status = {1}";
		} else if (type.equals("byEmailReceiver")) {
			return "a_notify_rule_oid = {0} and activation_status = {1} and (email_receiver is null or email_receiver = '''')";
		}

		return "";

	}

	/**
	 * Performs any special "validate" processing that is specific to the
	 * descendant of BusinessObject. Specifically, it checks to see whether
	 * or not the name entered for a corporate organization is unique.
	 *
	 * @see #validate()
	 */

	protected void userValidate() throws RemoteException, AmsException {
		
		String myOrgProfileValidation = this.getAttribute("myorg_profile_validation");
		/*MEer Rel 8.3 IR-T36000022048 fixing the validation issue */
		/*When request comes from My Organization profile page then skip the below user validations which needs 
		 * to be done on Corporate Customer profile  */
		if (StringFunction.isNotBlank(myOrgProfileValidation)){
			return ;
		}
		
	
		
		this.validateName();

		this.validateBankOrgGroupOid();

		this.validateParentCorporation();

		this.validateInstrumentCapabilities();

		//this.validateEmailSettings();//Rel9.5 CR-927B Removing the email receiver from Customer profile. Hence commenting the validations
		
		this.validatePayableCreditNote();
		
		this.validatePaymentsAuthorizationMethodSelection();	
		this.validateFundsTransferDailyLimits();
		this.validateTimeZone();
		this.validateFXRateGroup(); 
		this.validateBaseCurrency(); 

		this.validatePaymentInstCapabilities();
		// AAlubala - IR#AAUL08462630 and IR#NEUL012137189 - Rel8.0 - 09/21/11 - Start
		//Validate Email Address(es) Format
		//this.performEmailValidation(getAttribute("email_receiver"));//Rel9.5 CR-927B Removing the email receiver from Customer profile. Hence commenting the validations

		this.validateCustomerAlias(); // DK CR-587 Rel7.1
        // When a customer is allowed to process Loan Requests or Funds Transfer an account is
        // required on the transaction.  Therefore if this corp customer is allowed to process
        // either of these transactions check if any accounts have been entered
        if (this.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES) ||
        	this.getAttribute("allow_loan_request").equals(TradePortalConstants.INDICATOR_YES) ||
            this.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES) ||
            this.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES) ||		//IAZ CM-453 04/14/09 CNG
            panelsEnabled()) {																				//IAZ CM-453 04/14/09 ADD
            ComponentList accountList = (ComponentList)this.getComponentHandle("AccountList");
            if (accountList.getObjectCount() == 0) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ACCOUNTS_NOT_ENTERED);
            } else {
				BusinessObject busObj = null;
				int accountTot = accountList.getObjectCount();
				Hashtable accountNumberCurHash = new Hashtable();	//IAZ CM-451 04/07/09 Add
				// [START] PDUK010441815
				boolean availForDirectDebitChkbox = false;
				boolean availForLoanReqChkbox = false;
				boolean availForIPChkbox = false;
				boolean availForTBAChkbox = false;
				boolean availForPaymentChkbox = false;
				// [END] PDUK010441815
				//
				//CR610 Rel7.0.0.0 AAlubala - get overall user profile fx rate group
				//
				String overallFxRateGrp = this.getAttribute("fx_rate_group");
				String accntFxRateGrp = "";
				String otherAccntIndicator = "";
				//RKAZI HRUL061749069 06/20/2011 Start
				String acctBankOpOrg = "";
				ClientBank clientBank = null;
				if (clientBank == null) {
             	   String clientBankOid = this.getAttribute("client_bank_oid");
             	   clientBank = (ClientBank) createServerEJB("ClientBank");
             	   clientBank.getData(Long.parseLong(clientBankOid));
                }
				String allow_pay_by_another_accnt = clientBank.getAttribute("allow_pay_by_another_accnt");
				//RKAZI HRUL061749069 06/20/2011 End
				for (int i = 0; i < accountTot; i++) {
					accountList.scrollToObjectByIndex(i);
			   		busObj = accountList.getBusinessObject();
			   		String acctNumber = busObj.getAttribute("account_number");
			   		if (StringFunction.isNotBlank(acctNumber)) {
			   			String reportingType = this.getAttribute("reporting_type");
			   			//BSL IR VKUL072761455 03/28/2012 BEGIN
			   			String defaultTpsCustomerId = getAttribute("Proponix_customer_id");
			   			String defaultOpBankOrgOid = getAttribute("first_op_bank_org");

			   			if (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("othercorp_customer_indicator"))) {
			   				String otherOwnerOid = busObj.getAttribute("other_account_owner_oid");
			   				//cquinton 12/6/2012 add this generic error for bad data cases
                            if ( StringFunction.isBlank(otherOwnerOid) ) {
                                String corporateOrgOid = this.getAttribute("organization_oid");
                                String acctOid = busObj.getAttribute("account_oid");
                                String errorMsg = "Data error: Corporate org " + corporateOrgOid +
                                    " account " + acctOid + " is a subsidiary account " +
                                    " but owner_account_owner_oid is missing";
                                throw new AmsException(errorMsg);
                            }
			   				CorporateOrganization otherOwner = (CorporateOrganization) this.createServerEJB("CorporateOrganization",
			   						Long.parseLong(otherOwnerOid));

			   				defaultTpsCustomerId = otherOwner.getAttribute("Proponix_customer_id");
			   				defaultOpBankOrgOid = otherOwner.getAttribute("first_op_bank_org");
			   			}
			   			//BSL IR VKUL072761455 03/28/2012 END

			   			//
			   			//CR610 Rel7.0.0.0 AAlubala - Compare the overall profile FX Rate Group with that of each account
			   			//if they do not match, issue an error
			   			//
		   				accntFxRateGrp = busObj.getAttribute("fx_rate_group");
		   				if(StringFunction.isNotBlank(accntFxRateGrp) && StringFunction.isNotBlank(overallFxRateGrp)){
		   					//IR#VUL041344440 - AAlubala - 04/11/11 - Only check for accounts that belong to the user
		   					otherAccntIndicator = busObj.getAttribute("othercorp_customer_indicator");
		   					//RKAZI HRUL061749069 06/20/2011 Start
		   					if (TradePortalConstants.INDICATOR_YES.equals(allow_pay_by_another_accnt)){
			   					if(!(TradePortalConstants.INDICATOR_YES.equals(otherAccntIndicator))){
					   				if (!accntFxRateGrp.equalsIgnoreCase(overallFxRateGrp)) {
					   					this.getErrorManager().issueError(
					   							TradePortalConstants.ERR_CAT_1,
					   							TradePortalConstants.FXRATE_GROUP_COMPARISON,acctNumber);
					   				}
			   					}
		   					} else if (TradePortalConstants.INDICATOR_NO.equals(allow_pay_by_another_accnt)){
			   					if(!(TradePortalConstants.INDICATOR_YES.equals(otherAccntIndicator))){
			   						if (!accntFxRateGrp.equalsIgnoreCase(overallFxRateGrp)) {
			   							busObj.setAttribute("fx_rate_group", overallFxRateGrp);
			   							//busObj.save();
			   						}
			   					}
		   					}
		   					//RKAZI HRUL061749069 06/20/2011 End
		   				}

		   				//BSL IR VKUL072761455 03/28/2012 BEGIN

		   				acctBankOpOrg = busObj.getAttribute("op_bank_org_oid");

		   				if (StringFunction.isBlank(acctBankOpOrg)) {
		   					acctBankOpOrg = defaultOpBankOrgOid;
		   					busObj.setAttribute("op_bank_org_oid", acctBankOpOrg);
		   				}

		   				String tpsCustomerId = busObj.getAttribute("proponix_customer_id");
		   				if (StringFunction.isBlank(tpsCustomerId)) {
		   					tpsCustomerId = defaultTpsCustomerId;
		   					busObj.setAttribute("proponix_customer_id", tpsCustomerId);
		   				}
		   				//BSL IR VKUL072761455 03/28/2012 END
			   			//END CR610
			   			if (StringFunction.isBlank(busObj.getAttribute("account_name"))) {
			   				String accountName = this.getResourceManager().getText(
			   						"CorpCust.CustomerAccountName",
			   						TradePortalConstants.TEXT_BUNDLE);
			   				if (TradePortalConstants.REPORTING_TYPE_CASH_MGMT.equals(reportingType)) {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_ERR,
			   							accountName,acctNumber);
			   				} else {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_WRN,
			   							accountName,acctNumber);
			   				}
			   			}
			   			if (StringFunction.isBlank(busObj.getAttribute("account_type"))) {
			   				String accountType = this.getResourceManager().getText(
			   						"CorpCust.CustomerAccountType",
			   						TradePortalConstants.TEXT_BUNDLE);
			   				if (TradePortalConstants.REPORTING_TYPE_CASH_MGMT.equals(reportingType)) {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_ERR,
			   							accountType,acctNumber);
			   				} else {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_WRN,
			   							accountType,acctNumber);
			   				}
			   			}
			   			if (StringFunction.isBlank(busObj.getAttribute("bank_country_code"))) {
			   				String bankCountryCode = this.getResourceManager().getText(
			   						"CorpCust.BankCountryCode",
			   						TradePortalConstants.TEXT_BUNDLE);
			   				if (TradePortalConstants.REPORTING_TYPE_CASH_MGMT.equals(reportingType)) {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_ERR,
			   							bankCountryCode,acctNumber);
			   				} else {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_WRN,
			   							bankCountryCode,acctNumber);
			   				}
			   			}
			   			String sourceSystemOrBranch = this.getResourceManager().getText(
		   						"CorpCust.SourceSystemOrBranch",
		   						TradePortalConstants.TEXT_BUNDLE);
			   			if (StringFunction.isBlank(busObj.getAttribute("source_system_branch"))) {			   				
			   				if (TradePortalConstants.REPORTING_TYPE_CASH_MGMT.equals(reportingType)) {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_ERR,
			   							sourceSystemOrBranch,acctNumber);
			   				} else {
			   					this.getErrorManager().issueError(
			   							TradePortalConstants.ERR_CAT_1,
			   							TradePortalConstants.CUST_ACCTS_DATA_MISSING_WRN,
			   							sourceSystemOrBranch,acctNumber);
			   				}
			   			}else{
		   				/*Rel 935 - IR T36000045922 - Check for length sourceSystemOrBranch*/
		   					if(busObj.getAttribute("source_system_branch").length() > 6){
		   						this.getErrorManager().issueError(
		   								TradePortalConstants.ERR_CAT_1,
		   								TradePortalConstants.STRING_LENGTH_EXCEED,
		   								sourceSystemOrBranch,acctNumber);
		   					}			   					
			   			}

			   			//IAZ CM-451 04/07/09 Begin
			   			// verify that customer does not have same account number and same currency combination entered more than once
			   			String currencyCode = busObj.getAttribute("currency");
			   			if (isDuplicateAccount(accountNumberCurHash,
			   					acctNumber,
			   					currencyCode)) {
			   				this.getErrorManager().issueError(
			   						TradePortalConstants.ERR_CAT_1,
			   						TradePortalConstants.CUST_ACCTS_DATA_DUPLICATED,
			   						acctNumber, currencyCode);
			   			}
			   			//IAZ CM-451 04/07/09 End

			   			// [START] PDUK010441815
			   			if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_direct_debit")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_direct_debit"))))
			   			{
			   				availForDirectDebitChkbox = true;
			   			}

			   			if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_loan_request")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_loan_request"))))
			   			{
			   				availForLoanReqChkbox = true;
			   			}

			   			if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pymt_instrument_auth")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_internatnl_pay"))))
			   			{
			   				availForIPChkbox = true;
			   			}

			   			if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_xfer_btwn_accts")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_xfer_btwn_accts"))))
			   			{
			   				availForTBAChkbox = true;
			   			}

			   			if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_domestic_payments")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_domestic_pymts"))))
			   			{
			   				availForPaymentChkbox = true;
			   			}
			   			
			   			
			   		
			   			
			   			//PMitnala Rel 8.3 IR T36000022461 - Corrected the condition for the boolean variable isPanelSelected.
			   			boolean isPanelSelected = busObj.getAttribute("panel_auth_group_oid_1").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_2").trim().length() != 0
	   					|| busObj.getAttribute("panel_auth_group_oid_3").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_4").trim().length() != 0
	   					|| busObj.getAttribute("panel_auth_group_oid_5").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_6").trim().length() != 0
	   					|| busObj.getAttribute("panel_auth_group_oid_7").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_8").trim().length() != 0
	   					|| busObj.getAttribute("panel_auth_group_oid_9").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_10").trim().length() != 0
   						|| busObj.getAttribute("panel_auth_group_oid_11").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_12").trim().length() != 0
   						|| busObj.getAttribute("panel_auth_group_oid_13").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_14").trim().length() != 0
	   					|| busObj.getAttribute("panel_auth_group_oid_15").trim().length() != 0 || busObj.getAttribute("panel_auth_group_oid_16").trim().length() != 0;
			   			//IR T36000020410 Rel 8.3 - Incorrect warning messages when panel group is selected
			   			if(((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_auth_funds_transfer")))
			   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_internatnl_pay")))
			   					&& !isPanelSelected)
			   				|| ((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_xfer_btwn_accts")))
				   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_xfer_btwn_accts")))
				   					&& !isPanelSelected)
				   			|| ((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_domestic_payments")))
				   					&& (TradePortalConstants.INDICATOR_YES.equals(busObj.getAttribute("available_for_domestic_pymts")))
				   					&& !isPanelSelected))
			   			{
			   				this.getErrorManager().issueError(
			   						TradePortalConstants.ERR_CAT_1,
			   						TradePortalConstants.NO_PANEL_GROUPS_SPECIFIED,
			   						acctNumber);
			   			}

			   			// [END] PDUK010441815
			   		}
				}
				// [START] PDUK010441815
				if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_direct_debit")))
						&& (availForDirectDebitChkbox == false))
				{
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACCOUNT_SELECTED,
						this.getResourceManager().getText(
	   						"CorpCust.DirectDebits",
	   						TradePortalConstants.TEXT_BUNDLE));
				}

				if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_loan_request")))
						&& (availForLoanReqChkbox == false))
				{
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACCOUNT_SELECTED,
						this.getResourceManager().getText(
	   						"CorpCust.LoanRequests",
	   						TradePortalConstants.TEXT_BUNDLE));
				}

				//if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pymt_instrument_auth")))   // NSX 04/22/2010 - SEUK042071957
				if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_funds_transfer")))      // NSX 04/22/2010 - SEUK042071957
						&& (availForIPChkbox == false))
				{
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACCOUNT_SELECTED,
						this.getResourceManager().getText(
							"CorpCust.FundsTransferRequests",
	   						TradePortalConstants.TEXT_BUNDLE));
				}

				if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_xfer_btwn_accts")))
						&& (availForTBAChkbox == false))
				{
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACCOUNT_SELECTED,
						this.getResourceManager().getText(
							"CorpCust.TransferBtwAccts",
	   						TradePortalConstants.TEXT_BUNDLE));
				}

				if((TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_domestic_payments")))
						&& (availForPaymentChkbox == false))
				{
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACCOUNT_SELECTED,
						this.getResourceManager().getText(
							"CorpCust.DomesticPayment",
	   						TradePortalConstants.TEXT_BUNDLE));
				}
				// [END] PDUK010441815
			}
        }

		String authMethod = this.getAttribute("authentication_method");

		if (!validateCorpUserSecurityData(authMethod)) {
			if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD)) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SECURITY_CORP_USER_NO_PASSWORD);
			} else if (authMethod.equals(TradePortalConstants.AUTH_SSO)) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SECURITY_CORP_USER_NO_SSO);
		    }else if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SECURITY_CORP_USER_NO_CERT);
            }else if (authMethod.equals(TradePortalConstants.AUTH_2FA)) {
                this.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.SECURITY_CORP_USER_NO_2FA);
            } else if (TradePortalConstants.AUTH_REGISTERED_SSO.equals(authMethod)) {//BSL 09/07/11 CR663 Rel 7.1 Add
                this.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.SECURITY_CORP_USER_NO_REGISTERED_SSO);
			}
		}

		ClientBank clientBank = null; //Vshah - NEUI060565508 - Increase the scope of this reference variable
		// cr498 11/22/2010 change to use requireTranAuth instead of require_certs_loan_and_funds
		// check for Certs if required at client bank level for LRQ and FTRQ transactions
		if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD) ) { //RKAZI T36000019772 REL 8.2 08/22/2013 - Removed SSO condition. 

			String clientBankOid = this.getAttribute("client_bank_oid");
			clientBank = (ClientBank) createServerEJB("ClientBank");
			clientBank.getData(Long.parseLong(clientBankOid));
			String requireTranAuth =
				clientBank.getAttribute("require_tran_auth");



		// // Narayan IR-KRUK120152767 Begin
			if ( (( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__IMP_DLC_ISS) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__IMP_DLC_DCR))&&
				       (this.getAttribute("allow_import_DLC")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {

				    this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ImportDLC", TradePortalConstants.TEXT_BUNDLE));

			}

			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__SLC_ISS)|| InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__SLC_AMD) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__SLC_DCR))&&
				       (this.getAttribute("allow_SLC")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.StandbyDLC", TradePortalConstants.TEXT_BUNDLE));

			}

			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__EXP_DLC_TRN) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ATR) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ASN)|| InstrumentAuthentication.
										   requireTransactionAuthentication(requireTranAuth,
												   InstrumentAuthentication.TRAN_AUTH__EXP_DLC_DCR))&&
				       (this.getAttribute("allow_export_LC")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ExportLC", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__EXP_COL_ISS) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__EXP_COL_AMD))&&
				       (this.getAttribute("allow_export_collection")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ExportCollections", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__EXP_OCO_ISS) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__EXP_OCO_AMD)) &&
				       (this.getAttribute("allow_new_export_collection")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.NewExportCollections", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__GUAR_ISS)|| InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__GUAR_AMD) )&&
				       (this.getAttribute("allow_guarantee")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.Guarantees", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__AIR_REL) &&
				       (this.getAttribute("allow_airway_bill")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.AirWaybills", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__SHP_ISS) &&
				       (this.getAttribute("allow_shipping_guar")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ShippingGuarantees", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__FTRQ_ISS) &&
				       (this.getAttribute("allow_funds_transfer")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.FundsTransferRequests", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__LRQ_ISS) &&
				       (this.getAttribute("allow_loan_request")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.LoanRequests", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__RQA_ISS) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__RQA_AMD) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__RQA_DCR))&&
				       (this.getAttribute("allow_request_advise")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.RequestAdvise", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__ATP_ISS) || InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__ATP_AMD) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__ATP_APR))&&
				       (this.getAttribute("allow_approval_to_pay")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ApprovaltoPay", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( (InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE)|| InstrumentAuthentication.
						   requireTransactionAuthentication(requireTranAuth,
								   InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT) || InstrumentAuthentication.
								   requireTransactionAuthentication(requireTranAuth,
										   InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE) || InstrumentAuthentication.
										   requireTransactionAuthentication(requireTranAuth,
												   InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE) || InstrumentAuthentication.
												   requireTransactionAuthentication(requireTranAuth,
														   InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE) )&&
				       (this.getAttribute("allow_arm")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.ReceivablesManagement", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__DDI_ISS) &&
				       (this.getAttribute("allow_direct_debit")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.DirectDebits", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__FTBA_ISS) &&
				       (this.getAttribute("allow_xfer_btwn_accts")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.TransferBtwAccts", TradePortalConstants.TEXT_BUNDLE));

			}
			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__FTDP_ISS) &&
				       (this.getAttribute("allow_domestic_payments")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.DomesticPayment", TradePortalConstants.TEXT_BUNDLE));

			}

			if ( ( InstrumentAuthentication.
					   requireTransactionAuthentication(requireTranAuth,
						   InstrumentAuthentication.TRAN_AUTH__INC_SLC_DCR) &&
				       (this.getAttribute("allow_non_prtl_org_instr_ind")).equals(
				    	   TradePortalConstants.INDICATOR_YES))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CERT_REQD_LOAN_AND_FUND,resMgr.getText("CorpCust.NonPortalOriginatedInstruments", TradePortalConstants.TEXT_BUNDLE));

			}

			
		}
		//cr498 end

               // If this corporate is set up for doc prep, make sure it has an ID set
               if(StringFunction.isNotBlank(this.getAttribute("doc_prep_ind")) &&
                  this.getAttribute("doc_prep_ind").equals(TradePortalConstants.INDICATOR_YES))
                {
                   attributeMgr.requiredAttribute("doc_prep_accountname");
                   //CR-433 Krishna 05/13/2008 Begin
                   String corpServicesURL = this.getAttribute("doc_prep_url");
		   //Vshah - 06/09/08 - IR#NEUI060565508 - ADD BEGIN
                   if (clientBank == null) { // W Zhu 7/7/08 PNUI070759748 instantiate clientBank if needed.
                	   String clientBankOid = this.getAttribute("client_bank_oid");
                	   clientBank = (ClientBank) createServerEJB("ClientBank");
                	   clientBank.getData(Long.parseLong(clientBankOid));
                   }
                   String cbCcorpServiceURL = clientBank.getAttribute("doc_prep_url");
                   if(StringFunction.isBlank(corpServicesURL) && StringFunction.isBlank(cbCcorpServiceURL))
                   {
                	   this.getErrorManager().issueError(
      							TradePortalConstants.ERR_CAT_1,
      							TradePortalConstants.CORP_SERVICES_URL_MSG1);
                   }
                   //Vshah - 06/09/08 - IR#NEUI060565508 - ADD END
                   else if(StringFunction.isBlank(corpServicesURL))
                   {
   				    this.getErrorManager().issueError(
   							TradePortalConstants.ERR_CAT_1,
   							TradePortalConstants.CORP_SERVICES_URL_MSG);
                   }
                   //CR-433 Krishna 05/13/2008 End
                }

		validateCityStateZip();
		
		//Ravindra - CR-708B - Start
		
		/*
		 * Interest/Discount Rate Group must be defined If Supplier Portal is selected 
		 * in Customer Authorisations and Capabilities
		 * 
		 * Move this validation to CorpBean
		*/
		if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_supplier_portal")) 
				&& StringFunction.isBlank(this.getAttribute("interest_disc_rate_group"))){
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INTR_DISC_GRP_REQ);
		}
		//Ravindra - CR-708B - End
		
		//Kyriba CR 268 - Validate External Bank
		this.validateExternalBank();
		
		//M Eerupula 04/16/2013 Rel8.3 CR776 
		if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_supplier_portal")) 
				&& StringFunction.isBlank(this.getAttribute("payables_prog_name"))){
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.REQUIRED_ATTRIBUTE, resMgr.getText("CorpCust.SPPayablesProgName", TradePortalConstants.TEXT_BUNDLE));
		}
			
		
	    //CR 821 - Rel 8.3 START
		this.validatePanelGroups();
		this.validateParentSubsidiaryPanelSettings();
		

		// jgadela  REL 8.3 IR T36000021109  [START] - If the new settings are changed from Yes to No, the system will now check if there are any Pending record
		String replStr = null;
		
		if (checkPendingRecordsInTheQue("corp_user_dual_ctrl_reqd_ind")) {
			replStr = this.getResourceManager().getText(
					"ClientBankDetail.CorporateUsers",
					TradePortalConstants.TEXT_BUNDLE);
			
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CAN_NOT_TURN_OFF_DUAL_CONTROL,
					replStr);
	     }

		if (checkPendingRecordsInTheQue("panel_auth_dual_ctrl_reqd_ind") ) {
			replStr = this.getResourceManager().getText(
					"RefDataHome.PanelAuthGroup",
					TradePortalConstants.TEXT_BUNDLE);
			
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CAN_NOT_TURN_OFF_DUAL_CONTROL,
					replStr);
	     }
		// jgadela  REL 8.3 IR T36000021109  [END]
		
		this.validateSecondaryAddress(); //T36000049101
				
	}

	/**
	 * Rel 951 - T36000049101
	 * Validate secondary address sequence number 
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void validateSecondaryAddress() throws RemoteException,
			AmsException {
		ComponentList addressComponenet = (ComponentList) this
				.getComponentHandle("AddressList");
		int count = addressComponenet.getObjectCount();
		BusinessObject tObj = null;
		List<String> tempList = new ArrayList<String>();
		Set<String> tempSet = new HashSet<String>();
		Set<String> duplicate = new HashSet<String>();
		Iterator<String> iterator = null;
		String element = null;

		for (int i = 0; i < count; i++) {
			addressComponenet.scrollToObjectByIndex(i);
			tObj = addressComponenet.getBusinessObject();
			tempList.add(tObj.getAttribute("address_seq_num"));
		}

		for (String str : tempList) {
			if (!tempSet.add(str)) {
				duplicate.add(str);
			}
		}
		if (!duplicate.isEmpty()) {
			iterator = duplicate.iterator();
			while (iterator.hasNext()) {
				element = iterator.next();
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ALREADY_EXISTS,
						element,
						this.resMgr.getText("AddressBeanAlias.address_seq_num",
								TradePortalConstants.TEXT_BUNDLE));
			}
		}

	}

// Rkazi IR BLUL040459173 04/11/2011 Start.
	// This method validates that the base currency code matches the FX Rate Group
	// Base currency. If the base currency selected is not the base curency specified
	// for the FX Rate Group then an error is raised.
   private void validateBaseCurrency() throws RemoteException, AmsException{
		String fxRateGroup = getAttribute("fx_rate_group");
		if (StringFunction.isNotBlank(fxRateGroup)){
			String baseCurrencyCode = null;
				// select the  base currency code from fx_rate table based on
				// entered FX Rate Group.
				// IAZ: IR SEUL052654252 05/30/11 Start
				// Added restrictions by grp owner to be this corp or its client bank
				//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		        String fxRateGroupSql =
		        	"select DISTINCT(fx_rate_group) FXRTGRP, base_currency_code BCC"
		                    	+ " from fx_rate where fx_rate_group = ? and p_owner_org_oid in (?, ?)";
				//IAZ IR SEUL052654252 End
			DocumentHandler result =
				DatabaseQueryBean.getXmlResultSet(
						fxRateGroupSql,
					false, fxRateGroup, getAttribute("organization_oid"), getAttribute("client_bank_oid"));

            //IAZ IR PIUL051734251 05/17/2011 Begin
            if (result == null)
            {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FXRATE_GROUP_BASE_IS_NOT_VALID, fxRateGroup);
				return;
			}
            //IAZ IR PIUL051734251 05/17/2011 End

			baseCurrencyCode =
				result.getAttribute(
					"/ResultSetRecord(0)/BCC");
			if (StringFunction.isBlank(baseCurrencyCode) | ! (getAttribute("base_currency_code").equals(baseCurrencyCode))){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FXRATE_GROUP_BASE_CURRECY_COMPARISON,
						getAttribute("base_currency_code"),fxRateGroup,baseCurrencyCode);

			}

		}

	}
   // Rkazi IR BLUL040459173 04/11/2011 End

/**
    * When saving a Customer, validate combination of City , State, Postal Code
    */
	  public void validateCityStateZip()
		 throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	  {
		  int countTotalNoOfChars = 0;
		  String city = this.getAttribute("address_city");
		  if (city != null)
			  countTotalNoOfChars += city.length();
		  String state = this.getAttribute("address_state_province");
		  if (state != null)
			  countTotalNoOfChars += state.length();
		  String postalcode = this.getAttribute("address_postal_code");
		  if (postalcode != null)
			  countTotalNoOfChars += postalcode.length();
		  //Surrewsh IR-T36000043283 12/3/2015 Start 
		  //Validate total no. of characters
		  if (countTotalNoOfChars > TradePortalConstants.CITY_STATE_ZIP_PARTYBEAN_LENGTH)
		  {
			  this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.TOO_MANY_CHARS_IN_COMB_PARTYBEAN);
		  }
	  }
	      //Surrewsh IR-T36000043283 12/3/2015 End 

	/**
	 * This method is used to perform any processing necessary before
	 * an object is deactivated. If the Corporate Organization is a
	 * parent to any other Corporate Orgs or has any active users,
	 * issue error and do not deactivate.
	 */
	protected void userDeactivate() throws RemoteException, AmsException {
		String corporateOrgName = null;
		String corporateOrgOid = null;

		// Get the corporate org's name for substitution in the error messages if necessary
		corporateOrgName = this.getAttribute("name");
		corporateOrgOid = this.getAttribute("organization_oid");

		// Determine whether the corporate org is a parent to any other corporate orgs
		if (isAssociated("CorporateOrganization",
			"activeByParentCorporateOrg",
			new String[] { corporateOrgOid, TradePortalConstants.ACTIVE })) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.PARENT_TO_CORP_CUSTOMERS,
				corporateOrgName);
		}

		// Determine whether there are any active users in the corporate org
		if (isAssociated("User",
			"activeByCorporateOrg",
			new String[] { corporateOrgOid, TradePortalConstants.ACTIVE })) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ACTIVE_USERS,
				corporateOrgName);
		}

		//IAZ IR-LIUK020456367 04/27/10 BEGIN
        ComponentList accountList = (ComponentList)this.getComponentHandle("AccountList");
        if (accountList.getObjectCount() > 0)
        {
				BusinessObject busObj = null;
				int accountTot = accountList.getObjectCount();
				for (int i = 0; i < accountTot; i++)
				{
					accountList.scrollToObjectByIndex(i);
			   		busObj = accountList.getBusinessObject();
			   		LOG.debug("Deactivating account: {}" , busObj.getAttribute("account_number"));
			   		busObj.setAttribute("deactivate_indicator", "Y");
			   		busObj.save();
				}
		}
		//IAZ LIUK020456367 04/27/10 END

	}

	/**
	 * This method is used to perform any processing necessary before an object
	 * is saved. It will set any operational bank orgs selected by the user in
	 * the correct order in the corporate org business object (i.e., first,
	 * second, third, etc.).
	 */
	protected void preSave() throws AmsException {
		
		try{
		
			//Rel8.3 CR 776
			String myOrgProfileValidation = getAttribute("myorg_profile_validation");
			/*MEer Rel 8.3 IR-T36000022048 moving the validations on Organization profile page to two separate methods  */
			if(StringFunction.isNotBlank(myOrgProfileValidation)){
				this.validateMyOrganizationProfile();
				// Srinivasu_D CR-599 Rel8.4  11/21/2013 start
				boolean errorFlag = this.findDuplicatePayDefinitions();
				
				if(errorFlag){				
			    		this.getErrorManager().issueError(
			    					TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAY_DEF_DUPLICATE_FOUND);	
				}
				// Srinivasu_D CR-599 Rel8.4  11/21/2013 end
			}else{	
				this.validateCorpCustomerProfile();	
			}
		}catch (RemoteException e) {
			throw new AmsException(e.getMessage());
		}

	}

	/**
	 * This method is used to initialize several attributes for a new corporate
	 * organization. Specifically, it sets the activation status and creation
	 * date for a new organization.
	 */
	protected void userNewObject() throws RemoteException, AmsException {
		super.userNewObject();

		this.setAttribute("creation_date", DateTimeUtility.getGMTDateTime());
		this.setAttribute("activation_status", TradePortalConstants.ACTIVE);
	}

	/**
		* This method is used for validating the name of the corporate org that
		* is being updated. An error is issued if the name that was entered is
		* not unique.
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void validateName() throws RemoteException, AmsException {
		String corporateOrgName = null;

		corporateOrgName = this.getAttribute("name");

		if (!isUnique("name",
			corporateOrgName,
			" and a_client_bank_oid = "
				+ this.getAttribute("client_bank_oid"))) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ALREADY_EXISTS,
				corporateOrgName,
				attributeMgr.getAlias("name"));
		}

		String proponixId = this.getAttribute("Proponix_customer_id");

		if (!isUnique("Proponix_customer_id",
			proponixId,
			" and a_client_bank_oid = "
				+ this.getAttribute("client_bank_oid"))) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ALREADY_EXISTS,
				proponixId,
				attributeMgr.getAlias("Proponix_customer_id"));
		}

	}

	/**
		* This method is used for validating the bank organization group oid of
		* the corporate org that is being updated. An error is issued if the BOG
		* that was selected does not belong to the corporate org's client bank.
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void validateBankOrgGroupOid()
		throws RemoteException, AmsException {
		BankOrganizationGroup bankOrgGroup = null;
		String bankOrgGroupOid = null;
		int numberOfRows = 0;

		bankOrgGroupOid = this.getAttribute("bank_org_group_oid");

		// The following validates that the corporate org's bank organization
		// group hasn't become deactivated since the user submitted the request.
		if (!bankOrgGroupOid.equals("")) {
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			String whereClause = "organization_oid = ?  and p_client_bank_oid = ?  and activation_status = '"+TradePortalConstants.ACTIVE+"'";
			numberOfRows =
				DatabaseQueryBean.getCount(
					"ORGANIZATION_OID",
					"BANK_ORGANIZATION_GROUP",
                        whereClause, false, bankOrgGroupOid, this.getAttribute("client_bank_oid"));

			if (numberOfRows < 1) {
				bankOrgGroup =
					(BankOrganizationGroup) createServerEJB("BankOrganizationGroup");

				bankOrgGroup.getData(
					this.getAttributeLong("bank_org_group_oid"));

				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_CLIENT_BANK_ORG,
					bankOrgGroup.getAttribute("name"));
			}
		}
	}

	/**
		* This method is used for validating the corporate org's parent corporation
		* if one exists. It will issue an error if the corporate org selected
		* doesn't exist for the updated corporate org's client bank.
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void validateParentCorporation()
		throws RemoteException, AmsException {
		CorporateOrganization parentCorporateOrg = null;
		String parentCorporateOrgOid = null;

		parentCorporateOrgOid = this.getAttribute("parent_corp_org_oid");

		if (!parentCorporateOrgOid.equals("")) {
			parentCorporateOrg =
				(CorporateOrganization) createServerEJB("CorporateOrganization");

			parentCorporateOrg.getData(
                    Long.parseLong(parentCorporateOrgOid));

			if (!((parentCorporateOrg
				.getAttribute("client_bank_oid")
				.equals(this.getAttribute("client_bank_oid")))
				&& (parentCorporateOrg
					.getAttribute("activation_status")
					.equals(TradePortalConstants.ACTIVE)))) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_CLIENT_BANK_ORG,
					parentCorporateOrg.getAttribute("name"));
			}
		}
	}

	/**
		* This method is used for setting the corporate org's operational bank
		* organizations that were selected by the user. This method first gets
		* the banks that were selected and issues errors if nothing was selected
		* in the first (required) dropdown list or if the same bank was selected
		* more than once. If no errors were issued in the method, it will call a
		* method to set the operational bank orgs in the appropriate order (i.e.,
		* if a user selects a bank in the first and fourth dropdown lists, the
		* first and *second* (not the fourth) bank orgs will get set in the
		* corporate org)
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void setOperationalBankOrgs() throws AmsException {
		boolean isSecondOpBankOrgSet = false;
		boolean isFourthOpBankOrgSet = false;
		boolean isThirdOpBankOrgSet = false;
		boolean hasErrors = false;
		String secondOpBankOrgOid = null;
		String fourthOpBankOrgOid = null;
		String firstOpBankOrgOid = null;
		String thirdOpBankOrgOid = null;

		try {
			firstOpBankOrgOid = this.getAttribute("first_op_bank_org");
			secondOpBankOrgOid = this.getAttribute("second_op_bank_org");
			thirdOpBankOrgOid = this.getAttribute("third_op_bank_org");
			fourthOpBankOrgOid = this.getAttribute("fourth_op_bank_org");

			// Set the first op bank org no matter what mode we're in and what data it contains. If we're in
			// insert mode and the value is "", the framework will catch this and issue the appropriate error;
			// if we're in update mode, we need to set it in case the user has switched a previously set value
			// back to "". If the first op bank org isn't selected, don't even bother checking for other errors
			// here. Also, if more than two op bank orgs are identical, just issue the error once.
			if (!firstOpBankOrgOid.equals("")) {
				if ((firstOpBankOrgOid.equals(secondOpBankOrgOid))
					|| (firstOpBankOrgOid.equals(thirdOpBankOrgOid))
					|| (firstOpBankOrgOid.equals(fourthOpBankOrgOid))) {
					hasErrors = true;

					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DUPLICATE_OP_BANK_ORGS);
				} else {
					if (!secondOpBankOrgOid.equals("")) {
						if ((secondOpBankOrgOid.equals(thirdOpBankOrgOid))
							|| (secondOpBankOrgOid.equals(fourthOpBankOrgOid))) {
							hasErrors = true;

							this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DUPLICATE_OP_BANK_ORGS);
						} else {
							isSecondOpBankOrgSet = true;
						}
					}

					if (!thirdOpBankOrgOid.equals("")) {
						if (thirdOpBankOrgOid.equals(fourthOpBankOrgOid)) {
							if (!hasErrors) {
								hasErrors = true;

								this.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants
										.DUPLICATE_OP_BANK_ORGS);
							}
						} else {
							isThirdOpBankOrgSet = true;
						}
					}

					if (!fourthOpBankOrgOid.equals("")) {
						isFourthOpBankOrgSet = true;
					}
				}
			}

			if (!hasErrors) {
				setOperationalBankOrgs(
					firstOpBankOrgOid,
					secondOpBankOrgOid,
					thirdOpBankOrgOid,
					fourthOpBankOrgOid,
					isSecondOpBankOrgSet,
					isThirdOpBankOrgSet,
					isFourthOpBankOrgSet);
			}
		} catch (RemoteException e) {
			LOG.error(
				"RemoteException occurred in CorporateOrganizationBean: "
					, e);
		}
	}

	/**
		* This method is used for setting the operational bank orgs that were
		* selected by the user in the appropriate order (i.e., if a user
		* selects a bank in the first and fourth dropdown lists, the first and
		* *second* (not the fourth) bank orgs will get set in the corporate
		* org).
		*
		* @param       firstOpBankOrgOid       String   - the first operational bank org oid
		* @param       secondOpBankOrgOid      String   - the second operational bank org oid
		* @param       thirdOpBankOrgOid       String   - the third operational bank org oid
		* @param       fourthOpBankOrgOid      String   - the fourth operational bank org oid
		* @param       isSecondOpBankOrgSet    boolean  - flag indicating whether a bank was selected in the
		*                                                 second dropdown list
		* @param       isThirdOpBankOrgSet     boolean  - flag indicating whether a bank was selected in the
		*                                                 third dropdown list
		* @param       isFourthOpBankOrgSet    boolean  - flag indicating whether a bank was selected in the
		*                                                 fourth dropdown list
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void setOperationalBankOrgs(
		String firstOpBankOrgOid,
		String secondOpBankOrgOid,
		String thirdOpBankOrgOid,
		String fourthOpBankOrgOid,
		boolean isSecondOpBankOrgSet,
		boolean isThirdOpBankOrgSet,
		boolean isFourthOpBankOrgSet)
		throws RemoteException, AmsException {
		this.setAttribute("first_op_bank_org", firstOpBankOrgOid);

		if (isSecondOpBankOrgSet) {
			this.setAttribute("second_op_bank_org", secondOpBankOrgOid);

			if (isThirdOpBankOrgSet) {
				this.setAttribute("third_op_bank_org", thirdOpBankOrgOid);

				if (isFourthOpBankOrgSet) {
					this.setAttribute("fourth_op_bank_org", fourthOpBankOrgOid);
				} else {
					this.setAttribute("fourth_op_bank_org", "");
				}
			} else {
				if (isFourthOpBankOrgSet) {
					this.setAttribute("third_op_bank_org", fourthOpBankOrgOid);
				} else {
					this.setAttribute("third_op_bank_org", "");
				}

				this.setAttribute("fourth_op_bank_org", "");
			}
		} else {
			if (isThirdOpBankOrgSet) {
				this.setAttribute("second_op_bank_org", thirdOpBankOrgOid);

				if (isFourthOpBankOrgSet) {
					this.setAttribute("third_op_bank_org", fourthOpBankOrgOid);
				} else {
					this.setAttribute("third_op_bank_org", "");
				}

				this.setAttribute("fourth_op_bank_org", "");
			} else {
				if (isFourthOpBankOrgSet) {
					this.setAttribute("second_op_bank_org", fourthOpBankOrgOid);
				} else {
					this.setAttribute("second_op_bank_org", "");
				}

				this.setAttribute("third_op_bank_org", "");
				this.setAttribute("fourth_op_bank_org", "");
			}
		}
		/*Kyriba CR 268 - Check if organisation section and 
		 * external bank section are blank.  If so, throw Error msg.*/
		if( ("".equals(firstOpBankOrgOid)) && ("".equals(secondOpBankOrgOid)) 
				&& ("".equals(thirdOpBankOrgOid)) && ("".equals(fourthOpBankOrgOid)) ){			
		
			ComponentList extBankComponenet = (ComponentList) this.getComponentHandle("ExternalBankList");
			int eBankTotal = extBankComponenet.getObjectCount();
			
			if(eBankTotal == 0){
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.OP_BANK_OR_EXTERNAL_BANK_REQ);
			}
		}
		 
	
		
		
		
	}

	/**
	* Suresh CR-603 03/14/2011 Begin
	* This method is used for setting the corporate org's report
	* categories that were selected by the user. This method first gets
	* the report category that were selected and issues errors if the same report category was selected
	* more than once. If no errors were issued in the method, it will call a
	* method to set the report categs in the appropriate order (i.e.,
	* if a user selects a report category in the first and tenth dropdown lists, the
	* first and *second* (not the tenth) report categs will get set in the
	* corporate org)
	*
	* @return      void
	* @exception   java.rmi.RemoteException
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
//	Narayan - 06/17/2011 IR-ALUL0616770572 Begin
	private boolean isDuplicateReportCategs() throws RemoteException, AmsException {
		String firstReptCategOid = this.getAttribute("first_rept_categ");
		String secondReptCategOid = this.getAttribute("second_rept_categ");
		String thirdReptCategOid = this.getAttribute("third_rept_categ");
		String fourthReptCategOid = this.getAttribute("fourth_rept_categ");
		String fifthReptCategOid = this.getAttribute("fifth_rept_categ");
		String sixthReptCategOid = this.getAttribute("sixth_rept_categ");
		String seventhReptCategOid = this.getAttribute("seventh_rept_categ");
		String eighthReptCategOid = this.getAttribute("eighth_rept_categ");
		String ninethReptCategOid = this.getAttribute("nineth_rept_categ");
		String tenthReptCategOid = this.getAttribute("tenth_rept_categ");

		       if (StringFunction.isNotBlank(firstReptCategOid)) {
			      if ((firstReptCategOid.equals(secondReptCategOid))
				       || (firstReptCategOid.equals(thirdReptCategOid))
				       || (firstReptCategOid.equals(fourthReptCategOid))
				       || (firstReptCategOid.equals(fifthReptCategOid))
				       || (firstReptCategOid.equals(sixthReptCategOid))
				       || (firstReptCategOid.equals(seventhReptCategOid))
				       || (firstReptCategOid.equals(eighthReptCategOid))
				       || (firstReptCategOid.equals(ninethReptCategOid))
				       || (firstReptCategOid.equals(tenthReptCategOid))) {
				       return  true;
			          }
		         }
				if (StringFunction.isNotBlank(secondReptCategOid)) {
					if ((secondReptCategOid.equals(thirdReptCategOid))
						|| (secondReptCategOid.equals(fourthReptCategOid))
						|| (secondReptCategOid.equals(fifthReptCategOid))
						|| (secondReptCategOid.equals(sixthReptCategOid))
						|| (secondReptCategOid.equals(seventhReptCategOid))
						|| (secondReptCategOid.equals(eighthReptCategOid))
						|| (secondReptCategOid.equals(ninethReptCategOid))
						|| (secondReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					   }
				   }

				if (StringFunction.isNotBlank(thirdReptCategOid)) {
					if ((thirdReptCategOid.equals(fourthReptCategOid))
						|| 	(thirdReptCategOid.equals(fifthReptCategOid))
						|| 	(thirdReptCategOid.equals(sixthReptCategOid))
						|| 	(thirdReptCategOid.equals(seventhReptCategOid))
						|| 	(thirdReptCategOid.equals(eighthReptCategOid))
						|| 	(thirdReptCategOid.equals(ninethReptCategOid))
						|| 	(thirdReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(fourthReptCategOid)) {
					if ((fourthReptCategOid.equals(fifthReptCategOid))
							|| 	(fourthReptCategOid.equals(sixthReptCategOid))
							|| 	(fourthReptCategOid.equals(seventhReptCategOid))
							|| 	(fourthReptCategOid.equals(eighthReptCategOid))
							|| 	(fourthReptCategOid.equals(ninethReptCategOid))
							|| 	(fourthReptCategOid.equals(tenthReptCategOid))) {

						return  true;
					}
				}
				if (StringFunction.isNotBlank(fifthReptCategOid)) {
					if ((fifthReptCategOid.equals(sixthReptCategOid))
						|| 	(fifthReptCategOid.equals(seventhReptCategOid))
						|| 	(fifthReptCategOid.equals(eighthReptCategOid))
						|| 	(fifthReptCategOid.equals(ninethReptCategOid))
						|| 	(fifthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(sixthReptCategOid)) {
					if ((sixthReptCategOid.equals(seventhReptCategOid))
						|| 	(sixthReptCategOid.equals(eighthReptCategOid))
						|| 	(sixthReptCategOid.equals(ninethReptCategOid))
						|| 	(sixthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(seventhReptCategOid)) {
					if ((seventhReptCategOid.equals(eighthReptCategOid))
						|| 	(seventhReptCategOid.equals(ninethReptCategOid))
						|| 	(seventhReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(eighthReptCategOid)) {
					if ((eighthReptCategOid.equals(ninethReptCategOid))
						|| 	(eighthReptCategOid.equals(tenthReptCategOid))) {
						return  true;
					}
				}
				if (StringFunction.isNotBlank(ninethReptCategOid)) {
					if (ninethReptCategOid.equals(tenthReptCategOid)) {
						return  true;
					}
				}
			return  false;
   }//Narayan - 06/17/2011 IR-ALUL0616770572 End

	/**
		* This method is used for validating the corporate organization's instrument
		* capabilities and authorization attributes. Specifically, it validates the
		* Import Letters of Credit, Standby Letters of Credit, Export Letters of
		* Credit, Direct Send Collection,Export Collection, Guarantee, Airway Bill Release, and Shipping
		* Guarantee indicators for the corporate org.
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void validateInstrumentCapabilities()
		throws RemoteException, AmsException {
		this.validateInstrumentCapability(
			"allow_import_DLC",
			"dual_auth_import_DLC",
			"CorpCust.ImportDLC");
		this.validateInstrumentCapability(
			"allow_SLC",
			"dual_auth_SLC",
			"CorpCust.StandbyDLC");
		this.validateInstrumentCapability(
			"allow_export_LC",
			"dual_auth_export_LC",
			"CorpCust.ExportLC");
		this.validateInstrumentCapability(
			"allow_export_collection",
			"dual_auth_export_coll",
			"CorpCust.ExportCollections");
		//Vasavi CR 524 03/31/2010 Begin
		this.validateInstrumentCapability(
			"allow_new_export_collection",
			"dual_auth_new_export_coll",
			"CorpCust.NewExportCollections");
		//Vasavi CR 524 03/31/2010 End
		this.validateInstrumentCapability(
			"allow_guarantee",
			"dual_auth_guarantee",
			"CorpCust.Guarantees");
		this.validateInstrumentCapability(
			"allow_airway_bill",
			"dual_auth_airway_bill",
			"CorpCust.AirWaybills");
		this.validateInstrumentCapability(
			"allow_shipping_guar",
			"dual_auth_shipping_guar",
			"CorpCust.ShippingGuarantees");
		this.validateInstrumentCapability(
			"allow_funds_transfer",
			"dual_auth_funds_transfer",
			"CorpCust.FundsTransferRequests");
		this.validateInstrumentCapability(
			"allow_loan_request",
			"dual_auth_loan_request",
			"CorpCust.LoanRequests");
		this.validateInstrumentCapability(
			"allow_request_advise",
			"dual_auth_request_advise",
			"CorpCust.RequestAdvise");
		//Krishna CR 375-D 07/19/2007 Begin
		this.validateInstrumentCapability(
				"allow_approval_to_pay",
				"dual_auth_approval_to_pay",
				"CorpCust.ApprovaltoPay");
		//Krishna CR 375-D 07/19/2007 End
		// Chandrakanth IR AAUI120245963 12/04/2008 Begin
		this.validateInstrumentCapability(
				"allow_xfer_btwn_accts",
				"dual_xfer_btwn_accts",
				"CorpCust.TransferBtwAccts");
		this.validateInstrumentCapability(
				"allow_domestic_payments",
				"dual_domestic_payments",
				"CorpCust.DomesticPayment");
		this.validateInstrumentCapability(
				"allow_arm",
				"dual_auth_arm",
				"CorpCust.MatchingApproval");
		// Chandrakanth IR AAUI120245963 12/04/2008 End.
		//Leelavathi CR-710 Rel-8.0.0.0 17/11/2011 Begin.
		this.validateInstrumentCapability(
				"allow_arm_invoice",
				"dual_auth_arm_invoice",
				"CorpCust.InvoiceAuthorisation");
		//Leelavathi CR-710 Rel-8.0.0.0 17/11/2011 End.
		//Vasavi CR-509 12/10/2009 BEGIN
		this.validateInstrumentCapability(
				"allow_direct_debit",
				"dual_auth_direct_debit",
				"CorpCust.DirectDebits");
		//Vasavi CR-509 12/10/2009 END.
		//Leelavathi CR-710 Rel-8.0.0.0 25/11/2011 Begin.
		this.validateInstrumentCapability(
				"allow_credit_note",
				"dual_auth_credit_note",
				"CorpCust.CreditNoteAuthorisation");
		//Leelavathi CR-710 Rel-8.0.0.0 25/11/2011 End.
		this.validateInstrumentCapability(
				"allow_supplier_portal",
				"dual_auth_supplier_portal",
				"CorpCust.SupplierPortal");
		//Rel 9.0 CR 913 START
		this.validateInstrumentCapability(
				"allow_payables",
				"dual_auth_payables_inv",
				"CorpCust.PayablesMgmtInvoiceUpdates");
		this.validateInstrumentCapability(
				"allow_payables_upl",
				"dual_auth_upload_pay_inv",
				"CorpCust.PayablesInvoiceAuthorisation");
		//Rel 9.0 CR 913 ENDS
		//Rel9.2 CR 914A START
		this.validateInstrumentCapability(
				"allow_pay_credit_note",
				"dual_auth_pay_credit_note",
				"CorpCust.CreditNoteAuthorisation");
		//Rel9.2 CR 914A END
	}

	/**
		* This method is a generic method used for validating the corporate org's
		* instrument capabilities and authorization attributes. It verifies that if
		* an instrument capability was checked by the user, a corresponding
		* authorization attribute was selected as well.  If this isn't the case,
		* the method will issue an error.
		*
		* @return      void
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	private void validateInstrumentCapability(
		String capabilityName,
		String authorizationName,
		String errorKey)
		throws RemoteException, AmsException {
		String errorText = null;

		if (this
			.getAttribute(capabilityName)
			.equals(TradePortalConstants.INDICATOR_YES)) {
			if (this.getAttribute(authorizationName).equals("")) {
				errorText =
					this.getResourceManager().getText(
						errorKey,
						TradePortalConstants.TEXT_BUNDLE);

				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.AUTHORIZED_USERS_NOT_SELECTED,
					errorText);
			}

		}
	}

	/**
	 * Performs a security validation test for the corporate authentication method.
	 * If method is certificate, all corporate customer users of the org must have
	 * a non-blank certificate.  If method is password, all corporate customer users of
	 * the org must have a non-blank login id AND password.  This test is only
	 * done if the method changes.  (Users must be active)
	 *
	 * @return boolean - true if unique, false otherwise
	 * @param authMethod String - AUTH_PASSWORD or AUTH_CERTIFICATE constant.
	 */
	public boolean validateCorpUserSecurityData(String authMethod)
		throws RemoteException, AmsException {
		boolean passesTest = true;

		if (isAttributeModified("authentication_method")) {
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			StringBuilder whereClause = new StringBuilder();
			 List<Object> sqlParams = new ArrayList<>();
			 
			whereClause.append("OWNERSHIP_LEVEL = '");
			whereClause.append(TradePortalConstants.OWNER_CORPORATE);
			whereClause.append("' and p_owner_org_oid= ? ");
			sqlParams.add(this.getObjectID());
			whereClause.append(" and ACTIVATION_STATUS='");
			whereClause.append(TradePortalConstants.ACTIVE);
			whereClause.append("'");
			
			 
			if (authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)) {
				whereClause.append(" and CERTIFICATE_ID IS NULL and CERTIFICATE_TYPE != ?");//gemalto smartcard doesnt have cert id.
				sqlParams.add(TradePortalConstants.GEMALTO);
			} else if (authMethod.equals(TradePortalConstants.AUTH_PASSWORD)) {
				whereClause.append(
					" and (USER_IDENTIFIER IS NULL or PASSWORD IS NULL)");
			} else if (authMethod.equals(TradePortalConstants.AUTH_SSO)) {
				whereClause.append(" and SSO_ID IS NULL");
			} else if (authMethod.equals(TradePortalConstants.AUTH_2FA)) { //CR-482
			    whereClause.append( " and TOKEN_ID_2FA IS NULL");
            } else if (authMethod.equals(TradePortalConstants.AUTH_REGISTERED_SSO)) {//BSL 09/07/11 CR663 Rel 7.1 Add
				whereClause.append(" and REGISTERED_SSO_ID IS NULL");
				whereClause.append(" and (REGISTRATION_LOGIN_ID IS NULL or REGISTRATION_PASSWORD IS NULL)");
			}

			int count =
				DatabaseQueryBean.getCount(
					"user_oid",
					"users",
					whereClause.toString(), false, sqlParams);

			passesTest = (count < 1);
		} else {
			LOG.debug(" Corporate User Authentication method not modified, skipping security validation test");
		}

		return passesTest;
	}

		/**
		* When a transaction comes in to the Portal for this Corporate Customer this method
		* will determine the email/notification setting for this Customer's
		* notification rule using the transaction type/action and the instrument type/category.
		* Changed method name as per Rel9.5 CR978
		* @return      String setting - notification rule's criterion setting
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	public String getNotificationRuleSettingOrFreq(
		String notifRuleOid,
		String instrumentTypeOrCategory,
		String transactionTypeOrAction,
		String criterionType)
		throws RemoteException, AmsException {
		String setting = null;

		try {

			// Set the parameters to be used in looking up the criterion in the database
			String parms[] = new String[4];
			parms[0] = notifRuleOid;//Rel9.5 CR-927B
			parms[1] = instrumentTypeOrCategory;
			parms[2] = transactionTypeOrAction;
			
			GenericList criterionList =
				(GenericList) EJBObjectFactory.createServerEJB(
					getSessionContext(),
					"GenericList");
			criterionList.prepareList(
				"NotificationRuleCriterion",
				"forCorpOrgTransaction",
				parms);
			criterionList.getData();

			if (criterionList.getObjectCount() == 1) {
				//Rel9.5 CR-927B Get the settings of notification/email
				if(TradePortalConstants.NOTIF_RULE_NOTIFICATION.equals(criterionType)){
					setting = criterionList.getAttribute("send_notif_setting");
				}else if(TradePortalConstants.NOTIF_RULE_EMAIL.equals(criterionType)){
					setting = criterionList.getAttribute("send_email_setting");
				}else if(TradePortalConstants.NOTIF_RULE_EMAIL_FREQ.equals(criterionType)){
					setting = criterionList.getAttribute("notify_email_freq");
				}
			}
			LOG.info("Inside CorporateOrgBean::getNotificationRuleSettingOrFreq()..instrType= {} and transType= {}",instrumentTypeOrCategory,transactionTypeOrAction );
			LOG.info("Inside CorporateOrgBean::getNotificationRuleSettingOrFreq()..criterionType= {} and setting= {}",criterionType,setting);
		} catch (Exception e) {
			LOG.error("CorporateOrganizationBean: Exception in getNotificationRuleSettingOrFreq(): ",e);
		}
		return setting;
	}

	/**
		* If this customer is assigned to a notification rule this method
	     * will retrieve the attribute value for the attribute name
		* passed in.
		*
		* @return      String attributeValue
		* @exception   java.rmi.RemoteException
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	public String getNotificationRuleAttribute(String attributeName)
		throws RemoteException, AmsException {
		String attributeValue = null;

		String notifRuleOid = this.fetchNotificationRule();//Rel9.5 CR-927B 
		if (!StringFunction.isBlank(notifRuleOid)) {
			NotificationRule notificationRule =
				(NotificationRule) this.createServerEJB(
					"NotificationRule",
					Long.parseLong(notifRuleOid));
			attributeValue = notificationRule.getAttribute(attributeName);
		}
		return attributeValue;
	}

	/**
	   * This method creates an email for the customer when a notification or message
	   * has been received in the portal. An email can be created for each individual
	   * notification or message; or an email can be sent daily to notify the customer
	   * that notificiations and/or messages have been received
	   *
	   * @param instrumentOid, oid of the instrument a notification or msg is associated to
	   * @param transactionOid, oid of transaction a notification or msg is associated to
	   * @param discrepancyAmount, amount of discrepancy message if it exists
	   * @param emailType, indicates type of email message that should be created
	   *                   (transaction, mail message, discrepancy, daily email)
	   */
	public void createEmailMessage(
		String instrumentOid,
		String transactionOid,
                String discrepancyCurrency,
                String discrepancyAmount,
		String emailType)
		throws RemoteException, AmsException {
		
		// Get the email receiver address and the bank org group this corp customer belongs to
		String emailReceiver="" ; 
		String bankOrgOid = this.getAttribute("bank_org_group_oid");
		String emailLanguage = this.getAttribute("email_language");
		String notifRuleOid = this.fetchNotificationRule();//Rel9.5 CR-927B 
		
		// Set the resource local equal according to the language specified by the corporate org
		// This enables the email to be sent in the coporate org's desired language
		String originalLocale = getResourceManager().getResourceLocale();
		String languageLocale =
			ReferenceDataManager.getRefDataMgr().getAdditionalValue(
				"INSTRUMENT_LANGUAGE",
				emailLanguage);
		getResourceManager().setResourceLocale(languageLocale);

		// Get the sender name, sender email address, system name, and bank url from the corp org's bank org group
		BankOrganizationGroup bankOrg =
			(BankOrganizationGroup) this.createServerEJB(
				"BankOrganizationGroup",
				Long.parseLong(bankOrgOid));
		String senderName = "";
		String senderEmailAddress = "";
		String bankUrl = "";
		String tradeSystemName = "";
                String doNotReplyInd = bankOrg.getAttribute("do_not_reply_ind");
                String incldBnkUrlTrade = bankOrg.getAttribute("incld_bnk_url_trade");
                String language1 = bankOrg.getAttribute("email_language");
                String language2 = bankOrg.getAttribute("email_language_2");
              
                if ((emailLanguage.equals(language1)) || (StringFunction.isBlank(language1) &&
                                                          !emailLanguage.equals(language2)))
                {
		  senderName = bankOrg.getAttribute("sender_name").trim();
		  senderEmailAddress = bankOrg.getAttribute("sender_email_address");
		  bankUrl = bankOrg.getAttribute("bank_url");
		  tradeSystemName = bankOrg.getAttribute("system_name").trim();
                }
                else if ((emailLanguage.equals(language2)) || (StringFunction.isBlank(language2)))
                {
		  senderName = bankOrg.getAttribute("sender_name_2").trim();
		  senderEmailAddress = bankOrg.getAttribute("sender_email_address_2");
		  bankUrl = bankOrg.getAttribute("bank_url_2");
		  tradeSystemName = bankOrg.getAttribute("system_name_2").trim();
                }

		// Get the data from instrument that will be used to build the email message
		String instrumentId = "";

		String copyOfRefNum = "";
		String openingBankRefNum = "";
		//rbhaduri - 19th July 06 - IR AYUG040360521
		String instrumentTypeCode = "";
		//narayan  CR - 506 25 jan 10 Begin
		String instrumentCounterParty = "";
		//end
		String relatedInstrumentOid = "";

		if (!StringFunction.isBlank(instrumentOid)) {
			Instrument instrument =
				(Instrument) this.createServerEJB(
					"Instrument",
					Long.parseLong(instrumentOid));
			instrumentId = instrument.getAttribute("complete_instrument_id");
			copyOfRefNum = instrument.getAttribute("copy_of_ref_num");
			openingBankRefNum = instrument.getAttribute("opening_bank_ref_num");
			//rbhaduri - 19th July 06 - IR AYUG040360521
			instrumentTypeCode = instrument.getAttribute("instrument_type_code");
            //narayan  CR - 506 25 jan 10 Begin
			String partyOID = instrument.getAttribute("a_counter_party_oid");
			if (!StringFunction.isBlank(partyOID)){
			TermsParty termsParty = (TermsParty) this.createServerEJB(
					"TermsParty", Long.parseLong(partyOID));
			instrumentCounterParty = termsParty.getAttribute("name");
			}
			//end
			relatedInstrumentOid = instrument.getAttribute("related_instrument_oid");
		}

		// Get the data from transaction that will be used to build the email message
		String transactionAmount = "";
		String currency = "";
		String transactionType = "";
		String transactionTypeDesc = "";
		String sequenceNum = "";
		String transactionStatusDesc = "";
		if (!StringFunction.isBlank(transactionOid)) {
			Transaction transaction =
				(Transaction) this.createServerEJB(
					"Transaction",
					Long.parseLong(transactionOid));
			transactionAmount = transaction.getAttribute("copy_of_amount");
			currency = transaction.getAttribute("copy_of_currency_code");
			transactionType = transaction.getAttribute("transaction_type_code");
			transactionTypeDesc =
				ReferenceDataManager.getRefDataMgr().getDescr(
					"TRANSACTION_TYPE",
					transactionType,
					getResourceManager().getResourceLocale());
			sequenceNum = transaction.getAttribute("sequence_num");
			transactionStatusDesc = ReferenceDataManager.getRefDataMgr().getDescr(
						"TRANSACTION_STATUS",
						transaction.getAttribute("transaction_status"),
						getResourceManager().getResourceLocale());
			
			// Certain transaction types should use another transaction type's
			// notification email settings. The following transaction types
			// should be handled in the following manner:
			//   1. Amendment transfers = amendment
			//   2. AR Payment (ARP) = AR Updates (ARU) 
			if (transactionType.equals(TransactionType.AMEND_TRANSFER))
				transactionType = TransactionType.AMEND;
			else if (transactionType.equals(TradePortalConstants.ACCOUNTS_RECEIVABLE_PAYMENT))
				transactionType = TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE;
						
						
		}

		// Build the subject line and content of the email based on the type of email (transaction, mail message, discrepancy, daily)
		StringBuilder emailSubject = new StringBuilder();
		StringBuilder emailContent = new StringBuilder();
		StringBuilder emailFooter = new StringBuilder();

		if (emailType.equals(TradePortalConstants.EMAIL_TRIGGER_TRANSACTION)) {
			//Rel9.5 CR 927B - Fetch instrumentType for spawned instrument types. This is required to fetch email address based on instrument type
			if(StringFunction.isNotBlank(relatedInstrumentOid)){
				instrumentTypeCode = fetchRelatedInstrumentType(instrumentTypeCode, relatedInstrumentOid);
			}
			emailReceiver = fetchEmailAddresses(instrumentTypeCode, transactionType, notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
                        emailSubject.append(instrumentId + " - ");

                        if (!StringFunction.isBlank(transactionTypeDesc))
                        {
		          emailSubject.append(transactionTypeDesc);
                        }

			emailContent.append(StringFunction
					.asciiToUnicode(getResourceManager().getText(
							"EmailTrigger.TransactionUpdated", TradePortalConstants.TEXT_BUNDLE)));
			emailContent.append(" \"").append(transactionStatusDesc).append("\"");
			emailContent.append(TradePortalConstants.NEW_LINE);

                        emailContent.append(StringFunction.asciiToUnicode(
			      getResourceManager().getText(
			              "EmailTrigger.InstrumentId",
				      TradePortalConstants.TEXT_BUNDLE))
				      + " "
				      + instrumentId);
			emailContent.append(TradePortalConstants.NEW_LINE);
			//narayan  CR - 506 25 jan 10 Begin
			if (!StringFunction.isBlank(instrumentCounterParty)) {
				emailContent.append(getResourceManager().getText(
						"EmailTrigger.CounterParty",
						TradePortalConstants.TEXT_BUNDLE)
						+ " " + instrumentCounterParty);
				emailContent.append(TradePortalConstants.NEW_LINE);
			}
			//end

                        if (!StringFunction.isBlank(openingBankRefNum))
                        {
			   //rbhaduri - 19th July 06 - IR AYUG040360521 - For Request To Advise instrument type
			   //do not include the originating bank text
			   if (!instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))
			   {
			     emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.OrigBankRefNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ openingBankRefNum);
			     emailContent.append(TradePortalConstants.NEW_LINE);
			   }
                        }

                        if (!StringFunction.isBlank(copyOfRefNum))
                        {
			  //rbhaduri - 19th July 06 - IR AYUG040360521 - For Request To Advise instrument type
			  //user 'Your reference number' text with the originating bank reference number

         
            if (!instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))  	   {
				emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.CopyOfRefNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ copyOfRefNum);
			  	emailContent.append(TradePortalConstants.NEW_LINE);
                           }
                        }

                        //TLE - 08/30/06 - IR#AYUG040360521 - Add Begin
			if (instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))
			  {

                            if ((!StringFunction.isBlank(openingBankRefNum)) ||
                               (!StringFunction.isBlank(copyOfRefNum)))
                               {
                                String refNumValue = "";
                                if (!StringFunction.isBlank(openingBankRefNum))
                                   {
                                    refNumValue = openingBankRefNum;
                                   }
                                else
                                   {
                                    refNumValue = copyOfRefNum;
                                   }

				emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.CopyOfRefNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ refNumValue);
			  	emailContent.append(TradePortalConstants.NEW_LINE);
                               }
                           }
                        //TLE - 08/30/06 - IR#AYUG040360521 - Add End


                        if (!StringFunction.isBlank(transactionTypeDesc))
                        {
			  emailContent.append(
				getResourceManager().getText(
					"EmailTrigger.TransactionType",
					TradePortalConstants.TEXT_BUNDLE)
					+ " "
					+ transactionTypeDesc);
			  emailContent.append(TradePortalConstants.NEW_LINE);
                        }

                      //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
           		        if (!StringFunction.isBlank(sequenceNum))
                        {
			  emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.SequenceNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ sequenceNum);
			  emailContent.append(TradePortalConstants.NEW_LINE);
                        }

                        if (!StringFunction.isBlank(transactionAmount))
                        {
                          transactionAmount = TPCurrencyUtility.getDisplayAmount(transactionAmount,
                                                                                 currency,
                                                                                 getResourceManager().getResourceLocale());

			  emailContent.append(
				getResourceManager().getText(
					"EmailTrigger.Amount",
					TradePortalConstants.TEXT_BUNDLE)
					+ " "
					+ currency
					+ " "
					+ transactionAmount);
			  emailContent.append(
				TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
                        }

		} else if (
			emailType.equals(TradePortalConstants.EMAIL_TRIGGER_DISCREPANCY)) {
			emailReceiver = fetchEmailAddresses("MESSAGES", "DISCR", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
			emailSubject.append(instrumentId + " - ");
			//Krishna IR-RIUH102946197 01/08/2008 Begin(added for ATP Notices
			//to get ATP specific display)
			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		   if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY))
			{
				emailSubject.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
							"EmailTrigger.ATPNotice",
							TradePortalConstants.TEXT_BUNDLE)));
				emailContent.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
							"EmailTrigger.ATPNoticeReceived",
							TradePortalConstants.TEXT_BUNDLE)));

			}
			//Krishna IR-RIUH102946197 01/08/2008 End(Apart from ATP, for remaining instruments it
			//remains as it is earlier.'else' block is added below.)
			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		                else
                        {
                               emailSubject.append(StringFunction.asciiToUnicode(
				               getResourceManager().getText(
					               "EmailTrigger.Discrepancy",
					                TradePortalConstants.TEXT_BUNDLE)));

			       emailContent.append(StringFunction.asciiToUnicode(
				               getResourceManager().getText(
					               "EmailTrigger.DiscrepancyReceived",
					                TradePortalConstants.TEXT_BUNDLE)));

                        }
			emailContent.append(TradePortalConstants.NEW_LINE);
			emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.InstrumentId",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ instrumentId);
			emailContent.append(TradePortalConstants.NEW_LINE);
			//narayan  CR - 506 25 jan 10 Begin
			if (!StringFunction.isBlank(instrumentCounterParty)) {
				emailContent.append(getResourceManager().getText(
						"EmailTrigger.CounterParty",
						TradePortalConstants.TEXT_BUNDLE)
						+ " " + instrumentCounterParty);
				emailContent.append(TradePortalConstants.NEW_LINE);
		    }
			//end
                        if (!StringFunction.isBlank(openingBankRefNum))
                        {
			  emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.OrigBankRefNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ openingBankRefNum);
			  emailContent.append(TradePortalConstants.NEW_LINE);
                        }

                      //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
           		        if (!StringFunction.isBlank(copyOfRefNum))
                        {
			  emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.CopyOfRefNum",
					TradePortalConstants.TEXT_BUNDLE))
					+ " "
					+ copyOfRefNum);
			  emailContent.append(TradePortalConstants.NEW_LINE);
                        }

                        if (!StringFunction.isBlank(discrepancyAmount))
                        {
                          discrepancyAmount = TPCurrencyUtility.getDisplayAmount(discrepancyAmount,
                                                                                 discrepancyCurrency,
                                                                                 getResourceManager().getResourceLocale());

			  emailContent.append(
				getResourceManager().getText(
					"EmailTrigger.Amount",
					TradePortalConstants.TEXT_BUNDLE)
					+ " "
					+ discrepancyCurrency
					+ " "
					+ discrepancyAmount);
			  emailContent.append(
				TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
                        }

		} else if (
			emailType.equals(TradePortalConstants.EMAIL_TRIGGER_MAIL_MSG)) {
			
			emailReceiver = fetchEmailAddresses("MESSAGES", "MAIL", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule

                        if (!StringFunction.isBlank(instrumentId))
                        {
                          emailSubject.append(instrumentId + " -  ");
                        }

                        emailSubject.append(
				getResourceManager().getText(
					"EmailTrigger.MailMessage",
					TradePortalConstants.TEXT_BUNDLE));

                      //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
           	emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.MailMessageReceived",
					TradePortalConstants.TEXT_BUNDLE)));

			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		                if (!StringFunction.isBlank(instrumentId))
                        {
			  emailContent.append(StringFunction.asciiToUnicode(
			   	  " " + getResourceManager().getText(
					  "EmailTrigger.ForInstrument",
					  TradePortalConstants.TEXT_BUNDLE))
					  + " ");

			  emailContent.append(instrumentId);
                        }

			// Include copy of ref num field only if it exists
			if (!StringFunction.isBlank(copyOfRefNum)) {
				emailContent.append(", ");
				emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.CopyOfRefNum",
						TradePortalConstants.TEXT_BUNDLE))
						+ " "
						+ copyOfRefNum);
			}
			emailContent.append(
				" "
					+ getResourceManager().getText(
						"EmailTrigger.AttentionRequired",
						TradePortalConstants.TEXT_BUNDLE));
			emailContent.append(
				TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
		} else if (
				emailType.equals(TradePortalConstants.EMAIL_TRIGGER_SETTLEMENT)) {
			
				emailReceiver = fetchEmailAddresses("MESSAGES", "SETTLEMENT", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule

	                        if (!StringFunction.isBlank(instrumentId))
	                        {
	                          emailSubject.append(instrumentId + " -  ");
	                        }

	                        emailSubject.append(
					getResourceManager().getText(
						"EmailTrigger.MailMessage",
						TradePortalConstants.TEXT_BUNDLE));

	                      //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
	           	emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.MailMessageReceived",
						TradePortalConstants.TEXT_BUNDLE)));

				//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
			                if (!StringFunction.isBlank(instrumentId))
	                        {
				  emailContent.append(StringFunction.asciiToUnicode(
				   	  " " + getResourceManager().getText(
						  "EmailTrigger.ForInstrument",
						  TradePortalConstants.TEXT_BUNDLE))
						  + " ");

				  emailContent.append(instrumentId);
	                        }

				// Include copy of ref num field only if it exists
				if (!StringFunction.isBlank(copyOfRefNum)) {
					emailContent.append(", ");
					emailContent.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
							"EmailTrigger.CopyOfRefNum",
							TradePortalConstants.TEXT_BUNDLE))
							+ " "
							+ copyOfRefNum);
				}
				emailContent.append(
					" "
						+ getResourceManager().getText(
							"EmailTrigger.AttentionRequired",
							TradePortalConstants.TEXT_BUNDLE));
				emailContent.append(
					TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
		}else if (
			emailType.equals(TradePortalConstants.EMAIL_TRIGGER_DAILY)) {
			emailReceiver = fetchEmailAddresses("MESSAGES", "MAIL", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
			if (!StringFunction.isBlank(tradeSystemName)) {
				emailSubject.append(tradeSystemName + " ");
			} else {
				//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
			   emailSubject.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.TradeSystem",
						TradePortalConstants.TEXT_BUNDLE))
						+ " ");
			}
			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		    emailSubject.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.Updates",
					TradePortalConstants.TEXT_BUNDLE)));

			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		    emailContent.append(StringFunction.asciiToUnicode(
				getResourceManager().getText(
					"EmailTrigger.DailyUpdates",
					TradePortalConstants.TEXT_BUNDLE)));
			emailContent.append(
				TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
		}
		// DK IR-SAUM053159077 Rel8.0 06/18/2012 Begin
		else if (
				((TradePortalConstants.EMAIL_TRIGGER_FUNDING).equals(emailType))) {
				emailReceiver = fetchEmailAddresses("MESSAGES", "MAIL", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
				emailSubject.append(instrumentId + " - ");

               emailSubject.append(
               getResourceManager().getText(
	               "EmailTrigger.Funding",
	                TradePortalConstants.TEXT_BUNDLE));

             //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
  		       emailContent.append(StringFunction.asciiToUnicode(
			               getResourceManager().getText(
				               "EmailTrigger.FundingReceived",
				                TradePortalConstants.TEXT_BUNDLE)));


			 //SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
			     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
				emailContent.append(TradePortalConstants.NEW_LINE);
				emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.InstrumentId",
						TradePortalConstants.TEXT_BUNDLE))
						+ " "
						+ instrumentId);
				emailContent.append(TradePortalConstants.NEW_LINE);

				if (!StringFunction.isBlank(instrumentCounterParty)) {
					emailContent.append(getResourceManager().getText(
							"EmailTrigger.CounterParty",
							TradePortalConstants.TEXT_BUNDLE)
							+ " " + instrumentCounterParty);
					emailContent.append(TradePortalConstants.NEW_LINE);
			    }

				//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
			     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
	            if (!StringFunction.isBlank(openingBankRefNum))
	                        {
				  emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.OrigBankRefNum",
						TradePortalConstants.TEXT_BUNDLE))
						+ " "
						+ openingBankRefNum);
				  emailContent.append(TradePortalConstants.NEW_LINE);
	                        }

	                        if (!StringFunction.isBlank(copyOfRefNum))
	                        {
				  emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.CopyOfRefNum",
						TradePortalConstants.TEXT_BUNDLE))
						+ " "
						+ copyOfRefNum);
				  emailContent.append(TradePortalConstants.NEW_LINE);
	                        }

	                        if (!StringFunction.isBlank(discrepancyAmount))
	                        {
	                          discrepancyAmount = TPCurrencyUtility.getDisplayAmount(discrepancyAmount,
	                                                                                 discrepancyCurrency,
	                                                                                 getResourceManager().getResourceLocale());

				  emailContent.append(
					getResourceManager().getText(
						"EmailTrigger.Amount",
						TradePortalConstants.TEXT_BUNDLE)
						+ " "
						+ discrepancyCurrency
						+ " "
						+ discrepancyAmount);
				  emailContent.append(
					TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
	                        }

			}
		else if (TradePortalConstants.EMAIL_TRIGGER_AR_MATCH.equals(emailType)) {
			   emailReceiver = fetchEmailAddresses("MESSAGES", "MATCH", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
            emailSubject.append(StringFunction.asciiToUnicode(
            getResourceManager().getText(
	               "EmailTrigger.ReceivablesMatchNotice",
	                TradePortalConstants.TEXT_BUNDLE)));
          
            emailSubject.append("-");
            emailSubject.append(StringFunction.asciiToUnicode( getResourceManager().getText("EmailTrigger.MatchNewInvoices",
     	                TradePortalConstants.TEXT_BUNDLE)));
         		           
			   emailContent.append(StringFunction.asciiToUnicode(
			               getResourceManager().getText(
				               "EmailTrigger.MatchEmailContent",
				                TradePortalConstants.TEXT_BUNDLE)));
			   emailContent.append(" ");
			  emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.MatchEmailContentInvoices",
						TradePortalConstants.TEXT_BUNDLE)));
				//IR 23178 added new line
				 emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
				
				doNotReplyInd = TradePortalConstants.INDICATOR_YES;	

		}

		// DK IR-SAUM053159077 Rel8.0 06/18/2012 End
		//CR 776 Rel 8.3
		//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
	     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
		else if (TradePortalConstants.EMAIL_TRIGGER_SUPPLIER_PORTAL.equals(emailType)) {
			   emailReceiver = fetchEmailAddresses("LRQ_INV", "LRQ_INV", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
               emailSubject.append(StringFunction.asciiToUnicode(
               getResourceManager().getText(
	               "EmailTrigger.SPPayablesProg",
	                TradePortalConstants.TEXT_BUNDLE)));
             
               emailSubject.append("-");
               emailSubject.append(this.getAttribute("payables_prog_name"));
               emailSubject.append("-");
               emailSubject.append(StringFunction.asciiToUnicode( getResourceManager().getText("EmailTrigger.SPNewInvoices",
        	                TradePortalConstants.TEXT_BUNDLE)));
            		           
			   emailContent.append(StringFunction.asciiToUnicode(
			               getResourceManager().getText(
				               "EmailTrigger.SPEmailContent",
				                TradePortalConstants.TEXT_BUNDLE)));
			  
			  if(StringFunction.isNotBlank(discrepancyCurrency)){
				  //IR 23178 added space
			  emailContent.append(" " +discrepancyCurrency + " " );
			  }

				emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.SPEmailContentInvoices",
						TradePortalConstants.TEXT_BUNDLE)));
				//IR 23178 added new line
				 emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
				
				doNotReplyInd = TradePortalConstants.INDICATOR_YES;	

		}else if (TradePortalConstants.EMAIL_TRIGGER_INVOICE_CRN.equals(emailType)) { //MEer Rel 9.3 CR-1006 
			if(TradePortalConstants.PINA_EMAIL_TASK_TYPE.equals(discrepancyAmount)){
				emailReceiver = fetchEmailAddresses("H2H_INV_CRN", "PIN", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
				emailSubject.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
								"EmailTrigger.PayablesInvoices",
								TradePortalConstants.TEXT_BUNDLE)));
			}else if(TradePortalConstants.PCNA_EMAIL_TASK_TYPE.equals(discrepancyAmount)){
				emailReceiver = fetchEmailAddresses("H2H_INV_CRN", "PCN", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
				emailSubject.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
								"EmailTrigger.PayablesCreditNotes",
								TradePortalConstants.TEXT_BUNDLE)));
			}else if(TradePortalConstants.RINA_EMAIL_TASK_TYPE.equals(discrepancyAmount)){
				emailReceiver = fetchEmailAddresses("H2H_INV_CRN", "RIN", notifRuleOid);//Rel9.5 CR-927B Fetch email addresses from Notification Rule
				emailSubject.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
								"EmailTrigger.ReceivablesInvoices",
								TradePortalConstants.TEXT_BUNDLE)));
			}
         		           
			emailContent.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
							"EmailTrigger.SPEmailContent",
							TradePortalConstants.TEXT_BUNDLE)));

			if(StringFunction.isNotBlank(discrepancyCurrency)){
				emailContent.append(" " +discrepancyCurrency + " " );
			}
			if(TradePortalConstants.PCNA_EMAIL_TASK_TYPE.equals(discrepancyAmount)){
				emailContent.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
								"EmailTrigger.InvCRNEmailContent",
								TradePortalConstants.TEXT_BUNDLE)));			 
			}else{
				emailContent.append(StringFunction.asciiToUnicode(
						getResourceManager().getText(
								"EmailTrigger.SPEmailContentInvoices",
								TradePortalConstants.TEXT_BUNDLE)));
			}

			emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);

			doNotReplyInd = TradePortalConstants.INDICATOR_YES;	

		}
		//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
	     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
		// Use default trade system name if bank org group has not specified one
		if (StringFunction.isBlank(tradeSystemName)) {
			tradeSystemName =
					StringFunction.asciiToUnicode(getResourceManager().getText(
					"EmailTrigger.TheTradeSystem",
					TradePortalConstants.TEXT_BUNDLE));
		}
		/*PGedupudi CR 1123 Rel 9.5 start*/
		if (TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlTrade)) {
			//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
		     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
			// Build the email's footer
			emailFooter.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.LogIn",
						TradePortalConstants.TEXT_BUNDLE))
						+ " ");

				
				emailFooter.append(tradeSystemName + " ");
				emailFooter.append(
					getResourceManager().getText(
						"EmailTrigger.AdditionalDetails",
						TradePortalConstants.TEXT_BUNDLE)
						+ " ");
				
				//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
			     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
				if(!(TradePortalConstants.EMAIL_TRIGGER_SUPPLIER_PORTAL.equals(emailType) || TradePortalConstants.EMAIL_TRIGGER_INVOICE_CRN.equals(emailType))){
			        if (!StringFunction.isBlank(bankUrl))
		                {
				  emailFooter.append(StringFunction.asciiToUnicode(
					getResourceManager().getText(
						"EmailTrigger.WebsiteAccess",
						TradePortalConstants.TEXT_BUNDLE))
						+ " ");
				  emailFooter.append(bankUrl + ".");
		                }
				}
		}else{
			emailFooter.append(getResourceManager().getText("EmailTrigger.ForAdditionalDetails",TradePortalConstants.TEXT_BUNDLE)+ ", ")
			.append(StringFunction.asciiToUnicode(getResourceManager().getText("EmailTrigger.PleaseLogIn",TradePortalConstants.TEXT_BUNDLE)))
			.append(" "+tradeSystemName+ " ")
			.append(StringFunction.asciiToUnicode(getResourceManager().getText("EmailTrigger.ByVisitingBankSite",TradePortalConstants.TEXT_BUNDLE)));
		}
		/*PGedupudi CR 1123 Rel 9.5 end*/
		

		//SSikhakolli - Rel 8.4.0.2 IR# T36000026868 06/19/2014 
	     //getText() method call is enclosed in to StringFunction.asciiToUnicode()
                if (doNotReplyInd.equals(TradePortalConstants.INDICATOR_YES))
                {
                  emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
                  emailFooter.append(StringFunction.asciiToUnicode(getResourceManager().getText(
                                "EmailTrigger.DoNotReply",
				TradePortalConstants.TEXT_BUNDLE)));
                }

		// Add the footer to the email content
		emailContent.append(emailFooter);
		
		// Create the format and content of the email
		DocumentHandler emailMessage = new DocumentHandler("<Email></Email>", false, true);
		emailMessage.setAttribute("/To", emailReceiver);
		emailMessage.setAttribute("/Cc", "");
		emailMessage.setAttribute("/Bcc", "");
		emailMessage.setAttribute("/From/SenderAddress", senderEmailAddress);
		emailMessage.setAttribute("/From/SenderName", senderName);
		emailMessage.setAttribute("/Subject", emailSubject.toString());
		emailMessage.setAttribute("/Content", emailContent.toString());
		LOG.debug("Email Message for transactionOid {}  is this: {}", transactionOid,emailMessage);
		// Create a new email trigger object containing the email content created above
		EmailTrigger emailTrigger = (EmailTrigger) this.createServerEJB("EmailTrigger");
		emailTrigger.newObject();
		emailTrigger.setAttribute("agent_id", "");
		emailTrigger.setAttribute("status", TradePortalConstants.EMAIL_TRIGGER_NEW);
		emailTrigger.setAttribute("email_data", emailMessage.toString());
		int i = emailTrigger.save();
		if(i!=1){
			LOG.debug(" Error saving Email Trigger Message ");
		}
		

		// After the email has been created set the resource locale back to the original value
		// This may not be necessary but has been done to ensure other functionality is not affected
		if (!StringFunction.isBlank(originalLocale)) {
			getResourceManager().setResourceLocale(originalLocale);
		}
		
	}

	/**
		  * This method generates the commands to update Supervisor hierarchy
		  * when parent corporate organization or bank organization group
		  * relationships are changed in TradePortal
		  *
		  * @param	reportingUserSuffix String global reporting suffix to
		  * distinguish between client banks and ASP
		  * @return      void
		  * @exception   java.rmi.RemoteException
		  * @exception   com.amsinc.ecsg.frame.AmsException
		  * rbhaduri - 07 Jun 10 - IR KSUK050849844 - Added one more argument to the function call to get the type of the entity being updated
		  */

	private void updateReportingUsers(String reportingUserSuffix, String updatedEntityType, String activation_status)
		throws RemoteException, AmsException {

		try {


			String oldBankOrgOid = 	this.getAttributeOriginalValue("bank_org_group_oid");
			String oldParentCorporateOrgOid = this.getAttributeOriginalValue("parent_corp_org_oid");
			String newParentCorporateOrgOid = this.getAttribute("parent_corp_org_oid");
			String newBankOrgOid = this.getAttribute("bank_org_group_oid");
			String corpOrgOid = this.getAttribute("organization_oid");


			if (( oldParentCorporateOrgOid != newParentCorporateOrgOid ) || activation_status.equals("INACTIVE"))
			{
				String oldParentOid = null;
				String newParentOid = null;

				String corpOrgOIDBeingChanged = null;  //rbhaduri - 07 Jun 10 - IR KSUK050849844
				SupervisorCommand supCommand1 = null;

				supCommand1 = (SupervisorCommand) createServerEJB("SupervisorCommand");
				supCommand1.newObject();

			
				//rbhaduri - 7 Jun 10 - IR KSUK050849844 - Add Begin
				if(updatedEntityType.equals("CC")){
					//corporate customer is the parent
					if (!StringFunction.isBlank(oldParentCorporateOrgOid)){
						oldParentOid = oldParentCorporateOrgOid + reportingUserSuffix;
					}
					newParentOid = newParentCorporateOrgOid + reportingUserSuffix;


				} else {
					//bank organization group is the parent
					if (!StringFunction.isBlank(oldBankOrgOid)){
						oldParentOid = oldBankOrgOid + reportingUserSuffix;
					}
					newParentOid = newBankOrgOid + reportingUserSuffix;

				}
				//rbhaduri - 7 Jun 10 - IR KSUK050849844 - Add End


				//rbhaduri - 27 Jun 10 - IR KSUK050849844 - Added the following line
				corpOrgOIDBeingChanged = corpOrgOid + reportingUserSuffix;

				//Delete the current corporate organization group in Supervisor
				//Please refer to Supervisor admin guide for detail description
				//on command syntax


				if (activation_status.equals("INACTIVE"))
				{

					supCommand1.setAttribute("command",
						SupervisorCommandUtility.deleteCorpOrgGroup(
								corpOrgOIDBeingChanged));


				}
				else
				{
					//rbhaduri - 07 Jun 10 - IR KSUK050849844 - Modify Begin --- added the following if/else block
					if (StringFunction.isBlank(oldParentOid))
					{

							supCommand1.setAttribute("command",
							SupervisorCommandUtility.changeCorpOrgParent(
								corpOrgOIDBeingChanged,
								oldBankOrgOid + reportingUserSuffix,
								newParentOid));

					}
					else
					{

					//rbhaduri - 07 Jun 10 - IR KSUK050849844 - Add Begin - called a new function to build the command for BO XI to change the parent
					//of one corporate customer to another


						if (StringFunction.isBlank(newParentCorporateOrgOid))
						{
							newParentOid = oldBankOrgOid + reportingUserSuffix;


						}

						supCommand1.setAttribute(
								"command",
								SupervisorCommandUtility.changeCorpOrgParent(
									corpOrgOIDBeingChanged,
									oldParentOid,
									newParentOid));

					//rbhaduri - 27 Jun 10 - IR KSUK050849844 - Add End
					}
					//rbhaduri - 07 Jun 10 - IR KSUK050849844 - Modify End

				}

				supCommand1.save();

				//Check if corporate customer has child corporate customer.
				//rbhaduri - 26 Jun 10 - IR KSUK050849844 - Added reporting_type in the following query
				
			}
		} catch (Exception e) {
			LOG.error("CorporateOrganizationBean: Exception in updateReportingUsers(): ",e);
		}


	}

     //Pratiksha change begin 9-Mar-09
     /**
		  * This method generates the commands to update Supervisor hierarchy
		  * when reporting type of customer is changed in TradePortal
		  *
		  * @param	reportingUserSuffix String global reporting suffix to
		  * distinguish between client banks and ASP
		  * @return      void
		  * @exception   java.rmi.RemoteException
		  * @exception   com.amsinc.ecsg.frame.AmsException
		  */

	private void modifyReportingUsers(String reportingUserSuffix)
		throws RemoteException, AmsException {

		try {

			String corpOrgOid = this.getAttribute("organization_oid");
			String oldReportingType = this.getAttributeOriginalValue("reporting_type");
			String reportingType = this.getAttribute("reporting_type");

			String oldCorpOrgOid = null;
			SupervisorCommand supCommand1 = null;

			supCommand1 = (SupervisorCommand) createServerEJB("SupervisorCommand");
			supCommand1.newObject();

			//Add reporting suffix to the corporate customer oid
			oldCorpOrgOid = corpOrgOid + reportingUserSuffix;

			//rbhaduri - 12 May 09 - PPX-051 - Add Begin
			String parentOID = "";
			String pclientbankOID = this.getAttribute("parent_corp_org_oid");
			String bankorggroupOID = this.getAttribute("bank_org_group_oid");

			//If parent corp org oid is null then the corporate customer parent is
			//BOG. If parent corp org oid is not null then parent is corporate customer.
			if (StringFunction.isNotBlank(pclientbankOID)) {
				parentOID = pclientbankOID;
			} else {
				parentOID = bankorggroupOID;
			}
			parentOID = parentOID + reportingUserSuffix;

			//added the following code to get altergroup command
			supCommand1.setAttribute(
				"command",
				SupervisorCommandUtility.getAlterGroupCommand(
					oldCorpOrgOid,
					parentOID,
					reportingType,
					oldReportingType));

			supCommand1.save();
			//rbhaduri - 12 May 09 - PPX-051 - Add End

		} catch (Exception e) {
			LOG.error("CorporateOrganizationBean: Exception in modifyReportingUsers(): ",e);
		}

	}
	//Pratiksha change end 9-Mar-09

	

	/**
		  * This method generates the commands to add new corporate customer
		  * reporting users in Superviosr based on parent organization oid and
		  * organization oid.
		  *
		  * @param	corpOrgOID String current organization oid
		  * @param	parentOrgOID String parent organization oid, this can
		  * be a BOG or a CC
		  * @param	reportingUserSuffix String global reporting suffix
		  * @return      void
		  * @exception   java.rmi.RemoteException
		  * @exception   com.amsinc.ecsg.frame.AmsException
		  */

	private void generateCorpOrgSupervisorCmd(
		String corpOrgOID,
		String parentOrgOID,
		String reportingUserSuffix,
		String reportingUserType,
		String clientbankOID) //Pratiksha
		throws RemoteException, AmsException {

		SupervisorCommand supCommand1 = null;

		String orgOID = null;

		String parentOID = null;



		try {

			//Following code creates new users in Supervisor module when
			//a new Corporate Customer is created

			supCommand1 = (SupervisorCommand) createServerEJB("SupervisorCommand");
			supCommand1.newObject();

			

			// Place the reporting user suffix on to the end of the OID
			// the suffix is used to distinguish between organizations being processed
			// by different ASPs that might be on the same reports server.

			orgOID = corpOrgOID + reportingUserSuffix;
			parentOID = parentOrgOID + reportingUserSuffix;

			//Please refer to Supervisor admin guide for detail description
			//on command syntax
			supCommand1.setAttribute(
				"command",
				SupervisorCommandUtility.getCorpOrgFirstCommand(
					parentOID,
					orgOID,
					reportingUserType)); //rbhaduri - 12 May 09 - PPX-051 - added reportingUserType argument
			supCommand1.save();

			

		} catch (Exception e) {
			LOG.error("CorporateOrganizationBean: Exception in generateCorpOrgSupervisorCmd(): ",e);
		}

	}

	/**
		 * This method reporting user suffix from global org table.
		 *
		 *
		 * @return      String
		 * @exception   java.rmi.RemoteException
		 * @exception   com.amsinc.ecsg.frame.AmsException
		 */

	public String getReportingSuffix() throws RemoteException, AmsException {

		//Get the reporting user suffix from the Global Org to which this org belongs

		String reportingUserSuffix = null;
		try {

			//rbhaduri - 10th Jul 08 - Modify Begin - M And T Bank Change - get reporting
		     	//suffix from client bank table instead of global organization table

			
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		        String reportingUserSuffixSql =
		        	"select c.reporting_user_suffix"
		                    	+ " from client_bank c"
		                     	+ " where c.organization_oid = ? ";
			//rbhaduri - 10th Jul 08 - Modify End
			DocumentHandler result =
				DatabaseQueryBean.getXmlResultSet(
					reportingUserSuffixSql,
					false, new Object[]{this.getAttribute("client_bank_oid")});

			reportingUserSuffix =
				result.getAttribute(
					"/ResultSetRecord(0)/REPORTING_USER_SUFFIX");

		} catch (Exception e) {
			LOG.error("CorporateOrganizationBean: Exception in getReportingSuffix(): ",e);
		}

		return reportingUserSuffix;

	}

    

		private void validateFundsTransferDailyLimits()
			throws RemoteException, AmsException {

			this.validateFundTransferDailyLimit(
				"enforce_daily_limit_transfer",
				"transfer_daily_limit_amt",
				"CorpCust.TransferBtwAccts");

			this.validateFundTransferDailyLimit(
				"enforce_daily_limit_dmstc_pymt",
				"domestic_pymt_daily_limit_amt",
				"CorpCust.DomesticPayment");

			this.validateFundTransferDailyLimit(
				"enforce_daily_limit_intl_pymt",
				"intl_pymt_daily_limit_amt",
				"CorpCust.FundsTransferRequests");

		}

		// CR-469 NShrestha 05/18/2009 Begin
		private void validateFXRateGroup() throws RemoteException, AmsException {
			String fxRateGroup = getAttribute("fx_rate_group");
			if(StringFunction.isBlank(fxRateGroup) &&
					TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pymt_instrument_auth"))){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FX_RATE_GROUP_REQD,
						getResourceManager().getText("CorpCust.FXRateGroup", TradePortalConstants.TEXT_BUNDLE),
						getResourceManager().getText("CorpCust.PaymentInstrumentCapabilities", TradePortalConstants.TEXT_BUNDLE));
			}
		}
		// CR-469 NShrestha 05/18/2009 End

// Nazia IR-PIUI120658250 Begin

private void validateTimeZone() throws RemoteException, AmsException {

		String corpOrgTimeZone = this.getAttribute("timezone_for_daily_limit");
		if(corpOrgTimeZone.equals("")){
			ComponentList accountList = (ComponentList)this.getComponentHandle("AccountList");
	          if (accountList.getObjectCount() > 0)
                {

                  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.TIMEZONE_REQUIRED);
		}
		}
}

            // Nazia IR-PIUI120658250 End

		private void validateFundTransferDailyLimit(String capabilityName,
			String DailyLimitAmount, String errorKey)
			throws RemoteException, AmsException  {
			String errorText = null;

			if (this
					.getAttribute(capabilityName)
					.equals(TradePortalConstants.INDICATOR_YES)) {
				if (this.getAttribute(DailyLimitAmount).equals("")) {
					errorText =
						this.getResourceManager().getText(
								errorKey,
								TradePortalConstants.TEXT_BUNDLE);

					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DAILY_LIMIT_AMT_NOT_ENTERED,
							errorText); // Chandrakanth IR PHUI120660503 12/12/2008 Begin
				} else if(this.getAttributeDecimal(DailyLimitAmount).compareTo(BigDecimal.ZERO) == -1) {
					errorText =
						this.getResourceManager().getText(
								errorKey,
								TradePortalConstants.TEXT_BUNDLE);

					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.NEGATIVE_DAILY_LIMIT_AMT_ENTERED,
							errorText);

				} else {

					TPCurrencyUtility.validateAmount(this.getAttribute("base_currency_code"),
							this.getAttribute(DailyLimitAmount), attributeMgr.getAlias(DailyLimitAmount), getErrorManager() );


				}
				 // Chandrakanth IR PHUI120660503 12/12/2008 End
			}

		}

	// Chandrakanth CR-451 11/21/2008 End

    //IAZ CR-511 11/09/09 Begin
	private void validatePaymentsAuthorizationMethodSelection() throws RemoteException, AmsException
	{
	
		boolean incorrectSelection = false;
		boolean panelAuthFound = false;
		if((TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_xfer_btwn_accts"))) ||
		   (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_domestic_payments"))) ||
		   (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_auth_funds_transfer"))))
		{
			panelAuthFound = true;
		
			if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_xfer_btwn_accts")) && (!TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_xfer_btwn_accts"))) &&
			   (StringFunction.isNotBlank(this.getAttribute("dual_xfer_btwn_accts"))))
			   incorrectSelection = true;
			
			if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_domestic_payments")) && (!TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_domestic_payments"))) &&
			   (StringFunction.isNotBlank(this.getAttribute("dual_domestic_payments"))))
			   incorrectSelection = true;
			
		   	if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_funds_transfer")) && (!TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_auth_funds_transfer"))) &&
			   (StringFunction.isNotBlank(this.getAttribute("dual_auth_funds_transfer"))))
			   incorrectSelection = true;
			
		}

		if (incorrectSelection)
		{
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DIFFERENT_AUTH_METHOD_SELECTED);
	    }
        else
		{

			this.setAttribute("allow_panel_auth_for_pymts", TradePortalConstants.INDICATOR_NO);
			this.setAttribute("allow_xfer_btwn_accts_panel", TradePortalConstants.INDICATOR_NO);
			this.setAttribute("allow_domestic_payments_panel", TradePortalConstants.INDICATOR_NO);
			this.setAttribute("allow_funds_transfer_panel", TradePortalConstants.INDICATOR_NO);
			
			if (panelAuthFound)
			{
				this.setAttribute("allow_panel_auth_for_pymts", TradePortalConstants.INDICATOR_YES);			
				//Srinivasu_D IR#T36000035345 ER9.1 01/01/2015 - handling domestic paymt/intl paymt/fund trans - Start
				if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_domestic_payments")))
		    	{				
					this.setAttribute("allow_domestic_payments_panel", TradePortalConstants.INDICATOR_YES);
				}
				if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_xfer_btwn_accts")))
		    	{			
					this.setAttribute("allow_xfer_btwn_accts_panel", TradePortalConstants.INDICATOR_YES);
				}
				if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_auth_funds_transfer")))
		    	{			
					this.setAttribute("allow_funds_transfer_panel", TradePortalConstants.INDICATOR_YES);
				}
				//Srinivasu_D IR#T36000035345 ER9.1 01/01/2015 - End
			}
				
	    }
		//IR T36000019854 Rel 8.3
		if((StringFunction.isNotBlank(this.getAttribute("dual_domestic_payments")) &&
				!(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(this.getAttribute("dual_domestic_payments"))))){
			if(StringFunction.isNotBlank(this.getAttribute("pmt_bene_panel_auth_ind")) && 
					TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("pmt_bene_panel_auth_ind"))){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PANEL_AUTH_BULK_PAYMENTS_NOT_ALLOWED);
			}
		}
		   
	}
    //IAZ CR-511 11/09/09 End

	private void validatePaymentInstCapabilities() throws RemoteException, AmsException {
		String allowXferBtwAcctsInd = this.getAttribute("allow_xfer_btwn_accts");
		String allowDomesticPmtInd = this.getAttribute("allow_domestic_payments");
		String allowIntlPmtInd = this.getAttribute("allow_funds_transfer");
		if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pymt_instrument_auth"))) {
			if(StringFunction.isBlank(this.getAttribute("allow_panel_auth_for_pymts"))) {
				if (!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_xfer_btwn_accts")) &&
					!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_domestic_payments")) &&
					!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_funds_transfer")) &&
					!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("dual_xfer_btwn_accts")) &&
					!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("dual_domestic_payments")) &&
					!TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("dual_auth_funds_transfer"))) {
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AUTH_METHOD_NOT_SELECTED);
				}
			}
		}

		if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pymt_instrument_auth")) &&
				TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("allow_panel_auth_for_pymts"))) {
			if(TradePortalConstants.INDICATOR_NO.equals(allowXferBtwAcctsInd) &&
					TradePortalConstants.INDICATOR_NO.equals(allowDomesticPmtInd) &&
					TradePortalConstants.INDICATOR_NO.equals(allowIntlPmtInd)) {
				// IR AOUJ021345688 Peter Ng 2/25/2009 Begin
				if ((StringFunction.isBlank(this.getAttribute("dual_xfer_btwn_accts")) || TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("dual_xfer_btwn_accts")))&&
					(StringFunction.isBlank(this.getAttribute("dual_domestic_payments")) || TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("dual_domestic_payments"))) &&
					(StringFunction.isBlank(this.getAttribute("dual_auth_funds_transfer")) || TradePortalConstants.INDICATOR_NO.equals(this.getAttribute("dual_auth_funds_transfer")))) {
    
	                this.setAttribute("allow_pymt_instrument_auth", TradePortalConstants.INDICATOR_NO);
	                //IAZ CR-511 11/09/09 End
	            // IR AOUJ021345688 Peter Ng 2/25/2009 End
				} else {
					this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INSTRUMENT_TYPE_NOT_SELECTED);
				}
			}
		}
	}

   


	//IAZ CM-451 04/07/09 and 04/14/09 Begin
	// verify that customer does not have same account number and same currency combination entered more than once
	private boolean isDuplicateAccount(Hashtable accountNumberCurrencyHash, String accountNumber, String currencyCode)
		throws RemoteException, AmsException {

	    if (StringFunction.isBlank(currencyCode))
	    	return false;

		String exisitingCurrency = (String)accountNumberCurrencyHash.get(accountNumber);
		
		if (exisitingCurrency == null)
		{
			accountNumberCurrencyHash.put(accountNumber, currencyCode);
			return false;
		}

		if (exisitingCurrency.indexOf(currencyCode)!=-1)
		{
				return true;
		}
		else
		{
				accountNumberCurrencyHash.put(accountNumber, exisitingCurrency + ";" + currencyCode);
				return false;
		}

	}

	//verify whjether panel authroization is set for payment instruments
	private boolean panelsEnabled()
		throws RemoteException, AmsException {

		if (TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_panel_auth_for_pymts")) ||
			TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_xfer_btwn_accts_panel")) ||
			TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_domestic_payments_panel")) ||
			TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_funds_transfer_panel")))

			return true;

		else

			return false;

	}
	//IAZ CM-451 04/07/09 and 04/14/09 End

	public void touch() throws RemoteException, AmsException {
		String oplock = getAttribute("opt_lock");
		setAttribute("opt_lock","");
		setAttribute("opt_lock",oplock);
	}


	// NSX 28/07/10 - PYUK071981074 - Begin
    public boolean isInstrumentTypeEnabled(String instrumentType) throws RemoteException, AmsException {
	    boolean inValidInstrType = false;

	    if (InstrumentType.AIR_WAYBILL.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_airway_bill"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.EXPORT_COL.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_export_collection"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.NEW_EXPORT_COL.equals(instrumentType)) {
		       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_new_export_collection"))) {
				  inValidInstrType = true;
			   }
		}
		else if (InstrumentType.GUARANTEE.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_guarantee"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.IMPORT_DLC.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_import_DLC"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.STANDBY_LC.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_SLC"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.EXPORT_DLC.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_export_LC"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.SHIP_GUAR.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_shipping_guar"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.LOAN_RQST.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_loan_request"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.FUNDS_XFER.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_funds_transfer"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType)) {
			   if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_xfer_btwn_accts"))) {
				  inValidInstrType = true;
			   }
			}
		else if (InstrumentType.DOM_PMT.equals(instrumentType)) {
		   if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_domestic_payments"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.REQUEST_ADVISE.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_request_advise"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType)) {
	       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_approval_to_pay"))) {
			  inValidInstrType = true;
		   }
		}
		else if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrumentType)) {
		       if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_direct_debit"))) {
				  inValidInstrType = true;
			   }
		}

	    return inValidInstrType;
    }
   // NSX 28/07/10 - PYUK071981074 - End
    // CJR 04/06/2011 CR-593 Begin
    public boolean isStraightThroughAuthorize() throws RemoteException, AmsException{
    	return TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("straight_through_authorize"));
    }
    // CJR 04/06/2011 CR-593 End
	// DK CR-587 Rel7.1 BEGINS
	private void validateCustomerAlias() throws RemoteException, AmsException {
		ComponentList customerAliasList = (ComponentList) this
				.getComponentHandle("CustomerAliasList");
		BusinessObject custAliasBusObj = null;
		int aliasTot = customerAliasList.getObjectCount();
		HashSet<String> aliasCollection = new HashSet<String>();

		for (int i = 0; i < customerAliasList.getObjectCount(); i++) {
			customerAliasList.scrollToObjectByIndex(i);
			custAliasBusObj = customerAliasList.getBusinessObject();
			String alias = custAliasBusObj.getAttribute("alias");
			String messageCategory = custAliasBusObj
					.getAttribute("gen_message_category_oid");

			if ((!alias.equals("") && messageCategory.equals(""))
					|| (alias.equals("") && !messageCategory.equals(""))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ALIAS_MSGCAT_PAIR_REQUIRED);
			} else if (!alias.equals("") && !messageCategory.equals("")){
				String alias_msgCat_pair = (messageCategory.concat("-"))
						.concat(alias);
				if (aliasCollection.contains(alias_msgCat_pair)) {
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DUPLICATE_ALIAS_MSGCAT_PAIR);
				} else {
					aliasCollection.add(alias_msgCat_pair);
				}
			}
		}
	}
	// DK CR-587 Rel7.1 ENDS

	// AAlubala - IR#AAUL08462630 and IR#NEUL012137189 - Rel8.0 - 09/21/11 - Start
	// Validate Email Address Format
	/**
	 *  Validatesemail address
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected void performEmailValidation(String emailList) throws RemoteException, AmsException {
		if (StringFunction.isBlank(emailList)) return;

	    if (!EmailValidator.validateEmailList(emailList))	{
	    	getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_EMAIL,
					"");
	    }
	}

	// IR#AAUL08462630 and IR#NEUL012137189 - 09/21/11 - End
	
	/**
	 * Added for CR 821 - Rel 8.3
	* This method is used for validating if the Panel Authorization settings at Subsidiary and Parent level are same.
	* If not same, a warning message is issued for all such instrument types.
	* 
	* @return      void
	* @exception   java.rmi.RemoteException
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
	private void validateParentSubsidiaryPanelSettings() throws RemoteException, AmsException {
		CorporateOrganization parentCorporateOrg = null;
		String parentCorporateOrgOid = null;
		String instrumentType = null;
		parentCorporateOrgOid = this.getAttribute("parent_corp_org_oid");

		if (!parentCorporateOrgOid.equals("")) {
			parentCorporateOrg =
				(CorporateOrganization) createServerEJB("CorporateOrganization");

			parentCorporateOrg.getData(
                    Long.parseLong(parentCorporateOrgOid));
				
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_airway_bill"))){
					if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_airway_bill"),this.getAttribute("dual_auth_airway_bill") ))
					{
						instrumentType = this.getResourceManager().getText("CorpCust.AirWaybills",TradePortalConstants.TEXT_BUNDLE);
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
								  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
								  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name")); 
					}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_export_LC"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_export_LC"),this.getAttribute("dual_auth_export_LC") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ExportLC",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_export_coll"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_export_coll"),this.getAttribute("dual_auth_export_coll") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ExportCollections",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_new_export_coll"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_new_export_coll"),this.getAttribute("dual_auth_new_export_coll") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.NewExportCollections",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			//SSikhakolli - Rel-9.4 CR-818 
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_imp_col"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_imp_col"),this.getAttribute("dual_imp_col") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ImportCollections",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_guarantee"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_guarantee"),this.getAttribute("dual_auth_guarantee") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.Guarantees",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_import_DLC"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_import_DLC"),this.getAttribute("dual_auth_import_DLC") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ImportDLC",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_SLC"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_SLC"),this.getAttribute("dual_auth_SLC") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.StandbyDLC",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_shipping_guar"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_shipping_guar"),this.getAttribute("dual_auth_shipping_guar") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ShippingGuarantees",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_loan_request"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_loan_request"),this.getAttribute("dual_auth_loan_request") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.LoanRequests",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_request_advise"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_request_advise"),this.getAttribute("dual_auth_request_advise") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.RequestAdvise",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_approval_to_pay"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_approval_to_pay"),this.getAttribute("dual_auth_approval_to_pay") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.ApprovaltoPay",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_supplier_portal"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_supplier_portal"),this.getAttribute("dual_auth_supplier_portal") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.SupplierPortal",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_arm"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_arm"),this.getAttribute("dual_auth_arm") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.MatchingApproval",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(parentCorporateOrg.getAttribute("dual_auth_arm_invoice")!= null && !parentCorporateOrg.getAttribute("dual_auth_arm_invoice").equals("")){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_arm_invoice"),this.getAttribute("dual_auth_arm_invoice") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.InvoiceAuthorisation",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_credit_note"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_credit_note"),this.getAttribute("dual_auth_credit_note") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.CreditNoteAuthorisation",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			//SSikhakolli - Rel-9.4 CR-818 
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_settle_instruction"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_settle_instruction"),this.getAttribute("dual_settle_instruction") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.SettlementInstructionMsgResponse",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_disc_response"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_disc_response"),this.getAttribute("dual_auth_disc_response") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.DiscrepancyResponse",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_auth_funds_transfer"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_auth_funds_transfer"),this.getAttribute("dual_auth_funds_transfer") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.FundsTransferRequests",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_xfer_btwn_accts"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_xfer_btwn_accts"),this.getAttribute("dual_xfer_btwn_accts") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.TransferBtwAccts",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
			if(StringFunction.isNotBlank(parentCorporateOrg.getAttribute("dual_domestic_payments"))){
				if(checkPanelSettings(parentCorporateOrg.getAttribute("dual_domestic_payments"),this.getAttribute("dual_domestic_payments") ))
				{
					instrumentType = this.getResourceManager().getText("CorpCust.DomesticPayment",TradePortalConstants.TEXT_BUNDLE);
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER,
							  this.getAttribute("name"),instrumentType,instrumentType,parentCorporateOrg.getAttribute("name"));
				}
			}
		}
	}
	/**
	 * Added for CR 821 - Rel8.3
	 * Checks if Panel authorisation is set at Subsidiary level but not at Parent level
	 * @param subParameterName
	 * @param parentParameterName
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	//IR T36000020410 Rel 8.3 - Signature changed,as the validation was not happening properly.
	private boolean checkPanelSettings(String parentParameterName, String subParameterName) throws RemoteException, AmsException{
		if ((subParameterName .equals("P") && !parentParameterName .equals("P") )){
			    return true;
			}
		else{
			return false;
		}
	}
	
	/**
	 * Added for CR 821 - Rel 8.3
	 * Checks if Panel Group is selected when Panel Authorization is selected, for each instrument type.
	 * @throws RemoteException
	 * @throws AmsException
	 */
	
	private void validatePanelGroups() throws RemoteException, AmsException{
		
		String instrumentType = null;
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_airway_bill")) && this.getAttribute("dual_auth_airway_bill").equals("P")){
			/*IR T36000019042 */
			if(StringFunction.isBlank(this.getAttribute("air_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.AirWaybills",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_export_LC")) && this.getAttribute("dual_auth_export_LC").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("exp_dlc_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ExportLC",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_export_coll")) && this.getAttribute("dual_auth_export_coll").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("exp_col_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ExportCollections",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_new_export_coll")) && this.getAttribute("dual_auth_new_export_coll").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("exp_oco_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.NewExportCollections",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		//SSikhakolli - Rel-9.4 CR-818 
		if(StringFunction.isNotBlank(this.getAttribute("dual_imp_col")) && this.getAttribute("dual_imp_col").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("imp_col_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ImportCollections",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_guarantee")) && this.getAttribute("dual_auth_guarantee").equals("P")){
			//MEer Rel 8.3 IR -19792-- change attribute guar_panel_group_oid to gua_panel_group_oid
			if(StringFunction.isBlank(this.getAttribute("gua_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.Guarantees",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_import_DLC")) && this.getAttribute("dual_auth_import_DLC").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("imp_dlc_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ImportDLC",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_SLC")) && this.getAttribute("dual_auth_SLC").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("slc_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.StandbyDLC",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_shipping_guar")) && this.getAttribute("dual_auth_shipping_guar").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("shp_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ShippingGuarantees",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_loan_request")) && this.getAttribute("dual_auth_loan_request").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("lrq_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.LoanRequests",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_request_advise")) && this.getAttribute("dual_auth_request_advise").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("rqa_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.RequestAdvise",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_approval_to_pay")) && this.getAttribute("dual_auth_approval_to_pay").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("atp_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.ApprovaltoPay",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_supplier_portal")) && this.getAttribute("dual_auth_supplier_portal").equals("P")){
			//MEer Rel 8.3 IR -19792-- change attribute supplier_portal_panel_group_oid to supplier_panel_group_oid
			if(StringFunction.isBlank(this.getAttribute("supplier_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.SupplierPortal",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_arm")) && this.getAttribute("dual_auth_arm").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("mtch_apprv_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.MatchingApproval",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_arm_invoice")) && this.getAttribute("dual_auth_arm_invoice").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("inv_auth_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.InvoiceAuthorisation",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_credit_note")) && this.getAttribute("dual_auth_credit_note").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("credit_auth_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.CreditNoteAuthorisation",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_disc_response")) && this.getAttribute("dual_auth_disc_response").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("dcr_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.DiscrepancyResponse",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		//SSikhakolli - Rel-9.4 CR-818 
		if(StringFunction.isNotBlank(this.getAttribute("dual_settle_instruction")) && this.getAttribute("dual_settle_instruction").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("sit_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.SettlementInstructionMsgResponse",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		//Rel 9.0 CR 913 START
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_payables_inv")) && this.getAttribute("dual_auth_payables_inv").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("pay_inv_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.PayablesMgmtInvoiceUpdates",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_upload_pay_inv")) && this.getAttribute("dual_auth_upload_pay_inv").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("upl_pay_inv_panel_group_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.PayablesInvoiceAuthorisation",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		//Rel 9.0 CR 913 END
		
		//Rel 9.2 CR 914 START
		if(StringFunction.isNotBlank(this.getAttribute("dual_auth_pay_credit_note")) && this.getAttribute("dual_auth_pay_credit_note").equals("P")){
			if(StringFunction.isBlank(this.getAttribute("pay_cr_note_auth_panel_grp_oid"))){
				instrumentType = this.getResourceManager().getText("CorpCust.CreditNoteAuthorisation",TradePortalConstants.TEXT_BUNDLE);
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION,instrumentType);
			}
		}
		//Rel 9.2 CR 914 END
	}
	//MEer Rel 8.3 IR_21992- Updated method signature with a boolean to ignore default panel level when required
	//MEer Rel 8.3 IR-19699- Updated method signature with locale and For each panel level, if the panel alias is not defined, it returns the default panel text
	/**
	 * This method has been added for CR 821 - Rel 8.3
	 * This method is intended to return the SelectField Options string. For each of the Panel Levels, it will fetch the corresponding Panel Alias
	 * from Database and display to the user the Panel Aliases instead of Panel Levels.
	 * In case the customer inherits panel level aliases from Parent, we need to fetch the Aliases from the Root Parent, until it reaches the topmost Parent
	 * or until the customer does not inherit from Parent, whichever comes first. 
	 * @param organizationOid
	 * @param selectedOption
	 * @param locale
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */

	public Hashtable getPanelLevelAliases(String organizationOid, String userLocale, boolean ignoreDefaultPanelValue) throws AmsException, RemoteException{
		Hashtable  refDataPanelAliases  = new Hashtable();
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		String panelAliasesSql =
        	"SELECT t.* FROM ( SELECT CONNECT_BY_ROOT PANEL_LEVEL_A_ALIAS AS panelA, CONNECT_BY_ROOT PANEL_LEVEL_B_ALIAS AS panelB,CONNECT_BY_ROOT PANEL_LEVEL_C_ALIAS AS panelC,"
			+" CONNECT_BY_ROOT PANEL_LEVEL_D_ALIAS AS panelD,CONNECT_BY_ROOT PANEL_LEVEL_E_ALIAS AS panelE, CONNECT_BY_ROOT PANEL_LEVEL_F_ALIAS AS panelF,"
			+" CONNECT_BY_ROOT PANEL_LEVEL_G_ALIAS AS panelG, CONNECT_BY_ROOT PANEL_LEVEL_H_ALIAS AS panelH, CONNECT_BY_ROOT PANEL_LEVEL_I_ALIAS AS panelI,"
			+" CONNECT_BY_ROOT PANEL_LEVEL_J_ALIAS AS panelJ FROM corporate_org WHERE organization_oid = ? "
			+ " AND (connect_by_root (INHERIT_PANEL_AUTH_IND)='N' ) CONNECT BY prior organization_oid = p_parent_corp_org_oid ORDER BY level) t WHERE rownum=1";
		
		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(panelAliasesSql,false, new Object[]{organizationOid});
		Hashtable aliasesList = new Hashtable();
		
		refDataPanelAliases = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr("PANEL_AUTH_TYPE", userLocale);
		
		/*IR T36000018991 Adding Null Check -Prateep*/
		if(result!=null){
			DocumentHandler vVector = result.getFragment("/ResultSetRecord(0)");

			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELA"))){
				aliasesList.put("A",vVector.getAttribute("/PANELA"));
			}else if (ignoreDefaultPanelValue){ //MEer Rel 8.3 IR-21992
				aliasesList.put("A","" );		
			}else{
				aliasesList.put("A",refDataPanelAliases.get("A").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELB"))){
				aliasesList.put("B",vVector.getAttribute("/PANELB"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("B","" );		
			}else{
				aliasesList.put("B",refDataPanelAliases.get("B").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELC"))){
				aliasesList.put("C",vVector.getAttribute("/PANELC"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("C","" );		
			}else{
				aliasesList.put("C",refDataPanelAliases.get("C").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELD"))){
				aliasesList.put("D",vVector.getAttribute("/PANELD"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("D","" );		
			}else{
				aliasesList.put("D",refDataPanelAliases.get("D").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELE"))){
				aliasesList.put("E",vVector.getAttribute("/PANELE"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("E","" );		
			}else{
				aliasesList.put("E",refDataPanelAliases.get("E").toString());
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELF"))){
				aliasesList.put("F",vVector.getAttribute("/PANELF"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("F","" );		
			}else{
				aliasesList.put("F",refDataPanelAliases.get("F").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELG"))){
				aliasesList.put("G",vVector.getAttribute("/PANELG"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("G","" );		
			}else{
				aliasesList.put("G",refDataPanelAliases.get("G").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELH"))){
				aliasesList.put("H",vVector.getAttribute("/PANELH"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("H","" );		
			}else{
				aliasesList.put("H",refDataPanelAliases.get("H").toString() );
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELI"))){
				aliasesList.put("I",vVector.getAttribute("/PANELI"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("I","" );		
			}else{
				aliasesList.put("I",refDataPanelAliases.get("I").toString());
			}
			if(StringFunction.isNotBlank(vVector.getAttribute("/PANELJ"))){
				aliasesList.put("J",vVector.getAttribute("/PANELJ"));
			}else if (ignoreDefaultPanelValue){
				aliasesList.put("J","" );		
			}else{
				aliasesList.put("J", refDataPanelAliases.get("J").toString() );
			}


		}else{
			//MEer Rel 8.3 IR-21992
			if(ignoreDefaultPanelValue){
				aliasesList.put("A","");
				aliasesList.put("B","");
				aliasesList.put("C","");
				aliasesList.put("D","");
				aliasesList.put("E","");
				aliasesList.put("F","");
				aliasesList.put("G","");
				aliasesList.put("H","");
				aliasesList.put("I","");
				aliasesList.put("J","");
			}else{
				aliasesList.put("A",refDataPanelAliases.get("A").toString());
				aliasesList.put("B",refDataPanelAliases.get("B").toString());
				aliasesList.put("C",refDataPanelAliases.get("C").toString());
				aliasesList.put("D",refDataPanelAliases.get("D").toString());
				aliasesList.put("E",refDataPanelAliases.get("E").toString());
				aliasesList.put("F",refDataPanelAliases.get("F").toString());
				aliasesList.put("G",refDataPanelAliases.get("G").toString());
				aliasesList.put("H",refDataPanelAliases.get("H").toString());
				aliasesList.put("I",refDataPanelAliases.get("I").toString());
				aliasesList.put("J",refDataPanelAliases.get("J").toString());
			}
		}
		return aliasesList;
	}
	
	
	
	
	// Nar Release 8.3.0.0 IR-T36000019930 Begin
	/**
	 * This method returns whether transaction type (Discrepancy or approval to pay response)  is panel enabled or not.
	 * @param instrumentType - this will have instrument type code i.e.FTDP,LRQ,GUA etc
	 * @param transactionType - this will have transaction type code i.e. ISS, DCR, APR, PAY etc
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public boolean isPanelAuthEnabled(String instrumentType, String transactionType) throws RemoteException, AmsException{
		
		boolean panelAuthEnabled = false;		
		if((TransactionType.DISCREPANCY.equals(transactionType) || TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transactionType)) &&
				!(TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(getAttribute("dual_auth_disc_response")))){
			
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_disc_response"));
			
		} else if((TransactionType.SIR.equals(transactionType) || TransactionType.SIM.equals(transactionType)) &&
				!(TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(getAttribute("dual_settle_instruction")))){
			
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_settle_instruction"));
			
		} else{
			panelAuthEnabled = isPanelAuthEnabled(instrumentType);
		}				
		return panelAuthEnabled;	
	}
	// Nar Release 8.3.0.0 IR-T36000019930 End
	
	// Nar Rel 8.3.0.0 CR 821 16 Jun 2013 Begin
	/**
	 * this method find out whether instrument type is panel enabled or not.
	 * @param instrumentType
	 * @return true if panel authorization is applicable for instrument type
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public boolean isPanelAuthEnabled(String instrumentType) throws RemoteException, AmsException{
		boolean panelAuthEnabled = false;
		
		if (InstrumentType.DOM_PMT.equals(instrumentType))
    		panelAuthEnabled = TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_domestic_payments_panel"));
    	
    	else if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType))
    		panelAuthEnabled = TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_xfer_btwn_accts_panel"));
    	
    	else if (InstrumentType.FUNDS_XFER.equals(instrumentType))
    		panelAuthEnabled = TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_funds_transfer_panel"));
    	
    	else if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_approval_to_pay"));
    	
  	    else if (InstrumentType.AIR_WAYBILL.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_airway_bill"));
    	
  	    else if (InstrumentType.EXPORT_COL.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_export_coll"));
    	
  	    else if (InstrumentType.NEW_EXPORT_COL.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_new_export_coll"));
    	
  	    else if (InstrumentType.EXPORT_DLC.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_export_LC"));
    	
        else if (InstrumentType.IMPORT_DLC.equals(instrumentType) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || 
				instrumentType.equals(InstrumentType.DEFERRED_PAY))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_import_DLC"));
    	
  	    else if (InstrumentType.GUARANTEE.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_guarantee"));
		
  	  else if (InstrumentType.SHIP_GUAR.equals(instrumentType))
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_shipping_guar"));
    	
  	    else if (InstrumentType.STANDBY_LC.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_SLC"));
    	
  	    else if (InstrumentType.LOAN_RQST.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_loan_request"));
    	
  	    else if (InstrumentType.REQUEST_ADVISE.equals(instrumentType))
  			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_request_advise"));
		
  	  else if (TradePortalConstants.SUPPLIER_PORTAL.equals(instrumentType))
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_supplier_portal"));

	  else if (TradePortalConstants.NEW_RECEIVABLES.equals(instrumentType))
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_arm"));
		
  	  else if (TradePortalConstants.INVOICE.equals(instrumentType))
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_arm_invoice"));
		
  	  else if (TradePortalConstants.RECEIVABLES_CREDIT_NOTE.equals(instrumentType))
		panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_credit_note"));
  	 
  	  else if (TradePortalConstants.PAYABLES_PROGRAM_INVOICE.equals(instrumentType))
			panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_upload_pay_inv"));
		
  	  else if (TradePortalConstants.PAYABLES_CREDIT_NOTE.equals(instrumentType))
		panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_auth_pay_credit_note"));
		
  	  else if (InstrumentType.IMPORT_COL.equals(instrumentType))
		panelAuthEnabled = TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(getAttribute("dual_imp_col"));

    	return panelAuthEnabled;
		
	}
	// Nar Rel 8.3.0.0 CR 821 16 Jun 2013 End
	
	
	// jgadela  REL 8.3 IR T36000021109  [START] - If the new settings are changed from Yes to No, the system will now check if there are any Pending record
	/**
	 * this method find out whether where there are any pending reference data records in the que.
	 * @param attributeName
	 * @return true if there are any records
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean checkPendingRecordsInTheQue(String attributeName) throws AmsException, RemoteException{
	      
	      String newDualCtrlVal = this.getAttribute(attributeName);
 	      String oldDualCtrlVal = DatabaseQueryBean.getObjectID("corporate_org", attributeName ,"organization_oid = ?", new Object[]{this.getObjectID()});
 	     
	      if(TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(newDualCtrlVal) && !newDualCtrlVal.equalsIgnoreCase(oldDualCtrlVal)) { 
	    	   //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	            StringBuilder dynamicWhere = new StringBuilder("");
	            List<Object> sqlParams = new ArrayList();
	            dynamicWhere.append(" select count(PENDING_DATA_OID) as COUNT from ref_data_pending a where ");
	            dynamicWhere.append(" a.owner_org_oid = ? ");
	            sqlParams.add(this.getObjectID());
	            
	      
	            if("corp_user_dual_ctrl_reqd_ind".equalsIgnoreCase(attributeName)){
	            	dynamicWhere.append(" and CLASS_NAME = 'User'");
	            }else if("panel_auth_dual_ctrl_reqd_ind".equalsIgnoreCase(attributeName)){
	            	dynamicWhere.append(" and CLASS_NAME = 'PanelAuthorizationGroup'");
	            }
	            //IR 23327 - check the user who modified the object and it should be a Non Admin user.
	            dynamicWhere.append(" and CHANGE_USER_OID in ("+getNonAdminUserObjectsSql()+")");
	            sqlParams.add(this.getObjectID());
	            
	            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet( dynamicWhere.toString(), false, sqlParams);
	            int count = resultSet.getAttributeInt("/ResultSetRecord(0)/COUNT");
	            if(count > 0) {
	                  return true;
	            }
	      }
	       return false;
	  }
	// jgadela  REL 8.3 IR T36000021109  [START]
	
	
	/* MEer Rel 8.3 IR-T36000022048 validations on my Organization profile page */
	private void validateMyOrganizationProfile() throws RemoteException, AmsException{
		LOG.info("Save Request is coming from My Org. Profile page, just do below validations...");
		// MEerupula Rel8.3 CR776
		try {
			
			//T36000024113 Rel 8.4 01/15/014 RKAZI - ADD -  moved required check here Since this field is required only from My Org Progile screen
	         if (StringFunction.isBlank(this.getAttribute("pmt_bene_sort_default"))) {
	              this.getErrorManager ().issueError(
	                      getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
	                      this.resMgr.getText("OrgProfileDetail.PmtBeneView", TradePortalConstants.TEXT_BUNDLE));
	          }
	       //T36000024113 Rel 8.4 01/15/014 RKAZI - End
	         
			// MEerupula Rel8.3 CR821
			String parent_corp_org_oid    = getAttribute("parent_corp_org_oid");
			if(StringFunction.isBlank(parent_corp_org_oid) ){
				setAttribute("inherit_panel_auth_ind", TradePortalConstants.INDICATOR_NO);	
			}else{
				if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("inherit_panel_auth_ind"))){
					//If the panel values are inherited from parent-- set the panel values to nulll for current/child user
					setAttribute("panel_level_A_alias", null);
					setAttribute("panel_level_B_alias", null);
					setAttribute("panel_level_C_alias", null);
					setAttribute("panel_level_D_alias", null);
					setAttribute("panel_level_E_alias", null);
					setAttribute("panel_level_F_alias", null);
					setAttribute("panel_level_G_alias", null);
					setAttribute("panel_level_H_alias", null);
					setAttribute("panel_level_I_alias", null);
					setAttribute("panel_level_J_alias", null);
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new AmsException(e.getMessage());
		}
		
		
	}	
	
	/* MEer Rel 8.3 IR-T36000022048 separated out the validations on Corporate Organization Customer profile page to new method  */
	private void validateCorpCustomerProfile() throws RemoteException, AmsException{
		
		this.setOperationalBankOrgs();

		String orgOID = null;
		String pclientbankOID = null;
		String parentOID = null;
		String bankorggroupOID = null;
		String reportingUserSuffix = null;

		String reportingUserType = null; //Pratiksha CR-451 CM Reporting
		String clientbankOID = null; //Pratiksha CR-451 CM Reporting


		String activation_status = null; //rbhaduri - 10 Jun 10 - IR KSUK050849844
		//IR KSUK050849844 Begin
		try {

			activation_status = getAttribute("activation_status");
			if(isDuplicateReportCategs()){
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DUPLICATE_REPT_CATEGS);
			} // Suresh CR-603 03/14/2011 Add---Narayan ALUL061670572

		} catch (RemoteException e) {
				e.printStackTrace();
				throw new AmsException(e.getMessage());
		}
		//IR KSUK050849844 End

		if (isNew) {




			try {

				// Get the reporting user suffix from the Global Org to which this org belongs
				reportingUserSuffix = getReportingSuffix();

				// Place the reporting user suffix on to the beginning of the OID
				// the suffix is used to distinguish between organizations being processed
				// by different ASPs that might be on the same reports server.

				orgOID = getAttribute("organization_oid");
				pclientbankOID = getAttribute("parent_corp_org_oid");
				bankorggroupOID = getAttribute("bank_org_group_oid");


				reportingUserType = getAttribute("reporting_type"); //Pratiksha CR-451 CM Reporting

				clientbankOID = getAttribute("client_bank_oid"); //Pratiksha CR-451 CM Reporting

				//If parent corp org oid is null then the corporate customer parent is
				//BOG. If parent corp org oid is not null then parent is corporate customer.
				if (StringFunction.isNotBlank(getAttribute("parent_corp_org_oid"))) {
					parentOID = pclientbankOID;
				} else {
					parentOID = bankorggroupOID;
				}

				//The following method will generate Supervisor commands to
				//add new corporate customer based on the parent and child oid's
				//Pratiksha modified below function for CM Reporting

				generateCorpOrgSupervisorCmd(
					orgOID,
					parentOID,
					reportingUserSuffix,
					reportingUserType,
					clientbankOID); //Pratiksha added xtra parameter
			} catch (RemoteException e) {
				e.printStackTrace();
				throw new AmsException(e.getMessage());
			}

		} else {



			try {

				String oldBankOrgOid = 	this.getAttributeOriginalValue("bank_org_group_oid");
				String newBankOrgOid = this.getAttribute("bank_org_group_oid");

				//Pratiksha change begin 9-Mar-09

				String oldReportingTypeOid = this.getAttributeOriginalValue("reporting_type");
				String newReportingTypeOid = this.getAttribute("reporting_type");

				boolean reportingTypeUpdated = false;

				//Pratiksha change end 9-Mar-09

				boolean bankOrgUpdated = false;

				if (!newBankOrgOid.equals(oldBankOrgOid)) {
					bankOrgUpdated = true;
				}

				//Pratiksha change begin 9-Mar-09

				if (!newReportingTypeOid.equals(oldReportingTypeOid)) {
					reportingTypeUpdated = true;
				}

				//Pratiksha change end 9-Mar-09

				//If the parent corporate or bank org has changed then
				//update the corresponding Supervisor user hierarchy

				String updatedEntityType = null;

				// Get the reporting user suffix from the Global Org to which this org belongs
				reportingUserSuffix = getReportingSuffix();

				//rbhaduri - 10 Jun 10 - IR KSUK050849844 - Add Begin


				if (bankOrgUpdated){
					updatedEntityType = "BOG";
				}
				else {
					updatedEntityType = "CC";
				}
				//rbhaduri - 10 Jun 10 - IR KSUK050849844 - Add End


				//Update the Supervisor reporting user hierarchy

				updateReportingUsers(reportingUserSuffix, updatedEntityType, activation_status); //rbhaduri - 10 Jun 10 - IR KSUK050849844 - added exra argument

				//Pratiksha change begin 9-Mar-09

				if (reportingTypeUpdated) {
					// Get the reporting user suffix from the Global Org to which this org belongs
					reportingUserSuffix = getReportingSuffix();
					//Update the Supervisor reporting user hierarchy in xx publishing group
					modifyReportingUsers(reportingUserSuffix);
				}
				//Pratiksha change end 9-Mar-09
		                        
		                        // CR-708B if SPMarginRuleList has changed, create InvoiceFinanceAmountQueue to update
		                        // Supplier Portal Invoices' Net Amount Offer.
		                        ComponentList spMarginRuleList = (ComponentList)this.getComponentHandle("SPMarginRuleList");
		                        if (spMarginRuleList.hasChangesPending()) {
		                            InvoiceFinanceAmountQueue invoiceFinanceAmountQueue  = (InvoiceFinanceAmountQueue) createServerEJB("InvoiceFinanceAmountQueue");
		                            invoiceFinanceAmountQueue.newObject();
		                            invoiceFinanceAmountQueue.setAttribute("creation_timestamp", DateTimeUtility.getCurrentDateTime());
		                            invoiceFinanceAmountQueue.setAttribute("status", TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_PENDING);
		                            invoiceFinanceAmountQueue.setAttribute("corp_org_oid", getAttribute("organization_oid"));
		                            invoiceFinanceAmountQueue.setAttribute("margin_rule_list_type", "SPMarginRuleList");
		                            invoiceFinanceAmountQueue.save();
		                        }

			} catch (RemoteException e) {
				e.printStackTrace();
				throw new AmsException(e.getMessage());
			}

		}

		/*
		 * If the authentication method is changing from per user to password or
		 * certificate, NULL out the users' authentication method. If the
		 * authentication method changes from password or certificate to per
		 * user, set the users' authentication method to the old authentication
		 * method of the corporate organizaiton.  Note this does not handle the
		 * force_certs_for_user_maint attribute.
		 */
		try {
			//change fx_rate_group to uppercase
			setAttribute("fx_rate_group",getAttribute("fx_rate_group").toUpperCase());  //  CR-469 NShrestha 05/18/2009

			if (isAttributeModified("authentication_method")) {
				String authMethod = this.getAttribute("authentication_method");
				String oldAuthenticationMethod =
					this.getAttributeOriginalValue("authentication_method");
				String newUserAuthenticationMethod = null;

				// Check the change to authentication method.
				// Determine if and what the users' authentication method should be set to.
				if (oldAuthenticationMethod
					.equals(TradePortalConstants.AUTH_PERUSER)
					&& !authMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
					newUserAuthenticationMethod = "";
				} else if (
					!oldAuthenticationMethod.equals(
						TradePortalConstants.AUTH_PERUSER)
						&& authMethod.equals(TradePortalConstants.AUTH_PERUSER)) {
					newUserAuthenticationMethod = oldAuthenticationMethod;
				}

				// Update the users' authentication method if needed.
				if (newUserAuthenticationMethod != null) {
					ComponentList userList =
						(ComponentList) this.getComponentHandle("UserList");
					int userCount = userList.getObjectCount();
					String userID;

					for (int userIndex = 0;
						userIndex < userCount;
						userIndex++) {
						userList.scrollToObjectByIndex(userIndex);
						userID = userList.getListAttribute("user_oid");
						userList.setAttribute(
							"/(" + userID + ")/authentication_method",
							newUserAuthenticationMethod);
					}
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new AmsException(e.getMessage());
		}
//Leelavathi CR-710 Rel-8.0 06/12/2011 Begin
		/*If "Invoices not uploaded from files" radio button is selected,
		 then all the buttons if selected in this section is automatically deselected including Text box.  */
		try{
			String uploadInvoices = getAttribute("allow_invoice_file_upload");
			if((TradePortalConstants.INVOICES_NOT_UPLOAD_FILE).equals(uploadInvoices)){
			      setAttribute("allow_grouped_invoice_upload", null);
			      setAttribute("allow_restricted_inv_upload", null);
			      setAttribute("restricted_inv_upload_dir", null);
			      setAttribute("interest_discount_ind", null);
			      setAttribute("calculate_discount_ind", null);
			   // DK IR - T36000011610 Rel8.2 02/14/2013 Starts
			      setAttribute("failed_invoice_indicator", null);
			      // DK IR - T36000011610 Rel8.2 03/13/2013 Ends
			   // DK IR - T36000014465 Rel8.2 02/14/2013 Starts
			      setAttribute("allow_auto_atp_inv_only_upld", null);
			      setAttribute("allow_auto_loan_req_inv_upld", null);
			      // DK IR - T36000014465 Rel8.2 03/13/2013 Ends
			      //Rel9.0 CR 913 Starts
			      setAttribute("invoice_file_upload_pay_ind", TradePortalConstants.INDICATOR_NO);
			      setAttribute("invoice_file_upload_rec_ind", TradePortalConstants.INDICATOR_NO);
			      setAttribute("pay_invoice_grouping_ind", TradePortalConstants.INDICATOR_NO);
			      setAttribute("pay_invoice_utilised_ind", TradePortalConstants.INDICATOR_NO);
			      setAttribute("rec_invoice_utilised_ind", TradePortalConstants.INDICATOR_NO);
			      //Rel9.0 CR 913 Ends
			}
			/*If "Invoices can be uploaded from files" is selected then user must select a radio button under
			 "Group Receivables", if not system will throw error.*/
			else if((TradePortalConstants.INVOICES_UPLOAD_FILE).equals(uploadInvoices)){

				  if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("invoice_file_upload_pay_ind")) &&
						  TradePortalConstants.INDICATOR_NO.equals(getAttribute("invoice_file_upload_rec_ind"))) {
					  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ERROR_SELECT_INVOICES_UPLOAD_FILE_TYPE);
				  }
				  String allowGroupedInvoiceUpload 	= getAttribute("allow_grouped_invoice_upload");
				  /* T36000029301 - If Receivables invoices is selected, then user must select atleast one group from 
				   * Trading Parter Grouping/Not-Grouping options. */ 
				  if( TradePortalConstants.INDICATOR_YES.equals(getAttribute("invoice_file_upload_rec_ind")) ){
					  if(!TradePortalConstants.INVOICES_GROUPED_TPR.equals(allowGroupedInvoiceUpload)
				    		  && !TradePortalConstants.INVOICES_NOT_GROUPED_TPR.equals(allowGroupedInvoiceUpload) ){
				            this.getErrorManager().issueError(
				                                    TradePortalConstants.ERR_CAT_1,
				                                    TradePortalConstants.MISSING_INVOICE_GROUP_INDICATOR);
				      }
				  }						  
				  /*T36000029301 : If Payables invoices is selected, then user must select atleast one payables invoices grouping*/
				  if(TradePortalConstants.INDICATOR_YES.equals(getAttribute("invoice_file_upload_pay_ind"))) {
					  if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("pay_invoice_grouping_ind")) &&
								TradePortalConstants.INDICATOR_NO.equals(getAttribute("pay_invoice_utilised_ind"))) {							
									this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.MISSING_PAYABLES_INVOICE_GROUP_INDICATOR);
						}
				  }
			      /*If "Invoices can be uploadedd from files ", is selected, then user must select a radio button under
			       "Restricted Invoice upload directory", otherwise system will throw error.  */
			      String allowRestrictedInvoiceUpload = getAttribute("allow_restricted_inv_upload");
			      if(!TradePortalConstants.INVOICE_UPLOAD_NOT_RESTRICTED_DIRECTORY.equals(allowRestrictedInvoiceUpload)
			    		  && !TradePortalConstants.INVOICE_UPLOAD_RESTRICTED_DIRECTORY.equals(allowRestrictedInvoiceUpload)){
			            this.getErrorManager().issueError(
			                                    TradePortalConstants.ERR_CAT_1,
			                                    TradePortalConstants.MISSING_SPECIFIC_RESTRICTED_DIRECTORY_INDICATOR);

			      }
			      /*If "Invoices must be restricted to a specific directory/drive" is selected, then user must enter a directory/drive
			       * otherwise system will throw error upon "save". */
			      else if(TradePortalConstants.INVOICE_UPLOAD_RESTRICTED_DIRECTORY.equals(allowRestrictedInvoiceUpload) ){
			    	  String restrictedInvoiceUploadDir=getAttribute("restricted_inv_upload_dir");
			    	  if( restrictedInvoiceUploadDir == null || "".equals(restrictedInvoiceUploadDir.trim())){

		                  this.getErrorManager().issueError(
		                                    TradePortalConstants.ERR_CAT_1,
		                                    TradePortalConstants.MISSING_RESTRICTED_INVOICE_UPLOAD_DIRECTORY);
					   }
			    	  /*If "Invoice uploads do not need to be restricted to a specific directory" is selected, then noo need to enter
			    	   * drive information on text field, if anything is entered, system will automatically clear that information.  */
		          }else if(TradePortalConstants.INVOICE_UPLOAD_NOT_RESTRICTED_DIRECTORY.equals(allowRestrictedInvoiceUpload) ){
		                  setAttribute("restricted_inv_upload_dir",null);
		          }
		        //Srinivasu IR-KIUL121454215 12/28/2011 Start
				// If a user didn't selected either a "Straight Discount" or "Discount to Yield" calculation method and also
			    // didn't selected "Interest collected separately and added to finance amount" or " Discount netted from finance amount"
			    // then the system will return the error:
			    // The "Invoices can be uploaded from files" indicator has been selected. Please indicate if Interest or Discount will be used.

			   
				//Rel9.0 CR 913 Start
			   if(TradePortalConstants.INDICATOR_YES.equals(getAttribute("pay_invoice_grouping_ind")) ||
						TradePortalConstants.INDICATOR_YES.equals(getAttribute("pay_invoice_utilised_ind"))) {
					if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("invoice_file_upload_pay_ind"))) {
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ERROR_SELECT_PAYABLE_INVOICES_UPLOAD_FILE);
					}
				}
				if(TradePortalConstants.INDICATOR_YES.equals(getAttribute("rec_invoice_utilised_ind"))) {
					if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("invoice_file_upload_rec_ind"))) {
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ERROR_SELECT_RECEIVABLE_INVOICES_UPLOAD_FILE);
					}
				}else if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("rec_invoice_utilised_ind"))){
					if(TradePortalConstants.INDICATOR_NO.equals(getAttribute("invoice_file_upload_rec_ind"))) {
						setAttribute("interest_discount_ind", null);
						setAttribute("calculate_discount_ind", null);
					}else if(TradePortalConstants.INDICATOR_YES.equals(getAttribute("invoice_file_upload_rec_ind"))) {
						if(StringFunction.isNotBlank(getAttribute("interest_discount_ind")) && StringFunction.isNotBlank(getAttribute("calculate_discount_ind"))){
							//if the 'Receivables Program Utilised' setting is not selected, but the user has selected the Radio Buttons beneath it, 
							//the system will automatically select the 'Receivables Program Utilised' checkbox upon save.  
							setAttribute("rec_invoice_utilised_ind", TradePortalConstants.INDICATOR_YES);
						}
					}
				}
				//Rel9.0 CR 913 End

			 }
			 //User selected 'Interest Collected seperately',but Calculate discount values are not resetting and handling here
			 String interestDiscIndicator = getAttribute("interest_discount_ind");
			 String calculatedDiscIndicator = getAttribute("calculate_discount_ind");

			 //Srinivasu IR-KIUL121454215 12/28/2011 End
			/*If "Discount netted from finance amount" is selected then user must select any one of the radio button under
			 *  "Calculate Discount Using", otherwise system will throw error. */

			 if(TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT.equals(interestDiscIndicator)){
			      if(!(TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculatedDiscIndicator) ||
			    		   TradePortalConstants.DISCOUNT_YIELD.equals(calculatedDiscIndicator)) ){
			            this.getErrorManager().issueError(
			                                    TradePortalConstants.ERR_CAT_1,
			                                    TradePortalConstants.MISSING_DISCOUNT_CAT);
			      }
			 }
			 /*If a user selected any one of the radio button under "Calculate Discount Using" option not selected the
			 * "Discount netted from finance amount" then system will automatically select the "Discount netted from
			 * finance amount" button. */

			if(TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculatedDiscIndicator)
					|| TradePortalConstants.DISCOUNT_YIELD.equals(calculatedDiscIndicator) ){

				if( !TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT.equals(interestDiscIndicator)
						&& !TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT.equals(interestDiscIndicator)){

				    setAttribute("interest_discount_ind", TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT);
				    /*If any of the "Calculate Discount Using" button is selected and user has selected "Interest collected from finance amount"
				     * button has selected then system will return the error and clear out any of the "Calculate Discount Using" button is selected.*/
		        }else if(TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT.equals(interestDiscIndicator)){
		    	  	setAttribute("calculate_discount_ind",null);
					this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants. DISCOUNT_IS_NOT_APPLICABLE);
				}
			  }

			//Leelavathi CR-707 Rel800 14th Feb 2012 Begin
			//Leelavathi CR-707 Rel800 5th Mar 2012 End
			} catch (RemoteException e) {
			e.printStackTrace();
			throw new AmsException(e.getMessage());
		}
			//Leelavathi CR-710 Rel 8.0 06/12/2011 End

		try{
			//Leelavathi CR-707 Rel800 14th Feb 2012 Begin

			/*If "Purchase Order uploads must be restricted to a specific directory/drive" is selected, then user must enter a directory/drive
		     * otherwise system will throw error upon "save". */
			String restrictedPoUploadDirInd=getAttribute("allow_restricted_po_upload");
			String restrictedPoUploadDir=getAttribute("restricted_po_upload_dir");
			if(TradePortalConstants.PURCHASE_ORDER_RESTRICTED_DIRECTORY.equals(restrictedPoUploadDirInd) ){
		    	  if( restrictedPoUploadDir == null || "".equals(restrictedPoUploadDir.trim())){
		              this.getErrorManager().issueError(
		                                TradePortalConstants.ERR_CAT_1,
		                                TradePortalConstants.MISSING_RESTRICTED_PO_UPLOAD_DIRECTORY);
				}
			  /*If "Purchase Order uploads do not need to be restricted to a specific directory/drive" is selected, then no need to enter
			   * drive information on text field, if anything is entered, system will automatically select
			   * "Purchase Order uploads must be restricted to a specific directory/drive" radio button.  */
			   //Srinivasu_D Rel8.0 IR# BUM030950033 04/02/2012 Start
		    }else if( restrictedPoUploadDir != null && restrictedPoUploadDir.trim().length()>0){
		       //setAttribute("allow_restricted_po_upload",TradePortalConstants.PURCHASE_ORDER_RESTRICTED_DIRECTORY);
					this.getErrorManager().issueError(
		                        TradePortalConstants.ERR_CAT_1,
		                        TradePortalConstants.PO_DEF_UPLOAD_DRIVE_SELECTION);
		    }
			 //Srinivasu_D Rel8.0 IR# BUM030950033 04/02/2012 End
			//Srinivasu_D Rel8.0 IR# BEUM030834162 03/27/2012 Start
			String allowPurchaseOrderUpload = getAttribute("allow_purchase_order_upload");
			String poUploadFormatInd = getAttribute("po_upload_format");
			if(TradePortalConstants.INDICATOR_YES.equals(allowPurchaseOrderUpload)){
				if(StringFunction.isBlank(poUploadFormatInd)){
					this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.MISSING_PO_SELECTION);
				}
			}
			//Srinivasu_D Rel8.0 IR# BEUM030834162 03/27/2012 End

			/*Upon save of the customer, when a user selects one of these radio buttons, if customer had previously been using the other method of PO upload,
			 * the user will be presented with a warning message: �Warning: any Purchase Order uploaded via the other method will no longer be accessible or
			 * available to the Customer�. */
			if(!isNew){
				 poUploadFormatInd = getAttribute("po_upload_format");
				String oldPOUploadFormatInd = this.getAttributeOriginalValue("po_upload_format");
				if( oldPOUploadFormatInd !=null && oldPOUploadFormatInd.trim().length()>0 && !oldPOUploadFormatInd.equals(poUploadFormatInd)){
					this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.PO_UPLOAD_METHOD_WARN);
				}
			}
			//Leelavathi CR-707 Rel800 22nd Feb 2012 End
			//Leelavathi CR-707 Rel800 5th Mar 2012 Begin

			 allowPurchaseOrderUpload = getAttribute("allow_purchase_order_upload");
			if(TradePortalConstants.INDICATOR_YES.equals(allowPurchaseOrderUpload)){
				if( !TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_auto_lc_create"))
						&& !TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_manual_po_entry"))
						&& !TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_auto_atp_create"))
						&& !TradePortalConstants.INDICATOR_YES.equals(getAttribute("allow_atp_manual_po_entry"))
				){
					this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.MISSING_PO_UPLOAD_FROM_LC_AND_ATP);
				}
			}
			//Leelavathi CR-707 Rel800 5th Mar 2012 End
			} catch (RemoteException e) {
			e.printStackTrace();
			throw new AmsException(e.getMessage());
		}
			//Leelavathi CR-710 Rel 8.0 06/12/2011 End

			// Srinivasu_D CR-708 Rel8.1 05/11/2012 Start
			try {

			  String atpLoanIndicator    = getAttribute("atp_invoice_indicator");		//process inv thru file upload by ATP
			  String uploadInvoices		 = getAttribute("allow_invoice_file_upload");
			  String loanReqIndicator    = getAttribute("process_invoice_file_upload");		// DK CR709 Rel 8.2 process inv thru file upload by Loan Request

			 //This validation is if ATP Request is selected but none of radio buttons not selected under this group throwing error
			
			//Invoice upload from files seleted but Process invoice thru files by LOAN REQUEST or ATP not selected
			  if(TradePortalConstants.INDICATOR_YES.equals(atpLoanIndicator)){
					if(TradePortalConstants.INVOICES_NOT_UPLOAD_FILE.equals(uploadInvoices)){

						this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.PROCESS_INV_FILES_REQUIRED);
						}
					}

			  // DK CR709 Rel8.2 Starts
				//Invoice upload from files seleted but Process invoice thru files by LOAN REQUEST not selected
				  if(TradePortalConstants.INDICATOR_YES.equals(loanReqIndicator)){
						if(TradePortalConstants.INVOICES_NOT_UPLOAD_FILE.equals(uploadInvoices)){

							this.getErrorManager().issueError(
		                        TradePortalConstants.ERR_CAT_1,
		                        TradePortalConstants.LOAN_REQUEST_PROCESS_INV_FILES_REQUIRED);
							}
						}
			  // DK CR709 Rel8.2 Ends

			String daysBefore        = getAttribute("days_before");
			String daysAfter         = getAttribute("days_after");
			String paymentDayAllow   = getAttribute("payment_day_allow");

			if(TradePortalConstants.INDICATOR_YES.equals(atpLoanIndicator)){
				if(TradePortalConstants.INDICATOR_YES.equals(paymentDayAllow) &&
					(!StringFunction.isNotBlank(daysBefore) || !StringFunction.isNotBlank(daysAfter)))
				{
					this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.ATP_INVOICE_RULES_REQUIRED);
				}
			 }
			} catch (RemoteException e) {
					e.printStackTrace();
				throw new AmsException(e.getMessage());
		}

			// Srinivasu_D CR-708 Rel8.1.1 05/11/2012 End
			
			// Srinivasu_D CR-599 Rel8.4  11/21/2013 Start

			String dirLocInd         = getAttribute("man_upld_pmt_file_dir_ind");
			String dirLocation		 = getAttribute("restricted_pay_mgmt_upload_dir");

			

			if(StringFunction.isBlank(dirLocation) && TradePortalConstants.INDICATOR_YES.equals(dirLocInd))
				{
					this.getErrorManager().issueError(
		                    TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.PAY_MGMT_DIR_REQUIRED);
				}			
		
				 
			
	}
	
	protected boolean findDuplicatePayDefinitions() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{

		String payDefOid = "";
		boolean errorFlag=false;

		ComponentList marginList = (ComponentList)this.getComponentHandle("UserPreferencesPayDefList");		
		int marginsCount = marginList.getObjectCount();
		Set<String> payDefSet = new HashSet<String>();

		for(int i=0; i< marginsCount; i++)
		{

			marginList.scrollToObjectByIndex(i);
			UserPreferencesPayDef payDefList = (UserPreferencesPayDef)marginList.getBusinessObject();
			payDefOid = payDefList.getAttribute("a_pay_upload_definition_oid");

			if(StringFunction.isNotBlank(payDefOid) && !payDefSet.add(payDefOid)){

				errorFlag=true;			    		
			}	
		}
		return errorFlag;
	} 
	    
	    // Srinivasu_D CR-599 Rel8.4  11/21/2013 End
	
		/*Kyriba CR 268 - Validate External Bank Details for unique*/
		private void validateExternalBank() throws RemoteException, AmsException{
			
			ComponentList externalBankComponenet = (ComponentList) this.getComponentHandle("ExternalBankList");
			int extBankTotal = externalBankComponenet.getObjectCount();
			BusinessObject externalBankBusinessObj = null;
			List<String> tempList = new ArrayList<String>();
			Set<String> tempSet = new HashSet<String>();
			
			for(int i=0; i<extBankTotal; i++){
				
				externalBankComponenet.scrollToObjectByIndex(i);
				externalBankBusinessObj = externalBankComponenet.getBusinessObject();
				tempList.add(externalBankBusinessObj.getAttribute("op_bank_org_oid"));
				
				
			}
			tempSet.addAll(tempList);
			if(tempList.size() != tempSet.size()){
				
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DUPLICATE_EXTERNAL_BANKS);
			}
		}
		
		
	/** IR 23327
 	 * this method build the query for non adminusers check pending refdata record.
 	 * @param 
 	 * @return String - sql string
 	 */
     private String getNonAdminUserObjectsSql() {
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
              return "select a.user_oid from users a where activation_status = 'ACTIVE'  and a.p_owner_org_oid = ?  and (a.ownership_level = '"+TradePortalConstants.OWNER_CORPORATE+"')";
     }
	
     private void validatePayableCreditNote(){

    	 // if credit note can be uploaded, then days must be available

    	 try {

    		 String canUpload = this.getAttribute("allow_pay_credit_note_upload");
    		 String days = this.getAttribute("pay_credit_note_available_days");

    		 if (StringFunction.isNotBlank(canUpload) && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(canUpload) &&
    				 StringFunction.isBlank(days)){

    			 this.getErrorManager().issueError(
    					 TradePortalConstants.ERR_CAT_1,
    					 TradePortalConstants.UNUTILIZED_CREDIT_NOTE_AVAILABLE_DAYS_REQUIRED);
    		 }


    		 if(!StringService.isDigit(days)){
    			 this.getErrorManager().issueError(
    					 TradePortalConstants.ERR_CAT_1,
    					 TradePortalConstants.UNUTILIZED_CREDIT_NOTE_AVAILABLE_DAYS_INVALID);
    		 }else if(StringFunction.isNotBlank(days)){

    			 int validDays = Integer.parseInt(days);

    			 if (validDays <=0){

    				 this.getErrorManager().issueError(
    						 TradePortalConstants.ERR_CAT_1,
    						 TradePortalConstants.UNUTILIZED_CREDIT_NOTE_AVAILABLE_DAYS_INVALID);

    			 }
    		 }


    		 String endToEnd= this.getAttribute("allow_end_to_end_id_process");
    		 String manual = this.getAttribute("allow_manual_cr_note_apply");
    		 String applicationType = this.getAttribute("apply_pay_credit_note");


    		 if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(canUpload) && StringFunction.isNotBlank(manual)){

    			 if (StringFunction.isBlank(applicationType)){

    				 this.getErrorManager().issueError(
    						 TradePortalConstants.ERR_CAT_1,
    						 TradePortalConstants.CREDIT_NOTE_PROPORTINATE_SEQ_MANDATORY);
    			 }
    		 }



    		 if (StringFunction.isNotBlank(endToEnd) && StringFunction.isNotBlank(manual) 
    				 && TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(endToEnd) &&
    				 TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(manual)){

    			 this.getErrorManager().issueError(
    					 TradePortalConstants.ERR_CAT_1,
    					 TradePortalConstants.CREDIT_NOTE_MANUAL_ID_PROCESS_MUTUALLY_EXCL);
    		 }



    		 //'Manual Credit Note Application' must be selected for applying credit note 'Proportionately' or 'Sequentially'

    		 if (StringFunction.isBlank(manual) ||TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(manual)){

    			 if (StringFunction.isNotBlank(applicationType) ){
    				 this.getErrorManager().issueError(
    						 TradePortalConstants.ERR_CAT_1,
    						 TradePortalConstants.CREDIT_NOTE_MANUAL_NOT_AVAILBLE);
    			 }
    		 }

    		 String sequentialType =this.getAttribute("pay_credit_sequential_type"); 

    		 //One of the sequential methods must be selected when credit notes are applied sequentially		 

    		 if (TradePortalConstants.CREDIT_NOTE_APPLICATION_SEQUENTIAL.equalsIgnoreCase(applicationType)){

    			 if (StringFunction.isBlank(sequentialType)){
    				 this.getErrorManager().issueError(
    						 TradePortalConstants.ERR_CAT_1,
    						 TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_METHOD_REQUIRED);
    			 }
    		 }
    		 
    		   
    		 
    		  String crNoteDualAuth = this.getAttribute("dual_auth_pay_credit_note");
    		  String invDualAuth = this.getAttribute("dual_auth_payables_inv");
    		  String isCrNoteAllowed = this.getAttribute("allow_pay_credit_note");
    				  
    		  if (TradePortalConstants.INDICATOR_YES.equals(isCrNoteAllowed) && !TradePortalConstants.DUAL_AUTH_NOT_REQ.equals(crNoteDualAuth)){
    			  
    			  if (StringFunction.isNotBlank(crNoteDualAuth)&&StringFunction.isNotBlank(invDualAuth) && !crNoteDualAuth.equals(invDualAuth) ){
     				 
    				  this.getErrorManager().issueError(
    						 TradePortalConstants.ERR_CAT_1,
    						 TradePortalConstants.CREDIT_NOTE_INV_AUTH_NOT_MATCHING);

    			  }
    			  
    		  }
    		  
    		  

    	 } catch (RemoteException e) {
    		 // TODO Auto-generated catch block
    		 e.printStackTrace();
    	 } catch (AmsException e) {
    		 // TODO Auto-generated catch block
    		 e.printStackTrace();
    	 }

     }
     
     // Nar CR-1029 06/11/2015 Rel 9.4
     /**
      * This method is used to check whether customer is integrated with TPS or not.
      * @return
      * @throws RemoteException
      * @throws AmsException
      */
     public boolean isCustNotIntegratedWihTPS() throws RemoteException, AmsException {
    	 
    	return TradePortalConstants.INDICATOR_YES.equals( this.getAttribute("cust_is_not_intg_tps") );  
     }
     
     /**
      * Rel9.5 CR-927B This method fetches email addresses of userOids, based on InstrumentType & TransactionType from Notification rules
      * @param instrumentType
      * @param transactionType
      * @param notifyRuleOid
      * @return
      */
     public String fetchEmailAddresses(String instrumentType, String transactionType, String notifyRuleOid) throws RemoteException, AmsException{
    	 StringBuilder emailAddresses = new StringBuilder();
    	 if(StringFunction.isNotBlank(notifyRuleOid)){
	 			 String fetchUserOidsSql = "select p_user_oid from notify_rule_user_list "
	 			  		+ "where p_criterion_oid="
	 			  		+ "(select criterion_oid from notification_rule_criterion where p_notify_rule_oid=? and instrument_type_or_category=? and transaction_type_or_action=?) ";
	 		                    	
	 			List resultVector = new ArrayList();
	 			int userCount = 0;
	 			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(fetchUserOidsSql,
	 					false, new Object[]{notifyRuleOid, instrumentType, transactionType});
	 			if ((resultXML != null) && (resultXML.getFragments("/ResultSetRecord/").size() > 0)) {
	
					resultVector = resultXML.getFragmentsList("/ResultSetRecord/");
					userCount = resultVector.size();
	 			}
	 			
	 			for(int i=0;i<userCount;i++){
	 				DocumentHandler userListDoc = (DocumentHandler) resultVector.get(i);
	 				String userOid = userListDoc.getAttribute("/P_USER_OID");
	 				String fetchUserEmailSql = "select email_addr from users where user_oid=?";
	 				resultXML = DatabaseQueryBean.getXmlResultSet(fetchUserEmailSql,
	 	 					false, new Object[]{userOid});
	 				if(resultXML!=null){
	 					emailAddresses = emailAddresses.append(resultXML.getAttribute("/ResultSetRecord(0)/EMAIL_ADDR"));
	 				}
	 				if(StringFunction.isNotBlank(emailAddresses.toString()) && i<userCount-1){
	 					emailAddresses = emailAddresses.append(",");
	 				}
	 	 			  		
	 			}
	 			String fetchAddlnEmailSql = "select additional_email_addr from notification_rule_criterion where p_notify_rule_oid=? and instrument_type_or_category=? and transaction_type_or_action=?";
	 			resultXML = DatabaseQueryBean.getXmlResultSet(fetchAddlnEmailSql,
		 					false, new Object[]{notifyRuleOid, instrumentType, transactionType});
					if(resultXML!=null){
						if(StringFunction.isNotBlank(emailAddresses.toString())){
							emailAddresses = emailAddresses.append(",");
						}
						emailAddresses = emailAddresses.append(resultXML.getAttribute("/ResultSetRecord(0)/ADDITIONAL_EMAIL_ADDR"));
					}
	 			LOG.info("CorporateOrganization::fetchEmailAddresses::Email Addresses - {}",emailAddresses.toString());
	 			
	 	 }

 		return emailAddresses.toString();
    	 
     }
     
     /**
      * Rel9.5 CR927B - This method fetched notification rule oid for the corporate customer
      * @return
      * @throws RemoteException
      * @throws AmsException
      */
     public String fetchNotificationRule() throws RemoteException, AmsException{
    	 
    	 String notifRuleOid = null;
    	 String fetchNotifRuleSql = "select notification_rule_oid from notification_rule where p_owner_org_oid=?";
    	 DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(fetchNotifRuleSql,
					false, new Object[]{this.getAttribute("organization_oid")});
			if(resultXML!=null){
				notifRuleOid = resultXML.getAttribute("/ResultSetRecord(0)/NOTIFICATION_RULE_OID");
			}
			LOG.info("CorporateOrganization::fetchNotificationRule::NotificationRuleOid- {}",notifRuleOid);
    	 return notifRuleOid;
     }
     
     /**
      * Rel9.5 CR927B - This method fetches instrumentType based on related Instrument
      * @param origInstrumentType
      * @param relatedInstrumentOid
      * @return
      * @throws NumberFormatException
      * @throws RemoteException
      * @throws AmsException
      */
     public String fetchRelatedInstrumentType(String origInstrumentType, String relatedInstrumentOid) throws NumberFormatException, RemoteException, AmsException{
    	 LOG.info("CorporateOrganization::fetchRelatedInstrumentType::origInstrumentType- {}",origInstrumentType);
    	 	String instrumentType = null;
			
			// The following instrument are created from another instrument, therefore we need to get the
			// instrument type of the related instrument in order to retrieve the appropriate setting value
			// from the notification rule
			if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA) 	||
					origInstrumentType.equals(InstrumentType.DEFERRED_PAY) 	||
					origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT) )
			{
				if(StringFunction.isNotBlank(relatedInstrumentOid))
				{
							Instrument relatedInstrument = (Instrument)createServerEJB("Instrument");
							relatedInstrument.getData(Long.parseLong(relatedInstrumentOid));
							instrumentType = relatedInstrument.getAttribute("instrument_type_code");
							// Changes done for Rel9.5 CR-927B - Set the instrument category based on the instrument type and determine if its Import or Export
							if (instrumentType.equals(InstrumentType.IMPORT_COL) 		||
									instrumentType.equals(InstrumentType.IMPORT_DLC)		||
									instrumentType.equals(InstrumentType.GUARANTEE)		||
									instrumentType.equals(InstrumentType.REQUEST_ADVISE)		||
									instrumentType.equals(InstrumentType.STANDBY_LC )		||
									instrumentType.equals(InstrumentType.APPROVAL_TO_PAY ) || 
									instrumentType.equals(InstrumentType.AIR_WAYBILL ) || 
									instrumentType.equals(InstrumentType.SHIP_GUAR )) 
							{
								if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA)) 
										instrumentType = TradePortalConstants.IMPORT_BANKER_ACCEPTANCE;
								else if (origInstrumentType.equals(InstrumentType.DEFERRED_PAY)) 
									instrumentType = TradePortalConstants.IMPORT_DEFERRED_PAYMENT;
								else if (origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT)) 
									instrumentType = TradePortalConstants.IMPORT_TRADE_ACCEPTANCE;
							}
							else if (instrumentType.equals(InstrumentType.EXPORT_COL) 	        ||
									instrumentType.equals(InstrumentType.NEW_EXPORT_COL) 		|| 
									instrumentType.equals(InstrumentType.EXPORT_DLC) 		||
									instrumentType.equals(InstrumentType.INCOMING_GUA)	        ||
									instrumentType.equals(InstrumentType.INCOMING_SLC))
							{
								if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA)) 
									instrumentType = TradePortalConstants.EXPORT_BANKER_ACCEPTANCE;
								else if (origInstrumentType.equals(InstrumentType.DEFERRED_PAY)) 
									instrumentType = TradePortalConstants.EXPORT_DEFERRED_PAYMENT;
								else if (origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT)) 
									instrumentType = TradePortalConstants.EXPORT_TRADE_ACCEPTANCE;
							}
				}
			}

			if (StringFunction.isBlank(instrumentType))
			{
				instrumentType = origInstrumentType;
			}
			LOG.info("CorporateOrganization::fetchRelatedInstrumentType::returnInstrumentType- {}",instrumentType);
			return instrumentType;
     }
}
