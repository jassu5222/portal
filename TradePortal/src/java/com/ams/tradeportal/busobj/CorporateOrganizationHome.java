
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This business object represents a business organization that is a customer
 * of the client banks.   Corporate organizations apply for instruments and,
 * by authorizing them, send them to the bank.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CorporateOrganizationHome extends EJBHome
{
   public CorporateOrganization create()
      throws RemoteException, CreateException, AmsException;

   public CorporateOrganization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
