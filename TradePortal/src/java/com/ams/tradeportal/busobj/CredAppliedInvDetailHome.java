package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;

import java.rmi.*;

import javax.ejb.*;
/**
 * this holds the invoice details on which credit note has been applied.
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface CredAppliedInvDetailHome extends EJBHome {
	
	public CredAppliedInvDetail create()
    throws RemoteException, CreateException, AmsException;

    public CredAppliedInvDetail create(ClientServerDataBridge csdb)
    throws RemoteException, CreateException, AmsException;

}
