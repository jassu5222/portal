package com.ams.tradeportal.busobj;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.DocumentHandler;

import java.rmi.*;
import java.util.List;


/**
 * Describes the mapping of an uploaded file to Invoice Definition stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface CreditNotes extends TradePortalBusinessObject {

		
	public ArMatchingRule getMatchingRule() throws AmsException, RemoteException;
	public String getTpRuleName() throws AmsException, RemoteException ;
	public String getTpRuleName(ArMatchingRule rule) throws AmsException, RemoteException ;
	public void close(String uploadUser)throws AmsException, RemoteException;
	public boolean modifyInvoiceCreditRelationData ( List<DocumentHandler> creditNoteAppliedInvList ) throws RemoteException, AmsException;
	public void setPayableCreditNoteData ( DocumentHandler invoiceDoc ) throws RemoteException, AmsException;
	
}
