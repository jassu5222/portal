package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashSet;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CrossRateCalcRuleBean extends CrossRateCalcRuleBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(CrossRateCalcRuleBean.class);


	/**
	 * Performs any special "validate" processing that is specific to the
	 * descendant of BusinessObject.
	 *
	 * This method is optinally coded in descendant objects as desired.
	 * Any object specific validation should be coded within this method
	 * on descendants of BusinessObject.
	 *
	 */
	protected void userValidate () throws AmsException, RemoteException {

		HashSet aSet = new HashSet();
		BusinessObject busObj = null;
		ComponentList ruleList = (ComponentList)this.getComponentHandle("CrossRateRuleCriterionList");
		int ruleTotal = ruleList.getObjectCount();

		//Vshah - IR#LUUL120259744 - 12/07/2011 - Rel7.1 - <Begin>
		if (!isUnique("name", getAttribute("name"), "")) {
		                this.getErrorManager().issueError(
		                                        TradePortalConstants.ERR_CAT_1,
		                                        TradePortalConstants.ALREADY_EXISTS,
		                                        getAttribute("name"),
		                                        attributeMgr.getAlias("name"));
        }
		//Vshah - IR#LUUL120259744 - 12/07/2011 - Rel7.1 - <End>

		// Validate atleast one rule is defined
		if (ruleTotal < 1) {
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ATLEAST_ONE_CCY_COMB_REQD);
		}

		for (int i = 0; i < ruleTotal; i++) {
			ruleList.scrollToObjectByIndex(i);
			busObj = ruleList.getBusinessObject();

			// Validate required fields
			if (StringFunction.isBlank(busObj.getAttribute("from_ccy")) || StringFunction.isBlank(busObj.getAttribute("to_ccy")) ||
					StringFunction.isBlank(busObj.getAttribute("lower_variance")) || StringFunction.isBlank(busObj.getAttribute("upper_variance"))) {
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CROSS_RATE_MISSING_REQD_FIELDS,
						i+1+"");
			}
			//Validate required field calc_method
			if(StringFunction.isBlank(busObj.getAttribute("calc_method"))){
				this.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CROSS_RATE_MISSING_REQD_FIELDS,
						i+1+"");
			}

			//MDB IR POUL112775885 Rel 7.1 12/7/2011 - Begin
			// Validate that the lower_variance field will fit in the database
			InstrumentServices.validateDecimalNumber(busObj.getAttribute("lower_variance"),
					5,
					8,
					"CrossRateCalculationRuleDetail.LowerVar",
					this.getErrorManager(),
					this.getResourceManager());

			// Validate that the upper_variance field will fit in the database
			InstrumentServices.validateDecimalNumber(busObj.getAttribute("upper_variance"),
					5,
					8,
					"CrossRateCalculationRuleDetail.UpperVar",
					this.getErrorManager(),
					this.getResourceManager());
			//MDB IR POUL112775885 Rel 7.1 12/7/2011 - End

			//check if from and to ccy are same
			if (StringFunction.isNotBlank(busObj.getAttribute("from_ccy")) && StringFunction.isNotBlank(busObj.getAttribute("to_ccy"))) {
				if (busObj.getAttribute("from_ccy").equals(busObj.getAttribute("to_ccy"))) {
					String subst[] = {i+1+"", busObj.getAttribute("from_ccy"), busObj.getAttribute("to_ccy")};
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.CROSS_RATE_FROM_AND_TO_CCY_SAME,
							subst);
				}

				//check for duplicate from and to ccy combination
				String comb = busObj.getAttribute("from_ccy") + "|" + busObj.getAttribute("to_ccy");
				if (!aSet.add(comb)) {
					String subst[] = {i+1+"", busObj.getAttribute("from_ccy"), busObj.getAttribute("to_ccy")};
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DUPLICATE_CROSS_RATE_ENTRY,
							busObj.getAttribute("from_ccy"), busObj.getAttribute("to_ccy"));

				}
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.amsinc.ecsg.frame.BusinessObjectBean#userDelete()
	 */
	public void userDelete() throws RemoteException, AmsException
	  {
		// check if the Cross Rate Calculation Rule is referenced

		if (isAssociated("BankOrganizationGroup", "activeByBankOrgGrp",	new String[] {getAttribute("cross_rate_calc_rule_oid")}))
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CROSS_RATE_IS_REFERENCED);

	}

}
