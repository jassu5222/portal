
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Business object to store Cross Rate Calculation rules.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CrossRateCalcRuleBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(CrossRateCalcRuleBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* cross_rate_calc_rule_oid - Unique identifier */
      attributeMgr.registerAttribute("cross_rate_calc_rule_oid", "cross_rate_calc_rule_oid", "ObjectIDAttribute");
      
      /* Pointer to the parent ReferenceDataOwner */
       attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
      
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* CrossRateRuleCriterionList - Each notification rule contains one to many notify criterion. */
      registerOneToManyComponent("CrossRateRuleCriterionList","CrossRateRuleCriterionList");
   }

 
   
 
 
   
}
