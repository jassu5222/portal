



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Allows Cross Rate Calculatoin Rules to be defined.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
   // DK IR-POUL110343233 Rel7.1
public interface CrossRateRuleCriterion extends TradePortalBusinessObject
{
}
