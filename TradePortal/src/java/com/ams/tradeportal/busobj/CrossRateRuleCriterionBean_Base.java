

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Allows Cross Rate Calculatoin Rules to be defined.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
   // DK IR-POUL110343233 Rel7.1
public class CrossRateRuleCriterionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(CrossRateRuleCriterionBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* criterion_oid - Unique Identifier */
      attributeMgr.registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");

      /* from_ccy - From currency */
      attributeMgr.registerReferenceAttribute("from_ccy", "from_ccy", "CURRENCY_CODE");

      /* to_ccy - To currency. */
      attributeMgr.registerReferenceAttribute("to_ccy", "to_ccy", "CURRENCY_CODE");

      /* calc_method - Calculation method */
      attributeMgr.registerReferenceAttribute("calc_method", "calc_method", "MULTIPLY_DIVIDE_IND");

      /* lower_variance - Lower variance */
      attributeMgr.registerAttribute("lower_variance", "lower_variance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");

      /* upper_variance - Upper variance */
      attributeMgr.registerAttribute("upper_variance", "upper_variance", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");

      /* Pointer to the parent CrossRateCalcRule */
       attributeMgr.registerAttribute("cross_rate_calc_rule_oid", "p_cross_rate_calc_rule_oid", "ParentIDAttribute");

   }






}
