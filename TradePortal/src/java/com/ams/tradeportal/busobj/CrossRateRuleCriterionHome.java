
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Allows Cross Rate Calculatoin Rules to be defined.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CrossRateRuleCriterionHome extends EJBHome
{
   public CrossRateRuleCriterion create()
      throws RemoteException, CreateException, AmsException;

   public CrossRateRuleCriterion create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
