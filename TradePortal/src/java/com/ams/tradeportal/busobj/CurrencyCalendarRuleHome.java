
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The rule that indicates which calendar is linked to each currency.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CurrencyCalendarRuleHome extends EJBHome
{
   public CurrencyCalendarRule create()
      throws RemoteException, CreateException, AmsException;

   public CurrencyCalendarRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
