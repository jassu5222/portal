
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business Object to store aliases (alternate names/identifiers) to be used
 * as indexes on the Customer File depending on the specific Message Category
 * for the Incoming Generic Message
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface CustomerAliasHome extends EJBHome
{
   public CustomerAlias create()
      throws RemoteException, CreateException, AmsException;

   public CustomerAlias create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
