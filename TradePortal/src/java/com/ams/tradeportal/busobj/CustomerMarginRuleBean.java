
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.*;




/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CustomerMarginRuleBean extends CustomerMarginRuleBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(CustomerMarginRuleBean.class);

	/**
	 * Performs any special "validate" processing that is specific to the
	 * descendant of BusinessObject.
	 *
	 *
	 * @see #validate()
	 */

	protected void userValidate() throws AmsException, RemoteException {
    //Shilpa R IR-SIUM012764826 start
		// Validate that the ThresholdAmt field will fit in the database
		InstrumentServices.validateDecimalNumber(getAttribute("threshold_amt"),
				15, 3, "CorpCust.ThresholdAmt", this.getErrorManager(),
				this.getResourceManager());

		// Validate that the Margin field will fit in the database
		InstrumentServices.validateDecimalNumber(getAttribute("margin"), 3, 6,
				"CorpCust.Margin", this.getErrorManager(),
				this.getResourceManager());

	}
	//Shilpa R IR-SIUM012764826 end

}
