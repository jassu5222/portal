

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 *
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CustomerMarginRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(CustomerMarginRuleBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* customer_margin_rule_oid - Unique Identifier */
      attributeMgr.registerAttribute("customer_margin_rule_oid", "customer_margin_rule_oid", "ObjectIDAttribute");

      /* currency - The currency of customer margin rule */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      attributeMgr.registerAlias("currency", getResourceManager().getText("CustomerMarginRuleBeanAlias.currency", TradePortalConstants.TEXT_BUNDLE));

      /* instrument_type - Code for the instrument type */
      attributeMgr.registerReferenceAttribute("instrument_type", "instrument_type", "INSTRUMENT_TYPE");
      attributeMgr.registerAlias("instrument_type", getResourceManager().getText("CustomerMarginRuleBeanAlias.instrument_type", TradePortalConstants.TEXT_BUNDLE));

      /* threshold_amt - Threshould Amount for margin rule */
      attributeMgr.registerAttribute("threshold_amt", "threshold_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      // DK IR LMUM050355987 Rel 8.0 05/10/2012 Begin
      attributeMgr.registerAlias("threshold_amt", getResourceManager().getText("CorpCust.ThresholdAmt", TradePortalConstants.TEXT_BUNDLE));
   // DK IR LMUM050355987 Rel 8.0 05/10/2012 End

      /* rate_type - The rate type for customer margin rule. */
      attributeMgr.registerReferenceAttribute("rate_type", "rate_type", "REFINANCE_RATE_TYPE");
      attributeMgr.registerAlias("rate_type", getResourceManager().getText("CustomerMarginRuleBeanAlias.rate_type", TradePortalConstants.TEXT_BUNDLE));

      /* margin - Margin for margin rule */
      attributeMgr.registerAttribute("margin", "margin", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("margin", getResourceManager().getText("CustomerMarginRuleBeanAlias.margin", TradePortalConstants.TEXT_BUNDLE));

      /* Pointer to the parent CorporateOrganization */
       attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

   }






}
