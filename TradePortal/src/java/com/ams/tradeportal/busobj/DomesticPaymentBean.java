
package com.ams.tradeportal.busobj;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.EmailValidator;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;



/*
 *
 *
 *     Copyright � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DomesticPaymentBean extends DomesticPaymentBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(DomesticPaymentBean.class);

	/**
	 * Reserved for future use
	 * @exception   com.amsinc.ecsg.frame.AmsException
	 */
	public void preSave() throws AmsException
	{
		
	}


	/**
	 * This method loops through the list of all the Domestic Payment of
	 * the source Domestic Payment Transaction or Template and creates
	 * copies of those for the target Domestic Payment Transaction/Template
	 *
	 * @param originalTransOid  java.lang.String - the source transaction oid
	 * @param newTransactionOid  java.lang.String - the target (new) transaction oid
	 * @return void
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	//IAZ CR-586 IR-VRUK091652765 Add Overload CreateNew Method
	public void createNewDomesticPayments(String originalTransOid, String newTransactionOid)
	throws RemoteException, AmsException
	{
		createNewDomesticPayments(originalTransOid, newTransactionOid, false);
	}

	public void createNewDomesticPayments(String originalTransOid, String newTransactionOid, boolean fixedTemplateFlag)
	throws RemoteException, AmsException

	{
		try
		{

			//obtain a list of all domestic payments from source DP Transaction/Template
			String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?" ;
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{originalTransOid});

			//IAZ CM-451 12/29/08 Begin - check if there is no Payee data to copy.
			//if so, exit w/o warning/errors
			if (resultsDoc == null)
			{
				LOG.debug("DOMPMTBean::createNewDomPmt::The source template/transaction has no Payee informationn to copy");
				return;
			}
			
			int curIndex = 0;
			String originalDomesticPaymentOidStr =
				resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

			//loop through all the source's domestic payments
			while (
					(originalDomesticPaymentOidStr != null) &&
					(!originalDomesticPaymentOidStr.equals(""))
			)
			{

				long originalDomesticPaymentOid = 0;
				originalDomesticPaymentOid =
					resultsDoc.getAttributeLong("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

				//dom payment oid must be present for each entry
				if (originalDomesticPaymentOid == 0)
				{
					LOG.debug("DOMPMTBean::createNewDomPmt::Error: domestic_payment_oid not present");
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DOM_PMT_OID_NOT_SUPPLIED_WRN);

				}
				else
				{
					//create a new tagert bean; copy from source object to target one; and save.

					DomesticPayment originalDomesticPayment = (DomesticPayment) this.createServerEJB("DomesticPayment");
					originalDomesticPayment.getData(originalDomesticPaymentOid);

					DomesticPayment domesticPayment = (DomesticPayment) this.createServerEJB("DomesticPayment");
					domesticPayment.newObject();

					//IAZ CM CR-507 12/12/09: Begin
					//   Move Entire data record from source bean to the target (rather than field by field). This will
					//   move all fileds, including ones introduced with CM CR-507.
					//   Make sure to copy Intermediary Banks data.
					DocumentHandler tmpXML = new DocumentHandler();
					originalDomesticPayment.populateXmlDoc(tmpXML);
					domesticPayment.populateFromXmlDoc(tmpXML);

					domesticPayment.setAttribute("transaction_oid", newTransactionOid);
					String dpPayeeName = originalDomesticPayment.getAttribute("payee_name");
					domesticPayment.setAttribute("payee_name", dpPayeeName);

			
					//Store source DP oid with new DP if copied form fixed template
					if (!fixedTemplateFlag)
						domesticPayment.setAttribute("source_template_dp_oid", "");
					else
						domesticPayment.setAttribute("source_template_dp_oid",
										originalDomesticPayment.getAttribute("domestic_payment_oid"));
	

					copyInterParty("c_FirstIntermediaryBank", domesticPayment);

					//IR 23588 - reset payment_ben_email_sent_flag when new instrument is created out of existing instrument
					domesticPayment.setAttribute("payment_ben_email_sent_flag", "N");

					domesticPayment.save();
                    //RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - Start
                    //create new invoice details objects attached to the original domestic payment from template.
                    StringBuffer invDetROSql = new StringBuffer();
                    invDetROSql.append("SELECT INVOICE_DETAIL_OID FROM INVOICE_DETAILS WHERE DOMESTIC_PAYMENT_OID = ?");

                    DocumentHandler invDetROOidDoc = DatabaseQueryBean.getXmlResultSet(invDetROSql.toString(), false, new Object[]{originalDomesticPaymentOid});
                    if (invDetROOidDoc != null) {
                        String invoiceDetailOid = invDetROOidDoc.getAttribute("/ResultSetRecord/INVOICE_DETAIL_OID");
                        if (StringFunction.isNotBlank(invoiceDetailOid)) {
                            InvoiceDetails invoiceDetails = (InvoiceDetails) this.createServerEJB("InvoiceDetails");
                            invoiceDetails.getData(Long.valueOf(invoiceDetailOid));

                            InvoiceDetails newInvoiceDetails = (InvoiceDetails) this.createServerEJB("InvoiceDetails");
                            newInvoiceDetails.newObject();
                            newInvoiceDetails.setAttribute("domestic_payment_oid",domesticPayment.getAttribute("domestic_payment_oid"));
                            newInvoiceDetails.setAttribute("payee_invoice_details",invoiceDetails.getAttribute("payee_invoice_details"));
                            newInvoiceDetails.save();

                        }
                    }
                    //RKAZI IR#VSUL081045430 Rel 8.1 09/25/2012 - End
					curIndex++;
					originalDomesticPaymentOidStr =
						resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");
				}

			}
		}
		catch (Exception any_e)
		{
			LOG.error("DOMPMTBean::createNewDomPmt::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_ERR_WRN);
		}

	}

	
	/**
	 * This method copies Intermediary Bank data form Source to Target bean.
	 *
	 * @param String c_Party (Party Pointer)
	 * @param com.ams.tradeportal.busobj.Domestic_Payment Source_DP
	 * @return void
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	private void copyInterParty(String c_Party, DomesticPayment domesticPayment)
	throws RemoteException, AmsException
	{

		DocumentHandler tmpXML = new DocumentHandler();
		String orgPartyOid = domesticPayment.getAttribute(c_Party);
		if (StringFunction.isNotBlank(orgPartyOid))
		{
			PaymentParty originalParty = (PaymentParty) createServerEJB("PaymentParty");
			originalParty.getData((domesticPayment.getAttributeLong(c_Party)));
			LOG.debug("DOMPMTBean::copyInterParty::orgPartyOid: {} " , orgPartyOid);
			PaymentParty newParty = (PaymentParty) createServerEJB("PaymentParty");
			newParty.newObject();
			originalParty.populateXmlDoc(tmpXML);
			newParty.populateFromXmlDoc(tmpXML);
			newParty.save();
			domesticPayment.setAttribute(c_Party, newParty.getAttribute("payment_party_oid"));
		}
	}

	
	//   -Move Amount calculation to a separate module
	//   -Populate entire Bean from the doc using populateFtomXmlDoc rather than copying field by filed
	//      This insures that all filed, including ones intriduced by CR-507 are getting processed.
	//   -Clean up old code out.
	//   -Porcess Intermediarty Banks Parties
	//   -Move Terms processinf to a separate module

	/**
	 * This method creates new or updates an exisiting Payee data on the Domestic Payment
	 * Transaction or Template based on the data in the input document.
	 * It checks whether Domestic Payment Oid has been supplied to create a new payee or
	 * update an exisiting one.
	 * It returs true if no error condition is raised. It is possible that no updates/inserts
	 * need to take place - in this case true is returned. fals is returned only if an error
	 * condition is raised.
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param outputDoc com.amsinc.ecsg.util - the output doc
	 * @return boolean -- success/ fail
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	public boolean updateDomesticPayment (DocumentHandler inputDoc, DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		try
		{
			//First, check if all required fields are provided and are in good format.
			//   if not, warning/error condition(s) will be raised and no payee update/insert takes
			//place.
			//NOTE: this methoid will return false only if error condition is raised. It is possible that
			//   there is no need to add/update any payees and so the Sucess is still returned even
			//   though no payee data is added to the trunsaction
			if (!doAddDomesticPayment(inputDoc))
			{
				if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
					return false;
				else
					return true;
			}

		
			//Obtain the current number of Payees with this transaction.  If this is an insert, we will need to
			//increment the total number of Payees so that Terms object contains proper value.
			boolean isNewDPInsert = false;	//IAZ IR-SRUK020834374 02/10/10 Add
			int prevNumberOfPayees = 0;
			try
			{
				prevNumberOfPayees = inputDoc.getAttributeInt("/prev_payee_number/");
			}
			catch (Exception any_e)
			{
				//if the current number is not known, do not update Terms object
				prevNumberOfPayees = -1;
			}
	

			//Validation is successful. Now check whether Domestic Payment oid is supplied.
			//   If so, this is an Update. If not, this is an Insert.
			//   Also, on Update, validate initial amount. On Insert, increase number of Payees per Transaction.

			long ldomesticPaymentOid = 0;
			String domesticPaymentOid = inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid/");
			String curAmount = "0.00";

			//if ((domesticPaymentOid != null) && (!domesticPaymentOid.equals("")))
			if (StringFunction.isNotBlank(domesticPaymentOid))
			{
				ldomesticPaymentOid = Long.parseLong(domesticPaymentOid);
				this.getData(ldomesticPaymentOid);

				curAmount = this.getAttribute("amount");
				if (StringFunction.isBlank(curAmount))
					curAmount = "0.00";
			}
			else
			{
				this.newObject();

				isNewDPInsert = true;
				
			}

			//set parent transaction oid
			this.setAttribute("transaction_oid",
					(new Long(inputDoc.getAttributeLong("/Transaction/transaction_oid"))).toString());


			//Validate amount/warn on bad input -- validation will also take
			//place on transaction's Verify.
			//In a case where this is a forced update/insert form a template
			//re-validate the amount and set it to zero if is not in proper format.

			try
			{
				BigDecimal totalAmountBI = calculateTotalAmount(inputDoc, curAmount);
				//BSL Rel 6.1 IR #VEUL031445617 03/22/11 Begin - use toPlainString rather than toString
				String totalAmountBIStr = totalAmountBI.toPlainString();
				LOG.debug("DOMPMT::updateDomPmt::totalAmount is {}" , totalAmountBIStr);
				inputDoc.setAttribute("/Terms/amount", totalAmountBIStr);
				inputDoc.setAttribute("/Terms/funding_amount", totalAmountBIStr);
				LOG.debug("DOMPMT::updateDomPmt::/Terms/amount is {}" , inputDoc.getAttribute("/Terms/amount"));
				//BSL Rel 6.1 IR #VEUL031445617 03/22/11 End
			}
			catch (Exception any_e)
			{
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_WRN, inputDoc.getAttribute("/DomesticPayment/amount"));
				this.setAttribute("amount", "0");
			}
	
			this.populateFromXmlDoc(inputDoc);


			this.setAttribute("bank_charges_type",
					inputDoc.getAttribute("/Terms/bank_charges_type"));			//kept as terms in mapping xml


			// Do CBFT/IP-specific Validations
			if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(this.getAttribute("payment_method_type")))
			{
				//Address-4 Line is not sued with CBFT/IP-SWIFT
				this.setAttribute("payee_address_line_4", "");

				//SWIFT chars are not allowed with SWIFT
				final String[] attributesToExclude = {"payee_name","payee_description", "other_charges_instructions", "payee_email"};
				InstrumentServices.checkForInvalidSwiftCharacters(this.getAttributeHash(), this.getErrorManager(), attributesToExclude);
			}

			//Along the way, update Intermidiary Bank parties.
			processIntermediaryBankParty(inputDoc, "First");

			//Finally, populate inputDoc with update Terms information so Terms get updated when navigation is back to TransactionMediator.
			processDPTerms(inputDoc, prevNumberOfPayees);

			//All processing is complete.  If no errors detected, save Data to the database.
			if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
			{
				LOG.debug("DOMPMTBean::updateNewDomPmt::Error Detected on DP/Interm Banks/Terms::Will Not Save DP");
				return false;
			}
			//IAZ IR-SRUK020834374 02/10/10 Begin: This code is moved here from up top.
			//  When no errors detected -- on insert, store new DP OID in to the output doc
			//  to be used by Verify if needed; also increase Payees Count for this transaction.
			else
			{
			
				if (isNewDPInsert)
				{
					
					// Sequence number generation (Order of Beneficiary)
					//Fixed by Vishal Sarkary-Rel 9.1 IR#T36000030197 - 24/07/2014- Begin
					String seqNumSql = "select max(SEQUENCE_NUMBER) as SEQUENCE_NUMBER from domestic_payment where p_transaction_oid = ?";

					String seqNum = "1";
					DocumentHandler seqNumRS = DatabaseQueryBean.getXmlResultSet(seqNumSql, true, 
							new Object[]{this.getAttribute("transaction_oid")});
					
					if (seqNumRS != null) {
						seqNum = seqNumRS.getAttribute("/ResultSetRecord(0)/SEQUENCE_NUMBER");
						if (StringFunction.isNotBlank(seqNum)) {
							seqNum = (new Integer(Integer.parseInt(seqNum) + 1)).toString();
						}
						else {
							seqNum = "1";
						}
					}
					//Fixed by Vishal Sarkary-Rel 9.1 IR#T36000030197 - 24/07/2014- End

					this.setAttribute("sequence_number", seqNum);
				}

			}


			this.save(true);
			return true;
		}
		catch(Exception any_e)
		{
			LOG.error("DOMPMTBean::updateNewDomPmt::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_UPD_ERR_WRN);
			return false;
		}
	}


	/**
	 * This method copies Intermediary Bank data form Doc to Traget bean.
	 *
	 * @return void
	 * @param com.amsinc.ecsg.util.DocumentHandler inputDoc
	 * @param String partyPointer
	 */
	private void processIntermediaryBankParty(DocumentHandler inputDoc, String partyPointer)
	throws RemoteException, AmsException
	{

		PaymentParty interBank = (PaymentParty) createServerEJB("PaymentParty");
		String strPPOid = inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/payment_party_oid");
		String bankBranchCode = inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/bank_branch_code");

		boolean saveParty = false;
		if (StringFunction.isNotBlank(strPPOid))
		{
			interBank.getData(Long.parseLong(strPPOid));
			if (StringFunction.isBlank(bankBranchCode))
			{
				interBank.delete();

			}
			else
				saveParty = true;
		}
		else
		{
			if (StringFunction.isNotBlank(bankBranchCode))
			{
				interBank.newObject();
				saveParty = true;
			}
		}
		if (saveParty)
		{
			interBank.setAttribute("bank_name", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/bank_name"));
			interBank.setAttribute("bank_branch_code", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/bank_branch_code"));
			interBank.setAttribute("address_line_1", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/address_line_1"));
			interBank.setAttribute("address_line_2", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/address_line_2"));
			interBank.setAttribute("address_line_3", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/address_line_3"));
			interBank.setAttribute("branch_name", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/branch_name"));
			interBank.setAttribute("party_type", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/party_type"));
			interBank.setAttribute("country", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "PaymentParty/country"));
			LOG.debug("DOMPMTBean::processIntermediaryBankParty::Bank Oid and Name are {} , {}" , interBank.getAttribute("payment_party_oid") , interBank.getAttribute("bank_name"));

			//Validate chars for SWIFT for CBFT
			if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(inputDoc.getAttribute("/DomesticPayment/payment_method_type")))
			{
				final String[] attributesToExclude = {"payee_name","payee_description", "other_charges_instructions", "payee_email"};
				InstrumentServices.checkForInvalidSwiftCharacters(interBank.getAttributeHash(),
						this.getErrorManager(),
						attributesToExclude,
						getResourceManager().getText("DomesticPaymentRequest." + partyPointer + "InterBank",
								TradePortalConstants.TEXT_BUNDLE));
			}
			if (this.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
			{
				if (StringFunction.isNotBlank(interBank.getAttribute("payment_party_oid")))
					this.setAttribute("c_"+ partyPointer +"IntermediaryBank", interBank.getAttribute("payment_party_oid"));

				interBank.save(true);
			}
		}
		else
		{
			this.setAttribute("c_"+ partyPointer +"IntermediaryBank", "");
		}

	}

	/**
	 * This method calculates new Transaction Amount.
	 *
	 * @return BigDecimal totalAmount
	 * @param com.amsinc.ecsg.util.DocumentHandler inputDoc
	 * @param String curAmount
	 */
	private BigDecimal calculateTotalAmount(DocumentHandler inputDoc, String curAmount)
	throws RemoteException, AmsException
	{
		String enteredPaymentAmount = inputDoc.getAttribute("/DomesticPayment/amount");

		if (StringFunction.isBlank(enteredPaymentAmount))
			enteredPaymentAmount = "0.00";

		enteredPaymentAmount =
			NumberValidator.getNonInternationalizedValue(enteredPaymentAmount,
					getResourceManager().getResourceLocale());

		String totalAmount = inputDoc.getAttribute("/Terms/amount");

		if ((StringFunction.isBlank(totalAmount)) || (totalAmount.equals("0")))
			totalAmount = "0.00";

		BigDecimal totalAmountBI = new BigDecimal(totalAmount);
		BigDecimal enteredPaymentAmountBI = new BigDecimal(enteredPaymentAmount);
		BigDecimal curAmountBI = new BigDecimal(curAmount);
		totalAmountBI=totalAmountBI.add(enteredPaymentAmountBI);
		totalAmountBI=totalAmountBI.subtract(curAmountBI);

		this.setAttribute("amount", enteredPaymentAmount);
		inputDoc.setAttribute("/DomesticPayment/amount", enteredPaymentAmount);

		return totalAmountBI.stripTrailingZeros(); //BSL Rel 6.1 IR #VEUL031445617 03/22/11 - use stripTrailingZeros

	}

	/**
	 * This method updates Terms parameters int he inputDoc so they can be saved with Transaction's save.
	 *
	 * @return BigDecimal totalAmount
	 * @param com.amsinc.ecsg.util.DocumentHandler inputDoc
	 * @param int numberOfPayees
	 */
	public DocumentHandler processDPTerms(DocumentHandler inputDoc, int numberOfPayees)
	throws RemoteException, AmsException
	{

		//IAZ CM-451 01/29/09 and CM CR-507/509 01/07/10 and 01/15/10 Begin
		//Ensure that Terms First Party data is properly set based on the total number of Payees with this transaction
		//In case of Direct Debit transaction, it is Payers.
		LOG.debug("DOMPMTBean::processDPTerms::curNumberPayees: {} " , numberOfPayees);
		//Srinivasu_D IR#T36000043520 Rel9.3 ER9.3.0.1 09/09/2015 - Start validating the length of ben name
		String benName = inputDoc.getAttribute("/DomesticPayment/payee_name/");
		
		if((benName != null && benName.length() >35) &&
			(TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(this.getAttribute("payment_method_type"))) &&				
		    (InstrumentType.DOM_PMT.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))) {  						
				
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.DPT_CBFT_BENE_NAME_MAX_LENGTH);
			}
		 
		//Srinivasu_D IR#T36000043520 Rel9.3 ER9.3.0.1 09/09/2015 - End
		if (numberOfPayees == 1)
		{
			//Beneficiary Name may be upto 140 chars long. Terms only allow upt 35 chars values: use substring with set.
			String partyName = inputDoc.getAttribute("/DomesticPayment/payee_name/");
			int termSizeLimit = partyName.length();
			if (termSizeLimit > 35) termSizeLimit = 35;
			inputDoc.setAttribute("/Terms/FirstTermsParty/name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/reference_number", inputDoc.getAttribute("/DomesticPayment/customer_reference/"));
		}
		if (numberOfPayees > 1)
		{

			if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						StringFunction.asciiToUnicode(getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)));
		                
				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						StringFunction.asciiToUnicode(getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE)));
			}
			else
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						StringFunction.asciiToUnicode(getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE)));

				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						StringFunction.asciiToUnicode(getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE)));
			}
			//IR - PAUJ083080997 - Set Address Line 1 , 2 and 3 to Empty in case of Multiple Payee.
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_1", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_2", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_3", "");
			inputDoc.setAttribute("/Terms/reference_number", StringFunction.asciiToUnicode(getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE)));
		}
		

		return inputDoc;

	}
	


	/**
	 * This method chck if the Domestic Payment should indeed be added to the
	 * Domestic Payment Transaction/ Template based on the data in the input document
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @return boolean -- true/false
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	private boolean doAddDomesticPayment (DocumentHandler inputDoc)
	throws RemoteException, AmsException
	{

		boolean returnFlag = true;
		
		try
		{

			LOG.debug("DOMPMTBean::doAddDomesticPayment::FunctionButton: {}" , inputDoc.getAttribute("/Update/ButtonPressed"));
			String enteredPaymentMethod = inputDoc.getAttribute("/DomesticPayment/payment_method_type/");
			ResourceManager resourceMgr = getResourceManager();

			
			//Check the lenght of Payee Descripton field.
			//This check must occur for both Transactions AND templates -- so this is done before we split
			//the processing.
			String payeeDescription = inputDoc.getAttribute("/DomesticPayment/payee_description/");
			if (StringFunction.isNotBlank(payeeDescription) && (payeeDescription.length() > 140))
			{
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
						resourceMgr.getText("DomesticPaymentRequest.PaymentDetails", TradePortalConstants.TEXT_BUNDLE),
						payeeDescription.substring(120, 140), "");
				inputDoc.setAttribute("/DomesticPayment/payee_description", payeeDescription.substring(0, 140));
			}
			
			
			String invoiceDetails=inputDoc.getAttribute("/InvoiceDetails/payee_invoice_details");
			
			if (StringFunction.isNotBlank(invoiceDetails) && invoiceDetails.length() > 80000)
			{
				String tempInvoiceDetails = invoiceDetails.replaceAll("[\n\r]", "");
				if(tempInvoiceDetails.length() > 80000){
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_DETAILS_EXCEEDS_80000_CHAR);
				}

			}
			

			//Verify is at least some payee data is entered.
			//If not, no Payee needs to be added/updated (so return false) but an
			//action should continue (e.g., if this is Verify, Verify must progress with
			//exisiting data verification; if this is SaveClose - non-Payee transaction data
			//should be saved and transaction window close, etc) -- so do not raise any error
			//conditions.
			//Note: for DDIs, Payment Type is always "DDI" so if this is the only data entered, treat as No-Data case.
			if(
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/amount/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_account_number/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_name/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_description"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_bank_code/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_bank_name/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/address_line_1/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/payee_address_line_1/"))) &&	
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoiceDetails/payee_invoice_details/"))) &&	
					(
							(StringFunction.isBlank(enteredPaymentMethod)) ||										
							(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(enteredPaymentMethod))					

					)//&& //BSL CR-597 03/24/11 - remove /Terms/bank_charges_type check since CR597 gave it a default value on the page

			)
			{
				returnFlag = false;
			}
			//Next, we check if this is a template reuqest. If so, partial Payee data is OK
			else if (inputDoc.getDocumentNode("/Template") != null)
			{
				LOG.debug("DOMPMTBean::doAddDomesticPayment::TemplateProcessing");
				returnFlag = true;
			}

	
			//    This is not a template request and there is at least one field entered. Now, if this is a full Fixed Pmt
			//    and user attempts to add a Bene (vs. Modify), return an error.
			else if (TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/DomesticPayment/fixedPmtInd/")))
			{
				LOG.debug("DPBean::updateDomesticPayment - trying to add/modify payee to fixed template...");
			    if (StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid/")))
				{
						LOG.debug("DPBean::updateDomesticPayment - Not allowed to add...");
						this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.CANT_ADD_DOM_PMT_FIXED_TR);
						return false;
				}

			}
	

			//If this is a regular Transaction requst, check if all required fileds are provided and are in a valid format
			//Note that some validations depend on the Payment Method of the Transaction
			else
			{
				String partyType = "DomesticPaymentRequest.Payee";
				if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(enteredPaymentMethod))
					partyType = "DirectDebitInstruction.Payer";

	
				//Validate Reporting Code for any Payments Transaction
				if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(enteredPaymentMethod))
				{
					validateReportingCodes(inputDoc, enteredPaymentMethod, false);
					validateCheckDetails(inputDoc, false, false);//BSL IR#PBUL042033374 04/29/11 Add overload
				}
			

				//IAZ IR PIUJ091776300 09/18/09 Begin - Read fields descriptions form the properties files vs being hardcoded

				if ((!TradePortalConstants.PAYMENT_METHOD_BCHK.equals(enteredPaymentMethod)) &&			
						(!TradePortalConstants.PAYMENT_METHOD_CCHK.equals(enteredPaymentMethod)))			

					validatePayeeField(inputDoc.getAttribute("/DomesticPayment/payee_account_number/"), returnFlag, //"Payee Account Number");
							resourceMgr.getText(partyType+"AccountNumber", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(inputDoc.getAttribute("/DomesticPayment/payee_name/"), returnFlag, //"Payee Name");
						resourceMgr.getText(partyType + "Name", TradePortalConstants.TEXT_BUNDLE));


				validatePayeeField(inputDoc.getAttribute("/Terms/bank_charges_type"), returnFlag, //"Bank Charges Type");
						resourceMgr.getText("DomesticPaymentRequest.BankCharges", TradePortalConstants.TEXT_BUNDLE));

				
				validatePayeeField(inputDoc.getAttribute("/DomesticPayment/amount/"), returnFlag, //"Amount");
						resourceMgr.getText("FundsTransferRequest.Amount", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(enteredPaymentMethod, returnFlag, //"Payment Method");			
						resourceMgr.getText("DomesticPaymentRequest.PaymentMethod", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(inputDoc.getAttribute("/Terms/amount_currency_code/"), returnFlag, //"Payment Currency");
						resourceMgr.getText("DomesticPaymentRequest.PaymentCurrency", TradePortalConstants.TEXT_BUNDLE));

				

	
				//Perform some validatiosn based on the Payemnt Method of the Transaction
				if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(enteredPaymentMethod))
				{
					validatePayeeField(inputDoc.getAttribute("/DomesticPayment/country"), returnFlag,
							resourceMgr.getText("DomesticPaymentRequest.Country", TradePortalConstants.TEXT_BUNDLE));

				}

				if (StringFunction.isNotBlank(inputDoc
								.getAttribute("/DomesticPayment/payee_instruction_number"))) {
					validateNumericField(
							inputDoc
									.getAttribute("/DomesticPayment/payee_instruction_number"),
							returnFlag, // "Beneficiary Instruction Number");
							resourceMgr
									.getText(
											"DomesticPaymentRequest.BeneInstructionsNum",
											TradePortalConstants.TEXT_BUNDLE));
				}
			

				if (StringFunction.isNotBlank(enteredPaymentMethod))
				{
					if ((TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(enteredPaymentMethod)) ||
							(TradePortalConstants.PAYMENT_METHOD_RTGS.equals(enteredPaymentMethod)) ||
							(TradePortalConstants.PAYMENT_METHOD_CBFT.equals(enteredPaymentMethod)) ||
							(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(enteredPaymentMethod)) ||
							(TradePortalConstants.PAYMENT_METHOD_BKT.equals(enteredPaymentMethod)))
					{
						validatePayeeField(inputDoc.getAttribute("/DomesticPayment/payee_bank_code"), returnFlag,
								resourceMgr.getText(partyType+"BankCode", TradePortalConstants.TEXT_BUNDLE));


						validateBankBranchRuleCombination(inputDoc, returnFlag, true); //Vshah BankBranchRule Validation...
					}

				}
		
				//Make validatePaymentMethodRuleCombination() return number of payemnt offset days
				//and call calculateValueDate() to get Bext Business Day passing this offset

				int valueDateOffset = validatePaymentMethodRuleCombination(inputDoc, returnFlag, true);	//Vshah/IAZ Payment Method Rule Validation

				if (valueDateOffset >= 0)
					calculateValueDate(inputDoc, valueDateOffset);

			

				//Check that amount entered is valid
				String enteredPaymentAmount = inputDoc.getAttribute("/DomesticPayment/amount"); //move to DP
				try
				{
					LOG.debug("DOMPMTBean::doAddDomesticPayment::Normal Processing");
					enteredPaymentAmount =
						NumberValidator.getNonInternationalizedValue(enteredPaymentAmount,
								getResourceManager().getResourceLocale());
					String currencyCode = inputDoc.getAttribute("/Terms/amount_currency_code");
					// IAZ CR-483B 08/13/09 Begin -- make this validation conditional
					if ((StringFunction.isNotBlank(enteredPaymentAmount))&&(new Float(enteredPaymentAmount).intValue() != 0))
					{
						TPCurrencyUtility.validateAmount(currencyCode,
								enteredPaymentAmount,
								"Payment Amount",
								this.getErrorManager() );
					}
			
					if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
					{
						returnFlag = false;
					}
				
				}
				catch (Exception any_e)
				{
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_ERR, enteredPaymentAmount);
					enteredPaymentAmount = "0";
					returnFlag = false;
				}

			}

            // CR-486 PDUK072834375 Check customer_reference does not have unicode.
            if (!StringFunction.canEncodeIn_WIN1252(inputDoc.getAttribute("/DomesticPayment/customer_reference"))) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.UNICODE_NOT_ALLOWED,
                        getResourceManager().getText("DomesticPaymentRequest.CustomerReference",
                                TradePortalConstants.TEXT_BUNDLE));
                returnFlag = false;
            }

            performEmailValidation(inputDoc.getAttribute("/DomesticPayment/payee_email"));   //IR KIUL080237339 - 08/11/11
    		inputDoc.setAttribute("/DomesticPayment/amount_currency_code", inputDoc.getAttribute("/Terms/amount_currency_code")); // DK IR T36000024166 Rel8.4 01/22/2014
		}
		catch (Exception any_e)
		{
			LOG.error ("DOMPMTBean::doAddDomesticPayment::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_UPD_ERR_WRN);
			returnFlag = false;

		}

		return returnFlag;
		
	}


	//BSL IR# PBUL042033374 04/29/11 Begin - add new overload
	/**
	 * This method verifies a single Domestic Payment on the Domestic Payment
	 * Transaction.
	 * Method verifies that all required fields are present
	 * Method verifies that amount is not zero
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param pmValueDates Hashtable - contains calculated Value Dates for each Payment Type
	 * @param creditAccountBankCountryCode country code used in bank branch validation
	 * @param debitAccountBankCountryCode country code used in bank branch validation
	 * @return String -- success indicator or the payee_name attribute
	 * of failed Domestic Payment entry to report to the user
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode)
	throws RemoteException, AmsException {
		return verifyDomesticPayment(inputDoc, pmValueDates,
				creditAccountBankCountryCode, debitAccountBankCountryCode,
				false);
	}
	//BSL IR# PBUL042033374 04/29/11 End

	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates, String creditAccountBankCountryCode,
			String debitAccountBankCountryCode, boolean isInitialFileUploadVerify)
	throws RemoteException, AmsException
	{
		return verifyDomesticPayment(inputDoc, pmValueDates,
				creditAccountBankCountryCode, debitAccountBankCountryCode,
				isInitialFileUploadVerify, false);
	}

	/**
	 * This method verifies a single Domestic Payment on the Domestic Payment
	 * Transaction.
	 * Method verifies that all required fields are present
	 * Method verifies that amount is not zero
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param pmValueDates Hashtable - contains calculated Value Dates for each Payment Type
	 * @param creditAccountBankCountryCode country code used in bank branch validation
	 * @param debitAccountBankCountryCode country code used in bank branch validation
	 * @param isInitialFileUploadVerify boolean - set true on initial verify of uploaded payment file
	 * @return String -- success indicator or the payee_name attribute
	 * of failed Domestic Payment entry to report to the user
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	//public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates, String creditAccountBankCountryCode, String debitAccountBankCountryCode) //BSL IR# PBUL042033374 04/29/11 Delete
	public String verifyDomesticPayment (DocumentHandler inputDoc, Hashtable pmValueDates, String creditAccountBankCountryCode,
			String debitAccountBankCountryCode, boolean isInitialFileUploadVerify, boolean isErrorFoundInPaneProcess)
	throws RemoteException, AmsException
	{
		long methodStart = System.currentTimeMillis();
		LOG.debug("[PERFORMANCE-1] \tInside DPBean::Verify\t {}" , System.currentTimeMillis());
		String successfulVerify = TradePortalConstants.DOM_PMT_VERIFY_SUCCESSFUL;
		String domesticPaymentPayee = null;
		try
		{
			
			domesticPaymentPayee = this.getAttribute("payee_name");

			String uploadedInd = inputDoc.getAttribute("/Transaction/uploaded_ind");
			PaymentParty firstIntBank = null;

			if (!(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(uploadedInd) || uploadedInd == null)) { //RKAZI IR ciul112233081 04/26/2011
																											 //added null check as uploadedInd is null when
																											 // submitted from UI.
				String beneBranchCityAndProvince = this.getAttribute("address_line_3");
				if (!StringFunction.isBlank(beneBranchCityAndProvince))
				{
					if (beneBranchCityAndProvince.length() > TradePortalConstants.MAX_CITY_PROVINCE_SIZE)
					{
						this.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.CITY_PROVINCE_MORE_THAN_31, domesticPaymentPayee);
					}
				}
				//for first intermediary bank
				if (StringFunction.isNotBlank(this.getAttribute("c_FirstIntermediaryBank")))
					firstIntBank = (PaymentParty) this.getComponentHandle("FirstIntermediaryBank");
				if(null != firstIntBank){
					String firstIntBranchCityAndProvince=firstIntBank.getAttribute("address_line_3");
					if (!StringFunction.isBlank(firstIntBranchCityAndProvince))
					{
						if (firstIntBranchCityAndProvince.length() > TradePortalConstants.MAX_CITY_PROVINCE_SIZE)
						{
							this.getErrorManager().issueError(getClass().getName(),
									TradePortalConstants.CITY_PROVINCE_MORE_THAN_31, domesticPaymentPayee);
						}
					}
				}
			}

			if (StringFunction.isBlank(this.getAttribute("sequence_number"))) {
				// Generate sequence number
				StringBuffer seqNumSql = new StringBuffer();
				seqNumSql.append("select max(SEQUENCE_NUMBER) as SEQUENCE_NUMBER from domestic_payment where p_transaction_oid = ?");

				String seqNum = "1";
				DocumentHandler seqNumRS = DatabaseQueryBean.getXmlResultSet(seqNumSql.toString(), false, new Object[]{this.getAttribute("transaction_oid")});
				if (seqNumRS != null) {
					seqNum = seqNumRS.getAttribute("/ResultSetRecord(0)/SEQUENCE_NUMBER");
					if (StringFunction.isNotBlank(seqNum)) {
						seqNum = (new Integer(Integer.parseInt(seqNum) + 1)).toString();
					}
					else {
						seqNum = "1";
					}
				}

				this.setAttribute("sequence_number", seqNum);
			}
	
			String amountStr = this.getAttribute("amount");
			if (!StringFunction.isBlank(amountStr))
			{
				if ((new BigDecimal(amountStr)).floatValue() == 0)
				{
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DOM_PMT_AMOUNT_IS_ZERO, domesticPaymentPayee);
				}
			}

			//Prepare Prefix for fields aliases for error reporting based on payemnt/transaction type
			String partyType = "DomesticPaymentRequest.Payee";
			if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(this.getAttribute("payment_method_type")))
				partyType = "DirectDebitInstruction.Payer";

			//Validate Required Fields
			boolean returnFlag = true;
			//IAZ IR PIUJ091776300 09/18/09 Begin - read fields descriptions form the properties files vs being hardcoded here
			ResourceManager resourceMgr = getResourceManager();

			//RKAZI IR- RUL052649194 05/26/2011 - add required field validation for payee address line 1 when paymentMethod is CBFT - Start

			validatePayeeField(this.getAttribute("payee_name"), returnFlag, //"Payee Name");
					resourceMgr.getText(partyType+"Name", TradePortalConstants.TEXT_BUNDLE));



			validatePayeeField(this.getAttribute("bank_charges_type"), returnFlag, //"Bank Charges Type");
					resourceMgr.getText("DomesticPaymentRequest.BankCharges", TradePortalConstants.TEXT_BUNDLE));


			validatePayeeField(this.getAttribute("amount"), returnFlag, //"Amount");
					resourceMgr.getText("FundsTransferRequest.Amount", TradePortalConstants.TEXT_BUNDLE));

			validatePayeeField(this.getAttribute("payment_method_type"), returnFlag, //"Payment Method");	//IAZ CR-483B 08/13/09 Add
					resourceMgr.getText("DomesticPaymentRequest.PaymentMethod", TradePortalConstants.TEXT_BUNDLE));
			//curruncy code will be validated by Terms Manager
			validatePayeeField(inputDoc.getAttribute("/Terms/amount_currency_code/"), returnFlag, //"Payment Currency");
					resourceMgr.getText("DomesticPaymentRequest.PaymentCurrency", TradePortalConstants.TEXT_BUNDLE));
		
			if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(this.getAttribute("payment_method_type")))
			{
				validatePayeeField(this.getAttribute("country"), returnFlag,
						resourceMgr.getText("DomesticPaymentRequest.Country", TradePortalConstants.TEXT_BUNDLE));
				
		
				validatePayeeField(this.getAttribute("payee_address_line_1"), returnFlag,
						resourceMgr.getText("DomesticPaymentBeanAlias.payee_address_line_1", TradePortalConstants.TEXT_BUNDLE));
				
	

				final String[] attributesToExclude = {"payee_name","payee_description", "other_charges_instructions", "payee_email"};
				long chkSwift = System.currentTimeMillis();
				InstrumentServices.checkForInvalidSwiftCharacters(this.getAttributeHash(), this.getErrorManager(), attributesToExclude);
				LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE] \tInstrumentServices.checkForInvalidSwiftCharacters \t {} \tmilliseconds ",(System.currentTimeMillis()-chkSwift));
			}
			
	        


			if (StringFunction.isNotBlank(this.getAttribute("payment_method_type")))
			{
				if ((TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(this.getAttribute("payment_method_type"))) ||
						(TradePortalConstants.PAYMENT_METHOD_RTGS.equals(this.getAttribute("payment_method_type"))) ||
						(TradePortalConstants.PAYMENT_METHOD_CBFT.equals(this.getAttribute("payment_method_type"))) ||
						(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(this.getAttribute("payment_method_type"))) ||
						(TradePortalConstants.PAYMENT_METHOD_BKT.equals(this.getAttribute("payment_method_type"))))
				{

					validatePayeeField(this.getAttribute("payee_account_number"), returnFlag, //"Payee Account Number");	//IAZ IR-SSUK012149800 01/26/10 Moved
							resourceMgr.getText(partyType+"AccountNumber", TradePortalConstants.TEXT_BUNDLE));

					validatePayeeField(this.getAttribute("payee_bank_code"), returnFlag,
							resourceMgr.getText(partyType+"BankCode", TradePortalConstants.TEXT_BUNDLE));

					long bankBranchRule = System.currentTimeMillis();
					validateBankBranchRuleCombination(inputDoc, returnFlag, false, creditAccountBankCountryCode, debitAccountBankCountryCode); //Vshah BankBranchRule Validation...
					LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE] \tvalidateBankBranchRuleCombination \t {} \tmilliseconds", (System.currentTimeMillis()-bankBranchRule));
				}

			}

		
			//Make validatePaymentMethodRuleCombination() return number of payemnt offset days
			//and call calculateValueDate() to get Next Business Day passing this offset.
			//02/22/10: If we already calculated Valuedate for given payemnt Type, use previously calculated value as it is teh same
			//          for all Payees for given payment Type.

			if ((StringFunction.isNotBlank(this.getAttribute("payment_method_type"))) &&
					(StringFunction.isNotBlank((String)pmValueDates.get(this.getAttribute("payment_method_type")))))
			{
				inputDoc.setAttribute("/DomesticPayment/value_date", (String)pmValueDates.get(this.getAttribute("payment_method_type")));
				LOG.debug("DOMPMTBean::verifyDomPmt:: Value Date is already calculated for {} {}" ,
						this.getAttribute("payment_method_type"),
						(String)pmValueDates.get(this.getAttribute("payment_method_type")));
			}
			else
			{
				long validatePayment = System.currentTimeMillis();
				int valueDateOffset = validatePaymentMethodRuleCombination(inputDoc, returnFlag, false, creditAccountBankCountryCode, debitAccountBankCountryCode);//Vshah Payment Method Rule Validation
				LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE] \tvalidatePaymentMethodRuleCombination\t {} \tmilliseconds",(System.currentTimeMillis()-validatePayment));

				if (valueDateOffset >= 0)
				{
					long calValDate = System.currentTimeMillis();
					calculateValueDate(inputDoc, valueDateOffset);
					LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE]  \tcalculateValueDate \t {} \tmilliseconds", (System.currentTimeMillis()-calValDate));
				}
			}

			if (inputDoc.getAttribute("/DomesticPayment/value_date") != null)
			{
				this.setAttribute("value_date", inputDoc.getAttribute("/DomesticPayment/value_date"));
				pmValueDates.put(this.getAttribute("payment_method_type"), inputDoc.getAttribute("/DomesticPayment/value_date"));
			}

			// Validate amount is in format of the currency
			TPCurrencyUtility.validateAmount(inputDoc.getAttribute("/Terms/amount_currency_code/"),
					this.getAttribute("amount"),
					"Payment Amount",
					this.getErrorManager() );
			

			//Verify Reporting Code for any Payments Transaction
			if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(this.getAttribute("payment_method_type")))
			{
				validateReportingCodes(inputDoc, this.getAttribute("payment_method_type"), true);
				validateCheckDetails(inputDoc, true, isInitialFileUploadVerify);//BSL IR#PBUL042033374 04/29/11 Add Overload
			}
			

			performSemicolonEdit(); 

			performEmailValidation(getAttribute("payee_email"));   

			//If error has been detected, return name of the payee at fault
			//Otherwise, assign Transaction Currenecy to the individual payemnt, save and return success for this payment
			if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY || isErrorFoundInPaneProcess)
			{
				successfulVerify = this.getAttribute("payee_name");
			}
			// IAZ CR-483B 08/13/09 Begin
			else
			{
				this.setAttribute("amount_currency_code",
						inputDoc.getAttribute("/Terms/amount_currency_code/"));

				//IAZ CM CR-507 12/12/09 Begin: if this is Cross Border transfer, clear up address 4 before saving
				if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(this.getAttribute("payment_method_type")))
					this.setAttribute("payee_address_line_4", "");
				
				long saveStart = System.currentTimeMillis();
				this.save();
				LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE] \tDomesticPaymentBean.save\t {} \tmilliseconds", (System.currentTimeMillis()-saveStart));
			}
			

		}
		catch (InvalidObjectIdentifierException ex)
		{
			// If the object does not exist, issue an error
			this.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());
			successfulVerify = domesticPaymentPayee;

		} catch (Exception any_e)
		{
			LOG.error("DOMPMTBean::verifyDomPmt::Exception: ",any_e);
			this.getErrorManager ().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PROVIDE_ALL_REQ_PARMS,
					"");
			successfulVerify = domesticPaymentPayee;

		} finally
		{
			if (TradePortalConstants.DOM_PMT_VERIFY_SUCCESSFUL.equals(successfulVerify))
				this.debug("Dopestic Payment Verify Complete");
			else
				this.debug("Dopestic Payment Verify Complete with errors");
		}

		LOG.debug("\t[DomesticPaymentBean.verifyDomesticPayment(DocumentHandler,Hashtable)] \t[PERFORMANCE]\tverifyDomesticPayment\t {} \tmilliseconds", (System.currentTimeMillis()-methodStart));
		return successfulVerify;
		
	}


	// IR KIUL080237339 - 08/11/11 - Start
	/**
	 *  Validates beneficary email
	 * @throws AmsException
	 * @throws RemoteException
	 */
	protected void performEmailValidation(String emailList) throws RemoteException, AmsException {
		if (StringFunction.isBlank(emailList)) return;

	    if (!EmailValidator.validateEmailList(emailList))	{
	    	getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_EMAIL,
					"");
	    }
	}

	// IR KIUL080237339 - 08/11/11 - End

	/**
	 * This method deletes a single Domestic Payment from the Domestic Payment
	 * Transaction/ Template.
	 * Method verifies that all required fields are presnt            .
	 * Method verifies that amount is not zero            .
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @return Sting -- success indicator or the payee_name attribute of failed Domestic Payment entry to report to the user
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	public String deleteDomesticPayment (DocumentHandler inputDoc)
	throws RemoteException, AmsException
	{

		String domesticPaymentPayee = null;
		try
		{
                //rkazi - IR SAUK121782011 - 01/15/2011 - Begin
                if (!("SelectTemplate".equals(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
                    TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/DomesticPayment/fixedPmtInd/")))
                {
                    LOG.debug("DPBean::deleteDomesticPayment - trying to delete payee to fixed template...");
                    if (!StringFunction.isBlank(inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid/")))
                        {
                            domesticPaymentPayee = TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR;
                        }
                }
                else
                //rkazi - IR SAUK121782011 - 01/15/2011 - End
                {
                    //Domestic payment oid must be supplied
                    long domesticPaymentOid = 0;

					String domesticPaymentOidStr =
						inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid/");
					if (StringFunction.isBlank(domesticPaymentOidStr))
					{
						LOG.debug("DomesticPaymentBean::Delete - Error: domestic_payment_oid not present");
						return TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE;
					}
					else
					{
						domesticPaymentOid = Long.parseLong(domesticPaymentOidStr);
					}

					//Obtains data; save payee_name attribute for reportin; delete DP Bean
					
					this.getData(domesticPaymentOid);
					domesticPaymentPayee = this.getAttribute("payee_name"); //store this in case it is needed with the error msg
					//store this so the inputDoc contains the amoutn from the database in case it is needed by the calling routine.
					String domesticPaymentAmount = this.getAttribute("amount");
					inputDoc.setAttribute("/DomesticPayment/amount", domesticPaymentAmount);
					this.delete();
                }
		}
		catch (InvalidObjectIdentifierException ex)
		{
			// If the object does not exist, issue an error
			this.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());
			domesticPaymentPayee = TradePortalConstants.TASK_NOT_SUCCESSFUL;
		} catch (Exception any_e)
		{
			LOG.error("DomesticPaymentBean::Delete - Error: generic exception" ,any_e);
			domesticPaymentPayee = TradePortalConstants.TASK_NOT_SUCCESSFUL;
		} finally
		{

			this.debug("DomesticPaymentBean: Result of delete is "
					+ domesticPaymentPayee);
		}
		return domesticPaymentPayee;
		
	}

	private void validatePayeeField(String fieldValue, boolean returnFlag, String fieldName)
	throws RemoteException, AmsException
	{
		Pattern ALPHANUMERICPATTERN = Pattern.compile("[\\s\\w-?:()/.,'+\\n\\r%��������������������������]+");
		if (StringFunction.isBlank(fieldValue)) {
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.PAYEE_DATA_MISSING_ERR, fieldName);

		}
		// DK IR T36000020202 Rel8.4 09/10/2013 starts
		else if(fieldName.equals(getResourceManager().getText("DomesticPaymentRequest.PayeeAccountNumber", TradePortalConstants.TEXT_BUNDLE))){
			Matcher m = ALPHANUMERICPATTERN.matcher(fieldValue);
			if(!m.matches())
				this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.MUST_BE_ALPHA_NUMERIC, fieldName);
		}
		// DK IR T36000020202 Rel8.4 09/10/2013 ends
	}

	//VShah - CR564 - 03/01/2011 - BEGIN
	private void performSemicolonEdit()
	throws RemoteException, AmsException
	{
	    //cquinton 2/7/2011 Rel 6.1.0.0 ir#paul020667882 - add mailing_address_line_4
		String attributeNames[] = {"payee_name",
		    "payee_address_line_1","payee_address_line_2",
		    "payee_address_line_3","payee_address_line_4",
		    "payee_email",
		    "mailing_address_line_1","mailing_address_line_2",
		    "mailing_address_line_3","mailing_address_line_4",
		    "central_bank_reporting_1","central_bank_reporting_2","central_bank_reporting_3",
		    "customer_reference","payee_description","payable_location","print_location"};
		Hashtable attributeValues = this.getAttributes(attributeNames);
		String value = null;
		for (int i = 0; i < attributeNames.length; i++)
		{
			value = (String) attributeValues.get(attributeNames[i]);
			if ( StringFunction.isNotBlank(value) && (value.contains(";") || value.contains("|")))
				this.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.SEMICOLONS_NOT_ALLOWED, attributeMgr.getAlias(attributeNames[i]));
		}
	}
	//VShah - CR564 - 03/01/2011 - END


	// Narayan IR-SLUK02154665 Begin
	private void validateNumericField(String fieldValue, boolean returnFlag,
			String fieldName) throws RemoteException, AmsException {
		if (!fieldValue.matches("^\\d*$")) {
			String[] substitutionValues = { "", "" };
			substitutionValues[1] = fieldName;
			this.getErrorManager().issueError(getClass().getName(),
					AmsConstants.INVALID_NUMBER_ATTRIBUTE, substitutionValues);

		}
	}

	// Narayan IR-SLUK02154665 End

	private int validatePaymentMethodRuleCombination(
			DocumentHandler inpuDocumentHandler, boolean returnFlag, boolean fromInputDoc)
	throws RemoteException, AmsException {
		return validatePaymentMethodRuleCombination(inpuDocumentHandler, returnFlag, fromInputDoc, "", "" );
	}

	// NSX 10/07/11 - CR-581/640 Rel. 7.1 - Begin
	/**
	*  This method calculates value data and payment date.
	*
	*/
	public DocumentHandler calculatePaymentDate(DocumentHandler inputDoc, String debitAccountBankCountryCode) throws RemoteException, AmsException {
				int valueDateOffset = validatePaymentMethodRuleCombination(inputDoc, false, false, debitAccountBankCountryCode, debitAccountBankCountryCode);

				if (valueDateOffset >= 0)
				{
					calculateValueDate(inputDoc, valueDateOffset);
				}
				return inputDoc;
			}


	//Vshah Payment Method Rule Validation - Start
	private int validatePaymentMethodRuleCombination(
			DocumentHandler inputDocumentHandler, boolean returnFlag, boolean fromInputDoc,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode)   //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	throws RemoteException, AmsException {

        //cquinton 2/3/2011 Rel 6.1.0 ir#hiul020257710
        //future date domestic payments still need to verify the payment method rule combination
        //they just don't consider the offset days for changing value date
        //accordingly move check for future date to after the selection query below

		StringBuilder whereClause = new StringBuilder();
		String paymentMethod = this.getAttribute("payment_method_type");
		if (fromInputDoc)
			paymentMethod = inputDocumentHandler.getAttribute("/DomesticPayment/payment_method_type/");

		String country = "";
		String currency = inputDocumentHandler.getAttribute("/Terms/amount_currency_code");

		//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
		//removed logic to instantiate an Account object for every domestic payment, because it is the
		//same Account for all Dom. Payments in one transaction.   The TransactionMediator will now
		//determine the account's bank country code before validating each Dom. Payment.
		
		if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethod)) {
			country = creditAccountBankCountryCode;
		} else {
			country = debitAccountBankCountryCode;
		}
		//trudden CR-564 08/31/10 End

		List<Object> sqlParams = new ArrayList<Object>();
		whereClause.append(" PAYMENT_METHOD = ? AND ");
		sqlParams.add(paymentMethod);
		if (StringFunction.isNotBlank(country)) {
		   whereClause.append("COUNTRY = ? AND ");
		   sqlParams.add(country);
		}
		whereClause.append("CURRENCY = ? ");
		sqlParams.add(currency);

	
		StringBuffer selectOffsetdays = (new StringBuffer("select offset_days from PAYMENT_METHOD_VALIDATION where")).append(whereClause);

		DocumentHandler paymentMethodRec = DatabaseQueryBean.getXmlResultSet(selectOffsetdays.toString(), false, sqlParams);
		if (paymentMethodRec == null)
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION,
					paymentMethod, currency, country);

			return -1;
		}
		String strOffsetDays = paymentMethodRec.getAttribute("/ResultSetRecord(0)/OFFSET_DAYS");
		if (StringFunction.isBlank(strOffsetDays))
		{
			LOG.debug("Can't retrive OffsetDays form the PM record");
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION,
					paymentMethod, currency, country);
			returnFlag = false;
			return -1;
		}

        //cquinton 2/3/2011 Rel 6.1.0 ir#hiul020257710
        //after the actual validation, force offset days to zero for
        //future date domestic payments
      
        String typeCode = inputDocumentHandler.getAttribute("/Instrument/instrument_type_code");
        if ((StringFunction.isNotBlank(typeCode)) && (typeCode.equals(InstrumentType.DOM_PMT)))
        {
            if (TPDateTimeUtility.isFutureDate(inputDocumentHandler.getAttribute("/Transaction/payment_date")))
                return 0;
        }
        

		return ((new Integer(strOffsetDays)).intValue());
		

	}
	//Vshah Payment Method Rule Validation - End

	//Vshah BankBranchRule Validation - Start
	private void validateBankBranchRuleCombination(
			DocumentHandler inpuDocumentHandler, boolean returnFlag, boolean fromInputDoc)
	throws RemoteException, AmsException {
		validateBankBranchRuleCombination(inpuDocumentHandler, returnFlag, fromInputDoc, "", "" );
	}

	private void validateBankBranchRuleCombination(
			DocumentHandler inpuDocumentHandler, boolean returnFlag, boolean fromInputDoc,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode)   
	throws RemoteException, AmsException {

		long startMethod = System.currentTimeMillis();
		String bankBranchCode[] = new String[2]; 
		String errorSubstitute[] = new String[2]; 
		DocumentHandler outBankDetailsArray[] = {null, null, null, null};	
		ResourceManager resourceMgr = getResourceManager();

		String paymentMethod = "";

		final int beneBankInt = 0;

		final int fiBankInt = 1; 

		if (fromInputDoc)
			paymentMethod = inpuDocumentHandler.getAttribute("/DomesticPayment/payment_method_type/");
		else
			paymentMethod = this.getAttribute("payment_method_type");

		String country = "";

		if (fromInputDoc)
		{
			bankBranchCode[beneBankInt] = inpuDocumentHandler.getAttribute("/DomesticPayment/payee_bank_code/");
			bankBranchCode[fiBankInt] = inpuDocumentHandler.getAttribute("/DomesticPayment/FirstPaymentParty/bank_branch_code/");
			if ((StringFunction.isBlank(inpuDocumentHandler.getAttribute("/DomesticPayment/payee_bank_name/")))
				|| (TradePortalConstants.INDICATOR_YES.equals(inpuDocumentHandler.getAttribute("/DomesticPayment/BBCode_Updated/"))))
				outBankDetailsArray[beneBankInt] = inpuDocumentHandler;
			if ((StringFunction.isBlank(inpuDocumentHandler.getAttribute("/DomesticPayment/FirstPaymentParty/bank_name/")))
				|| (TradePortalConstants.INDICATOR_YES.equals(inpuDocumentHandler.getAttribute("/DomesticPayment/FirstPaymentParty/BBCode_Updated/"))))
				outBankDetailsArray[fiBankInt] = inpuDocumentHandler;
		}
		else
		{
			long startGetComponent = System.currentTimeMillis();

			bankBranchCode[beneBankInt] = this.getAttribute("payee_bank_code");
			if (StringFunction.isNotBlank(this.getAttribute("c_FirstIntermediaryBank"))) {
				PaymentParty party = (PaymentParty)this.getComponentHandle("FirstIntermediaryBank");
				bankBranchCode[fiBankInt] = party.getAttribute("bank_branch_code");
			}

			LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] \t[PERFORMANCE]\tgetComponentForIntBank\t {}\tmilliseconds",(System.currentTimeMillis()-startGetComponent));
			
		}

		if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethod))
			errorSubstitute[beneBankInt] = resourceMgr.getText("DirectDebitInstruction.PayerBankBranch", TradePortalConstants.TEXT_BUNDLE);
		else
			errorSubstitute[beneBankInt] = resourceMgr.getText("DomesticPaymentRequest.PayeeBankCode", TradePortalConstants.TEXT_BUNDLE);

		errorSubstitute[fiBankInt] = resourceMgr.getText("DomesticPaymentRequest.FirstIntBankBranchCode", TradePortalConstants.TEXT_BUNDLE);



		
		if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethod)) {
			country = creditAccountBankCountryCode;
		} else {
			country = debitAccountBankCountryCode;
		}


		LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] \t[PERFORMANCE]\tpre-for-loop \t {} \tmilliseconds",(System.currentTimeMillis()-startMethod));

		for (int i=0; i < bankBranchCode.length; i++)
		{

			if (StringFunction.isNotBlank(bankBranchCode[i]))
			{
				String usePaymentMethod = paymentMethod;
				
				String useCountry = country;															
				if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod))						
					useCountry = null;

				//VS CR 573  06/15/10 Added ErrorSubstitute to method
				if (getBankBranchRuleCount(usePaymentMethod, bankBranchCode[i], useCountry, 			
																		outBankDetailsArray[i], errorSubstitute[i], i) <= 0)	
					{
						this.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.INVALID_BANK_BRANCH_RULE_COMBINATION,
								errorSubstitute[i] );

					}
				
			}
		}
		
	}

	private int getBankBranchRuleCount(String paymentMethod, String bankBranchCode, String country,
													DocumentHandler outBankDetails, String errorSubstitute, int bankPartyIndex)	
	throws RemoteException, AmsException {
		List<Object> sqlParams = new ArrayList<Object>();
		StringBuilder whereClause = new StringBuilder();
		boolean checkUnicode = false; 
		whereClause.append(" BANK_BRANCH_CODE = ? ");
		sqlParams.add(bankBranchCode);
		if (StringFunction.isNotBlank(paymentMethod)){
			whereClause.append(" AND PAYMENT_METHOD = ? ");
			sqlParams.add(paymentMethod);
		}
		if (StringFunction.isNotBlank(country))	{													
			whereClause.append(" AND ADDRESS_COUNTRY = ? ");
			sqlParams.add(country);
		}

		LOG.debug("DOMPMTBean::getBankBranchRuleCount::whereClause: {} " , whereClause.toString());


		//If Bank Details has not been retrieved via Bank Search Page (Bank Branch Code was manually enetered),
		//we need to pull those details form Bank Branch Rule table on Save (here).  If they are provided, just validate
		//Bank Branch Code.
		if (outBankDetails == null)
		{
		    Cache cache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.BANK_BRANCH_RULE_CACHE);
			DocumentHandler result = (DocumentHandler)cache.get(buildBankBranchRuleKey(paymentMethod, bankBranchCode, country));
		    if (result != null) {
		    	try {
		    	   String count = result.getAttribute("/ResultSetRecord(0)/BANK_BRANCH_RULE_COUNT");
				   return Integer.parseInt(count);
		    	}
		    	catch (NumberFormatException e) {
		    		return 0;
		    	}
		    }
			return 0;
		}
		else
		{

			String unicodeIndicatorClause = " AND unicode_indicator = 'N'";
			if ((TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)) ||
				(TradePortalConstants.PAYMENT_METHOD_RTGS .equals(paymentMethod)))
			{
				unicodeIndicatorClause = " order by unicode_indicator desc";
				checkUnicode = true; //VS CR 573
			}
			String getBankDetails = "SELECT BANK_NAME, BRANCH_NAME, ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_CITY, ADDRESS_STATE_PROVINCE, ADDRESS_COUNTRY, UNICODE_INDICATOR" +
									" FROM BANK_BRANCH_RULE WHERE " + whereClause.toString() + unicodeIndicatorClause;

			DocumentHandler bankDetails = DatabaseQueryBean.getXmlResultSet(getBankDetails, false, sqlParams);
			
			//If no records found, stop processing and display error to the user
			if (bankDetails == null){
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.INVALID_BANK_BRANCH_RULE,
						errorSubstitute );
				return 0;
			}
			else
			{
				Vector records = bankDetails.getFragments("/ResultSetRecord");
				int count = records.size();
				if((count > 2) || ((count>1) && (!checkUnicode)))
				{
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.MULTIPLE_BANK_BRANCH_RULE,
							bankBranchCode );
							return 1;
				}

				if (count == 2)
				{
					if (TradePortalConstants.INDICATOR_YES.equals(bankDetails.getAttribute("/ResultSetRecord(1)/UNICODE_INDICATOR")))
					{
						this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.MULTIPLE_BANK_BRANCH_RULE,
							bankBranchCode );
							return 1;
					}
					//VS PDUK062485929 Changed error code from invalid bank branch rule to multiple bank branch rule
					else if (TradePortalConstants.INDICATOR_NO.equals(bankDetails.getAttribute("/ResultSetRecord(0)/UNICODE_INDICATOR")))
					{
						this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.MULTIPLE_BANK_BRANCH_RULE,
							errorSubstitute );
							return 0;
					}

				}

				updateBankDetails(bankDetails, outBankDetails, bankPartyIndex);
				return 1;
			}
			
		}
		

	}
	//Vshah BankBranchRule Validation - End


	//This method will move data retrieved form database to proper tags of inputDocumentHandler (outBankDetails)
	//form where those details will be moved to appropriate Domestic Payment and/or Payment Party records at the later point
	//of Save()
	private void updateBankDetails(DocumentHandler bankDetails, DocumentHandler outBankDetails, int bankPartyIndex)
	throws RemoteException, AmsException {


		LOG.debug("DOMPMTBean::updateBankDetails::Processing Bank Party with index {}" , bankPartyIndex);
		String partyType = "";


		String bankName = bankDetails.getAttribute("/ResultSetRecord(0)/BANK_NAME");
		String branchName = bankDetails.getAttribute("/ResultSetRecord(0)/BRANCH_NAME");
		String addressCity = bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_CITY");
		String addressStateProvince = bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_STATE_PROVINCE");

		if (bankName == null )
		{
			bankName = "";
		}
		else
		{
			if (bankName.length() > 35)
				//get first 35 characters
				bankName = bankName.substring(0, 35);
		}
		if (branchName == null )
		{
			branchName = "";
		}
		else
		{
			if (branchName.length() > 35)
				//get first 35 characters
				branchName = branchName.substring(0, 35);
		}

		if (branchName == null )
		{
			branchName = "";
		}
		else
		{
			if (branchName.length() > 35)
				//get first 35 characters
				branchName = branchName.substring(0, 35);
		}

		if (addressStateProvince == null )
		{
			addressStateProvince = "";
		}
		if (addressCity == null )
		{
			addressCity = "";
		}
		else
		{
			int provinceLen = addressStateProvince.length();
			if (StringFunction.isNotBlank(addressStateProvince)) {
				provinceLen++;   // increase length for space delimiter
			}

			int len = 35 - provinceLen;
			if (addressCity.length() > len)
				addressCity = addressCity.substring(0, len);
		}


		if (bankPartyIndex == 0)

		{
			
				outBankDetails.setAttribute("/DomesticPayment/payee_bank_name/", bankName);
				outBankDetails.setAttribute("/DomesticPayment/payee_branch_name/", branchName);
	
				outBankDetails.setAttribute("/DomesticPayment/payee_bank_country/",
									bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_COUNTRY"));
		}

		else
		{
			if (bankPartyIndex == 1)
			{
				//partyType = "/OrderingPartyBank";
			}
			else if (bankPartyIndex == 2)
			{
				partyType = "/FirstPaymentParty";
			}
			else if (bankPartyIndex == 3)
			{
			//	partyType = "/SecondPaymentParty";
			}
		
			outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/bank_name/", bankName);
			outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/branch_name/", branchName);
	
			outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/country/",
								bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_COUNTRY"));

		}

		outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/address_line_1/",
					bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_1"));
		outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/address_line_2/",
					bankDetails.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_2"));
		
		outBankDetails.setAttribute("/DomesticPayment"+ partyType + "/address_line_3/",
				(addressCity + " " + addressStateProvince).trim());
	
	}


	

	/**
	 * This method will concatenate the key for lookups of the BankBranchRule cache.
	 * The concatentated key is made up of BankBranchCode, paymentMethod and country,
	 * and the fields are separated by |
	 * @param paymentMethod string
	 * @param bankBranchCode string
	 * @param country string
	 * @return String concatenated key
	 */
	private String buildBankBranchRuleKey(String paymentMethod, String bankBranchCode, String country)	{

       StringBuffer sb = new StringBuffer();
       sb.append(bankBranchCode).append("|");
	   if (StringFunction.isNotBlank(paymentMethod)) {
			sb.append(paymentMethod);
	   } else {
		   sb.append(" ");
	   }
	   sb.append("|");
	   if (StringFunction.isNotBlank(country)) {
			sb.append(country);
	   }

        return sb.toString();

     }
	//trudden CR-564 08/31/10 End

	/**
	 * This method gets Value Date for Payment/DDI tarnsaction.
	 * It obtains necessary records and invokes a method on TPCalendarYear to obtain correct next business day
	 * providing execute date, number of offset days, weekend pattern.
	 * TPCalendarYear object maintain holiday calendar for given year.      .
	 * @param inputDoc com.amsinc.ecsg.util - the input/output doc
	 * @param valueDateOffset - number of offset days as defined in Payment Method record.
	 * @return void -- the value is passed back in inputDoc.
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private void calculateValueDate(DocumentHandler inputDoc, int valueDateOffset)
	throws RemoteException, AmsException
	{
		
		//Get Currency Calendar Rule, TP Calendar and TP Calendar Year records to obtain nececssary input
		String sqlSelect = "Select CC.A_TP_CALENDAR_OID, TC.WEEKEND_DAY_1, TC.WEEKEND_DAY_2 from CURRENCY_CALENDAR_RULE CC, TP_CALENDAR TC"
			+ " where CC.CURRENCY = ? "
			+ " and CC.A_TP_CALENDAR_OID = TC.TP_CALENDAR_OID";


		DocumentHandler calendarCur = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[]{inputDoc.getAttribute("/Terms/amount_currency_code")});
		if (calendarCur == null)
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.CURRENCY_CALENDAR_NOT_DEFINED,
					inputDoc.getAttribute("/Terms/amount_currency_code"));
			return;
		}
		String tpCalOid = calendarCur.getAttribute("/ResultSetRecord(0)/A_TP_CALENDAR_OID");
		if (StringFunction.isBlank(tpCalOid))
		{
			LOG.info("Unexpected Error: Can't retrive Calendar Record OID form the Found Record");
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.CURRENCY_CALENDAR_NOT_DEFINED,
					inputDoc.getAttribute("/Terms/amount_currency_code"));
			return;
		}

		LOG.debug("DOMPMTBean::calculateValueDate::A_TP_CALENDAR_OID: {} " , tpCalOid);

		
		int intYear;
		String strInputDate = inputDoc.getAttribute("/Transaction/payment_date");
		try
		{
			intYear 	= (new Integer(strInputDate.substring(6,10))).intValue();
		}
		catch(Exception any_exc)
		{
			LOG.debug("DOMPMTBean::calculateValueDate::Execute Date is not entered/ is in invalid format. Value Date will be set to none");
			inputDoc.setAttribute("/DomesticPayment/value_date", "");
			return;
		}
		

		sqlSelect = "select TP_CALENDAR_YEAR_OID FROM TP_CALENDAR_YEAR WHERE P_TP_CALENDAR_OID = ?"
		+ " and year = ? ";

	
		DocumentHandler calendarYear = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, tpCalOid, intYear);
		if (calendarYear == null)
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.CALENDAR_NOT_DEFINED_FOR_YEAR,
					inputDoc.getAttribute("/Terms/amount_currency_code"),
					strInputDate.substring(6,10));
			return;
		}
		String calendarYearOid = calendarYear.getAttribute("/ResultSetRecord(0)/TP_CALENDAR_YEAR_OID");
		if (StringFunction.isBlank(calendarYearOid))
		{
			LOG.info("Unexpected Error: Can't retrive Calendar Year Oid form the Found Record");
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.CALENDAR_NOT_DEFINED_FOR_YEAR,
					inputDoc.getAttribute("/Terms/amount_currency_code"),
					strInputDate.substring(6,10));
			return;
		}

		//Obtain weekend days pattern, if defined
		int weekend1 =0;
		int weekend2 =0;

		String strWeekend1 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_1");
		if (StringFunction.isNotBlank(strWeekend1))
			weekend1 = (new Integer(strWeekend1)).intValue();
		String strWeekend2 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_2");
		if (StringFunction.isNotBlank(strWeekend2))
			weekend2 = (new Integer(strWeekend2)).intValue();

		LOG.debug("DOMPMTBean::calculateValueDate::weekends {} and {}" , weekend1 , weekend2);


		//Create TP Calendar Year Object for given year/currency.
		//Invoke method to obtain next business day provided execution date, number of offset days, and weekend dats pattern.
		TPCalendarYear tpCalendarYear = (TPCalendarYear) createServerEJB("TPCalendarYear");
		tpCalendarYear.getData(Long.parseLong(calendarYearOid));
		String valueDate = tpCalendarYear.getNextBusinessDay(inputDoc.getAttribute("/Transaction/payment_date"),
				valueDateOffset, weekend1, weekend2);
		
		LOG.debug("DOMPMTBean::this is value date {}" , valueDate);
		inputDoc.setAttribute("/DomesticPayment/value_date", valueDate.substring(0,10));

		//MDB CR-609 11/30/10 Begin
		//override execute date with updated value date if payment has a future date
		String typeCode = inputDoc.getAttribute("/Instrument/instrument_type_code");
		if ((StringFunction.isNotBlank(typeCode)) && (typeCode.equals(InstrumentType.DOM_PMT)))
		{
			if (TPDateTimeUtility.isFutureDate(inputDoc.getAttribute("/Transaction/payment_date")))
			
				inputDoc.setAttribute("/Transaction/payment_date", valueDate);
		}
		//MDB CR-609 11/30/10 End

		LOG.debug("DOMPMTBean::this is new execution date {}" , inputDoc.getAttribute("/Transaction/payment_date"));
	}

	//BSL IR# PBUL042033374 04/28/11 Begin - Reuse logic from validateReportingCodes in new validateCheckDetails method
	/**
	 * This method checks if the transaction was uploaded.
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param isFixedWidthConsideredFileUpload boolean - uploaded_ind is normally 'N' or 'Y', but on initial verify of a fixed-width upload, false means initial fixed-width is NOT considered a file upload
	 * @return boolean - true if the transaction was uploaded
	 */
	private boolean isPaymentFileUpload(DocumentHandler inputDoc, boolean isFixedWidthInitiallyConsideredFileUpload) throws AmsException {
		String uploadedInd = inputDoc.getAttribute("/Transaction/uploaded_ind");
		if (StringFunction.isBlank(uploadedInd)) {
			String uploadedTranSql = "select UPLOADED_IND from transaction where transaction_oid = ?";
			DocumentHandler uploadedTranRS = DatabaseQueryBean.getXmlResultSet(uploadedTranSql, false, new Object[]{inputDoc.getAttribute("/Transaction/transaction_oid")});
			if (uploadedTranRS != null)
			{
				uploadedInd = uploadedTranRS.getAttribute("/ResultSetRecord/UPLOADED_IND");
			}
			else
			{
				// if for some reason the query failed to get uploaded_ind from the DB, assume this is a manual payment
				return false;
			}
		}

		return TradePortalConstants.INDICATOR_YES.equals(uploadedInd) ||
			(isFixedWidthInitiallyConsideredFileUpload && !TradePortalConstants.INDICATOR_NO.equals(uploadedInd));
	}

	/**
	 * This method validates payment's Check Details entered on the screen
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param isForVerify boolean - If true, gets code values from bean rather than inputDoc
	 * @param isInitialFileUploadVerify boolean - true if this is the initial verify of a payment file upload (i.e., not a subsequent manual verify)
	 * @return void -- nothing is return; in case of error, an error condition is raised via ErrorManager
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private void validateCheckDetails(DocumentHandler inputDoc, boolean isForVerify, boolean isInitialFileUploadVerify)
	throws RemoteException, AmsException {
		if (isPaymentFileUpload(inputDoc, true) && !isInitialFileUploadVerify) {
			return;
		}

		String enteredPaymentMethod;
		String payableLocation;
		String printLocation;
		String deliveryMethod;

		if (isForVerify) {
			enteredPaymentMethod = this.getAttribute("payment_method_type");
			payableLocation = this.getAttribute("payable_location");
			printLocation = this.getAttribute("print_location");
			deliveryMethod = this.getAttribute("delivery_method");
		} else {
			enteredPaymentMethod = inputDoc.getAttribute("/DomesticPayment/payment_method_type");
			payableLocation = inputDoc.getAttribute("/DomesticPayment/payable_location");
			printLocation = inputDoc.getAttribute("/DomesticPayment/print_location");
			deliveryMethod = inputDoc.getAttribute("/DomesticPayment/delivery_method");
		}

		if (TradePortalConstants.PAYMENT_METHOD_BCHK.equals(enteredPaymentMethod) ||
				TradePortalConstants.PAYMENT_METHOD_CCHK.equals(enteredPaymentMethod)) {
			// validate the Payable Location, Print Location, and Delivery Method
			ResourceManager resourceMgr = getResourceManager();
			validatePayeeField(deliveryMethod, false, // "Delivery Method");
					resourceMgr.getText("DomesticPaymentBeanAlias.delivery_method",
							TradePortalConstants.TEXT_BUNDLE));
		}
	}
	//BSL IR# PBUL042033374 04/28/11 End

	//BSL IR#BAUL040763459 04/06/11 Begin - consolidate validateReportingCode and verifyReportingCode into common methods
	/**
	 * This method validates payment's Reporting Code(s) entered on the screen
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param paymentMethod String - payment method
	 * @param isForVerify boolean - If true, gets code values from bean rather than inputDoc
	 * @return void -- nothing is return; in case of error, an error condition is raised via ErrorManager
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private void validateReportingCodes(DocumentHandler inputDoc, String paymentMethod, boolean isForVerify)
	throws RemoteException, AmsException
	{
                // W Zhu 2/11/2014 T36000024856 comment out code.  Do validation for payment file upload.
		//if (isPaymentFileUpload(inputDoc, false)) //BSL IR# PBUL042033374 04/28/11 - Created separate method for this logic
		//{
		//	return;
		//}

		if(isPaymentFileUpload(inputDoc, false)){
			return;
		}
		//obtain data needed for validation from input doc and database records as needed
		String debAccountOid = inputDoc.getAttribute("/Terms/debit_account_oid");
		if (StringFunction.isBlank(debAccountOid))
		{
			return;
		}

		Account debitAccount = (Account) this.createServerEJB("Account");
		debitAccount.getData(Long.parseLong(debAccountOid));
		String opBankOrgOid = debitAccount.getAttribute("op_bank_org_oid");
		if (StringFunction.isBlank(opBankOrgOid))
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.REP_CODE_OP_ORG_ID_MISSING);
			return;
		}

		OperationalBankOrganization opBankOrg = (OperationalBankOrganization) this.createServerEJB("OperationalBankOrganization",
													  Long.parseLong(opBankOrgOid));
		String bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");
		if (StringFunction.isBlank(bankGroupOid))
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.REP_CODE_BANK_GRP_MISSING);
			return;
		}

		//Rel9.4 CR-1001 Adding additional validations START
		int currentReportCodeIndex = 0;
		String reportingCodes[] = new String[2];
		String repAttr = null;
		while (currentReportCodeIndex < TradePortalConstants.MAX_REP_CODE_IDX)
		{
			if (isForVerify) {
				repAttr = "reporting_code_" + (currentReportCodeIndex+1);
				reportingCodes[currentReportCodeIndex] = this.getAttribute(repAttr);
			}
			else {
		        repAttr = "/DomesticPayment/reporting_code_" + (currentReportCodeIndex+1);
				reportingCodes[currentReportCodeIndex] = inputDoc.getAttribute(repAttr);
			}
			currentReportCodeIndex++;
		}
		
		String reportingCode1ReqdForACH=null, reportingCode1ReqdForRTGS=null, reportingCode2ReqdForACH=null, reportingCode2ReqdForRTGS = null;
		String sqlQuery = "select reporting_code1_reqd_for_ach, reporting_code1_reqd_for_rtgs, reporting_code2_reqd_for_ach, reporting_code2_reqd_for_rtgs"+
					" from bank_organization_group where organization_oid=?";
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{bankGroupOid}); 
       	if(resultDoc!=null){
			reportingCode1ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_ACH");
			reportingCode1ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_RTGS");
			reportingCode2ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_ACH");
			reportingCode2ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_RTGS");
       	}
       	
		if(TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)){
			if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForACH) && StringFunction.isBlank(reportingCodes[0])){
				errMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
			}
			if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForACH) && StringFunction.isBlank(reportingCodes[1])){
				errMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
			}
		}
		else if(TradePortalConstants.PAYMENT_METHOD_RTGS.equals(paymentMethod)){
			if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForRTGS) && StringFunction.isBlank(reportingCodes[0])){
				errMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
			}
			if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForRTGS) && StringFunction.isBlank(reportingCodes[1])){
				errMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
			}
		}
		

		//Rel9.4 CR-1001 End
		currentReportCodeIndex = 0;
		while (currentReportCodeIndex < TradePortalConstants.MAX_REP_CODE_IDX)
		{
			validateReportingCode(inputDoc, bankGroupOid, isForVerify, Integer.toString(++currentReportCodeIndex));
		}
	}


	/**
	 * This method validates payment's Reporting Code(s) entered on the screen
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param bankGroupOid String - Bank Group OID to check for codes
	 * @param isForVerify boolean - If true, gets code values from bean rather than inputDoc
	 * @param repCodeIndex String - Reporting Code Index.
	 * @return void -- nothing is return; in case of error, an error condition is raised via ErrorManager
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private void validateReportingCode(DocumentHandler inputDoc, String bankGroupOid, boolean isForVerify, String repCodeIndex)
	throws RemoteException, AmsException
	{
		boolean repSelected = false;
		String repAttr;
		String reportingCode;

		//obtain data needed for validation from database records, input doc, and/or bean as needed
		if (isForVerify) {
			repAttr = "reporting_code_" + repCodeIndex;
			reportingCode = this.getAttribute(repAttr);
		}
		else {
	        repAttr = "/DomesticPayment/reporting_code_" + repCodeIndex;
			reportingCode = inputDoc.getAttribute(repAttr);
		}

		//build sql: if reporting code was entered for this payment, validate it against reporting codes table for given bank group
		// if it was not entered, verify it is not required (no codes stored for this bank group in the database)
		String whereSqlStr = " p_bank_group_oid = ?";
		List<Object> temp = new ArrayList<Object>();
		Object intOb = new Object();
		intOb = (bankGroupOid !=null)?Long.parseLong(bankGroupOid):bankGroupOid;
		temp.add(intOb);
		if (StringFunction.isNotBlank(reportingCode))
		{
			repSelected = true;
			whereSqlStr += " and code = ?";
			temp.add(reportingCode);
		}

		//obtain count.
		int reportCount = DatabaseQueryBean.getCount("code", "PAYMENT_REPORTING_CODE_" + repCodeIndex, whereSqlStr, false, temp);

		//if reporting code was selected on payment but was not found in the database, issue an error.
		if (repSelected && reportCount < 1)
		{
			if("1".equals(repCodeIndex)){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE1 , reportingCode);
			}else if("2".equals(repCodeIndex)){
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE2 , reportingCode);
			}
			return;
		}
		
	}
	

	/**
	 * Performs any special "validate" processing that is specific to the
	 * descendant of BusinessObject.
	 *
	 * @see #validate()
	 */

	protected void userValidate() throws AmsException, RemoteException
	{

	}

	/**
	 * Performs any special "issueOptimisticLockError" processing that is
	 * specific to the descendant of BusinessObject. (Note: this method was
	 * originally coded for the purpose of specifying exactly which fx rate
	 * object was updated (by someone else) in the case of multiple fx rates
	 * being updated at the same time. The default error (in
	 * BusinessObjectBean) uses the generic word 'item' which works fine for
	 * objects being updated one at a time.)
	 *
	 * This method verifies that rate is not zero.
	 * @throws RemoveException
	 *
	 * @see #validate()
	 */
	/*protected void issueOptimisticLockError() throws AmsException, RemoteException
   {
      this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.DOM_PMT_OPT_LOCK_EXCEPTION,
                                        getAttribute("currency_code"));
   }*/

	//CR 913 start
	//CR 913 start
	public String setAllAttributes(String[] attributePaths, String[] attributeValues,String corpOrgName)
			throws RemoteException, AmsException{
		String sentEmail = "N";
		for (int i = 0; i < attributePaths.length; i++) {
			if (attributePaths[i].equals("invoice_details") ) {
				if(StringFunction.isNotBlank(attributeValues[i])){
				InvoiceDetails newInvoiceDetails = (InvoiceDetails) this.createServerEJB("InvoiceDetails");
				newInvoiceDetails.newObject();
				newInvoiceDetails.setAttribute("domestic_payment_oid",getAttribute("domestic_payment_oid"));
				String invDetails = attributeValues[i].replaceAll("::", "\n");
				invDetails = invDetails.replaceAll(";", ", ");
				newInvoiceDetails.setAttribute("payee_invoice_details",invDetails);
				newInvoiceDetails.save();
				try {
					newInvoiceDetails.remove();
				} catch (RemoveException e) {
					LOG.error("DomesticPaymentBean:: Remote Exception caught in setAllAttributes(): ",e);
				}
				}
			}
			//sequence_number is comprised of the instrument id + '-' +  sequence number + '-' pay bene seq num
			else if (attributePaths[i].equals("sequence_number") && StringFunction.isNotBlank(attributeValues[i])
					&& attributeValues[i].contains("-")) {
				//String sequenceNumber = attributeValues[i].substring(attributeValues[i].indexOf('-')+1);
				String[] valArr =attributeValues[i].split("-");
				setAttribute(attributePaths[i],  valArr[1]); //sequence_number 
				if(valArr.length==3){
					setAttribute("payable_payment_seq_num", valArr[2]);//payable_payment_seq_num
				}
				
			}
			else if(attributePaths[i].equals("value_date") && StringFunction.isNotBlank(attributeValues[i])){
				//value date in MMDDYYYY format
			//	setAttribute("value_date",attributeValues[i].substring(4,6)+ "/" + attributeValues[i].substring(6,8)+ "/" +attributeValues[i].substring(0,4));
				setAttribute("value_date",attributeValues[i].substring(0,2)+ "/" + attributeValues[i].substring(2,4)+ "/" +attributeValues[i].substring(4,8));
			}
			//do not set attribute for buyer_name and if buyer name doesnt match corporate org name then dont send email
			else if(attributePaths[i].equals("buyer_name") ){
				/*if(StringFunction.isNotBlank(attributeValues[i])&& !attributeValues[i].equalsIgnoreCase(corpOrgName)){
					sentEmail = "Y";
				}*/
			}
			else {
				setAttribute(attributePaths[i], attributeValues[i]);
			}
		}
		return sentEmail;
	}
	//CR 913 end

	//CR 913 end
}
