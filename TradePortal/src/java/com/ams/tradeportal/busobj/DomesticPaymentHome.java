
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface DomesticPaymentHome extends EJBHome
{
   public DomesticPayment create()
      throws RemoteException, CreateException, AmsException;

   public DomesticPayment create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
