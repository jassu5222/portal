
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will be one of these records per account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface EODDailyDataHome extends EJBHome
{
   public EODDailyData create()
      throws RemoteException, CreateException, AmsException;

   public EODDailyData create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
