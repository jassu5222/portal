

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will potentially be multiple of these transactions recordsper
 * account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface EODDailyTransaction extends TradePortalBusinessObject
{   
}
