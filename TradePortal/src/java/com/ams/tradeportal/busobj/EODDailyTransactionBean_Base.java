
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will potentially be multiple of these transactions recordsper
 * account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyTransactionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(EODDailyTransactionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* eod_daily_trans_oid - Unique Identifier */
      attributeMgr.registerAttribute("eod_daily_trans_oid", "eod_daily_trans_oid", "ObjectIDAttribute");
      
      /* transaction_date - The date of the transaction in YYYYMMDD format. */
      attributeMgr.registerAttribute("transaction_date", "transaction_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("transaction_date");
      
      /* transaction_type - Bank-specific transaction type. Not validated against any reference/code
         tables. */
      attributeMgr.registerAttribute("transaction_type", "transaction_type");
      attributeMgr.requiredAttribute("transaction_type");
      
      /* transaction_reference - Indicates the source/type of account. */
      attributeMgr.registerAttribute("transaction_reference", "transaction_reference");
      
      /* transaction_amount - Transaction amount (signed amount). There will be an explicit decimal point.
         First character is always either a "+", which represents a Credit or a "-"
         which represents a Debit. Right justified. Zero filled */
      attributeMgr.registerAttribute("transaction_amount", "transaction_amount", "TradePortalSignedDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("transaction_amount");
      
      /* transaction_narrative - Transaction narrative. */
      attributeMgr.registerAttribute("transaction_narrative", "transaction_narrative");
      
      /* effective_date - Value date of the transaction in YYYYMMDD format. */
      attributeMgr.registerAttribute("effective_date", "effective_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("effective_date");
      
      /* trace_id - Trace Details. */
      attributeMgr.registerAttribute("trace_id", "trace_id");
      
      /* transaction_code - Bank-specific transaction code. Not validated against any reference/code
         tables. */
      attributeMgr.registerAttribute("transaction_code", "transaction_code");
      attributeMgr.requiredAttribute("transaction_code");
      
      /* auxiliary_dom - Auxiliary Domestic - bank-specific structured information regarding the
         transaction. Not validate against the bank's AuxDom/Trancode, */
      attributeMgr.registerAttribute("auxiliary_dom", "auxiliary_dom");
      
      /* ex_auxiliary_dom - ExAuxiliary Domestic - additional bank-specific structured information regarding
         the transaction. Not validate against the bank's AuxDom/Trancode, */
      attributeMgr.registerAttribute("ex_auxiliary_dom", "ex_auxiliary_dom");
      
      /* bai_code - BAI code. Not validated against any reference/code tables. */
      attributeMgr.registerAttribute("bai_code", "bai_code");
      attributeMgr.requiredAttribute("bai_code");
      
      /* remitter_name - Name of the remitter. */
      attributeMgr.registerAttribute("remitter_name", "remitter_name");
      
      /* lodgment_reference - Lodgment Reference. */
      attributeMgr.registerAttribute("lodgment_reference", "lodgment_reference");
      
      /* short_description - Short description */
      attributeMgr.registerAttribute("short_description", "short_description");
      
      /* number_collection_items - Number of collection items */
      attributeMgr.registerAttribute("number_collection_items", "number_collection_items", "NumberAttribute");
      
      /* sequence_number - A sequence number to be used in the portal to allow the transactions for
         one transaction date to be sorted and presented in the correct order on
         the report. */
      attributeMgr.registerAttribute("sequence_number", "sequence_number", "NumberAttribute");
      attributeMgr.requiredAttribute("sequence_number");
      
      /* date_time_received - Date and time on which EOD Data was received. */
      attributeMgr.registerAttribute("date_time_received", "date_time_received", "DateTimeAttribute");
      
        /* Pointer to the parent EODDailyData */
      attributeMgr.registerAttribute("eod_daily_data_oid", "p_eod_daily_data_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
