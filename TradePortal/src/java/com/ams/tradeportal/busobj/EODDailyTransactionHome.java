
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * An Incoming End of Day Transaction Request message will be sent to the portal
 * by the bank.  There will potentially be multiple of these transactions recordsper
 * account each day.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface EODDailyTransactionHome extends EJBHome
{
   public EODDailyTransaction create()
      throws RemoteException, CreateException, AmsException;

   public EODDailyTransaction create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
