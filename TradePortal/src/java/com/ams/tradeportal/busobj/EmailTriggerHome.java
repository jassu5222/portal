
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Placing a row into this table will cause an e-mail to be sent.  The contents
 * of the e-mail are based on the email_data column.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface EmailTriggerHome extends EJBHome
{
   public EmailTrigger create()
      throws RemoteException, CreateException, AmsException;

   public EmailTrigger create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
