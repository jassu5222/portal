package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kyriba
 */
public class ExternalBankBean extends ExternalBankBean_Base{
private static final Logger LOG = LoggerFactory.getLogger(ExternalBankBean.class);

	public String getListCriteria(String type) {
		
		if (type.equals("activeExBankOrgAssoc")){
			return "A_OP_ORGANIZATION_OID = {0}";
		}

		return "";

	}
}
