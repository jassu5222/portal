
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * A foreign exchange rate between one currency and the corporate customer's
 * base currency.  This is populated by the corporate customer users.
 * 
 * FX Rates are used for threshold checking and for grouping by amount during
 * Auto LC Creation.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class FXRateBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(FXRateBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* fx_rate_oid - Unique identifer */
      attributeMgr.registerAttribute("fx_rate_oid", "fx_rate_oid", "ObjectIDAttribute");
      
      /* currency_code - The currency code for which the FX Rate is being established. */
      attributeMgr.registerReferenceAttribute("currency_code", "currency_code", "CURRENCY_CODE");
      attributeMgr.requiredAttribute("currency_code");
      attributeMgr.registerAlias("currency_code", getResourceManager().getText("FXRateBeanAlias.currency_code", TradePortalConstants.TEXT_BUNDLE));
      
      /* rate - The foreign exchange rate */
      attributeMgr.registerAttribute("rate", "rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.requiredAttribute("rate");
      attributeMgr.registerAlias("rate", getResourceManager().getText("FXRateBeanAlias.rate", TradePortalConstants.TEXT_BUNDLE));
      
      /* multiply_indicator - Indicates whether someone should multiple or divide by the rate in order
         to convert between currencies */
      attributeMgr.registerReferenceAttribute("multiply_indicator", "multiply_indicator", "MULTIPLY_DIVIDE_IND");
      attributeMgr.requiredAttribute("multiply_indicator");
      attributeMgr.registerAlias("multiply_indicator", getResourceManager().getText("FXRateBeanAlias.multiply_indicator", TradePortalConstants.TEXT_BUNDLE));
      
      /* last_updated_date - Timestamp (in GMT) of when this FX rate was last updated. */
      attributeMgr.registerAttribute("last_updated_date", "last_updated_date", "DateTimeAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* buy_rate - Currency exchange rate specific to purchases. */
      attributeMgr.registerAttribute("buy_rate", "buy_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("buy_rate", getResourceManager().getText("FXRateBeanAlias.buy_rate", TradePortalConstants.TEXT_BUNDLE));
      
      /* sell_rate - Currency exchange rate specific to sales. */
      attributeMgr.registerAttribute("sell_rate", "sell_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("sell_rate", getResourceManager().getText("FXRateBeanAlias.sell_rate", TradePortalConstants.TEXT_BUNDLE));
      
      /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer) */
      attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");
      
      /* ownership_type - If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
      attributeMgr.registerReferenceAttribute("ownership_type", "ownership_type", "OWNERSHIP_TYPE");
      
      /* fx_rate_group - The FX Rate Group (as defined in TPS) for the account/base currency.  It
         is a simple TextAttribute and is not validated against REFDATA. */
      attributeMgr.registerAttribute("fx_rate_group", "fx_rate_group");
      attributeMgr.registerAlias("fx_rate_group", getResourceManager().getText("FXRateBeanAlias.fx_rate_group", TradePortalConstants.TEXT_BUNDLE));

      // DK CR-640 Rel7.1 BEGINS
      /* max_spot_rate_amount - Maximum settlement amount in base currency that can be converted using a
         Spot rate */
      attributeMgr.registerAttribute("max_spot_rate_amount", "max_spot_rate_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      /* max_deal_amount - Maximum Deal Amount allowed for the currency */
      attributeMgr.registerAttribute("max_deal_amount", "max_deal_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      // DK CR-640 Rel7.1 ENDS

      /* base_currency_code - The base currency of the FX Rate. */
      attributeMgr.registerReferenceAttribute("base_currency_code", "base_currency_code", "CURRENCY_CODE");
      attributeMgr.registerAlias("base_currency_code", getResourceManager().getText("FXRateBeanAlias.base_currency_code", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute"); //AAlubala Rel 7.0.0.0 CR610 -03/2011
      //AAlubala IR#IAUL050662274 save the org's base currency - 05/05/2011
      attributeMgr.registerAttribute("org_base_currency", "BASE_CURRENCY_CODE");
      attributeMgr.registerAlias("org_base_currency", "org_base_currency");      
   
   }
   
 
   
 
 
   
}
