package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


 /**
 * This Business object stores data from Generic Incoming Data Message sent
 * by bank. Bank uses Generic Incoming Data Message to send over data in no
 * specific format.  
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
    public interface GenericIncomingData extends BusinessObject
    {
		//public void deleteGenericIncDataMessage(java.lang.String genericIncDataMessageOid) throws RemoteException, AmsException;
		
    }
