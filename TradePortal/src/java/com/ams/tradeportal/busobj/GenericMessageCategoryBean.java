package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import java.util.regex.*;
import com.ams.tradeportal.common.*;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class GenericMessageCategoryBean extends GenericMessageCategoryBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(GenericMessageCategoryBean.class);
	// IR DOUL081056897 Check name fields contains only alpha-numeric characters
	protected void userValidate() throws AmsException, RemoteException {
		// Shilpa R REL 7.1 IR-DOUL120565963 start
		Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9 ]+");// Just accept Alphanumeric and space
		String description  = getAttribute("description");
		if (null != description) {
			// Shilpa R REL 7.1 IR-LUUL120240799 start
			String where = " and p_owner_org_oid = '" + getAttribute("owner_org_oid") + "'";

			// now we need to check that name is unique
			if (!isUnique("description", description, where))
				{
				   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					                              TradePortalConstants.ALREADY_EXISTS, description, attributeMgr.getAlias("description"));
				}
			// Shilpa R REL 7.1 IR-LUUL120240799 end
			Matcher m = ALPHANUMERIC.matcher(getAttribute("description"));
			validateAll(m,"GenericMessageCategoryDetail.MsgCatDescription");
		}
		if (null != getAttribute("name")) {
			Matcher m = ALPHANUMERIC.matcher(getAttribute("name"));
			validateAll(m,"GenericMessageCategoryDetail.MsgCatName");
		}
		if (null != getAttribute("short_description")) {
			Matcher m = ALPHANUMERIC.matcher(getAttribute("short_description"));
			validateAll(m,"GenericMessageCategoryDetail.MsgCatShortDescription");
		}
	}

	private void validateAll(Matcher m,String errorField) throws RemoteException, AmsException{
		if (!m.matches()) {
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.MUST_BE_ALPHA_NUMERIC,
					getResourceManager().getText(
							errorField,
							TradePortalConstants.TEXT_BUNDLE));
		}
	}
	// Shilpa R REL 7.1 IR-DOUL120565963 end
}
