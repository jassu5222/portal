 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GlobalOrganizationBean extends GlobalOrganizationBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(GlobalOrganizationBean.class);

   /**
    * Set the inital default values when a new object is created 
    * 
    */
   protected void userNewObject() throws RemoteException, AmsException
    {
	super.userNewObject();

        // Always default it so that users of this organization when logging in with
        // certificates must present a digital signature
        this.setAttribute("verify_logon_digital_sig", TradePortalConstants.INDICATOR_YES);
    }

}
