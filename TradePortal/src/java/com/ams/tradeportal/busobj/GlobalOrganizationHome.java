
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents the organization that provides trade processing services (or
 * in some cases, just the software) to the client banks.  In the hierarchy
 * of organizations, this is the highest.   Client banks are owned by this
 * organization.
 * 
 * Under the current design, there will only ever be one of these objects in
 * the database.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GlobalOrganizationHome extends EJBHome
{
   public GlobalOrganization create()
      throws RemoteException, CreateException, AmsException;

   public GlobalOrganization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
