
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GridColumnCustomizationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(GridColumnCustomizationBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* grid_cust_column_oid - Object identifier of this record */
      attributeMgr.registerAttribute("grid_column_cust_oid", "grid_column_cust_oid", "ObjectIDAttribute");
      
      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("column_id", "column_id");
      attributeMgr.requiredAttribute("column_id");

      /* Pointer to the parent grid customization */
      attributeMgr.registerAttribute("grid_cust_oid", "p_grid_cust_oid", "ParentIDAttribute");

      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("display_order", "display_order", "NumberAttribute");
      attributeMgr.requiredAttribute("display_order");
      
      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("width", "width");
      attributeMgr.requiredAttribute("width");
   }
   
}
