
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GridCustomizationHome extends EJBHome
{
   public GridCustomization create()
      throws RemoteException, CreateException, AmsException;

   public GridCustomization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
