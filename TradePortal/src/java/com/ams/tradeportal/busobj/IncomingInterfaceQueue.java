package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * This business object stores data that has been received from the middleware,
 * but has not yet been processed by the InboundAgent.   
 * The MQAgent picks data up off of the queue and places it into this table.
 * The InboundAgent picks data up off of this table and processes it.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface IncomingInterfaceQueue extends TradePortalBusinessObject
{   
	public void moveToHistory() throws RemoteException, AmsException;

}
