package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * After processing, data stored as IncomingInterfaceQueue business objects
 * is moved to be stored as IncomingQueueHistory.  This is done to prevenet
 * the IncomingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface IncomingQueueHistory extends TradePortalBusinessObject
{   
}
