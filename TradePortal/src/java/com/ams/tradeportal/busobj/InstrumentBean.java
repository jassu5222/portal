package com.ams.tradeportal.busobj;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.PanelAuthProcessor;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.OptimisticLockException;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InstrumentBean extends InstrumentBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentBean.class);
	// DK IR-RBUM062632733 Rel8.1.1 10/17/2012 Begins
	 private static final String STRUCTURED_PO_MASS_UPDATE_SQL =
         "update purchase_order " +
                 " set  " +
                 "     a_transaction_oid = {0}, " +
                 "     a_instrument_oid = {1} " ;
	// DK IR-RBUM062632733 Rel8.1.1 10/17/2012 Ends

	 private String SELECT_SQL = ""; // DK IR T36000015588 Rel 8.2 04/18/2013
	 private String SELECT_TERMS_PARTY_SQL = ""; // DK IR T36000015588 Rel 8.2 05/23/2013


  public void updateParent(java.util.Hashtable attributes) throws RemoteException, AmsException
   {

  LOG.debug ("We have started the updateParent method of the instrument");


           //We need to confirm that we have not been passed an empty hashtable
           //(i.e. no attribute names or attribute values for the
           //transaction we need to update
            if (attributes == null)
            throw new AmsException("Null attribute hash returned");

            //busobj.setAttributes takes two arrays as arguments
            //so we need to take the key, value pairs and put
            //them into two arrays
            String name[] = new String[attributes.size()] ;
            String value[] = new String[attributes.size()] ;


            Enumeration attributeKeys;
            attributeKeys = attributes.keys();
            int i = 0;
            while (attributeKeys.hasMoreElements()) {
                name[i] = (String) attributeKeys.nextElement();
                value[i] = (String) attributes.get(name[i]);
                i ++;
            }

            LOG.debug ("name.length -> {}" , name.length);
            

            //Create an instance of the related instrument and populate data fields from the database.
            Instrument instrumentRelated  = (Instrument)this.createServerEJB("Instrument", this.getAttributeLong("related_instrument_oid"));
           //The position data of an instrument is stored in the active transaction of that instrument
           //We need to get that active transaction of the related instrument and update the appropriate
           //position fields
            String activeTransactionOidRelated = instrumentRelated.getAttribute("active_transaction_oid");
				  if(activeTransactionOidRelated==null) activeTransactionOidRelated = "";
				  if(activeTransactionOidRelated.equals(""))
				  {
				    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Active Transaction to perform REVALUE or DEAL_CHANGE");
				  }
				  else
				  {
				   //In order for this to be part of the same transaction as the original instrument update, we have replaced the
				   //instrument.save() in the TPLUnPackagerMediator with a call to this function in cases where the position
				   //of the related instrument must be updated
				    Transaction activeTransactionRelated = (Transaction)instrumentRelated.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOidRelated));
				    activeTransactionRelated.setAttributes(name, value);
				   //In addition to setting the active transaction, we have a field in Instrument which is just a copy of the instrument
				   //amount in the transaction.  We need to set this as well on the related instrument
				   instrumentRelated.setAttribute("copy_of_instrument_amount", (String)    attributes.get("instrument_amount"));
				    instrumentRelated.save();
				    this.save();
				  }

   }


  /**
   *
   *
   * @param  transactionOid
   * @param  userOid
   * @param  securityRights
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */
   public void deleteTransaction(String transactionOid, String userOid, String securityRights) throws RemoteException, AmsException
   {

  /**
	* This method updates the status of the transaction to Deleted
	* It verifies that the transaction is in a state that can be deleted
	* and the user has the rights to delete the transaction.  For successful
	* change to Deleted transaction status, the transaction is reassigned to
	* the user.
	*
	* @param transactionOid java.lang.String - the transaction oid to delete
	* @param userOid  java.lang.String - the user oid used to delete transaction
	* @param securityRights java.lang.String - the user's security rights
	*/

	Transaction transaction = (Transaction)
	    getComponentHandle("TransactionList", Long.parseLong(transactionOid));

	String transactionType = transaction.getAttribute("transaction_type_code");
   	String OrigtransactionStatus = transaction.getAttribute("transaction_status");
	LOG.debug("InstrumentBean deleteTransaction: Deleting transaction: {}" , transactionOid);

		// if the transaction is in a deletable state, and the user has the rights
	// to delete, then update the status
		// to delete and assign the transaction to the user

	if (transaction.canDeleteState())
	{
	    if (SecurityAccess.canDeleteInstrument(securityRights,
		getAttribute("instrument_type_code"), transactionType))
	    {
	    	/*IR 31526 start - we throw a message on the screen when the instrument is deleted informing them that
	    	 *  there are invoices attached and they need to unlink the invoices before deleting the instrument 
	    	 */
	    	if(InstrumentType.LOAN_RQST.equals(getAttribute("instrument_type_code")) 
	    			|| InstrumentType.APPROVAL_TO_PAY.equals(getAttribute("instrument_type_code"))){
	    		
	    		StringBuilder sql = new StringBuilder();
				sql.append(" a_instrument_oid = ? ");
				int count = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", sql.toString(), false, new Object[]{getAttribute("instrument_oid")});
				if(count >0){
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DELETE_INVOICES_FROM_INST);
					return;
				}
	    	}
	    	
	    	//IR 31526 end
	    	
	    	transaction.setAttribute("transaction_status", 	TransactionStatus.DELETED);
	    	 //	IR#MJUL102039638 NFirdose BEGIN
	    	if ((TransactionStatus.FVD_AUTHORIZED).equals(OrigtransactionStatus))
			{
			     try
			     {
		 			long queue_oidLong = 0;
		 			String sqlStatement = "select QUEUE_OID from OUTGOING_QUEUE where A_INSTRUMENT_OID=? and A_TRANSACTION_OID=? and STATUS =? ";
		 			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{this.getAttribute("instrument_oid"),transactionOid,TradePortalConstants.OUTGOING_STATUS_STARTED  });

		 			if (resultXML != null)
		 				queue_oidLong = resultXML.getAttributeLong("/ResultSetRecord(0)/QUEUE_OID");

					LOG.debug("Queue OID to delete: {}" , queue_oidLong);

		            if(queue_oidLong != 0)
		            {
						LOG.debug("Future Value Payment Transaction being deleted with queue oid: {}" , queue_oidLong);
						OutgoingInterfaceQueue outgoing_queue = (OutgoingInterfaceQueue) createServerEJB("OutgoingInterfaceQueue", queue_oidLong);
	                	outgoing_queue.delete();
	            	}
		         } catch (RemoteException e) {
			         throw new AmsException("Remote exception found while deleting Future Value Payment transaction.Error: " , e);
		         }
			}

       //	IR#MJUL102039638 NFirdose END

   	        transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());

		    transaction.setAttribute("last_entry_user_oid", userOid);

            // If the transaction being deleted is the instrument's original transaction,
            // set the instrument's status to 'Deleted'
            if (transactionOid.equals(this.getAttribute("original_transaction_oid")))
            {
               this.setAttribute("instrument_status", TradePortalConstants.INSTR_STATUS_DELETED);
            }

            // Before saving this "deleted" transaction, we need to verify
            // that if that transaction is associated to a mail message, then we need
            // to remove that association from the message.  There is at most a 1 to 1
            // relationship between The MailMessage and the transaction.
            try{
                        MailMessage associatedMessage;
	                    associatedMessage = (MailMessage)createServerEJB( "MailMessage" );
                        associatedMessage.find("response_transaction_oid", transaction.getAttribute("transaction_oid") );

			            associatedMessage.setAttribute("response_transaction_oid", "");
			            associatedMessage.save();

			}catch( com.amsinc.ecsg.frame.ObjectNotFoundException e ) {
			}catch( AmsException e ) {
			            LOG.error("Error while removing associated message after deletion of a transaction: ",e);
			}

		    this.save();

	    	    if (getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY)
		    {
	            	// issue message that the transaction has been successfully routed
		    	this.getErrorManager().issueError(getClass ().getName (),
		            TradePortalConstants.TRANSACTION_PROCESSED, new String[]
		            {ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE",
		            		getAttribute("instrument_type_code"),
							csdb.getLocaleName()) + " - " + getAttribute("complete_instrument_id"),
		            ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		            transactionType, csdb.getLocaleName()),
		            getResourceManager().getText("TransactionAction.Deleted",
	    	            TradePortalConstants.TEXT_BUNDLE)});
		    }

		  
	    }
	    else
	    {
		// issue error that user does not have delete transaction authority
		// for the given instrument type
		this.getErrorManager().issueError(getClass ().getName (),
		    TradePortalConstants.NO_ACTION_TRANSACTION_AUTH, new String[]
		    {getAttribute("complete_instrument_id"),
		    ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    transactionType, csdb.getLocaleName()),
		    getResourceManager().getText("TransactionAction.Delete",
	    	    TradePortalConstants.TEXT_BUNDLE)});

		
	    }
	}
	else
	{
	    // issue error that transaction is not in a deletable state
	    this.getErrorManager().issueError(getClass ().getName (),
		TradePortalConstants.TRANSACTION_CANNOT_PROCESS , new String[]
		{getAttribute("complete_instrument_id"),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    transactionType, csdb.getLocaleName()),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_STATUS",
		    transaction.getAttribute("transaction_status"), csdb.getLocaleName()),
		    getResourceManager().getText("TransactionAction.Deleted",
	    	    TradePortalConstants.TEXT_BUNDLE)});
	   
	}
   }

  /**
   *
   *
   * @param  transactionOid
   * @param  userOid
   * @param  securityRights
   * @param  userOidRoute
   * @return boolean
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */
   public boolean routeTransaction(String transactionOid, String userOid, String securityRights, String userOidRoute) throws RemoteException, AmsException
   {
  /**
	* This method routes the transaction to userOid.
	* It verifies that the transaction is in a state that can be routed
	* and the user has the rights to route the transaction.  If the transaction
	* is successfully routed to the new user, this method returns a true.
	*
	* @param transactionOid java.lang.String - the transaction oid to delete
	* @param userOid  java.lang.String - the user oid used to delete transaction
	* @param securityRights java.lang.String - the user's security rights
	* @return boolean - true when route is successful, false if there are errors
	*/

	Transaction transaction = (Transaction)
	    getComponentHandle("TransactionList", Long.parseLong(transactionOid));
	String transactionType = transaction.getAttribute("transaction_type_code");
	LOG.debug("InstrumentBean routeTransaction: Routing transaction: {} to {}" ,transactionOid,userOidRoute);

		// if the transaction is in a routable state, and the user has the rights
	// to route, then route the transaction to the userOidRoute or the
	// the default user of orgOidRoute

	if (transaction.canRouteState())
	{
	    if (SecurityAccess.canRouteInstrument(securityRights,
		getAttribute("instrument_type_code"),transactionType))
	    {
		transaction.setAttribute("assigned_to_user_oid", userOidRoute);
		this.save();

	    	if (getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY)
		{

	            // issue message that the transaction has been successfully routed
		    this.getErrorManager().issueError(getClass ().getName (),
		    	TradePortalConstants.TRANSACTION_PROCESSED,
		    	ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE",
	            		getAttribute("instrument_type_code"),
						csdb.getLocaleName()) + " - " + getAttribute("complete_instrument_id"),
		    	ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    	transactionType, csdb.getLocaleName()),
		    	getResourceManager().getText("TransactionAction.Routed",
	    	    	TradePortalConstants.TEXT_BUNDLE));

		    return true;
		}
		else
		    return false;
	    }
	    else
	    {
		// issue error that user does not have route transaction authority
		// for the given instrument type
		this.getErrorManager().issueError(getClass ().getName (),
		    TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
		    getAttribute("complete_instrument_id"),
		    ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    transactionType, csdb.getLocaleName()),
		    getResourceManager().getText("TransactionAction.Route",
	    	    TradePortalConstants.TEXT_BUNDLE));
		return false;
	    }
	}
	else
	{
	    // issue error that transaction is not in a routeable state
	    this.getErrorManager().issueError(getClass ().getName (),
		TradePortalConstants.TRANSACTION_CANNOT_PROCESS ,
		getAttribute("complete_instrument_id"),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    transactionType, csdb.getLocaleName()),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_STATUS",
		    transaction.getAttribute("transaction_status"), csdb.getLocaleName()),
		    getResourceManager().getText("TransactionAction.Routed",
	    	    TradePortalConstants.TEXT_BUNDLE));
	    return false;
	}
   }

  /**
   *
   *
   * @param  transactionOid
   * @param  userOid
   * @param  securityRights
   * @return boolean
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
   */
   public boolean preAuthorizeTransaction(String transactionOid, String userOid, String securityRights) throws RemoteException, AmsException
   {
  /**
	* This method is used to prepare the transaction for the authorization
	* process in the background.
	* This method updates the status of the transaction to AuthorizePending
	* It verifies that the transaction is in a state that can be set to
	* authorize pending and prepares the data for use for authorizing the transaction.
	*
	* @param transactionOid java.lang.String - the transaction oid to delete
	* @param userOid  java.lang.String - the user oid used to delete transaction
	* @param securityRights java.lang.String - the user's security rights
	* @return boolean - true if preAuthorize is successful, false otherwise
   */


	Transaction transaction = (Transaction)
	    getComponentHandle("TransactionList", Long.parseLong(transactionOid));
	String transactionType = transaction.getAttribute("transaction_type_code");
	LOG.debug("InstrumentBean preAuthorizeTransaction: PreAuthorizing transaction: {}" , transactionOid);

	if (transaction.canAuthorizeState())
	{
		    transaction.setAttribute("transaction_status",
			TransactionStatus.AUTHORIZE_PENDING);

	    	transaction.setAttribute("transaction_status_date",
	      	  DateTimeUtility.getGMTDateTime());
	    this.save();


	    // prepare data to call AuthorizeTransactions Mediator
	    //
	    return (getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY);

	}
	else
	{
	    // issue error that transaction is not in an authorizable state
	    this.getErrorManager().issueError(getClass ().getName (),
		TradePortalConstants.TRANSACTION_CANNOT_PROCESS ,
		getAttribute("complete_instrument_id"),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
		    transactionType, csdb.getLocaleName()),
		ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_STATUS",
		    transaction.getAttribute("transaction_status"), csdb.getLocaleName()),
		    getResourceManager().getText("TransactionAction.Authorized",
	    	    TradePortalConstants.TEXT_BUNDLE));
	    return false;
	}

   }

 /**
	* This method updates the status of the transaction to Authorized,
	* or Failed through a series of validations.  It returns a boolean
	* indicating if the transaction status should be set to Failed.  The
	* Failed status cannot be set here because this is a transactional method
	* that rwill rollback when any error messages are issued.
	* If the transaction is Authorized, a new outgoing interface is created.
	*
	* This is a transactional method.  An assumption made is that the user has
	* the instrument locked currently.
	*
	* @param transactionOid java.lang.String - the transaction oid to delete
	* @param userOid  java.lang.String - the user oid used to delete transaction
	* @param securityRights java.lang.String - the user's security rights
	*  @param authorizeAtParentViewOrSubs java.lang.String - check the setting if the access mode is subsidiary
     * @param checkTransactionStatus
     * @param params
	* @return boolean - false if authorizing a transaction is unsuccessful and the
	* 			transaction status has to set to be set to FAILED,
	*                   true if the transaction successfully saves OR if the
	*                   the authorization fails BUT transaction should not be
	*                   set to FAILED.
    * @throws AmsException 
    * @throws RemoteException 
   */
   public boolean authorizeTransaction(String transactionOid, String userOid, String authorizeAtParentViewOrSubs, String securityRights, boolean checkTransactionStatus, HashMap params) 
   throws RemoteException, AmsException
   {


	boolean errorsFound = false;
	boolean checkThresholdLimit = false;
	boolean checkDailyLimit = false;
	boolean createOutgoingQueueAndIssueSuccess = false;
	boolean isFutureDatePayment = false;
	boolean isH2HStraightThroughInd = false; // CJR CR-593 4/06/11
	String userOrgOid;
	String baseCurrency;
	String dailyLimit = null;
	String thresholdAmount = null;
	BigDecimal amountInBase = null;
	String instrumentType = getAttribute("instrument_type_code");
	String rateType = instrumentType;				
	ThresholdGroup thresholdGroup;
	OutgoingInterfaceQueue oiQueue;
	CorporateOrganization corpOrg;
	CorporateOrganization accountsCorpOrg = null;	
	Terms terms = null;								
	String authorizeText;
	String authorizedText;
	Account debitAccount = null;
	Account creditAccount = null;
	BigDecimal debitAmt = null;

	//Ravindra - Rel7100 - 20th Jul 2011 - IR# VIUK112441836 - Begin
	//Transaction cannot be created/modified because the instrument has a status of Deactivated.
	if (TradePortalConstants.INSTR_STATUS_DEACTIVATED.equals(getAttribute("instrument_status")))
		{
		   	//ERROR
            LOG.debug("NTM_INSTR_DEACTIVATED error");
            getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NTM_INSTR_DEACTIVATED);
            return false;
		}
	//Ravindra - Rel7100 - 20th Jul 2011 - IR# VIUK112441836 - End
	ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();

	authorizeText = getResourceManager().getText("TransactionAction.Authorize",
	    TradePortalConstants.TEXT_BUNDLE);

	authorizedText = getResourceManager().getText("TransactionAction.Authorized",
	    TradePortalConstants.TEXT_BUNDLE);

	User user = (User) createServerEJB("User", Long.parseLong(userOid));
	userOrgOid = user.getAttribute("owner_org_oid");

	//IAZ CM-451 12/01/08 and 12/24/08 and 02/02/09 Begin
    
    String userPanelAuthSelf = user.getAttribute("authorize_own_input_ind");
    boolean panelAuthEnabled = false;
    String custDailyLimit;
    boolean isCashMgmtInstr = isCashManagementTypeInstrument(instrumentType);
    //IAZ CM-451 12/01/08 and 12/24/08 and 02/02/09 End

   
	Transaction transaction = (Transaction)
	    getComponentHandle("TransactionList", Long.parseLong(transactionOid));
	String transactionType = transaction.getAttribute("transaction_type_code");
	LOG.debug("InstrumentBean authorizeTransaction: authorizing transaction: {}" , transactionOid);
	String convertedTran = transaction.getAttribute("converted_transaction_ind");
	LOG.debug("convertedTran: {}",convertedTran);
	// ***********************************************************************
	// check transaction state - if transaction is not authorizable,
	// return error without saving any transaction information
	// ***********************************************************************

	if (checkTransactionStatus && !transaction.canAuthorizeState())
	{
	    // issue error that transaction is not in an authorizable state
	    this.getErrorManager().issueError(getClass ().getName (),
		    				TradePortalConstants.TRANSACTION_CANNOT_PROCESS ,
							getAttribute("complete_instrument_id"),
							refDataMgr.getDescr("TRANSACTION_TYPE",
		   										 transactionType,
		   										 csdb.getLocaleName()),
							refDataMgr.getDescr("TRANSACTION_STATUS",
		   										 transaction.getAttribute("transaction_status"),
		   										 csdb.getLocaleName()),
							authorizedText);
	    // return true to NOT set the transaction status to failed
	    return true;
	}


	// ***********************************************************************
	// check to make sure that the user is authorized to authorize the
	// transaction - the user has to be a corporate user (not an admin
	// user) and have the rights to authorize a transaction for this
	// particular instrument type
	// ***********************************************************************
	LOG.debug("InstrumentBean authorizeTransaction: ownershiplevel: {}"
		, user.getAttribute("ownership_level"));

	if (!(user.getAttribute("ownership_level")).equals(TradePortalConstants.OWNER_CORPORATE))
	{
	    this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.ADMIN_USER_CANNOT_AUTHORIZE,
							user.getAttribute("first_name"),
							user.getAttribute("last_name"));

	    // return false to set the transaction status to failed
	    return false;
	}
	else if (TradePortalConstants.INDICATOR_NO.equals(convertedTran) && !SecurityAccess.canAuthorizeInstrument(securityRights,
		getAttribute("instrument_type_code"), transactionType))
	{
	    // issue error that user does not have authorize transaction authority
	    // for the given instrument type
	    this.getErrorManager().issueError(getClass ().getName (),
							TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
							getAttribute("complete_instrument_id"),
							refDataMgr.getDescr("TRANSACTION_TYPE",
												transactionType,
												csdb.getLocaleName()),
							authorizeText);

	    // return false to set the transaction status to failed
	    return false;

	}
	if(TradePortalConstants.INDICATOR_YES.equals(convertedTran)){

	 return SecurityAccess.hasRights(securityRights,SecurityAccess.CC_GUA_CONVERT);

	}
    // IAZ CM-451 03/25/09 Begin
    // Make sure payment date is not backdated
    if (isCashMgmtInstr)
    {
		java.util.Date curDate;
		String timeZone = user.getAttribute("timezone");
		if (StringFunction.isNotBlank(timeZone))
			curDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(), timeZone);
		else
			curDate = GMTUtility.getGMTDateTime();
		if((StringFunction.isBlank(transaction.getAttribute("payment_date")))
			||((compareDate(curDate, transaction.getAttributeDate("payment_date"))) == 1))
		{

	
			String curDateStr = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(curDate);
			if (InstrumentType.DOMESTIC_PMT.equals(getAttribute("instrument_type_code"))) {
				updateExecutionDate(transaction,curDateStr);
			}
			else {
				transaction.setAttribute("payment_date",curDateStr);
			}

		}
	}
    

	corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
													  Long.parseLong(userOrgOid));

	if( corpOrg.isStraightThroughAuthorize() && transaction.isH2HSentPayment()){
		isH2HStraightThroughInd = true;
	}

	// check if it is subsidiary instrument
	if ((StringFunction.isNotBlank(getAttribute("corp_org_oid")))&&(!getAttribute("corp_org_oid").equals(userOrgOid))) {
		// check if use parent Product Authorization Rule is set
		if (TradePortalConstants.INDICATOR_NO.equals(user.getAttribute("use_subsidiary_profile"))) {
			// check if Instrument is disabled on parent Corp Organization
			if (!corpOrg.isInstrumentTypeEnabled(instrumentType)) {
				// display error
				getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.NO_AUTH_ACCESS_AT_PARENT_LEVEL,
						ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE",
								instrumentType, csdb.getLocaleName()));
				// return false to set the transaction status to failed
				return false;
			}
		}
	}
	

    // Make sure user authorizing the transaction can use accounts entered with this transaction.
    String accountsCorpOrgOid = null;
    if (isCashMgmtInstr)
    {
		accountsCorpOrgOid = validateAuthorizedAccounts(user, transaction, userOrgOid, isH2HStraightThroughInd);
		if(accountsCorpOrgOid == null)
    	{
			this.getErrorManager().issueError(getClass().getName(),
			    TradePortalConstants.USER_CANT_USE_ACCOUNTS,
			    	getAttribute("complete_instrument_id"),	//IAZ CM-451 03/20/09 Add
			    	user.getAttribute("first_name"),
			    	user.getAttribute("last_name"));
			return true;
		}
		else
		{
			accountsCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(accountsCorpOrgOid));
			terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
			if (!StringFunction.isBlank(terms.getAttribute("debit_account_oid")))
			{
				
				debitAccount = validateAccount(accountsCorpOrg, terms.getAttribute("debit_account_oid"));
				if (debitAccount == null)
					return false;
		    }

		    if (((InstrumentType.FUNDS_XFER.equals(instrumentType))) &&
		        (validateAccount(accountsCorpOrg, terms.getAttribute("payment_charges_account_oid")) == null))
		    	return false;
	
				// Moved validation logic up in the execution flow
		    if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType)) {
		    	creditAccount = validateAccount(accountsCorpOrg, terms.getAttribute("credit_account_oid"));
		    	if (creditAccount == null)
					return false;
		    }
		
		}

	}
	


	baseCurrency = corpOrg.getAttribute("base_currency_code");
	LOG.debug("InstrumentBean::authorizeTransaction:: Base Currency {}" , baseCurrency);


	// ***********************************************************************
	// If the user has threshold group limits, check the two limits,
	// transaction limits and daily limits.  Transaction amount has to be
	// converted from transaction currency to the instrument's corporate org's
	// base currency, since all limits are in base currency.
	// Note that both limits should be checked even if the other fails.  This
	// is to ensure that all limits errors are provided at once.
	// ***********************************************************************

	LOG.debug("InstrumentBean authorizeTransaction: Check Threshold Limits of threshold oid: {}" ,
		user.getAttribute("threshold_group_oid"));

	String currency = transaction.getAttribute("copy_of_currency_code");
	String amount = transaction.getAttribute("copy_of_amount");
	String thresholdGroupOid = null;    
	 
	if ((StringFunction.isNotBlank(getAttribute("corp_org_oid")))&&(getAttribute("corp_org_oid").equals(userOrgOid)) && (TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs))){
		thresholdGroupOid = user.getAttribute("subsidiary_threshold_group_oid");
	}
	
	// use user's default threshold group
	if (StringFunction.isBlank(thresholdGroupOid))    // NSX 06/29/10 RIUK062552727
	   thresholdGroupOid = user.getAttribute("threshold_group_oid");  // NSX 06/29/10 RIUK062552727


	// If a Loan Request has no amount then get the amount from the loan proceeds payment info
	if (amount.equals("") && this.getAttribute("instrument_type_code").equals(InstrumentType.LOAN_RQST)) {
		Terms customerTerms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
		currency = customerTerms.getAttribute("loan_proceeds_pmt_curr");
		amount = customerTerms.getAttribute("loan_proceeds_pmt_amount");
	}


	if(currency.equals("") || amount.equals("") || isH2HStraightThroughInd )
	{
		// If currency or amount is blank (which can only happen for non-issue
	 	// transactions like amendments), we cannot validate the threshold
	 	// or daily limit.  Set flags so that they will be skipped.
		checkDailyLimit = false;
		checkThresholdLimit = false;
	}
	else
	{
		// get the threshold and daily limit amounts
	    if (!(thresholdGroupOid == null || thresholdGroupOid.equals("")))
	    {

	        thresholdGroup = (ThresholdGroup) createServerEJB(
	        "ThresholdGroup", Long.parseLong(thresholdGroupOid));

	        dailyLimit = thresholdGroup.getDailyLimit(instrumentType, transactionType);
	        thresholdAmount = thresholdGroup.getThresholdAmount(
		    instrumentType, transactionType);

	        if (!(dailyLimit == null || dailyLimit.equals("")))
	    	    checkDailyLimit = true;

	        if (!(thresholdAmount == null || thresholdAmount.equals("")))
	    	    checkThresholdLimit = true;

	        LOG.debug("InstrumentBean authorizeTransaction: Check Threshold Limits Threshold Amount {} daily limit {}" ,
		    thresholdAmount ,dailyLimit);
	    }
	 }


     // ***********************************************************************
	 // if any limits need to be checked, convert transaction amount to base currency
	 //
	 // IAZ CM-451 11/25/08 and 12/01/08 and 01/03/09 Begin
	 // Make sure to convert to base if this is panel authorization transaction
	 // or if Customer Daily Limit enforced
	 // Also, invoke the getAmountInBaseCurrency with instrumentType so that different
	 // fx-rate can be used for different type of instruments
	 //
	 // IAZ ER 5.0.1.1 06/29/09 Begin
     // ***********************************************************************


     custDailyLimit = checkCustDailyLimitEnforced(instrumentType, corpOrg);
	 if (( checkDailyLimit || checkThresholdLimit || panelAuthEnabled) || (custDailyLimit != null))
	 {

  	    if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType))
  	    {
			if (debitAccount.getAttribute("currency").equals(currency))
				rateType = TradePortalConstants.USE_BUY_RATE;
		}
		if ((InstrumentType.FUNDS_XFER.equals(instrumentType)) ||
		    (InstrumentType.DOM_PMT.equals(instrumentType))) 		
		{
			if (debitAccount == null)
				rateType = TradePortalConstants.USE_MID_RATE;
			else
			if ((!currency.equals(baseCurrency)) && (!currency.equals(debitAccount.getAttribute("currency"))))
				rateType = TradePortalConstants.USE_SELL_RATE;
		}

		 amountInBase = getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, rateType);

  	  

	    if (amountInBase.compareTo(BigDecimal.ZERO) < 0 )
	    {
		errorsFound = true;
		checkDailyLimit = false;
		checkThresholdLimit = false;
	    }
		LOG.debug("InstrumentBean authorizeTransaction: Check Threshold Limits base currency conversion: from {} {} to base amount {} {}" ,
				new Object[]{currency , amount , baseCurrency , amountInBase});

	 }

	 

     // ***********************************************************************
  //IAZ CM-451 12/01/08
  //By now, the process calculated the base curency and did ownership check, etc.
  //At this point, we will either do User DailyCheck/ThresholdCheck
  //or Panel Authorization.

  //The if-statement below represent processing of transactions set for Panel Authorization.
  //There are four major parts of the processing:
  		//PART-1: Initial Check for User being able to authorize his/her own input
  		//PART-2: Populate Hashtable that will contain previous panel authorization for
  		//        this transaction.
  		//PART-3: Run Panel Autentication against all possible combinations for specific
  		//        amount range.
  		//PART-4: Process results of the Panel Authorization, set messages/warning/error
  		//        conditions, update statuses, etc.
  // ***********************************************************************

  // 12/16/08: at the moment the second check is redundant as we check instrumentType
  //           to set panelAuthEnabled -- but we have it in case rules
  // 		   for setting panelAuthEnabled change in the future

  // CJR CR-593 04/06/2011 Added isH2HStraightThroughInd condition -- TEST
  // Nar Rel 8.3.0.0 CR 821 16 June 2013 Begin
	String transactionOwnerOrgOid = getAttribute("corp_org_oid");
	CorporateOrganization transactionOwnerOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(transactionOwnerOrgOid));
	// isPanelAuthInProcessForTrans will indicate that at verify time transaction was verified with Panel setting is corp level.
	boolean isPanelAuthInProcessForThisTrans = StringFunction.isNotBlank(transaction.getAttribute("panel_auth_group_oid"));
	boolean isSubsidiaryTransAccess = PanelAuthUtility.isSubsidiaryAccess(transactionOwnerOrgOid, user);
	boolean isSubsidiaryAccessTab = false;
	if (TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs)) {
		isSubsidiaryAccessTab = true;
	}
			
	if(transactionOwnerOrg.isPanelAuthEnabled(instrumentType, transactionType) && 
			PanelAuthUtility.isTransactionAuthInProcess(transaction.getAttribute("transaction_status")) && 
			((!isPanelAuthInProcessForThisTrans && !isH2HStraightThroughInd) || 
					(isPanelAuthInProcessForThisTrans && isH2HStraightThroughInd) )){
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS);
			return false;
		

	}else if(isSubsidiaryTransAccess && !isSubsidiaryAccessTab){
		String logInUserOrgOid = user.getAttribute("owner_org_oid");
		CorporateOrganization logInUserOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(logInUserOrgOid));
		if(logInUserOrg.isPanelAuthEnabled(instrumentType, transactionType) && 
				PanelAuthUtility.isTransactionAuthInProcess(transaction.getAttribute("transaction_status")) &&
				((!isPanelAuthInProcessForThisTrans && !isH2HStraightThroughInd) || 
						(isPanelAuthInProcessForThisTrans && isH2HStraightThroughInd)) ){
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.PANEL_AUTH_CAN_NOT_PROCESS);
				return false;
		}
	}


	if(isPanelAuthInProcessForThisTrans && !isH2HStraightThroughInd) {
	  	  
       //PART-1: Check wether user is allowed to authorize his own input.
       //If not and there is such an attempt, Fail transaction regardless
       //of the current status.

	   String lastEntryUserOid = transaction.getAttribute("last_entry_user_oid");
		if (lastEntryUserOid.equals(userOid)) {

				if(!TradePortalConstants.INDICATOR_YES.equals(userPanelAuthSelf)){
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DIFFERENT_AUTHORIZER_REQUIRED);

					return false;
				}

		}
	   if(isSubsidiaryTransAccess){
		   String logInUserOrgOid = user.getAttribute("owner_org_oid");
		   CorporateOrganization logInUserOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(logInUserOrgOid));
		   if(!logInUserOrg.isPanelAuthEnabled(instrumentType, transactionType)){
			   this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);

				return false;
		   }else if(InstrumentType.DOM_PMT.equals(instrumentType) && TradePortalConstants.INDICATOR_YES.equals(transactionOwnerOrg.getAttribute("pmt_bene_panel_auth_ind")) &&
					!TradePortalConstants.INDICATOR_YES.equals(logInUserOrg.getAttribute("pmt_bene_panel_auth_ind"))){
			   this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);

				return false;
		   }
	   }
		
			LOG.debug("Panel Auth:: about to process previous authorizations");

			String panelGroupOid = transaction.getAttribute("panel_auth_group_oid");
			
            // validating panel group is not deleted.
			PanelAuthorizationGroup panelAuthGroup = (PanelAuthorizationGroup) createServerEJB("PanelAuthorizationGroup");
		    try{
		    	 panelAuthGroup.find("panel_auth_group_oid", panelGroupOid);
		    }catch(AmsException exe){
			     getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_REF_DATA_CHANGED);
			     return false;					
		    }
			String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(transactionOwnerOrg, panelAuthGroup, debitAccount, instrumentType);
			String panelAuthTransactionStatus = TradePortalConstants.AUTHFALSE;
			
			// Nar Release 8.3.0.0 CR 857
			// if beneficiary level panel check box is selected at organization level, then apply panel authorization process at beneficiary level
			// this check box is present only for domestic payment product. 
			if (InstrumentType.DOM_PMT.equals(instrumentType) && 
					TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))) {
				
				panelAuthTransactionStatus = processBeneficiaryLevelPanelAuth(transaction, user, instrumentType, isSubsidiaryAccessTab);
				
			} else {
				// Nar Release 8.3.0.0 CR 821
				// process panel authorization process at transaction level.
				panelAuthTransactionStatus = processTransactionLevelPanelAuth(transaction, user, instrumentType, isSubsidiaryAccessTab);
			}

			if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
				return false;
			}

			boolean isFinalAuth = TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus);
			boolean procceedToAuth = true;

  	   if(InstrumentServices.isCashManagementTypeInstrument(instrumentType)){
	      procceedToAuth = performFXprocess( transaction,  terms,  debitAccount,  creditAccount,  baseCurrencyToPanelGroup,  isFinalAuth, params);
  	   }
  	   
	   if (!procceedToAuth && TradePortalConstants.INDICATOR_YES.equals(params.get("RATE_CHANGED"))) {
		   
		   //Nar IR-T36000013665 10-Oct-2013 ADD Begin 
		   // Coping transaction data into doc to remove Panel Authorizer data and sending doc back to AuthorizeMediatorBean to perform save action.
		   DocumentHandler transactionDoc = new DocumentHandler();
		   transaction.populateXmlDoc(transactionDoc);
		   
		   if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))){
			   Vector<DocumentHandler> dmstPmtPanelAuthList = transactionDoc.getFragments("/Transaction/DmstPmtPanelAuthRangeList");
			   for(DocumentHandler doc : dmstPmtPanelAuthList){
			    transactionDoc.removeComponent("/Transaction/DmstPmtPanelAuthRangeList");
			   }
		   }else{
			   transactionDoc.removeComponent("/Transaction/PanelAuthorizerList");
		   }
		   params.put("transactionDoc", transactionDoc);
		 //Nar IR-T36000013665 10-Oct-2013 ADD End 
		   return true;
	   }
	   
	   
	  String authorizationReults = processPanelAuthResults(panelAuthTransactionStatus, transactionType, userOid,
					transaction, user, refDataMgr, procceedToAuth, isSubsidiaryTransAccess);
	   
	  if (TradePortalConstants.AUTHPARTL.equals(authorizationReults)){
  	   		return true;
	  }
	  
  	  if (TradePortalConstants.AUTHFALSE.equals(authorizationReults)){
  	   		return false;
  	  }
  	  
  	  if (TradePortalConstants.AUTHTRUE.equals(authorizationReults)){
	     if (procceedToAuth){     
			  createOutgoingQueueAndIssueSuccess = true;
	     }else {
  			errorsFound=true;                               	 
  			createOutgoingQueueAndIssueSuccess = false;          
  	     }
  	  }   
       
  	  if (InstrumentType.DOM_PMT.equals(instrumentType) && procceedToAuth) {
      	     updateDomesticPayment(transaction.getAttribute("transaction_oid"));
      }
 
  }// NAR CR 821
    //This is the end of Panel Authorization Case.
    //The else section will encapsulate DailyCheck/ThresholdLimit Case.
  else
  {
	// if threshold amount check fails, issue an error, set
	// the transaction status to failed and continue to validate for
	// daily limit

	if (checkThresholdLimit )
	{
		BigDecimal threshold = new BigDecimal(thresholdAmount);
	    LOG.debug("InstrumentBean authorizeTransaction: Checking Threshold Limits of {}"
		    , threshold);
	    if (amountInBase.compareTo(threshold) > 0)
	    {
			this.getErrorManager().issueError(getClass().getName(),
								    TradePortalConstants.TRANSACTION_LIMIT_EXCEEDED,
		    						user.getAttribute("first_name"),
		    						user.getAttribute("last_name"));

		// transaction status should be set to FAILED
		errorsFound = true;

	    }

	}


	// if daily limit check fails, issue an error, set
	// the transaction status to failed and stop further validations
	if (checkDailyLimit)
	{
		BigDecimal limit = new BigDecimal(dailyLimit);
	    LOG.debug("InstrumentBean authorizeTransaction: Checking Daily Limits of  {}"
		    ,limit);
	    BigDecimal amountAuthorized ;

	  //Get calculated amount depending upon if its subsidary Account
	 // get the total transactions amount authorized by the user today

	    	 amountAuthorized = getAmountAuthorized(userOid,
						user.getAttribute("timezone"),
						baseCurrency,
						userOrgOid,
						instrumentType,
						transactionType,
						rateType);


	    if (amountAuthorized.compareTo(BigDecimal.ZERO) < 0)
	    {
			errorsFound = true;
	    }
	    else
	    {
	    	BigDecimal sumAuthorized = amountInBase.add(amountAuthorized);
			LOG.debug("Daily Limit Total Authorized: {}" ,sumAuthorized);
	    	if (sumAuthorized.compareTo(limit) > 0 )
	    	{
			    this.getErrorManager().issueError(getClass().getName(),
							    	TradePortalConstants.DAILY_LIMIT_EXCEEDED,
							    	user.getAttribute("first_name"),
		   						 	user.getAttribute("last_name"));
			    // transaction status should be set to fail
			    errorsFound = true;
			}
	    }

	}



    //In Subsidiary Mode, Use Authorization Rules Set on Subsidiary Customer Profile if so srt on
    //suthorizing user's User Profile
    String useSubsidiaryProfileForAuthOption = user.getAttribute("use_subsidiary_profile");
    CorporateOrganization authorizationCorpOrg = corpOrg;
   //VS RVU062382912 Add Subsidiary Mode Check
    if(TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs))
    {
	    if (TradePortalConstants.INDICATOR_YES.equals(useSubsidiaryProfileForAuthOption ))
	    {
			if (isCashMgmtInstr)
				authorizationCorpOrg = accountsCorpOrg;
			else
			{
		   		String instrumentACorpOrgOid = getAttribute("corp_org_oid");
		   		LOG.debug("authorizeTransactionMain:: corpOrgOid = {} and userOrgOid = {}" , instrumentACorpOrgOid , userOrgOid);

				if (userOrgOid.equals(instrumentACorpOrgOid) || (StringFunction.isBlank(instrumentACorpOrgOid)))
					authorizationCorpOrg = corpOrg;
		   		else if ((StringFunction.isNotBlank(instrumentACorpOrgOid))&&(!instrumentACorpOrgOid.equals(userOrgOid)))
		   		{
					authorizationCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
														  Long.parseLong(instrumentACorpOrgOid));
			    }
			}
		}
	    //RPasupulati IR T36000024143 Start
	     //if authorization is at parent or Subsidiary level checking weather instrument createde CorpOrg and authorisation user CorpOrg is same or not.
		 //if CorpOrg is diffrent we are creating  authorizationCorpOrg with instrument created corpOrg
	    }else{
	    	String instrumentACorpOrgOid = getAttribute("corp_org_oid");
			
		 if ((StringFunction.isNotBlank(instrumentACorpOrgOid))&&(!instrumentACorpOrgOid.equals(userOrgOid))&&(TradePortalConstants.INDICATOR_YES.equals(useSubsidiaryProfileForAuthOption)))
			{
			authorizationCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
												  Long.parseLong(instrumentACorpOrgOid));
	    }
	    }
	    //RPasupulati IR T36000024143 End



	// ***********************************************************************
	// if a different user should authorize this transaction
	// and verification fails, issue error and stop further validations
	// ***********************************************************************

	LOG.debug("InstrumentBean authorizeTransaction: auth user different ind : {}" ,
	    authorizationCorpOrg.getAttribute("auth_user_different_ind"));


	if (!errorsFound && authorizationCorpOrg.getAttribute("auth_user_different_ind").equals(	
	    TradePortalConstants.INDICATOR_YES))
	{
	    if (transaction.getAttribute("last_entry_user_oid").equals(userOid))
	    {
			this.getErrorManager().issueError(getClass().getName(),
					    TradePortalConstants.DIFFERENT_AUTHORIZER_REQUIRED);
			// transaction status should be set to fail
			errorsFound = true;

	    }
	}

	    boolean procceedAuth = true;
	  	if (isCashMgmtInstr && !errorsFound) {    
	  		boolean isFinalAuthorization = isFinalAuthorization(transaction, user, authorizationCorpOrg,  userOrgOid,  userOid, authorizeAtParentViewOrSubs);
	  		procceedAuth = performFXprocess( transaction,  terms,  debitAccount,  creditAccount,  baseCurrency,  isFinalAuthorization, params);   // NSX 10/07/11 - CR-581/640 Rel. 7.1 -
	  	    if (!procceedAuth) {
	  	    	createOutgoingQueueAndIssueSuccess = false;

	  	    	if (TradePortalConstants.INDICATOR_YES.equals(params.get("RATE_CHANGED"))) {
				   
	  			   //Nar IR-T36000013665 10-Oct-2013 ADD Begin 
	  			   // Coping transaction data into doc send doc back to AuthorizeMediatorBean to perform save action.
	  			   DocumentHandler transactionDoc = new DocumentHandler();
	  			   transaction.populateXmlDoc(transactionDoc);
	  			   params.put("transactionDoc", transactionDoc);
	  			 //Nar IR-T36000013665 10-Oct-2013 ADD End 
	  	    	   return true;
	  	    	}
				
				if (isFinalAuthorization) errorsFound=true;
	  	    }
	  	}



	// ***********************************************************************
	// If dual authorization is required, set the transaction state to
	// partially authorized if it has not been previously authorized and set
	// the transaction to authorized if there has been a previous authorizer.
	// The second authorizer must not be the same as the first authorizer
	// not the same work groups, depending on the corporate org setting.
	// If dual authorization is not required, set the transaction state to
	// authorized.
	// ***********************************************************************

	LOG.debug("InstrumentBean authorizeTransaction: dual indicator: {}"
		, authorizationCorpOrg.getDualAuthIndicator(instrumentType, transactionType));

	if (!errorsFound)
	{

		String dualAuthIndicator = authorizationCorpOrg.getDualAuthIndicator(instrumentType, transactionType);
		String userWorkGroupOid = null;       

		//VS RVU062382912 Add Subsidiary Mode Check for work group
		if ((StringFunction.isNotBlank(getAttribute("corp_org_oid")))&&(!getAttribute("corp_org_oid").equals(userOrgOid)) && (TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs)))
			userWorkGroupOid = user.getAttribute("subsidiary_work_group_oid");

		// use user's default work group
		if (StringFunction.isBlank(userWorkGroupOid))             
			userWorkGroupOid = user.getAttribute("work_group_oid");    

		LOG.debug("InstrumentBean::authorizeTransaction: Dual indicatior: {}" , dualAuthIndicator);

		//pcutrone - 07-NOV-2007 - IR-KXUF021163489 - Get first authorizing user info earlier.  moved from below[BEGIN]
    	String firstAuthorizer = transaction.getAttribute("first_authorizing_user_oid");
        String firstAuthorizerWorkGroup = transaction.getAttribute("first_authorizing_work_group_oid");
	    //pcutrone - 07-NOV-2007 - IR-KXUF021163489 - Get first authorizing user info earlier.  moved from below[END]
		// Two work groups required and the user does not have a work group assigned.  Give error.
		// CJR CR-593 04/06/2011 Added isH2HStraightThroughInd condition
		if (!isH2HStraightThroughInd
			&& dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
		    && (userWorkGroupOid == null || userWorkGroupOid.equals("")))
		{
			errorsFound = true;
			this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
		}
	    else if (!isH2HStraightThroughInd
	    		&& (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS)
	        || dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
	        || !(firstAuthorizer == null || firstAuthorizer.equals("")))) //pcutrone - 07-NOV-2007 - IR-KXUF021163489 - Added check to see if first auth user was poplulated on transaction during second authorization
	    {

	    	if (firstAuthorizer == null || firstAuthorizer.equals(""))
	    	{
				// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
				transaction.setAttribute("first_authorizing_user_oid", userOid);
				transaction.setAttribute("transaction_status",
					TransactionStatus.PARTIALLY_AUTHORIZED);
				transaction.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
				transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());

				transaction.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());

				this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
							getAttribute("complete_instrument_id"),
							refDataMgr.getDescr("TRANSACTION_TYPE",
							transactionType,
							csdb.getLocaleName()));
		    	// rkazi CR-596 11/16/2010 Add code to update sequence number in domestic payment.
		    	updateDomesticPayment(transaction.getAttribute("transaction_oid"));
		    	// rkazi CR-596 11/16/2010 Add code to update sequence number in domestic payment. End
	    	}
	    	else  //firstAuthorizer not empty
	    	{
				// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
				if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
				          && userWorkGroupOid.equals(firstAuthorizerWorkGroup))
				{
					errorsFound = true;
					this.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
				}
				// Two authorizers required and this user is the same as the first authorizer. Give error.
				// Note requiring two work groups also implies requiring two users.
				else if (firstAuthorizer.equals(userOid))
				{
					this.getErrorManager().issueError(getClass().getName(),
						 TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
					// transaction status should be set to fail
					errorsFound = true;
				}
				// Authorize
				else
				{
					transaction.setAttribute("second_authorizing_user_oid", userOid);
					transaction.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
					transaction.setAttribute("second_authorize_status_date",
										DateTimeUtility.getGMTDateTime());

					authorize(transaction, user.getAttribute("locale"), user.getAttribute("timezone"),procceedAuth);

					if (procceedAuth)   {        
					createOutgoingQueueAndIssueSuccess = true;
					}
				}
	    	}
	    }
	    else
	    {
		    // dual auth ind is NO, first auth is EMPTY
		    transaction.setAttribute("first_authorizing_user_oid", userOid);
			transaction.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
		    transaction.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());

		    authorize(transaction, user.getAttribute("locale"), user.getAttribute("timezone"),procceedAuth);

		    if (procceedAuth) {           
		    createOutgoingQueueAndIssueSuccess = true;
	    	LOG.debug("InstrumentBean authorizeTransaction: EVALUATED dual ind : {}" ,
	    		corpOrg.getAttribute("auth_user_different_ind"));

	    	updateDomesticPayment(transaction.getAttribute("transaction_oid"));

		    }     
	    }

   	}
  }
  //End of User DailyLimit/ThresholdLimit Section



  // ***********************************************************************
  // IAZ CM-451 02/02/09 Begin
  // Update Customer Limits and/or Balances
  // ***********************************************************************

 if (isCashMgmtInstr && !errorsFound  && createOutgoingQueueAndIssueSuccess)
 {
     //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 start
     //if necessary add a POW party
	 //Leelavathi - IR-T36000013625 22/07/2013 Begin
	 //Debit and parent account is always same where tranferding funds to subsidiary account
	 //hence added new condition

	 if ( isAccountOwnedByOtherCorp(debitAccount)
			|| (InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code")) 
		 && isAccountOwnedByOtherCorp(creditAccount) )) {
         makeAcctParentPaymentOwner(instrumentType,terms,debitAccount);
     }
	 //Leelavathi - IR-T36000013625 22/07/2013 End
     //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 end

 

	 transaction.setAttribute("daily_limit_exceeded_indicator",
								TradePortalConstants.INDICATOR_NO);


	 if (!accountsCorpOrgOid.equals(userOrgOid))
	 {
		 LOG.debug("InstrumentBean::Authorize::Retrieveing Subsideary Data::CustDailyLimit, BaseCurrency, AmountinBase");
		 custDailyLimit = checkCustDailyLimitEnforced(instrumentType, accountsCorpOrg);
		 baseCurrency = accountsCorpOrg.getAttribute("base_currency_code");
		amountInBase = getAmountInBaseCurrency(currency, amount, baseCurrency, accountsCorpOrgOid, rateType);


		 if (amountInBase.compareTo(BigDecimal.ZERO) < 0 )
		 {
			 createOutgoingQueueAndIssueSuccess = false;
			 return true;
		 }

	 }

     int retryAttempt = 1;
    
     
     while (retryAttempt <= TradePortalConstants.MAX_CORP_ORG_RETRY)
     {
    	 LOG.debug("InstrumentBean::AuthorizeTransaction::retry is {}" , retryAttempt);
       	 accountsCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(accountsCorpOrgOid));

    	 if (StringFunction.isNotBlank(terms.getAttribute("debit_account_oid"))){
     		    //we retrieve debit account at the top of authroization method to find correct exchange rate				
     		debitAccount = (Account) accountsCorpOrg.getComponentHandle("AccountList", terms.getAttributeLong("debit_account_oid"));
     		if (InstrumentType.XFER_BET_ACCTS.equals(instrumentType))	{

     		
				if (isAccountOwnedByOtherCorp(creditAccount)) {
					updatePayeeOtherCorp(terms, creditAccount);
				}

     		} else if ((instrumentType.equals(InstrumentType.FUNDS_XFER))	||
     		           (instrumentType.equals(InstrumentType.DOM_PMT)))	
     		{
     			

     		}  else {
     			debitAmt = new BigDecimal(amount);
     		}
    	 }

    	 try
    	 {

			//MDB CR-609 11/30/10 Begin
			 if ((StringFunction.isNotBlank(instrumentType)) && (instrumentType.equals(InstrumentType.DOM_PMT)))
				//Ravindra - Rel710 - IR VAUL030955566 - 9th Aug 2011 - Start
			 {
				isFutureDatePayment = TPDateTimeUtility.isFutureDate(transaction.getAttribute("payment_date"), user.getAttribute("timezone"));

			 }
			//Ravindra - Rel710 - IR VAUL030955566 - 9th Aug 2011 - End
			//MDB CR-609 11/30/10 End

			 if (!isFutureDatePayment) //MDB IR REUK121164150 12/13/10 - move outside of available fund customer and don't process for either type of customer
			 {
	    		 if (StringFunction.isNotBlank(custDailyLimit))
	    		 {
	    			 transaction.setAttribute("daily_limit_exceeded_indicator",
								TradePortalConstants.INDICATOR_PASSED);
	    			//it's a dialy limit customer, check dialy limit and update running total
	    			updateCustomerDailyLimit(userOrgOid, accountsCorpOrg, transaction, instrumentType, custDailyLimit, amountInBase, retryAttempt);
					terms.setAttribute("debit_pending_amount_in_base", amountInBase.toString());
	    		 }
	    		 else {
	    			 //it's a available fund customer, check available balance with bank
	    			
	    		 }

			 }

    		 if (StringFunction.isNotBlank(custDailyLimit) ||
    		     StringFunction.isNotBlank(terms.getAttribute("debit_account_oid")))
    		 {

				
				//Handle Opt Lock Condition

	   			if (retryAttempt < TradePortalConstants.MAX_CORP_ORG_RETRY)
	   				accountsCorpOrg.setRetryOptLock(true);
				else
					accountsCorpOrg.setRetryOptLock(false);
	   			accountsCorpOrg.touch();
	   			int saveOK = accountsCorpOrg.save(false);

				if (saveOK == -1)
				{
					if (retryAttempt == TradePortalConstants.MAX_CORP_ORG_RETRY)
					{
						LOG.info("InstrumentBean::AuthorizeTransaction::Can't resolve Opt_Lock with retry: {}" , retryAttempt);
						this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.CANNOT_UPDATE_CUST_DAILY_LIMIT);
						return true;
					}
				}
				else if (saveOK == -2)
				{
					LOG.debug("optLockretry");
					retryAttempt++;
					continue;
				}
			  }
    		  break;

			  
    	 }
    	 catch (OptimisticLockException opt_lock_exc)
		 {
			 LOG.info("InstrumentBean::AuthorizeTransaction::Opt-lock error occured while updating customer, retry: {}" , retryAttempt++);
			 if (retryAttempt > TradePortalConstants.MAX_CORP_ORG_RETRY)
			 {
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.CANNOT_UPDATE_CUST_DAILY_LIMIT);
				return true;
			 }
			 retryAttempt++;
			
		 }
	 }

  	}
    

	// ***********************************************************************
	// create the interface event record if transaction has been set to
	// authorized.  It uses the Business Object to create it instead of
	// EJBObjectFactory to allow this object to participate in the current
	// transaction
	// ***********************************************************************

	if (createOutgoingQueueAndIssueSuccess)
	{
	    oiQueue = (OutgoingInterfaceQueue)
	    	this.createServerEJB("OutgoingInterfaceQueue");
	    oiQueue.newObject();
	    String attributeNames[] = new String[] {"date_created", "status",
	         "msg_type", "instrument_oid", "transaction_oid", "message_id","process_parameters"};//Add process_parameters for CR-1028 Rel9.3.5

	    // the message_id (oid) should be unique for the middleware -
	    // retrieve this information by getting an instance of ObjectIDCache
	    // for MESSAGE (middleware) to generate the object id.

		
	    String messageId = InstrumentServices.getNewMessageID();
		
		String createDate = DateTimeUtility.getGMTDateTime();
		if (isFutureDatePayment)
		{
            //cquinton - 05 Jan 11 - Rel 6.1.0 - IR#RSUK121166923 - Begin
            String futureDateTime = transaction.getAttribute("payment_date");
            //adjust the future date to the first minute in the future date local time
            
            //RPasupulati 14th Aug 2014 IR T36000020108 Start.
            String userTimeZone;
            //Checking for Subsidary if it is Subsidary will assing child timezone else it will parent.
            
            if(!(isSubsidiaryTransAccess)){
            	
			userTimeZone = corpOrg.getAttribute("timezone_for_daily_limit");
			
            }else{
            	CorporateOrganization SubCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
						  Long.parseLong(transactionOwnerOrgOid));
               
            	
            	userTimeZone = SubCorpOrg.getAttribute("timezone_for_daily_limit");
            } 
            
          //RPasupulati 14th Aug 2014 IR T36000020108 End.
            if (!StringFunction.isNotBlank(userTimeZone)) {
                userTimeZone = user.getAttribute("timezone");
            }
			try {
			    //get start of local day
                Date adjustedFutureGmtDateTime =
    			    TPDateTimeUtility.getStartOfLocalDayInGMT(
	        	        futureDateTime, userTimeZone);
                //add one minute
                long futureDateLong = adjustedFutureGmtDateTime.getTime();
                //futureDateLong += 60 * 1000; // 60 seconds in millis   //NSX IR# SOUL021631463 Rel. 6.1.0
                futureDateLong += 28800000; // 8hrs = 8*60*60*1000 milli sec  //NSX IR# SOUL021631463 Rel. 6.1.0
                adjustedFutureGmtDateTime = new Date(futureDateLong);
                createDate = DateTimeUtility.convertDateToDateTimeString(adjustedFutureGmtDateTime);
			} catch (AmsException aex) {
			    //if there is any issue, just default to the future date
			    //note: this skips gmt conversion
			    createDate = futureDateTime;
			}
            //cquinton - 05 Jan 11 - Rel 6.1.0 - IR#RSUK121166923 - Begin
		}
		
		//Rel9.3.5 CR-1028 START - If the Corporate Customer is not integrated with TPS, add a process_parameter to indicate the outgoing agent not to put message on MQ.
		//Check for the instrumentType and transactionType to see if they are in the scope of CR-1028
		StringBuilder processParameters = new StringBuilder();
		if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("cust_is_not_intg_tps")) && this.checkTypesForDoNotSendMQ(transactionType)){
			processParameters.append("DoNotSendMQ=");		 
			processParameters.append(TradePortalConstants.INDICATOR_YES).append("|");
			transaction.setAttribute("downloaded_xml_ind", TradePortalConstants.INDICATOR_NO);
		}
		//Rel9.3.5 CR-1028 END

	    String attributeValues[] = new String[] {createDate,
	        TradePortalConstants.OUTGOING_STATUS_STARTED,
	    	(instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION)) ? MessageType.DIRECTDEBIT : TradePortalConstants.MSG_TRANSACTION,
	    	this.getAttribute("instrument_oid"),
	    	transaction.getAttribute("transaction_oid"),
			messageId,processParameters.toString() }; //Add process_parameters for CR-1028 Rel9.3.5

	    // Set the return message id of the transaction to the message id generated
	    // for the outgoing queue
		transaction.setAttribute("return_message_id", messageId);

	    oiQueue.setAttributes(attributeNames, attributeValues);
	    oiQueue.save();


	    if (TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(corpOrg.getAttribute("po_upload_format")) &&
	    	 (InstrumentType.IMPORT_DLC.equals(instrumentType) || InstrumentType.APPROVAL_TO_PAY.equals(instrumentType))) {
	    	if (updatePurchaseOrders(transaction,userOid)) {
			    createPOMMessage(transaction,"POM");//SHR CR708 Rel8.1.1.0
	    	}
	    }


	    if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType) &&
	    	!TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transactionType)){
	    	Terms custTerms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
	    	if (TradePortalConstants.INDICATOR_YES.equals(custTerms.getAttribute("invoice_only_ind"))) {
	    		if (updateInvoices(transaction,userOid)) {
	    			createPOMMessage(transaction,"INM");
	    		}
	    	}
	    }


	    if (InstrumentType.LOAN_RQST.equals(instrumentType)) {

	    	if (updateInvoices(transaction,userOid)) {
			    createPOMMessage(transaction,"INM");
	    	}
	    }

	    this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.TRANSACTION_PROCESSED,
					refDataMgr.getDescr("INSTRUMENT_TYPE",
							instrumentType,
							csdb.getLocaleName()) + " - " + getAttribute("complete_instrument_id"),
					refDataMgr.getDescr("TRANSACTION_TYPE",
						transactionType,
						csdb.getLocaleName()),
					authorizedText);
  	}

	if (!errorsFound)
	{

	    this.save();

	    // return true to NOT set the transaction status to failed
	    return (getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY);
	    	
	}
	else
	    // return false to set the transaction status to failed
	    return false;


   }




    /** Updates status of all Purchase Orders associated with the transaction
     * @param userOid
     * @throws RemoteException
     * @throws AmsException
     */
   private boolean updatePurchaseOrders(Transaction transaction, String userOid) throws RemoteException, AmsException {

	   boolean success = false;
	   String select = "select purchase_order_oid from PURCHASE_ORDER where a_transaction_oid=? and a_instrument_oid =?";

	   DocumentHandler results = DatabaseQueryBean.getXmlResultSet(select, false, new Object[]{transaction.getAttribute("transaction_oid"),getAttribute("instrument_oid")});
	   if(results != null){
		   Vector poList = results.getFragments("/ResultSetRecord");
		   Vector poOids = new Vector();
		   if (poList.size() > 0) {
			   success = true;
			// DK IR-RBUM062632733 Rel8.1.1 10/17/2012 Begins
			   for(int i=0; i<poList.size(); i++){
				   poOids.add(results.getAttribute("/ResultSetRecord("+i+")/PURCHASE_ORDER_OID"));
			   }

		   StringBuilder sql         = new StringBuilder();
	        String         poListSql   = null;
	        int            resultCount = 0;
	        try
	        {
	            // Construct a comma separated list of the po oids within
	            // parenthesis
	        	String requiredOid = "purchase_order_oid";
	            poListSql = POLineItemUtility.getPOLineItemOidsSql(poOids,requiredOid);
	            Object[] arguments = {"?", "?"};

	            sql.append(MessageFormat.format(STRUCTURED_PO_MASS_UPDATE_SQL, arguments));
	            sql.append(" ,    status = ? ");

	            sql.append(" where ");
	            sql.append(poListSql);
	            resultCount = DatabaseQueryBean.executeUpdate(sql.toString(), false, new Object[]{transaction.getAttribute("transaction_oid"),getAttribute("instrument_oid"),TradePortalConstants.PO_STATUS_AUTHORISED});
	            //SHR IR T36000005804 - create history here instead of creating in PurchaseOrderBean.postSave () when instrument is authorised.
	            if(resultCount!=0){
	            	String poHistorySQl =  "INSERT into PURCHASE_ORDER_HISTORY (PURCHASE_ORDER_HISTORY_OID, ACTION_DATETIME, ACTION, " +
        	        "STATUS, P_PURCHASE_ORDER_OID, A_USER_OID) VALUES (?,?,?,?,?,?)";
	            	try(Connection con = DatabaseQueryBean.connect(false);
	            			PreparedStatement pStmt = con
		        					.prepareStatement(poHistorySQl)) {
	        		con.setAutoCommit(false);
	        		
	            	for(int oidCount=0;oidCount < poOids.size();oidCount++){
					   InvoiceUtility.createPurchaseOrderHisotryLog(pStmt,TradePortalConstants.PO_ACTION_AUTHORISE,TradePortalConstants.PO_STATUS_AUTHORISED,poOids.get(oidCount).toString(),userOid);
	            	}
	            	pStmt.executeBatch();
	    			con.commit();
	    			
	    		} catch (AmsException | SQLException e) {
	    			LOG.error("InstrumentBean:: Exception caught while updatePurchaseOrders(): ",e);
	    		}
	            }
	        }
	        catch (SQLException e)
	        {
	            throw new AmsException("SQL Exception found executing sql statement \n"
	                    + sql
	                    + "\nException is " + e.getMessage());
	        }
		   }
		// DK IR-RBUM062632733 Rel8.1.1 10/17/2012 Ends
	   }
	   return success;
   }


//SHR CR-708 Rel8.1.1 - Change - modify method to create INM/POM mesaage
   /**
    * Creates POM message
    * @param transaction
    * @throws RemoteException
    * @throws AmsException
    */
   private void createPOMMessage(Transaction transaction,String msgType) throws RemoteException, AmsException {

	   OutgoingInterfaceQueue oiQueue = (OutgoingInterfaceQueue) createServerEJB("OutgoingInterfaceQueue");
	   oiQueue.newObject();
	   oiQueue.setAttribute("date_created",DateTimeUtility.getGMTDateTime());
	   oiQueue.setAttribute("status",TradePortalConstants.OUTGOING_STATUS_STARTED);
	   oiQueue.setAttribute("msg_type",msgType);
	   oiQueue.setAttribute("instrument_oid",getAttribute("instrument_oid"));
	   oiQueue.setAttribute("transaction_oid",transaction.getAttribute("transaction_oid"));
	   oiQueue.setAttribute("message_id",InstrumentServices.getNewMessageID());
	   oiQueue.save();

   }
   

   /** Updates status of all Purchase Orders associated with the transaction
    * @param userOid
    * @throws RemoteException
    * @throws AmsException
    */
	private boolean updateInvoices(Transaction transaction, String userOid)
			throws RemoteException, AmsException {
		LOG.debug("updateInvoices() userOid-> {}",userOid);
		
		String select = "select upload_invoice_oid from INVOICES_SUMMARY_DATA where a_transaction_oid=? and a_instrument_oid =? " ;
	
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(select, false, transaction.getAttribute("transaction_oid"),getAttribute("instrument_oid"));

		if (results != null) {
			List<DocumentHandler> invOidsList = results.getFragmentsList("/ResultSetRecord");
			if (invOidsList.size() > 0) {
				try {
					
					for (DocumentHandler row :invOidsList) {
						  String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
						 InvoicesSummaryData inv = (InvoicesSummaryData) createServerEJB(
									"InvoicesSummaryData", Long.parseLong(invOid));
						 
						 String tpName =inv.getTpRuleName();
						 
						inv.setAttribute("tp_rule_name",tpName);
						inv.setAttribute("status", TradePortalConstants.PO_STATUS_AUTHORISED);
						inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_AUTHORISED);
						inv.setAttribute("action", TradePortalConstants.PO_ACTION_AUTHORISE);
						inv.setAttribute("user_oid", userOid);
						inv.save();
					}

					} catch (AmsException e) {
						LOG.error("InstrumentBean:: Exception caught while updateInvoices(): ",e);
					}
					return true;
			}

		}
		return false;
	}



   private void updateDomesticPayment(String transactionOid) {
       long methodStart = System.currentTimeMillis();

       //cquinton 6/4/2011 Rel 7.0.0 performance
	   //now that we are only updating payment status we can do in a single update statement
	   //rather than individual row updates. this is significantly faster.
       try(Connection con = DatabaseQueryBean.connect(true);
           PreparedStatement pStmt =  con.prepareStatement("update DOMESTIC_PAYMENT set PAYMENT_STATUS = ? where p_transaction_oid = ?")) {
           
           pStmt.setString(1, TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PENDING);
           pStmt.setString(2, transactionOid);
           pStmt.execute();
          
       } catch (AmsException | SQLException e) {
    	   LOG.error("InstrumentBean:: Exception caught while updateDomesticPayment(): ",e);
       }

		LOG.debug("\t[InstrumentBean.updateDomesticPayment(String)] \t[PERFORMANCE] \tcreateDPBeanInTran\t {} \tmilliseconds ",(System.currentTimeMillis()-methodStart));
	}



  
   /**
	* This method returns an amount from a base currency to
	* the given foreign currency.
	* Note: only the absolute value is returned.  If the amount is
	* provided is negative, the amount returned is positive.
	*
	* @param fxcurrency  java.lang.String - the foreign currency
	* @param baseCurrency  java.lang.String - the base currency
	* @param baseAmount java.lang.String - the base amount
	* @param userOrgOid    java.lang.String - the corporate org. oid
     * @param useType
	* @return double - the amount converted to foreign currency
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws java.rmi.RemoteException
 */


  //Update main method to be able to accept rate entered by the user and to return (in a vector)
  //a calc method used to calculate eqv amount
  //Maintain original method as well.

  public BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency,
			String baseAmount, String userOrgOid, String useType) throws AmsException, RemoteException {

		return 	getAmounInFXCurrency(fxcurrency, baseCurrency, baseAmount, userOrgOid, useType, null, null, false); //MDB MRUL122933633 Rel7.1 1/3/12
  }

  public BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency,
			String baseAmount, String userOrgOid, String useType, BigDecimal userEnteredRate, Vector outMDInd)
			throws AmsException, RemoteException {

		return 	getAmounInFXCurrency(fxcurrency, baseCurrency, baseAmount, userOrgOid, useType, userEnteredRate, outMDInd, false); //MDB MRUL122933633 Rel7.1 1/3/12
  }

  public BigDecimal getAmounInFXCurrency(String fxcurrency, String baseCurrency,
			String baseAmount, String userOrgOid, String useType, BigDecimal userEnteredRate, Vector outMDInd, boolean setScaleFlag) //MDB MRUL122933633 Rel7.1 1/3/12
			throws AmsException, RemoteException {

		BigDecimal amountInBase = (new BigDecimal(baseAmount)).abs();
		BigDecimal amountInForeign = new BigDecimal(-1.0f);
		DocumentHandler resultSet, fxRate;
		String multiplyIndicator = null;
		BigDecimal use_rate = null;
		String useRateParm = null;

		if (baseCurrency.equals(fxcurrency)) {
			return amountInBase;
		}

		//IAZ ER 5.2.1.3 06/29/10 - Update
		//useRateParm is used to obatin rate if it is not passed
		//by the calling routine (e.g., not entered by the user)
		if (userEnteredRate == null)
		{
			if (StringFunction.isBlank(useType))
			{
				useType = TradePortalConstants.USE_BUY_RATE;
			}
			if (TradePortalConstants.USE_SELL_RATE.equals(useType))
			{
				useRateParm = "/SELL_RATE";
			}
			else
			{
				useRateParm = "/BUY_RATE";
			}
		}
		else
			use_rate = userEnteredRate;
		//IAZ ER 5.2.1.3 06/29/10 - End-Update

		// get the foreign exchange rate. If it doesn't exist,
		// issue error. Otherwise, get the multiply indicator
		// and rate and convert the amount

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		resultSet = (DocumentHandler) fxCache.get(userOrgOid + "|" + fxcurrency);

	

		if (resultSet == null) {
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); 
		} else {
			fxRate = resultSet.getFragment("/ResultSetRecord");
			if (fxRate == null) {
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); 

			} else {
				multiplyIndicator = fxRate.getAttribute("/MULTIPLY_INDICATOR");


				//Only use rate form table if not entered by user
				if (use_rate == null)
				{
					try {
					
						use_rate = fxRate.getAttributeDecimal(useRateParm);
					} catch (Exception any_e) {
						this.getErrorManager().issueError(getClass().getName(),
						
							TradePortalConstants.NO_FOREX_DEFINED, fxcurrency); 
					}
			    }
			    
			}
		}


		//Send back Calc Method if requested by the callign routine
		if (multiplyIndicator != null && use_rate != null) {
			 int dec = TPCurrencyUtility.getDecimalPrecision(fxcurrency);
			if (multiplyIndicator.equals(TradePortalConstants.DIVIDE)){

				if (setScaleFlag)
					amountInForeign = amountInBase.multiply(use_rate);
				else
					amountInForeign = amountInBase.multiply(use_rate).setScale(dec, BigDecimal.ROUND_HALF_UP);

				if (outMDInd != null)
					outMDInd.add(0, TradePortalConstants.MULTIPLY);
			}else{

				if (setScaleFlag)
					amountInForeign = amountInBase.divide(use_rate, java.math.MathContext.DECIMAL128);
				else
					amountInForeign = amountInBase.divide(use_rate, dec, BigDecimal.ROUND_HALF_UP);

				if (outMDInd != null)
					outMDInd.add(0, TradePortalConstants.DIVIDE);
			}
		}
		

		LOG.debug("fx rate used is: {}  for type: {} ", use_rate ,useType);

		LOG.debug("Base amount: {} and amount in ForeignCurrency: {} ", baseAmount ,amountInForeign.toString());
		return amountInForeign;

	}


  /**
	 *
	 *
	 * @param instrNum
	 * @return boolean
         * @throws java.rmi.RemoteException
         * @throws com.amsinc.ecsg.frame.AmsException
	 */
   public boolean instrumentNumberIsUnique(String instrNum) throws RemoteException, AmsException
   {
	  return isUnique("complete_instrument_id", instrNum, " and a_client_bank_oid = " + this.getAttribute("client_bank_oid"));
   }

  /**
   *
   *
   * @param  transactionType
   * @return boolean
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */
	public boolean isTransactionTypeValid(String transactionType) throws RemoteException, AmsException {
		LOG.debug("Enter isTransactionTypeValid({}) ", transactionType);

		String instrumentTypeCode = this.getAttribute("instrument_type_code");

		// Discrepancy response is a valid transaction type for:
		// Import LCs, Export LCs, Incoming Standbys, and Outgoing Standbys
		// (that are not really guarantees)

		if (transactionType.equals(TransactionType.DISCREPANCY)) {
			if (instrumentTypeCode.equals(InstrumentType.IMPORT_DLC)
					|| instrumentTypeCode.equals(InstrumentType.EXPORT_DLC)
					|| instrumentTypeCode.equals(InstrumentType.INCOMING_SLC)
					|| instrumentTypeCode.equals(InstrumentType.STANDBY_LC)
					|| instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE))
				return true;
		}
		// rkrishna CR 375-D ATP-Response 08/22/2007 Begin
		else if (transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE)) {
			if (instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY))
				return true;
		}
		// rkrishna CR 375-D ATP-Response 08/22/2007 End

		List<String> tranList = InstrumentServices.getPossibleTransactionTypes(instrumentTypeCode,	this.getAttribute("original_transaction_oid"));

		return tranList.contains(transactionType);

	}

  /**
   *
   *
   * @return boolean
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */
   public boolean pendingTransactionsExist() throws RemoteException, AmsException
   {
		LOG.debug("Entering PendingTransactionsExist()");
		LOG.debug("Getting pending transaction count using sql statement.");

		int	pendingTransactionsCount;
		String instrumentOid  = this.getAttribute("instrument_oid");
		String selectClause ;
		String fromClause ;

		selectClause = "transaction_oid";
		fromClause   = "transaction";

		String whereClause = "p_instrument_oid = ?  AND transaction_status not in (?,?,?) ";
		List<Object> sqlParams = new ArrayList<Object>();
		sqlParams.add(instrumentOid);
		sqlParams.add(TransactionStatus.CANCELLED_BY_BANK);
		sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
		sqlParams.add(TransactionStatus.DELETED);

		pendingTransactionsCount = DatabaseQueryBean.getCount( selectClause, fromClause, whereClause.toString(), false, sqlParams );

		if (pendingTransactionsCount > 0)
		{
			LOG.debug("returning true");
			return true;
		}

		
		return false;
   }




  /**
   * String createNew ()
   *
   * This function will call other functions and execute logic that will create a new instrument.
   * A transaction will then be created for the newly created instrument.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /clientBankOid ---------- The oid of the client bank that owns the corporation
   *    /bankBranch ------------- The oid of the Operational Bank Org
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /copyType --------------- What we copy a new instrument from - possible values:
   *                              (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *    /ownerOrg ------------- The oid of the user's corporate organization
   *
   * @param doc  - this is a DocumentHandler stored as a char array
   * @return String - the oid of the transaction created from that you can get the instrument and terms
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */
  public String createNew (DocumentHandler doc) throws RemoteException, AmsException
  {
    return this.createNew(doc, true);
  }

  /*
   * This method is accessed directly only by createLCFromPOData because we don't want to
   * perform the save twice.
   */

  private String createNew (DocumentHandler doc, boolean performSave) throws RemoteException, AmsException
  {

	// W Zhu 8/17/2012 Rel 8.1 T36000004579 add key parameter.
	String instrumentOid ;
	String secretKeyString = csdb.getCSDBValue("SecretKey");
	javax.crypto.SecretKey secretKey = null;
	if (secretKeyString != null){
		byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
		secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");


	}
	String decryptedString = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/instrumentOid"), secretKey);
	if(StringFunction.isNotBlank(decryptedString) && decryptedString.contains("/")){
		instrumentOid = (decryptedString.split("/"))[1];
	}else{
		instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/instrumentOid"), secretKey);
	}
	String sourceOid        = InstrumentServices.parseForOid(instrumentOid); //the oid of the instrument from which we will copy
	String clientBankOid    = doc.getAttribute("/clientBankOid"); //the oid of the user's client bank
	String instrumentType   = doc.getAttribute("/instrumentType"); //at this point this will be populated only if we are copying from a default template
	String oboOid           = doc.getAttribute("/bankBranch"); //the operational bank org that will be assigned to this instrument
	String templateOid      = InstrumentServices.parseForOid(instrumentOid); //in case /instumentOid id actually a template oid
	String copyFrom         = doc.getAttribute("/copyType"); //the type of object (instrument, template, or default template (blank)) from which we will copy
	String corpOrgOid       = doc.getAttribute("/ownerOrg"); //the oid of the user's corporate org.
	String userOid          = doc.getAttribute("/userOid"); //the user's oid
	String fromExpress      = TradePortalConstants.INDICATOR_NO; //indicator that is set to 'Y' if we are copying from an express template
	String instrNumStr      = null; //the instrument number returned from the client bank
	String prefix           = ""; //the instrument id prefix returned from the operational bank org
	String suffix           = ""; //the instrument id suffix returned from the operational bank org
	String completeInstrId  = null; //concatpnation of prefix + instrNumStr + suffix
	// Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
	String manualInstId     = doc.getAttribute("/manualInstrumentOid");
	String fixedFlag      = TradePortalConstants.INDICATOR_NO;//Rpasupulati IR T36000019732
	// Srinivasu_D CR#269 Rel8.4 09/02/2013 - end
	// instrumentType is the value passed in via the xml.  It will usually
	// be a real instrument type code.  However, for the Standby as Guarantee
	// logic, it will be a fake code (like 'DetailedSLC').  Because of this
	// we convert this instrumentType to a logicalInstType and a physical
	// instrument type and use these values in setting info in the
	// instrument object.  We pass instrumentType to Transaction and he
	// will do the same kinds of conversions as necessary.
	String logicalInstType;
	String physicalInstType;

	long instrumentNum      = -1; //stores the long returned from client bank that is converted to instrNumStr
	long transactionOid     = -1; //will store the oid of the new transaction

	Instrument sourceInst; //will point to the instrument from which we copy

	LOG.debug("doc \n {}" , doc);

	//Get the user's copr org - we'll use this to get all the possible operational bank orgs
	LOG.debug("Getting corpOrg");
	CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization");
	corpOrg.getData(Long.parseLong(corpOrgOid));

	//Validate oboOid against the corp org's operational bank orgs
	LOG.debug("Validating oboOid == {}",oboOid);

	/*Kyriba CR 268 Start
	 * Get all External bank for thhis corp org, validate for the selected bank*/
	ComponentList externalBankComponenet = (ComponentList) corpOrg.getComponentHandle("ExternalBankList");
	int extBankTotal = externalBankComponenet.getObjectCount();
	BusinessObject externalBankBusinessObj;
	List<String> tempList = new ArrayList<>();

	for(int i=0; i<extBankTotal; i++){

		externalBankComponenet.scrollToObjectByIndex(i);
		externalBankBusinessObj = externalBankComponenet.getBusinessObject();
		tempList.add(externalBankBusinessObj.getAttribute("op_bank_org_oid"));


	}
	boolean hasExternalBank = tempList.contains(oboOid);
	//Kyriba CR 268 End

	if ( !( oboOid.equals(corpOrg.getAttribute("first_op_bank_org")) ||
		  oboOid.equals(corpOrg.getAttribute("second_op_bank_org"))||
		  oboOid.equals(corpOrg.getAttribute("third_op_bank_org")) ||
		  oboOid.equals(corpOrg.getAttribute("fourth_op_bank_org")) || hasExternalBank ) )
	{
		//ERROR
		LOG.debug("NTM_WRONG_OP_BANK_ORG error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_WRONG_OP_BANK_ORG);
		return null;
	}

	//get the operational bank org so that we can get the prefix and suffix

	OperationalBankOrganization opBankOrg = (OperationalBankOrganization)createServerEJB("OperationalBankOrganization");
	LOG.debug("Calling opBankOrg.getData({})",oboOid);

	opBankOrg.getData(Long.parseLong(oboOid));

	//get the client bank so that we can get the instrument number

	ClientBank clientBank = (ClientBank) createServerEJB("ClientBank");
	clientBank.getDataFromReferenceCache(Long.parseLong(clientBankOid));
	//Srinivasu_D CR#269 Rel8.4 09/02/2013 - added
	if(InstrumentServices.isNull(manualInstId)){

	instrumentNum = clientBank.useNextInstrumentNumber();

	LOG.debug("Returned instrument number = {}" , instrumentNum);

	//client bank returns -1 if the instrument number pool for the client bank has been exhausted
	if (instrumentNum != -1)
		instrNumStr = String.valueOf(instrumentNum);
	else
	{
		//ERROR
		LOG.debug("NTM_INSTR_NUM_POOL_EXHAUSTED error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_INSTR_NUM_POOL_EXHAUSTED);
		return null;
	}
	}
	else{
		instrumentNum = 0;
		instrNumStr = manualInstId;
	}
	// When getting the default template, use the logical instrument
	// type.  (Therefore, a Detailed SLC is logically a Guarantee)
	logicalInstType = InstrumentServices.getLogicalInstrumentType(instrumentType);
	LOG.debug("Getting default template for instrument type {}" , instrumentType);
	LOG.debug("     which is logically a {}" , logicalInstType);

	debug("Creating a new Instrument");
	//if the we are copying from a default template we must first get the oid of the default template from client bank
	if (copyFrom.equals(TradePortalConstants.FROM_BLANK))
	{
		LOG.debug("Copying from a default template with instrumentType -> {}" , logicalInstType);
		if (logicalInstType.equals(InstrumentType.EXPORT_DLC))
		{
			//ERROR
			LOG.debug("NTM_CREATE_EXPORT_DLC error");
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                                  TradePortalConstants.NTM_CREATE_EXPORT_DLC);
			return null;
		}

		//templateOid will be used in the next if block to get the template instrument's oid
		templateOid = clientBank.getDefaultTemplateOid(logicalInstType);

		if (StringFunction.isBlank(templateOid))
		{
			//ERROR
			LOG.debug("No default template exists. Corporate customer cannot create instrument");
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	    	                                              TradePortalConstants.NTM_NO_DEFAULT_TEMPLATE_CORP,
		                                                  logicalInstType);
		    return null;
		}
	}

	//here we instatiate the template to get the oid of the template instument
	String tempConfInd = null;
	if (copyFrom.equals(TradePortalConstants.FROM_TEMPL) ||
		copyFrom.equals(TradePortalConstants.FROM_BLANK))
	{
		LOG.debug("Copying from a non-default template with oid -> {}" , templateOid);
		Template temp = (Template)createServerEJB("Template");
		temp.getData(Long.parseLong(templateOid));
		sourceOid = temp.getAttribute("c_TemplateInstrument");
		LOG.debug("Got instrument_oid from template -> {}" , sourceOid);
		fromExpress = temp.getAttribute("express_flag");


		tempConfInd = temp.getAttribute("confidential_indicator");
		fixedFlag = temp.getAttribute("fixed_flag");

	}

    //Instantiate the source instrument
	long oid = Long.parseLong(sourceOid);
	LOG.debug("Getting source object with oid -> '{}'" , oid);
	sourceInst = (Instrument) createServerEJB("Instrument");
	try
	{
		sourceInst.getData(oid);
	}
	catch (InvalidObjectIdentifierException e) //The source oid was invalid
	{
		//ERROR
		LOG.debug("NTM_INVALID_INSTR_OID error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                 TradePortalConstants.NTM_INVALID_INSTR_OID);
		return null;
	}


    if (copyFrom.equals(TradePortalConstants.TRANSFER_ELC))
	{
	    LOG.debug("Transfering an Export LC");
    	instrumentType = InstrumentType.EXPORT_DLC;
	   	physicalInstType = instrumentType;
    }
    else
    {
	    // Use the instrument type passed to the method and get the physical type
	    // for the new instrument (e.g., Detailed SLC is actually an SLC).  If the
	    // type is blank, use whatever instrument type is on the source instrument .
		physicalInstType = InstrumentServices.getPhysicalInstrumentType(instrumentType);
		if (StringFunction.isBlank(physicalInstType)) {
			physicalInstType = sourceInst.getAttribute("instrument_type_code");
			instrumentType = physicalInstType;
		}
		LOG.debug("The instrument type for the new {}" , instrumentType);
		LOG.debug("     is actually a {}" , physicalInstType);

		//verify that we are not copying from an export lc, because that is against the business rules
		if (logicalInstType.equals(InstrumentType.EXPORT_DLC))
		{
			//ERROR
			LOG.debug("NTM_CREATE_EXPORT_DLC error");
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NTM_CREATE_EXPORT_DLC);
			return null;
		}
	}

	//Validate the instument Type and get the prefix and suffix
	LOG.debug("Attempting to validate user can create instrument type of ' {} '" ,physicalInstType);
	//make sure that this instrument type can be created by the portal
	if (InstrumentServices.canCreateInstrumentType(physicalInstType))
	{
		//make sure the user is authorized to create the instrument type
		if (SecurityAccess.canCreateModInstrument(doc.getAttribute("/securityRights"),
							physicalInstType, "") && InstrumentServices.isNull(manualInstId))
		//we don't just call canCreateModInstrument() to do both the last two validations because we
		//want to issue different errors depending on which fails
		{

			prefix = opBankOrg.getPrefix(physicalInstType);
			suffix = opBankOrg.getSuffix(physicalInstType);


		}
		else if (!InstrumentServices.isNull(manualInstId) && SecurityAccess.canCreateModConvertInstrument(doc.getAttribute("/securityRights"),
							physicalInstType, ""))
		//we don't just call canCreateModInstrument() to do both the last two validations because we
		//want to issue different errors depending on which fails
		{

			prefix = opBankOrg.getPrefix(physicalInstType);
			suffix = opBankOrg.getSuffix(physicalInstType);


		}
		else
		{
			//ERROR
			LOG.debug("ERROR - User not authorized to create instruments");
		    getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                        TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
		                                        physicalInstType,
							InstrumentServices.getOriginalTransType(instrumentType));
			return null;
		}

	}
	else
	{
		//ERROR
		LOG.debug("NTM_INVALID_INSTR_TYPE error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_INVALID_INSTR_TYPE,
		                                              physicalInstType);
		return null;
	}



    if (copyFrom.trim().toUpperCase().equals("TEMPL")) {
    	

    	if (!corpOrg.isInstrumentTypeEnabled(physicalInstType)) {
		    //ERROR
		    LOG.debug("ERROR - User not authorized to create instruments");
		    getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                        TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
		                                        physicalInstType,
							InstrumentServices.getOriginalTransType(instrumentType));
		   return null;
		}
	}


	//Create the new instrument
	this.newObject();

	//Validate that the complete instrument number is complete
	if (!this.instrumentNumberIsUnique(completeInstrId))
	{
		//ERROR
		LOG.debug("NTM_DUPLICATE_INSTR_NUM error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                              TradePortalConstants.NTM_DUPLICATE_INSTR_NUM);
		return null;
	}
	if(!InstrumentServices.isNull(manualInstId)){
		prefix="";
		suffix="";
	}
	//Set instrument Data
	this.setAttribute("instrument_status",TradePortalConstants.INSTR_STATUS_PENDING);
	this.setAttribute("template_flag",TradePortalConstants.INDICATOR_NO);
	this.setAttribute("op_bank_org_oid",doc.getAttribute("/bankBranch"));
	this.setAttribute("instrument_prefix",prefix);
	this.setAttribute("instrument_suffix",suffix);

	if(!InstrumentServices.isNull(manualInstId)){
	this.setAttribute("converted_transaction_ind",TradePortalConstants.INDICATOR_YES);
	this.setAttribute("instrument_num","");
	}else
	{
	this.setAttribute("converted_transaction_ind",TradePortalConstants.INDICATOR_NO);
	this.setAttribute("instrument_num",instrNumStr);
	}

	this.setAttribute("complete_instrument_id",prefix+instrNumStr+suffix);
	this.setAttribute("instrument_type_code", physicalInstType);
	this.setAttribute("corp_org_oid", corpOrgOid);
	this.setAttribute("client_bank_oid", clientBankOid);
	this.setAttribute("from_express_template", fromExpress);
	this.setAttribute("fixed_payment_flag",fixedFlag);
	this.setAttribute("template_oid", templateOid);//RKAZI 07-07-2015 T36000038018 - Add
	
	if (!copyFrom.equals(TradePortalConstants.TRANSFER_ELC))
	{
		// SJUG061954179 Do not copy copy_of_ref_num and copy_of_expiry_date
		// These two attributes are copied later, if they are registered on terms.
		//this.setAttribute("copy_of_ref_num", sourceInst.getAttribute("copy_of_ref_num"));
		//this.setAttribute("copy_of_expiry_date",sourceInst.getAttribute("copy_of_expiry_date"));
		this.setAttribute("language",sourceInst.getAttribute("language"));
	}

	// TLE - 11/10/06 - IR#ACUG110962196 - Add Begin
	// Copy import_indicator for loan request.
	if (instrumentType.equals(InstrumentType.LOAN_RQST)) {
		this.setAttribute("import_indicator",sourceInst.getAttribute("import_indicator"));
	}
	// TLE - 11/10/06 - IR#ACUG110962196 - Add End

	// W Zhu 12/6/06 FAUG120452533
	// copy vendor id
	this.setAttribute("vendor_id", sourceInst.getAttribute("vendor_id"));

	//Create the new transaction
	LOG.debug("Creating Transaction component");
	transactionOid = this.newComponent("TransactionList");
	Transaction newTrans = (Transaction) this.getComponentHandle("TransactionList", transactionOid);
	Transaction sourceTrans  = null;

	if (!copyFrom.equals(TradePortalConstants.TRANSFER_ELC))
	{
		//Get a handle to the transaction from which we will copy
		sourceTrans = (Transaction) sourceInst.getComponentHandle("TransactionList",
								sourceInst.getAttributeLong("original_transaction_oid"));
		// Copy the transaction.  Use the original instrumentType code (which could
		// be one of the fake codes (like DetailedSLC).  Transaction object knows
		// how to convert the type appropriately.
		newTrans.createNewCopy(sourceTrans, userOid, corpOrgOid, instrumentType, false);

        //IAZ CM-451 10/29/08 Begin: If this is Domestic Payment template, create
        //                       copies of all domestic payments associated with
        //                       the original template for the new template
		/* @Komal M PR ANZ Incident Report_Issue No.796 dated 13-02-2013*/
		//Komal M 15-02-2013 PR ANZ Incident Report_Issue No.803

	if (instrumentType.equals(InstrumentType.DOM_PMT) || (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION) && !instrumentOid.equals("")))
		{
			DomesticPayment domesticPayment = (DomesticPayment) this.createServerEJB("DomesticPayment");
	    	domesticPayment.createNewDomesticPayments( sourceTrans.getAttribute("transaction_oid"),
    								  newTrans.getAttribute("transaction_oid"), 
    								  TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("fixed_payment_flag")) );// Nar CR 966 Added flag for fixed template
		}
    	//IAZ CM-451 End
	}
	else
	{
	        newTrans.createNewBlank(TransactionType.TRANSFER, userOid, corpOrgOid, instrumentType, false, "",
	                                sourceInst.getAttribute("complete_instrument_id"));

		// Set the relationship to the related instrument on the Instrument bean
		// Note that this is also set (by createNewBlank) on Terms
		this.setAttribute("related_instrument_oid", sourceInst.getAttribute("instrument_oid"));

                // Copy the language of the export LC being transferred to the issue transfer transaction
                this.setAttribute("language", sourceInst.getAttribute("language"));

 	}



	if (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/uploaded_ind"))) {
		newTrans.setAttribute("uploaded_ind", TradePortalConstants.INDICATOR_YES);
	}else {
		newTrans.setAttribute("uploaded_ind", TradePortalConstants.INDICATOR_NO);
	}



	newTrans.setAttribute("gxs_ref", doc.getAttribute("/gxs_ref")); 
	newTrans.setAttribute("h2h_source", doc.getAttribute("/h2h_source")); // Nar CR694A Rel9.0

	if (instrumentType.equals(InstrumentType.DOM_PMT)	&& sourceTrans!=null){
		newTrans.setAttribute("customer_file_ref", sourceTrans.getAttribute("customer_file_ref"));
	}


	this.setAttribute("original_transaction_oid", newTrans.getAttribute("transaction_oid"));

        Terms newTerms = (Terms) newTrans.getComponentHandle("CustomerEnteredTerms");

        // Set the counterparty for the instrument
	this.setAttribute("a_counter_party_oid", newTerms.getAttribute("c_FirstTermsParty"));
        this.setAttribute("copy_of_instrument_amount", newTrans.getAttribute("instrument_amount"));

	if(newTerms.isAttributeRegistered("expiry_date"))
	 {
              this.setAttribute("copy_of_expiry_date", newTerms.getAttribute("expiry_date"));
         }

	if(newTerms.isAttributeRegistered("reference_number"))
	 {
              this.setAttribute("copy_of_ref_num", newTerms.getAttribute("reference_number"));
         }
	
	/*KMehta - 14th July 2015 - Rel9.4 IR-T36000038535 - Add  - Begin*/
		if(newTerms.isAttributeRegistered("work_item_number"))
		 {
				if(!(newTerms.getAttribute("work_item_number")).isEmpty()){
					newTerms.setAttribute("work_item_number", null);
					
					LOG.debug("work_item_numberset to null ");
				}
	        }
	//}
	/*KMehta - 14th July 2015 - Rel9.4 IR-T36000038535 - Add  - End*/
        String returnValue = newTrans.getAttribute("transaction_oid");

    //IAZ CR-586 IR-PRUK092452162 09/29/10 Add
    if (StringFunction.isNotBlank(tempConfInd))
    	this.setAttribute("confidential_indicator", tempConfInd);
    else
    	this.setAttribute("confidential_indicator", sourceInst.getAttribute("confidential_indicator"));
    LOG.debug("createinstr::set confidential indicator to {} {}" , this.getAttribute("confidential_indicator")
    															 , sourceInst.getAttribute("confidential_indicator"));

  //PMitnala IR T36000020561 - CR709 Rel 8.3  -Start
    if (!copyFrom.equals(TradePortalConstants.TRANSFER_ELC))
	{
	String loanProceedsCreditType = null;
	if(newTerms.isAttributeRegistered("loan_proceeds_credit_type")){
		loanProceedsCreditType = newTerms.getAttribute("loan_proceeds_credit_type");
	}
	int numberOfInvoicesAttached = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", "a_transaction_oid =? " , false, new Object[]{sourceTrans.getAttribute("transaction_oid")});
	boolean areInvoicesAttached = numberOfInvoicesAttached > 0;
	if (instrumentType.equals(InstrumentType.LOAN_RQST) && TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType))
	{
		if (!areInvoicesAttached)
		{
			createNewInvPaymentInstructions(sourceTrans.getAttribute("transaction_oid"), newTrans.getAttribute("transaction_oid"));
		}
	}
	}
  //PMitnala IR T36000020561 - CR709 Rel 8.3 -End
   
    //IR 22683 Begin- WHEN Outgoing SLC (Detailed) Instrument is created from the default template, use SLC-simple default template to get ICC applicable rule.
    if (TradePortalConstants.DETAILED_SLC.equals(instrumentType) && TradePortalConstants.FROM_BLANK.equals(copyFrom)){ 
        String slcInstrOid = "";
        DocumentHandler resultSet =null;
    	String ucpVer ="";
		String ucpInd ="";
		String ucpDetails ="";
        //templateOid will be of SCL (simple) default template
        templateOid = clientBank.getDefaultTemplateOid(InstrumentType.STANDBY_LC);
        if(StringFunction.isNotBlank(templateOid)){
    	Template template = (Template)createServerEJB("Template");
    	template.getData(Long.parseLong(templateOid));
    	slcInstrOid = template.getAttribute("c_TemplateInstrument");
        }
    	if (StringFunction.isNotBlank(slcInstrOid)){
		StringBuilder sql = new StringBuilder("select ucp_version,ucp_version_details_ind,ucp_details from transaction t, terms m where t.p_instrument_oid = ?");
		sql.append(" and t.c_cust_enter_terms_oid = m.terms_oid");
		resultSet = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new Object[]{slcInstrOid});
    	}
		if(resultSet!=null){
			ucpVer= resultSet.getAttribute("/ResultSetRecord/UCP_VERSION");
			ucpInd= resultSet.getAttribute("/ResultSetRecord/UCP_VERSION_DETAILS_IND");
			ucpDetails= resultSet.getAttribute("/ResultSetRecord/UCP_DETAILS");
		}
		newTerms.setAttribute("ucp_version", ucpVer);
		newTerms.setAttribute("ucp_version_details_ind", ucpInd);
		newTerms.setAttribute("ucp_details", ucpDetails);
    }
    // Nar CR-966 Rel 9.2 09/22/2014 Add - Begin
		if (InstrumentType.DOM_PMT.equals(instrumentType)) {
			if (TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("fixed_payment_flag")) && sourceTrans!=null) {
				newTerms.setAttribute("source_template_trans_oid", sourceTrans.getAttribute("transaction_oid"));
			} else {
				newTerms.setAttribute("source_template_trans_oid", "");
			}
		}
    // Nar CR-966 Rel 9.2 09/22/2014 Add - End
    //IR 22683 End
    if (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/ConvTransInd"))){
    	newTrans.setAttribute("converted_transaction_ind", TradePortalConstants.INDICATOR_YES);
    }
    if (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/conversionCenterMenuInd"))){
    	this.setAttribute("converted_transaction_ind", TradePortalConstants.INDICATOR_YES);
    }
    
    //RKAZI 07-07-2015 T36000038018 - START
    String srcTemplateOid = sourceInst.getAttribute("template_oid");
    if (TradePortalConstants.FROM_INSTR.equals(copyFrom)){
        
    	if (StringFunction.isNotBlank(srcTemplateOid)){
        	templateOid =  srcTemplateOid;
        }else{
        	templateOid = clientBank.getDefaultTemplateOid(instrumentType);
        }
    	this.setAttribute("template_oid", templateOid);
    }
    
    if (newTrans.getAttribute("transaction_type_code").equals(
						TransactionType.ISSUE)){
//    	copyBankDefinedFields(instrumentType, templateOid, newTerms, clientBank.getDefaultTemplateOid(instrumentType));	
    }
    
 //RKAZI 07-07-2015 T36000038018 - END
    
	if (performSave)
    {
	    LOG.debug("Saving instrument with oid ' {} ' ......" , this.getAttribute("instrument_oid"));
	    this.save(false);//save without validate
	    LOG.debug("Instrument saved");
	}

	return returnValue;
  }


/**
   * String createTransOnExisting (char[] chars)
   *
   * This method creats a new transaction on and existing instrument.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /transactionType -------- The type of the transaction being created
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *
   * @param  doc - this is a DocumentHandler stored as a char array
   * @return String - the oid of the transaction created from that you can get the instrument and terms
   * @throws java.rmi.RemoteException
   * @throws com.amsinc.ecsg.frame.AmsException
   */

  public String createTransOnExisting (DocumentHandler doc) throws RemoteException, AmsException
  {
	LOG.debug("entering createTransOnExisting()");
	long longSourceOid = -1;
	String instrumentOid ;
	
	try{
		//This is to identify if the instrumentOid is encrypted or not.
		longSourceOid = Long.parseLong(InstrumentServices.parseForOid(doc.getAttribute("/instrumentOid")));
		instrumentOid = InstrumentServices.parseForOid(doc.getAttribute("/instrumentOid"));
	}catch(NumberFormatException ne){
		// W Zhu 8/17/2012 Rel 8.1 T36000004579 add key parameter.
		String secretKeyString = csdb.getCSDBValue("SecretKey");
		byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
		javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
		instrumentOid = InstrumentServices.parseForOid(EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/instrumentOid"), secretKey));
	}
	String transType      = doc.getAttribute("/transactionType");
	String userOid        = doc.getAttribute("/userOid");
	String ownerOrg       = doc.getAttribute("/ownerOrg"); //the oid of the user's owner org.
	String instrumentType ;

	long transactionOid ;

	//Added for Descrepancy Response - START
	String messageOid     = doc.getAttribute("/mailMessageOid");
	if (messageOid == null)
		messageOid = "";
	//Added for Descrepancy Response - END

	/*****************************
	 * Get/Create the Instrument
	 *****************************/
	LOG.debug("Getting an existing instrument with oid -> {}" ,instrumentOid);
	try //to get the instrument data
	{
		this.getData(Long.parseLong(instrumentOid));
	}
	catch (InvalidObjectIdentifierException e) //The source oid was invalid
	{
		//ERROR
		LOG.debug("NTM_INVALID_EXIST_INSTR_ID error");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
													  TradePortalConstants.NTM_INVALID_EXIST_INSTR_ID);
		return null;
	}

	instrumentType = this.getAttribute("instrument_type_code");
	LOG.debug("instrumentType -> ' {} '" ,instrumentType);
	String converTranInd = this.getAttribute("converted_transaction_ind");
	LOG.debug("converTranInd: {}",converTranInd);
	if (!InstrumentServices.canCreateInstrumentType(instrumentType))
	{
		//ERROR
		LOG.debug("ERROR - NTM_INVALID_EXIST_INSTR_TYPE ==> {}" , instrumentType);
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_INVALID_EXIST_INSTR_TYPE, instrumentType);
		return null;
	}
	if (!this.isTransactionTypeValid(transType))
	{
		//ERROR
		LOG.debug("NTM_INVALID_TRANS_TYPE error {}" , transType);
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_INVALID_TRANS_TYPE,
									 transType, instrumentType);
		return null;
	}
	//Added for Descrepancy Response - Start
	String rights = doc.getAttribute("/securityRights");
	//Krishna IR RIUH101733973 10/29/2007
	//Inserted For ATP Response '||transType.equals(TradePortalConstants.APPROVAL_TO_PAY_RESPONSE)'
	if ((transType.equals(TransactionType.DISCREPANCY)||transType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE)) && !SecurityAccess.hasRights(rights,
		SecurityAccess.DISCREPANCY_CREATE_MODIFY))	{
		//ERROR
		LOG.debug("ERROR - User not authorized to create instruments");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
									 instrumentType, transType);
		return null;
	}
	//Added for Descrepancy Response - End
	else if (TradePortalConstants.INDICATOR_NO.equals(converTranInd) && !SecurityAccess.canCreateModInstrument(rights, instrumentType, transType))
	{
		//ERROR
		LOG.debug("ERROR - User not authorized to create instruments");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
									 instrumentType, transType);
		return null;
	}
	else if (TradePortalConstants.INDICATOR_YES.equals(converTranInd) && !SecurityAccess.canCreateModConvertInstrument(rights, instrumentType, transType))
	{
		//ERROR
		LOG.debug("ERROR - User not authorized to create instruments");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
									 instrumentType, transType);
		return null;
	}
        // Issue one error message if the status is Pending, a different one for all other bad statuses
        if (this.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_PENDING))
        {
                //ERROR
                LOG.debug("NTM_INSTR_PENDING error");
                getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                                         TradePortalConstants.NTM_INSTR_PENDING);
                return null;
        }
        if (this.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_DELETED) ||
            this.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_CANCELLED) )
        {
                //ERROR
                LOG.debug("NTM_INSTR_NOT_ACTIVE error");
                getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                                         TradePortalConstants.NTM_INSTR_BAD_STATUS);
                return null;
        }
	if (this.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_DEACTIVATED))
	{
	   	//ERROR
                LOG.debug("NTM_INSTR_DEACTIVATED error");
                getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                                         TradePortalConstants.NTM_INSTR_DEACTIVATED);
                return null;
	}
	if (this.pendingTransactionsExist()&&!(instrumentType.equals(InstrumentType.IMPORT_DLC)))
	{
		//ERROR
		LOG.debug("NTM_OTHER_TRANS_PENDING warning");
		getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									 TradePortalConstants.NTM_OTHER_TRANS_PENDING);

	}

	try //to lock the instrument
	{
		LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
										  Long.parseLong(userOid), true);
	}
	catch (InstrumentLockException e)
	{
		long lockingUserOid = e.getUserOid();
		if (!userOid.equals(String.valueOf(lockingUserOid)))
		{
			//ERROR
			LOG.debug("NTM_INSTRUMENT_LOCKED error");
			User lockUser = (User) createServerEJB("User");
			LOG.debug("Instrument locked by user -> {}" , lockUser.getAttribute("user_oid"));
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										 TradePortalConstants.NTM_INSTRUMENT_LOCKED, instrumentType,
										 transType, lockUser.getAttribute("first_name"),
										 lockUser.getAttribute("last_name"));
			return null;
		}
	}

	//Create the new transaction
	LOG.debug("Creating Transaction component");
	transactionOid = this.newComponent("TransactionList");
	Transaction newTrans = (Transaction) this.getComponentHandle("TransactionList", transactionOid);

	newTrans.createNewBlank(transType, userOid, ownerOrg, instrumentType, false, messageOid, "");

        String returnValue = newTrans.getAttribute("transaction_oid");
    if (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/ConvTransInd"))){
    	newTrans.setAttribute("converted_transaction_ind", TradePortalConstants.INDICATOR_YES);
    }
    // CR-818
    if ( TransactionType.SIM.equals(transType)  || TransactionType.SIR.equals(transType) ) {
    	setSettlmentWorkItemType(newTrans, transType, instrumentType, doc.getAttribute("/settleInstrWorkItemType"));
    	
    }
	this.save(false);

	return returnValue;
  }

/**
	* This method returns an amount from a foreign currency to
	* the instrument's base currency.  If the foreign exchange rate
	* is not found for the instrument's org, it will return a -1.0
	* which indicates an error.
	* Note: only the absolute value is returned.  If the amount is
	* provided is negative, the amount returned is positive.
	*
	* @param currency  java.lang.String - the foreign currency
	* @param amount    java.lang.String - the amount to convert to base currency
     * @param baseCurrency
     * @param userOrgOid
     * @param rateType
     * @return 
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws java.rmi.RemoteException
     
 */

  //IAZ E

  //IAZ ER 5.2.1.3 06/29/10 Begin
  //Update main method to be able to accept rate entered by the user and to return (in a vector)
  //a calc method used to calculate eqv amount
  //Maintain original method as well.
   public BigDecimal getAmountInBaseCurrency(String currency, String amount,
   			String baseCurrency, String userOrgOid, String rateType)
			throws AmsException, RemoteException {

		return getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, rateType, null, null, false); //MDB MRUL122933633 Rel7.1 1/3/12
	}

   public BigDecimal getAmountInBaseCurrency(String currency, String amount,
   			String baseCurrency, String userOrgOid, String rateType, BigDecimal userEnteredRate, Vector outMDInd)
			throws AmsException, RemoteException {

		return getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, rateType, userEnteredRate, outMDInd, false); //MDB MRUL122933633 Rel7.1 1/3/12
	}

   //MDB MRUL122933633 Rel7.1 1/3/12 - Begin
   public BigDecimal getAmountInBaseCurrency(String currency, String amount,
   			String baseCurrency, String userOrgOid, String rateType, boolean noScaleFlag)
			throws AmsException, RemoteException {

		return getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, rateType, null, null, noScaleFlag); //MDB
	}
	//MDB MRUL122933633 Rel7.1 1/3/12 - End

   public BigDecimal getAmountInBaseCurrency(String currency, String amount,
   			String baseCurrency, String userOrgOid, String rateType, BigDecimal userEnteredRate, Vector outMDInd, boolean noScaleFlag) //MDB MRUL122933633 Rel7.1 1/3/12
			throws AmsException, RemoteException {

 		BigDecimal amountInForeign = (new BigDecimal(amount)).abs();
		BigDecimal amountInBase = new BigDecimal(-1.0f);
		DocumentHandler resultSet ;
		DocumentHandler fxRate ;
		String multiplyIndicator = null;
		BigDecimal rate = null;
		BigDecimal use_rate = null;

		// Get the foreign exchange rate for the given userOrgOid
		// and foreign currency code

		if (baseCurrency.equals(currency)){
			amountInBase = amountInForeign;
		} else {
			// get the foreign exchange rate.  If it doesn't exist,
			// issue error.  Otherwise, get the multiply indicator
			// and rate and convert the amount

			// CRhodes - 1/16/2009 Modifications to use FXRate cache.
			Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
			resultSet = (DocumentHandler)fxCache.get(userOrgOid + "|" + currency);
		
			 //if not found in corporate or bank level throw error
			if (resultSet == null) {
		   	this.getErrorManager().issueError(getClass().getName(),
		   			TradePortalConstants.NO_FOREX_DEFINED, currency); 
		    } else {
		   	fxRate = resultSet.getFragment("/ResultSetRecord");
		   	if (fxRate == null) {
		   		this.getErrorManager().issueError(getClass().getName(),
		   				TradePortalConstants.NO_FOREX_DEFINED, currency); 

				} else {
				  multiplyIndicator = fxRate.getAttribute("/MULTIPLY_INDICATOR");


				  //Only get date from table if not entered by user
				  if (userEnteredRate == null)
				  {
					rate = fxRate.getAttributeDecimal("/RATE");


					// Use SELL_RATE for Transfer between Accounts Transactions
					if (InstrumentType.XFER_BET_ACCTS.equals(rateType) || TradePortalConstants.USE_SELL_RATE.equals(rateType)) {
						if (StringFunction.isBlank(fxRate.getAttribute("/SELL_RATE"))) {
							this.getErrorManager().issueError(getClass().getName(),
									TradePortalConstants.NO_SELL_FOREX_DEFINED);
						}
						try {
							use_rate = fxRate.getAttributeDecimal("/SELL_RATE");
						} catch (Exception any_e) {
							this.getErrorManager().issueError(getClass().getName(),
									TradePortalConstants.NO_SELL_FOREX_DEFINED);
						}
						rate = use_rate;
					}

					// Use BUY_RATE for DP, IP and explicit requests
					if (InstrumentType.FUNDS_XFER.equals(rateType) ||
						InstrumentType.DOM_PMT.equals(rateType) ||
						TradePortalConstants.USE_BUY_RATE.equals(rateType)) {
						if (StringFunction.isBlank(fxRate.getAttribute("/BUY_RATE"))) {
							this.getErrorManager().issueError(getClass().getName(),
									TradePortalConstants.NO_BUY_FOREX_DEFINED);
						} else {
						try {
							use_rate = fxRate.getAttributeDecimal("/BUY_RATE");
						} catch (Exception any_e) {
							this.getErrorManager().issueError(getClass().getName(),
									TradePortalConstants.NO_BUY_FOREX_DEFINED);
						}
					}
						rate = use_rate;
					}
			      }
			      else
			      	rate = userEnteredRate;


				  LOG.debug("rate used was: {} for type: {}" , rate ,rateType);

				}
		   }


		//Send back Calc Method if requested by the callign routine
		   if (multiplyIndicator != null && rate != null) {
			   int dec = TPCurrencyUtility.getDecimalPrecision(baseCurrency);
		   	 if (multiplyIndicator.equals(TradePortalConstants.DIVIDE)){

				 if (noScaleFlag)
					amountInBase = amountInForeign.divide(rate, java.math.MathContext.DECIMAL128);

				 else
		   			amountInBase = amountInForeign.divide(rate, dec, BigDecimal.ROUND_HALF_UP);
	   			if (outMDInd != null)
	   				outMDInd.add(0, TradePortalConstants.DIVIDE);
	   		 } else {
	   			amountInBase = amountInForeign.multiply(rate); 
	   			if (outMDInd != null)
	   				outMDInd.add(0, TradePortalConstants.MULTIPLY);
	   		 }
		   }

		}

	   LOG.debug("Original amount: {} and amount in base: {}" , amount ,amountInBase.toString());
		return amountInBase;

   }

   protected BigDecimal getAmountAuthorized(String userOid, String timeZone,
	String baseCurrency, String userOrgOid, String instrumentType, String transactionType)
	throws AmsException, RemoteException
   {
		return getAmountAuthorized(userOid, timeZone, baseCurrency, userOrgOid, instrumentType, transactionType, null);
   }

   private BigDecimal getAmountAuthorized(String userOid, String timeZone,
	String baseCurrency, String userOrgOid, String instrumentType, String transactionType, String rateType)
	throws AmsException, RemoteException
   {
 /**
	* This method returns the total transactions amount (in base currency)
	* of transactionType and instrumentType approved by the user
	* today.  "Today" is determined by the user's time zone.
	*
	* @param userOid  java.lang.String - the user's oid
	* @param timeZone java.lang.String - the user's time zone
	* @param instrumentType java.lang.String - the instrument type to search for
	* @param transactionType java.lang.String - the transaction type to search for
	* @return double - total transaction amount approved by the user
	* in that day
   */

	LOG.debug("*** getAmountAuthorized: base Currency / userOrgOid {}/ {} " , baseCurrency , userOrgOid);
	// find out user's date (without the time)
	// and convert the date to GMT date and time to determine the start of
	// the user's day in GMT time.

	DocumentHandler resultSet;
	List<DocumentHandler> transactionList = null;
	BigDecimal amountAuthorized ;
	BigDecimal amountToAdd ;
	String transactionAmount ;
	String transactionCurrency ;


	Date gmtDateTime = TPDateTimeUtility.getStartOfLocalDayInGMT(timeZone);
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	String dateToCompare = formatter.format(gmtDateTime);
	LOG.debug("getAmountAuthorized: timezone: {} ; GMT Date: {} ;  Date to Compare: {}",
			new Object[]{timeZone,DateTimeUtility.getGMTDateTime(),dateToCompare });

	// note: the first authorizing user and second authorizing user cannot be the
	// same, hence there is no danger of selecting a row twice.

	List<Object> sqlParams = new ArrayList<Object>();
	String whereClause =
	    "where ((a_first_authorizing_user_oid = ?" + 
	    " and first_authorize_status_date " +
	    ">= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')) or " +
	    "(a_second_authorizing_user_oid = ?" + 
			" and second_authorize_status_date " +
	    ">= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')))" +
	    " and transaction_type_code = ?   and instrument_type_code = ?" +
	    " and p_instrument_oid = instrument_oid" +
		" and c_cust_enter_terms_oid = terms.terms_oid";
	String sql = "select distinct transaction_oid, " +
	    " copy_of_currency_code, copy_of_amount, instrument_type_code, " +
		"terms.loan_proceeds_pmt_amount, terms.loan_proceeds_pmt_curr from " +
		"transaction, instrument, terms " +
	    whereClause;
	sqlParams.add(userOid);
	sqlParams.add(dateToCompare);
	sqlParams.add(userOid);
	sqlParams.add(dateToCompare);
	sqlParams.add(transactionType);
	sqlParams.add(instrumentType);

		amountAuthorized = BigDecimal.ZERO;

	resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
	if (resultSet != null) {
	
		transactionList = resultSet.getFragmentsList("/ResultSetRecord");

		if(transactionList!=null){
			// get the amounts in base currency of the user
		for (DocumentHandler transactionData:transactionList) {
	    transactionCurrency = transactionData.getAttribute("/COPY_OF_CURRENCY_CODE");
		transactionAmount = transactionData.getAttribute("/COPY_OF_AMOUNT");

		// Handle case for blank Loan Request Amount (Get loan proceeds payment amount and currency instead.
		if ((transactionData.getAttribute("/INSTRUMENT_TYPE_CODE").equals(InstrumentType.LOAN_RQST)) && (transactionAmount.equals(""))) {
			transactionCurrency = transactionData.getAttribute("/LOAN_PROCEEDS_PMT_CURR");
			transactionAmount = transactionData.getAttribute("/LOAN_PROCEEDS_PMT_AMOUNT");
		}

		// Handle any amendments with blank amounts (Treat them as 0)
	    if (!transactionAmount.equals("")) {
		
	    	if (StringFunction.isBlank(rateType))
			{
    			rateType = instrumentType;
			}

	    		amountToAdd = getAmountInBaseCurrency(transactionCurrency,
	    				transactionAmount, baseCurrency, userOrgOid, rateType);

	    	
		} else {
			amountToAdd = BigDecimal.ZERO;
		}

	    if (amountToAdd.compareTo(BigDecimal.ZERO) < 0)
	    {
	    	amountAuthorized = new BigDecimal(-1.0f);
		break;
	    }
	    else
	    {
	        amountAuthorized = amountAuthorized.add(amountToAdd);
	    }
	  }
	 }
	}


	LOG.debug("InstrumentBean getAmountAuthorized: {}" , amountAuthorized);

	return amountAuthorized;

   }

/**
	* This method updates the show_on_notification_tab field to N for the
	* instrument.  It verifies the user has the rights to delete notifications.
	*
     * @param transactionOid
	* @param securityRights java.lang.String - the user's security rights
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	*/
public void deleteNotification(String transactionOid, String securityRights) throws RemoteException, AmsException {

	LOG.debug("InstrumentBean deleteNotification: Deleting notification for instrument: {} and transaction: {} "
			,getAttribute("complete_instrument_id") ,transactionOid);

	Transaction transaction = (Transaction)
	    getComponentHandle("TransactionList", Long.parseLong(transactionOid));

    // If the user has delete notification rights, update the notification flag

	if (SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_NOTIFICATION)) {

		transaction.setAttribute("show_on_notifications_tab", TradePortalConstants.INDICATOR_NO);
		this.save();

		// Issue successful delete message
		this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.NOTIFICATION_PROCESSED,
						new String[] {
							getAttribute("complete_instrument_id"),
							getResourceManager().getText(
								"TransactionAction.Deleted",
								TradePortalConstants.TEXT_BUNDLE)});

		
	} else {
		// issue error that user does not have delete notification authority
		// for the given instrument
		this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.NO_ACTION_NOTIFICATION_AUTH,
						new String[] {
							getAttribute("complete_instrument_id"),
							getResourceManager().getText("TransactionAction.DeleteNotifications",
														TradePortalConstants.TEXT_BUNDLE)});

		
	}
}

/**
* This method updates the show_on_notification_tab field to N for the
* instrument.  It verifies the user has the rights to delete notifications.
*
     * @param transactionOid
* @param securityRights java.lang.String - the user's security rights
     * @param instrumentList
     * @param isDeleteNotification
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
*/
public void deleteAllNotification(String transactionOid, String securityRights,List<String> instrumentList,boolean isDeleteNotification) throws RemoteException, AmsException {

LOG.debug("InstrumentBean deleteAllNotification: Deleting notification for instrument: {}  and transaction {}"
		, getAttribute("complete_instrument_id") , transactionOid);

Transaction transaction = (Transaction)
    getComponentHandle("TransactionList", Long.parseLong(transactionOid));

// If the user has delete notification rights, update the notification flag

if (SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_NOTIFICATION)) {

	transaction.setAttribute("show_on_notifications_tab", TradePortalConstants.INDICATOR_NO);
	this.save();

	// Issue successful delete message
	if (!instrumentList.isEmpty() && isDeleteNotification == true){
		if (instrumentList.size()>10){
			StringBuilder messageAppend =new StringBuilder();
			for ( int i=0; i<10; i++ ) {
			    String theId = instrumentList.get(i);
			    messageAppend.append(theId);
			    if (i<10-1) {
			        messageAppend.append(", ");
			    }
			}
			messageAppend.append(" ...and (");
			messageAppend.append(instrumentList.size()-10).append(" additional)");
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.NOTIFICATION_PROCESSED_ALL,
					messageAppend.toString(),
						getResourceManager().getText(
							"TransactionAction.Deleted",
							TradePortalConstants.TEXT_BUNDLE));
		}else{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.NOTIFICATION_PROCESSED_MULTIPLE,
					instrumentList.toString().substring(1, instrumentList.toString().length()-1),
						getResourceManager().getText(
							"TransactionAction.Deleted",
							TradePortalConstants.TEXT_BUNDLE));
		}
	}

	
} else {
	// issue error that user does not have delete notification authority
	// for the given instrument
	this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.NO_ACTION_NOTIFICATION_AUTH,
					new String[] {
						getAttribute("complete_instrument_id"),
						getResourceManager().getText("TransactionAction.DeleteNotifications",
													TradePortalConstants.TEXT_BUNDLE)});

	
}
}

   /* createLCFromPOData() */
   public DocumentHandler createLCFromPOData(DocumentHandler inputDoc, Vector poOids, Vector poNums, String lcRuleName) throws RemoteException, AmsException
    {
	LOG.debug("Entered createLCFromPOData(). inputDoc follows ->\n {}" , inputDoc.toString());

	DocumentHandler outputDoc = new DocumentHandler();


	String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the po line items
	String currency             = inputDoc.getAttribute("/currency"); //the currency of the po line items
	String newTransactionOid    ; //the oid of the transaction created for these po line items
	String partyOid            ; //the oid of the beneficiary

	// <Amit - IR EEUE032659500 -05/15/2005 >
	String userLocale              = inputDoc.getAttribute("/locale"); //the locale of the user
	// </Amit - IR EEUE032659500 -05/15/2005 >

	//Create a new instrument
	newTransactionOid = this.createNew(inputDoc, false);
	LOG.debug("New instrument created with an original transacion oid of -> {}" , newTransactionOid);

	if (newTransactionOid == null)
	{
		LOG.debug("newTransactionOid is null.  Error in this.createNew()");
		return outputDoc; //outputDoc is null
	}
	else
	    outputDoc.setAttribute("/transactionOid", newTransactionOid);

	LOG.debug("ben_name     = {}" , benName);
	LOG.debug("currency     = {}" , currency);

	/************************************************************************
	 * Instantiate the transaction, terms, and termsParty and set the fields
	 * derived from the po line item data
	 ************************************************************************/

	//get a handle to the terms object through the transaction then set the values on terms
	//we'll also have to create a termsParty object and associate it to the terms object
	Transaction transaction = (Transaction) this.getComponentHandle("TransactionList",
	                                                                Long.parseLong(newTransactionOid));
	LOG.debug("Transaction handle acquired -> {}" , transaction.getAttribute("transaction_oid"));

	Terms terms = null;
	try
	{
        terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
	    LOG.debug("terms handle acquired -> {}" , terms.getAttribute("terms_oid"));
	}
	catch (RemoteException | AmsException e)
	{
	    LOG.error("Caught error getting handle to terms: ",e);
	}

	long oid = terms.newComponent("FirstTermsParty");
	TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
	LOG.debug("termsParty handle acquired -> {}" , termsParty.getAttribute("terms_party_oid"));

        // Either use the first shipment that exists or create one
        ShipmentTerms shipmentTerms = terms.getFirstShipment();
        if(shipmentTerms == null)
          {
             shipmentTerms = terms.createFirstShipment();
          }

        // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
        // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
        // Note all the PO Line Items have been saved into database by now.
		// <Amit - IR EEUE032659500 -05/15/2005 >
       
		POLineItemUtility.deriveTransactionDataFromPO(false,transaction, shipmentTerms, 1, poOids, 0, getSessionContext(),userLocale);
		// </Amit - IR EEUE032659500 -05/15/2005 >

        outputDoc.setAttribute("/shipmentOid", shipmentTerms.getAttribute("shipment_oid"));

	//check if the benificiary name for the po line items matches the name
	//of a party owned by user's coproration
        String sql = "select party_oid from party where name = ? and p_owner_org_oid = ? ";
    	

    	DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, benName, inputDoc.getAttribute("/ownerOrg"));

	if (resultSet == null) //the party was not found
	{
	    LOG.debug("No party match found for ben_name." +
	                           " Setting termsParty name to -> {}" , benName);
	    termsParty.setAttribute("name", benName);
	}

	else //the party was found, copy data to TermsParty
	//copy the Party info to the new TermsParty
	{
	    LOG.debug("resultSet is --> {}" , resultSet.toString());
	    partyOid = resultSet.getAttribute("/ResultSetRecord(0)/PARTY_OID");
	    LOG.debug("partyOid -> {}" , partyOid);

	    Party party = (Party) this.createServerEJB("Party");
	    party.getData(Long.parseLong(partyOid));

            termsParty.copyFromParty(party);

            // If the party has a designated bank, pull in its data as the advising bank
            if(!StringFunction.isBlank(party.getAttribute("designated_bank_oid")))
             {
                // Load the designated bank from the database
                Party desigBank = (Party) this.createServerEJB("Party");
	        desigBank.getData(Long.parseLong(party.getAttribute("designated_bank_oid")));

                TermsParty advisingBank;

                if(StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty")))
                 {
                      terms.newComponent("ThirdTermsParty");
                      advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
                 }
                else
                     advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

                advisingBank.copyFromParty(desigBank);
             }
	}



        // Set the counterparty for the instrument and some "copy of" fields
	this.setAttribute("a_counter_party_oid", terms.getAttribute("c_FirstTermsParty"));
	transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
        transaction.setAttribute("instrument_amount", terms.getAttribute("amount"));
        this.setAttribute("copy_of_instrument_amount", transaction.getAttribute("instrument_amount"));

	if(terms.isAttributeRegistered("expiry_date"))
	 {
              this.setAttribute("copy_of_expiry_date", terms.getAttribute("expiry_date"));
         }

	if(terms.isAttributeRegistered("reference_number"))
	 {
              this.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number"));
         }

    LOG.debug("Saving instrument with oid ' {}  '....." , this.getAttribute("instrument_oid"));
	this.save(false);//save without validate
	LOG.debug("Instrument saved");

	return outputDoc;
  }




    /* createLCFromPOData() */
    public DocumentHandler createLCFromStructuredPOData(DocumentHandler inputDoc, Vector poOids, Vector poNums, String lcRuleName) throws RemoteException, AmsException
    {
        LOG.debug("Entered createLCFromPOData(). inputDoc follows ->\n  {}" , inputDoc.toString());

        DocumentHandler outputDoc = new DocumentHandler();


        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the po line items
        String currency             = inputDoc.getAttribute("/currency"); //the currency of the po line items
        String newTransactionOid    ; //the oid of the transaction created for these po line items
        String partyOid           ; //the oid of the beneficiary

        // <Amit - IR EEUE032659500 -05/15/2005 >
        String userLocale              = inputDoc.getAttribute("/locale"); //the locale of the user
        // </Amit - IR EEUE032659500 -05/15/2005 >

        //Create a new instrument
        newTransactionOid = this.createNew(inputDoc, false);
        LOG.debug("New instrument created with an original transacion oid of -> {}" , newTransactionOid);

        if (newTransactionOid == null)
        {
            LOG.debug("newTransactionOid is null.  Error in this.createNew()");
            return outputDoc; //outputDoc is null
        }
        else
            outputDoc.setAttribute("/transactionOid", newTransactionOid);

        LOG.debug("ben_name     = {}" , benName);
        LOG.debug("currency     = {} " , currency);

        /************************************************************************
         * Instantiate the transaction, terms, and termsParty and set the fields
         * derived from the po line item data
         ************************************************************************/

        //get a handle to the terms object through the transaction then set the values on terms
        //we'll also have to create a termsParty object and associate it to the terms object
        Transaction transaction = (Transaction) this.getComponentHandle("TransactionList",
                Long.parseLong(newTransactionOid));
        LOG.debug("Transaction handle acquired -> {}" , transaction.getAttribute("transaction_oid"));

        Terms terms = null;
        try
        {
            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
            LOG.debug("terms handle acquired -> {}" , terms.getAttribute("terms_oid"));
        }
        catch (RemoteException | AmsException e)
        {
            LOG.error("Caught error getting handle to terms: ",e);
        }

        long oid = terms.newComponent("FirstTermsParty");
        TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
        LOG.debug("termsParty handle acquired -> {}" , termsParty.getAttribute("terms_party_oid"));

        // Either use the first shipment that exists or create one
        ShipmentTerms shipmentTerms = terms.getFirstShipment();
        if(shipmentTerms == null)
        {
            shipmentTerms = terms.createFirstShipment();
        }

        // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
        // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
        // Note all the PO Line Items have been saved into database by now.
  
        POLineItemUtility.deriveTranDataFromStrucPO(true, transaction,
				shipmentTerms, 1, poOids,
				0, getSessionContext(),null,
				userLocale,outputDoc,poNums); //TODO instrument
  
		// set default goods description text
		shipmentTerms.setAttribute("goods_description", TradePortalConstants.getPropertyValue("TextResources", "StructuredPurchaseOrders.defaultGoodsDescription", null));

        outputDoc.setAttribute("/shipmentOid", shipmentTerms.getAttribute("shipment_oid"));

        //check if the benificiary name for the po line items matches the name
        //of a party owned by user's coproration
        String sql = "select party_oid from party where name = ? and p_owner_org_oid = ? ";


        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, benName, inputDoc.getAttribute("/ownerOrg"));;

        if (resultSet == null) //the party was not found
        {
            LOG.debug("No party match found for ben_name." +
                    " Setting termsParty name to -> {}" , benName);
            termsParty.setAttribute("name", benName);
        }

        else //the party was found, copy data to TermsParty
        //copy the Party info to the new TermsParty
        {
            partyOid = resultSet.getAttribute("/ResultSetRecord(0)/PARTY_OID");
            LOG.debug("partyOid -> {}" , partyOid);

            Party party = (Party) this.createServerEJB("Party");
            party.getData(Long.parseLong(partyOid));

            termsParty.copyFromParty(party);

            // If the party has a designated bank, pull in its data as the advising bank
            if(!StringFunction.isBlank(party.getAttribute("designated_bank_oid")))
            {
                // Load the designated bank from the database
                Party desigBank = (Party) this.createServerEJB("Party");
                desigBank.getData(Long.parseLong(party.getAttribute("designated_bank_oid")));

                TermsParty advisingBank;

                if(StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty")))
                {
                    terms.newComponent("ThirdTermsParty");
                    advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
                }
                else
                    advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

                advisingBank.copyFromParty(desigBank);
            }
        }



        // Set the counterparty for the instrument and some "copy of" fields
        this.setAttribute("a_counter_party_oid", terms.getAttribute("c_FirstTermsParty"));
        transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
        transaction.setAttribute("instrument_amount", terms.getAttribute("amount"));
        this.setAttribute("copy_of_instrument_amount", transaction.getAttribute("instrument_amount"));

        if(terms.isAttributeRegistered("expiry_date"))
        {
            this.setAttribute("copy_of_expiry_date", terms.getAttribute("expiry_date"));
        }

        if(terms.isAttributeRegistered("reference_number"))
        {
            this.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number"));
        }

        LOG.debug("Saving instrument with oid ' {} '..." , this.getAttribute("instrument_oid"));
        this.save(false);//save without validate
        LOG.debug("Instrument saved");

        return outputDoc;
    }



/**
 * This method sets the status of the transaction object passed in.  Usually
 * it is set to AUTHORIZED.  However, there is a special case for TRACER
 * transactions which are sent for information purpose only.  In this case
 * there is no action required by OTL so nothing triggers a response back
 * to the Portal.  Therefore, we bypass the AUTHORIZE status and go
 * straight to PROCESSED BY BANK.
 *
 * @param transaction com.ams.tradeportal.busobj.Transaction
 */


   private void authorize(Transaction transaction, String userLocale, String userTimezone, boolean procceedAuth)
	throws RemoteException, AmsException {
	
	String type = transaction.getAttribute("transaction_type_code");

	if (type.equals(TransactionType.TRACER)) {
		String termsOid = transaction.getAttribute("c_CustomerEnteredTerms");
		String sql = "select tracer_send_type from terms where terms_oid = ? ";

		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{termsOid});


		String tracerSendType = resultSet.getAttribute("/ResultSetRecord/TRACER_SEND_TYPE");

		if (StringFunction.isNotBlank(tracerSendType)
			&& tracerSendType.equals(TradePortalConstants.NO_ACTION_REQUIRED)) {
			
		transaction.setAttribute("transaction_status",
					TransactionStatus.AUTHORIZED);
		/*KMehta - 18 Dec 2014 - Rel9.2 IR-T36000027099 - Change  - End*/
	    	transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
			return;
		}

	}


	String typeCode = transaction.getAttribute("copy_of_instr_type_code");
	if ((StringFunction.isNotBlank(typeCode)) && (typeCode.equals(InstrumentType.DOM_PMT)))
	{
		//Ravindra - Rel710 - IR VAUL030955566 - 9th Aug 2011 - Start
		if (TPDateTimeUtility.isFutureDate(transaction.getAttribute("payment_date"),userTimezone))
		//Ravindra - Rel710 - IR VAUL030955566 - 9th Aug 2011 - End
			transaction.setAttribute("transaction_status", TransactionStatus.FVD_AUTHORIZED);
		else
			if (procceedAuth)  
			transaction.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);

	}
	else
	{
		// Default is to make the status AUTHORIZED
		if (procceedAuth)   
		transaction.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);
	}

	transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());

	// Clean up rejection attributes
	if((transaction.getAttribute("rejection_indicator") != null)
			&& (transaction.getAttribute("rejection_indicator").trim().length() > 0))
	{
		// Set the rejection indicator to "A" symbolizing the transaction has been
		// authorized but was once rejected
		transaction.setAttribute("rejection_indicator", TradePortalConstants.INDICATOR_A);
	}
	if((transaction.getAttribute("rejection_reason_text") != null)
			&& (transaction.getAttribute("rejection_reason_text").trim().length() > 0))
	{
		transaction.setAttribute("rejection_reason_text", "");
	}

}



  /**
	* This method return prefix such as "frist" and "second" for corresponding
	* numeric index
	* @param authorizersIndex int inout integer form 1 to 6
	* @return String - correspoding prefix
   */
   private String getAuthorizerPrefixForIndex(int authorizersIndex)
   {
	    String fieldPrefix;

        switch (authorizersIndex)
        {
            case 1:  fieldPrefix = "first"; break;
            case 2:  fieldPrefix = "second"; break;
            case 3:  fieldPrefix = "third"; break;
            case 4:  fieldPrefix = "fourth"; break;
            case 5:  fieldPrefix = "fifth"; break;
            case 6:  fieldPrefix = "sixth"; break;
            default: fieldPrefix = "first"; break;
		}

		return fieldPrefix;
   }

 
 

   private boolean isCashManagementTypeInstrument(String instrumentType) {
	   return (instrumentType.equals(InstrumentType.DOM_PMT) ||
       	 instrumentType.equals(InstrumentType.FUNDS_XFER) ||
       	 instrumentType.equals(InstrumentType.XFER_BET_ACCTS));
       
   }

  

   // CJR - 7/6/2011 - REUL070142455 - Added skipUserAuthorizedAccountCheck parameter
  private String validateAuthorizedAccounts(User user, Transaction transaction, String userOrgOid, boolean skipUserAuthorizedAccountCheck)
   	throws AmsException, RemoteException
   {

  	//For subsidiary access, any and all child organization accounts are valid
	   	//Check whether corp org stored with the instrument is a subsiadiary of authorizing user:


	   	String instrumentACorpOrgOid = getAttribute("corp_org_oid");
	   	LOG.debug("validateAuthorizedAccounts:: corpOrgOid = {} and userOrgOid = {} " , instrumentACorpOrgOid , userOrgOid);

	   	if ((StringFunction.isNotBlank(instrumentACorpOrgOid))&&(!instrumentACorpOrgOid.equals(userOrgOid)))
	   	{
			CorporateOrganization instrumentCorpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
													  Long.parseLong(instrumentACorpOrgOid));

			String corpParentOrgOid = instrumentCorpOrg.getAttribute("parent_corp_org_oid");

	   		LOG.debug("validateAuthorizedAccounts:: corpOrgParentOid = {} and userOrgOid = {}" , corpParentOrgOid , userOrgOid);

			if (userOrgOid.equals(corpParentOrgOid))
			{
				return instrumentACorpOrgOid;

			}
		}

		DocumentHandler resultSet;

		Terms terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));


        if (StringFunction.isBlank(terms.getAttribute("debit_account_oid")))
        {
			if (InstrumentType.FUNDS_XFER.equals(this.getAttribute("instrument_type_code")))
				return userOrgOid;
			else
				return null;
		}


        // CJR - 7/6/2011 - REUL070142455 - Added skipUserAuthorizedAccountCheck parameter to ignore checking the user_authorized_acct table based on this flag.
        // 									This parameter is used for straight through processing.
        if(skipUserAuthorizedAccountCheck == false){

        	String sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ? and a_account_oid = ?";
			resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, user.getAttribute("user_oid"), terms.getAttributeLong("debit_account_oid"));

			if (resultSet == null)
			{
			    return null;
			}

	        if (InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code")))
	        {
				sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ? and a_account_oid = ?" ;

				resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, user.getAttribute("user_oid"), terms.getAttributeLong("credit_account_oid"));



				if (resultSet == null)
				{
				    return null;
				}
			}


	        if (InstrumentType.FUNDS_XFER.equals(getAttribute("instrument_type_code")))
	        {
				sql = "select authorized_account_oid from user_authorized_acct where p_user_oid = ? and a_account_oid = ?" ;

				resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, user.getAttribute("user_oid"), terms.getAttributeLong("payment_charges_account_oid"));

				if (resultSet == null)
				{
				    return null;
				}
			}

        }

		return userOrgOid;
   }


  private Account validateAccount(CorporateOrganization accountsCorpOrg, String testAccountOid)
    throws AmsException, RemoteException
  {

		Account aAccount = (Account) accountsCorpOrg.getComponentHandle("AccountList", Long.parseLong(testAccountOid));
		if (TradePortalConstants.INDICATOR_YES.equals(aAccount.getAttribute("deactivate_indicator")))
		{
			this.getErrorManager().issueError(getClass().getName(),
			   	TradePortalConstants.ACCOUNT_DEACTIVATED,
			    aAccount.getAttribute("account_number"));
			return null;
		}
		return aAccount;
  }


  private boolean updateCustomerDailyLimit(String userOrgOid, CorporateOrganization lockCorpOrg, Transaction transaction, String instrumentType, String custDailyLimit, BigDecimal amountInBase, int retryAttempt)
   	throws AmsException, RemoteException
   {

     // ***********************************************************************
	 // Now we check Customer's (Corp) Daily Limit (note: this is different from
	 // users's daily limits/thresholds or Customer's Available Amount
     // ***********************************************************************
        BigDecimal amountAuthorized;
		BigDecimal limit = new BigDecimal(custDailyLimit);
	    LOG.debug("InstrumentBean updateCustomerDailyLimitMethod:: Checking Cust Daily Limits of {}"
		    , limit);
		LOG.debug("InstrumentBean updateCustomerDailyLimitMethod:: Adding amount of {}" , amountInBase.toString());

	    java.util.Date tranDate = GMTUtility.getGMTDateTime();

        //IAZ CM-451 05/02/09 Begin - Use DL TimeZone to obtain current (transaction) date
        String dlTimeZone = lockCorpOrg.getAttribute("timezone_for_daily_limit");
        if (StringFunction.isNotBlank(dlTimeZone))
        	tranDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(), dlTimeZone);
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy 00:00:00");
        

	    java.util.Date corpDate;

	    // get the total transactions amount authorized by the user today
        // get current limit total
		String amountAuthorizedStr = lockCorpOrg.getAttribute("cust_daily_balance_amt_"+instrumentType.toLowerCase());
		if (StringFunction.isBlank(amountAuthorizedStr))
			amountAuthorizedStr = "0.00";

        // get the date
        try
        {
        	corpDate = lockCorpOrg.getAttributeDate("cust_daily_balance_date");
        	LOG.debug("corp date: {}" , corpDate);
		}
		catch (RemoteException | AmsException any_ex)
		{
			LOG.error("InstrumentBean updateCustomerDailyLimitMethod::Exception getting date " , any_ex);
			LOG.debug("InstrumentBean updateCustomerDailyLimitMethod::corpDate is not set. setting to todays date");
			// set new defaults for both balance and date
			corpDate = tranDate;

			lockCorpOrg.setAttribute("cust_daily_balance_date", formatter.format(tranDate));	
			amountAuthorizedStr = "0.00";
			zeroOutAllBalancesOnNewDay(lockCorpOrg);
		}

        //Rel 9.2 IR T36000036503 - START - If corpDate is null, set it to tranDate
        if(corpDate == null){
        	LOG.debug("InstrumentBean updateCustomerDailyLimitMethod::corpDate is null. setting to todays date");
			// set new defaults for both balance and date
        	corpDate = tranDate;
			lockCorpOrg.setAttribute("cust_daily_balance_date", formatter.format(tranDate));	
		}
        //Rel 9.2 IR T36000036503 - END
        
        // update balance and dete as needed
        // todays date > corpDate stored ==> update date and start balance a new
 		if (compareDate(tranDate, corpDate) == 1)
 		{
			zeroOutAllBalancesOnNewDay(lockCorpOrg);
			amountAuthorized = amountInBase;

			lockCorpOrg.setAttribute("cust_daily_balance_date", formatter.format(tranDate));	
		}
		// todays date = corpDate stored ==> add trans amount to the total, no need to chnage date
        else
        {
			amountAuthorized = new BigDecimal(amountAuthorizedStr);
			amountAuthorized = amountAuthorized.add(amountInBase);
		}
		lockCorpOrg.setAttribute("cust_daily_balance_amt_"+instrumentType.toLowerCase(), amountAuthorized.toString());

		String corpName = lockCorpOrg.getAttribute("name");

		LOG.debug("updateCustomerDailyLimit Cust Daily Limit Total Authorized: {}" , amountAuthorized);
	    if (amountAuthorized.compareTo(limit) > 0 )
	    {
			//this is just a warning
			this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.CUST_DAILY_LIMIT_EXCEEDED,
						corpName);

			transaction.setAttribute("daily_limit_exceeded_indicator",
						TradePortalConstants.INDICATOR_YES);
			LOG.debug("InstrumentBean::AuthorizeTransaction::daily limit indicator set to Y");

		}

		return true;
    }

	private int compareDate(java.util.Date curDate, java.util.Date compDate) {
		try
	 	{
            //compare dates and set status/warning/error accordingly
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd");
			String curDateStr = formatter.format(curDate);
			String compDateStr = formatter.format(compDate);
			LOG.debug("TransactionMediator::validatePaymentDate:: {}  vs. {}" , curDateStr ,compDateStr);

	      	if (curDateStr.compareTo(compDateStr) > 0)
	      	{
				return 1;
			}
			else if (curDateStr.compareTo(compDateStr) < 0)
			{
				return 2;
			}

			return 0;
	 	}
	 	catch (Exception exc)
	 	{
			LOG.error("Exception is caught in InstrumentBean::compareDate:: " ,exc);
			return -1;
		}
	}


   private void zeroOutAllBalancesOnNewDay(CorporateOrganization corpOrg)
   	throws AmsException, RemoteException
   {
		corpOrg.setAttribute("cust_daily_balance_amt_ftrq", "0.00");
		corpOrg.setAttribute("cust_daily_balance_amt_ftdp", "0.00");
		corpOrg.setAttribute("cust_daily_balance_amt_ftba", "0.00");

   }

   
  /**
	* This method return true if customer daily limit is enbaled for the type
	* of transaction being authorized.
	* @param String -- instrumentType of the transaction beign authorized
	* @param user com.ams.tradeportal.busobj.CorporateOrganization -- Authorizing User's corpOrg
	* @return boolean -- customer daily limit is enabled
   */
   private String checkCustDailyLimitEnforced(String instrumentType,
   										 CorporateOrganization corpOrg)
   	throws AmsException, RemoteException
   {

        String custDailyLimit = null;
    	String enforced = null;
    	String pretype = null;
    	if (instrumentType.equals(InstrumentType.DOM_PMT))
    	{
       		enforced = corpOrg.getAttribute("enforce_daily_limit_dmstc_pymt");
       		pretype = "domestic_pymt";
		}
    	if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
    	{
    	  	enforced = corpOrg.getAttribute("enforce_daily_limit_transfer");
    	  	pretype = "transfer";
		}
    	if (instrumentType.equals(InstrumentType.FUNDS_XFER))
    	{
  			enforced = corpOrg.getAttribute("enforce_daily_limit_intl_pymt");
    	  	pretype = "intl_pymt";
	    }

    	if ((enforced != null) &&
       		(enforced.equals(TradePortalConstants.INDICATOR_YES)))
		{
			custDailyLimit = corpOrg.getAttribute(pretype + "_daily_limit_amt");
			LOG.debug("Cust Daily Limit is set for this transaction {}" , custDailyLimit);
    	}
    	return custDailyLimit;
 	}
    /**
 	 * This method handles the authorization process for future dated payment transactions
	 *
	 * @param transaction  java.lang.String - transaction oid
	 * @return boolean - true : transaction is authorized
	 * 					false: transaction is not authorized
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
     */
	public boolean authorizeFVDTransaction(Transaction transaction) throws RemoteException, AmsException  
	{


		boolean isH2HStraightThroughInd = false;
        String instTypeCode = transaction.getAttribute("copy_of_instr_type_code"); 

		if (!validateAuthorizeUser(transaction.getAttribute("first_authorizing_user_oid")))
		{
   			updateAuthorizeFailed(transaction);
   			return false;  // not able to query account balance
		}

		if (StringFunction.isNotBlank(transaction.getAttribute("second_authorizing_user_oid")))
		{
			if (!validateAuthorizeUser(transaction.getAttribute("second_authorizing_user_oid")))
			{
	   			updateAuthorizeFailed(transaction);
	   			return false;  // not able to query account balance
			}
		}

		User user = (User) createServerEJB("User", Long.parseLong(transaction.getAttribute("assigned_to_user_oid")));
		String userOrgOid = user.getAttribute("owner_org_oid");

		LOG.debug("UserOrgOid:: {}" ,userOrgOid);
		//
		//Rel9.0 IR#T36000013347 - If this was H2H and is set up for STA
		//set indicator to skip account validation - [START]		
		CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(userOrgOid));
		
		if((transaction.isH2HSentPayment() && corpOrg.isStraightThroughAuthorize())	){
			isH2HStraightThroughInd=true;
		}
		
   		String accountsCorpOrgOid = validateAuthorizedAccounts(user, transaction, userOrgOid,isH2HStraightThroughInd); 
   	 	corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",  Long.parseLong(accountsCorpOrgOid));
	
	    String custDailyLimit = checkCustDailyLimitEnforced(instTypeCode, corpOrg); 

		LOG.debug("custDailyLimit:: {}" ,custDailyLimit);

	   	int retryAttempt = 1;
	   	while (retryAttempt <= TradePortalConstants.MAX_CORP_ORG_RETRY)
	   	{
	  		 LOG.debug("InstrumentBean::authorizeFVDTransaction::retry is {}" ,retryAttempt);

 			 Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");

	   		 try
	  		 {

	    			 if (StringFunction.isNotBlank(custDailyLimit))
    			 {
	   				BigDecimal amountInBase;

	  			    	amountInBase = getAmountInBaseCurrency(transaction.getAttribute("copy_of_currency_code"),
		 						  transaction.getAttribute("copy_of_amount"),
								  corpOrg.getAttribute("base_currency_code"),
								  userOrgOid,
								  instTypeCode);

	  			 //Maheswar CR-610 14/03/2011 Begin
	  		  	transaction.setAttribute("daily_limit_exceeded_indicator",	TradePortalConstants.INDICATOR_PASSED);
	  		  	//Maheswar CR-610 14/03/2011 End
    				updateCustomerDailyLimit(userOrgOid,
    										 corpOrg,
    										 transaction,
    										 instTypeCode, //MDB IR RUK121160973 12/13/10
    										 custDailyLimit,
    										 amountInBase,
    										 retryAttempt);
					terms.setAttribute("debit_pending_amount_in_base", amountInBase.toString());
    			 }
    			 if (StringFunction.isNotBlank(terms.getAttribute("debit_account_oid")))
    				
				 LOG.debug("InstrumentBean::authorizeFVDTransaction::setting transaction status to authorised");
				 transaction.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);

    			 if (StringFunction.isNotBlank(custDailyLimit) || StringFunction.isNotBlank(terms.getAttribute("debit_account_oid")))
    			 {
	 	  			if (retryAttempt < TradePortalConstants.MAX_CORP_ORG_RETRY)
	 	  				corpOrg.setRetryOptLock(true);
					else
						corpOrg.setRetryOptLock(false);
	 	  			corpOrg.touch();
	 	  			int saveOK = corpOrg.save(false);

					if (saveOK == -1)
					{
						if (retryAttempt == TradePortalConstants.MAX_CORP_ORG_RETRY)
						{
							LOG.debug("InstrumentBean::AuthorizeTransaction::Can't resolve Opt_Lock with retry: {}" , retryAttempt);
							this.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.CANNOT_UPDATE_CUST_DAILY_LIMIT);
							return false;
						}
					}
					else if (saveOK == -2)
					{
						LOG.debug("optLockretry");
						retryAttempt++;
						continue;
					}
				  }
    			  break;
    		 }
    		 catch (OptimisticLockException opt_lock_exc)
			 {
				 LOG.debug("InstrumentBean::AuthorizeFVDTransaction::Opt-lock error occured while updating customer, retry: {}" ,retryAttempt++);
				 if (retryAttempt > TradePortalConstants.MAX_CORP_ORG_RETRY)
				 {
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.CANNOT_UPDATE_CUST_DAILY_LIMIT);
					return false;
				 }
				 retryAttempt++;
				 
			 }
    		 catch (   RemoteException | AmsException e)
			 {
				 LOG.error("InstrumentBean::AuthorizeFVDTransaction::Exception: " , e);
				 this.getErrorManager().issueError(getClass().getName(),TradePortalConstants.TRANSACTION_CANNOT_PROCESS);
				 return false;
			 }
	 	}


		return true;
	}

    /**
	 * This method validates that users authorizing future dated transaction are still active users
	 *
	 * @param user  java.lang.String - the user's oid
	 * @return boolean - true : user is active
	 * 					false: user is not active
     */
	private boolean validateAuthorizeUser(String user) throws AmsException, RemoteException
	{
		DocumentHandler resultSet ;

		String checkActiveUser = "SELECT 'Y' AS ACTIVE FROM USERS WHERE USER_OID = ? AND ACTIVATION_STATUS = ? ";
		resultSet = DatabaseQueryBean.getXmlResultSet(checkActiveUser,false, new Object[]{user, TradePortalConstants.ACTIVE});

		if (resultSet != null)
			return true;
		else
		{
			this.getErrorManager().issueError(getClass().getName(),TradePortalConstants.FVD_AUTH_USER_INACTIVE, user);
			return false;
		}
    }
	
    /**
	 * This method sets transaction status to failed and transaction date to current date
	 *
	 * @param transaction com.ams.tradeportal.busobj.Transaction
     */
	private void updateAuthorizeFailed(Transaction transaction) throws AmsException, RemoteException
	{
  	 	transaction.setAttribute("transaction_status",	TransactionStatus.AUTHORIZE_FAILED);
	   	transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());

		for (int authIndex = 1; authIndex <= TradePortalConstants.MAX_TRAN_AUTH_NUM; authIndex++)
		{
			String indexPrefix = getAuthorizerPrefixForIndex(authIndex);
			String authIndexStr = Integer.toString(authIndex);
			transaction.setAttribute(indexPrefix + "_authorizing_user_oid", "");
			transaction.setAttribute(indexPrefix + "_authorizing_work_group_oid", "");
			transaction.setAttribute(indexPrefix + "_authorize_status_date", "");
			transaction.setAttribute("authorizing_panel_code_" + authIndexStr, "");
		}

		transaction.save();
    }



	//cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 start
	/**
	 * Is the account owned by a different corporate org?
	 * This is now simple with the other_account_owner_oid on account.
	 * Its only populated when this condition is true.
	 *
	 * Note this implicitly deals with user subsidiary access
	 * becuase it doesn't look at the user org!
	 *
	 * This should probably be a method on Account...
	 */
	private boolean isAccountOwnedByOtherCorp(Account account) throws AmsException, RemoteException {
        boolean isOtherCorp = false;

        if ( account!=null ) {
            String otherOwnerOid = account.getAttribute( "other_account_owner_oid" );
            if ( otherOwnerOid!=null && otherOwnerOid.length()> 0 ) {
                isOtherCorp = true;
            }
        }
        return isOtherCorp;
	}

	/**
	 * Make the account parent the payment owner.
	 * Also the APP a POW and add the APP.
	 *
	 * @param transaction
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private void makeAcctParentPaymentOwner(String instrumentType, Terms terms, Account account) throws AmsException, RemoteException {
		//KMehta 13 Feb 2015 Rel 9.2 IR T36000032454 Change Start
		long accountOwnerOid =  (InstrumentType.XFER_BET_ACCTS.equals(instrumentType) && !TradePortalConstants.INDICATOR_YES.equals(account.getAttribute("othercorp_customer_indicator"))) ?
									Long.parseLong(account.getAttribute("owner_oid"))
									:Long.parseLong(account.getAttribute("other_account_owner_oid"));
		 CorporateOrganization acctOwnerCorp =
        (CorporateOrganization) createServerEJB("CorporateOrganization",accountOwnerOid);
		//Leelavathi IR#T36000013625 22/07/2013 End

	    //if no existing POW, and there is an APP, change the POW to be an APP
	    TermsParty pow = terms.getTermsPartyByPartyType( TermsPartyType.PAYMENT_OWNER );
	    if ( pow==null ) {
	        String payerPartyType = TermsPartyType.APPLICANT; //ftdp, ftba
            if ( InstrumentType.FUNDS_XFER.equals(instrumentType)) { //ftrq
                payerPartyType = TermsPartyType.PAYER;
            }

            TermsParty acctParentTermsParty = terms.getTermsPartyByPartyType( payerPartyType );
            //KMehta 13 Feb 2015 Rel 9.2 IR T36000032454 Change Start
	        if ( acctParentTermsParty!=null ) {
	            //make the app the pow
	        	if(InstrumentType.XFER_BET_ACCTS.equals(instrumentType) && TradePortalConstants.INDICATOR_YES.equals(account.getAttribute("othercorp_customer_indicator"))){
					acctParentTermsParty.populateTermsParty(acctOwnerCorp, TermsPartyType.PAYMENT_OWNER );
				}else
					
		            acctParentTermsParty.setAttribute("terms_party_type", TermsPartyType.PAYMENT_OWNER);
		    }
	        //KMehta 13 Feb 2015 Rel 9.2 IR T36000032454 Change End
            //now populate new payer
            //cquinton 7/6/2011 irhhul070665325 -
            // for ftrq start with the 4th terms party to avoid placement logic with 1-3
            String tpComponentId;
           
	            if ( InstrumentType.FUNDS_XFER.equals(instrumentType)) { //ftrq
	                tpComponentId = terms.getNextEmptyTermsParty("FourthTermsParty");
	            } else {
	                tpComponentId = terms.getNextEmptyTermsParty();
	            }
		        if ( tpComponentId != null ) {
		            //first populate account owner
	                terms.newComponent(tpComponentId);
	                TermsParty otherAcctTermsParty = (TermsParty) terms.getComponentHandle( tpComponentId );
		            otherAcctTermsParty.populateTermsParty(acctOwnerCorp, payerPartyType );
		        }
            
	    }
	}
    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 end


	/**
	 * Update the payee info for account owned by subsidiary.
	 *
	 * @param terms
	 * @param account
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private void updatePayeeOtherCorp(Terms terms, Account account) throws AmsException, RemoteException {
		//get the account owner corp
		CorporateOrganization acctOwnerCorp =
			(CorporateOrganization) createServerEJB("CorporateOrganization",
				Long.parseLong(account.getAttribute("other_account_owner_oid")));

		//get the payee and populate it with the correct corp
		TermsParty payee = terms.getTermsPartyByPartyType(TermsPartyType.PAYEE);
		if (payee != null) {
			payee.populateTermsParty(acctOwnerCorp, TermsPartyType.PAYEE);
		}
	}

	/**
	 * Processing for Transaction involving FX rates and two currencies different than base
	 *
	 * @param transferCurrency - payment currency
	 * @param fromCurrency - debit currency
	 * @param baseCurrency - base currency
	 * @param toCurrency - to currency for TBAs
	 * @param amount - transaction amount
	 * @param terms - terms data
         * @throws java.rmi.RemoteException
         * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public void fxCrossRateProcessing(String transferCurrency, String fromCurrency, String baseCurrency, String toCurrency,
		       String amount, Terms terms) throws RemoteException, AmsException
    {
		fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, toCurrency, amount, terms, false);

    }

	private boolean fxCrossRateProcessing(String transferCurrency, String fromCurrency, String baseCurrency, String toCurrency,
									       String amount, Terms terms, boolean isAuthProcess) throws RemoteException, AmsException
	{
		boolean isSuccess = true;
		Vector outMDInd = new Vector(1,1);
		String s_ex_amount ;
		BigDecimal ex_amount = null;
		BigDecimal rate = null;      
		String ex_ind = null;
		String useRateBase = TradePortalConstants.USE_SELL_RATE;
		String instrumentTypeCode = getAttribute("instrument_type_code");
		String fromAcctOID = terms.getAttribute("debit_account_oid"); 
		String corpOrgOid = null; 

		if (((InstrumentType.XFER_BET_ACCTS).equals(instrumentTypeCode)) && (transferCurrency.equals(fromCurrency)))
			useRateBase = TradePortalConstants.USE_BUY_RATE;

		if (StringFunction.isNotBlank(fromAcctOID))
		{
			Account fromAccount = (Account) createServerEJB("Account");
			fromAccount.getData(Long.parseLong(fromAcctOID));

		    
			corpOrgOid = fromAccount.getOwnerOfAccount();
			baseCurrency = fromAccount.getBaseCurrencyOfAccount();
		    
		}

		BigDecimal amountInBase = getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corpOrgOid, useRateBase, true);  //MDB PIUL102167761 Rel7.1 10/21/11 //MDB MRUL122933633 Rel7.1 1/3/12

		LOG.debug("fxCrossRateProcessing::getAmountInBaseCurrency: {}" , amountInBase);


	    BigDecimal midRate ;
		if (StringFunction.isNotBlank(toCurrency))  //TBA
		{
			if (fromCurrency.equalsIgnoreCase(transferCurrency))
			    midRate = getFXMidRate(toCurrency, corpOrgOid);
			else
			    midRate = getFXMidRate(fromCurrency, corpOrgOid);
		}
		else
		    midRate = getFXMidRate(fromCurrency, corpOrgOid);


		if (midRate!=null && StringFunction.isNotBlank(midRate.toString()))
		{
			LOG.debug("fxCrossRateProcessing::midRate: {}" , midRate);


			String fxCurrency=fromCurrency;    	
			if (StringFunction.isNotBlank(toCurrency))  //TBA
			{
				if (fromCurrency.equalsIgnoreCase(transferCurrency)) {
			    	ex_amount = getAmounInFXCurrency(toCurrency, baseCurrency, amountInBase.toString(), corpOrgOid, TradePortalConstants.USE_BUY_RATE, midRate, outMDInd, true);  //MDB PIUL102167761 Rel7.1 10/21/11 //MDB MRUL122933633 Rel7.1 1/3/12
			    	fxCurrency=toCurrency;  
				}
				else {
			    	ex_amount = getAmounInFXCurrency(fromCurrency, baseCurrency, amountInBase.toString(), corpOrgOid, TradePortalConstants.USE_BUY_RATE, midRate, outMDInd, true);  //MDB PIUL102167761 Rel7.1 10/21/11 //MDB MRUL122933633 Rel7.1 1/3/12
				}
			}
			else
		    	ex_amount = getAmounInFXCurrency(fromCurrency, baseCurrency, amountInBase.toString(), corpOrgOid, TradePortalConstants.USE_BUY_RATE, midRate, outMDInd, true);  //MDB PIUL102167761 Rel7.1 10/21/11 //MDB MRUL122933633 Rel7.1 1/3/12


			if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))
				ex_ind = (String)outMDInd.get(0);

			LOG.debug("fxCrossRateProcessing::payment amountmidRate: {}" , new BigDecimal(amount));
	
			rate = ((new BigDecimal(amount)).divide(ex_amount, 8, BigDecimal.ROUND_HALF_UP));

            // re-calculate exchance amount using cross rate                                                                                  //NSX PYUM022074514 Rel 7.1.0 02/22/12 -
			ex_amount = (new BigDecimal(amount)).divide(rate, TPCurrencyUtility.getDecimalPrecision(fxCurrency), BigDecimal.ROUND_HALF_UP);   //NSX PYUM022074514 Rel 7.1.0 02/22/12 -


			LOG.debug("fxCrossRateProcessing::getAmounInFXCurrency: {}" , ex_amount);
			LOG.debug("fxCrossRateProcessing::indicator: {}" , ex_ind);
			LOG.debug("fxCrossRateProcessing::crossRate: {}" , rate);
		}

		if (ex_amount == null)
		{
			rate = null;
			s_ex_amount = null;
			ex_ind = null;
		}
		else {
			s_ex_amount = ex_amount.toString();

			if (isAuthProcess) {

				BigDecimal cur_rate = terms.getAttributeDecimal("transfer_fx_rate");
				if (cur_rate.compareTo(rate) != 0) {
					getErrorManager().issueError(getClass().getName(),	TradePortalConstants.FX_RATE_CHANGED,getAttribute("complete_instrument_id"));
					isSuccess = false;
				}

			}
		}


	    //for TBAs, populate for currency display in equivalent amount
		if ((StringFunction.isNotBlank(toCurrency)) && !(transferCurrency.equals(fromCurrency) && transferCurrency.equals(toCurrency)))
		{
			if (transferCurrency.equals(fromCurrency))
				terms.setAttribute("equivalent_exch_amt_ccy", toCurrency);
			else if (transferCurrency.equals(toCurrency))
				terms.setAttribute("equivalent_exch_amt_ccy", fromCurrency);
		}


		terms.setAttribute("equivalent_exch_amount", s_ex_amount);
		if(rate!=null) terms.setAttribute("transfer_fx_rate", rate.toString());
		terms.setAttribute("display_fx_rate_method", TradePortalConstants.DIVIDE); //MDB PYUL101952085 Rel7.1 10/19/11

		return isSuccess;
	}
  
	/**
	 * Determine mid rate for cross rate calculation
	 *
         * @param currency - payment currency
         * @param userOrgOid - organization oid
	 * @return BigDedcimal - mid rate value
         * @throws com.amsinc.ecsg.frame.AmsException
         * @throws java.rmi.RemoteException
	 */
    public BigDecimal getFXMidRate(String currency, String userOrgOid) throws AmsException, RemoteException
    {
		BigDecimal buyRate;
		BigDecimal sellRate;
		BigDecimal midRate=null;

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler)fxCache.get(userOrgOid + "|" + currency);
		if (resultSet == null)
	   		this.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.NO_FOREX_DEFINED, currency); //MDB CR-640 Rel7.1 9/21/11
		else
		{
		   	DocumentHandler fxRate = resultSet.getFragment("/ResultSetRecord");
		   	if (fxRate == null)
		   		this.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.NO_FOREX_DEFINED, currency); //MDB CR-640 Rel7.1 9/21/11
			else
			{
				if ((StringFunction.isBlank(fxRate.getAttribute("/SELL_RATE"))) || (StringFunction.isBlank(fxRate.getAttribute("/BUY_RATE"))))
					this.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.NO_SELL_FOREX_DEFINED);

		    	buyRate = fxRate.getAttributeDecimal("/BUY_RATE");
			    sellRate = fxRate.getAttributeDecimal("/SELL_RATE");

			    midRate = buyRate.add(sellRate).divide(new BigDecimal(2));
			}
		}
		return midRate;
    }
    
	/**
	 * Max Deal Amount validation during FX Rate processing
	 *
     * @param inputDoc
	 * @param currency - currency
	 * @param terms - terms data
	 * @param transaction - transaction 
	 * @return boolean  - true: validation passes; false: validation fails
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	 */

	public boolean validateMaxDealamount(DocumentHandler inputDoc, String currency, Terms terms, Transaction transaction) throws RemoteException, AmsException
	{
		boolean flag=false;
		String fromAcctOID = terms.getAttribute("debit_account_oid");
		String paymentCurrency = terms.getAttribute("amount_currency_code");
		BigDecimal paymentAmt = terms.getAttributeDecimal("amount");
		BigDecimal maxDealAmt = null;

		if (checkFXOnlineIndicator(terms))
		{
			if (StringFunction.isNotBlank(fromAcctOID))
			{
				Account fromAccount = (Account) createServerEJB("Account");
				fromAccount.getData(Long.parseLong(fromAcctOID));
				String fromCurrency = fromAccount.getAttribute("currency");
				

				String corpOrgOid = fromAccount.getOwnerOfAccount();
				String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
				String clientBankOid = fromAccount.getClientBankOfAccount();


				Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);

				LOG.debug("validateMaxDealamount::from currency: {}" , fromCurrency);
				LOG.debug("validateMaxDealamount::corpOrgOid: {}" , corpOrgOid);
				LOG.debug("validateMaxDealamount::baseCurrency: {}" , baseCurrency);
				LOG.debug("validateMaxDealamount::client_bank_oid: {}" , clientBankOid);

				DocumentHandler resultSet;

				//MDB LRUM011251749 Rel7.1 1/13/12 - Begin
				if(InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code")))
				{
					if (baseCurrency.equals(paymentCurrency))
					{
						if (fromCurrency.equals(paymentCurrency))
						{
							Account creditAccount = (Account) createServerEJB("Account");
							creditAccount.getData(Long.parseLong(terms.getAttribute("credit_account_oid")));
							String toCurrency = creditAccount.getAttribute("currency");
							resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" +  toCurrency);
							paymentAmt = getAmounInFXCurrency(toCurrency, baseCurrency, paymentAmt.toString(),corpOrgOid,TradePortalConstants.USE_BUY_RATE);
						}
						else
						{
							resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + fromCurrency);
							paymentAmt = getAmounInFXCurrency(fromCurrency, baseCurrency, paymentAmt.toString(),corpOrgOid,TradePortalConstants.USE_BUY_RATE);
						}
					}
					else
						resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + paymentCurrency);
				}
				else
				{
				if (baseCurrency.equals(paymentCurrency))
					{
						resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + fromCurrency);
						paymentAmt = getAmounInFXCurrency(fromCurrency, baseCurrency, paymentAmt.toString(),corpOrgOid,TradePortalConstants.USE_BUY_RATE);
					}
				else
					resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + paymentCurrency);
				}


				if (resultSet != null)
				{
					DocumentHandler fxRate = resultSet.getFragment("/ResultSetRecord");
					if (fxRate != null)
					{
						if(!fxRate.getAttribute("/MAX_DEAL_AMOUNT").equals(""))
							maxDealAmt = fxRate.getAttributeDecimal("/MAX_DEAL_AMOUNT");
					}
				}

				LOG.debug("validateMaxDealamount::baseCurrency.equals(paymentCurrency) is {}" , baseCurrency.equals(paymentCurrency));

			

				LOG.debug("validateMaxDealamount::maxDealAmt: {}" , maxDealAmt);
				LOG.debug("validateMaxDealamount::paymentAmt: {}" , paymentAmt);
				if ((maxDealAmt != null) && (paymentAmt.compareTo(maxDealAmt) == 1))
				{
					String instrumentType = getAttribute("instrument_type_code");
					if((StringFunction.isNotBlank(instrumentType)) &&
					   (instrumentType.equals(InstrumentType.DOM_PMT) ||
				       	   instrumentType.equals(InstrumentType.FUNDS_XFER) ||
						   instrumentType.equals(InstrumentType.XFER_BET_ACCTS)))
					{

						if (TradePortalConstants.BUTTON_CONFIRM.equals(inputDoc.getAttribute("/Update/ButtonPressed")))
						{
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_DEAL_AMOUNT_EXCEEDED_WARN);
					   		transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
					   		transaction.setAttribute("authorization_errors", TradePortalConstants.MAX_DEAL_AMOUNT_EXCEEDED);
						}
						else
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_DEAL_AMOUNT_EXCEEDED);

						if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
						{
					   		transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
					   		transaction.setAttribute("authorization_errors", TradePortalConstants.MAX_DEAL_AMOUNT_EXCEEDED);
						}

					}
				}
				else
					flag=true;
			}
		}
		LOG.debug("validateMaxDealamount::returning: {}" , flag);
		return flag;
	}
   
	/**
	 * Max Spot Amount validation during FX Rate processing
	 *
     * @param inputDoc
	 * @param currency - currency
	 * @param terms - terms data
	 * @param transaction - transaction
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public void validateMaxSpotRateAmtThreshold(DocumentHandler inputDoc, String currency, Terms terms, Transaction transaction) throws RemoteException, AmsException //MDB PRUL112347061 Rel7.1 12/15/11
	{
         validateMaxSpotRateAmtThreshold(inputDoc, currency, terms, transaction,  false, false, new HashMap()); //MDB PRUL112347061 Rel7.1 12/15/11
    }


	private boolean validateMaxSpotRateAmtThreshold(DocumentHandler inputDoc, String currency, Terms terms, Transaction transaction, boolean isAuthProcess, boolean isFinalAuthorization, HashMap params) throws RemoteException, AmsException
	{
		String fromAcctOID = terms.getAttribute("debit_account_oid");
		String paymentCurrency = terms.getAttribute("amount_currency_code");
		BigDecimal paymentAmt = terms.getAttributeDecimal("amount");
		BigDecimal maxSpotRateAmt = null;
		boolean isSuccess = true;

		if (StringFunction.isNotBlank(fromAcctOID))
		{
			Account fromAccount = (Account) createServerEJB("Account");
			fromAccount.getData(Long.parseLong(fromAcctOID));
			String fromCurrency = fromAccount.getAttribute("currency");
			LOG.debug("from currency: {}" , fromCurrency);
			String rateGroup = fromAccount.getAttribute("fx_rate_group");

			Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);


			String corpOrgOid = fromAccount.getOwnerOfAccount();
			String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
			String clientBankOid = fromAccount.getClientBankOfAccount();


			LOG.debug("validateMaxSpotRateAmtThreshold::rateGroup: {}" , rateGroup);
			LOG.debug("validateMaxSpotRateAmtThreshold::corpOrgOid: {}" , corpOrgOid);
			LOG.debug("validateMaxSpotRateAmtThreshold::client_bank_oid: {}" ,clientBankOid);

			DocumentHandler resultSet ;
			if (baseCurrency.equals(paymentCurrency))
				resultSet = DatabaseQueryBean.getXmlResultSet("select MAX_SPOT_RATE_AMOUNT from fx_rate where rownum = 1 and p_owner_org_oid=? and fx_rate_group=?",false, new Object[]{clientBankOid,rateGroup });
			else
				resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + paymentCurrency);

			if (resultSet != null)
			{
				DocumentHandler fxRate = resultSet.getFragment("/ResultSetRecord");
				if (fxRate != null)
				{
					if(!fxRate.getAttribute("/MAX_SPOT_RATE_AMOUNT").equals(""))
						maxSpotRateAmt = fxRate.getAttributeDecimal("/MAX_SPOT_RATE_AMOUNT");
				}
			}

			if (!baseCurrency.equals(paymentCurrency))
		    	paymentAmt = getAmountInBaseCurrency(paymentCurrency, paymentAmt.toString(), baseCurrency, corpOrgOid, TradePortalConstants.USE_SELL_RATE); //MDB PIUL102167761 Rel7.1 10/21/11

			LOG.debug("validateMaxSpotRateAmtThreshold::baseCurrency: {}" , baseCurrency);
			LOG.debug("validateMaxSpotRateAmtThreshold::maxSpotRateAmt: {}" , maxSpotRateAmt);
			LOG.debug("validateMaxSpotRateAmtThreshold::paymentAmt: {}" , paymentAmt);
			if ((maxSpotRateAmt != null) && (paymentAmt.compareTo(maxSpotRateAmt) == 1))
			{
				if (checkFXOnlineIndicator(terms))
				{
			      if (isAuthProcess) {

					if (isFinalAuthorization) {
						IssuedError err = ErrorManager.findErrorCode(TradePortalConstants.FX_SPOT_RATE_THREDSHOLD_EXCEED_MARKETRATE, csdb.getLocaleName());
						 err.setSeverity(ErrorManager.ERROR_SEVERITY);
						 getErrorManager().getIssuedErrorsList().add(err);
						 params.put("transaction_status", TransactionStatus.FX_THRESH_EXCEEDED);
							transaction.setAttribute("transaction_status", TransactionStatus.FX_THRESH_EXCEEDED);
							isSuccess = false;
					}else {
						getErrorManager().issueError(getClass().getName(), TradePortalConstants.FX_SPOT_RATE_THREDSHOLD_EXCEED_MARKETRATE);
					}
				  }else {
					//warning only
					this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED_WARN);
			    	transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_AUTHORIZE);
				  }

				}
				else
				{
					if (isAuthProcess) {

						if (isFinalAuthorization) {
							IssuedError err = ErrorManager.findErrorCode(TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED, csdb.getLocaleName()); //MDB LAUM010956938 Rel7.1 1/10/12 - changed error code
							 err.setSeverity(ErrorManager.ERROR_SEVERITY);
							 getErrorManager().getIssuedErrorsList().add(err);
							 params.put("transaction_status", TransactionStatus.FX_THRESH_EXCEEDED);
							transaction.setAttribute("transaction_status", TransactionStatus.FX_THRESH_EXCEEDED);
							isSuccess = false;
						}
						else {
							getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED); //MDB LAUM010956938 Rel7.1 1/10/12 - changed error code
						}
					} else {

						if (TradePortalConstants.BUTTON_CONFIRM.equals(inputDoc.getAttribute("/Update/ButtonPressed")))
						{
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED_WARN);
					    	transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
					   		transaction.setAttribute("authorization_errors", TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED);
						}
						else
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED);

						if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
						{
					   		transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
					   		transaction.setAttribute("authorization_errors", TradePortalConstants.MAX_SPOT_RATE_AMOUNT_EXCEEDED);
						}

					}
				}
			}
			else
			{
	
				if (!isAuthProcess) {
					if ((fromCurrency.equals(baseCurrency)) || (paymentCurrency.equals(baseCurrency)))
						transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_AUTHORIZE);
				}

			}


			if (isSuccess && isAuthProcess) {
				isSuccess = calculateAmount(fromAccount, paymentCurrency, terms, transaction, params);
			}

		}


		return isSuccess;
	}
 
	/**
	 * @param fromAccount
	 * @param paymentCurrency
	 * @param terms
	 * @param transaction
	 * @param params
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected boolean calculateAmount(Account fromAccount, String paymentCurrency, Terms terms, Transaction transaction, HashMap params) throws RemoteException, AmsException {
		boolean isSuccess = true;
		String ex_ind = null,ex_currency=null, s_ex_amount = null;
		BigDecimal ex_amount = null, bigDecRate=null;
		String fromCurrency = fromAccount.getAttribute("currency");
		String amount = terms.getAttribute("amount");
		String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
		String accOwnerOid = fromAccount.getOwnerOfAccount();
		Vector outMDInd = new Vector(1,1);
		String rateType=null;
		String toCurrency ;
		if (InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code")) && fromCurrency.equalsIgnoreCase(paymentCurrency))
		{
			Account toAccount = (Account) createServerEJB("Account",Long.parseLong(terms.getAttribute("credit_account_oid")));
			toCurrency=toAccount.getAttribute("currency");
			ex_currency = toCurrency;

			if (isTwoForeignCurrencyInvolved(baseCurrency, paymentCurrency, fromCurrency, toCurrency))
			   {
				return isSuccess;
			   }


			if (fromCurrency.equals(baseCurrency)) {
				rateType=TradePortalConstants.USE_SELL_RATE;
			}
			else {
				rateType=TradePortalConstants.USE_BUY_RATE;
			}

			if (ex_currency.equals(baseCurrency)) {
				bigDecRate = InstrumentServices.getFXRate(fromAccount, paymentCurrency, rateType);
				if (!hasRateChanged(terms,bigDecRate )) {
					return isSuccess;
				}
				ex_amount = getAmountInBaseCurrency(paymentCurrency, amount,baseCurrency , accOwnerOid, rateType, bigDecRate, outMDInd);
			}
			else {
				bigDecRate = InstrumentServices.getFXRate(fromAccount, ex_currency, rateType);
				if (!hasRateChanged(terms,bigDecRate )) {
					return isSuccess;
				}
				ex_amount = getAmounInFXCurrency(ex_currency, baseCurrency, amount, accOwnerOid,rateType, bigDecRate, outMDInd);
			}
		} else 	{

			if (isTwoForeignCurrencyInvolved(baseCurrency, null, fromCurrency, paymentCurrency)){
				return isSuccess;
			}
			ex_currency = fromCurrency;
			if (paymentCurrency.equals(fromCurrency))
			{
				return isSuccess;
			}
			else if (paymentCurrency.equals(baseCurrency))
			{
				bigDecRate=InstrumentServices.getFXRate(fromAccount, fromCurrency, TradePortalConstants.USE_BUY_RATE);
				if (!hasRateChanged(terms,bigDecRate )) {
					return isSuccess;
				}
				ex_amount = getAmounInFXCurrency(fromCurrency, baseCurrency, amount, accOwnerOid,rateType, bigDecRate, outMDInd);
			}
			else if (fromCurrency.equals(baseCurrency))
			{
				bigDecRate=InstrumentServices.getFXRate(fromAccount, paymentCurrency, TradePortalConstants.USE_SELL_RATE);
				if (!hasRateChanged(terms,bigDecRate )) {
					return isSuccess;
				}
				ex_amount = getAmountInBaseCurrency(paymentCurrency, amount, baseCurrency, accOwnerOid, rateType, bigDecRate, outMDInd);
			}
		}

		s_ex_amount = ex_amount!=null ?ex_amount.toString():null;
		if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))
			ex_ind = (String)outMDInd.get(0);
		getErrorManager().issueError(getClass().getName(),	TradePortalConstants.FX_RATE_CHANGED, getAttribute("complete_instrument_id"));
		params.put("RATE_CHANGED", TradePortalConstants.INDICATOR_YES);
		isSuccess = false;


		terms.setAttribute("equivalent_exch_amount", s_ex_amount);
		if(bigDecRate!=null)
		terms.setAttribute("transfer_fx_rate", bigDecRate.toString());
		terms.setAttribute("display_fx_rate_method", ex_ind);
    	terms.setAttribute("equivalent_exch_amt_ccy", ex_currency);

		return  isSuccess;
	}

	/**
	 * @param terms
	 * @param newRate
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected boolean hasRateChanged(Terms terms, BigDecimal newRate) throws RemoteException, AmsException {
		BigDecimal cur_rate = terms.getAttributeDecimal("transfer_fx_rate");
		return (cur_rate.compareTo(newRate) != 0);
	}
   
	/**
	 * Returns Market Rate Indicator Value
	 *
	 * @param terms - terms data
	 * @param transaction - transaction 
	 * @return boolean  - true for Y: false for N or null
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public boolean requestMarketRateIndicator(Terms terms, Transaction transaction) throws RemoteException, AmsException
	{
		String mktRateInd = terms.getAttribute("request_market_rate_ind");

		if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
		{
			if ((StringFunction.isNotBlank(mktRateInd)) && (TradePortalConstants.INDICATOR_YES.equals(mktRateInd)))
			{
				if (checkFXOnlineIndicator(terms))
				{
					if (TPDateTimeUtility.isFutureDate(transaction.getAttribute("payment_date")) )
				    	transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
				    else
				    {
				    	transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_AUTHORIZE);
						terms.setAttribute("request_market_rate_ind", TradePortalConstants.INDICATOR_YES);
					}
				}
				else
					this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MARKET_RATE_UNAVAILABLE);
			}
		}
		else
		{
		   String instrumentType = getAttribute("instrument_type_code");
 		   if((StringFunction.isNotBlank(instrumentType)) &&
		      (instrumentType.equals(InstrumentType.DOM_PMT) ||
	       	   instrumentType.equals(InstrumentType.FUNDS_XFER) ||
			   instrumentType.equals(InstrumentType.XFER_BET_ACCTS)))
		   {
		   	   if ((StringFunction.isNotBlank(mktRateInd)) && (TradePortalConstants.INDICATOR_YES.equals(mktRateInd)))
		    	   transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_AUTHORIZE);
		   }
		}

   	   return ((StringFunction.isNotBlank(mktRateInd)) && (TradePortalConstants.INDICATOR_YES.equals(mktRateInd)));
			
	}
   
	/**
	 * Returns true if bank_organization_group.fx_online_avail_ind is Y
	 *
	 * @param terms - terms data
	 * @return boolean - true if bank_organization_group.fx_online_avail_ind is Y, else false
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public boolean checkFXOnlineIndicator(Terms terms) throws RemoteException, AmsException
	{
		Account debitAcct = (Account) createServerEJB("Account", Long.parseLong(terms.getAttribute("debit_account_oid")));

        OperationalBankOrganization opOrg = (OperationalBankOrganization) createServerEJB("OperationalBankOrganization",
        																									Long.parseLong(debitAcct.getAttribute("op_bank_org_oid")));

        BankOrganizationGroup bankOrg = (BankOrganizationGroup) createServerEJB("BankOrganizationGroup");
		bankOrg.getData(Long.parseLong(opOrg.getAttribute("bank_org_group_oid")));

		LOG.debug("checkFXOnlineIndicator::opOrg.getAttribute(bank_org_group_oid): {}" , opOrg.getAttribute("bank_org_group_oid"));
		LOG.debug("checkFXOnlineIndicator::bankOrg.getAttribute(fx_online_avail_ind): {}" , bankOrg.getAttribute("fx_online_avail_ind"));

		return (TradePortalConstants.INDICATOR_YES.equals(bankOrg.getAttribute("fx_online_avail_ind")));
			
	}


	private boolean isTwoForeignCurrencyInvolved(String baseCurrency, String transferCurrency, String fromCurrency, String toCurrency) {
		return (!fromCurrency.equals(baseCurrency) && !toCurrency.equals(baseCurrency) && !fromCurrency.equals(toCurrency));
	}



	/**
	 * @param transaction
	 * @param terms
	 * @param curDateStr
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void updateExecutionDate(Transaction transaction, String curDateStr)  throws RemoteException, AmsException {

		DocumentHandler inputDoc = new DocumentHandler();
		String transactionOid = transaction.getAttribute("transaction_oid");
		Terms terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));

		String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE, DP.SEQUENCE_NUMBER FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ? "
			+ " and rownum = 1";
	DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true,new Object[]{transactionOid});
		String domesticPaymentOidStr = resultsDoc.getAttribute("/ResultSetRecord(0)/DOMESTIC_PAYMENT_OID");

		DomesticPayment domPay = (DomesticPayment) createServerEJB("DomesticPayment", Long.parseLong(domesticPaymentOidStr));

		inputDoc.setAttribute("/DomesticPayment/payment_method_type",domPay.getAttribute("payment_method_type"));
		inputDoc.setAttribute("/Instrument/instrument_type_code",getAttribute("instrument_type_code"));
		inputDoc.setAttribute("/Transaction/payment_date",curDateStr);
		inputDoc.setAttribute("/Terms/amount_currency_code",terms.getAttribute("amount_currency_code"));

		Account account = (Account) createServerEJB("Account", Long.parseLong(terms.getAttribute("debit_account_oid")));
		String debitAccountBankCountryCode = account.getBankBranchCountryInISO();

		domPay.calculatePaymentDate(inputDoc, debitAccountBankCountryCode);

		String valueDate = inputDoc.getAttribute("/DomesticPayment/value_date");

		// Use SQL to update the value date of all the domestic payment in order to gain performance.
		if (valueDate != null) {
			String updateValueDateSQL = "UPDATE DOMESTIC_PAYMENT SET VALUE_DATE = to_date(?, 'mm/dd/yyyy') where p_transaction_oid = ?";
			try {
				DatabaseQueryBean.executeUpdate(updateValueDateSQL, false, valueDate, transactionOid);
			} catch (SQLException e) {
				throw new AmsException(e.getMessage());
			}
		}

		transaction.setAttribute("payment_date", inputDoc.getAttribute("/Transaction/payment_date"));
	}


	/**
	 * @param transaction
	 * @param terms
	 * @param debitAccount
	 * @param creditAccount
	 * @param baseCurrency
	 * @param isFinalAuthorization
     * @param params
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	protected boolean performFXprocess(Transaction transaction, Terms terms, Account debitAccount, Account creditAccount, String baseCurrency, boolean isFinalAuthorization, HashMap params) throws RemoteException, AmsException {
		boolean procceedToAuth = true;
		DocumentHandler inputDoc = new DocumentHandler();
		if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) return false;

		if (StringFunction.isBlank(terms.getAttribute("covered_by_fec_number")) ||
				StringFunction.isBlank(terms.getAttribute("fec_rate")))  {

			//Vshah - Rel7.1.0.2 - IR#SSUM021054546 - <BEGIN>
			//No need to check for Foreign Currency Involvement for International Payment when Settlement Type = "Other".
			if ( !(InstrumentType.FUNDS_XFER.equals(getAttribute("instrument_type_code")) &&
				   TradePortalConstants.XFER_SETTLE_OTHER.equals(terms.getAttribute("funds_xfer_settle_type"))) ) {
			//Vshah - Rel7.1.0.2 - IR#SSUM021054546 - <END>
				if (isForeignCurrencyInvolved(baseCurrency,terms,debitAccount,creditAccount)) {
					if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("request_market_rate_ind"))) {
						//revalidate rates and perform the steps below.

						if (isFinalAuthorization) {
							//Note: The user will not be able to request a live market rate until the final authorisation is received and the transaction status is set to �Authorised- Pending Market Rate�
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MARKET_RATE_OR_FX_CONTRACT_REQD);
							transaction.setAttribute("transaction_status", TransactionStatus.AUTH_PEND_MARKET_RATE);
							params.put("transaction_status", TransactionStatus.AUTH_PEND_MARKET_RATE);
							procceedToAuth = false;
						}else {
							//disp warning message
							this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.MARKET_RATE_REQRD_UPON_AUTH);
						}

					} else { 
						procceedToAuth = validateMaxSpotRateAmtThreshold(inputDoc,transaction.getAttribute("copy_of_currency_code"),  terms,  transaction, true, isFinalAuthorization, params);
					    
						if (procceedToAuth) {
							procceedToAuth = fxCrossRateProcessingAuth(baseCurrency,terms,debitAccount,creditAccount, procceedToAuth, transaction);
						
							if (!procceedToAuth) {
								params.put("RATE_CHANGED", TradePortalConstants.INDICATOR_YES);
							}
							
						}
					     
					}
				}
			}
		}
		return procceedToAuth;
	}

    /**
     * @param transaction
     * @param user
     * @param authorizationCorpOrg
     * @param userOrgOid
     * @param userOid
     * @param authorizeAtParentViewOrSubs
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean isFinalAuthorization(Transaction transaction, User user, CorporateOrganization authorizationCorpOrg, String userOrgOid, String userOid, String authorizeAtParentViewOrSubs) throws RemoteException, AmsException {

		boolean isFinalAuth ;
		String dualAuthIndicator = authorizationCorpOrg.getDualAuthIndicator(getAttribute("instrument_type_code"), transaction.getAttribute("transaction_type_code"));
		String userWorkGroupOid = null;


		if ((StringFunction.isNotBlank(getAttribute("corp_org_oid")))&&(!getAttribute("corp_org_oid").equals(userOrgOid)) && (TradePortalConstants.INDICATOR_NO.equals(authorizeAtParentViewOrSubs)))
			userWorkGroupOid = user.getAttribute("subsidiary_work_group_oid");

		// use user's default work group
		if (StringFunction.isBlank(userWorkGroupOid))
			userWorkGroupOid = user.getAttribute("work_group_oid");

		LOG.debug("InstrumentBean::authorizeTransaction: Dual indicatior: {}" , dualAuthIndicator);

		//Get first authorizing user info
    	String firstAuthorizer = transaction.getAttribute("first_authorizing_user_oid");
        String firstAuthorizerWorkGroup = transaction.getAttribute("first_authorizing_work_group_oid");

		if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
		    && (userWorkGroupOid == null || userWorkGroupOid.equals("")))
		{
			isFinalAuth = false;
		}
	    else if ((dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS)
	        || dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
	        || StringFunction.isNotBlank(firstAuthorizer)))
	    {
	    	if (StringFunction.isBlank(firstAuthorizer))
	    	{
	    		isFinalAuth = false;
	    	}
	    	else  //firstAuthorizer not empty
	    	{
				// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
				if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
				          && userWorkGroupOid.equals(firstAuthorizerWorkGroup))
				{
					isFinalAuth = false;
				}
				// Two authorizers required and this user is the same as the first authorizer. Give error.
				// Note requiring two work groups also implies requiring two users.
				else if (firstAuthorizer.equals(userOid))
				{
					isFinalAuth = false;
				}
				// Authorize
				else
				{
					isFinalAuth =  true;
				}
	    	}
	    }
	    else
	    {
	    	isFinalAuth =  true;
	    }

	   return isFinalAuth;
	}


    /**
     * @param baseCurrency
     * @param terms
     * @param debitAccount
     * @param creditAccount
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    protected boolean isForeignCurrencyInvolved(String baseCurrency, Terms terms, Account debitAccount,Account creditAccount) throws RemoteException, AmsException {
		String transferCurrency = terms.getAttribute("amount_currency_code");
		String fromCurrency = debitAccount.getAttribute("currency");
		boolean ixFXInvolved ;

		if (TradePortalConstants.INDICATOR_YES.equals(debitAccount.getAttribute("othercorp_customer_indicator"))) {
			CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(debitAccount.getAttribute("other_account_owner_oid")));
			baseCurrency = corpOrg.getAttribute("base_currency_code");
		}

		if (InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code"))) {
		   String toCurrency = creditAccount!=null?creditAccount.getAttribute("currency"):"";		   
		   ixFXInvolved =(!fromCurrency.equals(toCurrency) && (!fromCurrency.equals(baseCurrency) || !toCurrency.equals(baseCurrency)));
		}
		else {
			ixFXInvolved =(!fromCurrency.equals(transferCurrency) && (!fromCurrency.equals(baseCurrency) || !transferCurrency.equals(baseCurrency)));

		}
			return ixFXInvolved;
	}



    /**
     * @param baseCurrency
     * @param terms
     * @param debitAccount
     * @param creditAccount
     * @param procceedToAuth
     * @param transaction
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    protected boolean fxCrossRateProcessingAuth(String baseCurrency, Terms terms, Account debitAccount,Account creditAccount, boolean procceedToAuth, Transaction transaction) throws RemoteException, AmsException
    {
		String transferCurrency = terms.getAttribute("amount_currency_code");
		String fromCurrency = debitAccount.getAttribute("currency");
 	    String amount = terms.getAttribute("amount");
 	    boolean isSuccess = true;

		baseCurrency = debitAccount.getBaseCurrencyOfAccount();

		if (InstrumentType.XFER_BET_ACCTS.equals(getAttribute("instrument_type_code")))
		{
		   String toCurrency = creditAccount!=null?creditAccount.getAttribute("currency"):"";
		   if (isTwoForeignCurrencyInvolved(baseCurrency, transferCurrency, fromCurrency, toCurrency))
		   {
		   		isSuccess=fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, toCurrency, amount, terms, procceedToAuth);
			
		   }
		}
		else
		{
		   if (isTwoForeignCurrencyInvolved(baseCurrency, null, fromCurrency, transferCurrency))
		   {
			   isSuccess=fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, null, amount, terms, procceedToAuth);
			   
		   }
	   }

 		return isSuccess;
	}
    
	/**
	 * Returns bank_organization_group.cross_rate_calc_oid
	 *
	 * @param Terms - terms data
	 * @return String - cross_rate_calc_oid
	 */
	private String getCrossRateCalcRule(Terms terms) throws RemoteException, AmsException
	{
		Account debitAcct = (Account) createServerEJB("Account", Long.parseLong(terms.getAttribute("debit_account_oid")));

        OperationalBankOrganization opOrg = (OperationalBankOrganization) createServerEJB("OperationalBankOrganization",
        																									Long.parseLong(debitAcct.getAttribute("op_bank_org_oid")));

        BankOrganizationGroup bankOrg = (BankOrganizationGroup) createServerEJB("BankOrganizationGroup");
		bankOrg.getData(Long.parseLong(opOrg.getAttribute("bank_org_group_oid")));

		LOG.debug("getCrossRateCalcRule::bankOrg.getAttribute('cross_rate_calc_oid'): {}" ,bankOrg.getAttribute("cross_rate_calc_oid"));

		return bankOrg.getAttribute("cross_rate_calc_oid");
	}

	/**
	 * Returns CrossRateRuleCriterion object representing criterion_oid, from_ccy, and to_ccy
	 *
	 * @param String - criterion_oid
	 * @param String - from currency
	 * @param String - to currency
	 * @return CrossRateRuleCriterion
	 */
	private CrossRateRuleCriterion getCrossRateCriterionRow(String calcOid, String fromCurrency, String toCurrency) throws RemoteException, AmsException
	{
		if ((StringFunction.isBlank(calcOid)) || (StringFunction.isBlank(fromCurrency)) || (StringFunction.isBlank(toCurrency)))
			return null;

		LOG.debug("getCrossRateCriterionRow:: select CRITERION_OID from CROSS_RATE_RULE_CRITERION where p_cross_rate_calc_rule_oid = {} and from_ccy = ' {} ' and to_ccy = ' {} '" ,new Object[]{ calcOid, fromCurrency , toCurrency});
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet("select CRITERION_OID from CROSS_RATE_RULE_CRITERION where p_cross_rate_calc_rule_oid = ? and from_ccy = ? and to_ccy = ?",false, new Object[]{calcOid,fromCurrency,toCurrency});
		CrossRateRuleCriterion rateCriterion = null;
		if (resultSet != null)
		{
			String critOid = resultSet.getAttribute("/ResultSetRecord/CRITERION_OID");
			LOG.debug("getCrossRateCriterionRow::CRITERION_OID: {}" , critOid);
			rateCriterion = (CrossRateRuleCriterion) createServerEJB("CrossRateRuleCriterion", Long.parseLong(critOid));
			LOG.debug("getCrossRateCriterionRow::rateCriterion.getAttribute(from_ccy): {}" , rateCriterion.getAttribute("from_ccy"));
		}

		return rateCriterion;
	}

	/**
	 * Returns calculation method of determined cross_rate_rule_criterion
	 *
	 * @param terms - terms data
	 * @param fromCurrency - from currency
	 * @param toCurrency - to currency
	 * @param rate - fec rate
	 * @return String - calculation method
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
	 */
	public String crossRateCriterionProcessing(Terms terms, String fromCurrency, String toCurrency, String rate) throws RemoteException, AmsException
	{
	   BigDecimal rateBD = new BigDecimal(rate);
	   String calcMethod = null;

		String ruleOid = getCrossRateCalcRule(terms);

		if (StringFunction.isBlank(ruleOid))
			this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CALC_METHOD_NOT_DEFINED_FOR_CURRENCY_PAIR);
		else
		{
			CrossRateRuleCriterion rateCriterion = getCrossRateCriterionRow(ruleOid, fromCurrency, toCurrency);
			if (rateCriterion == null)
				this.getErrorManager().issueError(getClass().getName(), TradePortalConstants.CALC_METHOD_NOT_DEFINED_FOR_CURRENCY_PAIR);
			else
			{
				BigDecimal lowerVariance = rateCriterion.getAttributeDecimal("lower_variance");
				BigDecimal upperVariance = rateCriterion.getAttributeDecimal("upper_variance");
				String calc_method = rateCriterion.getAttribute("calc_method");
				if ((lowerVariance.compareTo(rateBD) <= 0) && (upperVariance.compareTo(rateBD) >= 0))
					calcMethod = calc_method;
				else
				{
					if (calc_method.equals(TradePortalConstants.DIVIDE))
						calcMethod = TradePortalConstants.MULTIPLY;
					else
						calcMethod = TradePortalConstants.DIVIDE;
				}
			}
		}
		return calcMethod;
	}
    
	/**
	 * This method creates a new instrument by invoking createNew() method.
	 * @param inputDoc
	 * @param poOids
	 * @param lcRuleName
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
    public DocumentHandler createLoanReqForInvoiceData(DocumentHandler inputDoc, Vector poOids, String lcRuleName) throws RemoteException, AmsException
    {
        LOG.debug("Entered createLoanReqForInvoiceData(). inputDoc follows ->\n {}" , inputDoc.toString());
		LOG.info("createLoanReqForInvoiceData()...");
        DocumentHandler outputDoc = new DocumentHandler();


        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the po line items
        String currency             = inputDoc.getAttribute("/currency"); //the currency of the po line items
        String tempDate             = inputDoc.getAttribute("/shipDate"); //used to hold/parse the last ship date
        String newTransactionOid    ; //the oid of the transaction created for these po line items
        String partyOid           ; //the oid of the beneficiary
        

        //Create a new instrument
        newTransactionOid = this.createNew(inputDoc);
        LOG.debug("New instrument created with an original transacion oid of -> {}" , newTransactionOid);
		LOG.info("newTransactionOid: {}",newTransactionOid);
        if (newTransactionOid == null)
        {
            LOG.debug("newTransactionOid is null.  Error in this.createNew()");
            return outputDoc; //outputDoc is null
        }
        else
            outputDoc.setAttribute("/transactionOid", newTransactionOid);

        LOG.debug("ben_name     = {}" , benName);
        LOG.debug("currency     = {}" , currency);
        String importIndicator = inputDoc.getAttribute("/importIndicator");
        String financeType = inputDoc.getAttribute("/financeType");
        String buyerBacked = inputDoc.getAttribute("/buyerBacked");
        if (StringFunction.isNotBlank(importIndicator)){
        	this.setAttribute("import_indicator", importIndicator);
        }
        /************************************************************************
         * Instantiate the transaction, terms, and termsParty and set the fields
         * derived from the po line item data
         ************************************************************************/

        //get a handle to the terms object through the transaction then set the values on terms
        //we'll also have to create a termsParty object and associate it to the terms object
        Transaction transaction = (Transaction) this.getComponentHandle("TransactionList",
                Long.parseLong(newTransactionOid));
        LOG.debug("Transaction handle acquired -> {}" , transaction.getAttribute("transaction_oid"));
        String uploadIndicator = inputDoc.getAttribute("/uploadIndicator");
        if (StringFunction.isNotBlank(uploadIndicator) && TradePortalConstants.INDICATOR_YES.equals(uploadIndicator)){
        	transaction.setAttribute("uploaded_ind", TradePortalConstants.INDICATOR_YES);
        }else{
        	transaction.setAttribute("uploaded_ind", TradePortalConstants.INDICATOR_NO);
        }
        Terms terms = null;
        try
        {
            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
            LOG.debug("terms handle acquired -> {}" , terms.getAttribute("terms_oid"));
        }
        catch (RemoteException | AmsException e)
        {
            LOG.error("Caught error getting handle to terms: ",e);
        }
	
		long oid = 0l;
		TermsParty termsParty ;
            try{

                termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

            }catch (RemoteException | AmsException e){
                oid = terms.newComponent("FirstTermsParty");
                termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

            }

        LOG.debug("termsParty handle acquired -> {}" , termsParty.getAttribute("terms_party_oid"));
        if (StringFunction.isNotBlank(financeType)){
        	terms.setAttribute("finance_type", financeType);
        }
        if (StringFunction.isNotBlank(buyerBacked)){
        	terms.setAttribute("financing_backed_by_buyer_ind", buyerBacked);
        }
        // Either use the first shipment that exists or create one
        ShipmentTerms shipmentTerms = terms.getFirstShipment();
        if(shipmentTerms == null)
        {
            shipmentTerms = terms.createFirstShipment();
        }

        // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
        // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
        // Note all the PO Line Items have been saved into database by now.

        InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(transaction, terms, poOids, null, null, "INT");
        if (InstrumentType.APPROVAL_TO_PAY.equals(this.getAttribute("instrument_type_code"))){
	  		terms.setAttribute("invoice_only_ind",
	  				TradePortalConstants.INDICATOR_YES);
	  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");
	  		ArMatchingRule rule ;
			InvoicesSummaryData invoice =
				(InvoicesSummaryData) this.createServerEJB("InvoicesSummaryData",
						Long.parseLong((String)poOids.get(0)));

			 tempDate = StringFunction.isNotBlank(invoice
					.getAttribute("payment_date")) ? invoice
					.getAttribute("payment_date") : invoice.getAttribute("due_date");

	  		try {
	  			if (!StringFunction.isBlank(tempDate)) {
	  				Date date = jPylon.parse(tempDate);
	  				terms.setAttribute("invoice_due_date",
	  						DateTimeUtility.convertDateToDateTimeString(date));
	  			//IR T36000021787 -set expiry date from invoice
					terms.setAttribute("expiry_date",
							DateTimeUtility.convertDateToDateTimeString(date));
	  			}
	  		} catch (ParseException | RemoteException | AmsException e) {
	  			LOG.error("Exception in createLoanReqForInvoiceData(): ",e);
	  			}

	  			rule =invoice.getMatchingRule();

	  		
	  		benName = StringFunction.isNotBlank(invoice.getAttribute("seller_name"))?invoice.getAttribute("seller_name"):invoice.getAttribute("seller_id"); 


			TermsParty firstTermsParty = (TermsParty) terms
			.getComponentHandle("FirstTermsParty");

			if(rule!=null){
			
			firstTermsParty.setAttribute("name", benName);
			firstTermsParty.setAttribute("address_line_1", rule.getAttribute("address_line_1"));
			firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
			firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
			firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
			firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
			firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
			firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			}

			firstTermsParty.setAttribute("vendor_id",this.getAttribute("vendor_id"));

			// DK IR T36000015588 Rel 8.2 04/18/2013  Starts
			 StringBuilder templateSql = new StringBuilder();
			 templateSql.append("select INV_ONLY_CREATE_RULE_TEMPLATE from inv_only_create_rule ");
			 templateSql.append("where name = ? ");

			  DocumentHandler template = DatabaseQueryBean.getXmlResultSet(templateSql.toString(), false, new Object[]{lcRuleName});
			  DocumentHandler termsDoc;
			  DocumentHandler termsPartyDoc;
	          String templateOid = "";

		      if( template != null )
		      {
		        Vector resultsVector = template.getFragments("/ResultSetRecord/");
		        if( resultsVector.elementAt(0) != null )
		        {
		        	DocumentHandler tempOid = ((DocumentHandler)resultsVector.elementAt(0));
		            templateOid = tempOid.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE");
		        }
		      }

		      SELECT_SQL = "select * from terms where terms_oid in (select c_cust_enter_terms_oid from transaction where transaction_oid in " +
		      "(select original_transaction_oid from instrument where instrument_oid in " +
		    		 " (select c_template_instr_oid from template where template_oid in "+templateOid+")))";

		      SELECT_TERMS_PARTY_SQL = "select * from terms_party where terms_party_oid in (select c_first_terms_party_oid from terms where terms_oid in (select c_cust_enter_terms_oid from transaction where transaction_oid in " +
		      "(select original_transaction_oid from instrument where instrument_oid in " +
		    		 " (select c_template_instr_oid from template where template_oid in "+templateOid+"))))";

		      
		      DocumentHandler termsResultDoc = DatabaseQueryBean.getXmlResultSet(SELECT_SQL, false, new Object[]{});
		      if( termsResultDoc != null )
		      {
		        List<DocumentHandler> termsVector = termsResultDoc.getFragmentsList("/ResultSetRecord/");
		        if( termsVector.get(0) != null )
		        {
		        	termsDoc = ((DocumentHandler)termsVector.get(0));
			        terms.setAttribute("comm_invoice_indicator", termsDoc.getAttribute("/COMM_INVOICE_INDICATOR"));
					terms.setAttribute("comm_invoice_originals",termsDoc.getAttribute("/COMM_INVOICE_ORIGINALS"));
					terms.setAttribute("comm_invoice_copies",termsDoc.getAttribute("/COMM_INVOICE_COPIES"));
					terms.setAttribute("comm_invoice_text",termsDoc.getAttribute("/COMM_INVOICE_TEXT"));
					terms.setAttribute("packing_list_indicator", termsDoc.getAttribute("/PACKING_LIST_INDICATOR"));
					terms.setAttribute("packing_list_originals",termsDoc.getAttribute("/PACKING_LIST_ORIGINALS"));
					terms.setAttribute("packing_list_copies",termsDoc.getAttribute("/PACKING_LIST_COPIES"));
					terms.setAttribute("packing_list_text",termsDoc.getAttribute("/PACKING_LIST_TEXT"));
					terms.setAttribute("cert_origin_indicator", termsDoc.getAttribute("/CERT_ORIGIN_INDICATOR"));
					terms.setAttribute("cert_origin_originals",termsDoc.getAttribute("/CERT_ORIGIN_ORIGINALS"));
					terms.setAttribute("cert_origin_copies",termsDoc.getAttribute("/CERT_ORIGIN_COPIES"));
					terms.setAttribute("cert_origin_text",termsDoc.getAttribute("/CERT_ORIGIN_TEXT"));
					terms.setAttribute("ins_policy_indicator", termsDoc.getAttribute("/INS_POLICY_INDICATOR"));
					terms.setAttribute("ins_policy_originals",termsDoc.getAttribute("/INS_POLICY_ORIGINALS"));
					terms.setAttribute("ins_policy_copies",termsDoc.getAttribute("/INS_POLICY_COPIES"));
					terms.setAttribute("insurance_endorse_value_plus",termsDoc.getAttribute("/INSURANCE_ENDORSE_VALUE_PLUS"));
					terms.setAttribute("insurance_risk_type",termsDoc.getAttribute("/INSURANCE_RISK_TYPE"));
					terms.setAttribute("ins_policy_text",termsDoc.getAttribute("/INS_POLICY_TEXT"));
					terms.setAttribute("other_req_doc_1_indicator", termsDoc.getAttribute("/OTHER_REQ_DOC_1_INDICATOR"));
					terms.setAttribute("other_req_doc_1_name", termsDoc.getAttribute("/OTHER_REQ_DOC_1_NAME"));
					terms.setAttribute("other_req_doc_1_originals",termsDoc.getAttribute("/OTHER_REQ_DOC_1_ORIGINALS"));
					terms.setAttribute("other_req_doc_1_copies",termsDoc.getAttribute("/OTHER_REQ_DOC_1_COPIES"));
					terms.setAttribute("other_req_doc_1_text",termsDoc.getAttribute("/OTHER_REQ_DOC_1_TEXT"));
					terms.setAttribute("other_req_doc_2_indicator", termsDoc.getAttribute("/OTHER_REQ_DOC_2_INDICATOR"));
					terms.setAttribute("other_req_doc_2_name", termsDoc.getAttribute("/OTHER_REQ_DOC_2_NAME"));
					terms.setAttribute("other_req_doc_2_originals",termsDoc.getAttribute("/OTHER_REQ_DOC_2_ORIGINALS"));
					terms.setAttribute("other_req_doc_2_copies",termsDoc.getAttribute("/OTHER_REQ_DOC_2_COPIES"));
					terms.setAttribute("other_req_doc_2_text",termsDoc.getAttribute("/OTHER_REQ_DOC_2_TEXT"));
					terms.setAttribute("other_req_doc_3_indicator", termsDoc.getAttribute("/OTHER_REQ_DOC_3_INDICATOR"));
					terms.setAttribute("other_req_doc_3_name", termsDoc.getAttribute("/OTHER_REQ_DOC_3_NAME"));
					terms.setAttribute("other_req_doc_3_originals",termsDoc.getAttribute("/OTHER_REQ_DOC_3_ORIGINALS"));
					terms.setAttribute("other_req_doc_3_copies",termsDoc.getAttribute("/OTHER_REQ_DOC_3_COPIES"));
					terms.setAttribute("other_req_doc_3_text",termsDoc.getAttribute("/OTHER_REQ_DOC_3_TEXT"));
					terms.setAttribute("other_req_doc_4_indicator", termsDoc.getAttribute("/OTHER_REQ_DOC_4_INDICATOR"));
					terms.setAttribute("other_req_doc_4_name", termsDoc.getAttribute("/OTHER_REQ_DOC_4_NAME"));
					terms.setAttribute("other_req_doc_4_originals",termsDoc.getAttribute("/OTHER_REQ_DOC_4_ORIGINALS"));
					terms.setAttribute("other_req_doc_4_copies",termsDoc.getAttribute("/OTHER_REQ_DOC_4_COPIES"));
					terms.setAttribute("other_req_doc_4_text",termsDoc.getAttribute("/OTHER_REQ_DOC_4_TEXT"));
					//IR T36000018528 Start- set place of expiry and bank charges type values
					terms.setAttribute("place_of_expiry",termsDoc.getAttribute("/PLACE_OF_EXPIRY"));
					terms.setAttribute("bank_charges_type",termsDoc.getAttribute("/BANK_CHARGES_TYPE"));
					//IR T36000018528 end
		        }
		      }

		      DocumentHandler termsPartyResultDoc = DatabaseQueryBean.getXmlResultSet(SELECT_TERMS_PARTY_SQL, false, new Object[]{});
		      if( termsPartyResultDoc != null )
		      {
		        List<DocumentHandler> termsPartyVector = termsPartyResultDoc.getFragmentsList("/ResultSetRecord/");
		        if( termsPartyVector.get(0) != null )
		        {
		        	termsPartyDoc = ((DocumentHandler)termsPartyVector.get(0));
		        	firstTermsParty.setAttribute("phone_number", termsPartyDoc.getAttribute("/PHONE_NUMBER"));
		        }
		      }

		      // DK IR T36000015588 Rel 8.2 04/18/2013  Ends

        }
       

        //check if the benificiary name for the po line items matches the name
        //of a party owned by user's coproration
        String sql = "select party_oid from party where name = ? and p_owner_org_oid = ? " ;
        

        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql,false, new Object[]{benName,inputDoc.getAttribute("/ownerOrg") });

        if (resultSet == null) //the party was not found
        {
            LOG.debug("No party match found for ben_name." +
                    " Setting termsParty name to -> {}" , benName);
            termsParty.setAttribute("name", benName);
        }

        else //the party was found, copy data to TermsParty
        //copy the Party info to the new TermsParty
        {
            LOG.debug("resultSet is --> {}" , resultSet.toString());
            partyOid = resultSet.getAttribute("/ResultSetRecord(0)/PARTY_OID");
            LOG.debug("partyOid -> {}" , partyOid);

            Party party = (Party) this.createServerEJB("Party");
            party.getData(Long.parseLong(partyOid));

            termsParty.copyFromParty(party);

            // If the party has a designated bank, pull in its data as the advising bank
            if(!StringFunction.isBlank(party.getAttribute("designated_bank_oid")))
            {
                // Load the designated bank from the database
                Party desigBank = (Party) this.createServerEJB("Party");
                desigBank.getData(Long.parseLong(party.getAttribute("designated_bank_oid")));

                TermsParty advisingBank;

                if(StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty")))
                {
                    terms.newComponent("ThirdTermsParty");
                    advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
                }
                else
                    advisingBank = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

                advisingBank.copyFromParty(desigBank);
            }
        }
        if (TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)){
	  		ArMatchingRule rule ;
			InvoicesSummaryData invoice =
				(InvoicesSummaryData) this.createServerEJB("InvoicesSummaryData",
						Long.parseLong((String)poOids.get(0)));
			rule =invoice.getMatchingRule();

			if(rule!=null){
			TermsParty firstTermsParty ;
			try{
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}catch(RemoteException | AmsException e){
				long firstTermsPartyOid = terms.newComponent("FirstTermsParty");
				firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			}
			
			firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			}

        }
        if (TradePortalConstants.TRADE_LOAN_REC.equals(inputDoc.getAttribute("/financeType"))){
	    	CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization");
	    	corpOrg.getData(Long.parseLong(inputDoc.getAttribute("/ownerOrg")));
	    	String attributeValue = corpOrg.getAttribute("inv_percent_to_fin_trade_loan");

	    	BigDecimal finPCTValue = BigDecimal.ZERO;
	    	if ((attributeValue != null)&&(!attributeValue.equals(""))) {
	    		finPCTValue = (new BigDecimal(attributeValue)).divide(new BigDecimal(100.00));
	    	}
	    	if (finPCTValue.compareTo(BigDecimal.ZERO) != 0){
	    		BigDecimal tempAmount;
	    		BigDecimal termAmount = terms.getAttributeDecimal("amount") ;
	    		tempAmount = finPCTValue.multiply(termAmount);
				String trnxCurrencyCode = transaction
				.getAttribute("copy_of_currency_code");
				int numberOfCurrencyPlaces = 2;

				if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
					numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
							.getRefDataMgr().getAdditionalValue(
									TradePortalConstants.CURRENCY_CODE,
									trnxCurrencyCode));
				}
				tempAmount = tempAmount.setScale(numberOfCurrencyPlaces,
						BigDecimal.ROUND_HALF_UP);
	    		terms.setAttribute("amount", String.valueOf(tempAmount));
	    	}


        }
        // Set the counterparty for the instrument and some "copy of" fields
        this.setAttribute("a_counter_party_oid", terms.getAttribute("c_FirstTermsParty"));
        transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
        transaction.setAttribute("instrument_amount", terms.getAttribute("amount"));
        this.setAttribute("copy_of_instrument_amount", transaction.getAttribute("instrument_amount"));

        if(terms.isAttributeRegistered("expiry_date"))
        {
            this.setAttribute("copy_of_expiry_date", terms.getAttribute("expiry_date"));
        }

        if(terms.isAttributeRegistered("reference_number"))
        {
            this.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number"));
        }

        LOG.debug("Saving instrument with oid ' {} '...." , this.getAttribute("instrument_oid"));
        this.save(false);//save without validate
        LOG.debug("Instrument saved");

        return outputDoc;
    }

    
   
    
    /**
     * this methos is used to set transaction status and insert user detail in panel Authorizer.
     * if procceedToAuth = AUTHFALSE, it will return false and authorize transaction mediator bean will take care and 
     *                     will set status as authorized fail.
     *                     AUTHPART, set the transaction status as partial auth and insert user entry in panel authorizer.
     *                     AUTHTRUE, set transaction status is authorize and inser user entry in panel authorizer.
     *  
     * @param panelAuthTransactionStatus status after processing panel group logic on transaction
     * @param transactionType transaction type which is processed
     * @param userOid login user oid
     * @param transaction transaction business object
     * @param user login user business object
     * @param refDataMgr
     * @param procceedToAuth true - proceed with saving data
     * @return retun result to process transaction in AuthorizaTransaction mediator bean.
     * @throws RemoteException
     * @throws AmsException
     */
    private String processPanelAuthResults(String panelAuthTransactionStatus, String transactionType, String userOid, Transaction transaction, 
			User user, ReferenceDataManager refDataMgr, boolean procceedToAuth, boolean isSubsidiaryTransAccess) throws RemoteException, AmsException {

		// if this is failed auth attempt altogether, just return false so
		// transactiuon will be set to Failed.
		if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
			return TradePortalConstants.AUTHFALSE;
		} else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
           
			try {
				// set the transaction status as partial authorized
				transaction.setAttribute("transaction_status", TransactionStatus.PARTIALLY_AUTHORIZED);
				transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
				
				int isSuccess = this.save();
				
				if(isSuccess >= 0){
					
				 this.getErrorManager().issueError(
							getClass().getName(),
							TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
							getAttribute("complete_instrument_id"),
							refDataMgr.getDescr("TRANSACTION_TYPE",
						    transactionType, csdb.getLocaleName()));	
					
				  return TradePortalConstants.AUTHPARTL;
				  
				}else{
					
				  return TradePortalConstants.AUTHFALSE;	
				}
			
			} catch (AmsException exc) {
				LOG.error("InstrumentBean::authorize::panel: Exception: " ,exc);
				throw (exc);
			}

		}

		// Requirements are met: Transaction is Authorized. Update
		// statuses/Transaction
		// Authorization fields and prepare to send a message to OTL
		else if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {

			authorize(transaction, user.getAttribute("locale"), user.getAttribute("timezone"), procceedToAuth);
			return TradePortalConstants.AUTHTRUE;
			
		} else {
			// can't be, we accounted for all the return cases
			// reserved for future use
			return TradePortalConstants.AUTHCONTINUE;
		}
	}
    
    /**
     * This method perform beneficiary level panel authorization.
     * @param transaction
     * @param user
     * @param instrumentType
     * @param isSubsidiaryAccessTab
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private String processBeneficiaryLevelPanelAuth(Transaction transaction, User user, String instrumentType, boolean isSubsidiaryAccessTab) 
    throws RemoteException, AmsException{
    	
    	String panelAuthTransactionStatus = TradePortalConstants.AUTHFALSE;
    	String panelRangeOid[] = null;
    	String dmstPmtPanelAuthRaneOid[] = null;
    	String panelGroupOid = transaction.getAttribute("panel_auth_group_oid");
		String panelOplockVal = transaction.getAttribute("panel_oplock_val");
		String userPanelAuthLevel = PanelAuthUtility.getPanelCode(isSubsidiaryAccessTab, user);
		
		// select all beneficaries group which has status other than authorization.
		StringBuilder sqlStatement = new StringBuilder(
				"SELECT a_panel_auth_range_oid, dmst_pmt_panel_auth_range_oid FROM dmst_pmt_panel_auth_range WHERE p_transaction_oid = ?");
		sqlStatement.append(" AND (auth_status IS NULL OR auth_status != 'AUTHORIZED')");
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});
		
		if (resultsDoc != null) {
			List<DocumentHandler> rangeOidList = resultsDoc.getFragmentsList("/ResultSetRecord");
			panelRangeOid = new String[rangeOidList.size()];
			dmstPmtPanelAuthRaneOid = new String[rangeOidList.size()];
			int i = 0;
			for (DocumentHandler doc : rangeOidList) {
				panelRangeOid[i] = doc.getAttribute("/A_PANEL_AUTH_RANGE_OID");
				dmstPmtPanelAuthRaneOid[i] = doc.getAttribute("/DMST_PMT_PANEL_AUTH_RANGE_OID");
				i++;
			}
		}
		else return panelAuthTransactionStatus;
		
		ErrorManager panelAuthProcessorErrorMgr = new ErrorManager();
		PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, instrumentType, panelAuthProcessorErrorMgr);
		panelAuthProcessor.init(panelGroupOid, panelRangeOid, panelOplockVal);
		DmstPmtPanelAuthRange dmstPmtPanelAuthRange ;
		List<String> BeneficiariesPanelAuthStatus = new ArrayList<>();
		
		for (int i = 0; i < panelRangeOid.length; i++) {
			dmstPmtPanelAuthRange = (DmstPmtPanelAuthRange) transaction.getComponentHandle("DmstPmtPanelAuthRangeList",
							Long.parseLong(dmstPmtPanelAuthRaneOid[i]));
			panelAuthTransactionStatus = panelAuthProcessor.process(dmstPmtPanelAuthRange, panelRangeOid[i], isSubsidiaryAccessTab);
			BeneficiariesPanelAuthStatus.add(panelAuthTransactionStatus);
			if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
				dmstPmtPanelAuthRange.setAttribute("auth_status", TransactionStatus.PARTIALLY_AUTHORIZED);						
			} else if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
				dmstPmtPanelAuthRange.setAttribute("auth_status", TransactionStatus.AUTHORIZED);
			}
		}

		// if list contain any status as 'Partial' or 'Full Auth and Fail both', set transaction status as partial
		if(BeneficiariesPanelAuthStatus.contains(TradePortalConstants.AUTHPARTL) || 
				(BeneficiariesPanelAuthStatus.contains(TradePortalConstants.AUTHTRUE) && BeneficiariesPanelAuthStatus.contains(TradePortalConstants.AUTHFALSE))){
			panelAuthTransactionStatus = TradePortalConstants.AUTHPARTL; 
			
			// set next awaiting approvals
			String panelApprovalsList = panelAuthProcessor.getPanelAuthNextApprovals(transaction, isSubsidiaryAccessTab);
			transaction.setAttribute("panel_auth_next_approvals", panelApprovalsList);
			
			// display message if it is partial authorized
			this.getErrorManager().issueError(getClass().getName(),
	    	    		TradePortalConstants.PANEL_AUTH_REQS_NOT_MET, userPanelAuthLevel);
			
		//if list has all status as fail, then set as fail
		} else if(BeneficiariesPanelAuthStatus.contains(TradePortalConstants.AUTHFALSE)){
			panelAuthTransactionStatus = TradePortalConstants.AUTHFALSE;
			// if final status of all beneficiary is fail, then only add error in Instrument Error Manager
			this.getErrorManager().addErrors(panelAuthProcessorErrorMgr.getIssuedErrorsList()); // Nar IR-T36000020087
			
		// else set as fully Auth as all are authorized successfully.
		}else{
			panelAuthTransactionStatus = TradePortalConstants.AUTHTRUE;
		}
		
		return panelAuthTransactionStatus;
    }
    
    /**
     * This method perform transaction level panel authorization.
     * @param transaction
     * @param user
     * @param instrumentType
     * @param isSubsidiaryAccessTab
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private String processTransactionLevelPanelAuth(Transaction transaction, User user, String instrumentType, boolean isSubsidiaryAccessTab) 
    throws RemoteException, AmsException{
    	
    	String panelAuthTransactionStatus = TradePortalConstants.AUTHFALSE;
		String panelGroupOid = transaction.getAttribute("panel_auth_group_oid");
		String panelOplockVal = transaction.getAttribute("panel_oplock_val");
		String rangeOid = transaction.getAttribute("panel_auth_range_oid");
		String[] panelRangeOid = new String[] { rangeOid };
		PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, instrumentType, this.getErrorManager());
		panelAuthProcessor.init(panelGroupOid, panelRangeOid, panelOplockVal);
		panelAuthTransactionStatus = panelAuthProcessor.process(transaction, rangeOid, isSubsidiaryAccessTab);
		
		// if panel auth process staus is partial auth, then set remaining panel Auth level.
		if(TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)){
			String panelApprovalsList = panelAuthProcessor.getPanelAuthNextApprovals(transaction, isSubsidiaryAccessTab);
			transaction.setAttribute("panel_auth_next_approvals", panelApprovalsList);
		}
		
		return panelAuthTransactionStatus;
    }
    

	/**
	 * Invokes a method on InvoicePaymentInstructions to add new Payee to a Loan Request
	 * Transaction.
	 *
	 * @param originalTransOid  java.lang.String - the source transaction oid
	 * @param newTransactionOid  java.lang.String - the target (new) transaction oid
	 * @return void
	 */
	private void createNewInvPaymentInstructions (String originalTransOid, String newTransactionOid)
	throws RemoteException, AmsException
	{
		InvoicePaymentInstructions invoicePaymentInstructions = (InvoicePaymentInstructions) createServerEJB("InvoicePaymentInstructions");

		invoicePaymentInstructions.createNewInvPaymentInstructions(originalTransOid, newTransactionOid);
		
	}

 /**
   	* This method updates the status of the transaction to Processed by bank for Issue/Amend
   	* and converted for Payment,
   	* or Failed through a series of validations.  It returns a boolean
   	* indicating if the transaction status should be set to Failed.  The
   	* Failed status cannot be set here because this is a transactional method
   	* that rwill rollback when any error messages are issued.
   	* If the transaction is Authorized, a new outgoing interface is created.
   	*
   	* This is a transactional method.  An assumption made is that the user has
   	* the instrument locked currently.
   	*
   	* @param transactionOid java.lang.String - the transaction oid to delete
   	* @param userOid  java.lang.String - the user oid used to delete transaction
   	* @param securityRights java.lang.String - the user's security rights
   	*  @param convertAtParentViewOrSubs java.lang.String - check the setting if the access mode is subsidiary
     * @param checkTransactionStatus
     * @param params
   	* @return boolean - false if authorizing a transaction is unsuccessful and the
   	* 			transaction status has to set to be set to FAILED,
   	*                   true if the transaction successfully saves OR if the
   	*                   the authorization fails BUT transaction should not be
   	*                   set to FAILED.
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
      */
      public boolean convertTransaction(String transactionOid, String userOid, String convertAtParentViewOrSubs, String securityRights, boolean checkTransactionStatus, HashMap params) throws RemoteException, AmsException
      {

    	  boolean isConverted ;
    	  String instrumentType = getAttribute("instrument_type_code");
    		//Transaction cannot be created/modified because the instrument has a status of Deactivated.
    		if (TradePortalConstants.INSTR_STATUS_DEACTIVATED.equals(getAttribute("instrument_status")))
    			{
    			   	//ERROR
    	            LOG.debug("NTM_INSTR_DEACTIVATED error");
    	            getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NTM_INSTR_DEACTIVATED);
    	            return false;
    			}
    		ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();

    		String convertText = getResourceManager().getText("TransactionAction.Convert",
    		    TradePortalConstants.TEXT_BUNDLE);

    		String convertedText = getResourceManager().getText("TransactionAction.Converted",
    		    TradePortalConstants.TEXT_BUNDLE);

    		Transaction transaction = (Transaction)
    			    getComponentHandle("TransactionList", Long.parseLong(transactionOid));
    		Terms terms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
    		String transactionTypeCode = transaction.getAttribute("transaction_type_code");
    		User user = (User) createServerEJB("User", Long.parseLong(userOid));
    		
			 LOG.debug("convertTransaction:checkTransactionStatus: {}",checkTransactionStatus);
    		if (checkTransactionStatus && !transaction.canAuthorizeState())
    		{
    		    // issue error that transaction is not in an authorizable state
    		    this.getErrorManager().issueError(getClass ().getName (),
    			    				TradePortalConstants.TRANSACTION_CANNOT_PROCESS ,
    								getAttribute("complete_instrument_id"),
    								refDataMgr.getDescr("TRANSACTION_TYPE",
    			   										 transactionTypeCode,
    			   										 csdb.getLocaleName()),
    								refDataMgr.getDescr("TRANSACTION_STATUS",
    			   										 transaction.getAttribute("transaction_status"),
    			   										 csdb.getLocaleName()),
    								convertedText);
    		    // return true to NOT set the transaction status to failed
    		    return true;
    		}
    		if (!(user.getAttribute("ownership_level")).equals(TradePortalConstants.OWNER_CORPORATE))
    		{
    		    this.getErrorManager().issueError(getClass().getName(),
    								TradePortalConstants.ADMIN_USER_CANNOT_AUTHORIZE,
    								user.getAttribute("first_name"),
    								user.getAttribute("last_name"));

    		    // return false to set the transaction status to failed
    		    return false;
    		}
    		else if (!SecurityAccess.canAuthorizeConvertInstrument(securityRights,
    			getAttribute("instrument_type_code"), transactionTypeCode))
    		{
    		    // issue error that user does not have authorize transaction authority
    		    // for the given instrument type
    		    this.getErrorManager().issueError(getClass ().getName (),
    								TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
    								getAttribute("complete_instrument_id"),
    								refDataMgr.getDescr("TRANSACTION_TYPE",
    													transactionTypeCode,
    													csdb.getLocaleName()),
    								convertText);

    		    // return false to set the transaction status to failed
    		    return false;

    		}
			Terms custEnteredTerms = (Terms)transaction.getComponentHandle("CustomerEnteredTerms");
			DocumentHandler custTermsDoc = new DocumentHandler();
			custEnteredTerms.populateXmlDoc(custTermsDoc);
			Terms bankReleasedTerms ;
			long bankReleasedTermsOid ;
			try{
				bankReleasedTerms = (Terms) transaction.getComponentHandle("BankReleasedTerms");
				bankReleasedTermsOid = bankReleasedTerms.getAttributeLong("terms_oid");
			}catch(RemoteException | AmsException e){

				bankReleasedTermsOid = transaction.newComponent("BankReleasedTerms");
				bankReleasedTerms = (Terms) transaction.getComponentHandle("BankReleasedTerms");
			}
			String benOid = custTermsDoc.getAttribute("/Terms/c_FirstTermsParty");
			String applicantOid = custTermsDoc.getAttribute("/Terms/c_SecondTermsParty");
			custTermsDoc.setAttribute("/Terms/c_FirstTermsParty", applicantOid);
			custTermsDoc.setAttribute("/Terms/c_SecondTermsParty", benOid);
			bankReleasedTerms.populateFromXmlDoc(custTermsDoc);
			bankReleasedTerms.setAttribute("terms_source_type", "BankReleasedTerms");
			transaction.setAttribute("c_BankReleasedTerms",String.valueOf(bankReleasedTermsOid));

    		//handle convert processing of Payment transactions.
    		if (TradePortalConstants.CONVERTED_TRANSACTION_PAYMENT.equals(transactionTypeCode)){
    			String closeAndDeactivate = terms.getAttribute("close_and_deactivate");
    			if (TradePortalConstants.INDICATOR_YES.equals(closeAndDeactivate)){
    				transaction.setAttribute("available_amount", "0");
    				if (StringFunction.isBlank(transaction.getAttribute("copy_of_amount"))){
    					transaction.setAttribute("copy_of_amount", "0");
					}
    				transaction.setAttribute("liability_amt_in_limit_curr", "0");
    				transaction.setAttribute("transaction_status",TransactionStatus.PROCESSED_BY_BANK);
    				this.setAttribute("active_transaction_oid",transaction.getAttribute("transaction_oid"));
    				this.setAttribute("instrument_status", TransactionType.DEACTIVATE);
    			} else{

   		    		Transaction activeTransaction = (Transaction)
		    			    getComponentHandle("TransactionList", Long.parseLong(this.getAttribute("active_transaction_oid")));
		    		String secondTermsParty = bankReleasedTerms.getAttribute("c_SecondTermsParty");
		    		if (StringFunction.isBlank(secondTermsParty)){
		    			Terms actTransBankReleasedTerms = (Terms) activeTransaction.getComponentHandle("BankReleasedTerms");
		    			String secondTermsPartyOid = actTransBankReleasedTerms.getAttribute("c_SecondTermsParty");
		    			bankReleasedTerms.setAttribute("c_SecondTermsParty", secondTermsPartyOid);
		    		}

    				String amountValue ;
    				BigDecimal originalAmount ;
    				BigDecimal  offsetAmount ;
    				  try {
                              if (TransactionType.PAYMENT.equals(activeTransaction.getAttribute("transaction_type_code"))){
                                  amountValue = activeTransaction.getAttribute("available_amount");
                              }else{
        					     amountValue = activeTransaction.getAttribute("instrument_amount");
                              }
                              originalAmount = new BigDecimal(amountValue);
					  } catch (Exception e) {
    					     originalAmount = BigDecimal.ZERO;
    					  }
    				     try {
    				         amountValue = transaction.getAttribute("copy_of_amount");
    				         offsetAmount = new BigDecimal(amountValue);
    				      } catch (Exception e) {
    				         offsetAmount = BigDecimal.ZERO;
    				      }
    				     BigDecimal calculatedTotal = originalAmount.add(offsetAmount);
    				     if (calculatedTotal.signum() == -1){
    				   	  calculatedTotal = BigDecimal.ZERO;
    				     }
    				     transaction.setAttribute("instrument_amount", activeTransaction.getAttribute("instrument_amount"));
 	    				 transaction.setAttribute("available_amount", calculatedTotal.toString());
	    				 transaction.setAttribute("liability_amt_in_limit_curr", calculatedTotal.toString());
	    				 transaction.setAttribute("copy_of_currency_code",activeTransaction.getAttribute("copy_of_currency_code"));
				 		 transaction.setAttribute("transaction_status",TransactionStatus.PROCESSED_BY_BANK);

				 		 this.setAttribute("active_transaction_oid",transaction.getAttribute("transaction_oid"));
				 		params.put("transaction_status", TransactionStatus.PROCESSED_BY_BANK);
    			}
    		}else{

    			if (TransactionType.ISSUE.equals(transactionTypeCode) || TransactionType.AMEND.equals(transactionTypeCode)){

    				String amountValue ;
    				BigDecimal originalAmount;
    				BigDecimal  offsetAmount ;
    				BigDecimal calculatedTotal = null;
    				if (TransactionType.AMEND.equals(transactionTypeCode)){
    		    		Transaction activeTransaction = (Transaction)
    		    			    getComponentHandle("TransactionList", Long.parseLong(this.getAttribute("active_transaction_oid")));
    		    		String secondTermsParty = bankReleasedTerms.getAttribute("c_SecondTermsParty");
    		    		if (StringFunction.isBlank(secondTermsParty)){
    		    			Terms actTransBankReleasedTerms = (Terms) activeTransaction.getComponentHandle("BankReleasedTerms");
    		    			String secondTermsPartyOid = actTransBankReleasedTerms.getAttribute("c_SecondTermsParty");
    		    			bankReleasedTerms.setAttribute("c_SecondTermsParty", secondTermsPartyOid);
    		    		}
    				  try {
                              if (TransactionType.PAYMENT.equals(activeTransaction.getAttribute("transaction_type_code"))){
                                  amountValue = activeTransaction.getAttribute("available_amount");
                              }else{
                                 amountValue = activeTransaction.getAttribute("instrument_amount");
                              }
    					     originalAmount = new BigDecimal(amountValue);
					  } catch (Exception e) {
    					     originalAmount = BigDecimal.ZERO;
    					  }
    				     try {
    				         amountValue = transaction.getAttribute("copy_of_amount");
    				         offsetAmount = new BigDecimal(amountValue);
    				      } catch (Exception e) {
    				         offsetAmount = BigDecimal.ZERO;
    				      }

    				     calculatedTotal = originalAmount.add(offsetAmount);

 	    				 transaction.setAttribute("instrument_amount", calculatedTotal.toString());
						 transaction.setAttribute("copy_of_currency_code",activeTransaction.getAttribute("copy_of_currency_code"));
						 this.setAttribute("copy_of_instrument_amount", calculatedTotal.toString());


						 setIssueDate(custEnteredTerms.getAttribute("guar_valid_from_date_type"),custEnteredTerms,(Terms) activeTransaction.getComponentHandle("CustomerEnteredTerms"));
						 setExpiryDate(custEnteredTerms.getAttribute("guar_expiry_date_type"),custEnteredTerms);
    				}else if (TransactionType.ISSUE.equals(transactionTypeCode)){
    					this.setAttribute("instrument_status", TradePortalConstants.INSTR_STATUS_ACTIVE);
    					calculatedTotal = new BigDecimal(transaction.getAttribute("copy_of_amount"));
    					setIssueDate(custEnteredTerms.getAttribute("guar_valid_from_date_type"),custEnteredTerms,null);
    				}

    				 transaction.setAttribute("available_amount", calculatedTotal.toString());
    				 transaction.setAttribute("liability_amt_in_limit_curr", calculatedTotal.toString());
			 		 transaction.setAttribute("transaction_status",TransactionStatus.PROCESSED_BY_BANK);
			 		 this.setAttribute("active_transaction_oid",transaction.getAttribute("transaction_oid"));
			 		 params.put("transaction_status", TransactionStatus.PROCESSED_BY_BANK);

    			}
    		}

    	    this.getErrorManager().issueError(getClass().getName(),
    					TradePortalConstants.TRANSACTION_PROCESSED,
    					refDataMgr.getDescr("INSTRUMENT_TYPE",
    							instrumentType,
    							csdb.getLocaleName()) + " - " + getAttribute("complete_instrument_id"),
    					refDataMgr.getDescr("TRANSACTION_TYPE",
    						transactionTypeCode,
    						csdb.getLocaleName()),
    					convertedText);
		int saved = 0;
		try {
			saved = this.save();
		} catch (Exception e) {
			LOG.error("Exception saving instrument --> " , e);
		}

		isConverted = (saved == 1);
    	  return isConverted;
      }


	private void setIssueDate(String attribute, Terms custEnteredTerms, Terms activeCustEnteredTerms) throws RemoteException, AmsException {
		if (TradePortalConstants.CURRENT_VALIDITY_FROM_DATE.equals(attribute)){
			if (activeCustEnteredTerms == null)
				this.setAttribute("issue_date",custEnteredTerms.getAttribute("expiry_date"));
			else{
				this.setAttribute("issue_date",activeCustEnteredTerms.getAttribute("expiry_date"));
			}
		}else if (TradePortalConstants.DATE_OF_ISSUE.equals(attribute)){
			if(StringFunction.isBlank(this.getAttribute("issue_date")))
				this.setAttribute("issue_date",DateTimeUtility.getGMTDateTime());
		} else if (TradePortalConstants.OTHER_VALID_FROM_DATE.equals(attribute)){
			this.setAttribute("issue_date",custEnteredTerms.getAttribute("guar_valid_from_date"));

		}

	}

	private void setExpiryDate(String attribute, Terms custEnteredTerms) throws RemoteException, AmsException {
		if (TradePortalConstants.CURRENT_VALIDITY_TO_DATE.equals(attribute)){
			this.setAttribute("copy_of_expiry_date",this.getAttribute("copy_of_expiry_date"));
		}else if (TradePortalConstants.VALIDITY_DATE.equals(attribute)){
			this.setAttribute("copy_of_expiry_date",custEnteredTerms.getAttribute("expiry_date"));
		}
	}
	
	/**
	 * Added for Rel9.3.5 CR-1028
	 * This method is used to check if the particular instrumentType and transactionType are valid for the messages that do not need to be out on MQ
	 * @param transactionType
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public boolean checkTypesForDoNotSendMQ(String transactionType) throws RemoteException, AmsException
	   {
			LOG.debug("Enter isTransactionTypeValidForNoMQ( {} )",transactionType);

	        String instrumentTypeCode = this.getAttribute("instrument_type_code");
	        if (transactionType.equals(TransactionType.ISSUE))
	                 {
	                  if(instrumentTypeCode.equals(InstrumentType.IMPORT_DLC) ||
	                     instrumentTypeCode.equals(InstrumentType.STANDBY_LC) ||
	                     instrumentTypeCode.equals(InstrumentType.GUARANTEE) ||
	                     instrumentTypeCode.equals(InstrumentType.SHIP_GUAR) ||
	                     instrumentTypeCode.equals(InstrumentType.LOAN_RQST))
	                	 return true;
	                 }
	        else if (transactionType.equals(TransactionType.AMEND))
	                 {
	        		  if(instrumentTypeCode.equals(InstrumentType.IMPORT_DLC) ||
	                     instrumentTypeCode.equals(InstrumentType.STANDBY_LC) ||
	                     instrumentTypeCode.equals(InstrumentType.GUARANTEE))
	                 	 return true;
	                 }
	        else if (transactionType.equals(TransactionType.RELEASE))
		    {
			  if(instrumentTypeCode.equals(InstrumentType.AIR_WAYBILL))
		         return true;
		    }
	        else{
	        	return false;
	        }
	      return false;  
	   }
	
	  /**
	   * This method is used to set work item type for settlement instruction message and Request/Response.
	   * @param newTrans
	   * @param instrumentType
	   * @param workItemType
	   * @throws RemoteException
	   * @throws AmsException
	   */
	  private void setSettlmentWorkItemType(Transaction newTrans, String transType, String instrumentType, String workItemType) throws RemoteException, AmsException {
		  
		if ( TransactionType.SIR.equals(transType) ) { // SIR transaction will always has work item type as 'WSR'
			newTrans.setAttribute("settle_instr_work_item_type", TradePortalConstants.SETTLEMENT_WORK_ITEM_TYPE_RESPONSE);
		} else {
		  if ( StringFunction.isNotBlank(workItemType) ) {
	  		newTrans.setAttribute("settle_instr_work_item_type",workItemType );
	  	  } else {
	  		if ( InstrumentType.IMPORT_COL.equals(instrumentType) ) {
	  			newTrans.setAttribute("settle_instr_work_item_type", TradePortalConstants.SETTLEMENT_WORK_ITEM_TYPE_PAYMENT);
	  		} else {
	  			newTrans.setAttribute("settle_instr_work_item_type", TradePortalConstants.SETTLEMENT_WORK_ITEM_TYPE_LIQUIDATE);
	  		}
	  	}
	  }
		
	}
	        
}