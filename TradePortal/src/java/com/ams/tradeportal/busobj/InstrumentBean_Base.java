


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Subclass of the InstrumentData business object that represents instruments
 * that are created by the corporate customers of the Trade Portal.    Each
 * instrument has one to many transactions.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InstrumentBean_Base extends InstrumentDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* instrument_num - The instrument number assigned to this instrument.  This is generated from
         the client bank's instrument number sequence. */
      attributeMgr.registerAttribute("instrument_num", "instrument_num");

      /* instrument_prefix - The instrument number prefix assigned to this instrument.  This is determined
         from the operational bank organization to which this instrument is associated. */
      attributeMgr.registerAttribute("instrument_prefix", "instrument_prefix");

      /* instrument_suffix - The instrument number suffix assigned to this instrument.  This is determined
         from the operational bank organization to which this instrument is associated. */
      attributeMgr.registerAttribute("instrument_suffix", "instrument_suffix");

      /* complete_instrument_id - The prefix, instrument number, and suffix concatenated together. */
      attributeMgr.registerAttribute("complete_instrument_id", "complete_instrument_id");

      /* instrument_status - The lifecycle status of an instrument. */
      attributeMgr.registerReferenceAttribute("instrument_status", "instrument_status", "INSTRUMENT_STATUS");

      /* from_express_template - Set to Yes if this instrument was created from an express template.   If
         it is created from an express template, certain fields on the transaction
         form may be read-only. */
      attributeMgr.registerAttribute("from_express_template", "from_express_template", "IndicatorAttribute");

      /* copy_of_ref_num - A copy of the most recently updated transactions' reference number.   This
         copy must exist so that instrument searching can occur based on the reference
         number. */
      attributeMgr.registerAttribute("copy_of_ref_num", "copy_of_ref_num");

      /* active_transaction_oid - A pointer to the active transaction of the instrument.   This is not a jPylon
         association because the possibility exists that the Instrument business
         object could be created at the same time as the active transaction's Transaction
         business object.   If this were a true jPylon association, it would always
         fail because the Transaction business object would not exist in the database
         yet.  To avoid this situation, this is just a number attribute instead of
         an assocation */
      attributeMgr.registerAttribute("active_transaction_oid", "active_transaction_oid", "NumberAttribute");

      /* issue_date - The issue date of the instrument.  This field is only updated by the middleware. */
      attributeMgr.registerAttribute("issue_date", "issue_date", "DateTimeAttribute");
      attributeMgr.registerAlias("issue_date", getResourceManager().getText("InstrumentBeanAlias.issue_date", TradePortalConstants.TEXT_BUNDLE));

      /* copy_of_expiry_date - A copy of the most recently updated transaction's expiry date.   This copy
         must exist so that instrument searching can occur based on the expiry date. */
      attributeMgr.registerAttribute("copy_of_expiry_date", "copy_of_expiry_date", "DateTimeAttribute");

      /* copy_of_instrument_amount - The current amount for this instrument.  When the original transaction of
         the instrument is saved, the transaction's instrument amount is copied to
         this field.   For all other transactions, the transaction's instrument amount
         field is only copied when the middleware updates a transaction. */
      attributeMgr.registerAttribute("copy_of_instrument_amount", "copy_of_instrument_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");

      /* opening_bank_ref_num - The reference number used by the opening bank. */
      attributeMgr.registerAttribute("opening_bank_ref_num", "opening_bank_ref_num");

      /* purge_date - The date/time at which this instrument was purged in OTL.  This is set to
         null if no purging has been requested yet for this instrument.  See the
         instrument_purge_type on client_bank. */
      attributeMgr.registerAttribute("purge_date", "purge_date", "DateTimeAttribute");

      /* copy_of_confirmation_ind - Determines whether the Instrument has been confirmed

         Yes=Instrument has been confirmed */
      attributeMgr.registerAttribute("copy_of_confirmation_ind", "copy_of_confirmation_ind", "IndicatorAttribute");

      /* present_docs_within_days - present_docs_within_days - The number of days after shipment that documents
         must be presented.  This         attribute is updated by incoming messages
         from bank. */
      attributeMgr.registerAttribute("present_docs_within_days", "present_docs_within_days", "NumberAttribute");

      /* vendor_id - A 15 character alphanumeric value representing the vendor ID associated
         with this instrument */
      attributeMgr.registerAttribute("vendor_id", "vendor_id");

      /* otl_po_invoice_portfolio_uoid - The original OTL UOID of the OTL_POInvoicePortfolio component.  We need
         this since the transaction may come in TP before the po_invoice_portfolio
         does.  We need to store the original uoid until the portfolio comes in and
         translate it to the oid association. */
      attributeMgr.registerAttribute("otl_po_invoice_portfolio_uoid", "otl_po_invoice_portfolio_uoid");

      /* otl_instrument_uoid - The UOID of the instrument in OTL DB.  This helps reporting that spans over
         the OTL &amp; TP db tables. */
      attributeMgr.registerAttribute("otl_instrument_uoid", "otl_instrument_uoid");

      /* client_bank_oid - Each instrument is associated to a corporate organization, which is then
         "owned" in the Trade Portal by a client bank.   This association is a shortcut
         to the client bank from instrument to make reporting more efficient. */
      attributeMgr.registerAssociation("client_bank_oid", "a_client_bank_oid", "ClientBank");

      /* corp_org_oid - Each instrument in related to a corporate organization.  This reflects the
         fact that the instrument was created by the corporate organization or deals
         with the business of that corporate organization. */
      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");

      /* op_bank_org_oid - When creating an instrument, the user must select the operational bank organization
         to which the instrument should be sent for processing.  This association
         represents that relationship. */
      attributeMgr.registerAssociation("op_bank_org_oid", "a_op_bank_org_oid", "OperationalBankOrganization");

      /* related_instrument_oid - Sometimes, instruments are related to other instruments.  This association
         represents this relationship. */
      attributeMgr.registerAssociation("related_instrument_oid", "a_related_instrument_oid", "Instrument");

      /* suppress_doc_link_ind - Suppress PDF Document link during transaction processing.  Only after the
	        transaction is successfully released in the TPS after a successful Denied
	        Party Compliance check has been executed, and the status of the transaction
	        in the Portal is updated to "Processed by Bank", will the links for the
	        Collection/Amendment Schedule and Bill of Exchange document be made available
	        in the Portal as part of the update process of the transaction. */
      attributeMgr.registerAttribute("suppress_doc_link_ind", "suppress_doc_link_ind", "IndicatorAttribute");//Vshah CR-452 - Register this new attribute

      /* confidential_ind - Indicates whether or not the instrument is confidential.

         Yes=template is a confidential template */
      attributeMgr.registerReferenceAttribute("confidential_indicator", "confidential_indicator", "CONFIDENTIAL_IND");

      /* fixed_payment_flag - Indicates whether or not the instrument is a fixed payment instrument.

         Yes=template is a fixed payment instrument */
      attributeMgr.registerAttribute("fixed_payment_flag", "fixed_payment_flag", "IndicatorAttribute");

	   //Srinivasu_D CR#269 Rel8.4 09/02/2013
	    attributeMgr.registerAttribute("converted_transaction_ind", "converted_transaction_ind", "IndicatorAttribute");
		   //RKAZI T3600038018 07/05/2015
	    attributeMgr.registerAttribute("template_oid", "template_oid", "NumberAttribute"); //RKAZI 07-07-2015 T36000038018 - Add
	    
	   //MEer Rel 9.3.5 CR-1029 
	    attributeMgr.registerAttribute("bank_instrument_id", "bank_instrument_id");
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* ActiveInstrumentPartyList - An instrument contains one or many ActiveInstrumentParty's after the instrument
         is approved by the bank.  The transactions that come through the middleware
         to TradePortal update the ActiveInstrumentParty's if neccessary.  This component
         is used for reporting purpose. */
      registerOneToManyComponent("ActiveInstrumentPartyList","ActiveInstrumentPartyList");
   }






}
