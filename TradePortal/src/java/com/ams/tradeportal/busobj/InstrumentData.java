package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Templates and instruments are very similar in data, and are stored in the
 * same database tables.
 * 
 * This is an abstract business object that is the superclass for both types
 * of instruments: templates and real instruments.   This business object contains
 * associations and attributes that are common to both templates and instruments.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InstrumentData extends TradePortalBusinessObject
{
 
}
