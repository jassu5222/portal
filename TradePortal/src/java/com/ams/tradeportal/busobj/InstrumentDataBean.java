 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrumentDataBean extends InstrumentDataBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentDataBean.class);
  /* 
   * Perform processing for a New instance of the business object
   */
   protected void userNewObject() throws AmsException, RemoteException
   {  
	/* Perform any New Object processing defined in the Ancestor class */
        super.userNewObject();

        // Default the language of issue to English
        this.setAttribute("language", TradePortalConstants.INSTRUMENT_LANGUAGE_ENGLISH); 
   }     
   
    /*
     * Overrides functionality of superclass' method.  Adds the value of template_flag
     * to the CSDB so that child components (Transaction, Terms, TermsParty, etc)
     * will know whether or not they belong to an instrument or a transaction. 
     *
     * @param componentIdentifier The identifier of the component as it was
     * registered in the object's registerComponents() method.
     * @return The objectID for the newly created component.
     *
     */
    public long newComponent (String componentIdentifier)
		   throws AmsException, RemoteException
	 {
	   if(getClientServerDataBridge() == null)
                setClientServerDataBridge(new ClientServerDataBridge());

           getClientServerDataBridge().setCSDBValue("template",this.getAttribute("template_flag"));

 	   return super.newComponent(componentIdentifier);
	 }

    /**
     * Overrides functionality of superclass' method.  Adds the value of template_flag
     * to the CSDB so that child components (Transaction, Terms, TermsParty, etc)
     * will know whether or not they belong to an instrument or a transaction. 
     *
     * @param componentIdentifier The name of the component as it was registered
     * in the business object's registerComponents() method.
     */
    public EJBObject getComponentHandle (String componentIdentifier)
		   throws RemoteException, AmsException
	{
	  if(getClientServerDataBridge() == null)
                setClientServerDataBridge(new ClientServerDataBridge()); 
           
          getClientServerDataBridge().setCSDBValue("template",this.getAttribute("template_flag"));

	  return super.getComponentHandle (componentIdentifier);
	}

    /**
     * Overrides functionality of superclass' method.  Adds the value of template_flag
     * to the CSDB so that child components (Transaction, Terms, TermsParty, etc)
     * will know whether or not they belong to an instrument or a transaction. 
     *
     * @param componentIdentifier The identifier of the component as it was registered
     * in the business object's registerComponents() method.
     * @param objectID The OID of the component object being requested
     * @return The remote interface for the object within the component list.
     *
     */
    public EJBObject getComponentHandle (String componentIdentifier, long objectId)
		   throws RemoteException, AmsException
 	{
	   if(getClientServerDataBridge() == null)
                setClientServerDataBridge(new ClientServerDataBridge()); 
           
           getClientServerDataBridge().setCSDBValue("template",this.getAttribute("template_flag"));

	   return super.getComponentHandle (componentIdentifier, objectId);
        }   
   
   
   
   
   
   
   
}
