package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Subclass of the InstrumentData business object that represents instruments
 * that are created by the corporate customers of the Trade Portal.    Each
 * instrument has one to many transactions.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InstrumentHome extends EJBHome
{
   public Instrument create()
      throws RemoteException, CreateException, AmsException;

   public Instrument create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
