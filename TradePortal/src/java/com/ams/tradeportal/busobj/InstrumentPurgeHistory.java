package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * When an instrument is purged from the Trade Portal database after a period
 * of time, a record is maintained in this table of the instruments that were
 * deleted.
 * 
 * This is populated by the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InstrumentPurgeHistory extends TradePortalBusinessObject
{   
}
