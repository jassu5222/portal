package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * When an instrument is purged from the Trade Portal database after a period
 * of time, a record is maintained in this table of the instruments that were
 * deleted.
 * 
 * This is populated by the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InstrumentPurgeHistoryHome extends EJBHome
{
   public InstrumentPurgeHistory create()
      throws RemoteException, CreateException, AmsException;

   public InstrumentPurgeHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
