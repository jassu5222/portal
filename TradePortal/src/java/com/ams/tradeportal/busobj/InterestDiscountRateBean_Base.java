

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Interest Discount Rate Class, Which will ultimately allow the Corporate
 * Customers to view Discount/Interest rates that will be applied on financed
 * invoices.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InterestDiscountRateBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InterestDiscountRateBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* interest_discount_rate_oid - Interest_discount_rate_oid - Unique Identifier */
      attributeMgr.registerAttribute("interest_discount_rate_oid", "interest_discount_rate_oid", "ObjectIDAttribute");

      /* currency_code - The currency code for which the Interest Discount Rate is being established. */
      attributeMgr.registerReferenceAttribute("currency_code", "currency_code", "CURRENCY_CODE");
      attributeMgr.registerAlias("currency_code", getResourceManager().getText("InterestDiscountRateBeanAlias.currency_code", TradePortalConstants.TEXT_BUNDLE));

      /* rate_type - The rate type for which the Interest Discount Rate is being established. */
      attributeMgr.registerReferenceAttribute("rate_type", "rate_type", "REFINANCE_RATE_TYPE");
      attributeMgr.registerAlias("rate_type", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_type", TradePortalConstants.TEXT_BUNDLE));

      /* call_rate - The Call Rate */
      attributeMgr.registerAttribute("call_rate", "call_rate", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("call_rate", getResourceManager().getText("InterestDiscountRateBeanAlias.call_rate", TradePortalConstants.TEXT_BUNDLE));

      /* rate_30_days -  */
      attributeMgr.registerAttribute("rate_30_days", "rate_30_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_30_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_30_days", TradePortalConstants.TEXT_BUNDLE));

      /* rate_60_days -  */
      attributeMgr.registerAttribute("rate_60_days", "rate_60_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_60_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_60_days", TradePortalConstants.TEXT_BUNDLE));

      /* rate_90_days -  */
      attributeMgr.registerAttribute("rate_90_days", "rate_90_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_90_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_90_days", TradePortalConstants.TEXT_BUNDLE));

      /* rate_120_days -  */
      attributeMgr.registerAttribute("rate_120_days", "rate_120_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_120_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_120_days", TradePortalConstants.TEXT_BUNDLE));

      /* rate_180_days -  */
      attributeMgr.registerAttribute("rate_180_days", "rate_180_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_180_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_180_days", TradePortalConstants.TEXT_BUNDLE));

      /* rate_360_days -  */
      attributeMgr.registerAttribute("rate_360_days", "rate_360_days", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("rate_360_days", getResourceManager().getText("InterestDiscountRateBeanAlias.rate_360_days", TradePortalConstants.TEXT_BUNDLE));

      /* num_interest_days - Number of interest days of currency. */
      attributeMgr.registerAttribute("num_interest_days", "num_interest_days", "NumberAttribute");
      attributeMgr.registerAlias("num_interest_days", getResourceManager().getText("InterestDiscountRateBeanAlias.num_interest_days", TradePortalConstants.TEXT_BUNDLE));

      /* effective_date - Effective of Interest Discount Rate in YYYYMMDD format. */
      attributeMgr.registerAttribute("effective_date", "effective_date", "DateTimeAttribute");
      attributeMgr.registerAlias("effective_date", getResourceManager().getText("InterestDiscountRateBeanAlias.effective_date", TradePortalConstants.TEXT_BUNDLE));

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* Pointer to the parent InterestDiscountRateGroup */
       attributeMgr.registerAttribute("interest_disc_rate_group_oid", "p_interest_disc_rate_group_oid", "ParentIDAttribute");

   }






}
