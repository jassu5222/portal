

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Interest Discount Rate Group
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InterestDiscountRateGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InterestDiscountRateGroupBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* interest_disc_rate_group_oid - interest_disc_rate_group_oid - Unique Identifier */
      attributeMgr.registerAttribute("interest_disc_rate_group_oid", "interest_disc_rate_group_oid", "ObjectIDAttribute");

      /* group_id - A set of letters that is used to identify the different group. */
      attributeMgr.registerAttribute("group_id", "group_id");
      attributeMgr.registerAlias("group_id", getResourceManager().getText("InterestDiscountRateGroupBeanAlias.group_id", TradePortalConstants.TEXT_BUNDLE));

      /* description - Description of the Interest Discount Rate Group */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.registerAlias("description", getResourceManager().getText("InterestDiscountRateGroupBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));

      /* short_description - Short Description of Interest Discount Rate Group */
      attributeMgr.registerAttribute("short_description", "short_description");
      attributeMgr.registerAlias("short_description", getResourceManager().getText("InterestDiscountRateGroupBeanAlias.short_description", TradePortalConstants.TEXT_BUNDLE));

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer) */
      attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");

      /* ownership_type - If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
      attributeMgr.registerReferenceAttribute("ownership_type", "ownership_type", "OWNERSHIP_TYPE");

      /* Pointer to the parent ReferenceDataOwner */
       attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* InterestDiscountRateList -  */
      registerOneToManyComponent("InterestDiscountRateList","InterestDiscountRateList");
   }






}
