
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Interest Discount Rate Class, Which will ultimately allow the Corporate
 * Customers to view Discount/Interest rates that will be applied on financed
 * invoices.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InterestDiscountRateHome extends EJBHome
{
   public InterestDiscountRate create()
      throws RemoteException, CreateException, AmsException;

   public InterestDiscountRate create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
