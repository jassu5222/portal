package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import com.ams.tradeportal.common.*;

public class InvOnlyCreateRuleBean extends InvOnlyCreateRuleBean_Base{
private static final Logger LOG = LoggerFactory.getLogger(InvOnlyCreateRuleBean.class);
	   public final static int MAX_VALUE_LENGTH = 140;
	   public final static String VALUE_SEPARATOR = "!";
	   
public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
{

	String name = getAttribute("name");
	if (null != name)
		name = name.trim();
	
	long myOrg = this.getAttributeLong("owner_org_oid");
	if (!isUnique("name", name, " and a_owner_org_oid = " + myOrg)) {
		this.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1, 
			TradePortalConstants.ALREADY_EXISTS, 
			name, attributeMgr.getAlias("name")); 
	}
    this.setAttribute("name", name);
    if(this.isNew)
     {
        this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime());
     }
	
	// Neither min nor max days is required.  However, if one is given,
	// both must be given and min days value must be less than max days value
	try {
		
		String min = getAttribute("due_or_pay_min_days");
		String max = getAttribute("due_or_pay_max_days");
	
		if (StringFunction.isBlank(min) && StringFunction.isBlank(max)) {
			// Both are blank.  This is OK.
		} else if (!StringFunction.isBlank(min) 
				&& !StringFunction.isBlank(max)) {
			// Both are not blank so test for valid numbers.	
			int minDays = getAttributeInteger("due_or_pay_min_days");
			int maxDays = getAttributeInteger("due_or_pay_max_days");
	
			if (minDays > maxDays) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.SHIPMENT_DAYS_WRONG_ORDER); 
			}
		} else {
			// One is blank and the other is not.
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1, 
				TradePortalConstants.SHIPMENT_DAYS_BOTH_REQUIRED);
		}
	} catch (AmsException e) {
		// This is probably because we have a bad number
		this.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1, 
			TradePortalConstants.SHIPMENT_DAYS_NOT_NUMERIC);
	} catch (Exception e) {
		LOG.error("Unexpected exception in LCCreationRule.userValidate: " , e);
	}
	
	// For the 6 criterion fields, only criterion 1 is required (this
	// is handled by jPylon required field logic).  For the remaining 5
	// items (i.e., 2-6), if the user gave a value for the data item or
	// value fields, then both fields are required.
	
	String data;
	String value;
	for (int i = 1; i <= 6; i++) {
		
		data = getAttribute("inv_data_item" + i);
		value = getAttribute("inv_data_item_val" + i);
		
		// Test for missing value in data or value fields.  If one is given,
		// the other is required.  We do not test the first data/value
		// because jPylon checks that (they are required fields).
		if (i > 1) {
			if (StringFunction.isNotBlank(data) && StringFunction.isBlank(value)) {
				// User gave data but no value
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.SELECT_VALUE_FOR_DATAITEM, 
					data); 
			}
				
			if (StringFunction.isNotBlank(value) && StringFunction.isBlank(data)) {
				// User gave value but no data
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.SELECT_DATAITEM_FOR_VALUE, 
					value); 
			}
		}
	
		// In any event, the value cannot exceed 140 characters.  Since we
		// use a text area field, we must test for max length here.
		if (value.length() > MAX_VALUE_LENGTH) {
			this.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1, 
				TradePortalConstants.LC_RULE_VALUE_TOO_BIG, 
				data, Integer.toString(MAX_VALUE_LENGTH));
		}
	}
	
	// Finally, set the pregenerated_sql based on what we know.  We still
	// haven't checked for required fields so it still may not be correct.
	// If however, all the data is valid, the sql will be correct.
	String sql = buildSql();
	setAttribute("pregenerated_sql", sql);
}

private String buildSql() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
	

	LOG.debug("Building the pregenerated sql");
	StringBuilder sql = new StringBuilder();
	
	// Always must match on upload definition
	if(!StringFunction.isBlank(getAttribute("invoice_def"))){
        	sql.append(" a_upload_definition_oid = ");
		    sql.append("'"+SQLParamFilter.filter(getAttribute("invoice_def"))+"'");
    }

	// Because the condition against the last_ship_dt requires a reference to
	// "today" (meaning the current LOCAL date), we cannot include that condition
	// in the pregenerated sql.

	// Now, for each criteria where both data and value are set, add another
	// condition.
	String data;
	String value;
	
	for (int i=1; i<=6; i++) {

		data = getAttribute("inv_data_item" + i);
		if (StringFunction.isNotBlank(data)){
			if (data.endsWith("label")){
				data = data.replace("label","value");
			}
		}
		value = getAttribute("inv_data_item_val" + i);
		if ("buyer_name".equalsIgnoreCase(data) || "seller_name".equalsIgnoreCase(data)){
			if (StringFunction.isNotBlank(value)){
				value = value.toUpperCase();
			}
		}
		
		
		if (StringFunction.isNotBlank(data) && StringFunction.isNotBlank(value)) {
			sql.append(" and UPPER(");
			sql.append(data);
			sql.append(")");
			if (value.indexOf(VALUE_SEPARATOR) > -1) {
				// Multiple values given, so use IN condition
				sql.append(" in ");
				sql.append(parseMultipleValues(value.toUpperCase()));
			} else {
				// Single value given, so use equality
				sql.append(" = '");
				sql.append(SQLParamFilter.filter(value.toUpperCase()));
				sql.append("'");
			}
		}
	}

	
	return sql.toString();
}

/**
 * Converts a set of values delimited by '!' into a clause that can be
 * used with the SQL 'IN' keyword.  All values are assumed to be strings
 *
 * For example, the valueList of Lisa!Bart!Maggie would be returned as
 * ('Lisa','Bart','Maggie')
 *
 * @return java.lang.String - list of value for SQL IN clause.
 * @param value java.lang.String - List of values with ! delimiters
 */
private String parseMultipleValues(String valueList) {
	
		StringBuffer result = new StringBuffer("('");
		int start = 0;
		int end = 0;
	
		// Essentially each ! gets converted into ',' (quote, comma, quote)
		end = valueList.indexOf(VALUE_SEPARATOR);
		while (end > -1) {
			result.append(SQLParamFilter.filter(valueList.substring(start, end)));
			result.append("','");
			start = end + 1;
			end = valueList.indexOf(VALUE_SEPARATOR, start);
		}
	
		// Append the last value and the closing quote and right paren
		result.append(SQLParamFilter.filter(valueList.substring(start)));	
		result.append("')");
	
		return result.toString();
	}
}
