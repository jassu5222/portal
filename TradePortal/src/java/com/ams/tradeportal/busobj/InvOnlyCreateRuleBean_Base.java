package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;

public class InvOnlyCreateRuleBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvOnlyCreateRuleBean_Base.class);


	   
	   protected void registerAttributes() throws AmsException
	   {

		   /*
	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();

	      /* inv_only_create_rule_oid - Unique identifier */
	      attributeMgr.registerAttribute("inv_only_create_rule_oid", "inv_only_create_rule_oid", "ObjectIDAttribute");

	      /* name - Name of the LC Creation Rule */
	      attributeMgr.registerAttribute("name", "name");
	      attributeMgr.requiredAttribute("name");
	      attributeMgr.registerAlias("name", getResourceManager().getText("LCCreationRuleBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
	      

		  attributeMgr.registerAttribute("inv_only_create_rule_desc", "inv_only_create_rule_desc");
	      attributeMgr.requiredAttribute("inv_only_create_rule_desc");
	      attributeMgr.registerAlias("inv_only_create_rule_desc", getResourceManager().getText("LCCreationRuleBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));
	      attributeMgr.registerAssociation("op_bank_org_oid", "a_op_bank_org_oid", "OperationalBankOrganization");
	      attributeMgr.requiredAttribute("op_bank_org_oid");
	      attributeMgr.registerAlias("op_bank_org_oid",getResourceManager().getText("InvOnlyRuleDetail.BankBranch",TradePortalConstants.TEXT_BUNDLE));
 	      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid","CorporateOrganization");
 	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
  		  attributeMgr.registerAttribute("invoice_def", "a_invoice_def");
 		  attributeMgr.registerAssociation("invoice_def", "a_invoice_def","InvoiceDefinition");
 		  attributeMgr.registerAttribute("instrument_type_code", "instrument_type_code");
		  attributeMgr.registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");
	      attributeMgr.registerAttribute("inv_only_create_rule_template", "inv_only_create_rule_template");
 	      attributeMgr.registerAttribute("inv_only_create_rule_bank", "inv_only_create_rule_bank");
		  
		  attributeMgr.registerAttribute("inv_only_create_rule_max_amt", "inv_only_create_rule_max_amt","NumberAttribute");
		  attributeMgr.registerAlias("inv_only_create_rule_max_amt",getResourceManager().getText("CreationRuleDetail.MaximumAmount",TradePortalConstants.TEXT_BUNDLE));
	      
		  attributeMgr.registerAttribute("due_or_pay_min_days", "due_or_pay_min_days","NumberAttribute");
		  attributeMgr.registerAlias("due_or_pay_min_days",getResourceManager().getText("InvOnlyRuleDetail.FromDueorPayDate",TradePortalConstants.TEXT_BUNDLE));
		  attributeMgr.registerAttribute("due_or_pay_max_days", "due_or_pay_max_days","NumberAttribute");
		  attributeMgr.registerAlias("due_or_pay_max_days",getResourceManager().getText("InvOnlyRuleDetail.ToDueorPayDate",TradePortalConstants.TEXT_BUNDLE));

 		  
		  attributeMgr.registerAttribute("inv_data_item1", "inv_data_item1");
	      attributeMgr.requiredAttribute("inv_data_item1");
	      attributeMgr.registerAlias("inv_data_item1",getResourceManager().getText("InvOnlyRuleDetail.InvDataItem",TradePortalConstants.TEXT_BUNDLE));

		  attributeMgr.registerAttribute("inv_data_item_val1", "inv_data_item_val1");
	      attributeMgr.requiredAttribute("inv_data_item_val1");
	      attributeMgr.registerAlias("inv_data_item_val1",getResourceManager().getText("InvOnlyRuleDetail.Value",TradePortalConstants.TEXT_BUNDLE));
		  
		  
		  attributeMgr.registerAttribute("inv_data_item2", "inv_data_item2");
		  attributeMgr.registerAttribute("inv_data_item_val2", "inv_data_item_val2");
		  attributeMgr.registerAttribute("inv_data_item3", "inv_data_item3");
		  attributeMgr.registerAttribute("inv_data_item_val3", "inv_data_item_val3");
		  attributeMgr.registerAttribute("inv_data_item4", "inv_data_item4");
		  attributeMgr.registerAttribute("inv_data_item_val4", "inv_data_item_val4");
		  attributeMgr.registerAttribute("inv_data_item5", "inv_data_item5");
		  attributeMgr.registerAttribute("inv_data_item_val5", "inv_data_item_val5");
		  attributeMgr.registerAttribute("inv_data_item6", "inv_data_item6");
		  attributeMgr.registerAttribute("inv_data_item_val6", "inv_data_item_val6");
	      attributeMgr.registerAttribute("pregenerated_sql", "pregenerated_sql");

	      
  	   }
 	}
