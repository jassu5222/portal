package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;

import java.rmi.*;

import javax.ejb.*;

public interface InvOnlyCreateRuleHome extends EJBHome{

	   public InvOnlyCreateRule create()
			      throws RemoteException, CreateException, AmsException;

	   public InvOnlyCreateRule create(ClientServerDataBridge csdb)
	      throws RemoteException, CreateException, AmsException;
}
