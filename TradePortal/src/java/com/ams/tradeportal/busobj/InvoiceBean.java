package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.sql.SQLException;
import java.util.*;
import java.math.*;

import com.ams.tradeportal.common.*;

/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceBean extends InvoiceBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceBean.class);

/**
* Performs any special "validate" processing that is specific to the
* descendant of BusinessObject. Specifically, it checks to see whether
* or not the the Account Number and Currency information has been entered correctly
*/

            /**
         * For the supplier portal invoices, calculate the net_amount_offered.
         * 
         * @since Release 8.2 CR-708B
         * @throws AmsException
         * @throws RemoteException 
         */
        public void calculateInvoiceOfferNetAmountOffered()
	throws AmsException, RemoteException {
           // Calculate the tenor days = due_payement_date - current date
            Date dueDate = getAttributeDate("invoice_due_date");
            Date paymentDate = getAttributeDate("invoice_payment_date");
            Date duePaymentDate = null;
            if (paymentDate != null) {
                duePaymentDate = paymentDate;
            }
            else if (dueDate != null) {
                duePaymentDate = dueDate;
            }
            else {
                setAttribute("net_amount_offered", null); // Should not happen
                return;
            }
            String duePaymentDateStr = DateTimeUtility.convertDateToDateString(duePaymentDate); 

            int numTenorDays = calculateDaysDiff(duePaymentDateStr);
            
            //IR 28009 start
            //CR 914A start- Adjusted Payment Amount(if present else invoice amount) - credit note amount
            String invAmount = (StringFunction.isNotBlank(getAttribute("invoice_payment_amount"))
                            && getAttributeDecimal("invoice_payment_amount").compareTo(BigDecimal.ZERO) != 0) ?
            		getAttribute("invoice_payment_amount"):getAttribute("invoice_total_amount");
            BigDecimal invoiceAmount = new BigDecimal(invAmount);
            invoiceAmount = invoiceAmount.subtract(getAttributeDecimal("total_credit_note_amount"));
            setAttribute("inv_amount_after_adj", invoiceAmount.toString());
            //CR 914A end 
            //IR 28009 end
            		
            //IR T36000021830 start
            LOG.debug("Invoice Bean:calculateInvoiceOfferNetAmountOffered numTenorDays ==>  {} ",numTenorDays);
            if(numTenorDays <0){
            	setAttribute("net_amount_offered", invAmount);
            	return;
            }
            //IR T36000021830 end
            BigDecimal numTenorDaysDecimal = new BigDecimal(numTenorDays);

            
            String currency = getAttribute("currency_code");

            long corpOrgOid = getAttributeLong("corp_org_oid");
            CorporateOrganization corpOrg = (CorporateOrganization) createServerEJB("CorporateOrganization",
                            corpOrgOid);

            String intDiscRateGroup = corpOrg.getAttribute("interest_disc_rate_group");
            String calculateMethod = corpOrg.getAttribute("sp_calculate_discount_ind");
            corpOrg = null;

            // Get the margin by matching the currency and closest threshold_amount.
            // Note empty threshold_amt has lower priority in matching.  we need to have a special sorting order for this since we sort threshold_mat by descension.
            StringBuilder marginSQL = new StringBuilder()
                    .append("SELECT RATE_TYPE, MARGIN FROM (select rate_type, margin from sp_margin_rule ")
                    .append(" where P_OWNER_OID = ? ")
                    .append(" AND (currency IS NULL OR currency = ?")
                    .append(") AND (threshold_amt IS NULL OR threshold_amt <= ?")
                    .append(") ORDER BY currency, decode(threshold_amt, null, 1, 0), threshold_amt desc")
                    .append(" ) WHERE rownum = 1");

            DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(marginSQL.toString(),false, corpOrgOid, currency, invoiceAmount);
            BigDecimal margin = BigDecimal.ZERO;
            String rateType = null;
            if (resultsDoc != null) {
                    String marginStr = resultsDoc.getAttribute("/ResultSetRecord/MARGIN");                        
                    if (StringFunction.isNotBlank(marginStr)) {
                            margin = new BigDecimal(marginStr, MathContext.DECIMAL64);
                    }
                    rateType = resultsDoc.getAttribute("/ResultSetRecord/RATE_TYPE");
            }

            // Get the interest rate 
            String intRateType = null;
            if(numTenorDays <= 30) {
                intRateType = "rate_30_days";
            }
            else if (numTenorDays <= 60) {
                intRateType = "rate_60_days";
            }
            else if (numTenorDays <= 90) {
                intRateType = "rate_90_days";
            }
            else if (numTenorDays <= 120) {
                intRateType = "rate_120_days";
            }
            else if (numTenorDays <= 180) {
                intRateType = "rate_180_days";
            }
            else {
                intRateType = "rate_360_days";
            }

            StringBuilder interestSQL = new StringBuilder()
                    .append("SELECT NUM_INTEREST_DAYS, ").append(intRateType).append(" as RATE")
                    .append(" FROM interest_discount_rate a, interest_discount_rate_group b")
                    .append(" WHERE a.p_interest_disc_rate_group_oid = b.interest_disc_rate_group_oid")
                    .append(" AND currency_code = ? ")
                    .append(" AND rate_type = ?")
                    .append(" AND group_id = ?")
                    .append(" AND effective_date  = (")
                    .append(" SELECT max(effective_date)")
                    .append(" FROM interest_discount_rate a2, interest_discount_rate_group b2")
                    .append(" WHERE a2.p_interest_disc_rate_group_oid = b2.interest_disc_rate_group_oid")
                    .append(" AND currency_code = ?")
                    .append(" AND rate_type = ? ")
                    .append(" AND group_id = ? )");
            
            List<Object> sqlParams = new ArrayList<Object>();
            sqlParams.add(getAttribute("currency_code"));
            sqlParams.add(rateType);
            sqlParams.add(intDiscRateGroup);
            sqlParams.add(getAttribute("currency_code"));
            sqlParams.add(rateType);
            sqlParams.add(intDiscRateGroup);

            String rateStr = null;
            String numInterestDaysStr = null;

            resultsDoc = DatabaseQueryBean.getXmlResultSet(interestSQL.toString(), false, sqlParams);
            if (resultsDoc != null) {
                    rateStr = resultsDoc.getAttribute("/ResultSetRecord/RATE");
                    numInterestDaysStr = resultsDoc.getAttribute("/ResultSetRecord/NUM_INTEREST_DAYS");
            }

            if (StringFunction.isBlank(rateStr)) {
                    rateStr = "0.0"; // Default if value not found in DB
            }

            if (StringFunction.isBlank(numInterestDaysStr)) {
                    numInterestDaysStr = "360"; // Default if value not found in DB
            }

            // discount rate = interest rate + margin 
            BigDecimal rate = new BigDecimal(rateStr, MathContext.DECIMAL64);
            rate = rate.add(margin).divide(new BigDecimal("100.0"));


            // Calculate discount amount
            BigDecimal numInterestDays = new BigDecimal(numInterestDaysStr);
            BigDecimal rateByDays = rate.multiply(numTenorDaysDecimal).divide(
                            numInterestDays, MathContext.DECIMAL64);

            BigDecimal discountAmount = BigDecimal.ZERO;
	    int precision = TPCurrencyUtility.getDecimalPrecision(getAttribute("currency_code"));  
            LOG.debug("InvoicesBean::calculateInvoiceOfferNetAmountOffer:: InvoiceOid = {} ;CalculateMethod = {} ;intRateType =  {} ;rate = {} ;margin = {} ;numInterestDays = {}"
                   ,new Object[]{ this.getAttribute("invoice_oid"),calculateMethod , intRateType, rateStr, margin , numInterestDaysStr});
            
            if (TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculateMethod)) {
                    discountAmount = invoiceAmount.multiply(rateByDays).setScale(precision, BigDecimal.ROUND_HALF_UP);
            }
            else if (TradePortalConstants.DISCOUNT_YIELD.equals(calculateMethod)) {
                    discountAmount =
                            invoiceAmount.multiply(BigDecimal.ONE.subtract(BigDecimal.ONE.divide(
                                            BigDecimal.ONE.add(rateByDays), MathContext.DECIMAL64))).setScale(precision, BigDecimal.ROUND_HALF_UP);
            }

            BigDecimal netAmountOffered = invoiceAmount.subtract(discountAmount);
            BigDecimal oldNetAmountOffered = getAttributeDecimal("net_amount_offered");
            if (!(netAmountOffered.compareTo(oldNetAmountOffered) == 0)) {
                setAttribute("old_net_amount_offered", oldNetAmountOffered.toString());            

                setAttribute("net_amount_offered", netAmountOffered.toString());
                String status = getAttribute("supplier_portal_invoice_status");
                if (status.equals("OFFER_ACCEPTED") || status.equals("PARTIALLY_AUTHORIZED")) {
                    Date futureValueDate = getAttributeDate("future_value_date");
                    if (futureValueDate != null) {
                        setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
                    }
                    else {
                     setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_BUYER_APPROVED);
                    }
                }
            }

	}
        /**
         * For the supplier portal invoice offer, updated its associated invoice offer group.
         * If it is not associated with any, or if the currently associated one no longer matches, then
         * find or create a new matching invoice offer group and associate the invoice to it.
         * 
         * This is called when:
         *  - an invoice offer is first received in portal
         *  - when net_amount_offered is changed
         * 
         * InvoiceOfferMediator also updates the invoice offer group in some situations using SQL.
         * 
         * @since Release 8.2 CR-708B
         * @throws AmsException
         * @throws RemoteException
         * @throws SQLException 
         */
        public void updateAssociatedInvoiceOfferGroup() throws AmsException, RemoteException{
            String invoiceStatus = getAttribute("supplier_portal_invoice_status");
            String invoiceOfferGroupOid = getAttribute("invoice_offer_group_oid");

            // Check whether there is another group that matches the invoice.
            InvoiceOfferGroup newInvoiceOfferGroup = null;
            newInvoiceOfferGroup = (InvoiceOfferGroup) createServerEJB("InvoiceOfferGroup");

            Date dueDate = getAttributeDate("invoice_due_date");
            Date paymentDate = getAttributeDate("invoice_payment_date");
            Date duePaymentDate = null;
            if (paymentDate != null) {
                duePaymentDate = paymentDate;
            }
            else if (dueDate != null){
                duePaymentDate = dueDate;
            }
            String duePaymentDateStr = "";
            if (duePaymentDate != null) duePaymentDateStr = DateTimeUtility.convertDateToDateString(duePaymentDate); 
            List<Object> sqlParams = new ArrayList<Object>();
            StringBuilder sql = new StringBuilder();
            sql.append("select invoice_offer_group_oid from invoice_offer_group ")
               .append(" where name is null and currency = ? ")
               .append(" and due_payment_date = ")
               .append("to_date(?, 'mm/dd/yyyy')")
               .append(" and a_corp_org_oid = ?  ")
               .append(" and supplier_portal_invoice_status = ? ");     ;
            String futureValueDate = getAttribute("future_value_date");
            sqlParams.add(getAttribute("currency_code"));
            sqlParams.add(duePaymentDateStr);
            sqlParams.add(getAttribute("corp_org_oid"));
            sqlParams.add(invoiceStatus);
            if (futureValueDate.length() == 0) {
                sql.append(" and future_value_date is null");
            }
            else {
                sql.append(" and future_value_date = to_date(?, 'mm/dd/yyyy')");
                sqlParams.add(futureValueDate);
            }

            DocumentHandler newInvoiceOfferGroupDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);

            String newInvoiceOfferGroupOid = null;
            if (newInvoiceOfferGroupDoc != null  && newInvoiceOfferGroupDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OFFER_GROUP_OID") != null) {
                // If there is already an invoice offer group for the new attributes, we will add the invoices to that one.
                newInvoiceOfferGroupOid = newInvoiceOfferGroupDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OFFER_GROUP_OID");
            }
            
            // Update the current associated group
            if (invoiceOfferGroupOid.length() > 0) {
                InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) createServerEJB("InvoiceOfferGroup", Long.parseLong(invoiceOfferGroupOid));
                String groupStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
                
                // If the invoice is no longer eligible, remove it from its invoice offer group
                if (invoiceStatus.equals(TradePortalConstants.SP_STATUS_INELLIGIBLE)) {
                    invoiceOfferGroup.setAttribute("total_number", String.valueOf(invoiceOfferGroup.getAttributeInteger("total_number") - 1)); 
                    invoiceOfferGroup.setAttribute("total_amount", invoiceOfferGroup.getAttributeDecimal("total_amount").subtract(getAttributeDecimal("invoice_total_amount")).toString());
                    invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(getAttributeDecimal("net_amount_offered")).toString());
                    invoiceOfferGroup.setAttribute("remaining_amount", invoiceOfferGroup.getAttributeDecimal("remaining_amount").subtract(getAttributeDecimal("inv_amount_after_adj")).toString());
                    setAttribute("invoice_offer_group_oid", null);
                    if (invoiceOfferGroup.getAttributeInteger("total_number") == 0) {
                        invoiceOfferGroup.delete();
                    }
                    else {
                        invoiceOfferGroup.save();
                    }
                    return;
                }
                
                // If an invoice belongs to a manual group, do not regroup the invoice but set back the group
                // status and adjust total_net_amount_offered if needed.
                // If this is the only invoice in the group and there is no other matching group, do not need to regroup either.
                	
                if (invoiceOfferGroup.getAttribute("name").length() > 0 
                        && invoiceOfferGroup.getAttributeInteger("total_number") == 1 && newInvoiceOfferGroupOid == null) {
                    
                	if (!invoiceStatus.equals(groupStatus)) {
                        invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", invoiceStatus);
                    }
                    if (getAttribute("old_net_amount_offered").length() > 0) {
                        invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(getAttributeDecimal("old_net_amount_offered")).add(getAttributeDecimal("net_amount_offered")).toString());
                    }
                    if (invoiceOfferGroup.getAttribute("name").length() == 0 
                            && !getAttribute("future_value_date").equals(invoiceOfferGroup.getAttribute("future_value_date"))) {
                        invoiceOfferGroup.setAttribute("future_value_date", getAttribute("future_value_date"));                        
                    }
                    
                    if (StringFunction.isNotBlank(duePaymentDateStr) && !duePaymentDateStr.equals(invoiceOfferGroup.getAttribute("due_payment_date"))) {
                        invoiceOfferGroup.setAttribute("due_payment_date", duePaymentDateStr);
                    }
                    invoiceOfferGroup.save();
                    return;
                } 

                // If the invoice still belongs to its group, adjust total net amount offered if needed but do not regroup.  
                if (groupStatus.equals(invoiceStatus)
                     && invoiceOfferGroup.getAttribute("future_value_date").equals(getAttribute("future_value_date"))
                     && StringFunction.isNotBlank(duePaymentDateStr) && duePaymentDateStr.equals(invoiceOfferGroup.getAttribute("due_payment_date"))) {
                    if (getAttribute("old_net_amount_offered").length() > 0) {
                        invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(getAttributeDecimal("old_net_amount_offered")).add(getAttributeDecimal("net_amount_offered")).toString());
                        invoiceOfferGroup.save();
                    }
                    return;
                }
                

                // Remove from the old invoice offer group.
                invoiceOfferGroup.setAttribute("total_number", String.valueOf(invoiceOfferGroup.getAttributeInteger("total_number") - 1)); 
                invoiceOfferGroup.setAttribute("total_amount", invoiceOfferGroup.getAttributeDecimal("total_amount").subtract(getAttributeDecimal("invoice_total_amount")).toString());
                if (getAttribute("old_net_amount_offered").length() > 0) {
                    invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(getAttributeDecimal("old_net_amount_offered")).toString());
                }
                else {
                    invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(getAttributeDecimal("net_amount_offered")).toString());
                }
                
                invoiceOfferGroup.save();                
            }
            else if (invoiceStatus.equals(TradePortalConstants.SP_STATUS_INELLIGIBLE)) {
                // If the invoice does not belong to any group and is no longer eligible, don't assign to new group
                return;
            }

            // Update the new group
            if (newInvoiceOfferGroupOid != null) {
                // The new group is an existing one, add invoice to it.
                newInvoiceOfferGroup.getData(Long.parseLong(newInvoiceOfferGroupOid));
                if (newInvoiceOfferGroup.getAttribute("attachment_ind").equals("N") 
                        && getAttribute("document_image_oid").length() > 0) {
                    newInvoiceOfferGroup.setAttribute("attachment_ind","Y");
                }
                newInvoiceOfferGroup.setAttribute("total_number", String.valueOf(newInvoiceOfferGroup.getAttributeInteger("total_number") + 1));
                newInvoiceOfferGroup.setAttribute("total_amount", newInvoiceOfferGroup.getAttributeDecimal("total_amount").add(getAttributeDecimal("invoice_total_amount")).toPlainString());
                newInvoiceOfferGroup.setAttribute("total_net_amount_offered", newInvoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").add(getAttributeDecimal("net_amount_offered")).toPlainString());
                newInvoiceOfferGroup.setAttribute("remaining_amount", newInvoiceOfferGroup.getAttributeDecimal("remaining_amount").subtract(getAttributeDecimal("inv_amount_after_adj")).toString());
            }
            else {
                // Create a new group.
                newInvoiceOfferGroupOid = String.valueOf(newInvoiceOfferGroup.newObject());
                newInvoiceOfferGroup.setAttribute("corp_org_oid", getAttribute("corp_org_oid"));
                newInvoiceOfferGroup.setAttribute("currency", getAttribute("currency_code"));
                newInvoiceOfferGroup.setAttribute("due_payment_date", duePaymentDateStr);
                newInvoiceOfferGroup.setAttribute("future_value_date", futureValueDate);
                newInvoiceOfferGroup.setAttribute("supplier_portal_invoice_status", invoiceStatus);
                if (getAttribute("document_image_oid").length() > 0) {
                    newInvoiceOfferGroup.setAttribute("attachment_ind", "Y");
                }
                else {
                    newInvoiceOfferGroup.setAttribute("attachment_ind", "N");
                }
                newInvoiceOfferGroup.setAttribute("total_number", "1");
                newInvoiceOfferGroup.setAttribute("total_amount", getAttribute("invoice_total_amount"));
                newInvoiceOfferGroup.setAttribute("total_net_amount_offered", getAttribute("net_amount_offered"));
                newInvoiceOfferGroup.setAttribute("remaining_amount", getAttribute("inv_amount_after_adj"));
            }


            newInvoiceOfferGroup.save();  

            setAttribute("invoice_offer_group_oid", newInvoiceOfferGroupOid);
            return;
        }
        

	private int calculateDaysDiff(String dateStr1) throws AmsException {
		Date date1 = DateTimeUtility.convertStringDateToDate(dateStr1);
		Date date2 = GMTUtility.getGMTDateTime();
                try {
                    PropertyResourceBundle properties = (PropertyResourceBundle)ResourceBundle.getBundle("TradePortal");
                    String overrideDate = properties.getString("DebugInvoiceOfferOverrideDate");
                    if (overrideDate!=null && overrideDate.length() > 0) {
                        java.text.SimpleDateFormat iso = new java.text.SimpleDateFormat("yyyy-MM-dd");  
                        date2 = iso.parse(overrideDate);                
                    }    
                }
                catch (Exception e){
			
		}
                

		Calendar cal = Calendar.getInstance();

		cal.setTime(date1);
		long dateInMillis1 = cal.getTimeInMillis();

		cal.setTime(date2);
		long dateInMillis2 = cal.getTimeInMillis();

		double daysDiff = (double)(dateInMillis1 - dateInMillis2) / (24 * 60 * 60 * 1000);
		// use ceil to make  full day if it is coming 20.24, then it should be 21 days.
		return ((int)Math.ceil(daysDiff));
	}

  /**
   * This method is used to check whether credit note is applied to invoice or not.
   * @return
   * @throws RemoteException
   * @throws AmsException
   */
  public boolean isCreditNoteApplied() throws RemoteException, AmsException{
	  boolean isApplied = false;
	  if( this.getAttributeDecimal( "total_credit_note_amount" ).compareTo(BigDecimal.ZERO) > 0 ){
		  isApplied = true;
	  }
	  
	  return isApplied;
  }
  
  /**
   * This method is used to update invoices comes from TPS and Invoice uploaded in portal if Applied amount changed or invoice declined from TPS.
   * @param invoiceDoc
   * @throws RemoteException
   * @throws AmsException
   */
  public void updateCreidtNoteAppliedAmout( DocumentHandler invoiceDoc ) throws RemoteException, AmsException {
	  
	  String appliedAmt = invoiceDoc.getAttribute("/CreditNoteAmountApplied");
	  if ( StringFunction.isNotBlank(appliedAmt) ) {
		  this.setAttribute("total_credit_note_amount", appliedAmt);
	  }
	  
	  String netInvoicePayAmt = invoiceDoc.getAttribute("/NetInvoicePaymentAmount");
	  if ( StringFunction.isNotBlank(netInvoicePayAmt) ) {
		  this.setAttribute("inv_amount_after_adj", netInvoicePayAmt);
	  }
	    
	  String uploadInvoiceOid = getUploadInvoiceOid( appliedAmt );
	  String invoiceStatus = invoiceDoc.getAttribute("/InvoiceProcessingStatus");		  
	  if ( StringFunction.isNotBlank(uploadInvoiceOid) ) {
		 updateUploadInvoiceData( invoiceStatus, uploadInvoiceOid, invoiceDoc.getAttributeDecimal("/CreditNoteAmountApplied") );
	  }
	  
	  if ( TradePortalConstants.CREDIT_NOTE_STATUS_DEC.equals(invoiceStatus) ) {
		  this.setAttribute("invoice_status", invoiceStatus); 
		  try {
		    unlinkAppliedCreditNote(this.getAttribute("invoice_oid"), uploadInvoiceOid);
		  } catch( SQLException e) {
			  throw new AmsException(e);
		  }
	  }
	  
  }
  
  /**
   * This method is used to get upload invoice oid if invoice is portal originated and applied amount has been changed.
   * @param appliedAmt
   * @return
   * @throws RemoteException
   * @throws AmsException
   */
  private String getUploadInvoiceOid(String appliedAmt) throws RemoteException, AmsException {
	  String uploadInvoiceOid = "";
	  if ( TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("portal_originated_ind")) && 
			  StringFunction.isNotBlank(appliedAmt) ) {		  
		  String uploadInvoiceSql = "SELECT upload_invoice_oid FROM invoices_summary_data where invoice_id = ? AND a_corp_org_oid = ?";
		  DocumentHandler uploadInvDoc = DatabaseQueryBean.getXmlResultSet(uploadInvoiceSql, true, new Object[] {this.getAttribute("invoice_reference_id"), 
				  this.getAttribute("corp_org_oid")} );
		  if ( uploadInvDoc != null ) {
			  uploadInvoiceOid = uploadInvDoc.getAttribute("/ResultSetRecord(0)/UPLOAD_INVOICE_OID");
		  }
	  }	  
	  return uploadInvoiceOid;
  }
  
  /**
   * This method is used to update upload invoice as well if invoice is portal originated.
   * @param invoiceStatus
   * @param uploadInvoiceOid
   * @param appliedAmt
   * @throws AmsException
   * @throws RemoteException 
   * @throws NumberFormatException 
   */
	private void updateUploadInvoiceData(String invoiceStatus, String uploadInvoiceOid, BigDecimal appliedAmt)
			throws AmsException, RemoteException {
		InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) createServerEJB("InvoicesSummaryData", Long.parseLong(uploadInvoiceOid));
		invoiceSummaryData.setAttribute("total_credit_note_amount", appliedAmt.negate().toPlainString());
		if (TradePortalConstants.CREDIT_NOTE_STATUS_DEC.equals(invoiceStatus)) {
            invoiceSummaryData.setAttribute("invoice_status", invoiceStatus);
		}
		invoiceSummaryData.save();
	}
	
	/**
	 * This is used to delete all entry of applied credit note from relation table for declined invoices.
	 * @param invoiceOid
	 * @param uploadInvoiceOid
	 * @throws SQLException
	 * @throws AmsException
	 */
	private void unlinkAppliedCreditNote( String invoiceOid, String uploadInvoiceOid ) throws SQLException, AmsException {
		String deleteSql = "DELETE FROM invoices_credit_notes_relation WHERE a_upload_invoice_oid in (?, ?)";
		DatabaseQueryBean.executeUpdate(deleteSql, true, new Object[] { invoiceOid, uploadInvoiceOid });
	}
  
}
