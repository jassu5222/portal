
  

package com.ams.tradeportal.busobj;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObjectBean;




/*
 * Invoice.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* invoice_oid - Unique Identifier. */
      attributeMgr.registerAttribute("invoice_oid", "invoice_oid", "ObjectIDAttribute");
      
      /* otl_invoice_uoid - The OTL UOID of the invoice.  This is sent back to OTL after manual matching
         with payment. */
      attributeMgr.registerAttribute("otl_invoice_uoid", "otl_invoice_uoid");
      
      /* invoice_reference_id - Invoice Reference ID */
      attributeMgr.registerAttribute("invoice_reference_id", "invoice_reference_id");
      
      /* link_to_instrument_type -  */
      attributeMgr.registerAttribute("link_to_instrument_type", "link_to_instrument_type");
      
      /* instrument_id - Instrument ID */
      attributeMgr.registerAttribute("instrument_id", "instrument_id");
      
      /* status - Status of the invoice. */
      attributeMgr.registerAttribute("status", "status");
      
      /* source_of_data -  */
      attributeMgr.registerAttribute("source_of_data", "source_of_data");
      
      /* otl_customer_id - OTL Customer ID of the buyer. */
      attributeMgr.registerAttribute("otl_customer_id", "otl_customer_id");
      
      /* invoice_version -  */
      attributeMgr.registerAttribute("invoice_version", "invoice_version", "NumberAttribute");
      
      /* invoice_issue_datetime -  */
      attributeMgr.registerAttribute("invoice_issue_datetime", "invoice_issue_datetime", "DateTimeAttribute");
      
      /* finance_status -  */
      attributeMgr.registerReferenceAttribute("finance_status", "finance_status", "INVOICE_FINANCE_STATUS");
      
      /* invoice_total_amount -  */
      attributeMgr.registerAttribute("invoice_total_amount", "invoice_total_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* finance_percentage -  */
      attributeMgr.registerAttribute("finance_percentage", "finance_percentage", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* finance_amount -  */
      attributeMgr.registerAttribute("finance_amount", "finance_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* matching_status -  */
      attributeMgr.registerAttribute("matching_status", "matching_status");
      
      /* invoice_due_date -  */
      attributeMgr.registerAttribute("invoice_due_date", "invoice_due_date", "DateAttribute");
      
      /* activity_sequence_datetime - The Sequence Date Time of the last activity that updated the invoice. */
      attributeMgr.registerAttribute("activity_sequence_datetime", "activity_sequence_datetime", "DateTimeAttribute");
      
      /* buyer_party_identifier -  */
      attributeMgr.registerAttribute("buyer_party_identifier", "buyer_party_identifier");
      
      /* buyer_address_line_1 -  */
      attributeMgr.registerAttribute("buyer_address_line_1", "buyer_address_line_1");
      
      /* buyer_address_line_2 -  */
      attributeMgr.registerAttribute("buyer_address_line_2", "buyer_address_line_2");
      
      /* buyer_address_country -  */
      attributeMgr.registerAttribute("buyer_address_country", "buyer_address_country");
      
      /* seller_party_identifier -  */
      attributeMgr.registerAttribute("seller_party_identifier", "seller_party_identifier");
      
      /* seller_address_line_1 -  */
      attributeMgr.registerAttribute("seller_address_line_1", "seller_address_line_1");
      
      /* seller_address_line_2 -  */
      attributeMgr.registerAttribute("seller_address_line_2", "seller_address_line_2");
      
      /* seller_address_country -  */
      attributeMgr.registerAttribute("seller_address_country", "seller_address_country");
      
      /* bill_party_identifier -  */
      attributeMgr.registerAttribute("bill_party_identifier", "bill_party_identifier");
      
      /* bill_address_line_1 -  */
      attributeMgr.registerAttribute("bill_address_line_1", "bill_address_line_1");
      
      /* bill_address_line_2 -  */
      attributeMgr.registerAttribute("bill_address_line_2", "bill_address_line_2");
      
      /* bill_address_country -  */
      attributeMgr.registerAttribute("bill_address_country", "bill_address_country");
      
      /* disputed_indicator -  */
      attributeMgr.registerAttribute("disputed_indicator", "disputed_indicator", "IndicatorAttribute");
      
      /* invoice_outstanding_amount - Outstanding amount. */
      attributeMgr.registerAttribute("invoice_outstanding_amount", "invoice_outstanding_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* currency_code - The currency of the invoice */
      attributeMgr.registerReferenceAttribute("currency_code", "currency_code", "CURRENCY_CODE");
      
      /* invoice_status - Invoice status which could be one of the following:
         OPN	Open 
         DSP	Disputed
         CLD	Closed
         PAD	Paid 
         MTH	Matched
         FNR	Finance requested
CFN	Closed/financed
 */
      attributeMgr.registerReferenceAttribute("invoice_status", "invoice_status", "INVOICE_STATUS");
      
      /* invoice_payment_status - Invoice status */
      attributeMgr.registerReferenceAttribute("invoice_payment_status", "invoice_payment_status", "INVOICE_PAYMENT_STATUS");
      
      /* invoice_payment_amount - The amount that is already paid for this invoice. */
      attributeMgr.registerAttribute("invoice_payment_amount", "invoice_payment_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* closed_indicator -  */
      attributeMgr.registerAttribute("closed_indicator", "closed_indicator", "IndicatorAttribute");
      
      /* invoice_credit_note_amount - Credit Note Amount */
      attributeMgr.registerAttribute("invoice_credit_note_amount", "invoice_credit_note_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* invoice_discount_amount - Discount amount of the invoice. */
      attributeMgr.registerAttribute("invoice_discount_amount", "invoice_discount_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* pending_payment_amount - The payment amount that has been sent to OTL via middleware but the Apply
         Payment activitiy is not released in OTL yet. */
      attributeMgr.registerAttribute("pending_payment_amount", "pending_payment_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* finance_instrument_oid -  */
      attributeMgr.registerAssociation("finance_instrument_oid", "a_finance_instrument_oid", "Instrument");
      
      /* client_bank_oid -  */
      attributeMgr.registerAssociation("client_bank_oid", "a_client_bank_oid", "ClientBank");
      
      /* corp_org_oid - The Corporate Org that is the seller party of the invoice. */
      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");
      
    //Ravindra - Rel800 CR710 - 20th Dec 2011 - Start
      /* invoice_payment_date -  */
      attributeMgr.registerAttribute("invoice_payment_date", "invoice_payment_date", "DateAttribute");
      /* disputed_indicator -  */
      attributeMgr.registerAttribute("portal_originated_ind", "portal_originated_ind", "IndicatorAttribute");
    //Ravindra - Rel800 CR710 - 20th Dec 2011 - End
      
                /* first_authorize_status_date - Timestamp (in GMT) of when the first authorization took place on this PayRemit.
	         Depending on the corporate customer's settings, this may be the only authorization
	         that takes places on this Invoice. */
	      attributeMgr.registerAttribute("first_authorize_status_date", "first_authorize_status_date", "DateTimeAttribute");
	      
	      /* second_authorize_status_date - Timestamp (in GMT) of when the second authorization took place on this transaction.
	         Depending on the corporate customer's settings, only one authorization may
	         be necessary. */
	      attributeMgr.registerAttribute("second_authorize_status_date", "second_authorize_status_date", "DateTimeAttribute");
	      
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
	      /* first_authorizing_user_oid - The user that performed the first authorization on a PayRemit.  Depending
	         on the corporate customer settings, this may be the only user to perform
	         an authorize on this Invoice. */
	      attributeMgr.registerAssociation("first_authorizing_user_oid", "a_first_authorizing_user_oid", "User");
	      
	      /* second_authorizing_user_oid - The user that performed the second authorization on a PayRemit.  Depending
	         on the corporate customer settings, this may be no second user to authorize
	         this Invoice. */
	      attributeMgr.registerAssociation("second_authorizing_user_oid", "a_second_authorizing_user_oid", "User");
	      
	      /* first_authorizing_work_group_oid - The work group of the first user that performed the authorization on a PayRemit.
	         Depending on the corporate customer settings, this user may be the only
	         user to perform an authorize on this Invoice. */
	      attributeMgr.registerAssociation("first_authorizing_work_group_oid", "a_first_auth_work_group_oid", "WorkGroup");
	      
	      /* second_authorizing_work_group_oid - The work group of the second user that performed the authorization on a
	         PayRemit.  Depending on the corporate customer settings, this may be no
	         second user to authorize this Invoice. */
	      attributeMgr.registerAssociation("second_authorizing_work_group_oid", "a_second_auth_work_group_oid", "WorkGroup");
		  attributeMgr.registerAttribute("supplier_portal_invoice_ind", "supplier_portal_invoice_ind", "IndicatorAttribute");
		  attributeMgr.registerAttribute("goods_description", "goods_description");
		  attributeMgr.registerAttribute("invoice_offer_group_oid", "a_invoice_offer_group_oid", "NumberAttribute");
		  attributeMgr.registerAttribute("future_value_date", "future_value_date", "DateAttribute");
		  attributeMgr.registerAttribute("net_amount_offered", "net_amount_offered", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
		  attributeMgr.registerReferenceAttribute("supplier_portal_invoice_status", "supplier_portal_invoice_status", "SUPPLIER_PORTAL_INVOICE_STATUS");
		  attributeMgr.registerAttribute("offer_expiry_date", "offer_expiry_date", "DateAttribute");
		  attributeMgr.registerAttribute("document_image_oid", "a_document_image_oid", "NumberAttribute");
                  attributeMgr.registerAttribute("old_net_amount_offered", "old_net_amount_offered", "LocalAttribute");
		  attributeMgr.registerAttribute("rejection_reason_text", "rejection_reason_text");
		  
		//M Eerupula 05/14/2013 Rel8.3 CR776
		  attributeMgr.registerAttribute("sp_email_sent_ind", "sp_email_sent_ind", "IndicatorAttribute");
          //CR913 start
		  attributeMgr.registerAttribute("send_to_supplier_date_ind", "send_to_supplier_date_ind", "IndicatorAttribute");
		  attributeMgr.registerAttribute("send_to_supplier_date", "send_to_supplier_date", "DateAttribute");
		  
		  /* panel_auth_group_oid - Panel group associated to an Invoice. */
	      attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");
	      
	      /* panel_auth_range_oid - Panel Range Oid to an invoice . */
	      attributeMgr.registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
	      
	      /* opt_lock - Optimistic lock attribute*/
	      attributeMgr.registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");
	      
	      //copy of send_to_supplier_date sent from TPS - used while authorising from portal 
	      attributeMgr.registerAttribute("send_to_supplier_date_tps", "send_to_supplier_date_tps", "DateAttribute");
	      //copy of invoice_payment_amount sent from TPS - used while authorising from portal 
	      attributeMgr.registerAttribute("invoice_payment_amount_tps", "invoice_payment_amount_tps", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      //copy of invoice_payment_date sent from TPS - used while authorising from portal 
	      attributeMgr.registerAttribute("invoice_payment_date_tps", "invoice_payment_date_tps", "DateAttribute");	      
		  //CR913 end	  
	      
	      attributeMgr.registerAttribute("total_credit_note_amount", "total_credit_note_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      attributeMgr.registerAttribute("inv_amount_after_adj", "inv_amount_after_adj","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      attributeMgr.registerAttribute("end_to_end_id", "end_to_end_id");
	      //CR1006 start
	      attributeMgr.registerAttribute("pay_method", "pay_method");
	      attributeMgr.registerAttribute("ben_acct_num", "ben_acct_num");
	      attributeMgr.registerAttribute("ben_add1", "ben_address_one");
	      attributeMgr.registerAttribute("ben_add2", "ben_address_two");
	      attributeMgr.registerAttribute("ben_add3", "ben_address_three");
	      attributeMgr.registerAttribute("ben_country", "ben_country");
	      attributeMgr.registerAttribute("ben_bank_name", "ben_bank_name");
	      attributeMgr.registerAttribute("ben_branch_code", "ben_branch_code");
	      attributeMgr.registerAttribute("ben_email_addr", "ben_email_addr");
	      attributeMgr.registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");
	      attributeMgr.registerAttribute("ben_branch_add1", "ben_branch_address1");
	      attributeMgr.registerAttribute("ben_branch_add2", "ben_branch_address2");
	      attributeMgr.registerAttribute("ben_bank_city", "ben_bank_city");
	      attributeMgr.registerAttribute("ben_bank_province", "ben_bank_prvnce");
	      attributeMgr.registerAttribute("ben_branch_country", "ben_branch_country");
	      attributeMgr.registerAttribute("charges", "payment_charges");
	      attributeMgr.registerAttribute("central_bank_rep1", "central_bank_rep1");
	      attributeMgr.registerAttribute("central_bank_rep2", "central_bank_rep2");
	      attributeMgr.registerAttribute("central_bank_rep3", "central_bank_rep3");	
	      //CR1006 end
		attributeMgr.registerAttribute("net_amount_paid", "net_amount_paid", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* InvoiceGoodsList - An Invoice could have a number of invoice goods. */
      registerOneToManyComponent("InvoiceGoodsList","InvoiceGoodsList");
      /* PanelAuthorizerList - The Invoice has many panel Authorizer Users.  */
	    registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
   }

 
   
 
 
   
}
