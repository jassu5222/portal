package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.*;
/**
 * This bean base class holds the mapping of physical & logical attributes

 * @version 0.1
 */
public class InvoiceCRNoteEmailNotificationBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceCRNoteEmailNotificationBean_Base.class);
	
	/* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	     /* sp_email_oid primary key attribute for SPEmailNotifications */
	      attributeMgr.registerAttribute("inv_crn_email_notify_oid", "inv_crn_email_notify_oid", "ObjectIDAttribute");
	      
	      /*  next_pin_email_due- A datetime (in GMT)  when next pin email is due */
	      attributeMgr.registerAttribute("next_pin_email_due", "next_pin_email_due", "DateTimeAttribute");
	      
	      /*  next_pcn_email_due- A datetime (in GMT)  when next pcn email is due */
	      attributeMgr.registerAttribute("next_pcn_email_due", "next_pcn_email_due", "DateTimeAttribute");
	      
	      /*  next_rin_email_due- A datetime (in GMT)  when rin emai is due */
	      attributeMgr.registerAttribute("next_rin_email_due", "next_rin_email_due", "DateTimeAttribute");
	      
	      /* Pointer to the CorporateOrganization */
	      attributeMgr.registerAttribute("p_corp_organization_oid", "p_corp_organization_oid", "NumberAttribute");
	      
	      /* Pointer to the parent NotificationRule */
	      attributeMgr.registerAttribute("p_notification_rule_oid", "p_notification_rule_oid", "NumberAttribute");
	  
	   }
	   
	

}
