package com.ams.tradeportal.busobj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoiceDefinitionBean extends InvoiceDefinitionBean_Base {
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceDefinitionBean.class);
	Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9 ]*");// Just accept Alphanumeric

        @Override
	public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		validateGeneralTab();
		// DK IR - T36000011727 Rel8.2 02/14/2013 Starts
		String credit_note_ind_text = getAttribute("credit_note_ind_text");
		Matcher m = ALPHANUMERIC.matcher(credit_note_ind_text);
		if (!m.matches()) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MUST_BE_ALPHA_NUMERIC,
					getResourceManager().getText("InvoiceUploadRequest.credit_note", TradePortalConstants.TEXT_BUNDLE));
		}

		String pay_method_req = getAttribute("pay_method_req");
		String ben_bank_name_req = getAttribute("ben_bank_name_req");
		String ben_branch_code_req = getAttribute("ben_branch_code_req");
		if (TradePortalConstants.INDICATOR_YES.equals(pay_method_req)
				&& (TradePortalConstants.INDICATOR_NO.equals(ben_bank_name_req) && TradePortalConstants.INDICATOR_NO.equals(ben_branch_code_req))) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.BEN_BANK_NAME_BRANCH_REQD);
		}

		// DK IR - T36000011727 Rel8.2 02/14/2013 Ends

		// CR 913 starts
		if (TradePortalConstants.INDICATOR_YES.equals(getAttribute("credit_note_req")) && StringFunction.isBlank(credit_note_ind_text)) {
			this.getErrorManager().issueError(getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
					this.resMgr.getText("InvoiceUploadRequest.credit_note_text", TradePortalConstants.TEXT_BUNDLE));
		}
		// CR 913 ends
	}

	public void validateGeneralTab() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		String name = getAttribute("name");
		String ownerOrg = this.getAttribute("owner_org_oid");

		// Make sure there is no other name in the db that matches this one.

		if (!isUnique("name", name, " and A_OWNER_ORG_OID=" + ownerOrg)) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		}

		// if field is marked as required for Seller or Buyer, then it's label should be present.

		for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("buyer_users_def" + x + "_req"))
					&& StringFunction.isBlank(this.getAttribute("buyer_users_def" + x + "_label"))) {

				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LABEL_REQUIRED, 	"Buyer User Def Label " + x);
			}
			// DK IR T36000014746 Rel8.2 03/13/2013 Starts
			else if (!StringFunction.isBlank(this.getAttribute("buyer_users_def" + x + "_label"))) {
				Matcher m1 = ALPHANUMERIC.matcher(this.getAttribute("buyer_users_def" + x + "_label")); // DK IR T36000014746 Rel8.2  03/13/2013
				if (!m1.matches()) {
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MUST_BE_ALPHA_NUMERIC, "Buyer User Def Label " + x);
				}
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("seller_users_def" + x + "_req"))
					&& StringFunction.isBlank(this.getAttribute("seller_users_def" + x + "_label"))) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LABEL_REQUIRED, "Seller User Def Label " + x);
			}
			
			else if (!StringFunction.isBlank(this.getAttribute("seller_users_def" + x + "_label"))) {
				Matcher m2 = ALPHANUMERIC.matcher(this.getAttribute("seller_users_def" + x + "_label")); // DK IR T36000014746  Rel8.2 03/13/2013
				if (!m2.matches()) {
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MUST_BE_ALPHA_NUMERIC, "Seller User Def Label " + x);
				}
			}
			// DK IR T36000014746 Rel8.2 03/13/2013 Ends
		}

		// if field is marked as required for Line item type/value, then it's label should be present.

		for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("prod_chars_ud" + x + "_type_req"))
					&& StringFunction.isBlank(this.getAttribute("prod_chars_ud" + x + "_type"))) {

				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LABEL_REQUIRED, "Product Chars UD " + x + " Type/Value Label");
			}

		}

		// Srinivasu_D IR#RVUM102633996 11/01/2012 start
		int dataWarningCount = 0;
		// Verifying if data required selected but type is not required.

		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("linked_to_instrument_type_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("linked_to_instrument_type_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("invoice_type_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("invoice_type_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("goods_description_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("goods_description_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("incoterm_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("incoterm_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("country_of_loading_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("country_of_loading_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("country_of_discharge_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("country_of_discharge_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("vessel_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("vessel_req"))) {

			dataWarningCount++;
		}
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("carrier_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("carrier_req"))) {

			dataWarningCount++;
		}

		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("actual_ship_date_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("actual_ship_date_req"))) {

			dataWarningCount++;
		}

		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("payment_date_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("payment_date_req"))) {

			dataWarningCount++;
		}
		// Narayan CR913 Rel9.0 30-Jan-2014 Begin
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("send_to_supplier_date_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("send_to_supplier_date_req"))) {
			dataWarningCount++;
		}
		// Narayan CR913 Rel9.0 30-Jan-2014 End
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("purchase_order_id_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("purchase_order_id_req"))) {

			dataWarningCount++;
		}
		// Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin
		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("end_to_end_id_data_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("end_to_end_id_req"))) {
			dataWarningCount++;
		}
		// Narayan CR914 Rel9.2 03-Nov-2014 Add- End

		for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {

			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("buyer_users_def" + x + "_label_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("buyer_users_def" + x + "_req"))) {
				dataWarningCount++;
			}

			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("seller_users_def" + x + "_label_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("seller_users_def" + x + "_req"))) {
				dataWarningCount++;
			}
		}

		if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_detail_provided"))) {

			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_id_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("line_item_id_req"))) {

				dataWarningCount++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_price_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("unit_price_req"))) {

				dataWarningCount++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_price_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("unit_of_measure_price_req"))) {

				dataWarningCount++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("inv_quantity_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("inv_quantity_req"))) {

				dataWarningCount++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_quantity_data_req"))
					&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("unit_of_measure_quantity_req"))) {

				dataWarningCount++;
			}

			for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
				if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("prod_chars_ud" + x + "_type_data_req"))
						&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("prod_chars_ud" + x + "_type_req"))) {

					dataWarningCount++;
				}
			}

		}

		if (dataWarningCount > 0) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_DEF_DATA_REQ_VALIDATION);
		}
		// Srinivasu_D IR#RVUM102633996 11/01/2012 End

		String buyerNameReq = this.getAttribute("buyer_name_req");
		String buyerIdReq = this.getAttribute("buyer_id_req");
		String typeInd = this.getAttribute("invoice_type_indicator");

		if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(typeInd) && (TradePortalConstants.INDICATOR_NO).equals(buyerIdReq)
				&& (TradePortalConstants.INDICATOR_NO).equals(buyerNameReq)) {

			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.BUYER_ID_AND_NAME_NOT_REQUIRED);

		} else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(typeInd)
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("seller_name_req"))
				&& (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("seller_id_req"))) {

			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SELLER_ID_AND_NAME_NOT_REQUIRED);

		}

		// If the Country of Loading is selected, then the Country of Discharge must also be selected (and vice versa)
		String countryLoding = this.getAttribute("country_of_loading_req");
		String countryDischarge = this.getAttribute("country_of_discharge_req");

		if (((TradePortalConstants.INDICATOR_YES).equals(countryLoding) || (TradePortalConstants.INDICATOR_YES).equals(countryDischarge))
				&& ((TradePortalConstants.INDICATOR_NO).equals(countryLoding) || (TradePortalConstants.INDICATOR_NO).equals(countryDischarge))) {

			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.COMPLETE_INV_COUNTRY_INFO);

		}

		// If 'Line Item Details Not Required Indicator' indicator is selected, then none of the Invoice Line Item Detail field
		// names can be selected.
		// If both are selected, upon save the system will present the user with an error
		if ((TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("line_item_detail_provided"))) {
			int warning = 0;
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_id_req")) && (warning == 0)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
				warning++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_price_req")) && (warning == 0)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
				warning++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_price_req")) && (warning == 0)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
				warning++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("inv_quantity_req")) && (warning == 0)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
				warning++;
			}
			if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("unit_of_measure_quantity_req")) && (warning == 0)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
				warning++;
			}
			for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
				if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("prod_chars_ud" + x + "_type_req"))
						&& (warning == 0)) {
					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LINE_ITEM_REQUIRED);
					warning++;
				}
			}

		}
		// IR T36000016317 Start
		else if ((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("line_item_detail_provided"))) {
			if ((TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("line_item_id_data_req"))) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_LINE_ITEMS_DATA_REQ,
						getResourceManager().getText("InvoiceDefinition.LineItemNumber", TradePortalConstants.TEXT_BUNDLE));

			}
			if ((TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("unit_price_data_req"))) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_LINE_ITEMS_DATA_REQ,
						getResourceManager().getText("InvoiceDefinition.LineItemUnitPrice", TradePortalConstants.TEXT_BUNDLE));

			}
			if ((TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("inv_quantity_data_req"))) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_LINE_ITEMS_DATA_REQ,
						getResourceManager().getText("InvoiceDefinition.LineItemDetailInvQunt", TradePortalConstants.TEXT_BUNDLE));

			}
		}
		// IR T36000016317 End
		// if no error, then do clean up of file order data which has been unselected from definition tab.
		if (!(this.getErrorManager().getErrorCount() > 0)) {
			cleanup(); // if user unselect required option, then need to clear file format order fields.
		}
	}

	/**
	 * This method is designed to clean up the invoice file format order if user deselect check box on general, then that value
	 * should not be present in file order tab. so it is used to clean up value in database befor saving.
	 */
	public void cleanup() throws AmsException, RemoteException {

		Set<String> hSummarySet = new HashSet<>();
		Set<String> hGoodsSet = new HashSet<>();
		Set<String> hLineItemSet = new HashSet<>();
		Set<String> hInvOrderSet = new HashSet<>(); // RKAZI Rel 8.2 IR# T36000011727 03/02/2013
		String uploadOrder = null;

		for (int x = 1; x <= TradePortalConstants.INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
			uploadOrder = getAttribute("inv_summary_order" + x);
			if (StringFunction.isNotBlank(uploadOrder))
				hSummarySet.add(uploadOrder);

		}
		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 START
		for (int x = 1; x <= TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS; x++) {
			uploadOrder = this.getAttribute("inv_credit_order" + x);
			if (StringFunction.isNotBlank(uploadOrder)) {
				hInvOrderSet.add(uploadOrder);
			}
		}
		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 END

		for (int x = 1; x <= TradePortalConstants.INV_GOODS_NUMBER_OF_FIELDS; x++) {
			uploadOrder = getAttribute("inv_goods_order" + x);
			if (StringFunction.isNotBlank(uploadOrder))
				hGoodsSet.add(uploadOrder);

		}

		for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_NUMBER_OF_FIELDS; x++) {
			uploadOrder = this.getAttribute("inv_line_item_order" + x);
			if (StringFunction.isNotBlank(uploadOrder)) {
				hLineItemSet.add(uploadOrder);
			}
		}

		// removing invoice summary data item from order tab which has been
		// unselected
		removeNonRequiredFields(hSummarySet, "linked_to_instrument_type_req", "linked_to_instrument_type");
		removeNonRequiredFields(hSummarySet, "payment_date_req", "payment_date");
		removeNonRequiredFields(hSummarySet, "invoice_type_req", "invoice_type");
		removeNonRequiredFields(hSummarySet, "buyer_name_req", "buyer_name");
		removeNonRequiredFields(hSummarySet, "seller_name_req", "seller_name");
		removeNonRequiredFields(hSummarySet, "seller_id_req", "seller_id");
		removeNonRequiredFields(hSummarySet, "buyer_id_req", "buyer_id");
		// RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
		removeNonRequiredFields(hSummarySet, "credit_note_req", "credit_note");
		removeNonRequiredFields(hSummarySet, "pay_method_req", "pay_method");
		removeNonRequiredFields(hSummarySet, "ben_acct_num_req", "ben_acct_num");
		removeNonRequiredFields(hSummarySet, "ben_add1_req", "ben_add1");
		removeNonRequiredFields(hSummarySet, "ben_add2_req", "ben_add2");
		removeNonRequiredFields(hSummarySet, "ben_add3_req", "ben_add3");
		removeNonRequiredFields(hSummarySet, "ben_email_addr_req", "ben_email_addr");
		removeNonRequiredFields(hSummarySet, "ben_country_req", "ben_country");
		removeNonRequiredFields(hSummarySet, "ben_bank_name_req", "ben_bank_name");
		removeNonRequiredFields(hSummarySet, "ben_bank_sort_code_req", "ben_bank_sort_code");
		removeNonRequiredFields(hSummarySet, "ben_branch_code_req", "ben_branch_code");
		removeNonRequiredFields(hSummarySet, "ben_branch_add1_req", "ben_branch_add1");
		removeNonRequiredFields(hSummarySet, "ben_branch_add2_req", "ben_branch_add2");
		removeNonRequiredFields(hSummarySet, "ben_bank_city_req", "ben_bank_city");
		removeNonRequiredFields(hSummarySet, "ben_bank_province_req", "ben_bank_province");
		removeNonRequiredFields(hSummarySet, "ben_branch_country_req", "ben_branch_country");
		removeNonRequiredFields(hSummarySet, "charges_req", "charges");
		removeNonRequiredFields(hSummarySet, "central_bank_rep1_req", "central_bank_rep1");
		removeNonRequiredFields(hSummarySet, "central_bank_rep2_req", "central_bank_rep2");
		removeNonRequiredFields(hSummarySet, "central_bank_rep3_req", "central_bank_rep3");
		// RKAZI Rel 8.2 IR# T36000012087 02/28/2013 END
		// Narayan CR913 Rel9.0 30-Jan-2014 Begin
		removeNonRequiredFields(hSummarySet, "send_to_supplier_date_req", "send_to_supplier_date");
		removeNonRequiredFields(hSummarySet, "buyer_acct_currency_req", "buyer_acct_currency");
		removeNonRequiredFields(hSummarySet, "buyer_acct_num_req", "buyer_acct_num");
		// Narayan CR913 Rel9.0 30-Jan-2014 End
		// Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin
		removeNonRequiredFields(hSummarySet, "end_to_end_id_req", "end_to_end_id");
		// Narayan CR914 Rel9.2 03-Nov-2014 Add- End
		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 START
		removeNonRequiredFields(hInvOrderSet, "discount_code_req", "discount_code");
		removeNonRequiredFields(hInvOrderSet, "discount_gl_code_req", "discount_gl_code");
		removeNonRequiredFields(hInvOrderSet, "discount_comments_req", "discount_comments");
		removeNonRequiredFields(hInvOrderSet, "discount_code_req", "credit_discount_code_id");
		removeNonRequiredFields(hInvOrderSet, "discount_gl_code_req", "credit_discount_gl_code_id");
		removeNonRequiredFields(hInvOrderSet, "discount_comments_req", "credit_discount_comments_id");
		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 END
		removeNonRequiredFields(hSummarySet, "reporting_code_1_req", "reporting_code_1");
		removeNonRequiredFields(hSummarySet, "reporting_code_2_req", "reporting_code_2");

		// removing invoice goods data item from order tab which has been
		// unselected

		removeNonRequiredFields(hGoodsSet, "goods_description_req", "goods_description");
		removeNonRequiredFields(hGoodsSet, "incoterm_req", "incoterm");
		removeNonRequiredFields(hGoodsSet, "country_of_loading_req", "country_of_loading");
		removeNonRequiredFields(hGoodsSet, "country_of_discharge_req", "country_of_discharge");
		removeNonRequiredFields(hGoodsSet, "vessel_req", "vessel");
		removeNonRequiredFields(hGoodsSet, "carrier_req", "carrier");
		removeNonRequiredFields(hGoodsSet, "actual_ship_date_req", "actual_ship_date");
		removeNonRequiredFields(hGoodsSet, "purchase_order_id_req", "purchase_order_id");

		for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
			removeNonRequiredFields(hGoodsSet, "buyer_users_def" + x + "_req", "buyer_users_def" + x + "_label");
			removeNonRequiredFields(hGoodsSet, "seller_users_def" + x + "_req", "seller_users_def" + x + "_label");
		}

		// removing invoice line item data from order tab which has been
		// unselected
		removeNonRequiredFields(hLineItemSet, "line_item_id_req", "line_item_id");
		removeNonRequiredFields(hLineItemSet, "unit_price_req", "unit_price");
		removeNonRequiredFields(hLineItemSet, "unit_of_measure_price_req", "unit_of_measure_price");
		removeNonRequiredFields(hLineItemSet, "inv_quantity_req", "inv_quantity");
		removeNonRequiredFields(hLineItemSet, "unit_of_measure_quantity_req", "unit_of_measure_quantity");

		for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
			removeNonRequiredFields(hLineItemSet, "prod_chars_ud" + x + "_type_req", "prod_chars_ud" + x + "_type");
		}

		// checking for invoice summary data file order . remove definiiton data from file format which has been unchecked in
		// definition tab.
		Hashtable<Integer, String> attributesHash = new Hashtable<>(TradePortalConstants.INV_SUMMARY_NUMBER_OF_FIELDS);
		int key = 1;
		String attUploadOrder;
		for (int x = 1; x <= TradePortalConstants.INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
			attUploadOrder = this.getAttribute("inv_summary_order" + x);
			if (StringFunction.isNotBlank(attUploadOrder) && hSummarySet.contains(attUploadOrder) && (hSummarySet.size() >= (x))) {// Added
																																	// condition
																																	// Rel
																																	// 8.2
																																	// T36000011727
				attributesHash.put(Integer.valueOf(key++), attUploadOrder);
			}
		}

		// Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		for (int x = 1; x <= TradePortalConstants.INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
			this.setAttribute("inv_summary_order" + x, (String) attributesHash.get(new Integer(x)));
		}

		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 START
		// checking for invoice credit file order . remove definiiton data from file format which has been unchecked in definition tab.
		attributesHash = new Hashtable<Integer, String>(TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS);
		key = 1;

		for (int x = 1; x <= TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS; x++) {
			attUploadOrder = this.getAttribute("inv_credit_order" + x);
			if (StringFunction.isNotBlank(attUploadOrder) && hInvOrderSet.contains(attUploadOrder)
					&& (hInvOrderSet.size() >= (x))) {// Added condition Rel 8.2 T36000011727
				attributesHash.put(new Integer(key++), attUploadOrder);
			}
		}

		// Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		for (int x = 1; x <= TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS; x++) {
			this.setAttribute("inv_credit_order" + x, (String) attributesHash.get(new Integer(x)));
		}
		// RKAZI Rel 8.2 IR# T36000011727 03/02/2013 END

		// checking for invoice summary Goods file order. remove definiiton data from file format which has been unchecked in definition tab
		attributesHash = new Hashtable<Integer, String>(TradePortalConstants.INV_GOODS_NUMBER_OF_FIELDS);
		key = 1;
		for (int x = 1; x <= TradePortalConstants.INV_GOODS_NUMBER_OF_FIELDS; x++) {
			attUploadOrder = this.getAttribute("inv_goods_order" + x);
			if (StringFunction.isNotBlank(attUploadOrder) && hGoodsSet.contains(attUploadOrder) && (hGoodsSet.size() >= (x))) {// Added
																																// condition
																																// Rel
																																// 8.2
																																// T36000011727
				attributesHash.put(new Integer(key++), attUploadOrder);
			}
		}

		// Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		for (int x = 1; x <= TradePortalConstants.INV_GOODS_NUMBER_OF_FIELDS; x++) {
			String val = attributesHash.get(new Integer(x));
			if ("country_of_loading".equals(val))
				val = "country_of_loading";
			if ("purchase_ord_id".equals(val))
				val = "purchase_order_id";

			this.setAttribute("inv_goods_order" + x, val);
		}

		// checking for invoice summary Goods file order. remove definiiton data from file format which has been unchecked in
		// definition tab
		attributesHash = new Hashtable<Integer, String>(TradePortalConstants.INV_LINE_ITEM_NUMBER_OF_FIELDS);
		key = 1;
		for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_NUMBER_OF_FIELDS; x++) {
			attUploadOrder = this.getAttribute("inv_line_item_order" + x);
			if (StringFunction.isNotBlank(attUploadOrder) && hLineItemSet.contains(attUploadOrder)
					&& (hLineItemSet.size() >= (x))) {// Added condition Rel 8.2 T36000011727
				attributesHash.put(new Integer(key++), attUploadOrder);
			}
		}

		// Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
		for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_NUMBER_OF_FIELDS; x++) {
			this.setAttribute("inv_line_item_order" + x, (String) attributesHash.get(new Integer(x)));
		}

	}

	/*
	 * @param hset
	 * 
	 * @param fieldName
	 * 
	 * @param lookUpValue
	 * 
	 * @throws java.rmi.RemoteException @throws com.amsinc.ecsg.frame.AmsException
	 */
	protected void removeNonRequiredFields(Set<String> hset, String fieldName, String lookUpValue)
			throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

		if (!TradePortalConstants.INDICATOR_YES.equals(getAttribute(fieldName))) {
			hset.remove(lookUpValue);
		}

	}

}
