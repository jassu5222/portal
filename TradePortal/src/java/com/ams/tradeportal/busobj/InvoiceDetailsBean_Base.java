
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Invoice Details.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceDetailsBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceDetailsBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      //BSL IR#PUL032965444 04/04/11
      /* domestic_payment_oid - Unique Identifier. */
      //attributeMgr.registerAttribute("domestic_payment_oid", "domestic_payment_oid", "ObjectIDAttribute");
	  attributeMgr.registerAttribute("domestic_payment_oid", "domestic_payment_oid", "NumberAttribute");

      /* invoice_details -  */
      attributeMgr.registerAttribute("payee_invoice_details", "payee_invoice_details");
      attributeMgr.registerAttribute("invoice_detail_oid", "invoice_detail_oid", "ObjectIDAttribute");
          
   }
   
 
   
 
 
   
}
