
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Invoice Details.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceDetailsHome extends EJBHome
{
   public InvoiceDetails create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceDetails create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
