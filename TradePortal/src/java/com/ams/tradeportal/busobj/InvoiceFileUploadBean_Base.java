
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceFileUploadBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* invoice_file_upload_oid - Unique Identifider */
      attributeMgr.registerAttribute("invoice_file_upload_oid", "invoice_file_upload_oid", "ObjectIDAttribute");
      
      /* invoice_file_name - Invoice File Name. */
      attributeMgr.registerAttribute("invoice_file_name", "invoice_file_name");
      
      /* validation_status - The status of Invoice File upload validation. */
      //attributeMgr.registerAttribute("validation_status", "validation_status");//BSL IR NNUM040229447 04/02/2012 - should be reference attribute
      attributeMgr.registerReferenceAttribute("validation_status", "validation_status", "VALIDATION_STATUS");
      
      /* number_of_invoices_uploaded - The number of invoices successfully uploaded. */
      attributeMgr.registerAttribute("number_of_invoices_uploaded", "number_of_invoices_uploaded", "NumberAttribute");
      
      /* number_of_invoices_failed - Number of invoices that failed for upload. */
      attributeMgr.registerAttribute("number_of_invoices_failed", "number_of_invoices_failed", "NumberAttribute");
      
      /* creation_timestamp - Date/Time when the file is uploaded. */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* completion_timestamp - Date/Time when the validation is completed. */
      attributeMgr.registerAttribute("completion_timestamp", "completion_timestamp", "DateTimeAttribute");
      
      /* agent_id - The name of the upload agent that processes/validates the file.  This prevents
         multiple agents from accessing the same file. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* deleted_ind - Whether the file is deleted. */
      attributeMgr.registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");
      
      /* error_msg - Error message of uploading. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* validation_seconds - The time it takes the agent to validate the invoice file. */
      attributeMgr.registerAttribute("validation_seconds", "validation_seconds", "NumberAttribute");
      
      /* user_oid - The user who initiates the invoice File Upload. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /* owner_org_oid - The Corporate Customer who uploaded the file. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
      /* currency used in uploaded file. */
      attributeMgr.registerAttribute("currency", "currency");
      /* amount- total amount of uploaded file. */
      //attributeMgr.registerAttribute("amount", "amount", "NumberAttribute");
      attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	  /* invoice file definition name. */
	  attributeMgr.registerAttribute("invoice_file_definition_name", "invoice_file_definition_name");

	  attributeMgr.registerAttribute("trading_partner_name","trading_partner_name");
	  //SHR CR708 Rel8.1.1 Start
	  attributeMgr.registerAttribute("invoice_upload_parameters","invoice_upload_parameters");
	  //SHR CR708 Rel8.1.1 End

      /* inv_result_parameters -  */
      attributeMgr.registerAttribute("inv_result_parameters", "inv_result_parameters");

      /* datetime_process_start - Date/Time when the file processing started. */
      attributeMgr.registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
      /* total_process_seconds - The total time it takes the agent to process the invoice file. */
      attributeMgr.registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
      /* process_seconds - The time it takes the agent to validate the invoice file. */
      attributeMgr.registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
      /* system_name -  */
      attributeMgr.registerAttribute("system_name", "system_name");
      /* server_name - */
      attributeMgr.registerAttribute("server_name", "server_name");

   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* InvoiceUploadErrorLogList -  */
      registerOneToManyComponent("InvoiceUploadErrorLogList","InvoiceUploadErrorLogList");
      registerOneToManyComponent("InvoicesSummaryDataList","InvoicesSummaryDataList");
   }
   
}
