
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceFileUploadHome extends EJBHome
{
   public InvoiceFileUpload create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceFileUpload create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
