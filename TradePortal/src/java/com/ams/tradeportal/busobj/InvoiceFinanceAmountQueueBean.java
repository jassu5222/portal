

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceFinanceAmountQueueBean extends InvoiceFinanceAmountQueueBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFinanceAmountQueueBean.class);

	
	
	
}
