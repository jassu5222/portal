
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The Invoice Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceFinanceAmountQueueBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFinanceAmountQueueBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
	   /* invoice_fin_amount_queue_oid - Unique Identifier. */
	   attributeMgr.registerAttribute("invoice_fin_amount_queue_oid", "invoice_fin_amount_queue_oid", "ObjectIDAttribute");
	         
	   /* creation_timestamp - The time the item was created. */
	   attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
	         
	   /* status - The status of the item. */
	   attributeMgr.registerReferenceAttribute("status", "status", "INVOICE_FIN_AMOUNT_STATUS");
	    
	   /* agent_id - The agent who is processing this item. */
	   attributeMgr.registerAttribute("agent_id", "agent_id");
	    
	   /* Interest_discount_rate_group_oid - The id of the interest discount rate group to process with this request. */
	   attributeMgr.registerAttribute("interest_disc_rate_grp_oid", "a_interest_disc_rate_grp_oid", "NumberAttribute");

	   /* error_msg � Error message of recalculation. */
	   attributeMgr.registerAttribute("error_msg", "error_msg");

	   /* corp_org_oid - The corporate org whose margin rule has changed. Mutually exclusive with interest_disc_rate_grp_oid*/
	   attributeMgr.registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");

           /* margin_rule_list_type - the type of margin rule list that has been changed.  SPMarginRuleList or CustomerMarginRuleList */
	   attributeMgr.registerAttribute("margin_rule_list_type", "margin_rule_list_type");

   }
   

 
   
 
 
   
}
