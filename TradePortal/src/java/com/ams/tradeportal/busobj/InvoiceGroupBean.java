package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoiceGroupBean extends InvoiceGroupBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupBean.class);
	/**
	 * Performs any special "issueOptimisticLockError" processing that is 
	 * specific to the descendant of BusinessObject.
	 */
	protected void issueOptimisticLockError() throws AmsException, RemoteException {
		if (!getRetryOptLock()) {
			getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.UPLOAD_INV_OPT_LOCK_EXCEPTION);
		}
	}

	/**
	 * Builds a string description of this group from the attributes
	 * used for grouping.
	 */
	public String getDescription() throws RemoteException, AmsException {
		StringBuilder description = new StringBuilder();
		description.append(getAttribute("trading_partner_name"));

		return description.toString(); 
	}

	/**
	 * Ensures this object will be saved, which increments the opt lock
	 */
	public void touch() throws RemoteException, AmsException {
		String oplock = getAttribute("opt_lock");
		setAttribute("opt_lock","");
		setAttribute("opt_lock",oplock);
	}

}
