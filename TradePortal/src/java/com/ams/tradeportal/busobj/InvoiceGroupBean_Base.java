package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


public class InvoiceGroupBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupBean_Base.class);

	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {
	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* invoice_group_oid - Unique identifier */
	      attributeMgr.registerAttribute("invoice_group_oid", "invoice_group_oid", "ObjectIDAttribute");
	      /*  This is the Trading Partner Name as established in the Trading Partner rule.  */
	      attributeMgr.registerAttribute("trading_partner_name", "trading_partner_name");
	      /* Invoice Status */
	      attributeMgr.registerReferenceAttribute("invoice_status", "invoice_status", "UPLOAD_INVOICE_STATUS");
	      /* Currency */
	      attributeMgr.registerAttribute("currency", "currency");
	      /* linked_instrument_type field representing the code value for the instrument to which the invoices should ultimately be associated:
	       * REC (Receivables), PAY (Payables), ATP (Approval to Pay), LNR (Loan request) */
	      attributeMgr.registerReferenceAttribute("linked_to_instrument_type", "linked_to_instrument_type", "UPLOAD_INV_LINKED_INSTR_TY");
	      /* payment_date - valid date format can be selected from the dropdown list. */
	      attributeMgr.registerAttribute("payment_date", "payment_date","DateAttribute");
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

	      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");
			//Srinivasu D CR-708 11/11/2012
		  attributeMgr.registerAttribute("attachment_ind", "attachment_ind", "IndicatorAttribute");
		  //Srinivasu_D CR-709 Rel8.2 02/02/2013
		  /* loan_type   */
	      attributeMgr.registerAttribute("loan_type", "loan_type");

		  attributeMgr.registerAttribute("invoice_classification", "invoice_classification");
		  
		// Pavani Rel 8.3 CR 821 Start
	      /* panel_auth_group_oid - Panel group associated to an Invoice. */
	      attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");
	      
	      /* panel_auth_range_oid - Panel Range Oid to a invoice group. */
	      attributeMgr.registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
	      
	      /* opt_lock - Optimistic lock attribute*/
	      attributeMgr.registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");

		  // Pavani Rel 8.3 CR 821 End
	      //CR 913 start - grouping uses supplier date for PYB invoices
	      attributeMgr.registerAttribute("send_to_supplier_date", "send_to_supplier_date", "DateAttribute");
	      
	      // Nar Rel 9.2 CR 914A  grouping uses End To End ID for PYB invoices
	      attributeMgr.registerAttribute("end_to_end_id", "end_to_end_id");
	   }
	   
	   /* 
	    * Register the components of the business object
	    */
	    protected void registerComponents() throws RemoteException, AmsException
	    {               
	       // Pavani release 8.3.0.0 CR 821 
	       /* PanelAuthorizerList - The InvoiceGroup has many panel Authorizer User.  */
	       registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
	                 
	    }

}
