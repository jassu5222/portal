
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Details for the various actions performed on the invoice
 * by a user
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* invoice_history_oid - Unique Identifier */
      attributeMgr.registerAttribute("invoice_history_oid", "invoice_history_oid", "ObjectIDAttribute");
      
      /* action_datetime - Timestamp (in GMT) when the action is taken. */
      attributeMgr.registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");
      
      /* action - Action. */
      attributeMgr.registerReferenceAttribute("action", "action", "UPLOAD_INVOICE_ACTION_TYPE");
      
      /* invoice_status - Staus of invoices. */
      attributeMgr.registerReferenceAttribute("invoice_status", "invoice_status", "UPLOAD_INVOICE_STATUS");
            
        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("upload_invoice_oid", "p_upload_invoice_oid", "ParentIDAttribute");
   
      /* user_oid - The user who the updates the invoices. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      //Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020395 ADD Begin
      /* panel_authority_code - The Panel Level at which the authorization action is taken. */
      attributeMgr.registerReferenceAttribute("panel_authority_code", "panel_authority_code", "PANEL_AUTH_TYPE");
     //Nar 04 Sep 2013 Release8.3.0.0 IR-T36000020395 ADD End
      
   }
   
 
   
 
 
   
}
