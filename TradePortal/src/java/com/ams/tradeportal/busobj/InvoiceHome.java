
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Invoice.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceHome extends EJBHome
{
   public Invoice create()
      throws RemoteException, CreateException, AmsException;

   public Invoice create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
