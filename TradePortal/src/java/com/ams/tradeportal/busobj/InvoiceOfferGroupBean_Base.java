
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The group of invoices used for Invoice Offer processing.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceOfferGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceOfferGroupBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* invoice_offer_group_oid - Unique Identifier */
      attributeMgr.registerAttribute("invoice_offer_group_oid", "invoice_offer_group_oid", "ObjectIDAttribute");
      
      /* currency - Currency of the invoices in this group. */
      attributeMgr.registerReferenceAttribute("currency", "currency", "CURRENCY_CODE");
      
      /* due_payment_date - The least of the due_date and payment_date of the invoices in the group. */
      attributeMgr.registerAttribute("due_payment_date", "due_payment_date", "DateAttribute");
      
      /* future_value_date - The future data when the offer will be accepted.  That is, the use can accept
         the offer in advance. */
      attributeMgr.registerAttribute("future_value_date", "future_value_date", "DateAttribute");
      
      /* name - The name of a manually create group. */
      attributeMgr.registerAttribute("name", "name");
      
      /* supplier_portal_invoice_status - The invoice status.  - BUYER_APPROVED, OFFER_ACCEPTED, OFFER_AUTHORIZED,
         OFFER_DECLINED etc. */
      attributeMgr.registerReferenceAttribute("supplier_portal_invoice_status", "supplier_portal_invoice_status", "SUPPLIER_PORTAL_INVOICE_STATUS");
      
      /* total_number - Total number of invoices in this group. */
      attributeMgr.registerAttribute("total_number", "total_number", "NumberAttribute");
      
      /* total_amount - Total amount of the invoices in this group. */
      attributeMgr.registerAttribute("total_amount", "total_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* total_net_amount_offer - Total net amount offer of the invoices in this group. */
      attributeMgr.registerAttribute("total_net_amount_offered", "total_net_amount_offered", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* attachment_ind - Whether any invoices in the group has attachment. */
      attributeMgr.registerAttribute("attachment_ind", "attachment_ind", "IndicatorAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* corp_org_oid - The corp org of the invoices in the group. */
      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");
      
	  // Narayan Rel 8.3 CR 821 07/03/2013 Start
      /* panel_auth_group_oid - Panel group associated to a InvoiceOfferGroup. */
      attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");
      
      /* panel_auth_range_oid - Panel Range Oid to a invoice offer group. */
      attributeMgr.registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
      
      /* opt_lock - Optimistic lock attribute*/
      attributeMgr.registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");

	  // Narayan Rel 8.3 CR 821 07/03/2013 End
      
      attributeMgr.registerAttribute("credit_note_ind", "credit_note_ind", "IndicatorAttribute");
      attributeMgr.registerAttribute("remaining_amount", "remaining_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
     }
   
   /* 
    * Register the components of the business object
    */
    protected void registerComponents() throws RemoteException, AmsException
    {               
       // Nar release 8.3.0.0 CR 821 3 July 2013
       /* PanelAuthorizerList - The InvoiceOfferGroup has many panel Authorizer User.  */
       registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
                 
    }
   
 
 
   
}
