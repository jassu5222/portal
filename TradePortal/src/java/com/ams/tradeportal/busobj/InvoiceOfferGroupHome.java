
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The group of invoices used for Invoice Offer processing.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface InvoiceOfferGroupHome extends EJBHome
{
   public InvoiceOfferGroup create()
      throws RemoteException, CreateException, AmsException;

   public InvoiceOfferGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
