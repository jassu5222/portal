package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.*;

import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class InvoicePaymentInstructionsBean extends InvoicePaymentInstructionsBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(InvoicePaymentInstructionsBean.class);
	/**
	 * This method creates new or updates an exisiting Payee data on the Domestic Payment
	 * Transaction or Template based on the data in the input document.
	 * It checks whether Domestic Payment Oid has been supplied to create a new payee or
	 * update an exisiting one.
	 * It returs true if no error condition is raised. It is possible that no updates/inserts
	 * need to take place - in this case true is returned. fals is returned only if an error
	 * condition is raised.
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @param outputDoc com.amsinc.ecsg.util - the output doc
	 * @return boolean -- success/ fail
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	public boolean updateInvPaymentInstructions (DocumentHandler inputDoc, DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		try
		{
			//First, check if all required fields are provided and are in good format.
			//   if not, warning/error condition(s) will be raised and no payee update/insert takes
			//place.
			//NOTE: this methoid will return false only if error condition is raised. It is possible that
			//   there is no need to add/update any payees and so the Sucess is still returned even
			//   though no payee data is added to the trunsaction
			if (!doAddInvPaymentInstructions(inputDoc))
			{
				if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
					return false;
				else
					return true;
			}

			//Obtain the current number of Payees with this transaction.  If this is an insert, we will need to
			//increment the total number of Payees so that Terms object contains proper value.
			boolean isNewDPInsert = false;	//IAZ IR-SRUK020834374 02/10/10 Add
			int prevNumberOfPayees = 0;
			try
			{
				prevNumberOfPayees = inputDoc.getAttributeInt("/prev_payee_number/");
			}
			catch (Exception any_e)
			{
				//if the current number is not known, do not update Terms object
				prevNumberOfPayees = -1;
			}

			//Validation is successful. Now check whether Domestic Payment oid is supplied.
			//   If so, this is an Update. If not, this is an Insert.
			//   Also, on Update, validate initial amount. On Insert, increase number of Payees per Transaction.

			long linvPaymentInstructionsOid = 0;
			String invPaymentInstructionsOid = inputDoc.getAttribute("/InvoicePaymentInstructions/inv_pay_inst_oid/");
			
			if (StringFunction.isNotBlank(invPaymentInstructionsOid))
			{
				linvPaymentInstructionsOid = Long.parseLong(invPaymentInstructionsOid);
				this.getData(linvPaymentInstructionsOid);


			}
			else
			{
				this.newObject();


				isNewDPInsert = true;
			}

			//set parent transaction oid
			this.setAttribute("transaction_oid",
					(new Long(inputDoc.getAttributeLong("/Transaction/transaction_oid"))).toString());


			//Validate amount/warn on bad input -- validation will also take
			//place on transaction's Verify.
			//In a case where this is a forced update/insert form a template
			//re-validate the amount and set it to zero if is not in proper format.

			try
			{
			}
			catch (Exception any_e)
			{
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_WRN, inputDoc.getAttribute("/DomesticPayment/amount"));
				this.setAttribute("amount", "0");
			}
			DocumentHandler tempDoc = inputDoc;			
			
			this.populateFromXmlDoc(tempDoc);

			//Finally, populate inputDoc with update Terms information so Terms get updated when navigation is back to TransactionMediator.
			processInvPayTerms(inputDoc, prevNumberOfPayees);
			// Generate sequence number
		
			String seqNumSql = "select max(SEQUENCE_NUMBER) as SEQUENCE_NUMBER from invoice_payment_instructions where p_transaction_oid = ? ";
			String seqNum = "1";
			DocumentHandler seqNumRS = DatabaseQueryBean.getXmlResultSet(seqNumSql, false, new Object[]{this.getAttribute("transaction_oid")});
			if (seqNumRS != null) {
				seqNum = seqNumRS.getAttribute("/ResultSetRecord(0)/SEQUENCE_NUMBER");
				if (StringFunction.isNotBlank(seqNum)) {
					seqNum = (new Integer(Integer.parseInt(seqNum) + 1)).toString();
				}
				else {
					seqNum = "1";
				}
			}

			this.setAttribute("sequence_number", seqNum);
		
			//All processing is complete.  If no errors detected, save Data to the database.
			if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
			{
				LOG.debug("DOMPMTBean::updateNewDomPmt::Error Detected on DP/Interm Banks/Terms::Will Not Save DP");
				return false;
			}
			//IAZ IR-SRUK020834374 02/10/10 Begin: This code is moved here from up top.
			//  When no errors detected -- on insert, store new DP OID in to the output doc
			//  to be used by Verify if needed; also increase Payees Count for this transaction.
			else
			{
				if (isNewDPInsert)
				{
					outputDoc.setAttribute("/InvoicePaymentInstructions/inv_pay_inst_oid/",
							this.getAttribute("inv_pay_inst_oid"));
					prevNumberOfPayees++;  //IAZ CM-451 01/29/09 Add: Increment the total number of Payees for this DP transaction
				}
			}

			this.save(true);
			return true;
		}
		catch(Exception any_e)
		{
			LOG.error("DOMPMTBean::updateNewDomPmt::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_UPD_ERR_WRN);
			return false;
		}
	}


	/**
	 * This method chck if the Domestic Payment should indeed be added to the
	 * Domestic Payment Transaction/ Template based on the data in the input document
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @return boolean -- true/false
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	private boolean doAddInvPaymentInstructions (DocumentHandler inputDoc)
	throws RemoteException, AmsException
	{

		boolean returnFlag = true;
		try
		{

			LOG.debug("DOMPMTBean::doAddDomesticPayment::FunctionButton: {}" , inputDoc.getAttribute("/Update/ButtonPressed"));
			String enteredPaymentMethod = inputDoc.getAttribute("/InvoicePaymentInstructions/pay_method/");
			ResourceManager resourceMgr = getResourceManager();
			long transactionOid = inputDoc.getAttributeLong("/Transaction/transaction_oid");
			String invPaymentInstructionsOid = inputDoc.getAttribute("/InvoicePaymentInstructions/inv_pay_inst_oid/");


			//Verify is at least some payee data is entered.
			//If not, no Payee needs to be added/updated (so return false) but an
			//action should continue (e.g., if this is Verify, Verify must progress with
			//exisiting data verification; if this is SaveClose - non-Payee transaction data
			//should be saved and transaction window close, etc) -- so do not raise any error
			//conditions.
			//Note: for DDIs, Payment Type is always "DDI" so if this is the only data entered, treat as No-Data case.
			if(
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/amount/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_acct_num/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_name/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_country/"))) &&
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/amount_currency_code/"))) &&	
					((StringFunction.isBlank(enteredPaymentMethod))) && 
					(StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/payment_charges/")))
			)
			{
				returnFlag = false;
			}
//			//Next, we check if this is a template reuqest. If so, partial Payee data is OK
			else
				if (inputDoc.getDocumentNode("/Template") != null)
			{
				LOG.debug("DOMPMTBean::doAddDomesticPayment::TemplateProcessing");
				returnFlag = true;
			}


			//If this is a regular Transaction requst, check if all required fileds are provided and are in a valid format
			//Note that some validations depend on the Payment Method of the Transaction
			else
			{
				String partyType = "DomesticPaymentRequest.Payee";

					validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_acct_num/"), returnFlag, //"Payee Account Number");
							resourceMgr.getText(partyType+"AccountNumber", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_name/"), returnFlag, //"Payee Name");
						resourceMgr.getText(partyType + "Name", TradePortalConstants.TEXT_BUNDLE));


				validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/payment_charges/"), returnFlag, //"Bank Charges Type");
						resourceMgr.getText("DomesticPaymentRequest.BankCharges", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/amount/"), returnFlag, //"Amount");
						resourceMgr.getText("FundsTransferRequest.Amount", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(enteredPaymentMethod, returnFlag, //"Payment Method");				//IAZ CR-483B 08/13/09 Add
						resourceMgr.getText("DomesticPaymentRequest.PaymentMethod", TradePortalConstants.TEXT_BUNDLE));

				validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/amount_currency_code/"), returnFlag, //"Payment Currency");
						resourceMgr.getText("DomesticPaymentRequest.PaymentCurrency", TradePortalConstants.TEXT_BUNDLE));

					validatePayeeField(inputDoc.getAttribute("/InvoicePaymentInstructions/ben_country"), returnFlag,
							resourceMgr.getText("DomesticPaymentRequest.Country", TradePortalConstants.TEXT_BUNDLE));
				if (StringFunction.isNotBlank(enteredPaymentMethod))
				{
					returnFlag = validateSamePaymentMethod(enteredPaymentMethod,transactionOid,invPaymentInstructionsOid);
					returnFlag = validatePaymentMethod(inputDoc.getFragment("/InvoicePaymentInstructions"),inputDoc.getAttribute("/Transaction/transaction_oid"));
					returnFlag = validateBankInformation(inputDoc.getFragment("/InvoicePaymentInstructions"), inputDoc);
					//CR-1001 Rel9.4 - Adding validations for Reporting Codes for ACH & RTGS payment methods
					if(TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(enteredPaymentMethod) || TradePortalConstants.PAYMENT_METHOD_RTGS.equals(enteredPaymentMethod)){
						returnFlag = validateReportingCodes(enteredPaymentMethod,inputDoc.getFragment("/InvoicePaymentInstructions"), inputDoc);
					}
				}
				String enteredCurrCode = inputDoc.getAttribute("/InvoicePaymentInstructions/amount_currency_code/");
				if (StringFunction.isNotBlank(enteredCurrCode)){
					returnFlag = validateSameCurrencyCode(enteredCurrCode,transactionOid, invPaymentInstructionsOid);
				}
				//Check that amount entered is valid
				String enteredPaymentAmount = inputDoc.getAttribute("/InvoicePaymentInstructions/amount/"); //move to DP
				try
				{
					LOG.debug("DOMPMTBean::doAddDomesticPayment::Normal Processing");
					enteredPaymentAmount =
						NumberValidator.getNonInternationalizedValue(enteredPaymentAmount,
								getResourceManager().getResourceLocale());
					String currencyCode = inputDoc.getAttribute("/InvoicePaymentInstructions/amount_currency_code/");
					
					if ((StringFunction.isNotBlank(enteredPaymentAmount))&&(new Float(enteredPaymentAmount).intValue() != 0))
					{
						TPCurrencyUtility.validateAmount(currencyCode,
								enteredPaymentAmount,
								"Payment Amount",
								this.getErrorManager() );
					}
					
					if (this.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
					{
						returnFlag = false;
					}else{
						inputDoc.setAttribute("/InvoicePaymentInstructions/amount/",enteredPaymentAmount);
					}
					
				}
				catch (Exception any_e)
				{
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_ERR, enteredPaymentAmount);
					enteredPaymentAmount = "0";
					returnFlag = false;
				}
			}
            if (!StringFunction.canEncodeIn_WIN1252(inputDoc.getAttribute("/InvoicePaymentInstructions/customer_reference"))) {
                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.UNICODE_NOT_ALLOWED,
                        getResourceManager().getText("DomesticPaymentRequest.CustomerReference",
                                TradePortalConstants.TEXT_BUNDLE));
                returnFlag = false;
            }
		}
		catch (Exception any_e)
		{
			LOG.error("DOMPMTBean::doAddDomesticPayment::Exception: ",any_e);
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_GEN_UPD_ERR_WRN);
			returnFlag = false;

		}

		return returnFlag;
	}
	
	//PMitnala IR T36000020561 - CR709 Rel 8.3  -Start
	/**
	 * This method loops through the list of all the Invoice Payment of
	 * the source Transaction or Template and creates
	 * copies of those for the target Transaction/Template
	 *
	 * @param originalTransOid  java.lang.String - the source transaction oid
	 * @param newTransactionOid  java.lang.String - the target (new) transaction oid
	 * @return void
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	public void createNewInvPaymentInstructions (String originalTransOid, String newTransactionOid)
	throws RemoteException, AmsException
	{
	
	try{
		LOG.debug("InvoicePaymentInstructionsBean::createNewInvPaymentInstructions:: originalTransactionOid is {}" , originalTransOid);

		//obtain a list of all invoice payments from source  Transaction/Template
	
		String sqlQuery = "SELECT IPI.INV_PAY_INST_OID FROM INVOICE_PAYMENT_INSTRUCTIONS IPI WHERE IPI.p_transaction_oid = ? " ;
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{originalTransOid});
		//check if there is no Payee data to copy.
		//if so, exit w/o warning/errors
		if (resultsDoc == null)
		{
			LOG.debug("InvoicePaymentInstructionsBean::createNewInvPaymentInstructions::The source template/transaction has no InvoicePayment informationn to copy");
			return;
		}
		
		int curIndex = 0;
		String originalInvoicePaymentOidStr =
			resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/INV_PAY_INST_OID");

		//loop through all the source's invoice oids
		while (
				(originalInvoicePaymentOidStr != null) &&
				(!originalInvoicePaymentOidStr.equals(""))
		)
		{

			long originalInvoicePaymentOid = 0;
			originalInvoicePaymentOid =
				resultsDoc.getAttributeLong("/ResultSetRecord(" + curIndex + ")/INV_PAY_INST_OID");

			//inv payment oid must be present for each entry
			if (originalInvoicePaymentOid == 0)
			{
				LOG.debug("InvoicePaymentInstructionsBean::createNewInvPaymentInstructions::Error: inv_pay_inst_oid not present");

			}
			else
			{
				//create a new target bean; copy from source object to target one; and save.

				InvoicePaymentInstructions originalInvoicePayment = (InvoicePaymentInstructions) this.createServerEJB("InvoicePaymentInstructions");
				originalInvoicePayment.getData(originalInvoicePaymentOid);

				InvoicePaymentInstructions invoicePayment = (InvoicePaymentInstructions) this.createServerEJB("InvoicePaymentInstructions");
				invoicePayment.newObject();

				//   Move Entire data record from source bean to the target (rather than field by field). 
				DocumentHandler tmpXML = new DocumentHandler();
				originalInvoicePayment.populateXmlDoc(tmpXML);
				invoicePayment.populateFromXmlDoc(tmpXML);

				invoicePayment.setAttribute("transaction_oid", newTransactionOid);
				curIndex++;
				invoicePayment.save();
                
			}

		}
	}
	
		catch(Exception any_e)
		{
			LOG.error("InvoicePaymentInstructionsBean::createNewInvPaymentInstructions::Exception: ",any_e);
		}
	}
	
	private boolean validateBankInformation(DocumentHandler invPayInstDoc, DocumentHandler inputDoc) throws RemoteException, AmsException {
		boolean returnFlag = true;
		String value = invPayInstDoc.getAttribute("/ben_branch_code");
		String transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");
		String country = InvoiceUtility.fetchOpOrgCountry(" ( SELECT C_CUST_ENTER_TERMS_OID FROM TRANSACTION WHERE TRANSACTION_OID ="+transactionOid+") "); 
		if (StringFunction.isNotBlank(value)){
		
			 List<Object> sqlParams = new ArrayList();
			 StringBuilder sqlQuery = new StringBuilder();
		     sqlQuery.append("select BANK_BRANCH_CODE, case when bank_name is null then branch_name else bank_name end as bank_name");
		     sqlQuery.append(", address_line_1, address_line_2, address_city, address_state_province, address_country from BANK_BRANCH_RULE ");
		     sqlQuery.append(" where upper(BANK_BRANCH_CODE) = ? ");
		     sqlParams.add(value.toUpperCase());
		     if (StringFunction.isNotBlank(country) && 
		    		 (StringFunction.isNotBlank(invPayInstDoc.getAttribute("/pay_method")) && 
		    				 !TradePortalConstants.PAYMENT_METHOD_CBFT.equals(invPayInstDoc.getAttribute("/pay_method")))){
		    	 sqlQuery.append(" and upper(address_country) =? ");
		    	 sqlParams.add(country.toUpperCase());
		     }
		     if (StringFunction.isNotBlank(invPayInstDoc.getAttribute("/pay_method"))){
		    	sqlQuery.append(" and upper(PAYMENT_METHOD) = ? ");
			    sqlParams.add(invPayInstDoc.getAttribute("/pay_method").toUpperCase());
		     }
			DocumentHandler result = (DocumentHandler)DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);
			
			if (result==null){
				this.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.INVALID_BANK_BRNCH_CODE, getDescription(invPayInstDoc.getAttribute("/pay_method"),"PAYMENT_METHOD"), getDescription(country,"COUNTRY"));
				returnFlag =  false;
			}else{
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("name")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/BANK_NAME"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_bank_name", result.getAttribute("/ResultSetRecord/BANK_NAME"));
				}
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("address_line_1")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_LINE_1"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_branch_address1", result.getAttribute("/ResultSetRecord/ADDRESS_LINE_1"));
				}
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("address_line_2")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_LINE_2"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_branch_address2", result.getAttribute("/ResultSetRecord/ADDRESS_LINE_2"));
				}
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("address_city")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_CITY"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_bank_city", result.getAttribute("/ResultSetRecord/ADDRESS_CITY"));
				}
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("address_state_province")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_STATE_PROVINCE"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_bank_prvnce", result.getAttribute("/ResultSetRecord/ADDRESS_STATE_PROVINCE"));
				}
				if (StringFunction.isBlank(invPayInstDoc.getAttribute("address_country")) && StringFunction.isNotBlank(result.getAttribute("/ResultSetRecord/ADDRESS_COUNTRY"))){
					inputDoc.setAttribute("/InvoicePaymentInstructions/ben_branch_country", result.getAttribute("/ResultSetRecord/ADDRESS_COUNTRY"));
				}
				
			}
		}	
		String bankName = invPayInstDoc.getAttribute("/ben_bank_name");
		String addressLine1 = invPayInstDoc.getAttribute("/ben_branch_address1");
		if (StringFunction.isBlank(value) && StringFunction.isBlank(bankName)){
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.BEN_BANK_NAME_BRANCH_REQD);
			returnFlag =  false;
		}
		if (StringFunction.isBlank(value) && StringFunction.isNotBlank(bankName) && StringFunction.isBlank(addressLine1)){
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.BEN_BANK_NAME_ADDR_DATA_REQD);
			returnFlag =  false;
		}

		return returnFlag;
	}


	private boolean validateSameCurrencyCode(String enteredCurrCode,
			long transactionOid, String invPaymentInstructionsOid) throws AmsException, RemoteException {
		boolean returnFlag = true;
		String transactionOID =	Long.toString(transactionOid);

		//jgadela R92 - SQL INJECTION FIX
		String sqlQuery = "SELECT IPI.AMOUNT_CURRENCY_CODE FROM INVOICE_PAYMENT_INSTRUCTIONS IPI WHERE IPI.p_transaction_oid = ? " ;
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{transactionOID});
		if (resultsDoc != null){
			Vector ccyCodeList = resultsDoc.getFragments("/ResultSetRecord");
			String firstCurrencyCode = resultsDoc.getAttribute("/ResultSetRecord(" + 0 + ")/AMOUNT_CURRENCY_CODE");
			if (StringFunction.isNotBlank(enteredCurrCode )){
				if (!enteredCurrCode.equals(firstCurrencyCode) && 
						!(ccyCodeList.size() == 1 && StringFunction.isNotBlank(invPaymentInstructionsOid))){
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DIFFERENT_PAYMENTBENE_CURRENCY, "");
					returnFlag = false;

				}
			}

		}
		return returnFlag;
		
	}


	private boolean validateSamePaymentMethod(String enteredPaymentMethod,
			long transactionOid, String invPaymentInstructionsOid) throws AmsException, RemoteException {
		boolean returnFlag = true;
		String transactionOID =	Long.toString(transactionOid);

		//jgadela R92 - SQL INJECTION FIX
		String sqlQuery = "SELECT IPI.PAY_METHOD  FROM INVOICE_PAYMENT_INSTRUCTIONS IPI WHERE IPI.p_transaction_oid = ? " ;
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{transactionOID});

		if (resultsDoc != null){
			Vector payMethodList = resultsDoc.getFragments("/ResultSetRecord");
			String firstPaymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(" + 0 + ")/PAY_METHOD");
			if (StringFunction.isNotBlank(enteredPaymentMethod )){
				if (!enteredPaymentMethod.equals(firstPaymentMethodType) && 
						!(payMethodList.size() == 1 && StringFunction.isNotBlank(invPaymentInstructionsOid))){
					this.getErrorManager().issueError(getClass().getName(),
							TradePortalConstants.DIFFERENT_PAYMENTBENE_PAYMENTMETHOD);
					returnFlag = false;

				}
			}

		}
		return returnFlag;
		
		
	}

	private boolean validatePaymentMethod(DocumentHandler invPayInstDoc, String transactionOid) throws RemoteException, AmsException {

		boolean isValid = true;
		Set validPaymentMethods = new HashSet();
		validPaymentMethods.add("ACH");
		validPaymentMethods.add("RTGS");
		validPaymentMethods.add("CBFT");

		StringBuilder whereClause = new StringBuilder();

		List<Object> sqlParams = new ArrayList();

		String paymentMethod = invPayInstDoc.getAttribute("/pay_method");
		String country = InvoiceUtility.fetchOpOrgCountry(" ( SELECT C_CUST_ENTER_TERMS_OID FROM TRANSACTION WHERE TRANSACTION_OID = "+transactionOid+") "); 

		String currency = invPayInstDoc.getAttribute("/amount_currency_code");

		whereClause.append(" PAYMENT_METHOD = ? AND ");
		sqlParams.add(paymentMethod);
		if (StringFunction.isNotBlank(country)) {
		   whereClause.append(" COUNTRY = ? AND ");
		   sqlParams.add(country);
		}
		whereClause.append(" CURRENCY = ?");
		sqlParams.add(currency);

		StringBuilder selectOffsetdays = (new StringBuilder("select offset_days from PAYMENT_METHOD_VALIDATION where ")).append(whereClause);
		DocumentHandler paymentMethodRec = DatabaseQueryBean.getXmlResultSet(selectOffsetdays.toString(), false, sqlParams);
		
		if (paymentMethodRec == null && StringFunction.isNotBlank(country) )
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION,
					getDescription(paymentMethod,"PAYMENT_METHOD"), getDescription(currency,"CURRENCY_CODE"), getDescription(country,"COUNTRY"));
			isValid = false;
		}

		if (!validPaymentMethods.contains(paymentMethod)){
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD,
					getDescription(paymentMethod,"PAYMENT_METHOD"));
			isValid = false;
		}

		return isValid;
	}


	private String getDescription(String code, String tableType) {

		try {
			return ReferenceDataManager.getRefDataMgr().getDescr(tableType, code);
		} catch (AmsException e) {
			LOG.error("Error fetching description: ",e);
			return code;
		}
	}

	
	private void validatePayeeField(String fieldValue, boolean returnFlag, String fieldName)
	throws RemoteException, AmsException
	{

		if (StringFunction.isBlank(fieldValue))
		{
			this.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.PAYEE_DATA_MISSING_ERR, fieldName);

		}
	}
	
	/**Added for CR-1001 Rel9.4
	 * 
	 * @param paymentMethod
	 * @param invPayInstDoc
	 * @param inputDoc
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean validateReportingCodes(String paymentMethod, DocumentHandler invPayInstDoc, DocumentHandler inputDoc)
			throws RemoteException, AmsException
			{

		String instrumentOid = inputDoc.getAttribute("/Instrument/instrument_oid");
		Instrument instrument = (Instrument) this.createServerEJB("Instrument");
		instrument.getData(Long.parseLong(instrumentOid));
		String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
		
		OperationalBankOrganization opBankOrg = (OperationalBankOrganization) this.createServerEJB("OperationalBankOrganization",
													  Long.parseLong(opBankOrgOid));
		String bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");
		
		String[] reportingCodes = new String[2];
    	reportingCodes[0] = invPayInstDoc.getAttribute("/reporting_code_1");
    	reportingCodes[1] = invPayInstDoc.getAttribute("/reporting_code_2");
		
    	boolean isValid = true;
    	isValid = InvoiceUtility.validateReportingCodes(bankGroupOid, paymentMethod, reportingCodes,"","", errMgr);
		return isValid;
	}

		/**
	 * This method deletes a single Domestic Payment from the Domestic Payment
	 * Transaction/ Template.
	 * Method verifies that all required fields are presnt            .
	 * Method verifies that amount is not zero            .
	 * @param inputDoc com.amsinc.ecsg.util - the input doc
	 * @return Sting -- success indicator or the payee_name attribute of failed Domestic Payment entry to report to the user
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	public String deleteInvoicePaymentInstruction (DocumentHandler inputDoc)
	throws RemoteException, AmsException
	{

		String domesticPaymentPayee = null;
		try
		{
                if (!("SelectTemplate".equals(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
                    TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/InvoicePaymentInstructions/fixedPmtInd/")))
                {
                    LOG.debug("DPBean::deleteDomesticPayment - trying to delete payee to fixed template...");
                    if (!StringFunction.isBlank(inputDoc.getAttribute("/InvoicePaymentInstructions/inv_pay_inst_oid/")))
                        {
                            domesticPaymentPayee = TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR;
                        }
                }
                else
                {
                    //Domestic payment oid must be supplied
                    long domesticPaymentOid = 0;

					String domesticPaymentOidStr =
						inputDoc.getAttribute("/InvoicePaymentInstructions/inv_pay_inst_oid/");
					if (StringFunction.isBlank(domesticPaymentOidStr))
					{
						LOG.debug("DopesticPaymentBean::Delete - Error: domestic_payment_oid not present");
						return TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE;
					}
					else
					{
						domesticPaymentOid = Long.parseLong(domesticPaymentOidStr);
					}

					//Obtains data; save payee_name attribute for reportin; delete DP Bean
					//long ldomesticPaymentOid = (new Long(domesticPaymentOid)).longValue();
					this.getData(domesticPaymentOid);
					domesticPaymentPayee = this.getAttribute("ben_name"); //store this in case it is needed with the error msg
					//store this so the inputDoc contains the amoutn from the database in case it is needed by the calling routine.
					String domesticPaymentAmount = this.getAttribute("amount");
					inputDoc.setAttribute("/InvoicePaymentInstructions/amount", domesticPaymentAmount);
					this.delete();
                }
		}
		catch (InvalidObjectIdentifierException ex)
		{
			// If the object does not exist, issue an error
			this.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());
			domesticPaymentPayee = TradePortalConstants.TASK_NOT_SUCCESSFUL;
		} catch (Exception any_e)
		{
			LOG.error("DopesticPaymentBean::Delete - Error: generic exception: ",any_e);
			domesticPaymentPayee = TradePortalConstants.TASK_NOT_SUCCESSFUL;
		} finally
		{

			LOG.debug("DomesticPaymentBean: Result of delete is {}"
					, domesticPaymentPayee);
		}
		return domesticPaymentPayee;
	}
	/**
	 * This method updates Terms parameters int he inputDoc so they can be saved with Transaction's save.
	 *
	 * @return BigDecimal totalAmount
	 * @param com.amsinc.ecsg.util.DocumentHandler inputDoc
	 * @param int numberOfPayees
	 */
	public DocumentHandler processInvPayTerms(DocumentHandler inputDoc, int numberOfPayees)
	throws RemoteException, AmsException
	{

		//Ensure that Terms First Party data is properly set based on the total number of Payees with this transaction
		//In case of Direct Debit transaction, it is Payers.
		if (numberOfPayees == 1)
		{
			//Beneficiary Name may be upto 140 chars long. Terms only allow upt 35 chars values: use substring with set.
			String partyName = inputDoc.getAttribute("/InvoicePaymentInstructions/ben_name/");
			int termSizeLimit = partyName.length();
			if (termSizeLimit > 35) termSizeLimit = 35;
			inputDoc.setAttribute("/Terms/FirstTermsParty/name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/reference_number", inputDoc.getAttribute("/InvoicePaymentInstructions/customer_reference/"));
			inputDoc.setAttribute("/Terms/FirstTermsParty/terms_party_type", TermsPartyType.BENEFICIARY);
		}
		if (numberOfPayees > 1)
		{
			if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE));

				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE));
			}
			else
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));

				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
				inputDoc.setAttribute("/Terms/FirstTermsParty/terms_party_type", TermsPartyType.BENEFICIARY);
			}
			//IR - PAUJ083080997 - Set Address Line 1 , 2 and 3 to Empty in case of Multiple Payee.
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_1", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_2", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_3", "");
			inputDoc.setAttribute("/Terms/reference_number", getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE));
		}

		return inputDoc;

	}
	}
