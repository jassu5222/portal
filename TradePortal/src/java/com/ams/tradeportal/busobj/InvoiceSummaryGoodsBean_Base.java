package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;

public class InvoiceSummaryGoodsBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceSummaryGoodsBean_Base.class);

	 
	/*
	 * Describes the mapping of an uploaded file to Invoice definiton.   
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* inv_upload_goods_oid - Unique identifier */
	      attributeMgr.registerAttribute("upload_invoice_goods_oid", "upload_invoice_goods_oid", "ObjectIDAttribute");
	      /* purchase_order_id - field representing the Purchase Order Id against which the invoice is shipping. */
	      attributeMgr.registerAttribute("purchase_order_id", "purchase_order_id");
	      
	      /* description - Description of the definition for convenience. */
	      attributeMgr.registerAttribute("goods_description", "goods_description");
	     
	      /* incoterm_req - field representing the Incoterm associated with an underlying shipment */
	      attributeMgr.registerReferenceAttribute("incoterm", "incoterm","INCOTERM");
	      
	      /* country_of_loding - Country of Loading for the goods that the invoice is covering */
	      attributeMgr.registerAttribute("country_of_loading", "country_of_loading");
	      
	      /* country_of_discharge - Country of Discharge for the goods that the invoice is covering */
	      attributeMgr.registerAttribute("country_of_discharge", "country_of_discharge");
	      
	      /* vessel - vessel shipping the goods that the invoice is covering */
	      attributeMgr.registerAttribute("vessel", "vessel");
	      
	      
	      /* carrier - the carrier for the goods that the invoice is covering. */
	      attributeMgr.registerAttribute("carrier", "carrier");
	      
	      /* actual_ship_date- The valid date format can be selected from the dropdown list*/
	      attributeMgr.registerAttribute("actual_ship_date", "actual_ship_date","DateAttribute");
	      /* Buyer User defined label 1 to 10- Buyer User Defined Fields  */
	      attributeMgr.registerAttribute("buyer_users_def1_label", "buyer_users_def1_label");
	      attributeMgr.registerAttribute("buyer_users_def2_label", "buyer_users_def2_label");
	      attributeMgr.registerAttribute("buyer_users_def3_label", "buyer_users_def3_label");
	      attributeMgr.registerAttribute("buyer_users_def4_label", "buyer_users_def4_label");
	      attributeMgr.registerAttribute("buyer_users_def5_label", "buyer_users_def5_label");
	      attributeMgr.registerAttribute("buyer_users_def6_label", "buyer_users_def6_label");
	      attributeMgr.registerAttribute("buyer_users_def7_label", "buyer_users_def7_label");
	      attributeMgr.registerAttribute("buyer_users_def8_label", "buyer_users_def8_label");
	      attributeMgr.registerAttribute("buyer_users_def9_label", "buyer_users_def9_label");
	      attributeMgr.registerAttribute("buyer_users_def10_label", "buyer_users_def10_label");
	      
	      /* Buyer User defined value 1 to 10 */
	      attributeMgr.registerAttribute("buyer_users_def1_value", "buyer_users_def1_value");
	      attributeMgr.registerAttribute("buyer_users_def2_value", "buyer_users_def2_value");
	      attributeMgr.registerAttribute("buyer_users_def3_value", "buyer_users_def3_value");
	      attributeMgr.registerAttribute("buyer_users_def4_value", "buyer_users_def4_value");
	      attributeMgr.registerAttribute("buyer_users_def5_value", "buyer_users_def5_value");
	      attributeMgr.registerAttribute("buyer_users_def6_value", "buyer_users_def6_value");
	      attributeMgr.registerAttribute("buyer_users_def7_value", "buyer_users_def7_value");
	      attributeMgr.registerAttribute("buyer_users_def8_value", "buyer_users_def8_value");
	      attributeMgr.registerAttribute("buyer_users_def9_value", "buyer_users_def9_value");
	      attributeMgr.registerAttribute("buyer_users_def10_value", "buyer_users_def10_value");
	      
	      /* Seller User defined label1 to 10 */
	      attributeMgr.registerAttribute("seller_users_def1_label", "seller_users_def1_label");
	      attributeMgr.registerAttribute("seller_users_def2_label", "seller_users_def2_label");
	      attributeMgr.registerAttribute("seller_users_def3_label", "seller_users_def3_label");
	      attributeMgr.registerAttribute("seller_users_def4_label", "seller_users_def4_label");
	      attributeMgr.registerAttribute("seller_users_def5_label", "seller_users_def5_label");
	      attributeMgr.registerAttribute("seller_users_def6_label", "seller_users_def6_label");
	      attributeMgr.registerAttribute("seller_users_def7_label", "seller_users_def7_label");
	      attributeMgr.registerAttribute("seller_users_def8_label", "seller_users_def8_label");
	      attributeMgr.registerAttribute("seller_users_def9_label", "seller_users_def9_label");
	      attributeMgr.registerAttribute("seller_users_def10_label", "seller_users_def10_label");
	      
	      /* Seller User defined value 1 to 10 */
	      attributeMgr.registerAttribute("seller_users_def1_value", "seller_users_def1_value");
	      attributeMgr.registerAttribute("seller_users_def2_value", "seller_users_def2_value");
	      attributeMgr.registerAttribute("seller_users_def3_value", "seller_users_def3_value");
	      attributeMgr.registerAttribute("seller_users_def4_value", "seller_users_def4_value");
	      attributeMgr.registerAttribute("seller_users_def5_value", "seller_users_def5_value");
	      attributeMgr.registerAttribute("seller_users_def6_value", "seller_users_def6_value");
	      attributeMgr.registerAttribute("seller_users_def7_value", "seller_users_def7_value");
	      attributeMgr.registerAttribute("seller_users_def8_value", "seller_users_def8_value");
	      attributeMgr.registerAttribute("seller_users_def9_value", "seller_users_def9_value");
	      attributeMgr.registerAttribute("seller_users_def10_value", "seller_users_def10_value");
	      attributeMgr.registerAttribute("invoice_summary_data_oid", "p_invoice_summary_data_oid","ParentIDAttribute");
	     
	    
	      
	   }
	   /* 
	    * Register the components of the business object
	    */
	    protected void registerComponents() throws RemoteException, AmsException
	    {  
	       /* InvoiceLineItemDetailList -  */
	       registerOneToManyComponent("InvoiceLineItemDetailList","InvoiceLineItemDetailList");
	    }
	}
