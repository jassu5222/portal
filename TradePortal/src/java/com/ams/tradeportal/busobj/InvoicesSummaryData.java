package com.ams.tradeportal.busobj;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.web.BeanManager;

import java.rmi.*;


/**
 * Describes the mapping of an uploaded file to Invoice Definition stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface InvoicesSummaryData extends TradePortalBusinessObject {

	/**
	 * Determines if this invoice is actually a credit note.
	 * 
	 * @return true if amount is negative; otherwise, false
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public boolean isCreditNote() throws AmsException, RemoteException;

	/**
	 * Determines if this invoice has an instrument type assigned.
	 * 
	 * @return true if instrument type is populated
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public boolean hasInstrumentType() throws AmsException, RemoteException;

	/**
	 * Validates the instrument type.  An AmsException is thrown with the
	 * appropriate error code if the instrument type is missing or invalid.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void validateInstrumentType() throws AmsException, RemoteException;

	/**
	 * Calculates the discount or interest and updates this invoice with the
	 * appropriate values.  If any validations fail, an AmsException is thrown
	 * with the appropriate error code.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void calculateInterestDiscount(BeanManager beanMr) throws AmsException, RemoteException;

	/**
	 * Calculates the discount or interest and updates this invoice with the
	 * appropriate values.  If any validations fail, an AmsException is thrown
	 * with the appropriate error code.
	 * 
	 * @param matchingRule
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void calculateInterestDiscount(ArMatchingRule matchingRule,BeanManager beanMr)
	throws AmsException, RemoteException;
        

	public void calculateInterestDiscount(ArMatchingRule matchingRule,CorporateOrganizationWebBean corpOrg)
			throws AmsException, RemoteException;

	/**
	 * Validates compliance of Days Variance of Payment Date to Invoice Due
	 * Date and the Days Allowed for Financing Before Invoice Due Date or
	 * Payment Date and calculates the number of tenor days.  If the days
	 * values are non-compliant, an AmsException is thrown with the
	 * appropriate error code.
	 * 
	 * @return number of tenor days
	 * @throws AmsException
	 * @throws RemoteException
	 */
	
	/**
	 * Identifies and retrieves the Matching Rule for this invoice.
	 * First it looks for a rule with a matching buyer_name.  If no match is
	 * found for the buyer_name, it looks for a rule with a matching buyer_id.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public ArMatchingRule getMatchingRule() throws AmsException, RemoteException;
	public void verifyInvoiceGroup() throws AmsException, RemoteException ;
        
	public void calculateInterestDiscount(String userOid) throws AmsException, RemoteException;
	
	public String getActualDate() throws AmsException, RemoteException;//IR T36000017596 
	public void updateInvoiceGroup(String invoiceGroupOid,boolean isIgnoreCurrentInvoice) throws AmsException, RemoteException ;// IR 18421
	public String getTpRuleName() throws AmsException, RemoteException ;
	public String getTpRuleName(ArMatchingRule rule) throws AmsException, RemoteException ;
	public String createInvoiceGroup() throws AmsException, RemoteException ;
 	public void close(String uploadUser) throws AmsException, RemoteException ;
 	    
}
