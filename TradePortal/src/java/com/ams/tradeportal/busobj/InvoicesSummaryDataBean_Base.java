package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


public class InvoicesSummaryDataBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesSummaryDataBean_Base.class);

	 
	/*
	 * Describes the mapping of an uploaded file to Invoice definiton.   
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* inv_upload_definition_oid - Unique identifier */
	      attributeMgr.registerAttribute("upload_invoice_oid", "upload_invoice_oid", "ObjectIDAttribute");
	      /* Invoice ID - Unique Id */
	      attributeMgr.registerAttribute("invoice_id", "invoice_id");
	      /* Issue date from the invoice file  */
	      attributeMgr.registerAttribute("issue_date", "issue_date", "DateAttribute");
	      /* Due date from the invoice file  */
	      attributeMgr.registerAttribute("due_date", "due_date","DateAttribute");
	      /*  This is the Buyer name as established in the Trading Partner rule.  */
	      attributeMgr.registerAttribute("buyer_name", "buyer_name");
	      /*  This is the Buyer id as established in the Trading Partner rule.  */
	      attributeMgr.registerAttribute("buyer_id", "buyer_id");
	      
	      /* description - Description of the definition for convenience. */
	      attributeMgr.registerAttribute("currency", "currency");
	      /* description - Description of the definition for convenience. */
	       attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	       /* payment_date - valid date format can be selected from the dropdown list. */
	      attributeMgr.registerAttribute("payment_date", "payment_date","DateAttribute");
	       attributeMgr.registerReferenceAttribute("invoice_status", "invoice_status", "UPLOAD_INVOICE_STATUS");
	           
	      /* linked_instrument_type field representing the code value for the instrument to which the invoices should ultimately be associated:
	       *  REC (Receivables), PAY (Payables), ATP (Approval to Pay), LNR (Loan request) */
	      attributeMgr.registerReferenceAttribute("linked_to_instrument_type", "linked_to_instrument_type", "UPLOAD_INV_LINKED_INSTR_TY");
	      /* invoice finance amount */
	      attributeMgr.registerAttribute("invoice_finance_amount", "invoice_finance_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      /* net invoice amount */
	      attributeMgr.registerAttribute("net_invoice_finance_amount", "net_invoice_finance_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");	
	      /* estimated interst amount */
	      attributeMgr.registerAttribute("estimated_interest_amount", "estimated_interest_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");	
	      /* amended invoice number */
	      attributeMgr.registerAttribute("amended_invoice_number", "amended_invoice_number","NumberAttribute");	
	      
          /* first_authorize_status_date - Timestamp (in GMT) of when the first authorization took place on this PayRemit.
	         Depending on the corporate customer's settings, this may be the only authorization
	         that takes places on this Invoice. */
	      attributeMgr.registerAttribute("first_authorize_status_date", "first_authorize_status_date", "DateTimeAttribute");
	      
	      /* second_authorize_status_date - Timestamp (in GMT) of when the second authorization took place on this transaction.
	         Depending on the corporate customer's settings, only one authorization may
	         be necessary. */
	      attributeMgr.registerAttribute("second_authorize_status_date", "second_authorize_status_date", "DateTimeAttribute");
	      
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
          /*  This is the Invoice Type = INT for now  */
	      attributeMgr.registerAttribute("invoice_type", "invoice_type");
	      /* first_authorizing_user_oid - The user that performed the first authorization on a PayRemit.  Depending
	         on the corporate customer settings, this may be the only user to perform
	         an authorize on this Invoice. */
	      attributeMgr.registerAssociation("first_authorizing_user_oid", "a_first_authorizing_user_oid", "User");
	      
	      /* second_authorizing_user_oid - The user that performed the second authorization on a PayRemit.  Depending
	         on the corporate customer settings, this may be no second user to authorize
	         this Invoice. */
	      attributeMgr.registerAssociation("second_authorizing_user_oid", "a_second_authorizing_user_oid", "User");
	      
	      /* first_authorizing_work_group_oid - The work group of the first user that performed the authorization on a PayRemit.
	         Depending on the corporate customer settings, this user may be the only
	         user to perform an authorize on this Invoice. */
	      attributeMgr.registerAssociation("first_authorizing_work_group_oid", "a_first_auth_work_group_oid", "WorkGroup");
	      
	      /* second_authorizing_work_group_oid - The work group of the second user that performed the authorization on a
	         PayRemit.  Depending on the corporate customer settings, this may be no
	         second user to authorize this Invoice. */
	      attributeMgr.registerAssociation("second_authorizing_work_group_oid", "a_second_auth_work_group_oid", "WorkGroup");

	      /* invoice_group_oid - The Invoice Group that this invoice currently belongs to.
	       * Note that this must be defined as an attribute to avoid an InvalidObjectIdentifierException
	       * when the InvoiceGroup is created during preSave */
	      //attributeMgr.registerAssociation("invoice_group_oid", "a_invoice_group_oid", "InvoiceGroup");
	      attributeMgr.registerAttribute("invoice_group_oid", "a_invoice_group_oid", "NumberAttribute");

	      attributeMgr.registerAssociation("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");
	      attributeMgr.registerAttribute("invoice_file_upload_oid", "p_invoice_file_upload_oid", "ParentIDAttribute");

	      //SHR CR708 Rel8.1.1 start
	      /* represents either payables/receivables invoice*/
	      attributeMgr.registerAttribute("invoice_classification","invoice_classification");
	      /*  This is the Seller name as established in the Trading Partner rule.  */
	      attributeMgr.registerAttribute("seller_name", "seller_name");
	      /*  This is the Seller id as established in the Trading Partner rule.  */
	      attributeMgr.registerAttribute("seller_id", "seller_id");
	      /* upload_definition_oid -  */
	      attributeMgr.registerAttribute("upload_definition_oid", "a_upload_definition_oid");
	      /*
		  * terms_oid - This association indicates to which, Invoice is
		  * associated.
		  */
		  attributeMgr.registerAttribute("terms_oid", "a_terms_oid");

		  /* user_oid - user oid . */
		  attributeMgr.registerAttribute("user_oid", "a_user_oid","LocalAttribute");
		  /* action - action performed */
		  attributeMgr.registerAttribute("action", "action", "LocalAttribute");
		  /* status - Invoice status */
		  attributeMgr.registerReferenceAttribute("status", "status", "PURCHASE_ORDER_STATUS");
		  /* creation_date_time - Timestamp when Purchase Order was created. */
		  attributeMgr.registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");

		  /* deleted_ind - Indicates whether the Purchase Order is deleted. */
		  attributeMgr.registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");

		  /* amend_seq_no - Tracks number of amend done. */
		  attributeMgr.registerAttribute("amend_seq_no", "amend_seq_no", "NumberAttribute");
		  /* transaction_oid - */
		  attributeMgr.registerAssociation("transaction_oid", "a_transaction_oid", "Transaction");

	      /* instrument_oid - */
		  attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
		  attributeMgr.registerAttribute("tp_rule_name", "tp_rule_name", "LocalAttribute");
	      //SHR CR708 Rel8.1.1 end
		  
		  //IValavala CR 741
		   attributeMgr.registerAttribute("cred_discount_code", "cred_discount_code");
		   attributeMgr.registerAttribute("cred_gl_code", "cred_gl_code");
		   attributeMgr.registerAttribute("cred_comments", "cred_comments");
		   //attributeMgr.registerAttribute("credit_discount_code_id", "credit_discount_code_id");
		   //attributeMgr.registerAttribute("credit_discount_gl_code_id", "credit_discount_gl_code_id");
		   //attributeMgr.registerAttribute("credit_discount_comments_id", "credit_discount_comments_id");	
		   attributeMgr.registerAttribute("credit_discount_code_id", "cred_discount_code");
		   attributeMgr.registerAttribute("credit_discount_gl_code_id", "cred_gl_code");
		   attributeMgr.registerAttribute("credit_discount_comments_id", "cred_comments");		   
		  //IValavala End CR 741
		  //Srini_D Rel 8.2 CR-709 01/23/2013 Start
		   /* loan_type   */
	      attributeMgr.registerAttribute("loan_type", "loan_type");

		  //Srini_D Rel 8.2 CR-709 01/23/2013 End
	   // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
	      attributeMgr.registerAttribute("credit_note", "credit_note");
	      attributeMgr.registerAttribute("pay_method", "pay_method");
	      attributeMgr.registerAttribute("ben_acct_num", "ben_acct_num");
	      attributeMgr.registerAttribute("ben_add1", "ben_address_one");
	      attributeMgr.registerAttribute("ben_add2", "ben_address_two");
	      attributeMgr.registerAttribute("ben_add3", "ben_address_three");
	      attributeMgr.registerAttribute("ben_country", "ben_country");
	      attributeMgr.registerAttribute("ben_bank_name", "ben_bank_name");
	      attributeMgr.registerAttribute("ben_branch_code", "ben_branch_code");
	      attributeMgr.registerAttribute("ben_branch_add1", "ben_branch_address1");
	      attributeMgr.registerAttribute("ben_branch_add2", "ben_branch_address2");
	      attributeMgr.registerAttribute("ben_bank_city", "ben_bank_city");
	      attributeMgr.registerAttribute("ben_bank_province", "ben_bank_prvnce");
	      attributeMgr.registerAttribute("ben_branch_country", "ben_branch_country");
	      attributeMgr.registerAttribute("charges", "payment_charges");
	      attributeMgr.registerAttribute("central_bank_rep1", "central_bank_rep1");
	      attributeMgr.registerAttribute("central_bank_rep2", "central_bank_rep2");
	      attributeMgr.registerAttribute("central_bank_rep3", "central_bank_rep3");	      
	   // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 END    
                  
	      attributeMgr.registerAttribute("buyer_tps_customer_id", "buyer_tps_customer_id");
	      attributeMgr.registerAttribute("seller_tps_customer_id", "seller_tps_customer_id");
	      
	      // Pavani Rel 8.3 CR 821 Start
	      /* panel_auth_group_oid - Panel group associated to an Invoice. */
	      attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");
	      
	      /* panel_auth_range_oid - Panel Range Oid to a invoices summary data. */
	      attributeMgr.registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
	      
	      /* opt_lock - Optimistic lock attribute*/
	      attributeMgr.registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");

		  // Pavani Rel 8.3 CR 821 End
	      
	      //CR 913 start
	      attributeMgr.registerAttribute("payment_amount", "payment_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      attributeMgr.registerAttribute("send_to_supplier_date", "send_to_supplier_date", "DateAttribute");  
	      attributeMgr.registerAttribute("buyer_acct_num", "buyer_acct_num");
	      attributeMgr.registerAttribute("buyer_acct_currency", "buyer_acct_currency");
	      attributeMgr.registerAttribute("ben_email_addr", "ben_email_addr");
	      attributeMgr.registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");
	      
	     
	      attributeMgr.registerAttribute("end_to_end_id", "end_to_end_id");
	      attributeMgr.registerAttribute("total_credit_note_amount", "total_credit_note_amount","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      attributeMgr.registerAttribute("end_to_end_group_num","end_to_end_group_num");
	      attributeMgr.registerAttribute("credit_invoice_group_num","credit_invoice_group_num");
	      
	      //Srinivasu_D CR#1006 Rel9.3 05/04/2015 - Added attributes to INVOICES_SUMMARY_DATA table
	      attributeMgr.registerAttribute("pin_notification_email_sent_ind", "pin_notify_email_sent_ind", "IndicatorAttribute");
	      attributeMgr.registerAttribute("rin_notification_email_sent_ind", "rin_notify_email_sent_ind", "IndicatorAttribute");
	      attributeMgr.registerAttribute("portal_approval_requested_ind", "portal_approval_req_ind", "IndicatorAttribute");
	      attributeMgr.registerAttribute("portal_approval_requested_work_item", "portal_approval_req_work_item");
	      attributeMgr.registerAttribute("customer_file_ref", "customer_file_ref");
	      attributeMgr.registerAttribute("file_name", "file_name");
	      //Srinivasu_D CR#1006 Rel9.3 05/04/2015 - End
	      attributeMgr.registerAttribute("reqPanleAuth", "reqPanleAuth", "LocalAttribute");
	      attributeMgr.registerAttribute("bypass_grouping", "bypass_grouping", "LocalAttribute");
	     
	      /*Rpasupulati CR 1001 Start */
		     
	      attributeMgr.registerAttribute("reporting_code_1", "reporting_code_1");
	      attributeMgr.registerAttribute("reporting_code_2", "reporting_code_2");
	      /*Rpasupulati CR 1001 End */
	      //CR 913 end
                  
	   }

	/*
	 * Register the components of the business object
	 */
	protected void registerComponents() throws RemoteException, AmsException {
		/* InvoiceSummaryGoodsList - */
		registerOneToManyComponent("InvoiceSummaryGoodsList", "InvoiceSummaryGoodsList");

	    // Nar release 8.3.0.0 CR 821 25 June 2013
	   /* PanelAuthorizerList - The Invoice has many panel Authorizer Users.  */
	    registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
	}

	}
