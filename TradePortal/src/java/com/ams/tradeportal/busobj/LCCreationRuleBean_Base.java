
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Defines a rule that is run against the unassigned purchase order line items
 * when the user wants to group POs and create LCs from them.    All POs that
 * meet a rule are placed into the same "bucket" from which LCs are created
 * and generally will wind up in the same LC.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class LCCreationRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(LCCreationRuleBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* lc_creation_rule_oid - Unique identifier */
      attributeMgr.registerAttribute("lc_creation_rule_oid", "lc_creation_rule_oid", "ObjectIDAttribute");
      
      /* name - Name of the LC Creation Rule */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("LCCreationRuleBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
      
      /* description - Description of the rule for convenience */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("LCCreationRuleBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));
      
      /* creation_date_time - A timestamp (in GMT) of when the rule was created.   This is important because
         the rules are applied in the order in which they were created. */
      attributeMgr.registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");
      
      /* maximum_amount - The maximum amount of an LC that can be created from this rule.  For example,
         if there are five purchase orders of $20,000 each that meet the rule and
         the maximum amount is $65,000, then three will go into one LC and two will
         go into another.  If there were no maximum amount, all five would be placed
         into the same LC. */
      attributeMgr.registerAttribute("maximum_amount", "maximum_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("maximum_amount", getResourceManager().getText("LCCreationRuleBeanAlias.maximum_amount", TradePortalConstants.TEXT_BUNDLE));
      
      /* min_last_shipment_date - All PO line items that meet this rule must have a latest shipment date that
         is greater than the current date plus the number of days in this field. */
      attributeMgr.registerAttribute("min_last_shipment_date", "min_last_shipment_date", "NumberAttribute");
      
      /* max_last_shipment_date - All PO line items that meet this rule must have a latest shipment date that
         is less than the current date plus the number of days in this field. */
      attributeMgr.registerAttribute("max_last_shipment_date", "max_last_shipment_date", "NumberAttribute");
      
      /* pregenerated_sql - When an LC creation rule is saved, the SQL to run the rule is pregenerated.
         This SQL will be used to determine which PO line items meet the criteria.
         By building the SQL when the LC creation rule is saved, it does not have
         to be generated each time the rule is run.  */
      attributeMgr.registerAttribute("pregenerated_sql", "pregenerated_sql");
      
      /* criterion1_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion1_data", "criterion1_data", "PO_LINE_ITEM_FIELD");
      attributeMgr.requiredAttribute("criterion1_data");
      attributeMgr.registerAlias("criterion1_data", getResourceManager().getText("LCCreationRuleBeanAlias.criterion1_data", TradePortalConstants.TEXT_BUNDLE));
      
      /* criterion1_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion1_value", "criterion1_value");
      attributeMgr.requiredAttribute("criterion1_value");
      attributeMgr.registerAlias("criterion1_value", getResourceManager().getText("LCCreationRuleBeanAlias.criterion1_value", TradePortalConstants.TEXT_BUNDLE));
      
      /* criterion2_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion2_data", "criterion2_data", "PO_LINE_ITEM_FIELD");
      
      /* criterion2_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion2_value", "criterion2_value");
      
      /* criterion3_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion3_data", "criterion3_data", "PO_LINE_ITEM_FIELD");
      
      /* criterion3_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion3_value", "criterion3_value");
      
      /* criterion4_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion4_data", "criterion4_data", "PO_LINE_ITEM_FIELD");
      
      /* criterion4_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion4_value", "criterion4_value");
      
      /* criterion5_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion5_data", "criterion5_data", "PO_LINE_ITEM_FIELD");
      
      /* criterion5_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion5_value", "criterion5_value");
      
      /* criterion6_data - The PO line item field on which this criterion is based. */
      attributeMgr.registerReferenceAttribute("criterion6_data", "criterion6_data", "PO_LINE_ITEM_FIELD");
      
      /* criterion6_value - Value that is acceptable for the PO line item field specified in the corresponding
         critertion_data field.   Multiple values can be placed into this field by
         separating them with an exclamation point. */
      attributeMgr.registerAttribute("criterion6_value", "criterion6_value");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* template_oid - The template that will be copied from when instruments are created using
         this LC Creation Rule. */
      attributeMgr.registerAssociation("template_oid", "a_template_oid", "Template");
      attributeMgr.requiredAttribute("template_oid");
      attributeMgr.registerAlias("template_oid",getResourceManager().getText("LCCreationRuleBeanAlias.template_oid",TradePortalConstants.TEXT_BUNDLE));

      /* op_bank_org_oid - The operational bank organization that the instrument will be related to
         when instruments are created using this LC Creation Rule. */
      attributeMgr.registerAssociation("op_bank_org_oid", "a_op_bank_org_oid", "OperationalBankOrganization");
      attributeMgr.requiredAttribute("op_bank_org_oid");
      attributeMgr.registerAlias("op_bank_org_oid",getResourceManager().getText("LCCreationRuleBeanAlias.op_bank_org_oid",TradePortalConstants.TEXT_BUNDLE));

      /* owner_org_oid - An LC Creation Rule is "owned" by an organization.  This component relationship
         represents the ownership. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
       
      /* po_upload_def_oid - Each LC Creation rule must be related to a PO Upload Definition so that
         it will know which fields are defined so that criteria can be set.  This
         association represents that relationship. */
      attributeMgr.registerAssociation("po_upload_def_oid", "a_po_upload_def_oid", "POUploadDefinition");
		//		Srinivasu_D CR707 rel8.0 02/23/2012 Start
	  attributeMgr.registerAssociation("po_struct_pload_def_oid", "a_struct_po_upload_def_oid", "PurchaseOrderDefinition");
		//		Srinivasu_D CR707 rel8.0 02/23/2012 End
      /* instrument_type_code - The instrument type that the creation rule is for.  It is either IMP_LC
         or ATP. */
		//attributeMgr.registerAttribute("po_upload_def_oid", "a_po_upload_def_oid");
		 
      attributeMgr.registerReferenceAttribute("instrument_type_code", "instrument_type_code", "INSTRUMENT_TYPE");
      attributeMgr.requiredAttribute("instrument_type_code");
      attributeMgr.registerAlias("instrument_type_code", getResourceManager().getText("LCCreationRuleBeanAlias.instrument_type_code", TradePortalConstants.TEXT_BUNDLE));
      

   }
   
 
   
 
 
   
}
