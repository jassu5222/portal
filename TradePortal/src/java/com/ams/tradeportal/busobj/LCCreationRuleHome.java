package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Defines a rule that is run against the unassigned purchase order line items
 * when the user wants to group POs and create LCs from them.    All POs that
 * meet a rule are placed into the same "bucket" from which LCs are created
 * and generally will wind up in the same LC.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface LCCreationRuleHome extends EJBHome
{
   public LCCreationRule create()
      throws RemoteException, CreateException, AmsException;

   public LCCreationRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
