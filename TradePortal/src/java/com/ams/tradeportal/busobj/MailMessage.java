package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Business object to store a message or discrepancy notice.   These messages
 * can be created in the back end system and placed into this table by the
 * middleware or they can be created by portal users and sent to the back end
 * system via the middleware.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface MailMessage extends TradePortalBusinessObject
{   
	public boolean deleteMessage(java.lang.String securityRights, java.lang.String userOid) throws RemoteException, AmsException;

	public boolean routeMessage(java.lang.String securityRights, java.lang.String userOid, java.lang.String routeUserOid) throws RemoteException, AmsException;

	public boolean isDeleted() throws RemoteException, AmsException;

	public void setRelatedInstrument(String completeInstrumentId, String ownerOrgOid) throws RemoteException, AmsException;

}
