package com.ams.tradeportal.busobj;

import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.rmi.RemoteException;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;



/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MailMessageBean extends MailMessageBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(MailMessageBean.class);

	public void preSave() throws AmsException {
		try {
			validateMessageForISO8859Chars();
		} catch (RemoteException e) {
			LOG.error("RemoteException while validateMessageForISO8859Chars(): ",e);
		}
	}

	/**
	 * Attempts to set the related_instrument_oid attribute.  Performs
	 * validations and possibly issues errors.
	 *
	 * @param completeInstrumentId - the complete instrument ID of the instrument to which the message is being associated
	 * @param ownerOrgOid - the organization that owns the message
	 */
	public void setRelatedInstrument(String completeInstrumentId, String ownerOrgOid) throws RemoteException, AmsException
	{
		//Always check the passed in Complete Instrument ID is in the DB and that it's there for the
		//Current users Corporate Org.  We need to set the Related instrument Oid which we cant count
		//on being passed in, and it may not always be a reliable oid if the users changes the complete
		//Instrument Id in the editable text field. 
		// The instrument must exist for the user's corporate org, or for a corporate org related to it
		// (children, parents, siblings, etc)      
		if( !StringFunction.isBlank(completeInstrumentId) ) 
		{
			StringBuffer sql = new StringBuffer(); 
			sql.append("select instrument_oid from instrument ");
			sql.append("where complete_instrument_id = ?");
			sql.append(" AND a_corp_org_oid in ( " );

			// The inner query retrieves all corporate orgs related to the user's 
			// corporate org.   In the inner part of this query, it walks up the 
			// tree to determine the highest corporate org in the hierarchy.
			// The outer part of this query retrieves the highest point and all of 
			// its children.  The final result is all of the orgs in the hierarchy.
			StringBuffer innerQuery = new StringBuffer();
			innerQuery.append("select organization_oid ");
			innerQuery.append("from corporate_org ");
			innerQuery.append("start with organization_oid = ");
			innerQuery.append("  (select organization_oid ");
			innerQuery.append("   from corporate_org ");
			innerQuery.append("   where p_parent_corp_org_oid is null ");
			innerQuery.append("   start with organization_oid =  ? ");
			innerQuery.append("   connect by organization_oid = prior p_parent_corp_org_oid) ");
			innerQuery.append("connect by prior organization_oid = p_parent_corp_org_oid ");

			sql.append(innerQuery.toString());
			sql.append(" ) ");


			DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, completeInstrumentId, ownerOrgOid);

			if( results != null )
			{


				Vector resultsVector = results.getFragments("/ResultSetRecord/");

				if( ((DocumentHandler)resultsVector.elementAt(0)) != null )
				{
					DocumentHandler myDoc = ((DocumentHandler)resultsVector.elementAt(0));
					this.setAttribute("related_instrument_oid", myDoc.getAttribute("/INSTRUMENT_OID"));
				}
				else
				{
					this.setAttribute("related_instrument_oid", ""); 
					getErrorManager ().issueError (TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ENTER_VALID_INSTRUMENT);
				}
			}
			else
			{
				this.setAttribute("related_instrument_oid", "");
				getErrorManager ().issueError ( TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ENTER_VALID_INSTRUMENT);
			}
		}
		else
		{
			this.setAttribute("related_instrument_oid", "");
		}
	}


	/** Given the securityRights, userOid who initiates the routing and routeUserOid of
	 * the user to route to, this method checks the security, sets the user to route to,
	 * status and date of the message
	 * 
	 * @param  String securityRights  	- the security rights of the user initiating the route
	 * @param  String userOid  		- user who initiates the route
	 * @param  String routeUserOid	- user to route to
	 * @return boolean		if route is successful
	 */
	public boolean routeMessage(String securityRights, String userOid, String routeUserOid)
			throws AmsException, RemoteException
	{



		String messageSource = null;
		String messageStatus = null;
		String [] attributeNames = null;
		String [] attributeValues = null;


		boolean discrepancyFlag = TradePortalConstants.INDICATOR_YES.equals(attributeMgr.getAttributeValue("discrepancy_flag"));

		boolean atpNoticeFlag = TradePortalConstants.INDICATOR_YES.equals(attributeMgr.getAttributeValue("atp_notice_flag"));


		// check to see if user has security to route this message or discrepancy notice
		if (discrepancyFlag)
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.ROUTE_DISCREPANCY_MSG))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Route",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		}

		// check to see if user has security to route this message or ATP Notice
		else if (atpNoticeFlag)
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.ROUTE_DISCREPANCY_MSG))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Route",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		}

		else
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.ROUTE_MESSAGE))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Route",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		}

		messageSource = getAttribute("message_source_type");
		messageStatus = getAttribute("message_status");

		// if the message has previously been deleted, issue error
		// that it's been deleted
		if (messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
				messageStatus.equals(TradePortalConstants.REC_DELETED))
		{
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ITEM_DELETED);
			return false;
		}

		attributeNames = new String[]{"assigned_to_user_oid","last_routed_by_user_oid", "unread_flag"};
		attributeValues = new String[]{routeUserOid, userOid, TradePortalConstants.INDICATOR_YES};

		setAttributes(attributeNames, attributeValues);


		// if message is outgoing, set the last update date
		if (messageSource.equals(TradePortalConstants.PORTAL))
		{
			//update date only for non pre-debit funding message
			if(StringFunction.isBlank(getAttribute("funding_amount")) &&
					StringFunction.isBlank(getAttribute("funding_date")) &&
					StringFunction.isBlank(getAttribute("funding_currency"))){
				setAttribute("last_update_date", DateTimeUtility.getGMTDateTime());
			}

			setAttribute("message_status", TradePortalConstants.SENT_ASSIGNED);
		}
		else
		{
			setAttribute("message_status", TradePortalConstants.REC_ASSIGNED);
		}

		this.save();
		if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY)
			return false;
		else
			return true;

	}

	/** Given the securityRights and userOid who initiates the deletion, 
	 * this method checks the security, deletes the message from the database
	 * or sets the message status to deleted.
	 * 
	 * @param  String securityRights  	- the security rights of the user initiating the delete
	 * @param  String userOid  		- user who initiates the delete
	 * @return boolean		if deletion is successful
	 */
	public boolean deleteMessage(String securityRights, String userOid)
			throws AmsException, RemoteException

	{


		String messageSource = null;
		String messageStatus = null;

		boolean discrepancyFlag = TradePortalConstants.INDICATOR_YES.equals(getAttribute("discrepancy_flag"));

		boolean atpNoticeFlag = TradePortalConstants.INDICATOR_YES.equals(attributeMgr.getAttributeValue("atp_notice_flag"));
		
		boolean settleInstrFlag = !TradePortalConstants.INDICATOR_NO.equals(getAttribute("settlement_instr_flag"));

		// check to see if user has security to route this message or discrepancy notice
		if (discrepancyFlag)
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_DISCREPANCY_MSG))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Delete",
								TradePortalConstants.TEXT_BUNDLE));
				return false;	    }
		}

		// check to see if user has security to route this message or ATP Notice
		else if (atpNoticeFlag)
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_DISCREPANCY_MSG))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Delete",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		} else if (settleInstrFlag) {
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Delete",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		}
		else
		{
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.DELETE_MESSAGE))
			{
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
						TradePortalConstants.NO_ACTION_MESSAGE_AUTH,
						getAttribute("message_subject"),	
						getResourceManager().getText("MessageAction.Delete",
								TradePortalConstants.TEXT_BUNDLE));
				return false;
			}
		}

		messageSource = getAttribute("message_source_type");
		messageStatus = getAttribute("message_status");

		// if the message has previously been deleted, issue error
		// that it's been deleted
		if (messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
				messageStatus.equals(TradePortalConstants.REC_DELETED))
		{
			getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ITEM_DELETED);
			return false;
		}

		// for Outgoing messages
		if (messageSource.equals(TradePortalConstants.PORTAL))
		{
			// message originates from here (Outgoing)
			// if has been sent, marked as Sent and Deleted
			// otherwise, delete the message from the database.
			if (messageStatus.equals(TradePortalConstants.DRAFT) ||
					messageStatus.equals(TradePortalConstants.SENT_ASSIGNED))
			{
				this.delete();
				// if delete is unsuccessful, errors are issued - if there are
				// errors, return false, otherwise return true
				if (this.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY)
					return false;
				else
					return true;
			}
			else
			{
				setAttribute("message_status", TradePortalConstants.SENT_TO_BANK_DELETED);
				setAttribute("last_entry_user_oid", userOid);
				setAttribute("last_update_date", DateTimeUtility.getGMTDateTime());

				this.save();
				if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY)
					return false;
				else
					return true;
			}

		}
		else
		{
			// message is sent from BANK (Incoming)
			setAttribute("message_status", TradePortalConstants.REC_DELETED);
			this.save();
			if (getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY)
				return false;
			else
				return true;
		}

	}  


	/**
	 *  This is a convience method that provides an easy way to 
	 *  determine if the message object has a status of 'deleted'.
	 *  
	 *  @return boolean     - Y/N it has been flagged as deleted.
	 *  @throws AmsException
	 *  @throws RemoteException

	 */

	public boolean isDeleted () throws AmsException, RemoteException
	{
		try{
			String messageStatus = this.getAttribute("message_status");   
			if( messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
					messageStatus.equals(TradePortalConstants.NOT_SENT_DELETED) ||
					messageStatus.equals(TradePortalConstants.REC_DELETED) )
				return true;

		}catch(NullPointerException e) {
			return false;
		}
		return false;
	}

	// REL9.5 IR 43891
	protected String getAllowISO8859OnlyInd(String corpOrgOid) throws AmsException, RemoteException {
		StringBuilder sqlQuery = new StringBuilder("select allow_only_iso8859_ind from CLIENT_BANK c ,corporate_org o");
		sqlQuery.append(" where  o.organization_oid =? AND o.a_client_bank_oid = c.organization_oid");

		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[] { corpOrgOid });
		if (results == null) {
			LOG.debug("validateISO8859OnlyChars(): DB query returned null results. Query was [  {}  ]",sqlQuery);
			return "";
		}

		return results.getAttribute("/ResultSetRecord(0)/ALLOW_ONLY_ISO8859_IND");

	}

	public boolean validateMessageForISO8859Chars() throws AmsException, RemoteException {
		String allowISO8859OnlyInd = getAllowISO8859OnlyInd(this.getAttribute("assigned_to_corp_org_oid"));
		if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(allowISO8859OnlyInd)) {
			final String[] attributesToInclude = { "message_text", "message_subject" };
			return isValidISO8859Chars(this.getErrorManager(), attributesToInclude);

		}
		return false;
	}

	private boolean isValidISO8859Chars(ErrorManager errMgr, String[] attributesToInclude) throws AmsException, RemoteException {
		boolean isValid = true;
		for (int i = 0; i < attributesToInclude.length; i++) {
			String attr = attributesToInclude[i];
			String value = this.getAttribute(attr);
			if (StringFunction.isNotBlank(value)) {
				CharsetEncoder encoder = Charset.forName("ISO-8859-1").newEncoder();
				boolean isValidCharSeq = encoder.canEncode(CharBuffer.wrap(value.toCharArray()));
				if (!isValidCharSeq) {
					isValid = false;
					char[] attrCharSeq = value.toCharArray();
					StringBuilder invalidChars = new StringBuilder();
					for (int j = 0; j < attrCharSeq.length; j++) {
						boolean isValidChar = encoder.canEncode(attrCharSeq[j]);
						if (!isValidChar)
							invalidChars.append(attrCharSeq[j]);
					}
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_ISO_ONLY_8859_CHARS_ERR_1, this.getResourceMgr().getText("MailMessageBeanAlias." + attr, TradePortalConstants.TEXT_BUNDLE), invalidChars.toString());
				}
			}
		}
		return isValid;
	}


}
