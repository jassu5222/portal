
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Business object to store a message or discrepancy notice.   These messages
 * can be created in the back end system and placed into this table by the
 * middleware or they can be created by portal users and sent to the back end
 * system via the middleware.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MailMessageBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(MailMessageBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* message_oid - Unique identifier */
      attributeMgr.registerAttribute("message_oid", "message_oid", "ObjectIDAttribute");
      
      /* message_text - Text that comprises the body of the message */
      attributeMgr.registerAttribute("message_text", "message_text");
      
      /* message_subject - Text that comprises the subject of the mail message */
      attributeMgr.registerAttribute("message_subject", "message_subject");
      attributeMgr.requiredAttribute("message_subject");
      attributeMgr.registerAlias("message_subject", getResourceManager().getText("MailMessageBeanAlias.message_subject", TradePortalConstants.TEXT_BUNDLE));
      
      /* discrepancy_flag - Indicates if this mail message is a discrepancy notice or not.  Yes=Discrepany
         Notice */
      attributeMgr.registerAttribute("discrepancy_flag", "discrepancy_flag", "IndicatorAttribute");
      attributeMgr.requiredAttribute("discrepancy_flag");
      
      /* message_source_type - Indicates whether the message was created on the back end system or created
         on the Trade Portal */
      attributeMgr.registerReferenceAttribute("message_source_type", "message_source_type", "MESSAGE_SOURCE");
      attributeMgr.requiredAttribute("message_source_type");
      
      /* message_status - The status in the lifecycle of a mail message.   Statuses can include Received,
         Draft, Sent to Bank, etc. */
      attributeMgr.registerReferenceAttribute("message_status", "message_status", "MESSAGE_STATUS");
      attributeMgr.requiredAttribute("message_status");
      
      /* unread_flag - Indicates whether or not the message has been read by its intended recipient.
         Yes = message has not been read */
      attributeMgr.registerAttribute("unread_flag", "unread_flag", "IndicatorAttribute");
      attributeMgr.requiredAttribute("unread_flag");
      
      /* last_update_date - Timestamp in GMT of when any part of this message (including status) was
         updated. */
      attributeMgr.registerAttribute("last_update_date", "last_update_date", "DateTimeAttribute");
      attributeMgr.requiredAttribute("last_update_date");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* is_reply - Inidcates whether or not this message is a reply to another message.   A
         Trade Portal user can create a message by pressing the 'Reply' button. 
         If this is done, this flag is set.  Yes = the message is a reply. */
      attributeMgr.registerAttribute("is_reply", "is_reply", "IndicatorAttribute");
      attributeMgr.requiredAttribute("is_reply");
      
      /* discrepancy_amount - If the message is a discrepancy notice, this field stores the amount of
         the discrepancy. */
      attributeMgr.registerAttribute("discrepancy_amount", "discrepancy_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* discrepancy_currency_code - If the message is a discrepancy notice, this field stores the currency 
         of the discrepancy. */
      attributeMgr.registerReferenceAttribute("discrepancy_currency_code", "discrepancy_currency_code", "CURRENCY_CODE");
      
      /* sequence_number - If the message is a discrepancy notice, this field stores the sequence number
         of the discrepancy. */
      attributeMgr.registerAttribute("sequence_number", "sequence_number");
      
      /* work_item_number - If the message is a discrepancy notice, this field stores the work item
         number of the discrepancy. */
      attributeMgr.registerAttribute("work_item_number", "work_item_number");
      
      /* response_transaction_oid - The transaction that was created in response to this discrepancy notice.
         Transactions are created as discrepancy responses instead of another mail
         message.
         
         This is an attribute and not a real jPylon association. */
      attributeMgr.registerAttribute("response_transaction_oid", "response_transaction_oid", "NumberAttribute");
      
      /* atp_notice_flag - Whether this mail message is an Approval To Pay Notice.  Y/N. */
      attributeMgr.registerAttribute("atp_notice_flag", "atp_notice_flag", "IndicatorAttribute");
      
      /* complete_instrument_id - The ID of the related instrument.  Used only if the mail message is received
         before its related instrument is created.  That could happen if the messages
         get out of order in the middleware.  When the UNV message later comes in
         and creates the instrument, proper a_related_instrument_oid will be set. */
      attributeMgr.registerAttribute("complete_instrument_id", "complete_instrument_id");
      
      /* presentation_date -  */
      attributeMgr.registerAttribute("presentation_date", "presentation_date", "DateTimeAttribute");
      
      /* discrepancy_text - discrepancy text or approval to pay text */
      attributeMgr.registerAttribute("discrepancy_text", "discrepancy_text");
      
      /* po_invoice_discrepancy_text - PO/Invoice Discrepancy Text */
      attributeMgr.registerAttribute("po_invoice_discrepancy_text", "po_invoice_discrepancy_text");
      
      /* assigned_to_corp_org_oid - The corporate organization to which a mail message is assigned.    This
         will never be empty.  When a mail message is assigned to a user, it is also
         still assigned to the corporate organization. */
      attributeMgr.registerAssociation("assigned_to_corp_org_oid", "a_assigned_to_corp_org_oid", "CorporateOrganization");
      
      /* related_instrument_oid - A mail message can be related to an instrument either by the user's action
         or by the middleware setting it.   This represents that the mail message
         contains information in regards to the related instrument. */
      attributeMgr.registerAssociation("related_instrument_oid", "a_related_instrument_oid", "Instrument");
      
      /* last_routed_by_user_oid - The user who last routed this message. */
      attributeMgr.registerAssociation("last_routed_by_user_oid", "a_last_routed_by_user_oid", "User");
      
      /* assigned_to_user_oid - The user to which a mail message is currently assigned. */
      attributeMgr.registerAssociation("assigned_to_user_oid", "a_assigned_to_user_oid", "User");
      
      /* last_entry_user_oid - The last user to enter data and save it for a mail message. */
      attributeMgr.registerAssociation("last_entry_user_oid", "a_last_entry_user_oid", "User");
      
    //Leelavathi - Rel800 CR618 - 20th Dec 2011 - Start
      /* payment_date */
      attributeMgr.registerAttribute("payment_date", "payment_date", "DateAttribute");
      //Leelavathi - Rel800 CR618 - 20th Dec 2011 - End

      // Vshah - PPX-220A - Rel8.0 - 04/09/2012 <Add/Register below attribute>
      // Below attribute will be populated by MAILOUT message coming from TPS. It will be used to tell the Portal how to process the mail message
      // and what it should instruct the TPS to do when uploading the Discrepancy Response ...
      /* presentation_status - */
      attributeMgr.registerAttribute("presentation_status", "presentation_status");
      
      //CR 913 start
      /*Date by which the customer needs to provide the funding. 
      It will be present only for payables funding mail messages */
      attributeMgr.registerAttribute("funding_amount", "funding_amount");
      attributeMgr.registerReferenceAttribute("funding_currency", "funding_currency", "CURRENCY_CODE");
      /*Amount that needs to be funded by the customer. 
       It will be present only for payables funding mail messages */
      attributeMgr.registerAttribute("funding_date", "funding_date", "DateAttribute");
      //CR 913 end

      //SSikhakolli - CR-944 Rel-9.1 - 07/29/2014 - Registered 'REFERENCE_NUMBER'
      //attributeMgr.registerAttribute("reference_number", "reference_number", "REFERENCE_NUMBER");
      attributeMgr.registerAttribute("reference_number", "reference_number");
      
      // CR 1029
      attributeMgr.registerAttribute("bank_instrument_id", "bank_instrument_id");
      
      // Nar CR-818 Rel 9.4.0.0 07/21/2015
      /* settlement_instr_flag - settlement instruction flag. */
      attributeMgr.registerReferenceAttribute("settlement_instr_flag", "settlement_instr_flag", "SETTLEMENT_INSTR_TYPE");
   }
   
 
   
 
 
   
}
