package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


public class MatchPayDedGlDtlsBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(MatchPayDedGlDtlsBean_Base.class);

	 
	/*
	 * Describes the mapping of an uploaded file to Invoice definiton.   
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */
	  
	  /* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	      /* match_pay_ded_gl_dtls_oid - MATCH_PAY_DED_GL_DTLS_OID - Unique identifier */
	      attributeMgr.registerAttribute("match_pay_ded_gl_dtls_oid", "match_pay_ded_gl_dtls_oid", "ObjectIDAttribute");
	      /*invoice_reference_id - Set to the Invoice Id*/
	      attributeMgr.registerAttribute("invoice_reference_id", "invoice_reference_id");
	      /* Transaction date - Date of the match processing */
	      attributeMgr.registerAttribute("transaction_date", "transaction_date", "DateAttribute");
	      /*  Transaction type - could be PAYMENT */
	      attributeMgr.registerAttribute("transaction_type", "transaction_type");
	      /*  This is the Applied Amount.  - Set to the Match Amount*/
	      attributeMgr.registerAttribute("applied_amount", "applied_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
	      /*  This is the General_Ledger_Code.  */
	      attributeMgr.registerAttribute("general_ledger_code", "general_ledger_code");	      
	      /*  This is the Discount_Code.  */
	      attributeMgr.registerAttribute("discount_code", "discount_code");	      
	      /* discount_description - Description of the discount. */
	      attributeMgr.registerAttribute("discount_description", "discount_description");
	      /* discount_comments - comments on the discount. */
	      attributeMgr.registerAttribute("discount_comments", "discount_comments");	      
	       /* system date time. */
	      attributeMgr.registerAttribute("system_date_time", "system_date_time","DateTimeAttribute");
	      /*Set to UOID of Payment/Remittance in the Portal*/
	      attributeMgr.registerAttribute("a_initiating_obj", "a_initiating_obj");	
	      /*Identifies the process creating this record - For portal it will be payremit*/
	      attributeMgr.registerAttribute("initiating_obj_cls", "initiating_obj_cls");	      
	      /*OTL_INVOICE _OID*/
	      attributeMgr.registerAttribute("otl_invoice_oid", "otl_invoice_oid");
	   }

	/*
	 * Register the components of the business object
	 */
	protected void registerComponents() throws RemoteException, AmsException {
		
		//registerOneToManyComponent("InvoiceSummaryGoodsList", "InvoiceSummaryGoodsList");
	}

	}
