package com.ams.tradeportal.busobj;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.EmailValidator;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.GenericList;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotificationRuleBean extends NotificationRuleBean_Base
{
	private static final Logger LOG = LoggerFactory.getLogger(NotificationRuleBean.class);
	public final List <String>instrumentCatagory = Arrays.asList(
			InstrumentType.AIR_WAYBILL,
			InstrumentType.APPROVAL_TO_PAY, 
			InstrumentType.BILLING,
			InstrumentType.CLEAN_BA, 
			InstrumentType.NEW_EXPORT_COL,
			InstrumentType.EXPORT_BANKER_ACCEPTANCE,
			InstrumentType.EXPORT_COL,
			InstrumentType.EXPORT_DEFERRED_PAYMENT,
			InstrumentType.EXPORT_DLC, 
			InstrumentType.INDEMNITY,
			InstrumentType.EXPORT_TRADE_ACCEPTANCE,
			InstrumentType.IMPORT_BANKER_ACCEPTANCE,
			InstrumentType.IMPORT_COL,
			InstrumentType.IMPORT_DEFERRED_PAYMENT,
			InstrumentType.IMPORT_DLC,
			InstrumentType.IMPORT_TRADE_ACCEPTANCE,
			InstrumentType.INCOMING_GUA,
			InstrumentType.INCOMING_SLC, 
			InstrumentType.FUNDS_XFER,
			InstrumentType.LOAN_RQST, 
			InstrumentType.GUARANTEE,
			InstrumentType.STANDBY_LC,
			InstrumentType.PAYABLES_MGMT,
			InstrumentType.DOMESTIC_PMT,
			InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT,
			InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT,
			InstrumentType.REFINANCE_BA,
			InstrumentType.REQUEST_ADVISE,
			InstrumentType.SHIP_GUAR,
			InstrumentType.SUPPLIER_PORTAL,
			InstrumentType.XFER_BET_ACCTS, 
			InstrumentType.MESSAGES,
			InstrumentType.SUPP_PORT_INST_TYPE,
			InstrumentType.HTWOH_INV_INST_TYPE);

	/**
	 * This method determines if this notification rule is assigned
	 * to any corporate customer(s) which have not specified an email address
	 *
	 * @return Vector -   list of corp org names which are assigned to this rule
	 * 			 and have not specified an email receiver address
	 */
	public Vector validateAssignedCorporateOrgs() 
	{
		GenericList corpOrgList = null;
		String parms[]  = new String[2];
		Vector corpOrgNameList = new Vector();

		try{
			// Set the parameters to be used in looking up the corp org in the database
			parms[0] = this.getAttribute("notification_rule_oid");
			parms[1] = TradePortalConstants.ACTIVE;

			// Create a generic list object and instantiate it based on the parameters passed in
			corpOrgList = (GenericList) EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");
			corpOrgList.prepareList("CorporateOrganization", "byEmailReceiver", parms);
			corpOrgList.getData();
			int corpOrgCount = corpOrgList.getObjectCount();
			LOG.debug("corpOrgCount: {}",corpOrgCount);
			for(int i=0; i< corpOrgCount; i++)
			{
				corpOrgList.scrollToObjectByIndex(i); 
				CorporateOrganization corpOrg = (CorporateOrganization)corpOrgList.getBusinessObject();
				corpOrgNameList.addElement(corpOrg.getAttribute("name"));
			}

		} catch (NullPointerException e) {
			LOG.error("Notification Rule - Null on query");
		} catch (Exception e) {
			LOG.error("Exception while validateAssignedCorporateOrgs(): ",e);
		} finally {
			// Remove the listview bean
			try {
				if (corpOrgList != null)
					corpOrgList.remove();
				LOG.debug("listview bean removed");
			} catch (RemoveException ex) {
			} catch (NoSuchObjectException e) {
			} catch (RemoteException e) {
			}
		}
		return corpOrgNameList;
	} 

	/**
	 * This method determines if this notification rule is assigned
	 * to a corporate customer.
	 *
	 * @return boolean - true = is being used, false = not used
	 */
	public boolean isUsed() 
	{
		boolean isUsed = true;
		GenericList corpOrgList = null;
		String parms[]  = new String[2];

		try{
			// Set the parameters to be used in looking up the corp org in the database
			parms[0] = this.getAttribute("notification_rule_oid");
			parms[1] = TradePortalConstants.ACTIVE;

			// Create a generic list object and instantiate it based on the parameters passed in
			corpOrgList = (GenericList) EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");
			corpOrgList.prepareList("CorporateOrganization", "byNotificationRule", parms);
			corpOrgList.getData();
			int corpOrgCount = corpOrgList.getObjectCount();
			LOG.debug("is used corpOrgCount: {}",corpOrgCount);
			// If any corporate orgs were found then this notification rule is being used
			if (corpOrgCount == 0) {
				isUsed = false;
			}

		} catch (NullPointerException e) {
			LOG.error("Notification Rule - Null on query");
		} catch (Exception e) {
			LOG.error("Exception while isUsed(): ",e);
			// error condition, so assume it is used
			isUsed = true;
		} finally {
			// Remove the listview bean
			try {
				if (corpOrgList != null)
					corpOrgList.remove();
				LOG.debug("listview bean removed");
			} catch (RemoveException ex) {
			} catch (NoSuchObjectException e) {
			} catch (RemoteException e) {
			}
		}
		return isUsed;
	}    

	/**
	 * Performs User validation:
	 *    unique name required.
	 * 
	 * @exception java.rmi.RemoteException 
	 * @exception com.amsinc.ecsg.frame.AmsException
	 */ 
	public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
	{
		

		// String partToValidate	  = null;

		//Part to validate is a local attribute, therefore the data is NOT stored in the db.
		// partToValidate = this.getAttribute("part_to_validate");

		// if (partToValidate.equals(TradePortalConstants.NOTIF_RULE_GENERAL))
		// {
		String name  = getAttribute("name");
		String where = " and p_owner_org_oid = '" + this.getAttribute("owner_org_oid") + "'";

		// now we need to check that name is unique
		if (!isUnique("name", name, where)) 
		{
			
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		} 
		//Srinivasu_D CR92b Rel9.5 01/02/2016 - Added for validating default/instrument level and transaction level
		String defaultApplyAllGrp       = this.getAttribute("default_apply_to_all_grp");
		String defaultClearAllGrp       = this.getAttribute("default_clear_to_all_grp");
		String defaultUpdateRecipientsOnly = this.getAttribute("default_update_recipients_only");
		String defaultUpdateEmailsOnly  = this.getAttribute("default_update_emails_only");
		String defaultNotifySetting     = this.getAttribute("default_notify_setting");
		String defaultEmailSetting  	= this.getAttribute("default_email_setting");
		String defaultUserOids			= this.getAttribute("default_notification_user_ids");
		String defaultEmailAddr			= this.getAttribute("default_addl_email_addr");
		boolean defaultValError		    = false;
		List <String>errorList = new ArrayList<String>();
		LOG.debug("defaultApplyAllGrp: {} ;defaultClearAllGrp: {} ; defaultUpdateRecipientsOnly: {} ;defaultUpdateEmailsOnly: {} ;defaultNotifySetting: {} ;defaultEmailSetting: {} ;defaultUserOids: {}; defaultEmailAddr: {}" 
			,new Object[]{defaultApplyAllGrp,defaultClearAllGrp,
				defaultUpdateRecipientsOnly,defaultUpdateEmailsOnly,
				defaultNotifySetting,defaultEmailSetting,defaultUserOids,defaultEmailAddr});  
		// validation on default sections.
		if(StringFunction.isNotBlank(defaultEmailAddr)){
			boolean val = performEmailValidation(defaultEmailAddr);
			
			if(val){			
			//errorList.add("add_email_val");
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_EMAIL,
					"");
			}
		}
		if(((StringFunction.isNotBlank(defaultNotifySetting) && StringFunction.isBlank(defaultEmailSetting)) ||
				(StringFunction.isBlank(defaultNotifySetting)) && StringFunction.isNotBlank(defaultEmailSetting))) {
			defaultValError = true;
		}
		
		if(defaultValError) {	
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.DEFAULT_NOTIFY_SETTINGS_MISSING);		

		}       

		if(StringFunction.isNotBlank(defaultNotifySetting) && StringFunction.isNotBlank(defaultEmailSetting)){
			if(StringFunction.isBlank(defaultApplyAllGrp) && StringFunction.isBlank(defaultClearAllGrp)) {	 
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.APPLY_CLEAR_BUTTON_NOTIFY_MISSING);

			}
		}
		
		if((TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(defaultEmailSetting) || TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defaultEmailSetting))
				&& (StringFunction.isBlank(defaultUserOids) && StringFunction.isBlank(defaultEmailAddr))) {	
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DEFAULT_USER_ID_MISSING);			
		}



		// }
		// else
		//{
		// Check if any rule criterion have a setting which requires the corporate customer to specify an email address
		
		// Retrieve this rule's criterion component list

		String tranTypeAction = null;
		String instTypeCategory = null;
		String instTypeDesc = null;
		String notfiSetting = null;
		String emailSetting = null;	
		String notifyUserIds = null;		
		String addiEmailIds = null;
		String notifyEmailFreq = null;
		//instrument level validations
		instTypeCategory = null;
		instTypeDesc =null;

		String sendEmailSetting = null;
		String sendNotifSetting = null;
		String applyToAllTran = null;
		String clearToAllTran = null;
		String updateRecipientsOnlyTran = null;
		String updateEmailsOnlyTran = null;
		String notificationUserIds = null;
		String addtionalEmailId = null;
		String instrumentType = null;
		for(int k=0;k<=34;k++){

			sendEmailSetting		= 		this.getAttribute("send_email_setting"+k);
			sendNotifSetting		= 		this.getAttribute("send_notif_setting"+k);
			applyToAllTran			=	 	this.getAttribute("apply_to_all_tran"+k);
			clearToAllTran			=	 	this.getAttribute("clear_to_all_tran"+k);
			updateRecipientsOnlyTran=	 	this.getAttribute("update_recipients_only_tran"+k);
			updateEmailsOnlyTran	=	 	this.getAttribute("update_emails_only_tran"+k);
			notificationUserIds		=		this.getAttribute("notification_user_ids"+k);
			addtionalEmailId		=		this.getAttribute("additional_email_addr"+k);
			instrumentType       	=     	this.getAttribute("instrument_type"+k);
		
			LOG.debug("sendEmailSetting: {} ; sendNotifSetting: {} ;applyToAllTran: {} ; clearToAllTran: {} ; updateRecipientsOnlyTran: {} ; updateEmailsOnlyTran: {} ; notificationUserIds: {} ; addtionalEmailId: {} ;instrumentType: {}",
			new Object[]{sendEmailSetting,sendNotifSetting,applyToAllTran,clearToAllTran,updateRecipientsOnlyTran,updateEmailsOnlyTran,notificationUserIds,addtionalEmailId,instrumentType});
			
			if(StringFunction.isNotBlank(instrumentType)){
				instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr("NOTIFICATION_INST_TYPE",
											instrumentType,getResourceManager().getResourceLocale());
				} 			
		
			if(StringFunction.isNotBlank(addtionalEmailId)){
				if(addtionalEmailId.length()>300){
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ADDITIONAL_EMAIL_LENGTH_EXCEEDS,
							instTypeDesc);
					break;
				}else{
					boolean val = performEmailValidation(addtionalEmailId);
					
					if(val){
					errorList.add(instrumentType+TradePortalConstants.INVALID_EMAIL_FORMAT);
			this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INVALID_EMAIL_FORMAT,
					instTypeDesc);
				//break;
				}
			  }
			}
			if(!defaultValError) {
				if(StringFunction.isNotBlank(applyToAllTran) && StringFunction.isNotBlank(clearToAllTran) && 
						StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)){	
					continue;
				}
				
				if((TradePortalConstants.MSG_INST_TYPE.equals(instrumentType) || TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instrumentType) || 
							TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instrumentType)) ) {
					if(StringFunction.isBlank(applyToAllTran)){ 
						if(StringFunction.isNotBlank(sendEmailSetting)){
							errorList.add(instrumentType+TradePortalConstants.APPLY_CLEAR_BUTTON_MISSING);
							this.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.APPLY_CLEAR_BUTTON_MISSING,
									instTypeDesc);
						}else if(StringFunction.isBlank(sendEmailSetting))
						{
							if(StringFunction.isNotBlank(notificationUserIds) || StringFunction.isNotBlank(addtionalEmailId)) {
								if(StringFunction.isBlank(updateEmailsOnlyTran) && StringFunction.isBlank(updateRecipientsOnlyTran)){
									errorList.add(instrumentType+TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA);
									
									this.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA,
											instTypeDesc);
								}
							}
						}

					}else if(StringFunction.isNotBlank(applyToAllTran)) {
						//email id validations start
						//if(!TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instrumentType)){
						if(StringFunction.isNotBlank(sendEmailSetting) && (TradePortalConstants.NOTIF_RULE_ALWAYS.equals(sendEmailSetting) || TradePortalConstants.INDICATOR_YES.equals(sendEmailSetting))) {
							if(StringFunction.isBlank(notificationUserIds) && StringFunction.isBlank(addtionalEmailId)) {
								errorList.add(instrumentType+TradePortalConstants.USER_ID_MISSING);
								//instTypeCategory = instrumentCatagory.get(k-1);
								this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.USER_ID_MISSING,instTypeDesc);	
							}
						  }
						//}
						if(StringFunction.isBlank(sendEmailSetting))
						{
							if(StringFunction.isNotBlank(notificationUserIds) || StringFunction.isNotBlank(addtionalEmailId)) {
								errorList.add(instrumentType+TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA);
								
								this.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA,
										instTypeDesc);
								//break;
							}
						}
						//email id validation end
					}

				}else {
					if(StringFunction.isNotBlank(applyToAllTran)){
						
						if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting))
						{
							if(StringFunction.isNotBlank(notificationUserIds) || StringFunction.isNotBlank(addtionalEmailId)) {
								errorList.add(instrumentType+TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA);
								
								this.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA,
										instTypeDesc);
								//break;
							}
						}else if(StringFunction.isBlank(sendEmailSetting) || StringFunction.isBlank(sendNotifSetting)){

							errorList.add(instrumentType+TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA);
							
							this.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA,
									instTypeDesc);
							//break;
						}	
					}					
					else if(StringFunction.isBlank(applyToAllTran) && StringFunction.isBlank(clearToAllTran)){

						if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting))
						{
							if(StringFunction.isNotBlank(notificationUserIds) || StringFunction.isNotBlank(addtionalEmailId)) {
								if(StringFunction.isBlank(updateEmailsOnlyTran) && StringFunction.isBlank(updateRecipientsOnlyTran)){
									errorList.add(instrumentType+TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA);
									
									this.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.NOTIF_RULE_INCOMPLETE_DATA,
											instTypeDesc);
									//break;
								}
							}
						} else
						if(StringFunction.isNotBlank(sendEmailSetting) || StringFunction.isNotBlank(sendNotifSetting)){	
							errorList.add(instrumentType+TradePortalConstants.INSTRUMENT_NOTIFY_MISSING);
							
							this.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INSTRUMENT_NOTIFY_MISSING,
									instTypeDesc);
						}
					}

					//Email User Ids validation start
					if(StringFunction.isNotBlank(sendEmailSetting) && 
							(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(sendEmailSetting) || TradePortalConstants.NOTIF_RULE_ALWAYS.equals(sendEmailSetting))) {
						if(StringFunction.isBlank(notificationUserIds) && StringFunction.isBlank(addtionalEmailId)) {
							System.out.println("yes error");
							errorList.add(instrumentType+TradePortalConstants.USER_ID_MISSING);
							this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.USER_ID_MISSING,instTypeDesc);	
							//break;
						}

					}
					
					//Email User Ids validation end
				}
			}
		}
		LOG.debug("errorList: {}",errorList);
		//Pavani-IR T36000050332 Rel9.6 - Fetching criterion values via sql instead of ejbs, to prevent memory/cachefullexception
		List criterionList = new ArrayList();
		String select = "select criterion_oid, transaction_type_or_action, instrument_type_or_category, send_notif_setting,"
				+ " send_email_setting, additional_email_addr, notify_email_freq, notification_user_ids, clear_tran_ind "
				+ " from NOTIFICATION_RULE_CRITERION where p_notify_rule_oid=? ";
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(select, false, new Object[]{this.getAttribute("notification_rule_oid")});
		if(results != null){
		   criterionList = results.getFragmentsList("/ResultSetRecord");
		}

		int criterionCount = criterionList.size();
		String criterionOid = null;
		String spEmailFreq = "";
		String pinEmailFreq = "";
		String rinEmailFreq = "";
		String pcnEmailFreq = "";
		String spEmailInd = "";
		String pinEmailInd = "";
		String rinEmailInd = "";
		String pcnEmailInd = "";
		String clearInd = "";
		notifyUserIds = "";
		NotificationRuleCriterion ruleCriterion = null;
		for (int criterionIndex = 0; criterionIndex < criterionCount; criterionIndex++) 
		{

			criterionOid = results.getAttribute("/ResultSetRecord("+criterionIndex+")/CRITERION_OID");
			tranTypeAction = results.getAttribute("/ResultSetRecord("+criterionIndex+")/TRANSACTION_TYPE_OR_ACTION");
			instTypeCategory = results.getAttribute("/ResultSetRecord("+criterionIndex+")/INSTRUMENT_TYPE_OR_CATEGORY");
			notfiSetting = results.getAttribute("/ResultSetRecord("+criterionIndex+")/SEND_NOTIF_SETTING");
			emailSetting =	results.getAttribute("/ResultSetRecord("+criterionIndex+")/SEND_EMAIL_SETTING");
			notifyUserIds = results.getAttribute("/ResultSetRecord("+criterionIndex+")/NOTIFICATION_USER_IDS");
			addiEmailIds = results.getAttribute("/ResultSetRecord("+criterionIndex+")/ADDITIONAL_EMAIL_ADDR");
			notifyEmailFreq = results.getAttribute("/ResultSetRecord("+criterionIndex+")/NOTIFY_EMAIL_FREQ");
			clearInd = results.getAttribute("/ResultSetRecord("+criterionIndex+")/CLEAR_TRAN_IND");
			LOG.debug("clearInd: {} ;criterionOid: {} ;tranTypeAction: {} ;instTypeCategory: {} ;notfiSetting: {} ;emailSetting: {} ;notifyUserIds: {} ;addiEmailIds: {}",
			new Object[]{clearInd,criterionOid,tranTypeAction,instTypeCategory,notfiSetting,emailSetting,notifyUserIds,addiEmailIds});
			
			if(StringFunction.isBlank(tranTypeAction) && StringFunction.isBlank(instTypeCategory) && 
						StringFunction.isBlank(notfiSetting) && StringFunction.isBlank(emailSetting)){	
					continue;
				}

			if(StringFunction.isNotBlank(instTypeCategory)){
				instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr("NOTIFICATION_INST_TYPE",
											instTypeCategory,getResourceManager().getResourceLocale());
				} 
			if(StringFunction.isNotBlank(addiEmailIds)){
				if(addiEmailIds.length()>300){
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ADDITIONAL_EMAIL_LENGTH_EXCEEDS,
							instTypeDesc);
					break;
				}else{
					boolean val = performEmailValidation(addiEmailIds);
					
					if(val && !errorList.contains(instTypeCategory+TradePortalConstants.INVALID_EMAIL_FORMAT)){
					errorList.add(instTypeCategory+TradePortalConstants.INVALID_EMAIL_FORMAT);
					this.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVALID_EMAIL_FORMAT,
							instTypeDesc);
						//break;
					}
				}
			}		
			

			//if( !errorList.contains(instTypeCategory)) {

				if((TradePortalConstants.MSG_INST_TYPE.equals(instTypeCategory) || TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instTypeCategory) || 
						TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instTypeCategory)) ) {
					
					if(StringFunction.isBlank(emailSetting) && !errorList.contains(instTypeCategory+TradePortalConstants.SEND_MAIL_MISSING)){	
						
						errorList.add(instTypeCategory+TradePortalConstants.SEND_MAIL_MISSING);
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_MISSING,
								instTypeDesc);
						
						//break;
					}
					
					if((TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instTypeCategory) ||
							TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instTypeCategory))
							&& TradePortalConstants.INDICATOR_YES.equals(emailSetting)){
						
						if(StringFunction.isBlank(notifyEmailFreq) && !errorList.contains(instTypeCategory+TradePortalConstants.EMAIL_INV_CN_FREQUENCY_MISSING)){	
							errorList.add(instTypeCategory+TradePortalConstants.EMAIL_INV_CN_FREQUENCY_MISSING);
							this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.EMAIL_INV_CN_FREQUENCY_MISSING,
															instTypeDesc);
							//break;
						}else{
							if(StringFunction.isBlank(notifyEmailFreq)){
								notifyEmailFreq="0";
							}				
							 if ((Integer.parseInt(notifyEmailFreq) == 0)||(Integer.parseInt(notifyEmailFreq) % 10 != 0) 
									 	&& !errorList.contains(instTypeCategory+TradePortalConstants.INVALID_INV_CN_EMAIL_FREQUENCY)){
								 errorList.add(instTypeCategory+TradePortalConstants.INVALID_INV_CN_EMAIL_FREQUENCY);
								 	 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_INV_CN_EMAIL_FREQUENCY,
															 instTypeDesc);
									// break;
							 }
						}
						
					}
					if(TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instTypeCategory)){
						spEmailFreq = notifyEmailFreq;
						spEmailInd = emailSetting;
					}
					if(TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instTypeCategory)){
						if(TradePortalConstants.PIN_TRANS_TYPE.equals(tranTypeAction)){
							pinEmailFreq = notifyEmailFreq;
							pinEmailInd = emailSetting;
						}else if(TradePortalConstants.RIN_TRANS_TYPE.equals(tranTypeAction)){
							rinEmailFreq = notifyEmailFreq;
							rinEmailInd = emailSetting;
						}else if(TradePortalConstants.PCN_TRANS_TYPE.equals(tranTypeAction)){
							pcnEmailFreq = notifyEmailFreq;
							pcnEmailInd = emailSetting;
						}
					}
				}
				else {
					if((StringFunction.isBlank(notfiSetting) || StringFunction.isBlank(emailSetting)) && !errorList.contains(instTypeCategory+TradePortalConstants.SEND_MAIL_NOTIFICATION_MISSING)) {        
						
						errorList.add(instTypeCategory+TradePortalConstants.SEND_MAIL_NOTIFICATION_MISSING);
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_NOTIFICATION_MISSING,
								instTypeDesc);
						//break;	        	 
					}

				}

				//Email User Ids validation start
				if(TradePortalConstants.INDICATOR_NO.equals(clearInd) && StringFunction.isNotBlank(emailSetting) && 
						(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(emailSetting) || TradePortalConstants.NOTIF_RULE_ALWAYS.equals(emailSetting) || 
														TradePortalConstants.INDICATOR_YES.equals(emailSetting))) {
					if((StringFunction.isBlank(notifyUserIds) && StringFunction.isBlank(addiEmailIds)) && !errorList.contains(instTypeCategory+TradePortalConstants.USER_ID_MISSING)) {		
						
						errorList.add(instTypeCategory+TradePortalConstants.USER_ID_MISSING);
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.USER_ID_MISSING,instTypeDesc);	
						//break;
					}

				}				
				//Email User Ids validation end
				
				
			//}   
		}	 
		
		validateAndSaveEmailNotifications(spEmailInd, pinEmailInd, rinEmailInd, pcnEmailInd, spEmailFreq, pinEmailFreq, rinEmailFreq, pcnEmailFreq ); 

		//Srinivasu_D CR92b Rel9.5 01/02/2016 - End
		

		//String instrumentCategory = criterionRule.getAttribute("instr_notify_category");

		// Only check criterion rules which have a setting = email and are in the instrument
		// category that we're validating

		//     }

		/*
	 for (int criterionIndex = 0; criterionIndex < criterionCount; criterionIndex++) 
         {
	    criterionList.scrollToObjectByIndex(criterionIndex);
            Long criterionOid = new Long(criterionList.getListAttribute("criterion_oid"));

	    NotificationRuleCriterion criterionRule = (NotificationRuleCriterion)criterionList.getComponentObject(criterionOid.longValue());

            String criterionType = criterionRule.getAttribute("criterion_type");
            String criterionSetting = criterionRule.getAttribute("setting");
            //String instrumentCategory = criterionRule.getAttribute("instr_notify_category");

            // Only check criterion rules which have a setting = email and are in the instrument
            // category that we're validating
	    if (criterionType.equals(TradePortalConstants.NOTIF_RULE_EMAIL))
            {
	       if (criterionSetting.equals(TradePortalConstants.NOTIF_RULE_ALWAYS) ||
		      criterionSetting.equals(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY))
               {
                  isEmailRequired = true;
                  break;
               }
            }
         }
		 */
		/*
	 if (isEmailRequired) 
         {
	    Vector corpOrgNameList = new Vector();
            corpOrgNameList = validateAssignedCorporateOrgs();

	    for (int i=0; i< corpOrgNameList.size(); i++)
            {
               String corpOrgName = (String)corpOrgNameList.elementAt(i);
               this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                                 TradePortalConstants.EMAIL_ADDRESS_REQUIRED, corpOrgName);
            }
         }
		 */
		//   }
	}

	/**
	 * Cannot delete notification rule if its assigned to a corporate customer.
	 *
	 * @exception java.rmi.RemoteException The exception description.
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	/*
   public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
   {

      String name = getAttribute("name");

      // Check if this profile is being used anywhere.
      if (isUsed())
      {
         this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                  TradePortalConstants.NOTIFICATION_RULE_ASSIGNED, name);
      }	
   }
	*/

	protected boolean performEmailValidation(String emailList) throws RemoteException, AmsException {
		if (StringFunction.isBlank(emailList)) return false;

	    if (!EmailValidator.validateEmailList(emailList))	{
	    	return true;
	    }
		return false;
	}
	
	/* MEer Rel 8.3 IR-T36000022048 validations on my Organization profile page */
	protected void validateAndSaveEmailNotifications(String spEmailInd, String pinEmailInd, String rinEmailInd, String pcnEmailInd, String spEmailFreq, 
			String pinEmailFreq, String rinEmailFreq, String pcnEmailFreq) throws AmsException {
		LOG.info("Inside preSave of NotificationRuleBean");
		try{
				String notifRuleOid = this.getAttribute("notification_rule_oid");
				
				SPEmailNotifications  spEmail = null;
				String spNextEmailDue ="";
				String corporateOrgOid = this.getAttribute("owner_org_oid");
				
					try {
						if(TradePortalConstants.INDICATOR_YES.equals(spEmailInd) && StringFunction.isNotBlank(spEmailFreq) && (Integer.parseInt(spEmailFreq) % 10 == 0)){
							spNextEmailDue = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(spEmailFreq));
							LOG.info("[nextEmailDue: ] {}", spNextEmailDue);
						}
					} catch (NumberFormatException | ParseException e) {
							LOG.error("Exception at validateAndSaveEmailNotifications(): ",e);
					} 
					
					if (StringFunction.isNotBlank(this.getAttribute("c_SPEmailNotifications")))
					    spEmail = (SPEmailNotifications)this.getComponentHandle("SPEmailNotifications");
					 
			
					if(TradePortalConstants.INDICATOR_YES.equals(spEmailInd)){
						if (StringFunction.isBlank(this.getAttribute("c_SPEmailNotifications"))){
							this.newComponent("SPEmailNotifications");
							spEmail = (SPEmailNotifications)this.getComponentHandle("SPEmailNotifications");
			
						}	
						spEmail.setAttribute("p_corp_organization_oid", corporateOrgOid);
						spEmail.setAttribute("next_sp_email_due", spNextEmailDue);
						spEmail.setAttribute("p_notification_rule_oid", notifRuleOid);
					}else{
			
						if (StringFunction.isNotBlank(this.getAttribute("c_SPEmailNotifications"))){
							this.deleteComponent("SPEmailNotifications");   
						}
					}
					
					//Srinivasu_D CR#1006 Rel9.3 04/28/2015 - creating c_InvoiceCRNoteEmailNotification object
					InvoiceCRNoteEmailNotification  cnEmail = null;
					String nextPINEmailDue ="";
					String nextRINmailDue ="";
					String nextPCNmailDue ="";
					
					try {
						if(TradePortalConstants.INDICATOR_YES.equals(pinEmailInd) && StringFunction.isNotBlank(pinEmailFreq) && (Integer.parseInt(pinEmailFreq) % 10 == 0)){
							nextPINEmailDue = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(pinEmailFreq));
							LOG.debug("[nextPINEmailDue: ] {}", nextPINEmailDue);
						}
						if(TradePortalConstants.INDICATOR_YES.equals(pcnEmailInd) && StringFunction.isNotBlank(pcnEmailFreq) && (Integer.parseInt(pcnEmailFreq) % 10 == 0)){
							nextPCNmailDue = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(pcnEmailFreq));
							LOG.debug("[nextPCNmailDue: ] {}", nextPCNmailDue);
						}
						if(TradePortalConstants.INDICATOR_YES.equals(rinEmailInd) && StringFunction.isNotBlank(rinEmailFreq) && (Integer.parseInt(rinEmailFreq) % 10 == 0)){
							nextRINmailDue = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(rinEmailFreq));
							LOG.debug("[nextRINmailDue: ] {}", nextRINmailDue);
						}
						
					} catch (NumberFormatException | ParseException e) {
							LOG.error("Exception at validateAndSaveEmailNotifications(): ",e);
					} 
					if (StringFunction.isNotBlank(this.getAttribute("c_InvoiceCRNoteEmailNotification")))
						cnEmail = (InvoiceCRNoteEmailNotification)this.getComponentHandle("InvoiceCRNoteEmailNotification");
					 
			
					if(TradePortalConstants.INDICATOR_YES.equals(pinEmailInd) || 
								TradePortalConstants.INDICATOR_YES.equals(pcnEmailInd) || 
									TradePortalConstants.INDICATOR_YES.equals(rinEmailInd) ){
						if (StringFunction.isBlank(this.getAttribute("c_InvoiceCRNoteEmailNotification"))){
							this.newComponent("InvoiceCRNoteEmailNotification");
							cnEmail = (InvoiceCRNoteEmailNotification)this.getComponentHandle("InvoiceCRNoteEmailNotification");
			
						}	
						cnEmail.setAttribute("p_corp_organization_oid", corporateOrgOid);
						cnEmail.setAttribute("p_notification_rule_oid", notifRuleOid);
						cnEmail.setAttribute("next_pin_email_due", nextPINEmailDue);
						cnEmail.setAttribute("next_pcn_email_due", nextPCNmailDue);
						cnEmail.setAttribute("next_rin_email_due", nextRINmailDue);
					}else{
			
						if (StringFunction.isNotBlank(this.getAttribute("c_InvoiceCRNoteEmailNotification"))){
							this.deleteComponent("InvoiceCRNoteEmailNotification");   
						}
					}
		}catch (RemoteException e) {
			LOG.error("RemoteException at validateAndSaveEmailNotifications(): ",e);
			throw new AmsException(e.getMessage());
		}
	}	
	
	/**
	* When a transaction comes in to the Portal for this Corporate Customer this method
	* will determine the email/notification setting for this Customer's
	* notification rule using the transaction type/action and the instrument type/category.
	* Changed method name as per Rel9.5 CR978
	* @return      String setting - notification rule's criterion setting
	* @exception   java.rmi.RemoteException
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
public String getNotificationRuleSettingOrFreq(
	String notifRuleOid,
	String instrumentTypeOrCategory,
	String transactionTypeOrAction,
	String criterionType)
	throws RemoteException, AmsException {
	String setting = null;

	try {

		// Set the parameters to be used in looking up the criterion in the database
		String parms[] = new String[4];
		parms[0] = notifRuleOid;//Rel9.5 CR-927B
		parms[1] = instrumentTypeOrCategory;
		parms[2] = transactionTypeOrAction;
		
		GenericList criterionList =
			(GenericList) EJBObjectFactory.createServerEJB(
				getSessionContext(),
				"GenericList");
		criterionList.prepareList(
			"NotificationRuleCriterion",
			"forCorpOrgTransaction",
			parms);
		criterionList.getData();

		if (criterionList.getObjectCount() == 1) {
			//Rel9.5 CR-927B Get the settings of notification/email
			if(TradePortalConstants.NOTIF_RULE_NOTIFICATION.equals(criterionType)){
				setting = criterionList.getAttribute("send_notif_setting");
			}else if(TradePortalConstants.NOTIF_RULE_EMAIL.equals(criterionType)){
				setting = criterionList.getAttribute("send_email_setting");
			}else if(TradePortalConstants.NOTIF_RULE_EMAIL_FREQ.equals(criterionType)){
				setting = criterionList.getAttribute("notify_email_freq");
			}
		}
		LOG.info("Inside NotificationRuleBean::getNotificationRuleSettingOrFreq()..instrType= {} and transType= {}" ,instrumentTypeOrCategory,transactionTypeOrAction );
		LOG.info("Inside NotificationRuleBean::getNotificationRuleSettingOrFreq()..criterionType= {} and setting= {}",criterionType,setting);
	} catch (Exception e) {
		LOG.error("Exception at getNotificationRuleSettingOrFreq(): ",e);
	}
	return setting;
}


}
