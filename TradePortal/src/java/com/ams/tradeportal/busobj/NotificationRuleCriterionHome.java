
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface NotificationRuleCriterionHome extends EJBHome
{
   public NotificationRuleCriterion create()
      throws RemoteException, CreateException, AmsException;

   public NotificationRuleCriterion create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
