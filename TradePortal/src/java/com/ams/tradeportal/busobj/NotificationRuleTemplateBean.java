package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import com.ams.tradeportal.common.*;

/*
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

/**
 * This EJB class saves notification rule templates.
 * @author srinivasu.doddapanen
 * @version 0.2
 */
public class NotificationRuleTemplateBean extends NotificationRuleTemplateBean_Base
{
	private static final Logger LOG = LoggerFactory.getLogger(NotificationRuleTemplateBean.class);

	public final String[] instrumentCatagory = {
			InstrumentType.AIR_WAYBILL,
			InstrumentType.APPROVAL_TO_PAY, 
			InstrumentType.BILLING,
			InstrumentType.CLEAN_BA, 
			InstrumentType.NEW_EXPORT_COL,
			InstrumentType.EXPORT_BANKER_ACCEPTANCE,
			InstrumentType.EXPORT_COL,
			InstrumentType.EXPORT_DEFERRED_PAYMENT,
			InstrumentType.EXPORT_DLC, 
			InstrumentType.INDEMNITY,
			InstrumentType.EXPORT_TRADE_ACCEPTANCE,
			InstrumentType.IMPORT_BANKER_ACCEPTANCE,
			InstrumentType.IMPORT_COL,
			InstrumentType.IMPORT_DEFERRED_PAYMENT,
			InstrumentType.IMPORT_DLC,
			InstrumentType.IMPORT_TRADE_ACCEPTANCE,
			InstrumentType.INCOMING_GUA,
			InstrumentType.INCOMING_SLC, 
			InstrumentType.FUNDS_XFER,
			InstrumentType.LOAN_RQST, 
			InstrumentType.GUARANTEE,
			InstrumentType.STANDBY_LC,
			InstrumentType.PAYABLES_MGMT,
			InstrumentType.DOMESTIC_PMT,
			InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT,
			InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT,
			InstrumentType.REFINANCE_BA,
			InstrumentType.REQUEST_ADVISE,
			InstrumentType.SHIP_GUAR,
			InstrumentType.SUPPLIER_PORTAL,
			InstrumentType.XFER_BET_ACCTS, 
			InstrumentType.MESSAGES,
			InstrumentType.SUPP_PORT_INST_TYPE,
			InstrumentType.HTWOH_INV_INST_TYPE };


	/**
	 * Performs User validation:
	 *    unique name required.
	 * 
	 * @exception java.rmi.RemoteException 
	 * @exception com.amsinc.ecsg.frame.AmsException
	 */ 
	public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
	{

		String name  = getAttribute("name");
		String where = " and p_owner_org_oid = '" + this.getAttribute("owner_org_oid") + "'";

		// now we need to check that name is unique
		if (!isUnique("name", name, where)) 
		{

			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		} 
		String defaultApplyAllGrp = this.getAttribute("default_apply_to_all_grp");
		String defaultClearAllGrp = this.getAttribute("default_clear_to_all_grp");
		String  defaultNotifySetting = this.getAttribute("default_notify_setting");
		String  defaultEmailSetting = this.getAttribute("default_email_setting");
		boolean defaultValError = false;
		String supplierReadOnly = this.getAttribute("supplierReadOnly"); 
		// validation on default sections.

		if(((StringFunction.isNotBlank(defaultNotifySetting) && StringFunction.isBlank(defaultEmailSetting)) ||
				(StringFunction.isBlank(defaultNotifySetting)) && StringFunction.isNotBlank(defaultEmailSetting))) {
			defaultValError = true;
		}

		if(defaultValError) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DEFAULT_NOTIFY_SETTINGS_MISSING);		

		}       

		if(StringFunction.isNotBlank(defaultNotifySetting) && StringFunction.isNotBlank(defaultEmailSetting)){
			if(StringFunction.isBlank(defaultApplyAllGrp) && StringFunction.isBlank(defaultClearAllGrp)) {
				this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.APPLY_CLEAR_BUTTON_NOTIFY_MISSING);					 
			}
		}
		
		// Retrieve this rule's criterion component list

		String tranTypeAction = null;
		String instTypeCategory = null;
		String instTypeDesc = null;
		String notfiSetting = null;
		String emailSetting = null;	
		String notifyEmailFreq = null;
		//instrument level validations
		instTypeCategory = null;
		instTypeDesc =null;
		List <String>errorList = new ArrayList<String>();
		String sendEmailSetting = null;
		String sendNotifSetting = null;
		String applyToAllTran = null;
		String clearToAllTran = null;
		
		if(!defaultValError) {

		for(int k=1;k<=34;k++){

			sendEmailSetting   = 	this.getAttribute("send_email_setting"+k);
			sendNotifSetting   = 	this.getAttribute("send_notif_setting"+k);
			applyToAllTran 	= 	this.getAttribute("apply_to_all_tran"+k);
			clearToAllTran 	= 	this.getAttribute("clear_to_all_tran"+k);

			LOG.debug("K: {} ; supplierReadOnly: {} ; sendEmailSetting: {} ; sendNotifSetting: {} ; applyToAllTran: {} ;clearToAllTran: {} ; ",
			new Object[]{k,supplierReadOnly,sendEmailSetting,sendNotifSetting,applyToAllTran,clearToAllTran});
			
			if(StringFunction.isNotBlank(applyToAllTran) && StringFunction.isNotBlank(clearToAllTran) && 
					StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)){	
				continue;
			}

			if(k>31) {
				boolean showError = false;
				if(StringFunction.isBlank(applyToAllTran)){ 
					if(StringFunction.isNotBlank(sendEmailSetting)){
						instTypeCategory = instrumentCatagory[k-1];	
						if(StringFunction.isNotBlank(instTypeCategory)){
							instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,
									instTypeCategory,getResourceManager().getResourceLocale());
						showError = true;
						}  
						if(k==33 && TradePortalConstants.INDICATOR_YES.equals(supplierReadOnly)){
						showError = false;
						}
						if(showError){
						errorList.add(instTypeCategory);
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.APPLY_CLEAR_BUTTON_MISSING,
								instTypeDesc);
						break;
						}
					}		

				}/* //Below condition is not required.
				else if(StringFunction.isNotBlank(applyToAllTran)) {
					if(StringFunction.isBlank(sendEmailSetting)) {
						instTypeCategory = instrumentCatagory[k-1];	
						if(StringFunction.isNotBlank(instTypeCategory)){
							instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,
									instTypeCategory,getResourceManager().getResourceLocale());
						}  
						errorList.add(instTypeCategory);
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_MISSING,
								instTypeDesc);
						break;
					}					
				}
				*/
			}else if(k<32) {
				if(StringFunction.isNotBlank(applyToAllTran)){

					if(StringFunction.isBlank(sendEmailSetting) || StringFunction.isBlank(sendNotifSetting)){

						instTypeCategory = instrumentCatagory[k-1];	
						instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
						
						errorList.add(instTypeCategory);
						
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_NOTIFICATION_MISSING,
								instTypeDesc);
						break;
					}	
				}					
				else if(StringFunction.isBlank(applyToAllTran) || StringFunction.isBlank(clearToAllTran)){

					if(StringFunction.isNotBlank(sendEmailSetting) || StringFunction.isNotBlank(sendNotifSetting)){	
						instTypeCategory = instrumentCatagory[k-1];
						
						instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
						errorList.add(instTypeCategory);
						
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INSTRUMENT_NOTIFY_MISSING,
								instTypeDesc);
						break;
					}
				}
			}
		}
		LOG.debug("errorList: {}",errorList);
		ComponentList criterionList = (ComponentList)this.getComponentHandle("NotificationRuleCriterionList");
		int criterionCount = criterionList.getObjectCount();
		String criterionOid = null;

		for (int criterionIndex = 0; criterionIndex < criterionCount; criterionIndex++) 
		{
			criterionList.scrollToObjectByIndex(criterionIndex);
			criterionOid = criterionList.getBusinessObject().getAttribute("criterion_oid");
			tranTypeAction = criterionList.getBusinessObject().getAttribute("transaction_type_or_action");
			instTypeCategory = criterionList.getBusinessObject().getAttribute("instrument_type_or_category");
			notfiSetting = criterionList.getBusinessObject().getAttribute("send_notif_setting");
			emailSetting =	criterionList.getBusinessObject().getAttribute("send_email_setting");
			notifyEmailFreq = criterionList.getBusinessObject().getAttribute("notify_email_freq");
			
			LOG.debug("criterionOid: {} ; tranTypeAction: {} ; instTypeCategory: {} ; notfiSetting: {} ; emailSetting: {} ",
						new Object[]{criterionOid,tranTypeAction,instTypeCategory,notfiSetting,emailSetting});
			
			if(!errorList.contains(instTypeCategory)) {

				if(TradePortalConstants.MSG_INST_TYPE.equals(instTypeCategory) || TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instTypeCategory) || 
						TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instTypeCategory)) {
					if(StringFunction.isBlank(emailSetting)){
						
						instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_MISSING,
								instTypeDesc);
						break;
					}
					if((TradePortalConstants.SUPP_PORT_INST_TYPE.equals(instTypeCategory) ||
							TradePortalConstants.HTWOH_INV_INST_TYPE.equals(instTypeCategory))
							&& TradePortalConstants.INDICATOR_YES.equals(emailSetting)){
						
						if(StringFunction.isBlank(notifyEmailFreq)){
							instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
							this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.EMAIL_INV_CN_FREQUENCY_MISSING,
															instTypeDesc);
							break;
						}else{
							if(StringFunction.isBlank(notifyEmailFreq)){
								notifyEmailFreq="0";
							}				
							 if ((Integer.parseInt(notifyEmailFreq) == 0)||(Integer.parseInt(notifyEmailFreq) % 10 != 0)){
								 instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
									 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_INV_CN_EMAIL_FREQUENCY,
															 instTypeDesc);
									 break;
							 }
						}
						
					}
					
					
				}
				else {
					if("0".equals(criterionOid) && (StringFunction.isBlank(notfiSetting) || StringFunction.isBlank(emailSetting))) {        

						instTypeDesc =	ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.NOTIFICATION_INST_TYPE,instTypeCategory,getResourceManager().getResourceLocale());
						
						this.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.SEND_MAIL_NOTIFICATION_MISSING,
								instTypeDesc);
						break;	        	 
					}
				}
			}   
		}
	 }
	}

	protected void preSave() throws AmsException {
		super.preSave();
	}
}
