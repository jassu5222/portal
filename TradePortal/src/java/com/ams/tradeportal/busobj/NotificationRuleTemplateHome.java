
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Defines a rule which determines if a Corporate Customer will receive notification
 * messages and emails after a transaction has been authorized.
 * 
 * A Notification Rule can be established by Client Banks and Bank Group level
 * users.  The Notification Rule can then be assigned to a specific Corporate
 * Customer.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface NotificationRuleTemplateHome extends EJBHome
{
   public NotificationRuleTemplate create()
      throws RemoteException, CreateException, AmsException;

   public NotificationRuleTemplate create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
