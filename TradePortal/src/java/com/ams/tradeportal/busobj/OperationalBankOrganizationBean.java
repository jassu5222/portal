
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class OperationalBankOrganizationBean extends OperationalBankOrganizationBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(OperationalBankOrganizationBean.class);

  /**
   *
   *
   * @param  instrumentType
   * @return String
   */
   public String getPrefix(String instrumentType) throws RemoteException
   {
	  String prefix = "";
	  try
	  {
		LOG.debug("### Within opBankOrg.getPrefix() - instrumentType = ' {} '" , instrumentType);
		if (instrumentType.equals(InstrumentType.IMPORT_DLC))
			prefix = this.getAttribute("imp_DLC_prefix");
		if (instrumentType.equals(InstrumentType.STANDBY_LC))
			prefix = this.getAttribute("incoming_SLC_prefix");
		if (instrumentType.equals(InstrumentType.GUARANTEE))
			prefix = this.getAttribute("guar_prefix");
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
			prefix = this.getAttribute("airway_bill_prefix");
		if (instrumentType.equals(InstrumentType.SHIP_GUAR))
			prefix = this.getAttribute("steamship_guar_prefix");
		if (instrumentType.equals(InstrumentType.EXPORT_COL))
			prefix = this.getAttribute("dir_snd_coll_prefix");
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) //Vasavi CR 524 03/31/2010 Add
			prefix = this.getAttribute("new_exp_coll_prefix");
		if (instrumentType.equals(InstrumentType.EXPORT_DLC))
			prefix = this.getAttribute("export_LC_prefix");
		if (instrumentType.equals(InstrumentType.FUNDS_XFER))
			prefix = this.getAttribute("funds_transfer_prefix");
		if (instrumentType.equals(InstrumentType.LOAN_RQST))
			prefix = this.getAttribute("loan_req_prefix");
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
			prefix = this.getAttribute("request_advise_prefix");
		//Krishna CR 375-D 07/19/2007 Begin
		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
			prefix = this.getAttribute("approval_to_pay_prefix");
		//Krishna CR 375-D 07/19/2007 End
		// Chandrakanth CR 451 CM 10/21/2008 Begin
		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
			prefix = this.getAttribute("transfer_btw_acct_prefix");
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT))
			prefix = this.getAttribute("domestic_payment_prefix");
		// Chandrakanth CR 451 CM 10/21/2008 End
		//Pratiksha CR-509 DDI 12/09/2009 Begin
		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
			prefix = this.getAttribute("direct_debit_prefix");
		//Pratiksha CR-509 DDI 12/09/2009 End
	  }
	  catch (AmsException e)
	  {
		LOG.error("Exception in getPrefix():  " ,e);
	  }
	  if (prefix == null)
		prefix = "";
	  LOG.debug("return prefix = ' {} '" , prefix);
	  return prefix;

   }

  /**
   *
   *
   * @param  instrumentType
   * @return String
   */
   public String getSuffix(String instrumentType) throws RemoteException
   {
	  String suffix = "";
	  try
	  {
		LOG.debug("### Within opBankOrg.getSuffix() - instrumentType = ' {} '", instrumentType );
		if (instrumentType.equals(InstrumentType.IMPORT_DLC))
			suffix = this.getAttribute("imp_DLC_suffix");
		if (instrumentType.equals(InstrumentType.STANDBY_LC))
			suffix = this.getAttribute("incoming_SLC_suffix");
		if (instrumentType.equals(InstrumentType.GUARANTEE))
			suffix = this.getAttribute("guar_suffix");
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
			suffix = this.getAttribute("airway_bill_suffix");
		if (instrumentType.equals(InstrumentType.SHIP_GUAR))
			suffix = this.getAttribute("steamship_guar_suffix");
		if (instrumentType.equals(InstrumentType.EXPORT_COL))
			suffix = this.getAttribute("dir_snd_coll_suffix");
		if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL)) //Vasavi CR 524 03/31/2010 Add
			suffix = this.getAttribute("new_exp_coll_suffix");
		if (instrumentType.equals(InstrumentType.EXPORT_DLC))
			suffix = this.getAttribute("export_LC_suffix");
		if (instrumentType.equals(InstrumentType.FUNDS_XFER))
			suffix = this.getAttribute("funds_transfer_suffix");
		if (instrumentType.equals(InstrumentType.LOAN_RQST))
			suffix = this.getAttribute("loan_req_suffix");
		if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
			suffix = this.getAttribute("request_advise_suffix");
		//Krishna CR 375-D 07/19/2007 Begin
		if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
			suffix = this.getAttribute("approval_to_pay_suffix");
		//Krishna CR 375-D 07/19/2007 End
		// Chandrakanth CR 451 CM 10/21/2008 Begin
		if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
			suffix = this.getAttribute("transfer_btw_acct_suffix");
		if (instrumentType.equals(InstrumentType.DOMESTIC_PMT))
			suffix = this.getAttribute("domestic_payment_suffix");
		// Chandrakanth CR 451 CM 10/21/2008 End
		//Pratiksha CR-509 DDI 12/09/2009 Begin
		if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
			suffix = this.getAttribute("direct_debit_suffix");
		//Pratiksha CR-509 DDI 12/09/2009 End
	  }
	  catch (AmsException e)
	  {
		LOG.error("Exception in getPrefix() " , e);
	  }

	  if (suffix == null)
		suffix = "";
	  LOG.debug("return suffix = ' {} '" , suffix);
	  return suffix;

   }


  /**
   * Descendent implemented method that identifies listing object
   * criteria.
   *
   * Used to identify one or more listTypes and list processing criteria.
   *
   * @param listTypeName The name of the listtype (as specified by the
   * developer)
   * @return The qsuedo SQL used for specifying the list criteria.
   *
   * Acceptable listTypes are:
   * activeByBankOrgOp - search active operational bank organizations
   * 			 with the given bank organizaion group oid
   *
   */
  public String getListCriteria(String type)
  {
	  if (type.equals("activeByBankOrgOp"))
	  {

	  return "a_bank_org_group_oid = {0} and activation_status = {1}";

	  }
	  return "";

  }


/**
 * Performs User validation:
 *    2 Main checks go on here.  <br>
 * 1) Verify that the name for the new Operational Bank Org (entered by the user is unique. <br>
 * 2) Verify that each of the Prefixes and Suffixes combined do not exceed 8 characters.  There
 *    are 7 sets of prefixes/suffixes so the if/else if logic is slightly cumbersome.  These
 *    checks were required by the use case for the Operational Bank Org Jsp.<br>
 *
 * <STRONG> NOTE: </STRONG>
 *    You'll notice that the rule regarding the validation for a maximum of 6 characters for a
 *    prefix or a suffix is enforced by Database and the input field only allowing 6 characters
 *    for entry.  For that reason, it was not necessary to add additional validation logic here.
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */


	protected void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{

		//Variables are set up to make the use of the attributes easier to read.
		String importPrefix         = this.getAttribute("imp_DLC_prefix");
		String importSuffix         = this.getAttribute("imp_DLC_suffix");
		String standByLCprefix      = this.getAttribute("incoming_SLC_prefix");
		String standByLCsuffix      = this.getAttribute("incoming_SLC_suffix");
		String guaranteePrefix      = this.getAttribute("guar_prefix");
		String guaranteeSuffix      = this.getAttribute("guar_suffix");
		String shippingPrefix       = this.getAttribute("steamship_guar_prefix");
		String shippingSuffix       = this.getAttribute("steamship_guar_suffix");
		String airWaybillPrefix     = this.getAttribute("airway_bill_prefix");
		String airWaybillSuffix     = this.getAttribute("airway_bill_suffix");
		String exportLCPrefix       = this.getAttribute("export_LC_prefix");
		String exportLCSuffix       = this.getAttribute("export_LC_suffix");
		String expCollPrefix        = this.getAttribute("dir_snd_coll_prefix");
		String expCollSuffix        = this.getAttribute("dir_snd_coll_suffix");
		String newExpCollPrefx      = this.getAttribute("new_exp_coll_prefix"); //Vasavi CR 524 03/31/2010 Add
		String newExpCollSuffx      = this.getAttribute("new_exp_coll_suffix"); //Vasavi CR 524 03/31/2010 Add
		String fundsTransferPrefix  = this.getAttribute("funds_transfer_prefix");
		String fundsTransferSuffix  = this.getAttribute("funds_transfer_suffix");
		String loanRequestPrefix    = this.getAttribute("loan_req_prefix");
		String loanRequestSuffix    = this.getAttribute("loan_req_suffix");
		String requestAdvisePrefix  = this.getAttribute("request_advise_prefix");
		String requestAdviseSuffix  = this.getAttribute("request_advise_suffix");
		//Krishna CR 375-D 07/19/2007 Begin
		String approvalToPayPrefix  = this.getAttribute("approval_to_pay_prefix");
		String approvalToPaySuffix  = this.getAttribute("approval_to_pay_suffix");
		// Chandrakanth CR 451 CM 10/21/2008 Begin
		String transferBtwAcctPrefix = this.getAttribute("transfer_btw_acct_prefix");
		String transferBtwAcctSuffix = this.getAttribute("transfer_btw_acct_suffix");
		String domesticPaymentPrefix = this.getAttribute("domestic_payment_prefix");
		String domesticPaymentSuffix = this.getAttribute("domestic_payment_suffix");
		// Chandrakanth CR 451 CM 10/21/2008 End
                //Krishna CR 375-D 07/19/2007 End
		//		Pratiksha CR-509 DDI 12/09/2009 Begin
		String directDebitPrefix  = this.getAttribute("direct_debit_prefix");
		String directDebitSuffix  = this.getAttribute("direct_debit_suffix");
		String external_bank_ind = this.getAttribute("external_bank_ind");
		String swift1=this.getAttribute("swift_address_part1");
		String swift2=this.getAttribute("swift_address_part2");
		
		
		//		Pratiksha CR-509 DDI 12/09/2009 End
                String name = this.getAttribute("name");

                    // verify that the entered Operational Bank Org name is unique

                if (StringFunction.isBlank(external_bank_ind)) {

                    if (!isUnique("name", name,
                            " and a_bank_org_group_oid = " + getAttribute("bank_org_group_oid") + " and external_bank_ind = 'N'"))
                    {
                    	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
                    }

                }else{
                    if (!isUnique("name", name,
                            " and p_owner_bank_oid = " + getAttribute("owner_bank_oid") + " and external_bank_ind = 'Y'"))
                    {
                    		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
                    }
                }


                String proponixId = this.getAttribute("Proponix_id");

                    // verify that the entered Operational Bank Org name is unique

                if (!isUnique("Proponix_id", proponixId,
                              " and p_owner_bank_oid = " + getAttribute("owner_bank_oid")))
                {
                    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                      TradePortalConstants.ALREADY_EXISTS, proponixId, attributeMgr.getAlias("Proponix_id"));
                }


                // smanohar KyribaPPX 268 start  
               
                
                String opId =this.getAttribute("bank_org_group_oid");
                		

                
                if (!StringFunction.isBlank(external_bank_ind) && TradePortalConstants.INDICATOR_YES.equals(external_bank_ind)){
                	
                	 if (StringFunction.isBlank(swift1) ||StringFunction.isBlank(swift2)){
                 		 this.getErrorManager().issueError(
             					TradePortalConstants.ERR_CAT_1, 
             					TradePortalConstants.SWIFT_ADDRESS_REQUIRED); 
                	 }
                }else {
               	 
                	if (StringFunction.isBlank(proponixId)){
                 		 this.getErrorManager().issueError(
              					TradePortalConstants.ERR_CAT_1, 
              					TradePortalConstants.TPS_ID_REQUIRED); 
                	}
                		
                   	if (StringFunction.isBlank(opId)){
                		 this.getErrorManager().issueError(
             					TradePortalConstants.ERR_CAT_1, 
             					TradePortalConstants.BANK_GROUP_REQUIRED); 
                   	}
                 
                }
                	
               // smanohar KyribaPPX 268 end  
                
               
                
		// Verify that the prefixes and suffixes do NOT exceed 8 characters
		// There is a series of 9 checks here to enforce required business logic.

		// Import Letter of Credit check
		compareLength(importPrefix, importSuffix, getResourceManager().
		getText("OpBankOrg.ILCPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Standby Letter of Credit check
		compareLength(standByLCprefix, standByLCsuffix, getResourceManager().
		getText("OpBankOrg.SLCPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Guarantee check
		compareLength(guaranteePrefix, guaranteeSuffix, getResourceManager().
		getText("OpBankOrg.GteePrefix", TradePortalConstants.TEXT_BUNDLE));

		//Shipping Guarantee check
		compareLength(shippingPrefix, shippingSuffix, getResourceManager().
		getText("OpBankOrg.GteePrefix", TradePortalConstants.TEXT_BUNDLE));

		//Air Waybill Release check
		compareLength(airWaybillPrefix, airWaybillSuffix, getResourceManager().
		getText("OpBankOrg.AWBPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Export Letter of Credit check
		compareLength(exportLCPrefix, exportLCSuffix, getResourceManager().
		getText("OpBankOrg.ELCPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Direct Send Collection check
		compareLength(expCollPrefix, expCollSuffix, getResourceManager().
		getText("OpBankOrg.ECollPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Export Collection check Vasavi CR 524 03/31/2010 
		compareLength(newExpCollPrefx, newExpCollSuffx, getResourceManager().
		getText("OpBankOrg.NExpCollPrefix", TradePortalConstants.TEXT_BUNDLE));

		//FundsTransfer check
		compareLength(fundsTransferPrefix, fundsTransferSuffix, getResourceManager().
		getText("OpBankOrg.FundsTransferRequestPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Loan Request check
		compareLength(loanRequestPrefix, loanRequestSuffix, getResourceManager().
		getText("OpBankOrg.LoanRequestPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Request Advise check
		compareLength(requestAdvisePrefix, requestAdviseSuffix, getResourceManager().
		getText("OpBankOrg.RequestAdvisePrefix", TradePortalConstants.TEXT_BUNDLE));

		//Krishna CR 375-D 07/19/2007 Begin

		// Approval to Pay check
		compareLength(approvalToPayPrefix, approvalToPaySuffix, getResourceManager().
		getText("OpBankOrg.ApprovaltoPayPrefix", TradePortalConstants.TEXT_BUNDLE));

		//Krishna CR 375-D 07/19/2007 End
		// Chandrakanth CR 451 CM 10/21/2008 Begin
		compareLength(transferBtwAcctPrefix, transferBtwAcctSuffix, getResourceManager().
		getText("OpBankOrg.TransferBtwAccountsPrefix", TradePortalConstants.TEXT_BUNDLE));

		compareLength(domesticPaymentPrefix, domesticPaymentSuffix, getResourceManager().
		getText("OpBankOrg.DomesticPaymentPrefix", TradePortalConstants.TEXT_BUNDLE));
		// Chandrakanth CR 451 CM 10/21/2008 End

		//		Pratiksha CR-509 DDI 12/09/2009  Begin
		// Direct Debit Instruction check
		compareLength(directDebitPrefix, directDebitSuffix, getResourceManager().
		getText("OpBankOrg.DirectDebitInstructionPrefix", TradePortalConstants.TEXT_BUNDLE));
		//Pratiksha CR-509 DDI 12/09/2009 End

		// End of Prefix and Suffix checks

		validateCityStateZip();

	}// End of UserValidate()

   /**
    * When saving an Operational BankOrg, validate combination of City , State,
    * Postal Code
    */
	public void validateCityStateZip()
		throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		int countTotalNoOfChars = 0;
		String city = this.getAttribute("address_city");
		if (city != null)
			countTotalNoOfChars += city.length();
		String state = this.getAttribute("address_state_province");
		if (state != null)
			countTotalNoOfChars += state.length();
		String postalcode = this.getAttribute("address_postal_code");
		if (postalcode != null)
			countTotalNoOfChars += postalcode.length();
		//Surrewsh IR-T36000043283 12/3/2015 Start 
		
		//Validate total no. of characters
		if (countTotalNoOfChars > TradePortalConstants.CITY_STATE_ZIP_PARTYBEAN_LENGTH)
		{
			this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.TOO_MANY_CHARS_IN_COMB_PARTYBEAN);
		}
	}
	   //Surrewsh IR-T36000043283 12/3/2015 End 

   /**
	* This method is used to perform any processing necessary before
	* an object is deactivated. If any Associated Corporate Orgs has any
	* active users, issue error and do not deactivate.
	*/
   protected void userDeactivate() throws RemoteException, AmsException
   {
	  // Determine whether there are any active users in the corporate org
	  if ( isAssociated("CorporateOrganization", "activeOpBankOrgAssoc", new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE}))
	  {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                  TradePortalConstants.ACTIVE_CORP_CUSTOMERS,
			                          getAttribute("name"));
	  }
	  
	  // Determine whether there are any active users in the corporate org
	  if ( isAssociated("ExternalBank", "activeExBankOrgAssoc", new String[] {getAttribute("organization_oid"), TradePortalConstants.ACTIVE}))
	  {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                  TradePortalConstants.ACTIVE_CORP_CUSTOMERS,
			                          getAttribute("name"));
	  }


	  // check if there are any lc creation rules
	  if (isAssociated("LCCreationRule", "byOpBankOrgOid",
		  new String[] {getAttribute("organization_oid")})) {
			  this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			      TradePortalConstants.ACTIVE_LC_CREATION_RULE, getAttribute("name"));
		  }

   }

   /**
	* This method localizes the if logic the gets called repititiously.
	*
	* @param String prefix - the prefix for the instrument being compared.
	* @param String suffix - the suffix for the instrument being compared.
	* @param String instrument - the literal instrument type for the error message.
	*/
   private void compareLength(String prefix, String suffix, String instrument)
		   throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
   {
		if( (prefix.length() + suffix.length()) > 8  )
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PREFIXES_SUFFIXES, instrument);

   }


}