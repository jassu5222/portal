package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Abstract business object to represent an organization in the Trade Portal.
 * All organizations are subclasses of this business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface OrganizationHome extends EJBHome
{
   public Organization create()
      throws RemoteException, CreateException, AmsException;

   public Organization create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
