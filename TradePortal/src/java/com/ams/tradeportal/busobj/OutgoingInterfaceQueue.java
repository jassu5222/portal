package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Contains data that is waiting to be processed by the outgoing agent.   Data
 * is placed into this table when an action happens on the portal that requires
 * OTL action (such as sending a message to the bank or authorizing).   The
 * outgoing agent polls this table looking for data to process.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface OutgoingInterfaceQueue extends TradePortalBusinessObject
{   
	public void moveToHistory() throws RemoteException, AmsException;

}
