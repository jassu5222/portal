package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.mediator.util.InvoicePackagerUtility;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OutgoingInterfaceQueueBean extends OutgoingInterfaceQueueBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(OutgoingInterfaceQueueBean.class);
    //provide a upper limit for duration seconds to avoid failing the entire record if calculations are out of bounds	
    private static long MAX_DURATION_SECONDS = 9999999999L;
    
    /**
    *   This method copies current row from outgoing_queue table into outgoing_q_history, 
    *   then deletes original from outgoing_queue table.
    *
    *   @return int
    */
    public void moveToHistory() throws RemoteException, AmsException{
    	PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("TradePortal");        
        OutgoingQueueHistory outgoing_q_history = (OutgoingQueueHistory)createServerEJB("OutgoingQueueHistory");         
                     
        outgoing_q_history.newObject();
        
        outgoing_q_history.setAttribute("date_created",             getAttribute("date_created"));
        outgoing_q_history.setAttribute("date_sent",                getAttribute("date_sent"));
        outgoing_q_history.setAttribute("status",                   getAttribute("status"));
        outgoing_q_history.setAttribute("confirm_status",           getAttribute("confirm_status"));           
        outgoing_q_history.setAttribute("msg_text",                 getAttribute("msg_text"));
        outgoing_q_history.setAttribute("packaging_error_text",     getAttribute("packaging_error_text"));
        outgoing_q_history.setAttribute("unpackaging_error_text",   getAttribute("unpackaging_error_text"));  
        outgoing_q_history.setAttribute("msg_type",                 getAttribute("msg_type"));
        outgoing_q_history.setAttribute("message_id",               getAttribute("message_id"));
        outgoing_q_history.setAttribute("reply_to_message_id",      getAttribute("reply_to_message_id"));
        outgoing_q_history.setAttribute("agent_id",                 getAttribute("agent_id"));
        outgoing_q_history.setAttribute("mail_message_oid",         getAttribute("mail_message_oid"));
        outgoing_q_history.setAttribute("instrument_oid",           getAttribute("instrument_oid"));
        outgoing_q_history.setAttribute("transaction_oid",          getAttribute("transaction_oid"));
        outgoing_q_history.setAttribute("process_parameters",       getAttribute("process_parameters"));//IR - PAUJ012856860
        outgoing_q_history.setAttribute("system_name",       "MIDDLEWARE");
        outgoing_q_history.setAttribute("server_name",       portalProperties.getString("serverName"));
        outgoing_q_history.setAttribute("xml_sent_time",       getAttribute("xml_sent_time"));
        //include values for processing slas
        String dateCreated = getAttribute("date_created");
        String packStart    = getAttribute("datetime_pack_start");
        String dateSent   = getAttribute("date_sent");
        outgoing_q_history.setAttribute("datetime_pack_start", packStart);
        try {
            long packSeconds = TPDateTimeUtility.calculateDurationSeconds(packStart,dateSent);
            if ( packSeconds >= 0 ) {
            	if ( packSeconds <= MAX_DURATION_SECONDS ) {
                	String secondsStr = "" + packSeconds;
                	outgoing_q_history.setAttribute("pack_seconds", secondsStr);
            	} else {
            		//avoid failure if duration is out of bounds
	        		debug("OutgoingInterfaceQueueBean.moveToHistory::pack duration out of bounds. Ignoring.");
            	}
        	} else {
            	//avoid failure if duration is negative
        		LOG.debug("OutgoingInterfaceQueueBean.moveToHistory::negative pack duration. Ignoring.");
        	}
        } catch (Throwable t) { //i.e. illegal argument exception
    		//just write out to the log (i.e. debug)
    		LOG.error("OutgoingInterfaceQueueBean.moveToHistory::unable to calculate pack seconds duration.Ignoring. Exception: " ,t);
        }
        try {
        	long processSeconds = TPDateTimeUtility.calculateDurationSeconds(dateCreated,dateSent);
        	if ( processSeconds>=0 ) {
            	if ( processSeconds <= MAX_DURATION_SECONDS ) {
            		String secondsStr = "" + processSeconds;
            	    outgoing_q_history.setAttribute("process_seconds", secondsStr);
            	} else {
            		//avoid failure if duration is out of bounds
	        		LOG.debug("OutgoingInterfaceQueueBean.moveToHistory::processing duration out of bounds. Ignoring.");
            	}
        	} else {
            	//avoid failure if duration is negative
        		LOG.debug("OutgoingInterfaceQueueBean.moveToHistory::negative processing duration. Ignoring.");
        	}
        } catch (Throwable t) { //i.e. illegal argument exception
			//just write out to the log (i.e. debug)
			LOG.error("OutgoingInterfaceQueueBean.moveToHistory::unable to calculate process seconds duration.Ignoring. Exception: " ,t);
		}

        if ( outgoing_q_history.save()<0 ) {
        	LOG.debug("OutgoingInterfaceQueueBean.moveToHistory::Problem saving outgoing queue history");
        }
        // Nar CR-914A Rel 9.2.0.0 02/10/2015 Begin
        if ( TradePortalConstants.INVOICE.equals(this.getAttribute("msg_type")) ) {
        	String credAppliedInvOid = InvoicePackagerUtility.getParm( "credAppliedInvDetailOid", this.getAttribute("process_parameters") );
        	if ( StringFunction.isNotBlank(credAppliedInvOid) ) {
        		CredAppliedInvDetail credAppliedInvDetail = (CredAppliedInvDetail) createServerEJB("CredAppliedInvDetail", Long.parseLong(credAppliedInvOid)); 
        		credAppliedInvDetail.setAttribute( "date_sent", this.getAttribute("date_sent") );
        		int success = credAppliedInvDetail.save();
        		if ( success != 1) {
        			LOG.debug("OutgoingInterfaceQueueBean.moveToHistory::Problem setting delete indicator in CredAppliedInvDetail");
        		}
        	}
        }
       // Nar CR-914A Rel 9.2.0.0 02/10/2015 End 
        
        this.delete(); 
            
    }// end of moveToHistory()

}
