package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Contains data that is waiting to be processed by the outgoing agent.   Data
 * is placed into this table when an action happens on the portal that requires
 * OTL action (such as sending a message to the bank or authorizing).   The
 * outgoing agent polls this table looking for data to process.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface OutgoingInterfaceQueueHome extends EJBHome
{
   public OutgoingInterfaceQueue create()
      throws RemoteException, CreateException, AmsException;

   public OutgoingInterfaceQueue create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
