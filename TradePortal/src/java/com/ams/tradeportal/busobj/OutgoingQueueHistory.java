package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * After processing, data stored as OutgoingInterfaceQueue business objects
 * is moved to be stored as OutgoingQueueHistory.  This is done to prevenet
 * the OutgoingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface OutgoingQueueHistory extends TradePortalBusinessObject
{   
}
