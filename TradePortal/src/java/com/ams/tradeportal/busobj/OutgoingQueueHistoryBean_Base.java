
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * After processing, data stored as OutgoingInterfaceQueue business objects
 * is moved to be stored as OutgoingQueueHistory.  This is done to prevenet
 * the OutgoingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OutgoingQueueHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(OutgoingQueueHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* queue_history_oid - Unique identifier */
      attributeMgr.registerAttribute("queue_history_oid", "queue_history_oid", "ObjectIDAttribute");
      
      /* date_created - Timestamp (in GMT)  when this data was originally created in the OutgoingInterfaceQueue
         table. */
      attributeMgr.registerAttribute("date_created", "date_created", "DateTimeAttribute");

      /* override_date_created - Timestamp (in GMT)  when this data was originally created in the OutgoingInterfaceQueue
      table. */
      	attributeMgr.registerAttribute("override_date_created", "override_date_created", "DateTimeAttribute");

      /* date_sent - Timestamp (in GMT) of when this data was placed onto the queue to be sent
         to OTL. */
      attributeMgr.registerAttribute("date_sent", "date_sent", "DateTimeAttribute");
      
      /* status - Lifecycle status of the message. */
      attributeMgr.registerReferenceAttribute("status", "status", "OUTGOING_Q_STATUS");
      
      /* confirm_status - Indicates whether or not a confirmation has been received from OTL for a
         message that was sent to OTL. */
      attributeMgr.registerReferenceAttribute("confirm_status", "confirm_status", "OUTGOING_Q_CONFIRM_STATUS");
      
      /* msg_text - The actual text of the XML message */
      attributeMgr.registerAttribute("msg_text", "msg_text");
      
      /* packaging_error_text - Errors from packaging. */
      attributeMgr.registerAttribute("packaging_error_text", "packaging_error_text");
      
      /* unpackaging_error_text - Errors from unpackaging */
      attributeMgr.registerAttribute("unpackaging_error_text", "unpackaging_error_text");
      
      /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML - a transaction or mail message. */
      attributeMgr.registerReferenceAttribute("msg_type", "msg_type", "QUEUE_MESSAGE_TYPE");
      
      /* message_id -  */
      attributeMgr.registerAttribute("message_id", "message_id");
      
      /* reply_to_message_id - Used for confirmation messages only.  Refers to the message that this message
         is confirming */
      attributeMgr.registerAttribute("reply_to_message_id", "reply_to_message_id");
      
      /* agent_id - The name of the agent that is currently dealing with this message.   This
         prevents multiple agents from accessing the same data. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* process_parameters - Extra processing parameters that are specific to certain message types.
         The parameters are formatted as name value pairs delimited by |, for example,
         name1=value1|name2=value2|name3=value3. */
      attributeMgr.registerAttribute("process_parameters", "process_parameters");
      
      /* transaction_oid - The OID of the transaction or main object on which an action was performed
         that required placing a row in the outgoing queue.  This is not an association
         so it can store OID of heterogeneous objects. */
      attributeMgr.registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");
      
      /* datetime_pack_start - Date and time when the packaging of the message starts. */
      attributeMgr.registerAttribute("datetime_pack_start", "datetime_pack_start", "DateTimeAttribute");
      
      /* pack_seconds - The amount of time (in seconds) it takes to package the message. */
      attributeMgr.registerAttribute("pack_seconds", "pack_seconds", "NumberAttribute");
      
      /* process_seconds - The amount of time (in seconds) it takes to process the message, starting
         from the outgoing_queue object is created to to the message being sent out. */
      attributeMgr.registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
      
      /* instrument_oid - The instrument on which an action was performed that required placing a
         row in the outgoing queue.. */
      attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
      
      /* mail_message_oid - The mail message on which an action was performed that required placing
         a row in the outgoing queue.. */
      attributeMgr.registerAssociation("mail_message_oid", "a_mail_message_oid", "MailMessage");
      
      /* xml_sent_time - Date/Time when the XML was placed on outgoing destination queue ie. TPS incoming Queue by TIM */
      attributeMgr.registerAttribute("xml_sent_time", "xml_sent_time", "DateTimeAttribute");
      /* system_name -  */
      attributeMgr.registerAttribute("system_name", "system_name");
      
      /* server_name -  */
      attributeMgr.registerAttribute("server_name", "server_name");
      
      //Rel9.3.5 CR-1028 - Start
      /* downloaded_xml_ind - Whether this transaction's xml has been downloaded. */
      attributeMgr.registerAttribute("downloaded_xml_ind", "downloaded_xml_ind", "IndicatorAttribute");
      //Rel9.3.5 CR-1028 - End
   }
   
 
   
 
 
   
}
