package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * After processing, data stored as OutgoingInterfaceQueue business objects
 * is moved to be stored as OutgoingQueueHistory.  This is done to prevenet
 * the OutgoingInterfaceQueue table from becoming too large.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface OutgoingQueueHistoryHome extends EJBHome
{
   public OutgoingQueueHistory create()
      throws RemoteException, CreateException, AmsException;

   public OutgoingQueueHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
