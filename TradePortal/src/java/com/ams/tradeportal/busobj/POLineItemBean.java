 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POLineItemBean extends POLineItemBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(POLineItemBean.class);

   public String getListCriteria(String type) 
   {
	  // Retrieve the rules by template
	  if (type.equals("byPOUploadDefinition")) {
		 return "a_owner_org_oid = {0} AND a_source_upload_definition_oid = {1}";
	  }

	  return "";
   }      
 

   /* Perform processing for a New instance of the business object */

   protected void userNewObject() throws AmsException, RemoteException
   {  
     /* Perform any New Object processing defined in the Ancestor class */
     super.userNewObject();
     this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime());
   }    
 
 
   
}
