package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents a purchase order line item that has been uploaded into the Trade
 * Portal. 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface POLineItemHome extends EJBHome
{
   public POLineItem create()
      throws RemoteException, CreateException, AmsException;

   public POLineItem create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
