

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Describes the mapping of an uploaded file to purchase order line items stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 * 
 * Can also be used to describe the process of manually entering purchase order
 * data.  The fields are used to define the fields that the user will enter,
 * the order in which they will appear and their format in goods description.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface POUploadDefinition extends TradePortalBusinessObject
{   
}
