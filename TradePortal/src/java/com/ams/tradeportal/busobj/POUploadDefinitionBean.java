
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class POUploadDefinitionBean extends POUploadDefinitionBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(POUploadDefinitionBean.class);
    public final static int STANDARD_DATE_FIELD_SIZE = 10;
    public final static int NUMBER_OF_OTHER_FIELDS = 24;
	public final static int NUMBER_OF_GOODS_DESCR_ORDER_FIELDS = 30;
    public final static int MAX_LINE_LENGTH = 65;

	StringBuffer sql = new StringBuffer();
	DocumentHandler tempDoc;
	boolean headerLengthExceeds = false;


   /**
    * User validate checks the 'part_to_validate attribute to determine which scetion of data needs
    * validating.  This only happens if the Business Object has changes pending.
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
	
      /*As part of TP-Refresh, we don't have tabs. Tabs validation need to be triggered when ever user selects save, save&close. */
      validateGeneralTab ();
      validateFileDefinitionTab ();
      validateGoodsDescriptionTab ();
   }


   /**
    *This implements the pre delete checks that were required by the business rules in the use case for the PO Upload
    *Defintition.  The 2 checks are:  <BR>
    * 	1:  Check for an Association from the PO Line Item to the PO Upload Definition object prior to deletion.
    *	2:  Check for an Association from the PO Line Item to the LC Creation Rule object prior to deletion.
    *If either of these associations exist then we need to issue an error.
    *
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        //Verify that a PO Line Item is not associated with this PO Upload Definition- if so
        //we can't let the user delete this definition.  Ie: issue an error.
	    if (isAssociated("POLineItem", "byPOUploadDefinition",
		    new String[] {getAttribute("owner_org_oid"), getAttribute("po_upload_definition_oid")} )) {

			    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.PO_ITEM_UPLOADED_WITH_THIS_DEF);
		}

        //Verify that an LC Creation Rule is not associated with this PO Upload Definition- if so
        //we can't let the user delete this definition.  Ie: issue an error.
	    if (isAssociated("LCCreationRule", "byPOUploadDefinition",
	        new String[] {getAttribute("owner_org_oid"), getAttribute("po_upload_definition_oid")} )) {

			    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.CANT_DELETE_PO_UPLD_DEFN,
			                                      getAttribute("name") );
		}
   }



      /**
    * Since the Data Display for the business object is broken up into 3 different 'views'
    * it is only appropriate to validate the data in 3 different methods ofr varying attributes.
    * To determine which attribute is validated, the value in the 'part_to_validate' attribute
    * is analyzed.  The rest of the code is based on the required business rules.
    *
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void validateGeneralTab ()
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

         LOG.debug("========= Validating the General Tab data in the PO Upload Definition Object ==========");
	     String name = getAttribute("name");
	     String ownerOrg = this.getAttribute("owner_org_oid");

	     //Make sure there is no other name in the db that matches this one.

	     if (!isUnique( "name", name, " and A_OWNER_ORG_OID=" + ownerOrg ))
	      { //" AND A_OWNER_ORG_OID=" + ownerOrg
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		  }

		 //Make sure there is only one po upload def in the db that is marked as the default.
		 //Ignore the check if the default_flag is set to 'N'.
		 String defaultFlag = this.getAttribute("default_flag");

		 if ( StringFunction.isNotBlank(defaultFlag) && defaultFlag.equals("Y") ) {

		    if (!isUnique("default_flag", defaultFlag, " AND A_OWNER_ORG_OID=" + ownerOrg ))
		    {
		        String originalName;

		        sql.append("select name from po_upload_definition ");
		        sql.append("where a_owner_org_oid = ?");
		        sql.append(" AND default_flag = ?");

		        tempDoc = DatabaseQueryBean.getXmlResultSet( sql.toString(), false, this.getAttribute("owner_org_oid"), TradePortalConstants.INDICATOR_YES );
		        LOG.debug( "*************** TempDoc == {}", tempDoc.toString() );

		        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.DEFAULT_DEFINITION,
			                              tempDoc.getAttribute("/ResultSetRecord(0)/NAME"));
		    }
		 }

         String definition_type = this.getAttribute("definition_type");

		 CorporateOrganization corporateOrg;
		 corporateOrg = (CorporateOrganization) createServerEJB("CorporateOrganization", getAttributeLong("owner_org_oid"));
		 String allowPOUpload = corporateOrg.getAttribute("allow_auto_lc_create");
		 String allowPOManual = corporateOrg.getAttribute("allow_manual_po_entry");
		 
	     String allowATPPOUpload = corporateOrg.getAttribute("allow_auto_atp_create");
		 String allowATPPOManual = corporateOrg.getAttribute("allow_atp_manual_po_entry");

		 // If the corporate customer does not allow PO Upload for both ATP and LC, the
		 // PO Definition Type cannot be for both upload and manual
		 if ((!allowPOUpload.equals(TradePortalConstants.INDICATOR_YES) && !allowATPPOUpload.equals(TradePortalConstants.INDICATOR_YES)) &&
				 (definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD) || definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_BOTH))) {
			 		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			 					TradePortalConstants.INVALID_PO_DEFINITION_TYPE);
		 }
		 
		 // If the corporate customer does not allow PO manually entered for both ATP and LC, the
		 // PO Definition Type cannot be for manual entry or for both upload and manual
		 else if ((!allowPOManual.equals(TradePortalConstants.INDICATOR_YES) && !allowATPPOManual.equals(TradePortalConstants.INDICATOR_YES)) &&
				 (definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL) || definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_BOTH))) {
			 		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			 					TradePortalConstants.INVALID_PO_DEFINITION_TYPE);
		 }
	    
		// Beneficiary name, currency and PO Amount are required if the PO Definition is for
		// Upload or both Upload and Manual
		 if (definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD)
			 || definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_BOTH)) {
			attributeMgr.requiredAttribute("ben_name_field_name");
			attributeMgr.requiredAttribute("ben_name_size");
			attributeMgr.requiredAttribute("currency_field_name");
			attributeMgr.requiredAttribute("currency_size");
			attributeMgr.requiredAttribute("amount_field_name");
			attributeMgr.requiredAttribute("amount_size");
         }


		 String fieldName;
		 String fieldSize;
		 String fieldDataType;

		 //If the field name or the field size for the Item number has a defined value,
		 //Then both pieces of data have to be present or else issue an error back to the user-
		 //forcing the user to fill in all the data for both text fields.

		 fieldName = this.getAttribute("item_num_field_name");
		 fieldSize = this.getAttribute("item_num_size");

		 if( StringFunction.isNotBlank( fieldName ) ||
		     StringFunction.isNotBlank( fieldSize ) ) {

		    if( StringFunction.isBlank( fieldName ) ||
		        StringFunction.isBlank( fieldSize ) ) {

		        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.COMPLETE_ALL_INFO, "Item Number");

		    }
		 }
		 //repeat the same check for all the 'Other' fields

		 for( int x=1; x<=NUMBER_OF_OTHER_FIELDS; x++) {
		    fieldName = this.getAttribute("other" + x + "_field_name");
		    fieldSize = this.getAttribute("other" + x + "_size");
		    fieldDataType = this.getAttribute("other" + x + "_datatype");

		    //If one of the fields for the other X is defined - then verify that all related fields for Other X
		    //has a defined value.  After that, check to see that the entered data is of the correct type.
		    if ( StringFunction.isNotBlank( fieldName ) ||
		         StringFunction.isNotBlank( fieldSize ) ||
		         StringFunction.isNotBlank( fieldDataType ) ) {

		            if( StringFunction.isBlank( fieldName ) ||
		                StringFunction.isBlank( fieldSize ) ||
		                StringFunction.isBlank( fieldDataType ) )

		                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.COMPLETE_ALL_INFO, TradePortalConstants.PO_FIELD_TYPE_DATA_ITEM + x);
		    }
         } // For loop

         //Verify that each 'Field Name' is unique (including the required fields as well as the 'Others')
         //AND verify that the field size.    The HashSet will carry the burden of testing for uniquenes.
         HashSet hSet = new HashSet();
         ResourceManager resMgr = this.getResourceManager();
         String attribute;
         String attributeUploadArray [] = new String [TradePortalConstants.PO_NUMBER_OF_FIELDS];
		 String attributeGoodsDescrArray [] = new String [NUMBER_OF_GOODS_DESCR_ORDER_FIELDS];
         Hashtable goodsDescHash;
         Hashtable fileDefHash;

         for( int x = 0; x <= (NUMBER_OF_GOODS_DESCR_ORDER_FIELDS - 1); x++ ) {
            attributeGoodsDescrArray[x] = "goods_descr_order_" + (x+1);
         }

         goodsDescHash = this.getAttributes( attributeGoodsDescrArray );

         for( int x = 0; x <= (TradePortalConstants.PO_NUMBER_OF_FIELDS - 1); x++ ) {
            attributeUploadArray[x] = "upload_order_" + (x+1);
         }

         fileDefHash = this.getAttributes( attributeUploadArray );

     // PO Number
         attribute = this.getAttribute("po_num_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, attribute );
         testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.PONumber", TradePortalConstants.TEXT_BUNDLE),
                              this.getAttribute("po_num_datatype"), this.getAttribute("po_num_size"), 0, 14);
         lookupValueInLCCreationRules( attribute, "po_num");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "po_num" );


    // Item Number
         attribute = this.getAttribute("item_num_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, attribute );
         testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.ItemNumber", TradePortalConstants.TEXT_BUNDLE),
                              this.getAttribute("item_num_datatype"), this.getAttribute("item_num_size"), 0, 14);
         lookupValueInLCCreationRules( attribute, "item_num");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "item_num" );

	// PO Text
		 attribute = this.getAttribute("po_text_field_name");
		 hSet = testNonUniqueFieldNameError ( hSet, attribute );
		 testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.POText", TradePortalConstants.TEXT_BUNDLE),
							  this.getAttribute("po_text_datatype"), this.getAttribute("po_text_size"), 0, 250);
		 lookupValueInLCCreationRules( attribute, "po_text");
		 lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "po_text" );

    // Benificiary Name
         attribute = this.getAttribute("ben_name_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, attribute );
         testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.BeneficiaryName", TradePortalConstants.TEXT_BUNDLE),
                              this.getAttribute("ben_name_datatype"), this.getAttribute("ben_name_size"), 0, 35);
         lookupValueInLCCreationRules( attribute, "ben_name");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "ben_name" );

    // Currency
         attribute = this.getAttribute("currency_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, attribute );
         testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.Currency", TradePortalConstants.TEXT_BUNDLE),
                              this.getAttribute("currency_datatype"), this.getAttribute("currency_size"), 0, 3);
         lookupValueInLCCreationRules( attribute, "currency");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "currency" );

    // Amount
         attribute = this.getAttribute("amount_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, attribute );
         testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.POAmount", TradePortalConstants.TEXT_BUNDLE),
                              this.getAttribute("amount_datatype"), this.getAttribute("amount_size"), 0, 22);
         lookupValueInLCCreationRules( attribute, "amount");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "amount" );

   // Latest Shipment Date
         attribute = this.getAttribute("last_ship_dt_field_name");
         hSet = testNonUniqueFieldNameError ( hSet, this.getAttribute("last_ship_dt_field_name") );
         //Don't need to test the Latest Shipment date size since the value is hardcoded - in the jsp.
         lookupValueInLCCreationRules( attribute, "last_ship_dt");
         lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "last_ship_dt" );

   // Other (s)
         for( int x = 1; x <= NUMBER_OF_OTHER_FIELDS; x++ ) {
              attribute = this.getAttribute("other" + x + "_field_name");
              hSet = testNonUniqueFieldNameError ( hSet, this.getAttribute("other" + x + "_field_name") );
              testFieldSizeError ( resMgr.getText("POUploadDefinitionDetail.Other", TradePortalConstants.TEXT_BUNDLE) + " " + x,
                                   this.getAttribute("other"+ x + "_datatype"), this.getAttribute("other" + x + "_size"), 0, 35);
              lookupValueInLCCreationRules( attribute, "other"+ x);
              lookupValueOnOtherTabs( goodsDescHash, fileDefHash, attribute, "other"+ x );
         }

        //When a field on the General tab is edited, if there is an existing Association From a PO Line Item
        //Table then we want to throw a warning back tot he user, that what ever changes they just made that
        //it could have an adverse affect - WARNING.
	    if (isAssociated("POLineItem", "byPOUploadDefinition",
		    new String[] {getAttribute("owner_org_oid"), getAttribute("po_upload_definition_oid")} )) {

			    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.EXISTING_LINE_ITEMS_W_THIS_DEF);
        }

   // Now that the validations for the general tab are complete - one job remains to be done here.
   // We need to run through the data in the 'Other' fields and IF there are any gaps:
   // Meaning if Other 1 has data in it's fields and Other 5 does as well, But other 2 - 4 do not,
   // then we need to move other 5's data to Other 2.
   // The beauty of this is, if there's an error that's been issued, then it does'nt really matter
   // since the save will roll the transaction back anyway - BUT we did'nt alter the XML doc
   // So when the user returns the data will look like what they tried to save.

   //Is there a StringService method that does something like this?  Takes in an array or hash and
   //returns an optimized object?

         Hashtable attributesHash = new Hashtable( NUMBER_OF_OTHER_FIELDS * 3 );
         int key = 1;
         for( int x = 1; x <= NUMBER_OF_OTHER_FIELDS; x++ ) {
              String attFieldName = this.getAttribute("other" + x + "_field_name");
              String attSize      = this.getAttribute("other" + x + "_size");
              String attDatatype  = this.getAttribute("other" + x + "_datatype");

              if( StringFunction.isNotBlank( attFieldName ) &&
                  StringFunction.isNotBlank( attSize )      &&
                  StringFunction.isNotBlank( attDatatype )) {

                    attributesHash.put( new Integer(key++), attFieldName );
                    attributesHash.put( new Integer(key++), attSize );
                    attributesHash.put( new Integer(key++), attDatatype );
              }
         }


         //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
         key = 1;
         for( int x = 1; x <= NUMBER_OF_OTHER_FIELDS; x++ ) {
              this.setAttribute( "other" + x + "_field_name", (String)attributesHash.get( new Integer(key++) ) );
              this.setAttribute( "other" + x + "_size", (String)attributesHash.get( new Integer(key++) ) );
              this.setAttribute( "other" + x + "_datatype", (String)attributesHash.get( new Integer(key++) ) );
         }

   }



   /**
    * Since the Data Display for the business object is broken up into 3 different 'views'
    * it is only appropriate to validate the data in 3 different methods ofr varying attributes.
    * To determine which attribute is validated, the value in the 'part_to_validate' attribute
    * is analyzed.  The rest of the code is based on the required business rules.
    *
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void validateFileDefinitionTab ()
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

         LOG.debug("========= Validating the File Definition Tab data in the PO Upload Definition Object ==========");

         String file_Format_Type = this.getAttribute("file_format");
         String definition_type = this.getAttribute("definition_type");
         String delimiter_Character = this.getAttribute("delimiter_char");
         HashSet hSet = new HashSet();
         Hashtable orderList = new Hashtable( TradePortalConstants.PO_NUMBER_OF_FIELDS );

         // If this PO Definition could be used for file upload, need to validate the file format
		 if (definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD)
		     || definition_type.equals(TradePortalConstants.PO_DEFINITION_TYPE_BOTH)) {
         	// one of the file format radial buttons must be selected.
         	if ( StringFunction.isBlank( file_Format_Type ) ) {

            	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                  TradePortalConstants.SPECIFY_PO_FILE_FORMAT );

         	// If the user selected a delimited file format, then verify that a delimiter character was selected.
         	// Otherwise issue an error.
         	}else if( file_Format_Type.equals( TradePortalConstants.PO_DELIMITED ) &&
                   StringFunction.isBlank( delimiter_Character ) ) {

            	this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                              TradePortalConstants.SPECIFY_CHARACTER_TYPE );
         	}
		 }

         for( int x = 1; x <= TradePortalConstants.PO_NUMBER_OF_FIELDS; x++ ) {

            String uploadOrder = this.getAttribute("upload_order_" + x);

            if( StringFunction.isNotBlank( uploadOrder ) ) {

                // If the user tries to select the same option more than once - issue an error.
                if ( !hSet.add( uploadOrder ) ) {

                    String uploadOrderDescription = this.getAttribute(uploadOrder + "_field_name");

                    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                      TradePortalConstants.FIELD_ALREADY_SELECTED,
                                                      uploadOrderDescription,
                                                      (String)orderList.get( uploadOrder ) );
                }else{
                    orderList.put( uploadOrder, String.valueOf(x) );
                }

            }
         }

         //Validate that all the fields defined in the General tab is selected in the File Definition
         //tab.  If A defined field is NOT used on the PO Details tab - then throw a warning to the user.
         int warningCount= 0;

         warningCount = lookupFieldNameInOrderList ( hSet, "po_num", warningCount );
         warningCount = lookupFieldNameInOrderList ( hSet, "item_num", warningCount);
		 warningCount = lookupFieldNameInOrderList ( hSet, "po_text", warningCount );
         warningCount = lookupFieldNameInOrderList ( hSet, "ben_name", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSet, "currency", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSet, "amount", warningCount);
         warningCount = lookupFieldNameInOrderList ( hSet, "last_ship_dt", warningCount);

         for ( int x = 1; x <= NUMBER_OF_OTHER_FIELDS; x++ ) {
            warningCount = lookupFieldNameInOrderList ( hSet, "other" + x , warningCount);
         }

         // The rest of this method will look at the user selected list of file definition (order)
         // and if the user left gaps in the selected list: ie made a selection for #2 and #4 but not for #3,
         // then we need to move the #4 selection into Order #3.
         Hashtable  attributesHash = new Hashtable( TradePortalConstants.PO_NUMBER_OF_FIELDS );
         String     attUploadOrder = "";
         int        key = 1;

         for( int x = 1; x <= TradePortalConstants.PO_NUMBER_OF_FIELDS; x++ ) {
              attUploadOrder = this.getAttribute("upload_order_" + x );

              if( StringFunction.isNotBlank( attUploadOrder ) ) {

                    attributesHash.put( new Integer(key++), attUploadOrder );
              }
         }

         //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
         for( int x = 1; x <= TradePortalConstants.PO_NUMBER_OF_FIELDS; x++ ) {
              this.setAttribute( "upload_order_" + x, (String)attributesHash.get( new Integer(x) ) );
         }

   }



   /**
    * Since the Data Display for the business object is broken up into 3 different 'views'
    * it is only appropriate to validate the data in 3 different methods ofr varying attributes.
    * To determine which attribute is validated, the value in the 'part_to_validate' attribute
    * is analyzed.  The rest of the code is based on the required business rules.
    *
    * Ths only validation for this method is we need to chack to verify that the user selected
    * length is not longer than 65 characters.
    *
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void validateGoodsDescriptionTab ()
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	   LOG.debug("========= Validating the Goods Description Tab data in the PO Upload Definition Object ==========");

	   int lineLength = getLineLengthInteger(); //<Amit - IR NNUF032267490 -06/01/2005 >

	   if(lineLength == 0) {
		   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				   TradePortalConstants.FIELD_NAME_REQUIRED);
	   }else{    		
		   if(TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("include_column_header"))){
			   int fieldSizeLength = getFieldSizeLength();    			
			   if( lineLength > MAX_LINE_LENGTH  && headerLengthExceeds ) 
				   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						   TradePortalConstants.COLUMN_LENGTH_IN_HEADER_EXCEEDS);
			   if( fieldSizeLength > MAX_LINE_LENGTH ) 
				   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						   TradePortalConstants.LINE_LENGTH_EXCEEDED_SHORT);
		   }else{    			 
			   if( lineLength > MAX_LINE_LENGTH ) {
				   this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						   TradePortalConstants.LINE_LENGTH_EXCEEDED_SHORT );
			   }
		   }
	   }

         // The rest of this mthod will look at the user selected list of goods description (order)
         // and if the user left gaps in the selected list: ie made a selection for #2 and #4 but not for #3,
         // then we need to move the #4 selection into Order #3.
         Hashtable  attributesHash = new Hashtable( NUMBER_OF_GOODS_DESCR_ORDER_FIELDS );
         String     attGoodsDescOrder = "";
         int        key = 1;

         for( int x = 1; x <= NUMBER_OF_GOODS_DESCR_ORDER_FIELDS; x++ ) {
              attGoodsDescOrder = this.getAttribute("goods_descr_order_" + x );

              if( StringFunction.isNotBlank( attGoodsDescOrder ) ) {

                    attributesHash.put( new Integer(key++), attGoodsDescOrder );
              }
         }

         //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
         for( int x = 1; x <= NUMBER_OF_GOODS_DESCR_ORDER_FIELDS; x++ ) {
              this.setAttribute( "goods_descr_order_" + x, (String)attributesHash.get( new Integer(x) ) );
         }

   }



   /**
    * This method is designed to use a HashSet to verify uniqueness of a String
    * against a list of Strings. If a duplication occurs, then an error is issued.
    * No Test occurs if the attribute is null -or- an empty string.
    *
    * @param HashSet hSet       - the set used to check the attribute against.
    * @param String attribute   - the object being tested for uniqueness.
    *
    * @return HashSet   - resulting object based on the addition of the string.
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public HashSet testNonUniqueFieldNameError (HashSet hSet, String attribute)
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        if( StringFunction.isNotBlank( attribute ) ) {

          if ( !hSet.add( attribute ) ){

                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                           TradePortalConstants.DUPLICATES_NOT_ALLOWED);
		  }
		}
		return hSet;
   }



   /**
    * This method checks for all the size validations for the General tab (as specified by the use case).
    * Basically a minimum size and a maximum size is passed in, if the user entered size is not in this
    * range, then we issue an error.  If the user specifies the datatype to be a date then we need to
    * verify that the size is equal to '10' or issue an error.
    *
    * This method is written generically so that it can cover 3 different business rule requirements.
    *
    * @param String FieldName       - the Field Name to be used for the Error display
    * @param String DataType        - The user defined datatype : Date/Number/Text
    * @param int Size               - User defined size of thext field for data entry.
    * @param int MinSize            - The minimum size this field is allowed to be...
    * @param int MaxSize            - The maximum size this field is allowed to be...
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void testFieldSizeError (String fieldName, String dataType, String size, int minSize, int maxSize)
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

     try{
        if( StringFunction.isNotBlank( size ) ) {

            Integer iSize = new Integer( size );

            if( (StringFunction.isNotBlank( dataType ) ) &&
                (dataType.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE)) ) {

                if( iSize.intValue() != STANDARD_DATE_FIELD_SIZE ) {

                    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                               TradePortalConstants.SIZE_CANNOT_BE, fieldName, iSize.toString() );
                }
            }
            if( (iSize.intValue() <= minSize) || (iSize.intValue() > maxSize) ) {

                    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                               TradePortalConstants.SIZE_CANNOT_BE, fieldName, iSize.toString() );
            }
        }//size isNotBlank
        //Should never get here - if so then the data was null then so what - there's another validation for that.
     }catch( NumberFormatException e ) {
        LOG.error("NumberFormatException at testFieldSizeError(): ",e);
        throw new AmsException( e.getMessage() );
     }
   }


   /**
    * The business rule in question requires the check that the field that's about to
    * be removed - verify that this field is not referenced on the LC Creation Rules page.
    * If it is being used on that (table) page then issue an error.  To accomplish this,
    * specific sql needed to be written.  Each LC Creation Rule is associated with a
    * certain PO Upload Definition.  Only the LC Creation Rules that are associated to this
    * PO Upload Definition need to be checked.
    *
    * @param String FieldValue   - Value of the field to be checked for a null
    * value (erased by the user)
    * @param String Physical Field Name - Field to be looked up in the LC Creation Rules table.
    *
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void lookupValueInLCCreationRules(String fieldValue, String physicalFieldName)
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

          if( StringFunction.isBlank( fieldValue ) ) {

	            Vector list;
                sql = new StringBuffer();

                sql.append("select name from lc_creation_rule where a_owner_org_oid = ?");
                sql.append(" AND a_po_upload_def_oid= ?");
                sql.append(" AND ( '");
                sql.append( physicalFieldName );
                sql.append("' in ( criterion1_data, criterion2_data, criterion3_data, ");
                sql.append("criterion4_data, criterion5_data, criterion6_data ) )");

                tempDoc = DatabaseQueryBean.getXmlResultSet( sql.toString(), false, this.getAttribute("owner_org_oid"), this.getAttribute("po_upload_definition_oid") );

                if ( tempDoc != null) {

		            list = tempDoc.getFragments("/ResultSetRecord");

		            if( list.size() > 0 ) {

		                this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                    TradePortalConstants.IN_USE_FOR_LC_CREATION_RULE,
			                                    physicalFieldName, tempDoc.getAttribute("/ResultSetRecord(0)/NAME"));
			        }
			    }
          }//isBlank (fieldName)
   }


   /**
    * The business rule in question requires that before a value be deleted, that we look up
    * the data on the two other tabs and make sure that this value is not referenced there.  If this value is referenced
    * on either of the other tabs, then issue an error.  This method uses BusinessObjects.getAttributes
    * method to build a hashtable of value that the field might exist in. if the Hashtable does 'contain'
    * the field being removed by the user then we have a problem. - issue an Error!
    *
    * @param Hashtable GoodsDescHash    - List of values on the Goods Desc.
    * @param Hashtable fileDefHash    - List of values on the File Def tab.
    * @param String FieldName           - Field being removed by the user.
    * @param String Physical Field Name - Value stored on the tab that needs to be looked up.
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public void lookupValueOnOtherTabs(Hashtable goodsDescHash, Hashtable fileDefHash, String fieldName, String physicalFieldName)
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        if( StringFunction.isBlank( fieldName ) ) {
            if( goodsDescHash.contains( physicalFieldName ) ) {

		            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                               TradePortalConstants.IN_USE_FOR_GOODS_DESCRIPTION,
			                               physicalFieldName, resMgr.getText("POUploadDefinitionDetail.GoodsDescriptionLayout", TradePortalConstants.TEXT_BUNDLE));

            }

            if( fileDefHash.contains( physicalFieldName ) ) {
		            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                               TradePortalConstants.IN_USE_FOR_GOODS_DESCRIPTION,
			                               physicalFieldName, resMgr.getText("POUploadDefinitionDetail.FileDefinition", TradePortalConstants.TEXT_BUNDLE));
            }
         }
   }



   /**
    *  This method is designed to lookup a passed in value in the passed in HashSet by trying
    *  to add itto the current list.  If the added value is returned true, then the user has NOT
    *  added this value to the list yet.  In this case we issue a warning back to the user.
    *  This is all contingent that the value was populated on the PO Upload Definition object to
    *  begin with...
    *
    *  @param HashSet hset  - list of values already added based on selection in the File Definition tab.
    *  @param String  vlaue - this is the value to lookup in the list.
    *  @param int warningCount - the number of warnings issued so far
    *  @return int the new warningCount after running the method
    *  @exception java.rmi.RemoteException
    *  @exception com.amsinc.ecsg.frame.AmsException
    */
   public int lookupFieldNameInOrderList (HashSet hset, String lookUpValue, int warningCount)
          throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

         String attributeValue = this.getAttribute( lookUpValue + "_field_name" );

         if ( StringFunction.isNotBlank( attributeValue ) ) {

            if( hset.add( lookUpValue ) ) {

                if(warningCount == 0)
                 {
                   this.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1,
			                                       TradePortalConstants.MISSING_PO_FIELDS );
                   return warningCount + 1;
                 }
            }
         }
        return warningCount;
   }


   /**
    * This method makes a call to get the int value of all the attributes' sizes
    * and chnges the value from an int to a String for returned display.
    *
    * This method can probablybe removed if we do this on the WebBean.
    *
    * @return java.lang.String
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public String getLineLengthString()
                throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        String lineLength = "";
        return lineLength.valueOf( getLineLengthInteger() );
   }

   /**
    * This method loops through all the attributes looking for the associated size
    * and adds up the returned integer values.  The total is returned for comparison.
    *
    * @return int AttributeLength
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public int getLineLengthInteger()
                throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        int length = 0;
        int attributeCnt = 0;

        for( int x = 1; x <= NUMBER_OF_GOODS_DESCR_ORDER_FIELDS; x++ ) {
               //because we need to add a space between each selection we
               //need to keep track of the number of attributes that have a value.
               if( StringFunction.isNotBlank(this.getAttribute("goods_descr_order_" + x)) )
                    attributeCnt += 1;

               length += getFieldSize( "goods_descr_order_" + x , false);
        }
        //Now that we have the count of attributes that have a populated value,
        //we now know how mant spaces to account for between attributes -
        //The number of spaces = # of attributes populated - 1
        if( attributeCnt != 0 )
            attributeCnt -= 1;
      return (length + attributeCnt);
      
   }


   /**
    * This method does the work of getting the actual field size for a passed in
    * attribute name.  Basically we are passing in a "goods_desc_order_##" which
    * has a value like: 'po_num'  : ie some attribute that was defined on the
    * General Data Definition tab.  Once we have the actual attribute name that
    * was selected by the user, we then can look up what the associated field size
    * is...If at any time the value is null, we return a 0, otherwise we return the
    * stored size for the associated attribute.
    *
    * @param String Goods_Desc_Order_value_##   - This is the initial attribute name.
    * @return int Attribute Size
    * @exception java.rmi.RemoteException
    * @exception com.amsinc.ecsg.frame.AmsException
    */
   public int getFieldSize (String value, boolean onlyFiedSizeInculded)
                throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        String  attribute = this.getAttribute( value );
        String  attributeSize = "";
        String  attributeFieldName = "";

        Integer iSize = new Integer(0);

        try{
        	if( StringFunction.isNotBlank( attribute ) ) {
        		attributeSize = getAttribute( attribute + "_size" );
        		attributeFieldName = getAttribute( attribute + "_field_name" );
        	}
        	if( StringFunction.isNotBlank( attributeSize ) ) {
        		iSize = new Integer( attributeSize );
        	}

        	// If the user has chosen to include a column header, the
        	// space taken up by the data will be the greater of the field size
        	// and the size of the field name (which will be in the header)
        	if(!onlyFiedSizeInculded){
        		if(this.getAttribute("include_column_header").equals(TradePortalConstants.INDICATOR_YES))
        		{        			
        			if( StringFunction.isNotBlank( attributeFieldName ) &&
        					attributeFieldName.length() > iSize.intValue()    ) {

        				iSize = new Integer( attributeFieldName.length() );
        				headerLengthExceeds = true;	         
        			}
        		}
        	}

        }catch(NumberFormatException e) {   //should never get here...
        	LOG.error("NumberFormatException at getFieldSize(): ",e);
        }       
        return iSize.intValue();
   }
   
   /*
    * Rel 9.3.5. IR-44154 This method is used to get the size of all fields, irrespective of the 
   	  field name length.
   */
   public int getFieldSizeLength()
		   throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	   int length = 0;
	   int attributeCnt = 0;

	   for( int x = 1; x <= NUMBER_OF_GOODS_DESCR_ORDER_FIELDS; x++ ) {	
		   if( StringFunction.isNotBlank(this.getAttribute("goods_descr_order_" + x)) )
			   attributeCnt += 1;
		   length += getFieldSize( "goods_descr_order_" + x , true);
	   }

	   if( attributeCnt != 0 )
		   attributeCnt -= 1;

	   return (length + attributeCnt);
	   
   }

}
