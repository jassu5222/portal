
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Describes the mapping of an uploaded file to purchase order line items stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 * 
 * Can also be used to describe the process of manually entering purchase order
 * data.  The fields are used to define the fields that the user will enter,
 * the order in which they will appear and their format in goods description.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POUploadDefinitionBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(POUploadDefinitionBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* po_upload_definition_oid - Unique identifier */
      attributeMgr.registerAttribute("po_upload_definition_oid", "po_upload_definition_oid", "ObjectIDAttribute");
      
      /* name - Name of the PO Upload Definition.  This will be used in dropdown lists when
         choosing a definition. */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("POUploadDefinitionBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
      
      /* description - Description of the definition for convenience. */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("POUploadDefinitionBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));
      
      /* file_format - Indicates which file format to use.  Examples of file formats are fixed
         length and delimited.  Not used if this definition is being used for manual
         PO entry */
      attributeMgr.registerReferenceAttribute("file_format", "file_format", "PO_UPLOAD_FILE_FORMAT");
      
      /* delimiter_char - Only relevant if using a delimited file format.   This attribute indicates
         what character to use as the delimiter between fields in the uploaded file. */
      attributeMgr.registerReferenceAttribute("delimiter_char", "delimiter_char", "DELIMITER_CHAR");
      
      /* include_column_header - Indicates whether or not column headers should be included when the goods
         description is formatted. */
      attributeMgr.registerAttribute("include_column_header", "include_column_header", "IndicatorAttribute");
      
      /* include_footer - Indicates whether or not the footer should be included when the goods description
         is formatted. */
      attributeMgr.registerAttribute("include_footer", "include_footer", "IndicatorAttribute");
      
      /* footer_line_1 - Line 1 of the footer */
      attributeMgr.registerAttribute("footer_line_1", "footer_line_1");
      
      /* footer_line_2 - Line 2 of the footer */
      attributeMgr.registerAttribute("footer_line_2", "footer_line_2");
      
      /* footer_line_3 - Line 3 of the footer */
      attributeMgr.registerAttribute("footer_line_3", "footer_line_3");
      
      /* po_num_field_name - The user-defined description of the PO number field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("po_num_field_name", "po_num_field_name");
      attributeMgr.requiredAttribute("po_num_field_name");
      attributeMgr.registerAlias("po_num_field_name", getResourceManager().getText("POUploadDefinitionBeanAlias.po_num_field_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* po_num_size - Size for PO number field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("po_num_size", "po_num_size", "NumberAttribute");
      attributeMgr.requiredAttribute("po_num_size");
      attributeMgr.registerAlias("po_num_size", getResourceManager().getText("POUploadDefinitionBeanAlias.po_num_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* po_num_datatype - The datatype of the PO number field */
      attributeMgr.registerReferenceAttribute("po_num_datatype", "po_num_datatype", "PO_FIELD_DATA_TYPE");
      attributeMgr.requiredAttribute("po_num_datatype");
      
      /* item_num_field_name - The user-defined description of the Item number field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("item_num_field_name", "item_num_field_name");
      
      /* item_num_size - Size for  Item Number field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("item_num_size", "item_num_size", "NumberAttribute");
      attributeMgr.registerAlias("item_num_size", getResourceManager().getText("POUploadDefinitionBeanAlias.item_num_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* item_num_datatype - The datatype of the item number field */
      attributeMgr.registerReferenceAttribute("item_num_datatype", "item_num_datatype", "PO_FIELD_DATA_TYPE");
      
      /* ben_name_field_name - The user-defined description of the beneficiary name field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("ben_name_field_name", "ben_name_field_name");
      attributeMgr.registerAlias("ben_name_field_name", getResourceManager().getText("POUploadDefinitionBeanAlias.ben_name_field_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* ben_name_size - Size for beneficiary name field.   If the user is uploading a fixed length
         file, this will be the fixed length for the field.  If the user is uploading
         a delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("ben_name_size", "ben_name_size", "NumberAttribute");
      attributeMgr.registerAlias("ben_name_size", getResourceManager().getText("POUploadDefinitionBeanAlias.ben_name_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* ben_name_datatype - The datatype of the beneficiary name field */
      attributeMgr.registerReferenceAttribute("ben_name_datatype", "ben_name_datatype", "PO_FIELD_DATA_TYPE");
      
      /* currency_field_name - The user-defined description of the currency field.   This description will
         be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("currency_field_name", "currency_field_name");
      attributeMgr.registerAlias("currency_field_name", getResourceManager().getText("POUploadDefinitionBeanAlias.currency_field_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* currency_size - Size for currency field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("currency_size", "currency_size", "NumberAttribute");
      attributeMgr.registerAlias("currency_size", getResourceManager().getText("POUploadDefinitionBeanAlias.currency_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* currency_datatype - The datatype of the currency field */
      attributeMgr.registerReferenceAttribute("currency_datatype", "currency_datatype", "PO_FIELD_DATA_TYPE");
      
      /* amount_field_name - The user-defined description of the amount field.   This description will
         be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("amount_field_name", "amount_field_name");
      attributeMgr.registerAlias("amount_field_name", getResourceManager().getText("POUploadDefinitionBeanAlias.amount_field_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* amount_size - Size for user amount field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("amount_size", "amount_size", "NumberAttribute");
      attributeMgr.registerAlias("amount_size", getResourceManager().getText("POUploadDefinitionBeanAlias.amount_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* amount_datatype - The datatype of the amount field */
      attributeMgr.registerReferenceAttribute("amount_datatype", "amount_datatype", "PO_FIELD_DATA_TYPE");
      
      /* last_ship_dt_field_name - The user-defined description of the latest shipment date field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("last_ship_dt_field_name", "last_ship_dt_field_name");
      
      /* last_ship_dt_size - Size for last shipment date field.   If the user is uploading a fixed length
         file, this will be the fixed length for the field.  If the user is uploading
         a delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("last_ship_dt_size", "last_ship_dt_size", "NumberAttribute");
      attributeMgr.registerAlias("last_ship_dt_size", getResourceManager().getText("POUploadDefinitionBeanAlias.last_ship_dt_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* last_ship_dt_datatype - The datatype of the latest shipment date field */
      attributeMgr.registerReferenceAttribute("last_ship_dt_datatype", "last_ship_dt_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other1_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other1_field_name", "other1_field_name");
      
      /* other1_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other1_size", "other1_size", "NumberAttribute");
      attributeMgr.registerAlias("other1_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other1_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other1_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other1_datatype", "other1_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other2_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other2_field_name", "other2_field_name");
      
      /* other2_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other2_size", "other2_size", "NumberAttribute");
      attributeMgr.registerAlias("other2_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other2_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other2_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other2_datatype", "other2_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other3_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other3_field_name", "other3_field_name");
      
      /* other3_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other3_size", "other3_size", "NumberAttribute");
      attributeMgr.registerAlias("other3_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other3_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other3_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other3_datatype", "other3_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other4_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other4_field_name", "other4_field_name");
      
      /* other4_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other4_size", "other4_size", "NumberAttribute");
      attributeMgr.registerAlias("other4_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other4_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other4_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other4_datatype", "other4_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other5_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other5_field_name", "other5_field_name");
      
      /* other5_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other5_size", "other5_size", "NumberAttribute");
      attributeMgr.registerAlias("other5_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other5_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other5_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other5_datatype", "other5_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other6_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other6_field_name", "other6_field_name");
      
      /* other6_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other6_size", "other6_size", "NumberAttribute");
      attributeMgr.registerAlias("other6_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other6_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other6_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other6_datatype", "other6_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other7_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other7_field_name", "other7_field_name");
      
      /* other7_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other7_size", "other7_size", "NumberAttribute");
      attributeMgr.registerAlias("other7_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other7_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other7_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other7_datatype", "other7_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other8_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other8_field_name", "other8_field_name");
      
      /* other8_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other8_size", "other8_size", "NumberAttribute");
      attributeMgr.registerAlias("other8_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other8_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other8_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other8_datatype", "other8_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other9_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other9_field_name", "other9_field_name");
      
      /* other9_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other9_size", "other9_size", "NumberAttribute");
      attributeMgr.registerAlias("other9_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other9_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other9_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other9_datatype", "other9_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other10_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other10_field_name", "other10_field_name");
      
      /* other10_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other10_size", "other10_size", "NumberAttribute");
      attributeMgr.registerAlias("other10_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other10_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other10_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other10_datatype", "other10_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other11_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other11_field_name", "other11_field_name");
      
      /* other11_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other11_size", "other11_size", "NumberAttribute");
      attributeMgr.registerAlias("other11_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other11_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other11_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other11_datatype", "other11_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other12_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other12_field_name", "other12_field_name");
      
      /* other12_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other12_size", "other12_size", "NumberAttribute");
      attributeMgr.registerAlias("other12_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other12_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other12_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other12_datatype", "other12_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other13_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other13_field_name", "other13_field_name");
      
      /* other13_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other13_size", "other13_size", "NumberAttribute");
      attributeMgr.registerAlias("other13_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other13_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other13_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other13_datatype", "other13_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other14_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other14_field_name", "other14_field_name");
      
      /* other14_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other14_size", "other14_size", "NumberAttribute");
      attributeMgr.registerAlias("other14_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other14_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other14_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other14_datatype", "other14_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other15_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other15_field_name", "other15_field_name");
      
      /* other15_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other15_size", "other15_size", "NumberAttribute");
      attributeMgr.registerAlias("other15_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other15_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other15_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other15_datatype", "other15_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other16_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other16_field_name", "other16_field_name");
      
      /* other16_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other16_size", "other16_size", "NumberAttribute");
      attributeMgr.registerAlias("other16_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other16_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other16_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other16_datatype", "other16_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other17_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other17_field_name", "other17_field_name");
      
      /* other17_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other17_size", "other17_size", "NumberAttribute");
      attributeMgr.registerAlias("other17_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other17_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other17_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other17_datatype", "other17_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other18_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other18_field_name", "other18_field_name");
      
      /* other18_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other18_size", "other18_size", "NumberAttribute");
      attributeMgr.registerAlias("other18_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other18_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other18_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other18_datatype", "other18_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other19_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other19_field_name", "other19_field_name");
      
      /* other19_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other19_size", "other19_size", "NumberAttribute");
      attributeMgr.registerAlias("other19_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other19_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other19_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other19_datatype", "other19_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other20_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other20_field_name", "other20_field_name");
      
      /* other20_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other20_size", "other20_size", "NumberAttribute");
      attributeMgr.registerAlias("other20_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other20_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other20_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other20_datatype", "other20_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other21_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other21_field_name", "other21_field_name");
      
      /* other21_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other21_size", "other21_size", "NumberAttribute");
      attributeMgr.registerAlias("other21_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other21_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other21_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other21_datatype", "other21_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other22_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other22_field_name", "other22_field_name");
      
      /* other22_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other22_size", "other22_size", "NumberAttribute");
      attributeMgr.registerAlias("other22_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other22_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other22_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other22_datatype", "other22_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other23_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other23_field_name", "other23_field_name");
      
      /* other23_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other23_size", "other23_size", "NumberAttribute");
      attributeMgr.registerAlias("other23_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other23_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other23_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other23_datatype", "other23_datatype", "PO_FIELD_DATA_TYPE");
      
      /* other24_field_name - The user-defined description of the user defined field.   This description
         will be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("other24_field_name", "other24_field_name");
      
      /* other24_size - Size for user defined field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("other24_size", "other24_size", "NumberAttribute");
      attributeMgr.registerAlias("other24_size", getResourceManager().getText("POUploadDefinitionBeanAlias.other24_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* other24_datatype - The datatype of the user defined field */
      attributeMgr.registerReferenceAttribute("other24_datatype", "other24_datatype", "PO_FIELD_DATA_TYPE");
      
      /* upload_order_1 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_1", "upload_order_1", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_2 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_2", "upload_order_2", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_3 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_3", "upload_order_3", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_4 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_4", "upload_order_4", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_5 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_5", "upload_order_5", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_6 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_6", "upload_order_6", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_8 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_8", "upload_order_8", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_7 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_7", "upload_order_7", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_9 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_9", "upload_order_9", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_10 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_10", "upload_order_10", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_11 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_11", "upload_order_11", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_12 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_12", "upload_order_12", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_13 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_13", "upload_order_13", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_14 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_14", "upload_order_14", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_15 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_15", "upload_order_15", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_16 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_16", "upload_order_16", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_17 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_17", "upload_order_17", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_18 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_18", "upload_order_18", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_19 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_19", "upload_order_19", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_20 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_20", "upload_order_20", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_21 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_21", "upload_order_21", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_22 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_22", "upload_order_22", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_24 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_24", "upload_order_24", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_25 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_25", "upload_order_25", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_26 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_26", "upload_order_26", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_27 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_27", "upload_order_27", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_28 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_28", "upload_order_28", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_29 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_29", "upload_order_29", "PO_LINE_ITEM_FIELD");
      
      /* upload_order_30 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_30", "upload_order_30", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_1 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_1", "goods_descr_order_1", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_2 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_2", "goods_descr_order_2", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_3 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_3", "goods_descr_order_3", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_4 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_4", "goods_descr_order_4", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_5 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_5", "goods_descr_order_5", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_6 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_6", "goods_descr_order_6", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_7 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_7", "goods_descr_order_7", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_8 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_8", "goods_descr_order_8", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_9 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_9", "goods_descr_order_9", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_10 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_10", "goods_descr_order_10", "PO_LINE_ITEM_FIELD");
      
      /* default_flag - Set to Yes if this is the default PO upload definition for the corporate
         organization. */
      attributeMgr.registerAttribute("default_flag", "default_flag", "IndicatorAttribute");
      
      /* goods_descr_order_11 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_11", "goods_descr_order_11", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_12 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_12", "goods_descr_order_12", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_13 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_13", "goods_descr_order_13", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_14 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_14", "goods_descr_order_14", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_15 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_15", "goods_descr_order_15", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_16 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_16", "goods_descr_order_16", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_17 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_17", "goods_descr_order_17", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_18 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_18", "goods_descr_order_18", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_19 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_19", "goods_descr_order_19", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_20 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_20", "goods_descr_order_20", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_21 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_21", "goods_descr_order_21", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_22 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_22", "goods_descr_order_22", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_23 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_23", "goods_descr_order_23", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_24 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_24", "goods_descr_order_24", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_25 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_25", "goods_descr_order_25", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_26 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_26", "goods_descr_order_26", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_27 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_27", "goods_descr_order_27", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_28 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_28", "goods_descr_order_28", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_29 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_29", "goods_descr_order_29", "PO_LINE_ITEM_FIELD");
      
      /* goods_descr_order_30 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will appear in this position in the goods description text when it is formatted. */
      attributeMgr.registerReferenceAttribute("goods_descr_order_30", "goods_descr_order_30", "PO_LINE_ITEM_FIELD");
      
      /* part_to_validate - This is a  local attribute, meaning that it is not stored in the database.
         It stores the tab that the user is editing on the PO Upload Definition detail
         page.   Based on what tab is being edited, different validations occur. */
      attributeMgr.registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* upload_order_23 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_23", "upload_order_23", "PO_LINE_ITEM_FIELD");
      
      /* po_text_datatype - The datatype of the po text field */
      attributeMgr.registerReferenceAttribute("po_text_datatype", "po_text_datatype", "PO_FIELD_DATA_TYPE");
      
      /* po_text_field_name - The user-defined description of the PO Text field.   This description will
         be used whenever this field is refered to by the user. */
      attributeMgr.registerAttribute("po_text_field_name", "po_text_field_name");
      attributeMgr.registerAlias("po_text_field_name", getResourceManager().getText("POUploadDefinitionBeanAlias.po_text_field_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* po_text_size - Size for PO Text field.   If the user is uploading a fixed length file,
         this will be the fixed length for the field.  If the user is uploading a
         delimited file, this is the maximum length of the data in the field. */
      attributeMgr.registerAttribute("po_text_size", "po_text_size", "NumberAttribute");
      attributeMgr.registerAlias("po_text_size", getResourceManager().getText("POUploadDefinitionBeanAlias.po_text_size", TradePortalConstants.TEXT_BUNDLE));
      
      /* date_format - Indicates the format that will be used for uploading and manually entering
         dates. */
      attributeMgr.registerReferenceAttribute("date_format", "date_format", "PO_DATE_FORMAT");
      attributeMgr.requiredAttribute("date_format");
      attributeMgr.registerAlias("date_format", getResourceManager().getText("POUploadDefinitionBeanAlias.date_format", TradePortalConstants.TEXT_BUNDLE));
      
      /* include_po_text - Indicates whether or not PO Text should be included in the goods description. */
      attributeMgr.registerAttribute("include_po_text", "include_po_text", "IndicatorAttribute");
      
      /* definition_type - Indicates whether this PO definition is used for manual entry, upload or
         both. */
      attributeMgr.registerReferenceAttribute("definition_type", "definition_type", "PO_DEFINITION_TYPE");
      attributeMgr.requiredAttribute("definition_type");
      attributeMgr.registerAlias("definition_type", getResourceManager().getText("POUploadDefinitionBeanAlias.definition_type", TradePortalConstants.TEXT_BUNDLE));
      
      /* upload_order_31 - Points to a PO line item data attribute that was defined on the first tab
         of the PO Upload Definition page.   The data for the attribute pointed to
         will be retrieved from the uploaded file at this position in the uploaded
         file. */
      attributeMgr.registerReferenceAttribute("upload_order_31", "upload_order_31", "PO_LINE_ITEM_FIELD");
      
      /* owner_org_oid - A PO upload definition is "owned" by an organization.  This component relationship
         represents the ownership. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
      
   }
   
 
   
 
 
   
}
