package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;



/*
 *
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationGroupBean extends PanelAuthorizationGroupBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationGroupBean.class);
	protected void userValidate() throws AmsException, RemoteException
	{
		//Validate provided group id is unique or not. If not unique return error.  
		String panelAuthGroupId = getAttribute("panel_auth_group_id");
		String InstrumentType=getAttribute("instrument_group_type");
		String InstrumentPaymentType=getAttribute("payment_type");
		//Following condition will raise a warning message if panelauthgroup has edited  
		if(!this.isNew){
			this.checkUserDeleteInstruments(getAttribute("panel_auth_group_oid"));
		}
		
  		if (!isUnique("panel_auth_group_id", panelAuthGroupId,
		    " and p_corp_org_oid = " + getAttribute("corp_org_oid")))
		{
		    
  			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		           TradePortalConstants.ALREADY_EXISTS, panelAuthGroupId, attributeMgr.getAlias("panel_auth_group_id"));
		}
  		
  		
  		if(InstrumentType.equals("PYMTS") && InstrumentPaymentType.equals("PYMT") && (
				getAttribute("ACH_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("ACH_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO) 
				&& getAttribute("BCHK_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("BKT_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO)
				&& getAttribute("CBFT_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("CCHK_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO)
				&& getAttribute("RTGS_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("FTBA_pymt_method_ind").equals(TradePortalConstants.INDICATOR_NO))){
			
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_INSTRUMENT_TYPE_CHECK);	
			
		}
		else if(InstrumentType.equals("RECV") &&(
				getAttribute("credit_auth_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("inv_auth_ind").equals(TradePortalConstants.INDICATOR_NO) 
				&& getAttribute("matchin_approval_ind").equals(TradePortalConstants.INDICATOR_NO))){
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_INSTRUMENT_TYPE_CHECK);
		}
		else if(InstrumentType.equals("TRADE") && (
						getAttribute("AIR_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("ATP_ind").equals(TradePortalConstants.INDICATOR_NO )
						&& getAttribute("EXP_COL_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("EXP_DLC_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("EXP_OCO_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("IMP_DLC_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("LRQ_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("GUA_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("SLC_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("RQA_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("SHP_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("SP_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("DCR_ind").equals(TradePortalConstants.INDICATOR_NO) && getAttribute("import_col_ind").equals(TradePortalConstants.INDICATOR_NO)
						&& getAttribute("settlement_instr_ind").equals(TradePortalConstants.INDICATOR_NO))){
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_INSTRUMENT_TYPE_CHECK);
		}
		else if(InstrumentType.equals("PAYB") && (TradePortalConstants.INDICATOR_NO.equals(getAttribute("pay_mgm_inv_auth_ind")) 
				&& TradePortalConstants.INDICATOR_NO.equals(getAttribute("pay_upl_inv_auth_ind"))
				&& TradePortalConstants.INDICATOR_NO.equals(getAttribute("pay_credit_note_auth_ind"))) ){ 
			
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_INSTRUMENT_TYPE_CHECK);	
			
		}

		this.validatePanelAuthRangeValues();
		/*this.validatePanelAuthRules();*/
		
		//jgadela IR - Rel 8.3  T36000020977 - [START] Doing touch() to update the opt_lock when components are updated.
		touch();
		//jgadela IR - Rel 8.3  T36000020977 - [END]
		
	}

	
	private void validatePanelAuthRangeValues() throws RemoteException, AmsException
	{
			
		
		List<PanelAuthorizationRange> panelAuthRangeList=new ArrayList<PanelAuthorizationRange>();
		if(!StringFunction.isBlank(this.getAttribute("panel_range_1_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_1"));
		if(!StringFunction.isBlank(this.getAttribute("panel_range_2_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_2"));
		if(!StringFunction.isBlank(this.getAttribute("panel_range_3_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_3"));
		if(!StringFunction.isBlank(this.getAttribute("panel_range_4_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_4"));
		if(!StringFunction.isBlank(this.getAttribute("panel_range_5_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_5"));
		if(!StringFunction.isBlank(this.getAttribute("panel_range_6_oid")))
			panelAuthRangeList.add((PanelAuthorizationRange)this.getComponentHandle("panel_range_6"));
		
		
			int numOfPanelAuthRangeBeanList = panelAuthRangeList.size();
			if(numOfPanelAuthRangeBeanList!=0){
					BigDecimal minAmount = null;
					BigDecimal maxAmount = null;
				
					//List panelAuthRanges = new ArrayList();
				
					for (int i = 0; i < numOfPanelAuthRangeBeanList; i++) {
						
						minAmount = panelAuthRangeList.get(i).getAttributeDecimal("panel_auth_range_min_amt");
						maxAmount = panelAuthRangeList.get(i).getAttributeDecimal("panel_auth_range_max_amt");
						//Compare check
						if((minAmount.compareTo(maxAmount)) == 1) {
							this.getErrorManager().issueError(
									  TradePortalConstants.ERR_CAT_1,
									  TradePortalConstants.PANEL_AMOUNT_RANGE_CHECK);
						}
		
						
					}
					if(numOfPanelAuthRangeBeanList>=2){
					this.validatePanelAuthRangeSequence(panelAuthRangeList);
					}
			}else{
				this.getErrorManager().issueError(
						  TradePortalConstants.ERR_CAT_1,
						  TradePortalConstants.PANEL_AUTH_COMBINATION);
			}
			
	}

	private void validatePanelAuthRangeSequence(List<PanelAuthorizationRange> panelAuthRangeList) throws RemoteException, AmsException {
		Collections.sort(panelAuthRangeList, new Comparator() {

			public int compare(Object obj1, Object obj2) {
				BigDecimal panelMinAmount1 = BigDecimal.ZERO;
				BigDecimal panelMinAmount2 = BigDecimal.ZERO;

				try {
					panelMinAmount1 = ((PanelAuthorizationRange) obj1).getAttributeDecimal("panel_auth_range_min_amt");
					panelMinAmount2 = ((PanelAuthorizationRange) obj2).getAttributeDecimal("panel_auth_range_min_amt");

				} catch(AmsException | RemoteException ex) {
					LOG.error("Exception caught in validatePanelAuthRangeSequence(): ",ex);
				} 
				return panelMinAmount1.compareTo(panelMinAmount2);

		}
		});

		PanelAuthorizationRange firsPanelAuthObj = null;
		PanelAuthorizationRange secondPanelAuthObj = null;

		for(int i = 0; i < panelAuthRangeList.size(); i++) {
			if(firsPanelAuthObj == null) {
				firsPanelAuthObj = (PanelAuthorizationRange)panelAuthRangeList.get(i);
			} else {
				secondPanelAuthObj = (PanelAuthorizationRange)panelAuthRangeList.get(i);

				if(firsPanelAuthObj.getAttributeDecimal("panel_auth_range_max_amt").compareTo(
						secondPanelAuthObj.getAttributeDecimal("panel_auth_range_min_amt")) >= 0) {

					this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		                    TradePortalConstants.PANEL_AMOUNT_RANGE_NOT_IN_SEQUENCE);
				}
				firsPanelAuthObj = secondPanelAuthObj;
			}

		}
	}
	
	
	public void userDelete()
		throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		
		//SSikhakolli - Rel-8.3 SysTest IR#T36000021156 on 10/03/2013 - Begin
		String myOid = this.getAttribute("panel_auth_group_oid");
		String corp_org_oid = this.getAttribute("corp_org_oid");
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		String sql = "select account_oid, account_name from account where P_OWNER_OID = ? ";
		
		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{corp_org_oid});
		Vector accountList = null;
		DocumentHandler accountDoc = null;
		long accountOid = 0;
		int panelGroupUseCount = 0;
		
		if( result != null){
			StringBuffer whereClause = null;
			accountDoc = new DocumentHandler();
			accountList = result.getFragments("/ResultSetRecord");
			for (int i=0; i<accountList.size(); i++) {
				//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			    accountDoc = (DocumentHandler) accountList.elementAt(i);
			    accountOid = accountDoc.getAttributeLong("/ACCOUNT_OID");
			    List<Object> sqlParams = new ArrayList();
			    whereClause = new StringBuffer();
			    sqlParams.add(accountOid);
			    for(int j=1; j<=16; j++){
			    	sqlParams.add(myOid);
			    }
			    whereClause.append(" account_oid = ? ");
			    whereClause.append(" and ( ");
			    whereClause.append(" a_panel_auth_group_oid_1 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_2 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_3 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_4 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_5 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_6 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_7 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_8 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_9 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_10 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_11 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_12 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_13 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_14 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_15 in (?) ");
			    whereClause.append(" or ");
			    whereClause.append(" a_panel_auth_group_oid_16 in (?) ");
			    whereClause.append(" ) ");
			    
			    panelGroupUseCount = DatabaseQueryBean.getCount("*", "ACCOUNT", whereClause.toString(), false, sqlParams);
			    
				if(panelGroupUseCount > 0){
					LOG.debug("*** Error: ");
					this.getErrorManager().issueError(
							  TradePortalConstants.ERR_CAT_1,
							  TradePortalConstants.PANEL_AUTH_GROUP_CANNOT_DELETE);
					return;
				}
			}
		}
		//SSikhakolli - Rel-8.3 SysTest IR#T36000021156 on 10/03/2013 - End
		
		LOG.debug("[PanelAuthorizationGroupBean.userDelete()] Deleting Panel Authorization Group with oid: {}" , myOid);
		// Find the related Account oids
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		sql = "select account_oid, account_name from account where a_panel_auth_group_oid = ? ";
		
		result = DatabaseQueryBean.getXmlResultSet(sql, true , new Object[]{myOid});
		
		if( result != null)
		{
			LOG.debug("[PanelAuthorizationGroupBean.userDelete()] Result of SQL is: {}" , result.toString());

		    Account relatedAccount;
		    accountDoc = new DocumentHandler();
			accountList = result.getFragments("/ResultSetRecord");
		    // Loop through each of the Account oids in the document, get an instance
		    // of that Account and reset the panel authorization group oid to nothing.
		    for (int i=0; i<accountList.size(); i++) {
			    accountDoc = (DocumentHandler) accountList.elementAt(i);
			    try {
			    	accountOid = accountDoc.getAttributeLong("/ACCOUNT_OID");
			    	LOG.debug("[PanelAuthorizationGroupBean.userDelete()]: Loading Account with oid = {}",accountOid);
			    	relatedAccount = (Account)createServerEJB("Account", accountOid);
			    	
				    relatedAccount.setAttribute("panel_auth_group_oid", "");				    
				    
				    relatedAccount.save();
				    relatedAccount = null;
				    
				    LOG.debug("[PanelAuthorizationGroupBean.userDelete()] Blanked out panel_auth_group_oid for Account with oid = {}",accountOid);
			    } catch (AmsException e) {
				    LOG.error("[PanelAuthorizationGroupBean.userDelete()] AmsException trying to clear account.panel_auth_group_oid: "  , e);
			    } catch (Exception e) {
			    	LOG.error("[PanelAuthorizationGroupBean.userDelete()] Exception trying to clear account.panel_auth_group_oid: " ,e);
			    }
		    } // for (int i=0; i<accountList.size(); i++) {
	     } // if( result != null)
		
		this.checkUserDeleteInstruments(myOid);
	} // public void userDelete()


	private void checkUserDeleteInstruments(String myOid) throws AmsException, RemoteException {
		String whereClause=" a_panel_auth_group_oid = ?";
		
		int panelassignCount=0;
		int panelassignCountInvOffGroup=0;
		int panelassignCountPayRemit=0;
		int panelassignCountInvSummData=0;
		int panelassignCountInvGroup=0;
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		panelassignCount = DatabaseQueryBean.getCount("A_PANEL_AUTH_GROUP_OID", "transaction", whereClause , false, new Object[]{myOid});
		if(panelassignCount==0){
			panelassignCountInvOffGroup = DatabaseQueryBean.getCount("A_PANEL_AUTH_GROUP_OID", "INVOICE_OFFER_GROUP", whereClause , false, new Object[]{myOid});
			if(panelassignCountInvOffGroup==0){
				panelassignCountPayRemit = DatabaseQueryBean.getCount("A_PANEL_AUTH_GROUP_OID", "PAY_REMIT", whereClause , false, new Object[]{myOid});
				if(panelassignCountPayRemit==0){
					panelassignCountInvSummData = DatabaseQueryBean.getCount("A_PANEL_AUTH_GROUP_OID", "INVOICES_SUMMARY_DATA", whereClause , false, new Object[]{myOid});
					if(panelassignCountInvSummData==0){
						panelassignCountInvGroup = DatabaseQueryBean.getCount("A_PANEL_AUTH_GROUP_OID", "INVOICE_GROUP", whereClause , false, new Object[]{myOid});
					}
				}
			}
			
		}
		
		if(panelassignCount>0 || panelassignCountInvOffGroup>0 ||panelassignCountPayRemit>0){
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_GROUP_DELETE_ASSIGNED_INSTRUMENTS);
		}
		if(panelassignCountInvSummData>0 || panelassignCountInvGroup>0) {
			this.getErrorManager().issueError(
					  TradePortalConstants.ERR_CAT_1,
					  TradePortalConstants.PANEL_GROUP_DELETE_ASSIGNED_INVOICES);
		}
	}
	
	//jgadela IR - Rel 8.3  T36000020977 - [START] - Added this method to update the opt_lock when components are updated.	
	public void touch() throws RemoteException, AmsException {
		String oplock = getAttribute("opt_lock");
		setAttribute("opt_lock","");
		setAttribute("opt_lock",oplock);
	}
	//jgadela IR - Rel 8.3  T36000020977 - [END] 
	
	
}
