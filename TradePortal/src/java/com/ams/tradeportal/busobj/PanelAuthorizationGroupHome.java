
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A PanelAuthorizationGroup consists of a set of PalenAuthorizationRanges
 * to specifiy the requirements of panel authorization.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PanelAuthorizationGroupHome extends EJBHome
{
   public PanelAuthorizationGroup create()
      throws RemoteException, CreateException, AmsException;

   public PanelAuthorizationGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
