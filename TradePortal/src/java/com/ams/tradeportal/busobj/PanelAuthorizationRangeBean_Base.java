


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * "Panel authorization" allows thresholds of authorization to be set at the
 * corporation level for each payment instrument (not trade instruments).
 * When a payment instrument goes through the authorization process, if panel
 * authorization is selected for the corporation, the system will use the type
 * and amount of the payment instrument and look at the corresponding level
 * (threshold) to determine the total number of authorizers and the profile
 * / panel level that the user must have (based on the new Panel Authority
 * -  A, B or C - present on the authorizing user's profile).
 *
 *     CGI, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationRangeBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationRangeBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* panel_auth_range_oid - Unique identifier */
      attributeMgr.registerAttribute("panel_auth_range_oid", "panel_auth_range_oid", "ObjectIDAttribute");

      
      /* panel_auth_range_min_amt - The minimum amount of a Panel Authorization range of amounts. */
      attributeMgr.registerAttribute("panel_auth_range_min_amt", "panel_auth_range_min_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("panel_auth_range_min_amt", getResourceManager().getText("CorpCustAlias.panel_auth_range_min_amt", TradePortalConstants.TEXT_BUNDLE));

      /* panel_auth_range_max_amt - The maximum amount of a Panel Authorization range of amounts.  The last
         field in the last range can be left empty (or it may be determined in the
         development process that a large number such as 999,999,999,999.999 should
         be entered or an "*" or some other symbol representing 'infinity' should
         be entered if no upper limit is entered. */
      attributeMgr.registerAttribute("panel_auth_range_max_amt", "panel_auth_range_max_amt", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("panel_auth_range_max_amt", getResourceManager().getText("CorpCustAlias.panel_auth_range_max_amt", TradePortalConstants.TEXT_BUNDLE));

      //attributeMgr.registerAttribute("panel_auth_group_oid", "p_panel_auth_group_oid", "ParentIDAttribute");
      //

   }
   
   
   /*
    * Register the components of the business object
    */
    protected void registerComponents() throws RemoteException, AmsException
    {
       /* Register the components defined in the Ancestor class */
       super.registerComponents();

       /* PanelAuthorizationRuleList -  */
       registerOneToManyComponent("PanelAuthorizationRuleList","PanelAuthorizationRuleList");
    }






}
