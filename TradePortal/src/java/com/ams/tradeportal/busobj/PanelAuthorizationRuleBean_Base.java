


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 * 
 *
 *     
 *     CGI, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizationRuleBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationRuleBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      
      attributeMgr.registerAttribute("panel_auth_rule_oid", "panel_auth_rule_oid", "ObjectIDAttribute");
      
      /*PanelAuthorizationRange ParentIDAttribute*/
      /*attributeMgr.registerAttribute("panel_auth_range_oid", "p_panel_auth_range_oid", "ParentIDAttribute");*/
      
      /*Approver sequence Reference Attribute*/
      attributeMgr.registerReferenceAttribute("approver_sequence", "approver_sequence", "PANEL_APPROVE_SEQUENCE");

      /* Approver Level Reference Attribute*/
      attributeMgr.registerReferenceAttribute("approver_1", "approver_1", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_2", "approver_2", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_3", "approver_3", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_4", "approver_4", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_5", "approver_5", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_6", "approver_6", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_7", "approver_7", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_8", "approver_8", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_9", "approver_9", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_10", "approver_10", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_11", "approver_11", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_12", "approver_12", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_13", "approver_13", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_14", "approver_14", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_15", "approver_15", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_16", "approver_16", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_17", "approver_17", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_18", "approver_18", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_19", "approver_19", "PANEL_AUTH_TYPE");
      attributeMgr.registerReferenceAttribute("approver_20", "approver_20", "PANEL_AUTH_TYPE");
      
      
      attributeMgr.registerAttribute("panel_auth_range_oid", "p_panel_auth_range_oid", "ParentIDAttribute");
   }






}
