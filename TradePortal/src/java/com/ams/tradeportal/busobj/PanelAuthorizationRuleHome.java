
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * "Panel authorization" allows thresholds of authorization to be set at the
 * corporation level for each payment instrument (not trade instruments). 
 * When a payment instrument goes through the authorization process, if panel
 * authorization is selected for the corporation, the system will use the type
 * and amount of the payment instrument and look at the corresponding level
 * (threshold) to determine the total number of authorizers and the profile
 * / panel level that the user must have (based on the new Panel Authority
 * -  A, B or C - present on the authorizing user's profile). 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PanelAuthorizationRuleHome extends EJBHome
{
   public PanelAuthorizationRule create()
      throws RemoteException, CreateException, AmsException;

   public PanelAuthorizationRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
