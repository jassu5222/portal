package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
/*
 * A PanelAuthorization consists of a set of authorize users
 *
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PanelAuthorizerBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizerBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* panel_authorizer_oid - Unique identifier */
      attributeMgr.registerAttribute("panel_authorizer_oid", "panel_authorizer_oid", "ObjectIDAttribute");
      
      /* panel_authority_code - The level at which the reference data is owned (Proponix, client bank, bank
      organization group, or corporate customer */
      attributeMgr.registerReferenceAttribute("panel_code", "panel_code", "PANEL_AUTH_TYPE");
      
      /* user_oid - The user who authorize the transaction. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /*authorize_status_date - Timestamp (in GMT) of when authorization took place on this transaction. */
      attributeMgr.registerAttribute("auth_date_time", "auth_date_time", "DateTimeAttribute");
            
      /* owner_object_type - the type of object that is the owner of this user */
      attributeMgr.registerReferenceAttribute("owner_object_type", "owner_object_type", "PANEL_AUTH_OWNER_TYPE");
   
     /* Pointer to the parent ReferenceDataOwner */
     attributeMgr.registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");
            
   }










}
