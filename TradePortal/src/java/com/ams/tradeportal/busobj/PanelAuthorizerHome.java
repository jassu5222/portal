
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A PanelAuthorizer consists of a set of Authorize users
 * of transaction.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PanelAuthorizerHome extends EJBHome
{
   public PanelAuthorizer create()
      throws RemoteException, CreateException, AmsException;

   public PanelAuthorizer create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
