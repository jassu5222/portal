package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PartyBean extends PartyBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PartyBean.class);
   
/**
* Performs any special "validate" processing that is specific to the
* descendant of BusinessObject.
* Show Warning message if Branch Code is entered for a Party Type of Bank
*/
public void userValidate()
	throws RemoteException, AmsException {

	String partyType = this.getAttribute("party_type_code");
	String branchCode = this.getAttribute("branch_code");

	if ((partyType.equals(TradePortalConstants.PARTY_TYPE_CORP)) && !(branchCode.equals(""))) {
			this.getErrorManager().issueError(
							  TradePortalConstants.ERR_CAT_1, 
							  TradePortalConstants.PARTY_TYPE_BANK_BRANCH_CODE); 
	}
	validateCityStateZip();

}
// End userValidate

	/**
	  * When saving a Party, validate combination of City , State, Postal
	  * Code
	  */
	public void validateCityStateZip()
	   throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		int countTotalNoOfChars = 0;
		String city = this.getAttribute("address_city");
		if (city != null)
			countTotalNoOfChars += city.length();
		String state = this.getAttribute("address_state_province");
		if (state != null)
			countTotalNoOfChars += state.length();
		String postalcode = this.getAttribute("address_postal_code");
		if (postalcode != null)
			countTotalNoOfChars += postalcode.length();
		/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - Start */
		//Validate total no. of characters
		if (countTotalNoOfChars > TradePortalConstants.CITY_STATE_ZIP_PARTYBEAN_LENGTH)
		{
			this.errMgr.issueError(TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.TOO_MANY_CHARS_IN_COMB_PARTYBEAN);
		}
		/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - Start */
	}


/* 
 * When deleting a party, any other parties that have that party as the
 * designated bank should have that field reset to nothing.
 */
public void userDelete()
	throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	String myOid = this.getAttribute("party_oid");
	LOG.debug("the oid is {}" , myOid);
   //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	String sql = "select party_oid, name from party where a_designated_bank_oid = ?";
	DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{myOid} );
	
	if( result != null)
	 {
	    LOG.debug("  Result of sql is: {}" , result.toString());

	    Party relatedParty;
	    relatedParty = (Party)createServerEJB("Party");
	    Vector partyList = result.getFragments("/ResultSetRecord");
	    DocumentHandler partyDoc = new DocumentHandler();
	    long partyOid = 0l;

	    // Loop through each of the party oids in the document, get an instance
	    // of that party and reset the designated bank oid to nothing.
	    for (int i=0; i<partyList.size(); i++) {
		    partyDoc = (DocumentHandler) partyList.elementAt(i);
		    try {
			    partyOid = partyDoc.getAttributeLong("/PARTY_OID");
			    relatedParty.getData(partyOid);
			    relatedParty.setAttribute("designated_bank_oid", "");
			    relatedParty.save();
		    } catch (AmsException e) {
			    LOG.error("AmsException trying to remove designated party: {}" ,e);
		    } catch (Exception e) {
			    LOG.error("Exception trying to remove designated party: {}",e);
		    }
	    }
     }
	

}


//CR-486 05/24/10  - Begin
/* 
 * Add pre-save logic here
 * 
 */
protected void preSave () throws AmsException
{
	super.preSave();
	
	// check if name and address fields have unicode character(s) and set the unicode_indicator accordingly 

	try  {
		if (StringFunction.canEncodeIn_WIN1252(getAttribute("name")) && 
			StringFunction.canEncodeIn_WIN1252(getAttribute("address_line_1")) && 
			StringFunction.canEncodeIn_WIN1252(getAttribute("address_line_2")) &&
			StringFunction.canEncodeIn_WIN1252(getAttribute("address_city")) && 
			StringFunction.canEncodeIn_WIN1252(getAttribute("address_state_province"))) {
			
			   setAttribute("unicode_indicator",TradePortalConstants.INDICATOR_NO);
		}
		else {
			   setAttribute("unicode_indicator",TradePortalConstants.INDICATOR_YES);
		}

	}
	catch (RemoteException ex) {
		throw new AmsException(ex.getMessage());
	}
}
//CR-486 05/24/10  - End

}
