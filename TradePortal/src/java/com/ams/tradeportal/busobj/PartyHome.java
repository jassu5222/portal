package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A business (corporate or bank) that will be a party to an instrument in
 * the portal.    This business object is part of reference data.   The TermsParty
 * business object contains parties that are actually a part of instruments.
 * 
 * When a party is added to an instrument, the data for the party is copied
 * out of the Party business object and into the TermsParty business object.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PartyHome extends EJBHome
{
   public Party create()
      throws RemoteException, CreateException, AmsException;

   public Party create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
