
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Used to keep track of the passwords that have been used by a user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PasswordHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PasswordHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* password_history_oid - Object identifier */
      attributeMgr.registerAttribute("password_history_oid", "password_history_oid", "ObjectIDAttribute");
      
      /* creation_timestamp - The date and time (stored in the database in the GMT timezone) when the
         password was changed. */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* password - A password entered by the user. */
      attributeMgr.registerAttribute("password", "password");
      attributeMgr.registerAlias("password", getResourceManager().getText("PasswordHistoryBeanAlias.password", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent User */
      attributeMgr.registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
