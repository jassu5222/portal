
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayMatchResultBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PayMatchResultBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* pay_match_result_oid - Unique Identifier */
      attributeMgr.registerAttribute("pay_match_result_oid", "pay_match_result_oid", "ObjectIDAttribute");
      
      /* invoice_matching_status - The status of the matched invoice. A-automatched, M-Manually Matched, P-Partial
         Match, U-Unmatched - an association to the Matched Results Type table. 
         THESE SEEM TO CORRELATE TO THE NEW PORTAL matching_status_type - PPD (proposed),
         AMD (automatched), MMD (manual match), NMD- Not matched */
      attributeMgr.registerReferenceAttribute("invoice_matching_status", "invoice_matching_status", "MATCHING_STATUS_TYPE");
      
      /* invoice_paid_status - The status can be pending/applied/none */
      attributeMgr.registerAttribute("invoice_paid_status", "invoice_paid_status");
      
      /* match_explanation - The explanation of the matching
 */
      attributeMgr.registerAttribute("match_explanation", "match_explanation");
      
      /* matched_amount - The amount that was matched */
      attributeMgr.registerAttribute("matched_amount", "matched_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* 
       * IR#T36000018260 - save the discount amount applied
       * matched_amount - The amount that was matched */
      attributeMgr.registerAttribute("discount_applied", "discount_applied", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");      
      
      /* manual_indicator - Whether the Invoice is manually picked by TP user for matching.  
         
         Y = The invoice is manually picked by TP user for matching.
         N = The invoice is proposed by OTL for matching. */
      attributeMgr.registerAttribute("manual_indicator", "manual_indicator", "IndicatorAttribute");
      
      /* otl_invoice_uoid - OTL Uoid of the Invoice being matched. */
      attributeMgr.registerAttribute("otl_invoice_uoid", "otl_invoice_uoid");
      
      /* invoice_reference_id - Invoice Reference ID of the invoice being matched. */
      attributeMgr.registerAttribute("invoice_reference_id", "invoice_reference_id");
      
      /* otl_pay_remit_invoice_uoid - OTL Uoid of the Pay Remit Invoice being matched. */
      attributeMgr.registerAttribute("otl_pay_remit_invoice_uoid", "otl_pay_remit_invoice_uoid");
      
      /* invoice_outstanding_amount - The outstanding amount of the invoice at the time of matching on TP.  This
         data will be sent to OTL and in case the outstanding amount is changed on
         invoice and matched_amount > Outsdanding amount on invoice, the match will
         be returned to TP for re-match.  This could happen if this invoice is matched
         to another payment while the current payment is being matched in TP. */
      attributeMgr.registerAttribute("invoice_outstanding_amount", "invoice_outstanding_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
        /* Pointer to the parent PayRemit */
      attributeMgr.registerAttribute("pay_remit_oid", "p_pay_remit_oid", "ParentIDAttribute");
      
      /* IR 22112 - pending_matched_amount - copy of The amount that was matched from TPS*/
      attributeMgr.registerAttribute("pending_matched_amount", "pending_matched_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
   
      /* pay_remit_invoice_oid -  */
      attributeMgr.registerAssociation("pay_remit_invoice_oid", "a_pay_remit_invoice_oid", "PayRemitInvoice");
      
      /* invoice_oid -  */
      attributeMgr.registerAssociation("invoice_oid", "a_invoice_oid", "Invoice");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
   }
   
 
   
 
 
   
}
