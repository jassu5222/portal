
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PayMatchResultHome extends EJBHome
{
   public PayMatchResult create()
      throws RemoteException, CreateException, AmsException;

   public PayMatchResult create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
