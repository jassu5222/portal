
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayRemitBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PayRemitBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* pay_remit_oid - Unique Identifier */
      attributeMgr.registerAttribute("pay_remit_oid", "pay_remit_oid", "ObjectIDAttribute");
      
      /* payment_source_type - The source of the payment data that is being sent to Proponix360 by the
         bank */
      attributeMgr.registerReferenceAttribute("payment_source_type", "payment_source_type", "PAYMENT_SOURCE_TYPE");
      
      /* currency_code -  */
      attributeMgr.registerReferenceAttribute("currency_code", "currency_code", "CURRENCY_CODE");
      
      /* received_data_status -  */
      attributeMgr.registerAttribute("received_data_status", "received_data_status");
      
      /* payment_datetime - The date/time the payment data is inserted into the OTL databse record when
         an incoming payremit message is received with a MessageSubType = PAYMENT
         or PAYREM. */
      attributeMgr.registerAttribute("payment_datetime", "payment_datetime", "DateTimeAttribute");
      
      /* remittance_datetime - The date/time the remittance data is inserted into OTL database when an
         incoming payremit message is received with a MessageSubType = REMIT or PAYREM.
         Required when MessageSubType = REMIT or PAYREM. */
      attributeMgr.registerAttribute("remittance_datetime", "remittance_datetime", "DateTimeAttribute");
      
      /* payment_value_date - Specifies the value date of the payment. */
      attributeMgr.registerAttribute("payment_value_date", "payment_value_date", "DateAttribute");
      
      /* payment_amount - The amount received in the payment.  */
      attributeMgr.registerAttribute("payment_amount", "payment_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* total_invoice_amount - Total amount of all line item amounts from the remittance data/message.
 */
      attributeMgr.registerAttribute("total_invoice_amount", "total_invoice_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* total_number_of_invoices - Specifies the total number of invoices covered by this payment/remittance
         (message) */
      attributeMgr.registerAttribute("total_number_of_invoices", "total_number_of_invoices", "NumberAttribute");
      
      /* seller_name - Specifies the name of the seller as it came in on the remittance notice */
      attributeMgr.registerAttribute("seller_name", "seller_name");
      
      /* seller_contact_name - Specifies a contact name at the Seller�s as it came in on the remittance
         notice */
      attributeMgr.registerAttribute("seller_contact_name", "seller_contact_name");
      
      /* seller_address - Specifies the Seller�s address as it came in on the remittance notice */
      attributeMgr.registerAttribute("seller_address", "seller_address");
      
      /* seller_reference - Specifies a reference that the Seller uses internally in his own system
         to reference this item as it came in on the remittance notice */
      attributeMgr.registerAttribute("seller_reference", "seller_reference");
      
      /* seller_bank_details - The account number the seller maintains at the receiving (PPX) bank as it
         came in on the remittance notice */
      attributeMgr.registerAttribute("seller_bank_details", "seller_bank_details");
      
      /* seller_id_for_buyer - Seller's ID for the Buyer  - this data could have been enriched from seller/buyer
         instructions. */
      attributeMgr.registerAttribute("seller_id_for_buyer", "seller_id_for_buyer");
      
      /* seller_name_for_buyer - Seller's Name for the Buyer - this data could have been enriched from seller/buyer
         instructions . */
      attributeMgr.registerAttribute("seller_name_for_buyer", "seller_name_for_buyer");
      
      /* buyer_customer_id - Specifies the identifier representing the buyer customer for the payment
         source */
      attributeMgr.registerAttribute("buyer_customer_id", "buyer_customer_id");
      
      /* buyer_name - Specifies the name of the Buyer. */
      attributeMgr.registerAttribute("buyer_name", "buyer_name");
      
      /* buyer_contact_name - Specifies a contact name at the Buyer�s
 */
      attributeMgr.registerAttribute("buyer_contact_name", "buyer_contact_name");
      
      /* buyer_address - Specifies the Buyer�s address */
      attributeMgr.registerAttribute("buyer_address", "buyer_address");
      
      /* buyer_reference - Specifies a reference that the Buyer uses internally in his own system to
         reference this item
 */
      attributeMgr.registerAttribute("buyer_reference", "buyer_reference");
      
      /* payment_invoice_reference_id - This represents the invoice number from the payment. (seller's ref) */
      attributeMgr.registerAttribute("payment_invoice_reference_id", "payment_invoice_reference_id");
      
      /* buyer_invoice_reference_id - This specifies/contains the invoice id that the payment is covering. This
         is often received in a narrative text field in the incoming payment advice.(Buyer's
         ref) */
      attributeMgr.registerAttribute("buyer_invoice_reference_id", "buyer_invoice_reference_id");
      
      /* narrative - This specifies/contains the invoice id that the payment is covering. This
         is often received in a narrative text field in the incoming payment advice.(tex) */
      attributeMgr.registerAttribute("narrative", "narrative");
      
      /* tracer_line - Reference to be used to trace the transaction back through the bank�s systems. */
      attributeMgr.registerAttribute("tracer_line", "tracer_line");
      
      /* cheque_reference - If payment is received by cheque, this specifies the cheque number. */
      attributeMgr.registerAttribute("cheque_reference", "cheque_reference");
      
      /* unapplied_payment_amount - Returned from TP to OTL if all the payment could not be applied. There will
         be no matching results for this amount. */
      attributeMgr.registerAttribute("unapplied_payment_amount", "unapplied_payment_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* transaction_status - These are the portal transaction stauses that exist today, with some new
         ones (MATCH_REQUIRED, DISC_APPROVAL, IN_PROGRESS, DISC_APPR_FAILED) */
      attributeMgr.registerReferenceAttribute("transaction_status", "transaction_status", "TRANSACTION_STATUS");
      
      /* discount_indicator - An indication that one of the matched invoices has been discounted and thus
         an additional level of approval is required by a portal user (YES/NO) */
      attributeMgr.registerAttribute("discount_indicator", "discount_indicator", "IndicatorAttribute");
      
      /* remit_source_type - The source of the remit data that is being sent to Proponix360 by the bank */
      attributeMgr.registerReferenceAttribute("remit_source_type", "remit_source_type", "PAYMENT_SOURCE_TYPE");
      
      /* first_authorize_status_date - Timestamp (in GMT) of when the first authorization took place on this PayRemit.
         Depending on the corporate customer's settings, this may be the only authorization
         that takes places on this Pay Remit. */
      attributeMgr.registerAttribute("first_authorize_status_date", "first_authorize_status_date", "DateTimeAttribute");
      
      /* second_authorize_status_date - Timestamp (in GMT) of when the second authorization took place on this transaction.
         Depending on the corporate customer's settings, only one authorization may
         be necessary. */
      attributeMgr.registerAttribute("second_authorize_status_date", "second_authorize_status_date", "DateTimeAttribute");
      
      /* transaction_status_date - Timestamp (in GMT) of the last time the status of this Pay Remit was updated */
      attributeMgr.registerAttribute("transaction_status_date", "transaction_status_date", "DateTimeAttribute");
      
      /* otl_pay_remit_uoid - OTL Uoid. */
      attributeMgr.registerAttribute("otl_pay_remit_uoid", "otl_pay_remit_uoid");
      
      /* buyer_added_in_portal_indicator - Whether the buyer information is added/updated in TP. */
      attributeMgr.registerAttribute("buyer_added_in_portal_indicator", "BUYER_ADDED_IN_PORTAL_IND", "IndicatorAttribute");
      
      /* receipt_datetime - Timestamp (in GMT) when this Pay Remit is received in TP. */
      attributeMgr.registerAttribute("receipt_datetime", "receipt_datetime", "DateTimeAttribute");
      
        /* Pointer to the parent Transaction */
      attributeMgr.registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
   
      /* seller_corp_org_oid - The Seller */
      attributeMgr.registerAssociation("seller_corp_org_oid", "a_seller_corp_org_oid", "CorporateOrganization");
      
      /* instrument_oid - The associated instrument. */
      attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
      
      /* last_entry_user_oid - The last user to enter data and save it for a pay remit. */
      attributeMgr.registerAssociation("last_entry_user_oid", "a_last_entry_user_oid", "User");
      
      /* assigned_to_user_oid - The user to which a pay remit is currently assigned. */
      attributeMgr.registerAssociation("assigned_to_user_oid", "a_assigned_to_user_oid", "User");
      
      /* first_authorizing_user_oid - The user that performed the first authorization on a PayRemit.  Depending
         on the corporate customer settings, this may be the only user to perform
         an authorize on this PayRemit. */
      attributeMgr.registerAssociation("first_authorizing_user_oid", "a_first_authorizing_user_oid", "User");
      
      /* second_authorizing_user_oid - The user that performed the second authorization on a PayRemit.  Depending
         on the corporate customer settings, this may be no second user to authorize
         this PayRemit. */
      attributeMgr.registerAssociation("second_authorizing_user_oid", "a_second_authorizing_user_oid", "User");
      
      /* first_authorizing_work_group_oid - The work group of the first user that performed the authorization on a PayRemit.
         Depending on the corporate customer settings, this user may be the only
         user to perform an authorize on this PayRemit. */
      attributeMgr.registerAssociation("first_authorizing_work_group_oid", "a_first_auth_work_group_oid", "WorkGroup");
      
      /* second_authorizing_work_group_oid - The work group of the second user that performed the authorization on a
         PayRemit.  Depending on the corporate customer settings, this may be no
         second user to authorize this PayRemit. */
      attributeMgr.registerAssociation("second_authorizing_work_group_oid", "a_second_auth_work_group_oid", "WorkGroup");
      
     // Pavani Rel 8.3 CR 821 Start
      /* panel_auth_group_oid - Panel group associated to a PayRemit. */
      attributeMgr.registerAssociation("panel_auth_group_oid", "a_panel_auth_group_oid", "PanelAuthorizationGroup");
      
      /* panel_auth_range_oid - Panel Range Oid to a pay remit. */
      attributeMgr.registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
      
      /* opt_lock - Optimistic lock attribute*/
      attributeMgr.registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");

	  // Pavani Rel 8.3 CR 821 End
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* PayRemitInvoiceList -  */
      registerOneToManyComponent("PayRemitInvoiceList","PayRemitInvoiceList");

      /* PayMatchResultList -  */
      registerOneToManyComponent("PayMatchResultList","PayMatchResultList");
      
      /*DocumentImageList - */ //IValavala Added for CR742
      registerOneToManyComponent("DocumentImageList","DocumentImageList");
      
      // Nar release 8.3.0.0 CR 821 25 June 2013
      /* PanelAuthorizerList - The Invoice has many panel Authorizer Users.  */
      registerOneToManyComponent("PanelAuthorizerList","PanelAuthorizerList");
   }

 
   
 
 
   
}
