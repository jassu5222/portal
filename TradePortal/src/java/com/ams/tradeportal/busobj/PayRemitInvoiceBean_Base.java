
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayRemitInvoiceBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PayRemitInvoiceBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* pay_remit_invoice_oid - Unique Identifier */
      attributeMgr.registerAttribute("pay_remit_invoice_oid", "pay_remit_invoice_oid", "ObjectIDAttribute");
      
      /* invoice_reference_id - This represents the invoice number from the remittance line item (invoice). */
      attributeMgr.registerAttribute("invoice_reference_id", "invoice_reference_id");
      
      /* invoice_payment_amount - This represents the amount being paid on this invoice. It will be a negative
         amount if credit note ind = YES. */
      attributeMgr.registerAttribute("invoice_payment_amount", "invoice_payment_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* invoice_original_amount - Original amount of the invoice. */
      attributeMgr.registerAttribute("invoice_original_amount", "invoice_original_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* invoice_balanace_amount - Remaing amount tobe paid on this invoice.
 */
      attributeMgr.registerAttribute("invoice_balanace_amount", "invoice_balanace_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* invoice_date - Date of the invoice */
      attributeMgr.registerAttribute("invoice_date", "invoice_date", "DateAttribute");
      
      /* invoice_discount_amount - Amount of discount taken by the buyer. */
      attributeMgr.registerAttribute("invoice_discount_amount", "invoice_discount_amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* credit_note_indicator - Indicates if this invoice is a credit note.  (YES/NO). NOTE: any payment_remittance_invoice
         lines with the credit note indicator = Yes are ignored by the matching engine.
         The credit note-related fields are available for reporting only. */
      attributeMgr.registerAttribute("credit_note_indicator", "credit_note_indicator", "IndicatorAttribute");
      
      /* otl_pay_remit_invoice_uoid - OTL uoid. */
      attributeMgr.registerAttribute("otl_pay_remit_invoice_uoid", "otl_pay_remit_invoice_uoid");
     
      /* buyer_disc_comments */
      attributeMgr.registerAttribute("buyer_disc_comments", "buyer_disc_comments");
      
      /* buyer_disc_code  */
      attributeMgr.registerAttribute("buyer_disc_code", "buyer_disc_code");
      
        /* Pointer to the parent PayRemit */
      attributeMgr.registerAttribute("pay_remit_oid", "p_pay_remit_oid", "ParentIDAttribute");
   
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* PayRemitInvoicePoList -  */
      registerOneToManyComponent("PayRemitInvoicePoList","PayRemitInvoicePoList");
   }

 
   
 
 
   
}
