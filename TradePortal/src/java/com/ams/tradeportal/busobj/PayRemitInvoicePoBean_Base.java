
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayRemitInvoicePoBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PayRemitInvoicePoBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* pay_remit_invoice_po_oid - Unique Idenfier */
      attributeMgr.registerAttribute("pay_remit_invoice_po_oid", "pay_remit_invoice_po_oid", "ObjectIDAttribute");
      
      /* po_reference_id - Purchase Order Number.  Note if there is no purchase order reference, this
         should still be populated with something like �No PO Ref Provided� */
      attributeMgr.registerAttribute("po_reference_id", "po_reference_id");
      
      /* po_issue_date - Purchase Order Issue Date. */
      attributeMgr.registerAttribute("po_issue_date", "po_issue_date", "DateAttribute");
      
      /* po_type - This details the type of purchase order designation. */
      attributeMgr.registerAttribute("po_type", "po_type");
      
      /* po_description - This provide the description of the purchase order. */
      attributeMgr.registerAttribute("po_description", "po_description");
      
      /* seller_information_label1 - Seller defined data: Label1 */
      attributeMgr.registerAttribute("seller_information_label1", "seller_information_label1");
      
      /* seller_information_label2 - Seller defined data: Label2 */
      attributeMgr.registerAttribute("seller_information_label2", "seller_information_label2");
      
      /* seller_information_label3 - Seller defined data: Label3 */
      attributeMgr.registerAttribute("seller_information_label3", "seller_information_label3");
      
      /* seller_information_label4 - Seller defined data: Label4 */
      attributeMgr.registerAttribute("seller_information_label4", "seller_information_label4");
      
      /* seller_information_label5 - Seller defined data: Label5 */
      attributeMgr.registerAttribute("seller_information_label5", "seller_information_label5");
      
      /* seller_information_label6 - Seller defined data: Label6 */
      attributeMgr.registerAttribute("seller_information_label6", "seller_information_label6");
      
      /* seller_information_label7 - Seller defined data: Label7 */
      attributeMgr.registerAttribute("seller_information_label7", "seller_information_label7");
      
      /* seller_information_label8 - Seller defined data: Label8 */
      attributeMgr.registerAttribute("seller_information_label8", "seller_information_label8");
      
      /* seller_information_label9 - Seller defined data: Label9 */
      attributeMgr.registerAttribute("seller_information_label9", "seller_information_label9");
      
      /* seller_information_label10 - Seller defined data: Label10 */
      attributeMgr.registerAttribute("seller_information_label10", "seller_information_label10");
      
      /* seller_information1 - Seller defined data: Data Field1 */
      attributeMgr.registerAttribute("seller_information1", "seller_information1");
      
      /* seller_information2 - Seller defined data: Data Field2 */
      attributeMgr.registerAttribute("seller_information2", "seller_information2");
      
      /* seller_information3 - Seller defined data: Data Field3 */
      attributeMgr.registerAttribute("seller_information3", "seller_information3");
      
      /* seller_information4 - Seller defined data: Data Field4 */
      attributeMgr.registerAttribute("seller_information4", "seller_information4");
      
      /* seller_information5 - Seller defined data: Data Field5 */
      attributeMgr.registerAttribute("seller_information5", "seller_information5");
      
      /* seller_information6 - Seller defined data: Data Field6 */
      attributeMgr.registerAttribute("seller_information6", "seller_information6");
      
      /* seller_information7 - Seller defined data: Data Field7 */
      attributeMgr.registerAttribute("seller_information7", "seller_information7");
      
      /* seller_information8 - Seller defined data: Data Field8 */
      attributeMgr.registerAttribute("seller_information8", "seller_information8");
      
      /* seller_information9 - Seller defined data: Data Field9 */
      attributeMgr.registerAttribute("seller_information9", "seller_information9");
      
      /* seller_information10 - Seller defined data: Data Field10 */
      attributeMgr.registerAttribute("seller_information10", "seller_information10");
      
        /* Pointer to the parent PayRemitInvoice */
      attributeMgr.registerAttribute("pay_remit_invoice_oid", "p_pay_remit_invoice_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
