

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;

/*
 * The Payable Invoice Payment Instructions for Bank defined Trading Parter.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PayableInvPayInstBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PayableInvPayInstBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {
      /* payable_inv_pay_inst_oid  - Unique Identifier */
      attributeMgr.registerAttribute("payable_inv_pay_inst_oid", "payable_inv_pay_inst_oid", "ObjectIDAttribute");

      /* inv_pay_instruction_ind - Whether Payable invoice payment instruction exists in TPS for respective currency*/
      attributeMgr.registerAttribute("inv_pay_instruction_ind", "inv_pay_instruction_ind",  "IndicatorAttribute");
      
      /* currency_code - The currency for Payable invoice instruction in Trading Partner. */
      attributeMgr.registerReferenceAttribute("currency_code", "currency_code", "CURRENCY_CODE");
            
      /* Pointer to the parent ArMatchingRule */
      attributeMgr.registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");

   }






}
