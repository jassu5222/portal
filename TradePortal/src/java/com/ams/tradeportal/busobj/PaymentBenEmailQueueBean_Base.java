
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * Placing a row into this table will cause an e-mail to be sent for each beneficiaries
 * of the associated FTDP transaction. 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentBenEmailQueueBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentBenEmailQueueBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* payment_ben_email_queue_oid - Unique Identifier. */
      attributeMgr.registerAttribute("payment_ben_email_queue_oid", "payment_ben_email_queue_oid", "ObjectIDAttribute");
      
      /* creation_timestamp - The time the item was created. */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* status - The status of the item. */
      attributeMgr.registerReferenceAttribute("status", "status", "PAYMENT_BEN_EMAIL_STATUS");
      
      /* agent_id - The agent who is processing this item. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* required_status - The required payment_status of the domestic payment needed to send the email. */
      attributeMgr.registerReferenceAttribute("required_status", "required_status", "BENEFICIARY_PAYMENT_STATUS");
      
      /* transaction_oid - The id of the transaction to process with this request. */
      attributeMgr.registerAssociation("transaction_oid", "a_transaction_oid", "Transaction");
      
   }
   
 
   
 
 
   
}
