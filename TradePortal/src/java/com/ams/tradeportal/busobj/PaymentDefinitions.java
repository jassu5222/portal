package com.ams.tradeportal.busobj;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Describes the mapping of an uploaded file to Invoice Definition stored
 * in the database.   The fields contained in the file are described, the file
 * format is specified, and the goods description format is provided.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface PaymentDefinitions extends TradePortalBusinessObject {

}
