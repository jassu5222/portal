package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.util.regex.Matcher;
import com.ams.tradeportal.common.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */


public class PaymentDefinitionsBean extends PaymentDefinitionsBean_Base {
private static final Logger LOG = LoggerFactory.getLogger(PaymentDefinitionsBean.class);
	//Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9 ]*");// Just accept Alphanumeric

    Map sizeMap = new HashMap();

	   public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		    sizeMap.put("payment_method_size",4);
		    sizeMap.put("debit_account_number_size",30);
		    sizeMap.put("beneficiary_name_size",120);
		    sizeMap.put("bene_account_number_size",34);
		    sizeMap.put("bene_bank_branch_code_size",35);
		    sizeMap.put("payment_currency_size",3);
		    sizeMap.put("payment_amount_size",19);
		    sizeMap.put("customer_reference_size",20);
		    sizeMap.put("beneficiary_country_size",2);
		    sizeMap.put("execution_date_size",10);
		    sizeMap.put("bene_address1_size",35);
		    sizeMap.put("bene_address2_size",35);
		    sizeMap.put("bene_address3_size",35);
		    sizeMap.put("bene_address4_size",35);
		    sizeMap.put("bene_fax_no_size",15);
		    sizeMap.put("bene_email_id_size",255);
		    sizeMap.put("charges_size",3);
		    sizeMap.put("bene_bank_name_size",35);
		    sizeMap.put("bene_bnk_brnch_name_size",35);
		    sizeMap.put("bene_bnk_brnch_addr1_size",35);
		    sizeMap.put("bene_bnk_brnch_addr2_size",35);
		    sizeMap.put("bene_bnk_brnch_city_size",31);
		    sizeMap.put("bene_bnk_brnch_prvnc_size",8);
		    sizeMap.put("bene_bnk_brnch_cntry_size",2);
		    sizeMap.put("payable_location_size",20);
		    sizeMap.put("print_location_size",20);
		    sizeMap.put("delvry_mth_n_delvrto_size",2);
		    sizeMap.put("mailing_address1_size",35);
		    sizeMap.put("mailing_address2_size",35);
		    sizeMap.put("mailing_address3_size",35);
		    sizeMap.put("mailing_address4_size",35);
		    sizeMap.put("details_of_payment_size",140);
		    sizeMap.put("instruction_number_size",10);
		    sizeMap.put("f_int_bnk_brnch_cd_size",35);
		    sizeMap.put("f_int_bnk_name_size",35);
		    sizeMap.put("f_int_bnk_brnch_nm_size",35);
		    sizeMap.put("f_int_bnk_brnch_addr1_size",35);
		    sizeMap.put("f_int_bnk_brnch_addr2_size",35);
		    sizeMap.put("f_int_bnk_brnch_city_size",31);
		    sizeMap.put("f_int_bnk_brnch_prvnc_size",8);
		    sizeMap.put("f_int_bnk_brnch_cntry_size",2);
		    sizeMap.put("central_bank_rep1_size",35);
		    sizeMap.put("central_bank_rep2_size",35);
		    sizeMap.put("central_bank_rep3_size",35);
		    sizeMap.put("confidential_ind_size",1);
		    sizeMap.put("indv_acct_entry_ind_size",1);
		    sizeMap.put("header_identifier_size",3);
		    sizeMap.put("file_reference_size",35);
		    sizeMap.put("detail_identifier_size",3);
		    sizeMap.put("beneficiary_code_size",15);
		    sizeMap.put("reporting_code1_size",3);
		    sizeMap.put("reporting_code2_size",3);
		    sizeMap.put("inv_details_header_size",3);
		    sizeMap.put("inv_details_lineitm_size",80);
		    sizeMap.put("users_def1_size",35);
		    sizeMap.put("users_def2_size",35);
		    sizeMap.put("users_def3_size",35);
		    sizeMap.put("users_def4_size",35);
		    sizeMap.put("users_def5_size",35);
		    sizeMap.put("users_def6_size",35);
		    sizeMap.put("users_def7_size",35);
		    sizeMap.put("users_def8_size",35);
		    sizeMap.put("users_def9_size",35);
		    sizeMap.put("users_def10_size",35);
		    sizeMap.put("fx_contract_number_size",14); //MEer Rel 8.3  CR-921
		    sizeMap.put("fx_contract_rate_size",14);

		           validateGeneralTab ();

		  
    }
	 public void validateGeneralTab ()
       throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

      LOG.debug("========= Validating the General Tab data in the Payment Upload Definition Object ==========");
	     String name = getAttribute("name");
	     String ownerOrg = this.getAttribute("owner_org_oid");
	     Matcher m1; // DK IR T36000014746 Rel8.2 03/13/2013
	     Matcher m2; // DK IR T36000014746 Rel8.2 03/13/2013
	     
         if (StringFunction.isNotBlank(name)){
             if(!name.trim().matches("^[\\w\\-\\s]*$")){
		  			this.getErrorManager().issueError(
		  					TradePortalConstants.ERR_CAT_1,
		  					TradePortalConstants.MUST_BE_ALPHA_NUMERIC,
                            attributeMgr.getAlias("name"));

             }
         }
         String description = getAttribute("description");
         if (StringFunction.isNotBlank(description)){
             if(!description.trim().matches("^[\\w\\-\\s]*$")){
		  			this.getErrorManager().issueError(
		  					TradePortalConstants.ERR_CAT_1,
		  					TradePortalConstants.MUST_BE_ALPHA_NUMERIC,
                            attributeMgr.getAlias("description"));

             }
         }
         
	     //Make sure there is no other name in the db that matches this one.
	     if (!isUnique( "name", name, " and A_OWNER_ORG_OID=" + ownerOrg ))
	      {
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
		  }

          String fileFormatType = this.getAttribute("file_format_type");
          if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType) && StringFunction.isBlank(this.getAttribute("delimiter_char"))) {
              this.getErrorManager ().issueError(
                      getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
                      this.resMgr.getText("PaymentUploadRequest.DelimiterChar", TradePortalConstants.TEXT_BUNDLE));
          }
          
          if (TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)){
        	  validateSizeFields();
          }


         int dataWarningCount = 0;
         // Verifying if data required selected but type is not required.

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("customer_reference_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("customer_reference_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("beneficiary_country_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("beneficiary_country_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("execution_date_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("execution_date_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_address1_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_address1_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_address2_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_address2_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_address3_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_address3_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_address4_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_address4_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_fax_no_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_fax_no_req"))){

            		 dataWarningCount++;
            	 }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_email_id_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_email_id_req"))){

            		 dataWarningCount++;
            	 }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("charges_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("charges_req"))){

            		 dataWarningCount++;
            	 }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bank_name_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bank_name_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_name_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_name_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_addr1_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_addr1_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_addr2_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_addr2_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_city_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_city_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_prvnc_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_prvnc_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("bene_bnk_brnch_cntry_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("bene_bnk_brnch_cntry_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("payable_location_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("payable_location_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("print_location_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("print_location_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("delvry_mth_n_delvrto_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("delvry_mth_n_delvrto_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("mailing_address1_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("mailing_address1_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("mailing_address2_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("mailing_address2_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("mailing_address3_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("mailing_address3_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("mailing_address4_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("mailing_address4_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("details_of_payment_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("details_of_payment_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("instruction_number_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("instruction_number_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_cd_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_cd_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_name_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_name_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_nm_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_nm_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_addr1_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_addr1_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_addr2_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_addr2_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_city_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_city_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_prvnc_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_prvnc_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("f_int_bnk_brnch_cntry_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("f_int_bnk_brnch_cntry_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("central_bank_rep1_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("central_bank_rep1_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("central_bank_rep2_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("central_bank_rep2_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("central_bank_rep3_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("central_bank_rep3_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("confidential_ind_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("confidential_ind_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("header_identifier_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("header_identifier_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("file_reference_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("file_reference_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("detail_identifier_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("detail_identifier_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("beneficiary_code_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("beneficiary_code_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("reporting_code1_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("reporting_code1_req"))){

             dataWarningCount++;
         }

         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("reporting_code2_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("reporting_code2_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("inv_details_header_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("inv_details_header_req"))){

             dataWarningCount++;
         }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("inv_details_lineitm_data_req")) &&
                 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("inv_details_lineitm_req"))){

             dataWarningCount++;
         }

         for(int x = 1; x<=TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++ ){

	    	 if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("users_def" + x + "_data_req")) &&
	    			 (TradePortalConstants.INDICATOR_NO).equals( this.getAttribute("users_def" + x + "_req") )   ){
	    		     dataWarningCount++;
	    	 }

         }
         
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("fx_contract_number_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("fx_contract_number_req"))){

            		 dataWarningCount++;
            	 }
         if((TradePortalConstants.INDICATOR_YES).equals(this.getAttribute("fx_contract_rate_data_req")) &&
            	 (TradePortalConstants.INDICATOR_NO).equals(this.getAttribute("fx_contract_rate_req"))){

            		 dataWarningCount++;
            	 }


         if(dataWarningCount>0){
        	 this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVOICE_DEF_DATA_REQ_VALIDATION);
         }



         //if no error, then do clean up of file order data which has been unselected from definition tab.
        if(!(this.getErrorManager().getErrorCount() > 0)){
           cleanup(); // if user unselect required option, then need to clear file format order fields.
         //  autoLinkingInvDefinitionData(); // this method is used to link data, which are defined on invoice general definition tab, to file format order fields.
       }
	 }



    private void validateSizeFields() throws RemoteException, AmsException {
    	String maxFXRateSize = "5,8";
    	String maxPmtAmountSize = "15,3";
		for (Iterator iterator = sizeMap.keySet().iterator(); iterator.hasNext();) {
			String fieldName = (String) iterator.next();
			String fieldSize = String.valueOf(sizeMap.get(fieldName));
			String inputFieldSize = this.getAttribute(fieldName);
			if (StringFunction.isNotBlank(fieldSize) && StringFunction.isNotBlank(inputFieldSize)){
				int fdSize = Integer.parseInt(fieldSize);
				int ifdSize = Integer.parseInt(inputFieldSize);
				// Rel 9.0 IR-29133- show fieldSize in correct format for FXRate and payment amount
				if (fdSize < ifdSize){
					if("fx_contract_rate_size".equalsIgnoreCase(fieldName)){
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PAY_DEF_MAX_FIELD_SIZE_EXCEEDED,
								getResourceManager().getText("PaymentUploadRequest." + fieldName.substring(0,fieldName.indexOf("_size")), TradePortalConstants.TEXT_BUNDLE),
								maxFXRateSize );
					}
					if("payment_amount_size".equalsIgnoreCase(fieldName)){
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				                TradePortalConstants.PAY_DEF_MAX_FIELD_SIZE_EXCEEDED,
				                getResourceManager().getText("PaymentUploadRequest." + fieldName.substring(0,fieldName.indexOf("_size")), TradePortalConstants.TEXT_BUNDLE),
				                maxPmtAmountSize );
					}
					if(!(("fx_contract_rate_size".equalsIgnoreCase(fieldName)) || ("payment_amount_size".equalsIgnoreCase(fieldName)) )){
						this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PAY_DEF_MAX_FIELD_SIZE_EXCEEDED,
								getResourceManager().getText("PaymentUploadRequest." + fieldName.substring(0,fieldName.indexOf("_size")), TradePortalConstants.TEXT_BUNDLE),
								fieldSize );
					}
				}
				
			}
			
		}
		
	}
    

 /**
  *  This method is designed to clean up the invoice file format order if user deselect check box on general,
  *  then that value should not be present in file order tab. so it is used to clean up value in database befor saving.
  */
 public void cleanup() throws AmsException, RemoteException {

     HashSet hSummarySet = new HashSet();
     HashSet hHdrSummarySet = new HashSet();
     HashSet hPaySummarySet = new HashSet();
     HashSet hInvSummarySet = new HashSet();
     String uploadOrder = null;

     for (int x = 1; x <= TradePortalConstants.PMT_MTHD_SUMMARY_NUMBER_OF_FIELDS; x++) {
         uploadOrder = getAttribute("pmt_mthd_summary_order" + x);
         if (StringFunction.isNotBlank(uploadOrder))
             hSummarySet.add(uploadOrder);

     }

     for (int x = 1; x <= TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS; x++) {
         uploadOrder = getAttribute("pmt_hdr_summary_order" + x);
         if (StringFunction.isNotBlank(uploadOrder))
             hHdrSummarySet.add(uploadOrder);

     }
     for (int x = 1; x <= TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS; x++) {
         uploadOrder = getAttribute("pmt_pay_summary_order" + x);
         if (StringFunction.isNotBlank(uploadOrder))
             hPaySummarySet.add(uploadOrder);

     }
     for (int x = 1; x <= TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
         uploadOrder = getAttribute("pmt_inv_summary_order" + x);
         if (StringFunction.isNotBlank(uploadOrder))
             hInvSummarySet.add(uploadOrder);

     }
     // removing payment summary data item from order tab which has been
     // unselected
     removeNonRequiredFields(hSummarySet, "customer_reference_req", "customer_reference");
     removeNonRequiredFields(hSummarySet, "beneficiary_country_req", "beneficiary_country");
     removeNonRequiredFields(hSummarySet, "bene_account_number_req", "bene_account_number");
     removeNonRequiredFields(hSummarySet, "bene_bank_branch_code_req", "bene_bank_branch_code");
     removeNonRequiredFields(hSummarySet, "execution_date_req", "execution_date");
     removeNonRequiredFields(hSummarySet, "bene_address1_req", "bene_address1");
     removeNonRequiredFields(hSummarySet, "bene_address2_req", "bene_address2");
     removeNonRequiredFields(hSummarySet, "bene_address3_req", "bene_address3");
     removeNonRequiredFields(hSummarySet, "bene_address4_req", "bene_address4");
     removeNonRequiredFields(hSummarySet, "bene_fax_no_req", "bene_fax_no");
     removeNonRequiredFields(hSummarySet, "bene_email_id_req", "bene_email_id");
     removeNonRequiredFields(hSummarySet, "charges_req", "charges");
     removeNonRequiredFields(hSummarySet, "bene_bank_name_req", "bene_bank_name");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_name_req", "bene_bnk_brnch_name");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_addr1_req", "bene_bnk_brnch_addr1");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_addr2_req", "bene_bnk_brnch_addr2");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_city_req", "bene_bnk_brnch_city");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_prvnc_req", "bene_bnk_brnch_prvnc");
     removeNonRequiredFields(hSummarySet, "bene_bnk_brnch_cntry_req", "bene_bnk_brnch_cntry");
     removeNonRequiredFields(hSummarySet, "payable_location_req", "payable_location");
     removeNonRequiredFields(hSummarySet, "print_location_req", "print_location");
     removeNonRequiredFields(hSummarySet, "delvry_mth_n_delvrto_req", "delvry_mth_n_delvrto");
     removeNonRequiredFields(hSummarySet, "mailing_address1_req", "mailing_address1");
     removeNonRequiredFields(hSummarySet, "mailing_address2_req", "mailing_address2");
     removeNonRequiredFields(hSummarySet, "mailing_address3_req", "mailing_address3");
     removeNonRequiredFields(hSummarySet, "mailing_address4_req", "mailing_address4");
     removeNonRequiredFields(hSummarySet, "details_of_payment_req", "details_of_payment");
     removeNonRequiredFields(hSummarySet, "instruction_number_req", "instruction_number");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_cd_req", "f_int_bnk_brnch_cd");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_name_req", "f_int_bnk_name");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_nm_req", "f_int_bnk_brnch_nm");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_addr1_req", "f_int_bnk_brnch_addr1");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_addr2_req", "f_int_bnk_brnch_addr2");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_city_req", "f_int_bnk_brnch_city");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_city_req", "f_int_bnk_brnch_city");
     removeNonRequiredFields(hSummarySet, "f_int_bnk_brnch_prvnc_req", "f_int_bnk_brnch_prvnc");
     removeNonRequiredFields(hSummarySet, "central_bank_rep1_req", "central_bank_rep1");
     removeNonRequiredFields(hSummarySet, "central_bank_rep2_req", "central_bank_rep2");
     removeNonRequiredFields(hSummarySet, "central_bank_rep3_req", "central_bank_rep3");
     removeNonRequiredFields(hSummarySet, "confidential_ind_req", "confidential_ind");
     removeNonRequiredFields(hSummarySet, "reporting_code1_req", "reporting_code1");
     removeNonRequiredFields(hSummarySet, "reporting_code2_req", "reporting_code2");
     removeNonRequiredFields(hHdrSummarySet, "confidential_ind_req", "confidential_ind");
     removeNonRequiredFields(hHdrSummarySet, "indv_acct_entry_ind_req", "indv_acct_entry_ind");
//        removeNonRequiredFields(hHdrSummarySet,    "header_identifier_req", "header_identifier");
     removeNonRequiredFields(hHdrSummarySet, "file_reference_req", "file_reference");
//        removeNonRequiredFields(hPaySummarySet, "detail_identifier_req", "detail_identifier");
     removeNonRequiredFields(hPaySummarySet, "customer_reference_req", "customer_reference");
     removeNonRequiredFields(hPaySummarySet, "beneficiary_country_req", "beneficiary_country");
     removeNonRequiredFields(hPaySummarySet, "execution_date_req", "execution_date");
     removeNonRequiredFields(hPaySummarySet, "bene_address1_req", "bene_address1");
     removeNonRequiredFields(hPaySummarySet, "bene_address2_req", "bene_address2");
     removeNonRequiredFields(hPaySummarySet, "bene_address3_req", "bene_address3");
     removeNonRequiredFields(hPaySummarySet, "bene_address4_req", "bene_address4");
     removeNonRequiredFields(hPaySummarySet, "bene_fax_no_req", "bene_fax_no");
     removeNonRequiredFields(hPaySummarySet, "bene_email_id_req", "bene_email_id");
     removeNonRequiredFields(hPaySummarySet, "charges_req", "charges");
     removeNonRequiredFields(hPaySummarySet, "bene_bank_branch_code_req", "bene_bank_branch_code");
     removeNonRequiredFields(hPaySummarySet, "bene_bank_name_req", "bene_bank_name");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_name_req", "bene_bnk_brnch_name");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_addr1_req", "bene_bnk_brnch_addr1");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_addr2_req", "bene_bnk_brnch_addr2");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_city_req", "bene_bnk_brnch_city");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_prvnc_req", "bene_bnk_brnch_prvnc");
     removeNonRequiredFields(hPaySummarySet, "bene_bnk_brnch_cntry_req", "bene_bnk_brnch_cntry");
     removeNonRequiredFields(hPaySummarySet, "payable_location_req", "payable_location");
     removeNonRequiredFields(hPaySummarySet, "print_location_req", "print_location");
     removeNonRequiredFields(hPaySummarySet, "delvry_mth_n_delvrto_req", "delvry_mth_n_delvrto");
     removeNonRequiredFields(hPaySummarySet, "mailing_address1_req", "mailing_address1");
     removeNonRequiredFields(hPaySummarySet, "mailing_address2_req", "mailing_address2");
     removeNonRequiredFields(hPaySummarySet, "mailing_address3_req", "mailing_address3");
     removeNonRequiredFields(hPaySummarySet, "mailing_address4_req", "mailing_address4");
     removeNonRequiredFields(hPaySummarySet, "details_of_payment_req", "details_of_payment");
     removeNonRequiredFields(hPaySummarySet, "instruction_number_req", "instruction_number");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_cd_req", "f_int_bnk_brnch_cd");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_name_req", "f_int_bnk_name");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_nm_req", "f_int_bnk_brnch_nm");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_addr1_req", "f_int_bnk_brnch_addr1");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_addr2_req", "f_int_bnk_brnch_addr2");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_city_req", "f_int_bnk_brnch_city");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_cntry_req", "f_int_bnk_brnch_cntry");
     removeNonRequiredFields(hPaySummarySet, "f_int_bnk_brnch_prvnc_req", "f_int_bnk_brnch_prvnc");
     removeNonRequiredFields(hPaySummarySet, "central_bank_rep1_req", "central_bank_rep1");
     removeNonRequiredFields(hPaySummarySet, "central_bank_rep2_req", "central_bank_rep2");
     removeNonRequiredFields(hPaySummarySet, "central_bank_rep3_req", "central_bank_rep3");
     removeNonRequiredFields(hPaySummarySet, "beneficiary_code_req", "beneficiary_code");
     removeNonRequiredFields(hPaySummarySet, "reporting_code1_req", "reporting_code1");
     removeNonRequiredFields(hPaySummarySet, "reporting_code2_req", "reporting_code2");
     for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
         removeNonRequiredFields(hPaySummarySet, "users_def" + x + "_req", "users_def" + x);
     }
     removeNonRequiredFields(hInvSummarySet, "inv_details_header_req", "inv_details_header");
     removeNonRequiredFields(hInvSummarySet, "inv_details_lineitm_req", "inv_details_lineitm");
     removeNonRequiredFields(hSummarySet, "fx_contract_number_req", "fx_contract_number");
     removeNonRequiredFields(hSummarySet, "fx_contract_rate_req", "fx_contract_rate");
     removeNonRequiredFields(hHdrSummarySet, "fx_contract_number_req", "fx_contract_number");
     removeNonRequiredFields(hHdrSummarySet, "fx_contract_rate_req", "fx_contract_rate");


     //checking for invoice summary data file order  . remove definiiton data from file format which has been unchecked in definition tab.
     Hashtable attributesHash = new Hashtable(TradePortalConstants.PMT_MTHD_SUMMARY_NUMBER_OF_FIELDS);
     int key = 1;
     String attUploadOrder;
     for (int x = 1; x <= TradePortalConstants.PMT_MTHD_SUMMARY_NUMBER_OF_FIELDS; x++) {
         attUploadOrder = this.getAttribute("pmt_mthd_summary_order" + x);
         if (StringFunction.isNotBlank(attUploadOrder) && hSummarySet.contains(attUploadOrder) && (hSummarySet.size() >= (x))) {//Added condition Rel 8.2 T36000011727
             attributesHash.put(new Integer(key++), attUploadOrder);
         }
     }

     //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
     for (int x = 1; x <= TradePortalConstants.PMT_MTHD_SUMMARY_NUMBER_OF_FIELDS; x++) {
         this.setAttribute("pmt_mthd_summary_order" + x, (String) attributesHash.get(new Integer(x)));
     }

     attributesHash = new Hashtable(TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS);
     key = 1;
     for (int x = 1; x <= TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS; x++) {
         attUploadOrder = this.getAttribute("pmt_hdr_summary_order" + x);
         if (StringFunction.isNotBlank(attUploadOrder) && hHdrSummarySet.contains(attUploadOrder) && (hHdrSummarySet.size() >= (x))) {//Added condition Rel 8.2 T36000011727
             attributesHash.put(new Integer(key++), attUploadOrder);
         }
     }
     //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
     for (int x = 1; x <= TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS; x++) {
         this.setAttribute("pmt_hdr_summary_order" + x, (String) attributesHash.get(new Integer(x)));
     }

     attributesHash = new Hashtable(TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS);
     key = 1;
     for (int x = 1; x <= TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS; x++) {
         attUploadOrder = this.getAttribute("pmt_pay_summary_order" + x);
         if (StringFunction.isNotBlank(attUploadOrder) && hPaySummarySet.contains(attUploadOrder) && (hPaySummarySet.size() >= (x))) {//Added condition Rel 8.2 T36000011727
             attributesHash.put(new Integer(key++), attUploadOrder);
         }
     }
     //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
     for (int x = 1; x <= TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS; x++) {
         this.setAttribute("pmt_pay_summary_order" + x, (String) attributesHash.get(new Integer(x)));
     }

     attributesHash = new Hashtable(TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS);
     key = 1;
     for (int x = 1; x <= TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
         attUploadOrder = this.getAttribute("pmt_inv_summary_order" + x);
         if (StringFunction.isNotBlank(attUploadOrder) && hInvSummarySet.contains(attUploadOrder) && (hInvSummarySet.size() >= (x))) {//Added condition Rel 8.2 T36000011727
             attributesHash.put(new Integer(key++), attUploadOrder);
         }
     }
     //Now loop through the data in the attribute list - hashtable and re-put them into the bus Obj.
     for (int x = 1; x <= TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS; x++) {
         this.setAttribute("pmt_inv_summary_order" + x, (String) attributesHash.get(new Integer(x)));
     }

 }

 /*
  * @param hset
  * @param fieldName
  * @param lookUpValue
  * @throws java.rmi.RemoteException @throws com.amsinc.ecsg.frame.AmsException
  */
 protected void removeNonRequiredFields(HashSet hset, String fieldName,
			String lookUpValue) throws java.rmi.RemoteException,
			com.amsinc.ecsg.frame.AmsException {

		if (!TradePortalConstants.INDICATOR_YES.equals(getAttribute(fieldName))) {
			hset.remove(lookUpValue);
		}

	}


}
