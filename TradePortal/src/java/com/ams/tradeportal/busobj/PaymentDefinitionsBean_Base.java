package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;

public class PaymentDefinitionsBean_Base extends TradePortalBusinessObjectBean {
private static final Logger LOG = LoggerFactory.getLogger(PaymentDefinitionsBean_Base.class);

	 
	/*
     * Describes the mapping of an uploaded file to Invoice definiton.
	 * The fields contained in the file are described, the file
	 * format is specified.
	 * 
	 * Can also be used to describe order of Invoice Summary and line Item detail data
	 *     Copyright  � 2003                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */

    /*
     * Register the attributes and associations of the business object
     */
    protected void registerAttributes() throws AmsException {

	      /* Register attributes defined in the Ancestor class */
        super.registerAttributes();
	      
	      /* inv_upload_definition_oid - Unique identifier */
        attributeMgr.registerAttribute("payment_definition_oid", "payment_definition_oid", "ObjectIDAttribute");
	      
	      /* name - Name of the Invoice Definition Definition.  This will be used in dropdown lists when
	         choosing a definition. */
        attributeMgr.registerAttribute("name", "name");
        attributeMgr.requiredAttribute("name");
        attributeMgr.registerAlias("name", getResourceManager().getText("PaymentDefinitions.name", TradePortalConstants.TEXT_BUNDLE));
	      
	      /* description - Description of the definition for convenience. */
        attributeMgr.registerAttribute("description", "description");
        attributeMgr.requiredAttribute("description");
        attributeMgr.registerAlias("description", getResourceManager().getText("PaymentDefinitions.Description", TradePortalConstants.TEXT_BUNDLE));
	      
	      /* delimiter_char - Only relevant if using a delimited file format.   This attribute indicates
	         what character to use as the delimiter,comma, semi-colon,tab between fields in the uploaded file. */
        attributeMgr.registerReferenceAttribute("delimiter_char", "delimiter_char", "INVOICE_DELIMITER_CHAR");
//        attributeMgr.requiredAttribute("delimiter_char");
        attributeMgr.registerAlias("delimiter_char", getResourceManager().getText("PaymentDefinitions.DelimiterChar", TradePortalConstants.TEXT_BUNDLE));
	      
	      /* date_format - describe the date format to be used for Invoice file upload. */
        attributeMgr.registerReferenceAttribute("date_format", "date_format", "PAYMENT_DATE_FORMAT");
        attributeMgr.requiredAttribute("date_format");
        attributeMgr.registerAlias("date_format", getResourceManager().getText("PaymentDefinitions.DateFormat", TradePortalConstants.TEXT_BUNDLE));
	      
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
        attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
	      
        // Nar IR-NNUM022937252 03/24/2012 Begin
	      /* include_column_headers - Indicates whether Invoice file will have column headings. */
        attributeMgr.registerAttribute("include_column_headers", "include_column_headers", "IndicatorAttribute");
        // Nar IR-NNUM022937252 03/24/2012 End

        //count of invoice summary fields
        attributeMgr.registerAttribute("pmt_mthd_summary_count", "pmt_mthd_summary_count", "LocalAttribute");

	      /* owner_org_oid - A PO upload definition is "owned" by an organization.  This component relationship
	         represents the ownership. */
        attributeMgr.registerAttribute("owner_org_oid", "a_owner_org_oid");

        //Srinivasu_D CR-708 Rel8.1 05/30/2012 Start
        attributeMgr.registerReferenceAttribute("payment_method", "payment_method", "PAYMENT_METHOD");
        attributeMgr.requiredAttribute("payment_method");
        attributeMgr.registerAlias("payment_method", getResourceManager().getText("PaymentDefinitions.PaymentMethod", TradePortalConstants.TEXT_BUNDLE));
		      /* incoterm_req - field representing the Incoterm associated with an underlying shipment */
        attributeMgr.registerReferenceAttribute("file_format_type", "file_format_type", "PAYMENT_FILE_TYPE");
        attributeMgr.requiredAttribute("file_format_type");
        attributeMgr.registerAlias("file_format_type", getResourceManager().getText("PaymentDefinitions.FileFormat", TradePortalConstants.TEXT_BUNDLE));
		      /* vessel_req - vessel shipping the goods that the Payment Method is covering */
        attributeMgr.registerAttribute("debit_account_number_req", "debit_account_number_req", "IndicatorAttribute");


		      /* carrier_req - the carrier for the goods that the Payment Method is covering. */
        attributeMgr.registerAttribute("beneficiary_name_req", "beneficiary_name_req", "IndicatorAttribute");

		      /* actual_ship_date_req - The valid date format can be selected from the dropdown list*/
        attributeMgr.registerAttribute("bene_account_number_req", "bene_account_number_req", "IndicatorAttribute");

		      /* payment_date_req - valid date format can be selected from the dropdown list. */
        attributeMgr.registerAttribute("bene_bank_branch_code_req", "bene_bank_branch_code_req", "IndicatorAttribute");

		      /* purchase_ord_id_req - field representing the Purchase Order Id against which the Payment Method is shipping. */
        attributeMgr.registerAttribute("payment_currency_req", "payment_currency_req", "IndicatorAttribute");

		      /* goods_description_req - field representing the goods description for the Payment Method goods. */
        attributeMgr.registerAttribute("payment_amount_req", "payment_amount_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def1_label", "users_def1_label");
        attributeMgr.registerAttribute("users_def2_label", "users_def2_label");
        attributeMgr.registerAttribute("users_def3_label", "users_def3_label");
        attributeMgr.registerAttribute("users_def4_label", "users_def4_label");
        attributeMgr.registerAttribute("users_def5_label", "users_def5_label");
        attributeMgr.registerAttribute("users_def6_label", "users_def6_label");
        attributeMgr.registerAttribute("users_def7_label", "users_def7_label");
        attributeMgr.registerAttribute("users_def8_label", "users_def8_label");
        attributeMgr.registerAttribute("users_def9_label", "users_def9_label");
        attributeMgr.registerAttribute("users_def10_label", "users_def10_label");

        attributeMgr.registerAttribute("users_def1_req", "users_def1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def2_req", "users_def2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def3_req", "users_def3_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def4_req", "users_def4_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def5_req", "users_def5_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def6_req", "users_def6_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def7_req", "users_def7_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def8_req", "users_def8_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def9_req", "users_def9_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def10_req", "users_def10_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("customer_reference_req", "customer_reference_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("beneficiary_country_req", "beneficiary_country_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("execution_date_req", "execution_date_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address1_req", "bene_address1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address2_req", "bene_address2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address3_req", "bene_address3_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address4_req", "bene_address4_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_fax_no_req", "bene_fax_no_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_email_id_req", "bene_email_id_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("charges_req", "charges_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bank_name_req", "bene_bank_name_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_name_req", "bene_bnk_brnch_name_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr1_req", "bene_bnk_brnch_addr1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr2_req", "bene_bnk_brnch_addr2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_city_req", "bene_bnk_brnch_city_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_prvnc_req", "bene_bnk_brnch_prvnc_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_cntry_req", "bene_bnk_brnch_cntry_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("payable_location_req", "payable_location_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("print_location_req", "print_location_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("delvry_mth_n_delvrto_req", "delvry_mth_n_delvrto_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address1_req", "mailing_address1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address2_req", "mailing_address2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address3_req", "mailing_address3_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address4_req", "mailing_address4_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("details_of_payment_req", "details_of_payment_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("instruction_number_req", "instruction_number_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cd_req", "f_int_bnk_brnch_cd_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_name_req", "f_int_bnk_name_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_nm_req", "f_int_bnk_brnch_nm_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr1_req", "f_int_bnk_brnch_addr1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr2_req", "f_int_bnk_brnch_addr2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_city_req", "f_int_bnk_brnch_city_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_prvnc_req", "f_int_bnk_brnch_prvnc_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cntry_req", "f_int_bnk_brnch_cntry_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep1_req", "central_bank_rep1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep2_req", "central_bank_rep2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep3_req", "central_bank_rep3_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("confidential_ind_req", "confidential_ind_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("indv_acct_entry_ind_req", "indv_acct_entry_ind_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("header_identifier_req", "header_identifier_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("file_reference_req", "file_reference_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("detail_identifier_req", "detail_identifier_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("beneficiary_code_req", "beneficiary_code_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("reporting_code1_req", "reporting_code1_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("reporting_code2_req", "reporting_code2_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("inv_details_header_req", "inv_details_header_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("inv_details_lineitm_req", "inv_details_lineitm_req", "IndicatorAttribute");
//        attributeMgr.registerAttribute("include_column_headers", "include_column_headers", "IndicatorAttribute");
//        attributeMgr.registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");
        attributeMgr.registerAttribute("payment_method_data_req", "payment_method_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("debit_account_number_data_req", "debit_account_number_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("beneficiary_name_data_req", "beneficiary_name_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_account_number_data_req", "bene_account_number_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bank_branch_code_data_req", "bene_bank_branch_code_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("payment_currency_data_req", "payment_currency_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("payment_amount_data_req", "payment_amount_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("customer_reference_data_req", "customer_reference_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("beneficiary_country_data_req", "beneficiary_country_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("execution_date_data_req", "execution_date_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address1_data_req", "bene_address1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address2_data_req", "bene_address2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address3_data_req", "bene_address3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_address4_data_req", "bene_address4_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_fax_no_data_req", "bene_fax_no_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_email_id_data_req", "bene_email_id_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("charges_data_req", "charges_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bank_name_data_req", "bene_bank_name_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_name_data_req", "bene_bnk_brnch_name_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr1_data_req", "bene_bnk_brnch_addr1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr2_data_req", "bene_bnk_brnch_addr2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_city_data_req", "bene_bnk_brnch_city_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_prvnc_data_req", "bene_bnk_brnch_prvnc_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_cntry_data_req", "bene_bnk_brnch_cntry_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("payable_location_data_req", "payable_location_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("print_location_data_req", "print_location_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("delvry_mth_n_delvrto_data_req", "delvry_mth_n_delvrto_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address1_data_req", "mailing_address1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address2_data_req", "mailing_address2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address3_data_req", "mailing_address3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("mailing_address4_data_req", "mailing_address4_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("details_of_payment_data_req", "details_of_payment_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("instruction_number_data_req", "instruction_number_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cd_data_req", "f_int_bnk_brnch_cd_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_name_data_req", "f_int_bnk_name_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_nm_data_req", "f_int_bnk_brnch_nm_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr1_data_req", "f_int_bnk_brnch_addr1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr2_data_req", "f_int_bnk_brnch_addr2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_city_data_req", "f_int_bnk_brnch_city_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_prvnc_data_req", "f_int_bnk_brnch_prvnc_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cntry_data_req", "f_int_bnk_brnch_cntry_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep1_data_req", "central_bank_rep1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep2_data_req", "central_bank_rep2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("central_bank_rep3_data_req", "central_bank_rep3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("confidential_ind_data_req", "confidential_ind_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("indv_acct_entry_ind_data_req", "indv_acct_entry_ind_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("header_identifier_data_req", "header_identifier_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("file_reference_data_req", "file_reference_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("detail_identifier_data_req", "detail_identifier_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("beneficiary_code_data_req", "beneficiary_code_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("reporting_code1_data_req", "reporting_code1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("reporting_code2_data_req", "reporting_code2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("inv_details_header_data_req", "inv_details_header_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("inv_details_lineitm_data_req", "inv_details_lineitm_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def1_data_req", "users_def1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def2_data_req", "users_def2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def3_data_req", "users_def3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def4_data_req", "users_def4_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def5_data_req", "users_def5_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def6_data_req", "users_def6_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def7_data_req", "users_def7_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def8_data_req", "users_def8_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def9_data_req", "users_def9_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("users_def10_data_req", "users_def10_data_req", "IndicatorAttribute");


        attributeMgr.registerAttribute("pmt_mthd_summary_order1", "pmt_mthd_summary_order1");
        attributeMgr.registerAttribute("pmt_mthd_summary_order2", "pmt_mthd_summary_order2");
        attributeMgr.registerAttribute("pmt_mthd_summary_order3", "pmt_mthd_summary_order3");
        attributeMgr.registerAttribute("pmt_mthd_summary_order4", "pmt_mthd_summary_order4");
        attributeMgr.registerAttribute("pmt_mthd_summary_order5", "pmt_mthd_summary_order5");
        attributeMgr.registerAttribute("pmt_mthd_summary_order6", "pmt_mthd_summary_order6");
        attributeMgr.registerAttribute("pmt_mthd_summary_order7", "pmt_mthd_summary_order7");
        attributeMgr.registerAttribute("pmt_mthd_summary_order8", "pmt_mthd_summary_order8");
        attributeMgr.registerAttribute("pmt_mthd_summary_order9", "pmt_mthd_summary_order9");
        attributeMgr.registerAttribute("pmt_mthd_summary_order10", "pmt_mthd_summary_order10");
        attributeMgr.registerAttribute("pmt_mthd_summary_order11", "pmt_mthd_summary_order11");
        attributeMgr.registerAttribute("pmt_mthd_summary_order12", "pmt_mthd_summary_order12");
        attributeMgr.registerAttribute("pmt_mthd_summary_order13", "pmt_mthd_summary_order13");
        attributeMgr.registerAttribute("pmt_mthd_summary_order14", "pmt_mthd_summary_order14");
        attributeMgr.registerAttribute("pmt_mthd_summary_order15", "pmt_mthd_summary_order15");
        attributeMgr.registerAttribute("pmt_mthd_summary_order16", "pmt_mthd_summary_order16");
        attributeMgr.registerAttribute("pmt_mthd_summary_order17", "pmt_mthd_summary_order17");
        attributeMgr.registerAttribute("pmt_mthd_summary_order18", "pmt_mthd_summary_order18");
        attributeMgr.registerAttribute("pmt_mthd_summary_order19", "pmt_mthd_summary_order19");
        attributeMgr.registerAttribute("pmt_mthd_summary_order20", "pmt_mthd_summary_order20");
        attributeMgr.registerAttribute("pmt_mthd_summary_order21", "pmt_mthd_summary_order21");
        attributeMgr.registerAttribute("pmt_mthd_summary_order22", "pmt_mthd_summary_order22");
        attributeMgr.registerAttribute("pmt_mthd_summary_order23", "pmt_mthd_summary_order23");
        attributeMgr.registerAttribute("pmt_mthd_summary_order24", "pmt_mthd_summary_order24");
        attributeMgr.registerAttribute("pmt_mthd_summary_order25", "pmt_mthd_summary_order25");
        attributeMgr.registerAttribute("pmt_mthd_summary_order26", "pmt_mthd_summary_order26");
        attributeMgr.registerAttribute("pmt_mthd_summary_order27", "pmt_mthd_summary_order27");
        attributeMgr.registerAttribute("pmt_mthd_summary_order28", "pmt_mthd_summary_order28");
        attributeMgr.registerAttribute("pmt_mthd_summary_order29", "pmt_mthd_summary_order29");
        attributeMgr.registerAttribute("pmt_mthd_summary_order30", "pmt_mthd_summary_order30");
        attributeMgr.registerAttribute("pmt_mthd_summary_order31", "pmt_mthd_summary_order31");
        attributeMgr.registerAttribute("pmt_mthd_summary_order32", "pmt_mthd_summary_order32");
        attributeMgr.registerAttribute("pmt_mthd_summary_order33", "pmt_mthd_summary_order33");
        attributeMgr.registerAttribute("pmt_mthd_summary_order34", "pmt_mthd_summary_order34");
        attributeMgr.registerAttribute("pmt_mthd_summary_order35", "pmt_mthd_summary_order35");
        attributeMgr.registerAttribute("pmt_mthd_summary_order36", "pmt_mthd_summary_order36");
        attributeMgr.registerAttribute("pmt_mthd_summary_order37", "pmt_mthd_summary_order37");
        attributeMgr.registerAttribute("pmt_mthd_summary_order38", "pmt_mthd_summary_order38");
        attributeMgr.registerAttribute("pmt_mthd_summary_order39", "pmt_mthd_summary_order39");
        attributeMgr.registerAttribute("pmt_mthd_summary_order40", "pmt_mthd_summary_order40");
        attributeMgr.registerAttribute("pmt_mthd_summary_order41", "pmt_mthd_summary_order41");
        attributeMgr.registerAttribute("pmt_mthd_summary_order42", "pmt_mthd_summary_order42");
        attributeMgr.registerAttribute("pmt_mthd_summary_order43", "pmt_mthd_summary_order43");
        attributeMgr.registerAttribute("pmt_mthd_summary_order44", "pmt_mthd_summary_order44");
        attributeMgr.registerAttribute("pmt_mthd_summary_order45", "pmt_mthd_summary_order45");
        attributeMgr.registerAttribute("pmt_mthd_summary_order46", "pmt_mthd_summary_order46");
        attributeMgr.registerAttribute("pmt_mthd_summary_order47", "pmt_mthd_summary_order47");
        attributeMgr.registerAttribute("pmt_mthd_summary_order48", "pmt_mthd_summary_order48");
        attributeMgr.registerAttribute("pmt_mthd_summary_order49", "pmt_mthd_summary_order49");
        attributeMgr.registerAttribute("pmt_mthd_summary_order50", "pmt_mthd_summary_order50");
        attributeMgr.registerAttribute("pmt_mthd_summary_order51", "pmt_mthd_summary_order51");
        attributeMgr.registerAttribute("pmt_mthd_summary_order52", "pmt_mthd_summary_order52");
        attributeMgr.registerAttribute("pmt_mthd_summary_order53", "pmt_mthd_summary_order53");
        attributeMgr.registerAttribute("pmt_mthd_summary_order54", "pmt_mthd_summary_order54");
        attributeMgr.registerAttribute("pmt_mthd_summary_order55", "pmt_mthd_summary_order55");
        attributeMgr.registerAttribute("pmt_mthd_summary_order56", "pmt_mthd_summary_order56");
        attributeMgr.registerAttribute("pmt_mthd_summary_order57", "pmt_mthd_summary_order57");
        attributeMgr.registerAttribute("pmt_mthd_summary_order58", "pmt_mthd_summary_order58");
        attributeMgr.registerAttribute("pmt_mthd_summary_order59", "pmt_mthd_summary_order59");
        attributeMgr.registerAttribute("pmt_mthd_summary_order60", "pmt_mthd_summary_order60");
        attributeMgr.registerAttribute("pmt_mthd_summary_order61", "pmt_mthd_summary_order61");
        attributeMgr.registerAttribute("pmt_mthd_summary_order62", "pmt_mthd_summary_order62");
        attributeMgr.registerAttribute("pmt_mthd_summary_order63", "pmt_mthd_summary_order63");
        attributeMgr.registerAttribute("pmt_mthd_summary_order64", "pmt_mthd_summary_order64");
        //MEer Rel 9.0 CR-921 Added 2 more fields for FX Contract details to payment definitions
        attributeMgr.registerAttribute("pmt_mthd_summary_order65", "pmt_mthd_summary_order65");
        attributeMgr.registerAttribute("pmt_mthd_summary_order66", "pmt_mthd_summary_order66");
//               registerAttribute("inv_summary_order", "inv_summary_order");
        attributeMgr.registerAttribute("pmt_hdr_summary_order1", "pmt_hdr_summary_order1");
        attributeMgr.registerAttribute("pmt_hdr_summary_order2", "pmt_hdr_summary_order2");
        attributeMgr.registerAttribute("pmt_hdr_summary_order3", "pmt_hdr_summary_order3");
        attributeMgr.registerAttribute("pmt_hdr_summary_order4", "pmt_hdr_summary_order4");
        attributeMgr.registerAttribute("pmt_hdr_summary_order5", "pmt_hdr_summary_order5");
        attributeMgr.registerAttribute("pmt_hdr_summary_order6", "pmt_hdr_summary_order6");
        attributeMgr.registerAttribute("pmt_hdr_summary_order7", "pmt_hdr_summary_order7");
        attributeMgr.registerAttribute("pmt_hdr_summary_order8", "pmt_hdr_summary_order8");
        attributeMgr.registerAttribute("pmt_hdr_summary_order9", "pmt_hdr_summary_order9");
        attributeMgr.registerAttribute("pmt_hdr_summary_order10", "pmt_hdr_summary_order10");
        attributeMgr.registerAttribute("pmt_hdr_summary_order11", "pmt_hdr_summary_order11");
        attributeMgr.registerAttribute("pmt_hdr_summary_order12", "pmt_hdr_summary_order12");
        attributeMgr.registerAttribute("pmt_hdr_summary_order13", "pmt_hdr_summary_order13");

        attributeMgr.registerAttribute("pmt_pay_summary_order1", "pmt_pay_summary_order1");
        attributeMgr.registerAttribute("pmt_pay_summary_order2", "pmt_pay_summary_order2");
        attributeMgr.registerAttribute("pmt_pay_summary_order3", "pmt_pay_summary_order3");
        attributeMgr.registerAttribute("pmt_pay_summary_order4", "pmt_pay_summary_order4");
        attributeMgr.registerAttribute("pmt_pay_summary_order5", "pmt_pay_summary_order5");
        attributeMgr.registerAttribute("pmt_pay_summary_order6", "pmt_pay_summary_order6");
        attributeMgr.registerAttribute("pmt_pay_summary_order7", "pmt_pay_summary_order7");
        attributeMgr.registerAttribute("pmt_pay_summary_order8", "pmt_pay_summary_order8");
        attributeMgr.registerAttribute("pmt_pay_summary_order9", "pmt_pay_summary_order9");
        attributeMgr.registerAttribute("pmt_pay_summary_order10", "pmt_pay_summary_order10");
        attributeMgr.registerAttribute("pmt_pay_summary_order11", "pmt_pay_summary_order11");
        attributeMgr.registerAttribute("pmt_pay_summary_order12", "pmt_pay_summary_order12");
        attributeMgr.registerAttribute("pmt_pay_summary_order13", "pmt_pay_summary_order13");
        attributeMgr.registerAttribute("pmt_pay_summary_order14", "pmt_pay_summary_order14");
        attributeMgr.registerAttribute("pmt_pay_summary_order15", "pmt_pay_summary_order15");
        attributeMgr.registerAttribute("pmt_pay_summary_order16", "pmt_pay_summary_order16");
        attributeMgr.registerAttribute("pmt_pay_summary_order17", "pmt_pay_summary_order17");
        attributeMgr.registerAttribute("pmt_pay_summary_order18", "pmt_pay_summary_order18");
        attributeMgr.registerAttribute("pmt_pay_summary_order19", "pmt_pay_summary_order19");
        attributeMgr.registerAttribute("pmt_pay_summary_order20", "pmt_pay_summary_order20");
        attributeMgr.registerAttribute("pmt_pay_summary_order21", "pmt_pay_summary_order21");
        attributeMgr.registerAttribute("pmt_pay_summary_order22", "pmt_pay_summary_order22");
        attributeMgr.registerAttribute("pmt_pay_summary_order23", "pmt_pay_summary_order23");
        attributeMgr.registerAttribute("pmt_pay_summary_order24", "pmt_pay_summary_order24");
        attributeMgr.registerAttribute("pmt_pay_summary_order25", "pmt_pay_summary_order25");
        attributeMgr.registerAttribute("pmt_pay_summary_order26", "pmt_pay_summary_order26");
        attributeMgr.registerAttribute("pmt_pay_summary_order27", "pmt_pay_summary_order27");
        attributeMgr.registerAttribute("pmt_pay_summary_order28", "pmt_pay_summary_order28");
        attributeMgr.registerAttribute("pmt_pay_summary_order29", "pmt_pay_summary_order29");
        attributeMgr.registerAttribute("pmt_pay_summary_order30", "pmt_pay_summary_order30");
        attributeMgr.registerAttribute("pmt_pay_summary_order31", "pmt_pay_summary_order31");
        attributeMgr.registerAttribute("pmt_pay_summary_order32", "pmt_pay_summary_order32");
        attributeMgr.registerAttribute("pmt_pay_summary_order33", "pmt_pay_summary_order33");
        attributeMgr.registerAttribute("pmt_pay_summary_order34", "pmt_pay_summary_order34");
        attributeMgr.registerAttribute("pmt_pay_summary_order35", "pmt_pay_summary_order35");
        attributeMgr.registerAttribute("pmt_pay_summary_order36", "pmt_pay_summary_order36");
        attributeMgr.registerAttribute("pmt_pay_summary_order37", "pmt_pay_summary_order37");
        attributeMgr.registerAttribute("pmt_pay_summary_order38", "pmt_pay_summary_order38");
        attributeMgr.registerAttribute("pmt_pay_summary_order39", "pmt_pay_summary_order39");
        attributeMgr.registerAttribute("pmt_pay_summary_order40", "pmt_pay_summary_order40");
        attributeMgr.registerAttribute("pmt_pay_summary_order41", "pmt_pay_summary_order41");
        attributeMgr.registerAttribute("pmt_pay_summary_order42", "pmt_pay_summary_order42");
        attributeMgr.registerAttribute("pmt_pay_summary_order43", "pmt_pay_summary_order43");
        attributeMgr.registerAttribute("pmt_pay_summary_order44", "pmt_pay_summary_order44");
        attributeMgr.registerAttribute("pmt_pay_summary_order45", "pmt_pay_summary_order45");
        attributeMgr.registerAttribute("pmt_pay_summary_order46", "pmt_pay_summary_order46");
        attributeMgr.registerAttribute("pmt_pay_summary_order47", "pmt_pay_summary_order47");
        attributeMgr.registerAttribute("pmt_pay_summary_order48", "pmt_pay_summary_order48");
        attributeMgr.registerAttribute("pmt_pay_summary_order49", "pmt_pay_summary_order49");
        attributeMgr.registerAttribute("pmt_pay_summary_order50", "pmt_pay_summary_order50");
        attributeMgr.registerAttribute("pmt_pay_summary_order51", "pmt_pay_summary_order51");
        attributeMgr.registerAttribute("pmt_pay_summary_order52", "pmt_pay_summary_order52");
        attributeMgr.registerAttribute("pmt_pay_summary_order53", "pmt_pay_summary_order53");
        attributeMgr.registerAttribute("pmt_pay_summary_order54", "pmt_pay_summary_order54");

        attributeMgr.registerAttribute("pmt_inv_summary_order1", "pmt_inv_summary_order1");
        attributeMgr.registerAttribute("pmt_inv_summary_order2", "pmt_inv_summary_order2");

        attributeMgr.registerAttribute("payment_method_size", "payment_method_size", "NumberAttribute");
        attributeMgr.registerAttribute("debit_account_number_size", "debit_account_number_size", "NumberAttribute");
        attributeMgr.registerAttribute("beneficiary_name_size", "beneficiary_name_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_account_number_size", "bene_account_number_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bank_branch_code_size", "bene_bank_branch_code_size", "NumberAttribute");
        attributeMgr.registerAttribute("payment_currency_size", "payment_currency_size", "NumberAttribute");
        attributeMgr.registerAttribute("payment_amount_size", "payment_amount_size", "NumberAttribute");
        attributeMgr.registerAttribute("customer_reference_size", "customer_reference_size", "NumberAttribute");
        attributeMgr.registerAttribute("beneficiary_country_size", "beneficiary_country_size", "NumberAttribute");
        attributeMgr.registerAttribute("execution_date_size", "execution_date_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_address1_size", "bene_address1_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_address2_size", "bene_address2_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_address3_size", "bene_address3_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_address4_size", "bene_address4_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_fax_no_size", "bene_fax_no_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_email_id_size", "bene_email_id_size", "NumberAttribute");
        attributeMgr.registerAttribute("charges_size", "charges_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bank_name_size", "bene_bank_name_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_name_size", "bene_bnk_brnch_name_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr1_size", "bene_bnk_brnch_addr1_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_addr2_size", "bene_bnk_brnch_addr2_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_city_size", "bene_bnk_brnch_city_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_prvnc_size", "bene_bnk_brnch_prvnc_size", "NumberAttribute");
        attributeMgr.registerAttribute("bene_bnk_brnch_cntry_size", "bene_bnk_brnch_cntry_size", "NumberAttribute");
        attributeMgr.registerAttribute("payable_location_size", "payable_location_size", "NumberAttribute");
        attributeMgr.registerAttribute("print_location_size", "print_location_size", "NumberAttribute");
        attributeMgr.registerAttribute("delvry_mth_n_delvrto_size", "delvry_mth_n_delvrto_size", "NumberAttribute");
        attributeMgr.registerAttribute("mailing_address1_size", "mailing_address1_size", "NumberAttribute");
        attributeMgr.registerAttribute("mailing_address2_size", "mailing_address2_size", "NumberAttribute");
        attributeMgr.registerAttribute("mailing_address3_size", "mailing_address3_size", "NumberAttribute");
        attributeMgr.registerAttribute("mailing_address4_size", "mailing_address4_size", "NumberAttribute");
        attributeMgr.registerAttribute("details_of_payment_size", "details_of_payment_size", "NumberAttribute");
        attributeMgr.registerAttribute("instruction_number_size", "instruction_number_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cd_size", "f_int_bnk_brnch_cd_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_name_size", "f_int_bnk_name_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_nm_size", "f_int_bnk_brnch_nm_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr1_size", "f_int_bnk_brnch_addr1_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_addr2_size", "f_int_bnk_brnch_addr2_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_city_size", "f_int_bnk_brnch_city_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_prvnc_size", "f_int_bnk_brnch_prvnc_size", "NumberAttribute");
        attributeMgr.registerAttribute("f_int_bnk_brnch_cntry_size", "f_int_bnk_brnch_cntry_size", "NumberAttribute");
        attributeMgr.registerAttribute("central_bank_rep1_size", "central_bank_rep1_size", "NumberAttribute");
        attributeMgr.registerAttribute("central_bank_rep2_size", "central_bank_rep2_size", "NumberAttribute");
        attributeMgr.registerAttribute("central_bank_rep3_size", "central_bank_rep3_size", "NumberAttribute");
        attributeMgr.registerAttribute("confidential_ind_size", "confidential_ind_size", "NumberAttribute");
        attributeMgr.registerAttribute("indv_acct_entry_ind_size", "indv_acct_entry_ind_size", "NumberAttribute");
        attributeMgr.registerAttribute("header_identifier_size", "header_identifier_size", "NumberAttribute");
        attributeMgr.registerAttribute("file_reference_size", "file_reference_size", "NumberAttribute");
        attributeMgr.registerAttribute("detail_identifier_size", "detail_identifier_size", "NumberAttribute");
        attributeMgr.registerAttribute("beneficiary_code_size", "beneficiary_code_size", "NumberAttribute");
        attributeMgr.registerAttribute("reporting_code1_size", "reporting_code1_size", "NumberAttribute");
        attributeMgr.registerAttribute("reporting_code2_size", "reporting_code2_size", "NumberAttribute");
        attributeMgr.registerAttribute("inv_details_header_size", "inv_details_header_size", "NumberAttribute");
        attributeMgr.registerAttribute("inv_details_lineitm_size", "inv_details_lineitm_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def1_size", "users_def1_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def2_size", "users_def2_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def3_size", "users_def3_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def4_size", "users_def4_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def5_size", "users_def5_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def6_size", "users_def6_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def7_size", "users_def7_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def8_size", "users_def8_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def9_size", "users_def9_size", "NumberAttribute");
        attributeMgr.registerAttribute("users_def10_size", "users_def10_size", "NumberAttribute");

        attributeMgr.registerAttribute("fd_record_type_size", "fd_record_type_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_bsb_size", "fd_bsb_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_account_size", "fd_account_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_reserved1_size", "fd_reserved1_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_sequence_number_size", "fd_sequence_number_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_nm_of_usr_fin_inst_size", "fd_nm_of_usr_fin_inst_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_reserved2_size", "fd_reserved2_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_nm_of_usr_sup_file_size", "fd_nm_of_usr_sup_file_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_user_ident_num_size", "fd_user_ident_num_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_desc_of_entr_on_file_size", "fd_desc_of_entr_on_file_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_date_to_be_process_size", "fd_date_to_be_process_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_time_size", "fd_time_size", "NumberAttribute");
        attributeMgr.registerAttribute("fd_reserved3_size", "fd_reserved3_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_record_type_size", "dr_record_type_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_bsb_size", "dr_bsb_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_account_type_size", "dr_account_type_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_acct_num_to_be_cred_size", "dr_acct_num_to_be_cred_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_withhold_tax_ind_size", "dr_withhold_tax_ind_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_trans_code_size", "dr_trans_code_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_nw_var_bsb_acct_det_size", "dr_nw_var_bsb_acct_det_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_title_acct_cred_size", "dr_title_acct_cred_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_amt_to_be_cred_size", "dr_amt_to_be_cred_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_lodge_ref_size", "dr_lodge_ref_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_trace_bsb_num_size", "dr_trace_bsb_num_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_trace_acct_num_size", "dr_trace_acct_num_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_nm_of_remitter_size", "dr_nm_of_remitter_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_withhold_amt_size", "dr_withhold_amt_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_lodge_ref_bsb_num_size", "dr_lodge_ref_bsb_num_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_lodge_ref_acct_typ_size", "dr_lodge_ref_acct_typ_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_lodeg_ref_acct_num_size", "dr_lodeg_ref_acct_num_size", "NumberAttribute");
        attributeMgr.registerAttribute("dr_reserved_size", "dr_reserved_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_record_type_size", "br_record_type_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_reserved1_size", "br_reserved1_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_reserved2_size", "br_reserved2_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_btch_net_tot_amt_size", "br_btch_net_tot_amt_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_btch_cred_tot_amt_size", "br_btch_cred_tot_amt_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_btch_deb_tot_amt_size", "br_btch_deb_tot_amt_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_reserved3_size", "br_reserved3_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_btch_tot_itm_cnt_size", "br_btch_tot_itm_cnt_size", "NumberAttribute");
        attributeMgr.registerAttribute("br_reserved4_size", "br_reserved4_size", "NumberAttribute");

        attributeMgr.registerAttribute("fd_bsb_data_req", "fd_bsb_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("fd_account_data_req", "fd_account_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("fd_reserved1_data_req", "fd_reserved1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("fd_reserved2_data_req", "fd_reserved2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("fd_time_data_req", "fd_time_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("fd_reserved3_data_req", "fd_reserved3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("dr_withhold_amt_data_req", "dr_withhold_amt_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("dr_reserved_data_req", "dr_reserved_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("br_reserved2_data_req", "br_reserved2_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("br_btch_cred_tot_amt_data_req", "br_btch_cred_tot_amt_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("br_btch_deb_tot_amt_data_req", "bene_address1_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("br_reserved3_data_req", "br_reserved3_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("br_reserved4_data_req", "br_reserved4_data_req", "IndicatorAttribute");
        attributeMgr.registerAttribute("dr_withhold_tax_ind_data_req", "dr_withhold_tax_ind_data_req", "IndicatorAttribute");

        attributeMgr.registerAttribute("fd_summary_order1", "fd_summary_order1");
        attributeMgr.registerAttribute("fd_summary_order2", "fd_summary_order2");
        attributeMgr.registerAttribute("fd_summary_order3", "fd_summary_order3");
        attributeMgr.registerAttribute("fd_summary_order4", "fd_summary_order4");
        attributeMgr.registerAttribute("fd_summary_order5", "fd_summary_order5");
        attributeMgr.registerAttribute("fd_summary_order6", "fd_summary_order6");
        attributeMgr.registerAttribute("fd_summary_order7", "fd_summary_order7");
        attributeMgr.registerAttribute("fd_summary_order8", "fd_summary_order8");
        attributeMgr.registerAttribute("fd_summary_order9", "fd_summary_order9");
        attributeMgr.registerAttribute("fd_summary_order10", "fd_summary_order10");
        attributeMgr.registerAttribute("fd_summary_order11", "fd_summary_order11");
        attributeMgr.registerAttribute("fd_summary_order12", "fd_summary_order12");
        attributeMgr.registerAttribute("fd_summary_order13", "fd_summary_order13");

        attributeMgr.registerAttribute("dr_summary_order1", "dr_summary_order1");
        attributeMgr.registerAttribute("dr_summary_order2", "dr_summary_order2");
        attributeMgr.registerAttribute("dr_summary_order3", "dr_summary_order3");
        attributeMgr.registerAttribute("dr_summary_order4", "dr_summary_order4");
        attributeMgr.registerAttribute("dr_summary_order5", "dr_summary_order5");
        attributeMgr.registerAttribute("dr_summary_order6", "dr_summary_order6");
        attributeMgr.registerAttribute("dr_summary_order7", "dr_summary_order7");
        attributeMgr.registerAttribute("dr_summary_order8", "dr_summary_order8");
        attributeMgr.registerAttribute("dr_summary_order9", "dr_summary_order9");
        attributeMgr.registerAttribute("dr_summary_order10", "dr_summary_order10");
        attributeMgr.registerAttribute("dr_summary_order11", "dr_summary_order11");
        attributeMgr.registerAttribute("dr_summary_order12", "dr_summary_order12");
        attributeMgr.registerAttribute("dr_summary_order13", "dr_summary_order13");
        attributeMgr.registerAttribute("dr_summary_order14", "dr_summary_order14");
        attributeMgr.registerAttribute("dr_summary_order15", "dr_summary_order15");
        attributeMgr.registerAttribute("dr_summary_order16", "dr_summary_order16");
        attributeMgr.registerAttribute("dr_summary_order17", "dr_summary_order17");
        attributeMgr.registerAttribute("dr_summary_order18", "dr_summary_order18");

        attributeMgr.registerAttribute("br_summary_order1", "br_summary_order1");
        attributeMgr.registerAttribute("br_summary_order2", "br_summary_order2");
        attributeMgr.registerAttribute("br_summary_order3", "br_summary_order3");
        attributeMgr.registerAttribute("br_summary_order4", "br_summary_order4");
        attributeMgr.registerAttribute("br_summary_order5", "br_summary_order5");
        attributeMgr.registerAttribute("br_summary_order6", "br_summary_order6");
        attributeMgr.registerAttribute("br_summary_order7", "br_summary_order7");
        attributeMgr.registerAttribute("br_summary_order8", "br_summary_order8");
        attributeMgr.registerAttribute("br_summary_order9", "br_summary_order9");
        
        /* ownership_level - The level at which the reference data is owned (global, client bank, bank
        organization group, or corporate customer */
		attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");
		attributeMgr.requiredAttribute("ownership_level");
		/* ownership_type - This attribute is used to populate the "Added By" column of listviews. 
		If the ownership level is set to corporate customer, the ownership type
		is set to non-admin.  If ownership level is set to anything else, the ownership
		type is admin. */
		attributeMgr.registerReferenceAttribute("ownership_type", "ownership_type", "OWNERSHIP_TYPE");
		
		//MEerupula Rel 9.0 CR-921 Add FX Contract details to payment file
		attributeMgr.registerAttribute("fx_contract_number_data_req", "fx_contract_number_data_req", "IndicatorAttribute");
		attributeMgr.registerAttribute("fx_contract_number_req", "fx_contract_number_req", "IndicatorAttribute");
		attributeMgr.registerAttribute("fx_contract_number_size", "fx_contract_number_size", "NumberAttribute");
		attributeMgr.registerAttribute("fx_contract_rate_data_req", "fx_contract_rate_data_req", "IndicatorAttribute");
		attributeMgr.registerAttribute("fx_contract_rate_req", "fx_contract_rate_req", "IndicatorAttribute");
		attributeMgr.registerAttribute("fx_contract_rate_size", "fx_contract_rate_size", "NumberAttribute");


    }
}
