package com.ams.tradeportal.busobj;

    import com.amsinc.ecsg.frame.*;
    import com.amsinc.ecsg.util.*;
    import java.rmi.*;
    import java.util.*;
    import java.math.*;
    import javax.ejb.*;
import com.ams.tradeportal.common.*;


    /**
    * The Payment Files that are to be uploaded.
    *
    *     Copyright  � 2003
    *     American Management Systems, Incorporated
    *     All rights reserved
    */
    public interface PaymentFileUpload extends BusinessObject
    {
		public void deletePaymentFileUpload(java.lang.String paymentFileUploadOid) throws RemoteException, AmsException;
		public void rejectFileUplaod(boolean setDefaultErrorMsg) throws RemoteException, AmsException;
		public boolean isH2HSentPayment() throws RemoteException, AmsException;
		public boolean isPartialPayAuthAllowed() throws RemoteException, AmsException; //Rel9.2 CR 988
    }
