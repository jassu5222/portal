/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.ejb.RemoveException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.StringFunction;


/*
 * The Payment Files that are to be uploaded.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentFileUploadBean extends PaymentFileUploadBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadBean.class);

 	public void deletePaymentFileUpload(java.lang.String paymentFileUploadOid) throws RemoteException, AmsException
 	{
		//Create an instance of the related instrument and populate data fields from the database.
		PaymentFileUpload upload = (PaymentFileUpload) this.createServerEJB("PaymentFileUpload");
		upload.getData(Long.parseLong(paymentFileUploadOid));
		upload.setAttribute("deleted_ind", TradePortalConstants.INDICATOR_YES);
		upload.save();
	}
 	
 	/**
	 * Sets Validation status to Rejected Status. Also, set status of Instrument, Transaction accordingly.
	 *
	 *
	 * @param  String Error message
	 * @throws AmsException
	 * @throws RemoveException 
	 * @throws IOException 
	 */
	public void rejectFileUplaod(boolean setDefaultErrorMsg) throws RemoteException, AmsException {
		
		
		String instrument_oid = getAttribute("instrument_oid");

		if (StringFunction.isNotBlank(instrument_oid)) {
			Instrument instrument = (Instrument)createServerEJB("Instrument",Long.parseLong(instrument_oid));
			long tran_oid = instrument.getAttributeLong("original_transaction_oid");
		
			//NSX IR# HIUL021165255    Rel. 6.1.0 -Begin
			getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_VERIFY);
			instrument.setClientServerDataBridge(getClientServerDataBridge());
			//NSX IR# HIUL021165255    Rel. 6.1.0 -End
			Transaction transaction = (Transaction)instrument.getComponentHandle("TransactionList",tran_oid);
			instrument.setAttribute("instrument_status", TradePortalConstants.INSTRUMENT_STATUS_FILE_UPLOAD_REJECTED);
			transaction.setAttribute("transaction_status", TransactionStatus.FILE_UPLOAD_REJECT);

			if (instrument.save(false) < 0) {
				LOG.info("Error saving instrument...: instrument_oid = {}" , instrument_oid);
				throw new AmsException("Error saving instrument...: instrument_oid = " + instrument_oid);
			}
			
			clearDP(tran_oid);	
		}

		if (setDefaultErrorMsg) {
			String errorMsg = getResourceManager().getText(TradePortalConstants.ERR_PROCESSING_PAYMENTUPLOAD_FILE,"ErrorMessages");
			setAttribute("error_msg", errorMsg);
		}

		setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);
		if (save() < 0) {
			throw new AmsException("error saving PaymentFileUpload ....: PaymentFileUpload_oid = " + getAttribute("payment_file_upload_oid"));
		}
	}
	
	/**
	 * Deletes any existing Domestic Payments
	 *
	 *
	 * @param  String Transaction OID
	 */
	protected void clearDP(long transactionOid) {

		try {

			String sqlQuery = "DELETE FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?";

			DatabaseQueryBean.executeUpdate(sqlQuery,false, new Object[]{transactionOid});
		} catch (Exception e) {
			LOG.error("Error deleting domestic payments: ",e);
		}
	}
	
	// CJR CR-593 04/06/2011 Begin
	/**
	 * Returns true if this is a payment sent from H2H.
	 *
	 * @return true if a H2H Sent Payment, false otherwise.
	 */
	public boolean isH2HSentPayment() throws RemoteException, AmsException{
		return StringFunction.isNotBlank(this.getAttribute("h2h_source"));
	}
	// CJR CR-593 04/06/2011 End
	
	// PMitnala CR-988 11/04/2014 Begin
	/**
	 * 
	 * @return true if auto authorize for H2H partial payments is allowed.
	 */
	public boolean isPartialPayAuthAllowed() throws RemoteException, AmsException{
		return (StringFunction.isNotBlank(this.getAttribute("allow_partial_pay_auth")) && TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_partial_pay_auth")));
	}
	// PMitnala CR-988 11/04/2014 End
}
