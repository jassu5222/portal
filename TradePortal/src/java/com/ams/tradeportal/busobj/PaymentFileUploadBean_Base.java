
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The Payment Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentFileUploadBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* payment_file_upload_oid - Unique Identifider */
      attributeMgr.registerAttribute("payment_file_upload_oid", "payment_file_upload_oid", "ObjectIDAttribute");
      
      /* payment_file_name - Payment File Name. */
      attributeMgr.registerAttribute("payment_file_name", "payment_file_name");
      
      /* validation_status - The status of Payment File upload validation. */
      attributeMgr.registerReferenceAttribute("validation_status", "validation_status", "VALIDATION_STATUS");
      
      /* number_of_bene_uploaded - The number of beneficiaries successfully uploaded. */
      attributeMgr.registerAttribute("number_of_bene_uploaded", "number_of_bene_uploaded", "NumberAttribute");
      
      /* number_of_bene_failed - Number of beneficiaries that failed for upload. */
      attributeMgr.registerAttribute("number_of_bene_failed", "number_of_bene_failed", "NumberAttribute");
      
      /* creation_timestamp - Date/Time when the file is uploaded. */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* completion_timestamp - Date/Time when the validation is completed. */
      attributeMgr.registerAttribute("completion_timestamp", "completion_timestamp", "DateTimeAttribute");
      
      /* agent_id - The name of the upload agent that processes/validates the file.  This prevents
         multiple agents from accessing the same file. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* deleted_ind - Whether the file is deleted. */
      attributeMgr.registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");
      
      /* error_msg - Error message of uploading. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* validation_seconds - The time it takes the agent to validate the payment file. */
      attributeMgr.registerAttribute("validation_seconds", "validation_seconds", "NumberAttribute");
      
      /* user_oid - The user who initiates the Payment File Upload. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /* instrument_oid - The instrument that the Payment File Upload creates. */
      attributeMgr.registerAssociation("instrument_oid", "a_instrument_oid", "Instrument");
      
      /* owner_org_oid - The Corporate Customer who uploaded the file. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
	  
      /* CJR CR-593 gxs_ref */
	  attributeMgr.registerAttribute("gxs_ref", "gxs_ref");
	  
	  /* CJR CR-593 customer_file_ref */
	  attributeMgr.registerAttribute("customer_file_ref", "customer_file_ref");

      /* payment_definition_oid The oid of the payment_definition.  Register as Number Attribute for now until payment_definition is checked in the code */
      attributeMgr.registerAttribute("payment_definition_oid", "a_payment_definition_oid", "NumberAttribute");
      
      /* NAR CR-694A Rel9.0.0.0 05-MAY-2014  */
      /* h2h_source - sender of payment file i.e GXS or FLA */
	  attributeMgr.registerAttribute("h2h_source", "h2h_source");

      /* datetime_process_start - Date/Time when the file processing started. */
      attributeMgr.registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
      /* total_process_seconds - The total time it takes the agent to process the invoice file. */
      attributeMgr.registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
      /* process_seconds - The time it takes the agent to validate the invoice file. */
      attributeMgr.registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
      /* system_name -  */
      attributeMgr.registerAttribute("system_name", "system_name");
      /* server_name - */
      attributeMgr.registerAttribute("server_name", "server_name");

      /* PMitnala CR-970 Rel9.1.0.0  */
      /* message_id - messageID of payment file i.e GXS or FLA */
	  attributeMgr.registerAttribute("message_id", "message_id");

	  //Rel9.2 CR 988 Begin
	  /* allow_partial_pay_auth - Indicates whether to automatically authorize H2H partial payments. */
      attributeMgr.registerAttribute("allow_partial_pay_auth", "allow_partial_pay_auth", "IndicatorAttribute");
      //Rel9.2 CR 988 End
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* PaymentUploadErrorLogList -  */
      registerOneToManyComponent("PaymentUploadErrorLogList","PaymentUploadErrorLogList");
   }

 
   
 
 
   
}
