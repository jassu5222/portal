
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Payment Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentFileUploadHome extends EJBHome
{
   public PaymentFileUpload create()
      throws RemoteException, CreateException, AmsException;

   public PaymentFileUpload create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
