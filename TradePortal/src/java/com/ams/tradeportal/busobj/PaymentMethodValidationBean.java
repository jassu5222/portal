
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ReferenceDataManager;

/*
 *
 *
 *     Copyright  � 2009
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentMethodValidationBean extends PaymentMethodValidationBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodValidationBean.class);
    public String userGetAttribute(String attributeName) 
              throws RemoteException, AmsException   {
        
        if ("description".equals(attributeName)){
            return  ReferenceDataManager.getRefDataMgr().getDescr("PAYMENT_METHOD", getAttribute("payment_method"), csdb.getLocaleName())
                    + "," + ReferenceDataManager.getRefDataMgr().getDescr("COUNTRY", getAttribute("country"), csdb.getLocaleName())
                    + "," + ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", getAttribute("currency"), csdb.getLocaleName());
        }
        return "";
    }
    
    protected void userValidate ()  throws AmsException, RemoteException {
        if (!this.isUnique("payment_method", getAttribute("payment_method"), " and country = '"+getAttribute("country")+"' and currency = '"+getAttribute("currency") + "'",false)) {
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                      TradePortalConstants.PAY_METHOD_RULE_ALREADY_EXISTS);
        }
    }
    

}










