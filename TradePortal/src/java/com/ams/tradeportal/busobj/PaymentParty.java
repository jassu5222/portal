

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A party of Payment instrument.  It should be a BankBranch.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentParty extends TradePortalBusinessObject
{   
	public void copy(com.ams.tradeportal.busobj.PaymentParty source) throws RemoteException, AmsException; //SHR Rel 8.1 IR VIUL061757186 
}
