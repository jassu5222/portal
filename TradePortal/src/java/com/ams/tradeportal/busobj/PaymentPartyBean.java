
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.InstrumentServices;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentPartyBean extends PaymentPartyBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentPartyBean.class);

/**
* Performs any special "validate" processing that is specific to the
* descendant of BusinessObject. Specifically, it checks to see whether
* or not the the Account Number and Currency information has been entered correctly
*/
public void userValidate()
	throws RemoteException, AmsException {

}
// End userValidate

//SHR Rel 8.1 IR VIUL061757186 Start

/**
 * copy(...)
 *
 * this method takes a Payment party and copies all the information to this Payment party
 *
 * @param source PaymentParty
 */
public void copy(PaymentParty source) throws RemoteException, AmsException
{
  LOG.debug("Entering PaymentParty.copy");

  //get the attributes from the source PaymentParty and use them to set
  //the attributes on the new PaymentParty
  Vector nameValueArrays = InstrumentServices.attributeHashProcessor(source.getAttributeHash());
  String [] name = (String []) nameValueArrays.elementAt(0);
  String [] value = (String []) nameValueArrays.elementAt(1);

  this.setAttributes(name, value);
}

//SHR IR VIUL061757186 End
}
