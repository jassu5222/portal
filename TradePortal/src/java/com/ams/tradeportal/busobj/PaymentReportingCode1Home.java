
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Payment Reporting Code 1
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PaymentReportingCode1Home extends EJBHome
{
   public PaymentReportingCode1 create()
      throws RemoteException, CreateException, AmsException;

   public PaymentReportingCode1 create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
