package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.busobj.util.InstrumentServices;


/*
 * Payment Reporting Code 2
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentReportingCode2Bean extends PaymentReportingCode2Bean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentReportingCode2Bean.class);
	//BSL IR #VIUL032443438 CR-655 03/28/11 Begin
	protected void userValidate ()  throws AmsException, RemoteException {
		StringBuffer extraCriteria = new StringBuffer(" and p_bank_group_oid = ");
		extraCriteria.append(getAttribute("bank_group_oid"));

		if (!this.isUnique("code", getAttribute("code"), extraCriteria.toString())) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REP_CODE_NOT_UNIQUE, "2");
		}

        //IAZ IR-DNUL061462150 06/18/11 Begin
		if (StringFunction.isNotBlank(InstrumentServices.isAlphaNumeric(getAttribute("short_description"), false)))
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REP_FIELD_HAS_NON_ALPHA_NM, InstrumentServices.isAlphaNumeric(getAttribute("short_description"), false));

		if (StringFunction.isNotBlank(InstrumentServices.isAlphaNumeric(getAttribute("description"), false)))
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REP_FIELD_HAS_NON_ALPHA_NM, InstrumentServices.isAlphaNumeric(getAttribute("description"), false));

		if (StringFunction.isNotBlank(InstrumentServices.isAlphaNumeric(getAttribute("code"), false)))
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REP_FIELD_HAS_NON_ALPHA_NM, InstrumentServices.isAlphaNumeric(getAttribute("code"), false));
        //IAZ IR-DNUL061462150 06/18/11 End

	}
	//BSL IR #VIUL032443438 CR-655 03/28/11 End
}
