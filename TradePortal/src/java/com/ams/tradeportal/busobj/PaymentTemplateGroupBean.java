
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.DatabaseQueryBean;




/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentTemplateGroupBean extends PaymentTemplateGroupBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentTemplateGroupBean.class);
    public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
    {

        String name  = getAttribute("name");
	    String where = " and p_owner_org_oid = '" + this.getAttribute("owner_org_oid") + "'";

    	debug("PaymentTemplateGroupBean.userValidate: where -> " + where);

	    // now we need to check that name is unique
	    if (!isUnique("name", name, where)) {
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                  TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
	    }
    }
    
    /**
     * Cannot delete Template Group if it is currently being used.
     *
     * @exception java.rmi.RemoteException The exception description.
     * @exception com.amsinc.ecsg.frame.AmsException The exception description.
     */
    public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

        // VSUK110464315
        // Check if this Template Group is being used anywhere.  Give error if yes.
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
        if (DatabaseQueryBean.getCount("authorized_template_group_oid", "user_authorized_template_group", "a_template_group_oid = ? ", false, new Object[]{getAttribute("payment_template_group_oid")}) > 0) {
            String name = getAttribute("name");
            this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                              TradePortalConstants.CURRENTLY_ASSIGNED, name);
        }
     }
}
