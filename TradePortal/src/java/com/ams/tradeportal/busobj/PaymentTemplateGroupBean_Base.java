


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentTemplateGroupBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentTemplateGroupBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* payment_template_group_oid - Unique identifier */
      attributeMgr.registerAttribute("payment_template_group_oid", "payment_template_group_oid", "ObjectIDAttribute");

      
      /* threshold_group_name - Name of the threshold group */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.requiredAttribute("name");
      attributeMgr.registerAlias("name", getResourceManager().getText("PaymentTemplateGroupBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));


      /* phrase_text - The text that comprises the phrase */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.requiredAttribute("description");
      attributeMgr.registerAlias("description", getResourceManager().getText("PaymentTemplateGroupBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));


        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

   }






}
