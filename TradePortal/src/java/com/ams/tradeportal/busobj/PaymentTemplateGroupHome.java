package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface PaymentTemplateGroupHome extends EJBHome
{
   public PaymentTemplateGroup create()
      throws RemoteException, CreateException, AmsException;

   public PaymentTemplateGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
