
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentUploadErrorLogBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentUploadErrorLogBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* payment_upload_error_log_oid - Unique identifier */
      attributeMgr.registerAttribute("payment_upload_error_log_oid", "payment_upload_error_log_oid", "ObjectIDAttribute");
      
      /* beneficiary_info - Beneficiary Info (Name, customer ref, amount) */
      attributeMgr.registerAttribute("beneficiary_info", "beneficiary_info");
      
      /* error_msg - Error message. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* line_number - The line number in the upload file. */
      attributeMgr.registerAttribute("line_number", "line_number", "NumberAttribute");
      
        /* Pointer to the parent PaymentFileUpload */
      attributeMgr.registerAttribute("payment_file_upload_oid", "p_payment_file_upload_oid", "ParentIDAttribute");
      
      /* Pointer to domestic_payment_oid */
      attributeMgr.registerAttribute("domestic_payment_oid", "p_domestic_payment_oid", "NumberAttribute");
      
      /* reporting_code_error_ind - Indicates if the error is related to Reporting code errors  */
      attributeMgr.registerAttribute("reporting_code_error_ind", "reporting_code_error_ind", "IndicatorAttribute");
   
   }
   
 
   
 
 
   
}
