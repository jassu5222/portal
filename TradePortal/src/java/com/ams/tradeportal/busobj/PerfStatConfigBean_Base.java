
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;


/*
 * The configuration of the application.  These configuration entries used
 * to reside on the properties files.  They are moved to database for easier
 * management.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerfStatConfigBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PerfStatConfigBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* perf_stat_config_oid - Unique identifier */
      attributeMgr.registerAttribute("perf_stat_config_oid", "perf_stat_config_oid", "ObjectIDAttribute");
      
      /* name - The name of the page */
      attributeMgr.registerAttribute("page_name", "page_name");
      attributeMgr.requiredAttribute("page_name");
      
      
      /* activation_status - Indicates whether or not the SLA monitoring has been activated.  */
      attributeMgr.registerAttribute("active", "active","IndicatorAttribute");
      
      attributeMgr.registerAttribute("client_bank", "client_bank");
      
   }
   
 
   
 
 
   
}
