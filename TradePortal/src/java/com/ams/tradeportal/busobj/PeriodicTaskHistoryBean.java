package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * This object represents the running of a periodic task, such as purge or
 * sending daily email.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PeriodicTaskHistoryBean extends PeriodicTaskHistoryBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PeriodicTaskHistoryBean.class);


   
}
