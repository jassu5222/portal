
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * This object represents the running of a periodic task, such as purge or
 * sending daily email.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PeriodicTaskHistoryHome extends EJBHome
{
   public PeriodicTaskHistory create()
      throws RemoteException, CreateException, AmsException;

   public PeriodicTaskHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
