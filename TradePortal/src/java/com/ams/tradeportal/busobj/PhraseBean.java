 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ams.tradeportal.common.*;

/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PhraseBean extends PhraseBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PhraseBean.class);
    public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException 
    {

        String name  = getAttribute("name");
	    String where = " and p_owner_org_oid = '" + this.getAttribute("owner_org_oid") + "'";
	
    	LOG.debug("where -> {}" , where);
	
	    // now we need to check that name is unique
	    if (!isUnique("name", name, where)) {
		    LOG.error("name already used");
		    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                  TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
	    }
	    
	    String phraseCat = this.getAttribute("phrase_category");
	    LOG.debug("phraseCat -> {}" , phraseCat);
	    if (phraseCat.equals(TradePortalConstants.PHRASE_CAT_GUAR_BANK) ||
	        phraseCat.equals(TradePortalConstants.PHRASE_CAT_PMT_INST))
	    {
	        if (this.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE))
	        {
		        LOG.error("Invalid Phrase type");
		        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.UNAUTH_PHRASE_TYPE);
			}
	    }


        // Validate the length of the phrase text 
        String text = this.getAttribute("phrase_text");

	if (text.length() > 4000) {
		String endText = text.substring(3980, 4000);
		this.getErrorManager().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
					attributeMgr.getAlias("phrase_text"),
					endText);
	}


    }


}
