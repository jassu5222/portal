/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.AmsException;


/*
 * 
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PmtTermsTenorDtlBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PmtTermsTenorDtlBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      attributeMgr.registerAttribute("pmt_terms_tenor_dtl_oid", "pmt_terms_tenor_dtl_oid", "ObjectIDAttribute");
      attributeMgr.registerAttribute("percent", "percent", "NumberAttribute");
      attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerReferenceAttribute("tenor_type", "tenor_type", "TENOR");
    //Leelavathi IR#T36000012144 03/06/2013 Rel-8.2 Begin
     // attributeMgr.registerAttribute("num_days_after", "num_days_after", "NumberAttribute");
      attributeMgr.registerAttribute("num_days_after", "num_days_after", "DecimalAttribute");
    //Leelavathi IR#T3600001214 03/06/2013 Rel-8.2 End
      attributeMgr.registerReferenceAttribute("days_after_type", "days_after_type", "TENOR_TYPE");
      attributeMgr.registerAttribute("maturity_date", "maturity_date", "DateTimeAttribute");
      attributeMgr.registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

   }






}
