//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Initialversion

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PmtTermsTenorDtlHome extends EJBHome
{
   public PmtTermsTenorDtl create()
      throws RemoteException, CreateException, AmsException;

   public PmtTermsTenorDtl create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
