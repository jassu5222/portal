package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */


public class PurchaseOrderBean extends PurchaseOrderBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderBean.class);

	public String getListCriteria(String type) 
		   {
			  // Retrieve the PurchaseOrderLineItem by a_purchase_order_oid
			  if (type.equals("byPurchaseOrderDefinition"))
			  {
				 return "a_owner_org_oid = {0} and a_upload_definition_oid = {1}";
			  }

			  return "";
		   }           


    /* Perform processing for a New instance of the business object */

    protected void userNewObject() throws AmsException, RemoteException
    {
        /* Perform any New Object processing defined in the Ancestor class */
        super.userNewObject();
        this.setAttribute("creation_date_time", DateTimeUtility.getGMTDateTime(true, false));//BSL IR BLUM041758623 04/19/2012 - do not use override date
    }

    /**
     *
     */
    public int save (boolean shouldValidate) throws RemoteException, AmsException
    {
        int val = super.save(shouldValidate);

        if (val == 1) { // continue postSave if no error
            val = postSave();
        }
        return val;
    }

    /**
     * Post save processing goes here
     *
     */
    protected int postSave() throws RemoteException, AmsException {
        int val = 0;
            String action =  getAttribute("action"); 
            //SHR IR T36000005804
            if (!StringFunction.isBlank(action) && !action.equals(TradePortalConstants.PO_ACTION_AUTHORISE)) {
                // only create Transaction History if action is specified,
                val = createPurchaseOrderHisotryLog(action);
            }

        return val;
    }

    /**
     *
     * Creates createPurchaseOrderHisotryLog.
     *
     */
    protected int createPurchaseOrderHisotryLog(String action) throws RemoteException, AmsException {
		
    	// DK IR-RBUM062632733 Rel8.1 09/24/2012	Start
    	String poHistorySQl =  "INSERT into PURCHASE_ORDER_HISTORY (PURCHASE_ORDER_HISTORY_OID, ACTION_DATETIME, ACTION, " +
        "STATUS, P_PURCHASE_ORDER_OID, A_USER_OID) VALUES (?,?,?,?,?,?)";

	    Date date = new Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
		String status = getAttribute("status");
		String poOID = getAttribute("purchase_order_oid");
		String userOid = getAttribute("user_oid");
		int ret = -1;
		
		try(Connection con = DatabaseQueryBean.connect(true);
				PreparedStatement insStmt = con.prepareStatement(poHistorySQl)){
			
			insStmt.setLong(1,oid.generateObjectID());
			insStmt.setTimestamp(2, timeStamp);
			insStmt.setString(3, action);
			insStmt.setString(4,status);
			insStmt.setString(5,poOID);
			insStmt.setString(6,userOid);
			int exec = insStmt.executeUpdate();
	        if (exec<1) {
	            LOG.info("Error occured while creating Transaction History Log...");
	        }
	        else {
	        	ret = 1;
	        }
	       
		}
		catch(Exception e){
			throw new AmsException("SQL Exception \n"
					 + "\nException is " +e.getMessage());
		}
		
		// DK IR-RBUM062632733 Rel8.1 09/24/2012	End
        return ret;

    }
	//Srinivasu_D CR-707 Rel8.0 03/06/2012 Start 
    /**
     * This method performs deletion of Purchase Order Line Items for the given purchaseOrderOid
     * and updates the delete_ind as Y
     * @param purchaseOrderOid
     * @param userOid
     * @param status
     * @return
     * @throws java.rmi.RemoteException
     * @throws com.amsinc.ecsg.frame.AmsException
     */
    public int deletePO(String userOid) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{   	    	
    	   	  	
    	  
    	int res=0;  
    	String purchaseOrderOid = this.getAttribute("purchase_order_oid");
    	String status = this.getAttribute("status");    	
    	String deleteStatement  ="delete purchase_order_line_item where p_purchase_order_oid = ? ";
    	
		if(TradePortalConstants.PO_STATUS_UNASSIGNED.equals(status) || TradePortalConstants.PO_STATUS_CANCELLED.equals(status)){
			this.setAttribute("deleted_ind", TradePortalConstants.INDICATOR_YES);	
			this.setAttribute("action", TradePortalConstants.PO_ACTION_DELETE);
			this.setAttribute("status", TradePortalConstants.PO_STATUS_DELETE);
			this.setAttribute("user_oid",userOid);		
		
			//Deleting Purchase order Line Items for the corresponding POOrderOid		
		try {
			DatabaseQueryBean.executeUpdate(deleteStatement, false, new Object[]{purchaseOrderOid});
				
			} catch (SQLException e) {			
				throw new AmsException ("SQL Exception found executing sql statement \n"
				 + "\nException is " + e.getMessage());
		}		
		res = this.save();	
		
		}
	    return res;
    	
    }
//Srinivasu_D CR-707 Rel8.0 03/06/2012 End 
}
