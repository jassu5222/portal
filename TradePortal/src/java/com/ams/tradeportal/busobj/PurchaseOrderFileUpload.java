

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The PurchaseOrder files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderFileUpload extends BusinessObject
{   
	public void deletePOFileUpload(java.lang.String poFileUploadOid) throws RemoteException, AmsException;
	public void rejectFileUplaod(boolean setDefaultErrorMsg) throws RemoteException, AmsException;
	
}
