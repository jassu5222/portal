package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The PurchaseOrder files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderFileUploadBean extends PurchaseOrderFileUploadBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderFileUploadBean.class);   
	public void deletePOFileUpload(java.lang.String poFileUploadOid) throws RemoteException, AmsException
 	{
		PurchaseOrderFileUpload upload = (PurchaseOrderFileUpload) this.createServerEJB("PurchaseOrderFileUpload");
		upload.getData(Long.parseLong(poFileUploadOid));
		upload.setAttribute("deleted_ind", TradePortalConstants.INDICATOR_YES);
		
		upload.save();
	}
 	
 	/**
	 * Sets Validation status to Rejected Status.
	 *
	 *
	 * @param  setDefaultErrorMsg
	 * @throws AmsException
	 * @throws RemoveException 
	 * @throws IOException 
	 */
	public void rejectFileUplaod(boolean setDefaultErrorMsg) throws RemoteException, AmsException {
	
		setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);
		if (save() < 0) {
			throw new AmsException("error saving PurchaseOrderFileUpload ....: PurchaseOrderFileUpload_oid = " + getAttribute("po_file_upload_oid"));
		}
	}
 
}
