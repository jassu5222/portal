
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The PurchaseOrder files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderFileUploadBean_Base extends BusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderFileUploadBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  
      /* po_file_upload_oid - Unique Identifider */
      attributeMgr.registerAttribute("po_file_upload_oid", "po_file_upload_oid", "ObjectIDAttribute");
      
      /* po_file_name - Purchase Order File Name. */
      attributeMgr.registerAttribute("po_file_name", "po_file_name");
      
      /* validation_status - The status of Purchase Order File upload validation. */
      attributeMgr.registerReferenceAttribute("validation_status", "validation_status", "VALIDATION_STATUS");
      
      /* number_of_po_uploaded - The number of purchase orders successfully uploaded. */
      attributeMgr.registerAttribute("number_of_po_uploaded", "number_of_po_uploaded", "NumberAttribute");
      
      /* number_of_po_failed - Number of purchase orders that failed for upload. */
      attributeMgr.registerAttribute("number_of_po_failed", "number_of_po_failed", "NumberAttribute");
      
      /* creation_timestamp - Date/Time when the file is uploaded. */
      attributeMgr.registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");
      
      /* completion_timestamp - Date/Time when the validation is completed. */
      attributeMgr.registerAttribute("completion_timestamp", "completion_timestamp", "DateTimeAttribute");
      
      /* agent_id - The name of the upload agent that processes/validates the file.  This prevents
         multiple agents from accessing the same file. */
      attributeMgr.registerAttribute("agent_id", "agent_id");
      
      /* deleted_ind - Whether the file is deleted. */
      attributeMgr.registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");
      
      /* error_msg - Error message of uploading. */
      attributeMgr.registerAttribute("error_msg", "error_msg");
      
      /* validation_seconds - The time it takes the agent to validate the file. */
      attributeMgr.registerAttribute("validation_seconds", "validation_seconds", "NumberAttribute");
      
      /* currency - The currency of the Purchase Orders in the file. If the file contains multiple
         currencies, the system will display "Mixed" */
      attributeMgr.registerAttribute("currency", "currency");
      
      /* amount - The total amount of all Purchase Orders. If the Purchase Orderss have mixed
         currencies, the field will be blank. */
      attributeMgr.registerAttribute("amount", "amount", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* po_upload_parameters - An XML document containing information about what the user wants to happen
         with the data they just uploaded. */
      attributeMgr.registerAttribute("po_upload_parameters", "po_upload_parameters");
      
      /* a_upload_definition_oid - The PO upload definition that was used to upload this purchase order data. */
      attributeMgr.registerAttribute("a_upload_definition_oid", "a_upload_definition_oid", "NumberAttribute");
      
      /* po_result_parameters -  */
      attributeMgr.registerAttribute("po_result_parameters", "po_result_parameters");
      
      /* user_oid - The user who initiates the Purchase Order File Upload. */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
      /* owner_org_oid - The Corporate Customer who uploaded the file. */
      attributeMgr.registerAssociation("owner_org_oid", "a_owner_org_oid", "CorporateOrganization");
      
      /* datetime_process_start - Date/Time when the file processing started. */
      attributeMgr.registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
      /* total_process_seconds - The total time it takes the agent to process the invoice file. */
      attributeMgr.registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
      /* process_seconds - The time it takes the agent to validate the invoice file. */
      attributeMgr.registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
      /* system_name -  */
      attributeMgr.registerAttribute("system_name", "system_name");
      /* server_name - */
      attributeMgr.registerAttribute("server_name", "server_name");
    
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* PurchaseOrderUploadErrorLogList -  */
      registerOneToManyComponent("PurchaseOrderUploadErrorLogList","PurchaseOrderUploadErrorLogList");
   }

 
   
 
 
   
}
