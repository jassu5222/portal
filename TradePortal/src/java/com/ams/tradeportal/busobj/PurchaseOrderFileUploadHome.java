
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The PurchaseOrder files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderFileUploadHome extends EJBHome
{
   public PurchaseOrderFileUpload create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrderFileUpload create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
