
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * The history audit of Purchase Order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* purchase_order_history_oid - Unique Identifier */
      attributeMgr.registerAttribute("purchase_order_history_oid", "purchase_order_history_oid", "ObjectIDAttribute");
      
      /* action_datetime - Timestamp (in GMT) when the action is taken. */
      attributeMgr.registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");
      
      /* action - Action */
      attributeMgr.registerReferenceAttribute("action", "action", "PURCHASE_ORDER_ACTION_TYPE");
      
      /* status - Staus of Purchase Order. */
      attributeMgr.registerReferenceAttribute("status", "status", "PURCHASE_ORDER_STATUS");
      
      /* Pointer to the parent PurchaseOrder */
       attributeMgr.registerAttribute("purchase_order_oid", "p_purchase_order_oid", "ParentIDAttribute");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      
   }
   
 
   
 
 
   
}
