
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The history audit of Purchase Order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderHistoryHome extends EJBHome
{
   public PurchaseOrderHistory create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrderHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
