 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderLineItemBean extends PurchaseOrderLineItemBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderLineItemBean.class);
	public String getListCriteria(String type) 
		   {
			  // Retrieve the PurchaseOrderLineItem by p_purchase_order_oid
			  if (type.equals("byPurchaseOrderLineItem"))
			  {
				 return "p_purchase_order_oid = {0}";
			  }

			  return "";
		   }             

	 
 
   
}
