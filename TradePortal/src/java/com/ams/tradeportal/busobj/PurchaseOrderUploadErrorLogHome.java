
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The Error Logs of uploading Purchase Order File for each purchase order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface PurchaseOrderUploadErrorLogHome extends EJBHome
{
   public PurchaseOrderUploadErrorLog create()
      throws RemoteException, CreateException, AmsException;

   public PurchaseOrderUploadErrorLog create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
