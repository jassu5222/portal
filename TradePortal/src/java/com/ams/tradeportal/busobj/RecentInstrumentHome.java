package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * RecentInstrument home.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface RecentInstrumentHome extends EJBHome
{
   public RecentInstrument create()
      throws RemoteException, CreateException, AmsException;

   public RecentInstrument create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
