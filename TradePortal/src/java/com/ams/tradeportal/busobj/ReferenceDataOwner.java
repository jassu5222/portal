package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Any organization that can own reference data is a subclass of this business
 * object.  It defines one-to-many component relationships to reference data
 * such as phrases, templates, etc.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ReferenceDataOwner extends Organization
{
 
}
