
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Any organization that can own reference data is a subclass of this business
 * object.  It defines one-to-many component relationships to reference data
 * such as phrases, templates, etc.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceDataOwnerBean_Base extends OrganizationBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReferenceDataOwnerBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* default_user_oid - The default user for an organization.  The use of this association differs
         from one organization to another. */
      attributeMgr.registerAssociation("default_user_oid", "a_default_user_oid", "User");
      
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* UserList - A user is "owned" by an organization.  This component relationship represents
         the ownership. */
      registerOneToManyComponent("UserList","UserList");

      /* PhraseList - A phrase is "owned" by an organization.  This component relationship represents
         the ownership. */
      registerOneToManyComponent("PhraseList","PhraseList");

      /* PartyList - A phrase is "owned" by an organization.  This component relationship represents
         the ownership. */
      registerOneToManyComponent("PartyList","PartyList");

      /* TemplateList - A template is "owned" by an organization.  This component relationship represents
         the ownership. */
      registerOneToManyComponent("TemplateList","TemplateList");

      /* SecurityProfileList - A security profile is "owned" by an organization.  This component relationship
         represents the ownership. */
      registerOneToManyComponent("SecurityProfileList","SecurityProfileList");
   }

 
   
 
 
   
}
