



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface ReferenceDataPending extends TradePortalBusinessObject
{
	
}
