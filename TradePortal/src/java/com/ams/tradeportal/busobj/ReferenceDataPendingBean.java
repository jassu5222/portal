
package com.ams.tradeportal.busobj;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;



/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ReferenceDataPendingBean extends ReferenceDataPendingBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ReferenceDataPendingBean.class);

	
	

	/**
	 * Performs any special "validate" processing that is specific to the
	 * descendant of BusinessObject. Specifically, it checks to see whether
	 * or not the name entered for a corporate organization is unique.
	 *
	 * @see #validate()
	 */

	protected void userValidate() throws RemoteException, AmsException {
		
	}
	
	
	protected void validateUserData() throws RemoteException, AmsException{
		if (!isUnique("bus_id",
				getRefDataID(),
				" and class_name = '" + getAttribute("class_name") + "'  and P_OWNER_OID = "
					+ this.getAttribute("P_OWNER_OID"))) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ALREADY_EXISTS,
					"",
					attributeMgr.getAlias("name"));
			}
	}
	
	
	protected void validateCorporateCustomerData() throws RemoteException, AmsException{
		if (!isUnique("bus_id",
				getAttribute("Proponix_customer_id"),
				" and class_name = '" + getAttribute("class_name") + "'  and P_OWNER_OID = "
					+ this.getAttribute("P_OWNER_OID"))) {
				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ALREADY_EXISTS,
					"",
					attributeMgr.getAlias("name"));
			}
		
	}
	
	protected String getRefDataID() throws RemoteException, AmsException {
		String className = getAttribute("class_name");
		String refDataID = "";

		
		if ("User".equals(className)) {
			refDataID= getAttribute("name");
			
		}
		return refDataID;
	}
	
}
