package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.*;

public class SPEmailNotificationsBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(SPEmailNotificationsBean_Base.class);
	
	/* 
	   * Register the attributes and associations of the business object
	   */
	   protected void registerAttributes() throws AmsException
	   {  

	      /* Register attributes defined in the Ancestor class */
	      super.registerAttributes();
	      
	     /* sp_email_oid primary key attribute for SPEmailNotifications */
	      attributeMgr.registerAttribute("sp_email_oid", "sp_email_oid", "ObjectIDAttribute");
	      
	      /*  next_sp_email_due- A datetime (in GMT)  when next email is due */
	      attributeMgr.registerAttribute("next_sp_email_due", "next_sp_email_due", "DateTimeAttribute");
	      
	      
	      /* Pointer to the CorporateOrganization */
	      attributeMgr.registerAttribute("p_corp_organization_oid", "p_corp_organization_oid", "NumberAttribute");
	      
	      /* Pointer to the parent NotificationRule */
	      attributeMgr.registerAttribute("p_notification_rule_oid", "p_notification_rule_oid", "NumberAttribute");
	  
	   }
	   
	

}
