
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Supplier Portal Margin Rule for a Corporate Customer.  These are the margins
 * used to calculate the invoice offer's net amount offer.  See InvoicesSummaryDate.calculateSupplierPortalDiscount().
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SPMarginRuleHome extends EJBHome
{
   public SPMarginRule create()
      throws RemoteException, CreateException, AmsException;

   public SPMarginRule create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
