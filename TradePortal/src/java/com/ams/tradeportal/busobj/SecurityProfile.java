package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Business object to store the Security Profile for a user.  A security profile
 * describes the rights and privledges that a user has within the Trade Portal.
 * These rights are stored in the security_rights attributes.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SecurityProfile extends ReferenceData
{   
}
