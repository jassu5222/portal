
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class SecurityProfileBean extends SecurityProfileBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(SecurityProfileBean.class);


/**
 * This method determines if this security profile is currently being
 * used by a user.
 *
 * @return boolean - true = is being used, false = not used
 */

public boolean isUsed() {
	boolean isUsed = true;
	GenericList userList = null;
	String parms[]  = new String[2];

	try{
		// Set the parameters to be used in looking up the user in the database
		parms[0] = this.getAttribute("security_profile_oid");
		parms[1] = TradePortalConstants.ACTIVE;

		// Create a generic list object and instantiate it based on the parameters passed in
		userList = (GenericList) EJBObjectFactory.createServerEJB(getSessionContext(), "GenericList");

		LOG.debug("searching for users assigned to this profile");
		userList.prepareList("User", "bySecurityProfile", parms);
		userList.getData();

		LOG.debug("users assigned to profile is {}" , userList.getObjectCount());
		// If any users were found, the profile is being used
		if (userList.getObjectCount() == 0) {
			isUsed = false;
		}
	} catch (NullPointerException e) {
		LOG.error("Security Profile - Null on query");
	} catch (Exception e) {
		LOG.error("Exception caught in isUsed(): ",e);
		// error condition, so assume it is used
		isUsed = true;
	} finally {
		// Remove the listview bean
		try {
			if (userList != null)
				userList.remove();
				LOG.debug("listview bean removed");
		} catch (RemoveException ex) {
		} catch (NoSuchObjectException e) {
		} catch (RemoteException e) {
		}
	}

	return isUsed;
 }

/**
 * Cannot delete security profile if it is currently being used.
 *
 * @exception java.rmi.RemoteException The exception description.
 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
 */
public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	String name = getAttribute("name");

	// Check if this profile is being used anywhere.
	if (isUsed()) {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.CURRENTLY_ASSIGNED, name);
	}


 }

/**
 * Performs User validation:
 *    unique name required.
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */

public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {

	String name = getAttribute("name");

	// now we need to check that name is unique
	long myOrg = this.getAttributeLong("owner_org_oid");
	if (!isUnique("name", name, " and p_owner_org_oid = " + myOrg)) {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                              TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("name"));
	}

	//call a function to Validate against Create New Party selection
	String securityRights = this.getAttribute("security_rights");
	validateMailoMessageSelection(securityRights); //IR - GAUI022069369
	validateCreateNewPartySelection(securityRights);
	validateInvoiceMgmt(securityRights);
	validateUploadedInvoice(securityRights);	
 }
	private void validateUploadedInvoice(String securityRights) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{
				
		if(SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_FINANCING)|| 
				SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_REMOVE_INVOICE)||
				SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE)||
				SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_DELETE_INVOICE)){
			
			if(!SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_UPLOADED_INVOICES)){
				this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.MISSING_INVOICE_UPLOAD);
			}
			
		}
	}
	


private void validateInvoiceMgmt(String securityRights) throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException{
	
	boolean isError=true;
	if(SecurityAccess.hasRights(securityRights, SecurityAccess.ACCESS_INVOICE_MGT_AREA)){
			if(SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_UPLOADED_INVOICES))
				isError=false;
			else if(SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_INVOICES))
				isError=false;
			else if( SecurityAccess.hasRights(securityRights, SecurityAccess.REMOVE_UPLOADED_FILES))
				isError=false;			
			else
				isError=true;
		
			if(isError){
			this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.MISSING_INVOICE_MGMT_OPTIONS);
		}
		//Srinivasu Rel8.0 IR-DEUM050357968  05/10/2012 Start
		if(SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_INVOICES) || SecurityAccess.hasRights(securityRights, SecurityAccess.REMOVE_UPLOADED_FILES)){
			if(!SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_UPLOADED_INVOICES)){
					this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.VIEW_UPLOAD_MISSING);
			}
		}
		//Srinivasu Rel8.0 IR-DEUM050357968  05/10/2012 Start
	}	
}

/**
 * Validates Create New Party selection:
 *    SecurityRights required.
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */

private void validateCreateNewPartySelection(String securityRights)  throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
{
   boolean canViewOrMaintainParty, canCreateNewParty;

   canViewOrMaintainParty = canCreateNewParty = false;

   canViewOrMaintainParty = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_OR_MAINTAIN_PARTIES);
   canCreateNewParty = SecurityAccess.hasRights(securityRights, SecurityAccess.CREATE_NEW_TRANSACTION_PARTY);

   //throw error if the User does not have view access but has selected create new party option
   if ((!canViewOrMaintainParty) && (canCreateNewParty))
   {
	  this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.SECURITY_USER_DOES_NOT_HAVE_RIGHTS);
   }

}

//IR - GAUI022069369 - New Method...
/**
 * Validates Mail Message selection:
 *    SecurityRights required.
 *
 * @exception java.rmi.RemoteException
 * @exception com.amsinc.ecsg.frame.AmsException
 */

private void validateMailoMessageSelection(String securityRights)  throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
{
   //throws error if the User has selected the option "Send to Bank" but the "Create / Reply" option is not selected.
   if (SecurityAccess.hasRights(securityRights, SecurityAccess.SEND_MESSAGE))
   {
	   if (!SecurityAccess.hasRights(securityRights, SecurityAccess.CREATE_MESSAGE))
	   {
		   this.errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CREATE_REPLY_NOT_SELECTED);
	   }

   }

}


}
