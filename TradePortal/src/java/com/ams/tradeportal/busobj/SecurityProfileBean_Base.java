
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;




/*
 * Business object to store the Security Profile for a user.  A security profile
 * describes the rights and privledges that a user has within the Trade Portal.
 * These rights are stored in the security_rights attributes.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SecurityProfileBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(SecurityProfileBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* security_profile_oid - Unique identifier */
      attributeMgr.registerAttribute("security_profile_oid", "security_profile_oid", "ObjectIDAttribute");
      
      /* security_profile_type - Indicates whether this security profile is an admin profile (describing
         rights and privledges for admin users) or a regular profile (used by corporate
         customers and by admin users acting on behalf of corporate customers). */
      attributeMgr.registerReferenceAttribute("security_profile_type", "security_profile_type", "SEC_PROFILE_TYPE");
      attributeMgr.requiredAttribute("security_profile_type");
      
      /* security_rights - Contains the security rights for the user.   The value stored in this field
         will be the string representation of a number.  This number can be converted
         into a string of 1s and 0s.  Each of these bits (a 1 or 0) represents whether
         a right is allowed or disallowed.
         
         For more information on how this attribute is used, see the SecurityAccess
         class. */
      attributeMgr.registerAttribute("security_rights", "security_rights");
      attributeMgr.requiredAttribute("security_rights");
      
        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
