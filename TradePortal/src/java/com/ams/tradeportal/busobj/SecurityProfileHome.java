package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Business object to store the Security Profile for a user.  A security profile
 * describes the rights and privledges that a user has within the Trade Portal.
 * These rights are stored in the security_rights attributes.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SecurityProfileHome extends EJBHome
{
   public SecurityProfile create()
      throws RemoteException, CreateException, AmsException;

   public SecurityProfile create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
