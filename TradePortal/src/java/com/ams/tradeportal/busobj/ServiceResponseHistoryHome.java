
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * ServiceResponseHistory is used for tracking the Service Request sent from
 * Tradeportal to an external system.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ServiceResponseHistoryHome extends EJBHome
{
   public ServiceResponseHistory create()
      throws RemoteException, CreateException, AmsException;

   public ServiceResponseHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
