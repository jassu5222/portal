package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ShipmentTermsBean extends ShipmentTermsBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ShipmentTermsBean.class);

 
         /**
	  * This method takes a source ShipmentTerms and copies all the attributes to
	  * this ShipmentTerms
	  *
	  * @param source ShipmentTerms
	  */
         public void copy(ShipmentTerms source) throws RemoteException, AmsException
	 {
	   //get the attributes from the source ShipmentTerms and use them to set
	   //the attributes on the new ShipmentTerms
	   Vector nameValueArrays = InstrumentServices.attributeHashProcessor(source.getAttributeHash());
	   String [] name = (String []) nameValueArrays.elementAt(0);
	   String [] value = (String []) nameValueArrays.elementAt(1);

	   this.setAttributes(name, value);
	   //Vshah - 07/29/08 - IR#MUUI042953663 - Add Below line.
	   this.setAttribute("po_line_items", "");	

           // Copy the shipment terms parties if they are present
           copyShipmentTermsPartyInfo("NotifyParty", source);
           copyShipmentTermsPartyInfo("OtherConsigneeParty", source);
	 }

         /**
          * This method makes a copy of all non-null shipment terms parties for a 
          * Shipment Terms object
          *
          * @param source ShipmentTerms
          */
         protected void copyShipmentTermsPartyInfo(String partyId, ShipmentTerms source) throws AmsException, RemoteException
         {
           // Check to see if the shipment terms party exists
           if(StringFunction.isBlank(source.getAttribute("c_"+partyId)))
              return;

           // Create a new terms party component and copy
           TermsParty sourceParty = (TermsParty) source.getComponentHandle(partyId);
           long oid = this.newComponent(partyId);
           TermsParty newTermsParty = (TermsParty) this.getComponentHandle(partyId);
           newTermsParty.copy(sourceParty);
         }
 
	/**
	 * THIS METHOD SHOULD ONLY BE USED DURING THE AUTO LC CREATION PROCESS AND
	 * IS THEREFORE ONLY VALID FOR IMPORT DLC'S
	 * 
	 * @param poLineItem POLineItem - a single PO line Item to be used to derive the goods description
	 */
	public void setPOGoodsDescription(POLineItem poLineItem) throws RemoteException, AmsException
	 {
		 this.setAttribute("po_line_items", GoodsDescriptionUtility.buildGoodsDescription(poLineItem));
		 LOG.debug("the goods desc is \n {} " , this.getAttribute("po_line_items"));
	 }

	/**
	 * THIS METHOD SHOULD ONLY BE USED DURING THE AUTO LC CREATION PROCESS AND
	 * IS THEREFORE ONLY VALID FOR IMPORT DLC'S
	 * 
	 * @param poOids java.util.Vector
	 */
	public void setPOGoodsDescription(java.util.Vector poOids) throws RemoteException, AmsException
	 {
             if((poOids != null) && (poOids.size() > 0))
		 this.setAttribute("po_line_items", GoodsDescriptionUtility.buildGoodsDescription(poOids));
             else
                 // If there are no POs, set to blank
 		 this.setAttribute("po_line_items", "");
		 LOG.debug("the goods desc is \n {}" , this.getAttribute("po_line_items"));
	 }

	/**
	 * Set the goods description (po_line_item) given the Oids of the POs in
	 * addition to the existing PO Line Item of the ShipmentTerms.
	 * 
	 *  THIS METHOD SHOULD ONLY BE USED DURING THE AUTO LC CREATION PROCESS
	 * AND IS THEREFORE ONLY VALID FOR IMPORT DLC'S
	 */
	public void setPOGoodsDescription(java.util.Vector poOids, boolean incremental) throws RemoteException, AmsException
	 {
		 String goodsDescription;
		 if (incremental) {
			goodsDescription = GoodsDescriptionUtility.buildGoodsDescription(poOids, this.getAttribute(this.getIDAttributeName()));
		 }
		 else {
			goodsDescription = GoodsDescriptionUtility.buildGoodsDescription(poOids);
		 }
		this.setAttribute("po_line_items", goodsDescription);
		LOG.debug("the goods desc is \n {}" , goodsDescription);
	 }

	/**
	 *
	 * THIS METHOD SHOULD ONLY BE USED DURING THE AUTO LC CREATION PROCESS AND IS THEREFORE ONLY VALID 
	 * FOR IMPORT DLC'S
	 *
	 * Build and set the po_line_items from all the PO Line Items of the
	 * ShipmentTerms.  Retrieve the data of the PO Line Items either from the
	 * POLineItemList component or from querying the database.
	 * 
	 * @param poLineItemList ComponentList - The handle to the POLineItemList if
	 * it is already instantiated, null otherwise.  The method will get data
	 * from the component if the handle is not null and retrieve data from
	 * database otherwise.
	 * @param numberOfPOLineItems int - Number of PO Line Items.  Needed to
	 * avoid transaction conflict.
	 * @return void
	 * 
	 * 
	 */
	public void setPOGoodsDescription(ComponentList poLineItemList, int numberOfPOLineItems) throws RemoteException, AmsException
	 {
	 	String poLineItems;
	 	if(poLineItemList != null){
	 		// If POLineItemList component is instantiated, get data from the component.
	 		poLineItems = GoodsDescriptionUtility.buildGoodsDescription(poLineItemList, numberOfPOLineItems);
	 	}
	 	else {
	 		// If POLineItemList component is not instantiated, get data from querying the database.
	 		poLineItems = GoodsDescriptionUtility.buildGoodsDescription(this.getAttribute(this.getIDAttributeName()));
	 	}
	 	
	 	this.setAttribute("po_line_items", poLineItems);
		LOG.debug("the goods desc is \n: {}" , this.getAttribute("po_line_items"));
	 }


	   /* Perform processing for a New instance of the business object
	  */
	  protected void userNewObject() throws AmsException, RemoteException
	  {  
		/* Perform any New Object processing defined in the Ancestor class */
		super.userNewObject();

		this.setAttribute("creation_date", DateTimeUtility.getGMTDateTime(true, false));
	  }    

	  /* 
	  * Deletes all manually created POs assigned to this shipment.
	  *

	  * Performs a database update (using DatabaseQueryBean) to unassign
	  * all the uploaded PO Line Items currently assigned to this Transaction.  
	  * 
	  * Because this performs direct database updates, you must consider how
	  * this method call is included within transactional processing for 
	  * rollback purposes.
	  */
	  public void removeAssignedPOs(String instrumentOid, String transactionOid) throws AmsException, RemoteException 
	  {

		// This SQL deletes all of the POs that were associated to this transaction
		// that had been manually created
		StringBuffer deleteManualSql = new StringBuffer();
		deleteManualSql.append("delete from po_line_item ");
		deleteManualSql.append("where p_shipment_oid = ?");
		deleteManualSql.append(" and source_type = '");
		deleteManualSql.append(TradePortalConstants.PO_SOURCE_MANUAL);
		deleteManualSql.append("'");

		// The SQL updates the older, previous versions of the POs that were
		// uploaded and makes them active for the instrument again. They 
		// previously were active, but then when the PO was reuploaded, the 
		// old PO data became inactive.
		StringBuffer previousSql = new StringBuffer();
		previousSql.append("update po_line_item ");
		previousSql.append("set a_active_for_instrument= ?");
		previousSql.append(" where po_line_item_oid in "); 
		previousSql.append("(select previous_po_line_item_oid from po_line_item ");
		previousSql.append("where p_shipment_oid = ?");
        previousSql.append(" and a_active_for_instrument is not null)");

        // W Zhu 11/17/09 RAUJ103048141 BEGIN
        // Update the later PO to point its previous_po_line_item_oid to the previous one
        StringBuffer laterSql = new StringBuffer();
        laterSql.append("update po_line_item p1 ");
        laterSql.append("set p1.previous_po_line_item_oid = ");
        laterSql.append("(select previous_po_line_item_oid from po_line_item p2 ");
        laterSql.append("where p2.p_shipment_oid = ?");
        laterSql.append(" and p1.previous_po_line_item_oid = p2.po_line_item_oid) ");
        laterSql.append("where p1.previous_po_line_item_oid in ");
        laterSql.append("(select po_line_item_oid from po_line_item ");
        laterSql.append("where p_shipment_oid = ?");
        laterSql.append(")");
        // RAUJ103048141 END

        // This SQL deletes all of the POs that were associated to this transaction
		// that had been re-uploaded (and therefore had a previous po line item assoc)
		// Since the transaction is being deleted, we no longer need the data for
		// these POs
		StringBuffer deleteSql = new StringBuffer();
		deleteSql.append("delete from po_line_item ");
		deleteSql.append("where p_shipment_oid = ?");
		deleteSql.append(" and previous_po_line_item_oid is not null");

		// Unassign any POs that were part of this transaction and that did not have 
		// previous associations
		StringBuffer unassignSql = new StringBuffer();
		unassignSql.append("update po_line_item ");
		unassignSql.append("set a_assigned_to_trans_oid = null, a_active_for_instrument = null, p_shipment_oid = null ");
		unassignSql.append("where p_shipment_oid = ?");

		try {
		  int resultCount = 0;
		  resultCount = DatabaseQueryBean.executeUpdate(deleteManualSql.toString(), false, new Object[]{this.getAttribute("shipment_oid")}); 
		  resultCount = DatabaseQueryBean.executeUpdate(previousSql.toString(), false, instrumentOid, this.getAttribute("shipment_oid") ); 
          resultCount = DatabaseQueryBean.executeUpdate(laterSql.toString(), false, this.getAttribute("shipment_oid"), this.getAttribute("shipment_oid")); 
		  resultCount = DatabaseQueryBean.executeUpdate(deleteSql.toString(), false, new Object[]{this.getAttribute("shipment_oid")}); 
		  resultCount = DatabaseQueryBean.executeUpdate(unassignSql.toString(), false, new Object[]{this.getAttribute("shipment_oid")}); 

		} catch (java.sql.SQLException e) {
		  throw new AmsException("SQL Exception found executing sql statement \n"
	   + "\nException is " + e.getMessage());
		}
	  }

	/** ShilpaR CR707 Rel8.0 Update the PO status when shipment is deleted
	 * @param instrumentOid
	 * @param transactionOid
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void updateAssignedStrucPOStatus(String instrumentOid, String transactionOid,Map map) throws AmsException, RemoteException
	  {
		//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
		  String sql = "Select purchase_order_oid, purchase_order_type, previous_purchase_order_oid from PURCHASE_ORDER "
				 +"where a_shipment_oid = ?";

			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{this.getAttribute("shipment_oid")});
			//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
			
			if(result!=null){
			Vector rows = result.getFragments("/ResultSetRecord");
			 for (int j=0; j<rows.size(); j++) {
				 DocumentHandler row = (DocumentHandler) rows.elementAt(j);
				 String strucPOOid = row.getAttribute("/PURCHASE_ORDER_OID");
				 String strucPOType = row.getAttribute("/PURCHASE_ORDER_TYPE");
				 PurchaseOrder strucPO = (PurchaseOrder) createServerEJB(
							"PurchaseOrder", Long.parseLong(strucPOOid));
                 String poStatus = (String)map.get("status");
                 if (TradePortalConstants.PO_STATUS_CANCELLED.equals(poStatus)){
                     //RKAZI IR RRUM052453512 REL 8.1 10/17/2012 Start - When ISS/Amend is cancelled we need
                     //to set the instrument oid from current PO into previous PO for handling the upload of next
                     //AMEND PO.
                     String prevPOOid=row.getAttribute("/PREVIOUS_PURCHASE_ORDER_OID");
                     if(StringFunction.isNotBlank(prevPOOid)){
                     PurchaseOrder prevPO = (PurchaseOrder) createServerEJB(
                             "PurchaseOrder", Long.parseLong(prevPOOid));
                     prevPO.setAttribute("instrument_oid",strucPO.getAttribute("instrument_oid"));
                     prevPO.save();
                     }
                     //RKAZI IR RRUM052453512 REL 8.1 10/17/2012 End
                     if(TransactionType.ISSUE.equals((String) map.get("tranType"))){
                     strucPO.setAttribute("transaction_oid", null);
                     strucPO.setAttribute("instrument_oid", null);
                     strucPO.setAttribute("shipment_oid", null);
                     }
                 }
				//if amendment transaction is  cancelled by bank then clear out amend counter in PO
				 if(map.get("isAmended") != null && (Boolean) map.get("isAmended") && TransactionType.AMEND.equals(strucPOType)){
					//IR 13510 start- if po is amended and then canceled, set correct status and action
					// strucPO.setAttribute("action",(String)map.get("action"));  // DK IR T36000013510 Rel8.2 05/29/2012
					// strucPO.setAttribute("status",(String)map.get("status"));   // DK IR T36000013510 Rel8.2 05/29/2012
					 strucPO.setAttribute("action",(String)map.get("amendAction"));  
					 strucPO.setAttribute("status",(String)map.get("amendStatus"));   
					//IR 13510 end
					 strucPO.setAttribute("deleted_ind", TradePortalConstants.INDICATOR_YES);	
					 strucPO.setAttribute("amend_seq_no","0");
				 }
				 else{
                     strucPO.setAttribute("action",(String)map.get("action"));
                     strucPO.setAttribute("status",(String)map.get("status"));
				 }
				 if(StringFunction.isNotBlank((String) map.get("user"))) {
					strucPO.setAttribute("user_oid",(String)map.get("user"));
                 }
				 if(strucPO.save()!=1){
					 LOG.error(" Error Saving in updateAssignedStrucPOStatus from ShipmentTermsBean" );
				 }
			 }
		  }
	  }



          /**
           * Performs presave processing (regardless of whether validation is done)
           *
           * @exception com.amsinc.ecsg.frame.AmsException The exception description.
           */
          public void preSave() throws AmsException
          {
            String[] partyComponents = {"NotifyParty", "OtherConsigneeParty"};
            String[] parties = {"c_NotifyParty", "c_OtherConsigneeParty"};

            try {

              for (int i=0; i<parties.length; i++)
              {
                if (StringFunction.isBlank(this.getAttribute(parties[i])))
                {
                  deleteComponent(partyComponents[i]);
                }
              }

            } catch (RemoteException e) {
            	LOG.error("Remote Exception caught in ShipmentTerms.preSave()",e);
            }
          }

          /**
           * Called from one of the Terms Manager classes, this method validates that
           * particular shipment attributes contain a value
           *
           * @param requiredAttributes java.lang.String[]
           * @param shipmentNumber java.lang.String
           */
          public void validateRequiredAttributes(String[] requiredAttributes, String shipmentNumber) 
                    throws RemoteException, AmsException 
          {
            String aliasName;

            try {

              for (int i=0; i < requiredAttributes.length; i++)
              {
                // If the attribute is required and does not have a value issue an error
                if (StringFunction.isBlank(this.getAttribute(requiredAttributes[i])))
                {
                  // Use the alias name for the error message if it exists
                  aliasName = attributeMgr.getAlias(requiredAttributes[i]);
                  if (StringFunction.isBlank(aliasName))
                  {
                    aliasName = requiredAttributes[i];
                  }

                  String[] errorParameters = {aliasName, shipmentNumber};
                  boolean[] wrapParameters = {true, false};
                  this.getErrorManager().issueError (TradePortalConstants.ERR_CAT_1, TradePortalConstants.REQUIRED_ATTRIBUTE,
                                                     errorParameters, wrapParameters);
                }
                else 
                {
                  if (requiredAttributes[i].equals("description"))
                  {
	            String description = getAttribute("description");
	            String termsOid = this.getAttribute("terms_oid");

	            // Check if any shipments of the same transaction have matching descriptions
	            if (!isUnique( "description", description, " and P_TERMS_OID=" + termsOid, false ))
	            { 
                      String[] errorParameters = {shipmentNumber, description, attributeMgr.getAlias("description")};
                      boolean[] wrapParameters = {false, true, true};
	              this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                                                TradePortalConstants.SHIPMENT_ATTRIBUTE_EXISTS,
                                                        errorParameters,
                                                        wrapParameters);
                    }
                  }
                }
              }

            } catch (RemoteException e) {
                LOG.error("Remote Exception caught in ShipmentTerms.validateRequiredAttributes: ",e);
            }
          }

          /**
           * Returns the alias for an attribute on the shipment terms object.  
           * This allows the TermMgr beans to get the alias for error messages.
           *
           * @return java.lang.String
           * @param attributeName java.lang.String
           */
          public String getAlias(String attributeName)
          {
	    String alias = attributeMgr.getAlias(attributeName);
	    if (alias == null || alias.equals("")) alias = attributeName;
	     return alias;
          }

}
