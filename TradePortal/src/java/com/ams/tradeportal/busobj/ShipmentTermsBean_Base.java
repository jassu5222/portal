
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ShipmentTermsBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ShipmentTermsBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* shipment_oid - Unique identifier */
      attributeMgr.registerAttribute("shipment_oid", "shipment_oid", "ObjectIDAttribute");
      
      /* description - Description */
      attributeMgr.registerAttribute("description", "description");
      attributeMgr.registerAlias("description", getResourceManager().getText("ShipmentTermsBeanAlias.description", TradePortalConstants.TEXT_BUNDLE));
      
      /* creation_date - Timestamp (in GMT) of when the shipment was created.  Used to order them
         on the page. */
      attributeMgr.registerAttribute("creation_date", "creation_date", "DateTimeAttribute");
      
      /* trans_doc_consign_order_of_pty - The party to whom the goods are consigned. */
      attributeMgr.registerReferenceAttribute("trans_doc_consign_order_of_pty", "trans_doc_consign_order_of_pty", "CONSIGNMENT_PARTY_TYPE");
      attributeMgr.registerAlias("trans_doc_consign_order_of_pty", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_consign_order_of_pty", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_consign_to_pty - The party to whom the goods are consigned. */
      attributeMgr.registerReferenceAttribute("trans_doc_consign_to_pty", "trans_doc_consign_to_pty", "CONSIGNMENT_PARTY_TYPE");
      attributeMgr.registerAlias("trans_doc_consign_to_pty", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_consign_to_pty", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_consign_type - Indicates whether the goods are consigned directly to or are consigned to
         the order of */
      attributeMgr.registerReferenceAttribute("trans_doc_consign_type", "trans_doc_consign_type", "CONSIGNMENT_TYPE");
      attributeMgr.registerAlias("trans_doc_consign_type", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_consign_type", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_copies - The number of copies for the transport document */
      attributeMgr.registerAttribute("trans_doc_copies", "trans_doc_copies", "NumberAttribute");
      attributeMgr.registerAlias("trans_doc_copies", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_copies", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_included - Indicates whether or not transport documents data is entered. */
      attributeMgr.registerAttribute("trans_doc_included", "trans_doc_included", "IndicatorAttribute");
      attributeMgr.registerAlias("trans_doc_included", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_included", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_marked_freight - Indicates whether the freight is being paid by the Beneficiary (Prepaid)
         or by the Applicant (Collect). */
      attributeMgr.registerReferenceAttribute("trans_doc_marked_freight", "trans_doc_marked_freight", "MARKED_FREIGHT_TYPE");
      attributeMgr.registerAlias("trans_doc_marked_freight", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_marked_freight", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_originals - The number of originals for the transport document. */
      attributeMgr.registerReferenceAttribute("trans_doc_originals", "trans_doc_originals", "TRANS_DOC_ORIGINALS_TYPE");
      attributeMgr.registerAlias("trans_doc_originals", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_originals", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_text - The transport document text for the instrument. */
      attributeMgr.registerAttribute("trans_doc_text", "trans_doc_text");
      attributeMgr.registerAlias("trans_doc_text", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_text", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_doc_type - The type of transport document required by the instrument. For example,
         �Multi-Modal Transport Document or Port to Port B/L */
      attributeMgr.registerReferenceAttribute("trans_doc_type", "trans_doc_type", "TRANS_DOC_TYPE");
      attributeMgr.registerAlias("trans_doc_type", getResourceManager().getText("ShipmentTermsBeanAlias.trans_doc_type", TradePortalConstants.TEXT_BUNDLE));
      
      /* goods_description - The description of the goods being purchased. */
      attributeMgr.registerAttribute("goods_description", "goods_description");
      attributeMgr.registerAlias("goods_description", getResourceManager().getText("ShipmentTermsBeanAlias.goods_description", TradePortalConstants.TEXT_BUNDLE));
      
      /* incoterm - The shipping term, or Incoterm, that describes what is included in the total
         cost of the transaction goods. For example, Cost and Freight or Cost Insurance
         Freight. */
      attributeMgr.registerReferenceAttribute("incoterm", "incoterm", "INCOTERM");
      attributeMgr.registerAlias("incoterm", getResourceManager().getText("ShipmentTermsBeanAlias.incoterm", TradePortalConstants.TEXT_BUNDLE));
      
      /* incoterm_location - The location where the purchaser of the transaction goods will take responsibility
         for them. For example, the port of discharge for the goods */
      attributeMgr.registerAttribute("incoterm_location", "incoterm_location");
      attributeMgr.registerAlias("incoterm_location", getResourceManager().getText("ShipmentTermsBeanAlias.incoterm_location", TradePortalConstants.TEXT_BUNDLE));
      
      /* po_line_items - The purchase order line items assigned to the transaction. This read-only
         text area appears only if the organisation uses the system's purchase orders
         functionality and there are one or more PO line items are assigned to this
         transaction. */
      attributeMgr.registerAttribute("po_line_items", "po_line_items");
      attributeMgr.registerAlias("po_line_items", getResourceManager().getText("ShipmentTermsBeanAlias.po_line_items", TradePortalConstants.TEXT_BUNDLE));
      
      /* shipment_from - Place of Taking Charge/Dispatch From.../Receipt */
      attributeMgr.registerAttribute("shipment_from", "shipment_from");
      attributeMgr.registerAlias("shipment_from", getResourceManager().getText("ShipmentTermsBeanAlias.shipment_from", TradePortalConstants.TEXT_BUNDLE));
      
      /* shipment_to - Port of Discharge/Airport of Destination */
      attributeMgr.registerAttribute("shipment_to", "shipment_to");
      attributeMgr.registerAlias("shipment_to", getResourceManager().getText("ShipmentTermsBeanAlias.shipment_to", TradePortalConstants.TEXT_BUNDLE));
      
      /* transshipment_allowed - If this checkbox is selected, transhipment is allowed for this instrument.
         For example, if the goods can be shipped part of the way by ship and the
         remaining distance by truck. */
      attributeMgr.registerAttribute("transshipment_allowed", "transshipment_allowed", "IndicatorAttribute");
      attributeMgr.registerAlias("transshipment_allowed", getResourceManager().getText("ShipmentTermsBeanAlias.transshipment_allowed", TradePortalConstants.TEXT_BUNDLE));
      
      /* vessel_name_voyage_num - The vessel name and/or voyage number for the goods represented by the instrument */
      attributeMgr.registerAttribute("vessel_name_voyage_num", "vessel_name_voyage_num");
      attributeMgr.registerAlias("vessel_name_voyage_num", getResourceManager().getText("ShipmentTermsBeanAlias.vessel_name_voyage_num", TradePortalConstants.TEXT_BUNDLE));
      
      /* air_waybill_num - The number of the air waybill being released by the instrument. */
      attributeMgr.registerAttribute("air_waybill_num", "air_waybill_num");
      attributeMgr.registerAlias("air_waybill_num", getResourceManager().getText("ShipmentTermsBeanAlias.air_waybill_num", TradePortalConstants.TEXT_BUNDLE));
      
      /* bill_of_lading_num - The number of the bill of lading for �the instrument. */
      attributeMgr.registerAttribute("bill_of_lading_num", "bill_of_lading_num");
      attributeMgr.registerAlias("bill_of_lading_num", getResourceManager().getText("ShipmentTermsBeanAlias.bill_of_lading_num", TradePortalConstants.TEXT_BUNDLE));
      
      /* carrier_name_flight_num - The carrier name and/or flight number for the goods represented by the instrument. */
      attributeMgr.registerAttribute("carrier_name_flight_num", "carrier_name_flight_num");
      attributeMgr.registerAlias("carrier_name_flight_num", getResourceManager().getText("ShipmentTermsBeanAlias.carrier_name_flight_num", TradePortalConstants.TEXT_BUNDLE));
      
      /* container_number - The container number for the goods. */
      attributeMgr.registerAttribute("container_number", "container_number");
      attributeMgr.registerAlias("container_number", getResourceManager().getText("ShipmentTermsBeanAlias.container_number", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_addl_doc_included - Indicates whether there are additional transport documents for the instrument. */
      attributeMgr.registerAttribute("trans_addl_doc_included", "trans_addl_doc_included", "IndicatorAttribute");
      attributeMgr.registerAlias("trans_addl_doc_included", getResourceManager().getText("ShipmentTermsBeanAlias.trans_addl_doc_included", TradePortalConstants.TEXT_BUNDLE));
      
      /* trans_addl_doc_text - Additional transport documents for the instrument. */
      attributeMgr.registerAttribute("trans_addl_doc_text", "trans_addl_doc_text");
      attributeMgr.registerAlias("trans_addl_doc_text", getResourceManager().getText("ShipmentTermsBeanAlias.trans_addl_doc_text", TradePortalConstants.TEXT_BUNDLE));
      
      /* latest_shipment_date - The latest date that the goods can be shipped for the instrument. */
      attributeMgr.registerAttribute("latest_shipment_date", "latest_shipment_date", "DateTimeAttribute");
      attributeMgr.registerAlias("latest_shipment_date", getResourceManager().getText("ShipmentTermsBeanAlias.latest_shipment_date", TradePortalConstants.TEXT_BUNDLE));
      
      /* shipment_from_loading - Port of Loading/Airport of Departure */
      attributeMgr.registerAttribute("shipment_from_loading", "shipment_from_loading");
      attributeMgr.registerAlias("shipment_from_loading", getResourceManager().getText("ShipmentTermsBeanAlias.shipment_from_loading", TradePortalConstants.TEXT_BUNDLE));
      
      /* shipment_to_discharge - Place of Final Destination/Deliver/For Transport To */
      attributeMgr.registerAttribute("shipment_to_discharge", "shipment_to_discharge");
      attributeMgr.registerAlias("shipment_to_discharge", getResourceManager().getText("ShipmentTermsBeanAlias.shipment_to_discharge", TradePortalConstants.TEXT_BUNDLE));
      
        /* Pointer to the parent Terms */
      attributeMgr.registerAttribute("terms_oid", "p_terms_oid", "ParentIDAttribute");
   
      /* Pointer to the component TermsParty */
      /* NotifyParty -  */
      attributeMgr.registerAttribute("c_NotifyParty", "c_notify_party_oid", "NumberAttribute");
        
      /* Pointer to the component TermsParty */
      /* OtherConsigneeParty -  */
      attributeMgr.registerAttribute("c_OtherConsigneeParty", "c_other_consignee_party_oid", "NumberAttribute");
        
   }
   
  /* 
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {  
      /* Register the components defined in the Ancestor class */
      super.registerComponents();
      
      /* NotifyParty -  */
      registerOneToOneComponent("NotifyParty", "TermsParty", "c_NotifyParty");
   
      /* OtherConsigneeParty -  */
      registerOneToOneComponent("OtherConsigneeParty", "TermsParty", "c_OtherConsigneeParty");
   
      /* POLineItemList - This association indicates to which association a PO is assoicated. */
      registerOneToManyComponent("POLineItemList","POLineItemList");
   }

 
   
 
 
   
}
