
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * As organizations are added to the Trade Portal, a corresponding entry must
 * be made using the Supervisor software and stored in the reports repository.
 * Supervisor sets permissions for each organization.   
 * 
 * When a corporate organization or bank organization group is added to the
 * portal, an entry is placed into this table containing the commands that
 * Supervisor needs to run in order to add the organization to the reports
 * repository.   A nightly job on the reports server will examine this table,
 * run the command on Supervisor, and then delete the rows from this table.
 * 
 * Supervisor users for the global organization or for the client bank organizations
 * must be added manually; they are not added through placing commands in this
 * table.
 * 
 * For more information on the use of this object, please see the documentation
 * of how the Trade Portal is integrated with reporting.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SupervisorCommandHome extends EJBHome
{
   public SupervisorCommand create()
      throws RemoteException, CreateException, AmsException;

   public SupervisorCommand create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
