

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The business calendar that stores the holiday information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TPCalendar extends TradePortalBusinessObject
{   
         public boolean isWeekend(java.util.Calendar date) throws AmsException, RemoteException;
         public TPCalendarYear getTPCalendarYear(int year) throws AmsException, RemoteException;


}
