



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * The holiday informatino for any given year of a TPCalendar.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface TPCalendarYear extends TradePortalBusinessObject
{
	public String getNextBusinessDay(String strInputDate, int offsetDays, int weekend1, int weekend2)
	throws RemoteException, AmsException;
                 
        public boolean isHoliday(java.util.Calendar date) throws AmsException, RemoteException;


}
