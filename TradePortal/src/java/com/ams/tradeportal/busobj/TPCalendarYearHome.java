
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The holiday informatino for any given year of a TPCalendar.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TPCalendarYearHome extends EJBHome
{
   public TPCalendarYear create()
      throws RemoteException, CreateException, AmsException;

   public TPCalendarYear create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
