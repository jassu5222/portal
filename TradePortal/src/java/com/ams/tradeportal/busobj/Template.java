package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Represents an instrument template.  The data for an instrument template
 * is stored in the TemplateInstrument business object.  This business object
 * contains the characteristics of the template from a reference data point
 * of view (who owns it, its name, whether or not it is express), where as
 * the TemplateInstrument actually contains the data that comprises the template.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface Template extends ReferenceData
{   
	public com.amsinc.ecsg.util.DocumentHandler createNew(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException, AmsException;

}
