package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;




/*
 *
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TemplateBean extends TemplateBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(TemplateBean.class);


   public void userDelete() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		// Users cannot delete default templates
	if( this.getAttribute("default_template_flag").equals(TradePortalConstants.INDICATOR_YES) )
		 {
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DELETE_DEFAULT_TEMPLATE);
		 }

	// check if there are any lc creation rules
	if (isAssociated("LCCreationRule", "byTemplate",
		new String[] {getAttribute("template_oid")})) {
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			    TradePortalConstants.ACTIVE_LC_CREATION_RULE, getAttribute("name"));
		}


	}




	public void userValidate() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
	{
		LOG.debug("entering userValidate");

		if (this.hasChangesPending())
		{
			LOG.debug("hasChangesPending == true");

	        String name  = getAttribute("name");
	        String owner = this.getAttribute("owner_org_oid");
	        String where = " and p_owner_org_oid=" + owner;

	        LOG.debug("name ->  {}" , name);
	        LOG.debug("owner -> {}" , owner);
	        LOG.debug("where -> {}" , where);

	        if (!isUnique("name",name,where))
	        {
	            
	        	LOG.debug("isUnique == false");
		        this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                                      TradePortalConstants.ALREADY_EXISTS,
			                                      name,
			                                      attributeMgr.getAlias("name"));
		    }

	        //Vshah/IAZ - IR#PKUK092358952 10/13/10 - [START]
	        //If trying to save to a template group, validate confidential indicator
	        //of this group - even if a newly created template
	        //if(!this.isNew)
	        //{
	        	String paymentTempOid = getAttribute("payment_templ_grp_oid");
	        	String confidentialInd = getAttribute("confidential_indicator");
	        	if (StringFunction.isBlank(confidentialInd))
	        		confidentialInd = TradePortalConstants.INDICATOR_NO;

	        	if (StringFunction.isNotBlank(paymentTempOid))
	        	{
	        		// Check if group to which we are saving to save
	        		// has any templates with different confidential status
	        		// if more than 1 of those, generate an error
	        	    // LSuresh R91 IR T36000026319 - SQLINJECTION FIX
	        		String selectClause = null;
	        		String fromClause = null;
	        		StringBuilder whereClause  = new StringBuilder();

	        		selectClause = "template_oid";
	        		fromClause   = "template";

	        		whereClause.append("payment_templ_grp_oid = ?");
	        		
	        		whereClause.append(" AND ");
	        		whereClause.append("confidential_indicator != ?");

	        		int recordCount = DatabaseQueryBean.getCount( selectClause, fromClause, whereClause.toString(),false,paymentTempOid,confidentialInd);
	        		boolean wrongConfGroup = false;
	        		if (recordCount > 1)
	        		{
	        			wrongConfGroup = true;
	        		}
	        		else if (recordCount == 1)
	        		{
	        			//if group to which we are trying to save, has one template with
	        			//a different conf ind, check if this is teh template we are saving (and changing conf ind)
	        			//if so, proceed with save, if not - generate an error.
	        			// LSuresh R91 IR T36000026319 - SQLINJECTION FIX
	        			whereClause.append(" AND ");
	        			whereClause.append("template_oid =  ?");
	        			
	        		
	        			LOG.debug("second call {}" , whereClause);
	        			if(DatabaseQueryBean.getCount( selectClause, fromClause, whereClause.toString(),false,new Object[]{getAttribute("template_oid")}) < 1)
	        			{
	        				wrongConfGroup = true;
	        			}
	        		}

	        		if (wrongConfGroup)
	        		{
	        			ResourceManager   resourceManager  = null;
	        			resourceManager = this.getResourceManager();

	        			PaymentTemplateGroup paymentTempGroup = null;
	        			paymentTempGroup = (PaymentTemplateGroup) this.createServerEJB("PaymentTemplateGroup", Long.parseLong(paymentTempOid));
	        			String groupName = paymentTempGroup.getAttribute("name");

	        			LOG.debug("Confidential and Non Confidential Payment Templates cannot be in the same group");
	        			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	        			TradePortalConstants.PAYMENT_TEMPLATES_CANNOT_BE_IN_SAME_GROUP,
	        					groupName,
	        					confidentialInd.equals(TradePortalConstants.CONF_IND_FRM_TMPLT) ? resourceManager.getText("TemplateDetail.NonConfidential", TradePortalConstants.TEXT_BUNDLE) : resourceManager.getText("TemplateDetail.Confidential", TradePortalConstants.TEXT_BUNDLE));
	        		}

	        	}
	        //}
	        //Vshah/IAZ - IR#PKUK092358952 10/13/10 - [END]


	    }
	}




  /**
   * This function creates a new template.  A template instrumetn and transaction
   * are also created.
   *
   * @param DocumentHandler doc - this is a DocumentHandler stored as a char array
   * @return String - the oid of the transaction created from that you can get the instrument and terms
   */
  public DocumentHandler createNew (DocumentHandler doc) throws RemoteException, AmsException
  {
	LOG.debug("entering createNew()");

	String ownerLevel       = doc.getAttribute("/ownerLevel"); //the level of the user's owner org.
	String instrumentType   = doc.getAttribute("/instrumentType"); //type of the template instrument
	String express          = doc.getAttribute("/isExpress"); //tells is the template is express
	String fixedFlag        = doc.getAttribute("/isFixed"); //IAZ CR-586 08/16/10 tells is the template is fixed payments
	String clientBankOid    = doc.getAttribute("/clientBankOid"); //the oid of the user's client bank
	String copyFrom         = doc.getAttribute("/copyType"); //the type of object (instrument, template, or default template (blank)) from which we will copy
	String templateOid      = null;
	String transactionOid   = null;
	String logicalType      = null;

	boolean isDefault       = false;

	ClientBank clientBank   = null;


	// Create the template adn set several attributes.
	this.newObject();
	this.setAttribute("name", doc.getAttribute("/name"));
	this.setAttribute("ownership_level", ownerLevel);

	// Set ownership type (used by the Added by column on the listview)
	if(ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE))
		this.setAttribute("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
	else
		this.setAttribute("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);

	this.setAttribute("owner_org_oid", doc.getAttribute("/ownerOrg"));
	if (express != null && express.equals("on"))
		this.setAttribute("express_flag", TradePortalConstants.INDICATOR_YES);
	else
		this.setAttribute("express_flag", TradePortalConstants.INDICATOR_NO);

	//IAZ CR-586 08/16/10 Begin
	if (fixedFlag != null && fixedFlag.equals("on"))
		this.setAttribute("fixed_flag", TradePortalConstants.INDICATOR_YES);
	else
		this.setAttribute("fixed_flag", TradePortalConstants.INDICATOR_NO);
	//IAZ CR-586 08/16/10 End

	/*
	 To determine if this will be the default template we must know if it
	 is the first template of this type that has been created for this
	 client bank.  I  only check for this if the user selected to create
	 the template from a blank form because no instruments or templates can
	 exist to be copied from if a default template does not exist.  This
	 is enforced by the user interface and by the back end.
	*/
	if (copyFrom.equals(TradePortalConstants.FROM_BLANK))
	{
		//get the client bank so that we can get the default template
		LOG.debug("Getting clientBank");
		clientBank = (ClientBank) createServerEJB("ClientBank");
		clientBank.getData(Long.parseLong(clientBankOid));

		// When getting the default template, use the logical instrument
		// type.  (Therefore, a Detailed SLC is logically a Guarantee)
		logicalType = InstrumentServices.getLogicalInstrumentType(instrumentType);
		LOG.debug("Getting default template for instrument type {} which is logically a {}" ,instrumentType,logicalType);

		templateOid = clientBank.getDefaultTemplateOid(logicalType);

		if (StringFunction.isBlank(templateOid))
		{
			if (ownerLevel.equals(TradePortalConstants.OWNER_BANK))
			{
				isDefault = true;
				this.setAttribute("default_template_flag", TradePortalConstants.INDICATOR_YES);
				LOG.debug("No default template exists. Client bank customer is creating a default template");
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											 TradePortalConstants.NTM_NO_DEFAULT_TEMPLATE_BANK);
			}
			else
			{
				//ERROR
				LOG.debug("No default template exists. Corporate customer cannot create template");
				getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.NTM_NO_DEFAULT_TEMPLATE_CORP);
				return new DocumentHandler();
			}
		}
		else
		{
			this.setAttribute("default_template_flag", TradePortalConstants.INDICATOR_NO);
			//store the default template oid so I don't have to look it up in template instrument bean
			doc.setAttribute("/instrumentOid", templateOid);
		}
	}
	else
		this.setAttribute("default_template_flag", TradePortalConstants.INDICATOR_NO);

	//Create the template instrument component
	long instrOid = this.newComponent("TemplateInstrument");
	TemplateInstrument newInst = (TemplateInstrument) this.getComponentHandle("TemplateInstrument");
	if (isDefault)
	{
		transactionOid = newInst.createNewDefault(doc);
		// Again, when setting the default template based on the instrument
		// type, use the logical instrument type
		clientBank.setDefaultTemplateOid(logicalType,
										this.getAttribute("template_oid"));

                // Save the client bank - There's no need to do validations
		clientBank.save(false);
	}
	else
	{
		transactionOid = newInst.createNew(doc);

		//IAZ CR-586 IR-PRUK092452162 09/29/10 Begin
		//           Set Confidential Indicator as needed
		if (StringFunction.isNotBlank(doc.getAttribute("/NewTemplate/confidential_indicator")))
		{
			LOG.debug("in bean {}" , doc.getAttribute("/NewTemplate/confidential_indicator"));
			this.setAttribute("confidential_indicator", doc.getAttribute("/NewTemplate/confidential_indicator"));
		}
		//IAZ CR-586 IR-PRUK092452162 09/29/10 End
	}

	//we're done with this doc, lets reuse it.
	doc.removeAllChildren("/DocRoot");
	doc.setAttribute("/Template/oid", this.getAttribute("template_oid"));
	doc.setAttribute ("/Transaction/oid", transactionOid);

	// Call userValidate to check for the uniqueness of the template name
	// We can't call this.save(true) because it was also validate the required
	// fields on the template instrument.

    //VS CR-586 08/16/10 Begin - store template group
    this.setAttribute("payment_templ_grp_oid", doc.getAttribute("/payment_templ_grp_oid"));
    //VS CR-586 08/16/10 End - store template group

	this.userValidate();

	LOG.debug("Saving template with oid ' {} '.....", this.getAttribute("template_oid"));
	this.save(false);
	LOG.debug("template saved");

	return doc;
  }


}