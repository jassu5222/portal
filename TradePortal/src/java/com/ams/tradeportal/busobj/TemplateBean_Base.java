


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Represents an instrument template.  The data for an instrument template
 * is stored in the TemplateInstrument business object.  This business object
 * contains the characteristics of the template from a reference data point
 * of view (who owns it, its name, whether or not it is express), where as
 * the TemplateInstrument actually contains the data that comprises the template.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TemplateBean_Base extends ReferenceDataBean
{
private static final Logger LOG = LoggerFactory.getLogger(TemplateBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* template_oid - Unique identifier */
      attributeMgr.registerAttribute("template_oid", "template_oid", "ObjectIDAttribute");

      /* express_flag - Indicates whether or not the template is an express template.   When instruments
         are created from express templates, certain fields are read-only.  This
         is to allow for straight-through processing of LCs without human involvement.

         Yes=template is an express template */
      attributeMgr.registerAttribute("express_flag", "express_flag", "IndicatorAttribute");

      /* default_template_flag - Indicates whether or not a default template exists.  A default template
         is the template that is used when the user indicates that they want to create
         a transaction from a "blank form".

         Yes = this template is a default template */
      attributeMgr.registerAttribute("default_template_flag", "default_template_flag", "IndicatorAttribute");

        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

      /* Pointer to the component TemplateInstrument */
      /* TemplateInstrument - The instrument data that represents the real data for the template.  This
         association is used to get from the name of a template to the data that
         is being copied from. */
      attributeMgr.registerAttribute("c_TemplateInstrument", "c_TEMPLATE_INSTR_OID", "NumberAttribute");

      /* fixed_flag - Indicates whether or not the template is a fixed payment template.

         Yes=template is a fixed payment template */
      attributeMgr.registerAttribute("fixed_flag", "fixed_flag", "IndicatorAttribute");

      /* confidential_ind - Indicates whether or not the template is confidential.

         Yes=template is a confidential template */
      attributeMgr.registerReferenceAttribute("confidential_indicator", "confidential_indicator", "CONFIDENTIAL_IND");
      //attributeMgr.registerAttribute("confidential_indicator", "confidential_indicator", "IndicatorAttribute");

      /* Payment_Templ_Grp_OID - Payment Template Group */
      attributeMgr.registerAttribute("payment_templ_grp_oid", "Payment_Templ_Grp_OID", "NumberAttribute");

   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* TemplateInstrument - The instrument data that represents the real data for the template.  This
         association is used to get from the name of a template to the data that
         is being copied from. */
      registerOneToOneComponent("TemplateInstrument", "TemplateInstrument", "c_TemplateInstrument");
      }






}
