package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Subclass of InstrumentData that represents a template's data in the database
 * (in the INSTRUMENT table).  There is another business object, Template,
 * that represents the reference data entry for a template.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TemplateInstrument extends InstrumentData
{
	public String createNew(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException, AmsException;

	public String createNewDefault(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException, AmsException;

}
