package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * The contractual terms that comprise a transaction.  Data entered by the
 * user on the transaction forms pages will be stored in the Terms business
 * object.
 * 
 * Attributes for this business object are dynamically registered.  That is,
 * all of the attributes defined here are not registered for all instances.
 * Terms objects have a terms manager object associated with them.  The terms
 * manager object determines which attributes should be registered and handles
 * any instance-specific method calls.
 * 
 * For more information, see the "Terms Manager Delegation" section of the
 * Trade Portal Infrastructure Document.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TermsHome extends EJBHome
{
   public Terms create()
      throws RemoteException, CreateException, AmsException;

   public Terms create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
