

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A business (coporate or bank) that is a party to an instrument.   The terms_party_type
 * attribute distinguishes between the different  types of parties that can
 * be associated to an instrument.
 * 
 * When a party is referenced in the Trade Portal by using the Party Search
 * page, data is copied from a 'Party' business object into a 'TermsParty'
 * business object.  Because of this, many of the attributes of this business
 * object are similar to the attributes of the Party business object.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TermsParty extends TradePortalBusinessObject
{   
	public String getAlias(java.lang.String attributeName) throws RemoteException, AmsException;

	public ResourceManager getResourceManager() throws RemoteException;

	public void populateFirstTermsParty(com.ams.tradeportal.busobj.CorporateOrganization corp, java.lang.String instrType) throws RemoteException, AmsException;

	public void copy(com.ams.tradeportal.busobj.TermsParty source) throws RemoteException, AmsException;

	public com.amsinc.ecsg.util.DocumentHandler packageTermsPartyAttributes(int tpid, com.amsinc.ecsg.util.DocumentHandler termsPartyDoc) throws RemoteException, AmsException;

	public void copyFromParty(com.ams.tradeportal.busobj.Party source) throws RemoteException, AmsException;

	public com.amsinc.ecsg.util.DocumentHandler packageShipmentTermsPartyAttributes(int shipmentIndex, java.lang.String partyType, com.amsinc.ecsg.util.DocumentHandler termsDoc) throws RemoteException, AmsException;

	public void populateSixthTermsParty(com.ams.tradeportal.busobj.CorporateOrganization corp, java.lang.String instrType) throws RemoteException, AmsException;

    //cquinton 4/8/2011 Rel 7.0.0 ir#bkul040547648 add populateTermsParty
    public void populateTermsParty(CorporateOrganization corp, String partyType) throws RemoteException, AmsException;

}
