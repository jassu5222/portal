
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A business (coporate or bank) that is a party to an instrument.   The terms_party_type
 * attribute distinguishes between the different  types of parties that can
 * be associated to an instrument.
 * 
 * When a party is referenced in the Trade Portal by using the Party Search
 * page, data is copied from a 'Party' business object into a 'TermsParty'
 * business object.  Because of this, many of the attributes of this business
 * object are similar to the attributes of the Party business object.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TermsPartyBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(TermsPartyBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* terms_party_oid - Unique identifier */
      attributeMgr.registerAttribute("terms_party_oid", "terms_party_oid", "ObjectIDAttribute");
      
      /* terms_party_type - The type of terms party.  This type describes the relationship of the party
         to the transaction.   Examples of terms party types are applicant, beneficiary,
         advising bank, etc. 
         
         Each of the terms parties that are related to a particular transaction will
         have different terms party types. */
      attributeMgr.registerReferenceAttribute("terms_party_type", "terms_party_type", "TERMS_PARTY_TYPE");
      attributeMgr.requiredAttribute("terms_party_type");
      
      /* name - Name of the party. */
      attributeMgr.registerAttribute("name", "name");
      attributeMgr.registerAlias("name", getResourceManager().getText("TermsPartyBeanAlias.name", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_1 - First line of the address for this party */
      attributeMgr.registerAttribute("address_line_1", "address_line_1");
      attributeMgr.registerAlias("address_line_1", getResourceManager().getText("TermsPartyBeanAlias.address_line_1", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_2 - Second line of the address for this party */
      attributeMgr.registerAttribute("address_line_2", "address_line_2");
      attributeMgr.registerAlias("address_line_2", getResourceManager().getText("TermsPartyBeanAlias.address_line_2", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_line_3 - Third line of the address for this party.
         
         This attribute will only be populated when the state, province, country
         and postal code attributes are not.   Some party entry fields allow for
         the entry of state/province, etc whereas some allow only for the entry of
         address_line_3.    When copying from a reference data Party object, state/province,
         country and postal code are combined together to form address line 3 if
         appropriate. */
      attributeMgr.registerAttribute("address_line_3", "address_line_3");
      attributeMgr.registerAlias("address_line_3", getResourceManager().getText("TermsPartyBeanAlias.address_line_3", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_city - City of the party */
      attributeMgr.registerAttribute("address_city", "address_city");
      attributeMgr.registerAlias("address_city", getResourceManager().getText("TermsPartyBeanAlias.address_city", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_state_province - State or province where the party is located. */
      attributeMgr.registerAttribute("address_state_province", "address_state_province");
      attributeMgr.registerAlias("address_state_province", getResourceManager().getText("TermsPartyBeanAlias.address_state_province", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_country - Country  where the party is located. */
      attributeMgr.registerReferenceAttribute("address_country", "address_country", "COUNTRY");
      attributeMgr.registerAlias("address_country", getResourceManager().getText("TermsPartyBeanAlias.address_country", TradePortalConstants.TEXT_BUNDLE));
      
      /* address_postal_code - Postal code  where the party is located. */
      attributeMgr.registerAttribute("address_postal_code", "address_postal_code");
      attributeMgr.registerAlias("address_postal_code", getResourceManager().getText("TermsPartyBeanAlias.address_postal_code", TradePortalConstants.TEXT_BUNDLE));
      
      /* phone_number - Phone number of this party */
      attributeMgr.registerAttribute("phone_number", "phone_number");
      attributeMgr.registerAlias("phone_number", getResourceManager().getText("TermsPartyBeanAlias.phone_number", TradePortalConstants.TEXT_BUNDLE));
      
      /* OTL_customer_id - The unique identifier of this party on OTL, the back end system.   This
         value is only entered directly on the Party Detail page.  It would only
         get populated in a TermsParty object if the user searches for a party using
         the Party Search page.
         
         Care must be taken to clear out this field when appropriate.  For example,
         if a user references a party in a JSP, this copies all of the fields for
         the party (including otl_customer_id) to the terms party.  If the user then
         changes the name of the terms party, the otl_customer_id is no longer valid
         (since the user is now referring to a different party).  In this scenario,
         the otl_customer_id must be set to null. */
      attributeMgr.registerAttribute("OTL_customer_id", "OTL_customer_id");
      
      /* branch_code - Branch code of this party */
      attributeMgr.registerAttribute("branch_code", "branch_code");
      
      /* contact_name - Contact name for this party */
      attributeMgr.registerAttribute("contact_name", "contact_name");
      attributeMgr.registerAlias("contact_name", getResourceManager().getText("TermsPartyBeanAlias.contact_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* acct_num - Account number */
      attributeMgr.registerAttribute("acct_num", "acct_num");
      attributeMgr.registerAlias("acct_num", getResourceManager().getText("TermsPartyBeanAlias.acct_num", TradePortalConstants.TEXT_BUNDLE));
      
      /* acct_currency - The currency of the account */
      attributeMgr.registerReferenceAttribute("acct_currency", "acct_currency", "CURRENCY_CODE");
      attributeMgr.registerAlias("acct_currency", getResourceManager().getText("TermsPartyBeanAlias.acct_currency", TradePortalConstants.TEXT_BUNDLE));
      
      /* acct_choices - HTML (or some other format) representing the choices available in the account
         list dropdown for this terms party */
      attributeMgr.registerAttribute("acct_choices", "acct_choices");
      
      /* address_seq_num - Sequence Number of Address (1-99) */
      attributeMgr.registerAttribute("address_seq_num", "address_seq_num", "NumberAttribute");
      
      /* address_search_indicator - Indicates whether Search for an Address button should be displayed.  Defaults
         to Yes. */
      attributeMgr.registerAttribute("address_search_indicator", "address_search_indicator", "IndicatorAttribute");
      
      /* vendor_id - A 15 character alphanumeric value representing the vendor ID associated with this party.
       *             The vendor_id for the counter-party is saved to instrument.vendor_id when the transaction
       *             is saved.  This is a hidden value on the transaction screens.  The unpackager/packager
       *             will update/use instrument.vendor_id and not the values that exist on terms_party */
      attributeMgr.registerAttribute("vendor_id", "vendor_id");
      
      attributeMgr.registerAttribute("central_bank_rep1", "central_bank_rep1");
      attributeMgr.registerAttribute("central_bank_rep2", "central_bank_rep2");
      attributeMgr.registerAttribute("central_bank_rep3", "central_bank_rep3");
      attributeMgr.registerAttribute("customer_reference", "customer_reference");
      attributeMgr.registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");//CR 709A
      /* Rpasupulati adding reporting code 1 and 2 CR 1001 Start*/
      attributeMgr.registerAttribute("reporting_code_1", "reporting_code_1");
      attributeMgr.registerAttribute("reporting_code_2", "reporting_code_2");
      /* Rpasupulati adding reporting code 1 and 2 CR 1001 End*/
      
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
      //added attributes for Portal only Bank Additional address user defined fields
      attributeMgr.registerAttribute("user_defined_field_1", "user_defined_field_1");
      attributeMgr.registerAttribute("user_defined_field_2", "user_defined_field_2");
      attributeMgr.registerAttribute("user_defined_field_3", "user_defined_field_3");
      attributeMgr.registerAttribute("user_defined_field_4", "user_defined_field_4");
      //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End

      
   }
   
 
   
 
 
   
}
