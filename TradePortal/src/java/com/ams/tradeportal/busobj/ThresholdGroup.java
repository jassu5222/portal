package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * A threshold group is a set of threshold settings.  There are two types of
 * thresholds: a LC threshold and a daily limit threshold.  An LC threshold
 * a rule that indicates the maximum value of an LC that the user can authorize.
 * A daily limit threshold is the maximum total value that a user can authorize
 * in a single day.
 * 
 * An LC threshold and a daily threshold exists for every type of instrument
 * type / transaction type combination that can be created in the Trade Portal.

 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface ThresholdGroup extends TradePortalBusinessObject
{   
	public java.lang.String getDailyLimit(java.lang.String instrumentType, java.lang.String transactionType) throws RemoteException, AmsException;

	public java.lang.String getThresholdAmount(java.lang.String instrumentType, java.lang.String transactionType) throws RemoteException, AmsException;

}
