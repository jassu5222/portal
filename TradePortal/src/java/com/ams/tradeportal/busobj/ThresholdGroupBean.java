
package com.ams.tradeportal.busobj;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TradePortalDecimalAttribute;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.GenericList;
import com.amsinc.ecsg.util.StringFunction;





/*
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ThresholdGroupBean extends ThresholdGroupBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupBean.class);


  /**
   * Descendent implemented method that identifies listing object
   * criteria.
   *
   * Used to identify one or more listTypes and list processing criteria.
   *
   * @param listTypeName The name of the listtype (as specified by the
   * developer)
   * @return The qsuedo SQL used for specifying the list criteria.
   *
   * Acceptable listTypes are:
   * byNameAndOid - search threshold groups with the given name, parent corp org,
   *		    and exclude the given threshold group oid
   */

   public String getListCriteria(String listTypeName)
   {
	return "";
   }

  /**
   * Performs any special "validate" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method verifies that threshold group name is unique
   * for the Client Bank and all amounts have the correct precision
   *
   * @see #validate()
   */

   protected void userValidate() throws AmsException, RemoteException
   {
	/**
	 Verify uniqueness of threshold_group_name
	*/
	CorporateOrganization org;
	String baseCurrency;
	Hashtable attributes;
	Enumeration enumer;
	
    	// verify that the threshold group name is unique

	String name = getAttribute("threshold_group_name");
  	if (!isUnique("threshold_group_name", name,
	    " and p_corp_org_oid = " + getAttribute("corp_org_oid")))
	{
	    this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	           TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("threshold_group_name"));
	}

//IR-PAUI110551807 Pratiksha 11/14/2008 Begin
  	// 	The approve discount amount cannot be greater then the response amount.  If the approve discount amount is
  	//great than the match response amount an error will be given at save: �Approve discount amount must be less
  	// than the Match response amount.�

	try {
		String matchresthold = getAttribute("ar_match_response_thold");
		String appdisthold = getAttribute("ar_approve_discount_thold");
		String matchresdlimit = getAttribute("ar_match_response_dlimit");
		String appdisdlimit = getAttribute("ar_approve_discount_dlimit");

		if (StringFunction.isBlank(matchresthold) && StringFunction.isBlank(appdisthold) &&
		StringFunction.isBlank(matchresdlimit) && StringFunction.isBlank(appdisdlimit)) {
			// Both are blank.  This is OK.
		} else{
			if (!StringFunction.isBlank(matchresthold) && !StringFunction.isBlank(appdisthold)) {
			// Both are not blank so test for valid numbers.

			BigDecimal appdistholdAmount;
			BigDecimal matchrestholdAmount;

			appdistholdAmount = getAttributeDecimal("ar_approve_discount_thold");
			matchrestholdAmount = getAttributeDecimal("ar_match_response_thold");

			if (appdistholdAmount.compareTo(matchrestholdAmount) > 0) {

				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CANNOT_BE_GREATER);
			}
		}
		if (!StringFunction.isBlank(matchresdlimit) && !StringFunction.isBlank(appdisdlimit)) {
			// Both are not blank so test for valid numbers.

			BigDecimal appdisdlimitAmount;
			BigDecimal matchresdlimitAmount;

			appdisdlimitAmount = getAttributeDecimal("ar_approve_discount_dlimit");
			matchresdlimitAmount = getAttributeDecimal("ar_match_response_dlimit");

			if (appdisdlimitAmount.compareTo(matchresdlimitAmount) > 0) {

				this.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CANNOT_BE_GREATER);
			}
		  }
		}
	}catch (AmsException e) {
		//	In this case, skip over the validation.
	}
	//IR-PAUI110551807 Pratiksha 11/14/2008 End


        // verify that every amount has the correct precision for the given
	// currency the currency for all amounts in threshold groups is
	// the corporate org's base currency.
	// this has to be performed here since it is the only place where the currency
	// and amount is known

        org = (CorporateOrganization) createServerEJB("CorporateOrganization", Long.parseLong(getAttribute("corp_org_oid")));

	baseCurrency = org.getAttribute("base_currency_code");

    attributes = getAttributeHash();
	enumer = attributes.keys();

	while (enumer.hasMoreElements())
	{
	    String attrName = (String) enumer.nextElement();
	    if ((attributes.get(attrName)) instanceof TradePortalDecimalAttribute)
	    {
                // Validate that the amount will fit in the database and that the
                // currency precision is correct
                TPCurrencyUtility.validateAmount(baseCurrency, getAttribute(attrName), attributeMgr.getAlias(attrName), this.getErrorManager() );
	    }

	}
   }


  /**
   * This method is called in the business object
   * method delete.
   *
   * Any processing that is necessary before an object is deleted should
   * be placed within this method.  If business object errors are issued
   * within this method, delete processing will not continue and the
   * delete transaction will be rolled back.
   *
   * If the Threshold Group has any active active users, issue error and
   * do not delete
   *
   * @see #delete()
   */

   protected void userDelete() throws AmsException, RemoteException
   {
       /**
	Check users for references to this threshold group
	*/
	// scan the list of users for threshold group
	GenericList usersList;
	super.userDelete();
	usersList = (GenericList)EJBObjectFactory.createServerEJB(getSessionContext(),
		"GenericList");
	usersList.prepareList("User", "activeByThresholdGroupOid",
		new String[]{getAttribute("threshold_group_oid"), TradePortalConstants.ACTIVE});
	usersList.getData();
	if (usersList.getObjectCount() > 0) this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		TradePortalConstants.CURRENTLY_ASSIGNED, new String[] {getAttribute("threshold_group_name")});

	try
	{
	    usersList.remove();
	}
	catch (javax.ejb.RemoveException e)
	{
		LOG.error("ThresholdGroupBean:: RemoveException at userDelete(): ",e);
	}
   }

  /**
   * This method is called to retrieve the Threshold Amount based on
   * the instrument type and transaction type
   *
   * @param instrumentType 	String - the instrument type
   * @param transactionType	String - the transaction type
   * @return String	- the threshold amount in String
   *
   */

   public String getThresholdAmount(String instrumentType, String transactionType)
	throws AmsException, RemoteException
   {
	String attrName = getAttributeName(instrumentType, transactionType) + "thold";

	// verify that the attribute name exists
	Hashtable attrHash = attributeMgr.getAttributeHashtable();
	if (!attrHash.containsKey(attrName))
		throw new AmsException("Invalid instrument type / transaction type combination: " +
		instrumentType + "/" + transactionType + ".");

	return getAttribute(attrName);
   }

  /**
   * This method is called to retrieve the Daily Limit amount based on
   * the instrument type and transaction type
   *
   * @param instrumentType 	String - the instrument type
   * @param transactionType	String - the transaction type
   * @return String	- the daily limit amount in String
   *
   */
   public String getDailyLimit(String instrumentType, String transactionType)
	throws AmsException, RemoteException
   {
	String attrName;

	// specify the threshold attributes
	// if the instrument type is Export LC and
	// the transaction type is Transfer, specify a different attribute name
	// due to database name length limit

	if (instrumentType.equals(InstrumentType.EXPORT_DLC) &&
	    transactionType.equals(TransactionType.AMEND_TRANSFER) )
	{
	    attrName = "export_LC_amend_transfer_dlim";
	}
	else
	    attrName = getAttributeName(instrumentType, transactionType)
		+ "dlimit";

	// verify that the attribute name exists
	Hashtable attrHash = attributeMgr.getAttributeHashtable();
	if (!attrHash.containsKey(attrName))
		throw new AmsException("Invalid instrument type / transaction type combination: " +
		instrumentType + "/" + transactionType + ".");

	return getAttribute(attrName);

   }

  /**
   * This method is called to provide the Threshold Group attribute name
   * for threshold amounts and daily limits based on the instrument type
   * and transaction type.
   *
   * @param instrumentType 	String - the instrument type
   * @param transactionType	String - the transaction type
   * @return String	- the prefix attribute name for threshold and
   *   			  daily limit amounts
   *
   */

   protected String getAttributeName(String instrumentType,
	String transactionType)
	throws AmsException, RemoteException
   {
	StringBuffer attributeName = new StringBuffer();

	// construct the attribute name based on the instrument type
	// and transaction type

	if (instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || 
			instrumentType.equals(InstrumentType.DEFERRED_PAY) )
	{
	    attributeName.append("import_LC_");
	}
	else if (instrumentType.equals(InstrumentType.STANDBY_LC))
	{
	    attributeName.append("standby_LC_");
	}
	else if (instrumentType.equals(InstrumentType.EXPORT_DLC))
	{
	    attributeName.append("export_LC_");
	}
	else if (instrumentType.equals(InstrumentType.GUARANTEE))
	{
	    attributeName.append("guarantee_");
	}
	else if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
	{
	    attributeName.append("airwaybill_");
	}
	else if (instrumentType.equals(InstrumentType.SHIP_GUAR))
	{
	    attributeName.append("ship_guarantee_");
	}
	else if (instrumentType.equals(InstrumentType.EXPORT_COL))
	{
	    attributeName.append("export_collection_");
	}
	//Vasavi CR 524 03/31/2010 Begin
	else if (instrumentType.equals(InstrumentType.NEW_EXPORT_COL))
	{
	    attributeName.append("new_export_coll_");
	}
	//Vasavi CR 524 03/31/2010 End
	else if (instrumentType.equals(InstrumentType.INCOMING_SLC))
	{
	    attributeName.append("inc_standby_LC_");
	}
	else if (instrumentType.equals(InstrumentType.FUNDS_XFER))
	{
	    attributeName.append("funds_transfer_");
	}
	else if (instrumentType.equals(InstrumentType.LOAN_RQST))
	{
	    attributeName.append("loan_request_");
	}
	else if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
	{
	    attributeName.append("request_advise_");
	}
        //rkrishna CR 375-D ATP 08/22/2007 Begin
	else if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
	{
	    attributeName.append("approval_to_pay_");
	}
        //rkrishna CR 375-D ATP 08/22/2007 End
	 //Pratiksha CR-434 ARM 10/01/2008 Begin
	else if (instrumentType.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT))
	{
	    attributeName.append("ar_");
	}
        //Pratiksha CR-434 ARM 10/01/2008 End
        //	Chandrakanth CR 451 CM 10/21/2008 Begin
	else if (instrumentType.equals(InstrumentType.XFER_BET_ACCTS))
	{
		attributeName.append("transfer_btw_accts_");
	}
    //IAZ CM-451 10/29/08 Begin: Domestic Payment is defined as DOM_PMT ("FTDP")
	else if (instrumentType.equals(InstrumentType.DOM_PMT))
	//else if (instrumentType.equals(TradePortalConstants.DOMESTIC_PAYMENT))
	{
		attributeName.append("domestic_payment_");
	}
	//IAZ CM-451 End
	//Chandrakanth CR 451 CM 10/21/2008 End
	//	Pratiksha CR-509 DDI 12/09/2009 Begin
	else if (instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
	{
		attributeName.append("direct_debit_");
	}
	//	Pratiksha CR-509 DDI 12/09/2009 End
	else if (instrumentType.equals(InstrumentType.IMPORT_COL))
	{
		attributeName.append("imp_col_");
	}
	if (transactionType.equals(TransactionType.ISSUE))
	{
		//IAZ CR-509 01/07/10 Begin: resolve direct debit attribute
	    //attributeName.append("issue_");
		if (!instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
		{
	    	attributeName.append("issue_");
		}
		//IAZ CR-509 01/07/10 End
	}

	else if (transactionType.equals(TransactionType.ASSIGNMENT))
	{
	    attributeName.append("issue_assign_");
	}
	else if (transactionType.equals(TransactionType.TRANSFER))
	{
	    if (instrumentType.equals(InstrumentType.EXPORT_DLC))
		attributeName.append("issue_");
	    else
	        attributeName.append("transfer_");
	}
	else if (transactionType.equals(TransactionType.AMEND))
	{
	    attributeName.append("amend_");
	}
	else if (transactionType.equals(TransactionType.AMEND_TRANSFER))
	{
	    attributeName.append("amend_transfer_");
	}
	else if (transactionType.equals(TransactionType.RELEASE))
	{
	    attributeName.append("issue_");
	}
	else if (transactionType.equals(TransactionType.DISCREPANCY))
	{
	    attributeName.append("discr_");
	}
            //rkrishna CR 375-D ATP 08/22/2007 Begin
	else if (transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE))
	{
	     attributeName.append("discr_");
                 // rkrishna 10/29/2007 IR-RIUH101733973 End
	}
            //rkrishna CR 375-D ATP 08/22/2007 End
	//Pratiksha CR-434 ARM 10/01/2008 Begin
	else if (transactionType.equals(TransactionType.RECEIVABLES_MATCH_RESPONSE))
	{
	    attributeName.append("match_response_");
	}
	else if (transactionType.equals(TransactionType.RECEIVABLES_APPROVE_DISCOUNT))
	{
	    attributeName.append("approve_discount_");
	}
	else if (transactionType.equals(TransactionType.RECEIVABLES_CLOSE_INVOICE))
	{
	    attributeName.append("close_invoice_");
	}
	else if (transactionType.equals(TransactionType.RECEIVABLES_FINANCE_INVOICE))
	{
	    attributeName.append("finance_invoice_");
	}
	//Pratiksha CR-434 ARM 10/01/2008 End
	//Chandrakanth CR 451 CM 10/21/2008 Begin
  	else if (transactionType.equals(TransactionType.RECEIVABLES_DISPUTE_INVOICE))
  	{
  	    attributeName.append("dispute_invoice_");
	}
	//Chandrakanth CR 451 CM 10/21/2008 End
	
	//CR 913 start
  	else if (transactionType.equals(TradePortalConstants.INV_LINKED_INSTR_PAY))
  	{
  	    attributeName.append("upload_pay_inv_");
	}
  	else if (transactionType.equals(TradePortalConstants.INV_LINKED_INSTR_REC))
  	{
  	    attributeName.append("upload_rec_inv_");
	}
  	else if (transactionType.equals(TradePortalConstants.INV_LINKED_INSTR_PYB))
  	{
  	    attributeName.append("pyb_mgm_inv_");
	}
	//CR 913 end
  	else if (transactionType.equals(TransactionType.SIM) || transactionType.equals(TransactionType.SIR))
  	{
  		if (instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || 
  				instrumentType.equals(InstrumentType.DEFERRED_PAY) )
  		{
  			attributeName.append("discr_");
  		} else {
  	        attributeName.append("set_instr_");
  	   }
	}

	LOG.debug("ThresholdGroup::AttrName: {} " ,attributeName.toString());

	return attributeName.toString();
   }




}
