
  
/*
 * This file is generated from the model.  Normally it should not be modified manually.  
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted 
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A threshold group is a set of threshold settings.  There are two types of
 * thresholds: a LC threshold and a daily limit threshold.  An LC threshold
 * a rule that indicates the maximum value of an LC that the user can authorize.
 * A daily limit threshold is the maximum total value that a user can authorize
 * in a single day.
 * 
 * An LC threshold and a daily threshold exists for every type of instrument
 * type / transaction type combination that can be created in the Trade Portal.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ThresholdGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* threshold_group_oid - Unique identifier */
      attributeMgr.registerAttribute("threshold_group_oid", "threshold_group_oid", "ObjectIDAttribute");
      
      /* threshold_group_name - Name of the threshold group */
      attributeMgr.registerAttribute("threshold_group_name", "threshold_group_name");
      attributeMgr.requiredAttribute("threshold_group_name");
      attributeMgr.registerAlias("threshold_group_name", getResourceManager().getText("ThresholdGroupBeanAlias.threshold_group_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_issue_thold - LC threshold for an Issue Import LC transaction */
      attributeMgr.registerAttribute("import_LC_issue_thold", "import_LC_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_amend_thold - LC threshold for an Amend Import LC */
      attributeMgr.registerAttribute("import_LC_amend_thold", "import_LC_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_discr_thold - LC threshold for an Import LC Discrepancy Response */
      attributeMgr.registerAttribute("import_LC_discr_thold", "import_LC_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_issue_thold - LC threshold for an Issue Standby LC */
      attributeMgr.registerAttribute("standby_LC_issue_thold", "standby_LC_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_amend_thold - LC threshold for an Amend Standby LC */
      attributeMgr.registerAttribute("standby_LC_amend_thold", "standby_LC_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_discr_thold - LC threshold for a Standby LC Discrepancy Response */
      attributeMgr.registerAttribute("standby_LC_discr_thold", "standby_LC_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_issue_thold - LC threshold for an Export LC Issue Transfer transaction */
      attributeMgr.registerAttribute("export_LC_issue_thold", "export_LC_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_amend_transfer_thold - LC threshold for an Export LC Amend Transfer  transaction */
      attributeMgr.registerAttribute("export_LC_amend_transfer_thold", "export_LC_amend_transfer_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_amend_transfer_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_amend_transfer_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_issue_assign_thold - LC threshold for an Export LC Assignment of Proceeds transaction */
      attributeMgr.registerAttribute("export_LC_issue_assign_thold", "export_LC_issue_assign_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_issue_assign_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_issue_assign_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_discr_thold - LC threshold for an Export LC Discrepancy Response transaction */
      attributeMgr.registerAttribute("export_LC_discr_thold", "export_LC_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* guarantee_issue_thold - LC threshold for an Issue Guarantee transaction */
      attributeMgr.registerAttribute("guarantee_issue_thold", "guarantee_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("guarantee_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.guarantee_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* guarantee_amend_thold - LC threshold for an Amend Guarantee transaction */
      attributeMgr.registerAttribute("guarantee_amend_thold", "guarantee_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("guarantee_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.guarantee_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* airwaybill_issue_thold - LC threshold for an Air Waybill Release transaction */
      attributeMgr.registerAttribute("airwaybill_issue_thold", "airwaybill_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("airwaybill_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.airwaybill_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* ship_guarantee_issue_thold - LC threshold for an Issue Shipping Guarantee transaction */
      attributeMgr.registerAttribute("ship_guarantee_issue_thold", "ship_guarantee_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ship_guarantee_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ship_guarantee_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_collection_issue_thold - LC threshold for an Issue Direct Send Collection transaction.   (Note the
         proper Export Collection from TPS perspective is new_export_coll_issue_thold.) */
      attributeMgr.registerAttribute("export_collection_issue_thold", "export_collection_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_collection_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_collection_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_collection_amend_thold - LC threshold for an Amend Direct Send Collection transaction.  (Note the
         proper Export Collection from TPS perspective is new_export_coll_amend_thold.) */
      attributeMgr.registerAttribute("export_collection_amend_thold", "export_collection_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_collection_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.export_collection_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* inc_standby_LC_discr_thold - LC threshold for an Incoming Standby Discrepancy Reponse */
      attributeMgr.registerAttribute("inc_standby_LC_discr_thold", "inc_standby_LC_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("inc_standby_LC_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.inc_standby_LC_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* loan_request_issue_thold - LC threshold for a Loan Request Issue */
      attributeMgr.registerAttribute("loan_request_issue_thold", "loan_request_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("loan_request_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.loan_request_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* funds_transfer_issue_thold - Threshold for Funds Transfer Issue */
      attributeMgr.registerAttribute("funds_transfer_issue_thold", "funds_transfer_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("funds_transfer_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.funds_transfer_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_discr_dlimit - Daily limit threshold for an Import LC Discrepancy Response transaction. */
      attributeMgr.registerAttribute("import_LC_discr_dlimit", "import_LC_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_issue_dlimit - Daily limit threshold for an Issue Import LC  transaction. */
      attributeMgr.registerAttribute("import_LC_issue_dlimit", "import_LC_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* import_LC_amend_dlimit - Daily limit threshold for an Amend Import LC transaction. */
      attributeMgr.registerAttribute("import_LC_amend_dlimit", "import_LC_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("import_LC_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.import_LC_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_issue_dlimit - Daily limit threshold for an Issue Standby LC transaction. */
      attributeMgr.registerAttribute("standby_LC_issue_dlimit", "standby_LC_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_amend_dlimit - Daily limit threshold for an Amend Standby LC transaction. */
      attributeMgr.registerAttribute("standby_LC_amend_dlimit", "standby_LC_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* standby_LC_discr_dlimit - Daily limit threshold for a Standby LC Discrepancy Reponse transaction. */
      attributeMgr.registerAttribute("standby_LC_discr_dlimit", "standby_LC_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("standby_LC_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.standby_LC_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_issue_dlimit - Daily limit threshold for an Issue Transfer Export  LC  transaction. */
      attributeMgr.registerAttribute("export_LC_issue_dlimit", "export_LC_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_amend_transfer_dlim - Daily limit threshold for an Export LC Amend Transfer  transaction. */
      attributeMgr.registerAttribute("export_LC_amend_transfer_dlim", "export_LC_amend_transfer_dlim", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_amend_transfer_dlim", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_amend_transfer_dlim", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_issue_assign_dlimit - Daily limit threshold for an Export LC Assignment of Proceeds transaction. */
      attributeMgr.registerAttribute("export_LC_issue_assign_dlimit", "export_LC_issue_assign_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_issue_assign_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_issue_assign_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_LC_discr_dlimit - Daily limit threshold for an Export LC Discrepancy Response transaction. */
      attributeMgr.registerAttribute("export_LC_discr_dlimit", "export_LC_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_LC_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.export_LC_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* guarantee_issue_dlimit - Daily limit threshold for an Issue Guarantee transaction. */
      attributeMgr.registerAttribute("guarantee_issue_dlimit", "guarantee_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("guarantee_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.guarantee_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* guarantee_amend_dlimit - Daily limit threshold for an Amend Guarantee transaction. */
      attributeMgr.registerAttribute("guarantee_amend_dlimit", "guarantee_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("guarantee_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.guarantee_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* airwaybill_issue_dlimit - Daily limit threshold for an Air Waybill release transaction. */
      attributeMgr.registerAttribute("airwaybill_issue_dlimit", "airwaybill_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("airwaybill_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.airwaybill_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* ship_guarantee_issue_dlimit - Daily limit threshold for an Issue Shipping Guarantee transaction. */
      attributeMgr.registerAttribute("ship_guarantee_issue_dlimit", "ship_guarantee_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ship_guarantee_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ship_guarantee_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_collection_issue_dlimit - Daily limit threshold for an Issue Direct Send Collection transaction. 
         (Note new_export_collection_issue_dlimit is the proper Export Collection.) */
      attributeMgr.registerAttribute("export_collection_issue_dlimit", "export_collection_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_collection_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.export_collection_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* export_collection_amend_dlimit - Daily limit threshold for an Amend Export Collection  transaction.  (Note
         new_export_collection_amend_dlimit is the proper export collection.) */
      attributeMgr.registerAttribute("export_collection_amend_dlimit", "export_collection_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("export_collection_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.export_collection_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* inc_standby_LC_discr_dlimit - Daily limit threshold for an Incoming Standby Discrepancy Response transaction. */
      attributeMgr.registerAttribute("inc_standby_LC_discr_dlimit", "inc_standby_LC_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("inc_standby_LC_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.inc_standby_LC_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* loan_request_issue_dlimit - Daily limit threshold for an Loan Request issue transaction */
      attributeMgr.registerAttribute("loan_request_issue_dlimit", "loan_request_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("loan_request_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.loan_request_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* funds_transfer_issue_dlimit - Daily limit threshold for an Funds Transfer Issue transaction. */
      attributeMgr.registerAttribute("funds_transfer_issue_dlimit", "funds_transfer_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("funds_transfer_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.funds_transfer_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_issue_thold - Threshold amount for Request to Advise Issue transaction */
      attributeMgr.registerAttribute("request_advise_issue_thold", "request_advise_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_amend_thold - Threshold amount for Request to Advise Amend transaction */
      attributeMgr.registerAttribute("request_advise_amend_thold", "request_advise_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_discr_thold - Threshold amount for Request to Advise Discrepancy Response transaction */
      attributeMgr.registerAttribute("request_advise_discr_thold", "request_advise_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_issue_dlimit - Daily limit threshold for an Issue Request to Advise transaction */
      attributeMgr.registerAttribute("request_advise_issue_dlimit", "request_advise_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_amend_dlimit - Daily limit threshold for an Amend Request to Advise transaction */
      attributeMgr.registerAttribute("request_advise_amend_dlimit", "request_advise_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* request_advise_discr_dlimit - Daily limit threshold for a Discrpancy Response Request to Advise transaction */
      attributeMgr.registerAttribute("request_advise_discr_dlimit", "request_advise_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("request_advise_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.request_advise_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
	  //Krishna CR 375-D ATP 07/19/2007 Begin
       /* approval_to_pay_issue_thold - Threshold amount for Approval to Pay Issue transaction */
      attributeMgr.registerAttribute("approval_to_pay_issue_thold", "approval_to_pay_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* approval_to_pay_amend_thold - Threshold amount for Approval to Pay  Amend transaction */
      attributeMgr.registerAttribute("approval_to_pay_amend_thold", "approval_to_pay_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* approval_to_pay_discr_thold - Threshold amount for Approval to Pay  Discrepancy Response transaction */
      attributeMgr.registerAttribute("approval_to_pay_discr_thold", "approval_to_pay_discr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_discr_thold", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_discr_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* approval_to_pay_issue_dlimit - Daily limit threshold for an Issue Approval to Pay  transaction */
      attributeMgr.registerAttribute("approval_to_pay_issue_dlimit", "approval_to_pay_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* approval_to_pay_amend_dlimit - Daily limit threshold for an Amend Approval to Pay  transaction */
      attributeMgr.registerAttribute("approval_to_pay_amend_dlimit", "approval_to_pay_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_amend_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_amend_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* approval_to_pay_discr_dlimit - Daily limit threshold for a Discrpancy Response Approval to Pay  transaction */
      attributeMgr.registerAttribute("approval_to_pay_discr_dlimit", "approval_to_pay_discr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("approval_to_pay_discr_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.approval_to_pay_discr_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      //Krishna CR 375-D ATP 07/19/2007 End

      //Pratiksha CR-434 ARM 09/29/2008 Begin
      /* ar_match_response_thold - Threshold amount for AR Match Response */
      attributeMgr.registerAttribute("ar_match_response_thold", "ar_match_response_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_match_response_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ar_match_response_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* ar_match_response_dlimit - Daily limit threshold for AR Match Response. */
      attributeMgr.registerAttribute("ar_match_response_dlimit", "ar_match_response_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_match_response_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ar_match_response_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* ar_approve_discount_thold - Threshold amount for AR approve discount */
      attributeMgr.registerAttribute("ar_approve_discount_thold", "ar_approve_discount_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_approve_discount_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ar_approve_discount_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* ar_approve_discount_dlimit - Daily limit threshold for AR Approve Discount */
      attributeMgr.registerAttribute("ar_approve_discount_dlimit", "ar_approve_discount_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_approve_discount_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ar_approve_discount_dlimit", TradePortalConstants.TEXT_BUNDLE));

	  /* ar_close_invoice_thold - Threshold amount for AR Close Invoice*/
      attributeMgr.registerAttribute("ar_close_invoice_thold", "ar_close_invoice_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_close_invoice_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ar_close_invoice_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* ar_close_invoice_dlimit - Daily limit threshold for AR Close Invoice. */
      attributeMgr.registerAttribute("ar_close_invoice_dlimit", "ar_close_invoice_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_close_invoice_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ar_close_invoice_dlimit", TradePortalConstants.TEXT_BUNDLE));

	  /* ar_finance_invoice_thold - Threshold amount for AR Finance Invoice */
      attributeMgr.registerAttribute("ar_finance_invoice_thold", "ar_finance_invoice_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_finance_invoice_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ar_finance_invoice_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* ar_finance_invoice_dlimit - Daily limit threshold for AR Finance Invoice. */
      attributeMgr.registerAttribute("ar_finance_invoice_dlimit", "ar_finance_invoice_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_finance_invoice_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ar_finance_invoice_dlimit", TradePortalConstants.TEXT_BUNDLE));

   	   //Pratiksha CR-434 ARM 09/29/2008 End
	//CR-451 Changes Begin
      /* transfer_btw_accts_issue_thold - Threshold amount for Transfer Between Account Issue transaction. */
      attributeMgr.registerAttribute("transfer_btw_accts_issue_thold", "transfer_btw_accts_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("transfer_btw_accts_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.transfer_btw_accts_issue_thold", TradePortalConstants.TEXT_BUNDLE));

      /* transfer_btw_accts_issue_dlimit - Daily limit threshold for Transfer Between Accounts Issue transaction. */
      attributeMgr.registerAttribute("transfer_btw_accts_issue_dlimit", "transfer_btw_accts_iss_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("transfer_btw_accts_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.transfer_btw_accts_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));

      /* domestic_payment_issue_thold - Threshold amount for Domestic Payment Issue transaction. */
      attributeMgr.registerAttribute("domestic_payment_issue_thold", "domestic_payment_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("domestic_payment_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.domestic_payment_issue_thold", TradePortalConstants.TEXT_BUNDLE));

      /* domestic_payment_issue_dlimit - Daily limit threshold for Domestic Payment Issue transaction. */
      attributeMgr.registerAttribute("domestic_payment_issue_dlimit", "domestic_payment_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("domestic_payment_issue_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.domestic_payment_issue_dlimit", TradePortalConstants.TEXT_BUNDLE));

	 /* ar_dispute_invoice_thold - Threshold amount for AR dispute/undispute invoice. */
      attributeMgr.registerAttribute("ar_dispute_invoice_thold", "ar_dispute_invoice_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_dispute_invoice_thold", getResourceManager().getText("ThresholdGroupBeanAlias.ar_dispute_invoice_thold", TradePortalConstants.TEXT_BUNDLE));

      /* ar_dispute_invoice_dlimit - Daily limit threshold for AR Dispute/Undispute Invoice */
      attributeMgr.registerAttribute("ar_dispute_invoice_dlimit", "ar_dispute_invoice_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("ar_dispute_invoice_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.ar_dispute_invoice_dlimit", TradePortalConstants.TEXT_BUNDLE));
      	//CR-451 Changes End
      
      //    Pratiksha CR-509 DDI 12/09/2009 Begin

      /* direct_debit_thold - Threshold amount for Direct Debit Issue. */
      
      attributeMgr.registerAttribute("direct_debit_thold","direct_debit_thold","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("direct_debit_thold",getResourceManager().getText("ThresholdGroupBeanAlias.direct_debit_thold",TradePortalConstants.TEXT_BUNDLE));
      
      /* direct_debit_dlimit' - Daily limit threshold for Direct Debit Issue. */
      attributeMgr.registerAttribute("direct_debit_dlimit","direct_debit_dlimit","TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("direct_debit_dlimit",getResourceManager().getText("ThresholdGroupBeanAlias.direct_debit_dlimit",TradePortalConstants.TEXT_BUNDLE));
      // Pratiksha CR-509 DDI 12/09/2009 End
	 
   /* new_export_coll_issue_thold - Instrument threshold for an Issue Export Collection transaction.   (Note
         export_collection_issue_thold is for Direct Send Collection from TPS perspective.) */
      attributeMgr.registerAttribute("new_export_coll_issue_thold", "new_export_coll_issue_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("new_export_coll_issue_thold", getResourceManager().getText("ThresholdGroupBeanAlias.new_export_coll_issue_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* new_export_coll_amend_thold - Instrument threshold for an Amend Export Collection transaction.   (Note
         export_collection_amend_thold is for Direct Send Collection from TPS perspective.) */
      attributeMgr.registerAttribute("new_export_coll_amend_thold", "new_export_coll_amend_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("new_export_coll_amend_thold", getResourceManager().getText("ThresholdGroupBeanAlias.new_export_coll_amend_thold", TradePortalConstants.TEXT_BUNDLE));
      
      /* new_export_coll_issue_dlimit - Daily limit threshold for an Issue Export Collection transaction.  (Note
         this is the proper export colleciton.  export_collection_issue_dlimit is
         only Direct Send Collection.) */
      attributeMgr.registerAttribute("new_export_coll_issue_dlimit", "new_export_coll_issue_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
      /* new_export_coll_amend_dlimit - Daily limit threshold for an Amend Export Collection transaction.  (Note
         this is the proper export colleciton.  export_collection_amend_dlimit is
         only Direct Send Collection.) */
      attributeMgr.registerAttribute("new_export_coll_amend_dlimit", "new_export_coll_amend_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      
     //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- BEGIN      
      /* upload_rec_inv_thold - Threshold amount for Uploaded Receivable Invoices */
      attributeMgr.registerAttribute("upload_rec_inv_thold", "upload_rec_inv_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("upload_rec_inv_thold", getResourceManager().getText("ThresholdGroupBeanAlias.upload_rec_inv_thold", TradePortalConstants.TEXT_BUNDLE));

      /* upload_rec_inv_dlimit - Daily limit threshold for Uploaded Receivable Invoices */
      attributeMgr.registerAttribute("upload_rec_inv_dlimit", "upload_rec_inv_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("upload_rec_inv_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.upload_rec_inv_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* upload_pay_inv_thold - Threshold amount for Uploaded Payable Invoices */
      attributeMgr.registerAttribute("upload_pay_inv_thold", "upload_pay_inv_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("upload_pay_inv_thold", getResourceManager().getText("ThresholdGroupBeanAlias.upload_pay_inv_thold", TradePortalConstants.TEXT_BUNDLE));

      /* upload_pay_inv_dlimit - Daily limit threshold for Uploaded Payable Invoices */
      attributeMgr.registerAttribute("upload_pay_inv_dlimit", "upload_pay_inv_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("upload_pay_inv_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.upload_pay_inv_dlimit", TradePortalConstants.TEXT_BUNDLE));
      
      /* pyb_mgm_inv_thold - Threshold amount for Payable  Management Invoices */
      attributeMgr.registerAttribute("pyb_mgm_inv_thold", "pyb_mgm_inv_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("pyb_mgm_inv_thold", getResourceManager().getText("ThresholdGroupBeanAlias.pyb_mgm_inv_thold", TradePortalConstants.TEXT_BUNDLE));

      /* pyb_mgm_inv_dlimit - Daily limit threshold for Payable  Management Invoices */
      attributeMgr.registerAttribute("pyb_mgm_inv_dlimit", "pyb_mgm_inv_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("pyb_mgm_inv_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.pyb_mgm_inv_dlimit", TradePortalConstants.TEXT_BUNDLE));
     //Narayan CR-913 03-Feb-2014 Rel9.0 ADD- END
      
      //vdesingu - CR-914A - Rel 9.2 - Added attributes - Start
      /* pay_credit_note_thold - Threshold amount for Payables Credit Note */
      attributeMgr.registerAttribute("pay_credit_note_thold", "pay_credit_note_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("pay_credit_note_thold", getResourceManager().getText("ThresholdGroupBeanAlias.pay_credit_note_thold", TradePortalConstants.TEXT_BUNDLE));

      /* pay_credit_note_dlimit - Daily limit threshold for Payables Credit Note */
      attributeMgr.registerAttribute("pay_credit_note_dlimit", "pay_credit_note_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAlias("pay_credit_note_dlimit", getResourceManager().getText("ThresholdGroupBeanAlias.pay_credit_note_dlimit", TradePortalConstants.TEXT_BUNDLE));
      //vdesingu - CR-914A - Rel 9.2 - Added attributes - End

        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
      
      //SSikhakolli - Rel-9.4 CR-818 - adding new attributes
      attributeMgr.registerAttribute("imp_col_set_instr_thold", "imp_col_set_instr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAttribute("imp_col_set_instr_dlimit", "imp_col_set_instr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAttribute("loan_request_set_instr_dlimit", "loan_request_set_instr_dlimit", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
      attributeMgr.registerAttribute("loan_request_set_instr_thold", "loan_request_set_instr_thold", "TradePortalDecimalAttribute", "com.ams.tradeportal.common");
   }
}