package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * All Trade Portal business objects inherit from this business object directly
 * or indirectly.   Any logic or attributes common to all Trade Portal business
 * objects should be placed here.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TradePortalBusinessObject extends BusinessObject
{   
	public void deactivate() throws RemoteException, AmsException;

}
