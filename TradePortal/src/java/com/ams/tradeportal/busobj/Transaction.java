



package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Represents a transaction between the corporate customer and their bank.
 * When a transaction is authorized, it is sent to the back end system, OTL,
 * and processed.
 *
 * A transaction has two sets of terms associated with it: Customer Entered
 * Terms and Bank Released Terms.   Customer Entered terms are created and
 * edited on the Trade Portal.  Bank Released Terms are updated only by OTL.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public interface Transaction extends TradePortalBusinessObject
{
	public Terms populateCustomerTerms(com.amsinc.ecsg.util.DocumentHandler doc) throws RemoteException;

	public boolean canDeleteState() throws RemoteException, AmsException;

	public boolean canRouteState() throws RemoteException, AmsException;

	public boolean canAuthorizeState() throws RemoteException, AmsException;

	public void createNewCopy(com.ams.tradeportal.busobj.Transaction source, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean isTemplateTransaction) throws RemoteException, AmsException;

	public void createNewBlank(java.lang.String transType, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean ownedByTemplate, java.lang.String messageOid) throws RemoteException, AmsException;

	public void createNewBlank(java.lang.String transType, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean ownedByTemplate, java.lang.String messageOid, java.lang.String relatedInstrID) throws RemoteException, AmsException;

	public void unassignPOAssignment() throws RemoteException, AmsException;

	public void createNewBlank(java.lang.String transType, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean ownedByTemplate, java.lang.String messageOid, java.lang.String relatedInstrID, com.ams.tradeportal.busobj.POLineItem poLineItem) throws RemoteException, AmsException;

    public void createNewBlankStructPOTran(java.lang.String transType, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean ownedByTemplate, java.lang.String messageOid, java.lang.String relatedInstrID, com.ams.tradeportal.busobj.PurchaseOrder purchaseOrder) throws RemoteException, AmsException;

    public void createNewBlankInvTran(java.lang.String transType, java.lang.String userOid, java.lang.String corpOrgOid, java.lang.String instrumentType, boolean ownedByTemplate, java.lang.String messageOid, java.lang.String relatedInstrID, com.ams.tradeportal.busobj.InvoicesSummaryData invoicesSummaryData) throws RemoteException, AmsException;

    public String getPaymentDate(String userOid) throws RemoteException, AmsException;

    public String getPaymentDate(String timeZone, boolean timeZoneFlag) throws RemoteException, AmsException;
    
    public ClientServerDataBridge getClientServerDataBridge()  throws RemoteException;    
    
    public boolean isH2HSentPayment() throws RemoteException, AmsException; // Nar CR694A Rel9.0
	
	//ShilpaR CR 707 Rel 8.0
	public void updateAssociatedPOStatus(Map map) throws RemoteException, AmsException;
}
