
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Details for the various actions performed on the Cash Management transactions
 * by a user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TransactionHistoryHome extends EJBHome
{
   public TransactionHistory create()
      throws RemoteException, CreateException, AmsException;

   public TransactionHistory create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
