
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * Represents a transaction between the corporate customer and their bank.
 * When a transaction is authorized, it is sent to the back end system, OTL,
 * and processed.   
 * 
 * A transaction has two sets of terms associated with it: Customer Entered
 * Terms and Bank Released Terms.   Customer Entered terms are created and
 * edited on the Trade Portal.  Bank Released Terms are updated only by OTL.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface TransactionHome extends EJBHome
{
   public Transaction create()
      throws RemoteException, CreateException, AmsException;

   public Transaction create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
