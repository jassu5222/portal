package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import com.ams.tradeportal.common.*;

/*
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserAuthorizedAccountBean extends UserAuthorizedAccountBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(UserAuthorizedAccountBean.class);

	/**
	* Validate both Account and account description are entered.  
	*/
	public void userValidate()
		throws RemoteException, AmsException {

		
		String acctOid = this.getAttribute("account_oid").trim();
		String acctDesc = this.getAttribute("account_description").trim();

  	    if (acctOid.equals("") && !acctDesc.equals("")) {
				this.getErrorManager().issueError(
								  TradePortalConstants.ERR_CAT_1, 
								  TradePortalConstants.ACCT_NUMBER_NAME_MISSING); 
			
		}else if (!acctOid.equals("") && acctDesc.equals("")) {
				this.getErrorManager().issueError(
								  TradePortalConstants.ERR_CAT_1, 
								  TradePortalConstants.ACCT_DESC_MISSING);
		}
	}
}
