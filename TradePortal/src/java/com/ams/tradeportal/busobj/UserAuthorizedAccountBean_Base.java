
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserAuthorizedAccountBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserAuthorizedAccountBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* authorized_account_oid - Unique Identifier */
      attributeMgr.registerAttribute("authorized_account_oid", "authorized_account_oid", "ObjectIDAttribute");
      
      /* account_description - the user can enter a description or "nickname' for the account. */
      attributeMgr.registerAttribute("account_description", "account_description");
      
        /* Pointer to the parent User */
      attributeMgr.registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");
   
      /* account_oid -  */
      attributeMgr.registerAssociation("account_oid", "a_account_oid", "Account");
      
   }
   
 
   
 
 
   
}
