

  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/**
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface UserAuthorizedTemplateGroup extends TradePortalBusinessObject
{   
}
