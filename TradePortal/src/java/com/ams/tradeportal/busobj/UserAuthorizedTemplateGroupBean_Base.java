


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserAuthorizedTemplateGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserAuthorizedTemplateGroupBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* authorized_template_group_oid - Unique Identifier */
      attributeMgr.registerAttribute("authorized_template_group_oid", "authorized_template_group_oid", "ObjectIDAttribute");

      /* Pointer to the parent User */
      attributeMgr.registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");

      /* account_oid -  */
      attributeMgr.registerAssociation("payment_template_group_oid", "a_template_group_oid", "PaymentTemplateGroup");

   }






}
