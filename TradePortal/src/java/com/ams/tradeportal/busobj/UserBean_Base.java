

/*
 * This file is generated from the model.  Normally it should not be modified manually.
 * Contact the modeler first.  (In a pinch, it can be modified first and then retrofitted
 * to the model.)
 *
 */
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;


/*
 * A user of the Trade Portal system.   Each active user in this table is able
 * to log into the Trade Portal and perform actions for their organization.
 * Users can exist for corporate organizations, bank organization groups, client
 * banks, and global orgs.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();

      /* date_deactivated - Timestamp (in GMT) of when the user was deactivated. */
      attributeMgr.registerAttribute("date_deactivated", "date_deactivated", "DateTimeAttribute");

      /* activation_status - Indicates whether or not the user has been deactivated. Users  are not deleted
         from the database; they are deactivated instead. */
      attributeMgr.registerReferenceAttribute("activation_status", "activation_status", "ACTIVATION_STATUS");
      
  	//Ravindra - Rel7100 -IR VDUK100867774 - 11th Aug 2011 - Start
      /* creation_date - Timestamp (in GMT) when the user was created. */
      attributeMgr.registerAttribute("creation_date", "creation_date", "DateTimeAttribute");
  	//Ravindra - Rel7100 -IR VDUK100867774 - 11th Aug 2011 - End
      
    //Ravindra - Rel7100 -IR KMUL042736516 - 11th Aug 2011 - Start
      /* login_timestamp - Timestamp (in GMT) when the user was login. */
      attributeMgr.registerAttribute("login_timestamp", "login_timestamp", "DateTimeAttribute");
  	//Ravindra - Rel7100 -IR KMUL042736516 - 11th Aug 2011 - End
      /* first_name - User's first name */
      attributeMgr.registerAttribute("first_name", "first_name");
      attributeMgr.requiredAttribute("first_name");
      attributeMgr.registerAlias("first_name", getResourceManager().getText("UserBeanAlias.first_name", TradePortalConstants.TEXT_BUNDLE));

      /* last_name - User's last name */
      attributeMgr.registerAttribute("last_name", "last_name");
      attributeMgr.requiredAttribute("last_name");
      attributeMgr.registerAlias("last_name", getResourceManager().getText("UserBeanAlias.last_name", TradePortalConstants.TEXT_BUNDLE));

      /* telephone_num - User's telephone number */
      attributeMgr.registerAttribute("telephone_num", "telephone_num");

      /* user_oid - Unique identifier */
      attributeMgr.registerAttribute("user_oid", "user_oid", "ObjectIDAttribute");

      /* middle_initial - User's middle initial */
      attributeMgr.registerAttribute("middle_initial", "middle_initial");

      /* fax_num - User's fax number */
      attributeMgr.registerAttribute("fax_num", "fax_num");

      /* email_addr - User's e-mail address */
      attributeMgr.registerAttribute("email_addr", "email_addr");

      /* password_change_date - Timestamp (in GMT) indicating when the user last changed their password.
         Used to determine if a password has expired or not. */
      attributeMgr.registerAttribute("password_change_date", "password_change_date", "DateTimeAttribute");

      /* newPassword - This is a local attribute, meaning that it is not saved to the database.
         It simply holds the value that the user entered for their "New Password".
         If this value passes validation, it is encrypted and copied into the password
         field. */
      attributeMgr.registerAttribute("newPassword", "newPassword", "LocalAttribute");

      /* password - The user's password.   This is stored as an encrypted value.  For more information
         on the encryption process, see the Trade Portal Infrastructure document.
         Passwords may not be used by certain users based on their organization's
         settings. */
      attributeMgr.registerAttribute("password", "password");
      attributeMgr.registerAlias("password", getResourceManager().getText("UserBeanAlias.password", TradePortalConstants.TEXT_BUNDLE));

      /* retypePassword - This is a local attribute, meaning that it is not saved to the database.
         It simply holds the value that the user entered for their "Retype Password".
         If this value passes validation, it is encrypted and copied into the password
         field. */
      attributeMgr.registerAttribute("retypePassword", "retypePassword", "LocalAttribute");

      /* new_user_ind - Set to Yes if the user has been created, but has not succesfully logged
         in. */
      attributeMgr.registerAttribute("new_user_ind", "new_user_ind", "IndicatorAttribute");

      /* encryption_salt - A set of randomly generated bytes is placed into this field when the user
         is created.  The salt is used in combination with the user entered password
         during the encryption process. */
      attributeMgr.registerAttribute("encryption_salt", "encryption_salt");

      /* locked_out_ind - Set to Yes if the user is locked out due to an excessive number of invalid
         logon attempts. */
      attributeMgr.registerAttribute("locked_out_ind", "locked_out_ind", "IndicatorAttribute");

      /* default_wip_view - Indicates the default "work in process" view that the user will see on the
         Pending Transactions page.   This setting also affects other "Show" dropdowns
         in the application. */
      attributeMgr.registerReferenceAttribute("default_wip_view", "default_wip_view", "WIP_VIEW");
      attributeMgr.registerAlias("default_wip_view", getResourceManager().getText("UserBeanAlias.default_wip_view", TradePortalConstants.TEXT_BUNDLE));

      /* customer_access_ind - Set to Yes if the user is allowed to act on behalf of corporate organizations
         using the customer access or subsidiary access feature. */
      attributeMgr.registerAttribute("customer_access_ind", "customer_access_ind", "IndicatorAttribute");

      /* login_id - The unique identifier that the user uses to log into the Trade Portal if
         they are using passwords.  This identifier will be unique within all users
         for a client bank (including BOGs and corporate customers under that client
         bank). */
      attributeMgr.registerAttribute("login_id", "login_id");
      attributeMgr.registerAlias("login_id", getResourceManager().getText("UserBeanAlias.login_id", TradePortalConstants.TEXT_BUNDLE));

      /* timezone - The timezone in which the user is located. */
      attributeMgr.registerReferenceAttribute("timezone", "timezone", "TIMEZONE");
      attributeMgr.requiredAttribute("timezone");
      attributeMgr.registerAlias("timezone", getResourceManager().getText("UserBeanAlias.timezone", TradePortalConstants.TEXT_BUNDLE));
      
      /*pgedupudi - Rel-9.3 CR-976A 03/26/2015 Bank Group Restriction Rule */
      attributeMgr.registerAssociation("bank_grp_restrict_rule_oid", "a_bank_grp_restrict_rule_oid", "BankGrpRestrictRule");
      
      /* datepattern - The datepattern setting for the user. */
      attributeMgr.registerReferenceAttribute("datepattern", "datepattern", "DATEPATTERN");
      attributeMgr.requiredAttribute("datepattern");
      attributeMgr.registerAlias("datepattern", getResourceManager().getText("UserBeanAlias.datepattern", TradePortalConstants.TEXT_BUNDLE));

      /* ownership_level - The level at which the reference data is owned (Proponix, client bank, bank
         organization group, or corporate customer */
      attributeMgr.registerReferenceAttribute("ownership_level", "ownership_level", "OWNERSHIP_LEVEL");
      attributeMgr.requiredAttribute("ownership_level");

      /* locale - The locale (region) setting for the user.  Includes both language and country. */
      attributeMgr.registerReferenceAttribute("locale", "locale", "LOCALE");
      attributeMgr.requiredAttribute("locale");
      attributeMgr.registerAlias("locale", getResourceManager().getText("UserBeanAlias.locale", TradePortalConstants.TEXT_BUNDLE));

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* user_identifier - A handle that is assigned to the user to easily refer to them.  Not used
         to authenticate or identify a user logging in. */
      attributeMgr.registerAttribute("user_identifier", "user_identifier");
      attributeMgr.requiredAttribute("user_identifier");
      attributeMgr.registerAlias("user_identifier", getResourceManager().getText("UserBeanAlias.user_identifier", TradePortalConstants.TEXT_BUNDLE));

      /* invalid_login_attempt_count - The cumulative number of invalid logon attempts during the lifetime of a
         user.  If this reaches a certain threshold, the locked_out_ind is set to
         Yes. */
      attributeMgr.registerAttribute("invalid_login_attempt_count", "invalid_login_attempt_count", "NumberAttribute");

      /* user_multi_part_trans_view - Indicates how the user wants to view their transaction pages: one long page
         or several smaller pages.  If Yes, user wants to use several smaller pages. */
      attributeMgr.registerAttribute("user_multi_part_trans_view", "user_multi_part_trans_view", "IndicatorAttribute");

      /* certificate_id - The unique identifier that is determined from fields on the user's certificate
         when they log into the Trade Portal.  This identifier will be unique within
         all users for a client bank (including BOGs and corporate customers under
         that client bank). */
      attributeMgr.registerAttribute("certificate_id", "certificate_id");
      attributeMgr.registerAlias("certificate_id", getResourceManager().getText("UserBeanAlias.certificate_id", TradePortalConstants.TEXT_BUNDLE));

      /* display_extended_listviews - Indicates how the user wishes to view their listviews.  If set to 'Y', columns
         that are marked as 'extended' will appear when viewing a listview.  Otherwise,
         they will be hidden. */
      attributeMgr.registerAttribute("display_extended_listviews", "display_extended_listviews", "IndicatorAttribute");
      /* defect IRT#36000003827 by dillip dt 08/02/2012            */
      /* Making display_extended_listviews as required field and setting the value to N for newly created users*/
      attributeMgr.requiredAttribute("display_extended_listviews");

      /* authentication_method - How this user will authenticate him/herself.  This attribute is not NULL
         if and only if the corporate allows each user to specify their own authentication
         method. */
      attributeMgr.registerReferenceAttribute("authentication_method", "authentication_method", "AUTHENTICATION_METHOD");
      attributeMgr.registerAlias("authentication_method", getResourceManager().getText("UserBeanAlias.authentication_method", TradePortalConstants.TEXT_BUNDLE));

      /* doc_prep_ind - Indicates whether or not a user has access to the doc prep functionality. */
      attributeMgr.registerAttribute("doc_prep_ind", "doc_prep_ind", "IndicatorAttribute");

      /* doc_prep_username - The user's doc prep login ID.   This is sent to the document prep system. */
      attributeMgr.registerAttribute("doc_prep_username", "doc_prep_username");
      attributeMgr.registerAlias("doc_prep_username", getResourceManager().getText("UserBeanAlias.doc_prep_username", TradePortalConstants.TEXT_BUNDLE));

      /* use_data_while_in_sub_access - Indicates whether or not a user has access to their own organization's data
         while using subsidiary access. */
      attributeMgr.registerAttribute("use_data_while_in_sub_access", "use_data_while_in_sub_access", "IndicatorAttribute");

      /* sso_id - The unique identifier that the user uses to log into the Trade Portal if
         they are using single sign-on.  This identifier will be unique within all
         users for a client bank (including BOGs and corporate customers under that
         client bank). */
      attributeMgr.registerAttribute("sso_id", "sso_id");
      attributeMgr.registerAlias("sso_id", getResourceManager().getText("UserBeanAlias.sso_id", TradePortalConstants.TEXT_BUNDLE));

      /* reporting_type - Reporting Type indicates whether the reporting type for the user is Cash
         Management or Trade Services. */
      attributeMgr.registerAttribute("reporting_type", "reporting_type");

      //Suresh CR-603 03/17/2011 Begin
      /* first_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("first_rept_categ", "first_rept_categ", "REPORTING_CATEGORY");
      
      /* second_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("second_rept_categ", "second_rept_categ", "REPORTING_CATEGORY");
      
      /* third_rept_categ - Each User can do business with up to ten report categories.*/
      attributeMgr.registerReferenceAttribute("third_rept_categ", "third_rept_categ", "REPORTING_CATEGORY");
      
      /* fourth_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("fourth_rept_categ", "fourth_rept_categ", "REPORTING_CATEGORY");
      
      /* fifth_rept_categ - Each User can do business with up to ten report categories.  */
      attributeMgr.registerReferenceAttribute("fifth_rept_categ", "fifth_rept_categ", "REPORTING_CATEGORY");
      
      /* sixth_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("sixth_rept_categ", "sixth_rept_categ", "REPORTING_CATEGORY");
      
      /* seventh_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("seventh_rept_categ", "seventh_rept_categ", "REPORTING_CATEGORY");
      
      /* eighth_rept_categ - Each User can do business with up to ten report categories.*/
      attributeMgr.registerReferenceAttribute("eighth_rept_categ", "eighth_rept_categ", "REPORTING_CATEGORY");
      
      /* nineth_rept_categ - Each User can do business with up to ten report categories.  */
      attributeMgr.registerReferenceAttribute("nineth_rept_categ", "nineth_rept_categ", "REPORTING_CATEGORY");
      
      /* tenth_rept_categ - Each User can do business with up to ten report categories. */
      attributeMgr.registerReferenceAttribute("tenth_rept_categ", "tenth_rept_categ", "REPORTING_CATEGORY");
      //suresh CR-603 03/17/2011 End
      
      /* authorize_own_input_ind - Indicates whether user can authorize their own input. */
      attributeMgr.registerAttribute("authorize_own_input_ind", "authorize_own_input_ind", "IndicatorAttribute");

      /* panel_authority_code - The level at which the reference data is owned (Proponix, client bank, bank
         organization group, or corporate customer */
      attributeMgr.registerReferenceAttribute("panel_authority_code", "panel_authority_code", "PANEL_AUTH_TYPE");

      /* token_id_2fa - 2-phase authentication token ID */
      attributeMgr.registerAttribute("token_id_2fa", "token_id_2fa");
      attributeMgr.registerAlias("token_id_2fa", getResourceManager().getText("UserBeanAlias.token_id_2fa", TradePortalConstants.TEXT_BUNDLE));

      /* additional_auth_type - The additional authentication method to use for payment transaction, if
         the corporate customer requires that. */
      attributeMgr.registerReferenceAttribute("additional_auth_type", "additional_auth_type", "AUTHENTICATION_METHOD");

      /* passwordValidationError - This is a local attribute, meaning that it is not saved to the database.
         It simply holds the value that the user's browser passed back, if at all,
         when password validation is being handled by the client / browser.  For
         example, password length.  If the password is less than the required password
         length, the client / browser would have passed back the value of TradePortalConstants.ENTRIES_DONT_MATCH. */
      attributeMgr.registerAttribute("passwordValidationError", "passwordValidationError", "LocalAttribute");

      //BSL 09/01/11 CR663 Rel 7.1 Begin
      /* registered_sso_id - Account linking identifier for SSO by Registration */
      attributeMgr.registerAttribute("registered_sso_id", "registered_sso_id");
      attributeMgr.registerAlias("registered_sso_id", getResourceManager().getText("UserBeanAlias.registered_sso_id", TradePortalConstants.TEXT_BUNDLE));

      /* registration_login_id - SSO Registration username */
      attributeMgr.registerAttribute("registration_login_id", "registration_login_id");
      attributeMgr.registerAlias("registration_login_id", getResourceManager().getText("UserBeanAlias.registration_login_id", TradePortalConstants.TEXT_BUNDLE));

      /* registration_password - SSO Registration password.  This is stored as
         an encrypted value.  For more information on the encryption process,
         see the Trade Portal Infrastructure document.*/
      attributeMgr.registerAttribute("registration_password", "registration_password");
      attributeMgr.registerAlias("registration_password", getResourceManager().getText("UserBeanAlias.registration_password", TradePortalConstants.TEXT_BUNDLE));

      /* newRegistrationPassword - This is a local attribute.  It simply holds
         the value that the user entered for their "Registration Password".
         If this value passes validation, it is encrypted and copied into the
         registration_password field. */
      attributeMgr.registerAttribute("newRegistrationPassword", "newRegistrationPassword", "LocalAttribute");
      //BSL 09/01/11 CR663 Rel 7.1 End

      /* client_bank_oid - A shortcut to the client bank that controls this user.  For a client bank
         user, this will be the same as the client bank pointed to in the owner_org_oid
         association.  For bank organization group users, this association will point
         to the BOG's parent.  For corporate customers, this association will point
         to the parent client bank of the corporate customer.   For Propoinx users,
         this will be left blank. */
      attributeMgr.registerAssociation("client_bank_oid", "a_client_bank_oid", "ClientBank");

        /* Pointer to the parent ReferenceDataOwner */
      attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

      /* security_profile_oid - Each user is assigned to a security profile.  This relationship describes
         the rights and privledges that the user has when they are logged into the
         Trade Portal */
      attributeMgr.registerAssociation("security_profile_oid", "a_security_profile_oid", "SecurityProfile");
      attributeMgr.requiredAttribute("security_profile_oid");
      attributeMgr.registerAlias("security_profile_oid",getResourceManager().getText("UserBeanAlias.security_profile_oid",TradePortalConstants.TEXT_BUNDLE));

      /* cust_access_sec_profile_oid - Only used for admin users.   If an admin user is allowed to act on behalf
         of corporate customers using the customer access functionality, this association
         indicates the security profile that is in effect when they are acting on
         behalf of the corporate customer. */
      attributeMgr.registerAssociation("cust_access_sec_profile_oid", "a_cust_access_sec_profile_oid", "SecurityProfile");

      /* threshold_group_oid - Each user is assigned to a threshold group.   This relationship indicates
         how much  a particular user is able to authorize. */
      attributeMgr.registerAssociation("threshold_group_oid", "a_threshold_group_oid", "ThresholdGroup");

      /* work_group_oid - Each user is associated with zero or one WorkGroup */
      attributeMgr.registerAssociation("work_group_oid", "a_work_group_oid", "WorkGroup");

      /* UseSubsidiaryProfile - Use Product Authorisation rules defined on Subsidiary Customer Profile  */
      attributeMgr.registerAttribute("use_subsidiary_profile", "use_subsidiary_profile", "IndicatorAttribute");

      /* SubsidiaryThresholdGroup - Threshold Group to be used with Subsidiary Access  */
      attributeMgr.registerAssociation("subsidiary_threshold_group_oid", "a_sub_threshold_group_oid", "ThresholdGroup");

      /* SubsidiaryWorkGroup - Work Group to be used with Subsidiary Access  */
      attributeMgr.registerAssociation("subsidiary_work_group_oid", "a_sub_work_group_oid", "WorkGroup");

      /*ConfidentialIndicator - This will allow the Corporate Customer to designate whether a particular user has access to Confidential Payment Instruments and Templates. */
      attributeMgr.registerAttribute("confidential_indicator", "confidential_indicator", "IndicatorAttribute");//Vshah CR-586

      /*ConfidentialIndicator - This will allow the Corporate Customer to designate whether a particular user has access to Confidential Payment Instruments and Templates. */
      attributeMgr.registerAttribute("subsid_confidential_indicator", "subsid_confidential_indicator", "IndicatorAttribute");//IAZ CR-586
      
      /* live_market_rate_ind -  */
      attributeMgr.registerAttribute("live_market_rate_ind", "live_market_rate_ind", "IndicatorAttribute");
      
      /* token_type - RSA or VASCO Token used for 2-phase authentication  */
      attributeMgr.registerAttribute("token_type", "token_type");   //AAlubala CR-711 Rel8.0 10/14/2011
      attributeMgr.registerAttribute("auth2fa_subtype", "auth2fa_subtype");   //AAlubala CR-711 Rel8.0 02/08/2012       
      attributeMgr.registerAttribute("showtips", "showtips");//UI preference
      
      //CR 821 - Rel 8.3 START
      /* sub_authorize_own_input_ind - Indicates whether user can authorize their own input.This is in case for the subsidiary, 
       * the �Use Product Authorisation Rules on Subsidiary Customer Profile� is selected. */
      attributeMgr.registerAttribute("sub_authorize_own_input_ind", "sub_authorize_own_input_ind", "IndicatorAttribute");

      /* sub_panel_authority_code - The level at which the reference data is owned (Proponix, client bank, bank
         organization group, or corporate customer.This is in case for the subsidiary, 
       * the �Use Product Authorisation Rules on Subsidiary Customer Profile� is selected. */
      attributeMgr.registerReferenceAttribute("sub_panel_authority_code", "sub_panel_authority_code", "PANEL_AUTH_TYPE");
      //CR 821 - Rel 8.3 END
      
      //CR 821 - Rel 8.3 START
      /* last_login_timestamp - Timestamp (in GMT) when the user was last logged-in. */
      attributeMgr.registerAttribute("last_login_timestamp", "last_login_timestamp", "DateTimeAttribute");
      //CR 821 - Rel 8.3 END
      // JGadela Rel 8.4 CR 854 - reporting_language - Reporting language option 
      attributeMgr.registerAttribute("reporting_language", "reporting_language");
      //JGadela Rel 8.4 CR 854 
      
      //MEerupula Rel 8.4 CR-934A Add mobile banking indicator to enable web services for Users
      //Enabling Users to access the Trade360 Portal via  mobile device(Trade360 Web Services)
      attributeMgr.registerAttribute("mobile_banking_access_ind", "mobile_banking_access_ind", "IndicatorAttribute");

      // W Zhu Rel 9.4 CR-923
      // Certificate type - C for Certificate ID, G for Gemalto
      attributeMgr.registerAttribute("certificate_type", "certificate_type"); 

      // W Zhu 9/28/2015 Rel 9.4 CR-1018 
      // Allow the user to log into bank portal using login-id/password and then Single Sign-On to T360 Portal
      attributeMgr.registerAttribute("allow_sso_password_ind", "allow_sso_password_ind", "IndicatorAttribute");
      
   }

  /*
   * Register the components of the business object
   */
   protected void registerComponents() throws RemoteException, AmsException
   {
      /* Register the components defined in the Ancestor class */
      super.registerComponents();

      /* PasswordHistoryList -  */
      registerOneToManyComponent("PasswordHistoryList","PasswordHistoryList");

      /* UserAuthorizedAccountList -  */
      registerOneToManyComponent("UserAuthorizedAccountList","UserAuthorizedAccountList");

       /* UserAuthorizedTemplateGroupList -  */
      registerOneToManyComponent("UserAuthorizedTemplateGroupList","UserAuthorizedTemplateGroupList");
   }






}
