
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;



/*
 *
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserPreferencesPayDefBean extends UserPreferencesPayDefBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(UserPreferencesPayDefBean.class);
	protected void userValidate() throws AmsException, RemoteException
	{
		LOG.debug("UserPreferencesPayDef.userValidate()....");
		
	}
	
	public void userDelete()
		throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		LOG.debug("delete ....");
		
		
	} 
	
}
