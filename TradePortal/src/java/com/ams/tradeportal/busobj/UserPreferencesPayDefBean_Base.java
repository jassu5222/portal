


package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A UserPreferencesPayDefBean_Base consists of a set of UserPreferencesPayDef Bean
 * to specifiy the requirements of panel authorization.
 *
 *     Copyright  © 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserPreferencesPayDefBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserPreferencesPayDefBean_Base.class);


  /*
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();	

	 /* user_pref_pay_def_oid - Unique Identifier */
      attributeMgr.registerAttribute("user_pref_pay_def_oid", "user_pref_pay_def_oid", "ObjectIDAttribute");   

      /* Pointer to the parent CorporateOrganization */
       attributeMgr.registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

	   attributeMgr.registerAttribute("a_pay_upload_definition_oid", "a_pay_upload_definition_oid");    
      
	   attributeMgr.registerAttribute("pmt_file_default_ind", "pmt_file_default_ind", "IndicatorAttribute");
   }
	
	 protected void registerComponents() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException
   {

      /* Register the components defined in the Ancestor class */
      super.registerComponents();


    
   }
}
