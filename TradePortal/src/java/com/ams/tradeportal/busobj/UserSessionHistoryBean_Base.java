
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserSessionHistoryBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserSessionHistoryBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* session_history_oid - Object identifier of this record */
      attributeMgr.registerAttribute("session_history_oid", "session_history_oid", "ObjectIDAttribute");
      
      /* login_timestamp - The date and time (stored in the database in the GMT timezone) when the
         ActiveUserSession record was created for the user's session. */
      attributeMgr.registerAttribute("login_timestamp", "login_timestamp", "DateTimeAttribute");
      attributeMgr.requiredAttribute("login_timestamp");
      
      /* server_instance_name - The name of the server instance to which the user logged in. */
      attributeMgr.registerAttribute("server_instance_name", "server_instance_name");
      attributeMgr.requiredAttribute("server_instance_name");
      
      /* logout_timestamp - The date and time (stored in the database in the GMT timezone) at which
         the user's session was logged out. */
      attributeMgr.registerAttribute("logout_timestamp", "logout_timestamp", "DateTimeAttribute");
      attributeMgr.requiredAttribute("logout_timestamp");
      
      /* user_oid -  */
      attributeMgr.registerAssociation("user_oid", "a_user_oid", "User");
      attributeMgr.requiredAttribute("user_oid");
      
      //initial or relogon attempt
      attributeMgr.registerAttribute("logon_type", "logon_type");
      
      //if a relogon attempt, did the user confirm relogon or not
      attributeMgr.registerAttribute("confirm_relogon", "confirm_relogon");
      
      //flag to determine if the session should be forced out
      attributeMgr.registerAttribute("force_logout", "force_logout");
   }
   
 
   
 
 
   
}
