 
package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.html.*;




/*
 * 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class WorkGroupBean extends WorkGroupBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(WorkGroupBean.class);

	/**
	 * Default the following WorkGoroup attributes for a new object:
	 * activation_status = ACTIVE   
	 * 
	 * @exception java.rmi.RemoteException 
	 * @exception com.amsinc.ecsg.frame.AmsException 
	 */
	protected void userNewObject() throws java.rmi.RemoteException, com.amsinc.ecsg.frame.AmsException {
		/* Perform any New Object processing defined in the Ancestor class */
		super.userNewObject();
	
		/* Default value for activation_status */
		this.setAttribute("activation_status", TradePortalConstants.ACTIVE);

	} 
	
	
  /**
   * Performs any special "validate" processing that is specific to the
   * descendant of BusinessObject.
   *
   * This method verifies that WorkGroup name is unique for the Client Bank and
   * all amounts have the correct precision
   *
   * @see #validate()
   */

   protected void userValidate() throws AmsException, RemoteException
   {
	/**
	 Verify uniqueness of work_group_name within the corporate organization
	*/
	CorporateOrganization org;
	String baseCurrency;
	Hashtable attributes;
	Enumeration enumer;
	int numAttributes;
	
		// verify that the threshold group name is unique
	
	String name = getAttribute("work_group_name");
	if (!isUnique("work_group_name", name, 
		" and p_corp_org_oid = " + getAttribute("corp_org_oid") 
		+ " and activation_status = '"+TradePortalConstants.ACTIVE+"'")) 
	{
		this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			   TradePortalConstants.ALREADY_EXISTS, name, attributeMgr.getAlias("work_group_name"));
	}

   }


  /**
   * This method is called in the business object
   * method deactivate.
   *
   * Any processing that is necessary before an object is deactivated should be
   * placed within this method.  If business object errors are issued within
   * this method, deactivate processing will not continue and the deactivate
   * transaction will be rolled back.
   *
   * If the Work Group has any active active users, issue error and do not
   * deactivate
   *
   * @see #deactivate()
   */

   protected void userDeactivate() throws AmsException, RemoteException
   {
		// scan the list of users for threshold group
		GenericList usersList;	
		super.userDelete();
		usersList = (GenericList)EJBObjectFactory.createServerEJB(getSessionContext(), 
			"GenericList");
		usersList.prepareList("User", "activeByWorkGroupOid", 
			new String[]{getAttribute("work_group_oid"), TradePortalConstants.ACTIVE});
		usersList.getData();
		if (usersList.getObjectCount() > 0) 
		{
			this.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.CURRENTLY_ASSIGNED, new String[] {getAttribute("work_group_name")});
		}
                
		try
		{
			usersList.remove();
		}
		catch (javax.ejb.RemoveException e)
		{	
			LOG.error("WorkGroupBean:: RemoteException at userDeactivate(): ",e);
		}                
   }


}
