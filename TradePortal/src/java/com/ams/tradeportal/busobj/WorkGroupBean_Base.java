
  

package com.ams.tradeportal.busobj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;




/*
 * A group of users.  The corporate customer can specify whether a transaction
 * must be authorized by two user that belong to different WorkGroups.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class WorkGroupBean_Base extends TradePortalBusinessObjectBean
{
private static final Logger LOG = LoggerFactory.getLogger(WorkGroupBean_Base.class);

  
  /* 
   * Register the attributes and associations of the business object
   */
   protected void registerAttributes() throws AmsException
   {  

      /* Register attributes defined in the Ancestor class */
      super.registerAttributes();
      
      /* work_group_oid - Unique Identifier */
      attributeMgr.registerAttribute("work_group_oid", "work_group_oid", "ObjectIDAttribute");
      
      /* work_group_name - Name of a WorkGroup.  The name must be unique among the WorkGroups of one
         corporate customer. */
      attributeMgr.registerAttribute("work_group_name", "work_group_name");
      attributeMgr.requiredAttribute("work_group_name");
      attributeMgr.registerAlias("work_group_name", getResourceManager().getText("WorkGroupBeanAlias.work_group_name", TradePortalConstants.TEXT_BUNDLE));
      
      /* work_group_description - Description of the WorkGroup */
      attributeMgr.registerAttribute("work_group_description", "work_group_description");
      attributeMgr.requiredAttribute("work_group_description");
      attributeMgr.registerAlias("work_group_description", getResourceManager().getText("WorkGroupBeanAlias.work_group_description", TradePortalConstants.TEXT_BUNDLE));
      
      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      attributeMgr.registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
      
      /* activation_status - Indicates whether or not the WorkGroup has been deactivated. WorkGroups
         are not deleted from the database; they are deactivated instead so that
         the transactions authorized with these WorkGroups still has valid association. */
      attributeMgr.registerReferenceAttribute("activation_status", "activation_status", "ACTIVATION_STATUS");
      
      /* date_deactivated - Timestamp (in GMT) of when the WorkGroup was deactivated. */
      attributeMgr.registerAttribute("date_deactivated", "date_deactivated", "DateTimeAttribute");
      
        /* Pointer to the parent CorporateOrganization */
      attributeMgr.registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
   
   }
   
 
   
 
 
   
}
