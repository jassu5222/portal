
  

package com.ams.tradeportal.busobj;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * A group of users.  The corporate customer can specify whether a transaction
 * must be authorized by two user that belong to different WorkGroups.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface WorkGroupHome extends EJBHome
{
   public WorkGroup create()
      throws RemoteException, CreateException, AmsException;

   public WorkGroup create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
