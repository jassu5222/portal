package com.ams.tradeportal.busobj.termsmgr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

/**
 * This class manages the dynamic attribute registration and validation for a Terms business object or TermsWebBean for bank
 * released terms of a transaction of type Advise
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ADVBankRelTermsMgr extends TermsMgr {
	private static final Logger LOG = LoggerFactory.getLogger(ADVBankRelTermsMgr.class);

	/**
	 * Constructor that is used by business objects to create their manager. This method also register the dynamic attributes of the
	 * bean.
	 *
	 * @param mgr
	 *            AttributeManager attribute manager of the bean being managed. Used to register attributes
	 */
	public ADVBankRelTermsMgr(AttributeManager mgr) throws AmsException {
		super(mgr);
	}

	/**
	 * Constructor that is used by web beans to create their manager. This method also register the dynamic attributes of the bean.
	 *
	 * @param mgr
	 *            TermsWebBean - the bean being managed. Used to register attributes.
	 */
	public ADVBankRelTermsMgr(TermsWebBean terms) throws AmsException {
		super(terms);
	}

	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for bank released terms with this
	 * transaction type
	 *
	 * @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister() {
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add(TermsBean.Attributes.create_amount());
		attrList.add(TermsBean.Attributes.create_amount_currency_code());
		attrList.add(TermsBean.Attributes.create_amt_tolerance_neg());
		attrList.add(TermsBean.Attributes.create_amt_tolerance_pos());
		attrList.add(TermsBean.Attributes.create_confirmation_type());
		attrList.add(TermsBean.Attributes.create_confirmation_indicator());
		attrList.add(TermsBean.Attributes.create_expiry_date());
		attrList.add(TermsBean.Attributes.create_opening_bank_instr_num());
		attrList.add(TermsBean.Attributes.create_pmt_terms_days_after_type());
		attrList.add(TermsBean.Attributes.create_pmt_terms_fixed_maturity_date());
		attrList.add(TermsBean.Attributes.create_pmt_terms_num_days_after());
		attrList.add(TermsBean.Attributes.create_pmt_terms_percent_invoice());
		attrList.add(TermsBean.Attributes.create_pmt_terms_type());
		attrList.add(TermsBean.Attributes.create_final_expiry_date());
		attrList.add(TermsBean.Attributes.create_transferrable());

		return attrList;
	}

	/**
	 * This method is supposed to be used to set attributes of Terms business object.
	 *
	 * @param inputDoc
	 *            DocumentHandler - universal XML message from MQSeries
	 * @param terms
	 *            TermsBean
	 * @return boolean
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc) throws java.rmi.RemoteException, AmsException {

		terms.setAttribute("amount", inputDoc.getAttribute("/Terms/TermsDetails/Amount"));
		terms.setAttribute("amount_currency_code", inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
		terms.setAttribute("amt_tolerance_pos", inputDoc.getAttribute("/Terms/TermsDetails/AmountTolerancePositive"));
		terms.setAttribute("amt_tolerance_neg", inputDoc.getAttribute("/Terms/TermsDetails/AmountToleranceNegative"));
		terms.setAttribute("expiry_date", inputDoc.getAttribute("/Terms/TermsDetails/ExpiryDate"));
		terms.setAttribute("confirmation_type", inputDoc.getAttribute("/Terms/OtherConditions/ConfirmationType"));
		terms.setAttribute("pmt_terms_fixed_maturity_date",
				inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsFixedMaturityDate"));
		terms.setAttribute("pmt_terms_type", inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsType"));
		terms.setAttribute("pmt_terms_num_days_after", inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsNumberDaysAfter"));
		terms.setAttribute("pmt_terms_days_after_type", inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsDaysAfterType"));
		terms.setAttribute("pmt_terms_percent_invoice", inputDoc.getAttribute("/Terms/PaymentTerms/PaymentTermsPercentInvoice"));

		String myConfirmationType = inputDoc.getAttribute("/Terms/OtherConditions/ConfirmationType");
		if ((myConfirmationType != null) && (myConfirmationType.equals(TradePortalConstants.ADDING_CONFIRMATION)
				|| myConfirmationType.equals(TradePortalConstants.SILENT_CONFIRMATION))) {
			terms.setAttribute("confirmation_indicator", TradePortalConstants.INDICATOR_YES);
		} else if ((myConfirmationType != null) && (myConfirmationType.equals(TradePortalConstants.NOT_ADDING_CONFIRMATION)
				|| myConfirmationType.equals(TradePortalConstants.BANK_MAY_ADD))) {
			terms.setAttribute("confirmation_indicator", TradePortalConstants.INDICATOR_NO);
		}

		// Now create the shipment terms since some of the data needs to go there
		ShipmentTerms shipmentTerms = terms.createFirstShipment();
		shipmentTerms.setAttribute("latest_shipment_date", inputDoc.getAttribute("/Terms/ShipmentTerms/LatestShipmentDate"));

		terms.setAttribute("final_expiry_date", inputDoc.getAttribute("/Terms/TermsDetails/FinalExpiryDate"));
		if (StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/TermsDetails/Transferrable"))) {
			terms.setAttribute("transferrable", inputDoc.getAttribute("/Terms/TermsDetails/Transferrable"));
		}

		return true;
	}

}