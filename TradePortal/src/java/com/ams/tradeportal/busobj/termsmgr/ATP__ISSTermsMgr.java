package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.AdditionalReqDoc;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for Approval to Pay.
 *
 * Note: for Approval to Pay, the 3 terms parties represent these types:
 *        first     beneficiary
 *        second    applicant
 *        third     advising bank
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ATP__ISSTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(ATP__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public ATP__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public ATP__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for Approval to Pay
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_additional_conditions() );
		attrList.add( TermsBean.Attributes.create_addl_doc_indicator() );
		attrList.add( TermsBean.Attributes.create_addl_doc_text() );
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
		attrList.add( TermsBean.Attributes.create_bank_charges_type() );
		attrList.add( TermsBean.Attributes.create_branch_code() );
		attrList.add( TermsBean.Attributes.create_cert_origin_copies() );
		attrList.add( TermsBean.Attributes.create_cert_origin_indicator() );
		attrList.add( TermsBean.Attributes.create_cert_origin_originals() );
		attrList.add( TermsBean.Attributes.create_cert_origin_text() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_copies() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_indicator() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_originals() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_text() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
		attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_fec_amount() );
		attrList.add( TermsBean.Attributes.create_fec_maturity_date() );
		attrList.add( TermsBean.Attributes.create_fec_rate() );
		attrList.add( TermsBean.Attributes.create_finance_drawing () );
		attrList.add( TermsBean.Attributes.create_finance_drawing_num_days() );
		attrList.add( TermsBean.Attributes.create_finance_drawing_type() );
		attrList.add( TermsBean.Attributes.create_ins_policy_copies() );
		attrList.add( TermsBean.Attributes.create_ins_policy_indicator() );
		attrList.add( TermsBean.Attributes.create_ins_policy_originals() );
		attrList.add( TermsBean.Attributes.create_ins_policy_text() );
		attrList.add( TermsBean.Attributes.create_insurance_endorse_value_plus() );
		attrList.add( TermsBean.Attributes.create_insurance_risk_type() );
		attrList.add( TermsBean.Attributes.create_internal_instructions() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_originals() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_text () );
		attrList.add( TermsBean.Attributes.create_packing_list_copies() );
		attrList.add( TermsBean.Attributes.create_packing_list_indicator() );
		attrList.add( TermsBean.Attributes.create_packing_list_originals() );
		attrList.add( TermsBean.Attributes.create_packing_list_text() );
		attrList.add( TermsBean.Attributes.create_place_of_expiry() );
		attrList.add( TermsBean.Attributes.create_present_docs_days_user_ind() );
		attrList.add( TermsBean.Attributes.create_present_docs_within_days() );
		attrList.add( TermsBean.Attributes.create_purpose_type() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_settlement_our_account_num() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_partial_shipment_allowed() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
		attrList.add( TermsBean.Attributes.create_invoice_only_ind() );
		attrList.add( TermsBean.Attributes.create_invoice_due_date() );
		attrList.add( TermsBean.Attributes.create_invoice_details() );//SHR CR 708 Rel8.1.1
		//Srinivasu IR#T36000023986 Rel8.4 01/21/2014 - start
		attrList.add( TermsBean.Attributes.create_guar_deliver_by() );
		attrList.add( TermsBean.Attributes.create_guar_deliver_to() );
		//Srinivasu IR#T36000023986 Rel8.4 01/21/2014 - end

		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		// Variables used for shipment terms validations
		boolean       hasMultipleShipments = false;
		String []     requiredAttributes   = new String [1];
		String        shipmentNumber;
		StringBuffer  shipmentNumDesc;
		int           shipmentCount;
		int           shipmentIndex;
		String        shipmentOid;
		Vector        shipmentTermsSorted = new Vector();
		ComponentList shipmentTermsList;
		ShipmentTerms shipmentTerms;

		super.validate(terms);

		terms.registerRequiredAttribute("expiry_date");
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");
		terms.registerRequiredAttribute("bank_charges_type");
		// The second terms party is the applicant.
		terms.registerRequiredAttribute("c_SecondTermsParty");

		validateGeneral(terms);
		validateDocsRequired(terms);

		// Get a handle to the term's shipment terms components
		shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		// Sort the list of shipment terms so they are placed in the order they were created
		shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);

		shipmentCount = shipmentTermsSorted.size();

		// Validate each set of shipment terms
		for (shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
		{
			shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
			shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
			shipmentNumDesc = new StringBuffer();

			// If there are multiple shipments, validate any required attributes
			if (shipmentCount > 1)
			{
				hasMultipleShipments = true;
				shipmentNumber = String.valueOf(shipmentIndex + 1);
				shipmentNumDesc.append(terms.getResourceManager().getText("ApprovalToPayIssue.Shipment",
						TradePortalConstants.TEXT_BUNDLE));
				shipmentNumDesc.append(" ");
				shipmentNumDesc.append(shipmentNumber + " -");
				requiredAttributes[0] = "description";
				shipmentTerms.validateRequiredAttributes(requiredAttributes, shipmentNumDesc.toString());
			}
			else
			{
				shipmentNumDesc.append(terms.getResourceManager().getText("ApprovalToPayIssue.Shipment",
						TradePortalConstants.TEXT_BUNDLE));
				shipmentNumDesc.append(" -");
			}

			// Perform subclass-specific validation of text fields for SWIFT characters
			validateForSwiftCharacters(shipmentTerms);

			// Validate the transport docs section
			validateTransportDocs(terms, shipmentTerms, shipmentNumDesc.toString(), hasMultipleShipments);

			// validate the shipment section
			validateShipment(terms, shipmentTerms, shipmentNumDesc.toString());
		}

		validateOtherConditions(terms);
		validateInstructions(terms);
		validateBeneficiary(terms);


	}


	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
	{
		final String[] attributesToExclude = {"special_bank_instructions", "branch_code", "settlement_our_account_num",
				"settlement_foreign_acct_num", "coms_chrgs_our_account_num", "coms_chrgs_foreign_acct_num",
				"coms_chrgs_other_text", "covered_by_fec_number", "internal_instructions"};

		InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

		validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "ApprovalToPayIssue.Seller");
		validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "ApprovalToPayIssue.Buyer");
		validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "ApprovalToPayIssue.InsuringParty");
	}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the ShipmentTerms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(ShipmentTerms shipmentTerms) throws AmsException, RemoteException
	{
		InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());

		validateTermsPartyForSwiftChars(shipmentTerms, "NotifyParty", "ApprovalToPayIssue.NotifyParty");
		validateTermsPartyForSwiftChars(shipmentTerms, "OtherConsigneeParty", "ApprovalToPayIssue.OtherConsignee");
	}

	/**
	 * Get the other consignee terms party and then ask it to validate
	 * himself
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 */
	public void checkOtherConsignee(ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws RemoteException, AmsException
	{
		this.editForAnyConsigneeValue(shipmentTerms, "OtherConsigneeParty", shipmentNumDesc);
	}

	/**
	 * Get the Beneficiary terms party and then ask it to validate himself.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateBeneficiary(TermsBean terms)
	throws RemoteException, AmsException
	{
		this.editRequiredBeneficiaryValue(terms, "FirstTermsParty", "ApprovalToPayIssue.Seller");
	}

	/**
	 * Performs validation for the documents required section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateDocsRequired(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		String value;

		// For each of the 4 types of required documents (Commercial Invoice,
		// Packing List, Certificate of Origin, and Insurance Policy) and the
		// 4 freeform other documents, the rules are:
		// if the checkbox is checked, then either originals or copies must
		// be non-blank (valid number check is done elsewhere).  If
		// the checkbox is unchecked, blank out the values.  The text field
		// is never required.  For the freeform fields, the name is required
		// if the checkbox is checked
		value = terms.getAttribute("comm_invoice_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("comm_invoice_originals", "");
			terms.setAttribute("comm_invoice_copies", "");
			terms.setAttribute("comm_invoice_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("comm_invoice_originals"))
					&& StringFunction.isBlank(terms.getAttribute("comm_invoice_copies"))) {
				value = terms.getResourceManager().getText("ApprovalToPayIssue.CommercialInv",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		value = terms.getAttribute("packing_list_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("packing_list_originals", "");
			terms.setAttribute("packing_list_copies", "");
			terms.setAttribute("packing_list_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("packing_list_originals"))
					&& StringFunction.isBlank(terms.getAttribute("packing_list_copies"))) {
				value = terms.getResourceManager().getText("ApprovalToPayIssue.PackingList",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		value = terms.getAttribute("cert_origin_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("cert_origin_originals", "");
			terms.setAttribute("cert_origin_copies", "");
			terms.setAttribute("cert_origin_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("cert_origin_originals"))
					&& StringFunction.isBlank(terms.getAttribute("cert_origin_copies"))) {
				value = terms.getResourceManager().getText("ApprovalToPayIssue.OriginCertificate",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		// Additional validation for ins policy: If the checkbox is checked,
		// endorse value and risk type fields are both required.
		value = terms.getAttribute("ins_policy_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("ins_policy_originals", "");
			terms.setAttribute("ins_policy_copies", "");
			terms.setAttribute("ins_policy_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("ins_policy_originals"))
					&& StringFunction.isBlank(terms.getAttribute("ins_policy_copies"))) {
				value = terms.getResourceManager().getText("ApprovalToPayIssue.InsPolicyCert",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
			if (StringFunction.isBlank(terms.getAttribute("insurance_endorse_value_plus"))
					|| StringFunction.isBlank(terms.getAttribute("insurance_risk_type"))) {
				value = terms.getResourceManager().getText("ApprovalToPayIssue.InsPolicyCert",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INS_DOC_INFO_MISSING,
						value);
			}
		}

		// Validation for the 4 "Other" docs is the same
		for (int x=1; x<=4; x++) {
			String prefix = "other_req_doc_" + x;
			value = terms.getAttribute(prefix + "_indicator");
			LOG.debug("the other doc indicator for {} is {}", x, value);
			if (StringFunction.isBlank(value) ||
					value.equals(TradePortalConstants.INDICATOR_NO)) {
				LOG.debug("resetting to blank");
				terms.setAttribute(prefix + "_originals", "");
				terms.setAttribute(prefix + "_copies", "");
				terms.setAttribute(prefix + "_text", "");
				terms.setAttribute(prefix + "_name", "");

			} else {
				// Either originals or copies is required.  Name is required.
				if (StringFunction.isBlank(terms.getAttribute(prefix + "_originals"))
						&& StringFunction.isBlank(terms.getAttribute(prefix + "_copies"))) {
					// We're missing some information.  Give an error.  Try to use the
					// doc name given by the user.  Otherwise use a generic "Other document x".
					value = terms.getAttribute(prefix + "_name");
					if (StringFunction.isBlank(value)) {
						value = terms.getResourceManager().getText("ApprovalToPayIssue.OtherDocument",
								TradePortalConstants.TEXT_BUNDLE);
						value += " " + x;
					}
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.REQD_DOC_INFO_MISSING,
							value);
				}
				if (StringFunction.isBlank(terms.getAttribute(prefix + "_name"))) {
					value = terms.getResourceManager().getText("ApprovalToPayIssue.DocName",
							TradePortalConstants.TEXT_BUNDLE);
					value += " " + x;
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							AmsConstants.REQUIRED_ATTRIBUTE,
							value);
				}
			}
		}

		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		//Get a handle to the term's shipment terms components
		ComponentList additionalReqDocList = (ComponentList) terms.getComponentHandle("AdditionalReqDocList");

		int addRedDocCount = additionalReqDocList.getObjectCount();

		// Validate each set of shipment terms
		for (int addReqDocIndex = 0; addReqDocIndex < addRedDocCount; addReqDocIndex++){
			additionalReqDocList.scrollToObjectByIndex(addReqDocIndex);
			value = additionalReqDocList.getListAttribute("addl_req_doc_ind");
			if (StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)) {
				// Both originals or copies is required.  As well as Name is required.
				if (StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_originals"))
						&& StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_copies"))) {
					value = additionalReqDocList.getListAttribute("addl_req_doc_name");
					if (StringFunction.isBlank(value)) {
						value = "Document Name "+(addReqDocIndex+5);
					}
					terms.getErrorManager().issueError(	TradePortalConstants.ERR_CAT_1,
														TradePortalConstants.REQD_DOC_INFO_MISSING,
														value);
				}
				if (StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_name"))) {
					terms.getErrorManager().issueError(	TradePortalConstants.ERR_CAT_1, 
														AmsConstants.REQUIRED_ATTRIBUTE,
														"Document Name "+(addReqDocIndex+5));
				}
			}
		
		}
		
		//Leelavathi IR#T36000021281 11/21/2013 Rel8400 Begin
		value = terms.getAttribute("addl_doc_indicator");
		if ((StringFunction.isNotBlank(value) &&
				value.equals(TradePortalConstants.INDICATOR_YES))
				&& StringFunction.isBlank(terms.getAttribute("addl_doc_text"))) {
				terms.getErrorManager().issueError(
					 TradePortalConstants.ERR_CAT_1,TradePortalConstants.ADDL_DOC_INFO_MISSING,	value);
		}
		
	}

	/**
	 *  Performs validation of the other Conditions section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */

	public void validateOtherConditions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{


		String value;
		boolean checkForPOs = false;
		String InvoiceDate;

//		Invoice Only Indicator
		value = terms.getAttribute("invoice_only_ind");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			// Check if invoice due date is blank
			value = terms.getAttribute("invoice_due_date");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INV_DUE_DATE_REQD);
			}
			checkForPOs = true;

		} else {
			value = terms.getAttribute("invoice_due_date");
			if (!StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INV_ONLY_REQD);
				checkForPOs = true;
			}

		}

		if (checkForPOs) {
			String  shipmentOid;
			ComponentList shipmentTermsList;
			DocumentHandler poLineItemsDoc;

			shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

			shipmentOid   = shipmentTermsList.getListAttribute("shipment_oid");

			if (!StringFunction.isBlank(shipmentOid))
			{
				// First compose the SQL to retrieve PO Line Items that are currently assigned to
				// this shipment
				//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
				String sqlQuery = " select a_source_upload_definition_oid, ben_name, currency, source_type " +
				" from po_line_item where p_shipment_oid = ?";

				// Retrieve any PO line items that satisfy the SQL query that was just constructed
				poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, 
						new Object[]{shipmentOid});
				//SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End

				if (poLineItemsDoc != null)
				{
					terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PO_INV_ONLY);

				}
			}


		}
//		 Expiry date must be after invoice due date (if provided).
		// (Expiry date is a required field.  Invoice due date is not.)
		InvoiceDate = terms.getAttribute("invoice_due_date");
		value = terms.getAttribute("expiry_date");
		if (!StringFunction.isBlank(InvoiceDate) && !StringFunction.isBlank(value)) {
			GregorianCalendar InvDate = new GregorianCalendar();
			GregorianCalendar expiryDate = new GregorianCalendar();

			InvDate.setTime(DateTimeUtility.convertStringDateTimeToDate(InvoiceDate));
			expiryDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));
			
			/* IR T36000017596 -error out if expiry date does not match invoice due/payment date
			// If the ship date is after the expiry date, give error
			//Krishna IR-RAUI011460207-(Removed ||(InvDate.equals(expiryDate)) from if condition)
			if ((InvDate.after(expiryDate))) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INV_DATE_PAST_EXPIRY_DATE);
				} */
			
			if (!(InvDate.equals(expiryDate))) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.EXP_DUE_DATE_UNMATCH);
				}
			// IR T36000017596 - end 
		 }
		//SHR
		if(StringFunction.isNotBlank(value)){
			Date date1 = DateTimeUtility.convertStringDateToDate(value);
			Date date2 = GMTUtility.getGMTDateTime();
			if(date1.before(date2)){
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SYSTEM_DATE_AFTER_EXPIRY);
			}
		}
		//SHR
	}

	/**
	 *  Performs validation of the general section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateGeneral(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		String value;

		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END

		// Validate the Amount currency code with the PO Line Items currency code.  They must match.
		validateAmountCurrencyAndPOCurrency(terms);
		validateAmountCurrencyAndInvoiceCurrency(terms);
	}


	/**
	 * Performs validation for the bank instructions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;
		String alias;

		// If the foreign account number is given, the foreign currency
		// must also be given; and vice-versa.
		value = terms.getAttribute("settlement_foreign_acct_num");

		if (!StringFunction.isBlank(value))
		{
			value = terms.getAttribute("settlement_foreign_acct_curr");

			if (StringFunction.isBlank(value))
			{
				alias = terms.getAlias("settlement_foreign_acct_curr");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SETTLE_INFO_MISSING, alias);
			}
		}
		else
		{
			value = terms.getAttribute("settlement_foreign_acct_curr");

			if (!StringFunction.isBlank(value))
			{
				alias = terms.getAlias("settlement_foreign_acct_num");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SETTLE_INFO_MISSING, alias);
			}
		}

		// If finance drawing indicator is Y, then num days and type
		// fields are required.
		value = terms.getAttribute("finance_drawing");
		if (!StringFunction.isBlank(value)
				&& value.equals(TradePortalConstants.INDICATOR_YES)) {
			value = terms.getAttribute("finance_drawing_num_days");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FINANCE_INFO_REQD);
			}
			value = terms.getAttribute("finance_drawing_type");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FINANCE_INFO_REQD);
			}
		} else {
			// finance_drawing checkbox is unchecked -- blank out the fields
			// associated with this checkbox.
			terms.setAttribute("finance_drawing_num_days", "");
			terms.setAttribute("finance_drawing_type", "");
		}

		// If the c&c foreign account number is given, the c&c foreign currency
		// must also be given; and vice-versa.
		value = terms.getAttribute("coms_chrgs_foreign_acct_num");

		if (!StringFunction.isBlank(value))
		{
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (StringFunction.isBlank(value))
			{
				alias = terms.getAlias("coms_chrgs_foreign_acct_curr");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
			}
		}
		else
		{
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (!StringFunction.isBlank(value))
			{
				alias = terms.getAlias("coms_chrgs_foreign_acct_num");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
			}
		}
	}

	/**
	 * Performs validation for the shipment section.
	 *
	 * @param currentTerms com.ams.tradeportal.busobj.TermsBean
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 */
	public void validateShipment(TermsBean currentTerms, ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws java.rmi.RemoteException, AmsException {

		String value;
		String incoterm;
		String location;
		String shipmentDate;
		String alias;
		String shipFrom;
		String shipLoad;
		String shipTo;
		String shipDischarge;
		String invoiceDate;
		//VS IR SDUH021456070 01/13/11 Rel 7.0
		final long ONE_HOUR = 60 * 60 * 1000L;


		// Shipment date must be valid.  This has already been done.
		// (Blank date is "valid")

		// Expiry date must be after shipment date (if provided).
		// (Expiry date is a required field.  Shipment date is not.)
		shipmentDate = shipmentTerms.getAttribute("latest_shipment_date");
		value = currentTerms.getAttribute("expiry_date");
		if (!StringFunction.isBlank(shipmentDate) && !StringFunction.isBlank(value)) {
			GregorianCalendar shipDate = new GregorianCalendar();
			GregorianCalendar expiryDate = new GregorianCalendar();

			shipDate.setTime(DateTimeUtility.convertStringDateTimeToDate(shipmentDate));
			expiryDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));

			//VS IR SDUH021456070 01/13/11 Rel 7.0 Begin

			long diffDays = ((expiryDate.getTimeInMillis() - shipDate.getTimeInMillis()  + ONE_HOUR)/ (ONE_HOUR * 24));

			//The number of days between the Expiry Date and Latest Shipment Date cannot be greater than 999 days.
			if (diffDays > 999){
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DIFFERENCE_IN_EXPIRY_DATE_AND_SHIP_DATE,
						errorParameters,
						wrapParameters);
			}
			//VS IR SDUH021456070 01/13/11 Rel 7.0 End

			// If the ship date is after the expiry date, give error
			if (shipDate.after(expiryDate)) {
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SHIP_DATE_PAST_EXPIRY_DATE,
						errorParameters,
						wrapParameters);
			}

			// Now try adding the present days attribute to the ship date.
			// This should be equal to or later than the expiry date.  If
			// not, give an error.
			try {
				String presentDays = currentTerms.getAttribute("present_docs_within_days");
				String indicator = currentTerms.getAttribute("present_docs_days_user_ind");

				// Calculate the presentation days when it is not set by the user (as indicated by the
				// present_docs_days_user_ind)
				if(StringFunction.isBlank(indicator))
				{
					presentDays = Integer.toString(calculatePresentationDays(shipDate, expiryDate));
					currentTerms.setAttribute("present_docs_within_days", presentDays);
				}

				shipDate.add(Calendar.DATE, Integer.parseInt(presentDays));
				if (shipDate.before(expiryDate))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PRESENT_DATE_BEFORE_EXPIRY,
							errorParameters,
							wrapParameters);
				}
				else if (shipDate.after(expiryDate))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PRESENT_DATE_AFTER_EXPIRY,
							errorParameters,
							wrapParameters);
				}
			} catch (Exception e) {
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PRESENT_DATE_BEFORE_EXPIRY,
						errorParameters,
						wrapParameters);
			}
			// Set Presentation Days to 21 if Shipment Date and Presentation Days are both blank
		} else if (StringFunction.isBlank(shipmentDate)) {
			String presentationDays = currentTerms.getAttribute("present_docs_within_days");
			if (StringFunction.isBlank(presentationDays)) {
				currentTerms.setAttribute("present_docs_within_days", Long.toString(TradePortalConstants.DEFAULT_PRESENTATION_DAYS));
			}
		}

		// If either incoterm or location is provided, both must be given.
		incoterm = shipmentTerms.getAttribute("incoterm");
		location = shipmentTerms.getAttribute("incoterm_location");
		if (!StringFunction.isBlank(incoterm)
				&& StringFunction.isBlank(location)) {
			// Location is missing
			alias = shipmentTerms.getAlias("incoterm_location");
			String[] errorParameters = {alias, shipmentNumDesc};
			boolean[] wrapParameters = {true, false};

			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REQUIRED_ATTRIBUTE,
					errorParameters, wrapParameters);

		}
		if (StringFunction.isBlank(incoterm)
				&& !StringFunction.isBlank(location)) {
			// Incoterm is missing
			alias = shipmentTerms.getAlias("incoterm");
			String[] errorParameters = {alias, shipmentNumDesc};
			boolean[] wrapParameters = {true, false};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REQUIRED_ATTRIBUTE,
					errorParameters, wrapParameters);
		}

		// If incoterm, location and latest shipment date are all blank
		// give a missing information warning for all three.
		//Leelavathi IR#T36000017675 Rel8400 25/11/2013 Begin
		if (StringFunction.isBlank(incoterm)
				&& StringFunction.isBlank(location)
				&& StringFunction.isBlank(shipmentDate)) {
			alias = shipmentTerms.getAlias("incoterm_location");
			boolean[] wrapParameters = {true, false};

			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_DET_INC_LOC_MISSING);
			alias = shipmentTerms.getAlias("incoterm");
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_DET_INCOTERN_MISSING);
			//Leelavathi IR#T36000017675 Rel8400 25/11/2013 End 
			alias = shipmentTerms.getAlias("latest_shipment_date");
			String[] errorParameters3 = {alias, shipmentNumDesc};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_INFO_MISSING,
					errorParameters3, wrapParameters);
		}

		// Give warning if both shipment from fields are blank
		shipFrom = shipmentTerms.getAttribute("shipment_from");
		shipLoad = shipmentTerms.getAttribute("shipment_from_loading");
		if (StringFunction.isBlank(shipFrom) && StringFunction.isBlank(shipLoad)) {
			//Leelavathi IR#T36000017675 Rel8400 25/11/2013 Begin
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_DET_FROM_MISSING);
		}
		//Leelavathi IR#T36000017675 Rel8400 25/11/2013 End
		// Give warning if both shipment to fields are blank.
		shipTo = shipmentTerms.getAttribute("shipment_to");
		shipDischarge= shipmentTerms.getAttribute("shipment_to_discharge");
		if (StringFunction.isBlank(shipTo)&& StringFunction.isBlank(shipDischarge)) {
			//Leelavathi IR#T36000017675 Rel8400 25/11/2013 Begin
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_DET_TO_MISSING);
			//Leelavathi IR#T36000017675 Rel8400 25/11/2013 End
		}

		String freightType = shipmentTerms.getAttribute("trans_doc_marked_freight");
		if (!StringFunction.isBlank(incoterm)) {
			if (StringFunction.isBlank(freightType)) {
				alias = shipmentTerms.getAlias("incoterm");
				String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
				String[] errorParameters = {alias, alias2, shipmentNumDesc};
				boolean[] wrapParameters = {true, true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_SHIP_TERM_COMBO,
						errorParameters, wrapParameters);
			} else {
				if (freightType.equals(TradePortalConstants.COLLECT)) {
					// For COLLECT freight type, EXW, FCA, FAS, and FOB are
					// the only allowed values for the incoterm.  If not one
					// of these, give error.
					if (!( incoterm.equals(TradePortalConstants.SHIPPING_EXW)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FCA)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FAS)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FOB))) {
						alias = shipmentTerms.getAlias("incoterm");
						String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_SHIP_TERM_COMBO,
								errorParameters, wrapParameters);
					}
				} else {
					// For PREPAID freight type, EXW, FCA, FAX, and FOB are
					// NOT allowed for the incoterm.  If one of these values,
					// give error.
					if ( ( incoterm.equals(TradePortalConstants.SHIPPING_EXW)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FCA)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FAS)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FOB))) {
						alias = shipmentTerms.getAlias("incoterm");
						String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_SHIP_TERM_COMBO,
								errorParameters, wrapParameters);
					}
				}
			}
		}

		// Validate the Goods Description and PO Line Items fields
		validateGoodsDescriptionFields(shipmentTerms, shipmentNumDesc, currentTerms);//SHR IR 6384
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			checkTextLength(terms, "additional_conditions", 5000);
		} 
		checkTextLength(terms, "coms_chrgs_other_text", 1000);
		checkTextLength(terms, "internal_instructions", 1000);
		checkTextLength(terms, "special_bank_instructions", 1000);
		checkTextLength(terms, "comm_invoice_text", 1000);
		checkTextLength(terms, "packing_list_text", 1000);
		checkTextLength(terms, "cert_origin_text", 1000);
		checkTextLength(terms, "ins_policy_text", 1000);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			checkTextLength(terms, "addl_doc_text", 1000);
		} 
		checkTextLength(terms, "other_req_doc_1_text", 1000);
		checkTextLength(terms, "other_req_doc_2_text", 1000);
		checkTextLength(terms, "other_req_doc_3_text", 1000);
		checkTextLength(terms, "other_req_doc_4_text", 1000);
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms ShipmentTerms - the shipment terms being validated
	 *  @param shipmentNumDesc String - the shipment number
	 */
	public void validateTextLengths(TermsBean terms, ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws java.rmi.RemoteException, AmsException
	{	
		checkTextLength(shipmentTerms, shipmentNumDesc, "trans_addl_doc_text", 1000);
		checkTextLength(shipmentTerms, shipmentNumDesc, "trans_doc_text", 500);
	}

	/**
	 * This method is used to validate the Goods Description and PO Line Items
	 * fields. If nothing has been entered in at least one of these two fields, an
	 * error message will be issued indicating that the Goods Description field is
	 * required (Note: we only use 'Goods Description' here because we can't assume
	 * that the user and user's org have rights to process purchase orders). If
	 * data has been entered in the Goods Description text area box, the method
	 * will issue an error if its length exceeds 6500 characters. If the length
	 * does NOT exceed 6500 characters and data has been enetered in the PO Line
	 * Items text area box, it will then determine whether the combined lengths are
	 * greater than 6500 characters; an appropriate error message will be issued if
	 * this total is greater than 6500.
	 *

	 * @param shipmentNumDesc java.lang.String
	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateGoodsDescriptionFields(ShipmentTerms shipmentTerms, String shipmentNumDesc, TermsBean terms)
	throws RemoteException, AmsException
	{
		String   goodsDescription = null;
		String   poLineItems      = null;
		String   endText          = null;
		
		goodsDescription = shipmentTerms.getAttribute("goods_description");
		poLineItems      = shipmentTerms.getAttribute("po_line_items");
		String invDetails = terms.getAttribute("invoice_details"); //SHR IR 6384

		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(shipmentTerms);

		// If nothing has been entered in the Goods Description text area box, determine whether
		// data was entered in the PO Line Items text area box
		if (StringFunction.isBlank(goodsDescription))
		{
			// If nothing was entered in the PO Line Items text area box as well, issue an error
			// indicating that the Goods Description field is required; otherwise, check the
			// number of characters in this field and issue an error if it exceeds 6500

			//SHR IR 6384 - goods description is required only if invoices are not in ATP instrument
			if (StringFunction.isBlank(poLineItems) && StringFunction.isBlank(invDetails))
			{
				String[] errorParameters = {shipmentTerms.getAlias("goods_description"), shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REQUIRED_ATTRIBUTE,
						errorParameters, wrapParameters);
			}
			else if ((poLineItems.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
						errorParameters, wrapParameters);
			}
		}
		else
		{
			// If the number of characters in the Goods Description text area box is greater
			// than 6500, issue an approrpiate error; otherwise, check the combined lengths of
			// the Goods Description and PO Line Items text area boxes and issue an error if
			// this total exceeds 6500 characters
			if ((goodsDescription.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				endText = goodsDescription.substring(6480, 6500);

				String[] errorParameters = {shipmentTerms.getAlias("goods_description"), endText, shipmentNumDesc};
				boolean[] wrapParameters = {true, true, false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
						errorParameters, wrapParameters);
			}
			else
			{
				if ((!StringFunction.isBlank(poLineItems))
						&& ((goodsDescription.length() + poLineItems.length()) > 6500)
						&& (overrideSwiftLengthInd != null)
						&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
							errorParameters, wrapParameters);
				}
			}
		}
	}



	/**
	 * This method is used to validate the currency Code of the Transaction Amount
	 * with the Currency Code of the PO Line Items.  The two must match.  The PO Line Items
	 * will always have the same currency code as per the PO design validations, therefore, we
	 * only need to determine what that single currency code is and compare it to the currency
	 * code of the transaction.
	 *

	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateAmountCurrencyAndPOCurrency(TermsBean terms) throws RemoteException, AmsException
	{

		// Get the currency code of the transaction.  If not blank, proceed with validating that it is
		// the same as the PO line items currency code, otherwise fall through and let the required
		// validation process flag it.
		String amountCurrency = terms.getAttribute("amount_currency_code");

		if ((amountCurrency != null) && (!amountCurrency.equals("")))
		{
			// Query the database to get the currency code of the PO line items.
			LOG.debug("Inside validateAmountCurrencyAndPOCurrency() - Getting currency code from po_line_item using sql statement.");

			DocumentHandler    poLineItemCurrencyDoc       = null;
			String             poLineItemCurrencyCode 	   = null;
			String 			   termsOid 			   	   = terms.getAttribute("terms_oid");


			String sqlQuery = "select distinct p.currency "
					+ "from po_line_item p, terms t, transaction r "
					+ "where t.terms_oid = r.c_cust_enter_terms_oid "
					+ "and r.transaction_oid = p.a_assigned_to_trans_oid "
					+ "and t.terms_oid = ?";

		
			// If value found, compare the PO line item value to the amountCurrency value.
			// If nothing found, then no PO line items have been added and we do not need to continue as there is
			// nothing for us to validate against.
			poLineItemCurrencyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, new Object[]{termsOid});

			if (poLineItemCurrencyDoc != null)
			{
				poLineItemCurrencyCode = poLineItemCurrencyDoc.getAttribute("/ResultSetRecord(0)/CURRENCY");

				if ((poLineItemCurrencyCode != null) && (!poLineItemCurrencyCode.equals("")))
				{
					if (!poLineItemCurrencyCode.equals(amountCurrency))
					{
						terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PO_ITEM_CURR_NOT_SAME_AS_TRANS);
					}
				}
			}
		}
	}



	/**
	 * Performs validation for the transport documents section.
	 *
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 * @param hasMultipleShipments boolean
	 */
	public void validateTransportDocs(TermsBean terms, ShipmentTerms shipmentTerms, String shipmentNumDesc, boolean hasMultipleShipments)
	throws java.rmi.RemoteException, AmsException {

		String value;
		String alias;

		// If the transport doc checkbox is checked, there are several fields
		// that are required: document type, originals, consignment and
		// marked freight.
		value = shipmentTerms.getAttribute("trans_doc_included");
		if (!StringFunction.isBlank(value) &&
				value.equals(TradePortalConstants.INDICATOR_YES)) {

			String originals = shipmentTerms.getAttribute("trans_doc_originals");
			String docType = shipmentTerms.getAttribute("trans_doc_type");

			if (StringFunction.isBlank(docType)) {
				alias = shipmentTerms.getAlias("trans_doc_type");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}


			if (StringFunction.isBlank(originals)) {
				alias = shipmentTerms.getAlias("trans_doc_originals");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_type"))) {
				alias = shipmentTerms.getAlias("trans_doc_consign_type");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_marked_freight"))) {
				alias = shipmentTerms.getAlias("trans_doc_marked_freight");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			// For the doctype/originals combination: 1/3, 2/3, 3/3, and full set can
			// only match up with marine/ocean or multi-modal (and vice versa).
			if (originals != null && docType != null) {
				if (originals.equals(TradePortalConstants.ONE_UPON_3)
						|| originals.equals(TradePortalConstants.TWO_UPON_3)
						|| originals.equals(TradePortalConstants.THREE_UPON_3)
						|| originals.equals(TradePortalConstants.FULL_SET)) {
					// The doc type MUST be marine/ocean or multi-modal
					if (!(docType.equals(TradePortalConstants.MARINE_OCEAN)
							|| docType.equals(TradePortalConstants.MULTI_MODAL))) {
						alias = shipmentTerms.getAlias("trans_doc_type");
						String alias2 = shipmentTerms.getAlias("trans_doc_originals");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.TRANS_DOC_INVALID_COMBO,
								errorParameters, wrapParameters);
					}

				}
			}

			// For airway transport doc type, the consignment type must be
			// "consigned to party".  If it is "consigned to order of"
			// it is bad.
			if ((docType != null)
					&& (docType.equals(TradePortalConstants.AIR_TRANSPORT))) {

				String type = shipmentTerms.getAttribute("trans_doc_consign_type");
				if (type.equals(TradePortalConstants.CONSIGN_ORDER)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String alias2 = shipmentTerms.getAlias("trans_doc_type");
					String[] errorParameters = {alias, alias2, shipmentNumDesc};
					boolean[] wrapParameters = {true, true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.TRANS_DOC_INVALID_COMBO,
							errorParameters, wrapParameters);
				}
			}

		} else {
			// Checkbox is unchecked.  Blank out all the fields that are
			// associated with this checkbox.
			shipmentTerms.setAttribute("trans_doc_type", "");
			shipmentTerms.setAttribute("trans_doc_originals", "");
			shipmentTerms.setAttribute("trans_doc_copies", "");
			shipmentTerms.setAttribute("trans_doc_consign_type", "");
			shipmentTerms.setAttribute("trans_doc_consign_order_of_pty", "");
			shipmentTerms.setAttribute("trans_doc_consign_to_pty", "");
			shipmentTerms.setAttribute("trans_doc_marked_freight", "");
			shipmentTerms.setAttribute("trans_doc_text", "");

			removeTermsParty(shipmentTerms, "NotifyParty");
			removeTermsParty(shipmentTerms, "OtherConsigneeParty");

		}

		// If the consignment type is not blank (error caught above), perform
		// some validation: for "Consign the the order of", the party is
		// required.  For "Consign to", the party is required.  If the party
		// is Other, at least one party field is required.  SHP is not a valid
		// party type for "consign to".
		value = shipmentTerms.getAttribute("trans_doc_consign_type");
		if (!StringFunction.isBlank(value)) {
			if (value.equals(TradePortalConstants.CONSIGN_ORDER)) {
				// The "Consigned to the order of" option is selected
				shipmentTerms.setAttribute("trans_doc_consign_to_pty", "");
				String party = shipmentTerms.getAttribute("trans_doc_consign_order_of_pty");
				if (StringFunction.isBlank(party)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String[] errorParameters = {alias, shipmentNumDesc};
					boolean[] wrapParameters = {true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PARTY_MISSING,
							errorParameters, wrapParameters);
				} else if (party.equals(TradePortalConstants.CONSIGN_OTHER)) {
					checkOtherConsignee(shipmentTerms, shipmentNumDesc);
				}
				else
				{
					if (this.hasAddressInfo(shipmentTerms,"OtherConsigneeParty"))
					{
						value = terms.getResourceManager().getText("ApprovalToPayIssue.OtherConsignee",
								TradePortalConstants.TEXT_BUNDLE);
						alias = shipmentTerms.getAlias("trans_doc_consign_order_of_pty");
						String[] errorParameters = {value, alias, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CONSIGN_TO_PARTY_DATA,
								errorParameters, wrapParameters);
					}
				}
			} else {
				// The "Consigned to" option is selected
				shipmentTerms.setAttribute("trans_doc_consign_order_of_pty", "");
				String party = shipmentTerms.getAttribute("trans_doc_consign_to_pty");
				if (StringFunction.isBlank(party)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String[] errorParameters = {alias, shipmentNumDesc};
					boolean[] wrapParameters = {true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PARTY_MISSING,
							errorParameters, wrapParameters);
				} else if (party.equals(TradePortalConstants.CONSIGN_OTHER)) {
					// At least one field is required for OTHER.
					checkOtherConsignee(shipmentTerms, shipmentNumDesc);
				} else if (party.equals(TradePortalConstants.CONSIGN_SHIPPER)) {
					// SHP type not allowed for Consigned To.
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String alias2 = shipmentTerms.getAlias("trans_doc_consign_to_pty");
					String[] errorParameters = {alias, alias2, shipmentNumDesc};
					boolean[] wrapParameters = {true, true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.TRANS_DOC_INVALID_COMBO,
							errorParameters, wrapParameters);
				}
				else
				{
					if (this.hasAddressInfo(shipmentTerms,"OtherConsigneeParty"))
					{
						value = terms.getResourceManager().getText("ApprovalToPayIssue.OtherConsignee",
								TradePortalConstants.TEXT_BUNDLE);
						alias = shipmentTerms.getAlias("trans_doc_consign_to_pty");
						String[] errorParameters = {value, alias, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CONSIGN_TO_PARTY_DATA,
								errorParameters, wrapParameters);
					}
				}
			}
		}

		// If the addl doc indicator is checked and there is no text, then
		// reset the indicator to N.  Conversely if there is text but the
		// indicator is unchecked, do not save the text.
		value = shipmentTerms.getAttribute("trans_addl_doc_included");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			shipmentTerms.setAttribute("trans_addl_doc_text", "");
		} else {
			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_addl_doc_text"))) {
				shipmentTerms.setAttribute("trans_addl_doc_included",
						TradePortalConstants.INDICATOR_NO);
			}
			else
			{
				if (hasMultipleShipments)
				{
					shipmentTerms.setAttribute("trans_addl_doc_included", TradePortalConstants.INDICATOR_NO);
					shipmentTerms.setAttribute("trans_addl_doc_text", "");
				}
			}
		}
	}

	//Beginning of code used by the Middleware team
	//Beginning of getManagerTerms, coded by Indu Valavala
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values ==> {} ", termsValuesDoc);



		//Terms_Details
		/*Change the below getAttribute to get the values from termsValuesDoc after checking out how it looks like*/
		termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", 	       		terms.getAttribute("place_of_expiry"));
		termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",					terms.getAttribute("expiry_date"));
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	            terms.getAttribute("reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",          terms.getAttribute("amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                      terms.getAttribute("amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	    terms.getAttribute("amt_tolerance_pos"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",  	terms.getAttribute("amt_tolerance_neg"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",		            terms.getAttribute("purpose_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/PresentDocumentsWithinDays", terms.getAttribute("present_docs_within_days"));
		termsDoc.setAttribute("/Terms/TermsDetails/InsuranceRiskType", terms.getAttribute("insurance_risk_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/InsuranceEndorseValuePlus", terms.getAttribute("insurance_endorse_value_plus"));
		termsDoc.setAttribute("/Terms/TermsDetails/PartialShipmentAllowed", terms.getAttribute("partial_shipment_allowed"));
		//End of Terms_Details

		//Docs_Required

		int numberOfDocsReq    = 8;
		int drid               = 0;
		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));

		String [] indicator        = {"comm_invoice_indicator","packing_list_indicator", "ins_policy_indicator",
				"cert_origin_indicator", "other_req_doc_1_indicator","other_req_doc_2_indicator",
				"other_req_doc_3_indicator","other_req_doc_4_indicator"};
		String [] DocumentName     = {"Commercial Invoice","Packing List","Insurance Policy/Certificate",
				"Certificate Of Origin","other_req_doc_1_name","other_req_doc_2_name",
				"other_req_doc_3_name", "other_req_doc_4_name"};
		String [] NumberOfOrginals = {"comm_invoice_originals","packing_list_originals","ins_policy_originals",
				"cert_origin_originals" ,"other_req_doc_1_originals","other_req_doc_2_originals",
				"other_req_doc_3_originals","other_req_doc_4_originals"};
		String [] NumberOfCopies   = {"comm_invoice_copies","packing_list_copies","ins_policy_copies",
				"cert_origin_copies" ,"other_req_doc_1_copies","other_req_doc_2_copies",
				"other_req_doc_3_copies","other_req_doc_4_copies"};
		String [] DocumentText     = {"comm_invoice_text","packing_list_text","ins_policy_text",
				"cert_origin_text" ,"other_req_doc_1_text","other_req_doc_2_text",
				"other_req_doc_3_text","other_req_doc_4_text"};
		termsDoc.removeComponent("/Terms/DocumentsRequired/FullDocumentRequired");

		for (int docIndex=0; docIndex<numberOfDocsReq; docIndex++)
		{
			if(terms.getAttribute(indicator[docIndex]).equals("Y"))
			{
				if(docIndex < 4)
					termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			DocumentName[docIndex]);
				else
					termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			terms.getAttribute(DocumentName[docIndex]));

				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			terms.getAttribute(DocumentText[docIndex]));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		terms.getAttribute(NumberOfCopies[docIndex]));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		terms.getAttribute(NumberOfOrginals[docIndex]));

				drid ++;

			}
		}

		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- Start
		List addReqDocSortedList = new ArrayList();
		ComponentList additionalReqDocList = (ComponentList) terms.getComponentHandle("AdditionalReqDocList");
		// Sort the list of additional documents so they are placed in the order they were created
		addReqDocSortedList = sortAddlReqDocs(additionalReqDocList);
		int addRedDocCount = addReqDocSortedList.size();
		for (int addReqDocIndex = 0 ; addReqDocIndex < addRedDocCount ; addReqDocIndex++){
			AdditionalReqDoc additionalReqDoc = (AdditionalReqDoc)additionalReqDocList.getComponentObject(Long.parseLong(addReqDocSortedList.get(addReqDocIndex).toString()));
			String value = additionalReqDoc.getAttribute("addl_req_doc_ind");
			if("Y".equalsIgnoreCase(value))
			{
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName", additionalReqDoc.getAttribute("addl_req_doc_name"));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText", additionalReqDoc.getAttribute("addl_req_doc_text"));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",	additionalReqDoc.getAttribute("addl_req_doc_copies"));//CQ T36000011047. IValavala fixed originals/copies getting switched.
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals", additionalReqDoc.getAttribute("addl_req_doc_originals"));
	
				drid ++;
			}
		}
		//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- End
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
		
		//IValavala T36000016518. Uncomment the below based on recent design updates for 737. continue sending addlDocText
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		//VS - IR#T36000021272 - 09/30/2013 - <BEGIN> - TPL message that had Additional Doc Information box checked, which sent over blank FullDocumentRequired tags caused Timekeeper to crash.
		//Modify the below "if" condition and rewrite it accordingly.
		if(terms.getAttribute("addl_doc_indicator").equals("Y") && StringFunction.isNotBlank(terms.getAttribute("addl_doc_text")) )
		//VS - IR#T36000021272 - 09/30/2013 - <END>
		{
			String addlDocText = terms.getAttribute("addl_doc_text");
			if(addlDocText.length()>30)
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			addlDocText.substring(0,29));
			else
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			addlDocText);

			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			addlDocText);
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		"");
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		"");
			drid++;
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
		
		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));


		// Transport Documents/Shipment Section
		Vector shipmentTermsSorted = new Vector();
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		// Sort the list of shipment terms so they are placed in the order they were created
		shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);
		int shipmentCount = shipmentTermsSorted.size();

		termsDoc.removeComponent("/Terms/TransportDocuments/FullTransportDocuments");
		termsDoc.removeComponent("/Terms/ShipmentTerms/FullShipmentTerms");

		for (int shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
		{
			String shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
			ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
			//Rpasupulati IR T36000045226 Start
			String nextLine = "";			
			if(StringFunction.isNotBlank(shipmentTerms.getAttribute("goods_description"))&&StringFunction.isNotBlank(shipmentTerms.getAttribute("po_line_items"))){
				nextLine = "\n";
			}
			//Rpasupulati IR T36000045226 End
			//TransportDocuments
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentOriginals", shipmentTerms.getAttribute("trans_doc_originals"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentCopies", shipmentTerms.getAttribute("trans_doc_copies"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentType", shipmentTerms.getAttribute("trans_doc_type"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignType", shipmentTerms.getAttribute("trans_doc_consign_type"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignOrderOfParty", shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignOrderToParty", shipmentTerms.getAttribute("trans_doc_consign_to_pty"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentMarkedFreight", shipmentTerms.getAttribute("trans_doc_marked_freight"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentText", shipmentTerms.getAttribute("trans_doc_text"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportAdditionalDocumentText", shipmentTerms.getAttribute("trans_addl_doc_text"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentShipmentDescription", shipmentTerms.getAttribute("description"));
			//End of TransportDocuments

			//ShipmentTerms
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/TransshipmentAllowed", shipmentTerms.getAttribute("transshipment_allowed"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/LatestShipmentDate", shipmentTerms.getAttribute("latest_shipment_date"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/Incoterm", shipmentTerms.getAttribute("incoterm"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/IncotermLocation", shipmentTerms.getAttribute("incoterm_location"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipmentFrom", shipmentTerms.getAttribute("shipment_from"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipmentTo", shipmentTerms.getAttribute("shipment_to"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/GoodsDescription", shipmentTerms.getAttribute("goods_description") + nextLine + shipmentTerms.getAttribute("po_line_items"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/POLineItems", shipmentTerms.getAttribute("po_line_items"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/AirWaybillNumber", shipmentTerms.getAttribute("air_waybill_num"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/VesselName", shipmentTerms.getAttribute("vessel_name_voyage_num"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/TransportTermsText", "");
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ContainerNumber", shipmentTerms.getAttribute("container_number"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/BillOfLadingNumber", shipmentTerms.getAttribute("bill_of_lading_num"));

			//iv Adding ShipmentFrom and ShipmentTo

			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipFromLoading", shipmentTerms.getAttribute("shipment_from_loading"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipToDischarge", shipmentTerms.getAttribute("shipment_to_discharge"));


			//End of ShipmentTerms

			// ShipmentTerms Parties
			String c_NotifyParty             = shipmentTerms.getAttribute("c_NotifyParty");
			String c_OtherConsigneeParty      = shipmentTerms.getAttribute("c_OtherConsigneeParty");
			String [] shipmentTermsPartyOID  = {c_NotifyParty ,c_OtherConsigneeParty};
			String [] shipmentTermsPartyColl = {"NotifyParty","OtherConsigneeParty"};
			String [] partyType = {TradePortalConstants.SHIPMENT_NOTIFY_PARTY,
					TradePortalConstants.SHIPMENT_CONSIGNEE_PARTY};

			for (int i = 0; i<partyType.length;i++)
			{
				if(!StringFunction.isBlank(shipmentTermsPartyOID[i]))
				{
					TermsParty termsParty = (TermsParty)(shipmentTerms.getComponentHandle(shipmentTermsPartyColl[i]));
					termsDoc = termsParty.packageShipmentTermsPartyAttributes(shipmentIndex, partyType[i], termsDoc);
				}
				else
				{
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"BusinessName", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine1", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine2", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine3", "");
				}
			}
		}
		termsDoc.setAttribute("/Terms/ShipmentTerms/TotalNumberOfEntries",String.valueOf(shipmentCount));
		termsDoc.setAttribute("/Terms/TransportDocuments/TotalNumberOfEntries",String.valueOf(shipmentCount));
		// End Transport Documents/Shipment Section

		//???       //Instructions
		//Instructions_to_Bank
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",       termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		//End of Instructions_to_Bank

		//Settlement_Instructions
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",				        termsValuesDoc.getAttribute("/Terms/branch_code"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementOurAccountNumber",       termsValuesDoc.getAttribute("/Terms/settlement_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountNumber",    termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountCurrency", termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_curr"));
		//End of Settlement_Instructions
		//???       //End of Instructions


		//Commission_and_Charges
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",      termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",  termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",             termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawing",                         termsValuesDoc.getAttribute("/Terms/finance_drawing"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingNumberOfDays",             termsValuesDoc.getAttribute("/Terms/finance_drawing_num_days"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingType",                     termsValuesDoc.getAttribute("/Terms/finance_drawing_type"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CoveredByFECNumber",                     termsValuesDoc.getAttribute("/Terms/covered_by_fec_number"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECAmount",                              termsValuesDoc.getAttribute("/Terms/fec_amount"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECRate",                                termsValuesDoc.getAttribute("/Terms/fec_rate"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECMaturityDate",                        termsValuesDoc.getAttribute("/Terms/fec_maturity_date"));
		//End of Commission_and_Charges

		//Bank_Charges
		termsDoc.setAttribute("/Terms/BankCharges/BankChargesType",   termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
		//End of Bank_Charges


		//Other_Conditions
		termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",     termsValuesDoc.getAttribute("/Terms/additional_conditions"));
		termsDoc.setAttribute("/Terms/OtherConditions/InvoiceDueDate",           termsValuesDoc.getAttribute("/Terms/invoice_due_date"));
		termsDoc.setAttribute("/Terms/OtherConditions/InvoiceOnlyIndicator",     termsValuesDoc.getAttribute("/Terms/invoice_only_ind"));
		//termsDoc.setAttribute("/Terms/OtherConditions/InvoiceDetails",     termsValuesDoc.getAttribute("/Terms/invoice_details")); //SHR CR708 Rel8.1.1
		//End of Other_Conditions

		termsDoc.setAttribute("/Terms/AdditionalDocumentText",     termsValuesDoc.getAttribute("/Terms/addl_doc_text"));

		//Terms Party

		DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		String testTP = TermsPartyType.ATP_BUYER;// buyercheck


		int id = 0;

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
		{
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
			if(!(termsPartyOID == null || termsPartyOID.equals("")))
			{
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(testTP))
				{
					LOG.debug("party_Type is APP, hence I need to check for thirdParty");
					termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
					if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
						id++;
				}
				termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
				//If the termsValuesDoc contains termsParty also need to modify the line below
				termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
				id++;
			}
		}

		LOG.debug("This is TermsPartyDoc {}" ,  termsPartyDoc);

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


		return termsDoc;
	}

	//End of GetManagerTerms Method

	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	{
		// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
				5,
				8,
				"TermsBeanAlias.fec_rate",
				terms.getErrorManager(),
				terms.getResourceManager());


		// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("insurance_endorse_value_plus"),
				3,
				1,
				"TermsBeanAlias.insurance_endorse_value_plus",
				terms.getErrorManager(),
				terms.getResourceManager());
	}


	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");
			validateAmount(terms, "fec_amount", "amount_currency_code");

			validateTextLengths(terms);

			validateDecimalFields(terms);

			// Validate the shipment terms text fields
			String        shipmentNumber;
			StringBuffer  shipmentNumDesc;
			int           shipmentCount;
			int           shipmentIndex;
			String        shipmentOid;
			Vector        shipmentTermsSorted = new Vector();
			ComponentList shipmentTermsList;
			ShipmentTerms shipmentTerms;

			// Get a handle to the term's shipment terms components
			shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

			// Sort the list of shipment terms so they are placed in the order they were created
			shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);

			shipmentCount = shipmentTermsSorted.size();
			// Validate each set of shipment terms
			for (shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
			{
				shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
				shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
				shipmentNumDesc = new StringBuffer();

				// If there are multiple shipments, validate any required attributes
				if (shipmentCount > 1)
				{
					shipmentNumber = String.valueOf(shipmentIndex + 1);
					shipmentNumDesc.append(terms.getResourceManager().getText("ApprovalToPayIssue.Shipment",
							TradePortalConstants.TEXT_BUNDLE));
					shipmentNumDesc.append(" ");
					shipmentNumDesc.append(shipmentNumber + " -");
				}
				else
				{
					shipmentNumDesc.append(terms.getResourceManager().getText("ApprovalToPayIssue.Shipment",
							TradePortalConstants.TEXT_BUNDLE));
					shipmentNumDesc.append(" -");
				}

				// Perform subclass-specific validation of text fields for SWIFT characters
				validateForSwiftCharacters(shipmentTerms);

				validateTextLengths(terms, shipmentTerms, shipmentNumDesc.toString());
			}
		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in ATP__ISSTermsMgr.preSave()", e);
		}

	}

	/**
	 * This method retrieves the term's collection of shipment terms and
	 * sorts them by creation date
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public Vector sortShipmentTerms(ComponentList shipmentTermsList) throws AmsException, RemoteException
	{
		boolean          dateSorted;
		Date             formattedDate;
		Date             compareDate;
		String           creationDate;
		String           shipmentOid;
		ShipmentTerms    shipmentTerms;
		Vector           shipTermsSorted = new Vector();
		Vector           shipmentDates   = new Vector();

		try {

			SimpleDateFormat dateFormatter   = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

			for (int index=0; index< shipmentTermsList.getObjectCount(); index++)
			{
				shipmentTermsList.scrollToObjectByIndex(index);
				shipmentOid   = shipmentTermsList.getListAttribute("shipment_oid");
				creationDate  = shipmentTermsList.getListAttribute("creation_date");
				formattedDate = dateFormatter.parse(creationDate);

				dateSorted = false;

				for (int i=0; i< shipmentDates.size(); i++)
				{
					compareDate = (Date) shipmentDates.elementAt(i);
					if (formattedDate.before(compareDate))
					{
						shipmentDates.insertElementAt(formattedDate, i);
						shipTermsSorted.insertElementAt(shipmentOid, i);
						dateSorted = true;
						break;
					}
				}

				if (!dateSorted)
				{
					shipmentDates.addElement(formattedDate);
					shipTermsSorted.addElement(shipmentOid);
				}
			}

		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in ATP__ISSTermsMgr.sortShipmentTerms()", e);

		} catch (ParseException e) {
			throw new AmsException("Date parse error caught in ATP__ISSTermsMgr.sortShipmentTerms()");
		}

		return shipTermsSorted;
	}

	public void validateAmountCurrencyAndInvoiceCurrency(TermsBean terms)
			throws RemoteException, AmsException {
		// Get the currency code of the transaction. If not blank, proceed with
		// validating that it is
		// the same as the PO line items currency code, otherwise fall through
		// and let the required
		// validation process flag it.
		String sqlQuery = "select count(*) as count,sum(i.amount) as AMOUNT,max(i.currency) as CURRENCY "
				+ "from invoices_summary_data i,terms t, transaction tr "
				+ "where t.terms_oid = tr.c_cust_enter_terms_oid "
				+ "and i.a_transaction_oid = tr.transaction_oid "
				+ "and t.terms_oid = ? ";
		double totalAmt = 0.0d;
		String currency = "";
		boolean throwError = false;
		double invAmt = 0.0d;
		DocumentHandler resultXml = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, new Object[]{terms.getAttribute("terms_oid")});
		
		if (resultXml != null) {
			DocumentHandler record = resultXml.getFragment("/ResultSetRecord");
			// SHR IR ROUM102444803 - only if invoice is attached then do the
			// validation and throw error.
			if (record != null) {
				if(StringFunction.isNotBlank(record.getAttribute("/COUNT")) && Integer.parseInt(record.getAttribute("/COUNT")) > 0){
				String amt = record.getAttribute("/AMOUNT");
				if (StringFunction.isNotBlank(amt))
					totalAmt = Double.parseDouble(amt);
				currency = record.getAttribute("/CURRENCY");
				if (StringFunction.isNotBlank(terms.getAttribute("amount")))
					invAmt = Double.parseDouble(terms.getAttribute("amount"));

				if (StringFunction.isNotBlank(currency)) {
					if (!currency.equals(terms
							.getAttribute("amount_currency_code")))
						throwError = true;
				}
				if (invAmt != totalAmt)
					throwError = true;

				if (throwError)
					terms.getErrorManager()
							.issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.ATP_INVOICE_INVALID_AMT_CURRENCY);

			}
			}
		}
	}
	//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- Start
	public List sortAddlReqDocs(ComponentList additionalReqDocList) throws AmsException, RemoteException
	{
		String           addlReqDocOid;
		List           addlReqDocSortedList = new ArrayList();
		int addRedDocCount = additionalReqDocList.getObjectCount();
		for (int i =0 ; i < addRedDocCount ; i++)
		{
			additionalReqDocList.scrollToObjectByIndex(i);
			addlReqDocOid   = additionalReqDocList.getListAttribute("addl_req_doc_oid");
			addlReqDocSortedList.add(addlReqDocOid);
		}
		Collections.sort(addlReqDocSortedList);
		return addlReqDocSortedList;
	}
	//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- End
}

