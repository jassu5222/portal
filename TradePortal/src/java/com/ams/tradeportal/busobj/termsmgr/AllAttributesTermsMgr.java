package com.ams.tradeportal.busobj.termsmgr;

import java.lang.reflect.Method;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;

public class AllAttributesTermsMgr extends TermsMgr {
	private static final Logger LOG = LoggerFactory.getLogger(AllAttributesTermsMgr.class);

	/**
	 * Constructor that is used by business objects to create their manager. This method also register the dynamic attributes of the
	 * bean.
	 * 
	 * @param mgr
	 *            AttributeManager attribute manager of the bean being managed. Used to register attributes
	 */
	public AllAttributesTermsMgr(AttributeManager mgr) throws AmsException {
		super(mgr);
	}

	/**
	 * Constructor that is used by web beans to create their manager. This method also register the dynamic attributes of the bean.
	 * 
	 * @param mgr
	 *            TermsWebBean - the bean being managed. Used to register attributes.
	 */
	public AllAttributesTermsMgr(TermsWebBean terms) throws AmsException {
		super(terms);
	}

	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for bank released terms with this
	 * transaction type
	 *
	 * @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister() {
		List<Attribute> attrList = super.getAttributesToRegister();
		TermsBean.Attributes temp = new TermsBean.Attributes();

		Method[] methods = temp.getClass().getDeclaredMethods();

		Object[] empty = {};

		for (int i = 0; i < methods.length; i++) {
			Method m = methods[i];

			if (m.getName().startsWith("create")) {
				try {
					Attribute attr = (Attribute) m.invoke(temp, empty);
					attrList.add(attr);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return attrList;
	}

}
