package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for bank released terms 
 * of a transaction of type Change
 *
 *     Copyright  � 2004                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CHGBankRelTermsMgr extends AMDBankRelTermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(CHGBankRelTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their 
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.  
	 *  
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public CHGBankRelTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}
	
	/**
	 *  Constructor that is used by web beans to create their 
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.  
	 *  
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public CHGBankRelTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}
	
	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{ 
		// Call method on parent to register Amend attributes 
		List<Attribute> attrList = super.getAttributesToRegister();
		
		// Register additional attributes for Usance instruments
		attrList.add( TermsBean.Attributes.create_usance_maturity_date() );
		attrList.add( TermsBean.Attributes.create_value_date() );
		attrList.add( TermsBean.Attributes.create_discount_rate() );
		attrList.add( TermsBean.Attributes.create_total_rate() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
		attrList.add( TermsBean.Attributes.create_loan_rate());
		attrList.add( TermsBean.Attributes.create_confirmation_indicator() );
		
		return attrList;
	}
	
	/**
	 * This method is supposed to be used to set attributes of Terms business object.
	 *
	 * @param inputDoc DocumentHandler - universal XML message from MQSeries
	 * @param terms TermsBean
	 * @return boolean
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc) throws java.rmi.RemoteException, AmsException
	{
		// Call method on parent to set Amend attribute values
		super.setManagerTerms(terms, inputDoc);
		
		// Set attribute values for Usance instruments
		terms.setAttribute("usance_maturity_date",          inputDoc.getAttribute("/Terms/TermsDetails/UsanceMaturityDate"));
		terms.setAttribute("value_date",                    inputDoc.getAttribute("/Terms/TermsDetails/ValueDate"));
		terms.setAttribute("discount_rate",                 inputDoc.getAttribute("/Terms/TermsDetails/DiscountRate"));
		terms.setAttribute("total_rate",                    inputDoc.getAttribute("/Terms/TermsDetails/TotalRate"));
		terms.setAttribute("loan_rate",    				    inputDoc.getAttribute("/Terms/LoanTerms/LoanRate"));
		
		String myConfirmationType = inputDoc.getAttribute("/Terms/OtherConditions/ConfirmationType");
		if((myConfirmationType != null) 
				&& (myConfirmationType.equals(TradePortalConstants.ADDING_CONFIRMATION) 
						|| myConfirmationType.equals(TradePortalConstants.SILENT_CONFIRMATION)))
		{
			terms.setAttribute("confirmation_indicator", TradePortalConstants.INDICATOR_YES);
		}
		else if((myConfirmationType != null) 
				&& (myConfirmationType.equals(TradePortalConstants.NOT_ADDING_CONFIRMATION) 
						|| myConfirmationType.equals(TradePortalConstants.BANK_MAY_ADD)))
		{
			terms.setAttribute("confirmation_indicator", TradePortalConstants.INDICATOR_NO);
		}
		
		return true; 	 	  
	}
	
}