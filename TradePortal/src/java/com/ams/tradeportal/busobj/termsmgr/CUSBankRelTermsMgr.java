package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for bank released terms 
 * of a transaction of type Create Usance
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CUSBankRelTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(CUSBankRelTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public CUSBankRelTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their 
     *  manager.  This method also register the dynamic attributes of
     *  the bean.  
     *  
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public CUSBankRelTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.  
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for bank released terms with this transaction type
     *
     *  @return Vector - list of attributes
     */
     public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_discount_rate() );
        attrList.add( TermsBean.Attributes.create_expiry_date() );
        attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_total_rate() );
        attrList.add( TermsBean.Attributes.create_usance_maturity_date() );
        attrList.add( TermsBean.Attributes.create_value_date() );

        return attrList;
     }
























     
   /**
    * This method is supposed to be used to set attributes of Terms business object.
    *
    * @param inputDoc DocumentHandler - universal XML message from MQSeries
    * @param terms TermsBean
    * @return boolean
    */
    public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc) throws java.rmi.RemoteException, AmsException{

		    terms.setAttribute("amount",			    inputDoc.getAttribute("/Terms/TermsDetails/Amount"));
		    terms.setAttribute("amount_currency_code", 	inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
	 	    terms.setAttribute("usance_maturity_date",  inputDoc.getAttribute("/Terms/TermsDetails/UsanceMaturityDate"));
	 	    terms.setAttribute("value_date",        	inputDoc.getAttribute("/Terms/TermsDetails/ValueDate"));
                terms.setAttribute("discount_rate",         inputDoc.getAttribute("/Terms/TermsDetails/DiscountRate"));
                //terms.setAttribute("funding_rate",          inputDoc.getAttribute("/Terms/TermsDetails/FundingRate"));
    		    //Krishna IR - RSUH111235799 11/18/2007 Begin
                // Set related_instrument_id.  Usually the related instrument should be found in TP and 
    		    // instrument.related_instrument_oid is set.  But if, for example, a new Payer Finance 
                //instrument is spawned from OAP,the Payer Finance may come in before the Parent-OAP .  
                //We need to store the related_instrument_id here and later update instrument.related_instrument_oid 
                //when the OAP comes in.
                terms.setAttribute("related_instrument_id",     inputDoc.getAttribute("/Terms/TermsDetails/RelatedInstrumentID"));
                //Krishna IR - RSUH111235799 11/18/2007 End
	 	    terms.setAttribute("total_rate",          inputDoc.getAttribute("/Terms/TermsDetails/TotalRate"));	 	    
	        return true; 	 	  
    }
    
 }