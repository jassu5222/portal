package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Fund Transfer Between Accounts.
 *
 * Note: for Request to Advise, the 3 terms parties represent these types:
 *        first     beneficiary
 *        second    applicant
 *        third     advising bank
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DDI__ISSTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(DDI__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public DDI__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public DDI__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an Issue Import DLC
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_credit_account_oid() );
        attrList.add( TermsBean.Attributes.create_debit_account_oid() );
        attrList.add( TermsBean.Attributes.create_work_item_priority() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_purpose_type() );
		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		super.validate(terms);
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");
		terms.registerRequiredAttribute("credit_account_oid");

		String value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
	}




	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	{


       //   	 Validate that the rate field will fit in the database


	}

	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values: {}" ,termsValuesDoc.toString());

	    termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/transfer_description"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",                              termsValuesDoc.getAttribute("/Terms/purpose_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/WorkItemPriority",                        termsValuesDoc.getAttribute("/Terms/work_item_priority"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/ExrtVariationAdjust", 		              termsValuesDoc.getAttribute("/Terms/ex_rt_variation_adjust"));

		// IValavala CR434 get the AppDebitAccPrincipal accountnum from account
		String accountUoid = termsValuesDoc.getAttribute("/Terms/debit_account_oid");
	    String accountNumSQL = "SELECT ACCOUNT_NUMBER FROM ACCOUNT WHERE ACCOUNT_OID = ?";
	    String accountNum = "";
	    DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
	    if(resultXML != null)
	     {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
	 	  }
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              accountNum);

		// IValavala CR434 Adding CreditAccPrincipal. accountnum from account
		accountUoid = termsValuesDoc.getAttribute("/Terms/credit_account_oid");
		accountNum = "";
		accountNumSQL = "SELECT ACCOUNT_NUMBER FROM ACCOUNT WHERE ACCOUNT_OID = ?" ;
		resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid});
		if(resultXML != null)
		 {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		  }
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditAccPrincipal", 		              accountNum);

		// IValavala CR434 Adding DailyLimitExceeded.,PaymentDate Values will be added in the TPLPackager as it comes from Transaction
		termsDoc.setAttribute("/Terms/LoansAndFunds/DailyLimitExceeded", 		              "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/PaymentDate", "");

		//Terms Party


	DocumentHandler termsPartyDoc = new DocumentHandler();


		int id = 0;

	   termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

       for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
	   	 {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
	        if(!(termsPartyOID == null || termsPartyOID.equals("")))
	   	      {
	   			 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
	   			 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
	   			 termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
	   		     id++;
	   		   }
	      }

		 LOG.debug("This is TermsPartyDoc: {} " , termsPartyDoc.toString());

        termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
        termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}


	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");

			validateDecimalFields(terms);

		} catch (RemoteException e) {
			LOG.info("Remote Exception caught in FTBA__ISSTermsMgr.preSave()");
		}

	}

}