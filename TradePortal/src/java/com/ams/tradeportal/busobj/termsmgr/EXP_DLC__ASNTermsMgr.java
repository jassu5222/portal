package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

import com.amsinc.ecsg.util.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export Collection Assignment.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class EXP_DLC__ASNTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_DLC__ASNTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_DLC__ASNTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_DLC__ASNTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
                            public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_assignees_account_number() );
        attrList.add( TermsBean.Attributes.create_branch_code() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
        attrList.add( TermsBean.Attributes.create_part_payments_percent() );
        attrList.add( TermsBean.Attributes.create_part_payments_type() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_transferrable() );

        return attrList;
     }



    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       super.validate(terms);

       terms.registerRequiredAttribute("amount");
       terms.registerRequiredAttribute("part_payments_type");

       validateGeneral(terms);
       validateInstructions(terms);
       validateTextLengths(terms);
    }

    /**
     * Performs validation for the general section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       String   value = null;

       // Validate that the required Assignee and Assignee Bank terms party fields have data entered in them
       this.editAssigneeValue(terms,     "FirstTermsParty");
       this.editAssigneeBankValue(terms, "SecondTermsParty");

       // Make sure that if the user selected the Partial Payments percentage radio button, a value was
       // entered in the percentage field
       value = terms.getAttribute("part_payments_type");

       if (value.equals(TradePortalConstants.PARTIAL_PAYMENT_TYPE_PERCENTAGE))
       {
          value = terms.getAttribute("part_payments_percent");

          if (StringFunction.isBlank(value))
          {
             terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.DRAWINGS_PERCENTAGE_MISSING);
          }
       }
    }

    /**
     * Performs validation for the Bank Instructions section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       String   value = null;
       String   alias = null;

       // If the c&c foreign account number is given, the c&c foreign currency
       // must also be given; and vice-versa.
       value = terms.getAttribute("coms_chrgs_foreign_acct_num");

       if (!StringFunction.isBlank(value))
       {
          value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

          if (StringFunction.isBlank(value))
          {
             alias = terms.getAlias("coms_chrgs_foreign_acct_curr");

             terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
          }
       }
       else
       {
          value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

          if (!StringFunction.isBlank(value))
          {
             alias = terms.getAlias("coms_chrgs_foreign_acct_num");

             terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
          }
       }
    }

   /**
    *  Performs validation of text fields for length.
    *
    *  @param terms TermsBean - the bean being validated
    */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       checkTextLength(terms, "additional_conditions",     1000);
       checkTextLength(terms, "special_bank_instructions", 1000);
       checkTextLength(terms, "coms_chrgs_other_text",     1000);
    }

   /**
    * Performs presave processing (regardless of whether validation is done)
    *
    * @param terms com.ams.tradeportal.busobj.termsBean
    * @exception com.amsinc.ecsg.frame.AmsException The exception description.
    */
    public void preSave(TermsBean terms) throws AmsException
    {
       super.preSave(terms);

       try
       {
          validateTextLengths(terms);

          validateAmount(terms, "amount", "amount_currency_code");
       }
       catch (RemoteException e)
       {
          LOG.info("Remote Exception caught in EXP_DLC__ASNTermsMgr.preSave()");
       }
    }


   //Beginning of code used by the Middleware team
    /**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

   {

   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
             LOG.debug("This is the termValuesDoc containing all the terms values: {}", termsValuesDoc.toString());


             //Terms_Details -- Added Branch code for IR#CYUE072951363 10/04/04
              termsDoc.setAttribute("/Terms/TermsDetails/BranchCode",	 			  termsValuesDoc.getAttribute("/Terms/branch_code"));
		      termsDoc.setAttribute("/Terms/TermsDetails/AssigneeAccountNumber",	  termsValuesDoc.getAttribute("/Terms/assignees_account_number"));
		   	  termsDoc.setAttribute("/Terms/TermsDetails/Amount",                     termsValuesDoc.getAttribute("/Terms/amount"));
		      termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",         termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		      termsDoc.setAttribute("/Terms/TermsDetails/Transferrable",		      termsValuesDoc.getAttribute("/Terms/transferrable"));
		   	 //End of Terms_Details


		   	//Instructions
		   	 termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		   	//CommissionAndCharges
		     termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",			termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		   	 termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",		termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
		   	 termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",		termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
		   	 termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",	                termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));
		     //End of CommissionCharges
		   	 //End of Instructions

		   	 //Payment_Terms
		   	 termsDoc.setAttribute("/Terms/PaymentTerms/PartPaymentType",	            termsValuesDoc.getAttribute("/Terms/part_payments_type"));
		   	 termsDoc.setAttribute("/Terms/PaymentTerms/PartPaymentPercent",            termsValuesDoc.getAttribute("/Terms/part_payments_percent"));
		   	 //End of Payment_Terms

		   	 //OtherConditions
		   	 termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",			termsValuesDoc.getAttribute("/Terms/additional_conditions"));
		   	 //End of Other_Conditions

	           //Terms Party

               DocumentHandler termsPartyDoc = new DocumentHandler();

                //Additional Terms Party
                String testTP = TermsPartyType.APPLICANT;


             int id = 0;

            termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

            for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
   			 {
				String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
   		      {
   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
   				 {
   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
   					  id++;
   				 }
   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
   				 //If the termsValuesDoc contains termsParty also need to modify the line below
   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
   			     id++;
   			   }
		      }

		 LOG.debug("This is TermsPartyDoc: {} " , termsPartyDoc.toString());

         termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
         termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


   		return termsDoc;
   }

     //End of Code by Indu Valavala
 }