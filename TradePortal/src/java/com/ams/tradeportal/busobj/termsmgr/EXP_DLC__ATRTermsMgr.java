package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;
  
/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Export DLC Amendment
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EXP_DLC__ATRTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_DLC__ATRTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_DLC__ATRTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_DLC__ATRTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
     public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
        attrList.add( TermsBean.Attributes.create_expiry_date() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_transferrable() );

        return attrList;
     }



    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
     public void validate(TermsBean terms) throws RemoteException, AmsException
     {
        super.validate(terms);
 
        String latestShipDate = "";
        ShipmentTerms shipmentTerms = terms.getFirstShipment();
        if(shipmentTerms != null)
           latestShipDate = shipmentTerms.getAttribute("latest_shipment_date");

        // If all of these fields are blank, issue an error
        if (StringFunction.isBlank(terms.getAttribute("amount")) &&
            StringFunction.isBlank(terms.getAttribute("amt_tolerance_neg")) &&
            StringFunction.isBlank(terms.getAttribute("amt_tolerance_pos")) &&
            StringFunction.isBlank(terms.getAttribute("expiry_date")) &&
            StringFunction.isBlank(latestShipDate) &&
            StringFunction.isBlank(terms.getAttribute("special_bank_instructions")) &&
            StringFunction.isBlank(terms.getAttribute("additional_conditions")) )
        {
           terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MUST_AMEND);
        }
     }

   /**
    *  Performs validation of text fields for length.
    *
    *  @param terms TermsBean - the bean being validated
    */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       checkTextLength(terms, "additional_conditions",     1000);
       checkTextLength(terms, "special_bank_instructions", 1000);
    }

    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.  
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
        final String[] attributesToExclude = {"special_bank_instructions"};

        InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

         ShipmentTerms shipmentTerms = terms.getFirstShipment();
         if(shipmentTerms != null)
          {
            InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());        
          } 
     }

   /**
    * Performs presave processing (regardless of whether validation is done)
    *
    * @param terms com.ams.tradeportal.busobj.termsBean
    * @exception com.amsinc.ecsg.frame.AmsException The exception description.
    */
    public void preSave(TermsBean terms) throws AmsException
    {
       super.preSave(terms);

       try
       {
          validateTextLengths(terms);

          validateAmount(terms, "amount", "amount_currency_code");
       }
       catch (RemoteException e)
       {
          LOG.info("Remote Exception caught in EXP_DLC__ATRTermsMgr.preSave()");
       }
    }

   //Beginning of code used by the Middleware team
     /**
 	 *  gets the values of the attributes of the managed object. This method is called
 	 *  from the getTermsAttributesDoc() hook in the managed object.
 	 *
 	 *  return com.amsinc.ecsg.util.DocumentHandler
 	 *  @param terms TermsBean - the bean being validated
 	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
 	 */
    public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

    {

    	      DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());

               //Terms_Details
	            termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",				    termsValuesDoc.getAttribute("/Terms/expiry_date"));
	            termsDoc.setAttribute("/Terms/TermsDetails/Amount",                     termsValuesDoc.getAttribute("/Terms/amount"));
	            termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
	            termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",	termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
	            termsDoc.setAttribute("/Terms/TermsDetails/RelatedInstrumentID",	    termsValuesDoc.getAttribute("/Terms/related_instrument_oid"));
	            termsDoc.setAttribute("/Terms/TermsDetails/Transferrable",		        termsValuesDoc.getAttribute("/Terms/transferrable"));
	            // Shipment Terms
                populateFirstShipmentTermsXml(terms, termsDoc);

	            //OtherConditions
	            termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",		termsValuesDoc.getAttribute("/Terms/additional_conditions"));
	            //End of Other_Conditions

	           //Instructions
	            termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
	            //End of Instructions

    		return termsDoc;
    }

    //End of Code by Indu Valavala

 //End of class
 }