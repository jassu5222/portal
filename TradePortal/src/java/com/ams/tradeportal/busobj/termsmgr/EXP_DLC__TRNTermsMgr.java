package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;


/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Transfer Export DLC.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class EXP_DLC__TRNTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(EXP_DLC__TRNTermsMgr.class);
    /**
     *  Constructor that is used by business objects to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
     */
    public EXP_DLC__TRNTermsMgr(AttributeManager mgr) throws AmsException
     {
	  super(mgr);
     }

    /**
     *  Constructor that is used by web beans to create their
     *  manager.  This method also register the dynamic attributes of
     *  the bean.
     *
     *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
     */
    public EXP_DLC__TRNTermsMgr(TermsWebBean terms) throws AmsException
     {
	  super(terms);
     }

    /**
     *  Returns a list of attributes.  These describe each of the attributes
     *  that will be registered for the bean being managed.
     *
     *  This method controls which attributes of TermsBean or TermsWebBean are registered
     *  for an this instrument and transaction type
     *
     *  @return Vector - list of attributes
     */
                      public List<Attribute> getAttributesToRegister()
     {
        List<Attribute> attrList = super.getAttributesToRegister();
        attrList.add( TermsBean.Attributes.create_additional_conditions() );
        attrList.add( TermsBean.Attributes.create_amount() );
        attrList.add( TermsBean.Attributes.create_amount_currency_code() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
        attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
        attrList.add( TermsBean.Attributes.create_branch_code() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
        attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
        attrList.add( TermsBean.Attributes.create_expiry_date() );
        attrList.add( TermsBean.Attributes.create_reference_number() );
        attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_transferrable() );
        return attrList;
     }



    /**
     *  Performs validation of the attributes of this type of instrument and transaction.   This method is
     *  called from the userValidate() hook in the managed object.
     *
     *  @param terms - the bean being validated
     */
    public void validate(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       super.validate(terms);

       terms.registerRequiredAttribute("related_instrument_id");
       terms.registerRequiredAttribute("amount_currency_code");
       terms.registerRequiredAttribute("amount");
       terms.registerRequiredAttribute("expiry_date");

       validateGeneral(terms);
       validateInstructions(terms);
       validateTextLengths(terms);
    }



    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
     {
         final String[] attributesToExclude = {"special_bank_instructions","branch_code","coms_chrgs_our_account_num","coms_chrgs_foreign_acct_num","coms_chrgs_other_text"};

         InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

         ShipmentTerms shipmentTerms = terms.getFirstShipment();
         if(shipmentTerms != null)
          {
            InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());
          }

         validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "TransferELC.Transferee");
         validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "TransferELC.TransfereesBank");
	     }

    /**
     * Performs validation for the general section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateGeneral(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       // Validate that a legal Instrument ID has been entered in the Related Instrument ID field
       this.validateRelatedInstrumentId(terms);

       // Validate that the required Transferee terms party fields have data entered in them
       this.editTransfereeValue(terms, "FirstTermsParty");
    }

    /**
     * Performs validation for the Related Instrument Id field in the General section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateRelatedInstrumentId(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       ClientServerDataBridge   csdb                 = new ClientServerDataBridge();
       JPylonProperties         jPylonProperties     = null;
       DocumentHandler          relatedInstrumentDoc = null;
       DocumentHandler          corporateOrgDoc      = null;
       Transaction              transaction          = null;
       Instrument               instrument           = null;
       String                   relatedInstrumentId  = null;
       String                   instrumentCurrency   = null;
       String                   corporateOrgOid      = null;
       String                   instrumentType       = null;
       String                   serverLocation       = null;
       String                   termsOid             = null;
       long                     transactionOid       = 0;
       long                     instrumentOid        = 0;

       relatedInstrumentId = terms.getAttribute("related_instrument_id");
       termsOid            = terms.getAttribute("terms_oid");

       // If a related instrument ID was not entered, don't do anything further (the required
       // attribute status will cause an error to be issued here)
       if (relatedInstrumentId.equals(""))
       {
          return;
       }

       // Next, we need to get the corporate org oid for the current terms object so that we
       // look for the correct related instrument (it's possible for more than one instrument
       // to have the same complete instrument id, so we need to look by organization)
     //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
		String sqlQueryStr = "select a.a_corp_org_oid "
				+ " from instrument a, transaction b"
				+ " where b.c_cust_enter_terms_oid = ? "
				+ " and b.p_instrument_oid = a.instrument_oid";

       corporateOrgDoc = DatabaseQueryBean.getXmlResultSet(sqlQueryStr, true, 
    		   new Object[]{termsOid});
       
       corporateOrgOid = corporateOrgDoc.getAttribute("/ResultSetRecord(0)/A_CORP_ORG_OID");

       // Now that we have the user's corporate org oid, look for a related instrument
       // having the same complete instrument id as the one entered by the user. If the
       // instrument ID entered is valid, populate an Instrument object with its data;
       // otherwise, issue an error
		sqlQueryStr = "select instrument_oid "
				+ " from instrument where complete_instrument_id = ?"
				+ " and a_corp_org_oid = ?";

       relatedInstrumentDoc = DatabaseQueryBean.getXmlResultSet(sqlQueryStr, true, relatedInstrumentId, corporateOrgOid);
     //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
       
       if (relatedInstrumentDoc == null)
       {
          terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                             TradePortalConstants.INVALID_INSTRUMENT_ID);
          return;
       }

       instrumentOid = relatedInstrumentDoc.getAttributeLong("/ResultSetRecord(0)/INSTRUMENT_OID");

       jPylonProperties = JPylonProperties.getInstance();
       serverLocation   = jPylonProperties.getString("serverLocation");

       csdb.setLocaleName("en_US");

       instrument = (Instrument) EJBObjectFactory.createClientEJB(serverLocation, "Instrument", instrumentOid, csdb);

       // If a valid instrument ID was entered, retrieve its instrument type; if it isn't an
       // export letter of credit, issue an error
       instrumentType = instrument.getAttribute("instrument_type_code");

       if (!instrumentType.equals(InstrumentType.EXPORT_DLC))
       {
          terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                             TradePortalConstants.INVALID_ISSUE_TRANSFER_INSTR);
          return;
       }

       // If we're dealing with an export letter of credit instrument, then determine if it has any Advise
       // transactions; if no Advise transactions exist for the instrument, issue an error
       //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - Begin
       String whereClause = "p_instrument_oid = ? and transaction_type_code = ?";

       if (DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause,
    		   false, instrumentOid, TransactionType.ADVISE) < 1)
       {
          terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                             TradePortalConstants.INVALID_ISSUE_TRANSFER_INSTR);
          return;
       }
       //SSikhakolli - 05/06/2014 - Rel 9.0 IR#T36000026319 - SQL fix - End
       
       // Finally, compare the currency code entered by the user with the currency code of the
       // instrument's active transaction; if they aren't the same, issue an error
       transactionOid = instrument.getAttributeLong("active_transaction_oid");

       transaction = (Transaction) EJBObjectFactory.createClientEJB(serverLocation, "Transaction", transactionOid, csdb);

       transaction.getData(transactionOid);

       instrumentCurrency = transaction.getAttribute("copy_of_currency_code");

       if (!instrumentCurrency.equals(terms.getAttribute("amount_currency_code")))
       {
          terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                             TradePortalConstants.INVALID_INSTRUMENT_CURRENCY, instrumentCurrency);
       }

       try
        {
            instrument.remove();
            transaction.remove();
        }
       catch(javax.ejb.RemoveException re)
        {
            LOG.error("RemoveException in validateRelatedInstrumentId",re);
        }
    }

    /**
     * Performs validation for the Bank Instructions section.
     *
     * @param terms com.ams.tradeportal.busobj.termsBean
     */
    public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
       String   value = null;
       String   alias = null;

       // If the c&c foreign account number is given, the c&c foreign currency
       // must also be given; and vice-versa.
       value = terms.getAttribute("coms_chrgs_foreign_acct_num");

       if (!StringFunction.isBlank(value))
       {
          value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

          if (StringFunction.isBlank(value))
          {
             alias = terms.getAlias("coms_chrgs_foreign_acct_curr");

             terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
          }
       }
       else
       {
          value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

          if (!StringFunction.isBlank(value))
          {
             alias = terms.getAlias("coms_chrgs_foreign_acct_num");

             terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
          }
       }
    }

    /**
     *  Performs validation of text fields for length.
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
    	String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);
    	
    	if((overrideSwiftLengthInd != null) 
    			&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
    	{
    		checkTextLength(terms.getFirstShipment(), "goods_description", 6500);
    		checkTextLength(terms, "additional_conditions",     1000);
    	} 
    	checkTextLength(terms, "special_bank_instructions", 1000);
    	checkTextLength(terms, "coms_chrgs_other_text",     1000);
    }

   /**
    * Performs presave processing (regardless of whether validation is done)
    *
    * @param terms com.ams.tradeportal.busobj.termsBean
    * @exception com.amsinc.ecsg.frame.AmsException The exception description.
    */
    public void preSave(TermsBean terms) throws AmsException
    {
       super.preSave(terms);

       try
       {
          validateTextLengths(terms);

          validateAmount(terms, "amount", "amount_currency_code");
       }
       catch (RemoteException e)
       {
          LOG.error("Remote Exception caught in EXP_DLC__TRNTermsMgr.preSave()",e);
       }
    }


   //Beginning of code used by the Middleware team
    /**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

   {

   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
             LOG.debug("This is the termValuesDoc containing all the terms values: {}" ,termsValuesDoc.toString());

		   	    //Terms_Details
	            termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",				  termsValuesDoc.getAttribute("/Terms/expiry_date"));
	            termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	      termsValuesDoc.getAttribute("/Terms/reference_number"));
	            termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
	            termsDoc.setAttribute("/Terms/TermsDetails/Amount",                   termsValuesDoc.getAttribute("/Terms/amount"));
	            termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",  termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
	            termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",  termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
	            termsDoc.setAttribute("/Terms/TermsDetails/RelatedInstrumentID",	  termsValuesDoc.getAttribute("/Terms/related_instrument_id"));
	            termsDoc.setAttribute("/Terms/TermsDetails/Transferrable",		      termsValuesDoc.getAttribute("/Terms/transferrable"));
	            //End of Terms_Details

                // ShipmentTerms
                populateFirstShipmentTermsXml(terms, termsDoc);

	            //OtherConditions
	            termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",			termsValuesDoc.getAttribute("/Terms/additional_conditions"));
	            //End of Other_Conditions

	           //Instructions
	            termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
	            //End of Instructions

			   //IR#IAUE072066154 Add BranchCode,com_chrgs_other_text,com_chrgs_foreign_acct_num,com_chrgs_foreign_acct_curr
			   termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",				        termsValuesDoc.getAttribute("/Terms/branch_code"));
			   termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",      termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
			   termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",  termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
			   termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
			   termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",             termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));

	           //Terms Party
               DocumentHandler termsPartyDoc = new DocumentHandler();

                //Additional Terms Party
                String testTP = TermsPartyType.APPLICANT;


             int id = 0;

            termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

            for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
   			 {
				String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
   		      {
   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
   				 {
   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
   					  id++;
   				 }
   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
   				 //If the termsValuesDoc contains termsParty also need to modify the line below
   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
   			     id++;
   			   }
		      }

		 LOG.debug("This is TermsPartyDoc: " ,  termsPartyDoc.toString());

         termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
         termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


   		return termsDoc;
   }

     //End of Code by Indu Valavala

 //End of class
 }
