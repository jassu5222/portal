package com.ams.tradeportal.busobj.termsmgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

import java.rmi.*;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Fund Transfer Between Accounts.
 *
 * Note: for Request to Advise, the 3 terms parties represent these types:
 *        first     beneficiary
 *        second    applicant
 *        third     advising bank
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class FTBA__ISSTermsMgr extends CMTermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(FTBA__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public FTBA__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public FTBA__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an Issue Import DLC
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_ex_rt_variation_adjust() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_related_instrument_id() );
        attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
        attrList.add( TermsBean.Attributes.create_equivalent_exch_amount() );
        attrList.add( TermsBean.Attributes.create_equivalent_exch_amt_ccy());
        attrList.add( TermsBean.Attributes.create_transfer_fx_rate() );
        attrList.add( TermsBean.Attributes.create_credit_account_oid() );
        attrList.add( TermsBean.Attributes.create_debit_account_oid() );
        attrList.add( TermsBean.Attributes.create_transfer_description() );
		attrList.add( TermsBean.Attributes.create_work_item_priority() );
		attrList.add( TermsBean.Attributes.create_work_item_number() );
        attrList.add( TermsBean.Attributes.create_purpose_type() );
        attrList.add( TermsBean.Attributes.create_credit_account_pending_amount());
        attrList.add( TermsBean.Attributes.create_debit_account_pending_amount());
        attrList.add( TermsBean.Attributes.create_display_fx_rate_method());		//IAZ ER 5.2.1.3 06/29/10 Add
        attrList.add( TermsBean.Attributes.create_request_market_rate_ind());		//NSX CR-640/581 Rel 7.1.0 09/29/11
        attrList.add( TermsBean.Attributes.create_fx_online_deal_confirm_date());	//NSX CR-640/581 Rel 7.1.0 09/29/11
		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		super.validate(terms);
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");

		String fromAcctOID = terms.getAttribute("debit_account_oid");
		String toAcctOID = terms.getAttribute("credit_account_oid");

		//TODO This whole block of code is not used, Should remove this
		//Vshah - IR#PIUJ020353018 - BEGIN
		boolean exchangerateVariationisReqd = false;
		String debitAcctCur = null;
		String creditAcctCur = null;
	    	JPylonProperties         jPylonProperties     = null;

		Account debitAccount = null;
		Account creditAccount = null;
		CorporateOrganization corpOrg = null;

		jPylonProperties = JPylonProperties.getInstance();
	   	String serverLocation   = jPylonProperties.getString("serverLocation");

	        if (StringFunction.isNotBlank(fromAcctOID) && StringFunction.isNotBlank(toAcctOID))
	        {
	    		debitAccount = (Account) EJBObjectFactory.createClientEJB(serverLocation, "Account", Long.parseLong(fromAcctOID));
		    	creditAccount = (Account) EJBObjectFactory.createClientEJB(serverLocation, "Account", Long.parseLong(toAcctOID));;
		    	debitAcctCur = debitAccount.getAttribute("currency");
	    		creditAcctCur = creditAccount.getAttribute("currency");

	    		corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(serverLocation, "CorporateOrganization", Long.parseLong(creditAccount.getAttribute("owner_oid")));

	    		if (!debitAcctCur.equals(creditAcctCur))
	    		{
	    			exchangerateVariationisReqd = true;
	    		}
	    		else if ( !debitAcctCur.equals(corpOrg.getAttribute("base_currency_code")) || !creditAcctCur.equals(corpOrg.getAttribute("base_currency_code")))
	    		{
	    			exchangerateVariationisReqd = true;
	    		}
	    	}
	    	//Vshah - IR#PIUJ020353018 - END
	        //TODO remove end

		if (StringFunction.isBlank(fromAcctOID)) {
			terms.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					terms.getResourceMgr().getText("TransferBetweenAccounts.FromAccount",
							TradePortalConstants.TEXT_BUNDLE));
		}
		if (StringFunction.isBlank(toAcctOID)) {
			terms.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					terms.getResourceMgr().getText("TransferBetweenAccounts.ToAccount",
							TradePortalConstants.TEXT_BUNDLE));
		}
		if (StringFunction.isNotBlank(fromAcctOID) && StringFunction.isNotBlank(toAcctOID)) {
			if (fromAcctOID.equals(toAcctOID)) {
				terms.getErrorManager ().issueError (
						getClass().getName (), TradePortalConstants.FROM_TO_ACCT_CANNOT_BE_SAME);
			}
		}

		if (StringFunction.isBlank(terms.getAttribute("transfer_description"))) {
			terms.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					terms.getResourceMgr().getText("TransferBetweenAccounts.Description",
							TradePortalConstants.TEXT_BUNDLE));
		}
		if (!StringFunction.isBlank(terms.getAttribute("fec_rate")) && StringFunction.isBlank(terms.getAttribute("covered_by_fec_number"))) {
			terms.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					terms.getResourceMgr().getText("TransferBetweenAccounts.FECCovered",
							TradePortalConstants.TEXT_BUNDLE));
		}
		if (!StringFunction.isBlank(terms.getAttribute("covered_by_fec_number")) && StringFunction.isBlank(terms.getAttribute("fec_rate"))) {
			terms.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					terms.getResourceMgr().getText("TransferBetweenAccounts.FECRate",
							TradePortalConstants.TEXT_BUNDLE));
		}

		String value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
	}




	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	{


       //   	 Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
										 5,
										 8,
										 "TermsBeanAlias.fec_rate",
										 terms.getErrorManager(),
										 terms.getResourceManager());

	}

	//Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values: {}" ,termsValuesDoc.toString());

	    termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	                      termsValuesDoc.getAttribute("/Terms/transfer_description"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",       	              termsValuesDoc.getAttribute("/Terms/amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                    	              termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",                              termsValuesDoc.getAttribute("/Terms/purpose_type"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/WorkItemPriority",                        termsValuesDoc.getAttribute("/Terms/work_item_priority"));
		termsDoc.setAttribute("/Terms/LoansAndFunds/ExrtVariationAdjust", 		              termsValuesDoc.getAttribute("/Terms/ex_rt_variation_adjust"));
		// IValavala CR434 get the AppDebitAccPrincipal accountnum from account
		String accountUoid = termsValuesDoc.getAttribute("/Terms/debit_account_oid");
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 add debit source system branch
		//MDB CR-640 Rel7.1 10/13/11 - BEGIN added currency and bank_country_code to debit and credit accounts
		String accountNumSQL = "SELECT ACCOUNT_NUMBER, SOURCE_SYSTEM_BRANCH, CURRENCY, ADDL_VALUE as BANK_COUNTRY_CODE FROM ACCOUNT, REFDATA WHERE ACCOUNT_OID = ?" 
								 + " AND ACCOUNT.BANK_COUNTRY_CODE=REFDATA.CODE AND REFDATA.TABLE_TYPE = ? and REFDATA.LOCALE_NAME IS NULL"; //MDB PKUL102163801 Rel7.1 10/21/11
	    String accountNum = "";
	    String debitBranch = "";
	    String debitCCY = "";
	    String debitCountry = "";
	    DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid, TradePortalConstants.COUNTRY_ISO3116_3});
	    if(resultXML != null)
	     {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
            debitBranch = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/SOURCE_SYSTEM_BRANCH");
		    debitCCY = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/CURRENCY");
		    debitCountry = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/BANK_COUNTRY_CODE");
	 	  }
		termsDoc.setAttribute("/Terms/LoansAndFunds/AppDebitAccPrincipal", 		              accountNum);
	    //MDB PKUL102043568 Rel7.1 10/20/11 Begin - moved location in XML to match DTD
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCCY",            	debitCCY);
		termsDoc.setAttribute("/Terms/LoansAndFunds/DebitCountry",        	debitCountry);
	    //MDB PKUL102043568 Rel7.1 10/20/11 End

		// IValavala CR434 Adding CreditAccPrincipal. accountnum from account
		accountUoid = termsValuesDoc.getAttribute("/Terms/credit_account_oid");
		// CR-610 - Adarsha K S- 2/21/2011 - Start
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 add debit source system branch
		accountNumSQL = "SELECT ACCOUNT_NUMBER, SOURCE_SYSTEM_BRANCH, CURRENCY, ADDL_VALUE as BANK_COUNTRY_CODE FROM ACCOUNT, REFDATA WHERE ACCOUNT_OID = ? " 
							 + " AND ACCOUNT.BANK_COUNTRY_CODE=REFDATA.CODE AND REFDATA.TABLE_TYPE=? and REFDATA.LOCALE_NAME IS NULL"; //MDB PKUL102163801 Rel7.1 10/21/11
        accountNum = "";
        String creditBranch = "";
	    String creditCCY = "";
	    String creditCountry = "";
		resultXML = DatabaseQueryBean.getXmlResultSet(accountNumSQL, false, new Object[]{accountUoid, TradePortalConstants.COUNTRY_ISO3116_3 });
		if(resultXML != null)
		 {
		    accountNum = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/ACCOUNT_NUMBER");
		    creditBranch = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/SOURCE_SYSTEM_BRANCH");
		    creditCCY = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/CURRENCY");
		    creditCountry = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/BANK_COUNTRY_CODE");
		  }
	    //MDB PKUL102043568 Rel7.1 10/20/11 Begin - capitalize beginning C
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditCCY",            	creditCCY);
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditCountry",        	creditCountry);
	    //MDB PKUL102043568 Rel7.1 10/20/11 End
		//MDB CR-640 Rel7.1 10/13/11 - END added currency and bank_country_code
		termsDoc.setAttribute("/Terms/LoansAndFunds/CreditAccPrincipal", 		              accountNum);
        

		// IValavala CR434 Adding DailyLimitExceeded.,PaymentDate Values will be added in the TPLPackager as it comes from Transaction
		termsDoc.setAttribute("/Terms/LoansAndFunds/DailyLimitExceeded", 		              "");
		termsDoc.setAttribute("/Terms/LoansAndFunds/PaymentDate", "");
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 start
        termsDoc.setAttribute("/Terms/LoansAndFunds/DebitAcctSourceSystemBranch", debitBranch);    
        termsDoc.setAttribute("/Terms/LoansAndFunds/CreditAcctSourceSystemBranch", creditBranch);    
        //cquinton 3/29/2011 Rel 7.0.0 ir#laul032634214 end

		//Terms Party


	DocumentHandler termsPartyDoc = new DocumentHandler();


		int id = 0;

	   termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

       for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
	   	 {
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
	        if(!(termsPartyOID == null || termsPartyOID.equals("")))
	   	      {
	   			 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
	   			 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
	   			 termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
	   		     id++;
	   		   }
	      }

		 LOG.debug("This is TermsPartyDoc: {} " ,  termsPartyDoc.toString());

        termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
        termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		return termsDoc;
	}


	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");

			validateDecimalFields(terms);

		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in FTBA__ISSTermsMgr.preSave()",e);
		}

	}

}