package com.ams.tradeportal.busobj.termsmgr;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;
//import java.rmi.*;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for a Guarantee Amendment
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GUA__AMDTermsMgr extends TermsMgr
 {
private static final Logger LOG = LoggerFactory.getLogger(GUA__AMDTermsMgr.class);
	 //End of Code by Indu Valavala

//End of Class
 	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public GUA__AMDTermsMgr(AttributeManager mgr) throws AmsException
	 {
	  super(mgr);
	 }

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public GUA__AMDTermsMgr(TermsWebBean terms) throws AmsException
	 {
	  super(terms);
	 }

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an this instrument and transaction type
	 *
	 *  @return Vector - list of attributes
	 */
				public List<Attribute> getAttributesToRegister()
	 {
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_guar_customer_text() );
		attrList.add( TermsBean.Attributes.create_guar_valid_from_date() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_guar_valid_from_date_type() );
		attrList.add( TermsBean.Attributes.create_guar_expiry_date_type() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );

		return attrList;
	 }





	/**
	 *  Performs validation of the attributes of this type of instrument and transaction.   This method is
	 *  called from the userValidate() hook in the managed object.
	 *
	 *  @param terms - the bean being validated
	 */
	public void validate(TermsBean terms)
		throws java.rmi.RemoteException, AmsException {

                super.validate(terms);

		String value;

		// If all of these fields are blank, issue an error
		if (StringFunction.isBlank(terms.getAttribute("guar_valid_from_date_type"))
			&& StringFunction.isBlank(terms.getAttribute("amount"))                
			&& StringFunction.isBlank(terms.getAttribute("guar_expiry_date_type"))
			&& StringFunction.isBlank(terms.getAttribute("guar_valid_from_date"))
			&& StringFunction.isBlank(terms.getAttribute("expiry_date"))
			&& StringFunction.isBlank(terms.getAttribute("special_bank_instructions"))
			&& StringFunction.isBlank(terms.getAttribute("guar_customer_text")))
		{
				terms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.MUST_AMEND);
				
		}

		// Upon validation if the valid from date type is date of issue,
		// blank out the valid from date
		value = terms.getAttribute("guar_valid_from_date_type");
		if (value.equals(TradePortalConstants.DATE_OF_ISSUE)) {
			terms.setAttribute("guar_valid_from_date", "");
		}
		

		if(value.equals(TradePortalConstants.OTHER_VALID_FROM_DATE) && StringFunction.isBlank(terms.getAttribute("guar_valid_from_date"))){
			
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.FROM_OTHER_DATE_NOT_ENTERED);
		}
		// Upon validation if the valid to date is other, blank out the
		// validity date
		value = terms.getAttribute("guar_expiry_date_type");
		if (value.equals(TradePortalConstants.OTHER_EXPIRY_DATE)) {
			terms.setAttribute("expiry_date", "");
		}
		
		if(value.equals(TradePortalConstants.VALIDITY_DATE) && StringFunction.isBlank(terms.getAttribute("expiry_date"))){
			
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.TO_NEW_VALIDITY_DATE_NOT_ENTERED);
		}

	}

    /**
     * Performs the Terms Manager-specific validation of text fields in the Terms
     * and TermsParty objects.  
     *
     * @param terms - the managed object
     *
     */
    protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
    {
     
    }


 	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.termsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException
	{
           super.preSave(terms);

	    String value;

	    try {
			//The text length check is done in the presave to guard against
			//a possible problem with saving the data into the DB if it is too long.
		    validateTextLengths(terms);

		    //Check all amount entry fields
		    validateAmount(terms, "amount", "amount_currency_code");

	    } catch (java.rmi.RemoteException e) {
	        e.printStackTrace();
		    LOG.info("Remote Exception caught in GUA__AMDTermsMgr.preSave()");
	    }

	}	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	    throws java.rmi.RemoteException, AmsException {

	    checkTextLength(terms, "guar_customer_text", 5000);
	    checkTextLength(terms, "special_bank_instructions", 1000);
	}



 //Beginning of code used by the Middleware team
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
   public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

   {

   	         DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
			 LOG.debug("This is the termValuesDoc containing all the terms values: {}",termsValuesDoc.toString());



		   	 //Terms_Details
		       termsDoc.setAttribute("/Terms/TermsDetails/Amount",                          termsValuesDoc.getAttribute("/Terms/amount"));
		       termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",                      termsValuesDoc.getAttribute("/Terms/expiry_date"));
		       termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaExpiryDateType",      termsValuesDoc.getAttribute("/Terms/guar_expiry_date_type"));
		       termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaValidFromDate",		termsValuesDoc.getAttribute("/Terms/guar_valid_from_date"));
		       termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaValidFromDateType",	termsValuesDoc.getAttribute("/Terms/guar_valid_from_date_type"));
		       termsDoc.setAttribute("/Terms/TermsDetails/GuaTerms/GuaCustomerText",		termsValuesDoc.getAttribute("/Terms/guar_customer_text"));
		      //End of Terms_Details

		     //Instructions
		       termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",			termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
			 //End of Instructions
	           //Terms Party

			   DocumentHandler termsPartyDoc = new DocumentHandler();

				//Additional Terms Party
				String testTP = "APP";


			 int id = 0;

			termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

			for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
   			 {
				String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);

   		      if(!(termsPartyOID == null || termsPartyOID.equals("")))
   		      {
   				 TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
   				 if(termsParty.getAttribute("terms_party_type").equals(testTP))
   				 {
   					 LOG.debug("party_Type is APP, hence I need to check for thirdParty");
   					 termsPartyDoc = packageAdditionalTermsParty(terms,id,termsPartyDoc,termsParty);
   					 if(termsPartyDoc.getAttribute("/AdditionalTermsParty").equals("Y"))
   					  id++;
   				 }
   				 termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
   				 //If the termsValuesDoc contains termsParty also need to modify the line below
   			     termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
   			     id++;
   			   }
		      }

		 LOG.debug("This is TermsPartyDoc:{} " ,  termsPartyDoc.toString());

		 termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		 termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));


   		return termsDoc;
   }

	 //End of Code by Indu Valavala

//End of Class
}