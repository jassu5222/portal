package com.ams.tradeportal.busobj.termsmgr;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.AdditionalReqDoc;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation
 * for a Terms business object or TermsWebBean for an Issue Request to Advise.
 *
 * Note: for Request to Advise, the 3 terms parties represent these types:
 *        first     beneficiary
 *        second    applicant
 *        third     advising bank
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class RQA__ISSTermsMgr extends TermsMgr
{
private static final Logger LOG = LoggerFactory.getLogger(RQA__ISSTermsMgr.class);
	/**
	 *  Constructor that is used by business objects to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr AttributeManager attribute manager of the bean being managed.  Used to register attributes
	 */
	public RQA__ISSTermsMgr(AttributeManager mgr) throws AmsException
	{
		super(mgr);
	}

	/**
	 *  Constructor that is used by web beans to create their
	 *  manager.  This method also register the dynamic attributes of
	 *  the bean.
	 *
	 *  @param mgr TermsWebBean - the bean being managed.  Used to register attributes.
	 */
	public RQA__ISSTermsMgr(TermsWebBean terms) throws AmsException
	{
		super(terms);
	}

	/**
	 *  Returns a list of attributes.  These describe each of the attributes
	 *  that will be registered for the bean being managed.
	 *
	 *  This method controls which attributes of TermsBean or TermsWebBean are registered
	 *  for an Issue Import DLC
	 *
	 *  @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister()
	{
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add( TermsBean.Attributes.create_additional_conditions() );
		attrList.add( TermsBean.Attributes.create_addl_doc_indicator() );
		attrList.add( TermsBean.Attributes.create_addl_doc_text() );
		attrList.add( TermsBean.Attributes.create_amount() );
		attrList.add( TermsBean.Attributes.create_amount_currency_code() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_neg() );
		attrList.add( TermsBean.Attributes.create_amt_tolerance_pos() );
		//Leelavathi CR-737 Rel-8.2 10th Dec 2012 start
		/*attrList.add( TermsBean.Attributes.create_available_by() );*/
		//Leelavathi CR-737 Rel-8.2 10th Dec 2012 End
		attrList.add( TermsBean.Attributes.create_available_with_party() );
		attrList.add( TermsBean.Attributes.create_bank_charges_type() );
		attrList.add( TermsBean.Attributes.create_branch_code() );
		attrList.add( TermsBean.Attributes.create_cert_origin_copies() );
		attrList.add( TermsBean.Attributes.create_cert_origin_indicator() );
		attrList.add( TermsBean.Attributes.create_cert_origin_originals() );
		attrList.add( TermsBean.Attributes.create_cert_origin_text() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_copies() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_indicator() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_originals() );
		attrList.add( TermsBean.Attributes.create_comm_invoice_text() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_other_text() );
		attrList.add( TermsBean.Attributes.create_coms_chrgs_our_account_num() );
		attrList.add( TermsBean.Attributes.create_confirmation_type() );
		attrList.add( TermsBean.Attributes.create_covered_by_fec_number() );
		attrList.add( TermsBean.Attributes.create_drafts_required() );
		attrList.add( TermsBean.Attributes.create_drawn_on_party() );
		attrList.add( TermsBean.Attributes.create_expiry_date() );
		attrList.add( TermsBean.Attributes.create_fec_amount() );
		attrList.add( TermsBean.Attributes.create_fec_maturity_date() );
		attrList.add( TermsBean.Attributes.create_fec_rate() );
		attrList.add( TermsBean.Attributes.create_finance_drawing () );
		attrList.add( TermsBean.Attributes.create_finance_drawing_num_days() );
		attrList.add( TermsBean.Attributes.create_finance_drawing_type() );
		attrList.add( TermsBean.Attributes.create_ins_policy_copies() );
		attrList.add( TermsBean.Attributes.create_ins_policy_indicator() );
		attrList.add( TermsBean.Attributes.create_ins_policy_originals() );
		attrList.add( TermsBean.Attributes.create_ins_policy_text() );
		attrList.add( TermsBean.Attributes.create_insurance_endorse_value_plus() );
		attrList.add( TermsBean.Attributes.create_insurance_risk_type() );
		attrList.add( TermsBean.Attributes.create_internal_instructions() );
		attrList.add( TermsBean.Attributes.create_irrevocable() );
		attrList.add( TermsBean.Attributes.create_operative() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_originals() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_1_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_2_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_3_text () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_copies () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_indicator () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_name() );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_originals () );
		attrList.add( TermsBean.Attributes.create_other_req_doc_4_text () );
		attrList.add( TermsBean.Attributes.create_packing_list_copies() );
		attrList.add( TermsBean.Attributes.create_packing_list_indicator() );
		attrList.add( TermsBean.Attributes.create_packing_list_originals() );
		attrList.add( TermsBean.Attributes.create_packing_list_text() );
		attrList.add( TermsBean.Attributes.create_place_of_expiry() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_days_after_type() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_num_days_after () );
		attrList.add( TermsBean.Attributes.create_pmt_terms_percent_invoice() );
		attrList.add( TermsBean.Attributes.create_pmt_terms_type() );
		attrList.add( TermsBean.Attributes.create_present_docs_days_user_ind() );
		attrList.add( TermsBean.Attributes.create_present_docs_within_days() );
		attrList.add( TermsBean.Attributes.create_purpose_type() );
		attrList.add( TermsBean.Attributes.create_reference_number() );
		attrList.add( TermsBean.Attributes.create_maximum_credit_amount() );
		attrList.add( TermsBean.Attributes.create_addl_amounts_covered() );
		attrList.add( TermsBean.Attributes.create_issuer_ref_num() );
		attrList.add( TermsBean.Attributes.create_revolve() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_curr() );
		attrList.add( TermsBean.Attributes.create_settlement_foreign_acct_num() );
		attrList.add( TermsBean.Attributes.create_settlement_our_account_num() );
		attrList.add( TermsBean.Attributes.create_special_bank_instructions() );
		attrList.add( TermsBean.Attributes.create_transferrable() );
		attrList.add( TermsBean.Attributes.create_ucp_details() );
		attrList.add( TermsBean.Attributes.create_ucp_version () );
		attrList.add( TermsBean.Attributes.create_ucp_version_details_ind() );
		attrList.add( TermsBean.Attributes.create_partial_shipment_allowed() );
        attrList.add( TermsBean.Attributes.create_work_item_number() );

      //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
        attrList.add( TermsBean.Attributes.create_payment_type() );
		attrList.add( TermsBean.Attributes.create_percent_amount_dis_ind() );
		attrList.add( TermsBean.Attributes.create_special_tenor_text() );
	  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

		return attrList;
	}

	/**
	 *  Performs validation of the attributes of the managed object.   This method is called
	 *  from the userValidate() hook in the managed object.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validate(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		// Variables used for shipment terms validations
		boolean       hasMultipleShipments = false;
		String []     requiredAttributes   = new String [1];
		String        shipmentNumber;
		StringBuffer  shipmentNumDesc;
		int           shipmentCount;
		int           shipmentIndex;
		String        shipmentOid;
		Vector        shipmentTermsSorted = new Vector();
		ComponentList shipmentTermsList;
		ShipmentTerms shipmentTerms;

		super.validate(terms);

		terms.registerRequiredAttribute("expiry_date");
		terms.registerRequiredAttribute("amount_currency_code");
		terms.registerRequiredAttribute("amount");
		terms.registerRequiredAttribute("bank_charges_type");
		terms.registerRequiredAttribute("available_with_party");
		terms.registerRequiredAttribute("place_of_expiry");
		terms.registerRequiredAttribute("drafts_required");

		// The second terms party is the applicant.
		//PDastidar IR#SVUG061649985 22-Jun-2006 Start
		//Instead of letting jPylon handle this, the error is explicitely
		//issued by calling issueTermsPartyError() from the super class TermsMgr


		if( StringFunction.isBlank(terms.getAttribute("c_SecondTermsParty")))
		{
			issueTermsPartyError(terms, "RequestAdviseIssue.Applicant");
		}
		//PDastidar IR#SVUG061649985 22-Jun-2006 End


		// The sixth Terms party is the Issuer
		// terms.registerRequiredAttribute("c_SixthTermsParty");

		validateGeneral(terms);
		validateDocsRequired(terms);

		// Get a handle to the term's shipment terms components
		shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		// Sort the list of shipment terms so they are placed in the order they were created
		shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);

		shipmentCount = shipmentTermsSorted.size();

		// Validate each set of shipment terms
		for (shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
		{
			shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
			shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
			shipmentNumDesc = new StringBuffer();

			// If there are multiple shipments, validate any required attributes
			if (shipmentCount > 1)
			{
				hasMultipleShipments = true;
				shipmentNumber = String.valueOf(shipmentIndex + 1);
				shipmentNumDesc.append(terms.getResourceManager().getText("RequestAdviseIssue.Shipment",
						TradePortalConstants.TEXT_BUNDLE));
				shipmentNumDesc.append(" ");
				shipmentNumDesc.append(shipmentNumber + " -");
				requiredAttributes[0] = "description";
				shipmentTerms.validateRequiredAttributes(requiredAttributes, shipmentNumDesc.toString());
			}
			else
			{
				shipmentNumDesc.append(terms.getResourceManager().getText("RequestAdviseIssue.Shipment",
						TradePortalConstants.TEXT_BUNDLE));
				shipmentNumDesc.append(" -");
			}

			// Perform subclass-specific validation of text fields for SWIFT characters
			validateForSwiftCharacters(shipmentTerms);

			// Validate the transport docs section
			validateTransportDocs(terms, shipmentTerms, shipmentNumDesc.toString(), hasMultipleShipments);

			// validate the shipment section
			validateShipment(terms, shipmentTerms, shipmentNumDesc.toString());
		}

		validateInstructions(terms);
		validateBeneficiary(terms);
		
		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
		validateOtherCondtions(terms);
		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End
	}


	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException
	{
		final String[] attributesToExclude = {"special_bank_instructions", "branch_code", "settlement_our_account_num",
				"settlement_foreign_acct_num", "coms_chrgs_our_account_num", "coms_chrgs_foreign_acct_num",
				"coms_chrgs_other_text", "covered_by_fec_number", "internal_instructions"};

		InstrumentServices.checkForInvalidSwiftCharacters(terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);

		validateTermsPartyForSwiftChars(terms, "FirstTermsParty", "RequestAdviseIssue.Beneficiary");
		validateTermsPartyForSwiftChars(terms, "SecondTermsParty", "RequestAdviseIssue.Applicant");
		validateTermsPartyForSwiftChars(terms, "ThirdTermsParty", "RequestAdviseIssue.ApplicantBank");
		validateTermsPartyForSwiftChars(terms, "FourthTermsParty", "RequestAdviseIssue.AdviseThroughBank");
		validateTermsPartyForSwiftChars(terms, "FifthTermsParty", "RequestAdviseIssue.ReimbursingBank");
	}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the ShipmentTerms
	 * and TermsParty objects.
	 *
	 * @param terms - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(ShipmentTerms shipmentTerms) throws AmsException, RemoteException
	{
		InstrumentServices.checkForInvalidSwiftCharacters(shipmentTerms.getAttributeHash(), shipmentTerms.getErrorManager());

		validateTermsPartyForSwiftChars(shipmentTerms, "NotifyParty", "RequestAdviseIssue.NotifyParty");
		validateTermsPartyForSwiftChars(shipmentTerms, "OtherConsigneeParty", "RequestAdviseIssue.OtherConsignee");
	}

	/**
	 * Get the other consignee terms party and then ask it to validate
	 * himself
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 */
	public void checkOtherConsignee(ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws RemoteException, AmsException
	{
		this.editForAnyConsigneeValue(shipmentTerms, "OtherConsigneeParty", shipmentNumDesc);
	}

	/**
	 * Get the Beneficiary terms party and then ask it to validate himself.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateBeneficiary(TermsBean terms)
	throws RemoteException, AmsException
	{
		this.editRequiredBeneficiaryValue(terms, "FirstTermsParty");
	}

	/**
	 * Performs validation for the documents required section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateDocsRequired(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		String value;

		// For each of the 4 types of required documents (Commercial Invoice,
		// Packing List, Certificate of Origin, and Insurance Policy) and the
		// 4 freeform other documents, the rules are:
		// if the checkbox is checked, then either originals or copies must
		// be non-blank (valid number check is done elsewhere).  If
		// the checkbox is unchecked, blank out the values.  The text field
		// is never required.  For the freeform fields, the name is required
		// if the checkbox is checked
		value = terms.getAttribute("comm_invoice_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("comm_invoice_originals", "");
			terms.setAttribute("comm_invoice_copies", "");
			terms.setAttribute("comm_invoice_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("comm_invoice_originals"))
					&& StringFunction.isBlank(terms.getAttribute("comm_invoice_copies"))) {
				value = terms.getResourceManager().getText("RequestAdviseIssue.CommercialInv",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		value = terms.getAttribute("packing_list_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("packing_list_originals", "");
			terms.setAttribute("packing_list_copies", "");
			terms.setAttribute("packing_list_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("packing_list_originals"))
					&& StringFunction.isBlank(terms.getAttribute("packing_list_copies"))) {
				value = terms.getResourceManager().getText("RequestAdviseIssue.PackingList",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		value = terms.getAttribute("cert_origin_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("cert_origin_originals", "");
			terms.setAttribute("cert_origin_copies", "");
			terms.setAttribute("cert_origin_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("cert_origin_originals"))
					&& StringFunction.isBlank(terms.getAttribute("cert_origin_copies"))) {
				value = terms.getResourceManager().getText("RequestAdviseIssue.OriginCertificate",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
		}

		// Additional validation for ins policy: If the checkbox is checked,
		// endorse value and risk type fields are both required.
		value = terms.getAttribute("ins_policy_indicator");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			terms.setAttribute("ins_policy_originals", "");
			terms.setAttribute("ins_policy_copies", "");
			terms.setAttribute("ins_policy_text", "");
		} else {
			if (StringFunction.isBlank(terms.getAttribute("ins_policy_originals"))
					&& StringFunction.isBlank(terms.getAttribute("ins_policy_copies"))) {
				value = terms.getResourceManager().getText("RequestAdviseIssue.InsPolicyCert",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REQD_DOC_INFO_MISSING,
						value);
			}
			if (StringFunction.isBlank(terms.getAttribute("insurance_endorse_value_plus"))
					|| StringFunction.isBlank(terms.getAttribute("insurance_risk_type"))) {
				value = terms.getResourceManager().getText("RequestAdviseIssue.InsPolicyCert",
						TradePortalConstants.TEXT_BUNDLE);
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INS_DOC_INFO_MISSING,
						value);
			}
		}

		// Validation for the 4 "Other" docs is the same
		for (int x=1; x<=4; x++) {
			String prefix = "other_req_doc_" + x;
			value = terms.getAttribute(prefix + "_indicator");
			LOG.debug("the other doc indicator for {} is {}" ,x ,value);
			if (StringFunction.isBlank(value) ||
					value.equals(TradePortalConstants.INDICATOR_NO)) {
				LOG.debug("resetting to blank");
				terms.setAttribute(prefix + "_originals", "");
				terms.setAttribute(prefix + "_copies", "");
				terms.setAttribute(prefix + "_text", "");
				terms.setAttribute(prefix + "_name", "");
				LOG.debug("finished resetting");
			} else {
				// Either originals or copies is required.  Name is required.
				if (StringFunction.isBlank(terms.getAttribute(prefix + "_originals"))
						&& StringFunction.isBlank(terms.getAttribute(prefix + "_copies"))) {
					// We're missing some information.  Give an error.  Try to use the
					// doc name given by the user.  Otherwise use a generic "Other document x".
					value = terms.getAttribute(prefix + "_name");
					if (StringFunction.isBlank(value)) {
						value = terms.getResourceManager().getText("RequestAdviseIssue.OtherDocument",
								TradePortalConstants.TEXT_BUNDLE);
						value += " " + x;
					}
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.REQD_DOC_INFO_MISSING,
							value);
				}
				if (StringFunction.isBlank(terms.getAttribute(prefix + "_name"))) {
					value = terms.getResourceManager().getText("RequestAdviseIssue.DocName",
							TradePortalConstants.TEXT_BUNDLE);
					value += " " + x;
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							AmsConstants.REQUIRED_ATTRIBUTE,
							value);
				}
			}
		}

		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		//Get a handle to the term's shipment terms components
		ComponentList additionalReqDocList = (ComponentList) terms.getComponentHandle("AdditionalReqDocList");

		int addRedDocCount = additionalReqDocList.getObjectCount();

		// Validate each set of shipment terms
		for (int addReqDocIndex = 0; addReqDocIndex < addRedDocCount; addReqDocIndex++){
			additionalReqDocList.scrollToObjectByIndex(addReqDocIndex);
			value = additionalReqDocList.getListAttribute("addl_req_doc_ind");
			if (StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)) {
				// Both originals or copies is required.  As well as Name is required.
				if (StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_originals"))
						&& StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_copies"))) {
					value = additionalReqDocList.getListAttribute("addl_req_doc_name");
					if (StringFunction.isBlank(value)) {
						value = "Document Name "+(addReqDocIndex+5);
					}
					terms.getErrorManager().issueError(	TradePortalConstants.ERR_CAT_1,
														TradePortalConstants.REQD_DOC_INFO_MISSING,
														value);
				}
				if (StringFunction.isBlank(additionalReqDocList.getListAttribute("addl_req_doc_name"))) {
					terms.getErrorManager().issueError(	TradePortalConstants.ERR_CAT_1,
														AmsConstants.REQUIRED_ATTRIBUTE,
														"Document Name "+(addReqDocIndex+5));
				}
			}

		}
		value = terms.getAttribute("addl_doc_indicator");
		if ((StringFunction.isNotBlank(value) &&
				value.equals(TradePortalConstants.INDICATOR_YES))
				&& StringFunction.isBlank(terms.getAttribute("addl_doc_text"))) {
				terms.getErrorManager().issueError(
					 TradePortalConstants.ERR_CAT_1,TradePortalConstants.ADDL_DOC_INFO_MISSING,	value);
		}
	
	}

	/**
	 *  Performs validation of the general section of the page
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateGeneral(TermsBean terms)
	throws java.rmi.RemoteException, AmsException {

		String value;
		//Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  Start
		//StringBuffer  sqlQuery  = null;
	  	//String 		  termsOid  = terms.getAttribute("terms_oid");
	  	DocumentHandler    instrumentAmountDoc       = null;
		//Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  End
		// Expiry date must be a valid date.  The required edit
		// logic guarantees it has a value.  The setAttribute logic
		// guarantees it is a valid date.  No logic required here.

		// If pmt terms type is the 'days after' option, both
		// the num days and tenor type fields are required.  If
		// the type is not 'days after' reset those attributes to blank.
		value = terms.getAttribute("pmt_terms_type");
		if (value.equals(TradePortalConstants.PMT_DAYS_AFTER)) {
			value = terms.getAttribute("pmt_terms_num_days_after");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NUM_DAYS_REQD);
			}
			value = terms.getAttribute("pmt_terms_days_after_type");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TENOR_REQD);
			}
		} else {
			terms.setAttribute("pmt_terms_num_days_after", "");
			terms.setAttribute("pmt_terms_days_after_type", "");
		}

		// Validate that drawn on party is selected if drafts are required
		value = terms.getAttribute("drafts_required");
		if (value.equals(TradePortalConstants.INDICATOR_YES)) {
			value = terms.getAttribute("drawn_on_party");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DRAWN_ON_PARTY_REQD);
			}
		}

		// Validate Instrument Issue Date
		DocumentHandler     issueDateDoc       = null;
		String				issueDate			= null;
		String termsOid = terms.getAttribute("terms_oid");
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("select distinct instrument.issue_date from instrument, transaction, terms ");
		sqlQuery.append("where terms.terms_oid = ?");
		sqlQuery.append(" AND ");
		sqlQuery.append("terms.terms_oid = transaction.c_cust_enter_terms_oid AND ");
		sqlQuery.append("transaction.p_instrument_oid = instrument.instrument_oid");

		issueDateDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});
		issueDate = issueDateDoc.getAttribute("/ResultSetRecord(0)/ISSUE_DATE");

		if ((issueDate != null) && (!issueDate.equals(""))) {
			// Do Nothing, Issue Date Exists
		} else { // No issue date entered, show error
			terms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.ISSUE_DATE_REQUIRED);
		}

		// pcutrone - 10/17/2007 - REUH101146143 - Check to make sure the amount field is not set to zero.
		value = terms.getAttribute("amount");
		if (!StringFunction.isBlank(value)) {
			if (Double.parseDouble(value) == 0) {
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.AMOUNT_IS_ZERO);
			}
		}
		// pcutrone - 10/17/2007 - REUH101146143 - END

		// Validate the Amount currency code with the PO Line Items currency code.  They must match.
		validateAmountCurrencyAndPOCurrency(terms);
		
		value = terms.getAttribute("payment_type");
		if(StringFunction.isBlank(value)){
			terms.getErrorManager ().issueError (
	                   getClass (). getName (), AmsConstants.REQUIRED_ATTRIBUTE,terms.getResourceManager().getText("RAQIssue.PmtType",
								TradePortalConstants.TEXT_BUNDLE));

		}else{

			ComponentList pmtTermsTenorDtlList = (ComponentList) terms.getComponentHandle("PmtTermsTenorDtlList");

			int pmtTermsCount = pmtTermsTenorDtlList.getObjectCount();
			String amountPercentInd = terms.getAttribute("percent_amount_dis_ind");
			int totalPercent = 0;
			BigDecimal totalAmount = BigDecimal.ZERO;
			BigDecimal instrumentAmount = BigDecimal.ZERO;
			String specialTenorText= terms.getAttribute("special_tenor_text");

			 if(value.equals("SPEC") ){
					if(StringFunction.isBlank(specialTenorText) ){
					//�Tenor Terms details are require if Available By is Special�.
						terms.getErrorManager ().issueError (
				                   getClass (). getName (), TradePortalConstants.PMT_TENOR_DTL_REQ);
					}
				} else
			// Validate each set of tenor payment terms
			for (int pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
				pmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
				//percent amount tenor_type num_days_after days_after_type maturity_date
				String percent= pmtTermsTenorDtlList.getListAttribute("percent");
				String amount= pmtTermsTenorDtlList.getListAttribute("amount");
				String tenorType= pmtTermsTenorDtlList.getListAttribute("tenor_type");
				String numDaysAfter= pmtTermsTenorDtlList.getListAttribute("num_days_after");
				String daysAfterType= pmtTermsTenorDtlList.getListAttribute("days_after_type");
				String maturityDate= pmtTermsTenorDtlList.getListAttribute("maturity_date");
				//Leelavathi IR#T36000017713 Rel-8.2 05/31/2013 Begin
				if(value.equals("MIXP") || value.equals("NEGO")){
					 if(StringFunction.isBlank(tenorType) ){
							//�If Tenor Details has been specified, then �Days after� must also be specified in Payment Summary line �x�
						 	//IR T36000048558 Rel9.5 05/09/2016
							terms.getErrorManager ().issueError (
					                   getClass (). getName (), TradePortalConstants.TENOR_TYPE_REQUIRED_CONDITION, 
					                   new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorType",
												TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
														TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
							}
				}
				if(value.equals("DEFP") || value.equals("ACCP") || (
						(value.equals("MIXP") || value.equals("NEGO"))
							&& ("ACC".equals(tenorType) || "DFP".equals(tenorType))
				) ){

										if(StringFunction.isNotBlank(daysAfterType) ){
											if(StringFunction.isBlank(numDaysAfter) ){
												//�If Tenor Details has been specified, then �Days after� must also be specified in Payment Summary line �x�
												terms.getErrorManager ().issueError (
										                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_REQUIRED_BASED_CONDITION,
														new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorDetails",
																TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("CorpCust.BankDefinedRulesDaysAfter",
																		TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
											}else if(Integer.parseInt(numDaysAfter)<=0){
												//�Value in days after field should be greater than 0 in Payment Summary line �x�
												terms.getErrorManager ().issueError (
										                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_MIN_CONDITION,
														new String[]{terms.getResourceManager().getText("ExportCollectionIssue.DaysAfter",
																TradePortalConstants.TEXT_BUNDLE),"0", terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
											}
										}else if (StringFunction.isBlank(maturityDate)){
											if(StringFunction.isNotBlank(numDaysAfter)
													&& Integer.parseInt(numDaysAfter)<=0){
														//�Value in days after field should be greater than 0 in Payment Summary line �x�
														terms.getErrorManager ().issueError (
												                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_MIN_CONDITION,
																new String[]{terms.getResourceManager().getText("ExportCollectionIssue.DaysAfter",
																		TradePortalConstants.TEXT_BUNDLE),"0", terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																		TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});

												}

													//�Either Tenor Details or Maturity Date is required for Payment Summary line x�
													//IR T36000048558 Rel9.5 05/09/2016
													terms.getErrorManager ().issueError (
											                   getClass (). getName (), TradePortalConstants.ROW_ATTRIBUTE_REQUIRED_EITHER_OF_TWO,
											                   new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorDetails",
																		TradePortalConstants.TEXT_BUNDLE),terms.getResourceManager().getText("OutgoingSLCIssue.MaturityDate",
																		TradePortalConstants.TEXT_BUNDLE), terms.getResourceManager().getText("OutgoingSLCIssue.PmtSummary",
																				TradePortalConstants.TEXT_BUNDLE),""+(pmtTermsIndex+1)});
													//Leelavathi IR#T36000012144 03/06/2013 Rel-8.2 End

					}else {
						SimpleDateFormat dateFormatter   = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						String creationDate = DateTimeUtility.getGMTDateTime();
						try {
							if(dateFormatter.parse(maturityDate).before(dateFormatter.parse(creationDate))){
							//Maturity Date in Payment Summary line �x� cannot be less than today�s date or the Issue date of this transaction
								terms.getErrorManager ().issueError (
						                   getClass (). getName (), TradePortalConstants.PMT_MATURITY_DATE_CHECK,
										new String[]{""+(pmtTermsIndex+1)});
							}
						} catch (ParseException e) {
							LOG.error("Parse exception at validateGeneral(): ",e);
							throw new AmsException("Date parse error caught in IMP_DLC__ISSTermsMgr.validateGeneral()");
						}
					}
				}
				 if(value.equals("MIXP") || value.equals("NEGO")){
					//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 Begin
					 if( "P".equals(amountPercentInd)){
						//PMitnala Rel 8.3 IR#T36000022676 - Check for Blank/Null values of percent
						    if(StringFunction.isNotBlank(percent)){
							totalPercent += Integer.parseInt(percent);
						    }
						}else if ( "A".equals(amountPercentInd)){

							if (StringFunction.isNotBlank(amount))
				            {
								StringBuffer alias = new StringBuffer() ;
								alias.append(terms.getResourceManager().getText("ImportDLCIssue.PaymentInAmount", TradePortalConstants.TEXT_BUNDLE));
								alias.append(" 'in Payment Summary "+(pmtTermsIndex+1)+"' ");

								try{
								    // Validate that value will fit in database and that currency precision is correct
								TPCurrencyUtility.validateAmount(terms.getAttribute("amount_currency_code"),
										amount, alias.toString(), terms.getErrorManager() );
								}catch(com.amsinc.ecsg.frame.AmsException amsExp){
									LOG.error("Ams exception at validateGeneral(): ",amsExp);
								}
								//Leelavathi IR#T36000017721 03/06/2013 CR-737 Rel-8.2 Begin
								totalAmount = totalAmount.add(new BigDecimal(amount));
								//Leelavathi IR#T36000017721 03/06/2013 CR-737 Rel-8.2 End
				            }
							
						}
					 //Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 End
				}
			}
			//Jyoti IR#T36000012016 CR737 Rel-8.2 27th Feb,2013  Start
			 try{
				 LOG.debug("termsOid is: {}",termsOid);
				 if(termsOid!=null){
				 	sqlQuery  = new StringBuffer();
					sqlQuery.append("select instrument_amount ");
					sqlQuery.append("from transaction ");
					sqlQuery.append("where C_CUST_ENTER_TERMS_OID= ?");
					instrumentAmountDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});

					if (instrumentAmountDoc != null)
					{
						//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 Begin
						String instrumentAmt=instrumentAmountDoc .getAttribute("/ResultSetRecord(0)/INSTRUMENT_AMOUNT");
						//Leelavathi IR#T36000015041 CR-737 Rel-8.2 07/05/2013 Begin
						Double instrumentLongAmount=new Double(StringFunction.isNotBlank(instrumentAmt)?
												instrumentAmt:terms.getAttribute("amount"));
						//Leelavathi IR#T36000015041 CR-737 Rel-8.2 07/05/2013 End
						instrumentAmount = BigDecimal.valueOf(instrumentLongAmount);
						//Leelavathi IR#T36000015183,IR#T36000015039,IR# T36000011401  CR-737 Rel-8.2 28/03/2013 End
						LOG.debug("instrumentAmount is: {}",instrumentAmount);
					}
				 }

			 }catch(Exception e ){
				 LOG.error("Exception at validateGeneral(): ",e);
			 }
			 if(value.equals("MIXP") || value.equals("NEGO")){
			 if(  "P".equals(amountPercentInd) && totalPercent!= 100){
					//�Total of all Tenor percents must equal to 100�
					terms.getErrorManager ().issueError (
			                   getClass (). getName (), TradePortalConstants.ROW_COLUMN_TOT_CHECK,
			                   					new String[]{terms.getResourceManager().getText("OutgoingSLCIssue.TenorPct",
														TradePortalConstants.TEXT_BUNDLE), "100"});
					//Leelavathi IR#T36000017301 21/05/2013 CR-737 Rel-8.2 Begin
				}
		}
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

	}

	/**
	 *  Performs validation of Payment Terms with Available By selection.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validatePaymentTerms(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		//Leelavathi CR-737 Rel-8.2 10th Dec 2012 start
		String availableBy = terms.getAttribute("payment_type");
		String pmtTermsType = terms.getAttribute("pmt_terms_type");
//Modified by dillip IR#T36000015652
		if (((availableBy != null) && (!availableBy.equals("PAYM")))
			&& ((pmtTermsType != null) && ((!pmtTermsType.equals("SGT")) && (!pmtTermsType.equals("OTH")))))
		{
			//Incorrect Combination Throw Error
			//Kiran IR#T36000015648 Rel-8.2 04/22/2013 Start
			//Replaced the values 'Avalaible By' & 'Payment Terms' with TextResources.properties constants
			terms.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.INVALID_COMB_AVAILBY_PMTTERMS,terms.getResourceManager().getText("RequestAdviseIssue.AvailableBy",
                        TradePortalConstants.TEXT_BUNDLE), terms.getResourceManager().getText("RequestAdviseIssue.PaymentTerms",
                                TradePortalConstants.TEXT_BUNDLE));
		}
			//Kiran IR#T36000015648 Rel-8.2 04/08/2013 End
		if (((availableBy != null) && (availableBy.equals("DEFP")))
			&& ((pmtTermsType != null) && ((!pmtTermsType.equals("DAY")) && (!pmtTermsType.equals("OTH")) && ((!pmtTermsType.equals("PAYM"))))))
		{
			//Incorrect Combination Throw Error
			//Kiran IR#T36000015648 Rel-8.2 04/22/2013 Start
			//Replaced the values 'Available By' & 'Payment Terms' with TextResources.properties constants
			terms.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.INVALID_COMB_AVAILBY_PMTTERMS, terms.getResourceManager().getText("RequestAdviseIssue.AvailableBy",
                        TradePortalConstants.TEXT_BUNDLE), terms.getResourceManager().getText("RequestAdviseIssue.PaymentTerms",
                                TradePortalConstants.TEXT_BUNDLE));
		}
			//Kiran IR#T36000015648 Rel-8.2 04/22/2013 End
	}


	/**
	 * Performs validation for the bank instructions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateInstructions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value;
		String alias;

		// If the foreign account number is given, the foreign currency
		// must also be given; and vice-versa.
		value = terms.getAttribute("settlement_foreign_acct_num");

		if (!StringFunction.isBlank(value))
		{
			value = terms.getAttribute("settlement_foreign_acct_curr");

			if (StringFunction.isBlank(value))
			{
				alias = terms.getAlias("settlement_foreign_acct_curr");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SETTLE_INFO_MISSING, alias);
			}
		}
		else
		{
			value = terms.getAttribute("settlement_foreign_acct_curr");

			if (!StringFunction.isBlank(value))
			{
				alias = terms.getAlias("settlement_foreign_acct_num");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SETTLE_INFO_MISSING, alias);
			}
		}

		// If finance drawing indicator is Y, then num days and type
		// fields are required.
		value = terms.getAttribute("finance_drawing");
		if (!StringFunction.isBlank(value)
				&& value.equals(TradePortalConstants.INDICATOR_YES)) {
			value = terms.getAttribute("finance_drawing_num_days");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FINANCE_INFO_REQD);
			}
			value = terms.getAttribute("finance_drawing_type");
			if (StringFunction.isBlank(value)) {
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.FINANCE_INFO_REQD);
			}
		} else {
			// finance_drawing checkbox is unchecked -- blank out the fields
			// associated with this checkbox.
			terms.setAttribute("finance_drawing_num_days", "");
			terms.setAttribute("finance_drawing_type", "");
		}

		// If the c&c foreign account number is given, the c&c foreign currency
		// must also be given; and vice-versa.
		value = terms.getAttribute("coms_chrgs_foreign_acct_num");

		if (!StringFunction.isBlank(value))
		{
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (StringFunction.isBlank(value))
			{
				alias = terms.getAlias("coms_chrgs_foreign_acct_curr");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
			}
		}
		else
		{
			value = terms.getAttribute("coms_chrgs_foreign_acct_curr");

			if (!StringFunction.isBlank(value))
			{
				alias = terms.getAlias("coms_chrgs_foreign_acct_num");

				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.COMM_CHRG_INFO_MISSING, alias);
			}
		}
	}

	/**
	 * Performs validation for the shipment section.
	 *
	 * @param currentTerms com.ams.tradeportal.busobj.TermsBean
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 */
	public void validateShipment(TermsBean currentTerms, ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws java.rmi.RemoteException, AmsException {

		String value;
		String incoterm;
		String location;
		String shipFrom;
		String shipLoad;
		String shipTo;
		String shipDischarge;
		String shipmentDate;
		String alias;
		//VS IR SDUH021456070 01/13/11 Rel 7.0
		final long ONE_HOUR = 60 * 60 * 1000L;

		// Shipment date must be valid.  This has already been done.
		// (Blank date is "valid")

		// Expiry date must be after shipment date (if provided).
		// (Expiry date is a required field.  Shipment date is not.)
		shipmentDate = shipmentTerms.getAttribute("latest_shipment_date");
		value = currentTerms.getAttribute("expiry_date");
		if (!StringFunction.isBlank(shipmentDate) && !StringFunction.isBlank(value)) {
			GregorianCalendar shipDate = new GregorianCalendar();
			GregorianCalendar expiryDate = new GregorianCalendar();

			shipDate.setTime(DateTimeUtility.convertStringDateTimeToDate(shipmentDate));
			expiryDate.setTime(DateTimeUtility.convertStringDateTimeToDate(value));

			//VS IR SDUH021456070 01/13/11 Rel 7.0 Begin

			long diffDays = ((expiryDate.getTimeInMillis() - shipDate.getTimeInMillis()  + ONE_HOUR)/ (ONE_HOUR * 24));

			//The number of days between the Expiry Date and Latest Shipment Date cannot be greater than 999 days.
			if (diffDays > 999){
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DIFFERENCE_IN_EXPIRY_DATE_AND_SHIP_DATE,
						errorParameters,
						wrapParameters);
			}
			//VS IR SDUH021456070 01/13/11 Rel 7.0 End

			// If the ship date is after the expiry date, give error
			if (shipDate.after(expiryDate)) {
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.SHIP_DATE_PAST_EXPIRY_DATE,
						errorParameters,
						wrapParameters);
			}

			// Now try adding the present days attribute to the ship date.
			// This should be equal to or later than the expiry date.  If
			// not, give an error.
			try {
				String presentDays = currentTerms.getAttribute("present_docs_within_days");
				String indicator = currentTerms.getAttribute("present_docs_days_user_ind");

				// Calculate the presentation days when it is not set by the user (as indicated by the
				// present_docs_days_user_ind)
				if(StringFunction.isBlank(indicator))
				{
					presentDays = Integer.toString(calculatePresentationDays(shipDate, expiryDate));
					currentTerms.setAttribute("present_docs_within_days", presentDays);
				}

				shipDate.add(Calendar.DATE, Integer.parseInt(presentDays));
				if (shipDate.before(expiryDate))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PRESENT_DATE_BEFORE_EXPIRY,
							errorParameters,
							wrapParameters);
				}
				else if (shipDate.after(expiryDate))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PRESENT_DATE_AFTER_EXPIRY,
							errorParameters,
							wrapParameters);
				}
			} catch (Exception e) {
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PRESENT_DATE_BEFORE_EXPIRY,
						errorParameters,
						wrapParameters);
			}
		// Set Presentation Days to 21 if Shipment Date and Presentation Days are both blank
		} else if (StringFunction.isBlank(shipmentDate)) {
			String presentationDays = currentTerms.getAttribute("present_docs_within_days");
			if (StringFunction.isBlank(presentationDays)) {
				currentTerms.setAttribute("present_docs_within_days", Long.toString(TradePortalConstants.DEFAULT_PRESENTATION_DAYS));
			}
		}

		// If either incoterm or location is provided, both must be given.
		incoterm = shipmentTerms.getAttribute("incoterm");
		location = shipmentTerms.getAttribute("incoterm_location");
		if (!StringFunction.isBlank(incoterm)
				&& StringFunction.isBlank(location)) {
			// Location is missing
			alias = shipmentTerms.getAlias("incoterm_location");
			String[] errorParameters = {alias, shipmentNumDesc};
			boolean[] wrapParameters = {true, false};

			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REQUIRED_ATTRIBUTE,
					errorParameters, wrapParameters);

		}
		if (StringFunction.isBlank(incoterm)
				&& !StringFunction.isBlank(location)) {
			// Incoterm is missing
			alias = shipmentTerms.getAlias("incoterm");
			String[] errorParameters = {alias, shipmentNumDesc};
			boolean[] wrapParameters = {true, false};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REQUIRED_ATTRIBUTE,
					errorParameters, wrapParameters);
		}

		// If incoterm, location and latest shipment date are all blank
		// give a missing information warning for all three.
		if (StringFunction.isBlank(incoterm)
				&& StringFunction.isBlank(location)
				&& StringFunction.isBlank(shipmentDate)) {
			alias = shipmentTerms.getAlias("incoterm_location");
			String[] errorParameters = {alias, shipmentNumDesc};
			boolean[] wrapParameters = {true, false};

			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_INFO_MISSING,
					errorParameters, wrapParameters);
			alias = shipmentTerms.getAlias("incoterm");
			String[] errorParameters2 = {alias, shipmentNumDesc};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_INFO_MISSING,
					errorParameters2, wrapParameters);
			alias = shipmentTerms.getAlias("latest_shipment_date");
			String[] errorParameters3 = {alias, shipmentNumDesc};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_INFO_MISSING,
					errorParameters3, wrapParameters);
		}

		// Give warning if both shipment from fields are blank
		shipFrom = shipmentTerms.getAttribute("shipment_from");
		shipLoad = shipmentTerms.getAttribute("shipment_from_loading");
		if (StringFunction.isBlank(shipFrom) && StringFunction.isBlank(shipLoad)) {
			String[] errorParameters = {shipmentNumDesc};
			boolean[] wrapParameters = {false};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_FROM_MISSING,
					errorParameters, wrapParameters);
		}

		// Give warning if both shipment to fields are blank.
		shipTo = shipmentTerms.getAttribute("shipment_to");
		shipDischarge= shipmentTerms.getAttribute("shipment_to_discharge");
		if (StringFunction.isBlank(shipTo)&& StringFunction.isBlank(shipDischarge)) {
			String[] errorParameters = {shipmentNumDesc};
			boolean[] wrapParameters = {false};
			shipmentTerms.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SHIP_TO_MISSING,
					errorParameters, wrapParameters);
		}

		String freightType = shipmentTerms.getAttribute("trans_doc_marked_freight");
		if (!StringFunction.isBlank(incoterm)) {
			if (StringFunction.isBlank(freightType)) {
				alias = shipmentTerms.getAlias("incoterm");
				String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
				String[] errorParameters = {alias, alias2, shipmentNumDesc};
				boolean[] wrapParameters = {true, true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_SHIP_TERM_COMBO,
						errorParameters, wrapParameters);
			} else {
				if (freightType.equals(TradePortalConstants.COLLECT)) {
					// For COLLECT freight type, EXW, FCA, FAS, and FOB are
					// the only allowed values for the incoterm.  If not one
					// of these, give error.
					if (!( incoterm.equals(TradePortalConstants.SHIPPING_EXW)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FCA)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FAS)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FOB))) {
						alias = shipmentTerms.getAlias("incoterm");
						String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_SHIP_TERM_COMBO,
								errorParameters, wrapParameters);
					}
				} else {
					// For PREPAID freight type, EXW, FCA, FAX, and FOB are
					// NOT allowed for the incoterm.  If one of these values,
					// give error.
					if ( ( incoterm.equals(TradePortalConstants.SHIPPING_EXW)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FCA)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FAS)
							|| incoterm.equals(TradePortalConstants.SHIPPING_FOB))) {
						alias = shipmentTerms.getAlias("incoterm");
						String alias2 = shipmentTerms.getAlias("trans_doc_marked_freight");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_SHIP_TERM_COMBO,
								errorParameters, wrapParameters);
					}
				}
			}
		}

		// Validate the Goods Description and PO Line Items fields
		validateGoodsDescriptionFields(shipmentTerms, shipmentNumDesc);
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms TermsBean - the bean being validated
	 */
	public void validateTextLengths(TermsBean terms)
	throws java.rmi.RemoteException, AmsException
	{
		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);

		if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
		{
			checkTextLength(terms, "additional_conditions", 5000);
			checkTextLength(terms, "addl_doc_text", 1000);
		} 
		checkTextLength(terms, "coms_chrgs_other_text", 1000);
		checkTextLength(terms, "internal_instructions", 1000);
		checkTextLength(terms, "special_bank_instructions", 1000);
		checkTextLength(terms, "comm_invoice_text", 1000);
		checkTextLength(terms, "packing_list_text", 1000);
		checkTextLength(terms, "cert_origin_text", 1000);
		checkTextLength(terms, "ins_policy_text", 1000);
		checkTextLength(terms, "other_req_doc_1_text", 1000);
		checkTextLength(terms, "other_req_doc_2_text", 1000);
		checkTextLength(terms, "other_req_doc_3_text", 1000);
		checkTextLength(terms, "other_req_doc_4_text", 1000);
		checkTextLength(terms, "addl_amounts_covered", 140);
		checkTextLength(terms, "special_tenor_text", 1000);
	}

	/**
	 *  Performs validation of text fields for length.
	 *
	 *  @param terms ShipmentTerms - the shipment terms being validated
	 *  @param shipmentNumDesc String - the shipment number
	 */
	public void validateTextLengths(TermsBean terms, ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws java.rmi.RemoteException, AmsException
	{
	   
		checkTextLength(shipmentTerms, shipmentNumDesc, "trans_addl_doc_text", 1000);
		checkTextLength(shipmentTerms, shipmentNumDesc, "trans_doc_text", 500);
	}

	/**
	 * This method is used to validate the Goods Description and PO Line Items
	 * fields. If nothing has been entered in at least one of these two fields, an
	 * error message will be issued indicating that the Goods Description field is
	 * required (Note: we only use 'Goods Description' here because we can't assume
	 * that the user and user's org have rights to process purchase orders). If
	 * data has been entered in the Goods Description text area box, the method
	 * will issue an error if its length exceeds 6500 characters. If the length
	 * does NOT exceed 6500 characters and data has been enetered in the PO Line
	 * Items text area box, it will then determine whether the combined lengths are
	 * greater than 6500 characters; an appropriate error message will be issued if
	 * this total is greater than 6500.
	 *

	 * @param shipmentNumDesc java.lang.String
	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateGoodsDescriptionFields(ShipmentTerms shipmentTerms, String shipmentNumDesc)
	throws RemoteException, AmsException
	{
		String   goodsDescription = null;
		String   poLineItems      = null;
		String   endText          = null;

		goodsDescription = shipmentTerms.getAttribute("goods_description");
		poLineItems      = shipmentTerms.getAttribute("po_line_items");

		String overrideSwiftLengthInd = getOverrideSwiftLengthInd(shipmentTerms);

		// If nothing has been entered in the Goods Description text area box, determine whether
		// data was entered in the PO Line Items text area box
		if (StringFunction.isBlank(goodsDescription))
		{
			// If nothing was entered in the PO Line Items text area box as well, issue an error
			// indicating that the Goods Description field is required; otherwise, check the
			// number of characters in this field and issue an error if it exceeds 6500
			if (StringFunction.isBlank(poLineItems))
			{
				String[] errorParameters = {shipmentTerms.getAlias("goods_description"), shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REQUIRED_ATTRIBUTE,
						errorParameters, wrapParameters);
			}
			else if ((poLineItems.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				String[] errorParameters = {shipmentNumDesc};
				boolean[] wrapParameters = {false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
						errorParameters, wrapParameters);
			}
		}
		else
		{
			// If the number of characters in the Goods Description text area box is greater
			// than 6500, issue an approrpiate error; otherwise, check the combined lengths of
			// the Goods Description and PO Line Items text area boxes and issue an error if
			// this total exceeds 6500 characters
			if ((goodsDescription.length() > 6500)
					&& (overrideSwiftLengthInd != null)
					&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
			{
				endText = goodsDescription.substring(6480, 6500);

				String[] errorParameters = {shipmentTerms.getAlias("goods_description"), endText, shipmentNumDesc};
				boolean[] wrapParameters = {true, true, false};
				shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TEXT_TOO_LONG_SHOULD_END,
						errorParameters, wrapParameters);
			}
			else
			{
				if ((!StringFunction.isBlank(poLineItems))
						&& ((goodsDescription.length() + poLineItems.length()) > 6500)
						&& (overrideSwiftLengthInd != null)
						&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)))
				{
					String[] errorParameters = {shipmentNumDesc};
					boolean[] wrapParameters = {false};
					shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PO_LINE_ITEMS_EXCEED_MAX_CHARS,
							errorParameters, wrapParameters);
				}
			}
		}
	}



	/**
	 * This method is used to validate the currency Code of the Transaction Amount
	 * with the Currency Code of the PO Line Items.  The two must match.  The PO Line Items
	 * will always have the same currency code as per the PO design validations, therefore, we
	 * only need to determine what that single currency code is and compare it to the currency
	 * code of the transaction.
	 *

	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @exception  java.rmi.RemoteException
	 * @return     void
	 */
	public void validateAmountCurrencyAndPOCurrency(TermsBean terms) throws RemoteException, AmsException
	{
		String   amountCurrency	  = null;

		// Get the currency code of the transaction.  If not blank, proceed with validating that it is
		// the same as the PO line items currency code, otherwise fall through and let the required
		// validation process flag it.
		amountCurrency = terms.getAttribute("amount_currency_code");

		if ((amountCurrency != null) && (!amountCurrency.equals("")))
		{
			// Query the database to get the currency code of the PO line items.
			LOG.debug("Inside validateAmountCurrencyAndPOCurrency()");
			LOG.debug("Getting currency code from po_line_item using sql statement.");

			DocumentHandler    poLineItemCurrencyDoc       = null;
			StringBuffer       sqlQuery                    = null;
			String             poLineItemCurrencyCode 	   = null;
			String 			   termsOid 			   	   = terms.getAttribute("terms_oid");


			//select distinct p.currency
			//from po_line_item p, terms t, transaction r
			//where t.terms_oid = r.c_cust_enter_terms_oid
			//and r.transaction_oid = p.a_assigned_to_trans_oid
			//and t.terms_oid = ####### (termsOid)

			sqlQuery = new StringBuffer();
			sqlQuery.append("select distinct p.currency ");
			sqlQuery.append("from po_line_item p, terms t, transaction r ");
			sqlQuery.append("where t.terms_oid = r.c_cust_enter_terms_oid ");
			sqlQuery.append("and r.transaction_oid = p.a_assigned_to_trans_oid ");
			sqlQuery.append("and t.terms_oid = ?");

			// If value found, compare the PO line item value to the amountCurrency value.
			// If nothing found, then no PO line items have been added and we do not need to continue as there is
			// nothing for us to validate against.
			poLineItemCurrencyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{termsOid});

			if (poLineItemCurrencyDoc != null)
			{
				poLineItemCurrencyCode = poLineItemCurrencyDoc.getAttribute("/ResultSetRecord(0)/CURRENCY");

				if ((poLineItemCurrencyCode != null) && (!poLineItemCurrencyCode.equals("")))
				{
					if (!poLineItemCurrencyCode.equals(amountCurrency))
					{
						terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PO_ITEM_CURR_NOT_SAME_AS_TRANS);
					}
				}
			}
		}
	}



	/**
	 * Performs validation for the transport documents section.
	 *
	 * @param terms com.ams.tradeportal.busobj.ShipmentTerms
	 * @param shipmentNumDesc java.lang.String
	 * @param hasMultipleShipments boolean
	 */
	public void validateTransportDocs(TermsBean terms, ShipmentTerms shipmentTerms, String shipmentNumDesc, boolean hasMultipleShipments)
	throws java.rmi.RemoteException, AmsException {

		String value;
		String alias;

		// If the transport doc checkbox is checked, there are several fields
		// that are required: document type, originals, consignment and
		// marked freight.
		value = shipmentTerms.getAttribute("trans_doc_included");
		if (!StringFunction.isBlank(value) &&
				value.equals(TradePortalConstants.INDICATOR_YES)) {

			String originals = shipmentTerms.getAttribute("trans_doc_originals");
			String docType = shipmentTerms.getAttribute("trans_doc_type");

			if (StringFunction.isBlank(docType)) {
				alias = shipmentTerms.getAlias("trans_doc_type");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}


			if (StringFunction.isBlank(originals)) {
				alias = shipmentTerms.getAlias("trans_doc_originals");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_consign_type"))) {
				alias = shipmentTerms.getAlias("trans_doc_consign_type");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_doc_marked_freight"))) {
				alias = shipmentTerms.getAlias("trans_doc_marked_freight");
				String[] errorParameters = {alias, shipmentNumDesc};
				boolean[] wrapParameters = {true, false};
				shipmentTerms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANS_DOC_INFO_MISSING,
						errorParameters, wrapParameters);
			}

			// For the doctype/originals combination: 1/3, 2/3, 3/3, and full set can
			// only match up with marine/ocean or multi-modal (and vice versa).
			if (originals != null && docType != null) {
				if (originals.equals(TradePortalConstants.ONE_UPON_3)
						|| originals.equals(TradePortalConstants.TWO_UPON_3)
						|| originals.equals(TradePortalConstants.THREE_UPON_3)
						|| originals.equals(TradePortalConstants.FULL_SET)) {
					// The doc type MUST be marine/ocean or multi-modal
					if (!(docType.equals(TradePortalConstants.MARINE_OCEAN)
							|| docType.equals(TradePortalConstants.MULTI_MODAL))) {
						alias = shipmentTerms.getAlias("trans_doc_type");
						String alias2 = shipmentTerms.getAlias("trans_doc_originals");
						String[] errorParameters = {alias, alias2, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.TRANS_DOC_INVALID_COMBO,
								errorParameters, wrapParameters);
					}

				}
			}

			// For airway transport doc type, the consignment type must be
			// "consigned to party".  If it is "consigned to order of"
			// it is bad.
			if ((docType != null)
					&& (docType.equals(TradePortalConstants.AIR_TRANSPORT))) {

				String type = shipmentTerms.getAttribute("trans_doc_consign_type");
				if (type.equals(TradePortalConstants.CONSIGN_ORDER)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String alias2 = shipmentTerms.getAlias("trans_doc_type");
					String[] errorParameters = {alias, alias2, shipmentNumDesc};
					boolean[] wrapParameters = {true, true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.TRANS_DOC_INVALID_COMBO,
							errorParameters, wrapParameters);
				}
			}

		} else {
			// Checkbox is unchecked.  Blank out all the fields that are
			// associated with this checkbox.
			shipmentTerms.setAttribute("trans_doc_type", "");
			shipmentTerms.setAttribute("trans_doc_originals", "");
			shipmentTerms.setAttribute("trans_doc_copies", "");
			shipmentTerms.setAttribute("trans_doc_consign_type", "");
			shipmentTerms.setAttribute("trans_doc_consign_order_of_pty", "");
			shipmentTerms.setAttribute("trans_doc_consign_to_pty", "");
			shipmentTerms.setAttribute("trans_doc_marked_freight", "");
			shipmentTerms.setAttribute("trans_doc_text", "");

			removeTermsParty(shipmentTerms, "NotifyParty");
			removeTermsParty(shipmentTerms, "OtherConsigneeParty");

		}

		// If the consignment type is not blank (error caught above), perform
		// some validation: for "Consign the the order of", the party is
		// required.  For "Consign to", the party is required.  If the party
		// is Other, at least one party field is required.  SHP is not a valid
		// party type for "consign to".
		value = shipmentTerms.getAttribute("trans_doc_consign_type");
		if (!StringFunction.isBlank(value)) {
			if (value.equals(TradePortalConstants.CONSIGN_ORDER)) {
				// The "Consigned to the order of" option is selected
				shipmentTerms.setAttribute("trans_doc_consign_to_pty", "");
				String party = shipmentTerms.getAttribute("trans_doc_consign_order_of_pty");
				if (StringFunction.isBlank(party)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String[] errorParameters = {alias, shipmentNumDesc};
					boolean[] wrapParameters = {true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PARTY_MISSING,
							errorParameters, wrapParameters);
				} else if (party.equals(TradePortalConstants.CONSIGN_OTHER)) {
					checkOtherConsignee(shipmentTerms, shipmentNumDesc);
				}
				else
				{
					if (this.hasAddressInfo(shipmentTerms,"OtherConsigneeParty"))
					{
						value = terms.getResourceManager().getText("RequestAdviseIssue.OtherConsignee",
								TradePortalConstants.TEXT_BUNDLE);
						alias = shipmentTerms.getAlias("trans_doc_consign_order_of_pty");
						String[] errorParameters = {value, alias, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CONSIGN_TO_PARTY_DATA,
								errorParameters, wrapParameters);
					}
				}
			} else {
				// The "Consigned to" option is selected
				shipmentTerms.setAttribute("trans_doc_consign_order_of_pty", "");
				String party = shipmentTerms.getAttribute("trans_doc_consign_to_pty");
				if (StringFunction.isBlank(party)) {
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String[] errorParameters = {alias, shipmentNumDesc};
					boolean[] wrapParameters = {true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.PARTY_MISSING,
							errorParameters, wrapParameters);
				} else if (party.equals(TradePortalConstants.CONSIGN_OTHER)) {
					// At least one field is required for OTHER.
					checkOtherConsignee(shipmentTerms, shipmentNumDesc);
				} else if (party.equals(TradePortalConstants.CONSIGN_SHIPPER)) {
					// SHP type not allowed for Consigned To.
					alias = shipmentTerms.getAlias("trans_doc_consign_type");
					String alias2 = shipmentTerms.getAlias("trans_doc_consign_to_pty");
					String[] errorParameters = {alias, alias2, shipmentNumDesc};
					boolean[] wrapParameters = {true, true, false};
					shipmentTerms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.TRANS_DOC_INVALID_COMBO,
							errorParameters, wrapParameters);
				}
				else
				{
					if (this.hasAddressInfo(shipmentTerms,"OtherConsigneeParty"))
					{
						value = terms.getResourceManager().getText("RequestAdviseIssue.OtherConsignee",
								TradePortalConstants.TEXT_BUNDLE);
						alias = shipmentTerms.getAlias("trans_doc_consign_to_pty");
						String[] errorParameters = {value, alias, shipmentNumDesc};
						boolean[] wrapParameters = {true, true, false};
						shipmentTerms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVALID_CONSIGN_TO_PARTY_DATA,
								errorParameters, wrapParameters);
					}
				}
			}
		}

		// If the addl doc indicator is checked and there is no text, then
		// reset the indicator to N.  Conversely if there is text but the
		// indicator is unchecked, do not save the text.
		value = shipmentTerms.getAttribute("trans_addl_doc_included");
		if (StringFunction.isBlank(value) ||
				value.equals(TradePortalConstants.INDICATOR_NO)) {
			shipmentTerms.setAttribute("trans_addl_doc_text", "");
		} else {
			if (StringFunction.isBlank(shipmentTerms.getAttribute("trans_addl_doc_text"))) {
				shipmentTerms.setAttribute("trans_addl_doc_included",
						TradePortalConstants.INDICATOR_NO);
			}
			else
			{
				if (hasMultipleShipments)
				{
					shipmentTerms.setAttribute("trans_addl_doc_included", TradePortalConstants.INDICATOR_NO);
					shipmentTerms.setAttribute("trans_addl_doc_text", "");
				}
			}
		}
	}

	//Beginning of code used by the Middleware team
	//Beginning of getManagerTerms, coded by Indu Valavala
	/**
	 *  gets the values of the attributes of the managed object. This method is called
	 *  from the getTermsAttributesDoc() hook in the managed object.
	 *
	 *  return com.amsinc.ecsg.util.DocumentHandler
	 *  @param terms TermsBean - the bean being validated
	 *  @param termsDoc com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());
		LOG.debug("This is the termValuesDoc containing all the terms values: {}",termsValuesDoc.toString());



		//Terms_Details
		/*Change the below getAttribute to get the values from termsValuesDoc after checking out how it looks like*/
		termsDoc.setAttribute("/Terms/TermsDetails/Irrevocable",	                terms.getAttribute("irrevocable"));
		termsDoc.setAttribute("/Terms/TermsDetails/Transferrable",		        terms.getAttribute("transferrable"));
		termsDoc.setAttribute("/Terms/TermsDetails/Operative",       	        terms.getAttribute("operative"));
		termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", 	       		terms.getAttribute("place_of_expiry"));
		termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate",					terms.getAttribute("expiry_date"));
		termsDoc.setAttribute("/Terms/TermsDetails/ReferenceNumber",	            terms.getAttribute("reference_number"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountCurrencyCode",          terms.getAttribute("amount_currency_code"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount",                      terms.getAttribute("amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",	    terms.getAttribute("amt_tolerance_pos"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",  	terms.getAttribute("amt_tolerance_neg"));
		termsDoc.setAttribute("/Terms/TermsDetails/MaximumCreditAmount",  	terms.getAttribute("maximum_credit_amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/IssuersReferenceNumber",  	terms.getAttribute("issuer_ref_num"));
		termsDoc.setAttribute("/Terms/TermsDetails/PurposeType",		            terms.getAttribute("purpose_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/DraftsRequired",				terms.getAttribute("drafts_required"));
		termsDoc.setAttribute("/Terms/TermsDetails/AvailableBy",		            terms.getAttribute("payment_type"));
 		termsDoc.setAttribute("/Terms/TermsDetails/AvailableWithParty",          terms.getAttribute("available_with_party"));
		termsDoc.setAttribute("/Terms/TermsDetails/DrawnOnParty",		        terms.getAttribute("drawn_on_party"));
		termsDoc.setAttribute("/Terms/TermsDetails/UcpVersion",				    terms.getAttribute("ucp_version"));
		termsDoc.setAttribute("/Terms/TermsDetails/UcpDetails",					terms.getAttribute("ucp_details"));
		termsDoc.setAttribute("/Terms/TermsDetails/UcpIndicator",               terms.getAttribute("ucp_version_details_ind"));
		termsDoc.setAttribute("/Terms/TermsDetails/PresentDocumentsWithinDays", terms.getAttribute("present_docs_within_days"));
		termsDoc.setAttribute("/Terms/TermsDetails/InsuranceRiskType", terms.getAttribute("insurance_risk_type"));
		termsDoc.setAttribute("/Terms/TermsDetails/InsuranceEndorseValuePlus", terms.getAttribute("insurance_endorse_value_plus"));
		termsDoc.setAttribute("/Terms/TermsDetails/PartialShipmentAllowed", terms.getAttribute("partial_shipment_allowed"));
		//End of Terms_Details

		//Docs_Required

		int numberOfDocsReq    = 8;
		int drid               = 0;
		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));

		String [] indicator        = {"comm_invoice_indicator","packing_list_indicator", "ins_policy_indicator",
				"cert_origin_indicator", "other_req_doc_1_indicator","other_req_doc_2_indicator",
				"other_req_doc_3_indicator","other_req_doc_4_indicator"};
		String [] DocumentName     = {"Commercial Invoice","Packing List","Insurance Policy/Certificate",
				"Certificate Of Origin","other_req_doc_1_name","other_req_doc_2_name",
				"other_req_doc_3_name", "other_req_doc_4_name"};
		String [] NumberOfOrginals = {"comm_invoice_originals","packing_list_originals","ins_policy_originals",
				"cert_origin_originals" ,"other_req_doc_1_originals","other_req_doc_2_originals",
				"other_req_doc_3_originals","other_req_doc_4_originals"};
		String [] NumberOfCopies   = {"comm_invoice_copies","packing_list_copies","ins_policy_copies",
				"cert_origin_copies" ,"other_req_doc_1_copies","other_req_doc_2_copies",
				"other_req_doc_3_copies","other_req_doc_4_copies"};
		String [] DocumentText     = {"comm_invoice_text","packing_list_text","ins_policy_text",
				"cert_origin_text" ,"other_req_doc_1_text","other_req_doc_2_text",
				"other_req_doc_3_text","other_req_doc_4_text"};
		termsDoc.removeComponent("/Terms/DocumentsRequired/FullDocumentRequired");

		for (int docIndex=0; docIndex<numberOfDocsReq; docIndex++)
		{
			if(terms.getAttribute(indicator[docIndex]).equals("Y"))
			{
				if(docIndex < 4)
					termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			DocumentName[docIndex]);
				else
					termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			terms.getAttribute(DocumentName[docIndex]));

				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			terms.getAttribute(DocumentText[docIndex]));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		terms.getAttribute(NumberOfCopies[docIndex]));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		terms.getAttribute(NumberOfOrginals[docIndex]));

				drid ++;

			}
		}
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
		//Docs_Required
		//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- Start
		List addReqDocSortedList = new ArrayList();
		ComponentList additionalReqDocList = (ComponentList) terms.getComponentHandle("AdditionalReqDocList");
		// Sort the list of additional documents so they are placed in the order they were created
		addReqDocSortedList = sortAddlReqDocs(additionalReqDocList);
		int addRedDocCount = addReqDocSortedList.size();
		for (int addReqDocIndex = 0 ; addReqDocIndex < addRedDocCount ; addReqDocIndex++){
			AdditionalReqDoc additionalReqDoc = (AdditionalReqDoc)additionalReqDocList.getComponentObject(Long.parseLong(addReqDocSortedList.get(addReqDocIndex).toString()));
			String value = additionalReqDoc.getAttribute("addl_req_doc_ind");
			if("Y".equalsIgnoreCase(value))
			{
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName", additionalReqDoc.getAttribute("addl_req_doc_name"));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText", additionalReqDoc.getAttribute("addl_req_doc_text"));
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",	additionalReqDoc.getAttribute("addl_req_doc_copies"));//CQ T36000011047. IValavala fixed originals/copies getting switched.
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals", additionalReqDoc.getAttribute("addl_req_doc_originals"));
	
				drid ++;
			}
		}
		//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- End
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
		//VS - IR#T36000021272 - 09/30/2013 - <BEGIN> - TPL message that had Additional Doc Information box checked, which sent over blank FullDocumentRequired tags caused Timekeeper to crash.
		//Modify the below "if" condition and rewrite it accordingly.
		//if(terms.getAttribute("addl_doc_indicator").equals("Y"))
		if(terms.getAttribute("addl_doc_indicator").equals("Y") && StringFunction.isNotBlank(terms.getAttribute("addl_doc_text")) )
		//VS - IR#T36000021272 - 09/30/2013 - <END>
		{
			String addlDocText = terms.getAttribute("addl_doc_text");
			if(addlDocText.length()>30)
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			addlDocText.substring(0,29));
			else
				termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentName",			addlDocText);

			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/DocumentText",			addlDocText);
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfCopies",		"");
			termsDoc.setAttribute("/Terms/DocumentsRequired/FullDocumentRequired(" + drid + ")/NumberOfOriginals",		"");
			drid++;
		}

		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));
		termsDoc.setAttribute("/Terms/DocumentsRequired/TotalNumberOfEntries", String.valueOf(drid));


		// Transport Documents/Shipment Section
		Vector shipmentTermsSorted = new Vector();
		ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

		// Sort the list of shipment terms so they are placed in the order they were created
		shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);
		int shipmentCount = shipmentTermsSorted.size();

		termsDoc.removeComponent("/Terms/TransportDocuments/FullTransportDocuments");
		termsDoc.removeComponent("/Terms/ShipmentTerms/FullShipmentTerms");

		for (int shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
		{
			String shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
			ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
			//Rpasupulati IR T36000045226 Start
			String nextLine = "";			
			if(StringFunction.isNotBlank(shipmentTerms.getAttribute("goods_description"))&&StringFunction.isNotBlank(shipmentTerms.getAttribute("po_line_items"))){
				nextLine = "\n";
			}
			//Rpasupulati IR T36000045226 Ends
			//TransportDocuments
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentOriginals", shipmentTerms.getAttribute("trans_doc_originals"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentCopies", shipmentTerms.getAttribute("trans_doc_copies"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentType", shipmentTerms.getAttribute("trans_doc_type"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignType", shipmentTerms.getAttribute("trans_doc_consign_type"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignOrderOfParty", shipmentTerms.getAttribute("trans_doc_consign_order_of_pty"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentConsignOrderToParty", shipmentTerms.getAttribute("trans_doc_consign_to_pty"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentMarkedFreight", shipmentTerms.getAttribute("trans_doc_marked_freight"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentText", shipmentTerms.getAttribute("trans_doc_text"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportAdditionalDocumentText", shipmentTerms.getAttribute("trans_addl_doc_text"));
			termsDoc.setAttribute("/Terms/TransportDocuments/FullTransportDocuments(" + shipmentIndex + ")/TransportDocumentShipmentDescription", shipmentTerms.getAttribute("description"));
			//End of TransportDocuments

			//ShipmentTerms
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/TransshipmentAllowed", shipmentTerms.getAttribute("transshipment_allowed"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/LatestShipmentDate", shipmentTerms.getAttribute("latest_shipment_date"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/Incoterm", shipmentTerms.getAttribute("incoterm"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/IncotermLocation", shipmentTerms.getAttribute("incoterm_location"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipmentFrom", shipmentTerms.getAttribute("shipment_from"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipmentTo", shipmentTerms.getAttribute("shipment_to"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/GoodsDescription", shipmentTerms.getAttribute("goods_description") + nextLine + shipmentTerms.getAttribute("po_line_items"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/POLineItems", shipmentTerms.getAttribute("po_line_items"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/AirWaybillNumber", shipmentTerms.getAttribute("air_waybill_num"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/VesselName", shipmentTerms.getAttribute("vessel_name_voyage_num"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/TransportTermsText", "");
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ContainerNumber", shipmentTerms.getAttribute("container_number"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/BillOfLadingNumber", shipmentTerms.getAttribute("bill_of_lading_num"));

	   		//iv Adding ShipmentFrom and ShipmentTo

			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipFromLoading", shipmentTerms.getAttribute("shipment_from_loading"));
			termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/ShipToDischarge", shipmentTerms.getAttribute("shipment_to_discharge"));

			//End of ShipmentTerms

			// ShipmentTerms Parties
			String c_NotifyParty             = shipmentTerms.getAttribute("c_NotifyParty");
			String c_OtherConsigneeParty      = shipmentTerms.getAttribute("c_OtherConsigneeParty");
			String [] shipmentTermsPartyOID  = {c_NotifyParty ,c_OtherConsigneeParty};
			String [] shipmentTermsPartyColl = {"NotifyParty","OtherConsigneeParty"};
			String [] partyType = {TradePortalConstants.SHIPMENT_NOTIFY_PARTY,
					TradePortalConstants.SHIPMENT_CONSIGNEE_PARTY};

			for (int i = 0; i<partyType.length;i++)
			{
				if(!StringFunction.isBlank(shipmentTermsPartyOID[i]))
				{
					TermsParty termsParty = (TermsParty)(shipmentTerms.getComponentHandle(shipmentTermsPartyColl[i]));
					termsDoc = termsParty.packageShipmentTermsPartyAttributes(shipmentIndex, partyType[i], termsDoc);
				}
				else
				{
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"BusinessName", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine1", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine2", "");
					termsDoc.setAttribute("/Terms/ShipmentTerms/FullShipmentTerms(" + shipmentIndex + ")/"+partyType[i]+"AddressLine3", "");
				}
			}
		}
		termsDoc.setAttribute("/Terms/ShipmentTerms/TotalNumberOfEntries",String.valueOf(shipmentCount));
		termsDoc.setAttribute("/Terms/TransportDocuments/TotalNumberOfEntries",String.valueOf(shipmentCount));
		// End Transport Documents/Shipment Section

		//???       //Instructions
		//Instructions_to_Bank
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",       termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		//End of Instructions_to_Bank

		//Settlement_Instructions
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/BranchCode",				        termsValuesDoc.getAttribute("/Terms/branch_code"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementOurAccountNumber",       termsValuesDoc.getAttribute("/Terms/settlement_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountNumber",    termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/SettlementInstructions/SettlementForeignAccountCurrency", termsValuesDoc.getAttribute("/Terms/settlement_foreign_acct_curr"));
		//End of Settlement_Instructions
		//???       //End of Instructions


		//Commission_and_Charges
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOurAccountNumber",      termsValuesDoc.getAttribute("/Terms/coms_chrgs_our_account_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountNumber",  termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_num"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesForeignAccountCurrency",termsValuesDoc.getAttribute("/Terms/coms_chrgs_foreign_acct_curr"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CommissionChargesOtherText",             termsValuesDoc.getAttribute("/Terms/coms_chrgs_other_text"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawing",                         termsValuesDoc.getAttribute("/Terms/finance_drawing"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingNumberOfDays",             termsValuesDoc.getAttribute("/Terms/finance_drawing_num_days"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FinanceDrawingType",                     termsValuesDoc.getAttribute("/Terms/finance_drawing_type"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/CoveredByFECNumber",                     termsValuesDoc.getAttribute("/Terms/covered_by_fec_number"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECAmount",                              termsValuesDoc.getAttribute("/Terms/fec_amount"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECRate",                                termsValuesDoc.getAttribute("/Terms/fec_rate"));
		termsDoc.setAttribute("/Terms/Instructions/CommissionAndCharges/FECMaturityDate",                        termsValuesDoc.getAttribute("/Terms/fec_maturity_date"));
		//End of Commission_and_Charges



		//Payment_Terms
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsType",              termsValuesDoc.getAttribute("/Terms/pmt_terms_type"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsNumberDaysAfter",   termsValuesDoc.getAttribute("/Terms/pmt_terms_num_days_after"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsDaysAfterType",     termsValuesDoc.getAttribute("/Terms/pmt_terms_days_after_type"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsPercentInvoice",    termsValuesDoc.getAttribute("/Terms/pmt_terms_percent_invoice"));
		//End of Payment_Terms


		//Bank_Charges
		termsDoc.setAttribute("/Terms/BankCharges/BankChargesType",   termsValuesDoc.getAttribute("/Terms/bank_charges_type"));
		//End of Bank_Charges


		//Other_Conditions
		termsDoc.setAttribute("/Terms/OtherConditions/ConfirmationType",         termsValuesDoc.getAttribute("/Terms/confirmation_type"));
		// termsDoc.setAttribute("/Terms/OtherConditions/Transferrable",            termsValuesDoc.getAttribute("/Terms/transferrable"));
		termsDoc.setAttribute("/Terms/OtherConditions/Revolve",                  termsValuesDoc.getAttribute("/Terms/revolve"));
		termsDoc.setAttribute("/Terms/OtherConditions/AdditionalConditions",     termsValuesDoc.getAttribute("/Terms/additional_conditions"));
		//End of Other_Conditions

		termsDoc.setAttribute("/Terms/AdditionalDocumentText",     termsValuesDoc.getAttribute("/Terms/addl_doc_text"));
		termsDoc.setAttribute("/Terms/AdditionalAmountsCovered",     termsValuesDoc.getAttribute("/Terms/addl_amounts_covered"));

		//Terms Party

		DocumentHandler termsPartyDoc = new DocumentHandler();

		//Additional Terms Party
		int id = 0;

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries","0");

		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES;i++)
		{
			String termsPartyOID = termsValuesDoc.getAttribute("/Terms/"+TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]);
			if(!(termsPartyOID == null || termsPartyOID.equals("")))
			{
				TermsParty termsParty = (TermsParty)(terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]));
				if(termsParty.getAttribute("terms_party_type").equals(TermsPartyType.ISSUING_BANK))
				{
					String corporateOrgTypeCode = getCorporateOrgTypeCode(terms);
					if((corporateOrgTypeCode != null)
							&& (corporateOrgTypeCode.equals(TradePortalConstants.PARTY_TYPE_BANK)))
					{
						termsParty.setAttribute("terms_party_type", TermsPartyType.OPENING_BANK);
					}
				}

				termsPartyDoc.setAttribute("/AdditionalTermsParty","N");
				//If the termsValuesDoc contains termsParty also need to modify the line below
				termsPartyDoc = termsParty.packageTermsPartyAttributes(id,termsPartyDoc);
				id++;
			}
		}

		LOG.debug("This is TermsPartyDoc: {} " ,termsPartyDoc.toString());

		termsPartyDoc.setAttribute("/TermsParty/TotalNumberOfEntries",String.valueOf(id));
		termsDoc.setComponent("/TermsParty",termsPartyDoc.getFragment("/TermsParty"));

		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	    termsDoc.setAttribute("/Terms/PaymentTerms/TenorSpecialText", terms.getAttribute("special_tenor_text"));
		ComponentList pmtTermsTenorDtlList = (ComponentList) terms.getComponentHandle("PmtTermsTenorDtlList");
		int pmtTermsCount = pmtTermsTenorDtlList.getObjectCount();
		int pmtTermsIndex = 0;
		for ( pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
			pmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Amount", pmtTermsTenorDtlList.getListAttribute("amount"));
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Percent", pmtTermsTenorDtlList.getListAttribute("percent"));
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/Days", pmtTermsTenorDtlList.getListAttribute("num_days_after"));
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/DateMaturity", pmtTermsTenorDtlList.getListAttribute("maturity_date"));
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/TenorType",	pmtTermsTenorDtlList.getListAttribute("tenor_type"));
			termsDoc.setAttribute("/Terms/PaymentTerms/FullPaymentTerms(" + pmtTermsIndex + ")/TenorDetailType",	pmtTermsTenorDtlList.getListAttribute("days_after_type"));
		}
		termsDoc.setAttribute("/Terms/PaymentTerms/TotalNumberOfEntries", String.valueOf(pmtTermsIndex));
		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End



		return termsDoc;
	}

	//End of GetManagerTerms Method

	/**
	 * Validates the fields that can only have a certain number of decimal places
	 *
	 */
	public void validateDecimalFields(TermsBean terms) throws AmsException, RemoteException
	{
		// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("fec_rate"),
				5,
				8,
				"TermsBeanAlias.fec_rate",
				terms.getErrorManager(),
				terms.getResourceManager());


		// Validate that the rate field will fit in the database
		InstrumentServices.validateDecimalNumber(terms.getAttribute("insurance_endorse_value_plus"),
				3,
				1,
				"TermsBeanAlias.insurance_endorse_value_plus",
				terms.getErrorManager(),
				terms.getResourceManager());
	}

	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
	 /* Performs validation for the Other Conditions section.
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 */
	public void validateOtherCondtions(TermsBean terms) throws java.rmi.RemoteException, AmsException
	{
		String value = terms.getAttribute("ucp_version_details_ind");
		
		if (StringFunction.isNotBlank(value) && TradePortalConstants.INDICATOR_YES.equals(value)) {
			value = terms.getAttribute("ucp_version");
			
			if(StringFunction.isBlank(value)){
				terms.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE,
						terms.getResourceManager().getText( "common.ICCVersionRequired", TradePortalConstants.TEXT_BUNDLE)
						);
			}
			
			if(StringFunction.isNotBlank(value) && TradePortalConstants.RQA_OTHER.equals(value)){
				value = terms.getAttribute("ucp_details");
				if(StringFunction.isBlank(value)){
					terms.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ICC_PUBLICATION_DETAILS_CHECK);
				}
			}
		}
	}
	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End
	
	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		try {
			validateAmount(terms, "amount", "amount_currency_code");
			validateAmount(terms, "fec_amount", "amount_currency_code");

			validateTextLengths(terms);

			validateDecimalFields(terms);

			// Validate the shipment terms text fields
			String        shipmentNumber;
			StringBuffer  shipmentNumDesc;
			int           shipmentCount;
			int           shipmentIndex;
			String        shipmentOid;
			Vector        shipmentTermsSorted = new Vector();
			ComponentList shipmentTermsList;
			ShipmentTerms shipmentTerms;

			// Get a handle to the term's shipment terms components
			shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");

			// Sort the list of shipment terms so they are placed in the order they were created
			shipmentTermsSorted = sortShipmentTerms(shipmentTermsList);

			shipmentCount = shipmentTermsSorted.size();
			// Validate each set of shipment terms
			for (shipmentIndex = 0; shipmentIndex < shipmentCount; shipmentIndex++)
			{
				shipmentOid   = (String) shipmentTermsSorted.elementAt(shipmentIndex);
				shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(Long.parseLong(shipmentOid));
				shipmentNumDesc = new StringBuffer();

				// If there are multiple shipments, validate any required attributes
				if (shipmentCount > 1)
				{
					shipmentNumber = String.valueOf(shipmentIndex + 1);
					shipmentNumDesc.append(terms.getResourceManager().getText("RequestAdviseIssue.Shipment",
							TradePortalConstants.TEXT_BUNDLE));
					shipmentNumDesc.append(" ");
					shipmentNumDesc.append(shipmentNumber + " -");
				}
				else
				{
					shipmentNumDesc.append(terms.getResourceManager().getText("RequestAdviseIssue.Shipment",
							TradePortalConstants.TEXT_BUNDLE));
					shipmentNumDesc.append(" -");
				}

				// Perform subclass-specific validation of text fields for SWIFT characters
				validateForSwiftCharacters(shipmentTerms);

				validateTextLengths(terms, shipmentTerms, shipmentNumDesc.toString());
			}
		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in RQA__ISSTermsMgr.preSave()",e);
		}

	}

	/**
	 * This method retrieves the term's collection of shipment terms and
	 * sorts them by creation date
	 *
	 * @param terms com.ams.tradeportal.busobj.TermsBean
	 * @exception com.amsinc.ecsg.frame.AmsException The exception description.
	 */
	public Vector sortShipmentTerms(ComponentList shipmentTermsList) throws AmsException, RemoteException
	{
		boolean          dateSorted;
		Date             formattedDate;
		Date             compareDate;
		String           creationDate;
		String           shipmentOid;
		ShipmentTerms    shipmentTerms;
		Vector           shipTermsSorted = new Vector();
		Vector           shipmentDates   = new Vector();

		try {

			SimpleDateFormat dateFormatter   = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

			for (int index=0; index< shipmentTermsList.getObjectCount(); index++)
			{
				shipmentTermsList.scrollToObjectByIndex(index);
				shipmentOid   = shipmentTermsList.getListAttribute("shipment_oid");
				creationDate  = shipmentTermsList.getListAttribute("creation_date");
				formattedDate = dateFormatter.parse(creationDate);

				dateSorted = false;

				for (int i=0; i< shipmentDates.size(); i++)
				{
					compareDate = (Date) shipmentDates.elementAt(i);
					if (formattedDate.before(compareDate))
					{
						shipmentDates.insertElementAt(formattedDate, i);
						shipTermsSorted.insertElementAt(shipmentOid, i);
						dateSorted = true;
						break;
					}
				}

				if (!dateSorted)
				{
					shipmentDates.addElement(formattedDate);
					shipTermsSorted.addElement(shipmentOid);
				}
			}

		} catch (RemoteException e) {
			LOG.error("Remote Exception caught in RQA__ISSTermsMgr.sortShipmentTerms()",e);

		} catch (ParseException e) {
			LOG.error("Parse Exception caught in RQA__ISSTermsMgr.sortShipmentTerms()",e);
			throw new AmsException("Date parse error caught in RQA__ISSTermsMgr.sortShipmentTerms()");
		}

		return shipTermsSorted;
	}
	//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- Start
	public List sortAddlReqDocs(ComponentList additionalReqDocList) throws AmsException, RemoteException
	{
		String           addlReqDocOid;
		List           addlReqDocSortedList = new ArrayList();
		int addRedDocCount = additionalReqDocList.getObjectCount();
		for (int i =0 ; i < addRedDocCount ; i++)
		{
			additionalReqDocList.scrollToObjectByIndex(i);
			addlReqDocOid   = additionalReqDocList.getListAttribute("addl_req_doc_oid");
			addlReqDocSortedList.add(addlReqDocOid);
		}
		Collections.sort(addlReqDocSortedList);
		return addlReqDocSortedList;
	}
	//Leelavathi - IR#T36000015151 - Rel8200 CR-737 26/06/2013- End
}
