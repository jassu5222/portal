package com.ams.tradeportal.busobj.termsmgr;

import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class manages the dynamic attribute registration and validation for a Terms business object or TermsWebBean for a Standby
 * Letter of Credit Amendment
 *
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */
public class SLC__AMDTermsMgr extends TermsMgr {
	private static final Logger LOG = LoggerFactory.getLogger(SLC__AMDTermsMgr.class);

	/**
	 * Constructor that is used by business objects to create their manager. This method also register the dynamic attributes of the
	 * bean.
	 *
	 * @param mgr
	 *            AttributeManager attribute manager of the bean being managed. Used to register attributes
	 */
	public SLC__AMDTermsMgr(AttributeManager mgr) throws AmsException {
		super(mgr);
	}

	/**
	 * Constructor that is used by web beans to create their manager. This method also register the dynamic attributes of the bean.
	 *
	 * @param mgr
	 *            TermsWebBean - the bean being managed. Used to register attributes.
	 */
	public SLC__AMDTermsMgr(TermsWebBean terms) throws AmsException {
		super(terms);
	}

	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for an this instrument and transaction type
	 *
	 * @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister() {
		List<Attribute> attrList = super.getAttributesToRegister();
		attrList.add(TermsBean.Attributes.create_amendment_details());
		attrList.add(TermsBean.Attributes.create_amount());
		attrList.add(TermsBean.Attributes.create_amount_currency_code());
		attrList.add(TermsBean.Attributes.create_amt_tolerance_neg());
		attrList.add(TermsBean.Attributes.create_amt_tolerance_pos());
		attrList.add(TermsBean.Attributes.create_expiry_date());
		attrList.add(TermsBean.Attributes.create_place_of_expiry());
		attrList.add(TermsBean.Attributes.create_reference_number());
		attrList.add(TermsBean.Attributes.create_special_bank_instructions());
		attrList.add(TermsBean.Attributes.create_work_item_number());
		attrList.add(TermsBean.Attributes.create_characteristic_type());
		attrList.add(TermsBean.Attributes.create_pmt_terms_type());

		return attrList;
	}

	/**
	 * Performs the Terms Manager-specific validation of text fields in the Terms and TermsParty objects.
	 *
	 * @param terms
	 *            - the managed object
	 *
	 */
	protected void validateForSwiftCharacters(TermsBean terms) throws AmsException, RemoteException {

	}

	/**
	 * Performs presave processing (regardless of whether validation is done)
	 *
	 * @param terms
	 *            com.ams.tradeportal.busobj.termsBean
	 * @exception com.amsinc.ecsg.frame.AmsException
	 *                The exception description.
	 */
	public void preSave(TermsBean terms) throws AmsException {
		super.preSave(terms);

		// Check if amount/currency combination has valid precision
		try {
			validateTextLengths(terms); // IR T36000048753 Rel9.5
			validateAmount(terms, "amount", "amount_currency_code");
		} catch (java.rmi.RemoteException e) {
			LOG.error("Remote Exception caught in IMP_DLC__AMDTermsMgr.preSave()",e);
		}

	}

	/**
	 * Performs validation of the attributes of this type of instrument and transaction. This method is called from the
	 * userValidate() hook in the managed object.
	 *
	 * @param terms
	 *            - the bean being validated
	 */
	public void validate(TermsBean terms) throws java.rmi.RemoteException, AmsException {
		super.validate(terms);

		// If all of these fields are blank, issue an error
		if (StringFunction.isBlank(terms.getAttribute("amount")) && StringFunction.isBlank(terms.getAttribute("amt_tolerance_neg"))
				&& StringFunction.isBlank(terms.getAttribute("amt_tolerance_pos"))
				&& StringFunction.isBlank(terms.getAttribute("expiry_date"))
				&& StringFunction.isBlank(terms.getAttribute("special_bank_instructions"))
				&& StringFunction.isBlank(terms.getAttribute("amendment_details"))) {
			terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MUST_AMEND);
		}

	}

	// Beginning of code used by the Middleware team
	/**
	 * gets the values of the attributes of the managed object. This method is called from the getTermsAttributesDoc() hook in the
	 * managed object.
	 *
	 * return com.amsinc.ecsg.util.DocumentHandler
	 * 
	 * @param terms
	 *            TermsBean - the bean being validated
	 * @param termsDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 */
	public DocumentHandler getManagerTerms(TermsBean terms, DocumentHandler termsDoc) throws java.rmi.RemoteException, AmsException

	{

		DocumentHandler termsValuesDoc = terms.populateXmlDoc(new DocumentHandler());

		// Terms_Details
		termsDoc.setAttribute("/Terms/TermsDetails/PlaceOfExpiry", termsValuesDoc.getAttribute("/Terms/place_of_expiry"));
		termsDoc.setAttribute("/Terms/TermsDetails/ExpiryDate", termsValuesDoc.getAttribute("/Terms/expiry_date"));
		termsDoc.setAttribute("/Terms/TermsDetails/Amount", termsValuesDoc.getAttribute("/Terms/amount"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountTolerancePositive",
				termsValuesDoc.getAttribute("/Terms/amt_tolerance_pos"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmountToleranceNegative",
				termsValuesDoc.getAttribute("/Terms/amt_tolerance_neg"));
		termsDoc.setAttribute("/Terms/TermsDetails/AmendmentDetails", termsValuesDoc.getAttribute("/Terms/amendment_details"));
		termsDoc.setAttribute("/Terms/TermsDetails/CharacteristicType", termsValuesDoc.getAttribute("/Terms/characteristic_type"));
		termsDoc.setAttribute("/Terms/PaymentTerms/PaymentTermsType", termsValuesDoc.getAttribute("/Terms/pmt_terms_type"));
		// End of Terms_Details

		// Instructions
		// Instructions_to_Bank
		termsDoc.setAttribute("/Terms/Instructions/InstructionsToBank",
				termsValuesDoc.getAttribute("/Terms/special_bank_instructions"));
		// End of Instructions_to_Bank

		return termsDoc;
	}

	// End of Code by Indu Valavala

	// End of Class
	   /**
     *  Performs validation of text fields for length.
     *
     *  @param terms TermsBean - the bean being validated
     */
    public void validateTextLengths(TermsBean terms) throws java.rmi.RemoteException, AmsException
    {
    	// IR T36000048753 Rel9.5
    	String overrideSwiftLengthInd = getOverrideSwiftLengthInd(terms);
    	
    	if((overrideSwiftLengthInd != null)
				&& (overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO))){
	    	checkTextLength(terms, "amendment_details", 1000);
	    	checkTextLength(terms, "special_bank_instructions", 1000);
    	}
    }
}