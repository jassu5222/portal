package com.ams.tradeportal.busobj.termsmgr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import com.ams.tradeportal.busobj.TermsBean;
import com.ams.tradeportal.busobj.webbean.TermsWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.AttributeManager;
import com.amsinc.ecsg.util.DocumentHandler;

/**
 * This class manages the dynamic attribute registration and validation for a Terms business object or TermsWebBean for bank
 * released terms of a transaction of type Release
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */

public class UPDBankRelTermsMgr extends TermsMgr {
	private static final Logger LOG = LoggerFactory.getLogger(UPDBankRelTermsMgr.class);

	/**
	 * Constructor that is used by business objects to create their manager. This method also register the dynamic attributes of the
	 * bean.
	 *
	 * @param mgr
	 *            AttributeManager attribute manager of the bean being managed. Used to register attributes
	 */
	public UPDBankRelTermsMgr(AttributeManager mgr) throws AmsException {
		super(mgr);
	}

	/**
	 * Constructor that is used by web beans to create their manager. This method also register the dynamic attributes of the bean.
	 *
	 * @param mgr
	 *            TermsWebBean - the bean being managed. Used to register attributes.
	 */
	public UPDBankRelTermsMgr(TermsWebBean terms) throws AmsException {
		super(terms);
	}

	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for bank released terms with this
	 * transaction type
	 *
	 * @return Vector - list of attributes
	 */
	/**
	 * Returns a list of attributes. These describe each of the attributes that will be registered for the bean being managed.
	 *
	 * This method controls which attributes of TermsBean or TermsWebBean are registered for bank released terms with this
	 * transaction type
	 *
	 * @return Vector - list of attributes
	 */
	public List<Attribute> getAttributesToRegister() {
		List<Attribute> attrList = super.getAttributesToRegister();

		attrList.add(TermsBean.Attributes.create_expiry_date());
		attrList.add(TermsBean.Attributes.create_amount_currency_code());
		attrList.add(TermsBean.Attributes.create_amount());

		attrList.add(TermsBean.Attributes.create_financing_allowed());
		attrList.add(TermsBean.Attributes.create_finance_percentage());
		attrList.add(TermsBean.Attributes.create_unapplied_amount());

		return attrList;
	}

	/**
	 * This method is supposed to be used to set attributes of Terms business object.
	 *
	 * @param inputDoc
	 *            DocumentHandler - universal XML message from MQSeries
	 * @param terms
	 *            TermsBean
	 * @return boolean
	 */
	public boolean setManagerTerms(TermsBean terms, DocumentHandler inputDoc) throws java.rmi.RemoteException, AmsException {

		terms.setAttribute("expiry_date", inputDoc.getAttribute("/Terms/TermsDetails/ExpiryDate"));
		terms.setAttribute("amount_currency_code", inputDoc.getAttribute("/Terms/TermsDetails/AmountCurrencyCode"));
		terms.setAttribute("amount", inputDoc.getAttribute("/Terms/TermsDetails/Amount"));

		terms.setAttribute("financing_allowed", inputDoc.getAttribute("/Terms/TermsDetails/FinancingAllowed"));
		terms.setAttribute("finance_percentage", inputDoc.getAttribute("/Terms/TermsDetails/FinancePercentage"));
		terms.setAttribute("unapplied_amount", inputDoc.getAttribute("/Terms/TermsDetails/UnappliedAmount"));

		return true;
	}
}
