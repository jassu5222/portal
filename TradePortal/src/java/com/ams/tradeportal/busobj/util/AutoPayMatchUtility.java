package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.math.BigDecimal;

import com.ams.tradeportal.busobj.MatchPayDedGlDtls;
import com.ams.tradeportal.busobj.PayMatchResult;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This utility class provides methods for inserting payment details for a given invoice id.
 * 
 * @version 0.1
 */
public class AutoPayMatchUtility {
	private static final Logger LOG = LoggerFactory.getLogger(AutoPayMatchUtility.class);
	protected static JPylonProperties jPylonProperties;
	protected static String ejbServerLocation;

	static {
		try {
			jPylonProperties = JPylonProperties.getInstance();
			ejbServerLocation = jPylonProperties.getString("serverLocation");
		} catch (Exception e) {
			LOG.error("Unable determine JPylon property and Server location: ", e);
		}
	}

	/**
	 * This method inserts the payment details for the matched invoices upon user selects in unmatched invoice in UI.
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return int
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public long createPayMtchResult(DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {
		LOG.debug("inputDoc parameter passed to createPayMtchResult() : {}" , inputDoc);
		String outStandingAmt = "";
		String corpOrgOid = "";
		String remitAmount = "";
		long status = 0;
		String amount = "";
		String remitInd = "";
		Double d1 = null;

		String buttonName = "";
		if (inputDoc != null) {
			Vector<DocumentHandler> autoPayVec = inputDoc.getFragments("/AutoPayment");
			LOG.debug("autoPayVec: {}" ,autoPayVec);
			if (autoPayVec != null && autoPayVec.size() > 0) {
				for (int i = 0; i < autoPayVec.size(); i++) {
					DocumentHandler currDoc = (DocumentHandler) autoPayVec.elementAt(i);
					LOG.debug("currDoc: {}" ,currDoc);
					formatAmount(currDoc);
					outStandingAmt = currDoc.getAttribute("/applied_amount");
					corpOrgOid = currDoc.getAttribute("/corpOrgOid");
					remitAmount = currDoc.getAttribute("/remitAmount");
					remitInd = currDoc.getAttribute("/remitInd");
					buttonName = currDoc.getAttribute("/autoMatchButton");
					getGLCode(corpOrgOid, TradePortalConstants.ERP_PAYMENT, currDoc, mediatorServices);
					try {
						if (TradePortalConstants.AUTO_PAY_SEARCH.equals(buttonName)) {
							status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
						} else {
							if (TradePortalConstants.REMIT_IND_TRUE.equals(remitInd)) {
								if (Double.valueOf(remitAmount) > Double.valueOf(outStandingAmt)) {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
								} else if (Double.valueOf(remitAmount) < Double.valueOf(outStandingAmt)) {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, remitAmount, mediatorServices);
								} else {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
									status = 0;
								}
								//updateMatchAmount(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
							} else {
								if (Double.valueOf(remitAmount) > Double.valueOf(outStandingAmt)) {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
									getGLCode(corpOrgOid, TradePortalConstants.ERP_OVERPAYMENT, currDoc, mediatorServices);
									d1 = Double.valueOf(remitAmount) - Double.valueOf(outStandingAmt);
									amount = (d1 != null) ? d1.toString() : "";
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_OVERPAYMENT, amount, mediatorServices);
								} else if (Double.valueOf(remitAmount) < Double.valueOf(outStandingAmt)) {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, remitAmount, mediatorServices);
									getGLCode(corpOrgOid, TradePortalConstants.ERP_DISCOUNT, currDoc, mediatorServices);
									amount = remitAmount;
									d1 = Double.valueOf(outStandingAmt) - Double.valueOf(remitAmount);
									amount = (d1 != null) ? d1.toString() : "";
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_DISCOUNT, amount, mediatorServices);
								} else {
									status = preparePayDiscountObject(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
									status = 0;
								}
								//updateMatchAmount(currDoc, TradePortalConstants.ERP_PAYMENT, outStandingAmt, mediatorServices);
							}
						}
					} catch (RemoteException e) {
						LOG.error("Remote Exception in createPayMtchResult(): ", e);
					}
				}
			}
		}
		return status;
	}

	/**
	 * This is method creates ejb object and pass it insertPaymnetDetails method for db operation.
	 * 
	 * @param inputDoc
	 * @param tranType
	 * @param amount
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private long preparePayDiscountObject(DocumentHandler inputDoc, String tranType, String amount, MediatorServices mediatorServices) throws RemoteException, AmsException {
		MatchPayDedGlDtls matPayDedDtls = null;
		long updateRes = 0l;
		matPayDedDtls = (MatchPayDedGlDtls) EJBObjectFactory.createClientEJB(ejbServerLocation, "MatchPayDedGlDtls");
		updateRes = insertPaymentDetails(inputDoc, tranType, amount, matPayDedDtls, mediatorServices);
		return updateRes;
	}

	/**
	 * This method inserts payment/discount details based on tran type
	 * 
	 * @param inputDoc
	 * @param tranType
	 * @param amount
	 * @param matPayDedDtls
	 * @param mediatorServices
	 * @return oid
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private long insertPaymentDetails(DocumentHandler inputDoc, String tranType, String amount, MatchPayDedGlDtls matPayDedDtls, MediatorServices mediatorServices) throws RemoteException, AmsException {
		LOG.debug("Parameter passed into insertPaymentDetails(): inputDoc: {} \ttranType: {} \tamount: {}", new Object[]{inputDoc, tranType, amount});
		String outStandingAmt = "";
		String paymentGlCode = "";
		String invoiceReferenceId = "";
		String currentDt = "";
		String currentDtTime = "";
		String payRemitOid = "";
		long status = 0;
		String discCode = null;
		String discDesc = null;
		String discComments = null;
		String otlInvoiceUoid = null;
		long oid = 0;

		if (inputDoc != null) {
			outStandingAmt = inputDoc.getAttribute("/applied_amount");
			invoiceReferenceId = inputDoc.getAttribute("/invoiceID");
			payRemitOid = inputDoc.getAttribute("/payRemitOid");
			discCode = inputDoc.getAttribute("/discount_code");
			discDesc = inputDoc.getAttribute("/discount_description");
			discComments = inputDoc.getAttribute("/discount_comments");
			paymentGlCode = inputDoc.getAttribute("/general_ledger_code");
			otlInvoiceUoid = inputDoc.getAttribute("/OTLInvoiceUoid");
			discDesc = (discDesc == null) ? "" : discDesc;
			discComments = (discComments == null) ? "" : discComments;
			discCode = (discCode == null) ? "" : discCode;

			if (TradePortalConstants.ERP_PAYMENT.equals(tranType)) {
				status = deletePaymentDetails(otlInvoiceUoid);
			}

			oid = matPayDedDtls.newObject();
			currentDt = DateTimeUtility.getCurrentDate();
			currentDtTime = DateTimeUtility.getCurrentDateTime();
			LOG.debug("insertPaymentDetails(): otlInvoiceUoid: {} \tpaymentGlCode: {} \tinvoiceReferenceId: {} \toutStandingAmt: {} ", new Object[]{otlInvoiceUoid, paymentGlCode, invoiceReferenceId, outStandingAmt});
			LOG.debug("insertPaymentDetails(): discCode: {} \tdiscComments: {} \tdiscDesc: {} \tpayRemitOid: {} \tcurrentDtTime: {}", new Object[]{discCode, discComments, discDesc, payRemitOid,currentDtTime});
			matPayDedDtls.setAttribute("invoice_reference_id", invoiceReferenceId);
			matPayDedDtls.setAttribute("transaction_type", tranType);
			matPayDedDtls.setAttribute("applied_amount", amount);
			matPayDedDtls.setAttribute("general_ledger_code", paymentGlCode);
			matPayDedDtls.setAttribute("transaction_date", currentDt);
			matPayDedDtls.setAttribute("system_date_time", currentDtTime);
			matPayDedDtls.setAttribute("discount_code", discCode);
			matPayDedDtls.setAttribute("discount_description", discDesc);
			matPayDedDtls.setAttribute("discount_comments", discComments);
			matPayDedDtls.setAttribute("a_initiating_obj", payRemitOid);
			matPayDedDtls.setAttribute("otl_invoice_oid", otlInvoiceUoid);
			matPayDedDtls.setAttribute("initiating_obj_cls", "PayRemit");
			status = matPayDedDtls.save();
		}
		LOG.debug("insertPaymentDetails() status: {}",status);
		return oid;
	}

	// Srinivasu_D IR#39806 Rel9.3 05/28/2015 - Added below method to update match amount
	/**
	 * This method updates matched amount for the given matchItemOid
	 * 
	 * @param inputDoc
	 * @param tranType
	 * @param amount
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void updateMatchAmount(DocumentHandler inputDoc, String tranType, String amount, MediatorServices mediatorServices) throws RemoteException, AmsException {
		LOG.debug("Parameter passed into updateMatchAmount(): inputDoc: {} \ttranType: {} \tamount: {}", new Object[]{inputDoc, tranType, amount});
		String outStandingAmt = "";
		String invoiceReferenceId = "";
		String payRemitOid = "";
		String payInvOid = "";
		long oid = 0;

		if (inputDoc != null) {
			outStandingAmt = inputDoc.getAttribute("/applied_amount");
			invoiceReferenceId = inputDoc.getAttribute("/invoiceID");
			payRemitOid = inputDoc.getAttribute("/payRemitOid");
			payInvOid = inputDoc.getAttribute("/payInvOid");
			String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
			byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
			javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
			String payMatchInvOid = EncryptDecrypt.decryptStringUsingTripleDes(payInvOid, secretKey);
			LOG.debug("updateMatchAmount(): payMatchInvOid: {} \toutStandingAmt: {} \tinvoiceReferenceId: {} \tpayRemitOid: {} ", new Object[]{payMatchInvOid, outStandingAmt, invoiceReferenceId, payRemitOid});
			if (StringFunction.isNotBlank(payMatchInvOid)) {
				PayMatchResult payMatchResultBean = (PayMatchResult) mediatorServices.createServerEJB("PayMatchResult", Long.parseLong(payMatchInvOid));
				// start Total of the applied amounts where transaction type = Payment is copied over to the match amount field
				if (StringFunction.isNotBlank(amount))
					payMatchResultBean.setAttribute("matched_amount", amount);
				// save the bean
				oid = payMatchResultBean.save(true);
			}
		}
		LOG.debug("updateMatchAmount() status: {}", oid);
	}

	/**
	 * This method finds Payment GL Code from AR_MATCHING_TABLE if available else check in CUSTOMER_ERP_GL_CODE for the same
	 * otherwise blank would return.
	 * 
	 * @param corporateOrgOid
	 * @return String
	 * @throws AmsException
	 */
	private void getGLCode(String corporateOrgOid, String tranType, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException {
		String codeFound = TradePortalConstants.INDICATOR_NO;
		String codeToReturn = "";
		String paymentGLCode = "";
		String discountGLCode = "";
		String overPaymentGLCode = "";
		String buyerDiscDescription = "";
		String buyerComments = "";
		String buyerDiscCode = "";
		String buyerDiscountCode = "";
		String payRemitInvoiceOid = inputDoc.getAttribute("/payInvOid");
		long payInvOid = 0l;
		List<Object> sqlParams = new ArrayList();
		if (StringFunction.isNotBlank(payRemitInvoiceOid)) {
			String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
			byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
			javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
			payInvOid = Long.valueOf(EncryptDecrypt.decryptStringUsingTripleDes(payRemitInvoiceOid, secretKey));

		}
		LOG.debug("getGLCode() payInvOid: {}" ,payInvOid);
		String arMatchingSql = "select payment_gl_code,over_payment_gl_code,discount_gl_code from ar_matching_rule where p_corp_org_oid = ?";
		sqlParams.add(corporateOrgOid);
		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(arMatchingSql, false, sqlParams);
		if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
			Vector glCodeVector = result.getFragments("/ResultSetRecord");
			for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
				DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
				paymentGLCode = glCodeDoc.getAttribute("/PAYMENT_GL_CODE");
				discountGLCode = glCodeDoc.getAttribute("/DISCOUNT_GL_CODE");
				overPaymentGLCode = glCodeDoc.getAttribute("/OVER_PAYMENT_GL_CODE");
				LOG.debug("getGLCode(): paymentGLCode: {} \tdiscountGLCode: {} \toverPaymentGLCode: {}",new Object[]{paymentGLCode,discountGLCode,overPaymentGLCode});
				if (TradePortalConstants.ERP_PAYMENT.equals(tranType) && StringFunction.isNotBlank(paymentGLCode)) {
					codeFound = TradePortalConstants.INDICATOR_YES;
					codeToReturn = paymentGLCode;
					break;
				}
				else if (TradePortalConstants.ERP_DISCOUNT.equals(tranType) && StringFunction.isNotBlank(discountGLCode)) {
					codeFound = TradePortalConstants.INDICATOR_YES;
					codeToReturn = discountGLCode;
					break;
				} else if (TradePortalConstants.ERP_OVERPAYMENT.equals(tranType) && StringFunction.isNotBlank(overPaymentGLCode)) {
					codeFound = TradePortalConstants.INDICATOR_YES;
					codeToReturn = overPaymentGLCode;
					break;
				}
			}
		}
		LOG.debug("getGLCode(): first codeToReturn: {}" ,codeToReturn);

		if (TradePortalConstants.INDICATOR_NO.equals(codeFound)) {
			String glCodeSql = "select erp_gl_code, erp_gl_description from CUSTOMER_ERP_GL_CODE where p_owner_oid = ? and deactivate_ind = ? and erp_gl_category = ? and default_gl_code_ind = ?";
			sqlParams = new ArrayList();
			sqlParams.add(corporateOrgOid);
			sqlParams.add(TradePortalConstants.INDICATOR_NO);
			if (TradePortalConstants.ERP_PAYMENT.equals(tranType) && StringFunction.isBlank(paymentGLCode)) {
				sqlParams.add(TradePortalConstants.ERP_PAYMENT);
			} else if (TradePortalConstants.ERP_DISCOUNT.equals(tranType) && StringFunction.isBlank(discountGLCode)) {
				sqlParams.add(TradePortalConstants.ERP_DISCOUNT);
			} else if (TradePortalConstants.ERP_OVERPAYMENT.equals(tranType) && StringFunction.isBlank(overPaymentGLCode)) {
				sqlParams.add(TradePortalConstants.ERP_OVERPAYMENT);
			}
			sqlParams.add(TradePortalConstants.INDICATOR_YES);
			result = DatabaseQueryBean.getXmlResultSet(glCodeSql, false, sqlParams);
			if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
				Vector glCodeVector = result.getFragments("/ResultSetRecord");
				for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
					DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
					paymentGLCode = glCodeDoc.getAttribute("/ERP_GL_CODE");
					codeToReturn = paymentGLCode;

				}
			}
		}
		inputDoc.setAttribute("/general_ledger_code", codeToReturn);
		LOG.debug("getGLCode(): second codeToReturn: {}" , codeToReturn);
		if (TradePortalConstants.ERP_DISCOUNT.equals(tranType)) {
			DocumentHandler resCode = getBuyerDiscountCode(String.valueOf(payInvOid));
			if (resCode != null) {
				buyerDiscCode = resCode.getAttribute("/BUYER_DISC_CODE");
				buyerComments = resCode.getAttribute("/BUYER_DISC_COMMENTS");
			}
			buyerDiscountCode = StringFunction.isNotBlank(buyerDiscCode) ? buyerDiscCode.substring(0, 2) : "";
			codeToReturn = buyerDiscountCode;
			if (StringFunction.isNotBlank(buyerDiscCode) && buyerDiscCode.length() == 2) {
				buyerDiscDescription = getDiscountCodeDescription(inputDoc, buyerDiscCode);
			}
			if (StringFunction.isBlank(buyerDiscountCode)) {
				String glCodeSql = "select discount_code,discount_description from customer_discount_code where deactivate_ind = ? and default_disc_rate_ind = ? and p_owner_oid = ? ";
				sqlParams = new ArrayList();
				sqlParams.add(TradePortalConstants.INDICATOR_NO);
				sqlParams.add(TradePortalConstants.INDICATOR_YES);
				sqlParams.add(corporateOrgOid);
				result = DatabaseQueryBean.getXmlResultSet(glCodeSql, false, sqlParams);
				if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
					Vector glCodeVector = result.getFragments("/ResultSetRecord");
					for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
						DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
						paymentGLCode = glCodeDoc.getAttribute("/DISCOUNT_CODE");
						codeToReturn = paymentGLCode;
					}
				}
			}
		}
		if (TradePortalConstants.ERP_DISCOUNT.equals(tranType)) {
			inputDoc.setAttribute("/discount_description", buyerDiscDescription);
			inputDoc.setAttribute("/discount_code", codeToReturn);
			inputDoc.setAttribute("/discount_comments", buyerComments);
		}
		LOG.debug("getGLCode(): third codeToReturn: {}" , codeToReturn);
	}

	/**
	 * This method returns Discount code/comments for the payment reimit invoice id
	 * 
	 * @param payRemitInvoiceOid
	 * @return DocumentHandler
	 * @throws AmsException
	 */
	private static DocumentHandler getBuyerDiscountCode(String payRemitInvoiceOid) throws AmsException {
		String descr = "";
		String paymentGLCode = "";
		List<Object> sqlParams = new ArrayList();
		DocumentHandler glCodeDoc = null;
		String discSql = "select buyer_disc_code,buyer_disc_comments from pay_remit_invoice where pay_remit_invoice_oid = ?";
		sqlParams = new ArrayList();
		sqlParams.add(payRemitInvoiceOid);

		if (StringFunction.isNotBlank(payRemitInvoiceOid)) {

			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(discSql, false, sqlParams);
			if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
				Vector glCodeVector = result.getFragments("/ResultSetRecord");
				for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
					glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
					paymentGLCode = glCodeDoc.getAttribute("/BUYER_DISC_CODE");
					descr = glCodeDoc.getAttribute("/BUYER_DISC_COMMENTS");
				}
			}

		}
		return glCodeDoc;

	}

	/**
	 * This method returns the discount description for the given discount code
	 * 
	 * @param inputDoc
	 * @param code
	 * @return String
	 * @throws AmsException
	 */
	private static String getDiscountCodeDescription(DocumentHandler inputDoc, String code) throws AmsException {
		String descr = "";
		List<Object> sqlParams = new ArrayList();
		String glCodeSql = "select erp_gl_code, erp_gl_description from CUSTOMER_ERP_GL_CODE where  erp_gl_code = ?";
		String cbName = inputDoc.getAttribute("/cbName");
		sqlParams = new ArrayList();
		sqlParams.add(code);
		if (StringFunction.isNotBlank(code)) {

			DocumentHandler result = DatabaseQueryBean.getXmlResultSet(glCodeSql, false, sqlParams);
			if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
				Vector glCodeVector = result.getFragments("/ResultSetRecord");
				for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
					DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
					descr = glCodeDoc.getAttribute("/ERP_GL_DESCRIPTION");
				}
			}

			if (StringFunction.isBlank(descr)) {
				glCodeSql = "select code as DISCOUNT_CODE, descr as DISCOUNT_DESCRIPTION from bankrefdata where table_type ='DISCOUNT_CODE' and client_bank_id = ? ";
				sqlParams = new ArrayList();
				sqlParams.add(cbName);
				result = DatabaseQueryBean.getXmlResultSet(glCodeSql, false, sqlParams);
				if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
					Vector glCodeVector = result.getFragments("/ResultSetRecord");
					for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
						DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
						descr = glCodeDoc.getAttribute("/DISCOUNT_DESCRIPTION");
					}
				}
			}
			if (StringFunction.isBlank(descr)) {
				glCodeSql = "select discount_code,discount_description from customer_discount_code where discount_code = ? ";
				sqlParams = new ArrayList();
				sqlParams.add(code);
				result = DatabaseQueryBean.getXmlResultSet(glCodeSql, false, sqlParams);
				if (result != null && result.getFragments("/ResultSetRecord").size() > 0) {
					Vector glCodeVector = result.getFragments("/ResultSetRecord");
					for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
						DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
						descr = glCodeDoc.getAttribute("/DISCOUNT_DESCRIPTION");
					}
				}
			}
		}
		return descr;

	}

	/**
	 * This method formats amount by removing commas to avoid any db exceptions
	 * 
	 * @param inputDoc
	 * @throws InvalidAttributeValueException
	 */
	private static void formatAmount(DocumentHandler inputDoc) throws InvalidAttributeValueException {

		String outStandingAmt = inputDoc.getAttribute("/applied_amount");
		String remitAmount = inputDoc.getAttribute("/remitAmount");
		if (StringFunction.isBlank(outStandingAmt)) {
			outStandingAmt = "0";
		}
		if (StringFunction.isBlank(remitAmount)) {
			remitAmount = "0";
		}
		BigDecimal outAmount = new BigDecimal(outStandingAmt.replace(",", ""));
		BigDecimal remAmount = new BigDecimal(remitAmount.replace(",", ""));
		inputDoc.setAttribute("/applied_amount", String.valueOf(outAmount));
		inputDoc.setAttribute("/remitAmount", String.valueOf(remAmount));
	}

	/**
	 * This method deletes payment details in match_pay_ded_gl_dtls when user invokes UnMatch action.
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return delete status.
	 * @throws RemoteException
	 * @throws AmsException
	 * @throws SQLException
	 */
	public int deletePayMatchResult(DocumentHandler inputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException, SQLException {
		LOG.debug("deletePayMatchResult()...: {}" , inputDoc);
		String invoiceReferenceId = "";
		int status = 0;
		if (inputDoc != null && StringFunction.isNotBlank(inputDoc.getAttribute("/AutoPayment(0)/invoiceID"))) {

			invoiceReferenceId = inputDoc.getAttribute("/AutoPayment(0)/invoiceID");
			String deleteSql = "delete from match_pay_ded_gl_dtls where invoice_reference_id = ? ";
			LOG.debug("invoiceReferenceId: {}" , invoiceReferenceId);
			List<Object> sqlParams = new ArrayList();
			sqlParams.add(invoiceReferenceId);
			status = DatabaseQueryBean.executeUpdate(deleteSql, false, sqlParams);
		}
		LOG.debug("delete  status: {}" , status);
		return status;
	}

	// Srinivasu_D IR#T36000040971 Rel9.3 06/30/2015- Added below method to delete exisitng match details before creating a new ones
	private int deletePaymentDetails(String otlInvoiceUoid) throws RemoteException, AmsException {		
		int status = 0;
		if (StringFunction.isNotBlank(otlInvoiceUoid)) {
			String deleteSql = "delete from match_pay_ded_gl_dtls where otl_invoice_oid = ? ";
			List<Object> sqlParams = new ArrayList();
			sqlParams.add(otlInvoiceUoid);
			try {
				status = DatabaseQueryBean.executeUpdate(deleteSql, false, sqlParams);
			} catch (SQLException sql) {
				LOG.debug("deletePaymentDetails() otlInvoiceUoid: {}",otlInvoiceUoid);
				throw new AmsException(sql);
			}

		}
		LOG.debug("deletePaymentDetails() status: {}", status);
		return status;
	}
	// Srinivasu_D IR#T36000040971 Rel9.3 06/30/2015- End

	// Srinivasu_D CR#997 Rel9.3 04/03/2015 - End
}
