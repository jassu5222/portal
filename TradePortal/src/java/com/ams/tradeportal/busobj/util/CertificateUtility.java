package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.security.cert.X509Certificate;
import java.util.*;

import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.common.*;

/**
 * Utility methods for certificates.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */
public class CertificateUtility {
	private static final Logger LOG = LoggerFactory.getLogger(CertificateUtility.class);

	/**
	 * Never instantiate.
	 */
	private CertificateUtility() {
	}

	/**
	 * Validate the certificate. This is commonly used for transaction authorization, payment match authorization, and certain
	 * invoice actions.
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static boolean validateCertificate(DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {

		// CR-473 BEGIN
		// Validate Smart Card
		String userOid = inputDoc.getAttribute("/User/userOid");
		String reCertOK = inputDoc.getAttribute("/Authorization/reCertOK");
		String logonResponse = inputDoc.getAttribute("/Authorization/logonResponse");
		String logonCertificate = inputDoc.getAttribute("/Authorization/logonCertificate");

		LOG.debug("validateCertificate(): reCertOK: {}" , reCertOK);

		// If the 2FA failed or SmartCard reading failed ...
		if (TradePortalConstants.INDICATOR_NO.equals(reCertOK)) {
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.RECERTIFICATION_FAILED);
			return false;
		}

		// W Zhu 12/16/09 CR-482 BEGIN
		// Skip validation if recertOK = Y, when we did a dummy recert page or if 2FA succeeded (2FA is validated on
		// TradePortalRecertification.jsp
		// Also skip validation if reCertOK is empty, when there is no recert.
		// Only continue to validate the certificate data if recertOK = M, when we have read the certificate data from Smart Card.
		// TODO: we should unify the action flow for different recertification in the future.
		if (TradePortalConstants.INDICATOR_YES.equals(reCertOK) || StringFunction.isBlank(reCertOK)) {
			return true;
		}

		// IAZ CR-473 05/28/09 Begin: Check if this is dummy/dev/test authentication that does not req smartcard and certificate
		// IAZ CR-473 06/04/09 Begin: Get ClientBank Oid directly from User's data
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));

		if (StringFunction.isBlank(logonCertificate)) {
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.RECERTIFICATION_FAILED);
			return false;
		}
		// IAZ CR-473 05/28/09 End
		// Get the smart card certificate
		byte[] logonCertificateBytes = logonCertificate.getBytes();
		X509Certificate smartCardCert = EncryptDecrypt.getCertificate(logonCertificateBytes);
		if (smartCardCert == null) {
			LOG.debug("Invalid certificate: {}" ,logonCertificate);
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.RECERTIFICATION_FAILED);
			return false;
		}

		// Check the Challenge String Response matches the original challenge string
		java.security.PublicKey smartCardPublicKey = smartCardCert.getPublicKey();
		byte[] logonResponseBytes = EncryptDecrypt.base64StringToBytes(logonResponse, false);
		byte[] decryptedLogonResponseBytes = EncryptDecrypt.decryptUsingPublicKey(logonResponseBytes, smartCardPublicKey);
		final int CHALLENGE_RESPONSE_FILLER_NUMBER = 24; // The first 24 characters in the response is filler. Ignore.
		String challengeStringResponse = null;
		try {
			challengeStringResponse = EncryptDecrypt.bytesToBase64String(decryptedLogonResponseBytes, false).substring(CHALLENGE_RESPONSE_FILLER_NUMBER);
		} catch (Exception e) {
			LOG.debug("logon response error: {}" ,logonResponse);
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.RECERTIFICATION_FAILED);
			return false;
		}
		String challengeString = mediatorServices.getCSDB().getCSDBValue("SmartCardChallengeString");
		String challengeStringKey = mediatorServices.getCSDB().getCSDBValue("SmartCardChallengeStringKey");

		if (challengeString == null || !challengeString.equals(challengeStringResponse)) {
			LOG.debug("recertification failed: challengeStringKey: {} \tchallengeString: {} \tchallengeStringResponse: {}", new Object[] { challengeStringKey, challengeString, challengeStringResponse });
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.RECERTIFICATION_FAILED);
			return false;
		}

		// Get the ID on the certificate, which is the CN part of the Subject Distinguished Name
		String subjectDN = smartCardCert.getSubjectDN().getName();
		StringTokenizer parser = new StringTokenizer(subjectDN, "=, ");
		String certificateIDResponse = null;
		while (parser.hasMoreTokens()) {
			String token = parser.nextToken();
			if (token.equals("CN") && parser.hasMoreTokens()) {
				certificateIDResponse = parser.nextToken();
				break;
			}
		}

		// Validate the ID on the certificate is the certificate_id of the user
		String certificateID = user.getAttribute("certificate_id");
		if (certificateID == null || !certificateID.equals(certificateIDResponse)) {
			LOG.debug("certificateID: {} \t certificateIDResponse: {} ", certificateID, certificateIDResponse);
			mediatorServices.getErrorManager().issueError("CertificateUtility", TradePortalConstants.INCORRECT_CERTIFICATE_ID);
			return false;
		}
		return true;
	}
	// CR-473 Validate Smart Card ends

}
