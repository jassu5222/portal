package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FavoriteTaskObject {
private static final Logger LOG = LoggerFactory.getLogger(FavoriteTaskObject.class);
	
	private String favOid = null;
   
	private int displayOrder;
	private String favId = null;
	private String favOptLock = null;
	private String addlParams = null;
	private String userOid = null;
	private String favoriteType = null;
	
	public String getFavOid() {
		return favOid;
	}

	public void setFavOid(String favOid) {
		this.favOid = favOid;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getFavId() {
		return favId;
	}

	public void setFavId(String favId) {
		this.favId = favId;
	}

	public String getFavOptLock() {
		return favOptLock;
	}

	public void setFavOptLock(String favOptLock) {
		this.favOptLock = favOptLock;
	}

	public String getAddlParams() {
		return addlParams;
	}

	public void setAddlParams(String addlParams) {
		this.addlParams = addlParams;
	}

	public String getUserOid() {
		return userOid;
	}

	public void setUserOid(String userOid) {
		this.userOid = userOid;
	}

	public String getFavoriteType() {
		return favoriteType;
	}

	public void setFavoriteType(String favoriteType) {
		this.favoriteType = favoriteType;
	}
	
}