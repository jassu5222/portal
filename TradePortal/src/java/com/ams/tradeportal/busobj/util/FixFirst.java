package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;

public class FixFirst extends PanelRule{
private static final Logger LOG = LoggerFactory.getLogger(FixFirst.class);



    FixFirst(DocumentHandler doc) {
    	super(doc);
    }


	FixFirst(String str) {
		super(str); 
	}


	public String toString(){
		super.setSequence("FIX_FIRST");
		return super.toString("FIX_FIRST: ");
	}

	protected String getFirstApprover() {
		return panelApprovers.substring(0, 1);
	}

	
	
	@Override
	/*
	 * Updated method to ignore panel level in the history if the panel level doesn't exist in the rule
	 */
	protected String matchSub(String authHistory, String userPanelLevel) throws AmsException {

		authHistory += userPanelLevel;

		//validating first panel level
		int index = -1;
		int totAuthHis = authHistory.length();
		String panelLevel=null;
		for(int i=0; i<totAuthHis; i++){
			panelLevel = authHistory.substring(i, i+1);
			if (panelLevel.equalsIgnoreCase(getFirstApprover())){
				index = i;
				break;
			} 
		}

		if (index == -1) {		
			return Panel.AUTH_RESULT_FAILED;
		}
		authHistory = authHistory.substring(index+1);
		//MEerupula Rel 8.3 IR-20194 This method is used to capture history and to delete currentUserPanelLevel from rule
		updateMatchedPanel(0);
				
		return  processAuth(authHistory,true);

	}


	
	/*Added by MEerupula Rel8.3 CR 821
	 * 
	 * 
	 */
	public String getNextAwaitingPanelLevels()  {
		String nextPanelLevel = "";
		if (Panel.AUTH_RESULT_FAILED.equals(result)) {
			nextPanelLevel = getFirstApprover();
		} else {
			nextPanelLevel = tempPanelApprovers.toString();
		}
		return nextPanelLevel;
	}
	
	
}
