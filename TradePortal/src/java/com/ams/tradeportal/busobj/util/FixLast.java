package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class FixLast extends PanelRule{
private static final Logger LOG = LoggerFactory.getLogger(FixLast.class);



	FixLast (String str) {
		super(str);
	}

    FixLast(DocumentHandler doc) {
    	super(doc);
    }
	
    
	public String toString(){
		super.setSequence("FIX_LAST");
		return super.toString("FIX_LAST: ");
	}



	protected String getLastApprover() {

		return panelApprovers.substring(getTotalAuthorizers()-1,getTotalAuthorizers());
	}


	protected String separateLastApprover() {
		String approver="";
		int len = tempPanelApprovers.length();
		if (len > 0) {
			approver = tempPanelApprovers.substring(len-1,len);
			tempPanelApprovers.deleteCharAt(len -1);
		}
		return approver;
	}
	
	@Override
	protected String matchSub(String authHistory, String userPanelLevel) throws AmsException{

		
		String lastApprover = separateLastApprover();
		processAuth(authHistory,false);
		tempPanelApprovers.append(lastApprover);
		
		if (StringFunction.isBlank(userPanelLevel) && isLastUser()) {
			for(int i=index; i<authHistory.length(); i++, index++){
				String panelLevel = authHistory.substring(i, i+1);
				int idx = tempPanelApprovers.indexOf(panelLevel);
				if (idx != -1 && idx < getMaxCount()) {
					updateMatchedPanel(idx);
				}
			}
		} 
		

		if (isLastUser()) {
			if (userPanelLevel.equals(lastApprover)) {
				result= Panel.AUTH_RESULT_COMPLETE;
				updateMatchedPanel(0);
			} else {
				result= Panel.AUTH_RESULT_FAILED;
			}
		} else {
			if (StringFunction.isNotBlank(userPanelLevel)){
				int idx = tempPanelApprovers.indexOf(userPanelLevel);
				if (idx != -1 && idx+1 != tempPanelApprovers.length()) {
					updateMatchedPanel(idx);
					result= Panel.AUTH_RESULT_PARTIAL;
				} else  {
					result= Panel.AUTH_RESULT_FAILED;
				}
			}	
		}
		return result;
	}

	protected boolean isLastUser() {
		return tempPanelApprovers.length()==1;
	}

	public String getNextAwaitingPanelLevels() throws AmsException {
		
		String nextPanelLevel = "";
		if (isLastUser()) {
			nextPanelLevel = getLastApprover();
		} else {
			nextPanelLevel = tempPanelApprovers.toString();
		}
		return nextPanelLevel;
	}

}
