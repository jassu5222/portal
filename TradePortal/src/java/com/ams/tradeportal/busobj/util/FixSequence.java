package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


public class FixSequence extends PanelRule{
private static final Logger LOG = LoggerFactory.getLogger(FixSequence.class);



	FixSequence(DocumentHandler doc) {
		super(doc);
	}


	FixSequence(String str) {
		super(str);
	}

	public String toString(){
		super.setSequence("FIX_ALL");
		return super.toString("FIX_ALL: ");
	}


	protected String getNextApprover() {
		String approver="";
		if (tempPanelApprovers.length() > 0) { 
			approver= tempPanelApprovers.substring(0, 1);
	   }
		return approver;
	}


	@Override
	/*
	 * Updated method to ignore panel level in the history if the panel level doesn't exist in the rule
	 */
	protected String matchSub(String authHistory, String userPanelLevel) {



		int totAuthHis = authHistory.length();
		String panelLevel=null;
		for(int i=0; i<totAuthHis && tempPanelApprovers.length()>0; i++){
			panelLevel = authHistory.substring(i, i+1);
			if (panelLevel.equalsIgnoreCase(getNextApprover())){
				updateMatchedPanel(0);
			} 
		}


		if(StringFunction.isNotBlank(userPanelLevel)){
			if (getNextApprover().equals(userPanelLevel)) {
				updateMatchedPanel(0);
				if (tempPanelApprovers.length() == 0 ) {
					result= Panel.AUTH_RESULT_COMPLETE;
				}else {
					result= Panel.AUTH_RESULT_PARTIAL;
				}
			} else {
				result= Panel.AUTH_RESULT_FAILED;
			}
		}

		return result;
	}


	public String getNextAwaitingPanelLevels() throws AmsException {

		return (tempPanelApprovers.length() > 1 ? tempPanelApprovers.substring(0, 1):"");
	}
	
}
