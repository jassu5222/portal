package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.*;

/**
 * Utility to determine if a user must re-authenticate when authorizing an instrument transaction.
 */
public class InstrumentAuthentication {
	private static final Logger LOG = LoggerFactory.getLogger(InstrumentAuthentication.class);
	// Instrument Transaction Authentication requirements.
	// Use bitwise comparison to test for required authentication.
	// Each transaction is assigned a unique BigInteger value of
	// a power of 2 (thus each right is a binary number consisting of
	// only one 1 and a bunch of 0s. The various rights a user has
	// are ANDed together to come up with a single long value.

	private final static BigInteger ZERO = new BigInteger("0");
	private final static BigInteger TWO = new BigInteger("2");

	// NEVER EVER change the values assigned to these constants!
	public final static BigInteger NO_TRAN_AUTH = ZERO;
	public final static BigInteger TRAN_AUTH__AIR_REL = TWO.pow(0);
	public final static BigInteger TRAN_AUTH__ATP_ISS = TWO.pow(1);
	public final static BigInteger TRAN_AUTH__ATP_AMD = TWO.pow(2);
	public final static BigInteger TRAN_AUTH__ATP_APR = TWO.pow(3);
	public final static BigInteger TRAN_AUTH__DDI_ISS = TWO.pow(4);
	public final static BigInteger TRAN_AUTH__EXP_COL_ISS = TWO.pow(5);
	public final static BigInteger TRAN_AUTH__EXP_COL_AMD = TWO.pow(6);
	public final static BigInteger TRAN_AUTH__EXP_OCO_ISS = TWO.pow(7);
	public final static BigInteger TRAN_AUTH__EXP_OCO_AMD = TWO.pow(8);
	public final static BigInteger TRAN_AUTH__EXP_DLC_TRN = TWO.pow(9);
	public final static BigInteger TRAN_AUTH__EXP_DLC_ATR = TWO.pow(10);
	public final static BigInteger TRAN_AUTH__EXP_DLC_ASN = TWO.pow(11);
	public final static BigInteger TRAN_AUTH__EXP_DLC_DCR = TWO.pow(12);
	public final static BigInteger TRAN_AUTH__IMP_DLC_ISS = TWO.pow(13);
	public final static BigInteger TRAN_AUTH__IMP_DLC_AMD = TWO.pow(14);
	public final static BigInteger TRAN_AUTH__IMP_DLC_DCR = TWO.pow(15);
	public final static BigInteger TRAN_AUTH__INC_SLC_DCR = TWO.pow(16);
	public final static BigInteger TRAN_AUTH__FTRQ_ISS = TWO.pow(17);
	public final static BigInteger TRAN_AUTH__LRQ_ISS = TWO.pow(18);
	public final static BigInteger TRAN_AUTH__GUAR_ISS = TWO.pow(19);
	public final static BigInteger TRAN_AUTH__GUAR_AMD = TWO.pow(20);
	public final static BigInteger TRAN_AUTH__SLC_ISS = TWO.pow(21);
	public final static BigInteger TRAN_AUTH__SLC_AMD = TWO.pow(22);
	public final static BigInteger TRAN_AUTH__SLC_DCR = TWO.pow(23);
	public final static BigInteger TRAN_AUTH__FTDP_ISS = TWO.pow(24);
	public final static BigInteger TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE = TWO.pow(25);
	public final static BigInteger TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT = TWO.pow(26);
	public final static BigInteger TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE = TWO.pow(27);
	public final static BigInteger TRAN_AUTH__RECEIVABLES_FIN_INVOICE = TWO.pow(28);
	public final static BigInteger TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE = TWO.pow(29);
	public final static BigInteger TRAN_AUTH__RQA_ISS = TWO.pow(30);
	public final static BigInteger TRAN_AUTH__RQA_AMD = TWO.pow(31);
	public final static BigInteger TRAN_AUTH__RQA_DCR = TWO.pow(32);
	public final static BigInteger TRAN_AUTH__SHP_ISS = TWO.pow(33);
	public final static BigInteger TRAN_AUTH__FTBA_ISS = TWO.pow(34);
	//
	// AAlubala - CR710. Rel8.0 - Invoice Management Instr Auth - 4/20/2012 - START
	public final static BigInteger TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE = TWO.pow(35);
	// CR710 - END
	//

	// Rel 9.0 CR 913 START
	public final static BigInteger TRAN_AUTH__PAYABLES_MGMT_INV_UPDATES = TWO.pow(36);
	public final static BigInteger TRAN_AUTH__PAYABLES_UPLOAD_INV_PROCESS = TWO.pow(37);
	// Rel 9.0 CR 913 END

	// Rel 9.4 CR-818
	public final static BigInteger TRAN_AUTH__LRQ_SIT = TWO.pow(38);
	public final static BigInteger TRAN_AUTH__IMP_DLC_SIT = TWO.pow(39);
	public final static BigInteger TRAN_AUTH__IMC_SIT = TWO.pow(40);

	/*
	 * A special literal used to determine if any instrument/transactions require authentication. Note this is a special literal and
	 * cannot be used for comparison external to the InstrumentAuthentication class (it is not a bitand of all transaction
	 * authentications).
	 */
	public final static BigInteger ANY_TRAN_AUTH = new BigInteger("-1");

	/**
	 * Constructor. Never let this be created. All methods are static.
	 */
	private InstrumentAuthentication() {
	}

	/**
	 * Determine whether the requested transaction authentication is in the set of required transaction authentications.
	 * 
	 * @param requireTranAuth
	 * @param requestedTranAuth
	 * @return
	 */
	public static boolean requireTransactionAuthentication(String requireTranAuth, BigInteger requestedTranAuth) {
		boolean require = false;

		// convert requireTranAuth to a BigInteger
		BigInteger compareTranAuth;
		try {
			compareTranAuth = new BigInteger(requireTranAuth);
		} catch (Exception e) {
			// if not valid, assume no authentication
			// is this backwards???
			compareTranAuth = NO_TRAN_AUTH;
		}

		// do special checks first
		if (ANY_TRAN_AUTH.equals(requestedTranAuth)) {
			// if compareTranAuth > 0 something requires auth.
			// note this requires NO_TRAN_AUTH == 0
			require = compareTranAuth.compareTo(ZERO) > 0;
		} else {
			// use AND to determine if the requested transaction authentication
			// is in the set of required transaction authentications
			// if so, result is greater than 0
			require = compareTranAuth.and(requestedTranAuth).compareTo(ZERO) > 0;
		}
		return require;
	}

	/**
	 * Determine whether the requested transaction authentication is in the set of required transaction authentications.
	 * 
	 * Note: this method does not include receivables management authentications
	 * 
	 * @param requireTranAuth
	 * @param instrumentTranType
	 *            - this is a concatenation of the instrument type and transaction type separated by _ char.
	 * @return
	 */
	public static boolean requireTransactionAuthentication(String requireTranAuth, String instrumentTranType) {
		boolean require = false;

		// map values to a requireTranAuth value.
		// if not valid assume not authentication
		// is this backwards???
		BigInteger requestedTranAuth = NO_TRAN_AUTH;
		if ("AIR_REL".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__AIR_REL;
		} else if ("ATP_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__ATP_ISS;
		} else if ("ATP_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__ATP_AMD;
		} else if ("ATP_APR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__ATP_APR;
		} else if ("DDI_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__DDI_ISS;
		} else if ("EXP_COL_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_COL_ISS;
		} else if ("EXP_COL_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_COL_AMD;
		} else if ("EXP_OCO_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_OCO_ISS;
		} else if ("EXP_OCO_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_OCO_AMD;
		} else if ("EXP_DLC_TRN".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_DLC_TRN;
		} else if ("EXP_DLC_ATR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_DLC_ATR;
		} else if ("EXP_DLC_ASN".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_DLC_ASN;
		} else if ("EXP_DLC_DCR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__EXP_DLC_DCR;
		} else if ("IMP_DLC_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__IMP_DLC_ISS;
		} else if ("IMP_DLC_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__IMP_DLC_AMD;
		} else if ("IMP_DLC_DCR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__IMP_DLC_DCR;
		} else if ("INC_SLC_DCR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__INC_SLC_DCR;
		} else if ("FTRQ_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__FTRQ_ISS;
		} else if ("LRQ_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__LRQ_ISS;
		} else if ("GUA_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__GUAR_ISS;
		} else if ("GUA_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__GUAR_AMD;
		} else if ("SLC_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__SLC_ISS;
		} else if ("SLC_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__SLC_AMD;
		} else if ("SLC_DCR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__SLC_DCR;
		} else if ("FTDP_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__FTDP_ISS;
		} else if ("RQA_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__RQA_ISS;
		} else if ("RQA_AMD".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__RQA_AMD;
		} else if ("RQA_DCR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__RQA_DCR;
		} else if ("SHP_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__SHP_ISS;
		} else if ("FTBA_ISS".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__FTBA_ISS;
		} else if ("LRQ_SIM".equals(instrumentTranType) || "LRQ_SIR".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__LRQ_SIT;
		} else if ("IMC_SIM".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__IMC_SIT;
		} else if ("IMP_DLC_SIR".equals(instrumentTranType) || "DBA_SIR".equals(instrumentTranType)
				|| "DBA_SIM".equals(instrumentTranType) || "DFP_SIR".equals(instrumentTranType)
				|| "DFP_SIM".equals(instrumentTranType)) {
			requestedTranAuth = TRAN_AUTH__IMP_DLC_SIT;
		}
		return requireTransactionAuthentication(requireTranAuth, requestedTranAuth);
	}
}
