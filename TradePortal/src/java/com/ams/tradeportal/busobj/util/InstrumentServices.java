package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.busobj.*;

/**
 * Service methods for instruments and transactions. All methods on this class are static so InstrumetServices would never be
 * instantiated.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */

public class InstrumentServices {
	private static final Logger LOG = LoggerFactory.getLogger(InstrumentServices.class);
	// Static variables used in validating instrument character input
	private static String additionalValidCharacters;
	private static boolean allowUpperAndLowerCaseEnglishLetters;

	/**
	 * InstrumentServices constructor.
	 */
	public InstrumentServices() {
		super();
	}

	static {
		// Determine which characters are valid for input in transaction forms
		try {
			PropertyResourceBundle characterValidationProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("ValidCharacters");
			String allow = characterValidationProperties.getString("allowUpperAndLowerCaseEnglishLetters");
			if (allow != null){
				allowUpperAndLowerCaseEnglishLetters = allow.equalsIgnoreCase("true");
			}
			else{
				allowUpperAndLowerCaseEnglishLetters = false;
			}
			additionalValidCharacters = characterValidationProperties.getString("additionalValidCharacters");

			if (additionalValidCharacters == null){
				additionalValidCharacters = "";
			}
		} catch (Exception e) {
			LOG.info("Can't determine list of valid characters: ", e);
			allowUpperAndLowerCaseEnglishLetters = false;
			additionalValidCharacters = "";
		}
	}

	/**
	 * Performs a validation to determine if all characters in a string are contained in a set of valid characters
	 * 
	 * @param textToCheck
	 *            - the text to check for
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 * @param attributeAlias
	 *            - the alias of the attribute being validated (used in the error message)
	 */
	public static void checkForInvalidSwiftCharacters(String textToCheck, ErrorManager errMgr, String attributeAlias) throws AmsException {
		checkForInvalidSwiftCharacters(textToCheck, errMgr, attributeAlias, null);
	}

	/**
	 * Performs a validation to determine if all characters in a string are contained in a set of valid characters
	 * 
	 * @param textToCheck
	 *            - the text to check for
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 * @param attributeAlias
	 *            - the alias of the attribute being validated (used in the error message)
	 * @param prependTextForError
	 *            - text to prepend to the beginning of the field name in the error message (used for terms parties)
	 */
	public static void checkForInvalidSwiftCharacters(String textToCheck, ErrorManager errMgr, String attributeAlias, String prependTextForError) throws AmsException {
		// Only instantiate this if there are invalid characters... most of the time we won't need to
		Set invalidCharsFound = null;
		// Convert the string to be validated to a character array for faster processing
		char[] chars = textToCheck.toCharArray();

		// Loop through each character in the string, making sure that
		// it matches one of the characters in the valid SWIFT character set
		for (int i = 0; i < chars.length; i++) {
			// If upper and lower case English characters are allowed
			// and this character is one of those, continue the loop
			// and don't add to the list of bad characters
			// We check ranges here because it is much faster than checking
			// to see if the character is one of 52 characters.
			if (allowUpperAndLowerCaseEnglishLetters
					&& (((chars[i] >= 'a') && (chars[i] <= 'z')) || ((chars[i] >= 'A') && (chars[i] <= 'Z')))) {
				continue;
			}

			// If the character is one of the additional characters that are allowed,
			// continue the loop and don't add to the list of bad characters
			else if (additionalValidCharacters.indexOf(chars[i]) >= 0) {
				continue;
			} else if (i != 0 && String.valueOf(chars[i]).equalsIgnoreCase(":")) { // IR SSUL041956956 - manohar - 08/10/2011
				continue;
			} else {
				// If there's a match, add the invalid character to the list
				// Sets make sure that there will be no duplicates in the list
				if (invalidCharsFound == null){
					invalidCharsFound = new HashSet(4);
				}
				invalidCharsFound.add(String.valueOf(chars[i]));
			}
		}

		// Check if any invalid characters were found
		// Only one error message is issued per string that is checked
		if (invalidCharsFound != null) {
			// Build the list of invalid characters for display in the error message
			StringBuilder listOfInvalidCharacters = new StringBuilder();

			Iterator iter = invalidCharsFound.iterator();
			while (iter.hasNext()) {
				listOfInvalidCharacters.append(iter.next());
				if (iter.hasNext())
					listOfInvalidCharacters.append(" ,");
			}

			// Add the alias prefix if it is passed in
			if (prependTextForError != null) {
				attributeAlias = prependTextForError + " " + attributeAlias;
			}
			// Issue the error message
			if (listOfInvalidCharacters.toString().equalsIgnoreCase(":")) {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_SWIFT_CHAR_SEMICOLON, attributeAlias, listOfInvalidCharacters.toString());
			} else {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_SWIFT_CHARS, attributeAlias, listOfInvalidCharacters.toString());
			}
		}
	}

	/**
	 * Performs a validation to determine if all of the characters in all text attributes of a business object exist within the set
	 * of valid SWIFT characters. Only attributes that are regular Attributes (not subclasses of Attribute) will have their values
	 * validated.
	 * 
	 * @param attributeHash
	 *            - attributes from the business object - obtained using getAttributeHash()
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 * @param prependTextForError
	 *            - text to prepend to the beginning of the field name in the error message (used for terms parties)
	 */
	public static void checkForInvalidSwiftCharacters(Hashtable attributeHash, ErrorManager errMgr, String prependTextForError) throws AmsException {
		checkForInvalidSwiftCharacters(attributeHash, errMgr, null, prependTextForError);
	}

	/**
	 * Performs a validation to determine if all of the characters in all text attributes of a business object exist within the set
	 * of valid SWIFT characters. Only attributes that are regular Attributes (not subclasses of Attribute) will have their values
	 * validated.
	 * 
	 * @param attributeHash
	 *            - attributes from the business object - obtained using getAttributeHash()
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 */
	public static void checkForInvalidSwiftCharacters(Hashtable attributeHash, ErrorManager errMgr) throws AmsException {
		checkForInvalidSwiftCharacters(attributeHash, errMgr, null, null);
	}

	/**
	 * Performs a validation to determine if all of the characters in all text attributes of a business object exist within the set
	 * of valid SWIFT characters. Only attributes that are regular Attributes (not subclasses of Attribute) will have their values
	 * validated.
	 * 
	 * @param attributeHash
	 *            - attributes from the business object - obtained using getAttributeHash()
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 * @param attributesToExcludeArray
	 *            - an array of attribute names to exclude from checking for invalid characters
	 */
	public static void checkForInvalidSwiftCharacters(Hashtable attributeHash, ErrorManager errMgr, String[] attributesToExclude) throws AmsException {
		checkForInvalidSwiftCharacters(attributeHash, errMgr, attributesToExclude, null);
	}

	/**
	 * Performs a validation to determine if all of the characters in all text attributes of a business object exist within the set
	 * of valid SWIFT characters. Only attributes that are regular Attributes (not subclasses of Attribute) will have their values
	 * validated.
	 * 
	 * @param attributeHash
	 *            - attributes from the business object - obtained using getAttributeHash()
	 * @param errMgr
	 *            - the error manager on which the error should be issued
	 * @param attributesToExcludeArray
	 *            - an array of attribute names to exclude from checking for invalid characters
	 * @param prependTextForError
	 *            - text to prepend to the beginning of the field name in the error message (used for terms parties)
	 */
	public static void checkForInvalidSwiftCharacters(Hashtable attributeHash, ErrorManager errMgr, String[] attributesToExclude, String prependTextForError) throws AmsException {
		Enumeration attributes = attributeHash.elements();
		// Loop through all the attributes
		attributeLoop: while (attributes.hasMoreElements()) {
			Attribute theAttribute = (Attribute) attributes.nextElement();
			String className = theAttribute.getClass().getName();

			// If this attribute is to be excluded, skip this item in the list
			if (attributesToExclude != null)
				excludeLoop: for (int i = 0; i < attributesToExclude.length; i++)
					if (attributesToExclude[i].equals(theAttribute.getAttributeName()))
						continue attributeLoop;

			// Only perform validation for regular attributes (not subclassed from Attribute)
			// We can't use instanceof here because then all
			// subclasses of Attributes would return true for "xxx instanceof Attribute"
			// Exclude attributes that are listed to be excluded
			if (className.equals("com.amsinc.ecsg.frame.Attribute")) {
				// IAZ 01/12/10 Begin Make Sure if alias is not registered, execution still proceeds
				String aliasToUse = theAttribute.getAlias();
				if ((aliasToUse == null) && (prependTextForError == null))
					aliasToUse = theAttribute.getAttributeName();
				if (StringFunction.isBlank(aliasToUse))
					aliasToUse = "attribute:";
				// IAZ 01/12/10 End
				checkForInvalidSwiftCharacters(theAttribute.getAttributeValue(), errMgr,
				// theAttribute.getAlias(), //IAZ 01/12/10 CHF
				aliasToUse, // IAZ 01/12/10 CHT
						prependTextForError);
			}
		}
	}

	// GGAYLE - DFUH031368817 - 04/03/2007
	/**
	 * Performs a validation to determine if any invalid Swift characters exists in the text parameter. This is used for the Auto LC
	 * creation process.
	 * 
	 * @param textToCheck
	 *            - the text to check for invalid Swift characters
	 */
	public static String checkAutoLCForInvalidSwiftCharacters(String textToCheck) throws AmsException {
		// Only instantiate this if there are invalid characters... most of the time we won't need to
		Set invalidCharsFound = null;
		// Convert the string to be validated to a character array for faster processing
		char[] chars = textToCheck.toCharArray();
		// Loop through each character in the string, making sure that
		// it matches one of the characters in the valid SWIFT character set
		for (int i = 0; i < chars.length; i++) {
			// If upper and lower case English characters are allowed
			// and this character is one of those, continue the loop
			// and don't add to the list of bad characters
			// We check ranges here because it is much faster than checking
			// to see if the character is one of 52 characters.
			if (allowUpperAndLowerCaseEnglishLetters
					&& (((chars[i] >= 'a') && (chars[i] <= 'z')) || ((chars[i] >= 'A') && (chars[i] <= 'Z')))) {
				continue;
			}
			// If the character is one of the additional characters that are allowed,
			// continue the loop and don't add to the list of bad characters
			else if (additionalValidCharacters.indexOf(chars[i]) >= 0) {
				continue;
			} else {
				// If there's a match, add the invalid character to the list
				// Sets make sure that there will be no duplicates in the list
				if (invalidCharsFound == null)
					invalidCharsFound = new HashSet(4);

				invalidCharsFound.add(String.valueOf(chars[i]));
			}
		}

		// Check if any invalid characters were found
		if (invalidCharsFound != null) {
			// Build the list of invalid characters for display in the error message
			StringBuilder listOfInvalidCharacters = new StringBuilder();
			Iterator iter = invalidCharsFound.iterator();
			while (iter.hasNext()) {
				listOfInvalidCharacters.append(iter.next());
				if (iter.hasNext())
					listOfInvalidCharacters.append(" ,");
			}
			// Return the string of invalid characters
			return listOfInvalidCharacters.toString();
		}
		return null;
	}

	// GGAYLE - DFUH031368817 - 04/03/2007

	/**
	 * This method returns all transaction types that can be created on a given instrument (based on the original transaction oid
	 * and the instrument type).
	 * 
	 * @param String
	 *            instrType = instrument.instrument_type_code
	 * @param String
	 *            origTrans = instrument.original_transaction_oid
	 */
	public static List<String> getPossibleTransactionTypes(String instrType, String origTrans) throws AmsException {
		LOG.debug("Getting all possible transaction types for instrument type {} with original transaction oid of {} ", instrType, origTrans);
		List<String> tranList = new ArrayList<>();
		boolean firstTransaction = StringFunction.isBlank(origTrans);

		if (instrType.equals(InstrumentType.IMPORT_DLC)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else
				tranList.add(TransactionType.AMEND);
			tranList.add(TransactionType.SIR);
		}
		// rkrishna CR 375-D ATP 07/21/2007 Begin
		if (instrType.equals(InstrumentType.APPROVAL_TO_PAY)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else
				tranList.add(TransactionType.AMEND);
		}
		// rkrishna CR 375-D ATP 07/21/2007 End
		if (instrType.equals(InstrumentType.STANDBY_LC)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else
				tranList.add(TransactionType.AMEND);
		}
		if (instrType.equals(InstrumentType.GUARANTEE)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else
				tranList.add(TransactionType.AMEND);
			tranList.add(TradePortalConstants.CONVERTED_TRANSACTION_PAYMENT);
		}
		if (instrType.equals(InstrumentType.AIR_WAYBILL)) {
			if (firstTransaction)
				tranList.add(TransactionType.RELEASE);
		}
		if (instrType.equals(InstrumentType.SHIP_GUAR)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
		}
		if (instrType.equals(InstrumentType.EXPORT_COL)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else {
				tranList.add(TransactionType.AMEND);
				tranList.add(TransactionType.TRACER);
			}
		}
		// Vasavi CR 524 03/31/2010 Begin
		if (instrType.equals(InstrumentType.NEW_EXPORT_COL)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else {
				tranList.add(TransactionType.AMEND);
				tranList.add(TransactionType.TRACER);
			}
		}
		// Vasavi CR 524 03/31/2010 End
		if (instrType.equals(InstrumentType.EXPORT_DLC)) {
			if (firstTransaction)
				tranList.add(TransactionType.TRANSFER);
			else {
				try {
					// Because the type of "amendment" that is possible for a Export LC depends on the type of the
					// original transaction. We need to instantiate a transaction EJB to get the type
					String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
					Transaction trans = (Transaction) EJBObjectFactory.createClientEJB(serverLocation, "Transaction", Long.parseLong(origTrans));
					String transType = trans.getAttribute("transaction_type_code");
					LOG.debug("Export LC original transaction type: {}", transType);

					if (transType.equals(TransactionType.TRANSFER))
						tranList.add(TransactionType.AMEND_TRANSFER);
					else
						tranList.add(TransactionType.ASSIGNMENT);

					trans.remove();
				} catch (Exception e) {
					LOG.error("Exception thrown getting transaction_type_code in InstrumentServices.getPossibleTransactionTypes() ", e);
				}
			}
		}
		if (instrType.equals(InstrumentType.LOAN_RQST)) {
			if (firstTransaction) {
				tranList.add(TransactionType.ISSUE);
			} else {
				tranList.add(TransactionType.SIM);
				tranList.add(TransactionType.SIR);
			}
		}
		if (instrType.equals(InstrumentType.FUNDS_XFER)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
		}
		// CR-451 NShrestha 12/03/2008 Begin
		if (instrType.equals(InstrumentType.XFER_BET_ACCTS)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
		}
		// CR-451 NShrestha 12/03/2008 End
		// IAZ CM 10/29/08 Starts
		if (instrType.equals(InstrumentType.DOM_PMT)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
		}
		// IAZ CM 10/29/08 Ends
		if (instrType.equals(InstrumentType.REQUEST_ADVISE)) {
			if (firstTransaction)
				tranList.add(TransactionType.ISSUE);
			else
				tranList.add(TransactionType.AMEND);
		}
		if (instrType.equals(InstrumentType.IMPORT_COL) || instrType.equals(InstrumentType.DOCUMENTARY_BA)
				|| instrType.equals(InstrumentType.DEFERRED_PAY)) {
			tranList.add(TransactionType.SIR);
			tranList.add(TransactionType.SIM);
		}
		return tranList;
	}

	/**
	 * Looks at the transaction status and determines if it is an editable status
	 * 
	 * @return boolean - true: is editable
	 * @param transStatus
	 *            java.lang.String - status to test
	 */
	public static boolean isEditableTransStatus(String transStatus) {
		if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
			return false;
		if (transStatus.equals(TransactionStatus.PARTIALLY_AUTHORIZED))
			return false;
		if (transStatus.equals(TransactionStatus.AUTHORIZED))
			return false;
		if (transStatus.equals(TransactionStatus.FVD_AUTHORIZED))
			return false; // VS CR 609 11/23/10
		if (transStatus.equals(TransactionStatus.AUTHORIZE_PENDING))
			return false;
		if (transStatus.equals(TransactionStatus.AUTHORIZE_FAILED))
			return false;
		if (transStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
			return false;
		if (transStatus.equals(TransactionStatus.DELETED))
			return false;
		if (transStatus.equals(TransactionStatus.PROCESSED_BY_BANK))
			return false;
		if (transStatus.equals(TransactionStatus.STARTED))
			return true;
		if (transStatus.equals(TransactionStatus.REJECTED_BY_BANK))
			return true;
		if (transStatus.equals(TransactionStatus.TRANSACTION_PROCESSING))
			return false;
		if (transStatus.equals(TransactionStatus.VERIFIED))
			return true; // VShah - CR564
		if (transStatus.equals(TransactionStatus.VERIFIED_PENDING_FX))
			return true;// VShah - CR564
		return false;
	}

	/**
	 * Checks the given instrument type to see if the Portal can create an instrument of that type. Returns true if the Portal can
	 * create the instrument type. Returns false if the instrument type must be created by OTL
	 * 
	 * @return boolean - true: Portal can create, false: Portal cannot create
	 * @param instrumentType
	 *            java.lang.String - instrument type to test
	 */
	public static boolean canCreateInstrumentType(String instrumentType)
	// This function checks the instrumentType to see if it can be created by the portal
	{
		if (instrumentType == null)
			return false;
		if (instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.STANDBY_LC)
				|| instrumentType.equals(InstrumentType.INCOMING_SLC) || instrumentType.equals(InstrumentType.GUARANTEE)
				|| instrumentType.equals(InstrumentType.AIR_WAYBILL) || instrumentType.equals(InstrumentType.SHIP_GUAR)
				|| instrumentType.equals(InstrumentType.EXPORT_DLC) || instrumentType.equals(InstrumentType.EXPORT_COL)
				|| instrumentType.equals(InstrumentType.NEW_EXPORT_COL) || instrumentType.equals(InstrumentType.LOAN_RQST)
				|| instrumentType.equals(InstrumentType.FUNDS_XFER) || instrumentType.equals(InstrumentType.DOM_PMT)
				|| instrumentType.equals(InstrumentType.REQUEST_ADVISE) || instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) || // rkrishna
																																	// CR
																																	// 375-D
																																	// ATP-ISS
																																	// 07/21/2007
				instrumentType.equals(InstrumentType.XFER_BET_ACCTS) || // CR-451 NShrestha 10/27/2008 --
				instrumentType.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION) || // NSX CR-509 12/15/2009
				instrumentType.equals(InstrumentType.IMPORT_COL) || // CR 818
				instrumentType.equals(InstrumentType.DOCUMENTARY_BA) || // CR 818
				instrumentType.equals(InstrumentType.DEFERRED_PAY)) // CR 818
			return true;
		else
			return false;
	}

	/**
	 * This function takes the instrument type and returns the type of its original transaction
	 * 
	 * @return java.lang.String - the original transaction type
	 * @param instrumentType
	 *            java.lang.String - instrument type to convert
	 */
	public static String getOriginalTransType(String instrumentType) {
		// Set the transaction type
		if (instrumentType.equals(InstrumentType.AIR_WAYBILL))
			return TransactionType.RELEASE;
		else if (instrumentType.equals(InstrumentType.EXPORT_DLC))
			return TransactionType.TRANSFER;
		return TransactionType.ISSUE;
	}

	/**
	 * Returns true if the given value is null or a blank or white space string
	 * 
	 * @return boolean - true: string is blank, null or white space
	 * @param value
	 *            java.lang.String - value to test
	 * @deprecated use StringFunction.isBlank instead
	 */
	public static boolean isBlankOrWhiteSpace(String value) {
		return isBlank(value) || value.trim().length() < 1;
	}

	/**
	 * Returns true if the given value is null or a blank string
	 * 
	 * @return boolean - true: string is blank or null
	 * @param value
	 *            java.lang.String - value to test
	 * @deprecated use StringFunction.isBlank instead
	 */
	public static boolean isBlank(String value) {
		if (value == null)
			return true;
		if (value.trim().length() < 1)
			return true; // Narayan-LIUK031741938
		return false;
	}

	/**
	 * Returns true if the given value is NOT null or a blank string
	 * 
	 * @return boolean - string is non blank
	 * @param value
	 *            java.lang.String - value to test
	 * @deprecated use StringFunction.isNotBlank instead
	 */
	public static boolean isNotBlank(String value) {
		return !isBlank(value);
	}

	/**
	 * 
	 * This method checks an attribute type against known non-data attribute types that should not be copied from one object to
	 * another. Attributes that are associations, object ids, optimistic locking, or parent ids are classified as 'non-data'
	 * attributes.
	 * 
	 * @return boolean - attribute is a data attribute
	 * @param thisAttribute
	 *            Attribute - attribute to test
	 */
	private static boolean isDataAttribute(Attribute thisAttribute) {
		if (thisAttribute instanceof AssociationAttribute || thisAttribute instanceof ObjectIDAttribute
				|| thisAttribute instanceof OptimisticLockAttribute || thisAttribute instanceof ParentIDAttribute)
			return false;

		return true;
	}

	/**
	 * 
	 * This method takes a hastable of registered attributes and returns a array of the attribute names and an array of their
	 * values. Those arrays are stored in in a vector and returned. This method is specifically designed to facilitate the copying
	 * of business objects. Associations, oids, etc. are not processed.
	 * 
	 * @param hash
	 *            java.util.Hashtable
	 * @return java.util.Vector
	 */
	public static Vector attributeHashProcessor(Hashtable hash) {
		// get an Enumeration of the keys
		Enumeration attributeList = hash.elements();
		Vector nameVector = new Vector();
		Vector valVector = new Vector();

		// Cycle through the key Enumeration storing the key and element in
		// the proper arrays.
		while (attributeList.hasMoreElements()) {
			// get the next attribute
			Attribute thisAttribute = (Attribute) attributeList.nextElement();

			// Only process data attributes
			if (isDataAttribute(thisAttribute)) {
				// we use vectors here because the size is dynamic and at
				// this point we don't know how many DATA attributes there are.
				nameVector.addElement(thisAttribute.getAttributeName());
				valVector.addElement(thisAttribute.getAttributeValue());
			}
		}
		String[] name = new String[nameVector.size()];
		String[] value = new String[valVector.size()];

		for (int i = 0; i < nameVector.size(); i++) {
			name[i] = (String) nameVector.elementAt(i);
			value[i] = (String) valVector.elementAt(i);
		}

		// Store the arrays in a vector to be returned.
		Vector returnVector = new Vector();
		returnVector.addElement(name);
		returnVector.addElement(value);

		return returnVector;
	}

	/**
	 * This method parses the instrument oid from the string returned by the instrument search page. The original string is in the
	 * form oid/instrumentId for example 1234/IMP5678BOM.
	 * 
	 * @return String - the oid portion of the passed in parameter
	 * @param oidAndID
	 *            String - the oid and id to parse
	 */

	public static String parseForOid(String oidAndId) {
		String returnString = null;

		if (oidAndId == null)
			return null;
		StringTokenizer st = new StringTokenizer(oidAndId, "/");
		try {
			returnString = st.nextToken();
		} catch (Exception e) {
			returnString = oidAndId;
		}
		return returnString;
	}

	/**
	 * This method parses the instrument ID from the string returned by the instrument search page. The original string is in the
	 * form oid/instrumentId (for example: 1234/IMP5678BOM). Instrument Id may contains / (e.g., 1234/IMP5678/BOM).
	 * 
	 * @return String - the id portion of the passed in parameter
	 * @param oidAndID
	 *            String - the oid and id to parse
	 */
	public static String parseForInstrumentId(String oidAndId) {
		StringTokenizer stringTokenizer = null;
		String instrumentOid = null;
		String instrumentId = null;

		if (oidAndId == null) {
			return null;
		}
		stringTokenizer = new StringTokenizer(oidAndId, "/");
		try {
			instrumentOid = stringTokenizer.nextToken();
			instrumentId = stringTokenizer.nextToken();

			// The instrument id may contain a slash. In this case,
			// continue processing tokens and appending to the instrument
			// id with a / separator.
			while (stringTokenizer.hasMoreTokens()) {
				instrumentId += "/";
				instrumentId += stringTokenizer.nextToken();
			}
		} catch (Exception e) {
			return oidAndId;
		}
		return instrumentId;
	}

	/**
	 * Although called from many places, this method exists to support the Standby-as-Guarantee logic. Its purpose is to translate
	 * the passed in instrument type into the logical instrument type. In general it will be the same value. However, for the
	 * following types we convert to a different instrument type:<br>
	 * SIMPLE_SLC, becomes STANDBY_LC<br>
	 * DETAILED_SLC becomes GUARANTEE<br>
	 * SLC_IS_GUA becomes GUARANTEE
	 * 
	 * @return java.lang.String - the logical instrument type
	 * @param instrumentType
	 *            java.lang.String - instrument type to convert
	 */
	public static String getLogicalInstrumentType(String instrumentType) {
		if (instrumentType.equals(TradePortalConstants.SIMPLE_SLC)) {
			return InstrumentType.STANDBY_LC;
		} else if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)) {
			return InstrumentType.GUARANTEE;
		} else if (instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
			return InstrumentType.GUARANTEE;
		}
		return instrumentType;
	}

	/**
	 * Although called from many places, this method exists to support the Standby-as-Guarantee logic. It's purpose is to translate
	 * the passed in instrument type into the physical instrument type. In general it will be the same value. However, for the
	 * following types we convert to a different instrument type:<br>
	 * SIMPLE_SLC, becomes STANDBY_LC<br>
	 * DETAILED_SLC becomes STANDBY_LC<br>
	 * SLC_IS_GUA becomes STANDBY_LC
	 * 
	 * @return java.lang.String - the physical instrument type
	 * @param instrumentType
	 *            java.lang.String - the instrument type to convert
	 */
	public static String getPhysicalInstrumentType(String instrumentType) {
		if (instrumentType != null) {
			if (instrumentType.equals(TradePortalConstants.SIMPLE_SLC)) {
				return InstrumentType.STANDBY_LC;
			} else if (instrumentType.equals(TradePortalConstants.DETAILED_SLC)) {
				return InstrumentType.STANDBY_LC;
			} else if (instrumentType.equals(TradePortalConstants.SLC_IS_GUA)) {
				return InstrumentType.STANDBY_LC;
			}
		}
		return instrumentType;
	}

	/**
	 * Validates a decimal number to ensure that it has the proper number of digits to the left and right of the decimal point
	 * 
	 * @param number
	 *            - the number being validated
	 * @param maxLengthBeforeDecimalPoint
	 * @param maxLengthAfterDecimalPoint
	 * @param aliasTextResourceKey
	 *            - text resource key of the alias for the field being checked
	 * @param errMgr
	 *            - the error manager for the business object on which the field is being validated
	 * @param resMgr
	 *            - the resource manager for the user
	 * @exception AmsException
	 */
	public static void validateDecimalNumber(String number, int maxLengthBeforeDecimalPoint, int maxLengthAfterDecimalPoint, String aliasTextResourceKey, ErrorManager errMgr, ResourceManager resMgr) throws AmsException {
		int length = number.length();
		String decimalPoint = ".";
		int indexOfDecimalPoint = number.indexOf(decimalPoint);
		int indexOfFirstCharacterAfterDecimalPoint = indexOfDecimalPoint + 1;

		// If there is a decimal point, check digits to the left and to the righ
		if (indexOfDecimalPoint >= 0) {
			if ((length - indexOfFirstCharacterAfterDecimalPoint) > maxLengthAfterDecimalPoint) {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DECIMAL_PLACE, resMgr.getText(aliasTextResourceKey, TradePortalConstants.TEXT_BUNDLE), String.valueOf(maxLengthAfterDecimalPoint));
			}
			if (indexOfDecimalPoint > maxLengthBeforeDecimalPoint) {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DECIMAL_PLACE_LEFT, resMgr.getText(aliasTextResourceKey, TradePortalConstants.TEXT_BUNDLE), String.valueOf(maxLengthBeforeDecimalPoint));
			}
		} else {
			// Otherwise, only check the length against the digits to the left of the dec pt
			if (length > maxLengthBeforeDecimalPoint) {
				errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DECIMAL_PLACE_LEFT, resMgr.getText(aliasTextResourceKey, TradePortalConstants.TEXT_BUNDLE), String.valueOf(maxLengthBeforeDecimalPoint));
			}
		}
	}

	/*
	 * Generates unique messageID and returns it
	 */

	public static String getNewMessageID() throws AmsException {
		String messageId = Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false));
		/* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
		/* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
		PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
		String serverName = portalProperties.getString("serverName");
		messageId += serverName.substring(serverName.length() - 2);
		return messageId;
	}

	// CR-451 NShrestha 12/03/2008 Begin
	/*
	 * Converts given string value to BigDecimal. If string value is blank or null, it returns zero.
	 */
	public static BigDecimal convertToDecimal(String val) {
		BigDecimal decimal;
		if (StringFunction.isBlank(val)) {
			decimal = BigDecimal.ZERO;
		} else {
			decimal = new BigDecimal(val);
		}
		return decimal;
	}

	// CR-451 NShrestha 12/03/2008 End

	// CR-597 Manohar 04/14/2011 Begin
	/**
	 * Returns true if the given the indicator value is Y or N
	 * 
	 * @return boolean - true: string is blank or null
	 * @param indicator
	 *            java.lang.String - indicator to test
	 */
	// RKAZI IR RRUL052435989 05/26/2011 Added boolean to allow users to indicate if case is to be ignored - Start
	public static boolean validateIndicator(String indicator, boolean shouldIgnoreCase) {
		if (shouldIgnoreCase) {
			if (indicator.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES)
					|| indicator.equalsIgnoreCase(TradePortalConstants.INDICATOR_NO)) {
				return true;
			}
		} else {
			if (indicator.equals(TradePortalConstants.INDICATOR_YES) || indicator.equals(TradePortalConstants.INDICATOR_NO)) {
				return true;
			}
		}
		return false;
	}

	// RKAZI IR RRUL052435989 05/26/2011 Added boolean to allow users to indicate if case is to be ignored - End
	// CR-597 Manohar 04/14/2011 End

	// IAZ REL 7.0 06/17/11
	/**
	 * Validates if string is alphanumeric. Ignores Single Space Char Takes boolean param re whether to consider native chars -
	 * alphanumeric Returns first non-alphanumeric char on error or empty string on suceess
	 * 
	 * @return java.lang.String - bad char or empty string
	 * @param indicator
	 *            java.lang.String - input String
	 * @param boolean - is native chars ok?
	 */
	public static String isAlphaNumeric(String inputString, boolean nativeOK) {
		int strLen = inputString.length();
		for (int i = 0; i < strLen; i++) {
			if (inputString.charAt(i) == ' ') {
				continue;
			}
			if (nativeOK) {
				if (!Character.isDigit(inputString.charAt(i)) && !Character.isLetter(inputString.charAt(i))) {
					char returnChar[] = { inputString.charAt(i) };
					return (new String(returnChar));
				}
			} else {
				if ((inputString.charAt(i) >= '0') && ((inputString.charAt(i) <= '9')))
					continue;
				if ((inputString.charAt(i) >= 'a') && ((inputString.charAt(i) <= 'z')))
					continue;
				if ((inputString.charAt(i) >= 'A') && ((inputString.charAt(i) <= 'Z')))
					continue;
				char returnChar[] = { inputString.charAt(i) };
				return (new String(returnChar));
			}
		}
		return "";
	}

	/**
	 * @param instrumentType
	 * @return
	 */
	public static boolean isCashManagementTypeInstrument(String instrumentType) {
		return (instrumentType.equals(InstrumentType.DOM_PMT) || instrumentType.equals(InstrumentType.FUNDS_XFER) || instrumentType.equals(InstrumentType.XFER_BET_ACCTS));
	}

	public static boolean isTradeTypeInstrument(String instrumentType) {
		return (instrumentType.equals(InstrumentType.AIR_WAYBILL) || instrumentType.equals(InstrumentType.EXPORT_COL)
				|| instrumentType.equals(InstrumentType.NEW_EXPORT_COL) || instrumentType.equals(InstrumentType.GUARANTEE)
				|| instrumentType.equals(InstrumentType.IMPORT_DLC) || instrumentType.equals(InstrumentType.STANDBY_LC)
				|| instrumentType.equals(InstrumentType.EXPORT_DLC) || instrumentType.equals(InstrumentType.INCOMING_SLC)
				|| instrumentType.equals(InstrumentType.CLEAN_BA) || instrumentType.equals(InstrumentType.DOCUMENTARY_BA)
				|| instrumentType.equals(InstrumentType.REFINANCE_BA) || instrumentType.equals(InstrumentType.INDEMNITY)
				|| instrumentType.equals(InstrumentType.DEFERRED_PAY) || instrumentType.equals(InstrumentType.COLLECT_ACCEPT)
				|| instrumentType.equals(InstrumentType.INCOMING_GUA) || instrumentType.equals(InstrumentType.IMPORT_COL)
				|| instrumentType.equals(InstrumentType.SHIP_GUAR) || instrumentType.equals(InstrumentType.LOAN_RQST)
				|| instrumentType.equals(InstrumentType.REQUEST_ADVISE) || instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) || instrumentType.equals(InstrumentType.DEFERRED_PAY));
	}

	// IR# SEUL122073113 Rel7.1.0 12/01/12 - Begin -
	/**
	 * This method returns a sell rate from FX rate table for given currency, rate group, and owner org.
	 * 
	 * @param account
	 *            Account - the account
	 * @param currency
	 *            String - the foreign currency
	 * @param rateType
	 *            String - the rate type
	 * @return BigDecimal - the rate
	 */

	public static BigDecimal getFXRate(Account account, String currency, String rateType) throws RemoteException, AmsException {
		BigDecimal bigDecRate = null;
		String rate = getFXRate(currency, account, rateType);
		if (StringFunction.isNotBlank(rate)) {
			bigDecRate = new BigDecimal(rate);
		}
		return bigDecRate;
	}

	/**
	 * This method returns a sell rate from FX rate table for given currency, rate group, and owner org.
	 * 
	 * @param currency
	 *            String - the foreign currency
	 * @param account
	 *            Account - the account
	 * @param rateType
	 *            String - the rate type
	 * @return String - the rate
	 */

	public static String getFXRate(String currency, Account account, String rateType) throws RemoteException, AmsException {
		String rateTypePath;
		if (TradePortalConstants.USE_SELL_RATE.equals(rateType)) {
			rateTypePath = "/SELL_RATE";
		} else if (TradePortalConstants.USE_BUY_RATE.equals(rateType)) {
			rateTypePath = "/BUY_RATE";
		} else {
			rateTypePath = "/RATE";
		}

		// MDB PIUL102167761 Rel7.1 10/21/11 Begin
		String corpOrgOid = account.getOwnerOfAccount();
		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corpOrgOid + "|" + currency);
		// MDB PIUL102167761 Rel7.1 10/21/11 End

		if (resultSet != null) {
			DocumentHandler fxRate = resultSet.getFragment("/ResultSetRecord");
			if (fxRate != null)
				return fxRate.getAttribute(rateTypePath);
		}
		return null;
	}

	// IR# SEUL122073113 Rel7.1.0 12/01/12 - End -

	/**
	 * This method checks if a String contains only numbers -AAlubala
	 */
	public static boolean containsOnlyNumbers(String str) {
		// It can't contain only numbers if it's null or empty...
		if (str == null || str.length() == 0)
			return false;

		for (int i = 0; i < str.length(); i++) {
			// If we find a non-digit character we return false.
			if (!Character.isDigit(str.charAt(i)))
				return false;
		}
		return true;
	}

	// BSL IR LIUM050240824 05/07/2012 Rel 8.0 BEGIN
	/**
	 * Provides the margin rule that most closely matches the supplied instrument type, currency, and amount. All parameters are
	 * required and must be non-blank. First attempts to find the margin rule that most closely matches the supplied parameters and
	 * that belongs to the supplied trading partner rule. If no trading partner matches are found, attempts to find the closest
	 * matching customer margin rule.
	 * 
	 * Margin rules can have blank instrument type, currency, and/or amount values. The following criteria should be used to
	 * determine which rule takes precedence when comparing rules that have blank values:
	 * 
	 * 1) When choosing between two margin rules, one with Instrument Type and the other without, the one with Instrument Type
	 * ALWAYS takes precedence. 2) When choosing between two margin rules, one with Currency and the other without, the one with
	 * Currency ALWAYS takes precedence, EXCEPT when rule (1) applies. 3) When choosing between two margin rules, one with Threshold
	 * Amount (even 0) and the other without, the one with Threshold Amount ALWAYS takes precedence, EXCEPT when rule (1) applies.
	 * 
	 * @param instrumentType
	 * @param currency
	 * @param amount
	 * @param tradingPartnerRuleOid
	 * @param corpOrgOid
	 * @return null, if no matches are found in trading partner or customer margin rules; otherwise, an XML document of the form
	 *         {@code
	 * <ResultSetRecord>
	 *   <RATE_TYPE>PRM</RATE_TYPE>
	 *   <MARGIN>0.8</MARGIN>
	 * </ResultSetRecord>
     * }
	 */
	public static DocumentHandler getMarginAndRateType(String instrumentType, String currency, String amount, String tradingPartnerRuleOid, String corpOrgOid) throws AmsException, RemoteException {
		final String SELECT = "SELECT RATE_TYPE, MARGIN FROM ";
		String sqlQuery = SELECT + "(" + SELECT + " trading_partner_margin_rules WHERE p_ar_matching_rule_oid = ? "
				+ " AND (margin_instrument_type IS NULL OR margin_instrument_type = ? "
				+ ") AND (currency_code IS NULL OR currency_code = ?"
				+ ") AND (threshold_amount IS NULL OR threshold_amount <= ?) "
				+ "ORDER BY margin_instrument_type, currency_code, ABS(threshold_amount - ?)) WHERE rownum = 1";

		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, tradingPartnerRuleOid, instrumentType, currency, amount, amount);

		if (resultsDoc == null) {
			sqlQuery = SELECT + "(" + SELECT + "customer_margin_rule WHERE p_owner_oid = ?"
					+ " AND (instrument_type IS NULL OR instrument_type = ?" + ") AND (currency IS NULL OR currency = ?"
					+ ") AND (threshold_amt IS NULL OR threshold_amt <= ?"
					+ ") ORDER BY instrument_type, currency, ABS(threshold_amt - ?)) WHERE rownum = 1";

			resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, corpOrgOid, instrumentType, currency, amount, amount);
		}

		if (resultsDoc != null) {
			resultsDoc = resultsDoc.getFragment("/ResultSetRecord");
		}
		return resultsDoc;
	}

	// BSL IR LIUM050240824 05/07/2012 Rel 8.0 END
	// AAlubala Rel8.0 IR_WZUM060733306 - 05/17/2012
	/**
	 * This method determines if the Instrument/Transaction Authorize Button that was pressed was for Offline Authorization
	 * 
	 * @param buttonPressed
	 *            - The Button pressed
	 * @return true or false - true if it is an offline button, false otherwise
	 * 
	 */
	public static boolean isAnOfflineButton(String buttonPressed) {
		if (TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_INVOICES.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_CREDIT_NOTE.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_PROXY_AUTHORIZE.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_PROXY_APPROVEDISCOUNTAUTHORIZE.equalsIgnoreCase(buttonPressed)
				|| "ProxyAuthorize".equalsIgnoreCase(buttonPressed)) {
			return true;
		}
		return false;
	}

	public static boolean isNull(String value) {
		if (value == null)
			return true;
		if (value.trim().length() < 1)
			return true;
		if ("null".equals(value))
			return true;
		return false;
	}

	/**
	 * this us method is used to identify whether transactionStatus is a valid status to set status as verified for upload payment
	 * file when user perform Edit action.
	 * 
	 * @param transactionStatus
	 *            - transaction current status.
	 * @return true if it is valid status false if it is not a valid status
	 */
	public static boolean isValidTransStatusForVerifiedStatus(String transactionStatus) {
		boolean isValidStatus = false;
		if (TransactionStatus.READY_TO_AUTHORIZE.equals(transactionStatus)
				|| TransactionStatus.AUTHORIZE_FAILED.equals(transactionStatus) || // MDB Rel6.1 IR# PKUL012175538
				TransactionStatus.FVD_AUTHORIZED.equals(transactionStatus) || // cquinton Rel 6.1.0 ir#haul020463388
				TransactionStatus.PARTIALLY_AUTHORIZED.equals(transactionStatus) || // Nar Rel 8.3.0.0
																									// ir#T36000020408
				TransactionStatus.READY_TO_CHECK.equals(transactionStatus) || // Nar Rel 8.3.0.0 ir#T36000020408
				TransactionStatus.REPAIR.equals(transactionStatus))// Nar Rel 8.3.0.0 ir#T36000020408
		{
			isValidStatus = true;
		}
		return isValidStatus;
	}

	// Nar IR-T36000020596 10-Sep-2013 ADD- BEGIN
	// moved IR-T36000015559 code from DomesticPayment.verifyDomesticPayment() to here. in DomesticPayment.verifyDomesticPayment,
	// it was executing for every beneficiary while it is related to transaction level validation.
	// it will validate at transaction time and will improve performance.
	/**
	 * this method check whether currency is present in FXGroup.
	 * 
	 * @param corpOrg
	 * @param currencycode
	 * @param errorMgr
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static void verifyCurrencyDefineForFXGroup(CorporateOrganization corpOrg, String currencycode, ErrorManager errorMgr) throws AmsException, RemoteException {
		// IR #T36000015559 Starts Rpasupulati.
		// it Will check currencies defined for a particular FX Group.
		boolean isDefined = false;
		String fxrategroup = corpOrg.getAttribute("fx_rate_group");
		String clientbankid = corpOrg.getAttribute("client_bank_oid");
		Hashtable<String, String> currencies = null;

		if (StringFunction.isNotBlank(fxrategroup)) {
			currencies = getFxCurrenciesForGroup(fxrategroup, clientbankid);
			if (currencies != null) {
				Iterator<String> it = currencies.keySet().iterator();
				Iterator<String> bc = currencies.values().iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					String basecurrency = (String) bc.next();
					if (currencycode.equals(key) || currencycode.equals(basecurrency)) {
						isDefined = true;
						break;
					}
				}
			}
		}
		if (!isDefined) {
			// throw error if not present
			errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_BUY_FOREX_DEFINED);
		}
		// IR #T36000015559 End Rpasupulati
	}

	/**
	 * This method has been added for IR # T36000015559 This method will get the currencies defined for a particular FX Group.
	 * 
	 * @param fxGroup
	 * @param organizationOid
	 * @throws AmsException
	 */
	private static Hashtable<String, String> getFxCurrenciesForGroup(String fxGroup, String clientbankOid) throws AmsException {
		String fxGroupQuery = "select currency_code,base_currency_code from fx_rate where fx_rate_group=? and p_owner_org_oid=?";
		Hashtable<String, String> currencyList = null;
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(fxGroupQuery, false, fxGroup, clientbankOid);
		if (resultDoc != null) {
			Vector<DocumentHandler> cusrrencies = resultDoc.getFragments("/ResultSetRecord");
			if (cusrrencies != null) {
				currencyList = new Hashtable<String, String>();
				for (DocumentHandler doc : cusrrencies) {
					currencyList.put(doc.getAttribute("/CURRENCY_CODE"), doc.getAttribute("/BASE_CURRENCY_CODE"));
				}
			}
		}
		return currencyList;
	}

	// Nar IR-T36000020596 10-Sep-2013 ADD- END
	// IR 21814 - get the latest shipment date from AMD if available else from ISS, but the latest one
	public static String getLatestShipmentDate(String instrOID) throws AmsException, RemoteException {

		String sqlQuery = "select s.latest_shipment_date from shipment_terms s, transaction t "
				+ "where s.p_terms_oid = t.c_bank_release_terms_oid and t.p_instrument_oid = ? "
				+ "and t.transaction_status = ? and s.latest_shipment_date is not null "
				+ "order by transaction_oid desc, transaction_status_date desc";
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, instrOID, TransactionStatus.PROCESSED_BY_BANK);
		if (resultsDoc != null) {
			resultsDoc = resultsDoc.getFragment("/ResultSetRecord");
		}
		String date = (resultsDoc != null) ? resultsDoc.getAttribute("/LATEST_SHIPMENT_DATE") : "";
		return date;
	}

	public static String getLatestPartyOid(String instrOID, String termsPartyOidColumn) throws AmsException, RemoteException {
		termsPartyOidColumn = "s." + termsPartyOidColumn;
		String sqlQuery = "select p.terms_party_oid  from terms s, terms_party p, transaction t "
				+ "where s.terms_oid = t.c_bank_release_terms_oid and  p.terms_party_oid = " + termsPartyOidColumn
				+ " and t.p_instrument_oid = ? and t.transaction_status = ? "
				+ " order by transaction_oid desc, transaction_status_date desc";
		Long logInstrOid = instrOID != null ? Long.parseLong(instrOID) : null;
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[] { logInstrOid, TransactionStatus.PROCESSED_BY_BANK });
		if (resultsDoc != null) {
			resultsDoc = resultsDoc.getFragment("/ResultSetRecord");
		}
		String oid = (resultsDoc != null) ? resultsDoc.getAttribute("/TERMS_PARTY_OID") : "";
		return oid;
	}

	/**
	 * Returns true if the given instrument type is valid for BTMU CR-1026- 6 instrument types
	 * 
	 * @return boolean - true: returns true if instrument type is valid for CR-1026
	 * @param instrument
	 *            type java.lang.String - instrument type code
	 */

	public static boolean isValidInstrumentTypeForBTMU(String instrumentOid, String instrumentTypeCode) throws AmsException {
		boolean isValid = false;
		if (InstrumentType.IMPORT_DLC.equals(instrumentTypeCode) || InstrumentType.AIR_WAYBILL.equals(instrumentTypeCode)
				|| InstrumentType.STANDBY_LC.equals(instrumentTypeCode) || InstrumentType.GUARANTEE.equals(instrumentTypeCode)
				|| InstrumentType.SHIP_GUAR.equals(instrumentTypeCode)) {
			isValid = true;
		} else if (InstrumentType.LOAN_RQST.equals(instrumentTypeCode)) {
			String sqlQuery = "select import_indicator  from instrument where instrument_oid = ? ";
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, instrumentOid);
			if (resultsDoc != null) {
				resultsDoc = resultsDoc.getFragment("/ResultSetRecord");
			}
			String invFinancing = (resultsDoc != null) ? resultsDoc.getAttribute("/IMPORT_INDICATOR") : "";
			if (TradePortalConstants.INDICATOR_X.equals(invFinancing))
				isValid = true;
		}
		return isValid;
	}

	/**
	 * Rel 9.3.5 Check for bank transaction status and returns true if its an editable status.
	 * 
	 * @param bankTransStatus
	 * @return boolean
	 */
	public static boolean isEditableBankTransStatus(String bankTransStatus) {
		if (bankTransStatus.equals(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPLIED))
			return true;
		if (bankTransStatus.equals(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_NOT_STARTED))
			return true;
		if (bankTransStatus.equals(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_IN_PROGRESS))
			return true;
		if (bankTransStatus.equals(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_REPAIR))
			return true;
		return false;
	}
}