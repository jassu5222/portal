package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.ams.tradeportal.common.InvoiceFileDetails;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;

public class InvoiceLineItemObject {
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceLineItemObject.class);

	private long upload_line_item_detail_oid;
	private String line_item_id;
	private BigDecimal unit_price;
	private String unit_of_measure_price;
	private BigDecimal inv_quantity;
	private String unit_of_measure_quantity;
	private String prod_chars_ud1_type;
	private String prod_chars_ud2_type;
	private String prod_chars_ud3_type;
	private String prod_chars_ud4_type;
	private String prod_chars_ud5_type;
	private String prod_chars_ud6_type;
	private String prod_chars_ud7_type;
	private String prod_chars_ud1_val;
	private String prod_chars_ud2_val;
	private String prod_chars_ud3_val;
	private String prod_chars_ud4_val;
	private String prod_chars_ud5_val;
	private String prod_chars_ud6_val;
	private String prod_chars_ud7_val;
	private String derived_line_item_ind;

	private long p_invoice_summary_goods_oid;
	private Map fields = null;
	private boolean isValid = true;

	public BigDecimal getInv_quantity() {
		return inv_quantity;
	}

	public void setInv_quantity(BigDecimal inv_quantity) {
		this.inv_quantity = inv_quantity;
	}

	public String getLine_item_id() {
		return line_item_id;
	}

	public void setLine_item_id(String line_item_id) {
		this.line_item_id = line_item_id;
	}

	public long getP_invoice_summary_goods_oid() {
		return p_invoice_summary_goods_oid;
	}

	public void setP_invoice_summary_goods_oid(long p_invoice_summary_goods_oid) {
		this.p_invoice_summary_goods_oid = p_invoice_summary_goods_oid;
	}

	public String getProd_chars_ud1_type() {
		return prod_chars_ud1_type;
	}

	public void setProd_chars_ud1_type(String prod_chars_ud1_type) {
		this.prod_chars_ud1_type = prod_chars_ud1_type;
	}

	public String getProd_chars_ud1_val() {
		return prod_chars_ud1_val;
	}

	public void setProd_chars_ud1_val(String prod_chars_ud1_val) {
		this.prod_chars_ud1_val = prod_chars_ud1_val;
	}

	public String getProd_chars_ud2_type() {
		return prod_chars_ud2_type;
	}

	public void setProd_chars_ud2_type(String prod_chars_ud2_type) {
		this.prod_chars_ud2_type = prod_chars_ud2_type;
	}

	public String getProd_chars_ud2_val() {
		return prod_chars_ud2_val;
	}

	public void setProd_chars_ud2_val(String prod_chars_ud2_val) {
		this.prod_chars_ud2_val = prod_chars_ud2_val;
	}

	public String getProd_chars_ud3_type() {
		return prod_chars_ud3_type;
	}

	public void setProd_chars_ud3_type(String prod_chars_ud3_type) {
		this.prod_chars_ud3_type = prod_chars_ud3_type;
	}

	public String getProd_chars_ud3_val() {
		return prod_chars_ud3_val;
	}

	public void setProd_chars_ud3_val(String prod_chars_ud3_val) {
		this.prod_chars_ud3_val = prod_chars_ud3_val;
	}

	public String getProd_chars_ud4_type() {
		return prod_chars_ud4_type;
	}

	public void setProd_chars_ud4_type(String prod_chars_ud4_type) {
		this.prod_chars_ud4_type = prod_chars_ud4_type;
	}

	public String getProd_chars_ud4_val() {
		return prod_chars_ud4_val;
	}

	public void setProd_chars_ud4_val(String prod_chars_ud4_val) {
		this.prod_chars_ud4_val = prod_chars_ud4_val;
	}

	public String getProd_chars_ud5_type() {
		return prod_chars_ud5_type;
	}

	public void setProd_chars_ud5_type(String prod_chars_ud5_type) {
		this.prod_chars_ud5_type = prod_chars_ud5_type;
	}

	public String getProd_chars_ud5_val() {
		return prod_chars_ud5_val;
	}

	public void setProd_chars_ud5_val(String prod_chars_ud5_val) {
		this.prod_chars_ud5_val = prod_chars_ud5_val;
	}

	public String getProd_chars_ud6_type() {
		return prod_chars_ud6_type;
	}

	public void setProd_chars_ud6_type(String prod_chars_ud6_type) {
		this.prod_chars_ud6_type = prod_chars_ud6_type;
	}

	public String getProd_chars_ud6_val() {
		return prod_chars_ud6_val;
	}

	public void setProd_chars_ud6_val(String prod_chars_ud6_val) {
		this.prod_chars_ud6_val = prod_chars_ud6_val;
	}

	public String getProd_chars_ud7_type() {
		return prod_chars_ud7_type;
	}

	public void setProd_chars_ud7_type(String prod_chars_ud7_type) {
		this.prod_chars_ud7_type = prod_chars_ud7_type;
	}

	public String getProd_chars_ud7_val() {
		return prod_chars_ud7_val;
	}

	public void setProd_chars_ud7_val(String prod_chars_ud7_val) {
		this.prod_chars_ud7_val = prod_chars_ud7_val;
	}

	public String getUnit_of_measure_price() {
		return unit_of_measure_price;
	}

	public void setUnit_of_measure_price(String unit_of_measure_price) {
		this.unit_of_measure_price = unit_of_measure_price;
	}

	public String getUnit_of_measure_quantity() {
		return unit_of_measure_quantity;
	}

	public void setUnit_of_measure_quantity(String unit_of_measure_quantity) {
		this.unit_of_measure_quantity = unit_of_measure_quantity;
	}

	public BigDecimal getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(BigDecimal unit_price) {
		this.unit_price = unit_price;
	}

	public long getUpload_line_item_detail_oid() {
		return upload_line_item_detail_oid;
	}

	public void setUpload_line_item_detail_oid(long upload_line_item_detail_oid) {
		this.upload_line_item_detail_oid = upload_line_item_detail_oid;
	}

	public Map getFields() {
		return fields;
	}

	public void setFields(Map fields) {
		this.fields = fields;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean valid) {
		isValid = valid;
	}

	// SHR Cr708 Rel8.1.1 Start
	public String getDerived_line_item_ind() {
		return derived_line_item_ind;
	}

	public void setDerived_line_item_ind(String derived_line_item_ind) {
		this.derived_line_item_ind = derived_line_item_ind;
	}

	// SHR Cr708 Rel8.1.1 End
	public void processInvoiceLineItem(List<String> invLineItemFieldNamesList, String[] record, InvoiceDefinitionWebBean invoiceDefinition, MediatorServices mediatorServices, int lineNum) throws AmsException, RemoteException {
		InvoiceFileDetails invoiceFileDetails = new InvoiceFileDetails().getInstance(invoiceDefinition);
		invoiceFileDetails.setMediatorServices(mediatorServices);
		invoiceFileDetails.setCurrentLineNum(lineNum);
		fields = invoiceFileDetails.setFieldValues(invLineItemFieldNamesList, record, lineNum, this);

	}
}