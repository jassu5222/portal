package com.ams.tradeportal.busobj.util;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.InvoiceFileDetails;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

public class InvoiceSummaryDataObject {
private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(InvoiceSummaryDataObject.class);

	private static final int MAX_BEN_BANK_ADDRESS_SIZE = 31;
	private long upload_invoice_oid;
	private String invoice_id;
	private Date issue_date;
	private Date due_date;
	private String buyer_id;
	private String buyer_name;
    private String buyer_tps_customer_id;
	private String currency;
	private BigDecimal amount;
	private String linked_to_instrument_type;
	private Date payment_date;
	private String invoice_status;
	private BigDecimal invoice_finance_amount;
	private BigDecimal net_invoice_finance_amount;
	private BigDecimal estimated_interest_amount;
	private long amended_invoice_number;
	private Date first_authorize_status_date;
	private Date second_authorize_status_date;
	private long a_first_authorizing_user_oid;
	private long a_second_authorizing_user_oid;
	private long a_first_auth_work_group_oid;
	private long a_second_auth_work_group_oid;
	private long opt_lock;
	private String invoice_type;
	private long a_invoice_group_oid;
	private long a_corp_org_oid;
	private long p_invoice_file_upload_oid;
	private String tp_rule_name;
	// RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
	private String credit_note;
	private String pay_method;
	private String ben_acct_num;
	private String ben_add1;
	private String ben_add2;
	private String ben_add3;
	private String ben_email_addr;
	private String ben_bank_sort_code;
	private String ben_country;
	private String ben_bank_name;
	private String ben_branch_code;
	private String ben_branch_add1;
	private String ben_branch_add2;
	private String ben_bank_city;
	private String ben_bank_province;
	private String ben_branch_country;
	private String charges;
	private String central_bank_rep1;
	private String central_bank_rep2;
	private String central_bank_rep3;
	// RKAZI Rel 8.2 IR# T36000012087 02/28/2013 END
	//AiA Rel 8.2 IR T36000015016 03/18/2013 BEGIN
	private String cred_discount_code;
	private String cred_gl_code;
	private String cred_comments;
	private String credit_discount_code_id;
	private String credit_discount_gl_code_id;
	private String credit_discount_comments_id;
	//AiA Rel 8.2 IR T36000015016 03/18/2013 END
	private String loan_type;
	private String end_to_end_id;
	private String reporting_code_1;//Added for CR1001 Rel9.4
	private String reporting_code_2;//Added for CR1001 Rel9.4

	protected static final String loggerCategory = TradePortalConstants.getPropertyValue("AgentConfiguration", "loggerServerName", "NoLogCategory");
	
	public String getTp_rule_name() {
		return tp_rule_name;
	}

	public void setTp_rule_name(String tp_rule_name) {
		this.tp_rule_name = tp_rule_name;
	}
    public long getA_user_oid() {
        return a_user_oid;
    }

    public void setA_user_oid(long a_user_oid) {
        this.a_user_oid = a_user_oid;
    }

    private long a_user_oid;

	private InvoiceDefinitionWebBean invoiceDefinition;
	private Set<InvoiceSummaryGoodObject> invoiceSummaryGood;
	private Map<String, InvoiceSummaryGoodObject> invoiceSummaryGoodMap = new HashMap<String, InvoiceSummaryGoodObject>();
	private boolean isValid = true;
	private final ClientServerDataBridge csdb;
	private MediatorServices mediatorServices;
	private List<IssuedError> errorList;
	private String localName;
	private String ejbServerLocation = null;
	private boolean isNewINV = false;
	private InvoiceFileDetails invoiceFileDetail = null;
	private List<String> invSummaryFieldNamesList = null;
	private List<String> invSummaryGoodsFieldNamesList = null;
	private List<String> invLineItemFieldNamesList = null;
	//AiA Rel8.2 IR T36000015016 - 03/21/2013 - Add Credit Codes list
	private List<String> invCreditFieldNamesList = null;
	List<String> invList = new ArrayList<String>();
	private Map<String, Object> fields = new HashMap<String, Object>();
	private Set<String> goodsNums = new HashSet();
	//SHR CR708 Rel8.1.1.0 start
	private String seller_id;
	private String seller_name;
        private String seller_tps_customer_id;
	private String invoice_classification;
	private long a_upload_definition_oid;
	private String a_transaction_oid;
	private String a_instrument_oid;
	private String a_terms_oid;
	private DocumentHandler   authorisedInvSummaryDoc = null;
	//SHR end

	private Transaction invSummaryTransaction = null;
    private InvoicesSummaryData invoicesSummaryData = null;
    private InvoicesSummaryData copyOfinvoicesSummaryData = null;
    private String             instrumentType               = null;
    private Date send_to_supplier_date;
    private String buyer_acct_currency;
    private String buyer_acct_num;

    public InvoicesSummaryData getInvoicesSummaryData() {
        return invoicesSummaryData;
    }

    public void setInvoicesSummaryData(InvoicesSummaryData invoicesSummaryData) {
        this.invoicesSummaryData = invoicesSummaryData;
    }


    private static PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle(TradePortalConstants.TEXT_BUNDLE);
    
    public InvoiceSummaryDataObject(InvoiceDefinitionWebBean invoiceDefinition,
			MediatorServices mediatorServices, ClientServerDataBridge csdb,InvoiceFileDetails invoiceFileDetails,
			List<String> invSummaryFieldNamesList,List<String> invSummaryGoodsFieldNamesList,List<String> invLineItemFieldNamesList,List<String> invCreditFieldNamesList ) {
	
    	this.mediatorServices = mediatorServices;
		this.invoiceDefinition = invoiceDefinition;
		this.csdb = csdb;
		this.invoiceFileDetail =invoiceFileDetails;
		invoiceFileDetail.setLocaleName(this.localName);
		this.invSummaryFieldNamesList=invSummaryFieldNamesList;
		this.invSummaryGoodsFieldNamesList =invSummaryGoodsFieldNamesList;
		this.invLineItemFieldNamesList =invLineItemFieldNamesList;
		this.invCreditFieldNamesList=invCreditFieldNamesList;
		

		
	}
	// Getter and Setters for all declared fields. START
	public long getA_corp_org_oid() {
		return a_corp_org_oid;
	}

	public void setA_corp_org_oid(long a_corp_org_oid) {
		this.a_corp_org_oid = a_corp_org_oid;
	}

	public long getA_first_auth_work_group_oid() {
		return a_first_auth_work_group_oid;
	}

	public void setA_first_auth_work_group_oid(long a_first_auth_work_group_oid) {
		this.a_first_auth_work_group_oid = a_first_auth_work_group_oid;
	}

	public long getA_first_authorizing_user_oid() {
		return a_first_authorizing_user_oid;
	}

	public void setA_first_authorizing_user_oid(
			long a_first_authorizing_user_oid) {
		this.a_first_authorizing_user_oid = a_first_authorizing_user_oid;
	}

	public long getA_invoice_group_oid() {
		return a_invoice_group_oid;
	}

	public void setA_invoice_group_oid(long a_invoice_group_oid) {
		this.a_invoice_group_oid = a_invoice_group_oid;
	}

	public long getA_second_auth_work_group_oid() {
		return a_second_auth_work_group_oid;
	}

	public void setA_second_auth_work_group_oid(
			long a_second_auth_work_group_oid) {
		this.a_second_auth_work_group_oid = a_second_auth_work_group_oid;
	}

	public long getA_second_authorizing_user_oid() {
		return a_second_authorizing_user_oid;
	}

	public void setA_second_authorizing_user_oid(
			long a_second_authorizing_user_oid) {
		this.a_second_authorizing_user_oid = a_second_authorizing_user_oid;
	}

	public long getAmended_invoice_number() {
		return amended_invoice_number;
	}

	public void setAmended_invoice_number(long amended_invoice_number) {
		this.amended_invoice_number = amended_invoice_number;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}

        public String getBuyer_tps_customer_id () {
            return this.buyer_tps_customer_id;
        }

        public void setBuyer_tps_customer_id (String buyer_tps_customer_id) {
            this.buyer_tps_customer_id = buyer_tps_customer_id;
        }


	public String getBuyer_name() {
		return buyer_name;
	}

	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public BigDecimal getEstimated_interest_amount() {
		return estimated_interest_amount;
	}

	public void setEstimated_interest_amount(
			BigDecimal estimated_interest_amount) {
		this.estimated_interest_amount = estimated_interest_amount;
	}

	public Date getFirst_authorize_status_date() {
		return first_authorize_status_date;
	}

	public void setFirst_authorize_status_date(Date first_authorize_status_date) {
		this.first_authorize_status_date = first_authorize_status_date;
	}

	public BigDecimal getInvoice_finance_amount() {
		return invoice_finance_amount;
	}

	public void setInvoice_finance_amount(BigDecimal invoice_finance_amount) {
		this.invoice_finance_amount = invoice_finance_amount;
	}

	public String getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}

	public String getInvoice_status() {
		return invoice_status;
	}

	public void setInvoice_status(String invoice_status) {
		this.invoice_status = invoice_status;
	}

	public String getInvoice_type() {
		return invoice_type;
	}

	public void setInvoice_type(String invoice_type) {
		this.invoice_type = invoice_type;
	}

	public Date getIssue_date() {
		return issue_date;
	}

	public void setIssue_date(Date issue_date) {
		this.issue_date = issue_date;
	}

	public String getLinked_to_instrument_type() {
		return linked_to_instrument_type;
	}

	public void setLinked_to_instrument_type(String linked_to_instrument_type) {
		this.linked_to_instrument_type = linked_to_instrument_type;
		fields.put("linked_to_instrument_type",linked_to_instrument_type);
	}

	public BigDecimal getNet_invoice_finance_amount() {
		return net_invoice_finance_amount;
	}

	public void setNet_invoice_finance_amount(
			BigDecimal net_invoice_finance_amount) {
		this.net_invoice_finance_amount = net_invoice_finance_amount;
	}

	public long getOpt_lock() {
		return opt_lock;
	}

	public void setOpt_lock(long opt_lock) {
		this.opt_lock = opt_lock;
	}

	public long getP_invoice_file_upload_oid() {
		return p_invoice_file_upload_oid;
	}

	public void setP_invoice_file_upload_oid(long p_invoice_file_upload_oid) {
		this.p_invoice_file_upload_oid = p_invoice_file_upload_oid;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public Date getSecond_authorize_status_date() {
		return second_authorize_status_date;
	}

	public void setSecond_authorize_status_date(
			Date second_authorize_status_date) {
		this.second_authorize_status_date = second_authorize_status_date;
	}

	public long getUpload_invoice_oid() {
		return upload_invoice_oid;
	}

	public void setUpload_invoice_oid(long upload_invoice_oid) {
		this.upload_invoice_oid = upload_invoice_oid;
	}
	public String getSeller_name() {
		return seller_name;
	}

	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
	}
	public String getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

        public String getSeller_tps_customer_id () {
            return this.seller_tps_customer_id;
        }

        public void setSeller_tps_customer_id (String seller_tps_customer_id) {
            this.seller_tps_customer_id = seller_tps_customer_id;
        }

	public String getInvoice_classification(){
		return invoice_classification;
	}
	public void SetInvoice_classification(String invoice_classification){
		this.invoice_classification = invoice_classification;
	}
	public long getA_upload_definition_oid() {
		return a_upload_definition_oid;
	}

	public void setA_upload_definition_oid(long a_upload_definition_oid) {
		this.a_upload_definition_oid = a_upload_definition_oid;
	}
	public String getA_transaction_oid() {
		return a_transaction_oid;
	}

	public void setA_transaction_oid(String a_transaction_oid) {
		this.a_transaction_oid = a_transaction_oid;
	}
	public String getA_instrument_oid() {
		return a_instrument_oid;
	}

	public void setA_intsrument_oid(String a_instrument_oid) {
		this.a_instrument_oid = a_instrument_oid;
	}
	public String getA_terms_oid() {
		return a_terms_oid;
	}

	public void setA_terms_oid(String a_terms_oid) {
		this.a_terms_oid = a_terms_oid;
	}

	public InvoiceDefinitionWebBean getInvoiceDefinition() {
		return invoiceDefinition;
	}

	public void setInvoiceDefinition(InvoiceDefinitionWebBean invoiceDefinition) {
		this.invoiceDefinition = invoiceDefinition;
	}

	public Set<InvoiceSummaryGoodObject> getInvoiceSummaryGood() {
		return invoiceSummaryGood;
	}

	public void setInvoiceSummaryGood(
			Set<InvoiceSummaryGoodObject> invoiceSummaryGood) {
		this.invoiceSummaryGood = invoiceSummaryGood;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public List<IssuedError> getErrorList() {
		if (errorList == null) {
			errorList = new ArrayList<>();
		}
		return errorList;
	}

	public void setErrorList(List<IssuedError> errorList) {
		this.errorList = errorList;
	}

	public String getEjbServerLocation() {
		return ejbServerLocation;
	}

	public void setEjbServerLocation(String ejbServerLocation) {
		this.ejbServerLocation = ejbServerLocation;
	}

	public boolean isNewINV() {
		return isNewINV;
	}

	public void setNewINV(boolean isNewINV) {
		this.isNewINV = isNewINV;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

	private BigDecimal totLineAmount = BigDecimal.ZERO;

	public void setTotLineAmount(BigDecimal amt) {
		totLineAmount =this.totLineAmount.add(amt);
	}

	public BigDecimal getTotLineAmount() {
		return totLineAmount;
	}


	/**
	 * @return the credit_note
	 */
	public String getCredit_note() {
		return credit_note;
	}

	/**
	 * @param credit_note the credit_note to set
	 */
	public void setCredit_note(String credit_note) {
		this.credit_note = credit_note;
	}

	/**
	 * @return the pay_method
	 */
	public String getPay_method() {
		return pay_method;
	}

	/**
	 * @param pay_method the pay_method to set
	 */
	public void setPay_method(String pay_method) {
		this.pay_method = pay_method;
	}

	/**
	 * @return the ben_acct_num
	 */
	public String getBen_acct_num() {
		return ben_acct_num;
	}

	/**
	 * @param ben_acct_num the ben_acct_num to set
	 */
	public void setBen_acct_num(String ben_acct_num) {
		this.ben_acct_num = ben_acct_num;
	}

	/**
	 * @return the ben_add1
	 */
	public String getBen_add1() {
		return ben_add1;
	}

	/**
	 * @param ben_add1 the ben_add1 to set
	 */
	public void setBen_add1(String ben_add1) {
		this.ben_add1 = ben_add1;
	}

	/**
	 * @return the ben_add2
	 */
	public String getBen_add2() {
		return ben_add2;
	}

	/**
	 * @param ben_add2 the ben_add2 to set
	 */
	public void setBen_add2(String ben_add2) {
		this.ben_add2 = ben_add2;
	}

	/**
	 * @return the ben_add3
	 */
	public String getBen_add3() {
		return ben_add3;
	}

	/**
	 * @param ben_add3 the ben_add3 to set
	 */
	public void setBen_add3(String ben_add3) {
		this.ben_add3 = ben_add3;
	}
	
	public String getBen_email_addr() {
		return ben_email_addr;
	}

	/**
	 * @param ben_email_addr the ben_email_addr to set
	 */
	public void setBen_email_addr(String ben_email_addr) {
		this.ben_email_addr = ben_email_addr;
	}

	public String getBen_bank_sort_code() {
		return ben_bank_sort_code;
	}

	/**
	 * @param ben_email_addr the ben_email_addr to set
	 */
	public void setBen_bank_sort_code(String ben_bank_sort_code) {
		this.ben_bank_sort_code = ben_bank_sort_code;
	}
	
	public String getEnd_to_end_id() {
		return end_to_end_id;
	}

	/**
	 * @param ben_email_addr the ben_email_addr to set
	 */
	public void setEnd_to_end_id(String end_to_end_id) {
		this.end_to_end_id = end_to_end_id;
	}

	/**
	 * @return the ben_country
	 */
	public String getBen_country() {
		return ben_country;
	}

	/**
	 * @param ben_country the ben_country to set
	 */
	public void setBen_country(String ben_country) {
		this.ben_country = ben_country;
	}

	/**
	 * @return the ben_bank_name
	 */
	public String getBen_bank_name() {
		return ben_bank_name;
	}

	/**
	 * @param ben_bank_name the ben_bank_name to set
	 */
	public void setBen_bank_name(String ben_bank_name) {
		this.ben_bank_name = ben_bank_name;
	}

	/**
	 * @return the ben_branch_code
	 */
	public String getBen_branch_code() {
		return ben_branch_code;
	}

	/**
	 * @param ben_branch_code the ben_branch_code to set
	 */
	public void setBen_branch_code(String ben_branch_code) {
		this.ben_branch_code = ben_branch_code;
	}

	/**
	 * @return the ben_branch_add1
	 */
	public String getBen_branch_add1() {
		return ben_branch_add1;
	}

	/**
	 * @param ben_branch_add1 the ben_branch_add1 to set
	 */
	public void setBen_branch_add1(String ben_branch_add1) {
		this.ben_branch_add1 = ben_branch_add1;
	}

	/**
	 * @return the ben_branch_add2
	 */
	public String getBen_branch_add2() {
		return ben_branch_add2;
	}

	/**
	 * @param ben_branch_add2 the ben_branch_add2 to set
	 */
	public void setBen_branch_add2(String ben_branch_add2) {
		this.ben_branch_add2 = ben_branch_add2;
	}

	/**
	 * @return the ben_bank_city
	 */
	public String getBen_bank_city() {
		return ben_bank_city;
	}

	/**
	 * @param ben_bank_city the ben_bank_city to set
	 */
	public void setBen_bank_city(String ben_bank_city) {
		this.ben_bank_city = ben_bank_city;
	}

	/**
	 * @return the ben_bank_province
	 */
	public String getBen_bank_province() {
		return ben_bank_province;
	}

	/**
	 * @param ben_bank_province the ben_bank_province to set
	 */
	public void setBen_bank_province(String ben_bank_province) {
		this.ben_bank_province = ben_bank_province;
	}

	/**
	 * @return the ben_branch_country
	 */
	public String getBen_branch_country() {
		return ben_branch_country;
	}

	/**
	 * @param ben_branch_country the ben_branch_country to set
	 */
	public void setBen_branch_country(String ben_branch_country) {
		this.ben_branch_country = ben_branch_country;
	}

	/**
	 * @return the charges
	 */
	public String getCharges() {
		return charges;
	}

	/**
	 * @param charges the charges to set
	 */
	public void setCharges(String charges) {
		this.charges = charges;
	}

	/**
	 * @return the central_bank_rep1
	 */
	public String getCentral_bank_rep1() {
		return central_bank_rep1;
	}

	/**
	 * @param central_bank_rep1 the central_bank_rep1 to set
	 */
	public void setCentral_bank_rep1(String central_bank_rep1) {
		this.central_bank_rep1 = central_bank_rep1;
	}

	/**
	 * @return the central_bank_rep2
	 */
	public String getCentral_bank_rep2() {
		return central_bank_rep2;
	}

	/**
	 * @param central_bank_rep2 the central_bank_rep2 to set
	 */
	public void setCentral_bank_rep2(String central_bank_rep2) {
		this.central_bank_rep2 = central_bank_rep2;
	}

	/**
	 * @return the central_bank_rep3
	 */
	public String getCentral_bank_rep3() {
		return central_bank_rep3;
	}

	/**
	 * @param central_bank_rep3 the central_bank_rep3 to set
	 */
	public void setCentral_bank_rep3(String central_bank_rep3) {
		this.central_bank_rep3 = central_bank_rep3;
	}

	/**
	 * @return the cred_discount_code
	 */
	public String getCred_discount_code() {
		return cred_discount_code;
	}

	/**
	 * @param cred_discount_code the cred_discount_code to set
	 */
	public void setCred_discount_code(String cred_discount_code) {
		this.cred_discount_code = cred_discount_code;
	}

	/**
	 * @return the cred_gl_code
	 */
	public String getCred_gl_code() {
		return cred_gl_code;
	}

	/**
	 * @param cred_gl_code the cred_gl_code to set
	 */
	public void setCred_gl_code(String cred_gl_code) {
		this.cred_gl_code = cred_gl_code;
	}

	/**
	 * @return the cred_comments
	 */
	public String getCred_comments() {
		return cred_comments;
	}

	/**
	 * @param cred_comments the cred_comments to set
	 */
	public void setCred_comments(String cred_comments) {
		this.cred_comments = cred_comments;
	}

	public String getLoan_type() {
		return loan_type;
	}

	public void setLoan_type(String loan_type) {
		this.loan_type = loan_type;
	}

	/**
	 * @return the credit_discount_code_id
	 */
	public String getCredit_discount_code_id() {
		return credit_discount_code_id;
	}

	/**
	 * @param credit_discount_code_id the credit_discount_code_id to set
	 */
	public void setCredit_discount_code_id(String credit_discount_code_id) {
		this.credit_discount_code_id = credit_discount_code_id;
	}

	/**
	 * @return the credit_discount_gl_code_id
	 */
	public String getCredit_discount_gl_code_id() {
		return credit_discount_gl_code_id;
	}

	/**
	 * @param credit_discount_gl_code_id the credit_discount_gl_code_id to set
	 */
	public void setCredit_discount_gl_code_id(String credit_discount_gl_code_id) {
		this.credit_discount_gl_code_id = credit_discount_gl_code_id;
	}

	/**
	 * @return the credit_discount_comments_id
	 */
	public String getCredit_discount_comments_id() {
		return credit_discount_comments_id;
	}

	/**
	 * @param credit_discount_comments_id the credit_discount_comments_id to set
	 */
	public void setCredit_discount_comments_id(String credit_discount_comments_id) {
		this.credit_discount_comments_id = credit_discount_comments_id;
	}

	//CR 913 Start
	
	public Date getSend_to_supplier_date() {
		return send_to_supplier_date;
	}

	public void setSend_to_supplier_date(Date send_to_supplier_date) {
		this.send_to_supplier_date = send_to_supplier_date;
		fields.put("send_to_supplier_date",send_to_supplier_date);
	}
    public String getBuyerAcctCurrency() {
		return buyer_acct_currency;
	}

	public void setBuyerAcctCurrency (String currency) {
		this.buyer_acct_currency = currency;
        fields.put("buyer_acct_currency", currency);
	}
    public String getBuyerAcctNum() {
		return buyer_acct_num;
	}

	public void setBuyerAcctNum (String num) {
		this.buyer_acct_num = num;
        fields.put("buyer_acct_num", num);
	}

	public String getReporting_code_1() {
		return reporting_code_1;
	}

	public void setReporting_code_1(String reporting_code_1) {
		this.reporting_code_1 = reporting_code_1;
		fields.put("reporting_code_1", reporting_code_1);
	}

	public String getReporting_code_2() {
		return reporting_code_2;
	}

	public void setReporting_code_2(String reporting_code_2) {
		this.reporting_code_2 = reporting_code_2;
		fields.put("reporting_code_2", reporting_code_2);
	}

	//CR 913 End
	/**
	 * ProcessINV method is called to process a passed in upload file line. It
	 * parses the record and maps all the values to appropriate fields on the
	 * object. Once all fields as mapped it validates the values and marks this
	 * object as valid or invalid. It also create, validates and adds any
	 * LineItems as child.
	 *
	 * @param record
	 * @param lineNum
	 * @throws AmsException
	 * @throws RemoteException
	*/

	public void newProcessINV(String[] record, int lineNum,MediatorServices mediatorServices, boolean isPayCreditNote,String alias)
	throws AmsException, RemoteException {		
		this.mediatorServices = mediatorServices;
		newProcessINV( record,  lineNum,isPayCreditNote,alias);
	}
	
	
	public void newProcessINV(String[] record, int lineNum,boolean isPayCreditNote,String alias)
			throws AmsException, RemoteException {
		invoiceFileDetail.setCurrentLineNum(lineNum);
	
		if (record != null && record.length > 0) {
				InvoiceSummaryGoodObject invoiceSummaryGoodObject = null;
				InvoiceLineItemObject invoiceLineItemObject = null;
				invoiceSummaryGood = null;
				if(isNewINV){
					//AiA Rel8.2 IR T36000015016 - 03/21/2013 - Add Credit Codes list
				  invList.addAll(invSummaryFieldNamesList);
				  invList.addAll(invCreditFieldNamesList);
				  invoiceFileDetail.setLoanType(getLoan_type());
				  invoiceFileDetail.setPayCreditNote(isPayCreditNote);
                  fields = invoiceFileDetail.setFieldValues(invList, record, lineNum, this);

				  if(!isValidBenBankAddressSize()){
					  isValid = false;
				  }

				  if (isValid && !invoiceFileDetail.validateCurrencyAmounts(getCurrency(),
						getAmount().toString(), alias)) {
					isValid = false;
				   }
				  // If loan type is Trade Loan, then if issue date is blank, set as Current Date and
				  // if due date is blank, set as Next Date of current Date
				  if(isValid && TradePortalConstants.TRADE_LOAN.equals(getLoan_type())){
					   Date today = null;
					   try {
					        today = GMTUtility.getGMTDateTime();
					  } catch (AmsException e) {
					        today = new Date(); //a backup
					  }
					  SimpleDateFormat userDefinedFormat = new SimpleDateFormat(invoiceDefinition.getAttributeWithoutHtmlChars("date_format"));

					  if(getIssue_date() == null || StringFunction.isBlank(getIssue_date().toString())){		// set issue date as current date
						    String currentDateStr = userDefinedFormat.format(today);
						    setIssue_date(today);
							fields.put("issue_date", currentDateStr);
					   }
					   if(getDue_date() == null || StringFunction.isBlank(getDue_date().toString())){ // Set Due date as next date
							long oneDayMiliSeconds = 1000 * 60 * 60 * 24;
						    long totalMiliSecond   = oneDayMiliSeconds + today.getTime();
						    String nextDateStr = userDefinedFormat.format(totalMiliSecond);
						    Date nextDate;
						    try{
						      nextDate = userDefinedFormat.parse(nextDateStr);
						    }catch(ParseException e) {
						    	// if issue with parsing, then set as today.
						    	nextDate = 	today;
						    }
                            setDue_date(nextDate);
                            fields.put("due_date", nextDateStr);
					  }
				  }
				}
				//SHR IR - T36000005931 Start
				if (!isNewINV &&((invoiceSummaryGoodMap == null || invoiceSummaryGoodMap.isEmpty()) ||
						(invSummaryGoodsFieldNamesList == null || invSummaryGoodsFieldNamesList.isEmpty())) && 
						(invLineItemFieldNamesList == null	|| invLineItemFieldNamesList.isEmpty())) {
					// duplicate line items or goods
					if(!isPayCreditNote){
						String[] substitutionValues = { "", "" };
						substitutionValues[0] = String.valueOf(this
								.getInvoice_id());
						substitutionValues[1] = String.valueOf(lineNum);
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DUPLICATE_INVOICE_ID,
								substitutionValues, null);
						isValid = false;
					}else{
						isValid = false;
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_WITH_CREDIT_NOTE_ID_EXISTS);
					}
				} else{
				//SHR IR - T36000005931 End
					invoiceSummaryGoodObject = new InvoiceSummaryGoodObject();
					invoiceSummaryGoodObject.processInvoiceSummaryGoods(
							invSummaryGoodsFieldNamesList, record,
							invoiceDefinition, mediatorServices, lineNum);
					// unique key
					if(isValid){
					 isValid = invoiceSummaryGoodObject.isValid();
					}
						String goodsUnique = invoiceSummaryGoodObject
								.getFields().values().toString();

						if (goodsNums.add(goodsUnique)) {
						} else {
							if(invoiceSummaryGoodMap!=null)
							invoiceSummaryGoodObject = invoiceSummaryGoodMap
									.get(goodsUnique);
							if (invLineItemFieldNamesList == null
									|| invLineItemFieldNamesList.isEmpty()) {
								// duplicate line items or goods
								String[] substitutionValues = { "", "" };
								substitutionValues[0] = String.valueOf(lineNum);
								substitutionValues[1] = String.valueOf(this
										.getInvoice_id());
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DUPLICATE_GOODS_LINEITEMS,
										substitutionValues, null);// TODO
								isValid = false;
							}
						}
						// line star
						    invoiceLineItemObject = new InvoiceLineItemObject();
						    boolean isDummyLineItemAdded = false;
							if (invLineItemFieldNamesList != null
									&& invLineItemFieldNamesList.size() > 0) {
								invoiceLineItemObject.processInvoiceLineItem(
										invLineItemFieldNamesList, record,
										invoiceDefinition, mediatorServices,
										lineNum);
								invoiceLineItemObject.setDerived_line_item_ind(TradePortalConstants.INDICATOR_NO);//SHR CR708 Rel8.1.1
								if (invoiceSummaryGoodMap!=null && !invoiceSummaryGoodMap.keySet().isEmpty()){
									InvoiceSummaryGoodObject invSummGoodObj = invoiceSummaryGoodObject;
									if (invSummGoodObj != null){
										Set<InvoiceLineItemObject> invLineItemSet = invSummGoodObj.getInvoiceLineItem();
										if(invLineItemSet != null && !invLineItemSet.isEmpty()){
											for (InvoiceLineItemObject invLineItemObject : invLineItemSet) {
												if (invLineItemObject != null){
													String lineItemId = invLineItemObject.getLine_item_id();
													if (invoiceLineItemObject.getLine_item_id().equals(lineItemId)){
														// duplicate line items or goods
														String[] substitutionValues = { "", "" };
														substitutionValues[0] = String.valueOf(lineNum);
														substitutionValues[1] = String.valueOf(this
																.getInvoice_id());
														mediatorServices.getErrorManager().issueError(
																TradePortalConstants.ERR_CAT_1,
																TradePortalConstants.DUPLICATE_GOODS_LINEITEMS,
																substitutionValues, null);
														invoiceLineItemObject.setValid(false);
														isValid = false;
														break;
													}
												}
											}
										}
									}
								}
								
							}
							else if (isValid && (invLineItemFieldNamesList == null
									|| invLineItemFieldNamesList.isEmpty())) {
								if (invoiceSummaryGoodMap!=null && !invoiceSummaryGoodMap.keySet().isEmpty()){
									InvoiceSummaryGoodObject invSummGoodObj = invoiceSummaryGoodMap.get(invoiceSummaryGoodMap.keySet().iterator().next());
									if (invSummGoodObj != null){
										Set invLineItemSet = invSummGoodObj.getInvoiceLineItem();
										if(invLineItemSet != null && !invLineItemSet.isEmpty()){
											InvoiceLineItemObject invLineItemObject = (InvoiceLineItemObject)invLineItemSet.iterator().next();
											if (invLineItemObject != null){
												String derivedLineItemInd = invLineItemObject.getDerived_line_item_ind();
												if (TradePortalConstants.INDICATOR_YES.equals(derivedLineItemInd)){
													isDummyLineItemAdded = true;
												}
											}
										}
									}
								}


								if (!isDummyLineItemAdded){
									invoiceLineItemObject.setLine_item_id("1");
									invoiceLineItemObject.setValid(true);
									invoiceLineItemObject
											.setUnit_price(getAmount());
									invoiceLineItemObject
											.setInv_quantity(new BigDecimal("1"));
									invoiceLineItemObject.setDerived_line_item_ind(TradePortalConstants.INDICATOR_YES);//SHR CR708 Rel8.1.1
									Map<String,String> fieldsMap = new HashMap();
									fieldsMap.put("line_item_id", "1");
									fieldsMap.put("unit_price",
											getAmount() != null ? getAmount()
													.toString() : "0");
									fieldsMap.put("inv_quantity", "1");
									fieldsMap.put("derived_line_item_ind", TradePortalConstants.INDICATOR_YES);//IR T36000016345
									invoiceLineItemObject.setFields(fieldsMap);
								}
							}
							
							if(isValid){
								calculateInvoiceLineItemAmount(invoiceLineItemObject);
								isValid = invoiceLineItemObject.isValid();
							}
							if(invoiceLineItemObject.getUnit_price() !=null && !TradePortalConstants.INDICATOR_YES.equals(invoiceLineItemObject.getDerived_line_item_ind())){
							 if (!invoiceFileDetail.validateCurrencyAmounts(
									getCurrency(), invoiceLineItemObject
											.getUnit_price().toString(),
									"unit_price")) {
								invoiceLineItemObject.setValid(false);
								isValid = false;
							 }
							}
							if (isValid) {
								if (!isDummyLineItemAdded){
									invoiceSummaryGoodObject
										.setInvoiceLineItem(invoiceLineItemObject);
								}
								if (invoiceSummaryGoodMap == null || invoiceSummaryGoodMap.isEmpty()) {
									invoiceSummaryGoodMap = new HashMap<String, InvoiceSummaryGoodObject>();
								}
								invoiceSummaryGoodMap.put(goodsUnique,
										invoiceSummaryGoodObject);
								invoiceSummaryGood = new HashSet<InvoiceSummaryGoodObject>();
								invoiceSummaryGood.addAll(invoiceSummaryGoodMap
										.values());
								// goods end
							}
						}
					}

		if (!isValid) {
			if (errorList == null) {
				errorList = new ArrayList();
			}
		}
	}

	public void addToErrorList(MediatorServices mediatorServices) {
		String[] substitutionValues = { "", "" };
		substitutionValues[0] = "Purchase Order";
		substitutionValues[1] = getInvoice_id();

		IssuedError ie = mediatorServices.getErrorManager().getIssuedError(csdb.getLocaleName(), "", substitutionValues, null);
		List errList = getErrorList();
		if (errList == null) {
			errList = new ArrayList();
		}

		errList.add(ie);
		setErrorList(errList);
	}

	/**
	 * Identifies and retrieves the Matching Rule for this invoice. First it
	 * looks for a rule with a matching buyer_name. If no match is found for the
	 * buyer_name, it looks for a rule with a matching buyer_id.
	 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public ArMatchingRule getMatchingRule() throws AmsException,
			RemoteException {

		final String outerSelect = "SELECT DISTINCT ar_matching_rule_oid"
				+ " FROM ar_matching_rule WHERE ";

		final String innerSelect = " OR ar_matching_rule_oid IN"
				+ " (SELECT p_ar_matching_rule_oid FROM";

		String sqlBuilder = null;
		DocumentHandler resultSet = null;
		String tradePartnerOid = null;
		String corpOrgOid = String.valueOf(getA_corp_org_oid());
		String buyerName = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getInvoice_classification())?getBuyer_name():getSeller_name();//SHR CR708 Rel8.1.1
		if (StringFunction.isNotBlank(buyerName)) {
			buyerName = StringFunction.toUnistr(buyerName.toUpperCase().trim());
			sqlBuilder = outerSelect + "p_corp_org_oid = ? and (buyer_name = unistr(?) " + innerSelect + " ar_buyer_name_alias WHERE buyer_name_alias = unistr(?)))";// Nar -MKUM061235356

			resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, buyerName, buyerName);
			if (resultSet != null) {
				tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
			}
		}
		if (tradePartnerOid == null) {
			String buyerId = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getInvoice_classification())?getBuyer_id():getSeller_id();
			if (StringFunction.isNotBlank(buyerId)) {
				buyerId = StringFunction.toUnistr(buyerId.toUpperCase().trim());
				sqlBuilder = outerSelect + " p_corp_org_oid = ? and (buyer_id = unistr(?) " + innerSelect + " ar_buyer_id_alias WHERE buyer_id_alias = unistr(?)))"; // Nar -MKUM061235356
				resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, buyerId, buyerId);
				if (resultSet != null) {
					tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
				}
			}
		}
		if (StringFunction.isNotBlank(tradePartnerOid)) {
			ArMatchingRule matchingRule = (ArMatchingRule) EJBObjectFactory.createClientEJB(ejbServerLocation, "ArMatchingRule", Long.parseLong(tradePartnerOid));
			// In case buyer_name or buyer_id are blank or an alias value,
			// update with the actual values from the rule.
			//SHR CR708 Rel8.1.1 start
			if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getInvoice_classification())&& StringFunction.isBlank(getBuyer_name())){
			setBuyer_name(matchingRule.getAttribute("buyer_name"));
			}
			if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(getInvoice_classification())&& StringFunction.isBlank(getSeller_name())){
			setSeller_name(matchingRule.getAttribute("buyer_name"));
			}
			//SHR CR708 Rel8.1.1 end
			return matchingRule;
		}
		return null;
	}

	public boolean validateInvoiceLineItemAmount() throws AmsException {
		if (totLineAmount.compareTo(getAmount()) != 0) {
			String[] substitutionValues = { "", "" };
			IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
					csdb.getLocaleName(), TradePortalConstants.INVALID_LINE_ITEMS_AMT, substitutionValues, null);
			List errList = getErrorList();
			if (errList == null) {
				errList = new ArrayList();
			}
			errList.add(ie);
			setErrorList(errList);
			return false;
		}
		return true;
	}

	public void calculateInvoiceLineItemAmount(InvoiceLineItemObject invLineItem)
			throws AmsException {
		BigDecimal lineAmount = BigDecimal.ZERO;
		BigDecimal unitPrice =invLineItem.getUnit_price();
		if ( unitPrice!= null
				&& invLineItem.getInv_quantity() != null) {
			lineAmount = unitPrice.multiply(
					invLineItem.getInv_quantity());
			setTotLineAmount(lineAmount);
		}else if (unitPrice != null){
			setTotLineAmount(unitPrice);
		}

	}
    //RKAZI 09/04/2012 CR-708 Rel 8.1.1 Start
    public boolean processAmendInvoices(InvoiceSummaryDataObject invoiceSummaryDataObject, HashMap removeOids,String securityRights,
    		boolean isPayCreditNote,String ownerOrgOid) {
        try
        {   
        	if(isPayCreditNote){
        		if(checkForUnassignedCreditNote(removeOids)){
        			return true;
        	 }
        	}
        	else{
        	//Rel 9.0 Performance tuning start - first check point is to Check if its new invoice ,if not then follow replacement process
        	if (checkIfNewInvoice(ownerOrgOid))
            {
                return false;
            }
        	//Rel 9.0 Performance tuning end
        	
            //Check if invoice_type is RPL and no matching invoice is present, log error and exit.
            if (checkForInvoiceTypeDefinedNoMatch())
            {
                return true;
            }
            //if invoice_type is blank and matching invoice is found of type receivables, then update the invoice
            //and recalculate interest and discount
            if (checkForUnassignedInvoicesMatch(removeOids))
            {
                return true;
            }
            //if invoice_type is blank, invoice is assigned to ATP and authorized to TPS, then log error
            //and exit.          
            if (checkForAuthorizedInvoicesMatch())
            {
                return true;
            }
            //if invoice is linked to ATP which has a status of Pending, then update the invoice and derive amount
            //and dates.
            if ((TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(getInvoice_classification()) ) && checkForPendingInvoicesMatch(removeOids))
            {
                return true;
            }
            // if the invoice is assigned to ATP which is Authorized or Partially Authorized then update the invoice,
            //ATP and set the transaction status to Started.
            if ((TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(getInvoice_classification()) ) && checkForAuthorizedOrPartAuthorizedInvoicesMatch(removeOids))
            {
                return true;
            }
            //if invoice is assigned to an ATP which has a status of PROCESSED BY BANK then a new amendment transaction
            // will be created.
            if ((TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(getInvoice_classification()) ) && checkIfAuthorizedInvMatches(mediatorServices.getCSDB().getLocaleName(),securityRights,removeOids))
            {
                return true;
            }
        	}
        }
        catch (AmsException | RemoteException ex)
        {
            LOG.error("Exception occurred in InvoiceSummaryDataObject.processAmendInvoices(): " ,ex);
            logException(ex);
        }
        // we do not match any condition and invoice_type is blank then it is a new invoice.
        return false;
    }

    private boolean checkForAuthorizedOrPartAuthorizedInvoicesMatch(HashMap removeOids) throws AmsException, RemoteException {
        DocumentHandler   matchingInvSummaryDataDoc = null;
        String      sqlQuery               = null;

        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to a transaction that has a
        // status other than Authorized,Partially Authorized,Rejected by Bank,
        // Processed By Bank, Cancelled By Bank, or Deleted
        // Only look for a match where the PO is active for the instrument
        sqlQuery = "select a.upload_invoice_oid "
        		+ "from invoices_summary_data a, transaction b "
        		+ "where a.a_corp_org_oid = ? and a.invoice_id = ? "
        		+ "and a.a_transaction_oid = b.transaction_oid "
        		+ "and a.a_instrument_oid is not null "
        		+ "and b.transaction_status in (?, ?)";

        // Retrieve any PO line items that satisfy the SQL that was just constructed
        matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_corp_org_oid, getInvoice_id(), TransactionStatus.READY_TO_AUTHORIZE, TransactionStatus.PARTIALLY_AUTHORIZED);

        // If a match was found, try to validate the current PO line item's data and
        // update the matching PO line item.
        if (matchingInvSummaryDataDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                 logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                         " have been found that match it");
                return true;
            }
          //SHR start
            String  invSummaryOid   = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/UPLOAD_INVOICE_OID");
            removeOids.put("invOID",invSummaryOid);
            removeOids.put("RPL_VAL","RI");

            // If the PO upload definition, beneficiary, and currency are the same for the
            // current PO line item and matching PO line item, update the existing PO line
            // item with the new data, derive several transaction, terms, and PO line item
            // fields, set several PO line item transaction attributes, and log a message
            // to the Auto LC log file; otherwise, unassign the PO line item, update it with
            // the new PO line item data, and log a message to the Auto LC log file.
            boolean invSummaryDataMatchFields = invSummaryDataFieldsMatchTransactionFields(matchingInvSummaryDataDoc);
            if (invSummaryDataMatchFields)
            {
               removeOids.put("updateTransaction", true);
            	return true;
            }           
        }
        return false;
    }

    private boolean checkForPendingInvoicesMatch(HashMap removeOids) throws AmsException, RemoteException {
        DocumentHandler   matchingInvSummaryDataDoc = null;


        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to a transaction that has a
        // status other than Authorized,Partially Authorized,Rejected by Bank,
        // Processed By Bank, Cancelled By Bank, or Deleted
        // Only look for a match where the PO is active for the instrument
        Object[] sqlPrmsLst = new Object[9];
        String sqlQuery = "select a.upload_invoice_oid,a.linked_to_instrument_type,a.due_date,a.payment_date "
        		+ "from invoices_summary_data a, transaction b "
        		+ "where a.a_corp_org_oid = ? and invoice_classification=? "
        		+ "and a.invoice_id = ? and a.a_transaction_oid = b.transaction_oid "
        		+ "and a.a_instrument_oid is not null and b.transaction_status not in (?,?,?,?,?,?)";
        
        sqlPrmsLst[0] = a_corp_org_oid;
        sqlPrmsLst[1] = getInvoice_classification();
        sqlPrmsLst[2] = getInvoice_id();
        sqlPrmsLst[3] = TransactionStatus.AUTHORIZED;
        sqlPrmsLst[4] = TransactionStatus.PROCESSED_BY_BANK;
        sqlPrmsLst[5] = TransactionStatus.CANCELLED_BY_BANK;
        sqlPrmsLst[6] = TransactionStatus.DELETED;
        sqlPrmsLst[7] = TransactionStatus.PARTIALLY_AUTHORIZED;
        sqlPrmsLst[8] = TransactionStatus.READY_TO_AUTHORIZE;

        // Retrieve any PO line items that satisfy the SQL that was just constructed
        matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

        // If a match was found, try to validate the current PO line item's data and
        // update the matching PO line item.
        if (matchingInvSummaryDataDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {

                logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                        " have been found that match it");

                return true;
            }
            //IR T36000017596 start -check if replacement invoice has different due date/payment date than the initial invoice.If so throw error
            CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory
			.createClientEJB(
					ejbServerLocation,
					"CorporateOrganization",
					a_corp_org_oid);
		   String dueDate = "";
           if (getDue_date()!=null)
           dueDate = TPDateTimeUtility.convertToISODate(
                   DateTimeUtility.convertDateToDateString(getDue_date()),"MM/dd/yyyy");
          String payDate = "";
          if (getPayment_date()!=null)
           payDate = TPDateTimeUtility.convertToISODate(
                  DateTimeUtility.convertDateToDateString(getPayment_date()),"MM/dd/yyyy");

           if (payDate!=null
   				&& TradePortalConstants.INDICATOR_YES.equals(corpOrg
   						.getAttribute("payment_day_allow")) && StringFunction.isNotBlank(matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/PAYMENT_DATE")) &&
        	    		!matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/PAYMENT_DATE").equalsIgnoreCase(payDate)){
        	   String[] substitutionValues = {bundle.getString("InvoiceUploadRequest.payment_date"), "" };
               IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                       csdb.getLocaleName(), TradePortalConstants.INVALID_DUE_DATE, substitutionValues, null);
               List errList = getErrorList();
               if (errList == null) {
                   errList = new ArrayList();
               }
               errList.add(ie);
               setErrorList(errList);
               return true;
        	}

           else if(StringFunction.isNotBlank(matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/DUE_DATE")) &&
       	    		!matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/DUE_DATE").equalsIgnoreCase(dueDate)){
        	   String[] substitutionValues = {bundle.getString("InvoiceUploadRequest.due_date"), "" };
               IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                       csdb.getLocaleName(), TradePortalConstants.INVALID_DUE_DATE, substitutionValues, null);
               List errList = getErrorList();
               if (errList == null) {
                   errList = new ArrayList();
               }
               errList.add(ie);
               setErrorList(errList);
               return true;
       	    }
       	     //IR T36000017596 end

          //SHR start
            String invSummaryOid   = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/UPLOAD_INVOICE_OID");
            removeOids.put("invOID",invSummaryOid);
            removeOids.put("RPL_VAL","PI");
            //SHR end

            boolean invSummaryDataMatchFields = invSummaryDataFieldsMatchTransactionFields(matchingInvSummaryDataDoc);
            if (invSummaryDataMatchFields)
            {
               removeOids.put("updateTransaction", true);
             }           
            return true;
        }
            return false;
    }

    private boolean checkForAuthorizedInvoicesMatch() throws AmsException {
        DocumentHandler   matchingInvSummaryDataDoc = null;
        String      sqlQuery               = null;
        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to a transaction that has a
        // status other than Authorized,Partially Authorized,Rejected by Bank,
        // Processed By Bank, Cancelled By Bank, or Deleted
        // Only look for a match where the PO is active for the instrument
        sqlQuery = "select a.upload_invoice_oid, b.copy_of_instr_type_code "
        		+ "from invoices_summary_data a, transaction b "
        		+ "where a.a_corp_org_oid = ? and invoice_classification=? "
        		+ "and a.invoice_id = ? and a.a_instrument_oid is not null "
        		+ "and a.a_transaction_oid = b.transaction_oid and b.transaction_status in (?)";
      //SHR -update replacement logic end
        // Retrieve any PO line items that satisfy the SQL that was just constructed
        matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_corp_org_oid, getInvoice_classification(), getInvoice_id(), TransactionStatus.AUTHORIZED);

        // If a match was found, try to validate the current PO line item's data and
        // update the matching PO line item.
        if (matchingInvSummaryDataDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                        " have been found that match it");
                return true;
            }
            String instrType = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/COPY_OF_INSTR_TYPE_CODE");
            String code = InstrumentType.LOAN_RQST.equalsIgnoreCase(instrType)? TradePortalConstants.UPDATING_AUTHORIZED_LRQ_INVOICE_ERROR:TradePortalConstants.UPDATING_AUTHORIZED_INVOICE_ERROR;
            String[] substitutionValues = { getInvoice_id(), "" };
            IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                    csdb.getLocaleName(), code, substitutionValues, null);
            List errList = getErrorList();
            if (errList == null) {
                errList = new ArrayList();
            }
            errList.add(ie);
            setErrorList(errList);
            return true;
        }
      //SHR -update replacement logic start
        else{
        	sqlQuery += " and b.copy_of_instr_type_code = ?";
        	matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_corp_org_oid, getInvoice_classification(), getInvoice_id(), TransactionStatus.AUTHORIZED, InstrumentType.LOAN_RQST);
        	if (matchingInvSummaryDataDoc != null)
            {
                // If more than one record is found, log an error message and return
                if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
                {
                    logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                            " have been found that match it");
                }
                String[] substitutionValues = { getInvoice_id(), "" };
                IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.UPDATING_AUTHORIZED_LRQ_INVOICE_ERROR, substitutionValues, null);
                List errList = getErrorList();
                if (errList == null) {
                    errList = new ArrayList();
                }
                errList.add(ie);
                setErrorList(errList);
                return true;
            }
        	//IR 16297 start
        	else if(InstrumentType.LOAN_RQST.equalsIgnoreCase(getLinked_to_instrument_type())){
        		sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a "
        				+ "where a.a_corp_org_oid = ? and invoice_classification=? "
        				+ "and a.invoice_id = ? and (invoice_status in (?,?,?) or a_transaction_oid is not null)";
                matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{a_corp_org_oid, getInvoice_classification(),
                		getInvoice_id(), TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH, TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH, 
                		TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN});
            	if (matchingInvSummaryDataDoc != null)
                {
                    // If more than one record is found, log an error message and return
                    if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
                    {
                        logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                                " have been found that match it");
                    }
                    String[] substitutionValues = { getInvoice_id(), "" };
                    IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                            csdb.getLocaleName(), TradePortalConstants.NO_MATCHING_INVOICE_ERROR, substitutionValues, null);
                    List errList = getErrorList();
                    if (errList == null) {
                        errList = new ArrayList();
                    }
                    errList.add(ie);
                    setErrorList(errList);
                    return true;
                }
        	} //IR 16297 end
        	//SHR -update replacement logic end
        }
            return false;
    }


    private boolean checkForInvoiceTypeDefinedNoMatch() throws AmsException, RemoteException {
    	     if (!findMatchingInvoice()){
                String[] substitutionValues = { getInvoice_id(), "" };
                IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.NO_MATCHING_INVOICE_ERROR, substitutionValues, null);
                List errList = getErrorList();
                if (errList == null) {
                    errList = new ArrayList();
                }
                errList.add(ie);
                setErrorList(errList);
                return true;
            } else if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equalsIgnoreCase(getInvoice_classification())){
            	DocumentHandler   matchingInvSummaryDataDoc = null;
                String sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a "
                		+ "where a.a_corp_org_oid = ?  and invoice_classification=? "
                		+ "and a.invoice_id = ? and invoice_status in (?, ?)";
                    matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_corp_org_oid, getInvoice_classification()
                    								, getInvoice_id(), TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH, TradePortalConstants.UPLOAD_INV_STATUS_FINANCED);

                    // If a match was found, try to validate the current PO line item's data and
                    // update the matching PO line item.
                    if (matchingInvSummaryDataDoc != null)
                    {
                    	String[] substitutionValues = { getInvoice_id(), "" };
                        IssuedError ie = mediatorServices.getErrorManager().getIssuedError( csdb.getLocaleName(),
                        		TradePortalConstants.DUPLICATE_INVOICE, substitutionValues, null);
                       
                        List errList = getErrorList();
                        if (errList == null) {
                            errList = new ArrayList();
                        }
                        errList.add(ie);
                        setErrorList(errList);
                        return true;
                    }
            }
            //IR T36000027526 start
            else if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(getInvoice_classification())){
            	DocumentHandler   matchingInvSummaryDataDoc = null;
            	 String sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a "
            	 		+ "where a.a_corp_org_oid = ? and invoice_classification=? and a.invoice_id = ? "
            	 		+ "and invoice_status in (?,?,?) and a.a_instrument_oid is null and a.a_transaction_oid is null ";
                 matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_corp_org_oid, getInvoice_classification(), 
                		 getInvoice_id(), TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH, TradePortalConstants.UPLOAD_INV_STATUS_AUTH,
                		 TransactionStatus.PROCESSED_BY_BANK);
                    if (matchingInvSummaryDataDoc != null)
                    {
                    	String[] substitutionValues = { getInvoice_id(), "" };
                        IssuedError ie = mediatorServices.getErrorManager().getIssuedError( csdb.getLocaleName(),
                        		TradePortalConstants.DUPLICATE_INVOICE, substitutionValues, null);                        
                        List errList = getErrorList();
                        if (errList == null) {
                            errList = new ArrayList();
                        }
                        errList.add(ie);
                        setErrorList(errList);
                        return true;
                    }
            }//IR T36000027526 end
            else {
                DocumentHandler   matchingInvSummaryDataDoc = null;
                List<Object> sqlPrmsLst = new ArrayList<Object>();
                String sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a, transaction b "
                		+ "where a.a_corp_org_oid = ? and invoice_classification=? and a.invoice_id = ? "
                		+ "and a.a_transaction_oid = b.transaction_oid and a.a_instrument_oid is not null and b.transaction_status in (?) ";                
                sqlPrmsLst.add(a_corp_org_oid);
                sqlPrmsLst.add(getInvoice_classification());
                sqlPrmsLst.add(getInvoice_id());
                sqlPrmsLst.add(TransactionStatus.AUTHORIZED);                
                if(getDue_date() != null){
                    sqlQuery += " and a.due_date != ? ";
                    sqlPrmsLst.add(TPDateTimeUtility.convertToDBDate(DateTimeUtility.convertDateToDateString(getDue_date()),"MM/dd/yyyy"));
                }
                // Retrieve any PO line items that satisfy the SQL that was just constructed
                matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

                // If a match was found, try to validate the current PO line item's data and
                // update the matching PO line item.
                if (matchingInvSummaryDataDoc != null)
                {
                    // If more than one record is found, log an error message and return
                    if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
                    {
                        logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                                " have been found that match it");
                        return true;
                    }
                    String[] substitutionValues = { getInvoice_id(), "" };
                    IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                            csdb.getLocaleName(), TradePortalConstants.AUTHORIZED_INVOICE_DUE_DATE_DIFF_ERROR, substitutionValues, null);
                    List errList = getErrorList();
                    if (errList == null) {
                        errList = new ArrayList();
                    }
                    errList.add(ie);
                    setErrorList(errList);
                    return true;
                }
            }
        return false;
    }

    private boolean checkIfAuthorizedInvMatches(String userLocale,String securityRights,HashMap removeOids) throws AmsException, RemoteException
    {
        String      sqlQuery               = null;
        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to either an Amend transaction
        // with a status of Authorized or any transaction with a status of Processed By Bank
        // Only look for a match where the PO is active for the instrument
        // Order by transaction_status_date and transaction_oid descendingly so that the po
        // line item belonging to the latest transaction comes up first.
        sqlQuery = "select a.upload_invoice_oid, a.a_instrument_oid, b.transaction_status, a.amend_seq_no  "
        		+ "from invoices_summary_data a, transaction b where a.a_corp_org_oid = ? "
        		+ "and a.invoice_classification=? and a.invoice_id = ? and a.a_transaction_oid = b.transaction_oid "
        		+ "and a.a_instrument_oid is not null and ((b.transaction_type_code = ";        
        String sqlQuery1 = sqlQuery +  "? and b.transaction_status = ?))";
        
        // Retrieve any PO line items that satisfy the SQL that was just constructed
        authorisedInvSummaryDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery1, false, a_corp_org_oid, getInvoice_classification(), getInvoice_id(),
        		TransactionType.AMEND, TransactionStatus.PROCESSED_BY_BANK);
        if (authorisedInvSummaryDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (authorisedInvSummaryDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                        " have been found that match it");
                return true;
            }
            // Checking matching fields.  If the key fields (invoice definition oid,
            // beneficiary, currency) are changed, give an error and do not upload.
            if (!invSummaryDataFieldsMatchTransactionFields(authorisedInvSummaryDoc))
            {         
                logDebug("Invoice " + getInvoice_id() + "cannot be uploaded to Instrument  due to change in data " +
                    "(Beneficiary, Currency or " +
                    "Invoice Definition format) from the original uploaded file");
                return true;
            }
           //IR T36000016939 start- throw error if invoice type is INT  after the instrument is processed
            if(TradePortalConstants.INITIAL_INVOICE.equalsIgnoreCase(getInvoice_type())){
            	String[] substitutionValues = { getInvoice_id(), "" };
                IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.DUPLICATE_INVOICE, substitutionValues, null);
                List errList = getErrorList();
                if (errList == null) {
                    errList = new ArrayList();
                }
                errList.add(ie);
                setErrorList(errList);
                return true;
            }
          //IR T36000016939 end
            else{
            	 removeOids.put("RPL_VAL", "AI");
                 //IR T36000015686
                 if(StringFunction.isBlank(getInvoice_type())){
               	  getFields().put("invoice_type", TradePortalConstants.REPLACEMENT);
                   setInvoice_type(TradePortalConstants.REPLACEMENT);
                 }
            }
         }
        else{
        	  sqlQuery1 = sqlQuery + "? and b.transaction_status = ?))";

              // Retrieve any PO line items that satisfy the SQL that was just constructed
              authorisedInvSummaryDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery1, false, a_corp_org_oid, getInvoice_classification(), getInvoice_id(),
              		TransactionType.ISSUE, TransactionStatus.PROCESSED_BY_BANK);
              if (authorisedInvSummaryDoc!= null ){
            	  if (authorisedInvSummaryDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            	  {
                  logError("Invoice "+ getInvoice_id() + " cannot be updated because multiple existing Invoices" +
                          " have been found that match it");
                  return true;
              	}

              // Checking matching fields.  If the key fields (invoice definition oid,
              // beneficiary, currency) are changed, give an error and do not upload.
              if (!invSummaryDataFieldsMatchTransactionFields(authorisedInvSummaryDoc))
              {
                  logDebug("Invoice " + getInvoice_id() + "cannot be uploaded to Instrument  due to change in data " +
                      "(Beneficiary, Currency or " +
                      "Invoice Definition format) from the original uploaded file");
                 return true;
              }
            //IR T36000016939 start- throw error if invoice type is INT  after the instrument is processed
              if(TradePortalConstants.INITIAL_INVOICE.equalsIgnoreCase(getInvoice_type())){

              	String[] substitutionValues = { getInvoice_id(), "" };
                  IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                          csdb.getLocaleName(), TradePortalConstants.DUPLICATE_INVOICE, substitutionValues, null);
                  List errList = getErrorList();
                  if (errList == null) {
                      errList = new ArrayList();
                  }
                  errList.add(ie);
                  setErrorList(errList);
                  return true;
              }
              //IR T36000016939 end
              else{
              removeOids.put("RPL_VAL", "AI");
              //IR T36000015686
              if(StringFunction.isBlank(getInvoice_type())){
            	  getFields().put("invoice_type", TradePortalConstants.REPLACEMENT);
              	  setInvoice_type(TradePortalConstants.REPLACEMENT);
              	}
              }
              }
        }
        //IR T36000017596 start - check if replacement invoice has different due date/payment date than the initial invoice.If so throw error
        if(authorisedInvSummaryDoc!=null){
        CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory
 			.createClientEJB(
 					ejbServerLocation,
 					"CorporateOrganization",
 					a_corp_org_oid);
        String dueDate = "";
        if (getDue_date()!=null)
        dueDate = TPDateTimeUtility.convertToISODate(
                DateTimeUtility.convertDateToDateString(getDue_date()),"MM/dd/yyyy");
       String payDate = "";
       if (getPayment_date()!=null)
        payDate = TPDateTimeUtility.convertToISODate(
               DateTimeUtility.convertDateToDateString(getPayment_date()),"MM/dd/yyyy");

        if (payDate!=null
				&& TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("payment_day_allow")) && StringFunction.isNotBlank(authorisedInvSummaryDoc.getAttribute("/ResultSetRecord/PAYMENT_DATE")) &&
     	    		!authorisedInvSummaryDoc.getAttribute("/ResultSetRecord/PAYMENT_DATE").equalsIgnoreCase(payDate)){
     	   String[] substitutionValues = {bundle.getString("InvoiceUploadRequest.payment_date"), "" };
            IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                    csdb.getLocaleName(), TradePortalConstants.INVALID_DUE_DATE, substitutionValues, null);
            List errList = getErrorList();
            if (errList == null) {
                errList = new ArrayList();
            }
            errList.add(ie);
            setErrorList(errList);
            return true;
     	}

        else if(StringFunction.isNotBlank(authorisedInvSummaryDoc.getAttribute("/ResultSetRecord/DUE_DATE")) &&
    	    		!authorisedInvSummaryDoc.getAttribute("/ResultSetRecord/DUE_DATE").equalsIgnoreCase(dueDate)){
     	   String[] substitutionValues = {bundle.getString("InvoiceUploadRequest.due_date"), "" };
            IssuedError ie = mediatorServices.getErrorManager().getIssuedError(
                    csdb.getLocaleName(), TradePortalConstants.INVALID_DUE_DATE, substitutionValues, null);
            List errList = getErrorList();
            if (errList == null) {
                errList = new ArrayList();
            }
            errList.add(ie);
            setErrorList(errList);
            return true;
    	    }
        return true;//IR 21663
        }
      //IR T36000017596 end
        return false; //IR 17776
    }

    /**
     * This method is used to determine whether or not the current PO line item
     * has the same PO number and item number as a PO line item assigned to an
     * Amend transaction having a status of Authorized or a transaction having a
     * status of Processed By Bank. If a match is found, the method will
     * validate the current PO line item's data. If validation is successful,
     * it will then call a method to create a new Import LC - Amend transaction.
     * Next, if the Amend transaction is created successfully, it will call
     * methods to update the existing record's data and add a message to the
     * Auto LC log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a PO line item match was found
     *                       (true  - a PO line item match was found, so stop processing the PO line item
     *                        false - a PO line item match was not found, so continue processing the PO line item)
     */
    public boolean checkForAuthorizedAmendOrProcessedByBankMatch(String securityRights,InvoicesSummaryData invSummaryData) throws AmsException, RemoteException //logu & amit
    {
            // Check whether there is pending amendment for the instrument.
            // If there is pending amendment, then add PO to amendment, otherwise create a new amendment.
            // If multiple POs are amended, the first amended PO may create a new amendment
            // or be appended to an existing amendment.  The following amended PO will be appended
            // to the new amendment or appended to the same existing amendment.
            String instrumentOid = authorisedInvSummaryDoc.getAttribute("/ResultSetRecord(0)/A_INSTRUMENT_OID");
            long pendingAmendmentOid = getPendingAmendmentTransaction(instrumentOid);
            if (createOrAppendAmendmentTransaction(authorisedInvSummaryDoc,pendingAmendmentOid, securityRights, invSummaryData) == false)
            {
                cleanupInvoiceSummaryData("checkForAuthorizedAmendOrProcessedByBankMatch");
                return true;
            }

            // If a copy of the PO line item has been created, save it too
            if(copyOfinvoicesSummaryData != null)
       
            // Log the creation of amendment or appending PO to existing amendment.
            if (pendingAmendmentOid != 0)
            {
                logDebug("Invoice "+ getInvoice_id() +" was added to amendment under Instrument "
                        + invSummaryTransaction.getAttributeLong("instrument_oid"));
            }
            else
            {
                logDebug("Amendment started under Instrument "
                        + invSummaryTransaction.getAttributeLong("instrument_oid") +" for Invoice " + getInvoice_id() );
            }
            cleanupInvoiceSummaryData("checkForAuthorizedAmendOrProcessedByBankMatch");
            return true;

    }

    /**
     * This method is used to create a new Import LC - Amend transaction for the
     * PO line item currently being processed, or to append the PO to an existing
     * and pending Amendment. It is only called when a PO line
     * item matches an existing PO line item that belong to an Authorized Amendment
     * or some other Processed transaction.  If a new Amend transaction is created,
     * it is created in the same way a normal Import LC - Amend transaction is created.
     *

     * @param      matchingInvSummaryDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @param     pendingAmendmentOid - Pending Amendment Oid.  Append the PO to this amendment
     *                                        if this parameter is not 0.
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not the Import LC - Amend transaction was created successfully
     *                       (true  - the Import LC - Amend transaction was created successfully
     *                        false - the Import LC - Amend transaction was not created successfully)
     */
    private boolean createOrAppendAmendmentTransaction(DocumentHandler matchingInvSummaryDoc, long pendingAmendmentOid, String securityRights,InvoicesSummaryData invSummaryData) // logu & amit
            throws AmsException, RemoteException
    {
        Transaction   amendTransaction = null;
        Instrument    instrument          = null;
        String[]      attributeValues     = null;
        String[]      attributePaths      = null;
        long          transactionOid      = 0;
        long          lockingUserOid      = 0;
        long          instrumentOid       = 0;

        invoicesSummaryData = invSummaryData;
        // Since the PO is now going to be active for the amendment,
        // a copy must be made of its data so that we can maintain data
        // at the transaction level after the PO is no longer active.

        // Populate a copy
        attributeValues = new String[1];
        attributePaths  = new String[1];

        // Clear out some of the associations on the copy.
        // Keep in mind that the transactional and shipment association (assigned_to_trans_oid, shipment_oid) are preserved
        // so that the previous transaction and shipment still own the old PO data
        // Do you really need to null out previous_purchase_order_oid and auto_added_to_amend_ind? Need investigation.  -- Weian Zhu 12/22/04
        attributePaths[0]  = "instrument_oid";
        attributeValues[0] = "";

         // Instantiate the instrument so that we can perform a bunch of validations on it
        // and save the new Amend transaction
         instrumentOid = matchingInvSummaryDoc.getAttributeLong("/ResultSetRecord(0)/A_INSTRUMENT_OID");
        instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", instrumentOid);

        // If the user doesn't have the security rights to create an Import LC - Amend
        // transaction, add an error message to the Auto LC log and stop processing the
        // PO line item

        if (StringFunction.isBlank(instrumentType)) {
            instrumentType = instrument.getAttribute("instrument_type_code");
        }

        if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
            if (!SecurityAccess.canCreateModInstrument(securityRights, InstrumentType.APPROVAL_TO_PAY,
                    TransactionType.AMEND))
            {
                removeInstrument(instrument, "createNewATPAmendmentTransaction");
                return false;
            }
        }

        // If the status of the instrument is Pending, add an error message to the Auto LC log
        // and stop processing the PO line item
        if (instrument.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_PENDING))
        {
            removeInstrument(instrument, "createNewImportLCAmendmentTransaction");
            return false;
        }


        // Try locking the instrument so that it's not updated by more than one person at the same
        // time; if someone else is currently updating the instrument, add an error message to the
        // Auto LC log and stop processing the PO line item. If the same user is trying to update
        // the instrument somehow, he/she already has the instrument locked so continue processing.
        try
        {
            LockingManager.lockBusinessObject(instrumentOid, (new Long(getA_user_oid())).longValue(), true);
        }
        catch (InstrumentLockException e)
        {
            lockingUserOid = e.getUserOid();

            if (!String.valueOf(a_user_oid).equals(String.valueOf(lockingUserOid)))
            {
                removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

                return false;
            }
        }

        Terms terms = null;
        String seq= authorisedInvSummaryDoc.getAttribute("/ResultSetRecord(0)/AMEND_SEQ_NO");
        if (StringFunction.isNotBlank(seq)) {
            seq=String.valueOf(Integer.parseInt(seq) + 1);
        }
        else {
            seq="1";
        }

        // If there is pending amendment passed in,
        // then add PO to amendment, otherwise create a new amendment.
        if ( pendingAmendmentOid!= 0)
        {
            transactionOid = pendingAmendmentOid;
            amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
            terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");

            amendTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
            amendTransaction.save(false);
        }
        else
        {
           // Create the new amendment transaction and save it
            transactionOid = instrument.newComponent("TransactionList");
            amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
            if (StringFunction.isBlank(instrumentType)) {
                instrumentType = instrument.getAttribute("instrument_type_code");
            }

            if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
              amendTransaction.createNewBlankInvTran(TransactionType.AMEND, String.valueOf(a_user_oid), String.valueOf(a_corp_org_oid),
                        InstrumentType.APPROVAL_TO_PAY, false, "", null, invoicesSummaryData);
            }
            terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");

            // Save the new transaction and Invoices before trying to derive the data
            // since the derivation assumes the data are saved in the database.
            instrument.save(false);
        }

       //IR T36000016940 start - set invoice status and verify Invoice group- if needed new invoice group will be created
		invSummaryData.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
		invSummaryData.verifyInvoiceGroup();
		String newGroupOid = invSummaryData.getAttribute("invoice_group_oid");
		//IR T36000016940 end
        String previousSql = "update invoices_summary_data "
        		+ "set a_instrument_oid=?, a_transaction_oid=?, "
        		+ "a_terms_oid=?, invoice_status=?, amend_seq_no=?, "
        		+ "linked_to_instrument_type= ?, a_invoice_group_oid=? "
        		+ "where upload_invoice_oid = ?";

		int success = 0;
		try {
			success = DatabaseQueryBean.executeUpdate(previousSql, false, new Object[]{String.valueOf(instrumentOid), String.valueOf(transactionOid),
					terms.getAttribute("terms_oid"), TradePortalConstants.PO_STATUS_ASSIGNED, seq, InstrumentType.APPROVAL_TO_PAY,
					newGroupOid, invSummaryData.getAttribute("upload_invoice_oid")});
		} catch (SQLException e) {
			LOG.error("SQLException while updating invoices_summary_data... ");
		}

		 Vector invOidsList = new Vector();
	        Vector invIDList = new Vector();
	     String sb = " select invoice_id,upload_invoice_oid from invoices_summary_data where a_transaction_oid = ?";

	      DocumentHandler resultSet1 = DatabaseQueryBean.getXmlResultSet(sb, false, new Object[]{transactionOid});
		  if(resultSet1!=null){
		  List<DocumentHandler> rows = resultSet1.getFragmentsList("/ResultSetRecord");
			 for ( DocumentHandler row :rows) {
				  String poNum = row.getAttribute("/INVOICE_ID");
				 invIDList.addElement(poNum);
				 String invOID = row.getAttribute("/UPLOAD_INVOICE_OID");
				invOidsList.addElement(invOID);
			 }
		  }

        //We need to calculate just amount and currency. Dates cannot be changed. If Due date is different then throw
        //error
        InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(amendTransaction,terms,invOidsList,instrument,invIDList,"");
        String dueDate =invSummaryData.getActualDate();

     	SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Date date = jPylon.parse(dueDate);
			terms.setAttribute("invoice_due_date",
					DateTimeUtility.convertDateToDateTimeString(date));
			terms.setAttribute("expiry_date",
					DateTimeUtility.convertDateToDateTimeString(date));
		} catch (ParseException e) {
			LOG.error("Exception while parsing Invoice Due Date: ",e);
		}
        terms.setAttribute("invoice_only_ind",
				TradePortalConstants.INDICATOR_YES);
        // Save any change of transaction data derived from invoices.
        instrument.save(false);
        removeInstrument(instrument, "createNewATPAmendmentTransaction");
        return true;
    }

    /**
     * This method is used to remove an Instrument EJB that was used during the
     * processing of the current PO line item. The calling method parameter is
     * used simply for debugging purposes if an error occurs while removing this
     * object.
     *

     * @param      instrument - the instrument EJB to remove from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeInstrument(Instrument instrument, String methodOrigin) throws RemoteException
    {
        try
        {
            if(instrument != null)
            {
                instrument.remove();
                instrument = null;
            }
        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("instrument", methodOrigin);
        }
    }

  
    /**
     * This method is used to get pending amendment for an instrument
     * If there are multiple amendment, use the one created earliest.
     *

     * @param      instrumentOid
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     long - Pending Amendment transaction Oid.
     *                0 if none if found.
     */
    private long getPendingAmendmentTransaction(String instrumentOid)
            throws AmsException
    {
        long pendingAmendmentOid = 0;
        DocumentHandler pendingAmdTransactionsDoc = null;
        String sqlQuery  = "select a.upload_invoice_oid, b.transaction_oid  "
        		+ "from invoices_summary_data a, transaction  b "
        		+ "where b.p_instrument_oid = ? and a.invoice_classification=? "
        		+ "and a.a_instrument_oid = b.p_instrument_oid AND b.transaction_status not in (?,?,?,?,?,?) "
        		+ "AND b.transaction_type_code = 'AMD' order by date_started";
        
        Object [] sqlPrmsLst = new Object[8];
        sqlPrmsLst[0] = instrumentOid;
        sqlPrmsLst[1] = getInvoice_classification();
        sqlPrmsLst[2] = TransactionStatus.AUTHORIZED;
        sqlPrmsLst[3] = TransactionStatus.PROCESSED_BY_BANK;
        sqlPrmsLst[4] = TransactionStatus.CANCELLED_BY_BANK;
        sqlPrmsLst[5] = TransactionStatus.DELETED;
        sqlPrmsLst[6] = TransactionStatus.PARTIALLY_AUTHORIZED;
        sqlPrmsLst[7] = TransactionStatus.REJECTED_BY_BANK;

        pendingAmdTransactionsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

        if (pendingAmdTransactionsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (pendingAmdTransactionsDoc.getFragmentsList("/ResultSetRecord").size() > 0)
            {
                pendingAmendmentOid = Long.parseLong(pendingAmdTransactionsDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_OID"));
            }
        }
        return pendingAmendmentOid;
    }

 
    /**
     * This method is used to determine whether or not the PO upload definition
     * OID, beneficiary name, and currency for the current Invoice Summary Data are the
     * same as the existing (i.e., matching) Invoice Summary Data's fields. It will
     * initially call a method to instantiate the Invoice Summary Data that was matched,
     * so its data can be retrieved for comparison.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the Invoice Summary Data that
     *                                                                           the current Invoice Summary Data matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not the current Invoice Summary Data fields are the same as the existing PO line
     *                       item fields
     *                       (true  - the Invoice Summary Data fields are the same
     *                        false - the Invoice Summary Data fields are not the same)
     */
    private boolean invSummaryDataFieldsMatchTransactionFields(DocumentHandler matchingPOLineItemsDoc)
            throws AmsException, RemoteException
    {
        String   transactionBeneficiaryName       = null;
        String   transactionCurrency              = null;
        long     transactionPOUploadDefinitionOid = 0;

        getMatchingInvoicesSummaryData(matchingPOLineItemsDoc);

        transactionPOUploadDefinitionOid = invoicesSummaryData.getAttributeLong("upload_definition_oid");
        transactionBeneficiaryName       = StringFunction.isNotBlank(invoicesSummaryData.getAttribute("seller_name"))?invoicesSummaryData.getAttribute("seller_name"):invoicesSummaryData.getAttribute("seller_id");
        transactionCurrency              = invoicesSummaryData.getAttribute("currency");
        //IR T36000016210 - check if trading partner is same
        if ((a_upload_definition_oid == transactionPOUploadDefinitionOid) &&
                (getTp_rule_name().equalsIgnoreCase(transactionBeneficiaryName)) &&
                (getCurrency().equals(transactionCurrency)))
        {
            return true;
        }
        return false;
    }

    /**
     * This method is used to determine whether or not the current Invoice Summary
     * has the same Invoice Id and item number as an unassigned Invoice Summary in
     * the Invoice Summary database table. If a match is found, the method will
     * call methods to validate the current Invoice Summary data, update the
     * existing record's data if successful, and add a message to the Auto LC
     * log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a Invoice Summary match was found
     *                       (true  - a Invoice Summary match was found, so stop processing the Invoice Summary
     *                        false - a Invoice Summary match was not found, so continue processing the Invoice Summary)
     */
    private boolean checkForUnassignedInvoicesMatch(HashMap removeOids) throws AmsException, RemoteException
    {
        DocumentHandler matchingInvSummaryDataDoc = null;
        String      sqlQuery               = null;
        List<Object> sqlPrmLst = new ArrayList<Object>();

        // Compose the SQL to retrieve Invoice Summarys that match the Invoice Id and line item
        // number passed in and that are NOT currently assigned to a transaction
        if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equalsIgnoreCase(getInvoice_classification())){
	        sqlQuery = "select upload_invoice_oid from invoices_summary_data "
	        		+ "where a_corp_org_oid = ? and invoice_classification=? "
	        		+ "and invoice_id = ? and a_transaction_oid is null";
	        
	        sqlPrmLst.add(getA_corp_org_oid());
	        sqlPrmLst.add(getInvoice_classification());
	        sqlPrmLst.add(getInvoice_id());
        }
        else if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equalsIgnoreCase(getInvoice_classification()) && !InstrumentType.LOAN_RQST.equalsIgnoreCase(getLinked_to_instrument_type())){
        	sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a "
        			+ "where a.a_corp_org_oid = ? and invoice_classification=? and a.invoice_id = ? "
        			+ "and invoice_status <> ? and a_transaction_oid is null";
            
            sqlPrmLst.add(a_corp_org_oid);
            sqlPrmLst.add(getInvoice_classification());
            sqlPrmLst.add(getInvoice_id());
            sqlPrmLst.add(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH);
        }
      //IR 16297 start
        else if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equalsIgnoreCase(getInvoice_classification()) && InstrumentType.LOAN_RQST.equalsIgnoreCase(getLinked_to_instrument_type())){
        	sqlQuery = "select a.upload_invoice_oid from invoices_summary_data a "
        			+ "where a.a_corp_org_oid = ? and invoice_classification=? and a.invoice_id = ? "
        			+ "and invoice_status not in (?,?,?) and a_transaction_oid is null";
            sqlPrmLst.add(a_corp_org_oid);
            sqlPrmLst.add(getInvoice_classification());
            sqlPrmLst.add(getInvoice_id());
            sqlPrmLst.add(TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH);
            sqlPrmLst.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
            sqlPrmLst.add(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
        }
      //IR 16297 end
        // Retrieve any Invoice Summarys that satisfy the SQL that was just constructed
        matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,false,sqlPrmLst);

        // If a match was found, try to validate the current Invoice Summary's data and
        // save it to the matching Invoice Summary.
        if (matchingInvSummaryDataDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                return true;
            }

            // Update the existing Invoice Summary's data with the data from the current
            // Invoice Summary and log a message to the Auto LC log file
            //SHR start
            String invSummaryOid   = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/UPLOAD_INVOICE_OID");
            removeOids.put("invOID",invSummaryOid);
            removeOids.put("RPL_VAL", "UI");
            //SHR end
            getFields().put("invoice_type", TradePortalConstants.INITIAL_INVOICE); //IR T36000016894
            return true;
        }
        return false;
    }
    
    private boolean checkForUnassignedCreditNote(HashMap removeOids) throws AmsException, RemoteException
    {
        DocumentHandler matchingInvSummaryDataDoc = null;
        StringBuilder      sqlQuery               = new StringBuilder("select upload_credit_note_oid,credit_note_applied_status,credit_note_status,end_to_end_id,deleted_ind from credit_note");
        List<Object> sqlPrmLst = new ArrayList<Object>();
             sqlQuery.append("where a_corp_org_oid = ? and invoice_classification=? ");
            sqlQuery.append("and invoice_id = ? ");
            sqlPrmLst.add(getA_corp_org_oid());
            sqlPrmLst.add(getInvoice_classification());
            sqlPrmLst.add(getInvoice_id());
	      
         matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(),false,sqlPrmLst);
         if (matchingInvSummaryDataDoc != null)
        {
        	 List errList = getErrorList();
             if (errList == null) {
                 errList = new ArrayList();
             }
             IssuedError ie = null;
            // If more than one record is found, log an error message and return
            if (matchingInvSummaryDataDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
            	String[] substitutionValues = { getInvoice_id(), "" };
                ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.DUPLICATE_INVOICE, substitutionValues, null);
                errList.add(ie);
                setErrorList(errList);
                return true;
            }
            String isDeleted = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/DELETED_IND");
        	String appliedStatus = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/CREDIT_NOTE_APPLIED_STATUS");
        	String endToendId = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/END_TO_END_ID");
            String invSummaryOid   = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/UPLOAD_CREDIT_NOTE_OID");
            String creditNoteStatus = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord/CREDIT_NOTE_STATUS");
           
            boolean isError=false;
            if(TradePortalConstants.INDICATOR_YES.equals(isDeleted)){
            	String[] substitutionValues = { getInvoice_id(), "" };
                ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.DELETED_INVOICE, substitutionValues, null);                
                isError= true;
            }
            else if((StringFunction.isNotBlank(getEnd_to_end_id()) && !getEnd_to_end_id().equalsIgnoreCase(endToendId)) ||
            		(StringFunction.isBlank(getEnd_to_end_id()) && StringFunction.isNotBlank(endToendId))){
            	String[] substitutionValues = { getInvoice_id(), "" };
                 ie = mediatorServices.getErrorManager().getIssuedError(
                        csdb.getLocaleName(), TradePortalConstants.DIFFERENT_ENDTOENDID, substitutionValues, null);               
                isError= true;
            }
            else if(StringFunction.isNotBlank(getEnd_to_end_id()) && TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(creditNoteStatus)){
        	   String[] substitutionValues = { getInvoice_id(), "" };
                ie = mediatorServices.getErrorManager().getIssuedError(
                       csdb.getLocaleName(), TradePortalConstants.PAUT_INVOICE, substitutionValues, null);              
               isError= true;
           }
           else if(StringFunction.isBlank(getEnd_to_end_id()) && !TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP.equals(appliedStatus)){
        	   String[] substitutionValues = { getInvoice_id(), "" };
               ie = mediatorServices.getErrorManager().getIssuedError(
                       csdb.getLocaleName(), TradePortalConstants.APPLIED_INVOICE, substitutionValues, null);               
               isError= true;
           }
           if(isError){
        	   errList.add(ie);
        	   setErrorList(errList);
        	   return true;
           }           
           removeOids.put("crOid",invSummaryOid);
           removeOids.put("RPL_VAL", "UI");
           getFields().put("invoice_type", TradePortalConstants.INITIAL_INVOICE); 
            return true;
        }
        return false;
    }


    /**
     * This method is used to instantiate the existing Invoice Summary that matches
     * the current Invoice Summary being processed. Once this object has been
     * instantiated, it can be accessed in other methods like saving, accessing
     * the transaction that the Invoice Summary is assigned to, etc.
     *

     * @param      matchingInvoicesSummaryDataDoc - xml document containing the OID that
     *                                                                           uniquely identifies the Invoice Summary that
     *                                                                           the current Invoice Summary matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void getMatchingInvoicesSummaryData(DocumentHandler matchingInvoicesSummaryDataDoc) throws AmsException, RemoteException
    {
        DocumentHandler   invSummaryDoc = null;
        List<DocumentHandler>   invSummary   = null;
        long              invSummaryOid = 0;
        invSummary   = matchingInvoicesSummaryDataDoc.getFragmentsList("/ResultSetRecord");
        invSummaryDoc =invSummary.get(0);
        invSummaryOid = invSummaryDoc.getAttributeLong("UPLOAD_INVOICE_OID");
        invoicesSummaryData = (InvoicesSummaryData) EJBObjectFactory.createClientEJB(ejbServerLocation,
                "InvoicesSummaryData", invSummaryOid, mediatorServices.getCSDB());
    }
    /**
     * This method is used to remove the Invoice Summary and Invoice Summary
     * transaction EJBs that were used during the processing of the current PO
     * line item. The calling method parameter is used simply for debugging
     * purposes if an error occurs while removing these objects.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void cleanupInvoiceSummaryData(String methodOrigin) throws RemoteException
    {
        removeInvSummaryData(methodOrigin);
        if (invSummaryTransaction != null)
        {
            removeInvSummaryDataTransaction(methodOrigin);
        }
    }
    /**
     * This method is used to remove the Invoice Summary EJB that was used during
     * the processing of the current Invoice Summary. The calling method parameter
     * is used simply for debugging purposes if an error occurs while removing
     * this object.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeInvSummaryData(String methodOrigin) throws RemoteException
    {
        try
        {
            if(invoicesSummaryData != null)
            {
                invoicesSummaryData.remove();
                invoicesSummaryData = null;
            }
            if(copyOfinvoicesSummaryData != null)
            {
                copyOfinvoicesSummaryData.remove();
                copyOfinvoicesSummaryData = null;
            }
        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("Invoice Summary Data", methodOrigin);
        }
    }

    /**
     * This method is used to remove the Invoice Summary transaction EJB that was
     * used during the processing of the current Invoice Summary. The calling
     * method parameter is used simply for debugging purposes if an error occurs
     * while removing this object.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeInvSummaryDataTransaction(String methodOrigin) throws RemoteException
    {
        try
        {
            if(invSummaryTransaction != null)
            {
                invSummaryTransaction.remove();
                invSummaryTransaction = null;
            }
        }
        catch (Exception ex)
        {
            issueRemoveErrorDebugMessage("Invoice Summary transaction", methodOrigin);
        }
    }

    /**
     * This method is used to issue a debug error message indicating that an
     * error occurred while trying to remove a particular EJB object from
     * memory.
     *

     * @param      objectToRemove - description of the EJB object being removed from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void issueRemoveErrorDebugMessage(String objectToRemove, String methodOrigin)
    {
        StringBuffer errorMessage = new StringBuffer();
        errorMessage.append("Error removing ");
        errorMessage.append(objectToRemove);
        errorMessage.append(" in InvoiceSummaryData.");
        errorMessage.append(methodOrigin);
        errorMessage.append("()!");

        LOG.info(errorMessage.toString());
    }


    private void logException(Exception ex) {
            logError(
                    "Exception Occured - Invoice Id: " +  getInvoice_id() + " StackTrace: " + ex.getMessage());
    }

    public boolean findMatchingInvoice() throws AmsException, RemoteException {
        DocumentHandler matchingInvSummaryDataDoc = null;
        String      sqlQuery               = null;

        // Compose the SQL to retrieve Invoice Summarys that match the Invoice Id and line item
        // number passed in and that are NOT currently assigned to a transaction
        sqlQuery = "select upload_invoice_oid,linked_to_instrument_type from invoices_summary_data where a_corp_org_oid = ? and invoice_classification=?  and invoice_id = ?";
        matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,false, getA_corp_org_oid(), getInvoice_classification(), getInvoice_id());

        //Replacement Updates start
        if (matchingInvSummaryDataDoc != null){
        	String type = matchingInvSummaryDataDoc.getAttribute("/ResultSetRecord(0)/LINKED_TO_INSTRUMENT_TYPE");
        	
        	/*
        	 *  If replacement invoices are uploaded with no instrument type, regardless if the original has an instrument type of ATP or not, then they can be replaced, but keep the assigned instrument type from the original.
				If the replacement has an instrument type and the original has no instrument type, then replace.
				If the replacement has an instrument type and the original has a different instrument type, then fail.
       	     */

        	if(StringFunction.isNotBlank(getLinked_to_instrument_type()) && StringFunction.isNotBlank(type) && !getLinked_to_instrument_type().equalsIgnoreCase(type)){
        		return false;
        	}
        	else if(StringFunction.isBlank(getLinked_to_instrument_type()) && StringFunction.isNotBlank(type)){
       		 getFields().put("linked_to_instrument_type", type);
       		 return true;
        	}
        	else{
        		return true;
        	}
        	//IR T36000017393 end
        }else if(TradePortalConstants.INVOICE_TYPE_RPL.equalsIgnoreCase(this.getInvoice_type())){
        	return false;
        }
        return true;

      //Replacement Updates end

    }
    
    /* Performance tuning - Check if its a fresh invoice */
    public boolean checkIfNewInvoice(String ownerOrgOid) throws AmsException, RemoteException {
    	  String sqlQuery = "select upload_invoice_oid from invoices_summary_data where a_corp_org_oid = ? and invoice_classification=? and invoice_id = ?";
          DocumentHandler matchingInvSummaryDataDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,false,ownerOrgOid, getInvoice_classification(), getInvoice_id());

          if (matchingInvSummaryDataDoc != null){
          	return false;          	
          }else if(TradePortalConstants.INVOICE_TYPE_RPL.equalsIgnoreCase(this.getInvoice_type())){
          	return false;
          }
          return true;

    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText) {
        String str = " : Invoice Summary Object: " + getInvoice_id()
                + "  ";

        Logger.getLogger().log(loggerCategory,getInvoice_id(),
                getInvoice_id() + ": ERROR: " + errorText + str,
                Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText, Exception e) {
        String str = " : Invoice Summary Object: " + getInvoice_id()
                + "  ";
        Logger.getLogger().log(
                loggerCategory,getInvoice_id(),
                getInvoice_id() + ": ERROR: " + errorText + ": " + e.getMessage()
                        + str, Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs Debug info
     *
     */
    public void logDebug(String infoText) {
        String str = " : Invoice Summary Object: " + getInvoice_id()
                + "  ";

        Logger.getLogger().log(loggerCategory,getInvoice_id(),
                getInvoice_id() + ": INFO: " + infoText + str,
                Logger.DEBUG_LOG_SEVERITY);
    }

//RKAZI 09/04/2012 CR-708 Rel 8.1.1 End

  // Release 8.2.0.0 IR-T36000014567 Narayan 03/15/2013
  /**
   * this method check for total length of Beneficiary Bank/Branch City, Province and Country.
   * total length should not be greater than 31 as these three value is mapped in TPS in address 3 field and this
   * field length is 31 in TPS. if length is more than 31, issue error.
   *
   * @return true if valid, false if not valid length.
   * @throws AmsException
   */
  public boolean isValidBenBankAddressSize() throws AmsException{
	boolean isValidLength     = true;
	int benBankCityLength     = 0;
	int benBankProvinceLength = 0;
	int benBankCountryLength  = 0;

	if(this.getBen_bank_city() != null){
		benBankCityLength = getBen_bank_city().length();
	}

	if(this.getBen_bank_province() != null){
		benBankProvinceLength = getBen_bank_province().length();
	}

	if(this.getBen_branch_country() != null){
		benBankCountryLength = getBen_branch_country().length();
	}

	int totalSizeInInvoice  = benBankCityLength + benBankProvinceLength + benBankCountryLength;

	if(totalSizeInInvoice > MAX_BEN_BANK_ADDRESS_SIZE){
		isValidLength = false;
		mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.INV_BEN_BANK_ADD_SIZE_EXCEED, String.valueOf(MAX_BEN_BANK_ADDRESS_SIZE));
	}

	return isValidLength;
  }
  
  public boolean isCreditNote() throws AmsException, RemoteException {
		String creditNote = getCredit_note();//(String) getFields().get("credit_note");
		BigDecimal amount = getAmount();
		if (StringFunction.isNotBlank(creditNote) && amount != null && amount.signum() != -1){
			amount = amount.negate();
		}
		
		return amount != null && amount.signum() == -1;
	}

  public boolean isPayCreditNote() throws AmsException, RemoteException {
		return isCreditNote() && TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(getInvoice_classification());
	}
  
  private CorporateOrganization getOwnerOrg() throws RemoteException, AmsException{
	  CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory
				.createClientEJB(
						ejbServerLocation,
						"CorporateOrganization",
						a_corp_org_oid);
	  return corpOrg;
	  
  }
}
