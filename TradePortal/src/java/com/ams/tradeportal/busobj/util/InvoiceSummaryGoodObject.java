package com.ams.tradeportal.busobj.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.ams.tradeportal.common.InvoiceFileDetails;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;

public class InvoiceSummaryGoodObject {
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceSummaryGoodObject.class);

	private long upload_invoice_goods_oid;
	private String purchase_order_id;
	private String goods_description;
	private String incoterm;
	private String country_of_loading;
	private String country_of_discharge;
	private String vessel;
	private String carrier;
	private Date actual_ship_date;
	private String buyer_users_def1_label;
	private String buyer_users_def2_label;
	private String buyer_users_def3_label;
	private String buyer_users_def4_label;
	private String buyer_users_def5_label;
	private String buyer_users_def6_label;
	private String buyer_users_def7_label;
	private String buyer_users_def8_label;
	private String buyer_users_def9_label;
	private String buyer_users_def10_label;
	private String buyer_users_def1_value;
	private String buyer_users_def2_value;
	private String buyer_users_def3_value;
	private String buyer_users_def4_value;
	private String buyer_users_def5_value;
	private String buyer_users_def6_value;
	private String buyer_users_def7_value;
	private String buyer_users_def8_value;
	private String buyer_users_def9_value;
	private String buyer_users_def10_value;
	private String seller_users_def1_label;
	private String seller_users_def2_label;
	private String seller_users_def3_label;
	private String seller_users_def4_label;
	private String seller_users_def5_label;
	private String seller_users_def6_label;
	private String seller_users_def7_label;
	private String seller_users_def8_label;
	private String seller_users_def9_label;
	private String seller_users_def10_label;
	private String seller_users_def1_value;
	private String seller_users_def2_value;
	private String seller_users_def3_value;
	private String seller_users_def4_value;
	private String seller_users_def5_value;
	private String seller_users_def6_value;
	private String seller_users_def7_value;
	private String seller_users_def8_value;
	private String seller_users_def9_value;
	private String seller_users_def10_value;
	private long p_invoice_summary_data_oid;
	private boolean isValid = true;
	private boolean isNewGoods = true;

	private Set<InvoiceLineItemObject> invoiceLineItem;
	private Map fields = null;

	public Date getActual_ship_date() {
		return actual_ship_date;
	}

	public void setActual_ship_date(Date actual_ship_date) {
		this.actual_ship_date = actual_ship_date;
	}

	public String getBuyer_users_def1_label() {
		return buyer_users_def1_label;
	}

	public void setBuyer_users_def1_label(String buyer_users_def1_label) {
		this.buyer_users_def1_label = buyer_users_def1_label;
	}

	public String getBuyer_users_def1_value() {
		return buyer_users_def1_value;
	}

	public void setBuyer_users_def1_value(String buyer_users_def1_value) {
		this.buyer_users_def1_value = buyer_users_def1_value;
	}

	public String getBuyer_users_def10_label() {
		return buyer_users_def10_label;
	}

	public void setBuyer_users_def10_label(String buyer_users_def10_label) {
		this.buyer_users_def10_label = buyer_users_def10_label;
	}

	public String getBuyer_users_def10_value() {
		return buyer_users_def10_value;
	}

	public void setBuyer_users_def10_value(String buyer_users_def10_value) {
		this.buyer_users_def10_value = buyer_users_def10_value;
	}

	public String getBuyer_users_def2_label() {
		return buyer_users_def2_label;
	}

	public void setBuyer_users_def2_label(String buyer_users_def2_label) {
		this.buyer_users_def2_label = buyer_users_def2_label;
	}

	public String getBuyer_users_def2_value() {
		return buyer_users_def2_value;
	}

	public void setBuyer_users_def2_value(String buyer_users_def2_value) {
		this.buyer_users_def2_value = buyer_users_def2_value;
	}

	public String getBuyer_users_def3_label() {
		return buyer_users_def3_label;
	}

	public void setBuyer_users_def3_label(String buyer_users_def3_label) {
		this.buyer_users_def3_label = buyer_users_def3_label;
	}

	public String getBuyer_users_def3_value() {
		return buyer_users_def3_value;
	}

	public void setBuyer_users_def3_value(String buyer_users_def3_value) {
		this.buyer_users_def3_value = buyer_users_def3_value;
	}

	public String getBuyer_users_def4_label() {
		return buyer_users_def4_label;
	}

	public void setBuyer_users_def4_label(String buyer_users_def4_label) {
		this.buyer_users_def4_label = buyer_users_def4_label;
	}

	public String getBuyer_users_def4_value() {
		return buyer_users_def4_value;
	}

	public void setBuyer_users_def4_value(String buyer_users_def4_value) {
		this.buyer_users_def4_value = buyer_users_def4_value;
	}

	public String getBuyer_users_def5_label() {
		return buyer_users_def5_label;
	}

	public void setBuyer_users_def5_label(String buyer_users_def5_label) {
		this.buyer_users_def5_label = buyer_users_def5_label;
	}

	public String getBuyer_users_def5_value() {
		return buyer_users_def5_value;
	}

	public void setBuyer_users_def5_value(String buyer_users_def5_value) {
		this.buyer_users_def5_value = buyer_users_def5_value;
	}

	public String getBuyer_users_def6_label() {
		return buyer_users_def6_label;
	}

	public void setBuyer_users_def6_label(String buyer_users_def6_label) {
		this.buyer_users_def6_label = buyer_users_def6_label;
	}

	public String getBuyer_users_def6_value() {
		return buyer_users_def6_value;
	}

	public void setBuyer_users_def6_value(String buyer_users_def6_value) {
		this.buyer_users_def6_value = buyer_users_def6_value;
	}

	public String getBuyer_users_def7_label() {
		return buyer_users_def7_label;
	}

	public void setBuyer_users_def7_label(String buyer_users_def7_label) {
		this.buyer_users_def7_label = buyer_users_def7_label;
	}

	public String getBuyer_users_def7_value() {
		return buyer_users_def7_value;
	}

	public void setBuyer_users_def7_value(String buyer_users_def7_value) {
		this.buyer_users_def7_value = buyer_users_def7_value;
	}

	public String getBuyer_users_def8_label() {
		return buyer_users_def8_label;
	}

	public void setBuyer_users_def8_label(String buyer_users_def8_label) {
		this.buyer_users_def8_label = buyer_users_def8_label;
	}

	public String getBuyer_users_def8_value() {
		return buyer_users_def8_value;
	}

	public void setBuyer_users_def8_value(String buyer_users_def8_value) {
		this.buyer_users_def8_value = buyer_users_def8_value;
	}

	public String getBuyer_users_def9_label() {
		return buyer_users_def9_label;
	}

	public void setBuyer_users_def9_label(String buyer_users_def9_label) {
		this.buyer_users_def9_label = buyer_users_def9_label;
	}

	public String getBuyer_users_def9_value() {
		return buyer_users_def9_value;
	}

	public void setBuyer_users_def9_value(String buyer_users_def9_value) {
		this.buyer_users_def9_value = buyer_users_def9_value;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getCountry_of_discharge() {
		return country_of_discharge;
	}

	public void setCountry_of_discharge(String country_of_discharge) {
		this.country_of_discharge = country_of_discharge;
	}

	public String getCountry_of_loading() {
		return country_of_loading;
	}

	public void setCountry_of_loading(String country_of_loading) {
		this.country_of_loading = country_of_loading;
	}

	public String getGoods_description() {
		return goods_description;
	}

	public void setGoods_description(String goods_description) {
		this.goods_description = goods_description;
	}

	public String getIncoterm() {
		return incoterm;
	}

	public void setIncoterm(String incoterm) {
		this.incoterm = incoterm;
	}

	public long getP_invoice_summary_data_oid() {
		return p_invoice_summary_data_oid;
	}

	public void setP_invoice_summary_data_oid(long p_invoice_summary_data_oid) {
		this.p_invoice_summary_data_oid = p_invoice_summary_data_oid;
	}

	public String getPurchase_order_id() {
		return purchase_order_id;
	}

	public void setPurchase_order_id(String purchase_order_id) {
		this.purchase_order_id = purchase_order_id;
	}

	public String getSeller_users_def1_label() {
		return seller_users_def1_label;
	}

	public void setSeller_users_def1_label(String seller_users_def1_label) {
		this.seller_users_def1_label = seller_users_def1_label;
	}

	public String getSeller_users_def1_value() {
		return seller_users_def1_value;
	}

	public void setSeller_users_def1_value(String seller_users_def1_value) {
		this.seller_users_def1_value = seller_users_def1_value;
	}

	public String getSeller_users_def10_label() {
		return seller_users_def10_label;
	}

	public void setSeller_users_def10_label(String seller_users_def10_label) {
		this.seller_users_def10_label = seller_users_def10_label;
	}

	public String getSeller_users_def10_value() {
		return seller_users_def10_value;
	}

	public void setSeller_users_def10_value(String seller_users_def10_value) {
		this.seller_users_def10_value = seller_users_def10_value;
	}

	public String getSeller_users_def2_label() {
		return seller_users_def2_label;
	}

	public void setSeller_users_def2_label(String seller_users_def2_label) {
		this.seller_users_def2_label = seller_users_def2_label;
	}

	public String getSeller_users_def2_value() {
		return seller_users_def2_value;
	}

	public void setSeller_users_def2_value(String seller_users_def2_value) {
		this.seller_users_def2_value = seller_users_def2_value;
	}

	public String getSeller_users_def3_label() {
		return seller_users_def3_label;
	}

	public void setSeller_users_def3_label(String seller_users_def3_label) {
		this.seller_users_def3_label = seller_users_def3_label;
	}

	public String getSeller_users_def3_value() {
		return seller_users_def3_value;
	}

	public void setSeller_users_def3_value(String seller_users_def3_value) {
		this.seller_users_def3_value = seller_users_def3_value;
	}

	public String getSeller_users_def4_label() {
		return seller_users_def4_label;
	}

	public void setSeller_users_def4_label(String seller_users_def4_label) {
		this.seller_users_def4_label = seller_users_def4_label;
	}

	public String getSeller_users_def4_value() {
		return seller_users_def4_value;
	}

	public void setSeller_users_def4_value(String seller_users_def4_value) {
		this.seller_users_def4_value = seller_users_def4_value;
	}

	public String getSeller_users_def5_label() {
		return seller_users_def5_label;
	}

	public void setSeller_users_def5_label(String seller_users_def5_label) {
		this.seller_users_def5_label = seller_users_def5_label;
	}

	public String getSeller_users_def5_value() {
		return seller_users_def5_value;
	}

	public void setSeller_users_def5_value(String seller_users_def5_value) {
		this.seller_users_def5_value = seller_users_def5_value;
	}

	public String getSeller_users_def6_label() {
		return seller_users_def6_label;
	}

	public void setSeller_users_def6_label(String seller_users_def6_label) {
		this.seller_users_def6_label = seller_users_def6_label;
	}

	public String getSeller_users_def6_value() {
		return seller_users_def6_value;
	}

	public void setSeller_users_def6_value(String seller_users_def6_value) {
		this.seller_users_def6_value = seller_users_def6_value;
	}

	public String getSeller_users_def7_label() {
		return seller_users_def7_label;
	}

	public void setSeller_users_def7_label(String seller_users_def7_label) {
		this.seller_users_def7_label = seller_users_def7_label;
	}

	public String getSeller_users_def7_value() {
		return seller_users_def7_value;
	}

	public void setSeller_users_def7_value(String seller_users_def7_value) {
		this.seller_users_def7_value = seller_users_def7_value;
	}

	public String getSeller_users_def8_label() {
		return seller_users_def8_label;
	}

	public void setSeller_users_def8_label(String seller_users_def8_label) {
		this.seller_users_def8_label = seller_users_def8_label;
	}

	public String getSeller_users_def8_value() {
		return seller_users_def8_value;
	}

	public void setSeller_users_def8_value(String seller_users_def8_value) {
		this.seller_users_def8_value = seller_users_def8_value;
	}

	public String getSeller_users_def9_label() {
		return seller_users_def9_label;
	}

	public void setSeller_users_def9_label(String seller_users_def9_label) {
		this.seller_users_def9_label = seller_users_def9_label;
	}

	public String getSeller_users_def9_value() {
		return seller_users_def9_value;
	}

	public void setSeller_users_def9_value(String seller_users_def9_value) {
		this.seller_users_def9_value = seller_users_def9_value;
	}

	public long getUpload_invoice_goods_oid() {
		return upload_invoice_goods_oid;
	}

	public void setUpload_invoice_goods_oid(long upload_invoice_goods_oid) {
		this.upload_invoice_goods_oid = upload_invoice_goods_oid;
	}

	public String getVessel() {
		return vessel;
	}

	public void setVessel(String vessel) {
		this.vessel = vessel;
	}

	public Set<InvoiceLineItemObject> getInvoiceLineItem() {
		return invoiceLineItem;
	}

	public void setInvoiceLineItem(Set<InvoiceLineItemObject> invoiceLineItem) {
		this.invoiceLineItem = invoiceLineItem;
	}

	public void setInvoiceLineItem(InvoiceLineItemObject invoiceLineItemObj) {
		if (invoiceLineItem == null)
			invoiceLineItem = new HashSet<InvoiceLineItemObject>();
		this.invoiceLineItem.add(invoiceLineItemObj);
	}

	public Map getFields() {
		return fields;
	}

	public void setFields(Map fields) {
		this.fields = fields;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean valid) {
		isValid = valid;
	}

	public boolean isNewGoods() {
		return isNewGoods;
	}

	public void setNewGoods(boolean isNewGoods) {
		this.isNewGoods = isNewGoods;
	}

	/**
	 * This method processes a Purchase Order Line Item Object. It maps all the passed in values to appropriate fields on the Object
	 * and while doing so it validates the values also. If any errors found then it logs these errros and marks itself invalid.
	 * 
	 * @param liFieldNamesList
	 * @param record
	 * @param purchaseOrderDefinition
	 * @param mediatorServices
	 * @param lineNum
	 * @throws AmsException
	 */
	public void processInvoiceSummaryGoods(List<String> invSummaryGoodsFieldNamesList, String[] record, InvoiceDefinitionWebBean invoiceDefinition, MediatorServices mediatorServices, int lineNum) throws AmsException, RemoteException {
		if (invSummaryGoodsFieldNamesList != null && invSummaryGoodsFieldNamesList.size() > 0) {
			InvoiceFileDetails invoiceFileDetails = new InvoiceFileDetails().getInstance(invoiceDefinition);
			invoiceFileDetails.setMediatorServices(mediatorServices);
			invoiceFileDetails.setCurrentLineNum(lineNum);
			fields = invoiceFileDetails.setFieldValues(invSummaryGoodsFieldNamesList, record, lineNum, this);
		} else if (invSummaryGoodsFieldNamesList == null || invSummaryGoodsFieldNamesList.isEmpty()) {
			setBuyer_users_def1_label("Buyer User Definition Label");
			setBuyer_users_def1_value("Dummy Line Item");
			setValid(true);
			Map fieldsMap = new HashMap();
			fieldsMap.put("buyer_users_def1_label", "Buyer User Definition Label");
			fieldsMap.put("buyer_users_def1_value", "Dummy Line Item");
			setFields(fieldsMap);
		}
		if (isValid)
			setNewGoods(false);
	}
}
