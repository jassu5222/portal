package com.ams.tradeportal.busobj.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.EmailTrigger;
import com.ams.tradeportal.busobj.FXRateBean;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.InvoiceHistory;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.TPCalendarYear;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.ThresholdGroup;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.mediator.InvoiceGroupMediator;
import com.ams.tradeportal.mediator.InvoiceMediator;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

public class InvoiceUtility {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(InvoiceUtility.class);
	// SHR CR708 Rel8.1.1 Start
	private static String userLocale = "en_US";
	private static int numberOfDecimalPlaces = 0;
	private static String transactionCurrency = null;

	private static final String INV_LINE_ITEM_SELECT = "select invoice_id, to_char((case when payment_date is null then due_date else payment_date end),'dd Mon yy') as payment_date, to_char(issue_date,'dd Mon yy') as issue_date, amount, seller_name, currency, buyer_name from invoices_summary_data ";

	private static final String INV_LINE_ITEM_ORDERBY = "order by invoice_id, issue_date";

	// SHR CR708 Rel8.1.1 End

	public static boolean isFinanceable(String invoiceStatus) {
		return (TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || isFinanceAuthorizable(invoiceStatus));
	}

	public static boolean isAuthorizable(String invoiceStatus) {
		return (isFinanceAuthorizable(invoiceStatus) || isNonFinanceAuthorizable(invoiceStatus));
	}

	public static boolean isFinanceAuthorizable(String invoiceStatus) {
		return (TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_RAA.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus));
	}

	public static boolean isNonFinanceAuthorizable(String invoiceStatus) {
		return (
		// IR#T36000005055 - Rel8.1 - 09/04/2012
		// System should not allow to authorize an invoice with status 'Pending - Unable to Finance'
		TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_RAA.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus));
	}

	public static boolean isDeletable(String invoiceStatus) {
		return (TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
				|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus));
	}

	public static boolean isCreditGroup(String invoiceStatus) {
		return (TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(invoiceStatus));
	}

	// Srinivasu_D Rel-8.1.1 IR# T36000005935 09/28/2012 Start
	public static boolean isPayableInvoiceDeletable(String invoiceStatus) {
		return (TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
				|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
				|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus)
				|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus) ||
		// CR913- available for auth invoices can also be deleted
		TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus));
	}

	// Srinivasu_D Rel-8.1.1 IR# T36000005935 09/28/2012 End
	// SHR CR708 Rel8.1.1 Start

	/*
	 * Derive the transaction amount and currency based on Invoices attached to the transaction
	 */
	public static void deriveTransactionCurrencyAndAmountsFromInvoice(Transaction transaction, Terms terms, Vector newInvOidList, Instrument instrument, Vector newInvIDList, String invType) throws AmsException, RemoteException {
		String transactionCurrencyCode = null;
		if (transaction != null) {
			transactionCurrencyCode = transaction.getAttribute("copy_of_currency_code");
		}
		String corpOrgOid = null;
		String baseCurrencyCode = null;
		if (StringFunction.isBlank(transactionCurrencyCode)) {
			if (instrument != null) {
				corpOrgOid = instrument.getAttribute("corp_org_oid");
			} else {
				String instrumentOId = transaction.getAttribute("instrument_oid");
				if (StringFunction.isNotBlank(instrumentOId)) {
					String sqlStatement = "select a_corp_org_oid from instrument where instrument_oid = ?";
					DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[] { instrumentOId });
					corpOrgOid = resultSet.getAttribute("/ResultSetRecord(0)/A_CORP_ORG_OID");
				}
			}

			String sqlStatement = "select base_currency_code from corporate_org where organization_oid = ?";
			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[] { corpOrgOid });
			baseCurrencyCode = resultSet.getAttribute("/ResultSetRecord(0)/BASE_CURRENCY_CODE");
		}
		String newListOfInvOidsAddedSql = null;

		// Create SQL for uploaded POs that are now a part of this shipment
		if ((newInvOidList != null) && (newInvOidList.size() > 0)) {
			newListOfInvOidsAddedSql = POLineItemUtility.getPOLineItemOidsSql(newInvOidList, "upload_invoice_oid");
		}
		String newListOfInvNumsAddedSql = null;
		if ((newInvIDList != null) && (newInvIDList.size() > 0)) {
			newListOfInvNumsAddedSql = POLineItemUtility.getPONumsSql(newInvIDList, "invoice_id");
		}

		String poCurrency = null;

		// Total up the POs from the instantiaed POLineItemList components.
		// These PO Line Items may not have been saved to database.
		if (newListOfInvOidsAddedSql != null) {
			String sqlStatement = "select sum(amount) as amount, max(currency) as currency from invoices_summary_data where (a_transaction_oid = ?) or ("
					+ newListOfInvOidsAddedSql + ")";
			DocumentHandler tempDoc = new DocumentHandler();
			tempDoc.setAttribute("/TEMPLATE_OID", "");
			tempDoc.setAttribute("/CORPORATE_ORG_OID", corpOrgOid);
			tempDoc.setAttribute("/USER_LOCALE", userLocale);
			tempDoc.setAttribute("/BASE_CURRENCY_CODE", baseCurrencyCode);
			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[] { transaction.getAttribute(transaction.getIDAttributeName()) });
			resultSet = resultSet.getFragment("/ResultSetRecord");

			if (StringFunction.isBlank(transactionCurrencyCode)) {
				transactionCurrencyCode = resultSet.getAttribute("/CURRENCY");
			}
			String tempCurrencyCode = resultSet.getAttribute("/CURRENCY");
			BigDecimal tempAmount = resultSet.getAttributeDecimal("/AMOUNT");
			BigDecimal totalinvoiceAmount = BigDecimal.ZERO;
			if (!transactionCurrencyCode.equals(tempCurrencyCode)) {
				BigDecimal convertedAmount = getConvertedAmount(tempAmount, tempCurrencyCode, transactionCurrencyCode, tempDoc);
				if (convertedAmount != null)
					totalinvoiceAmount = totalinvoiceAmount.add(convertedAmount);
			} else {
				totalinvoiceAmount = totalinvoiceAmount.add(tempAmount);
			}

			double totalAmountOfCurrTranFromPO = 0.00;
			try {
				totalAmountOfCurrTranFromPO = totalinvoiceAmount.doubleValue();
			} catch (Exception e) {
				/* Ignore null amount */
				LOG.error("Exception while converting Total Invoice amount to double: ", e);
			}
			// DK IR-RAUM051653353 Rel8.1.1 10/16/2012 Begin
			BigDecimal newTotalPOAmt = totalinvoiceAmount;
			String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
			if (StringFunction.isBlank(trnxCurrencyCode)) {
				trnxCurrencyCode = transactionCurrencyCode;
			}
			int numberOfCurrencyPlaces = 2;
			if (StringFunction.isNotBlank(trnxCurrencyCode)) {
				numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, trnxCurrencyCode));
			}
			LOG.debug("New total invoice Amount in the transaction: {}", newTotalPOAmt);

			if (transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND)) {
				BigDecimal oldTotalPOAmt = getMatchAmount(transaction, newListOfInvOidsAddedSql, instrument, newListOfInvNumsAddedSql, invType);
				LOG.debug("Old invoice Matching Amount: {}", oldTotalPOAmt);
				boolean haveOldAmt = false;

				BigDecimal offSetAmt = BigDecimal.ZERO;
				if (oldTotalPOAmt.intValue() != 0 && oldTotalPOAmt.compareTo(newTotalPOAmt) == 1) {
					offSetAmt = oldTotalPOAmt.subtract(newTotalPOAmt);
					offSetAmt = offSetAmt.negate();
					haveOldAmt = true;
				} else if (oldTotalPOAmt.intValue() != 0 && oldTotalPOAmt.compareTo(newTotalPOAmt) == -1) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				} else if (oldTotalPOAmt.intValue() != 0 && oldTotalPOAmt.compareTo(newTotalPOAmt) == 0) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				}

				newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
				if (offSetAmt.intValue() != 0) {
					offSetAmt = offSetAmt.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
				} else if (!haveOldAmt) {
					offSetAmt = newTotalPOAmt;
				}
				LOG.debug("OffSet INV Amount in the transaction: {}", offSetAmt);
				transaction.setAttribute("copy_of_amount", String.valueOf(offSetAmt));
				transaction.setAttribute("instrument_amount", String.valueOf(oldTotalPOAmt));
				// DK IR-RAUM051653353 Rel8.1.1 10/16/2012 Begin
				terms.setAttribute("amount", String.valueOf(offSetAmt));
			} else if (("RPL".equalsIgnoreCase(invType) || "AMD".equalsIgnoreCase(invType))
					&& !transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND)) {
				transaction.setAttribute("instrument_amount", String.valueOf(newTotalPOAmt));
				transaction.setAttribute("copy_of_amount", String.valueOf(newTotalPOAmt));
				terms.setAttribute("amount", String.valueOf(newTotalPOAmt));
				if (instrument != null) {
					instrument.setAttribute("copy_of_instrument_amount", String.valueOf(newTotalPOAmt));
				}
			} else {
				if (totalAmountOfCurrTranFromPO != 0) {
					newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
					transaction.setAttribute("copy_of_amount", String.valueOf(newTotalPOAmt));
					transaction.setAttribute("instrument_amount", String.valueOf(newTotalPOAmt));
					// DK IR-RAUM051653353 Rel8.1.1 10/16/2012 Begin
					terms.setAttribute("amount", String.valueOf(newTotalPOAmt));
					if (instrument != null) {
						instrument.setAttribute("copy_of_instrument_amount", String.valueOf(newTotalPOAmt));
					}
				}
			}
			// DK IR-RAUM051653353 Rel8.1.1 10/16/2012 End
			if (poCurrency == null) {
				poCurrency = resultSet.getAttribute("/CURRENCY");
			}

			// Format the amount according to the currency.
			// Update currency of the issuance if the PO currency is valid
			// Amendment does not derive the currency from POs.
			if (StringFunction.isNotBlank(poCurrency)
					&& transaction.getAttribute("transaction_type_code").equals(TransactionType.ISSUE)
					&& ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", poCurrency)) {

				terms.setAttribute("amount_currency_code", poCurrency);
				transaction.setAttribute("copy_of_currency_code", poCurrency);
			}
		} else {
			if (instrument != null) {
				instrument.setAttribute("copy_of_instrument_amount", "");
			}
			transaction.setAttribute("copy_of_amount", "");
			terms.setAttribute("amount", "");
			terms.setAttribute("amount_currency_code", poCurrency);
			transaction.setAttribute("copy_of_currency_code", poCurrency);
		}

		transactionCurrency = transaction.getAttribute("copy_of_currency_code");
		if (StringFunction.isBlank(transactionCurrency)) {
			transactionCurrency = transactionCurrencyCode;
		}
		if (StringFunction.isBlank(transactionCurrency)) {
			numberOfDecimalPlaces = 2;
		} else {
			numberOfDecimalPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, transactionCurrency));
		}
		// Derive the invoice details description.
		String invDescr = "";
		if ((newInvOidList != null) && (newInvOidList.size() > 0)) {
			invDescr = buildInvoiceDetailDescription(newInvOidList, userLocale, numberOfDecimalPlaces, terms.getAttribute(terms.getIDAttributeName()), transaction);
		}
		terms.setAttribute("invoice_details", invDescr);
	}

	// DK IR-RAUM051653353 Rel8.1.1 10/16/2012
	private static BigDecimal getMatchAmount(Transaction transaction, String uploadedPOsInShipmentSql, Instrument instrument, String newListOfPoNumsAddedSql, String invType) throws AmsException, RemoteException {
		double oldTotalPOAmt = 0.00;
		if (uploadedPOsInShipmentSql != null) {
			String sb = "SELECT sum(amount) as AMOUNT FROM (SELECT invoice_id,amount,a_transaction_oid,rank() "
					+ "over ( partition by invoice_id order by a_transaction_oid desc,RowNum) rank  FROM invoices_summary_data,transaction "
					+ "where a_instrument_oid=? and " + newListOfPoNumsAddedSql
					+ "   and a_transaction_oid =  transaction_oid and a_transaction_oid <> ?) WHERE rank <= 1";
			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb, false, transaction.getAttribute("instrument_oid"), transaction.getAttribute("transaction_oid"));
			if (resultSet != null) {
				resultSet = resultSet.getFragment("/ResultSetRecord");
				oldTotalPOAmt = resultSet != null && StringFunction.isNotBlank(resultSet.getAttribute("/AMOUNT")) ? resultSet.getAttributeDecimal("/AMOUNT").doubleValue() : 0;
			}
		}
		return new BigDecimal(oldTotalPOAmt);
	}

	/*
	 * get the detailed description of invoices attached to the transaction
	 */
	public static String buildInvoiceDetailDescription(Vector invOids, String userLocale, int numberOfDecimalPlaces, String termsOid, Transaction transaction) throws RemoteException, AmsException {

		Vector invList = getInvs(invOids);
		invList.addAll(getExistingINVOIds(transaction.getAttribute("transaction_oid"), invOids));
		Vector allInvOids = new Vector();
		allInvOids.addAll(invOids);
		allInvOids.addAll(getExistingInvOids(transaction.getAttribute("transaction_oid"), invOids));

		if (invOids.size() < 1)
			return null;

		StringBuilder invDesc = GoodsDescriptionUtility.buildInvoiceDataLine(invList, termsOid, allInvOids, numberOfDecimalPlaces, userLocale);
		return invDesc.toString();
	}

	private static Collection getExistingInvOids(String transactionOID, Vector invOids) throws AmsException {
		String sb = "SELECT UPLOAD_INVOICE_OID from invoices_summary_data WHERE A_TRANSACTION_OID = ? AND "
				+ POLineItemUtility.getInvoiceItemOidsSql(invOids, "UPLOAD_INVOICE_OID").replace("in", "not in") + " "
				+ INV_LINE_ITEM_ORDERBY;
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb, false, new Object[] { transactionOID });
		if (resultSet == null) {
			return new Vector();
		}
		List<DocumentHandler> resultSetVec = resultSet.getFragmentsList("/ResultSetRecord");
		Vector tempOids = new Vector();
		for (DocumentHandler resultDoc : resultSetVec) {
			tempOids.add(resultDoc.getAttribute("UPLAOD_INVOICE_OID"));
		}
		return tempOids;
	}

	private static Collection getExistingINVOIds(String transactionOID, Vector invOids) throws AmsException {
		String sb = INV_LINE_ITEM_SELECT + " WHERE A_TRANSACTION_OID = ? AND "
				+ POLineItemUtility.getInvoiceItemOidsSql(invOids, "UPLOAD_INVOICE_OID").replace("in", "not in") + " "
				+ INV_LINE_ITEM_ORDERBY;
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb, false, new Object[] { transactionOID });
		if (resultSet == null) {
			return new Vector();
		}
		return resultSet.getFragments("/ResultSetRecord");
	}

	private static Vector getInvs(Vector invOids) throws AmsException {
		String invLineItemSql = INV_LINE_ITEM_SELECT + "where "
				+ POLineItemUtility.getPOLineItemOidsSql(invOids, "upload_invoice_oid") + INV_LINE_ITEM_ORDERBY;
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invLineItemSql, false, new ArrayList<Object>());
		return resultSet.getFragments("/ResultSetRecord");

	}

	// SHR CR708 Rel8.1.1 End

	// SHR IR 6564 start
	public static void validatePaymentDate(Date paymentDate, Date dueDate, ATPInvoiceRules rules, MediatorServices mediatorServices) throws AmsException, RemoteException {
		if (rules != null) {
			int daysAfter = rules.getDaysAfter();
			int daysBefore = rules.getDaysBefore();
			String usePayamentDate = rules.getPaymentAllow();
			if (TradePortalConstants.INDICATOR_YES.equals(usePayamentDate)) {
				int dueDatePaymentDateDaysDiff = calculateDaysDiff(dueDate, paymentDate);
				int daysAllowed;
				if (dueDatePaymentDateDaysDiff < 0) {
					// Payment Date is after Due Date
					dueDatePaymentDateDaysDiff *= -1;
					daysAllowed = daysAfter;
				} else {
					// Payment Date is before Due Date
					daysAllowed = daysBefore;
				}
				if (dueDatePaymentDateDaysDiff > daysAllowed) {
					IssuedError is = new IssuedError();
					is.setErrorCode(TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL);
					throw new AmsException(is);
				}
			}
		}
	}

	public static int calculateDaysDiff(Date date1, Date date2) throws AmsException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		long dateInMillis1 = cal.getTimeInMillis();
		cal.setTime(date2);
		long dateInMillis2 = cal.getTimeInMillis();
		double daysDiff = (double) (dateInMillis1 - dateInMillis2) / (24 * 60 * 60 * 1000);
		// use ceil to make full day if it is coming 20.24, then it should be 21 days.
		return ((int) Math.ceil(daysDiff));
	}

	public static class ATPInvoiceRules {
		int daysBefore = 0;
		int daysAfter = 0;
		String paymentAllow = null;

		public int getDaysBefore() {
			return daysBefore;
		}

		public void setDaysBefore(int daysBefore) {
			this.daysBefore = daysBefore;
		}

		public int getDaysAfter() {
			return daysAfter;
		}

		public void setDaysAfter(int daysAfter) {
			this.daysAfter = daysAfter;
		}

		public String getPaymentAllow() {
			return paymentAllow;
		}

		public void setPaymentAllow(String paymentAllow) {
			this.paymentAllow = paymentAllow;
		}
	}

	/**
	 * This method returns the ATP Invoices rules object to determine the whether user have already been allowed to perform 'Apply
	 * payment date'
	 * 
	 * @param corpOrgOid
	 * @param mediatorServices
	 * @return ATPInvoiceRules
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static ATPInvoiceRules getATPInvoiceRules(CorporateOrganizationWebBean corpOrg) throws AmsException, RemoteException {
		ATPInvoiceRules rulesObj = null;
		if (corpOrg != null) {
			rulesObj = new ATPInvoiceRules();
			String paymentAllow = corpOrg.getAttributeWithoutHtmlChars("payment_day_allow");
			String daysBefore = corpOrg.getAttributeWithoutHtmlChars("days_before");
			String daysAfter = corpOrg.getAttributeWithoutHtmlChars("days_after");
			if (StringFunction.isBlank(daysAfter))
				daysAfter = "0";

			if (StringFunction.isBlank(daysBefore))
				daysBefore = "0";

			rulesObj.setPaymentAllow(paymentAllow);
			rulesObj.setDaysAfter(Integer.parseInt(daysAfter));
			rulesObj.setDaysBefore(Integer.parseInt(daysBefore));
		}
		return rulesObj;
	}

	// SHR IR 6564 end

	// SHR IR T36000005804 Start
	public static void createPurchaseOrderHisotryLog(PreparedStatement insStmt, String action, String status, String poOID, String userOid) throws RemoteException, AmsException, SQLException {
		Date date = new Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);

		insStmt.setLong(1, oid.generateObjectID());
		insStmt.setTimestamp(2, timeStamp);
		insStmt.setString(3, action);
		insStmt.setString(4, status);
		insStmt.setString(5, poOID);
		insStmt.setString(6, userOid);
		insStmt.addBatch();
		insStmt.clearParameters();
	}

	// SHR IR T36000005804 End

	public static String buildInvoiceDetailDescriptionForATP(String termsOid) throws RemoteException, AmsException {
		String invLineItemSql = INV_LINE_ITEM_SELECT + "where a_terms_oid = ?";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invLineItemSql, false, new Object[] { termsOid });
		StringBuilder invDesc = new StringBuilder();

		if (resultSet != null) {
			Vector invList = resultSet.getFragments("/ResultSetRecord");
			if (invList.size() < 1)
				return "";

			invDesc = GoodsDescriptionUtility.buildInvoiceDataLine(invList, termsOid, new Vector(), 0, "en_US");
		}
		return invDesc.toString();
	}

	/**
	 * This method builds a vector containing only PO line item oids (as String objects). This method is used when multiple PO line
	 * item oids are used in multiple places; instead of constantly retrieving the oids from xml docs, this vector can be
	 * constructed once and used afterwards wherever necessary. This method goes through a vector of PO line item xml docs,
	 * retrieves each PO line item oid, and adds it to the vector.
	 * 
	 * 
	 * @param poLineItemsList
	 *            - list of xml docs containing PO line item oid data
	 * @param multiplePOLineItems
	 *            - Is PO line item grouping turned on (YES/NO)
	 * @param addRemoveFlag
	 *            - Adding or Removing POs (ADD/REMOVE). Determines SQL to execute.
	 * @return java.util.Vector - a vector containing all PO line item oids
	 */
	public static Vector getInvoiceOids(Vector invoicesList, String multipleInvoices, String addRemoveFlag) {
		DocumentHandler invoicesDoc = null;
		Vector invoicesOidsList = new Vector();
		int numberOfItems = invoicesList.size();
		for (int i = 0; i < numberOfItems; i++) {
			invoicesDoc = (DocumentHandler) invoicesList.elementAt(i);
			invoicesOidsList.addElement(invoicesDoc.getAttribute("/invoiceOid"));

		}
		return invoicesOidsList;
	}

	// NAR CR 709 Rel 8.2 BEGIN
	/**
	 * Provides the margin rule that most closely matches the supplied instrument type, currency, and amount. All parameters are
	 * required and must be non-blank. attempts to find the margin rule that most closely matching customer margin rule.
	 * 
	 * Margin rules can have blank instrument type, currency, and/or amount values. The following criteria should be used to
	 * determine which rule takes precedence when comparing rules that have blank values:
	 * 
	 * 1) When choosing between two margin rules, one with Instrument Type and the other without, the one with Instrument Type
	 * ALWAYS takes precedence. 2) When choosing between two margin rules, one with Currency and the other without, the one with
	 * Currency ALWAYS takes precedence, EXCEPT when rule (1) applies. 3) When choosing between two margin rules, one with Threshold
	 * Amount (even 0) and the other without, the one with Threshold Amount ALWAYS takes precedence, EXCEPT when rule (1) applies.
	 * 
	 * @param instrumentType
	 * @param currency
	 * @param amount
	 * @param corpOrgOid
	 * @return null, if no matches are found in customer margin rules; otherwise, an XML document of the form
	 *         {@code
	 * <ResultSetRecord>
	 *   <RATE_TYPE>PRM</RATE_TYPE>
	 *   <MARGIN>0.8</MARGIN>
	 * </ResultSetRecord>
	     * }
	 */

	public static DocumentHandler getMarginAndRateType(String instrumentType, String currency, String amount, String corpOrgOid) throws AmsException {
		final String SELECT = "SELECT RATE_TYPE, MARGIN FROM ";
		String sqlQuery = SELECT + "(" + SELECT + "customer_margin_rule WHERE p_owner_oid = ? "
				+ " AND (instrument_type IS NULL OR instrument_type = ?) " + " AND (currency IS NULL OR currency = ?) "
				+ " AND (threshold_amt IS NULL OR threshold_amt >= ?) "
				+ " ORDER BY instrument_type, currency, ABS(threshold_amt - ?)) WHERE rownum = 1";
		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, corpOrgOid, instrumentType, currency, amount, amount);

		if (resultsDoc != null) {
			resultsDoc = resultsDoc.getFragment("/ResultSetRecord");
		}
		return resultsDoc;
	}

	public static void insertToInvErrorLog(String invoiceID, String invUploadOID, String errMsg, String displayable) {
		try (Connection con = DatabaseQueryBean.connect(false); PreparedStatement pStmt = con.prepareStatement("insert into INVOICE_UPLOAD_ERROR_LOG (INVOICE_UPLOAD_ERROR_LOG_OID,"
				+ " INVOICE_ID, error_msg, TRADING_PARTNER_NAME, P_INVOICE_FILE_UPLOAD_UOID, displayable)values(?,?,?,?,?,?)")) {

			con.setAutoCommit(false);

			String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(false));
			pStmt.setString(1, oid);
			String[] temp = invoiceID.split("-");
			pStmt.setString(2, temp[0]);
			pStmt.setString(3, errMsg);
			pStmt.setString(4, (temp[1] == null) ? null : (temp[1]).trim());
			pStmt.setString(5, invUploadOID);
			pStmt.setString(6, displayable);
			pStmt.execute();
			con.commit();

		} catch (AmsException e) {
			logError("Error occured during saving INV Upload Error log ...", e, "", "", "", invUploadOID);
		} catch (SQLException e) {
			logError("Error occured during saving INV Upload Error log ...", e, "", "", "", invUploadOID);
		}
	}

	/**
	 * This method Logs error
	 * 
	 */
	public static void logError(String errorText, Exception e, String loggerCategory, String process_id, String fileName, String invoiceFileOid) {
		String str = " : UploadFilename: " + fileName + "  :: InvoiceUpload_Oid: " + invoiceFileOid + " ";
		Logger.getLogger().log(loggerCategory, process_id, process_id + ": ERROR: " + errorText + ": " + e.getMessage() + str, Logger.ERROR_LOG_SEVERITY);
	}

	public static Vector getLoanTypesNotAllowed(String corpOrgOid) {
		Vector resultVector = new Vector();
		final String SELECT = "SELECT trade_loan_type, import_loan_type, export_loan_type, invoice_financing_type  FROM CORPORATE_ORG WHERE ORGANIZATION_OID in (?)";
		try {
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(SELECT, false, new Object[] { corpOrgOid });
			if (resultsDoc != null) {
				Vector invList = resultsDoc.getFragments("/ResultSetRecord");
				if (invList != null) {
					DocumentHandler doc = (DocumentHandler) invList.get(0);
					String tradeLoan = doc.getAttribute("/TRADE_LOAN_TYPE");
					String impportLoan = doc.getAttribute("/IMPORT_LOAN_TYPE");
					String exportLoan = doc.getAttribute("/EXPORT_LOAN_TYPE");
					String invoiceFinancingLoan = doc.getAttribute("/INVOICE_FINANCING_TYPE");
					if (TradePortalConstants.INDICATOR_NO.equals(tradeLoan)) {
						resultVector.add(TradePortalConstants.TRADE_LOAN);
					}
					if (TradePortalConstants.INDICATOR_NO.equals(impportLoan)) {
						resultVector.add(TradePortalConstants.IMPORT);
					}
					if (TradePortalConstants.INDICATOR_NO.equals(exportLoan)) {
						resultVector.add(TradePortalConstants.EXPORT_FIN);
					}
					if (TradePortalConstants.INDICATOR_NO.equals(invoiceFinancingLoan)) {
						resultVector.add(TradePortalConstants.INV_FIN_REC);
						resultVector.add(TradePortalConstants.INV_FIN_BR);
						resultVector.add(TradePortalConstants.INV_FIN_BB);
					}
				}
			}

		} catch (AmsException e) {
			LOG.error("AmsException while trying to retrieve loan types: ", e);
		}
		return resultVector;
	}

	public static boolean isPaymentDateNull(String invoiceGroupOid) throws RemoteException, AmsException {
		String cnt = null;
		String paymentQry = "select count(payment_date) PAY_COUNT from invoices_summary_data where "
				+ "a_invoice_group_oid in(?) and payment_date is not null";
		DocumentHandler payListDoc = DatabaseQueryBean.getXmlResultSet(paymentQry, true, new Object[] { invoiceGroupOid });
		Vector<DocumentHandler> payRec = (payListDoc != null) ? payListDoc.getFragments("/ResultSetRecord") : null;

		if (payRec != null && payRec.size() > 0) {
			cnt = payRec.get(0).getAttribute("PAY_COUNT");
		}

		boolean payDateNull = false;
		if (StringFunction.isNotBlank(cnt) && Integer.parseInt(cnt) > 0)
			payDateNull = false;
		else
			payDateNull = true;
		return payDateNull;
	}

	public static String getLoanType(String input, String financeType, String buyerBacked) {
		String loanType = "";
		if (StringFunction.isNotBlank(input)) {
			if (TradePortalConstants.INDICATOR_T.equals(input)) {
				loanType = TradePortalConstants.TRADE_LOAN;
			} else if (TradePortalConstants.INDICATOR_X.equals(input)) {
				if (TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)) {
					loanType = TradePortalConstants.INV_FIN_REC;
				} else if (TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType)
						&& TradePortalConstants.INDICATOR_NO.equals(buyerBacked)) {
					loanType = TradePortalConstants.INV_FIN_BR;
				} else if (TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(financeType)
						&& TradePortalConstants.INDICATOR_YES.equals(buyerBacked)) {
					loanType = TradePortalConstants.INV_FIN_BB;
				}
			} else if (TradePortalConstants.INDICATOR_NO.equals(input)) {
				loanType = TradePortalConstants.EXPORT_FIN;
			}
		}
		return loanType;
	}

	public static void resetInvoiceGroupForAssignedInvoices(String invOidsSQL) throws AmsException {
		String updateSQL = "UPDATE invoices_summary_data SET a_invoice_group_oid = NULL WHERE UPLOAD_INVOICE_OID in (" + invOidsSQL;
		try {
			DatabaseQueryBean.executeUpdate(updateSQL, false, new ArrayList<Object>());
		} catch (SQLException e) {
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERROR_GROUPING_INVS);
			throw new AmsException(is);
		}
	}

	public static Vector getDistinctLoanTypes(String loanSQL, String invoiceOids, String corpOrgOid) throws AmsException {
		Vector<String> loanTypesList = new Vector<String>();
		StringBuilder loanTypeSQL = new StringBuilder(loanSQL);
		loanTypeSQL.append(invoiceOids);
		loanTypeSQL.append(") and a_corp_org_oid = ? ");
		DocumentHandler loanResDoc = DatabaseQueryBean.getXmlResultSet(loanTypeSQL.toString(), false, new Object[] { corpOrgOid });
		Vector resultsVector = loanResDoc.getFragments("/ResultSetRecord/");
		DocumentHandler loanTypeDoc = null;
		String loanType = "";
		for (int i = 0; i < resultsVector.size(); i++) {
			loanTypeDoc = (DocumentHandler) resultsVector.elementAt(i);
			loanType = loanTypeDoc.getAttribute("/LOANTYPE");
			loanTypesList.add(loanType);
		}
		LOG.debug("getDistinctLoanTypes() LoanType: {}", loanTypesList);
		return loanTypesList;
	}

	public static Map getFinanceType(String loanType, String invoiceClassification) {
		Map returnMap = new HashMap();
		String financeType = "";
		String importIndicator = "";
		String buyerBacked = "";
		if (StringFunction.isNotBlank(loanType)) {
			if (TradePortalConstants.TRADE_LOAN.equals(loanType)) {
				importIndicator = TradePortalConstants.INDICATOR_T;
				if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceClassification)) {
					financeType = TradePortalConstants.TRADE_LOAN_REC;
				} else if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceClassification)) {
					financeType = TradePortalConstants.TRADE_LOAN_PAY;
				}
			} else if (TradePortalConstants.EXPORT_FIN.equals(loanType)) {
				importIndicator = TradePortalConstants.INDICATOR_NO;
			} else if (TradePortalConstants.INV_FIN_REC.equals(loanType)) {
				importIndicator = TradePortalConstants.INDICATOR_X;
				financeType = TradePortalConstants.SELLER_REQUESTED_FINANCING;
			} else if (TradePortalConstants.INV_FIN_BB.equals(loanType)) {
				importIndicator = TradePortalConstants.INDICATOR_X;
				financeType = TradePortalConstants.BUYER_REQUESTED_FINANCING;
				buyerBacked = TradePortalConstants.INDICATOR_YES;
			} else if (TradePortalConstants.INV_FIN_BR.equals(loanType)) {
				importIndicator = TradePortalConstants.INDICATOR_X;
				financeType = TradePortalConstants.BUYER_REQUESTED_FINANCING;
			}
		}
		returnMap.put("importIndicator", importIndicator);
		returnMap.put("financeType", financeType);
		returnMap.put("buyerBacked", buyerBacked);
		return returnMap;
	}

	/**
	 * This method attempts to convert the given fromAmount (which is in teh given currency which also happens to be the base
	 * currency) into the currency of the po line item. If the fromCurrency and toCurrency are the same, there is no conversion to
	 * do. Otherwise, we look at the currency of the po line item. If it is a valid TradePortal currency (i.e., in refdata manager),
	 * we try to get an fx rate using the toCurrency (the po line item currency). If found, use that information. Otherwise get the
	 * base currency of the template associated with the instrument creation rule. If we can't find an fx rate for that currency we
	 * can't convert and return a null amount. Since the conversion we are doing is FROM a base currency TO another currency (the
	 * opposite of the way the fx rate information is defined, our calculation is opposite of the fx rate info (e.g., we divide if
	 * the multiply_indicator is multiply).
	 * <p>
	 * 
	 * @return java.math.BigDecimal - the resulting converted amount, null if can't convert.
	 * @param fromAmount
	 *            java.math.BigDecimal - The amount to convert from
	 * @param fromCurrency
	 *            java.lang.String - The currency the fromAmount is in
	 * @param toCurrency
	 *            java.lang.String - The currency we want to convert to
	 * @param creationRuleDoc
	 *            DocumentHandler - The instrument creation rule record (as a doc)
	 */
	private static BigDecimal getConvertedAmount(BigDecimal fromAmount, String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc) {
		BigDecimal convertedAmount = null;

		try {
			// BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN - add null check and move before currency check
			// fromAmount may have been defaulted to 0 (to prevent a null
			// pointer). However, we don't want to set the maxAmount value
			// to 0 since nothing would get grouped. If the fromAmount is
			// 0, return a null (indicating maxAmount not relevant for
			// grouping).
			if ((fromAmount == null) || fromAmount.compareTo(BigDecimal.ZERO) == 0) {
				return null;
			}

			// No need to convert if the from and to currencies are the same
			if (fromCurrency.equals(toCurrency)) {
				return fromAmount;
			}

			DocumentHandler fxRateDoc = null;
			ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

			// Test whether the po line item currency code is a valid Trade
			// Portal currency.
			if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency, creationRuleDoc.getAttribute("/USER_LOCALE"))) {
				// Get the fx rate info for the po line item currency
				fxRateDoc = getFxRateDoc(creationRuleDoc.getAttribute("/CORPORATE_ORG_OID"), toCurrency);
				if (fxRateDoc == null) {
					// No fx rate found, isse warning and return a blank amount.
					// This indicates that maxAmount is not used for grouping.
					// Give error with the po line item currency.
					return null;
				} else {
					// Found an fx rate for the template currency. Use it.
				}
			} else {
				// Currency code for the po line item is not valid. Use
				// the currency code from the instrument creation rule's template.
				String sql = "select copy_of_currency_code from template, transaction where p_instrument_oid = c_template_instr_oid and template_oid = ?";
				DocumentHandler templateDoc = null;
				try {
					templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] { creationRuleDoc.getAttribute("/A_TEMPLATE_OID") });
				} catch (AmsException e) {
					templateDoc = null;
				}

				if (templateDoc == null) {
					// Can't get the currency code off the template. Issue a warning and return
					// a null amount. This indicates NOT to use maxAmount for grouping.

					return null;
				} else {
					// We got the template currency but it may be blank. We're only
					// concerned with the first record (there should not be more).
					String templateCurrency = templateDoc.getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
					if (StringFunction.isBlank(templateCurrency)) {
						// Can't get the currency code off the template. Issue a warning and return
						// a null amount. This indicates NOT to use maxAmount for grouping.

						return null;
					} else {
						// Currency is good so attempt to get the fx rate doc.
						fxRateDoc = getFxRateDoc(creationRuleDoc.getAttribute("/CORPORATE_ORG_OID"), templateCurrency);

						// Don't look for an FX rate if the template's currency is the base currency
						// No FX rate is needed in that case
						if (!templateCurrency.equals(creationRuleDoc.getAttribute("/BASE_CURRENCY_CODE"))) {
							if (fxRateDoc == null) {
								// No fx rate found, isse warning and return a blank amount.
								// This indicates that maxAmount is not used for grouping.
								// Give error using the template currency.

								return null;
							} else {
								// Found an fx rate for the template currency. Use it.
							}
						} else {
							// Base Currency and Template currency are identical
							// No need for conversion. Return the from amount
							return fromAmount;
						}
					} // end templateCurrency == null
				} // end templateDoc == null
			} // end checkCode

			// We now have an fx rate doc (although the values on the doc may be
			// blank.) Attempt to convert the amount.
			String multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
			BigDecimal rate = fxRateDoc.getAttributeDecimal("/RATE");

			if (multiplyIndicator != null && rate != null) {
				// We're converting FROM the base currency TO the po line item
				// currency so we need to do the opposite of the multiplyIndicator
				// (the indicator is set for converting FROM some currency TO the
				// base currency)
				if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
					convertedAmount = fromAmount.divide(rate, BigDecimal.ROUND_HALF_UP);
				} else {
					convertedAmount = fromAmount.multiply(rate);
				}
			} else {
				// We have bad data in the fx rate record. Issue warning and return
				// null amount (indicating that maxAmount is not used for grouping)

				return null;
			}
		} catch (AmsException e) {
			LOG.error("Exception found getting converted max amount: ", e);

		}
		return convertedAmount;
	}

	/**
	 * Uses the given corporate org oid and currency to get an fx rate record (as a document handler). The currency, rate, and
	 * multiple_indicator are returned in the document.
	 * 
	 * @return DocumentHandler - result of the query, may be null
	 * @param corporateOrg
	 *            java.lang.String - corporate org oid to use
	 * @param currency
	 *            java.lang.String - currency to use
	 */
	private static DocumentHandler getFxRateDoc(String corporateOrg, String currency) {
		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);

		if (resultSet != null) {
			Vector fxRateDocs = resultSet.getFragments("/ResultSetRecord");
			return (DocumentHandler) fxRateDocs.elementAt(0);
		}
		return null;
	}

	public static boolean validateSameCurrency(String invoiceOidsSql) throws AmsException {
		StringBuilder sameCurrencySQL = new StringBuilder("select count(distinct(currency)) currencyCount from invoices_summary_data where ");
		sameCurrencySQL.append(invoiceOidsSql);
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameCurrencySQL.toString(), false, new ArrayList<Object>());
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/CURRENCYCOUNT");

		return (currCount == 1);
	}

	public static boolean validateSameTradingPartner(String invoiceOidsSql, boolean useSeller) throws AmsException {
		String name = "buyer_name";
		if (useSeller) {
			name = "seller_name";
		}
		String sameTradingPartnerSQL = "select count(distinct(" + name + ")) tradingpartnerCount from invoices_summary_data where "
				+ invoiceOidsSql;
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameTradingPartnerSQL, false, new Object[] {});
		int currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/TRADINGPARTNERCOUNT");

		return (currCount == 1);
	}

	public static boolean validateSameDate(String invoiceOidsSql) throws AmsException {
		StringBuilder sameDateSQL = new StringBuilder("select count((case when payment_date is null then 'A' end )) nullPayDateCount,  count(distinct(payment_date )) paymentDateCount from invoices_summary_data where  ");
		sameDateSQL.append(invoiceOidsSql);
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sameDateSQL.toString(), false, new ArrayList<Object>());
		int nullPayDateCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/NULLPAYDATECOUNT");
		int payDateCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/PAYMENTDATECOUNT");
		boolean isSameDate = false;

		if (nullPayDateCount == 0 && payDateCount == 1) {
			isSameDate = true;
		} else {
			sameDateSQL = new StringBuilder("select count(distinct(due_date )) dueDateCount from invoices_summary_data where  ");
			sameDateSQL.append(invoiceOidsSql);
			resultDoc = DatabaseQueryBean.getXmlResultSet(sameDateSQL.toString(), false, new ArrayList<Object>());
			int dueDateCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/DUEDATECOUNT");
			isSameDate = (dueDateCount == 1);
		}
		return (isSameDate);
	}

	public static int isSingleBeneficairyInvoice(String invoiceOidsSQL) throws AmsException {
		StringBuilder singleBenSQL = new StringBuilder("select count(*) beneCount from (SELECT DISTINCT seller_name,  seller_id,  ");
		singleBenSQL.append("currency,  CASE    WHEN payment_date IS NULL    THEN due_date    ELSE payment_date  END as INVDATE,  ");
		singleBenSQL.append("pay_method,  ben_acct_num,  ben_name,  ben_address_one,  ben_address_two,  ben_address_three,  ben_country, ");
		singleBenSQL.append("ben_bank_name,  ben_branch_code,  ben_branch_address1,  ben_branch_address2,  ben_bank_city,  ben_bank_prvnce,  ");
		singleBenSQL.append("ben_bank_prvnce,  ben_branch_country,  payment_charges, central_bank_rep1,  central_bank_rep2,  central_bank_rep3, ben_bank_sort_code,reporting_code_1, reporting_code_2 ");
		singleBenSQL.append("FROM invoices_summary_data where ");
		singleBenSQL.append(invoiceOidsSQL);
		singleBenSQL.append(" ) ");
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(singleBenSQL.toString(), false, new ArrayList<Object>());

		return resultDoc.getAttributeInt("/ResultSetRecord(0)/BENECOUNT");
	}

	public static int isSinglePaymentCharge(String invoiceOidsSQL) throws AmsException {
		StringBuilder singleBenSQL = new StringBuilder("select count(*) chargeCount from (SELECT DISTINCT  payment_charges ");
		singleBenSQL.append("FROM invoices_summary_data where ");
		singleBenSQL.append(invoiceOidsSQL);
		singleBenSQL.append(" ) ");
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(singleBenSQL.toString(), false, new ArrayList<Object>());

		return resultDoc.getAttributeInt("/ResultSetRecord(0)/CHARGECOUNT");
	}

	// Srinivasu_D IR#T36000018276 Rel8.2 06/16/2013
	public static Vector getLoanTypesFromRefData() throws AmsException {
		Vector<String> loanTypesList = new Vector<String>();
		String loanTypeSQL = "select distinct code  loantype from refdata where table_type='LOAN_TYPE'";
		DocumentHandler loanResDoc = DatabaseQueryBean.getXmlResultSet(loanTypeSQL, false, new ArrayList<Object>());
		Vector resultsVector = loanResDoc.getFragments("/ResultSetRecord/");
		DocumentHandler loanTypeDoc = null;
		String loanType = "";
		for (int i = 0; i < resultsVector.size(); i++) {
			loanTypeDoc = (DocumentHandler) resultsVector.elementAt(i);
			loanType = loanTypeDoc.getAttribute("/LOANTYPE");
			loanTypesList.add(loanType);

		}
		LOG.debug("getLoanTypesFromRefData() LoanTYpe: {}", loanTypesList);

		return loanTypesList;
	}

	public static String fetchOpOrgCountry(String termsOid) {
		String trmsOid = StringFunction.isNotBlank(termsOid) ? termsOid : "0";
		DocumentHandler result = null;
		try {
			String sqlQuery = "SELECT  OPERATIONAL_BANK_ORG.ADDRESS_COUNTRY AS COUNTRY "
					+ "FROM OPERATIONAL_BANK_ORG, INSTRUMENT, TRANSACTION, TERMS " + "WHERE TERMS_OID = C_CUST_ENTER_TERMS_OID "
					+ "AND TRANSACTION_OID = ORIGINAL_TRANSACTION_OID "
					+ "AND A_OP_BANK_ORG_OID = ORGANIZATION_OID AND TERMS_OID = ?";
			result = (DocumentHandler) DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[] { trmsOid });
		} catch (AmsException e) {
			LOG.error("Ams Exception while trying to retrieve fetch OpOrg Country: ", e);
		}
		String country = "";
		if (result != null) {
			country = result.getAttribute("/ResultSetRecord/COUNTRY");
		}
		return country;
	}

	public static boolean validatePaymentMethod(String paymentMethod, String currency, String country, String benCountry, ErrorManager errorManager, boolean fromAutoCreate, String invoiceId) throws AmsException {
		return validatePaymentMethod(paymentMethod, currency, country, benCountry, errorManager, fromAutoCreate, invoiceId, new HashMap());
	}

	public static boolean validatePaymentMethod(String paymentMethod, String currency, String country, String benCountry, ErrorManager errorManager, boolean fromAutoCreate, String invoiceId, Map<String, String> countryMap) throws AmsException {
		Set validPaymentMethods = new HashSet();
		validPaymentMethods.add("ACH");
		validPaymentMethods.add("RTGS");
		validPaymentMethods.add("CBFT");

		List<Object> sqlPrmsLst = new ArrayList();
		sqlPrmsLst.add(paymentMethod);
		String whereClause = " PAYMENT_METHOD = ? AND ";
		if (StringFunction.isNotBlank(country)) {
			whereClause += " COUNTRY = ? AND ";
			sqlPrmsLst.add(country);
		}
		whereClause += " CURRENCY = ?";
		sqlPrmsLst.add(currency);
		String offDays = countryMap.get(paymentMethod + currency + country);
		if (StringFunction.isBlank(offDays)) {

			String selectOffsetdays = "select offset_days from PAYMENT_METHOD_VALIDATION where " + whereClause;
			DocumentHandler paymentMethodRec = DatabaseQueryBean.getXmlResultSet(selectOffsetdays, false, sqlPrmsLst);
			offDays = paymentMethodRec != null ? paymentMethodRec.getAttribute("/ResultSetRecord/OFFSET_DAYS") : "";
			countryMap.put(paymentMethod + currency + country, offDays);
		}
		boolean isValid = true;
		if (StringFunction.isBlank(offDays) && StringFunction.isNotBlank(country)) {
			if (fromAutoCreate) {
				if (!checkIfErrorAlreadyAdded(TradePortalConstants.INFO_INVALID_PAYMENT_METHOD_RULE_COMBINATION, invoiceId, errorManager.getIssuedErrors()))
					errorManager.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_INVALID_PAYMENT_METHOD_RULE_COMBINATION, invoiceId, getDescription(country, "COUNTRY"), getDescription(currency, "CURRENCY_CODE"));
			} else {
				errorManager.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION, getDescription(paymentMethod, "PAYMENT_METHOD"), getDescription(currency, "CURRENCY_CODE"), getDescription(country, "COUNTRY"));
			}
			isValid = false;
		}

		if (!validPaymentMethods.contains(paymentMethod)) {
			errorManager.issueError(TradePortalConstants.ERR_CAT_1, fromAutoCreate ? TradePortalConstants.INFO_INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD : TradePortalConstants.INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD, getDescription(paymentMethod, "PAYMENT_METHOD"));
			isValid = false;
		}
		return isValid;
	}

	private static String getDescription(String code, String tableType) {
		try {
			return ReferenceDataManager.getRefDataMgr().getDescr(tableType, code);
		} catch (AmsException e) {
			LOG.debug("Ams Exception fetching description: ", e);
			return code;
		}
	}

	public static boolean validateBankInformation(String paymentMethod, String bankBranchCode, String country, String bankName, String addressLine1, ErrorManager errorManager, boolean fromAutoCreate, String invoiceId) throws AmsException {
		boolean returnFlag = true;

		if (StringFunction.isNotBlank(bankBranchCode)) {
			List<Object> sqlPrmsLst = new ArrayList<Object>();
			sqlPrmsLst.add(bankBranchCode.toUpperCase());
			String sqlQuery = "select BANK_BRANCH_CODE from BANK_BRANCH_RULE  where upper(BANK_BRANCH_CODE) = ?";
			if (StringFunction.isNotBlank(paymentMethod)) {
				sqlQuery += " and upper(PAYMENT_METHOD) = ? ";
				sqlPrmsLst.add(paymentMethod.toUpperCase());
			}

			if (StringFunction.isNotBlank(country)
					&& (StringFunction.isNotBlank(paymentMethod) && !TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod))) {
				sqlQuery += " and upper(ADDRESS_COUNTRY) = ?";
				sqlPrmsLst.add(country.toUpperCase());
			}

			DocumentHandler result = (DocumentHandler) DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);
			if (result == null) {
				if (fromAutoCreate) {
					if (!checkIfErrorAlreadyAdded(TradePortalConstants.INFO_INVALID_BANK_BRNCH_CODE, invoiceId, errorManager.getIssuedErrorsList()))
						errorManager.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_INVALID_BANK_BRNCH_CODE, invoiceId, getDescription(country, "COUNTRY"), getDescription(paymentMethod, "PAYMENT_METHOD"));
				} else {
					errorManager.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_BANK_BRNCH_CODE, getDescription(paymentMethod, "PAYMENT_METHOD"), getDescription(country, "COUNTRY"));

				}
				returnFlag = false;
			}
		}

		if (StringFunction.isBlank(bankBranchCode) && StringFunction.isBlank(bankName)) {
			errorManager.issueError(TradePortalConstants.ERR_CAT_1, fromAutoCreate ? TradePortalConstants.INFO_BEN_BANK_NAME_BRANCH_REQD : TradePortalConstants.BEN_BANK_NAME_BRANCH_REQD);
			returnFlag = false;
		}
		if (StringFunction.isBlank(bankBranchCode) && StringFunction.isNotBlank(bankName) && StringFunction.isBlank(addressLine1)) {
			errorManager.issueError(TradePortalConstants.ERR_CAT_1, fromAutoCreate ? TradePortalConstants.INFO_BEN_BANK_NAME_ADDR_DATA_REQD : TradePortalConstants.BEN_BANK_NAME_ADDR_DATA_REQD);
			returnFlag = false;
		}

		return returnFlag;
	}

	private static boolean checkIfErrorAlreadyAdded(String errorCode, String invoiceId, List<IssuedError> issuedErrors) {
		for (IssuedError ie : issuedErrors) {
			if (ie != null) {
				String errCode = ie.getErrorCode();
				String errText = ie.getErrorText();
				if (StringFunction.isNotBlank(errCode) && StringFunction.isNotBlank(errText)) {
					if (errorCode.equalsIgnoreCase(errCode) && errText.contains(invoiceId)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static boolean validatePaymentMethodForInvoices(Vector invoiceOids, String country, String currency, ErrorManager errorManager, boolean fromAutoCreate) {
		boolean isValid = true;

		try {
			DocumentHandler resultDoc = fetchInvoiceInfo(invoiceOids);
			if (resultDoc != null) {
				Vector invoiceDoc = resultDoc.getFragments("/ResultSetRecord");
				for (Object object : invoiceDoc) {
					DocumentHandler invDoc = (DocumentHandler) object;
					String paymentMethod = invDoc.getAttribute("/PAY_METHOD");
					String benCountry = invDoc.getAttribute("/BEN_COUNTRY");
					String invoiceId = invDoc.getAttribute("/INVOICE_ID");
					if (StringFunction.isNotBlank(paymentMethod)) {
						isValid = isValid
								& validatePaymentMethod(paymentMethod, currency, country, benCountry, errorManager, fromAutoCreate, invoiceId);
					}
				}
			}

		} catch (AmsException e) {
			LOG.error("AmsException while validating Payment Method: ", e);
		}

		return isValid;
	}

	public static boolean validateBankInformationForInvoices(Vector invoiceOids, String country, String currency, ErrorManager errorManager, boolean fromAutoCreate) {
		boolean isValid = true;

		try {
			DocumentHandler resultDoc = fetchInvoiceInfo(invoiceOids);
			if (resultDoc != null) {
				Vector invoiceDoc = resultDoc.getFragments("/ResultSetRecord");
				for (Object object : invoiceDoc) {
					DocumentHandler invDoc = (DocumentHandler) object;
					String bankBranchCode = invDoc.getAttribute("/BEN_BRANCH_CODE");
					String paymentMethod = invDoc.getAttribute("/PAY_METHOD");
					String bankName = invDoc.getAttribute("/BEN_BANK_NAME");
					String addressLine1 = invDoc.getAttribute("/BEN_BRANCH_ADDRESS1");
					String invoiceId = invDoc.getAttribute("/INVOICE_ID");
					if (StringFunction.isNotBlank(paymentMethod)) {
						isValid = isValid
								& validateBankInformation(paymentMethod, bankBranchCode, country, bankName, addressLine1, errorManager, fromAutoCreate, invoiceId);
					}
				}
			}

		} catch (AmsException e) {
			LOG.error("AmsException while validating Bank Information: ", e);
		}

		return isValid;
	}

	/**
	 * Added for Rel9.4 CR-1001
	 * 
	 * @param invoiceOids
	 * @param country
	 * @param errMgr
	 * @param fromAutoCreate
	 * @return
	 */
	public static Vector validateReportingCodesForInvoices(Vector invoiceOids, String bankGroup, ErrorManager errMgr, boolean isFromAutoCreate) {
		Vector passedInvoiceOids = new Vector();
		String bankGroupOid = fetchBankGroupForOpOrg(bankGroup);
		Map<String, Map<String, String>> map = new HashMap<>();
		try {
			DocumentHandler resultDoc = fetchInvoiceInfo(invoiceOids);
			if (resultDoc != null) {
				List<DocumentHandler> invoiceDoc = resultDoc.getFragmentsList("/ResultSetRecord");
				boolean isValid = true;
				for (DocumentHandler invDoc : invoiceDoc) {
					String invoiceOid = invDoc.getAttribute("/UPLOAD_INVOICE_OID");
					String paymentMethod = invDoc.getAttribute("/PAY_METHOD");
					String invoiceID = invDoc.getAttribute("/INVOICE_ID");
					String[] reportingCodes = new String[2];
					reportingCodes[0] = invDoc.getAttribute("/REPORTING_CODE_1");
					reportingCodes[1] = invDoc.getAttribute("/REPORTING_CODE_2");
					if (StringFunction.isNotBlank(paymentMethod)) {

						isValid = validateReportingCodes(bankGroupOid, map, paymentMethod, reportingCodes, "", "", errMgr, isFromAutoCreate, invoiceID, true);
						if (isValid) {
							passedInvoiceOids.add(invoiceOid);
						}
					}
				}
			}

		} catch (Exception e) {
			LOG.error("Exception while validating Reporting Codes Information: ", e);
		}

		return passedInvoiceOids;
	}

	private static DocumentHandler fetchInvoiceInfo(Vector invoiceOids) throws AmsException {
		StringBuilder sb = new StringBuilder("SELECT UPLOAD_INVOICE_OID, INVOICE_ID, PAY_METHOD, BEN_NAME, BEN_COUNTRY, BEN_BRANCH_CODE, BEN_BANK_NAME, BEN_BRANCH_ADDRESS1, REPORTING_CODE_1, REPORTING_CODE_2 FROM INVOICES_SUMMARY_DATA WHERE ");
		String requiredOid = "upload_invoice_oid";
		String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(invoiceOids, requiredOid);
		sb.append(invoiceOidsSql);
		sb.append(" order by upload_invoice_oid ");

		return DatabaseQueryBean.getXmlResultSet(sb.toString(), false, new ArrayList<Object>());
	}

	public static String fetchOpOrgCountryForOpOrg(String opOrgOid) {

		opOrgOid = StringFunction.isNotBlank(opOrgOid) ? opOrgOid : "0";
		DocumentHandler result = null;
		try {
			String sqlQuery = "SELECT  OPERATIONAL_BANK_ORG.ADDRESS_COUNTRY AS COUNTRY FROM OPERATIONAL_BANK_ORG WHERE ORGANIZATION_OID = ?";
			result = (DocumentHandler) DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[] { opOrgOid });
		} catch (AmsException e) {
			LOG.error("Ams Exception while trying to fetch OpOrg Country For OpOrg: ", e);
		}
		String country = "";
		if (result != null) {
			country = result.getAttribute("/ResultSetRecord/COUNTRY");
		}
		return country;
	}

	// method which takes HashMap containing keys and values and pack them in an array
	public static void packInArray(Map<String, String> attributes, String[] keys, String[] values) {

		HashMap<String, String> attributesMap = (HashMap<String, String>) attributes;
		Iterator<String> ikeys = attributesMap.keySet().iterator();
		int index = 0;

		while (ikeys.hasNext()) {
			String oneKey = ikeys.next();
			keys[index] = oneKey;
			values[index] = attributesMap.get(oneKey);
			index++;
		}
	}

	// Nar CR-913 Rel9.0 20-Feb-2014 ADD - Begin

	/**
	 * This method is used to identify whether Payable Program Invoice Grouping is enable for Corporate customer.
	 * 
	 * @param corpOrg
	 *            Corporate Organization
	 * @return true- If Payable Program Invoice group enable false - If Payable Program Invoice group is disable
	 */
	public static boolean isPayablesPrgmInvoiceGroupEnable(CorporateOrganizationWebBean corpOrg) {
		return TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("pay_invoice_grouping_ind"));
	}

	// Nar CR-913 Rel9.0 20-Feb-2014 ADD - End

	// CR 913 start
	public static boolean isIntegratedPayPrgmEnabled(CorporateOrganizationWebBean corpOrg) {
		return TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("pay_invoice_utilised_ind"));
	}

	// CR 913 end
	public static boolean checkThresholds(String amount, String currency, String baseCurrency, String thresholdAttributeName, String dailyLimitAttributeName, String userOrgOid, String clientBankOid, User user, String selectClause, boolean discountIndicator, String subsAuth, MediatorServices mediatorServices) throws RemoteException, AmsException {
		if (StringFunction.isBlank(baseCurrency)) {
			baseCurrency = "USD";
		}

		String thresholdGroupOid = "";
		if ((StringFunction.isNotBlank(userOrgOid)) && (TradePortalConstants.INDICATOR_NO.equals(subsAuth)))
			thresholdGroupOid = user.getAttribute("subsidiary_threshold_group_oid");

		// use user's default threshold group
		if (StringFunction.isBlank(thresholdGroupOid)) {
			thresholdGroupOid = user.getAttribute("threshold_group_oid");
		}
		// get the threshold and daily limit amounts
		boolean errorsFound = false;
		if (StringFunction.isNotBlank(thresholdGroupOid)) {
			ThresholdGroup thresholdGroup = (ThresholdGroup) mediatorServices.createServerEJB("ThresholdGroup", Long.parseLong(thresholdGroupOid));
			String dailyLimit = thresholdGroup.getAttribute(dailyLimitAttributeName);
			String thresholdAmount = thresholdGroup.getAttribute(thresholdAttributeName);

			boolean checkDailyLimit = false;
			if (StringFunction.isNotBlank(dailyLimit))
				checkDailyLimit = true;

			boolean checkThresholdLimit = false;
			if (StringFunction.isNotBlank(thresholdAmount))
				checkThresholdLimit = true;

			// if any limits need to be checked, convert transaction amount to base
			// currency
			BigDecimal amountInBase = null;
			if (checkDailyLimit || checkThresholdLimit) {
				amountInBase = FXRateBean.getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());

				if (amountInBase.compareTo(BigDecimal.ZERO) < 0) {
					errorsFound = true;
					checkDailyLimit = false;
					checkThresholdLimit = false;
				}
			}

			// if threshold amount check fails, issue an error, set
			// the transaction status to failed and continue to validate for daily limit
			if (checkThresholdLimit) {
				BigDecimal threshold = new BigDecimal(thresholdAmount);
				if (amountInBase.compareTo(threshold) > 0) {
					mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(), TradePortalConstants.TRANSACTION_LIMIT_EXCEEDED, user.getAttribute("first_name"), user.getAttribute("last_name"));
					// transaction status should be set to FAILED
					errorsFound = true;
				}
			}
			// if daily limit check fails, issue an error and stop further validations
			if (checkDailyLimit) {
				BigDecimal limit = new BigDecimal(dailyLimit);
				BigDecimal amountAuthorized = getAmountAuthorized(user.getAttribute("user_oid"), user.getAttribute("timezone"), baseCurrency, userOrgOid, clientBankOid, discountIndicator, selectClause, mediatorServices);

				if (amountAuthorized.compareTo(BigDecimal.ZERO) < 0) {
					errorsFound = true;
				} else {
					BigDecimal sumAuthorized = amountInBase.add(amountAuthorized);
					if (sumAuthorized.compareTo(limit) > 0) {
						mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(), TradePortalConstants.DAILY_LIMIT_EXCEEDED, user.getAttribute("first_name"), user.getAttribute("last_name"));
						// transaction status should be set to fail
						errorsFound = true;
					}
				}
			}
		}
		return !errorsFound;
	}

	public static BigDecimal getAmountAuthorized(String userOid, String timeZone, String baseCurrency, String userOrgOid, String clientBankOid, boolean discountIndicator, String selectClause, MediatorServices mediatorServices) throws AmsException, RemoteException {
		// find out user's date (without the time) and convert the date to GMT
		// date and time to determine the start of the user's day in GMT time.

		Date gmtDateTime = TPDateTimeUtility.getStartOfLocalDayInGMT(timeZone);
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String dateToCompare = formatter.format(gmtDateTime);

		// note: the first authorizing user and second authorizing user
		// cannot be the same, hence there is no danger of selecting a row twice.

		String whereClause = "where ((a_first_authorizing_user_oid = ? " + " and first_authorize_status_date "
				+ ">= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')) or " + "(a_second_authorizing_user_oid = ? "
				+ " and second_authorize_status_date >= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')))";

		if (discountIndicator) {
			whereClause = " and discount_indicator = 'Y'";
		}

		BigDecimal amountAuthorized = BigDecimal.ZERO;
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(selectClause + whereClause, false, Long.parseLong(userOid), dateToCompare, Long.parseLong(userOid), dateToCompare);
		Vector transactionList = null;
		int count = 0;
		if (resultSet == null) {
			count = 0;
		} else {
			transactionList = resultSet.getFragments("/ResultSetRecord");
			count = transactionList.size();
		}
		// get the amounts in base currency of the user
		DocumentHandler transactionData = null;
		BigDecimal amountToAdd = null;
		String transactionAmount = null;
		String transactionCurrency = null;
		for (int i = 0; i < count; i++) {
			transactionData = (DocumentHandler) transactionList.get(i);
			transactionCurrency = transactionData.getAttribute("/CURRENCY_CODE");
			transactionAmount = transactionData.getAttribute("/PAYMENT_AMOUNT");

			if (StringFunction.isNotBlank(transactionAmount)) {
				amountToAdd = FXRateBean.getAmountInBaseCurrency(transactionCurrency, transactionAmount, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());
			} else {
				amountToAdd = BigDecimal.ZERO;
			}
			if (amountToAdd.compareTo(BigDecimal.ZERO) < 0) {
				amountAuthorized = new BigDecimal(-1.0f);
				break;
			} else {
				amountAuthorized = amountAuthorized.add(amountToAdd);
			}
		}
		return amountAuthorized;
	}

	public static void performNotifyPanelAuthUser(String ownerOrgOid, StringBuilder emailContent, StringBuilder emailReceivers, MediatorServices mediatorServices) throws RemoteException, AmsException {

		if (StringFunction.isNotBlank(emailReceivers.toString())) {
			// Retrieve Corporate Org
			CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(ownerOrgOid));
			String bankOrgOid = corpOrg.getAttribute("bank_org_group_oid");

			// Get the sender name, sender email address, system name, and bank
			// url from the corp org's bank org group
			BankOrganizationGroup bankOrg = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup", Long.parseLong(bankOrgOid));

			String senderName = bankOrg.getAttribute("repair_sender_name").trim();
			String senderEmailAddress = bankOrg.getAttribute("repair_sender_email_address");
			String tradeSystemName = bankOrg.getAttribute("repair_system_name").trim();
			String doNotReplyInd = bankOrg.getAttribute("repair_do_not_reply_ind");
			String incldBnkUrlInd = bankOrg.getAttribute("incld_bnk_url_repr_panel");
			String bankUrl = bankOrg.getAttribute("repair_bank_url");
			// Build the subject line and content of the email based on the type
			// of email (transaction, mail message)
			StringBuilder emailSubject = new StringBuilder();
			StringBuilder emailFooter = new StringBuilder();

			emailSubject.append(tradeSystemName
					+ " "
					+ mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelAuthorisationAlert", TradePortalConstants.TEXT_BUNDLE));
			emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
			emailContent.append(TradePortalConstants.NEW_LINE);

			// Build the email's footer
			emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.LogIn", TradePortalConstants.TEXT_BUNDLE)
					+ " ");

			// Use default trade system name if bank org group has not specified one
			if (StringFunction.isBlank(tradeSystemName)) {
				tradeSystemName = mediatorServices.getResourceManager().getText("EmailTrigger.PaymentGroup", TradePortalConstants.TEXT_BUNDLE);
			}

			emailFooter.append(" " + tradeSystemName + " ");
			emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.AuthoriseMessageInvoice", TradePortalConstants.TEXT_BUNDLE)
					+ " ");

			/* PGedupudi CR 1123 Rel 9.5 start */
			if (TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlInd)) {
				emailFooter.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.LogIn", TradePortalConstants.TEXT_BUNDLE))
						+ " ");

				emailFooter.append(tradeSystemName + " ");
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.AdditionalDetails", TradePortalConstants.TEXT_BUNDLE)
						+ " ");

				if (!StringFunction.isBlank(bankUrl)) {
					emailFooter.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.WebsiteAccess", TradePortalConstants.TEXT_BUNDLE))
							+ " ");
					emailFooter.append(bankUrl + ".");
				}

			} else {
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.ForAdditionalDetails", TradePortalConstants.TEXT_BUNDLE)
						+ ", ").append(mediatorServices.getResourceManager().getText("EmailTrigger.PleaseLogIn", TradePortalConstants.TEXT_BUNDLE)
						+ " " + tradeSystemName + " ").append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.ByVisitingBankSite", TradePortalConstants.TEXT_BUNDLE)));
			}
			/* PGedupudi CR 1123 Rel 9.5 end */

			if (TradePortalConstants.INDICATOR_YES.equals(doNotReplyInd)) {
				emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.DoNotReply", TradePortalConstants.TEXT_BUNDLE));
			}

			// Add the footer to the email content
			emailContent.append(emailFooter);

			// Create the format and content of the email
			DocumentHandler emailMessage = new DocumentHandler("<Email></Email>", false, true);
			emailMessage.setAttribute("/To", emailReceivers.toString());
			emailMessage.setAttribute("/Cc", "");
			emailMessage.setAttribute("/Bcc", "");
			emailMessage.setAttribute("/From/SenderAddress", senderEmailAddress);
			emailMessage.setAttribute("/From/SenderName", senderName);
			emailMessage.setAttribute("/Subject", StringFunction.asciiToUnicode(emailSubject.toString()));
			emailMessage.setAttribute("/Content", StringFunction.asciiToUnicode(emailContent.toString()));

			// Create a new email trigger object containing the email content
			// created above
			EmailTrigger emailTrigger = (EmailTrigger) mediatorServices.createServerEJB("EmailTrigger");
			emailTrigger.newObject();
			emailTrigger.setAttribute("agent_id", "");
			emailTrigger.setAttribute("status", TradePortalConstants.EMAIL_TRIGGER_NEW);
			emailTrigger.setAttribute("email_data", emailMessage.toString());
			emailTrigger.save();

			// display success message
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MESSAGE_PROCESSED, new String[] { mediatorServices.getResourceManager().getText("Home.PanelAuthTransactions.Notify.Message", TradePortalConstants.TEXT_BUNDLE), mediatorServices.getResourceManager().getText("Home.PanelAuthTransactions.Notify.Sent", TradePortalConstants.TEXT_BUNDLE) });
		}

	}

	public static void createInvoiceHistory(String invoiceOid, String invStatus, String action, String userOid, String corpOrgOid, MediatorServices mediatorServices) throws NumberFormatException, AmsException, RemoteException {
		createInvoiceHistory(invoiceOid, invStatus, action, userOid, corpOrgOid, mediatorServices, false);
	}

	public static void createInvoiceHistory(String invoiceOid, String invStatus, String action, String userOid, String corpOrgOid, MediatorServices mediatorServices, boolean isPanleAuth) throws NumberFormatException, AmsException, RemoteException {

		if (StringFunction.isBlank(userOid) && StringFunction.isNotBlank(corpOrgOid)) {
			CorporateOrganization corpOrgEJB = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
			userOid = corpOrgEJB.getAttribute("upload_system_user_oid");
			mediatorServices.removeServerEJB(corpOrgEJB);// T36000031888
		}
		InvoiceHistory invHistory = (InvoiceHistory) mediatorServices.createServerEJB("InvoiceHistory");
		invHistory.newObject();
		invHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime(true, false));
		invHistory.setAttribute("action", action);
		invHistory.setAttribute("invoice_status", invStatus);
		invHistory.setAttribute("upload_invoice_oid", invoiceOid);
		invHistory.setAttribute("user_oid", userOid);
		if (isPanleAuth) {
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String userPanelAuthLevel = user.getAttribute("panel_authority_code");
			invHistory.setAttribute("panel_authority_code", userPanelAuthLevel);
		}
		int ret = invHistory.save(false);
		if (ret != 1) {
			LOG.info("Error occured while creating Invoice History Log...");
		}

	}

	public static boolean isPayableInvoiceAuthorizable(String field, String fieldDescription, String invoiceStatus, MediatorServices mediatorServices) throws RemoteException, AmsException {

		boolean isValid = true;
		// only 'Available for Auth', 'Partially Authorized', 'Auth Failed' invoices can be authorized

		if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_RAA.equals(invoiceStatus)
				|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus))) {
			mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(), TradePortalConstants.TRANSACTION_CANNOT_PROCESS, field, fieldDescription, ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()), mediatorServices.getResourceManager().getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
			isValid = false;
		}

		return isValid;
	}

	public static void createOutgoingQueueAndIssueSuccess(String status, String invoiceOid, boolean isCreditNote, MediatorServices mediatorServices) throws AmsException, RemoteException {
		createOutgoingQueueAndIssueSuccess(status, invoiceOid, isCreditNote, mediatorServices, null);
	}

	public static void createOutgoingQueueAndIssueSuccess(String status, String invoiceOid, boolean isCreditNote, MediatorServices mediatorServices, String preProcessParameter) throws AmsException, RemoteException {
		createOutgoingQueueAndIssueSuccess(status, invoiceOid, isCreditNote, mediatorServices, preProcessParameter, null);
	}

	public static void createOutgoingQueueAndIssueSuccess(String status, String invoiceOid, boolean isCreditNote, MediatorServices mediatorServices, String preProcessParameter, String messageID) throws AmsException, RemoteException {

		OutgoingInterfaceQueue outgoingMessage = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
		outgoingMessage.newObject();
		outgoingMessage.setAttribute("date_created", DateTimeUtility.getGMTDateTime());
		outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
		if (StringFunction.isBlank(messageID)) {
			messageID = InstrumentServices.getNewMessageID();
		}
		outgoingMessage.setAttribute("message_id", messageID);

		StringBuilder processParameters = new StringBuilder("invoice_oid=");
		processParameters.append(invoiceOid).append("|");
		processParameters.append("status=").append(status);
		if (StringFunction.isNotBlank(preProcessParameter)) {
			processParameters.append("|").append(preProcessParameter);
		}

		outgoingMessage.setAttribute("process_parameters", processParameters.toString());

		if (isCreditNote) {
			outgoingMessage.setAttribute("msg_type", MessageType.TRIG);
		} else {
			outgoingMessage.setAttribute("msg_type", MessageType.BULKINVUPL);
		}

		if (outgoingMessage.save() != 1) {
			throw new AmsException("Error occurred while inserting invoice message type");
		}
	}

	/**
	 * Searches for an existing invoice group in the database that matches the supplied InvoiceGroup.
	 * 
	 * @return OID of the Invoice Group if a match is found; otherwise null.
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static String findMatchingInvoiceGroup(InvoiceGroup invoiceGroup) throws AmsException, RemoteException {
		return findMatchingInvoiceGroup(invoiceGroup, false);
	}

	public static String findMatchingInvoiceGroup(InvoiceGroup invoiceGroup, boolean isPayPrgm) throws AmsException, RemoteException {
		String paymentDateStr = invoiceGroup.getAttribute("payment_date");
		List<Object> sqlPrmsLst = new ArrayList<Object>();
		StringBuilder oidSql = new StringBuilder("select INVOICE_GROUP_OID");
		oidSql.append(" from invoice_group where ");

		if (StringFunction.isBlank(paymentDateStr)) {
			oidSql.append(" payment_date is null ");
		} else {
			Date date = DateTimeUtility.convertStringDateToDate(paymentDateStr);
			String dateStr = DateTimeUtility.formatDate(date, "dd-MMM-yyyy");
			oidSql.append(" payment_date = ? ");
			sqlPrmsLst.add(dateStr);
		}
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceGroup.getAttribute("linked_to_instrument_type"))) {
			String suppDateStr = invoiceGroup.getAttribute("send_to_supplier_date");
			if (StringFunction.isNotBlank(suppDateStr)) {
				Date suppDate = DateTimeUtility.convertStringDateToDate(suppDateStr);
				suppDateStr = DateTimeUtility.formatDate(suppDate, "dd-MMM-yyyy");
				oidSql.append(" and send_to_supplier_date = ? ");
				sqlPrmsLst.add(suppDateStr);
			} else {
				oidSql.append(" and send_to_supplier_date is null");
			}

		}
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceGroup.getAttribute("linked_to_instrument_type"))
				|| TradePortalConstants.INV_LINKED_INSTR_REC.equals(invoiceGroup.getAttribute("linked_to_instrument_type"))) {
			oidSql.append(" and invoice_status not in (?,?)");
			sqlPrmsLst.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
			sqlPrmsLst.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
		}
		String corpOrgOid = invoiceGroup.getAttribute("corp_org_oid");
		oidSql.append(" and a_corp_org_oid = ? ");
		sqlPrmsLst.add(corpOrgOid);

		String[] attributeColumns = { "trading_partner_name", "currency", "linked_to_instrument_type", "invoice_status", "loan_type", "invoice_classification" };
		String[] payAattributeColumns = { "trading_partner_name", "currency", "linked_to_instrument_type", "invoice_status", "invoice_classification" };
		String ATTR[] = attributeColumns;

		if (isPayPrgm || TradePortalConstants.INV_LINKED_INSTR_PYB.equals(invoiceGroup.getAttribute("linked_to_instrument_type"))) {
			ATTR = payAattributeColumns;
		}

		for (String aATTR : ATTR) {
			String currentValue = invoiceGroup.getAttribute(aATTR);
			oidSql.append(" and ");
			if (aATTR.equals("trading_partner_name")) {
				oidSql.append("upper(?)");
				sqlPrmsLst.add(aATTR);
				currentValue = StringFunction.isNotBlank(currentValue) ? currentValue.toUpperCase() : "";
			} else {
				oidSql.append(" ? ");
				sqlPrmsLst.add(aATTR);
			}

			if (StringFunction.isBlank(currentValue)) {
				oidSql.append(" is null");
			} else {
				oidSql.append(" = ? ");
				sqlPrmsLst.add(currentValue);
			}
		}

		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(oidSql.toString(), false, sqlPrmsLst);
		if (resultsDoc != null) {
			return resultsDoc.getAttribute("/ResultSetRecord/INVOICE_GROUP_OID");
		}

		return null;
	}

	public static int updateAttachmentDetails(String newGroupOid, String oldGroupOid, String invoiceOid, MediatorServices mediatorServices) throws AmsException, RemoteException {
		boolean oldGroupAvailable = false;
		String oldGroupSearchSQL = "select doc_image_oid,p_transaction_oid from document_image where p_transaction_oid in (select upload_invoice_oid from invoices_Summary_data "
				+ "where a_invoice_group_oid = ? and upload_Invoice_oid != ?)";

		DocumentHandler oldGroupDoc = DatabaseQueryBean.getXmlResultSet(oldGroupSearchSQL, false, oldGroupOid, invoiceOid);

		// Searching for image_id in document_image table if any attachments
		// available for exisitng group oid
		if (oldGroupDoc != null) {
			Vector oldGroupList = oldGroupDoc.getFragments("/ResultSetRecord");
			if (oldGroupList != null && oldGroupList.size() >= 1) {
				oldGroupAvailable = true;
			} else
				oldGroupAvailable = false;
		}

		// Searching for image_id in document_image table if any attachments
		// available for new group oid

		String newGroupSearchSQL = "select doc_image_oid from document_image where p_transaction_oid = ?";
		DocumentHandler newGroupDoc = DatabaseQueryBean.getXmlResultSet(newGroupSearchSQL, false, new Object[] { invoiceOid });

		boolean newGroupAvailable = false;
		if (newGroupDoc != null) {
			Vector newGroupList = newGroupDoc.getFragments("/ResultSetRecord");
			if (newGroupList != null && newGroupList.size() > 0) {
				newGroupAvailable = true;
			} else {
				newGroupAvailable = false;
			}

			LOG.debug("updateAttachmentDetails() newGroupAvailable: {}", newGroupAvailable);
		}
		String attachmentStatus = "";
		if (oldGroupAvailable)
			attachmentStatus = TradePortalConstants.INDICATOR_YES;
		else
			attachmentStatus = TradePortalConstants.INDICATOR_NO;

		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", Long.parseLong(oldGroupOid));
		invoiceGroup.setAttribute("attachment_ind", attachmentStatus);
		int status = invoiceGroup.save();
		if (newGroupAvailable)
			attachmentStatus = TradePortalConstants.INDICATOR_YES;
		else
			attachmentStatus = TradePortalConstants.INDICATOR_NO;

		invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", Long.parseLong(newGroupOid));
		invoiceGroup.setAttribute("attachment_ind", attachmentStatus);
		status = invoiceGroup.save();

		return status;
	}

	public static boolean isPayableInvoiceUpdatable(String invoiceStatus) throws RemoteException, AmsException {
		// only 'Available for Auth'invoices can be updated
		return (TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus));

	}

	public static void populateTerms(Terms terms, String invoiceOid, int i, MediatorServices mediatorServices) throws RemoteException, AmsException {
		terms.setAttribute("invoice_only_ind", TradePortalConstants.INDICATOR_YES);
		InvoicesSummaryData invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		ArMatchingRule rule = invoice.getMatchingRule();

		String addressLine1 = invoice.getAttribute("ben_add1");
		String addressLine2 = invoice.getAttribute("ben_add2");
		String addressLine3 = invoice.getAttribute("ben_add3");
		String addressCity = rule != null ? rule.getAttribute("address_city") : "";
		String name = rule != null ? rule.getAttribute("buyer_name") : invoice.getAttribute("buyer_name");
		String benName = StringFunction.isNotBlank(invoice.getAttribute("seller_name")) ? invoice.getAttribute("seller_name") : name;
		String benCountry = invoice.getAttribute("ben_country");
		String benAcctNum = invoice.getAttribute("ben_acct_num");
		String charges = StringFunction.isBlank(invoice.getAttribute("charges")) ? TradePortalConstants.CHARGE_UPLOAD_FW_SHARE : invoice.getAttribute("charges");
		String payMethod = invoice.getAttribute("pay_method");
		String bbkName = invoice.getAttribute("ben_bank_name");
		String bbkAddr1 = invoice.getAttribute("ben_branch_add1");
		String bbkBranchCode = invoice.getAttribute("ben_branch_code");
		String bbkSortCode = invoice.getAttribute("ben_bank_sort_code");
		String bbkAddr2 = invoice.getAttribute("ben_branch_add2");
		String bbkCity = invoice.getAttribute("ben_bank_city");
		String bbkPrvnsState = invoice.getAttribute("ben_bank_province");
		String bbkCountry = invoice.getAttribute("ben_branch_country");
		String centBankRep1 = invoice.getAttribute("central_bank_rep1");
		String centBankRep2 = invoice.getAttribute("central_bank_rep2");
		String centBankRep3 = invoice.getAttribute("central_bank_rep3");
		String benReference = invoice.getAttribute("invoice_id");
		String reportingCode1 = invoice.getAttribute("reporting_code_1");
		String reportingCode2 = invoice.getAttribute("reporting_code_2");
		terms.setAttribute("payment_method", payMethod);
		terms.setAttribute("bank_charges_type", charges);
		if (StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))) {
			terms.newComponent("FirstTermsParty");
		}
		TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

		firstTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY);

		if (!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
			firstTermsParty.setAttribute("name", benName);
		firstTermsParty.setAttribute("address_line_1", addressLine1);
		firstTermsParty.setAttribute("address_line_2", addressLine2);
		firstTermsParty.setAttribute("address_line_3", addressLine3);
		if (StringFunction.isNotBlank(addressCity) && addressCity.length() > 23) {
			addressCity = addressCity.substring(0, 23);
		}
		firstTermsParty.setAttribute("address_city", addressCity);
		firstTermsParty.setAttribute("address_country", benCountry);
		firstTermsParty.setAttribute("acct_num", benAcctNum);
		firstTermsParty.setAttribute("central_bank_rep1", centBankRep1);
		firstTermsParty.setAttribute("central_bank_rep2", centBankRep2);
		firstTermsParty.setAttribute("central_bank_rep3", centBankRep3);
		firstTermsParty.setAttribute("reporting_code_1", reportingCode1);
		firstTermsParty.setAttribute("reporting_code_2", reportingCode2);

		if (i > 1) {
			firstTermsParty.setAttribute("customer_reference", "Multiple Invoices");
		} else {
			firstTermsParty.setAttribute("customer_reference", benReference);
		}
		if (StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty"))) {
			terms.newComponent("ThirdTermsParty");
		}
		TermsParty thirdTermsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
		thirdTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY_BANK);

		if (StringFunction.isNotBlank(bbkName) && bbkName.length() > 35) {
			bbkName = bbkName.substring(0, 35);
		}
		thirdTermsParty.setAttribute("name", bbkName);
		thirdTermsParty.setAttribute("address_line_1", bbkAddr1);
		thirdTermsParty.setAttribute("address_line_2", bbkAddr2);
		if (StringFunction.isNotBlank(bbkCity) && bbkCity.length() > 23) {
			bbkCity = bbkCity.substring(0, 23);
		}
		thirdTermsParty.setAttribute("address_city", bbkCity);
		thirdTermsParty.setAttribute("address_state_province", bbkPrvnsState);
		thirdTermsParty.setAttribute("branch_code", bbkBranchCode);
		thirdTermsParty.setAttribute("address_country", bbkCountry);
		thirdTermsParty.setAttribute("ben_bank_sort_code", bbkSortCode);
	}

	public static boolean isPayableCreditNoteDeletable(String creditNoteStatus, String creditNoteAppliedStatus) {
		return (TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(creditNoteStatus) && TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP.equals(creditNoteAppliedStatus));
	}

	public static boolean isPayableCreditNoteClosable(String creditNoteStatus, String creditNoteAppliedStatus) {
		return ((TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(creditNoteStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditNoteStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(creditNoteStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_AWB.equals(creditNoteStatus) || TradePortalConstants.UPLOAD_INV_STATUS_DBC.equals(creditNoteStatus)) && (TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP.equals(creditNoteAppliedStatus) || TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_PAP.equals(creditNoteAppliedStatus)));
	}

	public static boolean isPayableCreditNoteToReset(String creditNoteStatus) {
		return (TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(creditNoteStatus) || TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditNoteStatus));
	}

	public static boolean isPayableCreditNoteToDocumentAction(String creditNoteStatus) {
		return (TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(creditNoteStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditNoteStatus) || TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(creditNoteStatus));
	}

	public static boolean isPayableCreditNoteAuthorizable(String creditNoteStatus) throws RemoteException, AmsException {
		return (TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(creditNoteStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditNoteStatus) || TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(creditNoteStatus));
	}

	public static String fetchOpOrgCountryForPYBInstrument(String currency, String corpOrg, Map<String, String> countryMap) {
		String country = countryMap.get(currency);
		if (StringFunction.isBlank(country)) {
			DocumentHandler result = null;
			try {
				String sqlQuery = "select o.ADDRESS_COUNTRY AS COUNTRY  from instrument i,transaction t,OPERATIONAL_BANK_ORG o "
						+ "where t.p_instrument_oid = i.instrument_oid and i.a_op_bank_org_oid=o.ORGANIZATION_OID and i.instrument_type_code =? "
						+ " and t.transaction_type_code=? and  t.copy_of_currency_code=? and i.a_corp_org_oid= ?"
						+ " order by t.transaction_oid desc ";
				result = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, TradePortalConstants.PAYABLES_MGMT, TradePortalConstants.PAYABLES_CREATE, currency, corpOrg);
			} catch (AmsException e) {
				LOG.error("Ams Exception while trying to fetch OpOrgCountry For PYBInstrument: ", e);
			}
			if (result != null) {
				country = result.getAttribute("/ResultSetRecord(0)/COUNTRY");
				countryMap.put(currency, country);
			}

		}
		return country;
	}

	public static boolean isValidRPMInstrumentAvailable(String currency, String corpOrg, Map<String, String> instrMap) {
		String instr = instrMap.get(currency);
		if (StringFunction.isBlank(instr)) {
			DocumentHandler result = null;
			try {
				String sqlQuery = "select i.instrument_oid  from instrument i,transaction t,OPERATIONAL_BANK_ORG o " + "where t.p_instrument_oid = i.instrument_oid and i.a_op_bank_org_oid=o.ORGANIZATION_OID and i.instrument_type_code =? " + " and t.transaction_type_code=? and  t.copy_of_currency_code=? and i.a_corp_org_oid= ?" + " order by t.transaction_oid desc ";
				result = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, TradePortalConstants.INV_LINKED_INSTR_REC, TradePortalConstants.NEW_RECEIVABLES, currency, corpOrg);
			} catch (AmsException e) {
				LOG.error("Ams Exception while trying to fetch fetching RPMInstrument: ", e);
			}
			if (result != null) {
				instr = result.getAttribute("/ResultSetRecord(0)/INSTRUMENT_OID");
				instrMap.put(currency, instr);
			}

		}
		return StringFunction.isNotBlank(instr);
	}
	/**
	 * This method is used to get total amount of Invoice and credit note for unique E2E ID.
	 * 
	 * @param corpOrgOid
	 * @param endToEndID
	 * @return
	 * @throws AmsException
	 */
	public static BigDecimal getGroupAmountOFEndToEndID(String corpOrgOid, String endToEndID) throws AmsException {
		String invsql = "SELECT SUM(CASE WHEN payment_amount IS null THEN amount else payment_amount END) invoice_amount FROM invoices_summary_data WHERE end_to_end_id = ? AND a_corp_org_oid = ?";

		DocumentHandler amountSumDoc = DatabaseQueryBean.getXmlResultSet(invsql, true, new Object[] { endToEndID, corpOrgOid });
		BigDecimal invAmount = BigDecimal.ZERO;
		if (amountSumDoc != null && StringFunction.isNotBlank(amountSumDoc.getAttribute("/ResultSetRecord(0)/INVOICE_AMOUNT"))) {
			invAmount = amountSumDoc.getAttributeDecimal("/ResultSetRecord(0)/INVOICE_AMOUNT");
		}

		String credSql = "SELECT SUM(AMOUNT) credit_amount FROM credit_notes WHERE end_to_end_id = ? AND a_corp_org_oid = ?";
		amountSumDoc = DatabaseQueryBean.getXmlResultSet(credSql, true, new Object[] { endToEndID, corpOrgOid });
		BigDecimal credAmount = BigDecimal.ZERO;
		if (amountSumDoc != null && StringFunction.isNotBlank(amountSumDoc.getAttribute("/ResultSetRecord(0)/CREDIT_AMOUNT"))) {
			credAmount = amountSumDoc.getAttributeDecimal("/ResultSetRecord(0)/CREDIT_AMOUNT");
		}

		BigDecimal totalGroupAmount = invAmount.add(credAmount.negate());

		return totalGroupAmount;
	}

	/**
	 * This method is used to get authentication type ( i.e. Single, Dual or Panel authorization) of unique E2D ID group. if group
	 * has invoices, then invoice authentication type will be used, else credit note authentication type will be used.
	 * 
	 * @param corpOrg
	 * @param endToEndID
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static String getAuthTypeOfEndToEndID(CorporateOrganization corpOrg, String endToEndID) throws RemoteException, AmsException {
		String authorizeType = null;
		String whereClause = " end_to_end_id = ? AND a_corp_org_oid = ? ";
		int count = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause, true, new Object[] { endToEndID, corpOrg.getAttribute("organization_oid") });
		if (count > 0) {
			authorizeType = corpOrg.getAttribute("dual_auth_upload_pay_inv");
		} else {
			authorizeType = corpOrg.getAttribute("dual_auth_pay_credit_note");
		}
		return authorizeType;
	}

	public static void setBankChargesForInvoices(String invOidsSQL) throws AmsException {
		String updateSQL = "UPDATE invoices_summary_data SET payment_charges = 'SHA' WHERE " + invOidsSQL;
		try {
			int resultCount = DatabaseQueryBean.executeUpdate(updateSQL, false, new ArrayList<Object>());
		} catch (SQLException e) {
			LOG.error("sql exception while trying to set BankCharges For Invoices: ", e);
		}
	}

	public static boolean isInvoiceCRDeclinable(String field, String fieldDescription, String invoiceStatus, MediatorServices mediatorServices) throws RemoteException, AmsException {
		boolean isValid = true;
		// only 'Available for Auth', 'Replacement Available for Auth', invoices can be declined

		if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus) || TradePortalConstants.UPLOAD_INV_STATUS_RAA.equals(invoiceStatus))) {
			mediatorServices.getErrorManager().issueError(InvoiceGroupMediator.class.getName(), TradePortalConstants.TRANSACTION_CANNOT_PROCESS, field, fieldDescription, ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()), mediatorServices.getResourceManager().getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
			isValid = false;
		}

		return isValid;
	}

	/**
	 * Added for CR-1001 Rel9.4 This method validates payment's Reporting Code(s) uploaded in the invoice file
	 * 
	 * @param bankGroupOid
	 * @param paymentMethod
	 * @param reportingCodes
	 * @param reportingCode1DataReq
	 * @param reportingCode2DataReq
	 * @param errMgr
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static boolean validateReportingCodes(String bankGroupOid, String paymentMethod, String[] reportingCodes, String reportingCode1DataReq, String reportingCode2DataReq, ErrorManager errMgr) throws RemoteException, AmsException {
		Map<String, Map<String, String>> payMap = new HashMap<>();
		return validateReportingCodes(bankGroupOid, payMap, paymentMethod, reportingCodes, reportingCode1DataReq, reportingCode2DataReq, errMgr, true);
	}

	public static boolean validateReportingCodes(String bankGroupOid, Map<String, Map<String, String>> payMap, String paymentMethod, String[] reportingCodes, String reportingCode1DataReq, String reportingCode2DataReq, ErrorManager errMgr, boolean isValidInv) throws RemoteException, AmsException {
		return validateReportingCodes(bankGroupOid, payMap, paymentMethod, reportingCodes, reportingCode1DataReq, reportingCode2DataReq, errMgr, false, "", isValidInv);
	}

	/**
	 * Added for Rel9.4 CR-1001
	 * 
	 * @param bankGroupOid
	 * @param paymentMethod
	 * @param reportingCodes
	 * @param reportingCode1DataReq
	 * @param reportingCode2DataReq
	 * @param errMgr
	 * @param fromAutoCreate
	 * @param invoiceID
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static boolean validateReportingCodes(String bankGroupOid, Map<String, Map<String, String>> payMap, String paymentMethod, String[] reportingCodes, String reportingCode1DataReq, String reportingCode2DataReq, ErrorManager errMgr, boolean fromAutoCreate, String invoiceID, boolean isValid) throws RemoteException, AmsException {
		if (TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)
				|| TradePortalConstants.PAYMENT_METHOD_RTGS.equals(paymentMethod)) {
			String reportingCode1ReqdForACH = null, reportingCode1ReqdForRTGS = null, reportingCode2ReqdForACH = null, reportingCode2ReqdForRTGS = null;
			Map<String, String> repCodeMap = payMap.get(bankGroupOid);
			if (repCodeMap != null) {
				reportingCode1ReqdForACH = repCodeMap.get("reportingCode1ReqdForACH");
				reportingCode1ReqdForRTGS = repCodeMap.get("reportingCode1ReqdForRTGS");
				reportingCode2ReqdForACH = repCodeMap.get("reportingCode2ReqdForACH");
				reportingCode2ReqdForRTGS = repCodeMap.get("reportingCode2ReqdForRTGS");
			} else {

				String sqlQuery = "select reporting_code1_reqd_for_ach, reporting_code1_reqd_for_rtgs, reporting_code2_reqd_for_ach, reporting_code2_reqd_for_rtgs"
						+ " from bank_organization_group where organization_oid=?";
				DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[] { bankGroupOid });
				if (resultDoc != null) {
					reportingCode1ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_ACH");
					reportingCode1ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_RTGS");
					reportingCode2ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_ACH");
					reportingCode2ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_RTGS");
					Map<String, String> repCodeMap1 = new HashMap<>();
					repCodeMap1.put("reportingCode1ReqdForACH", reportingCode1ReqdForACH);
					repCodeMap1.put("reportingCode1ReqdForRTGS", reportingCode1ReqdForRTGS);
					repCodeMap1.put("reportingCode2ReqdForACH", reportingCode2ReqdForACH);
					repCodeMap1.put("reportingCode2ReqdForRTGS", reportingCode2ReqdForRTGS);
					payMap.put(bankGroupOid, repCodeMap1);
				}
			}

			if (TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)) {
				if (TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForACH)
						&& StringFunction.isBlank(reportingCodes[0])
						&& !(TradePortalConstants.INDICATOR_YES.equals(reportingCode1DataReq))) {
					if (fromAutoCreate)
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_1_REQUIRED_INFO, invoiceID, paymentMethod);
					else
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_1_REQUIRED, paymentMethod);
					isValid = false;
				}
				if (TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForACH)
						&& StringFunction.isBlank(reportingCodes[1])
						&& !(TradePortalConstants.INDICATOR_YES.equals(reportingCode2DataReq))) {
					if (fromAutoCreate)
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_2_REQUIRED_INFO, invoiceID, paymentMethod);
					else
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_2_REQUIRED, paymentMethod);
					isValid = false;
				}
			} else if (TradePortalConstants.PAYMENT_METHOD_RTGS.equals(paymentMethod)) {
				if (TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForRTGS)
						&& StringFunction.isBlank(reportingCodes[0])
						&& !(TradePortalConstants.INDICATOR_YES.equals(reportingCode1DataReq))) {
					if (fromAutoCreate)
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_1_REQUIRED_INFO, invoiceID, paymentMethod);
					else
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_1_REQUIRED, paymentMethod);
					isValid = false;
				}
				if (TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForRTGS)
						&& StringFunction.isBlank(reportingCodes[1])
						&& !(TradePortalConstants.INDICATOR_YES.equals(reportingCode2DataReq))) {
					if (fromAutoCreate)
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_2_REQUIRED_INFO, invoiceID, paymentMethod);
					else
						errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORTING_CODE_2_REQUIRED, paymentMethod);
					isValid = false;
				}
			}
		}

		int currentReportCodeIndex = 0;
		while (currentReportCodeIndex < TradePortalConstants.MAX_REP_CODE_IDX) {
			isValid = isValid
					& validateReportingCode(reportingCodes[currentReportCodeIndex], bankGroupOid, Integer.toString(++currentReportCodeIndex), errMgr, fromAutoCreate, invoiceID);
		}

		return isValid;
	}

	/**
	 * Added for CR-1001 Rel9.4
	 * 
	 * @param reportingCode
	 * @param bankGroupOid
	 * @param repCodeIndex
	 * @param isValid
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static boolean validateReportingCode(String reportingCode, String bankGroupOid, String repCodeIndex, ErrorManager errMgr, boolean fromAutoCreate, String invoiceID) throws RemoteException, AmsException {

		String repAttr = "reporting_code_" + repCodeIndex;
		List<Object> temp = new ArrayList<Object>();
		Object intOb = new Object();
		intOb = (bankGroupOid != null) ? Long.parseLong(bankGroupOid) : bankGroupOid;
		temp.add(intOb);
		boolean repSelected = false;
		String whereSqlStr = " p_bank_group_oid = ?";
		if (StringFunction.isNotBlank(reportingCode)) {
			repSelected = true;
			whereSqlStr += " and code = ?";
			temp.add(reportingCode);
		}

		// obtain count.
		int reportCount = DatabaseQueryBean.getCount("code", "PAYMENT_REPORTING_CODE_" + repCodeIndex, whereSqlStr, false, temp);

		// if reporting code was selected on payment but was not found in the database, issue an error.
		boolean isValid = true;
		if (repSelected && reportCount < 1) {
			if ("1".equals(repCodeIndex)) {
				if (fromAutoCreate)
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_REPORTING_CODE1_INFO, invoiceID, reportingCode);
				else
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_REPORTING_CODE1, reportingCode);
			} else if ("2".equals(repCodeIndex)) {
				if (fromAutoCreate)
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_REPORTING_CODE2_INFO, invoiceID, reportingCode);
				else
					errMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_REPORTING_CODE2, reportingCode);
			}
			isValid = false;
		}
		return isValid;

	}

	public static String fetchBankGroupForOpOrg(String opOrgOid) {
		opOrgOid = StringFunction.isNotBlank(opOrgOid) ? opOrgOid : "0";
		DocumentHandler result = null;
		try {
			String sqlQuery = "SELECT ORGANIZATION_OID FROM BANK_ORGANIZATION_GROUP WHERE BANK_ORGANIZATION_GROUP.ORGANIZATION_OID = "
					+ "(SELECT OPERATIONAL_BANK_ORG.A_BANK_ORG_GROUP_OID FROM OPERATIONAL_BANK_ORG WHERE ORGANIZATION_OID = ? )";
			result = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, opOrgOid);
		} catch (AmsException e) {
			LOG.error("Ams Exception while trying to fetch BankGroup For OpOrg: ", e);
		}
		String bankGrp = "";
		if (result != null) {
			bankGrp = result.getAttribute("/ResultSetRecord/ORGANIZATION_OID");
		}
		return bankGrp;
	}

	public static int calculateNumTenorDays(ArMatchingRule matchingRule, Date gmtDate, SimpleDateFormat formatter, String dueDate, String paymentDate) throws AmsException, RemoteException {
		if (matchingRule == null) {
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO);
			throw new AmsException(is);
		}
		// BSL IR NNUM040526786 04/05/2012 BEGIN - check % inv val to finance
		String pctFinAllowedStr = matchingRule.getAttribute("invoice_value_finance");
		BigDecimal pctFinAllowed = StringFunction.isBlank(pctFinAllowedStr) ? BigDecimal.ZERO : new BigDecimal(pctFinAllowedStr).divide(new BigDecimal("100.0"), MathContext.DECIMAL64);

		if (BigDecimal.ZERO.compareTo(pctFinAllowed) == 0) {
			// Percent Invoice Value to Finance is zero
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO);
			throw new AmsException(is);
		}
		// BSL IR NNUM040526786 04/05/2012 END

		String paymentDateAllowed = matchingRule.getAttribute("payment_day_allow");

		boolean usePaymentDate = (StringFunction.isNotBlank(paymentDate) && TradePortalConstants.INDICATOR_YES.equals(paymentDateAllowed));

		String effectiveDate;
		if (usePaymentDate) {
			effectiveDate = paymentDate;
		} else {
			effectiveDate = dueDate;
		}

		String value = matchingRule.getAttribute("days_finance_of_payment");
		int numDaysBefore = StringFunction.isBlank(value) ? 0 : Integer.parseInt(value);

		// calculate based on current business date to due date/payment date
		if (gmtDate == null)
			gmtDate = GMTUtility.getGMTDateTime();
		if (formatter == null)
			formatter = new SimpleDateFormat("MM/dd/yyyy");

		try {
			formatter.parse(effectiveDate);
		} catch (ParseException e) {
			throw new AmsException("Error converting date: " + effectiveDate + " to java.util.Date object. Nested exception: "
					+ e.getMessage());
		}
		int numTenorDays = calculateDaysDiff(DateTimeUtility.convertStringDateToDate(effectiveDate), gmtDate);

		if (numTenorDays < numDaysBefore) {
			IssuedError is = new IssuedError();
			is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS);
			throw new AmsException(is);
		}

		// If the "days variance of payment date to invoice due date" is
		// outside of value, then we cannot finance.
		if (usePaymentDate) {
			int dueDatePaymentDateDaysDiff = calculateDaysDiff(dueDate, paymentDate);

			int daysAllowed;
			if (dueDatePaymentDateDaysDiff < 0) {
				// Payment Date is after Due Date
				dueDatePaymentDateDaysDiff *= -1;
				daysAllowed = Integer.parseInt(matchingRule.getAttribute("days_after"));
			} else {
				// Payment Date is before Due Date
				daysAllowed = Integer.parseInt(matchingRule.getAttribute("days_before"));
			}

			if (dueDatePaymentDateDaysDiff > daysAllowed) {
				IssuedError is = new IssuedError();
				is.setErrorCode(TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF);
				throw new AmsException(is);
			}
		}
		return numTenorDays;

	}

	public static int calculateDaysDiff(String dateStr1, String dateStr2) throws AmsException {
		Date date1 = DateTimeUtility.convertStringDateToDate(dateStr1);
		Date date2 = DateTimeUtility.convertStringDateToDate(dateStr2);

		return calculateDaysDiff(date1, date2);
	}

	public static int calculateDaysDiff(String dateStr1) throws AmsException {
		Date date1 = DateTimeUtility.convertStringDateToDate(dateStr1);
		Date date2 = GMTUtility.getGMTDateTime();

		return calculateDaysDiff(date1, date2);
	}

	public static Map getCurrencyCalendar(String currenyCode, String inputDate) throws RemoteException, AmsException {
		Map cal = new HashMap<String, String>();
		// Get Currency Calendar Rule, TP Calendar and TP Calendar Year records to obtain nececssary input
		String sqlSelect = "Select CC.A_TP_CALENDAR_OID, TC.WEEKEND_DAY_1, TC.WEEKEND_DAY_2 from CURRENCY_CALENDAR_RULE CC, TP_CALENDAR TC"
				+ " where CC.CURRENCY = ? " + " and CC.A_TP_CALENDAR_OID = TC.TP_CALENDAR_OID";

		DocumentHandler calendarCur = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[] { currenyCode });
		if (calendarCur == null) {
			LOG.debug("InvoiceUtility::getCalendarBasedNextBusinessDate::CURRENCY_CALENDAR_NOT_DEFINED: {}" ,currenyCode);
			return null;
		}
		String tpCalOid = calendarCur.getAttribute("/ResultSetRecord(0)/A_TP_CALENDAR_OID");
		if (StringFunction.isBlank(tpCalOid)) {
			LOG.debug("Unexpected Error: Can't retrive Calendar Record OID form the Found Record");
			return null;
		}
		int intYear;
		try {
			intYear = Integer.parseInt(inputDate.substring(6, 10));
		} catch (Exception any_exc) {
			LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::Execute Date is not entered/ is in invalid format. Value Date will be set to none");
			return null;
		}

		sqlSelect = "select TP_CALENDAR_YEAR_OID FROM TP_CALENDAR_YEAR WHERE P_TP_CALENDAR_OID = ?  and year = ? ";

		DocumentHandler calendarYear = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[] { tpCalOid, intYear });
		if (calendarYear == null) {
			LOG.debug("InvoiceUtility::getCalendarBasedNextBusinessDate::CALENDAR_NOT_DEFINED_FOR_YEAR {}:  /tcurrenyCode{}:", intYear,currenyCode);
			return null;
		}
		String calendarYearOid = calendarYear.getAttribute("/ResultSetRecord(0)/TP_CALENDAR_YEAR_OID");
		if (StringFunction.isBlank(calendarYearOid)) {
			LOG.debug("Unexpected Error: Can't retrive Calendar Year Oid form the Found Record");
			return null;
		}

		// Obtain weekend days pattern, if defined
		int weekend1 = 0;

		String strWeekend1 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_1");
		if (StringFunction.isNotBlank(strWeekend1)) {
			weekend1 = Integer.parseInt(strWeekend1);
		}
		String strWeekend2 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_2");
		int weekend2 = 0;
		if (StringFunction.isNotBlank(strWeekend2)) {
			weekend2 = Integer.parseInt(strWeekend2);
		}
		cal.put("tpCalOid", tpCalOid);
		cal.put("weekend1", weekend1);
		cal.put("weekend2", weekend2);
		cal.put("calendarYearOid", calendarYearOid);

		return cal;
	}

	public static String getCalendarBasedNextBusinessDate(String currenyCode, String inputDate, Map cal) throws RemoteException, AmsException {
		if (cal == null || cal.isEmpty()) {
			return inputDate;
		}

		// Obtain weekend days pattern, if defined
		int weekend1 = (Integer) cal.get("weekend1");
		int weekend2 = (Integer) cal.get("weekend2");

		LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::weekends {} and {}" ,weekend1,weekend2);

		// Create TP Calendar Year Object for given year/currency.
		// Invoke method to obtain next business day provided execution date, number of offset days, and weekend dats pattern.
		// Shilpa R CR710 Rel 8.0 start
		String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
		TPCalendarYear tpCalendarYear = (TPCalendarYear) EJBObjectFactory.createClientEJB(serverLocation, "TPCalendarYear");
		// Shilpa R CR710 Rel 8.0 end
		tpCalendarYear.getData(Long.parseLong((String) cal.get("calendarYearOid")));
		String valueDate = tpCalendarYear.getNextBusinessDay(inputDate, 0, weekend1, weekend2);
		LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::this is value date: {}", valueDate);

		return valueDate;
	}

	public static boolean validatePaymentMethodBenCountry(InvoiceSummaryDataObject invSummaryData, MediatorServices mediatorServices, Map<String, String> map) throws RemoteException, AmsException {

		if (!TradePortalConstants.INDICATOR_YES.equals(map.get("pay_method_req"))) {
			return true;
		}
		Set<String> validPaymentMethods = new HashSet<>();
		validPaymentMethods.add("ACH");
		validPaymentMethods.add("RTGS");
		validPaymentMethods.add("CBFT");
		String paymentMethod = invSummaryData.getPay_method();

		boolean isValid = true;
		if (!validPaymentMethods.contains(paymentMethod)) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD, paymentMethod);
			isValid = false;
		}
		// IR 22819 -BEGIN- if payment method is CBFT and no bene country or invalid country code then error
		if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod)) {
			if (TradePortalConstants.INDICATOR_NO.equals(map.get("ben_country_req"))
					|| StringFunction.isBlank(invSummaryData.getBen_country())) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.COUNTRY_REQD_FOR_CBFT_PAY_METHOD, paymentMethod);
				isValid = false;
			}
		}
		// IR 22819 -END

		return isValid;
	}

	public static void createOutgoingQueueAndIssueSuccess(String status, String invoiceOid, String ejbServerLocation, ClientServerDataBridge csdb) throws AmsException, RemoteException {
		OutgoingInterfaceQueue outgoingMessage = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue", csdb);
		outgoingMessage.newObject();
		outgoingMessage.setAttribute("date_created", DateTimeUtility.getGMTDateTime());
		outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
		outgoingMessage.setAttribute("message_id", InstrumentServices.getNewMessageID());

		StringBuilder processParameters = new StringBuilder("invoice_oid=");
		processParameters.append(invoiceOid).append("|");
		processParameters.append("status=").append(status);

		outgoingMessage.setAttribute("process_parameters", processParameters.toString());

		outgoingMessage.setAttribute("msg_type", MessageType.TRIG);

		if (outgoingMessage.save() != 1) {
			throw new AmsException("Error occurred while inserting Credit Note invoice message type in Outgoing Queue");
		}
	}
}