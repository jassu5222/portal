package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for representing the data for a single Purchase Order
 * (PO) line item in a PO data file. It consists of a list of PO line item
 * fields (POLineItemField objects), an Auto LC logger, and various fields
 * used for validating and saving a PO line item and for possibly creating or
 * updating an Import LC instrument.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *

 */

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.util.*;
import java.math.*;
import java.text.*;
import java.rmi.*;

import javax.ejb.*;

public class POLineItemData
{
private static final Logger LOG = LoggerFactory.getLogger(POLineItemData.class);

   private final static int   NUMBER_OF_STANDARD_PO_FIELDS = 7;
   private final static int   MAX_NUMBER_OF_PO_FIELDS      = 30;

   private SessionContext     sessionContext               = null;
   private AutoLCLogger       autoLCLogger                 = null;
   private ResourceManager    resMgr                       = null;
   private Transaction        poLineItemTransaction        = null;
   private POLineItem         poLineItem                   = null;
   private POLineItem         copyOfPoLineItem             = null;
   private Vector             poLineItemFieldList          = null;
   private String             poLineItemLastShipmentDate   = null;
   private String             poUploadUserSecurityRights   = null;
   private String             poLineItemBeneficiaryName    = null;
   private String             poUploadSequenceNumber       = null;
   private String             logSequenceNumber            = null;
   private String             poLineItemCurrency           = null;
   private String             poUploadUserOrgOid           = null;
   private String             poLineItemAmount             = null;
   private String             poDataFileFormat             = null;
   private String             poLineItemNumber             = null;
   private String             poText                       = null;
   private String             poUploadUserOid              = null;
   private String             itemNumber                   = null;
   private String             poNumber                     = null;
   private String             lastShipDate                 = null;
   private String             poDateFormat                 = null;
   private long               poUploadDefinitionOid        = 0;
   private char               poDataDelimiter              = '\u0000';
   private int                numberOfPOLineItemFields     = 0;
   private int                poNumberFieldIndex           = 0;
   private int                maxPOLineItemSize            = 0;
   private String             sourceType                   = null;

   //TLE - 08/-0/07 - CR-375 - Begin
   private String             instrumentType               = null;
   //TLE - 08/-0/07 - CR-375 - End

   // These following variables should only be set if the PO Line
   // Item source type is MANUAL
   private DocumentHandler    manualPOLineItemDoc          = null;
   private ErrorManager       errorMgr;

   /**
    * This method is the class's constructor, which is responsible for
    * initializing the PO line item data object with various attributes that
    * will be used throughout the parsing, validating, and saving of PO line
    * item data.
    *

    * @param      java.lang.String newPOUploadUserSecurityRights - the security rights of the user that uploaded the
    *                                                              file containing PO line item data
    * @param      java.lang.String newPOUploadUserOrgOid - the unique ID representing the user's organization
    * @param      java.lang.String newPOUploadUserOid - the unique ID representing the user who performed the PO upload
    * @param      java.lang.String newPOUploadSequenceNumber - the sequence number representing the current PO upload
    * @param      java.lang.String newLogSequenceNumber - the sequence number representing the current PO upload log
    * @param      long newPOUploadDefinitionOid - the unique ID representing the PO upload definition to use for
    *                                             parsing and validating PO line item data
    * @param      javax.ejb.SessionContext newSessionContext - the current mediator's session context
    * @return     void
    */
   public POLineItemData(String newPOUploadUserSecurityRights, String newPOUploadUserOrgOid, String newPOUploadUserOid,
                         String newPOUploadSequenceNumber, String newLogSequenceNumber, long newPOUploadDefinitionOid,
                         SessionContext newSessionContext, AutoLCLogger newAutoLCLogger, ResourceManager newResMgr)
   {
      POUploadDefinition   poUploadDefinition = null;

      // Initialize all necessary class attributes
      poUploadUserSecurityRights = newPOUploadUserSecurityRights;
      poUploadSequenceNumber     = newPOUploadSequenceNumber;
      logSequenceNumber          = newLogSequenceNumber;
      poUploadDefinitionOid      = newPOUploadDefinitionOid;
      poUploadUserOrgOid         = newPOUploadUserOrgOid;
      poUploadUserOid            = newPOUploadUserOid;
      sessionContext             = newSessionContext;
      autoLCLogger               = newAutoLCLogger;
      resMgr                     = newResMgr;
      poLineItemFieldList        = new Vector();
      sourceType                 = TradePortalConstants.PO_SOURCE_UPLOAD;

      // Retrieve the PO upload definition to use for parsing and validating
      // the PO line item data
      try
      {
         poUploadDefinition = (POUploadDefinition) EJBObjectFactory.createServerEJB(sessionContext,
                                                                                    "POUploadDefinition",
                                                                                    poUploadDefinitionOid);

         // Retrieve the format of the PO data file; if it's delimited, retrieve
         // the character to be used as the delimiter
         poDataFileFormat = poUploadDefinition.getAttribute("file_format");

         if (poDataFileFormat.equals(TradePortalConstants.PO_DELIMITED))
         {
            setPODataDelimiter(poUploadDefinition.getAttribute("delimiter_char"));
         }

         // Set the PO Definition's date format
         setPODateFormat(poUploadDefinition.getAttribute("date_format"));

         // Create and initialize all PO Line Item Field objects used for holding
         // the type, name, data type, size, and value for each PO line item field
         setPOLineItemFields(poUploadDefinition);
      }
      catch (Exception ex)
      {
         LOG.error("Exception occurred in POLineItemData constructor: ",ex);
      }
      finally
      {
         removePOUploadDefinition(poUploadDefinition);
      }
   }

   /**
    * This method is an overloaded version of the class's constructor, which is used for validating
    * manually entered PO Line Items
    *
    * @param      long newPOUploadDefinitionOid - the unique ID representing the PO definition to use for
    *                                             validating PO line item data
    * @param      com.amsinc.ecsg.util.DocumentHandler poLineItemDoc
    * @param      ErrorManger newErrorMgr
    * @param      javax.ejb.SessionContext newSessionContext - the current mediator's session context
    * @return     void
    */

   public POLineItemData(long newDefinitionOid, String newPOLineItemNumber, DocumentHandler newPOLineItemDoc,
                         ErrorManager newErrorMgr, SessionContext newSessionContext)
   {
      POUploadDefinition   poUploadDefinition = null;

      // Initialize all necessary class attributes
      poUploadDefinitionOid      = newDefinitionOid;
      poLineItemNumber           = newPOLineItemNumber;
      manualPOLineItemDoc        = newPOLineItemDoc;
      sessionContext             = newSessionContext;
      poLineItemFieldList        = new Vector();
      sourceType                 = TradePortalConstants.PO_SOURCE_MANUAL;
      errorMgr                   = newErrorMgr;

      // Retrieve the PO upload definition to use for parsing and validating
      // the PO line item data
      try
      {
         poUploadDefinition = (POUploadDefinition) EJBObjectFactory.createServerEJB(sessionContext,
                                                                                    "POUploadDefinition",
                                                                                    poUploadDefinitionOid);

         // Create and initialize all PO Line Item Field objects used for holding
         // the type, name, data type, size, and value for each PO line item field
         setPOLineItemFields(poUploadDefinition);

         // Set the PO Definition's date format
         setPODateFormat(poUploadDefinition.getAttribute("date_format"));
      }
      catch (Exception ex)
      {
         LOG.error("Exception occurred in POLineItemData constructor: " , ex);
      }
   }

   /**
    * This method simply returns the PO number for the current PO line item
    * (if it exists).
    *

    * @return     java.lang.String - the current PO number
    */
   public String getPONumber()
   {
      return poNumber;
   }

   /**
    * This method simply returns the last shipment date formatted for the current PO line item
    * (if it exists).
    *

    * @return     java.lang.String - the current last shipment date
    */
   public String getLastShipmentDate()
   {
      return lastShipDate;
   }

   /**
    * This method creates a new PO line item field based on the attributes
    * passed into it and adds it to the list of PO line item fields. It also
    * increments the class's number of fields counter accordingly.
    *

    * @param      java.lang.String fieldType - the type of PO line item field (i.e., po_num, ben_name, other1, etc.)
    * @param      java.lang.String fieldDataType - the data type of the PO line item field (i.e., date, number, text)
    * @param      java.lang.String fieldSize - the maximum number of characters allowed for the PO line item field
    * @param      java.lang.String fieldName - the name of the PO line item field
    * @return     void
    */
   public void addPOLineItemField(String fieldType, String fieldDataType, int fieldSize, String fieldName, String fieldValue)
   {
      POLineItemField   poLineItemField = new POLineItemField();

      poLineItemField.setFieldType(fieldType);
      poLineItemField.setFieldDataType(fieldDataType);
      poLineItemField.setFieldSize(fieldSize);
      poLineItemField.setFieldName(fieldName);
      poLineItemField.setFieldValue(fieldValue);

      poLineItemFieldList.addElement(poLineItemField);

      numberOfPOLineItemFields++;
   }

   /**
    * This method sets the character to be used for parsing a PO data file in
    * delimited format.
    *

    * @param      java.lang.String newPODataDelimiter - constant representing the PO delimiter to use
    * @return     void
    */
   public void setPODataDelimiter(String newPODataDelimiter)
   {
      if (newPODataDelimiter.equals(TradePortalConstants.PO_COMMA))
      {
         poDataDelimiter = ',';
      }
      else if (newPODataDelimiter.equals(TradePortalConstants.PO_TAB))
      {
         poDataDelimiter = '\t';
      }
      else
      {
         poDataDelimiter = ';';
      }
   }

   /**
    * This method sets the date format this PO Definition utilizes
    *

    * @param      java.lang.String newPODateFormat
    * @return     void
    */
   public void setPODateFormat(String newPODateFormat)
   {
      if (newPODateFormat.equals(TradePortalConstants.PO_DATE_FORMAT_EURO))
      {
         poDateFormat = TradePortalConstants.PO_FORMATTED_EURO_DATE;
      }
      else
      {
         poDateFormat = TradePortalConstants.PO_FORMATTED_US_DATE;
      }
   }

   /**
    * This method sets the type, name, data type, and size for each field
    * specified in the PO upload definition passed in.
    *

    * @param      com.ams.tradeportal.busobj.POUploadDefinition poUploadDefinition - the PO upload definition to use for
    *                                                                                parsing and validating the PO line
    *                                                                                item data
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void setPOLineItemFields(POUploadDefinition poUploadDefinition) throws AmsException, RemoteException
   {
      boolean   hasMoreFields        = true;
      String    currentFieldDataType = null;
      String    currentFieldType     = null;
      String    currentFieldName     = null;
      String    currentFieldValue    = "";
      int       currentFieldSize     = 0;
      int       index                = 1;

      // For each field specified in the PO upload definition, get the type, data
      // type, size, and name and save the field to the list of PO line item fields
      while (hasMoreFields)
      {
         currentFieldType = poUploadDefinition.getAttribute("upload_order_" + index);

         if ((currentFieldType != null) && (!currentFieldType.equals("")))
         {
            currentFieldDataType = poUploadDefinition.getAttribute(currentFieldType + "_datatype");
            currentFieldSize     = poUploadDefinition.getAttributeInteger(currentFieldType + "_size");
            currentFieldName     = poUploadDefinition.getAttribute(currentFieldType + "_field_name");

            // If this is a manually entered PO then set the PO Line Item's field value by getting
            // the value from the xml document
            if (sourceType.equals(TradePortalConstants.PO_SOURCE_MANUAL))
            {
              currentFieldValue = manualPOLineItemDoc.getAttribute("/" + currentFieldType);
              if (currentFieldValue == null) currentFieldValue = "";
            }

            maxPOLineItemSize += currentFieldSize;

            addPOLineItemField(currentFieldType, currentFieldDataType, currentFieldSize, currentFieldName, currentFieldValue);

            index++;
         }
         else
         {
            hasMoreFields = false;
         }
      }
   }

   /**
    * This method calls other methods (depending on the file format specified
    * in the PO upload definition) to validate the format of the current PO
    * line item string and to set the value attribute for each PO line item
    * field. If the current PO line item string is in the correct format and
    * all PO line item field values have been set, the method will set the
    * class's po line item number attribute for use in eventual Auto LC log
    * messages.
    *

    * @param      java.lang.String currentPOLineItemData - the current PO line item string pulled from the PO data file
    * @return     boolean - indicates whether or not the PO line item string is in the correct format for the file
    *                       format specified in the PO upload definition
    *                       (true  - the PO line item string is in the correct format and all PO line item field values
    *                                have been set
    *                        false - the PO line item string is not in the correct format and no PO line item field
    *                                values have been set)
    */
   public boolean setPOLineItemFieldValues(String currentPOLineItemData)
   {
      // Validate the format of the PO line item string. If it isn't in the correct
      // format, don't process the PO line item and continue with the next one;
      // otherwise, parse the string and set the value attribute for each PO line
      // item field
      try
      {
         if (poDataFileFormat.equals(TradePortalConstants.PO_DELIMITED))
         {
            if (validatePOLineItemDelimitedFormat(currentPOLineItemData) == false)
            {
               return false;
            }

            setPOLineItemFieldDelimtedValues(currentPOLineItemData);
         }
         else
         {
            if (validatePOLineItemFixedFormat(currentPOLineItemData) == false)
            {
               return false;
            }

            setPOLineItemFieldFixedValues(currentPOLineItemData);
         }
      }
      catch (AmsException ex)
      {
         LOG.error("AmsException occurred in POLineItemData.setPOLineItemFieldValues(): " ,ex);
      }

      // Set the po line item number attribute for use in eventual Auto LC log messages
      setPOLineItemNumber();

      return true;
   }

   /**
    * This method validates the format of the current PO line item string. It
    * determines whether the current PO line item string has the correct number
    * of delimiter characters in it (i.e., one less than the number of fields
    * in the string).
    *

    * @param      java.lang.String currentPOLineItemData - the current PO line item string pulled from the PO data file
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the PO line item string is in the correct format for the file
    *                       format specified in the PO upload definition
    *                       (true  - the PO line item string is in the correct format
    *                        false - the PO line item string is not in the correct format)
    */
   private boolean validatePOLineItemDelimitedFormat(String currentPOLineItemData) throws AmsException
   {
      boolean   hasMoreDelimiters  = true;
      int       currentLength      = 0;
      int       numberOfDelimiters = 0;
      int       delimiterIndex     = 0;

      currentLength = currentPOLineItemData.length();

      // Determine how many delimiter characters there are in the current PO line item string
      while (hasMoreDelimiters)
      {
         delimiterIndex = currentPOLineItemData.indexOf(poDataDelimiter);

         if (delimiterIndex == -1)
         {
            hasMoreDelimiters = false;

            continue;
         }

         numberOfDelimiters++;

         // If the delimiter isn't the last character in the string, set the string to the
         // substring after the delimiter and update the current string length; otherwise,
         // stop looking for delimiters since we've reached the end of the string
         if (delimiterIndex != (currentLength - 1))
         {
            currentPOLineItemData = currentPOLineItemData.substring(delimiterIndex + 1);

            currentLength = currentPOLineItemData.length();
         }
         else
         {
            hasMoreDelimiters = false;
         }
      }

      // If the number of delimiters is one less than the number of PO fields, we have a
      // valid format; otherwise, log an error message to the Auto LC log file and
      // continue with the next PO line item
      if (numberOfDelimiters == (numberOfPOLineItemFields - 1))
      {
         return true;
      }

      autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                 TradePortalConstants.LCLOG_INVALID_DELIMITED_FORMAT);

      return false;
   }

   /**
    * This method validates the format of the current PO line item string. It
    * determines whether the length of the current PO line item string matches
    * the combined lengths of each PO line item field specified in the PO
    * upload definition.
    *

    * @param      java.lang.String currentPOLineItemData - the current PO line item string pulled from the PO data file
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the PO line item string is in the correct format for the file
    *                       format specified in the PO upload definition
    *                       (true  - the PO line item string is in the correct format
    *                        false - the PO line item string is not in the correct format)
    */
   private boolean validatePOLineItemFixedFormat(String currentPOLineItemData) throws AmsException
   {
      // If the length of the current PO line item string is the same as the combined sizes
      // of each PO line item field, we have a valid format; otherwise, log an error message
      // to the Auto LC log file and continue with the next PO line item
      if (currentPOLineItemData.length() == maxPOLineItemSize)
      {
         return true;
      }

      autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                 TradePortalConstants.LCLOG_INVALID_FIXED_FORMAT);

      return false;
   }

   /**
    * This method parses the current PO line item string and sets the value
    * attribute for each PO line item field. Because we've already validated
    * that the string is in the correct delimited format, we can set the
    * value attribute to the string in between delimiters (excluding
    * surrounding whitespace). While parsing the string, if a field is one of
    * the 7 standard fields (i.e., po_num, item_num, ben_name, currency,
    * amount, po_text or last_ship_dt), set the corresponding class attribute
    * accordingly.
    *

    * @param      java.lang.String currentPOLineItemData - the current PO line item string pulled from the PO data file
    * @return     void
    */
   private void setPOLineItemFieldDelimtedValues(String currentPOLineItemData)
   {
      POLineItemField   poLineItemField           = null;
      String            poLineItemFieldValue      = null;
      String            poLineItemFieldType       = null;
      int               poLineItemFieldEndIndex   = 0;

      for (int i = 0; i < numberOfPOLineItemFields; i++)
      {
         poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(i);

         // Retrieve the index of the next delimiter character
         poLineItemFieldEndIndex = currentPOLineItemData.indexOf(poDataDelimiter);

         // If we don't find the delimiter, we must be at the end of the string
         // (and therefore the last PO line item field)
         if (poLineItemFieldEndIndex == -1)
         {
            poLineItemFieldValue = currentPOLineItemData.trim();
         }
         else
         {
            poLineItemFieldValue = currentPOLineItemData.substring(0, poLineItemFieldEndIndex).trim();
         }

         poLineItemField.setFieldValue(poLineItemFieldValue);

         // If the current field is one of the 7 standard fields, set the appropriate class
         // attribute to this value as well
         poLineItemFieldType = poLineItemField.getFieldType();

         if (poLineItemFieldType.indexOf(TradePortalConstants.PO_FIELD_TYPE_OTHER) == -1)
         {
            setPOLineItemStandardField(poLineItemFieldType, poLineItemFieldValue, i);
         }

         // Reset the current PO line item string to the substring after the delimiter
         currentPOLineItemData = currentPOLineItemData.substring(poLineItemFieldEndIndex + 1);
      }
   }

   /**
    * This method parses the current PO line item string and sets the value
    * attribute for each PO line item field. Because we've already validated
    * that the string is in the correct fixed format, we can set the value
    * attribute to the string of characters matching the size of the current
    * field (excluding surrounding whitespace). While parsing the string, if
    * a field is one of the 6 standard fields (i.e., po_num, item_num,
    * ben_name, currency, amount, or last_ship_dt), set the corresponding class
    * attribute accordingly.
    *

    * @param      java.lang.String currentPOLineItemData - the current PO line item string pulled from the PO data file
    * @return     void
    */
   private void setPOLineItemFieldFixedValues(String currentPOLineItemData)
   {
      POLineItemField   poLineItemField           = null;
      String            poLineItemFieldValue      = null;
      String            poLineItemFieldType       = null;
      int               poLineItemFieldBeginIndex = 0;
      int               poLineItemFieldEndIndex   = 0;

      for (int i = 0; i < numberOfPOLineItemFields; i++)
      {
         poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(i);

         // Retrieve the end index to use for the current field value based on
         // the current field's size
         poLineItemFieldEndIndex = poLineItemFieldBeginIndex + poLineItemField.getFieldSize();

         poLineItemFieldValue = currentPOLineItemData.substring(poLineItemFieldBeginIndex,
                                                                poLineItemFieldEndIndex).trim();

         poLineItemField.setFieldValue(poLineItemFieldValue);

         // If the current field is one of the 7 standard fields, set the appropriate class
         // attribute to this value as well
         poLineItemFieldType = poLineItemField.getFieldType();

         if (poLineItemFieldType.indexOf(TradePortalConstants.PO_FIELD_TYPE_OTHER) == -1)
         {
            setPOLineItemStandardField(poLineItemFieldType, poLineItemFieldValue, i);
         }

         // Reset the begin index to the previous end index; this is because the end index
         // is based on the field's size and not the actual index into the string (calling
         // substring using this end index returns a string up until end index - 1, which
         // is the correct end index in our case).
         poLineItemFieldBeginIndex = poLineItemFieldEndIndex;
      }
   }

   /**
    * This method sets one of the 7 standard class attributes (i.e., po_num,
    * item_num, ben_name, currency, amount, or last_ship_dt), to the current
    * PO line item field value passed in. This is so we don't have to search
    * through the entire list of PO line item fields each time we need to
    * access one of these attributes. If the current field being set is the
    * PO number field, also set the class's PO number index attribute. This
    * index is used only if we have an empty or null PO number (so we can get
    * the field name for an error message later).
    *

    * @param      java.lang.String poLineItemFieldType - the current PO line item field type
    * @param      java.lang.String poLineItemFieldValue - the current PO line item field value
    * @param      int index - the current index into the list of PO line item fields
    * @return     void
    */
   private void setPOLineItemStandardField(String poLineItemFieldType, String poLineItemFieldValue, int index)
   {
      if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_PO_NUMBER))
      {
         poNumber           = poLineItemFieldValue;
         poNumberFieldIndex = index;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_ITEM_NUMBER))
      {
         itemNumber = poLineItemFieldValue;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_BENEFICIARY_NAME))
      {
         poLineItemBeneficiaryName = poLineItemFieldValue;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_CURRENCY))
      {
         poLineItemCurrency = poLineItemFieldValue;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_AMOUNT))
      {
         poLineItemAmount = poLineItemFieldValue;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_PO_TEXT))
      {
         poText = poLineItemFieldValue;
      }
      else if (poLineItemFieldType.equals(TradePortalConstants.PO_FIELD_TYPE_LAST_SHIPMENT_DATE))
      {
         poLineItemLastShipmentDate = TPDateTimeUtility.convertFromEuroToJPylonDateTimeFormat(poLineItemFieldValue);
      }
   }

   /**
    * This method is used to set the class's PO line item number attribute for
    * eventual use in Auto LC error log messages. If an item number doesn't
    * exist, this number is simply the PO number; otherwise, this number is the
    * PO number plus a space plus the item number. If a PO number doesn't exist,
    * the method simply returns.
    *

    * @return     void
    */
   private void setPOLineItemNumber()
   {
      StringBuffer   newPOLineItemNumber = null;

      if ((poNumber != null) && (!poNumber.equals("")))
      {
         newPOLineItemNumber = new StringBuffer();
         newPOLineItemNumber.append(poNumber);
         newPOLineItemNumber.append(" ");

         if ((itemNumber != null) && (!itemNumber.equals("")))
         {
            newPOLineItemNumber.append(itemNumber);
         }

         poLineItemNumber = newPOLineItemNumber.toString();
      }
   }

   /**
    * This method is used to call other methods to determine whether or not the
    * PO line item exists already. If a match is found in the PO line item
    * table, certain things occur depending on the type of match found, such as
    * adding a log message to the Auto LC log, validation of field values,
    * updating and/or resetting of transaction field values, creating a new
    * transaction, etc. Once a match has been found (and all match-specific
    * actions have finished), the method will stop looking for other matches.
    *

    * @return     boolean - indicates whether or not a PO line item match was found
    *                       (true  - a PO line item match was found, so stop processing the PO line item
    *                        false - a PO line item match was not found, so continue processing the PO line item)
    */
   public boolean checkForPOLineItemMatch(String userLocale) //logu & amit
   {
      try
      {
         if (checkForUnassignedPOLineItemMatch() == true)
         {
            return true;
         }

         if (checkForAssignedPOLineItemMatch(userLocale) == true) //logu & amit
         {
            return true;
         }

         if (checkForAuthorizedIssuePOLineItemMatch() == true)
         {
            return true;
         }

         if (checkForAuthorizedAmendOrProcessedByBankMatch(userLocale) == true) //logu & amit
         {
            return true;
         }
      }
      catch (AmsException ex)
      {
         LOG.error("AmsException occurred in POLineItemData.checkForPOLineItemMatch(): ",ex);
      }
      catch (RemoteException ex)
      {
         LOG.error("RemoteException occurred in POLineItemData.checkForPOLineItemMatch(): " , ex);
      }

      return false;
   }

   /**
    * This method is used to determine whether or not the current PO line item
    * has the same PO number and item number as an unassigned PO line item in
    * the PO line item database table. If a match is found, the method will
    * call methods to validate the current PO line item data, update the
    * existing record's data if successful, and add a message to the Auto LC
    * log file.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not a PO line item match was found
    *                       (true  - a PO line item match was found, so stop processing the PO line item
    *                        false - a PO line item match was not found, so continue processing the PO line item)
    */
   private boolean checkForUnassignedPOLineItemMatch() throws AmsException, RemoteException
   {
      DocumentHandler   matchingPOLineItemsDoc = null;
      String      sqlQuery               = null;
      List<Object> sqlPrmsLst = new ArrayList();

      // Compose the SQL to retrieve PO Line Items that match the PO number and line item
      // number passed in and that are NOT currently assigned to a transaction
      sqlQuery = "select po_line_item_oid from po_line_item where source_type = ? and a_owner_org_oid = ? and po_num = ? ";

      sqlPrmsLst.add(TradePortalConstants.PO_SOURCE_UPLOAD);
      sqlPrmsLst.add(poUploadUserOrgOid);
      sqlPrmsLst.add(poNumber);
      
      if (itemNumber != null)
      {
         sqlQuery += " and item_num = ? ";
         sqlPrmsLst.add(itemNumber);
      }

      sqlQuery += " and a_assigned_to_trans_oid is null";

      // Retrieve any PO line items that satisfy the SQL that was just constructed
      matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

      // If a match was found, try to validate the current PO line item's data and
      // save it to the matching PO line item.
      if (matchingPOLineItemsDoc != null)
      {
         // If more than one record is found, log an error message and return
         if (matchingPOLineItemsDoc.getFragments("/ResultSetRecord").size() > 1)
         {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, poLineItemNumber);

            return true;
         }

         // Validate the current PO line item's data; if any field is invalid for
         // whatever reason, stop processing the PO line item and return
         if (validatePOLineItemData() == false)
         {
            return true;
         }

         // Update the existing PO line item's data with the data from the current
         // PO line item and log a message to the Auto LC log file
         savePOLineItemData(matchingPOLineItemsDoc);

         cleanupPOLineItemData("checkForUnassignedPOLineItemMatch");

         autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.LCLOG_LINE_ITEM_UPDATED, poLineItemNumber);

         return true;
      }

      return false;
   }

   /**
    * This method is used to determine whether or not the current PO line item
    * has the same PO number and item number as a PO line item assigned to a
    * transaction having a pending status (something other than Authorized, Deleted,
    * Cancelled By Bank, and Processed By Bank). If a match is found, the method
    * will validate the current PO line item's data. Next it will determine
    * whether or not several of the current PO line item's fields match the
    * corresponding fields in the matching PO line item. If the values are the
    * same, it will call methods to update the existing record's data, derive
    * several PO line item fields, set various transaction attributes, and add
    * a message to the Auto LC log file. If the values are not the same, the
    * method will unassign the matching PO line item from the transaction it's
    * assigned to, update the existing record's data, and add a message to the
    * Auto LC log file.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not a PO line item match was found
    *                       (true  - a PO line item match was found, so stop processing the PO line item
    *                        false - a PO line item match was not found, so continue processing the PO line item)
    */
   private boolean checkForAssignedPOLineItemMatch(String userLocale) throws AmsException, RemoteException //logu & amit
   {
      DocumentHandler   matchingPOLineItemsDoc = null;
      String      sqlQuery               = null;
      List<Object> sqlPrmsLst = new ArrayList();

      // Compose the SQL to retrieve PO Line Items that match the PO number and line item
      // number passed in and that are currently assigned to a transaction that has a
      // status other than Authorized,Partially Authorized,Rejected by Bank,
      // Processed By Bank, Cancelled By Bank, or Deleted
      // Only look for a match where the PO is active for the instrument
      sqlQuery = "select a.po_line_item_oid, a.p_shipment_oid from po_line_item a, transaction b where a.a_owner_org_oid = ? and a.source_type = ? and a.po_num = ? ";

      sqlPrmsLst.add(poUploadUserOrgOid);
      sqlPrmsLst.add(TradePortalConstants.PO_SOURCE_UPLOAD);
      sqlPrmsLst.add(poNumber);
      
      if (itemNumber != null)
      {
         sqlQuery += " and a.item_num = ? ";
         sqlPrmsLst.add(itemNumber);
      }

      sqlQuery += " and a.a_assigned_to_trans_oid = b.transaction_oid and a.a_active_for_instrument is not null and b.transaction_status not in (?,?,?,?,?,?)";

      sqlPrmsLst.add(TransactionStatus.AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.PROCESSED_BY_BANK);
      sqlPrmsLst.add(TransactionStatus.CANCELLED_BY_BANK);
      sqlPrmsLst.add(TransactionStatus.DELETED);
      sqlPrmsLst.add(TransactionStatus.PARTIALLY_AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.REJECTED_BY_BANK);
      
      // Retrieve any PO line items that satisfy the SQL that was just constructed
      matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

      // If a match was found, try to validate the current PO line item's data and
      // update the matching PO line item.
      if (matchingPOLineItemsDoc != null)
      {
         // If more than one record is found, log an error message and return
         if (matchingPOLineItemsDoc.getFragments("/ResultSetRecord").size() > 1)
         {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, poLineItemNumber);

            return true;
         }

         // Validate the current PO line item's data; if any field is invalid for
         // whatever reason, stop processing the PO line item and return
         if (validatePOLineItemData() == false)
         {
            return true;
         }

         // W Zhu 8/8/2007 NBUH013160710 Issue #2 Reorder the following code to derive transaction data
         // regardless whether poLineItemMatchFields = true or false.
         // It used to be done only if poLineItemMatchFields = true.

         // If the PO upload definition, beneficiary, and currency are the same for the
         // current PO line item and matching PO line item, update the existing PO line
         // item with the new data, derive several transaction, terms, and PO line item
         // fields, set several PO line item transaction attributes, and log a message
         // to the Auto LC log file; otherwise, unassign the PO line item, update it with
         // the new PO line item data, and log a message to the Auto LC log file.
         boolean poLineItemMatchFields = poLineItemFieldsMatchTransactionFields(matchingPOLineItemsDoc);
         if (poLineItemMatchFields)
         {
        	// Popoulate poLineItem with the uploaded data
            savePOLineItemData(false);

            // Save poLineItem
            // W Zhu 10/21/09 IRUJ050450231 Do not blank out previous_po_line_item_oid and auto_added_to_amend_ind
            // We are just updating PO information.
            //poLineItem.setAttribute("previous_po_line_item_oid", null);
            //poLineItem.setAttribute("auto_added_to_amend_ind",      TradePortalConstants.INDICATOR_NO);
            poLineItem.save();

            // Retrieve the transaction that the existing PO line item is currently assigned
            // to so that we can update attributes on it and access the terms associated to it
            getMatchingPOLineItemTransaction();
         }
         else
         {
             // W Zhu 11/3/09 LRUH060440487 begin
             // If the bene or currency or po definition is changed then ...
             // If this is a po auto-added to amendment, give error
             if (TradePortalConstants.INDICATOR_YES.equals(poLineItem.getAttribute("auto_added_to_amend_ind"))) {
                 getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);
                 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                         TradePortalConstants.LCLOG_LINE_ITEM_DATA_CHANGED,
                         getInstrumentSubstitutionValues(true));

                  cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");
                  return true;
             }  // LRUH060440487 end
             else {
                 // If this is a manually added PO, reset the matching PO line item so that it's no longer assigned to a
                 // transaction
                 // Note within unassignPOLineItem we first get the poLineItemTransaction before
                 // we clear the association on poLineItem.
                 unassignPOLineItem();

                 // Update the existing PO line item's data with the data from the current
                 // PO line item and log a message to the Auto LC log file
                 savePOLineItemData();
             }
         }

         // Derive transaction fields...
         Terms        terms      = null;

         terms = (Terms) poLineItemTransaction.getComponentHandle("CustomerEnteredTerms");
         ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
         int numShipments = shipmentTermsList.getObjectCount();

         Vector results   = matchingPOLineItemsDoc.getFragments("/ResultSetRecord");
         long shipmentTermsOid = ((DocumentHandler) results.elementAt(0)).getAttributeLong("P_SHIPMENT_OID");

         ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentTermsOid);

         // Get a list of the POs already associated with this instrument
         // Note we have updated the matching PO and saved to db.
         Vector shipmentPOsList = new Vector();
         String alreadyAssociatedSql = "select po_line_item_oid from po_line_item where p_shipment_oid = ?";
         DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{shipmentTermsOid});

         if (resultSet == null)  resultSet = new DocumentHandler();

         Vector alreadyAssociatedList = resultSet.getFragments("/ResultSetRecord");
         for (int i=0; i < alreadyAssociatedList.size(); i++)
         {
        	 String poOid = ((DocumentHandler) alreadyAssociatedList.elementAt(i)).getAttribute("/PO_LINE_ITEM_OID");
        	 shipmentPOsList.addElement(poOid);
         }

         // Derive transaction fields
         if(poLineItemTransaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND))
         {
         	// Instantiate the instrument so that we can perform a bunch of validations on it
         	// and save the new Amend transaction
         	long instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");
       		Instrument instrument = (Instrument) EJBObjectFactory.createServerEJB(sessionContext, "Instrument", instrumentOid);

       		// W Zhu 8/9/07 NBUH013160710 Isse #3 BEGIN Add new logic to get matchPreviousPODoc to replace matchingPOLineItemsDoc
       		// If all the POs on the amendment are new (i.e. do not exist on previous transaction),
       		// then will derive transaction amount by passing in non-null matchPreviousPODoc.
       		// If any PO on the amendment is not new, then do not derive transaction amount by
       		// passing in null matchPreviousPODoc.  We need to do this because this scenario
       		// requires calculating the differential amounts of the PO and the logic that
       		// derives transaction amount does not handle it.
       		// This is the same SQL as in checkForAuthorizedAmendOrProcessedByBankMatch().
       		sqlPrmsLst = new ArrayList();
       		sqlQuery = "select a.po_line_item_oid from po_line_item a, transaction b where a.a_owner_org_oid = ? and a.source_type = ? and a.po_num = ? ";

            sqlPrmsLst.add(poUploadUserOrgOid);
            sqlPrmsLst.add(TradePortalConstants.PO_SOURCE_UPLOAD);
            sqlPrmsLst.add(poNumber);
            
            if (itemNumber != null)
            {
               sqlQuery += " and a.item_num = ? ";
               sqlPrmsLst.add(itemNumber);
            }

            sqlQuery += " and a.a_assigned_to_trans_oid = b.transaction_oid and ((b.transaction_type_code = ? and b.transaction_status = ?) or (b.transaction_status = ?))";
            
            sqlPrmsLst.add(TransactionType.AMEND);
            sqlPrmsLst.add(TransactionStatus.AUTHORIZED);
            sqlPrmsLst.add(TransactionStatus.PROCESSED_BY_BANK);

            DocumentHandler matchPreviousPODoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

            POLineItemUtility.deriveAmendmentTransactionDataFromPOLocale(false,poLineItemTransaction, shipmentTerms,
            				numShipments, shipmentPOsList, 0, sessionContext, instrument,matchPreviousPODoc, userLocale);
            				//numShipments, shipmentPOsList, 0, sessionContext, instrument,matchingPOLineItemsDoc, userLocale);
       		// W Zhu 8/9/07 NBUH013160710 Isse #3 END

         }
         else
         {
        	 if (poLineItemMatchFields)
        	 {
        		 // Update the Import LC - Issue transaction's Beneficiary Name and Address
        		 updateTransactionBeneficiary(terms);

        		 // Update the Import LC - Issue transaction's Currency
        		 if (ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", poLineItemCurrency))
        		 {
            			poLineItemTransaction.setAttribute("copy_of_currency_code", poLineItemCurrency);
            			terms.setAttribute("amount_currency_code", poLineItemCurrency);
        		 }
        	 }
        	 // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
        	 // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
        	 // Note all the PO Line Items have been saved into database by now.
        	 POLineItemUtility.deriveTransactionDataFromPO(false,poLineItemTransaction, shipmentTerms, numShipments, shipmentPOsList, 0, sessionContext, userLocale);
         }

         poLineItemTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
         poLineItemTransaction.save(false);

         // Log the action we have taken.
         if (poLineItemMatchFields)
         {
        	 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_LINE_ITEM_UPDATED, poLineItemNumber);

         }
         else
         {
         	autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_LINE_ITEM_REMOVED,
                                       getInstrumentSubstitutionValues(true));
         }
         // W Zhu 8/8/2007 NBUH013160710 Issue #2 END

         cleanupPOLineItemData("checkForAssignedPOLineItemMatch");

         return true;
      }

      return false;
   	}

   /**
    * This method is used to determine whether or not the current PO line item
    * has the same PO number and item number as a PO line item assigned to an
    * Import LC - Issue transaction having a status of Authorized. If a match
    * is found, the method will retrieve the Issue transaction associated with
    * the matching PO line item and use it for logging an error message to the
    * Auto LC log file.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not a PO line item match was found
    *                       (true  - a PO line item match was found, so stop processing the PO line item
    *                        false - a PO line item match was not found, so continue processing the PO line item)
    */
   private boolean checkForAuthorizedIssuePOLineItemMatch() throws AmsException, RemoteException
   {
      DocumentHandler   matchingPOLineItemsDoc = null;
      String      sqlQuery               = null;
      List<Object> sqlPrmsLst = new ArrayList();

      // Compose the SQL to retrieve PO Line Items that match the PO number and line item
      // number passed in and that are currently assigned to an Issue transaction with a
      // status of Authorized,Partially Authorized or Rejected by Bank
      // Only look for a match where the PO is active for the instrument
      sqlQuery = "select a.po_line_item_oid,b.transaction_status from po_line_item a, transaction b where a.a_owner_org_oid = ? and a.source_type = ? and a.po_num = ? ";
      
      sqlPrmsLst.add(poUploadUserOrgOid);
      sqlPrmsLst.add(TradePortalConstants.PO_SOURCE_UPLOAD);
      sqlPrmsLst.add(poNumber);

      if (itemNumber != null)
      {
         sqlQuery += " and a.item_num = ? ";
         sqlPrmsLst.add(itemNumber);
      }

      sqlQuery += " and a.a_assigned_to_trans_oid = b.transaction_oid and a.a_active_for_instrument is not null and (b.transaction_type_code = ? ";

      //Krishna added IR 12/06/2007 PUH091837811 Begin
      sqlQuery += " and b.transaction_status in (?,?,?) or b.transaction_type_code = ? and b.transaction_status = ?)";

      sqlPrmsLst.add(TransactionType.ISSUE);
      sqlPrmsLst.add(TransactionStatus.AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.PARTIALLY_AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.REJECTED_BY_BANK);
      sqlPrmsLst.add(TransactionType.AMEND);
      sqlPrmsLst.add(TransactionStatus.REJECTED_BY_BANK);

      // Retrieve any PO line items that satisfy the SQL that was just constructed
      matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

      if (matchingPOLineItemsDoc != null)
      {
         // If more than one record is found, log an error message and return
         if (matchingPOLineItemsDoc.getFragments("/ResultSetRecord").size() > 1)
         {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, poLineItemNumber);

            return true;
         }

         // Retrieve the Issue transaction associated with the matching PO line
         // item and use it for logging an error message to the Auto LC log file
         getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);
         //Krishna added IR 12/06/2007 PUH091837811 Begin
         String transStatus=matchingPOLineItemsDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS");
         if(transStatus.equals(TransactionStatus.PARTIALLY_AUTHORIZED))
         {
        	 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                     TradePortalConstants.LCLOG_PART_AUTH_CANT_UPD_PO_DAT,getCompleteInstrumentId());
         }
         else if(transStatus.equals(TransactionStatus.REJECTED_BY_BANK))
         {
        	 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                     TradePortalConstants.LCLOG_REJ_BY_BANK_CANT_UPD_PO_DAT,getCompleteInstrumentId());
         }
         //Krishna added IR 12/06/2007 PUH091837811 End
         else
         {
         autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.LCLOG_CANT_UPDATE_PO_DATA,
                                    getInstrumentSubstitutionValues(false));
         }

         cleanupPOLineItemData("checkForAuthorizedIssuePOLineItemMatch");

         return true;
      }

      return false;
   }

   /**
    * This method is used to determine whether or not the current PO line item
    * has the same PO number and item number as a PO line item assigned to an
    * Amend transaction having a status of Authorized or a transaction having a
    * status of Processed By Bank. If a match is found, the method will
    * validate the current PO line item's data. If validation is successful,
    * it will then call a method to create a new Import LC - Amend transaction.
    * Next, if the Amend transaction is created successfully, it will call
    * methods to update the existing record's data and add a message to the
    * Auto LC log file.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not a PO line item match was found
    *                       (true  - a PO line item match was found, so stop processing the PO line item
    *                        false - a PO line item match was not found, so continue processing the PO line item)
    */
   private boolean checkForAuthorizedAmendOrProcessedByBankMatch(String userLocale) throws AmsException, RemoteException //logu & amit
   {
      DocumentHandler   matchingPOLineItemsDoc = null;
      String      sqlQuery               = null;
      List<Object> sqlPrmsLst = new ArrayList();

      // Compose the SQL to retrieve PO Line Items that match the PO number and line item
      // number passed in and that are currently assigned to either an Amend transaction
      // with a status of Authorized or any transaction with a status of Processed By Bank
      // Only look for a match where the PO is active for the instrument
      // Order by transaction_status_date and transaction_oid descendingly so that the po
      // line item belonging to the latest transaction comes up first.
      sqlQuery = "select a.po_line_item_oid, a.a_active_for_instrument, b.transaction_status  "
      		+ "from po_line_item a, transaction b where a.a_owner_org_oid = ? "
      		+ "and a.source_type = ? and a.po_num = ? ";

      sqlPrmsLst.add(poUploadUserOrgOid);
      sqlPrmsLst.add(TradePortalConstants.PO_SOURCE_UPLOAD);
      sqlPrmsLst.add(poNumber);
      
      if (itemNumber != null)
      {
         sqlQuery += " and a.item_num = ? ";
         sqlPrmsLst.add(itemNumber);
      }

      sqlQuery += " and a.a_assigned_to_trans_oid = b.transaction_oid "
      		+ "and a.a_active_for_instrument is not null "
      		+ "and ((b.transaction_type_code = ? and b.transaction_status = ? or b.transaction_status = ?) "
      		+ "or (b.transaction_status = ?))";
      
      sqlPrmsLst.add(TransactionType.AMEND);
      sqlPrmsLst.add(TransactionStatus.AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.PARTIALLY_AUTHORIZED);
      sqlPrmsLst.add(TransactionStatus.PROCESSED_BY_BANK);

      // Retrieve any PO line items that satisfy the SQL that was just constructed
      matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

      if (matchingPOLineItemsDoc != null)
      {
         // If more than one record is found, log an error message and return
         if (matchingPOLineItemsDoc.getFragments("/ResultSetRecord").size() > 1)
         {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, poLineItemNumber);

            return true;
         }

         // Validate the current PO line item's data; if any field is invalid for
         // whatever reason, stop processing the PO line item and return
         if (validatePOLineItemData() == false)
         {
            return true;
         }

         // W Zhu 8/14/07 NBUH013160710 Issue #4 BEGIN
         // Checking matching fields.  If the key fields (po definition oid,
         // beneficiary, currency) are changed, give an error and do not upload.
         if (!poLineItemFieldsMatchTransactionFields(matchingPOLineItemsDoc))
         {
        	getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);

             autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                    TradePortalConstants.LCLOG_LINE_ITEM_DATA_CHANGED,
                    getInstrumentSubstitutionValues(true));

             cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

             return true;
         }
         // W Zhu 8/14/07 NBUH013160710 Issue #4 END


         // W Zhu 5/22/07 CR-393 BEGIN
         // Check whether there is pending amendment for the instrument.
         // If there is pending amendment, then add PO to amendment, otherwise create a new amendment.
         // If multiple POs are amended, the first amended PO may create a new amendment
         // or be appended to an existing amendment.  The following amended PO will be appended
         // to the new amendment or appended to the same existing amendment.
         String instrumentOid = matchingPOLineItemsDoc.getAttribute("/ResultSetRecord(0)/A_ACTIVE_FOR_INSTRUMENT");
         long pendingAmendmentOid = getPendingAmendmentTransaction(instrumentOid);

         // Attempt to create or append to an Import LC - Amend transaction; if an error occurs
         // during this process, stop processing the PO line item and return
         // logu & amit
         if (createOrAppendAmendmentTransaction(matchingPOLineItemsDoc,pendingAmendmentOid, userLocale) == false)
         // W Zhu 5/22/07 CR-393 END
         {
            cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

            return true;
         }

         // After POs have been created and the PO updated with the proper data,
         // save it.
         poLineItem.save();

         // If a copy of the PO line item has been created, save it too
         if(copyOfPoLineItem != null)
           copyOfPoLineItem.save();

         // W Zhu 5/22/07 CR-393 BEGIN
         // Log the creation of amendment or appending PO to existing amendment.
         if (pendingAmendmentOid != 0)
         {
        	 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
        		 TradePortalConstants.LCLOG_ADDED_TO_AMENDMENT,
                 poLineItemNumber, getCompleteInstrumentId());
         }
         else
         {
             // W Zhu 5/22/07 CR-393 END
        	 autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.LCLOG_AMENDMENT_STARTED,
                                    getCompleteInstrumentId(), poLineItemNumber);
         }

         cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

         return true;
      }

      return false;
   }


   /**
    * This method is used to validate the data in each of the PO line item
    * fields. If validation fails for whatever reason for any field, the
    * method will stop processing the current PO line item and continue with
    * the next one.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not validation was successful for all of the current PO line item fields
    *                       (true  - validation was successful for all of the current PO line item fields
    *                        false - validation was not successful for all of the current PO line item fields)
    */
   public boolean validatePOLineItemData() throws AmsException
   {
      POLineItemField   poLineItemField  = null;

      for (int i = 0; i < numberOfPOLineItemFields; i++)
      {
         poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(i);

         if (validatePOLineItemField(poLineItemField) == false)
         {
            return false;
         }
      }

      return true;
   }

   /**
    * This method is used to validate the data in a single PO line item field.
    * If any validation fails for whatever reason for the field, the method
    * will stop processing the current PO line item and continue with the next
    * one. It calls validation methods that will validate whether a required
    * field is present, whether the field's data type is valid, and whether
    * the field's size is valid.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not validation was successful for the current PO line item field
    *                       (true  - validation was successful for the current PO line item field
    *                        false - validation was not successful for the current PO line item field)
    */
   private boolean validatePOLineItemField(POLineItemField poLineItemField) throws AmsException
   {
      boolean   isRequired    = false;
      String    fieldDataType = null;
      String    fieldValue    = null;
      String    fieldType     = null;
      int       fieldSize     = 0;

      // First, retrieve all PO line item field attributes necessary for validation
      fieldDataType = poLineItemField.getFieldDataType();
      fieldValue    = poLineItemField.getFieldValue();
      fieldType     = poLineItemField.getFieldType();
      fieldSize     = poLineItemField.getFieldSize();

      // Next, determine whether or not the field is required
      isRequired = getRequiredFieldStatus(fieldType);

      // Validate the field's required status; if the field is required but not present
      // (or empty), stop processing the PO line item and continue with the next one
      if (validateRequiredStatus(poLineItemField, isRequired, fieldValue) == false)
      {
         return false;
      }


      // GGAYLE - DFUH031368817 - 04/03/2007
      // Validate the field for invalid Swift characters.  If any are found
      // stop processing the PO line item and continue with the next one.
      if (validateValidSwiftCharacters(poLineItemField, fieldValue) == false)
      {
         return false;
      }
      // GGAYLE - DFUH031368817 - 04/03/2007


      // Validate the field's data type; if the field's data type is invalid, stop
      // processing the PO line item and continue with the next one
      if (validateDataType(poLineItemField, isRequired, fieldValue, fieldDataType) == false)
      {
         return false;
      }

      // Validate the field's size; if the field's size is too big, stop processing
      // the PO line item and continue with the next one
      if (validateSize(poLineItemField, isRequired, fieldValue, fieldDataType, fieldSize) == false)
      {
         return false;
      }

      return true;
   }

   /**
    * This method is used to return the required status for a PO line item
    * field. Only four PO line item fields are required in every PO line
    * item: PO Number, Beneficiary, Currency, and Amount.
    * Different fields will be required depending on the PO source type (upload or manual)
    *

    * @param      java.lang.String fieldType - the field type of the current PO line item field being validated
    * @return     boolean - indicates whether or not the current PO line item field is required
    *                       (true  - the current PO line item field is required
    *                        false - the current PO line item field is not required)
    */
   private boolean getRequiredFieldStatus(String fieldType)
   {
      if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
      {
        if ((fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_AMOUNT)) ||
            (fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_BENEFICIARY_NAME)) ||
            (fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_CURRENCY)) ||
            (fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_PO_NUMBER)))
        {
          return true;
        }
      }
      else
      {
        if (fieldType.equals(TradePortalConstants.PO_FIELD_TYPE_PO_NUMBER))
        {
          return true;
        }
      }
      return false;
   }

   /**
    * This method is used to validate whether or not a required field is
    * present. If a field is required but not present (or empty), the method
    * will issue an error message to the Auto LC log file and cause all
    * validation to fail for the current PO line item.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not validation of the current PO line item field's required status
    *                       was successful
    *                       (true  - the PO line item field is either not required or it's required and contains data
    *                        false - the PO line item field is required but does not contain data)
    */
   private boolean validateRequiredStatus(POLineItemField poLineItemField, boolean isRequired, String fieldValue)
                                          throws AmsException
   {
      if ((fieldValue == null) && (isRequired))
      {
         autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.LCLOG_DATA_MISSING, poLineItemField.getFieldName(),
                                    poLineItemNumber);

         return false;
      }

      return true;
   }



   // GGAYLE - DFUH031368817 - 04/03/2007
   /**
    * This method is used to validate the PO field data for invalid Swift characters.
    * If any are found, the method will issue an error message to the Auto LC log file
    * or to the error manager object.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not validation of the current PO line item field's data
    *                       was successful
    *                       (true  - the PO line item field does not contain any invalid Swift characters
    *                        false - the PO line item field does contain at least one invalid Swift character
    */
   private boolean validateValidSwiftCharacters(POLineItemField poLineItemField, String fieldValue)
                                                throws AmsException
   {
      String listOfInvalidCharacters = null;

      if (!StringFunction.isBlank(fieldValue))
      {
        listOfInvalidCharacters = InstrumentServices.checkAutoLCForInvalidSwiftCharacters(fieldValue);
        //Krishna IR  PNUH111961134 12/07/2007 Begin
        //fieldDataValue data of Date could be in "12/04/2007 00:00:00" JPylon format
        //As,":" is a SWIFT character DateValue is not checked for ":" inorder not to get the SWIFT Invalid character error.
        String fieldDataType = poLineItemField.getFieldDataType();
        String invalidCharsExclusiveOfDate="";
        if(fieldDataType.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE))
        {
        	if(!StringFunction.isBlank(listOfInvalidCharacters))
        	{
        		StringTokenizer invalidCharTokens=new StringTokenizer(listOfInvalidCharacters,":");
        	    while(invalidCharTokens.hasMoreTokens())
        	    {
        	     //Fetching all other Invalid SWIFT Chars apart from ":"
        	     invalidCharsExclusiveOfDate=invalidCharsExclusiveOfDate+invalidCharTokens.nextToken();
        	    }
        	}
        	listOfInvalidCharacters=invalidCharsExclusiveOfDate;
        }
        //Krishna IR  PNUH111961134 12/07/2007 End

        if (!StringFunction.isBlank(listOfInvalidCharacters))
        {
          // Log the error message
          if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
          {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                      TradePortalConstants.LCLOG_INVALID_SWIFT_CHARS_3PARM, poLineItemField.getFieldName(),
                                        poLineItemNumber, listOfInvalidCharacters);
          }
          else
          {
            errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LCLOG_INVALID_SWIFT_CHARS_3PARM,
                                poLineItemField.getFieldName(), poLineItemNumber, listOfInvalidCharacters);
          }
          return false;
        }
      }

      return true;
   }
   // GGAYLE - DFUH031368817 - 04/03/2007



   /**
    * This method is used to call methods to validate whether or not the value
    * for the current PO line item field is in the correct format. If a field's
    * value is not in the correct format, the method will cause all validation
    * to fail for the current PO line item. The field's data type is specified
    * in the PO upload definition and can be one of three values: Date, Number,
    * and Text.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @param      java.lang.String fieldDataType - the data type of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the current PO line item field's value is in the correct format
    *                       (true  - the PO line item field is in the correct format
    *                        false - the PO line item field is not in the correct format)
    */
   private boolean validateDataType(POLineItemField poLineItemField, boolean isRequired, String fieldValue,
                                    String fieldDataType) throws AmsException
   {
      if (fieldDataType.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE))
      {
         if (validateDateFormat(poLineItemField, fieldValue, isRequired) == false)
         {
            return false;
         }
      }
      else if (fieldDataType.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_NUMBER))
      {
         if (validateNumberFormat(poLineItemField, fieldValue, isRequired) == false)
         {
            return false;
         }
      }
      else
      {
         if (validateTextFormat(poLineItemField, fieldValue, isRequired) == false)
         {
            return false;
         }
      }

      return true;
   }

   /**
    * This method is used to validate whether or not the date value for the
    * current PO line item field is in the correct format. If the date value
    * is not in the correct format, the method will add an error message to the
    * Auto LC log file and cause all validation to fail for the current PO line
    * item. The correct date format is currently set to 'dd/MM/yyyy'; this must
    * be converted, however, to jPylon format so that it can be saved
    * successfully.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the current PO line item field's value is in the correct date format
    *                       (true  - the PO line item field is in the correct date format
    *                        false - the PO line item field is not in the correct date format)
    */
   private boolean validateDateFormat(POLineItemField poLineItemField, String fieldValue, boolean isRequired)
                                      throws AmsException
   {
      SimpleDateFormat   dateFormatter   = null;
      StringBuffer       convertedDate   = null;
      boolean            invalidDateFlag = false;
      String             month           = null;
      String             year            = null;
      String             day             = null;
      Date               date            = null;

      // If the date is already in jPylon format, it has already been validated and passed
      // Don't do any further validation.
      if(fieldValue.endsWith(" 00:00:00"))
        return true;

      // If this is a manually entered PO Line Item, do not validate the date if its empty
      if (StringFunction.isBlank(fieldValue) && sourceType.equals(TradePortalConstants.PO_SOURCE_MANUAL))
      {
        return true;
      }

      // First, determine whether the date has the same length as "dd/mm/yyyy" (i.e., 10 chars).
      // If it doesn't log an error and return; otherwise, keep validating
      if (fieldValue.length() != TPDateTimeUtility.NUMBER_OF_EURO_DATE_CHARS)
      {
         invalidDateFlag = true;
      }

      if (!invalidDateFlag)
      {
         // Set up the date formatter to parse the date
         dateFormatter = new SimpleDateFormat(poDateFormat);

         try
         {
            // Try to convert the date as a String to a Java Date
            date = dateFormatter.parse(fieldValue);
         }
         catch (ParseException e)
         {
            invalidDateFlag = true;
         }

         if (!invalidDateFlag)
         {
            // If the date was in the correct format, convert to a jPylon date (i.e., mm/dd/yyyy)
            // and make sure a valid date was entered (Note: '-1' check is because method returns
            // true if all date components are set to this value).
            if (poDateFormat.equals(TradePortalConstants.PO_FORMATTED_EURO_DATE))
            {
              month = fieldValue.substring(3, 5);
              year  = fieldValue.substring(6);
              day   = fieldValue.substring(0, 2);
            }
            else
            {
              day   = fieldValue.substring(3, 5);
              year  = fieldValue.substring(6);
              month = fieldValue.substring(0, 2);
            }

            if ((TPDateTimeUtility.isGoodDate(year, month, day)) && (!day.equals("-1")) && (!month.equals("-1")) &&
                (!year.equals("-1")))
            {
               convertedDate = new StringBuffer();
               convertedDate.append(month);
               convertedDate.append("/");
               convertedDate.append(day);
               convertedDate.append("/");
               convertedDate.append(year);
               convertedDate.append(" 00:00:00");

               poLineItemField.setFieldValue(convertedDate.toString());
               if (poLineItemField.getFieldType().equals(TradePortalConstants.PO_FIELD_TYPE_LAST_SHIPMENT_DATE))
               {
                 lastShipDate = convertedDate.toString();
               }
            }
            else
            {
               invalidDateFlag = true;
            }
         }
      }

      // If the date value is not in the correct format, add an error message to the
      // Auto LC log file and return false to stop further validation
      if (invalidDateFlag)
      {
         if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
         {
           autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                      TradePortalConstants.LCLOG_INVALID_DATE_FORMAT, poLineItemField.getFieldName(),
                                      poLineItemNumber);
         }
         else
         {
           errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LCLOG_INVALID_DATE_FORMAT,
                               poLineItemField.getFieldName(), poLineItemNumber);
         }

         return false;
      }

      return true;
   }

   /**
    * This method is used to validate whether or not the number value for the
    * current PO line item field is in the correct format. If the number value
    * is not in the correct format, the method will add an error message to the
    * Auto LC log file and cause all validation to fail for the current PO line
    * item.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the current PO line item field's value is in the correct number
    *                       format
    *                       (true  - the PO line item field is in the correct number format
    *                        false - the PO line item field is not in the correct number format)
    */
   private boolean validateNumberFormat(POLineItemField poLineItemField, String fieldValue, boolean isRequired)
                                        throws AmsException
   {
      BigDecimal   value = null;

      // If this is a manually entered PO Line Item, do not validate the date if its empty
      if (StringFunction.isBlank(fieldValue) && sourceType.equals(TradePortalConstants.PO_SOURCE_MANUAL))
      {
        return true;
      }

      // Try creating a BigDecimal object with the current PO line item field value;
      // this handles both integer and decimal values which is what we need
      try
      {
         value = new BigDecimal(fieldValue);
      }
      catch (NumberFormatException ex)
      {
         if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
         {
           autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                      TradePortalConstants.LCLOG_INVALID_NUMBER_FORMAT, poLineItemField.getFieldName(),
                                      poLineItemNumber);
         }
         else
         {
           errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LCLOG_INVALID_NUMBER_FORMAT,
                               poLineItemField.getFieldName(), poLineItemNumber);
         }

         return false;
      }

      return true;
   }

   /**
    * This method is used to validate whether or not the text value for the
    * current PO line item field is in the correct format. It only exists for
    * ensuring that required fields have non-whitespace data present. If the
    * field being validated is a required field but doesn't contain any
    * non-whitespace data, the method will add an error message to the Auto LC
    * log file and cause all validation to fail for the current PO line item.
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the current PO line item field's value is in the correct text format
    *                       (true  - the PO line item field is in the correct text format
    *                        false - the PO line item field is not in the correct text format)
    */
   private boolean validateTextFormat(POLineItemField poLineItemField, String fieldValue, boolean isRequired)
                                      throws AmsException
   {
      if ((!isRequired) || (fieldValue.trim().length() > 0))
      {
         return true;
      }

      if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
      {
        autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber, TradePortalConstants.LCLOG_DATA_MISSING,
                                   poLineItemField.getFieldName(), poLineItemNumber);
      }
      else
      {
        errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LCLOG_DATA_MISSING,
                            poLineItemField.getFieldName(), poLineItemNumber);
      }

      return false;
   }

   /**
    * This method is used to validate the size of the current PO line item
    * field's value. If the size is larger than the size specified in the PO
    * upload definition, the method will add an error message to the Auto LC
    * log file and cause all validation to fail for the current PO line item.
    * (Note: size validation is already enforced for dates in the validate
    * date format method, which is why we simply return true for their case).
    *

    * @param      com.ams.tradeportal.busobj.util.POLineItemField poLineItemField - the current PO line item field being
    *                                                                          validated
    * @param      boolean isRequired - indicator specifying whether or not the current PO line item field is required
    * @param      java.lang.String fieldValue - the value of the current PO line item field being validated
    * @param      java.lang.String fieldDataType - the data type of the current PO line item field being validated
    * @param      int fieldSize - the maximum number of characters allowed for the current PO line item field's value
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @return     boolean - indicates whether or not the size of the current PO line item field's value is less than or
    *                       equal to the size specified in the PO upload definition for the field
    *                       (true  - the PO line item field is valid
    *                        false - the PO line item field is too large)
    */
   private boolean validateSize(POLineItemField poLineItemField, boolean isRequired, String fieldValue,
                                String fieldDataType, int fieldSize) throws AmsException
   {
      // If we're dealing with a date field here, return; date sizes have already been validated
      if (fieldDataType.equals(TradePortalConstants.PO_FIELD_DATA_TYPE_DATE))
      {
         return true;
      }

      // If the value excluding whitespace is greater than the max size for the field specified
      // in the PO upload definition, log an error and stop processing this PO line item
      if (fieldValue.trim().length() > fieldSize)
      {
         if (sourceType.equals(TradePortalConstants.PO_SOURCE_UPLOAD))
         {
           autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                      TradePortalConstants.LCLOG_VALUE_TOO_BIG, poLineItemField.getFieldName(),
                                      poLineItemNumber);
         }
         else
         {
           errorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.LCLOG_VALUE_TOO_BIG,
                               poLineItemField.getFieldName(), poLineItemNumber);
         }
         return false;
      }

      return true;
   }

  /**
   * This method is used to validate that the po line item's currency is the same
   * as the transaction's currency and that the currency is the same for all PO
   * Line Items
   *
   * @param poLineItemCurrency String
   * @param transactionCurrency String
   * @param prevPOLineItemCurrency String
   * @param transactionOid String
   * @param shipmentOid String
   * @return boolean
   */

   public boolean validatePOLineItemCurrency(String poLineItemCurrency, String prevPOLineItemCurrency,
               String transactionCurrency, String transactionOid,
                                             String shipmentOid) throws AmsException, RemoteException
   {
     boolean         errorsFound         = false;
     String          currentPOCurrency   = null;
     String    sqlQuery            = null;
     DocumentHandler poLineItemsDoc      = null;

     // Validate that the PO Line Item's currency is the same as the transaction's
     // currency.
     if ((!StringFunction.isBlank(transactionCurrency)) &&
         (!poLineItemCurrency.equals(transactionCurrency)))
     {
       errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
                                                     TradePortalConstants.PO_ITEM_CURR_NOT_SAME_AS_TRANS);
       errorsFound = true;
     }

     // Validate that the currency is the same for all PO Line Items.
     if (!poLineItemCurrency.equals(prevPOLineItemCurrency))
     {
       errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
                                                     TradePortalConstants.PO_LINE_ITEM_CURR_NOT_SAME);
       errorsFound = true;
     }

     // Validate that the PO Line Item's currency is the same as existing PO Line Item's already
     // assigned to another shipment of the same transaction
     sqlQuery = "select currency from po_line_item where a_assigned_to_trans_oid = ? and p_shipment_oid <> ?";

     poLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, transactionOid, shipmentOid);
     if (poLineItemsDoc != null)
     {
       currentPOCurrency = poLineItemsDoc.getAttribute("/ResultSetRecord(0)/CURRENCY");
       if (!poLineItemCurrency.equals(currentPOCurrency))
       {
         errorMgr.issueError(TradePortalConstants.ERR_CAT_1,
                                                       TradePortalConstants.PO_CURR_NOT_SAME_ACROSS_SHIPS);
         errorsFound = true;
       }
     }
     return errorsFound;
   }

   /**
    * This method is used as a wrapper to call the main savePOLineItemData
    * method, which takes a boolean save flag as a parameter.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   public void savePOLineItemData() throws AmsException, RemoteException
   {
      savePOLineItemData(true);
   }

   /**
    * This method is used to create a new PO line item and save its data or
    * update an existing PO line item with new data. A boolean flag passed in
    * determines whether or not the save method will be called for the PO line
    * item business object. If this parameter is set to false, the save method
    * must be called elsewhere for all the data set in this method to be saved.
    *

    * @param      java.lang.String saveFlag - indicator of whether or not to save the PO line item in this method
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   public void savePOLineItemData(boolean saveFlag) throws AmsException, RemoteException
   {

      POLineItemField   poLineItemField               = null;
      String[]          attributeValues               = null;
      String[]          attributePaths                = null;
      boolean           isPOLineItemNew               = false;
      String            previousPOUploadDefinitionOid = null;

      // If a PO line item match hasn't already been found, create a new PO line item object
      if (poLineItem == null)
      {
         poLineItem = (POLineItem) EJBObjectFactory.createServerEJB(sessionContext, "POLineItem");
         poLineItem.newObject();
         isPOLineItemNew = true;
      }

      // Set the data for each PO line item field
      for (int i = 0; i < numberOfPOLineItemFields; i++)
      {
         poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(i);
         poLineItem.setAttribute(poLineItemField.getFieldType(), poLineItemField.getFieldValue());
      }

      // If we're dealing with an existing PO line item, get its PO upload definition OID and
      // compare it to the PO upload definition OID that was selected for the current PO
      // upload. If they aren't the same, it's possible that some fields that previously
      // existed in the old definition no longer exist in the new one; in this case, we need
      // to clear their values so that we don't have bad data.
      if (!isPOLineItemNew)
      {
         previousPOUploadDefinitionOid = poLineItem.getAttribute("source_upload_definition_oid");

         if (!previousPOUploadDefinitionOid.equals(String.valueOf(poUploadDefinitionOid)))
         {
            // Clear all fields that aren't part of the current PO Upload definition in case we're
            // updating an existing PO line item and the definition has changed
            if (numberOfPOLineItemFields >= NUMBER_OF_STANDARD_PO_FIELDS)
            {
               for (int j = (numberOfPOLineItemFields - NUMBER_OF_STANDARD_PO_FIELDS + 1);
                        j <= MAX_NUMBER_OF_PO_FIELDS - NUMBER_OF_STANDARD_PO_FIELDS; j++)
               {
                  poLineItem.setAttribute(TradePortalConstants.PO_FIELD_TYPE_OTHER + j, null);
               }
            }

            // Clear the Last Shipment Date field and Item Number field if they were removed from
            // the PO upload definition and we're updating an exisitng PO line item
            if (poLineItemLastShipmentDate == null)
            {
               poLineItem.setAttribute("last_ship_dt", null);
            }

            if (itemNumber == null)
            {
               poLineItem.setAttribute("item_num", null);
            }
         }
      }

      // Update all non-PO line item data values in the object
      attributeValues = new String[5];
      attributePaths  = new String[5];

      attributePaths[0]  = "source_upload_definition_oid";
      attributePaths[1]  = "upload_sequence_num";
      attributePaths[2]  = "creation_date_time";
      attributePaths[3]  = "owner_org_oid";
      attributePaths[4]  = "source_type";

      attributeValues[0] = String.valueOf(poUploadDefinitionOid);
      attributeValues[1] = poUploadSequenceNumber;
      attributeValues[2] = DateTimeUtility.getGMTDateTime();
      attributeValues[3] = poUploadUserOrgOid;
      attributeValues[4] = sourceType;

      poLineItem.setAttributes(attributePaths, attributeValues);



      // If the save flag is true, save the PO line item; otherwise, this save call needs to be
      // made elsewhere (somewhere after this method call)
      if (saveFlag)
      {
         poLineItem.save();

         // If a copy of the PO line item has been created, save it too
         if(copyOfPoLineItem != null)
            copyOfPoLineItem.save();
      }
   }

   /**
    * This method is used as a wrapper to call the main savePOLineItemData
    * method, which takes a boolean save flag as a parameter. Unlike the
    * wrapper method that doesn't take any parameters, this method receives an
    * xml document that contains the OID of the PO line item that the current
    * PO line item matched. It passes this xml document to a method to
    * instantiate the existing PO line item before the main savePOLineItemData
    * is called to update it.
    *

    * @param      com.amsinc.ecsg.util.DocumentHandler matchingPOLineItemsDoc - xml document containing the OID that
    *                                                                           uniquely identifies the PO line item that
    *                                                                           the current PO line item matched
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void savePOLineItemData(DocumentHandler matchingPOLineItemsDoc) throws AmsException, RemoteException
   {
      getMatchingPOLineItem(matchingPOLineItemsDoc);

      savePOLineItemData(true);
   }

   /**
    * This method is used to determine whether or not the PO upload definition
    * OID, beneficiary name, and currency for the current PO line item are the
    * same as the existing (i.e., matching) PO line item's fields. It will
    * initially call a method to instantiate the PO line item that was matched,
    * so its data can be retrieved for comparison.
    *

    * @param      com.amsinc.ecsg.util.DocumentHandler matchingPOLineItemsDoc - xml document containing the OID that
    *                                                                           uniquely identifies the PO line item that
    *                                                                           the current PO line item matched
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not the current PO line item fields are the same as the existing PO line
    *                       item fields
    *                       (true  - the PO line item fields are the same
    *                        false - the PO line item fields are not the same)
    */
   private boolean poLineItemFieldsMatchTransactionFields(DocumentHandler matchingPOLineItemsDoc)
                                                          throws AmsException, RemoteException
   {
      String   transactionBeneficiaryName       = null;
      String   transactionCurrency              = null;
      long     transactionPOUploadDefinitionOid = 0;

      getMatchingPOLineItem(matchingPOLineItemsDoc);

      transactionPOUploadDefinitionOid = poLineItem.getAttributeLong("source_upload_definition_oid");
      transactionBeneficiaryName       = poLineItem.getAttribute("ben_name");
      transactionCurrency              = poLineItem.getAttribute("currency");

      if ((poUploadDefinitionOid == transactionPOUploadDefinitionOid) &&
          (poLineItemBeneficiaryName.equals(transactionBeneficiaryName)) &&
          (poLineItemCurrency.equals(transactionCurrency)))
      {
         return true;
      }

      return false;
   }


   /**
    * This method is used to update the beneficiary for the transaction that
    * an existing (i.e., matching) PO line item is assigned to. It runs a SQL
    * query to determine if a party exists that matches the beneficiary name
    * currently set in the PO line item. If a match is found, the first terms
    * party associated to the transaction is updated with the beneficiary
    * party info. If the party has a designated bank, the method will update
    * the third terms party as well.
    *

    * @param      com.ams.tradeportal.busobj.terms terms - the terms associated to the transaction that the existing PO
    *                                                      line item is assigned to
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void updateTransactionBeneficiary(Terms terms) throws AmsException, RemoteException
   {
      DocumentHandler   resultSet         = null;
      String      sqlQuery          = null;
      TermsParty        termsParty        = null;
      String            designatedBankOid = null;
      Party             party             = null;
      long              partyOid          = 0;

      // Compose the SQL to retrieve the party that matches the beneficiary name currently
      // set in the PO line item and its designated bank (if one exists)
      sqlQuery = "select party_oid, a_designated_bank_oid from party where p_owner_org_oid = ? and name = ?";

      // Retrieve any parties that satisfy the SQL that was just constructed
      resultSet = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, poUploadUserOrgOid, poLineItemBeneficiaryName);

      // If a party was found, instantiate it and update the first terms party for the transaction
      // associated to the PO line item; otherwise, do nothing and return
      if (resultSet != null)
      {
         partyOid = resultSet.getAttributeLong("/ResultSetRecord(0)/PARTY_OID");

         party = (Party) EJBObjectFactory.createServerEJB(sessionContext, "Party", partyOid);

         termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

         termsParty.copyFromParty(party);

         // If the party found has a designated bank, update the third terms party as well
         designatedBankOid = resultSet.getAttribute("/ResultSetRecord(0)/A_DESIGNATED_BANK_OID");

         if ((designatedBankOid != null) && (!designatedBankOid.equals("")))
         {
            party = (Party) EJBObjectFactory.createServerEJB(sessionContext, "Party",
                                                             Long.parseLong(designatedBankOid));

            if(StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty")))
                 {
                      terms.newComponent("ThirdTermsParty");
                      termsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
                 }
            else
                termsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

            termsParty.copyFromParty(party);
         }

         removeParty(party, "updateTransactionBeneficiary");
      }
   }






  /**
   * Creates a copy of the passed-in purchase order.   The copy is identical except
   * for the OID.
   *
   * @param po - the PO line item that is being copied
   * @return a copy of the PO line item with all fields the same except for the OID
   *
   */
  private POLineItem createCopy (POLineItem po) throws AmsException, RemoteException
   {
      // Dump data to XML first, then use that to populate the new object
      DocumentHandler xml = new DocumentHandler();
      xml = po.populateXmlDoc(xml);

      POLineItem copy = (POLineItem) EJBObjectFactory.createServerEJB(sessionContext, "POLineItem");
      copy.newObject();
      copy.populateFromXmlDoc(xml);

      return copy;
   }


  // W Zhu 5/23/07 CR-393 BEGIN
  /**
   * This method is used to get pending amendment for an instrument
   * If there are multiple amendment, use the one created earliest.
   *

   * @param      String instrumentOid
   * @exception  com.amsinc.ecsg.frame.AmsException
   * @exception  java.rmi.RemoteException
   * @return     long - Pending Amendment transaction Oid.
   *                0 if none if found.
   */
  private long getPendingAmendmentTransaction(String instrumentOid)
                                                        throws AmsException
  {
	  long pendingAmendmentOid = 0;
	  DocumentHandler pendingAmdTransactionsDoc = null;
	  String sqlQuery  = "select transaction_oid from transaction "
	  		+ "where p_instrument_oid = ? AND transaction_status not in (?,?,?,?,?,?) "
	  		+ "AND transaction.transaction_type_code = 'AMD' order by date_started";

	  Object [] sqlPrmsLst =  new Object[7];
	  sqlPrmsLst[0] = instrumentOid;
	  sqlPrmsLst[1] = TransactionStatus.CANCELLED_BY_BANK;
	  sqlPrmsLst[2] = TransactionStatus.PROCESSED_BY_BANK;
	  sqlPrmsLst[3] = TransactionStatus.DELETED;
	  sqlPrmsLst[4] = TransactionStatus.AUTHORIZED;
	  sqlPrmsLst[5] = TransactionStatus.PARTIALLY_AUTHORIZED;
	  sqlPrmsLst[6] = TransactionStatus.REJECTED_BY_BANK;
	  
	  pendingAmdTransactionsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlPrmsLst);

	  if (pendingAmdTransactionsDoc != null)
	  {
		  // If more than one record is found, log an error message and return
	      if (pendingAmdTransactionsDoc.getFragments("/ResultSetRecord").size() > 0)
	      {
	    	  pendingAmendmentOid = Long.parseLong(pendingAmdTransactionsDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_OID"));
	       }
	  }
	  return pendingAmendmentOid;
  }
  // W Zhu 5/23/07 CR-393 END


  // W Zhu 5/23/07 CR-393 add parameter pendingAmendmentOid
   /**
    * This method is used to create a new Import LC - Amend transaction for the
    * PO line item currently being processed, or to append the PO to an existing
    * and pending Amendment. It is only called when a PO line
    * item matches an existing PO line item that belong to an Authorized Amendment
    * or some other Processed transaction.  If a new Amend transaction is created,
    * it is created in the same way a normal Import LC - Amend transaction is created.
    *

    * @param      com.amsinc.ecsg.util.DocumentHandler matchingPOLineItemsDoc - xml document containing the OID that
    *                                                                           uniquely identifies the PO line item that
    *                                                                           the current PO line item matched
    * @param     long pendingAmendmentOid - Pending Amendment Oid.  Append the PO to this amendment
    *                                        if this parameter is not 0.
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     boolean - indicates whether or not the Import LC - Amend transaction was created successfully
    *                       (true  - the Import LC - Amend transaction was created successfully
    *                        false - the Import LC - Amend transaction was not created successfully)
    */
   private boolean createOrAppendAmendmentTransaction(DocumentHandler matchingPOLineItemsDoc, long pendingAmendmentOid, String userLocale) // logu & amit
                                                         throws AmsException, RemoteException
   {
      Transaction   amendTransaction = null;
      Instrument    instrument          = null;
      String[]      attributeValues     = null;
      String[]      attributePaths      = null;
      long          transactionOid      = 0;
      long          lockingUserOid      = 0;
      long          instrumentOid       = 0;

      // First, retrieve the transaction associated to the existing PO line item that was
      // matched so that we can retrieve its instrument
      getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);

      // Since the PO is now going to be active for the amendment,
      // a copy must be made of its data so that we can maintain data
      // at the transaction level after the PO is no longer active.

      // Populate a copy
      copyOfPoLineItem = createCopy(poLineItem);
      attributeValues = new String[1];
      attributePaths  = new String[1];

      // Clear out some of the associations on the copy.
      // Keep in mind that the transactional and shipment association (assigned_to_trans_oid, shipment_oid) are preserved
      // so that the previous transaction and shipment still own the old PO data
      // Do you really need to null out previous_po_line_item_oid and auto_added_to_amend_ind? Need investigation.  -- Weian Zhu 12/22/04
      attributePaths[0]  = "active_for_instrument";
     
      attributeValues[0] = "";
     
      copyOfPoLineItem.setAttributes(attributePaths, attributeValues);

      // Update the existing PO line item's data with the data from the current
      // PO line item, but don't save yet.
      savePOLineItemData(false);

      // Instantiate the instrument so that we can perform a bunch of validations on it
      // and save the new Amend transaction
      instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");

      instrument = (Instrument) EJBObjectFactory.createServerEJB(sessionContext, "Instrument", instrumentOid);

      // If the user doesn't have the security rights to create an Import LC - Amend
      // transaction, add an error message to the Auto LC log and stop processing the
      // PO line item

     
			// - set the instrumentType Variable from instrument. 
			//Needed for PO's which do not have instrument type set.
			instrumentType = instrument.getAttribute("instrument_type_code");
			//RKAZI T36000019379 REL 8.2 08/20/213 - End

      if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
      	if (!SecurityAccess.canCreateModInstrument(poUploadUserSecurityRights, InstrumentType.APPROVAL_TO_PAY,
                                                 TransactionType.AMEND))
      	{
         		autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
                                    InstrumentType.APPROVAL_TO_PAY,
                                    TransactionType.AMEND);

         		removeInstrument(instrument, "createNewATPAmendmentTransaction");

         		return false;
      	}
      } else {
      	if (!SecurityAccess.canCreateModInstrument(poUploadUserSecurityRights, InstrumentType.IMPORT_DLC,
                                                 TransactionType.AMEND))
      	{
         		autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
                                    InstrumentType.IMPORT_DLC,
                                    TransactionType.AMEND);

         		removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

         		return false;
      	}
	}
      //TLE - 08/-0/07 - CR-375 - End

      // If the status of the instrument is Pending, add an error message to the Auto LC log
      // and stop processing the PO line item
      if (instrument.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_PENDING))
      {
         autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber, TradePortalConstants.NTM_INSTR_PENDING);

         removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

         return false;
      }


      // Try locking the instrument so that it's not updated by more than one person at the same
      // time; if someone else is currently updating the instrument, add an error message to the
      // Auto LC log and stop processing the PO line item. If the same user is trying to update
      // the instrument somehow, he/she already has the instrument locked so continue processing.
      try
      {
         LockingManager.lockBusinessObject(instrumentOid, Long.parseLong(poUploadUserOid), true);
      }
      catch (InstrumentLockException e)
      {
         lockingUserOid = e.getUserOid();

         if (!poUploadUserOid.equals(String.valueOf(lockingUserOid)))
         {
            autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                       TradePortalConstants.NTM_INSTRUMENT_LOCKED,
                                       getInstrumentLockSubstitutionValues(lockingUserOid));

            removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

            return false;
         }
      }

      Terms terms = null;
      long shipmentOid = 0;
      Vector shipmentPOsList = new Vector();


      // W Zhu 5/22/07 CR-393 BEGIN
      // If there is pending amendment passed in,
      // then add PO to amendment, otherwise create a new amendment.
      if ( pendingAmendmentOid!= 0)
      {
    	  transactionOid = pendingAmendmentOid;
		  amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
		  terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");
		  shipmentOid = terms.getAttributeLong("first_shipment_oid");

		  // Get the list of existing POs.  This is used later to derive transaction data.
	      String alreadyAssociatedSql = "select po_line_item_oid from po_line_item where p_shipment_oid = ?";
	      DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{shipmentOid});

	      if (resultSet != null)
	      {
	    	  Vector alreadyAssociatedList = resultSet.getFragments("/ResultSetRecord");
	    	  for (int i=0; i < alreadyAssociatedList.size(); i++)
	    	  {
	    		  String poOid = ((DocumentHandler) alreadyAssociatedList.elementAt(i)).getAttribute("/PO_LINE_ITEM_OID");
	    		  shipmentPOsList.addElement(poOid);
	    	  }
	      }
	      //Krishna IR- PHUH091241186 02/05/2008 - Begin
	      amendTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
	      amendTransaction.save(false);
          //Krishna IR- PHUH091241186 02/05/2008 - End
      }
      else
      {
          // W Zhu 5/22/07 CR-393 END
    	  // If no pending amendment is found, create a new amendment and save it
    	  // First, if the instrument has Pending transactions, add a warning message to the Auto LC log
    	  // but continue processing
    	  if (instrument.pendingTransactionsExist())
    	  {
    		  autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.NTM_OTHER_TRANS_PENDING);
    	  }

    	  // Create the new amendment transaction and save it
    	  transactionOid = instrument.newComponent("TransactionList");

    	  amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
			// - set the instrumentType Variable from instrument. 
			//Needed for AMEND PO's which do not have instrument type set.
			instrumentType = instrument.getAttribute("instrument_type_code");
			//RKAZI T36000019379 REL 8.2 08/20/213 - End
	
        if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
    	      amendTransaction.createNewBlank(TransactionType.AMEND, poUploadUserOid, poUploadUserOrgOid,
                                          InstrumentType.APPROVAL_TO_PAY, false, "", null, poLineItem);
        } else {
    	      amendTransaction.createNewBlank(TransactionType.AMEND, poUploadUserOid, poUploadUserOrgOid,
                                          InstrumentType.IMPORT_DLC, false, "", null, poLineItem);
        }
        //TLE - 08/-0/07 - CR-375 - End



		  terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");
		  shipmentOid = terms.getAttributeLong("first_shipment_oid");

		  // Begin W Zhu FDUG121561513
          // Save the new transaction and PO Line Items before trying to derive the data
          // since the derivation assumes the data are saved in the database.
          instrument.save(false);
      }

      shipmentPOsList.addElement(poLineItem.getAttribute("po_line_item_oid"));
      ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
      ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentOid);


      // Set several transaction-related attributes in the PO line item and save it.
      attributeValues = new String[5];
      attributePaths  = new String[5];

      attributePaths[0]  = "previous_po_line_item_oid";
      attributePaths[1]  = "assigned_to_trans_oid";
      attributePaths[2]  = "auto_added_to_amend_ind";
      attributePaths[3]  = "active_for_instrument";
      attributePaths[4]  = "shipment_oid";

      attributeValues[0] = copyOfPoLineItem.getAttribute("po_line_item_oid");
      attributeValues[1] = String.valueOf(transactionOid);
      attributeValues[2] = TradePortalConstants.INDICATOR_YES;
      attributeValues[3] = String.valueOf(instrumentOid);
      attributeValues[4] = String.valueOf(shipmentOid);

      poLineItem.setAttributes(attributePaths, attributeValues);
      poLineItem.save();
      copyOfPoLineItem.save();
      // End W Zhu FDUG121561513

      POLineItemUtility.deriveAmendmentTransactionDataFromPOLocale(false,amendTransaction, shipmentTerms,
            1, shipmentPOsList, 0, sessionContext, instrument, matchingPOLineItemsDoc, userLocale);
            String goodsDescription = resMgr.getText("PurchaseOrders.POItemsAmended", TradePortalConstants.TEXT_BUNDLE);
            shipmentTerms.setAttribute("goods_description", goodsDescription);
      // Save any change of transaction data derived from PO.
      instrument.save(false);

      removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

      return true;
   }

   /**
    * This method is used to instantiate the existing PO line item that matches
    * the current PO line item being processed. Once this object has been
    * instantiated, it can be accessed in other methods like saving, accessing
    * the transaction that the PO line item is assigned to, etc.
    *

    * @param      com.amsinc.ecsg.util.DocumentHandler matchingPOLineItemsDoc - xml document containing the OID that
    *                                                                           uniquely identifies the PO line item that
    *                                                                           the current PO line item matched
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void getMatchingPOLineItem(DocumentHandler matchingPOLineItemsDoc) throws AmsException, RemoteException
   {
      DocumentHandler   poLineItemDoc = null;
      Vector            poLineItems   = null;
      long              poLineItemOid = 0;

      poLineItems   = matchingPOLineItemsDoc.getFragments("/ResultSetRecord");
      poLineItemDoc = (DocumentHandler) poLineItems.elementAt(0);
      poLineItemOid = poLineItemDoc.getAttributeLong("PO_LINE_ITEM_OID");

      poLineItem = (POLineItem) EJBObjectFactory.createServerEJB(sessionContext, "POLineItem", poLineItemOid);
   }

   /**
    * This method is used to instantiate the transaction that an existing PO
    * line item is assigned to. Once this object has been instantiated, it can
    * be accessed in other methods like deriving fields, updating, etc.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void getMatchingPOLineItemTransaction() throws AmsException, RemoteException
   {
      long   transactionOid = 0;

      transactionOid = poLineItem.getAttributeLong("assigned_to_trans_oid");

      poLineItemTransaction = (Transaction) EJBObjectFactory.createServerEJB(sessionContext, "Transaction",
                                                                             transactionOid, new ClientServerDataBridge());
   }

   /**
    * This method is used to instantiate the transaction that an existing PO
    * line item is assigned to. It first calls a method to instantiate the PO
    * line item. Once the transaction object has been instantiated, it can
    * be accessed in other methods like deriving fields, updating, etc.
    *

    * @param      com.amsinc.ecsg.util.DocumentHandler matchingPOLineItemsDoc - xml document containing the OID that
    *                                                                           uniquely identifies the PO line item that
    *                                                                           the current PO line item matched
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void getMatchingPOLineItemTransaction(DocumentHandler matchingPOLineItemsDoc)
                                                 throws AmsException, RemoteException
   {

      getMatchingPOLineItem(matchingPOLineItemsDoc);
      getMatchingPOLineItemTransaction();
   }



   /**
    * This method is used to unassign a PO line item from a transaction. It
    * first needs to retrieve the transaction that the PO line item is assigned
    * to so that we can access info from it for an Auto LC log message later.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     void
    */
   private void unassignPOLineItem() throws AmsException, RemoteException
   {
      getMatchingPOLineItemTransaction();

      poLineItem.setAttribute("previous_po_line_item_oid", null);
      poLineItem.setAttribute("assigned_to_trans_oid",        null);
      poLineItem.setAttribute("active_for_instrument",        null);
      poLineItem.setAttribute("shipment_oid", null);
   }

   /**
    * This method is used to retrieve an array of substitution values
    * containing instrument data for use in several Auto LC error log messages.
    * The flag determines whether or not the PO line item number should be
    * included as a substitution parameter.
    *

    * @param      boolean includePOLineItemNumberFlag - indicates whether or not the PO line item number should be
    *                                                   included as a substitution parameter
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     java.lang.String [] - an array of Auto LC log error message substitution values containing instrument
    *                                   data such as the instrument ID and transaction type
    */
   private String[] getInstrumentSubstitutionValues(boolean includePOLineItemNumberFlag)
                                                    throws AmsException, RemoteException
   {
      String[]     substitutionValues = null;
      String       transactionType    = null;
       int          index              = 0;

      if (includePOLineItemNumberFlag)
      {
         substitutionValues = new String[3];
         substitutionValues[index++] = poLineItemNumber;
      }
      else
      {
         substitutionValues = new String[2];
      }

      transactionType = poLineItemTransaction.getAttribute("transaction_type_code");

      substitutionValues[index++] = getCompleteInstrumentId();
      substitutionValues[index]   = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
                                                                                  transactionType);

      return substitutionValues;
   }

   /**
    * This method is used to retrieve an array of substitution values
    * containing instrument lock data for use in an Auto LC error log message.
    * The oid passed in is used to lookup the name of the user who currently
    * has the instrument locked that the current user is trying to create a
    * transaction for.
    *

    * @param      long lockingUserOid - unique OID of the user who currently has the instrument locked
    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     java.lang.String [] - an array of Auto LC log error message substitution values containing instrument
    *                                   lock data such as the instrument type, transaction type, and user name
    */
   private String[] getInstrumentLockSubstitutionValues(long lockingUserOid) throws AmsException, RemoteException
   {
      String[]   substitutionValues = new String[4];
      User       lockUser           = null;

      lockUser = (User) EJBObjectFactory.createServerEJB(sessionContext, "User", lockingUserOid);


      if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
          substitutionValues[0] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                                                                            InstrumentType.APPROVAL_TO_PAY);
      } else {
          substitutionValues[0] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                                                                            InstrumentType.IMPORT_DLC);
      }
     
      substitutionValues[1] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
                                                                            TransactionType.AMEND);
      substitutionValues[2] = lockUser.getAttribute("first_name");
      substitutionValues[3] = lockUser.getAttribute("last_name");

      removeUser(lockUser, "getInstrumentLockSubstitutionValues");

      return substitutionValues;
   }

   /**
    * This method is used to retrieve the complete instrument ID of the
    * instrument that the user is trying to create a transaction for.
    *

    * @exception  com.amsinc.ecsg.frame.AmsException
    * @exception  java.rmi.RemoteException
    * @return     java.lang.String - the complete instrument ID of the instrument that the user is trying to create a
    *                                transaction for
    */
   private String getCompleteInstrumentId() throws AmsException, RemoteException
   {
      Instrument   instrument           = null;
      String       completeInstrumentId = null;
      long         instrumentOid        = 0;

      instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");

      instrument = (Instrument) EJBObjectFactory.createServerEJB(sessionContext, "Instrument", instrumentOid);

      completeInstrumentId = instrument.getAttribute("complete_instrument_id");

      removeInstrument(instrument, "getCompleteInstrumentId");

      return completeInstrumentId;
   }

   /**
    * This method is used to add an error message to the Auto LC log file
    * stating that the PO number is missing for the current PO line item being
    * processed.
    *

    * @return     void
    */
   public void logMissingPONumberMessage()
   {
      POLineItemField   poLineItemField = null;

      poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(poNumberFieldIndex);

      try
      {
         autoLCLogger.addLogMessage(poUploadUserOrgOid, logSequenceNumber,
                                    TradePortalConstants.LCLOG_DATA_MISSING,
                                    poLineItemField.getFieldName(), "");
      }
      catch (AmsException ex)
      {
         LOG.error("Exception occurred in POLineItemData.logMissingPONumberMessage(): ", ex);
      }
   }

   /**
    * This method is used to reset all PO line item attributes that could
    * change values during the processing of a PO line item. This method
    * reinitializes these fields to null so that they don't contain old data
    * during the processing of future PO line items.
    *

    * @return     void
    */
   public void resetPOLineItemData() throws RemoteException
   {
      cleanupPOLineItemData("resetPOLineItemData");

      poLineItemLastShipmentDate = null;
      poLineItemBeneficiaryName  = null;
      poLineItemTransaction      = null;
      poLineItemCurrency         = null;
      poLineItemAmount           = null;
      poLineItemNumber           = null;
      poLineItem                 = null;
      itemNumber                 = null;
      poNumber                   = null;
   }

   /**
    * This method is used to remove the PO line item and PO line item
    * transaction EJBs that were used during the processing of the current PO
    * line item. The calling method parameter is used simply for debugging
    * purposes if an error occurs while removing these objects.
    *

    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void cleanupPOLineItemData(String methodOrigin) throws RemoteException
   {
      removePOLineItem(methodOrigin);

      if (poLineItemTransaction != null)
      {
         removePOLineItemTransaction(methodOrigin);
      }
   }

   /**
    * This method is used to remove an Instrument EJB that was used during the
    * processing of the current PO line item. The calling method parameter is
    * used simply for debugging purposes if an error occurs while removing this
    * object.
    *

    * @param      com.ams.tradeportal.busobj.Instrument instrument - the instrument EJB to remove from memory
    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void removeInstrument(Instrument instrument, String methodOrigin) throws RemoteException
   {
      try
      {
         if(instrument != null)
          {
            instrument.remove();
            instrument = null;
          }
      }
      catch (RemoveException ex)
      {
         issueRemoveErrorDebugMessage("instrument", methodOrigin);
      }
   }

   /**
    * This method is used to remove the PO Line Item EJB that was used during
    * the processing of the current PO line item. The calling method parameter
    * is used simply for debugging purposes if an error occurs while removing
    * this object.
    *

    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void removePOLineItem(String methodOrigin) throws RemoteException
   {
      try
      {
         if(poLineItem != null)
          {
            poLineItem.remove();
            poLineItem = null;
          }

         if(copyOfPoLineItem != null)
          {
            copyOfPoLineItem.remove();
            copyOfPoLineItem = null;
          }

      }
      catch (RemoveException ex)
      {
         issueRemoveErrorDebugMessage("PO line item", methodOrigin);
      }
   }

   /**
    * This method is used to remove the PO Line Item transaction EJB that was
    * used during the processing of the current PO line item. The calling
    * method parameter is used simply for debugging purposes if an error occurs
    * while removing this object.
    *

    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void removePOLineItemTransaction(String methodOrigin) throws RemoteException
   {
      try
      {
         if(poLineItemTransaction != null)
          {
            poLineItemTransaction.remove();
            poLineItemTransaction = null;
          }
      }
      catch (Exception ex)
      {
         issueRemoveErrorDebugMessage("PO line item transaction", methodOrigin);
      }
   }

   /**
    * This method is used to remove a PO Upload Definition EJB that was used
    * during the processing of PO line items from a PO upload data file.
    *

    * @param      com.ams.tradeportal.busobj.POUploadDefinition poUploadDefinition - the PO Upload Definition EJB to
    *                                                                                remove from memory
    * @return     void
    */
   private void removePOUploadDefinition(POUploadDefinition poUploadDefinition)
   {
      try
      {
         if (poUploadDefinition != null)
         {
            poUploadDefinition.remove();
            poUploadDefinition = null;
         }
      }
      catch (Exception ex)
      {
         LOG.error("Error removing PO upload definition in POLineItemData.removePOUploadDefinition(): ", ex);
      }
   }

   /**
    * This method is used to remove a Party EJB that was used during the
    * processing of the current PO line item. The calling method parameter is
    * used simply for debugging purposes if an error occurs while removing this
    * object.
    *

    * @param      com.ams.tradeportal.busobj.Party party - the party EJB to remove from memory
    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void removeParty(Party party, String methodOrigin) throws RemoteException
   {
      try
      {
         if(party != null)
          {
            party.remove();
            party = null;
          }
      }
      catch (RemoveException ex)
      {
         issueRemoveErrorDebugMessage("party", methodOrigin);
      }
   }

   /**
    * This method is used to remove a User EJB that was used during the
    * processing of the current PO line item. The calling method parameter is
    * used simply for debugging purposes if an error occurs while removing this
    * object.
    *

    * @param      com.ams.tradeportal.busobj.User user - the user EJB to remove from memory
    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void removeUser(User user, String methodOrigin) throws RemoteException
   {
      try
      {
         if(user != null)
          {
             user.remove();
             user = null;
          }
      }
      catch (RemoveException ex)
      {
         issueRemoveErrorDebugMessage("user", methodOrigin);
      }
   }

   /**
    * This method is used to issue a debug error message indicating that an
    * error occurred while trying to remove a particular EJB object from
    * memory.
    *

    * @param      java.lang.String objectToRemove - description of the EJB object being removed from memory
    * @param      java.lang.String methodOrigin - the name of the calling method
    * @return     void
    */
   private void issueRemoveErrorDebugMessage(String objectToRemove, String methodOrigin)
   {
      StringBuffer errorMessage = new StringBuffer();

      errorMessage.append("Error removing ");
      errorMessage.append(objectToRemove);
      errorMessage.append(" in POLineItemData.");
      errorMessage.append(methodOrigin);
      errorMessage.append("()!");

      LOG.info(errorMessage.toString());
   }

   /**
    * This method sets instrument type to signal this object whether it's processing for LC or ATP, etc.
    */
   public void setInstrumentType(String instrType)
   {
      instrumentType = instrType;
   }
   

}
