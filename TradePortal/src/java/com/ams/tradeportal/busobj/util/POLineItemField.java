package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for representing a Purchase Order Line Item field. A PO 
 * Line Item field has a type, a data type, a name, a size, and a value. 
 * Objects of this class are used in the PO Line Item Data class during the 
 * parsing of a PO Upload Data file in order to create PO line items. This 
 * class is a simple bean class with just getters and setters for its 
 * attributes.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 */


public class POLineItemField
{
private static final Logger LOG = LoggerFactory.getLogger(POLineItemField.class);
   private String   fieldDataType = null;
   private String   fieldValue    = null;
   private String   fieldType     = null;
   private String   fieldName     = null;
   private int      fieldSize     = 0;

   /**
    * Creates an instance of the POLineItemField class.
    * 

    * @return   void
    */
   public POLineItemField()
   {

   }

   /**
    * This method returns the PO Line Item Field's data type.
    *

    * @return   java.lang.String - the PO Line Item Field's data type
    */
   public String getFieldDataType()
   {
      return fieldDataType;
   }

   /**
    * This method returns the PO Line Item Field's value.
    *

    * @return   java.lang.String - the PO Line Item Field's value
    */
   public String getFieldValue()
   {
      return fieldValue;
   }

   /**
    * This method returns the PO Line Item Field's type (i.e., ben_name, 
    * amount, other1, other24, etc.).
    *

    * @return   java.lang.String - the PO Line Item Field's type
    */
   public String getFieldType()
   {
      return fieldType;
   }

   /**
    * This method returns the PO Line Item Field's name.
    *

    * @return   java.lang.String - the PO Line Item Field's name
    */
   public String getFieldName()
   {
      return fieldName;
   }

   /**
    * This method returns the PO Line Item Field's size.
    *

    * @return   int - the PO Line Item Field's size
    */
   public int getFieldSize()
   {
      return fieldSize;
   }

   /**
    * This method sets the PO Line Item Field's data type.
    *

    * @param    java.lang.String newFieldDataType - the new PO Line Item Field data type
    * @return   void
    */
   public void setFieldDataType(String newFieldDataType)
   {
      fieldDataType = newFieldDataType;
   }

   /**
    * This method sets the PO Line Item Field's value.
    *

    * @param    java.lang.String newFieldValue - the new PO Line Item Field value
    * @return   void
    */
   public void setFieldValue(String newFieldValue)
   {
      fieldValue = newFieldValue;
   }

   /**
    * This method sets the PO Line Item Field's type.
    *

    * @param    java.lang.String newFieldType - the new PO Line Item Field type
    * @return   void
    */
   public void setFieldType(String newFieldType)
   {
      fieldType = newFieldType;
   }

   /**
    * This method sets the PO Line Item Field's name.
    *

    * @param    java.lang.String newFieldName - the new PO Line Item Field name
    * @return   void
    */
   public void setFieldName(String newFieldName)
   {
      fieldName = newFieldName;
   }

   /**
    * This method sets the PO Line Item Field's size.
    *

    * @param    java.lang.String newFieldSize - the new PO Line Item Field size
    * @return   void
    */
   public void setFieldSize(int newFieldSize)
   {
      fieldSize = newFieldSize;
   }
}
