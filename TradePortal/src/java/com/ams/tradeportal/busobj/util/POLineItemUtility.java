package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.*;
import java.sql.*;
import java.text.*;
import java.rmi.*;
import java.math.*;
import java.util.Date;

import javax.ejb.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;

/**
 * The POLinteItemUtility is a specialized database access class.  It
 * performs a mass update to several po line items for a specific set of
 * po line item oids.  This allows many line items to be assigned to an
 * instrument.
 * This class was created (rather than using an EJB) for performance
 * reasons (to reduce the number of EJbs that are part of a transaction.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *

 * @version 1.0
 */
public class POLineItemUtility
{
private static final Logger LOG = LoggerFactory.getLogger(POLineItemUtility.class);

	private static String MASS_UPDATE_SQL =
		"update po_line_item " +
		" set auto_added_to_amend_ind = ''N'', " +
		"     a_assigned_to_trans_oid = {0}, " +
		"     a_active_for_instrument = {1}, " +
		"     p_shipment_oid = {2}";

    private static String STRUCTURED_PO_MASS_UPDATE_SQL =
            "update purchase_order " +
                    " set  " +
                    "     a_transaction_oid = {0}, " +
                    "     a_instrument_oid = {1}, " +
                    "     a_shipment_oid = {2} " ;

   
    private static String INVOICE_MASS_UPDATE_SQL =
            "update invoices_summary_data " +
                    " set  " +
                    "     a_transaction_oid = {0}, " +
                    "     a_instrument_oid = {1}, " +
                    "     a_terms_oid = {2}, " +
                    "     invoice_status = {3} " ;
 
	private static String userLocale = "en_US";
	private static int numberOfDecimalPlaces = 0;
	private static String transactionCurrency = null;
	

	private POLineItemUtility ()
	{
	}


	/**
	 * Performs a mass update of several po line item records for a given
	 * set of po line item oids.  Each row has the auto amend ind set to N,
	 * the first amend oid set to null and the assigned to transaction oid
	 * set to the given transaction and the active_for_instrument set also.
	 *
	 * @param transactionOid java.lang.String - the transaction to assign to
	 *                                          the po line item
	 * @param poOids java.util.Vector - the set of po oids to update
	 */
	public static void updatePOTransAssignment(Vector poOids, String transactionOid, String instrumentOid, String shipmentOid)
		throws AmsException
	{
		StringBuilder   sql         = new StringBuilder();
		String         poListSql   = null;
		
		try
		{
			// Construct a comma separated list of the po oids within
			// parenthesis
			String requiredOid = "po_line_item_oid";//SHR CR 707 rel8.1.1
			poListSql = getPOLineItemOidsSql(poOids,requiredOid);

			Object[] arguments = {"?", "?", "?"};

			sql.append(MessageFormat.format(MASS_UPDATE_SQL, arguments));
			sql.append(" where ");
			sql.append(poListSql);
			
			List<Object> paramList = new ArrayList<Object>();
			paramList.add(transactionOid);
			paramList.add(instrumentOid);
			paramList.add(shipmentOid);

			DatabaseQueryBean.executeUpdate(sql.toString(),false, paramList);
		}
		catch (SQLException e)
		{
			throw new AmsException("SQL Exception found executing sql statement \n"
				+ sql
				+ "\nException is " + e.getMessage());
		}
	}

    /**
     * Performs a mass update of several po line item records for a given
     * set of po line item oids.  Each row has the auto amend ind set to N,
     * the first amend oid set to null and the assigned to transaction oid
     * set to the given transaction and the active_for_instrument set also.
     *
     * @param transactionOid java.lang.String - the transaction to assign to
     *                                          the po line item
     * @param poOids java.util.Vector - the set of po oids to update
     */
    public static void updateStructuredPOTransAssignment(Vector poOids, String transactionOid, String instrumentOid, String shipmentOid, String status)
            throws AmsException
    {
        StringBuilder   sql         = new StringBuilder();
        String         poListSql   = null;
        try
        {
            // Construct a comma separated list of the po oids within
            // parenthesis
        	String requiredOid = "purchase_order_oid";//SHR CR708 Rel8.1.1 
            poListSql = getPOLineItemOidsSql(poOids,requiredOid);

            Object[] arguments = {"?", "?", "?"};

            sql.append(MessageFormat.format(STRUCTURED_PO_MASS_UPDATE_SQL, arguments));
            sql.append(" ,    status = ? ");

            sql.append(" where ");
            sql.append(poListSql);
            List<Object> paramList = new ArrayList<Object>();
			paramList.add(transactionOid);
			paramList.add(instrumentOid);
			paramList.add(shipmentOid);
			paramList.add(status);

            DatabaseQueryBean.executeUpdate(sql.toString(),false, paramList);

        }
        catch (SQLException e)
        {
            throw new AmsException("SQL Exception found executing sql statement \n"
                    + sql
                    + "\nException is " + e.getMessage());
        }
    }

/**
 * Writes a series of log messages indicating what instrument a po line was
 * assigned to for which lc rule.
 *
 * @param instrumentId java.lang.String - the instrument id a po line was assigned to
 *              (not the oid, rather the instrument id -- it becomes part of the log
 *               message).
 * @param lcRuleName java.lang.String - Name of the LC Creation Rule used for the
 *              assignment.
 * @param poNums java.util.Vector - vector of po num/line nums used in the log
 *              message.  All po's in the vector were assigned to the instrument id
 *              given as another parameter
 * @param corporateOrg java.lang.String - the org that owns the instrument
 * @param logSequence java.lang.String - indicates an instance of the log for a
 *              specific org
 * @exception AmsException
 */
public static void logPOAssignment(String instrumentId, String lcRuleName, Vector poNums, String corporateOrg, String logSequence)
	throws AmsException {

	// For each PO Num in the vector, write a message

	for (int i=0; i<poNums.size(); i++) {
		String[] parms = {(String)poNums.elementAt(i),
						  instrumentId,
						  lcRuleName};

		AutoLCLogger.getInstance(corporateOrg).addLogMessage(corporateOrg,
					logSequence,
					TradePortalConstants.LCLOG_LINE_ITEM_ASSIGNED,
					parms);
	}
}

    /**
     * Writes a series of log messages indicating what instrument a po line was
     * assigned to for which lc rule.
     *
     * @param instrumentId java.lang.String - the instrument id a po line was assigned to
     *              (not the oid, rather the instrument id -- it becomes part of the log
     *               message).
     * @param lcRuleName java.lang.String - Name of the LC Creation Rule used for the
     *              assignment.
     * @param poNums java.util.Vector - vector of po num/line nums used in the log
     *              message.  All po's in the vector were assigned to the instrument id
     *              given as another parameter
     * @param corporateOrg java.lang.String - the org that owns the instrument
     * @param logSequence java.lang.String - indicates an instance of the log for a
     *              specific org
     * @exception AmsException
     */
    public static void logStructuredPOAssignment(String instrumentId, String lcRuleName, Vector poNums, String corporateOrg, String logSequence)
            throws AmsException {

        // For each PO Num in the vector, write a message

        for (int i=0; i<poNums.size(); i++) {
            String[] parms = {(String)poNums.elementAt(i),
                    instrumentId,
                    lcRuleName};

            StructuredPOLogger.getInstance(corporateOrg).addLogMessage(corporateOrg,
                    logSequence,
                    TradePortalConstants.LCLOG_LINE_ITEM_ASSIGNED,
                    parms);
        }
    }

   /**
	* This method builds a vector containing only PO line item oids (as String
	* objects). This method is used when multiple PO line item oids are used in
	* multiple places; instead of constantly retrieving the oids from xml docs,
	* this vector can be constructed once and used afterwards wherever
	* necessary. This method goes through a vector of PO line item xml docs,
	* retrieves each PO line item oid, and adds it to the vector.
	*

	* @param      poLineItemsList - list of xml docs containing PO line item oid data
	* @param      multiplePOLineItems - Is PO line item grouping turned on (YES/NO)
	* @param      addRemoveFlag - Adding or Removing POs (ADD/REMOVE). Determines SQL to execute.
	* @return     java.util.Vector - a vector containing all PO line item oids
	*/
   public static Vector getPOLineItemOids(Vector poLineItemsList, String multiplePOLineItems, String addRemoveFlag)
   {
	// NOTE: This might be obsolete and should be removed after CR207 (12/2/2003)
	  DocumentHandler   poLineItemDoc       = null;
	  Vector            poLineItemOidsList  = new Vector();
	  int               numberOfItems = 0;
	  String			poNumber = null;		// po_num
	  String			beneName = null;		// ben_name
	  String			currency = null;		// currency
	  String			ownerOrgOid = null;		// a_owner_org_oid
	  String			uploadDefOid = null;	// a_source_upload_definition_oid

	  numberOfItems = poLineItemsList.size();
	 
	  for (int i = 0; i < numberOfItems; i++)
	  {		  
		 poLineItemDoc = (DocumentHandler) poLineItemsList.elementAt(i);
		 if (TradePortalConstants.INDICATOR_YES.equals(multiplePOLineItems) ) {
		
			 StringTokenizer tokenizer = new StringTokenizer(poLineItemDoc.getAttribute("/poLineItemOid"), "/");
			 poNumber = tokenizer.nextToken();
			 beneName = tokenizer.nextToken();
			 currency = tokenizer.nextToken();
			 ownerOrgOid = tokenizer.nextToken();
			 uploadDefOid = tokenizer.nextToken();
			 String sql = "";
			 try {
				 sql = "select po_line_item_oid from po_line_item where po_num = ? and ben_name = ? and currency = ? and a_owner_org_oid = ? and a_source_upload_definition_oid = ? and source_type = 'UPLOAD' and ";
				 if (TradePortalConstants.PO_ADD.equals(addRemoveFlag))
					 sql = sql + "a_assigned_to_trans_oid is null";
				 else { // addRemoveFlag == TradePortalConstants.PO_REMOVE
					 sql = sql + "a_assigned_to_trans_oid is not null and ";
				 	 sql = sql + "p_shipment_oid is not null";
				 }
			
				 DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, poNumber, beneName, currency, ownerOrgOid, uploadDefOid);
				 List<DocumentHandler> rows = result.getFragmentsList("/ResultSetRecord");
				 for (DocumentHandler row : rows) {
					 String poOid = row.getAttribute("/PO_LINE_ITEM_OID");
					 poLineItemOidsList.addElement(poOid);
				 }
			 } catch (Exception e) {
					LOG.error("Exception while retrieving po line item: ", e);
			 }
		 } else {		 
			 poLineItemOidsList.addElement(poLineItemDoc.getAttribute("/poLineItemOid"));
		 }
	  }

	  return poLineItemOidsList;
   }
   
   // build sql for attributes other than OIDs
   public static String getPONumsSql(Vector poLineItemOidsList,String oid)

	{
		StringBuilder sql = null;
		int numberOfPOLineItems = 0;
		int allPos = 0;
		numberOfPOLineItems = poLineItemOidsList.size();
		sql = new StringBuilder();
	
		sql.append(" (");
		sql.append(oid);
		sql.append(" in (");
		if (numberOfPOLineItems <= 1000) {
			allPos = numberOfPOLineItems;
			sql = getAllPONumsSql(poLineItemOidsList, allPos, sql, 0);
		} 
		
		//if poLineItems are >1000 then break the items for every 1000 and create new 'IN' clause with OR condition
		 
		else {
			int quotient = numberOfPOLineItems / 1000;
			int remainder = numberOfPOLineItems % 1000;
			allPos = numberOfPOLineItems;
			for (int i = 1; i <= quotient; i++) {
				sql = getAllPONumsSql(poLineItemOidsList, 1000 * i,
						sql, (1000 * i) - 1000);
				if (i != quotient)	{
					sql.append(" or ");
					sql.append(oid);
					sql.append(" in (");
				}
				else if (remainder > 0)
				sql.append(" or ");
			
			}

			if (remainder > 0) {
		
				sql.append(oid);
				sql.append(" in (");
		
				sql = getAllPONumsSql(poLineItemOidsList,
						numberOfPOLineItems, sql, numberOfPOLineItems 
						- remainder);
			}
		}
		sql.append(")");
		return sql.toString();
	}

   /**
	* This method builds the SQL containing all of the PO line item oids to use
	* in numerous queries involving PO line items. It goes through the vector
	* of PO line item oids passed in, retrieves each PO line item oid, and
	* appends them to the SQL to be used as part of the where clause in
	* various SQL queries.
	*

	* @param      poLineItemOidsList - list of PO line item oids to use in SQL queries
	* @return     java.lang.String - string containing all of the PO line items oids to be used in various SQL queries
	*/
   public static String getPOLineItemOidsSql(Vector poLineItemOidsList,String oid)

	{
		StringBuilder sql = null;
		int numberOfPOLineItems = 0;
		int allPos = 0;
		numberOfPOLineItems = poLineItemOidsList.size();
		sql = new StringBuilder();
	
		sql.append(" (");
		sql.append(oid);
		sql.append(" in (");
		//if poLineItems are < 1000 then just create normal sql
		if (numberOfPOLineItems <= 1000) {
			allPos = numberOfPOLineItems;
			sql = getAllPOLineItemOidsSql(poLineItemOidsList, allPos, sql, 0);
		} 
		
		//if poLineItems are >1000 then break the items for every 1000 and create new 'IN' clause with OR condition
		 
		else {
			int quotient = numberOfPOLineItems / 1000;
			int remainder = numberOfPOLineItems % 1000;
			allPos = numberOfPOLineItems;
			for (int i = 1; i <= quotient; i++) {
				sql = getAllPOLineItemOidsSql(poLineItemOidsList, 1000 * i,
						sql, (1000 * i) - 1000);
				if (i != quotient)	{
					sql.append(" or ");
					sql.append(oid);
					sql.append(" in (");
				}
				else if (remainder > 0)
				sql.append(" or ");
			}

			if (remainder > 0) {
				
				sql.append(oid);
				sql.append(" in (");
			
				sql = getAllPOLineItemOidsSql(poLineItemOidsList,
						numberOfPOLineItems, sql, numberOfPOLineItems 
						- remainder);
			}
		}
		
		sql.append(")");
		return sql.toString();
	}

	/**
	 * SHILPA R- IR GNUH060565375 added
	 * This method builds the SQL containing all of the PO line item oids to use
	 * in numerous queries involving PO line items. 
	 * @param poLineItemOidsList
	 * @param numberOfPOLineItems
	 * @param sql
	 * @param n
	 * @return
	 */
	public static StringBuilder getAllPOLineItemOidsSql(
			Vector poLineItemOidsList, int numberOfPOLineItems,
			StringBuilder sql, int n) {
		for (int i = n; i < numberOfPOLineItems; i++) {
			sql.append("'");
			sql.append(SQLParamFilter.filter((String) poLineItemOidsList.elementAt(i)));
			sql.append("'");
			if (i != (numberOfPOLineItems - 1)) {
				sql.append(", ");
			}
		}
		sql.append(")");
		return sql;
	}
	
	public static StringBuilder getAllPONumsSql(
			Vector poLineItemOidsList, int numberOfPOLineItems,
			StringBuilder sql, int n) {
		for (int i = n; i < numberOfPOLineItems; i++) {
			sql.append("'");
			sql.append(SQLParamFilter.filter((String) poLineItemOidsList.elementAt(i)));
			sql.append("'");
			if (i != (numberOfPOLineItems - 1)) {
				
				sql.append(", ");
			}
		}
		sql.append(")");
		return sql;
	}

  
   //Sets the user locale to be used for formatting the Amount field
   /**
   * Derive transaction data (such as amount, expiry_date, ShipmentTerms'
   * last_shipment_date, goods description) from PO Line Items based on a
	   * list of POs passed in.  This assumes all POs are already in the database
	   * with the proper data.
	   *
   * @param transaction Transaction - The Transaction to derive data from its POs.  Required
   * @param shipmentTerms ShipmentTerms - The handle to the ShipmentTerms.
   * Required to avoid transactional error when accessing from outside EJB.
   * @param numberOfShipmentTerms int - number of shipment terms in the
   * transaction.  Used to determine whether need to query the other
   * ShipmentTerms.
   * @param listOfPoOids - a vector containing strings that are the OIDs of all POs
   * to derive from
   * @param definitionOid - the OID of the PO definition of the POs
   * @param sessionContext - context of the calling EJB
   * @param locale - users locale
   *
   * @throws AmsException
   * @throws RemoteException
   */
   public static void deriveTransactionDataFromPO(boolean isStructuredPO,Transaction transaction,
												  ShipmentTerms shipmentTerms,
												  int numberOfShipmentTerms,
												  Vector listOfPoOids,
												  long definitionOid,
												  SessionContext sessionContext,
												  String locale) throws AmsException, RemoteException
   {
	 userLocale = locale;
	 deriveTransactionDataFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms,
															 null, 0, listOfPoOids, definitionOid, sessionContext, null, null, null, true);
   }
  

  // New method added that sets the user locale to be used for formatting the amount field
  /**
	 * Derive transaction data (such as amount, expiry_date, ShipmentTerms'
	 * last_shipment_date, goods description) from PO Line Items based on a
	 * list of POs passed in.  This assumes all POs are already in the database
	 * with the proper data.
	 *
	 * @param instrument Instrument - the Instrument object to get the current
	 * expiry date.
	 * @param transaction Transaction - The Transaction to derive data from its POs.  Required
	 * @param shipmentTerms ShipmentTerms - The handle to the ShipmentTerms.
	 * Required to avoid transactional error when accessing from outside EJB.
	 * @param numberOfShipmentTerms int - number of shipment terms in the
	 * transaction.  Used to determine whether need to query the other
	 * ShipmentTerms.
	 * @param listOfPoOids - a vector containing strings that are the OIDs of all POs
	 * to derive from
	 * @param definitionOid - the OID of the PO definition of the POs
	 * @param sessionContext - context of the calling EJB
	 * @param locale - users locale
	 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
  public static void deriveAmendmentTransactionDataFromPOLocale(boolean isStructuredPO,Transaction transaction,
													 ShipmentTerms shipmentTerms,
													 int numberOfShipmentTerms,
													 Vector listOfPoOids,
													 long definitionOid,
													 SessionContext sessionContext,
													 Instrument instrument,
													 // logu & amit  IR EEUE032659500 -06/30/2005
													 DocumentHandler matchPOLineItems, 
													 String locale) throws AmsException, RemoteException 
  {
	 userLocale = locale;
	 deriveAmendmentTransactionDataFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms,
												  listOfPoOids, definitionOid, sessionContext, instrument, matchPOLineItems);
	 return;
  }



   /**
	* Derive transaction data (such as amount, expiry_date, ShipmentTerms'
	* last_shipment_date, goods description) from PO Line Items based on a
	* list of POs passed in.  This assumes all POs are already in the database
	* with the proper data.
	*
	* @param instrument Instrument - the Instrument object to get the current
	* expiry date.
	* @param transaction Transaction - The Transaction to derive data from its POs.  Required
	* @param shipmentTerms ShipmentTerms - The handle to the ShipmentTerms.
	* Required to avoid transactional error when accessing from outside EJB.
	* @param numberOfShipmentTerms int - number of shipment terms in the
	* transaction.  Used to determine whether need to query the other
	* ShipmentTerms.
		* @param listOfPoOids - a vector containing strings that are the OIDs of all POs
		* to derive from
		* @param definitionOid - the OID of the PO definition of the POs
		* @param sessionContext - context of the calling EJB
		* @param matchPOLineItems - DocumentHandler : matching PO Line Items
		* from the authorised transactions.  Will not derive transaction amount
		* if this is not null;
		*
	* @throws AmsException
	* @throws RemoteException
	*/
   public static void deriveAmendmentTransactionDataFromPO(boolean isStructuredPO,Transaction transaction,
													ShipmentTerms shipmentTerms,
													int numberOfShipmentTerms,
													Vector listOfPoOids,
													long definitionOid,
													SessionContext sessionContext,
													Instrument instrument,
													DocumentHandler matchPOLineItems) throws AmsException, RemoteException
   {
	  // Get the current latest shipment date
	  // This is the latest shipment date on the latest transaction's bank_release_terms or,
	  // if not yet processed by bank, the customer entered terms.
	String sqlQuery = "select s.latest_shipment_date from shipment_terms s, transaction t"
			+	" where s.p_terms_oid = decode(t.c_bank_release_terms_oid, null, t.c_cust_enter_terms_oid, t.c_bank_release_terms_oid)"
			+	"   and t.p_instrument_oid = ? "
			+	"   and (t.transaction_status = ?"
			+	"        or t.transaction_status = ?)"
			+	"   and t.transaction_oid <> ? "
			+	"   and s.latest_shipment_date is not null"
			+	" order by transaction_status_date desc, transaction_oid desc";

	// The current latest shipment date should exist.  But if it does not exist due to some error,
	// we will not derive dates, according to K Fix discussion with BMO.
	// Note that in deriveTransactionDataFromPO we cannot use whether currentLatestShipmentDate == null to determine
	// whether to derive dates since the derivation for issuance always has currentLatestShipmentDate == null but
	// we do need to derive dates for issuance.
	DocumentHandler shipmentTermsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, instrument.getAttribute("instrument_oid"),
			TransactionStatus.AUTHORIZED, TransactionStatus.PROCESSED_BY_BANK, transaction.getAttribute("transaction_oid"));
	java.util.Date currentLatestShipmentDate = null;
	if (shipmentTermsDoc != null) {
		List<DocumentHandler> shipmentTermsDocResults   = shipmentTermsDoc.getFragmentsList("/ResultSetRecord");
		SimpleDateFormat   dbDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		if (shipmentTermsDocResults.size() > 0)
		{
			DocumentHandler latestShipemntTermsDoc =  shipmentTermsDocResults.get(0);
			try
			{
				currentLatestShipmentDate = dbDateFormatter.parse(latestShipemntTermsDoc.getAttribute("LATEST_SHIPMENT_DATE"));
			}
			catch(ParseException e)
			{
				LOG.error("Exception while parsing latest shipment dat: ",e);
			}

		}
	}

	boolean deriveDates = currentLatestShipmentDate != null;

	deriveTransactionDataFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms,
								  null, -1, listOfPoOids, definitionOid, sessionContext,
								  instrument, currentLatestShipmentDate, matchPOLineItems, deriveDates);
   }



   
   /**
	 * Derive transaction data (such as amount, expiry_date, ShipmentTerms'
	 * last_shipment_date, goods description) from PO Line Items.  Retrieve
	 * information from the component (which is probably not saved to database).
	 *
	 * @param transaction Transaction - The Transaction to derive data from its
	 * POs.  Required
	 * @param shipmentTerms ShipmentTerms - The handle to the ShipmentTerms.
	 * Required to avoid transactional error when accessing from outside EJB.
	 * @param numberOfShipmentTerms int - number of shipment terms in the
	 * transaction.  Used to determine whether need to query the other
	 * ShipmentTerms.
	 * @param poLineItemList ComponentList - Handle to the POLineItemList
	 * component.  Pass in this argument only if this component is instantiated
	 * and not saved to the database yet. Otherwise pass in null and then
	 * deriveTransactionDataFromPO will query db for all the PO Line Items.
	 * @param numberOfPOLineItems int - Number of PO Line Items.  Required if
	 * poLineItemList is not null to avoid transaction conflict.
	 * @param definitionOid - the OID of the PO definition of the POs
	 * @param sessionContext - context of the calling EJB
	 * @param locale - users locale
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static void deriveTransactionDataFromPO(boolean isStructuredPO,Transaction transaction,
													   ShipmentTerms shipmentTerms,
													   int numberOfShipmentTerms,
													   ComponentList poLineItemList,
													   int numberOfPOLineItems,
													   long definitionOid,
													   SessionContext sessionContext,
													   String locale) throws AmsException, RemoteException
	{
		 userLocale = locale;
		 deriveTransactionDataFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms, poLineItemList, numberOfPOLineItems,
									   null, definitionOid, sessionContext, null, null, null, true);
	}


   /**
	* Master method used to derive fields from POs.  It can accept a variety
		* of combinations of parameters in its public methods (see public overloads
		* in this class)
		*
	* @param poLineItemList ComponentList - Handle to the POLineItemList
	* component.  Pass in this argument only if this component is instantiated
	* and not saved to the database yet. Otherwise pass in null and then
	* deriveTransactionDataFromPO will query db for all the PO Line Items.
	* @param numberOfPOLineItems int - Number of PO Line Items.  Required if
	* poLineItemList is not null to avoid transaction conflict.
		* @param uploadedPOsInShipment - a vector containing strings that are the OIDs of all POs
		* to derive from
		* @param definitionOid - the OID of the PO definition of the POs
		* @param sessionContext - context of the calling EJB
	 * @param instrument - Instrument object if this is an amendment
	 * transaction, in which case we update the expiry date only if the
	 * last_ship_dt is later than the current expiry date
	 * @param currentLatestShipmentDate - Current latest shipment date if this
	 * is an amendment, in which case we update the latest shipment date only if
	 * the last_ship_dt is later than the current latest shipment date
	 * @param deriveDates - boolean whether to derive dates.  There is a rare
	 * occastion in amendment where we do not want to derive dates.
		*
	* @throws AmsException
	* @throws RemoteException
	*/
   private static void deriveTransactionDataFromPO(boolean isStructuredPO,Transaction transaction,
													ShipmentTerms shipmentTerms,
													int numberOfShipmentTerms,
													ComponentList poLineItemList,
													int numberOfPOLineItems,
													Vector newListOfPoOidsAdded,
													long definitionOid,
													SessionContext sessionContext,
													Instrument instrument,
													java.util.Date currentLatestShipmentDate,
													DocumentHandler matchPOLineItems,
													boolean deriveDates) throws AmsException, RemoteException
   {
		   // These parameters are required
	   if (transaction == null || shipmentTerms == null) {
		  throw new AmsException("Invalid argument to POLineItemUtility.deriveTransactionDataFromPO()");
	   }

		   // One of these two parameters is required
		   if((poLineItemList == null) && (newListOfPoOidsAdded == null))
			{
		  throw new AmsException("Arguments to POLineItemUtility.deriveTransactionDataFromPO() must include either a component list of POs or a Vector of uploaded PO OIDs in the shipment");
			}

		   String newListOfPoOidsAddedSql = null;

		   // Create SQL for uploaded POs that are now a part of this shipment
		   if((newListOfPoOidsAdded != null) && (newListOfPoOidsAdded.size() > 0))
			{
			   String requiredOid = isStructuredPO?"purchase_order_oid":"po_line_item_oid";//SHR CR 707 rel8.1.1
			   newListOfPoOidsAddedSql = getPOLineItemOidsSql(newListOfPoOidsAdded,requiredOid);
			}

	   // Derive the transaction amount if there is no matching po line items
	   // Will set the amount of the transaction to the sum of all po line item amounts.
	   // If there are matching PO line items in previously authorised transaction, the
	   // amount of the (amendment) transaction should be the difference between the current
	   // po line items and the matching po line items.  This logic is not handled for now.
	  if (!isStructuredPO)
	 //  {
	   deriveTransactionCurrencyAndAmountsFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms, poLineItemList,
											  numberOfPOLineItems, newListOfPoOidsAddedSql, instrument,null);
	 //  }

	   // Derive the dates.  The ParseException should not happen.
	   if (deriveDates) {

			deriveTransactionDatesFromPO(isStructuredPO,transaction, shipmentTerms, numberOfShipmentTerms, poLineItemList,
											 numberOfPOLineItems, newListOfPoOidsAddedSql,
											 sessionContext, instrument, currentLatestShipmentDate);
	
	   }

	   transactionCurrency = transaction.getAttribute("copy_of_currency_code");
	   if(transactionCurrency.equals("") || (transactionCurrency == null))
	   {
		 numberOfDecimalPlaces = 2;
	   }
	   else
	   {
		 numberOfDecimalPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, transactionCurrency));
	   }
	
	
	   if(!isStructuredPO){
	   // Derive the goods description.
	   deriveGoodsDescriptionFromPO(shipmentTerms, poLineItemList, newListOfPoOidsAdded, numberOfPOLineItems);
	   }
	}
   
   
	
   /**
 * @param isStructurePO
 * @param transaction
 * @param shipmentTerms
 * @param uploadedPOsInShipmentSql
 * @param matchPOLineItems
 * @return
 * @throws AmsException
 * @throws RemoteException
 * calculates the amount difference for uploaded PO amendments
 */
   
private static BigDecimal getMatchAmount(boolean isStructurePO,Transaction transaction,ShipmentTerms shipmentTerms,String uploadedPOsInShipmentSql,
		Instrument instrument, String newListOfPoNumsAddedSql)  throws AmsException, RemoteException{
	     
	      double oldTotalPOAmt =0.00;
		 
		   if (uploadedPOsInShipmentSql != null ) {
			   String sb = null;
			   if(isStructurePO){
				   
					sb = "select sum(amount) as AMOUNT from purchase_order where a_instrument_oid=? and " +newListOfPoNumsAddedSql+ "and a_transaction_oid = ?";
		   }
		   
			   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb, false, transaction.getAttribute("instrument_oid"), instrument.getAttribute("active_transaction_oid"));
				 List<DocumentHandler> results = resultSet.getFragmentsList("/ResultSetRecord");
				  for (DocumentHandler row:results) {
					  oldTotalPOAmt += StringFunction.isNotBlank(row.getAttribute("/AMOUNT"))?row.getAttributeDecimal("/AMOUNT").doubleValue():0;
						 
				  }
		
		   }
		  
		return  new BigDecimal(oldTotalPOAmt);
	}
	/**
	 * Derive amount from PO Line Items.
	 * @param transaction - Transaction
	 * @param shipmentTerms - ShipmentTerms
	 * @param numberOfShipmentTerms - long - helps to determine if we need
	 * to retrieve the PO Line Items for the other ShipmentTerms.
	 * @param poLineItemList - ComponentList of the POs associated with the shipment
	 * @param uploadedPOsInShipmentSql - SQL to determine POs
		 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private static void deriveTransactionCurrencyAndAmountsFromPO(boolean isStructurePO,Transaction transaction,
								   ShipmentTerms shipmentTerms,
								   int numberOfShipmentTerms,
								   ComponentList poLineItemList,
								   int numberOfPOLineItems,
								   String newListOfPoOidsAddedSql,
								   Instrument instrument,DocumentHandler matchPOLineItems) throws AmsException, RemoteException
	{
	   double       totalAmountOfCurrTranFromPO                  = 0.00;

	   Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");

	   int          numberOfAllPOLineItems = numberOfPOLineItems;
	   String       poCurrency = null;

	   // Total up the POs from the instantiaed POLineItemList components.
	   // These PO Line Items may not have been saved to database.
	   if (poLineItemList != null) {
		  String poLineItemAmount = null;
		  for (int i = 0; i < numberOfPOLineItems; i++) {
			 poLineItemList.scrollToObjectByIndex(i);
					 poLineItemAmount = poLineItemList.getListAttribute("amount");
			 if (StringFunction.isNotBlank(poLineItemAmount)) {
				 totalAmountOfCurrTranFromPO += Double.valueOf(poLineItemAmount).doubleValue();
			 }
			 if (poCurrency == null) {
				poCurrency = poLineItemList.getListAttribute("currency");
			 }
		  }
	   }

	   /* ********************
		Total up the POs in the database that were not in the component list or
		passed in as an updated PO.
		This will happen when the shipment contains uploaded POs or when there
		are multiple shipments.  We want to make sure to exclude the
		auto uploaded POs since they match the existing POs and we cannot just
		add up their amounts to derive the transaction amount.
		Example SQL:
		select sum(amount) as amount, max(currency) as currency, count(*) as
	   pocount from po_line_item
		where ((auto_added_to_amend_ind is null or auto_added_to_amend_ind <>'Y')
				and ((a_assigned_to_trans_oid = 809114 and p_shipment_oid <> 809117)
					 or (po_line_item_oid in(809052, 798136))));
		 ************/
	   if ((newListOfPoOidsAddedSql != null || numberOfShipmentTerms > 1)) {
		   String sqlStatement = null;
	   
		   if(!isStructurePO){
		  // Start by creating a where clause that looks for all POs for this transaction
		  // excluding the ones in the current shipment
		 
		  sqlStatement = "select sum(amount) as amount, max(currency) as currency, count(*) as pocount from po_line_item "
				  +	" where ((auto_added_to_amend_ind is null or auto_added_to_amend_ind <> 'Y') and ((a_assigned_to_trans_oid = ? and p_shipment_oid <> ?)";

		  // Also include any POs that are part of this shipment
		  // If a component list was passed in above, nothing will happen here
		  // If a vector of uploaded PO oids was passed in, those POs will be included here
		  if (newListOfPoOidsAddedSql != null)  {
			sqlStatement += " or (" + newListOfPoOidsAddedSql + ")";
		  }
		  sqlStatement += "))";
		   }
		   
		  DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, transaction.getAttribute(transaction.getIDAttributeName()), shipmentTerms.getAttribute(shipmentTerms.getIDAttributeName()));
		  resultSet = resultSet.getFragment("/ResultSetRecord");

		  // Retrieve the total for the SQL query that was just constructed

		  try {
			  totalAmountOfCurrTranFromPO += resultSet.getAttributeDecimal("/AMOUNT").doubleValue();
		  }
		  catch(Exception e) {
			 /* Ignore null amount */
		  }
		 numberOfAllPOLineItems = numberOfPOLineItems + resultSet.getAttributeInt("/POCOUNT");

		  if (poCurrency == null) {
			  poCurrency = resultSet.getAttribute("/CURRENCY");
		  }
	   }
	   // Format the amount according to the currency.
	   BigDecimal totalAmount = new BigDecimal(totalAmountOfCurrTranFromPO);
	

	   // Update currency of the issuance if the PO currency is valid
	   // Amendment does not derive the currency from POs.
	   if (poCurrency!=null && !poCurrency.equals("")
		   && transaction.getAttribute("transaction_type_code").equals(TransactionType.ISSUE)
		   && ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", poCurrency))
	   {
		   terms.setAttribute("amount_currency_code", poCurrency);
		   transaction.setAttribute("copy_of_currency_code", poCurrency);
	   }

	   String trnxCurrencyCode = transaction.getAttribute("copy_of_currency_code");
	   int numberOfCurrencyPlaces = 2;

	   if(trnxCurrencyCode != null && !trnxCurrencyCode.equals("")){
		 numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, trnxCurrencyCode));
	   }
	   totalAmount = totalAmount.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
	   BigDecimal totalInstrumentAmount = totalAmount;
	   if(transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND))
	   {
		   totalInstrumentAmount = totalAmount.add(instrument.getAttributeDecimal("copy_of_instrument_amount"));
		   totalInstrumentAmount = totalInstrumentAmount.setScale(numberOfCurrencyPlaces, BigDecimal.ROUND_HALF_UP);
	   }

	   // Set the amount fields to the total amount just calculated
	   // unless there are PO Line Items but their amounts are not entered, in which
	   // case leave the transaction amount as it is.
	   if ( totalAmountOfCurrTranFromPO == 0 && numberOfAllPOLineItems > 0) {/*no-op*/ }
	   else if(!isStructurePO)
	   {
			// DK IR T36000013882 Rel9.0 06/05/2014 starts
		   if(totalAmount.intValue() != 0) {
			   transaction.setAttribute("copy_of_amount", String.valueOf(totalAmount));
			}
			else {
				transaction.setAttribute("copy_of_amount","");
			}
	
		 transaction.setAttribute("instrument_amount", String.valueOf(totalInstrumentAmount));

		 terms.setAttribute("amount", String.valueOf(totalAmount));

	   }
	}

	/**
	 * Derive expiry date and latest shipment date from PO Line Items.
     *
	 * @param isStructuredPO
     * @param transaction - Transaction
	 * @param shipmentTerms - ShipmentTerms
	 * @param numberOfShipmentTerms = int
	 * @param poLineItemList - ComponentList of the POs associated with the shipment
     * @param numberOfPOLineItems
	 * @param uploadedPOsInShipmentSql - SQL to determine POs
	 * @param sessionContext - context of the calling EJB
	 * @param instrument - Instrument object if this is an amendment
	 * transaction, in which case we update the expiry date only if the
	 * last_ship_dt is later than the current expiry date
	 * @param currentLatestShipmentDate - Current latest shipment date if this
	 * is an amendment, in which case we update the latest shipment date only if
	 * the last_ship_dt is later than the current latest shipment date
	 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private static void deriveTransactionDatesFromPO(boolean isStructuredPO,Transaction transaction,
							 ShipmentTerms shipmentTerms,
							 int numberOfShipmentTerms,
							 ComponentList poLineItemList,
							 int numberOfPOLineItems,
							 String uploadedPOsInShipmentSql,
							 SessionContext sessionContext,
							 Instrument instrument,
							 java.util.Date currentLatestShipmentDate) throws AmsException, RemoteException
	{
	   SimpleDateFormat   dbDateFormatter             = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
	   DocumentHandler    lastShipmentDatesDoc        = null;
	   StringBuilder       sqlQuery                    = null;
	   java.util.Date     transLastShipDate           = null;
	   java.util.Date  	  poLastShipDate              = null;
	   java.util.Date     shipmentLastShipDate        = null;
	   Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");

	   /////////////////////////////////////////////////////////////////////////
	   //
	   // Update the latest_shipment_date on ShipmentTerms
	   //
	   /////////////////////////////////////////////////////////////////////////
	   // Get the latest shipment date for all the PO Line Items belonging to the ShipmentTerms.
	   // Get from the POLineItemList component or retrieve from database depending on whether the
	   // component has been instantiated.
	   if (poLineItemList != null) { // component instantiaed.  Must retrieve from component since maybe not saved.
		   // Get the PO Definition's date format
		   String poLastShipDate_String;
		   for (int i = 0; i < numberOfPOLineItems; i++) {
			   poLineItemList.scrollToObjectByIndex(i);
			   poLastShipDate_String = poLineItemList.getListAttribute("last_ship_dt");
			   if (!poLastShipDate_String.equals("")) {
				   poLastShipDate = DateTimeUtility.convertStringDateTimeToDate(poLastShipDate_String);
				   if (shipmentLastShipDate == null || poLastShipDate.after(shipmentLastShipDate) ) {
					   shipmentLastShipDate = poLastShipDate;
				   }
			   }
		   }
	   }
	   else if(uploadedPOsInShipmentSql != null)
	   {
		   // component not instantiated, retrieve from database using DatabaseQueryBean using PO list passed in
		   sqlQuery = new StringBuilder();
		 
		   if(isStructuredPO){
			   sqlQuery.append("select max(latest_shipment_date) as last_ship_dt from purchase_order where ");
		   }else{
		   sqlQuery.append("select max(last_ship_dt) as last_ship_dt from po_line_item where ");
		   }
		
		   sqlQuery.append(uploadedPOsInShipmentSql);
		   lastShipmentDatesDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(),false,new ArrayList<Object>());
		
		   String shipmentLastShipDate_String = null;
		   if(lastShipmentDatesDoc!=null)
			   shipmentLastShipDate_String = lastShipmentDatesDoc.getAttribute("/ResultSetRecord(0)/LAST_SHIP_DT");
	
		   // W Zhu 8/6/07 NBUH013160710 Issue #1 BEGIN Add try catch block to silently ignore empty date.
		   try {
			   if (StringFunction.isNotBlank(shipmentLastShipDate_String)) shipmentLastShipDate = dbDateFormatter.parse(shipmentLastShipDate_String);
	   	   }
		   catch (ParseException e) {
			// The date format has been validated when the po file was uploaded.  The only  
			// parse error that could happen is that there is no last_ship_dt in PO file.  This
			// is allowed.  We just do not derive date.
		   }
		   // W Zhu 8/6/07 NBUH013160710 Issue #1 END
	   }


	   // Update the latest_shipment_date on ShipmentTerms
	   if (shipmentLastShipDate != null
			   && (currentLatestShipmentDate == null || shipmentLastShipDate.after(currentLatestShipmentDate)))
	   {
	
		 Date shipmentTermsLatestShipmentDate = null;
		 if (StringFunction.isNotBlank(shipmentTerms.getAttribute("latest_shipment_date"))) {
			 shipmentTermsLatestShipmentDate = DateTimeUtility.convertStringDateToDate(shipmentTerms.getAttribute("latest_shipment_date"));
		 }
		 
		 if (shipmentTermsLatestShipmentDate == null ||
				 shipmentLastShipDate.after(shipmentTermsLatestShipmentDate)) {
			 shipmentTerms.setAttribute("latest_shipment_date", DateTimeUtility.convertDateToDateTimeString(shipmentLastShipDate));
		 }
	
	   }


	   /////////////////////////////////////////////////////////////////////
	   //
	   // Use the latest of the last_ship_dt to update expiry_date on Terms
	   //
	   /////////////////////////////////////////////////////////////////////


	   if (numberOfShipmentTerms == 1) {
		  transLastShipDate = shipmentLastShipDate;
	   }
	   else {
		// Get the latest shipment date for the transaction that PO line items are
		// being added to and the POs that are being added
		sqlQuery = new StringBuilder();
		List<Object> sqlPrmsLst = new ArrayList();
	
		 if(isStructuredPO){
			   sqlQuery.append("select max(latest_shipment_date) as last_ship_dt from purchase_order where (a_transaction_oid = ?) and (a_shipment_oid <> ?");
			   sqlPrmsLst.add(transaction.getAttribute("transaction_oid"));
		   }
		 else{
			   sqlQuery.append("select max(last_ship_dt) as last_ship_dt from po_line_item where (a_assigned_to_trans_oid = ?) and (p_shipment_oid <> ?");
			   sqlPrmsLst.add(transaction.getAttribute("transaction_oid"));
		 }
	
		 		sqlPrmsLst.add(shipmentTerms.getAttribute("shipment_oid"));
			   sqlQuery.append(") ");

		lastShipmentDatesDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlPrmsLst);
				if(lastShipmentDatesDoc == null)
				  lastShipmentDatesDoc = new DocumentHandler();

		String transLastShipDate_String = lastShipmentDatesDoc.getAttribute("/ResultSetRecord(0)/LAST_SHIP_DT");
				if (!StringFunction.isBlank(transLastShipDate_String))
				{
					java.util.Date otherShipmentsLastShipDate = null;
				  // This is the latest last ship date from all other shipments
					try {
						otherShipmentsLastShipDate = dbDateFormatter.parse(transLastShipDate_String);
					}
				
					catch (ParseException e) {
						// If there is no existing last_ship_dt, it is allowed.
					}
			

				  // If the latest last ship date from the other shipments is later than the
				  // shipment's last ship date, set the transLastShipDate
				  if((otherShipmentsLastShipDate != null) && (shipmentLastShipDate != null) &&
					 (otherShipmentsLastShipDate.getTime() > shipmentLastShipDate.getTime()))
				  transLastShipDate = otherShipmentsLastShipDate;
				  else
			  transLastShipDate = shipmentLastShipDate;
				}
	   }

	   // Set the expiry date for the transaction
		   if (transLastShipDate != null)
		   {

				 Date expiryDate = null;
				 if (StringFunction.isNotBlank(terms.getAttribute("expiry_date"))) {
					 expiryDate = DateTimeUtility.convertStringDateToDate(terms.getAttribute("expiry_date"));
				 }
				 
				 if (expiryDate == null ||
						 transLastShipDate.after(expiryDate)) {
					 terms.updateExpiryDateFromPO(instrument, transLastShipDate);
				 }
			
		   }

   }


	/**
	 * Derive goods description from PO Data
		 *
	 * @param shipmentTerms - ShipmentTerms
	 * @param poLineItemList - ComponentList of the POs associated with the shipment
		 * @param uploadedPOsInShipment - Vector of strings of PO OIds
	 * @param numberOfPOLineItems - int Needed if poLineItemList is not null to
	 * avoid transaction conflict.
		 *
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private static void deriveGoodsDescriptionFromPO(ShipmentTerms shipmentTerms,
													   ComponentList poLineItemList,
														   Vector uploadedPOsInShipment,
													   int numberOfPOLineItems)
													   throws AmsException, RemoteException
	{
	  String poLineItems = "";

	  if(poLineItemList != null)
	  {
	
		// If POLineItemList component is instantiated, get data from the component.
		poLineItems = GoodsDescriptionUtility.buildGoodsDescription(poLineItemList, numberOfPOLineItems, userLocale, numberOfDecimalPlaces);

	  }
	  else if((uploadedPOsInShipment != null) && (uploadedPOsInShipment.size() > 0))
	  {
	
		// If POLineItemList component is not instantiated, get data from querying the database
		// and possibly passing in a single, updated PO Line Item also
		poLineItems = GoodsDescriptionUtility.buildGoodsDescription(uploadedPOsInShipment, userLocale, numberOfDecimalPlaces);
	
	  }

		  shipmentTerms.setAttribute("po_line_items", poLineItems);
	}


    public static void createHistoryLog(Vector poOids, String poAction, String poStatus, String userOid) throws AmsException {
        String historySQl = "INSERT into PURCHASE_ORDER_HISTORY (PURCHASE_ORDER_HISTORY_OID, ACTION_DATETIME, ACTION, " +
                "STATUS, P_PURCHASE_ORDER_OID, A_USER_OID) VALUES (?,?,?,?,?,?)";
        
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        java.sql.Timestamp timeStamp = java.sql.Timestamp.valueOf(sd.format(DateTimeUtility.convertStringDateTimeToDate(DateTimeUtility.getGMTDateTime())));
        
        try(Connection con = DatabaseQueryBean.connect(true);
            PreparedStatement pstmt = con.prepareStatement(historySQl)) {
            for (Object poOid : poOids){
                String poOID = (String)poOid;
                pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());

                pstmt.setTimestamp(2, timeStamp);
                pstmt.setString(3, poAction);
                pstmt.setString(4,poStatus);
                pstmt.setLong(5, Long.parseLong(poOID));
                pstmt.setLong(6, Long.parseLong(userOid));
                pstmt.addBatch();
            }
            int[] succesCount = pstmt.executeBatch();
            if (succesCount.length != poOids.size()){
                //TODO Log errors.
            }
            con.commit();
            
        } catch (AmsException e) {
            throw new AmsException(e);
        } catch (SQLException e) {
            throw new AmsException(e);
        }
    }
    
	private static void deriveTranCurrencyAndAmountsFromStrucPO(
			boolean isStructurePO, Transaction transaction,
			ShipmentTerms shipmentTerms, int numberOfShipmentTerms,
			ComponentList poLineItemList, int numberOfPOLineItems,
			String newListOfPoOidsAddedSql, Instrument instrument,
			DocumentHandler matchPOLineItems,DocumentHandler outputDoc,String listOfPoNums) throws AmsException,
			RemoteException {
		double totalAmountOfCurrTranFromPO = 0.00;

		Terms terms = (Terms) transaction
				.getComponentHandle("CustomerEnteredTerms");

		String poCurrency = null;


		// Total up the POs from the instantiaed POLineItemList components.
		// These PO Line Items may not have been saved to database.
		BigDecimal oldTotalPOAmt = BigDecimal.ZERO;
		BigDecimal offSetAmt =  BigDecimal.ZERO;
		BigDecimal newTotalPOAmt =  BigDecimal.ZERO;
		if ((newListOfPoOidsAddedSql != null || numberOfShipmentTerms > 1)) {
			StringBuilder sqlStatement = new StringBuilder();

				sqlStatement.append("select sum(amount) as amount, max(currency) as currency, count(*) as pocount from purchase_order");
				sqlStatement.append(" where (a_transaction_oid = ?)");
				if (newListOfPoOidsAddedSql != null) {
					sqlStatement.append(" or (");
					sqlStatement.append(newListOfPoOidsAddedSql);
					sqlStatement.append(")");
				}

			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), false, new Object[]{transaction.getAttribute(transaction.getIDAttributeName())});
			resultSet = resultSet.getFragment("/ResultSetRecord");

			// Retrieve the total for the SQL query that was just constructed

			try {
				totalAmountOfCurrTranFromPO += resultSet.getAttributeDecimal(
						"/AMOUNT").doubleValue();
			} catch (Exception e) {
				/* Ignore null amount */
			}
		
			oldTotalPOAmt = BigDecimal.ZERO;
			offSetAmt = BigDecimal.ZERO;
			newTotalPOAmt = new BigDecimal(totalAmountOfCurrTranFromPO);
			String trnxCurrencyCode = transaction
			.getAttribute("copy_of_currency_code");
			int numberOfCurrencyPlaces = 2;

			if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
				numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
						.getRefDataMgr().getAdditionalValue(
								TradePortalConstants.CURRENCY_CODE,
								trnxCurrencyCode));
			}
			LOG.debug("New PO Amount in the transaction: {}" ,newTotalPOAmt);
			if (transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND)) {
				oldTotalPOAmt = getMatchAmount(isStructurePO, transaction,
						shipmentTerms, newListOfPoOidsAddedSql,
						instrument,listOfPoNums);
				LOG.debug("Old PO Matching Amount: {}" ,oldTotalPOAmt);
				boolean haveOldAmt = false;
				if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == 1) {
					offSetAmt = oldTotalPOAmt.subtract(newTotalPOAmt);
					offSetAmt = offSetAmt.negate();
					haveOldAmt = true;
				} else if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == -1) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				}else if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == 0) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				}
				newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces,
						BigDecimal.ROUND_HALF_UP);
				if(offSetAmt.intValue() != 0){
				offSetAmt = offSetAmt.setScale(numberOfCurrencyPlaces,
						BigDecimal.ROUND_HALF_UP);
				}
				else if(!haveOldAmt){
					offSetAmt =	newTotalPOAmt;
				}
				LOG.debug("OffSet Amount in the transaction: {}" ,offSetAmt);
	
				if(offSetAmt.intValue() != 0) {
					transaction.setAttribute("copy_of_amount",
						String.valueOf(offSetAmt));
				}
				else {
					transaction.setAttribute("copy_of_amount","");
				}
		
				transaction.setAttribute("instrument_amount",
						String.valueOf(oldTotalPOAmt));
	
				
				terms.setAttribute("amount", String.valueOf(offSetAmt));
			}
			else{
				if (totalAmountOfCurrTranFromPO != 0 ) {
					newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces,
							BigDecimal.ROUND_HALF_UP);
					transaction.setAttribute("copy_of_amount",
							String.valueOf(newTotalPOAmt));
					transaction.setAttribute("instrument_amount",
							String.valueOf(oldTotalPOAmt));

					terms.setAttribute("amount", String.valueOf(newTotalPOAmt));
				}
			}
		
			if (poCurrency == null) {
				poCurrency = resultSet.getAttribute("/CURRENCY");
			}
		
		

		// Update currency of the issuance if the PO currency is valid
		// Amendment does not derive the currency from POs.
		if (poCurrency != null
				&& !poCurrency.equals("")
				&& transaction.getAttribute("transaction_type_code").equals(
						TransactionType.ISSUE)
				&& ReferenceDataManager.getRefDataMgr().checkCode(
						"CURRENCY_CODE", poCurrency)) {
			terms.setAttribute("amount_currency_code", poCurrency);
			transaction.setAttribute("copy_of_currency_code", poCurrency);
			}
		}
		else{
			transaction.setAttribute("copy_of_amount","");
			terms.setAttribute("amount", "");
		}
		
		// Set the amount fields to the total amount just calculated
		// unless there are PO Line Items but their amounts are not entered, in
		// which
		// case leave the transaction amount as it is.
		
	}

	public static void deriveTranDataFromStrucPO(boolean isStructuredPO,
			Transaction transaction, ShipmentTerms shipmentTerms,
			int numberOfShipmentTerms, Vector listOfPoOids, long definitionOid,
			SessionContext sessionContext,Instrument instrument, String locale,
			DocumentHandler outputDoc, Vector listOfPoNums) throws AmsException, RemoteException {
		userLocale = locale;
		deriveTranDataFromStruPO(isStructuredPO, transaction, shipmentTerms,
				numberOfShipmentTerms, null, 0, listOfPoOids, definitionOid,
				sessionContext, instrument, null, null, true, outputDoc,listOfPoNums);
	}

	public static void deriveTranDataFromStruPO(boolean isStructuredPO,
			Transaction transaction, ShipmentTerms shipmentTerms,
			int numberOfShipmentTerms, ComponentList poLineItemList,
			int numberOfPOLineItems, Vector newListOfPoOidsAdded,
			long definitionOid, SessionContext sessionContext,
			Instrument instrument, java.util.Date currentLatestShipmentDate,
			DocumentHandler matchPOLineItems, boolean deriveDates,
			DocumentHandler outputDoc,Vector listOfPoNums) throws AmsException, RemoteException {
		// These parameters are required
		if (transaction == null || shipmentTerms == null) {
			throw new AmsException(
					"Invalid argument to POLineItemUtility.deriveTransactionDataFromPO()");
		}

		// One of these two parameters is required
		if ((poLineItemList == null) && (newListOfPoOidsAdded == null)) {
			throw new AmsException(
					"Arguments to POLineItemUtility.deriveTranDataFromStruPO() must include either a component list of POs or a Vector of uploaded PO OIDs in the shipment");
		}

		String newListOfPoOidsAddedSql = null;
		String newListOfPoNumsAddedSql = null;

		// Create SQL for uploaded POs that are now a part of this shipment
		if ((newListOfPoOidsAdded != null) && (newListOfPoOidsAdded.size() > 0)) {
			String requiredOid = isStructuredPO ? "purchase_order_oid"
					: "po_line_item_oid";// SHR CR 707 rel8.1.1
			newListOfPoOidsAddedSql = getPOLineItemOidsSql(
					newListOfPoOidsAdded, requiredOid);
		}
		if ((listOfPoNums != null) && (listOfPoNums.size() > 0)) {
			String requiredOid =  "purchase_order_num";
			newListOfPoNumsAddedSql = getPONumsSql(
					listOfPoNums, requiredOid);
		}

		// Derive the transaction amount if there is no matching po line items
		// Will set the amount of the transaction to the sum of all po line item
		// amounts.
		// If there are matching PO line items in previously authorised
		// transaction, the
		// amount of the (amendment) transaction should be the difference
		// between the current
		// po line items and the matching po line items. This logic is not
		// handled for now.
			deriveTranCurrencyAndAmountsFromStrucPO(isStructuredPO,
					transaction, shipmentTerms, numberOfShipmentTerms,
					poLineItemList, numberOfPOLineItems,
					newListOfPoOidsAddedSql, instrument, matchPOLineItems,
					outputDoc,newListOfPoNumsAddedSql);
		// Derive the dates. The ParseException should not happen.
		if (deriveDates) {
		
			deriveTransactionDatesFromPO(isStructuredPO, transaction,
					shipmentTerms, numberOfShipmentTerms, poLineItemList,
					numberOfPOLineItems, newListOfPoOidsAddedSql,
					sessionContext, instrument, currentLatestShipmentDate);

		}

	
		transactionCurrency = transaction.getAttribute("copy_of_currency_code");
		if (transactionCurrency.equals("") || (transactionCurrency == null)) {
			numberOfDecimalPlaces = 2;
		} else {
			numberOfDecimalPlaces = Integer.parseInt(ReferenceDataManager
					.getRefDataMgr().getAdditionalValue(
							TradePortalConstants.CURRENCY_CODE,
							transactionCurrency));
		}

	}
	

	 /**
	  * This method updates the a_instrument_oid/a_transaction_oid details for the invoices
	  * in INVOICES_SUMMARY_DETAIL table.
	  * @param poOids
	  * @param transactionOid
	  * @param instrumentOid
	  * @param status
	 * @param termsOid 
	  * @throws AmsException
	  */
	 public static void updateInvoiceTransAssignment(Vector poOids, String transactionOid, String instrumentOid, String status, String termsOid)
	            throws AmsException
	    {
	        StringBuilder   sql         = new StringBuilder();
	        String         poListSql   = null;
	        int            resultCount = 0;

	        try
	        {
	            // Construct a comma separated list of the po oids within
	            // parenthesis
	        	String requiredOid = "upload_invoice_oid";
	           

	            Object[] arguments = {"?", "?", "?", "?"};

	            sql.append(MessageFormat.format(INVOICE_MASS_UPDATE_SQL, arguments));
	            sql.append(" ,    status = ? ");
	            List<Object> paramList = new ArrayList<Object>();
				paramList.add(transactionOid);
				paramList.add(instrumentOid);
				paramList.add(termsOid);
				paramList.add(TradePortalConstants.PO_STATUS_ASSIGNED);
				paramList.add(status);
	            sql.append(" where ");
	            poListSql = getInvoiceItemOidsSqlWithPlaceHolder(poOids,requiredOid,paramList);
	            sql.append(poListSql);

	            resultCount = DatabaseQueryBean.executeUpdate(sql.toString(),false, paramList);
	            LOG.debug("updateInvoiceTransAssignment() resultCount: {}", resultCount);
	        }
	        catch (SQLException e)
	        {
	            throw new AmsException("SQL Exception found executing sql statement \n"
	                    + sql
	                    + "\nException is " + e.getMessage());
	        }
	    }
	
	 /**
	  * This method prepares sql for the selected invoices
	  * @param invoiceItemOidsList
	  * @param oid
	  * @return
	  */
	public static String getInvoiceItemOidsSql(Vector invoiceItemOidsList,String oid)

	{
		StringBuilder sql = null;
		int numberOfInvoiceItems = 0;
		int allPos = 0;
		numberOfInvoiceItems = invoiceItemOidsList.size();
		sql = new StringBuilder();
		sql.append("(");
		sql.append(oid);
		sql.append(" in (");
	
		//if InvoiceLineItems are < 1000 then just create normal sql
		if (numberOfInvoiceItems <= 1000) {
			allPos = numberOfInvoiceItems;
			sql = getAllInvoiceOidsSql(invoiceItemOidsList, allPos, sql, 0);
		} 
		
		//if InvoiceLineItems are >1000 then break the items for every 1000 and create new 'IN' clause with OR condition
		 
		else {
			int quotient = numberOfInvoiceItems / 1000;
			int remainder = numberOfInvoiceItems % 1000;
			allPos = numberOfInvoiceItems;
			for (int i = 1; i <= quotient; i++) {
				sql = getAllInvoiceOidsSql(invoiceItemOidsList, 1000 * i,
						sql, (1000 * i) - 1000);
				if (i != quotient)	{
					sql.append(" or ");
					sql.append(oid);
					sql.append(" in (");
				}
				else if (remainder > 0)
				sql.append(" or ");
			
			}

			if (remainder > 0) {				
				sql.append(oid);
				sql.append(" in (");
				sql = getAllInvoiceOidsSql(invoiceItemOidsList,
						numberOfInvoiceItems, sql, numberOfInvoiceItems 
						- remainder);
			}
		}
		
		sql.append(") ");
		return sql.toString();
	}
	
	/**
	 * This method preprares sql for the list of invoices
	 * @param invoiceItemOidsList
	 * @param numberOfInvoiceItems
	 * @param sql
	 * @param n
	 * @return
	 */
	public static StringBuilder getAllInvoiceOidsSql(
			Vector invoiceItemOidsList, int numberOfInvoiceItems,
			StringBuilder sql, int n) {
		for (int i = n; i < numberOfInvoiceItems; i++) {
			sql.append("'");
			sql.append(SQLParamFilter.filter((String) invoiceItemOidsList.elementAt(i)));
			sql.append("'");
			if (i != (numberOfInvoiceItems - 1)) {
				sql.append(", ");
			}
		}
		sql.append(")");
		return sql;
	}
	
	/**
	 * jgadela  R92 - SQL Injection FIX - Added this method as part of SQL injectionfix
	 * This method preprares sql for the list of invoices
	 * @param invoiceItemOidsList
	 * @param numberOfInvoiceItems
	 * @param sql
	 * @param n
	 * @return
	 */
	public static StringBuilder getAllInvoiceOidsPlaceHolderSql(
			Vector invoiceItemOidsList, int numberOfInvoiceItems,
			StringBuilder sql, int n, List<Object> sqlParamsLsts) {
		for (int i = n; i < numberOfInvoiceItems; i++) {
			sql.append("?");
			sqlParamsLsts.add((String) invoiceItemOidsList.elementAt(i));
			if (i != (numberOfInvoiceItems - 1)) {
				sql.append(", ");
			}
		}
		sql.append(")");
		return sql;
	}
	
	
     /**
      * This static method logs the invoice details in INVOICE_HISTORY table which are loan request thru rules	
      * @param invoiceOids
      * @param invoiceAction
      * @param invoiceStatus
      * @param userOid
      * @throws AmsException
      */
	 public static void createInvoiceHistoryLog(Vector invoiceOids, String invoiceAction, String invoiceStatus, String userOid) throws AmsException {
		 
	        String historySQl = "INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, A_USER_OID,P_UPLOAD_INVOICE_OID,INVOICE_STATUS) " +
	                " VALUES (?,?,?,?,?,?)";
	        
	        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
	        java.sql.Timestamp timeStamp = java.sql.Timestamp.valueOf(sd.format(DateTimeUtility.convertStringDateTimeToDate(DateTimeUtility.getGMTDateTime())));
	        
	        try(Connection con = DatabaseQueryBean.connect(true);
		            PreparedStatement pstmt = con.prepareStatement(historySQl)) {
	            
	            for (Object poOid : invoiceOids){
	                String poOID = (String)poOid;
	                pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());

	                pstmt.setTimestamp(2, timeStamp);
	                pstmt.setString(3, invoiceAction);
	                pstmt.setLong(4, Long.parseLong(userOid));
	                pstmt.setLong(5, Long.parseLong(poOID));
  				    pstmt.setString(6,invoiceStatus);
	                pstmt.addBatch();
	            }
	            int[] succesCount = pstmt.executeBatch();
	            if (succesCount.length != invoiceOids.size()){
	                //TODO Log errors.
	            }
	            con.commit();
	            
	        } catch (AmsException e) {
	            throw new AmsException(e);
	        } catch (SQLException e) {
	            throw new AmsException(e);
	        }
	    }
	 
	// Srinivasu_D CR-709 Rel 8.2 02/03/2013  End

	/**
	  * This method returns the groupOids for the given upload invoice oids
	  * @param uploadInvoiceOids
	  * @return Vector
	  * @throws AmsException
	  */
	 public static Vector getInvoiceGroupOidsForInvoiceOids(Vector uploadInvoiceOids) throws AmsException {
		 
		  Vector groupOidList = new Vector();
		  StringBuilder oidSQL = new StringBuilder();
			oidSQL.append(" select distinct a_invoice_group_oid from invoices_summary_data where ");
			if(uploadInvoiceOids != null && uploadInvoiceOids.size()>0){
				List<Object> sqlprmLst = new ArrayList<Object>();
				oidSQL.append(getInvoiceItemOidsSqlWithPlaceHolder(uploadInvoiceOids,"UPLOAD_INVOICE_OID",sqlprmLst));
				DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(oidSQL.toString(),false,sqlprmLst);
				if (invoiceListDoc != null) {
					List<DocumentHandler> invoiceList = invoiceListDoc.getFragmentsList("/ResultSetRecord");
					for (DocumentHandler invoiceDoc : invoiceList) {
						String groupOid = invoiceDoc.getAttribute("/A_INVOICE_GROUP_OID");
						if (StringFunction.isNotBlank(groupOid)) { 	
							groupOidList.add(groupOid);
						}
					}			
			 }
			}
			LOG.debug("getInvoiceGroupOidsForInvoiceOids() groupOidList: {}",groupOidList);
			return groupOidList;
	 }
	 public static String getInvoiceItemOidsSqlWithPlaceHolder(Vector invoiceItemOidsList,String oid,List<Object> sqlParamsLsts)

		{
			StringBuilder sql = null;
			int numberOfInvoiceItems = 0;
			int allPos = 0;
			numberOfInvoiceItems = invoiceItemOidsList.size();
			sql = new StringBuilder();	
			sql.append(" (");
			sql.append(oid);
			sql.append(" in (");
		
			//if InvoiceLineItems are < 1000 then just create normal sql
			if (numberOfInvoiceItems <= 1000) {
				allPos = numberOfInvoiceItems;
				sql = getAllInvoiceOidsPlaceHolderSql(invoiceItemOidsList, allPos, sql, 0,sqlParamsLsts);
			} 
			
			//if InvoiceLineItems are >1000 then break the items for every 1000 and create new 'IN' clause with OR condition
			 
			else {
				int quotient = numberOfInvoiceItems / 1000;
				int remainder = numberOfInvoiceItems % 1000;
				allPos = numberOfInvoiceItems;
				for (int i = 1; i <= quotient; i++) {
					sql = getAllInvoiceOidsPlaceHolderSql(invoiceItemOidsList, 1000 * i,
							sql, (1000 * i) - 1000,sqlParamsLsts);
					if (i != quotient)	{
						sql.append(" or ");
						sql.append(oid);
						sql.append(" in (");
					}
					else if (remainder > 0)
					sql.append(" or ");
				
				}

				if (remainder > 0) {				
					sql.append(oid);
					sql.append(" in (");
					sql = getAllInvoiceOidsPlaceHolderSql(invoiceItemOidsList,
							numberOfInvoiceItems, sql, numberOfInvoiceItems 
							- remainder,sqlParamsLsts);
				}
			}
			
			sql.append(")");
			return sql.toString();
		}
}