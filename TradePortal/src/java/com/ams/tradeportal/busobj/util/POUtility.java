package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.util.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;

/**
 * Utility methods for the Purchase Order objects.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POUtility {
private static final Logger LOG = LoggerFactory.getLogger(POUtility.class);
	
/**
 * POUtility constructor comment.
 */
public POUtility() {
	super();
}
/**
 * For each of the 7 standard and 24 other fields in the upload defn,
 * create a hashtable containing the external name (i.e., the user given
 * name) keyed by the field's internal name (e.g., ben_name or other5).
 * 
 * @return java.util.Hashtable
 * @param uploadDefn POUploadDefinitionWebBean
 * @exception java.rmi.RemoteException 
 * @exception AmsException 
 */
public Hashtable loadFieldNames(POUploadDefinitionWebBean uploadDefn)
	throws RemoteException, AmsException {

	Hashtable fieldNames = new Hashtable(TradePortalConstants.PO_NUMBER_OF_FIELDS);
	String value;
	String attributeName;

	value = uploadDefn.getAttribute("amount_field_name");
	fieldNames.put("amount", value);

	value = uploadDefn.getAttribute("ben_name_field_name");
	fieldNames.put("ben_name", value);

	value = uploadDefn.getAttribute("currency_field_name");
	fieldNames.put("currency", value);

	value = uploadDefn.getAttribute("item_num_field_name");
	fieldNames.put("item_num", value);

	value = uploadDefn.getAttribute("last_ship_dt_field_name");
	fieldNames.put("last_ship_dt", value);

	value = uploadDefn.getAttribute("po_num_field_name");
	fieldNames.put("po_num", value);

	value = uploadDefn.getAttribute("po_text_field_name");
	fieldNames.put("po_text", value);

	for (int x = 1; x <= 24; x++) {
		attributeName = "other" + x + "_field_name";
		value = uploadDefn.getAttribute(attributeName);
		fieldNames.put(attributeName, value);
	}

	return fieldNames;
}
}
