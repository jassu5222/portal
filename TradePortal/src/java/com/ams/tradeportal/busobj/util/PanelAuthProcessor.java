package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.busobj.CorporateOrganizationBean;
import com.ams.tradeportal.busobj.DmstPmtPanelAuthRange;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.PanelAuthorizer;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class PanelAuthProcessor {
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthProcessor.class);

	private User user;
	private String instrType;
	private ErrorManager errorMgr;
	private String panelOplockVal;
	private static final String PROCEED_AUTH_SELECT_SQL = "SELECT PANEL_CODE FROM PANEL_AUTHORIZER WHERE ";
	private Panel panel;
	private boolean errorFound = false;

	public PanelAuthProcessor(User user, String instrType, ErrorManager errorMgr){
		this.user      = user;
		this.instrType = instrType;
		this.errorMgr  = errorMgr;
	}

    /**
     * 
     * @param panelGroupOid
     * @param panelOplockVal
     * @throws AmsException
     */
	public void init(String panelGroupOid, String panelOplockVal) throws AmsException{
	   this.panelOplockVal = panelOplockVal;
       panel = new Panel(panelGroupOid, panelOplockVal, instrType, errorMgr);
   }
	
    /**
	 * init is overridden to allow multiple panelRangeOids to be passed in for beneficiary
	 * level panel authorization where multiple panel ranges apply for a transaction
     * @param panelGroupOid
     * @param panelRangeOid
     * @param panelOpLockVal
     * @throws AmsException 
     */
	public void init(String panelGroupOid, String panelRangeOid[], String panelOpLockVal) throws AmsException{
		this.panelOplockVal = panelOpLockVal;
	    panel = new Panel(panelGroupOid, panelRangeOid, panelOplockVal, instrType, errorMgr);
	}

/**
 * 
 * @param busObj
 * @param panelAuthRangeOid
 * @param isSubsidiaryAccess
 * @return
 */
	public String process(BusinessObject busObj, String panelAuthRangeOid, boolean isSubsidiaryAccess){
		
		 String authStatus = TradePortalConstants.AUTHFALSE;

		 try {
        
		      String processedAuthPanels = retrieveProcessedAuthorizers(busObj);
		      if(errorFound){
		    	  return authStatus;
		      }
			  String userPanelLevel = PanelAuthUtility.getPanelCode(isSubsidiaryAccess, user);
		      authStatus = panel.canAuthorizeByRange(processedAuthPanels, panelAuthRangeOid, userPanelLevel);
		      
		      if(TradePortalConstants.AUTHPARTL.equals(authStatus) || TradePortalConstants.AUTHTRUE.equals(authStatus)){
		    	  insertAuthUserDetail(busObj, authStatus, isSubsidiaryAccess);		    	  
		      }
		      		      
		  } catch (Exception ex) {	
			  LOG.error("Error Occured while retriving and processing panel group for transaction: " ,ex);
		      return  TradePortalConstants.AUTHFALSE;
		  }

		  return authStatus;
	}
	
    /**
     * This method retrieves all panel code group which has already authorized the transaction, if the same user is not attempting to authorise
     * 
     * @param busObj
     * @return
     * @throws Exception
     */
	public String retrieveProcessedAuthorizers(BusinessObject busObj) throws Exception
	{

	    String processedAuthPanels = "";     	 
	      	    
  	    if(checkUserAuthAttempt(busObj) ){ //If same user is attempting to authorise
		   errorMgr.issueError(getClass().getName(), TradePortalConstants.USER_ALREADY_ATTEMPT_AUTHRZ_PANEL,
	    		   user.getAttribute("first_name"),
	    		   user.getAttribute("last_name"));
		   errorFound = true;
		   return processedAuthPanels;
	    }
	   
  	   processedAuthPanels = retrieveAuthHistory(busObj);
	     
	   return processedAuthPanels;
	}
	
	/**
	 * This method checks if the same user is attempting to authorise.
	 * @param busObj
	 * @return
	 * @throws Exception
	 */
	public boolean checkUserAuthAttempt(BusinessObject busObj) throws Exception{
		
		String ownerType = busObj.getDataTableName();
	    String idAttributeName = busObj.getIDAttributeName();
	    String oid = busObj.getAttribute(idAttributeName);	   	    
	    StringBuilder whereClause = new StringBuilder();
	    
	    
	  //SureshL R91 IR T36000026319 - SQL INJECTION FIX	
		List<Object> sqlParams = new ArrayList();
		if(busObj instanceof DmstPmtPanelAuthRange){
  	    	whereClause.append(" p_owner_oid in ( SELECT dmst_pmt_panel_auth_range_oid FROM dmst_pmt_panel_auth_range WHERE p_transaction_oid =? )");
  	    	sqlParams.add(busObj.getAttribute("transaction_oid"));
  	    }else{
  	    	whereClause.append(" p_owner_oid = ? ");
  	    	sqlParams.add(oid);
  	    }
  	    whereClause.append(" AND a_user_oid = ? ");
  	    sqlParams.add(user.getAttribute("user_oid"));
  	    whereClause.append(" AND owner_object_type= ? ");
  	    sqlParams.add(ownerType);
  	    int  userAuthCount = DatabaseQueryBean.getCount("panel_authorizer_oid", "panel_authorizer", whereClause.toString(),true,sqlParams);	   
	    if(userAuthCount > 0){
		   return true;
	    }
	    return false;
	}
	
	/**
	 * This method retrieves all panel code group which has already authorized the transaction.
	 * @param busObj
	 * @return
	 * @throws Exception
	 */
	public String retrieveAuthHistory(BusinessObject busObj) throws RemoteException, AmsException{
		
		String ownerType = busObj.getDataTableName();
	    String idAttributeName = busObj.getIDAttributeName();
	    String oid = busObj.getAttribute(idAttributeName);
		String processedAuthPanels = "";
		StringBuilder processAuthSQL = new StringBuilder(PROCEED_AUTH_SELECT_SQL);
		
		  Object sqlParams[] = new Object[2];
		   processAuthSQL.append(" owner_object_type = ?");	   
		   sqlParams[0] = ownerType;
		   processAuthSQL.append(" AND p_owner_oid = ?"); 
		   sqlParams[1] = oid;
		   processAuthSQL.append(" order by auth_date_time ");

		   //retrieve list of previous authorizers from Panel_Authorizer table for the given busObj
		   //Rpasupulati IR T36000032872
		   DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(processAuthSQL.toString(),true,sqlParams);
		   if (resultXML != null){
		     Vector<DocumentHandler> rows = resultXML.getFragments("/ResultSetRecord");

	         for (DocumentHandler doc: rows) {
	            String panelCode = doc.getAttribute("/PANEL_CODE");
	            processedAuthPanels = processedAuthPanels.concat(panelCode);
	         }
		   }
		   	   
		   return processedAuthPanels;
	}
	/**
	 * This method insert the user detail in PanelAuthUser object. this object contain all
	 * user for transaction which has authorized transaction as a part of panel authorization.
	 * 
	 * @param obj
	 * @param isSubsidiaryAccess
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void insertAuthUserDetail(BusinessObject busObj, String authStatus, boolean isSubsidiaryAccess) 
	throws RemoteException, AmsException{
		
		String ownerType = busObj.getDataTableName();
		//MEer Rel 8.3 T36000021992 
		   boolean ignoreDefaultPanelValue = false;
		
		String userPanelAuthLevel = PanelAuthUtility.getPanelCode(isSubsidiaryAccess, user);
		
		// Create panel Authorizer to insert user detail for processing transaction
		long panelAuthOid = busObj.newComponent("PanelAuthorizerList");
		PanelAuthorizer newPanelAuthorizer = (PanelAuthorizer) busObj.getComponentHandle("PanelAuthorizerList", panelAuthOid);		
		newPanelAuthorizer.setAttribute("owner_object_type", ownerType);				
    	newPanelAuthorizer.setAttribute("user_oid", user.getAttribute("user_oid"));
    	newPanelAuthorizer.setAttribute("auth_date_time",  DateTimeUtility.getCurrentDateTime());   	    	
    	newPanelAuthorizer.setAttribute("panel_code", userPanelAuthLevel);	    	

		// display message if it is partial authorized
    	
    	if(TradePortalConstants.AUTHPARTL.equals(authStatus)){
    		/*IR#T36000020789 Prateep Start*/	
    		CorporateOrganizationBean corpOrgBean = new CorporateOrganizationBean();
    		Hashtable corporgpanelAliases = corpOrgBean.getPanelLevelAliases(user.getAttribute("owner_org_oid"), errorMgr.getLocaleName(),ignoreDefaultPanelValue );
    		String userPanelAuthLevelAliase=(String)corporgpanelAliases.get(userPanelAuthLevel);
    		   		
    		errorMgr.issueError(getClass().getName(),
    	    		TradePortalConstants.PANEL_AUTH_REQS_NOT_MET, userPanelAuthLevelAliase);
    		/*IR#T36000020789 Prateep End*/
    	}
    	
	}
	
	/**
	 * this method set the list of next awaiting panel approvals.
	 * @param isSubsidiaryAccess
	 * @param obj
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 * @throws Exception
	 */
	public String getPanelAuthNextApprovals(BusinessObject obj, boolean isSubsidiaryAccess) throws RemoteException, AmsException {
		String panelAuthHistory = retrieveAuthHistory(obj)
				+ PanelAuthUtility.getPanelCode(isSubsidiaryAccess, user);
		Map panelApproversSummaryMap = panel.getNextPanelApproverList(panelAuthHistory, null);
		String panelApprovalsList = StringFunction.getMapKeyToStringFormat(panelApproversSummaryMap);

		return panelApprovalsList;
	}
}
