/**
 * 
 */
package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**

 *
 */
public class PanelAuthorizationRangeObject {
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationRangeObject.class);

	private String panel_auth_range_oid;
	private String panel_auth_range_min_amount;
	private String panel_auth_range_max_amount;
	private String panel_auth_range_base_currency;
	private String panel_auth_range_currency_basis;
	private HashMap<Integer, PanelAuthorizationRuleObject> panelAuthRuleMap;
	
	public String getPanel_auth_range_oid() {
		return panel_auth_range_oid;
	}
	public void setPanel_auth_range_oid(String panel_auth_range_oid) {
		this.panel_auth_range_oid = panel_auth_range_oid;
	}
	public String getPanel_auth_range_min_amount() {
		return panel_auth_range_min_amount;
	}
	public void setPanel_auth_range_min_amount(String panel_auth_range_min_amount) {
		this.panel_auth_range_min_amount = panel_auth_range_min_amount;
	}
	public String getPanel_auth_range_max_amount() {
		return panel_auth_range_max_amount;
	}
	public void setPanel_auth_range_max_amount(String panel_auth_range_max_amount) {
		this.panel_auth_range_max_amount = panel_auth_range_max_amount;
	}
	public String getPanel_auth_range_base_currency() {
		return panel_auth_range_base_currency;
	}
	public void setPanel_auth_range_base_currency(
			String panel_auth_range_base_currency) {
		this.panel_auth_range_base_currency = panel_auth_range_base_currency;
	}
	public String getPanel_auth_range_currency_basis() {
		return panel_auth_range_currency_basis;
	}
	public void setPanel_auth_range_currency_basis(
			String panel_auth_range_currency_basis) {
		this.panel_auth_range_currency_basis = panel_auth_range_currency_basis;
	}
	public HashMap<Integer, PanelAuthorizationRuleObject> getPanelAuthRuleMap() {
		return panelAuthRuleMap;
	}
	public void setPanelAuthRuleMap(
			HashMap<Integer, PanelAuthorizationRuleObject> panelAuthRuleMap) {
		this.panelAuthRuleMap = panelAuthRuleMap;
	}
	
	
}
