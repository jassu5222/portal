/**
 * 
 */
package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**

 *
 */
public class PanelAuthorizationRuleObject {
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthorizationRuleObject.class);
	
	private String approver_sequence;
	private int totalNoOfAuthorizersRemaining;
	private String approversHistory;
	private String approver_1;
	private String approver_2;
	private String approver_3;
	private String approver_4;
	private String approver_5;
	private String approver_6;
	private String approver_7;
	private String approver_8;
	private String approver_9;
	private String approver_10;
	private String approver_11;
	private String approver_12;
	private String approver_13;
	private String approver_14;
	private String approver_15;
	private String approver_16;
	private String approver_17;
	private String approver_18;
	private String approver_19;
	private String approver_20;
	
	public String getApprover_sequence() {
		return approver_sequence;
	}
	public void setApprover_sequence(String approver_sequence) {
		this.approver_sequence = approver_sequence;
	}
	public int getTotalNoOfAuthorizersRemaining() {
		return totalNoOfAuthorizersRemaining;
	}
	public void setTotalNoOfAuthorizersRemaining(int totalNoOfAuthorizersRemaining) {
		this.totalNoOfAuthorizersRemaining = totalNoOfAuthorizersRemaining;
	}
	public String getApproversHistory() {
		return approversHistory;
	}
	public void setApproversHistory(String approversHistory) {
		this.approversHistory = approversHistory;
	}
	public String getApprover_1() {
		return approver_1;
	}
	public void setApprover_1(String approver_1) {
		this.approver_1 = approver_1;
	}
	public String getApprover_2() {
		return approver_2;
	}
	public void setApprover_2(String approver_2) {
		this.approver_2 = approver_2;
	}
	public String getApprover_3() {
		return approver_3;
	}
	public void setApprover_3(String approver_3) {
		this.approver_3 = approver_3;
	}
	public String getApprover_4() {
		return approver_4;
	}
	public void setApprover_4(String approver_4) {
		this.approver_4 = approver_4;
	}
	public String getApprover_5() {
		return approver_5;
	}
	public void setApprover_5(String approver_5) {
		this.approver_5 = approver_5;
	}
	public String getApprover_6() {
		return approver_6;
	}
	public void setApprover_6(String approver_6) {
		this.approver_6 = approver_6;
	}
	public String getApprover_7() {
		return approver_7;
	}
	public void setApprover_7(String approver_7) {
		this.approver_7 = approver_7;
	}
	public String getApprover_8() {
		return approver_8;
	}
	public void setApprover_8(String approver_8) {
		this.approver_8 = approver_8;
	}
	public String getApprover_9() {
		return approver_9;
	}
	public void setApprover_9(String approver_9) {
		this.approver_9 = approver_9;
	}
	public String getApprover_10() {
		return approver_10;
	}
	public void setApprover_10(String approver_10) {
		this.approver_10 = approver_10;
	}
	public String getApprover_11() {
		return approver_11;
	}
	public void setApprover_11(String approver_11) {
		this.approver_11 = approver_11;
	}
	public String getApprover_12() {
		return approver_12;
	}
	public void setApprover_12(String approver_12) {
		this.approver_12 = approver_12;
	}
	public String getApprover_13() {
		return approver_13;
	}
	public void setApprover_13(String approver_13) {
		this.approver_13 = approver_13;
	}
	public String getApprover_14() {
		return approver_14;
	}
	public void setApprover_14(String approver_14) {
		this.approver_14 = approver_14;
	}
	public String getApprover_15() {
		return approver_15;
	}
	public void setApprover_15(String approver_15) {
		this.approver_15 = approver_15;
	}
	public String getApprover_16() {
		return approver_16;
	}
	public void setApprover_16(String approver_16) {
		this.approver_16 = approver_16;
	}
	public String getApprover_17() {
		return approver_17;
	}
	public void setApprover_17(String approver_17) {
		this.approver_17 = approver_17;
	}
	public String getApprover_18() {
		return approver_18;
	}
	public void setApprover_18(String approver_18) {
		this.approver_18 = approver_18;
	}
	public String getApprover_19() {
		return approver_19;
	}
	public void setApprover_19(String approver_19) {
		this.approver_19 = approver_19;
	}
	public String getApprover_20() {
		return approver_20;
	}
	public void setApprover_20(String approver_20) {
		this.approver_20 = approver_20;
	}

}
