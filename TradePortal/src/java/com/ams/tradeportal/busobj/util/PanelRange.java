package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.StringFunction;

public class PanelRange {
private static final Logger LOG = LoggerFactory.getLogger(PanelRange.class);
	
	
	List<PanelRule> panelRuleList = new ArrayList<PanelRule>();
	public String panelRangeMinAmt= null;
	public String panelRangeMaxAmt = null;
	public String panelRangeOid = null;
	

	public PanelRange(){
		
	}
	
	public PanelRange(String panelRangeMinAmt, String panelRangeMaxAmt){
		setPanelRangeMinAmt(panelRangeMinAmt);
		setPanelRangeMaxAmt(panelRangeMaxAmt);
	}

	
	public void add(PanelRule panelRule){
		panelRuleList.add(panelRule);
	}
	
	
	
	public String toString(){
		StringBuilder panelRange = new StringBuilder("PanelRangeMinAmt: ").append(panelRangeMinAmt).append(",").append("PanelRangeMaxAmt: ").append(panelRangeMaxAmt).append("\n");
		for(int i=0;i<panelRuleList.size(); i++){
			panelRange.append(panelRuleList.get(i).toString()).append("\n");
		}
		return panelRange.toString();
	}
	

	/**Added by MEerupula Rel 8.3 CR 821
	* This method is used to check whether current user with given panel authority can authorize a transaction(rule) or not
	* depending on the history.  If he/she can authorize then the method returns partial and if he is the last authorizer in the rule 
	* then the method returns complete and if he can't authorize the transaction then method returns  fail.
	*
	* @param authHistory  java.lang.String - the panel history
	* @param userPanelLevel java.lang.String -current user panel level
	* @return String - authorization result either fail or complete or partial
	* 
   */
	
	public String match(String authHistory, String userPanelLevel) throws AmsException {
		String  matchFound = "";
		String finalResult = Panel.AUTH_RESULT_FAILED;
		if(StringFunction.isBlank(userPanelLevel)){
			return finalResult;
		}
		for(int i=0;i<panelRuleList.size(); i++){
			matchFound  = panelRuleList.get(i).match(authHistory, userPanelLevel);
			if (Panel.AUTH_RESULT_COMPLETE.equals(matchFound)) {
				finalResult  = matchFound;
				break;
			} else if (Panel.AUTH_RESULT_PARTIAL.equals(matchFound)) {
				finalResult = matchFound;
			}

		}
		return finalResult;
	}

	
	/**
	* This method is used to determine what range given amount falls within .
	*
	* @param amount  java.lang.String - amount provided by user
	* @return boolean - returns true if range exists
	* 
   */
	
	public boolean withinRange(String amount) {
		boolean withinRange = false;
		if(Double.parseDouble(amount) >= Double.parseDouble(panelRangeMinAmt) && Double.parseDouble(amount) <= Double.parseDouble(panelRangeMaxAmt)){
			withinRange = true;
		}	
		return withinRange;
	}
	
	
	

public String getPanelRangeOid() {
	return panelRangeOid;
}
public void setPanelRangeOid(String panelRangeOid) {
	this.panelRangeOid = panelRangeOid;
}

public String getPanelRangeMinAmt() {
	return panelRangeMinAmt;
}
public void setPanelRangeMinAmt(String panelRangeMinAmt) {
	this.panelRangeMinAmt = panelRangeMinAmt;
}
public String getPanelRangeMaxAmt() {
	return panelRangeMaxAmt;
}
public void setPanelRangeMaxAmt(String panelRangeMaxAmt) {
	this.panelRangeMaxAmt = panelRangeMaxAmt;
}

public List<PanelRule> getPanelRuleList() {
	return panelRuleList;
}

public void setPanelRuleList(List<PanelRule> panelRuleList) {
	this.panelRuleList = panelRuleList;
}

	
}
