package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public abstract class PanelRule {
private static final Logger LOG = LoggerFactory.getLogger(PanelRule.class);


	protected StringBuffer tempPanelApprovers = null;
	protected String  panelApprovers = null;
	private int totalAuthorizers = 0;
	private String sequence = null;
	private String authHistory= null;
	private String curPanelLevel= null;
	protected int index=0;
	//MEerupula Rel 8.3 IR-20194 New variable is added to capture history for each approver_sequece
	protected StringBuffer authHistoryPerRule  = null;
	
	String result = null;
	
	


	public PanelRule(String s){
		panelApprovers = s;
		totalAuthorizers = panelApprovers.length();

	}

	PanelRule(DocumentHandler doc){

		String panelLevels = "";
		String str = null;
		for (int cnt=1; cnt<=20; cnt++){ 
			str = doc.getAttribute("/APPROVER_"+cnt);	
			if(StringFunction.isNotBlank(str)){
				panelLevels += str;
				totalAuthorizers++;
			}
		}
		this.panelApprovers = panelLevels;
	}


	

	public int getTotalAuthorizers() {
		return totalAuthorizers;
	}

	
	
	protected String toString(String str){
		StringBuilder sb = new StringBuilder(str).append("- ").append(panelApprovers).append(" history: ").append(authHistory).append(" userPanelLevel: ").append(curPanelLevel)
				.append(" awaiting: ").append(tempPanelApprovers).append(" totalauthorizer: ").append(totalAuthorizers);
		return sb.toString();		
	}

  protected int getMaxCount() {
	  return tempPanelApprovers.length();
  }
 

	protected boolean isCurrentUserPanel(int count, int total) {
		return count == total-1;
	}

/*Added by MEerupula Rel 8.3 CR 821
 * This method is used to process the history against the rule with given panel user
 * 
 * 
 */
	
	protected String processAuth(String  authHistory,  boolean userPanelIncluded) throws AmsException{
		
		String panelLevel = null;
		int idx = 0, i=0;
		int totAuthHis = authHistory.length();

		// process all history auth 
		for( i=0, index=0; i<totAuthHis && getMaxCount()>0; i++, index++){
			panelLevel = authHistory.substring(i, i+1);
			idx = tempPanelApprovers.indexOf(panelLevel);
			if (idx != -1 && idx < getMaxCount()) {
				//MEerupula Rel 8.3 IR-20194 This method is used to capture history and to delete currentUserPanelLevel from rule
				updateMatchedPanel(idx);
			} else  {//if in history but not in rule
				//error no more user at this panel level can authorzie  
				if (userPanelIncluded && isCurrentUserPanel(i, totAuthHis)) {
					result = Panel.AUTH_RESULT_FAILED;
					break;
				}
			}
		}


		if (!Panel.AUTH_RESULT_FAILED.equals(result)) {
			if (isRuleFullyMatched()){
				if(userPanelIncluded) {
					result = Panel.AUTH_RESULT_COMPLETE;
				} else {
					result = Panel.AUTH_RESULT_PARTIAL;
				}
			} else {
				result = Panel.AUTH_RESULT_PARTIAL;
			}
		}


		return result;
	}


	protected boolean isRuleFullyMatched() {

		return tempPanelApprovers.length() == 0;
	}


	public String match(String authHistory, String userPanelLevel) throws AmsException {
		this.authHistory = authHistory;
		this.curPanelLevel = userPanelLevel;
		
		result ="";
		tempPanelApprovers = new StringBuffer(panelApprovers);
		authHistoryPerRule  = new StringBuffer();
		return matchSub(authHistory,userPanelLevel);
	}
	
	protected abstract String matchSub(String authHistory, String userPanelLevel) throws AmsException;


	/**
	* This method provides all the remaining panel approvers in the rule with the sequence.
	*
	* @param authHistory  java.lang.String - the panel history
	* @return String - all awaiting approvers with the sequence
	* 
   */
	
	public String getAllAwaitingPanelApprovers(String history) throws AmsException {

		match(history, "");
		return tempPanelApprovers.toString();
	}

	
	/**Added by MEerupula Rel 8.3 CR 821
	* This method provides unique next panel approvers in the rule.It removes the duplicates if exists and
	* will provide in any order.
	*
	* @param authHistory  java.lang.String - the panel history
	* @return String - next approvers with no sequence and no duplicates
	* 
   */
	public String getNextAwaitingPanelLevels(String history) throws AmsException {
		String allAwaitingPanelLevels =  getAllAwaitingPanelApprovers(history);
		String nextPanelLevels = null;
			for (int i=0;i<allAwaitingPanelLevels .length(); i++){
				String	panelLevel = allAwaitingPanelLevels.substring(i, i+1);
				if(StringFunction.isBlank(nextPanelLevels)){
					nextPanelLevels = panelLevel;	
				}else if(StringFunction.isNotBlank(nextPanelLevels)&& !nextPanelLevels.contains(panelLevel)){
					nextPanelLevels += panelLevel;		
				}
			}
		return nextPanelLevels;
	}
	/**
	 * Rel 8.3 IR T36000021533
	 * @param history
	 * @param sequence
	 * @return
	 * @throws AmsException
	 */
	public String getNextAwaitingPanelLevels(String history, String sequence) throws AmsException {
		String allAwaitingPanelLevels =  getAllAwaitingPanelApprovers(history);
		String nextPanelLevels = null;
		if(("FIX_ALL").equals(sequence) || (("FIX_FIRST").equals(sequence) && history.equals(""))){ 
			nextPanelLevels = allAwaitingPanelLevels.substring(0, 1);
		}
		if(("FIX_LAST").equals(sequence)){
			if(allAwaitingPanelLevels.length() > 1){
				for (int i=0;i<allAwaitingPanelLevels .length()-1; i++){
					String	panelLevel = allAwaitingPanelLevels.substring(i, i+1);
					if(StringFunction.isBlank(nextPanelLevels)){
						nextPanelLevels = panelLevel;	
					}else if(StringFunction.isNotBlank(nextPanelLevels)&& !nextPanelLevels.contains(panelLevel)){
						nextPanelLevels += panelLevel;		
					}
				}
			}else if(allAwaitingPanelLevels.length() == 1){
				nextPanelLevels = allAwaitingPanelLevels.substring(0, 1);
			}
		}
		if(("NOT_FIXED").equals(sequence) || (("FIX_FIRST").equals(sequence) && !history.equals(""))){
			for (int i=0;i<allAwaitingPanelLevels .length(); i++){
				String	panelLevel = allAwaitingPanelLevels.substring(i, i+1);
				if(StringFunction.isBlank(nextPanelLevels)){
					nextPanelLevels = panelLevel;	
				}else if(StringFunction.isNotBlank(nextPanelLevels)&& !nextPanelLevels.contains(panelLevel)){
					nextPanelLevels += panelLevel;		
				}
			}
		}
		return nextPanelLevels;
	}
	
	//MEerupula Rel 8.3 IR-20194 This method is used to capture history for each rule(sequence) and 
	//to delete the currentUserPanelLevel from panelRule if he/she is valid approver(match) from rule
	protected void updateMatchedPanel(int panelLevelIndex){
		authHistoryPerRule.append(tempPanelApprovers.charAt(panelLevelIndex));
		tempPanelApprovers.deleteCharAt(panelLevelIndex);
	}
	
	public String getNextAwaitingPanelLevels() throws AmsException {
		return tempPanelApprovers.toString();
	}

	public String getPanelApprovers() {
		return panelApprovers;
	}

	public void setPanelApprovers(String panelApprovers) {
		this.panelApprovers = panelApprovers;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getAuthHistory() {
		return authHistory;
	}

	public void setAuthHistory(String authHistory) {
		this.authHistory = authHistory;
	}

	public StringBuffer getAuthHistoryPerRule() {
		return authHistoryPerRule;
	}

}