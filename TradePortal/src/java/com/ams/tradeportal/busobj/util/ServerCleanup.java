package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;

/**
 * This class handles the clean-up of any stale data that might exist
 * in the event of a server crash.  For the Trade Portal, this data
 * includes user sessions (stored on the ACTIVE_USER_SESSION table) and
 * instrument locks.  
 * 
 * The ACTIVE_USER_SESSION table keeps track of which users are logged
 * in to which server instance.  This table is queried to determine
 * what data needs to be cleaned up.
 * 
 * This class can be called from the command line (in the event of a 
 * server that crashes and cannot be brought back up again) or every
 * time that a server starts up (to remove any stale data from the
 * last time the server was run).
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ServerCleanup
 {
private static final Logger LOG = LoggerFactory.getLogger(ServerCleanup.class);
	/** Nar IR-T36000032592 Rel9.2 09/28/2014 Add - Begin 
     * Main method to run this class from the command line.  A server instance
     * name must be passed in.
     *
     * @param argc the standard array of strings passed into a main method
     *
    public static void main(String argc[])
     {
        // Check to see if the user specified a server instance name
        if(argc.length == 0)
         {
            LOG.info("Please indicate a server instance name...");
            return;
         }
        
        // Clean up that server instance
        cleanUp(argc[0]);
     }
      Nar IR-T36000032592 Rel9.2 09/28/2014 Add - End */
    /**
     * Cleans up the server instance in which the virtual machine is
     * running.  
     */
    public static void cleanUp()
     {
        try {        
	     PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");


            // Get the serverInstanceName from jPylonh config files, then
            // clean it up
            cleanUp( portalProperties.getString("serverName") );
         } 
        catch (Exception e) 
         {
            LOG.error("Exception occurred while cleaning up server: ",e);
         }        
     }
     
     
    /**
     * Cleans up a server instance.  First, gets a list of all left-over user 
     * sessions.  Then, loops through each of those, removing all instrument
     * locks and the user's record from the ACTIVE_USER_SESSION table.
     *
     * @param serverInstanceName - the server instance name 
     */
    public static void cleanUp(String serverInstanceName) 
     {
        LOG.info("##########################");
        LOG.info("##   SERVER CLEAN-UP    ##");
        LOG.info("##########################");

        LOG.info("\nCleaning up server instance with name: {}",serverInstanceName);
        
        try {
            // Get the server location from jPylon properties
            String serverLocation  = JPylonProperties.getInstance().getString("serverLocation");            
             
		    // Create a generic list object and instantiate it based on the parameters passed in
		    GenericList sessionList = (GenericList) EJBObjectFactory.createClientEJB(serverLocation, "GenericList", new ClientServerDataBridge());

            // Pass the server instance name as a parameter to the query
            String parms[] = {serverInstanceName};
            
		    sessionList.prepareList("ActiveUserSession", "byServer", parms);
		    sessionList.getData();        
    		
		    LOG.info("    \nFound {} users for which data needs to be cleaned up",sessionList.getObjectCount());
            
            // Loop through the results
            for(int i=0; i< sessionList.getObjectCount(); i++)
            {
                sessionList.scrollToObjectByIndex(i);
                
                // Grab the object from the list
                ActiveUserSession aSession = (ActiveUserSession) sessionList.getBusinessObject();

                //cquinton 2/16/2012 Rel 8.0 ppx255 start
                long userOid = aSession.getAttributeLong("user_oid");                
                LOG.info("    Cleaning up data for user with object ID (OID): {}",userOid);

                //release locks only if the user session is active
                String forceLogoutInd = aSession.getAttribute("force_logout"); 
                if ( !TradePortalConstants.INDICATOR_YES.equals( forceLogoutInd ) ) {
                    LOG.info("        Removing locked instruments...");
                
		            // Unlock any instruments that the user has locked
                    LockingManager.unlockBusinessObjectByUser(userOid, false);
                } else {
                    LOG.info("        Not removing locked instruments - session was forcibly logged out.");
                }
                //cquinton 2/16/2012 Rel 8.0 ppx255 end
                
                // Delete the active user session
                aSession.delete();
                LOG.info("        Deleting record from ACTIVE_USER_SESSION table...");            
                
            }
             
            sessionList.remove(); 
            
            LOG.info("Server clean-up complete...\n");
         } 
        catch (Exception e) 
         {
            LOG.error("Exception occurred while cleaning up server: ",e);            
         }
     }
 }