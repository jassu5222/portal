package com.ams.tradeportal.busobj.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;

public class SettlementInstrUtility {
private static final Logger LOG = LoggerFactory.getLogger(SettlementInstrUtility.class);

    /**
     * This method is used to identify whether corporate org allowed to include settlement instruction detail in DCR. 
     * @param corpOrgOid
     * @return
     * @throws AmsException
     */
    public static boolean isSettleInstrIncludeInDCR ( String corpOrgOid ) throws AmsException {
		boolean isInclude = false;
		String sql = "SELECT include_settle_instruction FROM corporate_org WHERE organization_oid = ?";
		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{corpOrgOid});
		if (resultDoc != null){
			if ( TradePortalConstants.INDICATOR_YES.equals(resultDoc.getAttribute("/ResultSetRecord(0)/INCLUDE_SETTLE_INSTRUCTION")) ) {
			    isInclude = true;
			}
		}		 
		return isInclude;
	}
    
    /**
     * This method is used to identify whether settlement instruction data verification is required for transaction.
     * @param corpOrgOid
     * @param transactionTypeCode
     * @return
     * @throws AmsException
     */
    public static boolean isSettlemntInstrVerificationReq ( String corpOrgOid, String transactionTypeCode, String dcrInstrcution ) throws AmsException {
    	boolean isVerificaitonReq = false;
    	
    	if ( TransactionType.SIM.equals(transactionTypeCode) || TransactionType.SIR.equals(transactionTypeCode) ) {
    		isVerificaitonReq = true;
    	} else if (TransactionType.DISCREPANCY.equals(transactionTypeCode) && isSettleInstrIncludeInDCR(corpOrgOid) ) {
    		if ( !TradePortalConstants.IMP_REJ.equals(dcrInstrcution) ) {
    		  isVerificaitonReq = true;
    		}
    	}    	
    	return isVerificaitonReq;
    }
    
    /**
     * This method is used to check whether settlement instruction message and response can be created for given instrument.
     * @param instrumentTypeCode
     * @return
     */
    public static boolean isCreateSettlmentAvailableForInstr ( String instrumentTypeCode) {
    	
    	boolean isCreateSettlement = false;
    	if ( InstrumentType.LOAN_RQST.equals(instrumentTypeCode) ||  InstrumentType.IMPORT_COL.equals(instrumentTypeCode) || 
    			InstrumentType.DOCUMENTARY_BA.equals(instrumentTypeCode) || InstrumentType.DEFERRED_PAY.equals(instrumentTypeCode)) {
    		isCreateSettlement = true;
    	}
    	return isCreateSettlement;
    }
    
    /**
     * This method is used to check whether Rollover can be created for given instrument.
     * @param instrumentTypeCode
     * @return
     */
    public static boolean isRolloverAvailableForInstr ( String instrumentTypeCode ) {
    	
    	return InstrumentType.LOAN_RQST.equals(instrumentTypeCode);
    }
       
}
