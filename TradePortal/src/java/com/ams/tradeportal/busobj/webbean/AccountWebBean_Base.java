



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AccountWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* account_oid - Unique identifier */
       registerAttribute("account_oid", "account_oid", "ObjectIDAttribute");


       /* account_number - Number identifying the account */
       registerAttribute("account_number", "account_number");


       /* currency - The currency of funds in the account */
       registerAttribute("currency", "currency");


       /* owner_object_type - the type of object that is the owner of this account */
       registerAttribute("owner_object_type", "owner_object_type");


       /* account_name - This is the legal account name that the bank has on file. */
       registerAttribute("account_name", "account_name");


       /* bank_country_code - This represents the Bank's Country code for the account. The CorporateCustomer
         will provide valid values to the Trade Portal Reference data for validation. */
       registerAttribute("bank_country_code", "bank_country_code");


       /* source_system_branch - Source System/Branch Identifier Codevalue that bank will enter. */
       registerAttribute("source_system_branch", "source_system_branch");


       /* account_type - A text field entered by the Corporate Customer to describe the account.
         The values for account type are defined by the Corporate Customer, and will
         not be validated in the Trade Portal. */
       registerAttribute("account_type", "account_type");


       /* available_for_loan_request - If selected this account will display as available when creating a loan
         request in the drop-down list of account numbers. (Note: currently all accounts
         in this list appear in the drop-down list). */
       registerAttribute("available_for_loan_request", "available_for_loan_request", "IndicatorAttribute");


       /* available_for_domestic_pymts - If selected this account will be available in the debit from account number
         dropdown list for selection for domestic payments. */
       registerAttribute("available_for_domestic_pymts", "available_for_domestic_pymts", "IndicatorAttribute");


       /* pending_transaction_balance - This field captures running pending movements for each account (to be subtracted/or
         added to from retrieved balances). This field will be display only; It is
         not editable. */
       registerAttribute("pending_transaction_balance", "pending_transaction_balance", "TradePortalSignedDecimalAttribute");


       /* current_balance - This field represents the current account balance returned from the bank
         request.   */
       registerAttribute("current_balance", "current_balance", "TradePortalSignedDecimalAttribute");


       /* last_retrieved_from_bank_date - This represents the date and time the account balance was updated in the
         portal. */
       registerAttribute("last_retrieved_from_bank_date", "last_retrieved_from_bank_date", "DateTimeAttribute");


       /* interest_rate - This presents the user with the current interest rate on the account, if
         applicable. */
       registerAttribute("interest_rate", "interest_rate", "TradePortalDecimalAttribute");


       /* available_funds - Available funds in this account. */
       registerAttribute("available_funds", "available_funds", "TradePortalDecimalAttribute");


       /* status_code - Status code from the bank. */
       registerAttribute("status_code", "status_code");


       /* deactivate_indicator - Whether the account is deactivated.  Defaults to not deactivated.

         Y=the account is deactivated
 */
       registerAttribute("deactivate_indicator", "deactivate_indicator", "IndicatorAttribute");


       /* available_for_internatnl_pay - If selected this account will be available in the debit from account number
         dropdown list for selection for International Payment (which used to be
         called Funds Transfer). */
       registerAttribute("available_for_internatnl_pay", "available_for_internatnl_pay", "IndicatorAttribute");


       /* available_for_xfer_btwn_accts - If selected this account will be available in the debit from account number
         dropdown list for selection for Transfer Between Accounts. */
       registerAttribute("available_for_xfer_btwn_accts", "available_for_xfer_btwn_accts", "IndicatorAttribute");


       /* available_for_direct_debit - If selected this account will display as available in the drop-down list
         of account numbers when creating a Direct Deposit. */
       registerAttribute("available_for_direct_debit", "available_for_direct_debit", "IndicatorAttribute");


       /* proponix_customer_id - The unique identifier of the corporate organization associated with the
         account.   This is used by TPS to match up corporate organizations on the
         portal with those stored in TPS. */
       registerAttribute("proponix_customer_id", "proponix_customer_id");


       /* fx_rate_group - The FX Rate Group (as defined in TPS) for the corporate customer associated
         with the account.  It is a simple TextAttribute and is not validated against
         REFDATA. */
       registerAttribute("fx_rate_group", "fx_rate_group");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

      /* Pointer to the parent Party */
       registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

       /* panel_auth_group_oid - An account could be associated with a Panel Authorization Group if this
         account is available for either International Payment or Domestic Payment
         or Transfer Between Accounts. */
       //Commenting below line for CR 821 - Rel 8.3
       //registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");

       /* op_bank_org_oid - The operational bank org that this account's is assigned to.  This should
         be the one of the corporation's associated Operational Bank Org. */
       registerAttribute("op_bank_org_oid", "a_op_bank_org_oid", "NumberAttribute");
       //Maheswar CR-610 Begin 28th Feb 2011 Begin
       registerAttribute("othercorp_customer_indicator", "othercorp_customer_indicator");
       registerAttribute("other_account_owner_oid", "other_account_owner_oid");
       //Maheswar CR-610 Begin 28th Feb 2011 End
       
     //CR 821 - Rel 8.3 START
       /* panel_auth_group_oid_1 to panel_auth_group_oid_16 - An account could be associated with multiple Panel Authorization Groups, up to 16, if this
       account is available for either International Payment or Domestic Payment
       or Transfer Between Accounts. */
       registerAttribute("panel_auth_group_oid_1", "a_panel_auth_group_oid_1", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_2", "a_panel_auth_group_oid_2", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_3", "a_panel_auth_group_oid_3", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_4", "a_panel_auth_group_oid_4", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_5", "a_panel_auth_group_oid_5", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_6", "a_panel_auth_group_oid_6", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_7", "a_panel_auth_group_oid_7", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_8", "a_panel_auth_group_oid_8", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_9", "a_panel_auth_group_oid_9", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_10", "a_panel_auth_group_oid_10", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_11", "a_panel_auth_group_oid_11", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_12", "a_panel_auth_group_oid_12", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_13", "a_panel_auth_group_oid_13", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_14", "a_panel_auth_group_oid_14", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_15", "a_panel_auth_group_oid_15", "NumberAttribute");
       registerAttribute("panel_auth_group_oid_16", "a_panel_auth_group_oid_16", "NumberAttribute");
       //CR 821 - Rel 8.3 END
       
     //SSikhakolli - Rel-9.4 CR-818 - adding new attribute
       registerAttribute("available_for_settle_instr", "available_for_settle_instr", "IndicatorAttribute");
     //dpatra - Rel-9.5 CR1051  -  adding new attribute
       registerAttribute("regulatory_account_type", "a_regulatory_account_type");
   }
}