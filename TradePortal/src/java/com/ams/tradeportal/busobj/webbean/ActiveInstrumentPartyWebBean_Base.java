

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ActiveInstrumentPartyWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* active_party_oid - Unique Identifier */
       registerAttribute("active_party_oid", "active_party_oid", "ObjectIDAttribute");


       /* terms_party_type - The type of terms party.  This information is also stored on the associated
         TermsParty but is replicated here for performance. */
       registerAttribute("terms_party_type", "terms_party_type");


       /* a_terms_party_oid - Association to terms party.  But cannot register as association since the
         terms party may not have been saved to the database yet and thus would not
         pass association validation. */
       registerAttribute("a_terms_party_oid", "a_terms_party_oid", "NumberAttribute");

      /* Pointer to the parent Instrument */
       registerAttribute("instrument_oid", "p_instrument_oid", "ParentIDAttribute");
   }
   
   




}
