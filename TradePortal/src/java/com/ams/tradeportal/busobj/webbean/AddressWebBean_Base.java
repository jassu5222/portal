

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AddressWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* address_oid - Unique Identifier */
       registerAttribute("address_oid", "address_oid", "ObjectIDAttribute");


       /* city - City */
       registerAttribute("city", "city");


       /* country - Country */
       registerAttribute("country", "country");


       /* address_line_1 - Address Line 1 */
       registerAttribute("address_line_1", "address_line_1");
       
      
       /* address_line_2 - Address Line 2 */
       registerAttribute("address_line_2", "address_line_2");


       /* state_province - State or Province */
       registerAttribute("state_province", "state_province");


       /* postal_code - Postal Code */
       registerAttribute("postal_code", "postal_code");


       /* address_seq_num - Sequence Number of Address (2-99) */
       registerAttribute("address_seq_num", "address_seq_num", "NumberAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");
       
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
	   //added attributes for Portal only Bank Additional address user defined fields
	    registerAttribute("user_defined_field_1", "user_defined_field_1");
	    registerAttribute("user_defined_field_2", "user_defined_field_2");
	    registerAttribute("user_defined_field_3", "user_defined_field_3");
	    registerAttribute("user_defined_field_4", "user_defined_field_4");
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
   }
   
   




}
