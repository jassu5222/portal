



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Parties and corporate customers can be set up to be related to accounts.
 * This account data is used on the loan request and funds transfer instruments.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class AnnouncementBankGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();

      /* announcement_bank_org_group_oid - Unique identifier */
      registerAttribute("announcement_bank_group_oid", "announcement_bank_group_oid", "ObjectIDAttribute");

      /* Pointer to the parent Announcement */
      registerAttribute("announcement_oid", "p_announcement_oid", "ParentIDAttribute");

      /* bog_oid - The bank org group association. */
      registerAttribute("bog_oid", "a_bog_oid", "NumberAttribute");

   }



}
