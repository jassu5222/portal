

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * This is the rule that a corporate customer can set up to match AR payment
 * and invoices.  This rule is sent to OTL.  It is not used in TP otherwise.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ArMatchingRuleWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* ar_matching_rule_oid -  */
       registerAttribute("ar_matching_rule_oid", "ar_matching_rule_oid", "ObjectIDAttribute");


       /* buyer_id - Buyer ID */
       registerAttribute("buyer_id", "buyer_id");


       /* buyer_name - Buyer Name */
       registerAttribute("buyer_name", "buyer_name");

		registerAttribute("part_to_validate", "part_to_validate", "LocalAttribute");
       /* tolerance_percent_indicator - Whether tolerance percent is used in matching. */
       registerAttribute("tolerance_percent_indicator", "tolerance_percent_indicator", "IndicatorAttribute");


       /* tolerance_percent_plus - Plus percentage of matching tolerance */
       registerAttribute("tolerance_percent_plus", "tolerance_percent_plus", "TradePortalDecimalAttribute");


       /* tolerance_percent_minus - Minus percent of matching tolerance. */
       registerAttribute("tolerance_percent_minus", "tolerance_percent_minus", "TradePortalDecimalAttribute");


       /* tolerance_amount_indicator - Whether tolerance amount is used in matching. */
       registerAttribute("tolerance_amount_indicator", "tolerance_amount_indicator", "IndicatorAttribute");


       /* tolerance_amount_plus - Plus amount used in matching tolerance. */
       registerAttribute("tolerance_amount_plus", "tolerance_amount_plus", "TradePortalDecimalAttribute");


       /* tolerance_amount_minus - Minus amount used in matching tolerance. */
       registerAttribute("tolerance_amount_minus", "tolerance_amount_minus", "TradePortalDecimalAttribute");


       /* no_tolerance_indicator - Y = No tolerance is used in matching. */
       registerAttribute("no_tolerance_indicator", "no_tolerance_indicator", "IndicatorAttribute");


       /* partial_pay_to_portal_indicator - Whether partially paid invoice is sent to portal. */
       registerAttribute("partial_pay_to_portal_indicator", "partial_pay_to_portal_ind", "IndicatorAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("corp_org_oid", "p_corp_org_oid", "ParentIDAttribute");

      /* address_line_1 - First line of the address */
       registerAttribute("address_line_1", "address_line_1");

	   /* address_line_1 - First line of the address */
       registerAttribute("address_line_2", "address_line_2");
		
		/* address_city - The city in which the party is located */
		registerAttribute("address_city", "address_city");
		
		/* address_state - The address_state in which the party is located */
		registerAttribute("address_state", "address_state");
		
		/* postalcode - The postalcode in which the party is located */
		registerAttribute("postalcode", "postalcode");		
		
	/* address_country - The address_country in which the party is located */
		registerAttribute("address_country", "address_country");
	
	/* creation_date_time - Timestamp (in GMT) of when the MAtching Rule was created. */
       registerAttribute("creation_date_time","creation_date_time");

       /* payment_day_allow - The payment_day_allow in which the party is located */
	   registerAttribute("payment_day_allow", "payment_day_allow");

	   /* days_before - The days_before in which the party is located */
		registerAttribute("days_before", "days_before","NumberAttribute");

		/* days_after - The days_after in which the party is located */
		registerAttribute("days_after", "days_after","NumberAttribute");

		/* invoice_value_finance - The invoice_value_finance in which the party is located */
		registerAttribute("invoice_value_finance", "invoice_value_finance","TradePortalDecimalAttribute");

		/* days_finance_of_payment - The days_finance_of_payment in which the party is located */
		registerAttribute("days_finance_of_payment", "days_finance_of_payment","NumberAttribute");
		
       /* receivable_invoice - The receivable_invoice in which the party is located */
	    registerAttribute("receivable_invoice", "receivable_invoice", "IndicatorAttribute");

		/* receivable_authorise - The receivable_authorise in which the party is located */
		  registerAttribute("receivable_authorise", "receivable_authorise");

		/* credit_authorise - The credit_authorise in which the party is located */
		registerAttribute("credit_note", "credit_note");

		/* credit_authorise - The credit_authorise in which the party is located */
		registerAttribute("credit_authorise", "credit_authorise");

		/* instrument_type - The instrument_type in which the tradepartner rule is located */
		// registerAttribute("instrument_type", "instrument_type"); //SHR CR-708 Rel8.1.1

		 /* individual_invoice_allowed - The individual_invoice_allowed in which the tradepartner rule is located */
	     registerAttribute("individual_invoice_allowed", "individual_invoice_allowed");

	    //SHR CR-708 Rel8.1.1 Start

		/* Set Of Payables Instrument Types */
		registerAttribute("pay_instrument_type", "pay_instrument_type");
		/* Set Of Receivables Instrument Types */
		registerAttribute("rec_instrument_type", "rec_instrument_type");

		//SHR CR-708 Rel8.1.1 End
		//AAlubala Rel8.2 CR741
		/* payment_gl_code - payment code  */
		registerAttribute("payment_gl_code", "payment_gl_code");
		/* over_payment_gl_code - overpayment code */
		registerAttribute("over_payment_gl_code", "over_payment_gl_code");		
		/* discount_gl_code */
		registerAttribute("discount_gl_code", "discount_gl_code");
		//CR741 - END
		
		// DK CR 709 Rel8.2 Start
		registerAttribute("rec_loan_type", "rec_loan_type");
		registerAttribute("pay_loan_type", "pay_loan_type");
		// DK CR 709 Rel8.2 End
		
		//Ravindra - CR-708B - Start
		registerAttribute("tps_customer_id", "tps_customer_id");
		//Ravindra - CR-708B - End

   }
   
   




}
