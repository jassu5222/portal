

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The Bank/Branch with the Bank/Branch code and address information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankBranchRuleWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* bank_branch_rule_oid - Object Identifier of the Bank Branch Rule. */
       registerAttribute("bank_branch_rule_oid", "bank_branch_rule_oid", "ObjectIDAttribute");


       /* bank_branch_code - Bank/Branch Code */
       registerAttribute("bank_branch_code", "bank_branch_code");


       /* bank_name - Bank Name */
       registerAttribute("bank_name", "bank_name");


       /* branch_name - Branch Name */
       registerAttribute("branch_name", "branch_name");


       /* address_line_1 - Address Line 1 */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Address Line 2 */
       registerAttribute("address_line_2", "address_line_2");


       /* opt_lock - Optimistic lock attribute.  We need this to take advantage the locking in
         the framework of RefDataMediator.  But we are not inheriting from ReferenceData
         since we do not need the other attributes.
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* payment_method - The Payment Method that is valid for the Bank/Branch and Country combination. */
       registerAttribute("payment_method", "payment_method");


       /* address_city - City */
       registerAttribute("address_city", "address_city");


       /* address_state_province - State or province */
       registerAttribute("address_state_province", "address_state_province");


       /* address_country - Country */
       registerAttribute("address_country", "address_country");


       /* unicode_indicator - Whether the Bank Branch Rule attributes have unicode characters that are
         not compatible with WIN-1252 (WE8MSWIN1252) encoding.  Such Bank Branches
         can only be used for Beneficiary Bank, not for First Intermediary Bank or
         Second Intermediary Bank. */
       registerAttribute("unicode_indicator", "unicode_indicator", "IndicatorAttribute");
   }
   
   




}
