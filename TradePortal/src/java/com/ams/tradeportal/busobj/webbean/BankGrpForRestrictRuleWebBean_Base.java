package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;

import javax.ejb.*;

import com.ams.tradeportal.common.*;

import javax.servlet.http.*;

import com.ams.tradeportal.html.*;

/**
 * Business object to store commonly used BankGrpRestrictRules
 *
 * Copyright � 2015 CGI, Incorporated All rights reserved
 */
public class BankGrpForRestrictRuleWebBean_Base extends
		TradePortalBusinessObjectWebBean {

	public void registerAttributes() {
		super.registerAttributes();

		/* bank_grp_for_restrict_rls_oid - Unique Identifier */
		registerAttribute("bank_grp_for_restrict_rls_oid",
				"BANK_GRP_FOR_RESTRICT_RLS_OID", "ObjectIDAttribute");

		/* Pointer to the parent User */
		registerAttribute("bank_grp_restrict_rules_oid",
				"P_BANK_GRP_RESTRICT_RULES_OID", "ParentIDAttribute");

		/* bank_organization_group_oid - */
		registerAttribute("bank_organization_group_oid",
				"BANK_ORGANIZATION_GROUP_OID", "NumberAttribute");
	}

}
