
package com.ams.tradeportal.busobj.webbean;

/**
 * Business object to store a message or discrepancy notice.   These messages
 * can be created in the back end system and placed into this table by the
 * middleware or they can be created by portal users and sent to the back end
 * system via the middleware.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class BankMailMessageWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* message_oid - Unique identifier */
       registerAttribute("bank_mail_message_oid", "bank_mail_message_oid", "ObjectIDAttribute");


       /* message_text - Text that comprises the body of the message */
       registerAttribute("message_text", "message_text");


       /* message_subject - Text that comprises the subject of the mail message */
       registerAttribute("message_subject", "message_subject");

       /* message_source_type - Indicates whether the message was created on the back end system or created
         on the Trade Portal */
       registerAttribute("message_source_type", "message_source_type");


       /* message_status - The status in the lifecycle of a mail message.   Statuses can include Received,
         Draft, Sent to Bank, etc. */
       registerAttribute("message_status", "message_status");


       

       /* unread_flag - Indicates whether or not the message has been read by its intended recipient.
         Yes = message has not been read */
       registerAttribute("unread_flag", "unread_flag", "IndicatorAttribute");


       /* last_update_date - Timestamp in GMT of when any part of this message (including status) was
         updated. */
       registerAttribute("last_update_date", "last_update_date", "DateTimeAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* complete_instrument_id - The ID of the related instrument.  Used only if the mail message is received
         before its related instrument is created.  That could happen if the messages
         get out of order in the middleware.  When the UNV message later comes in
         and creates the instrument, proper a_related_instrument_oid will be set. */
       registerAttribute("complete_instrument_id", "complete_instrument_id");


       /* assigned_to_corp_org_oid - The corporate organization to which a mail message is assigned.    This
         will never be empty.  When a mail message is assigned to a user, it is also
         still assigned to the corporate organization. */
       registerAttribute("assigned_to_corp_org_oid", "a_assigned_to_corp_org_oid", "NumberAttribute");

       /* related_instrument_oid - A mail message can be related to an instrument either by the user's action
         or by the middleware setting it.   This represents that the mail message
         contains information in regards to the related instrument. */
       registerAttribute("related_instrument_oid", "a_related_instrument_oid", "NumberAttribute");

       /* assigned_to_user_oid - The user to which a mail message is currently assigned. */
       registerAttribute("assigned_to_user_oid", "a_assigned_to_user_oid", "NumberAttribute");
       

       /* is_reply - Inidcates whether or not this message is a reply to another message.   A
         Trade Portal user can create a message by pressing the 'Reply' button. 
         If this is done, this flag is set.  Yes = the message is a reply. */
       registerAttribute("is_reply", "is_reply", "IndicatorAttribute");


         
       registerAttribute("bank_instrument_id", "bank_instrument_id");

   }
   
   




}
