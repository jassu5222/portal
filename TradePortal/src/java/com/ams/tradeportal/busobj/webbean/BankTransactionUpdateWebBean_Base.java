package com.ams.tradeportal.busobj.webbean;

/**
 *
 *
 */
public class BankTransactionUpdateWebBean_Base extends TradePortalBusinessObjectWebBean{
	
	public void registerAttributes()
	   {  
	      super.registerAttributes();

	      /* bank_transaction_update_oid - Unique Identifier */
	     registerAttribute("bank_transaction_update_oid", "bank_transaction_update_oid", "ObjectIDAttribute");
	     
	      /*bank_transaction_status - REFDATA table Type - BANK_TRANSACTION_STATUS*/
	     registerAttribute("bank_transaction_status", "bank_transaction_status");
	      
	      /*bank_rejection_reason_text - Rejection reason*/
	     registerAttribute("bank_rejection_reason_text", "bank_rejection_reason_text");
	      
	     registerAttribute("bank_internal_info", "bank_internal_info");
	      
	      /*bank_transaction_status - REFDATA table Type - BANK_UPDATE_STATUS_TYPE*/
	     registerAttribute("bank_update_status", "bank_update_status");
	      
	      /* user_oid -  */
	      registerAttribute("bank_user_oid", "a_bank_user_oid", "NumberAttribute");
	      
	      /* opt_lock - Optimistic lock attribute
	         See jPylon documentation for details on how this works */
	     registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
	      
	      /* transaction_oid - The transaction which initiated the request */
	      registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");

	   }

}
