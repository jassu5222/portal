



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * This business object represents a business organization that is a customer
 * of the client banks.   Corporate organizations apply for instruments and,
 * by authorizing them, send them to the bank.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class CorporateOrganizationWebBean_Base extends ReferenceDataOwnerWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* address_city - City where the corporate organization is located. */
       registerAttribute("address_city", "address_city");


       /* address_country - Country where the corporate organization is located. */
       registerAttribute("address_country", "address_country");
       
    
       /* address_line_1 - Address line 1 for this organization */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Address line 2 for this organization */
       registerAttribute("address_line_2", "address_line_2");


       /* address_postal_code - Postal code  where the corporate organization is located. */
       registerAttribute("address_postal_code", "address_postal_code");


       /* address_state_province - State or province   where the corporate organization is located. */
       registerAttribute("address_state_province", "address_state_province");


       /* allow_airway_bill - Indicates whether or not this corporate organization can create Air Waybill
         transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_airway_bill", "allow_airway_bill", "IndicatorAttribute");


       /* allow_auto_lc_create - Indicates whether or not this corporate organization has access to the Auto
         LC Create (by uploading Purchase Orders) functionality.

         yes=this organization has access */
       registerAttribute("allow_auto_lc_create", "allow_auto_lc_create", "IndicatorAttribute");


       /* allow_create_bank_parties - Indicates whether or not this corporate organization can create reference
         data parties that have a type of Bank.   If this is set to No, they can
         only create reference data parties with a type of Corporate */
       registerAttribute("allow_create_bank_parties", "allow_create_bank_parties", "IndicatorAttribute");


       /* allow_export_collection - Indicates whether or not this corporate organization can create Direct Send
         Collection transactions.  (For a proper Export Collection from TPS perspective,
         see allow_new_export_collection.)

         Yes=Corporate organization can create */
       registerAttribute("allow_export_collection", "allow_export_collection", "IndicatorAttribute");


       /* allow_export_LC - Indicates whether or not this corporate organization can create Export LC
         transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_export_LC", "allow_export_LC", "IndicatorAttribute");


       /* allow_funds_transfer - Indicates whether or not this corporate organization can create Funds Transfer
         transactions using user thresholds.

         Yes=Corporate organization can create */
       registerAttribute("allow_funds_transfer", "allow_funds_transfer", "IndicatorAttribute");


       /* allow_guarantee - Indicates whether or not this corporate organization can create Guarantee
         transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_guarantee", "allow_guarantee", "IndicatorAttribute");


       /* allow_import_DLC - Indicates whether or not this corporate organization can create Import LC
         transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_import_DLC", "allow_import_DLC", "IndicatorAttribute");


       /* allow_loan_request - Indicates whether or not this corporate organization can create Loan Request
         transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_loan_request", "allow_loan_request", "IndicatorAttribute");


       /* allow_manual_po_entry - Indicates whether or not this corporate organization has the ability to
         manually enter POs.

         yes=this organization has access */
       registerAttribute("allow_manual_po_entry", "allow_manual_po_entry", "IndicatorAttribute");


       /* allow_request_advise - Indicates whether or not this corporate organization can create Request
         to Advise transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_request_advise", "allow_request_advise", "IndicatorAttribute");
       
     //Ravindra - CR-708B - Start
       /* allow_supplier_portal - Indicates whether or not this corporate organization can create Supplier Portal transactions.

 	      Yes=Corporate organization can create */
 	   registerAttribute("allow_supplier_portal", "allow_supplier_portal", "IndicatorAttribute");
 	 //Ravindra - CR-708B - End


       /* allow_shipping_guar - Indicates whether or not this corporate organization can create Shipping
         Guarantee transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_shipping_guar", "allow_shipping_guar", "IndicatorAttribute");


       /* allow_SLC - Indicates whether or not this corporate organization can create Standby
         LC transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_SLC", "allow_SLC", "IndicatorAttribute");


       /* auth_user_different_ind - A setting indicating whether or not the authorizing user must be different
         than the last entry user.   This is enforced when all transactions are authorized
         by this corporate customer's users.

         Yes=Authorizing user must be different from last entry user. */
       registerAttribute("auth_user_different_ind", "auth_user_different_ind", "IndicatorAttribute");


       /* authentication_method - Determines how users of the corporate organization will authenticate themselves. */
       registerAttribute("authentication_method", "authentication_method");


       /* base_currency_code - The currency is which this corporate organization typically conducts business */
       registerAttribute("base_currency_code", "base_currency_code");


       /* corporate_org_type_code - Indicates whether this corporate organization is a bank or a non-bank corporation.

BANK/CORP=Bank/Corporate */
       registerAttribute("corporate_org_type_code", "corporate_org_type_code");


       /* creation_date - Timestamp (in GMT) when the corporate organization was created. */
       registerAttribute("creation_date", "creation_date", "DateTimeAttribute");


       /* dnld_adv_terms_format - Determines the format in which the user can download the Advice terms. */
       registerAttribute("dnld_adv_terms_format", "dnld_adv_terms_format");


       /* doc_prep_accountname - The company account name for this corporate organization in the document
         prep system.  This information is sent to the doc prep system during single
         sign-on. */
       registerAttribute("doc_prep_accountname", "doc_prep_accountname");


       /* doc_prep_domainname - The domain of the document prep system that the user of this corporation
         uses by default.  This is unique to the TradeBeam system we are using now.
         This information is sent to TradeBeam during single sign-on. */
       registerAttribute("doc_prep_domainname", "doc_prep_domainname");


       /* doc_prep_ind - Indicates whether or not users of the corporate customer will have access
         to the document preparation software (i.e. corporate service) that is integrated
         into the Trade Portal. */
       registerAttribute("doc_prep_ind", "doc_prep_ind", "IndicatorAttribute");


       /* dual_auth_airway_bill - Indicates whether or not dual authorization is required for  Air Waybill
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_airway_bill", "dual_auth_airway_bill");


       /* dual_auth_disc_response - Indicates whether or not dual authorization is required for  Discrepancy
         Response transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         I=Use Instrument Type Authorization Defaults */
       registerAttribute("dual_auth_disc_response", "dual_auth_disc_response");


       /* dual_auth_export_coll - Indicates whether or not dual authorization is required for  Direct Send
         Collection transactions.  (For proper Export Collection from TPS perspective,
         see dual_auth_new_export_coll)

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_export_coll", "dual_auth_export_coll");


       /* dual_auth_export_LC - Indicates whether or not dual authorization is required for  Export LC transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_export_LC", "dual_auth_export_LC");


       /* dual_auth_funds_transfer - Indicates whether or not dual authorization is required for  Funds Transfers
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization */
       registerAttribute("dual_auth_funds_transfer", "dual_auth_funds_transfer");


       /* dual_auth_guarantee - Indicates whether or not dual authorization is required for  Guarantee transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_guarantee", "dual_auth_guarantee");


       /* dual_auth_import_DLC - Indicates whether or not dual authorization is required for  Import LC transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_import_DLC", "dual_auth_import_DLC");


       /* dual_auth_loan_request - Indicates whether or not dual authorization is required for  Loan Request
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_loan_request", "dual_auth_loan_request");
     //Ravindra - CR-708B - Start
       /* dual_auth_supplier_portal - Indicates whether or not dual authorization is required for Supplier Portal transactions

       N or null=Can be authorized by one user
       Y=Must be authorized by two users
       G=Must be authorized by two users that belong to different work groups */
 	   registerAttribute("dual_auth_supplier_portal", "dual_auth_supplier_portal");
 	 //Ravindra - CR-708B - End


       /* dual_auth_request_advise - Indicates whether or not dual authorization is required for  Request to
         Advise transactions

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_request_advise", "dual_auth_request_advise");


       /* dual_auth_shipping_guar - Indicates whether or not dual authorization is required for  Shipping Guarantee
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_shipping_guar", "dual_auth_shipping_guar");


       /* dual_auth_SLC - Indicates whether or not dual authorization is required for  Standby transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_SLC", "dual_auth_SLC");


       /* email_language - Determines which language email notifications should be in. */
       registerAttribute("email_language", "email_language");


       /* email_receiver - Determines which user(s) will receive an email when a new notification come
         into the Portal */
       registerAttribute("email_receiver", "email_receiver");


       /* force_certs_for_user_maint - Whether the users of this corporate customer who can maintain users must
         use certificate authentication.  This attribute is applicable only when
         the corporate customer's authentication method is configured for each user
         individually. */
       registerAttribute("force_certs_for_user_maint", "force_certs_for_user_maint", "IndicatorAttribute");


       /* last_email_date - Date (in GMT) when the last email reminder was sent. */
       registerAttribute("last_email_date", "last_email_date", "DateAttribute");


       /* Proponix_customer_id - The unique identifier of this corporate organization on OTL, the back end
         system.   This is not used by the Trade Portal, but is used by OTL to match
         up corporate organizations on the portal with those stored in OTL. */
       registerAttribute("Proponix_customer_id", "Proponix_customer_id");


       /* route_within_hierarchy - Determine how the user is able to route within their hierarchy of organizations. */
       registerAttribute("route_within_hierarchy", "route_within_hierarchy");


       /* standbys_use_either_form - Indicates whether or not the users of this corporate organization will be
         given an option of which form to use when creating a Standby LC.   If this
         is set to Yes, the users will be given the option between the Standby Short
         Form (real Standby LC form) and the Standby Long Form (real Guarantee form). */
       registerAttribute("standbys_use_either_form", "standbys_use_either_form", "IndicatorAttribute");


       /* standbys_use_guar_form_only - Indicates that when this corporate organization creates a "Standby LC",
         it will use the same form as a "Guarantee".   The users of this corporate
         org will not have the option to choose between the original standby form
         and the guarantee form.

         Yes=organizations must use the guarantee form for all standbys. */
       registerAttribute("standbys_use_guar_form_only", "standbys_use_guar_form_only", "IndicatorAttribute");


       /* timeout_auto_save_ind - Whether the data that users have entered on web page would be automatically
         saved before time-outs. */
       registerAttribute("timeout_auto_save_ind", "timeout_auto_save_ind", "IndicatorAttribute");


       /* allow_atp_manual_po_entry - Indicates whether or not this corporate organization has the ability to
         manually enter POs on Approval To Pay transactions.

         yes=this organization has access */
       registerAttribute("allow_atp_manual_po_entry", "allow_atp_manual_po_entry", "IndicatorAttribute");


       /* allow_approval_to_pay - Indicates whether or not this corporate organization can create Approval
         To Pay transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_approval_to_pay", "allow_approval_to_pay", "IndicatorAttribute");


       /* dual_auth_approval_to_pay - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_approval_to_pay", "dual_auth_approval_to_pay");


       /* allow_auto_atp_create - Indicates whether or not this corporate organization has access to the Auto
         "Approval to Pay" Create (by uploading Purchase Orders) functionality.

         yes=this organization has access */
       registerAttribute("allow_auto_atp_create", "allow_auto_atp_create", "IndicatorAttribute");


       /* allow_arm - Indicates whether or not this corporate organization can create Account
         Receivable Management  transactions.

         Yes=Corporate organization can create */
       registerAttribute("allow_arm", "allow_arm", "IndicatorAttribute");


       /* dual_auth_arm - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_arm", "dual_auth_arm");


       /* enforce_daily_limit_transfer - When the indicator is set, daily limits should be enforced on Transfers
         between Accounts. */
       registerAttribute("enforce_daily_limit_transfer", "enforce_daily_limit_transfer", "IndicatorAttribute");


       /* transfer_daily_limit_amt - When enforce_daily_limit_transfer is set, this amount provides the daily
         limit for transferring between accounts. */
       registerAttribute("transfer_daily_limit_amt", "transfer_daily_limit_amt", "TradePortalDecimalAttribute");


       /* enforce_daily_limit_dmstc_pymt - When the indicator is set, daily limits should be enforced on Domestic Payments. */
       registerAttribute("enforce_daily_limit_dmstc_pymt", "enforce_daily_limit_dmstc_pymt", "IndicatorAttribute");


       /* domestic_pymt_daily_limit_amt - When enforce_daily_limit_domestic_pymt is set, this amount provides the
         daily limit for domestic payments. */
       registerAttribute("domestic_pymt_daily_limit_amt", "domestic_pymt_daily_limit_amt", "TradePortalDecimalAttribute");


       /* enforce_daily_limit_intl_pymt - When the indicator is set, daily limits should be enforced on International
         Payments. */
       registerAttribute("enforce_daily_limit_intl_pymt", "enforce_daily_limit_intl_pymt", "IndicatorAttribute");


       /* intl_pymt_daily_limit_amt - When enforce_daily_limit_international_pymt is set, this amount provides
         the daily limit for domestic payments. */
       registerAttribute("intl_pymt_daily_limit_amt", "intl_pymt_daily_limit_amt", "TradePortalDecimalAttribute");


       /* timezone_for_daily_limit - The timezone for daily limit  indicates what day it is for the Corporate
         Customer and will be used for zeroing out the Daily balance running totals. */
       registerAttribute("timezone_for_daily_limit", "timezone_for_daily_limit");


       /* allow_xfer_btwn_accts - Indicates whether or not this corporate organization can transfer between
         accounts using user thresholds.

         Yes=Corporate organization can create */
       registerAttribute("allow_xfer_btwn_accts", "allow_xfer_btwn_accts", "IndicatorAttribute");


       /* dual_xfer_btwn_accts - Indicates whether or not dual authorization is required for  Approval To
         Pay transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization */
       registerAttribute("dual_xfer_btwn_accts", "dual_xfer_btwn_accts");


       /* allow_domestic_payments - Indicates whether or not this corporate organization can make domestic payments
         using user thresholds.

         Yes=Corporate organization can create */
       registerAttribute("allow_domestic_payments", "allow_domestic_payments", "IndicatorAttribute");


       /* dual_domestic_payments - Indicates whether or not dual authorization is required for  Domestic Payment
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups
         P=Must be authorized using Panel Authorization
 */
       registerAttribute("dual_domestic_payments", "dual_domestic_payments");


       /* allow_pymt_instrument_auth - Indicates whether Payment Instrument Capabilities and Authorization Settings
         are enabled for this profile.

         Yes=Payment Instrument authorization is enabled */
       registerAttribute("allow_pymt_instrument_auth", "allow_pymt_instrument_auth", "IndicatorAttribute");


       /* allow_panel_auth_for_pymts - Indicates whether to allow Authorization with Panel Levels for Payment Instruments.

         Yes=Authorization with Panel Levels is enabled
         No=Authorization with User Thresholds is enabled */
       registerAttribute("allow_panel_auth_for_pymts", "allow_panel_auth_for_pymts", "IndicatorAttribute");


       /* allow_xfer_btwn_accts_panel - Indicates whether or not this corporate organization can transfer between
         accounts using panel levels.

         Yes=Corporate organization can create */
       registerAttribute("allow_xfer_btwn_accts_panel", "allow_xfer_btwn_accts_panel", "IndicatorAttribute");


       /* allow_domestic_payments_panel - Indicates whether or not this corporate organization can make domestic payments
         using panel levels.

         Yes=Corporate organization can create */
       registerAttribute("allow_domestic_payments_panel", "allow_domestic_payments_panel", "IndicatorAttribute");


       /* allow_funds_transfer_panel - Indicates whether or not this corporate organization can create Funds Transfer
         transactions using panel levels.

         Yes=Corporate organization can create */
       registerAttribute("allow_funds_transfer_panel", "allow_funds_transfer_panel", "IndicatorAttribute");


       /* doc_prep_url - The document preparation (i.e. corporate service) URL. */
       registerAttribute("doc_prep_url", "doc_prep_url");


       /* reporting_type - Reporting Type indicates whether the reporting type for the user of this
         corporate org is Cash Management or Trade Services. */
       registerAttribute("reporting_type", "reporting_type");


       /* cust_daily_balance_amt_ftrq - This amount tracks the daily running balance for funds transfer requests.
         The balance only applies to the date stored in cust_daily_balance_date,
         and the amount is reset to zero with the first transaction of each day. */
       registerAttribute("cust_daily_balance_amt_ftrq", "cust_daily_balance_amt_ftrq", "TradePortalDecimalAttribute");


       /* cust_daily_balance_amt_ftdp - This amount tracks the daily running balance for domestic payments.  The
         balance only applies to the date stored in cust_daily_balance_date, and
         the amount is reset to zero with the first transaction of each day. */
       registerAttribute("cust_daily_balance_amt_ftdp", "cust_daily_balance_amt_ftdp", "TradePortalDecimalAttribute");


       /* cust_daily_balance_amt_ftba - This amount tracks the daily running balance for funds transfer between
         accounts.  The balance only applies to the date stored in cust_daily_balance_date,
         and the amount is reset to zero with the first transaction of each day. */
       registerAttribute("cust_daily_balance_amt_ftba", "cust_daily_balance_amt_ftba", "TradePortalDecimalAttribute");


       /* cust_daily_balance_date - Timestamp of the current processing date associated with the cust_daily_balance_amt.
         When the balance  is reset to zero with the first transaction of each day,
         this field is set to the current processing date. */
       registerAttribute("cust_daily_balance_date", "cust_daily_balance_date", "DateTimeAttribute");


       /* fx_rate_group - The FX Rate Group (as defined in OTL) for the customer/base currency.  It
         is a simple TextAttribute and is not validated against REFDATA. */
       registerAttribute("fx_rate_group", "fx_rate_group");
       /* Suresh CR-713 Rel 8.0 -  interest_disc_rate_group - Interest Discount Rate Type */
       registerAttribute("interest_disc_rate_group", "interest_disc_rate_group");


       /* allow_non_prtl_org_instr_ind - Indicates whether this corporate organization can view instruments originated
         from OTL.  It is possible that a corporate organization cannot create any
         instruments but still can view those originated from OTL.

         Yes=Corporate organization can view */
       registerAttribute("allow_non_prtl_org_instr_ind", "allow_non_prtl_org_instr_ind", "IndicatorAttribute");


       /* allow_direct_debit - Indicates whether or not this corporate organization can do Direct Debit.

Y=Yes */
       registerAttribute("allow_direct_debit", "allow_direct_debit", "IndicatorAttribute");


       /* dual_auth_direct_debit - Indicates whether or not dual authorization is required for  Direct Debit
         transactions.

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_direct_debit", "dual_auth_direct_debit");


       /* allow_new_export_collection - Indicates whether or not this corporate organization can create Export Collection
         transactions.  (This is the proper Export Collection from TPS perspective.
         allow_export_collection is only a Direct Send collection.) */
       registerAttribute("allow_new_export_collection", "allow_new_export_collection", "IndicatorAttribute");


       /* dual_auth_new_export_coll - Indicates whether or not dual authorization is required for  Export Collection
         transactions.  (dual_auth_export_coll is only for Direct Send Collection.)

         N or null=Can be authorized by one user
         Y=Must be authorized by two users
         G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_new_export_coll", "dual_auth_new_export_coll");

       /* client_bank_oid - Each corporate organization is associated to a bank organization group,
         which is then "owned" in the Trade Portal by a client bank.   This association
         is a shortcut to the client bank from corporate org to make reporting more
         efficient. */
       registerAttribute("client_bank_oid", "a_client_bank_oid", "NumberAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("parent_corp_org_oid", "p_parent_corp_org_oid", "ParentIDAttribute");

       /* bank_org_group_oid - Each corporate customer is assigned to a bank oranization group.   This
         essentially means that the corporate organization thinks that they are doing
         business with the particalar BOG.  This association is used to determine
         which branding directory to use for each corporate customer's users. */
       registerAttribute("bank_org_group_oid", "a_bank_org_group_oid", "NumberAttribute");

       /* first_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
       registerAttribute("first_op_bank_org", "a_first_op_bank_org", "NumberAttribute");

       /* second_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
       registerAttribute("second_op_bank_org", "a_second_op_bank_org", "NumberAttribute");

       /* third_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
       registerAttribute("third_op_bank_org", "a_third_op_bank_org", "NumberAttribute");

       /* fourth_op_bank_org - Each corporate customer can do business with up to four operational bank
         organizations.  Instead of creating a one-to-many association, four one-to-one
         assocaitions were created to make coding simpler. */
       registerAttribute("fourth_op_bank_org", "a_fourth_op_bank_org", "NumberAttribute");
       //Suresh CR-603 03/11/2011 Begin
       /* first_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("first_rept_categ", "first_rept_categ");

       /* second_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("second_rept_categ", "second_rept_categ");

       /* third_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("third_rept_categ", "third_rept_categ");

       /* fourth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("fourth_rept_categ", "fourth_rept_categ");

       /* fifth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("fifth_rept_categ", "fifth_rept_categ");

       /* sixth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("sixth_rept_categ", "sixth_rept_categ");

       /* seventh_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("seventh_rept_categ", "seventh_rept_categ");

       /* eighth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("eighth_rept_categ", "eighth_rept_categ");

       /* nineth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("nineth_rept_categ", "nineth_rept_categ");

       /* tenth_rept_categ - Each corporate customer can do business with up to ten report
       categories.  Instead of creating a one-to-many association, ten one-to-one
       assocaitions were created to make coding simpler. */
       registerAttribute("tenth_rept_categ", "tenth_rept_categ");
       //Suresh CR-603 03/11/2011 End

              
       //Vshah - CR#564 - [BEGIN]
       /* allow_payment_file_upload - Indicates whether or not this corporate organization can Process Payment thru File Upload. */
       registerAttribute("allow_payment_file_upload", "allow_payment_file_upload", "IndicatorAttribute");
       //Vshah - CR#564 - [END]

       //Narayan CR-644 Begin
       /* alternate_name - Alternate Corporate Customer Name*/
       registerAttribute("alternate_name", "alternate_name");
       //Narayan CR-644 End

       // CJR - 04/06/2011 - CR-593 Begin
       /* auto_auth_incoming_payment - Indicates whether to automatically authorize incoming payments. */
       registerAttribute("straight_through_authorize", "straight_through_authorize");

       /* h2h_system_user - Host to Host System User. */
       registerAttribute("h2h_system_user_oid", "a_h2h_system_user_oid", "NumberAttribute");
       // CJR - 04/06/2011 - CR-593 End
    // Leelavathi CR-710 Rel-8.0 08/12/2011 Begin
       registerAttribute("upload_system_user_oid", "a_upload_system_user_oid", "NumberAttribute");
       // Leelavathi CR-710 Rel-8.0 08/12/2011 End

       // DK CR-640 Rel7.1 BEGINS
       /* fx_online_acct_id - FX Online Account ID */
       registerAttribute("fx_online_acct_id", "fx_online_acct_id");
       // DK CR-640 Rel7.1 ENDS

     //Leelavathi CR-710 Rel 8.0.0 24/10/2011 Begin
       /* allow_arm_invoice - Indicates whether or not this corporate organization can create Receivable Management invoices.
       Yes=Corporate organization can create */
       registerAttribute("allow_arm_invoice", "allow_arm_invoice", "IndicatorAttribute");

       /* dual_auth_arm_invoice - Indicates whether or not dual authorization is required for Receivable Management Invoices.
       	N or null=Can be authorized by one user
       	Y=Must be authorized by two users
       	G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_arm_invoice", "dual_auth_arm_invoice");
       /* allow_invoice_file_upload - Indicates whether or not this corporate organization can Upload invoices. */
       registerAttribute("allow_invoice_file_upload", "allow_invoice_file_upload", "IndicatorAttribute");
     //Leelavathi CR-710 Rel 8.0.0 24/10/2011 End

     //Leelavathi CR-710 Rel 8.0.0 23/11/2011 Begin
       /* allow_credit_note - Indicates whether or not this corporate organization can perform Credit Note Authorization.
       Yes=Corporate organization can create */
       registerAttribute("allow_credit_note", "allow_credit_note", "IndicatorAttribute");

       /* dual_auth_credit_note - Indicates whether or not dual authorization is required for Credit Note.
       N or null=Can be authorized by one user
       Y=Must be authorized by two users
       G=Must be authorized by two users that belong to different work groups */
       registerAttribute("dual_auth_credit_note", "dual_auth_credit_note");

       /* allow_grouped_invoice_upload - Indicates whether or not this corporate organization can Upload grouped invoices. */
       registerAttribute("allow_grouped_invoice_upload", "allow_grouped_invoice_upload", "IndicatorAttribute");

       /* allow_restricted_invoice_upload - Indicates whether or not this corporate organization can Restrict Invoice Upload Directory */
       registerAttribute("allow_restricted_inv_upload", "allow_restricted_inv_upload", "IndicatorAttribute");

       /* restricted_inv_upload_directory - Indicates directory / drive to which invoice uploads should be restricted */
       registerAttribute("restricted_inv_upload_dir", "restricted_inv_upload_dir");

     //Leelavathi CR-710 Rel 8.0.0 23/11/2011 End
     //Leelavathi CR-710 Rel 8.0.0 28/11/2011 Begin
       registerAttribute("interest_discount_ind", "interest_discount_ind");
       registerAttribute("calculate_discount_ind", "calculate_discount_ind");

      //Leelavathi CR-710 Rel 8.0.0 28/11/2011 End
	  //Leelavathi CR-707 Rel 8.0.0 02/02/2012 Begin      
       /* allow_purchase_order_upload - Indicates whether purchase order upload is available to this corporate organization.
       
       Yes=Purchase order upload is available. */
    registerAttribute("allow_purchase_order_upload", "allow_purchase_order_upload", "IndicatorAttribute");
    
    /* po_upload_format - Determine whether the Customer will process Purchase Order Uploads files
       in the new structured format or whether they will be processed in the existing
       format which will continue to be stored as text in the Import DLC or Approval
       to Payment application forms.
       
       S=Structured format
       T=Existing Text format
 */
    registerAttribute("po_upload_format", "po_upload_format");
    
    /* allow_restricted_po_upload - Indicates whether PO upload should be restricted to a specific folder */
    registerAttribute("allow_restricted_po_upload", "allow_restricted_po_upload", "IndicatorAttribute");
    
    /* restricted_po_upload_dir - Folder/directory where corporate customer are allowed to upload PO file from. */
    registerAttribute("restricted_po_upload_dir", "restricted_po_upload_dir");
    
    
  //jgadela CR ANZ 501 Rel 8.3 03/28/2013 START 
    /* corp_user_dual_ctrl_reqd_ind - Indicates whether Corporate Organization user reference data require dual
    control for maintenance. */
    registerAttribute("corp_user_dual_ctrl_reqd_ind", "corp_user_dual_ctrl_reqd_ind", "IndicatorAttribute");
    /* panel_auth_dual_ctrl_reqd_ind - Indicates whether Corporate Organization user reference data require dual
    control for maintenance. */
    registerAttribute("panel_auth_dual_ctrl_reqd_ind", "panel_auth_dual_ctrl_reqd_ind", "IndicatorAttribute");
  //jgadela CR ANZ 501 Rel 8.3 03/28/2013 END 
    
    
    //Leelavathi CR-707 Rel 8.0.0 02/02/2012 End

   // Srinivasu_D CR-708 Rel8.1.1 05/11/2012 Start

	 registerAttribute("atp_invoice_indicator", "atp_invoice_ind", "IndicatorAttribute");

	 registerAttribute("failed_invoice_indicator", "failed_invoice_ind", "IndicatorAttribute");
	
	 registerAttribute("payment_day_allow", "payment_day_allow_ind");
	  
	 registerAttribute("days_before", "days_before","NumberAttribute");
	
	 registerAttribute("days_after", "days_after","NumberAttribute");	
		
	 // Srinivasu_D CR-708 Rel8.1.1 05/11/2012 End
	 
     //AAlubala - Rel8.2 CR741 - BEGIN
     registerAttribute("erp_transaction_rpt_reqd", "erp_transaction_rpt_reqd", "IndicatorAttribute"); //ERP_TRANSACTION_RPT_REQD
     registerAttribute("corp_discount_code_type", "corp_discount_code_type"); //CORP_DISCOUNT_CODE_TYPE
     //END	 
    // DK CR709 Rel8.2 10/31/2012 BEGINS  
    registerAttribute("process_invoice_file_upload", "process_invoice_file_upload", "IndicatorAttribute");
    registerAttribute("trade_loan_type", "trade_loan_type","IndicatorAttribute");
    registerAttribute("import_loan_type", "import_loan_type", "IndicatorAttribute");
    registerAttribute("export_loan_type", "export_loan_type", "IndicatorAttribute");
    registerAttribute("invoice_financing_type", "invoice_financing_type",  "IndicatorAttribute");
    registerAttribute("allow_auto_loan_req_inv_upld", "allow_auto_loan_req_inv_upld", "IndicatorAttribute");
    registerAttribute("use_payment_date_allowed_ind", "use_payment_date_allowed_ind", "IndicatorAttribute");
    registerAttribute("days_before_payment_date", "days_before_payment_date","NumberAttribute");
    registerAttribute("days_after_payment_date", "days_after_payment_date","NumberAttribute");
    registerAttribute("fin_days_before_loan_maturity", "fin_days_before_loan_maturity","NumberAttribute");
    registerAttribute("inv_percent_to_fin_trade_loan", "inv_percent_to_fin_trade_loan", "TradePortalDecimalAttribute");
    registerAttribute("allow_auto_atp_inv_only_upld", "allow_auto_atp_inv_only_upld", "IndicatorAttribute");
    // DK CR709 Rel8.2 10/31/2012 ENDS  	 
	//Ravindra - Rel8200 CR-708B - Start
	  /* sp_calculate_discount_ind - S/D whether to calculate supplier portal invoice 
	   	with Straight discount or Discount to yield. */
	  registerAttribute("sp_calculate_discount_ind", "sp_calculate_discount_ind");
	  //Ravindra - Rel8200 CR-708B - End
	  
	//M Eerupula 04/16/2013 Rel8.3 CR776 
	  registerAttribute("payables_prog_name" ,  "payables_prog_name");
	  
	  
	  //M Eerupula 04/22/2013 Rel 8.3 CR 821
	  registerAttribute("inherit_panel_auth_ind" , "inherit_panel_auth_ind" , "IndicatorAttribute");
	  registerAttribute("panel_level_A_alias" , "panel_level_A_alias"); 
	  registerAttribute("panel_level_B_alias" , "panel_level_B_alias");
	  registerAttribute("panel_level_C_alias" , "panel_level_C_alias"); 
	  registerAttribute("panel_level_D_alias" , "panel_level_D_alias"); 
	  registerAttribute("panel_level_E_alias" , "panel_level_E_alias");
	  registerAttribute("panel_level_F_alias" , "panel_level_F_alias"); 
	  registerAttribute("panel_level_G_alias" , "panel_level_G_alias"); 
	  registerAttribute("panel_level_H_alias" , "panel_level_H_alias");
	  registerAttribute("panel_level_I_alias" , "panel_level_I_alias"); 
	  registerAttribute("panel_level_J_alias" , "panel_level_J_alias"); 
	 
	 //CR 812 - Rel 8.3 START
	  /*IR T36000019042 */
	  registerAttribute("air_panel_group_oid", "awb_panel_group_oid","NumberAttribute");
	  registerAttribute("exp_dlc_panel_group_oid", "exp_dlc_panel_group_oid","NumberAttribute");
	  registerAttribute("exp_col_panel_group_oid", "exp_col_panel_group_oid","NumberAttribute");
	  registerAttribute("exp_oco_panel_group_oid", "exp_oco_panel_group_oid","NumberAttribute");
	  /*IR T36000018638 Prateep fix. guar_panel_group_oid to gua_panel_group_oid*/
	  registerAttribute("gua_panel_group_oid", "guar_panel_group_oid","NumberAttribute");
	  registerAttribute("imp_dlc_panel_group_oid", "imp_dlc_panel_group_oid","NumberAttribute");
	  registerAttribute("slc_panel_group_oid", "slc_panel_group_oid","NumberAttribute");
	  registerAttribute("shp_panel_group_oid", "shp_panel_group_oid","NumberAttribute");
	  registerAttribute("lrq_panel_group_oid", "lrq_panel_group_oid","NumberAttribute");
	  registerAttribute("rqa_panel_group_oid", "rqa_panel_group_oid","NumberAttribute");
	  registerAttribute("supplier_panel_group_oid", "supplier_panel_group_oid","NumberAttribute");
	  registerAttribute("atp_panel_group_oid", "atp_panel_group_oid","NumberAttribute");
	  registerAttribute("dcr_panel_group_oid", "dcr_panel_group_oid","NumberAttribute");
	  registerAttribute("mtch_apprv_panel_group_oid", "mtch_apprv_panel_group_oid","NumberAttribute");
	  registerAttribute("inv_auth_panel_group_oid", "inv_auth_panel_group_oid","NumberAttribute");
	  registerAttribute("credit_auth_panel_group_oid", "credit_auth_panel_group_oid","NumberAttribute");
	  /*IR T36000019042 */
	  registerAttribute("air_checker_auth_ind", "awb_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("atp_checker_auth_ind", "atp_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("exp_col_checker_auth_ind", "exp_col_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("exp_dlc_checker_auth_ind", "exp_dlc_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("exp_oco_checker_auth_ind", "exp_oco_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("imp_dlc_checker_auth_ind", "imp_dlc_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("lrq_checker_auth_ind", "lrq_checker_auth_ind","IndicatorAttribute");
	  /*IR T36000018638 Prateep fix. guar_checker_auth_ind to gua_checker_auth_ind*/
	  registerAttribute("gua_checker_auth_ind", "guar_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("slc_checker_auth_ind", "slc_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("shp_checker_auth_ind", "shp_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("rqa_checker_auth_ind", "rqa_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("dcr_checker_auth_ind", "dcr_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("ftrq_checker_auth_ind", "ftrq_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("ftdp_checker_auth_ind", "ftdp_checker_auth_ind","IndicatorAttribute");
	  registerAttribute("ftba_checker_auth_ind", "ftba_checker_auth_ind","IndicatorAttribute");
	  
	  // CR 812 - Rel 8.3 END
	  
	  //Rel8.3 CR-776
	  registerAttribute("c_SPEmailNotifications", "c_ORG_SPEMAIL_OID", "NumberAttribute");
	  registerAttribute("myorg_profile_validation", "myorg_profile_validation", "LocalAttribute");
	  //CR 857
	  registerAttribute("pmt_bene_panel_auth_ind", "pmt_bene_panel_auth_ind","IndicatorAttribute");
	  
	  // DK CR-886 10/15/2013 Rel 8.4 starts
	  registerAttribute("pmt_bene_sort_default", "pmt_bene_sort_default", "PMT_BENE_SORT");
     // DK CR-886 10/15/2013 Rel 8.4 ends

	 // Srinivasu_D CR-599 Rel8.4 11/20/2013 start
	  registerAttribute("man_upld_pmt_file_frmt_ind", "man_upld_pmt_file_frmt_ind");
	  registerAttribute("man_upld_pmt_file_dir_ind", "man_upld_pmt_file_dir_ind","IndicatorAttribute");
	  registerAttribute("restricted_pay_mgmt_upload_dir", "restricted_pay_mgmt_upload_dir");
	  // Srinivasu_D CR-599 Rel8.4 11/20/2013 end
	  
 	//MEerupula Rel8.4 CR-934a Add mobile banking indicator to enable web services for Corporate Organizations(via mobile device)
     	 registerAttribute("mobile_banking_access_ind", "mobile_banking_access_ind", "IndicatorAttribute");
	 
	  //Rel9.0 CR 913 START
	  registerAttribute("allow_payables", "allow_payables", "IndicatorAttribute");
	  registerAttribute("allow_payables_upl", "allow_payables_upl", "IndicatorAttribute");
	  registerAttribute("dual_auth_payables_inv", "dual_auth_payables_inv");
	  registerAttribute("dual_auth_upload_pay_inv", "dual_auth_upload_pay_inv");
	  registerAttribute("pay_inv_panel_group_oid", "pay_inv_panel_group_oid", "NumberAttribute");
	  registerAttribute("upl_pay_inv_panel_group_oid", "upl_pay_inv_panel_group_oid", "NumberAttribute");
	  registerAttribute("invoice_file_upload_pay_ind", "invoice_file_upload_pay_ind", "IndicatorAttribute");
	  registerAttribute("invoice_file_upload_rec_ind", "invoice_file_upload_rec_ind", "IndicatorAttribute");
	  registerAttribute("pay_invoice_grouping_ind", "pay_invoice_grouping_ind", "IndicatorAttribute");
	  registerAttribute("pay_invoice_utilised_ind", "pay_invoice_utilised_ind", "IndicatorAttribute");
	  registerAttribute("rec_invoice_utilised_ind", "rec_invoice_utilised_ind", "IndicatorAttribute");
	  
	  //Rel9.0 CR 913 END
	  registerAttribute("pre_debit_funding_option_ind", "pre_debit_funding_option_ind", "IndicatorAttribute");
	  
	  registerAttribute("allow_pay_credit_note_upload", "allow_pay_credit_note_upload","IndicatorAttribute");
	  registerAttribute("allow_pay_credit_note", "allow_pay_credit_note","IndicatorAttribute");
 	  registerAttribute("allow_end_to_end_id_process", "allow_end_to_end_id_process","IndicatorAttribute");
	  registerAttribute("allow_manual_cr_note_apply", "allow_manual_cr_note_apply","IndicatorAttribute");
 
	  registerAttribute("apply_pay_credit_note", "apply_pay_credit_note","CREDIT_NOTE_APPLY_TYPE");
	  registerAttribute("pay_credit_sequential_type", "pay_credit_sequential_type","CREDIT_NOTE_SEQUENTIAL_TYPE");
	  registerAttribute("dual_auth_pay_credit_note", "dual_auth_pay_credit_note","DUAL_AUTH_REQ");
	  
	  registerAttribute("pay_credit_note_available_days", "pay_credit_note_available_days","NumberAttribute");
	  registerAttribute("pay_cr_note_auth_panel_grp_oid", "pay_cr_note_auth_panel_grp_oid","NumberAttribute");
	  


	  //Rel9.2 CR 988 Begin
	  /* allow_partial_pay_auth - Indicates whether to automatically authorize H2H partial payments. */
	   registerAttribute("allow_partial_pay_auth", "allow_partial_pay_auth", "IndicatorAttribute");
     	 //Rel9.2 CR 988 End   
	   //Srinivasu_D CR#1006 Rel9.3 04/27/2015 - InvoiceCRNoteEmailNotification
	   registerAttribute("c_InvoiceCRNoteEmailNotification", "c_org_inv_crn_oid", "NumberAttribute");
	   
	   //Srinivasu_D CR#1006 Rel9.3 04/27/2015 - End
	   //#RKAZI CR1006 04-24-2015 - BEGIN
	   registerAttribute("allow_rec_dec_inv", "allow_rec_dec_inv", "IndicatorAttribute");
       registerAttribute("allow_pay_dec_inv", "allow_pay_dec_inv", "IndicatorAttribute");
       registerAttribute("allow_pay_dec_crn", "allow_pay_dec_crn", "IndicatorAttribute");
       registerAttribute("dual_auth_rec_dec_inv", "dual_auth_rec_dec_inv", "DUAL_AUTH_REQ");
       registerAttribute("dual_auth_pay_dec_inv", "dual_auth_pay_dec_inv", "DUAL_AUTH_REQ");
       registerAttribute("dual_auth_pay_dec_crn", "dual_auth_pay_dec_crn", "DUAL_AUTH_REQ");
       registerAttribute("allow_h2h_pay_inv_approval", "allow_h2h_pay_inv_approval", "IndicatorAttribute");
       registerAttribute("allow_h2h_pay_crn_approval", "allow_h2h_pay_crn_approval", "IndicatorAttribute");
       registerAttribute("allow_h2h_rec_inv_approval", "allow_h2h_rec_inv_approval", "IndicatorAttribute");
	   //#RKAZI CR1006 04-24-2015 - END
      //SSikhakolli Rel 9.3.5 CR-1029 Adding new attribute cust_is_not_intg_tps
       registerAttribute("cust_is_not_intg_tps", "cust_is_not_intg_tps", "IndicatorAttribute");
       
     //SSikhakolli - Rel-9.4 CR-818 - adding new attributes
       registerAttribute("include_settle_instruction" , "include_settle_instruction", "IndicatorAttribute"); 
	   registerAttribute("allow_settle_instruction" , "allow_settle_instruction", "IndicatorAttribute");
	   registerAttribute("sit_checker_auth_ind" , "sit_checker_auth_ind", "IndicatorAttribute");
	   registerAttribute("dual_settle_instruction", "dual_settle_instruction");
	   registerAttribute("sit_panel_group_oid" , "sit_panel_group_oid", "NumberAttribute");
	   registerAttribute("allow_imp_col" , "allow_imp_col", "IndicatorAttribute");
	   registerAttribute("imp_col_checker_auth_ind" , "imp_col_checker_auth_ind", "IndicatorAttribute");
	   registerAttribute("dual_imp_col", "dual_imp_col", "DUAL_AUTH_REQ");
	   registerAttribute("imp_col_panel_group_oid" , "imp_col_panel_group_oid", "NumberAttribute");
	//SURREWSH   - Rel-9.4 CR-932 -adding new attributes
	   registerAttribute("enable_billing_instruments" , "enable_billing_instruments", "IndicatorAttribute");
	   
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - Begin
	   //added attributes for Portal only Bank Additional address user defined fields
	    registerAttribute("user_defined_field_1", "user_defined_field_1");
	    registerAttribute("user_defined_field_2", "user_defined_field_2");
	    registerAttribute("user_defined_field_3", "user_defined_field_3");
	    registerAttribute("user_defined_field_4", "user_defined_field_4");
	   //Nar Rel9.5.0.0 CR-1132 01/27/2016 - End
	      
}
}