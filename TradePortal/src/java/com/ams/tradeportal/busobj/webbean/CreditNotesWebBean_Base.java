package com.ams.tradeportal.busobj.webbean;


/**
  Credit Notes data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CreditNotesWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  
	         /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      
		      /* upload_credit_note_oid - Unique identifier */
		      registerAttribute("upload_credit_note_oid", "upload_credit_note_oid", "ObjectIDAttribute");
		      
		      /* invoice id - Unique Id */
		      registerAttribute("invoice_id", "invoice_id");
		      
		      /* issue_date  */
		      registerAttribute("issue_date", "issue_date","DateAttribute");
		      /* due_date */
		      registerAttribute("due_date", "due_date","DateAttribute");
		      /*  This is the Buyer name as established in the Trading Partner rule.  */
		      registerAttribute("buyer_name", "buyer_name");
		      /*  This is the Buyer id as established in the Trading Partner rule.  */
		      registerAttribute("buyer_id", "buyer_id");
		      /* currency used in uploading file */
		      registerAttribute("currency", "currency");
		      /* amount in invoice fiel uploaded */
		      registerAttribute("amount", "amount","TradePortalDecimalAttribute");
		      /* invoice_status */
		      registerAttribute("credit_note_status", "credit_note_status");     
		      /* linked_instrument_type- field representing the code value for the instrument to which the invoices should ultimately be associated:
		       *  REC (Receivables), PAY (Payables), ATP (Approval to Pay), LNR (Loan request) */
		      registerAttribute("linked_to_instrument_type", "linked_to_instrument_type");
		      
		      
		      /* payment_date - valid date format can be selected from the dropdown list. */
		      registerAttribute("payment_date", "payment_date","DateAttribute");
		    
		      /* invoice finance amount */
		     // registerAttribute("invoice_finance_amount", "invoice_finance_amount","TradePortalDecimalAttribute");
		      /* net invoice amount */
		     // registerAttribute("net_invoice_finance_amount", "net_invoice_finance_amount","TradePortalDecimalAttribute");	
		      /* estimated interst amount */
		     // registerAttribute("estimated_interest_amount", "estimated_interest_amount","TradePortalDecimalAttribute");	
		      /* amended invoice number */
		    //  registerAttribute("amended_invoice_number", "amended_invoice_number","NumberAttribute");	
		    
		      /* opt_lock - Optimistic lock attribute
		         See jPylon documentation for details on how this works */
		      registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
			  /*  This is the Invoice Type = INT for now  */
		      registerAttribute("invoice_type", "invoice_type");
		      
		     // registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");
		      registerAttribute("corp_org_oid", "a_corp_org_oid", "CorporateOrganization");

               /* first_authorize_status_date - Timestamp (in GMT) of when the first authorization took place on this PayRemit.
		         Depending on the corporate customer's settings, this may be the only authorization
		         that takes places on this Invoice. */
		      registerAttribute("first_authorize_status_date", "first_authorize_status_date", "DateTimeAttribute");
		      
		      /* second_authorize_status_date - Timestamp (in GMT) of when the second authorization took place on this transaction.
		         Depending on the corporate customer's settings, only one authorization may
		         be necessary. */
		      registerAttribute("second_authorize_status_date", "second_authorize_status_date", "DateTimeAttribute");
		      
		      /* first_authorizing_user_oid - The user that performed the first authorization on a PayRemit.  Depending
		         on the corporate customer settings, this may be the only user to perform
		         an authorize on this Invoice. */
		      registerAttribute("first_authorizing_user_oid", "a_first_authorizing_user_oid", "NumberAttribute");
		      
		      /* second_authorizing_user_oid - The user that performed the second authorization on a PayRemit.  Depending
		         on the corporate customer settings, this may be no second user to authorize
		         this Invoice. */
		      registerAttribute("second_authorizing_user_oid", "a_second_authorizing_user_oid", "NumberAttribute");
		      
		      /* first_authorizing_work_group_oid - The work group of the first user that performed the authorization on a PayRemit.
		         Depending on the corporate customer settings, this user may be the only
		         user to perform an authorize on this Invoice. */
		      registerAttribute("first_authorizing_work_group_oid", "a_first_auth_work_group_oid", "NumberAttribute");
		      
		      /* second_authorizing_work_group_oid - The work group of the second user that performed the authorization on a
		         PayRemit.  Depending on the corporate customer settings, this may be no
		         second user to authorize this Invoice. */
		      registerAttribute("second_authorizing_work_group_oid", "a_second_auth_work_group_oid", "NumberAttribute");

		      /* invoice_group_oid - The Invoice Group that this invoice currently belongs to */
		      registerAttribute("invoice_group_oid", "a_invoice_group_oid", "NumberAttribute");

		      registerAttribute("invoice_file_upload_oid", "p_invoice_file_upload_oid", "ParentIDAttribute");

		      registerAttribute("seller_name", "seller_name");
		      /*  This is the Seller id as established in the Trading Partner rule.  */
		      registerAttribute("seller_id", "seller_id");
		       /* represents either payables/receivables invoice*/
		       registerAttribute("invoice_classification","invoice_classification");

		       /* creation_date_time - Timestamp when Invoice was created. */
		       registerAttribute("creation_date_time", "creation_date_time", "DateTimeAttribute");

		       /* invoice definition oid  */
		       registerAttribute("upload_definition_oid", "a_upload_definition_oid", "NumberAttribute");
			   /* terms oid  */
		      // registerAttribute("terms_oid", "a_terms_oid");
		       /* instrument oid -  */
		      // registerAttribute("instrument_oid", "a_instrument_oid", "NumberAttribute");

		       /* deleted_ind - Indicates whether the Invoice is deleted. */
		       registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");
		       /* status - Invoice status based on the actions */
		       //registerAttribute("status", "status");
		       /* transaction_oid -  */
		      // registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");

		       /* amend_seq_no - Tracks number of amend done. */
		       //registerAttribute("amend_seq_no", "amend_seq_no", "NumberAttribute");
		       registerAttribute("tp_rule_name", "tp_rule_name", "LocalAttribute");
/*		       cred_discount_code 
		       registerAttribute("cred_discount_code", "cred_discount_code");
		        cred_gl_code -  
		       registerAttribute("cred_gl_code", "cred_gl_code"); 
		       cred_comments - 
		       registerAttribute("cred_comments", "cred_comments");
		       
		       registerAttribute("credit_discount_code_id", "cred_discount_code");
		        cred_gl_code -  
		       registerAttribute("credit_discount_gl_code_id", "cred_gl_code"); 
		       cred_comments - 
		       registerAttribute("credit_discount_comments_id", "cred_comments");			       
		       registerAttribute("loan_type", "loan_type");*/
		      registerAttribute("credit_note", "credit_note");
		      registerAttribute("pay_method", "pay_method");
		      registerAttribute("ben_acct_num", "ben_acct_num");
		      registerAttribute("ben_add1", "ben_address_one");
		      registerAttribute("ben_add2", "ben_address_two");
		      registerAttribute("ben_add3", "ben_address_three");
		      registerAttribute("ben_country", "ben_country");
		      registerAttribute("ben_bank_name", "ben_bank_name");
		      registerAttribute("ben_branch_code", "ben_branch_code");
		      registerAttribute("ben_branch_add1", "ben_branch_address1");
		      registerAttribute("ben_branch_add2", "ben_branch_address2");
		      registerAttribute("ben_bank_city", "ben_bank_city");
		      registerAttribute("ben_bank_province", "ben_bank_prvnce");
		      registerAttribute("ben_branch_country", "ben_branch_country");
		      registerAttribute("charges", "payment_charges");
		      registerAttribute("central_bank_rep1", "central_bank_rep1");
		      registerAttribute("central_bank_rep2", "central_bank_rep2");
		      registerAttribute("central_bank_rep3", "central_bank_rep3");	      
		      /* panel_auth_group_oid - Panel group associated to an Invoice. */
		      registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");
		        
		        /* panel_auth_range_oid - Panel Range to a CreditNote. */
		      registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
		       
		        /* opt_lock - Optimistic lock attribute*/
		      registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");
		       
		 	  registerAttribute("payment_amount", "payment_amount", "TradePortalDecimalAttribute");
			  registerAttribute("send_to_supplier_date", "send_to_supplier_date", "DateAttribute");  
			  registerAttribute("buyer_acct_num", "buyer_acct_num");
		      registerAttribute("buyer_acct_currency", "buyer_acct_currency");
		      registerAttribute("ben_email_addr", "ben_email_addr");
		      registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");
			  registerAttribute("end_to_end_id", "end_to_end_id");
		      registerAttribute("credit_note_utilised_status", "credit_note_utilised_status");
		      registerAttribute("credit_note_applied_status", "credit_note_applied_status");
		      registerAttribute("expiry_date", "expiry_date");
		      registerAttribute("credit_note_applied_amount", "credit_note_applied_amount","TradePortalDecimalAttribute");
		      registerAttribute("end_to_end_group_num","end_to_end_group_num");
		      registerAttribute("credit_invoice_group_num","credit_invoice_group_num");
		      registerAttribute("cannot_group_ind", "cannot_group_ind", "IndicatorAttribute");
		      registerAttribute("utilized_amount", "utilized_amount","TradePortalDecimalAttribute");
		      registerAttribute("otl_invoice_uoid", "otl_invoice_uoid");
		      
		      /* buyer_party_identifier -  */
		      registerAttribute("buyer_party_identifier", "buyer_party_identifier");	      
		      /* buyer_address_line_1 -  */
		      registerAttribute("buyer_address_line_1", "buyer_address_line_1");	      
		      /* buyer_address_line_2 -  */
		      registerAttribute("buyer_address_line_2", "buyer_address_line_2");	          
		      /* seller_party_identifier -  */
		      registerAttribute("seller_party_identifier", "seller_party_identifier");      
		      /* seller_address_line_1 -  */
		      registerAttribute("seller_address_line_1", "seller_address_line_1");      
		      /* seller_address_line_2 -  */
		      registerAttribute("seller_address_line_2", "seller_address_line_2");   
		      
	          registerAttribute("portal_originated_ind", "portal_originated_ind", "IndicatorAttribute");
	          
	          //Srinivasu_D CR#1006 Rel9.3 05/04/2015 - Added attributes to INVOICES_SUMMARY_DATA table
		      registerAttribute("pcn_notification_email_sent_ind", "pcn_notiy_email_sent_ind", "IndicatorAttribute");
		      registerAttribute("portal_approval_requested_ind", "portal_approval_req_ind", "IndicatorAttribute");	      
		      registerAttribute("portal_approval_requested_work_item", "portal_approval_req_work_item");
		      registerAttribute("customer_file_ref", "customer_file_ref");
		      registerAttribute("file_name", "file_name");
		      //Srinivasu_D CR#1006 Rel9.3 05/04/2015 - End
		      registerAttribute("remove_crnote_oid", "remove_crnote_oid", "LocalAttribute");
		      registerAttribute("reqPanleAuth", "reqPanleAuth", "LocalAttribute");
	          
		   }
		   		   
	}

