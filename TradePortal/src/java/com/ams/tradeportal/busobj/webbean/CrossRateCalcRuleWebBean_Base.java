

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Business object to store Cross Rate Calculation rules.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CrossRateCalcRuleWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* cross_rate_calc_rule_oid - Unique identifier */
       registerAttribute("cross_rate_calc_rule_oid", "cross_rate_calc_rule_oid", "ObjectIDAttribute");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }
   
   




}
