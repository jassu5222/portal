

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Business Object to store aliases (alternate names/identifiers) to be used
 * as indexes on the Customer File depending on the specific Message Category
 * for the Incoming Generic Message
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerAliasWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* alias - Alias(alternate names/identifiers) to be used as indexes on the Customer
         File depending on the specific Message Category for the Incoming Generic
         Message */
       registerAttribute("alias", "alias");


       /* customer_alias_oid - Unique Identifier */
       registerAttribute("customer_alias_oid", "customer_alias_oid", "ObjectIDAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");

       /* gen_message_category_oid -  */
       registerAttribute("gen_message_category_oid", "a_gen_message_category_oid", "NumberAttribute");
   }
   
   




}
