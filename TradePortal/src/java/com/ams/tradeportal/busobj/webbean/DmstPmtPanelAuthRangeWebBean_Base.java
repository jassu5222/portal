



package com.ams.tradeportal.busobj.webbean;


/**
 * A DmstPmtPanelAuthRange consists of a set of panel range for beneficiary
 * of payment transaction.
 * 
 *     Copyright  @ 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DmstPmtPanelAuthRangeWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


      /* dmst_pmt_panel_auth_range_oid - Unique identifier */
      registerAttribute("dmst_pmt_panel_auth_range_oid", "dmst_pmt_panel_auth_range_oid", "ObjectIDAttribute");

      /* panel_auth_range_oid - panel range associated to beneficiary. */
      registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
      
      /* auth_status - Beneficiary status */
      registerAttribute("auth_status", "auth_status");

      /* Pointer to the parent Transaction */
      registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");

   }

}
