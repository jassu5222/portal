package com.ams.tradeportal.busobj.webbean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

import java.math.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;

/*
 *
 *     Copyright  2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DomesticPaymentWebBean extends DomesticPaymentWebBean_Base
{
private static final Logger LOG = LoggerFactory.getLogger(DomesticPaymentWebBean.class);

    public DomesticPaymentWebBean sourceDomesticPayment = null;	//IAZ CR-586 IR-VRUK091652765 10/05/10 Add

	//Vshah CR507 Begin
	public void loadPaymentPartyFromDoc(DocumentHandler doc, String partyType)
	{
		if(doc == null)
		{
			// If the document is null, there is nothing to populate
			return;
		}

		String value;
		String city, province, postalCode ;

		if (TermsPartyType.PAYEE.equals(partyType) || TermsPartyType.PAYER.equals(partyType)) {
			value = doc.getAttribute("/party_oid");
			if (value != null && !value.equals("")) {
				this.setAttribute("beneficiary_party_oid", value);
			}
		}

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/name");
		if (value == null) value = "";
		this.setAttribute("payee_name", value);


		value = doc.getAttribute("/address_line_1");
		if (value == null) value = "";
		this.setAttribute("payee_address_line_1", value);


		value = doc.getAttribute("/address_line_2");
		if (value == null) value = "";
		this.setAttribute("payee_address_line_2", value);


		city = doc.getAttribute("/address_city");
		province = doc.getAttribute("/address_state_province");
		postalCode = doc.getAttribute("/address_postal_code");
		if (city == null) city = "";
		if (province == null) province = "";
		if (postalCode == null) postalCode = "";

		value = city + " " + province + " " + postalCode;
		this.setAttribute("payee_address_line_3",value);


		value = doc.getAttribute("/address_country");
		if (value == null) value = "";
		this.setAttribute("country", value);


		value = doc.getAttribute("/contact_fax");
		if (value == null) value = "";
		this.setAttribute("payee_fax_number", value);


		value = doc.getAttribute("/contact_email");
		if (value == null) value = "";
		this.setAttribute("payee_email", value);



	}

	public void loadPaymentPartyBankFromDoc(DocumentHandler doc)
	{
		if(doc == null)
		{
			// If the document is null, there is nothing to populate
			return;
		}

		String value, city, province;

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/bank_name");
		if (value == null) value = "";
		//NSX CR-573 07/08/10  Begin
		else  {
			if (value.length() > 35)
				//get first 35 characters
				value = value.substring(0, 35);
		}
		//NSX CR-573 07/08/10  End
		this.setAttribute("payee_bank_name", value);


		value = doc.getAttribute("/branch_name");
		if (value == null) value = "";
		//NSX CR-573 07/08/10  Begin
		else {
			if (value.length() > 35)
				//get first 35 characters
				value = value.substring(0, 35);
		}
		//NSX CR-573 07/08/10  End

		this.setAttribute("payee_branch_name", value);


		value = doc.getAttribute("/bank_branch_code");
		if (value == null) value = "";
		this.setAttribute("payee_bank_code", value);


		value = doc.getAttribute("/address_line_1");
		if (value == null) value = "";
		this.setAttribute("address_line_1", value);


		value = doc.getAttribute("/address_line_2");
		if (value == null) value = "";
		this.setAttribute("address_line_2",value);


		city = doc.getAttribute("/address_city");
		province = doc.getAttribute("/address_state_province");

		if (province == null) province = "";

		if (city == null) city = "";
		//NSX CR-573 07/08/10  Begin
		else {
			int provinceLen = province.length();
			if (StringFunction.isNotBlank(province)) {
				provinceLen++;   // increase length for space delimiter
			}

			int len = 35 - provinceLen;
			if (city.length() > len)
			    city = city.substring(0, len);
		}

		value = city + " " + province;
		value = value.trim();

		//NSX CR-573 07/08/10  End

		this.setAttribute("address_line_3",value);

		value = doc.getAttribute("/address_country");
		if (value == null) value = "";
		this.setAttribute("payee_bank_country",value);

	}
	//Vshah CR507 End

	public void clearPaymentPartyBank() {

		this.setAttribute("payee_bank_name", "");
		this.setAttribute("payee_branch_name", "");
		this.setAttribute("payee_bank_code", "");
		this.setAttribute("address_line_1", "");
		this.setAttribute("address_line_2", "");
		this.setAttribute("address_line_3", "");
		//Vasavi CR573 Begin
		this.setAttribute("payee_bank_country", "");
		//Vasavi CR573 End
	}

	//IAZ CR-586 IR-VRUK091652765 10/05/10 Begin

	public boolean isFixedValue(String propertyToCheck, boolean isTemplate) {
		String propertyValue = null;
		String propertyValueFromSourceDP = null;

		if (isTemplate)
			return false;
		LOG.debug("source Template in isFixedValue {} [] {}" , sourceDomesticPayment, this.getAttribute(propertyToCheck));
		if (StringFunction.isBlank(this.getAttribute(propertyToCheck)))
			return false;
		else if (sourceDomesticPayment == null)
			return false;
		// Nar Rel 9.2 CR 966 09/15/2014 Start
		else if ("amount".equals(propertyToCheck) && 
				TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pay_amt_modification")))
			return false;
		else if ("payee_invoice_details".equals(propertyToCheck)
				&& TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_inv_detail_modification")))
			return false;
		else if ("payee_description".equals(propertyToCheck)
				&& TradePortalConstants.INDICATOR_YES.equals(this.getAttribute("allow_pay_detail_modification")))
			return false;
		// Nar Rel 9.2 CR 966 09/15/2014 End
		else if (StringFunction.isBlank(sourceDomesticPayment.getAttribute(propertyToCheck)))
			return false;
		else
			// Vshah - 12/16/2010 - IR#MHUK121363451 - [BEGIN]
			propertyValue = this.getAttribute(propertyToCheck);
		if (propertyValue.contains(","))
			propertyValue = propertyValue.replace(",", "");
		BigDecimal dpAmount = null;

		try {
			dpAmount = new BigDecimal(propertyValue);

			BigDecimal souceDPAmount = null;
			BigDecimal zeroAmount = BigDecimal.ZERO;

			propertyValueFromSourceDP = sourceDomesticPayment
					.getAttribute(propertyToCheck);
			if (propertyValueFromSourceDP.contains(","))
				propertyValueFromSourceDP = propertyValueFromSourceDP.replace(
						",", "");

			souceDPAmount = new BigDecimal(propertyValueFromSourceDP);

			if ((dpAmount.compareTo(zeroAmount) == 0)
					&& (souceDPAmount.compareTo(zeroAmount) == 0))
				return false;
			else if ((dpAmount.compareTo(zeroAmount) != 0)
					&& (souceDPAmount.compareTo(zeroAmount) == 0))
				return false;
			else
				return true;
		} catch (java.lang.NumberFormatException exc) {
			return true;
		}
		// Vshah - 12/16/2010 - IR#MHUK121363451 - [END]

	}
	//IAZ CR-586 IR-VRUK091652765 10/05/10 End

}
