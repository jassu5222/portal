



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 *
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DomesticPaymentWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* domestic_payment_oid - Unique identifier */
       registerAttribute("domestic_payment_oid", "domestic_payment_oid", "ObjectIDAttribute");


       /* amount - The amount of the domestic payment. This is validated to have the correct
         number of decimal places based on the currency selected.   For amendments,
         this attribute is allowed to have a negative value.  For all other transactions,
         this attribute must be validated that it is a positive number. */
       registerAttribute("amount", "amount", "TradePortalSignedDecimalAttribute");


       /* amount_currency_code - The currency for the instrument. */
       registerAttribute("amount_currency_code", "amount_currency_code");


       /* payee_name - Name of the payee involved in the Domestic Payment. */
       registerAttribute("payee_name", "payee_name");


       /* payee_account_number - The bank account number for the payee */
       registerAttribute("payee_account_number", "payee_account_number");


       /* address_line_1 - First line of the address for this payee's bank. */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Second line of the address for this payee's bank. */
       registerAttribute("address_line_2", "address_line_2");


       /* address_line_3 - Third line of the address for this payee's bank.

 */
       registerAttribute("address_line_3", "address_line_3");


       /* bank_charges_type - Indicates the distribution of bank charges for the instrument. */
       registerAttribute("bank_charges_type", "bank_charges_type");


       /* payee_bank_code - Beneficiary Bank Code and Branch Code combined.

         * this field will only be required for Electronic Payments ( where Payment
         method = ACH/GIRO, RTGS, BKT)
 */
       registerAttribute("payee_bank_code", "payee_bank_code");


       /* payee_description - This field allows for a text description to be entered as an explanation
         of the payment for the recipient. */
       registerAttribute("payee_description", "payee_description");


       /* payee_bank_name - The name of the Payee's Bank. */
       registerAttribute("payee_bank_name", "payee_bank_name");


       /* payee_address_line_1 - First line of the Beneficiary's address */
       registerAttribute("payee_address_line_1", "payee_address_line_1");


       /* payee_address_line_2 - Second line of the beneficiary's address. */
       registerAttribute("payee_address_line_2", "payee_address_line_2");


       /* payee_address_line_3 - Third line of the beneficiary's address. */
       registerAttribute("payee_address_line_3", "payee_address_line_3");


       /* payment_method_type - Payment Method Type. */
       registerAttribute("payment_method_type", "payment_method_type");


       /* value_date - The value date is equal to the Execution Date + the # of Offset days that
         are valid for the Payment method and country of the debit account selected. */
       registerAttribute("value_date", "value_date", "DateAttribute");


       /* country - Beneficiary's country */
       registerAttribute("country", "country");


       /* payee_address_line_4 - Additional line used for capturing the beneficiary address. This will be
         used primarily for check payments */
       registerAttribute("payee_address_line_4", "payee_address_line_4");


       /* payee_fax_number - Beneficiary's Fax Number */
       registerAttribute("payee_fax_number", "payee_fax_number");


       /* payee_email - Beneficiary's email */
       registerAttribute("payee_email", "payee_email");


       /* payee_instruction_number - Instruction Number to be printed on the cheques- for other payment types
         it will be printed on beneficiary advice */
       registerAttribute("payee_instruction_number", "payee_instruction_number");


       /* payee_branch_code -  */
       registerAttribute("payee_branch_code", "payee_branch_code");


       /* delivery_method - Identify the method to be used when a customer wants a cheques to be delivered
         once printed and to whom it will be delivered. */
       registerAttribute("delivery_method", "delivery_method");


       /* payable_location - Cheque's payable location. */
       registerAttribute("payable_location", "payable_location");


       /* print_location - Cheque's print location. */
       registerAttribute("print_location", "print_location");


       /* mailing_address_line_1 - First line of the mailing address used for sending cheques. */
       registerAttribute("mailing_address_line_1", "mailing_address_line_1");


       /* mailing_address_line_2 - Second line of the mailing address used for sending cheques. */
       registerAttribute("mailing_address_line_2", "mailing_address_line_2");


       /* mailing_address_line_3 - Third line of the mailing address used for sending cheques. */
       registerAttribute("mailing_address_line_3", "mailing_address_line_3");


       /* mailing_address_line_4 - Fourth line of the mailing address used for sending cheques. */
       registerAttribute("mailing_address_line_4", "mailing_address_line_4");


       /* payee_branch_name -  */
       registerAttribute("payee_branch_name", "payee_branch_name");


       /* customer_reference -  */
       registerAttribute("customer_reference", "customer_reference");


       /* other_charges_instructions -  */
       registerAttribute("other_charges_instructions", "other_charges_instructions");


       /* central_bank_reporting_1 -  */
       registerAttribute("central_bank_reporting_1", "central_bank_reporting_1");


       /* central_bank_reporting_2 -  */
       registerAttribute("central_bank_reporting_2", "central_bank_reporting_2");


       /* central_bank_reporting_3 -  */
       registerAttribute("central_bank_reporting_3", "central_bank_reporting_3");

       //BSL CR-655 02/22/11 Begin
       /* reporting_code_1 -  */
       registerAttribute("reporting_code_1", "reporting_code_1");
       /* reporting_code_2 -  */
       registerAttribute("reporting_code_2", "reporting_code_2");
       //BSL CR-655 02/22/11 End
	   
       /* payee_bank_country - Beneficiary Bank's country. */
       registerAttribute("payee_bank_country", "payee_bank_country");


       /* source_template_dp_oid - oid of the template dp from where it was originally copied */
       registerAttribute("source_template_dp_oid", "source_template_dp_oid", "NumberAttribute");


       /* sequence_number - Sequence Number of this DomesticPayment within the transaction. */
       registerAttribute("sequence_number", "sequence_number", "NumberAttribute");


       /* payment_status - Beneficiary Payment Status.   */
       registerAttribute("payment_status", "payment_status");


       /* payment_system_ref - The beneficiary level reference returned from the payment/clearing system
         which can be used to reference the individual payment in correspondence
         with the Payment system. */
       registerAttribute("payment_system_ref", "payment_system_ref");


       /* error_text - Error text can be returned from the Payment system or can be manually entered
         in the TPS to be sent to the Portal. The Error Text, if present, should
         indicate why a specific beneficiary payment has been rejected. */
       registerAttribute("error_text", "error_text");


       /* payment_ben_email_sent_flag - Whether the email has been sent to the beneficiary. */
       registerAttribute("payment_ben_email_sent_flag", "payment_ben_email_sent_flag", "IndicatorAttribute");

      /* Pointer to the parent Transaction */
       registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");

       /* FirstIntermediaryBank -  */
       registerAttribute("c_FirstIntermediaryBank", "c_FIRST_INTERMEDIARY_BANK", "NumberAttribute");

       /* SecondIntermediaryBank -  */
       registerAttribute("c_SecondIntermediaryBank", "c_SECOND_INTERMEDIARY_BANK", "NumberAttribute");

       /* beneficiary_party_oid -  */
       registerAttribute("beneficiary_party_oid", "a_beneficiary_party_oid", "NumberAttribute");

       //BSL IR#PUL032965444 04/04/11 Remove
       /* SHILPA R CR-597 - oid of INVOICE_DETAIL*/
       //registerAttribute("invoice_detail_oid", "invoice_detail_oid", "NumberAttribute");
       
       /* MANOHAR CR-597 - User Defined fields 1-10*/
       registerAttribute("user_defined_field_1", "user_defined_field_1");
       registerAttribute("user_defined_field_2", "user_defined_field_2");
       registerAttribute("user_defined_field_3", "user_defined_field_3");
       registerAttribute("user_defined_field_4", "user_defined_field_4");
       registerAttribute("user_defined_field_5", "user_defined_field_5");
       registerAttribute("user_defined_field_6", "user_defined_field_6");
       registerAttribute("user_defined_field_7", "user_defined_field_7");
       registerAttribute("user_defined_field_8", "user_defined_field_8");
       registerAttribute("user_defined_field_9", "user_defined_field_9");
       registerAttribute("user_defined_field_10", "user_defined_field_10");
       
       // Narayan Rel 8.3 CR 857 07/15/2013 Start
       /* dmst_pmt_panel_auth_range - panel range associated to beneficiary. */
       registerAttribute("dmst_pmt_panel_auth_range", "a_dmst_pmt_panel_auth_range", "NumberAttribute");
      // Narayan Rel 8.3 CR 857 07/15/2013 End
       
      //CR913 - only for Payables
       registerAttribute("payable_payment_seq_num", "payable_payment_seq_num", "NumberAttribute");
       
       // Narayan Rel 9.2 CR 966 09/15/2014 Start
       /*allow_pay_amt_modification - indicator to check where amount field is editable or not for fixed payment*/
       registerAttribute("allow_pay_amt_modification", "allow_pay_amt_modification", "IndicatorAttribute");
       
       /*allow_pay_detail_modification - indicator to check where amount field is editable or not for fixed payment*/
       registerAttribute("allow_pay_detail_modification",  "allow_pay_detail_modification", "IndicatorAttribute");
       
       /*allow_inv_detail_modification - indicator to check where amount field is editable or not for fixed payment*/
       registerAttribute("allow_inv_detail_modification",  "allow_inv_detail_modification", "IndicatorAttribute");     
       // Narayan Rel 9.2 CR 966 09/15/2014 End
   }






}
