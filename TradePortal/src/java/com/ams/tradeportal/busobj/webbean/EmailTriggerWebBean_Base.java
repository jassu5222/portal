

  

package com.ams.tradeportal.busobj.webbean;

/**
 * Placing a row into this table will cause an e-mail to be sent.  The contents
 * of the e-mail are based on the email_data column.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EmailTriggerWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* email_trigger_oid - Unique identifier */
       registerAttribute("email_trigger_oid", "email_trigger_oid", "ObjectIDAttribute");


       /* agent_id - The ID of the agent that has picked up this row to send as an e-mail. */
       registerAttribute("agent_id", "agent_id");


       /* status - The status of the email trigger. */
       registerAttribute("status", "status");


       /* email_data - XML data that is used to describe the contents of the e-mail. */
       registerAttribute("email_data", "email_data");


       /* creation_timestamp - Timestamp (in GMT) of when this business object instance was created */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* send_attempt_count -  */
       registerAttribute("send_attempt_count", "send_attempt_count", "NumberAttribute");

       //BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
       /* attachment_data - The email attachment data */
       registerAttribute("attachment_data", "attachment_data");
       //BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
   }
   
   




}
