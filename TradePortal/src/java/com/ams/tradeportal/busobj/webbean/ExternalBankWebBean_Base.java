package com.ams.tradeportal.busobj.webbean;

/**
 * External Bank - Added for Kyriba
 *
 */
public class ExternalBankWebBean_Base extends TradePortalBusinessObjectWebBean{
	
	public void registerAttributes()
	   {  
	      super.registerAttributes();

	       
	       /* External Bank OID - Unique Identifier */
		      registerAttribute("external_bank_oid", "EXTERNAL_BANK_OID", "ObjectIDAttribute");

		      /* Pointer to the parent CorporateOrganization */
		       registerAttribute("corp_org_oid", "P_ORGANIZATION_OID", "ParentIDAttribute");
		       
		       /* Operational Bank Organization OID  */
		       registerAttribute("op_bank_org_oid", "A_OP_ORGANIZATION_OID", "NumberAttribute");
	   }

}
