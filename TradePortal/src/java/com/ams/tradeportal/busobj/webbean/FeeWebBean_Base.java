

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * A fee that is associated with a transaction.   This table is populated by
 * the middleware when unpackaging data that has come from OTL.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class FeeWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* fee_oid - Unique identifier */
       registerAttribute("fee_oid", "fee_oid", "ObjectIDAttribute");


       /* settlement_curr_code - Settlement currency of the fee */
       registerAttribute("settlement_curr_code", "settlement_curr_code");


       /* settlement_curr_amount - Settlement amount of the fee */
       registerAttribute("settlement_curr_amount", "settlement_curr_amount", "TradePortalDecimalAttribute");


       /* charge_type - Type of fee that is being charged to the user's corporate organization */
       registerAttribute("charge_type", "charge_type");


       /* base_curr_amount - Base currency amount */
       registerAttribute("base_curr_amount", "base_curr_amount", "TradePortalDecimalAttribute");


       /* acct_number - The account number for this fee. */
       registerAttribute("acct_number", "acct_number");


       /* settlement_how_type - A code that describes how the payment of the fee should be settled. */
       registerAttribute("settlement_how_type", "settlement_how_type");

      /* Pointer to the parent Transaction */
       registerAttribute("transaction_oid", "p_transaction_oid", "ParentIDAttribute");
   }
   
   




}
