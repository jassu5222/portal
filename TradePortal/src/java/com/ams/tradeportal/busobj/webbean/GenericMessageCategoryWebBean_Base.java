

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Business object to store generic message categories that comprise instruments
 * created in the Trade Portal.  
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GenericMessageCategoryWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* description - A description of the Generic Message Category. */
       registerAttribute("description", "description");


       /* short_description - A short description of the Generic Message Category. */
       registerAttribute("short_description", "short_description");

      /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
      registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

      /* ownership_level - Ownership level of the Generic Message Category. */
      registerAttribute("ownership_level", "ownership_level");
      
      /* ownership_type - Ownership type of the Generic Message Category. */
      registerAttribute("ownership_type", "ownership_type");

       /* gen_message_category_oid - Unique identifier */
       registerAttribute("gen_message_category_oid", "gen_message_category_oid", "ObjectIDAttribute");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }
   
   




}
