

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Each client bank has a range of instrument numbers that they are allowed
 * to use.   The beginning and end of the range is stored on the ClientBank
 * business object.  The current point in the sequence is stored in this business
 * object, a one-to-one component of ClientBank.
 * 
 * This data is stored on a separate object instead of ClientBank because it
 * will be frequently updated.   If the sequence were stored on the ClientBank
 * table, it would be difficult for anyone to edit the object because the constant
 * changes of the sequence pointer would cause optimistic lock errors to happen.
 * 
 * Data in this table should always be updated in its own transaction.  Because
 * this data is frequently updated, locking it by placing it into a long transaction
 * could cause contention problems.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InstrNumberSequenceWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* instr_num_seq_oid - Unique identifier */
       registerAttribute("instr_num_seq_oid", "instr_num_seq_oid", "ObjectIDAttribute");


       /* last_instrument_id_used - The last instrument number that was used in the ClientBank's instrument
         number range.  This attribute is incremented each time a new instrument
         is created for any organization under the client bank. */
       registerAttribute("last_instrument_id_used", "last_instrument_id_used", "NumberAttribute");
   }
   
   




}
