package com.ams.tradeportal.busobj.webbean;


/**
 * This object represents sending email notifications to buyers on 
 * new invoices available for financing
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceCRNoteEmailNotificationWebBean extends InvoiceCRNoteEmailNotificationWebBean_Base{

}
