/**
 * 
 */
package com.ams.tradeportal.busobj.webbean;

/**

 *
 */
public class InvoiceCRNoteEmailNotificationWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{
	public void registerAttributes()
	 {  
		 super.registerAttributes();

		 /* inv_crn_email_notify_oid primary key attribute for InvoiceCRNoteEmailNotification */
		 registerAttribute("inv_crn_email_notify_oid", "inv_crn_email_notify_oid", "ObjectIDAttribute");
	      
		  /*  next_pin_email_due- A datetime (in GMT)   when next pin email is due */
	      registerAttribute("next_pin_email_due", "next_pin_email_due", "DateTimeAttribute");
	      
	      /*  next_pcn_email_due- A datetime (in GMT)  when next pcn email is due */
	      registerAttribute("next_pcn_email_due", "next_pcn_email_due", "DateTimeAttribute");
	      
	      /*  next_rin_email_due- A datetime (in GMT)   when rin emai is due */
	      registerAttribute("next_rin_email_due", "next_rin_email_due", "DateTimeAttribute");
	      
	      /* Pointer to the CorporateOrganization */
	      registerAttribute("p_corp_organization_oid", "p_corp_organization_oid", "NumberAttribute");
	      
	      /* Pointer to the parent NotificationRule */
	      registerAttribute("p_notification_rule_oid", "p_notification_rule_oid", "NumberAttribute");
		
		
	 }
	

}
