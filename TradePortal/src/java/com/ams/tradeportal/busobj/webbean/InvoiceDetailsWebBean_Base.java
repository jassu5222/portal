package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Invoice Details.
 * 
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoiceDetailsWebBean_Base extends
		com.amsinc.ecsg.web.AmsEntityWebBean {

	public void registerAttributes() {

		/* domestic_payment_oid - Unique Identifier. */
		//BSL IR#PUL032965444 04/04/11
		//registerAttribute("domestic_payment_oid", "domestic_payment_oid", "ObjectIDAttribute");
		registerAttribute("domestic_payment_oid", "domestic_payment_oid", "NumberAttribute");

		/* domestic_payment_oid - */
		registerAttribute("payee_invoice_details", "payee_invoice_details");
		registerAttribute("invoice_detail_oid", "invoice_detail_oid", "ObjectIDAttribute");

	}

}
