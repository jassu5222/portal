package com.ams.tradeportal.busobj.webbean;

import java.util.Iterator;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.termsmgr.TermsMgr;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * The Invoice Files that are to be uploaded.
 * 
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 */
public class InvoiceFileUploadWebBean extends InvoiceFileUploadWebBean_Base {
	
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadWebBean.class);
	
	public DocumentHandler getResultParams(ResourceManager resMgr) {
		String str = this.getAttribute("po_result_parameters");
		DocumentHandler response = new DocumentHandler(str, false);
		DocumentHandler respDoc = new DocumentHandler();
		if (StringFunction.isNotBlank(str)) {
			Vector v = response.getFragments("/ResultSetRecord");
			Iterator itr = v.iterator();
			int i = 0;
			DocumentHandler recDoc = null;
			DocumentHandler tranRecNode = null;
			while (itr.hasNext()) {
				recDoc = new DocumentHandler();
				tranRecNode = (DocumentHandler) itr.next();

				String insType = getInstrumentType(
						tranRecNode.getAttribute("/INS_OID"), resMgr);
				String tranType = getTransactionType(
						tranRecNode.getAttribute("/TRAN_OID"), resMgr);

				recDoc.setAttribute("/ResultSetRecord/INSTRUID",
						tranRecNode.getAttribute("/INS_NUM"));
				recDoc.setAttribute("/ResultSetRecord/INSTRUTYPE", insType);
				recDoc.setAttribute("/ResultSetRecord/TRAN",tranType);
				if(null!=recDoc.getDocumentNode("/ResultSetRecord")){
					recDoc.getDocumentNode("/ResultSetRecord").setIdName("" + i++);			
					respDoc.addComponent("/", recDoc);
				}	
			}
		}
		return respDoc;
	}

	public String getInstrumentType(String oid, ResourceManager resMgr) {
		DocumentHandler results = null;
		String sqlQuery = null;
		String type = null;

		try {
			sqlQuery = "select instrument_type_code from instrument where instrument_oid = ? ";
			results = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{oid});

			if (results != null) {
				String code = results
						.getAttribute("/ResultSetRecord/INSTRUMENT_TYPE_CODE");
				if (code != null) {
					type = ReferenceDataManager.getRefDataMgr().getDescr(
							"INSTRUMENT_TYPE", code);					
				}
			}
		} catch (Exception e) {
			LOG.error("InvoiceFileUploadWebBean: Exception while fetching instrument type: ",e);
		}
		return type;
	}

	public String getTransactionType(String oid, ResourceManager resMgr) {
		DocumentHandler results = null;
		String sqlQuery = null;
		String type = null;

		try {
			sqlQuery = "select transaction_type_code from transaction where transaction_oid = ? ";
			results = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{oid});

			if (results != null) {
				String code = results
						.getAttribute("/ResultSetRecord/TRANSACTION_TYPE_CODE");
				if (code != null) {
					type = ReferenceDataManager.getRefDataMgr().getDescr(
							"TRANSACTION_TYPE", code);
				}
			}
		} catch (Exception e) {
			LOG.error("InvoiceFileUploadWebBean: Exception while fetching transaction type: ",e);
		}
		return type;
	}
}
