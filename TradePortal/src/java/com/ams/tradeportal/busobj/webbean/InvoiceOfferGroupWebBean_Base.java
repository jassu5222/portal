

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The group of invoices used for Invoice Offer processing.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceOfferGroupWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* invoice_offer_group_oid - Unique Identifier */
       registerAttribute("invoice_offer_group_oid", "invoice_offer_group_oid", "ObjectIDAttribute");


       /* currency - Currency of the invoices in this group. */
       registerAttribute("currency", "currency");


       /* due_payment_date - The least of the due_date and payment_date of the invoices in the group. */
       registerAttribute("due_payment_date", "due_payment_date", "DateAttribute");


       /* future_value_date - The future data when the offer will be accepted.  That is, the use can accept
         the offer in advance. */
       registerAttribute("future_value_date", "future_value_date", "DateAttribute");


       /* name - The name of a manually create group. */
       registerAttribute("name", "name");


       /* supplier_portal_invoice_status - The invoice status.  - BUYER_APPROVED, OFFER_ACCEPTED, OFFER_AUTHORIZED,
         OFFER_DECLINED etc. */
       registerAttribute("supplier_portal_invoice_status", "supplier_portal_invoice_status");


       /* total_number - Total number of invoices in this group. */
       registerAttribute("total_number", "total_number", "TradePortalNumberAttribute");


       /* total_amount - Total amount of the invoices in this group. */
       registerAttribute("total_amount", "total_amount", "TradePortalAmountAttribute");


       /* total_net_amount_offered - Total net amount offer of the invoices in this group. */
       registerAttribute("total_net_amount_offered", "total_net_amount_offered", "TradePortalAmountAttribute");


       /* attachment_ind - Whether any invoices in the group has attachment. */
       registerAttribute("attachment_ind", "attachment_ind", "IndicatorAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");

       /* corp_org_oid - The corp org of the invoices in the group. */
       registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");
       
 	  // Narayan Rel 8.3 CR 821 07/03/2013 Start
       /* panel_auth_group_oid - Panel group associated to a InvoiceOfferGroup. */
        registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");
        
        /* panel_auth_range_oid - Panel Range to a invoice offer group. */
        registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
       
        /* opt_lock - Optimistic lock attribute*/
        registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");
       
 	  // Narayan Rel 8.3 CR 821 07/03/2013 End
        registerAttribute("credit_note_ind", "credit_note_ind", "IndicatorAttribute");
        registerAttribute("remaining_amount", "remaining_amount", "TradePortalDecimalAttribute");
   }
   
   




}
