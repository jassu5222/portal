

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceUploadErrorLogWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


registerAttribute("invoice_upload_error_log_oid", "invoice_upload_error_log_oid", "ObjectIDAttribute");
      
      /* invoice_id unique key */
      registerAttribute("invoice_id", "invoice_id");
      
      /* error_msg - Error message. */
      registerAttribute("error_msg", "error_msg");
      
      /* Trading partner name. */
      registerAttribute("trading_partner_name", "trading_partner_name");
      
        /* Pointer to the parent InvoiceFileUpload */
      registerAttribute("invoice_file_upload_oid", "p_invoice_file_upload_uoid", "ParentIDAttribute");
   }
   
   




}
