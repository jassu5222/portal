

  

package com.ams.tradeportal.busobj.webbean;


/**
 * Invoice.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvoiceWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* invoice_oid - Unique Identifier. */
       registerAttribute("invoice_oid", "invoice_oid", "ObjectIDAttribute");


       /* otl_invoice_uoid - The OTL UOID of the invoice.  This is sent back to OTL after manual matching
         with payment. */
       registerAttribute("otl_invoice_uoid", "otl_invoice_uoid");


       /* invoice_reference_id - Invoice Reference ID */
       registerAttribute("invoice_reference_id", "invoice_reference_id");


       /* link_to_instrument_type -  */
       registerAttribute("link_to_instrument_type", "link_to_instrument_type");


       /* instrument_id - Instrument ID */
       registerAttribute("instrument_id", "instrument_id");


       /* status - Status of the invoice. */
       registerAttribute("status", "status");


       /* source_of_data -  */
       registerAttribute("source_of_data", "source_of_data");


       /* otl_customer_id - OTL Customer ID of the buyer. */
       registerAttribute("otl_customer_id", "otl_customer_id");


       /* invoice_version -  */
       registerAttribute("invoice_version", "invoice_version", "NumberAttribute");


       /* invoice_issue_datetime -  */
       registerAttribute("invoice_issue_datetime", "invoice_issue_datetime", "DateTimeAttribute");


       /* finance_status -  */
       registerAttribute("finance_status", "finance_status");


       /* invoice_total_amount -  */
       registerAttribute("invoice_total_amount", "invoice_total_amount", "TradePortalDecimalAttribute");


       /* finance_percentage -  */
       registerAttribute("finance_percentage", "finance_percentage", "TradePortalDecimalAttribute");


       /* finance_amount -  */
       registerAttribute("finance_amount", "finance_amount", "TradePortalDecimalAttribute");


       /* matching_status -  */
       registerAttribute("matching_status", "matching_status");


       /* invoice_due_date -  */
       registerAttribute("invoice_due_date", "invoice_due_date", "DateAttribute");


       /* activity_sequence_datetime - The Sequence Date Time of the last activity that updated the invoice. */
       registerAttribute("activity_sequence_datetime", "activity_sequence_datetime", "DateTimeAttribute");


       /* buyer_party_identifier -  */
       registerAttribute("buyer_party_identifier", "buyer_party_identifier");


       /* buyer_address_line_1 -  */
       registerAttribute("buyer_address_line_1", "buyer_address_line_1");


       /* buyer_address_line_2 -  */
       registerAttribute("buyer_address_line_2", "buyer_address_line_2");


       /* buyer_address_country -  */
       registerAttribute("buyer_address_country", "buyer_address_country");


       /* seller_party_identifier -  */
       registerAttribute("seller_party_identifier", "seller_party_identifier");


       /* seller_address_line_1 -  */
       registerAttribute("seller_address_line_1", "seller_address_line_1");


       /* seller_address_line_2 -  */
       registerAttribute("seller_address_line_2", "seller_address_line_2");


       /* seller_address_country -  */
       registerAttribute("seller_address_country", "seller_address_country");


       /* bill_party_identifier -  */
       registerAttribute("bill_party_identifier", "bill_party_identifier");


       /* bill_address_line_1 -  */
       registerAttribute("bill_address_line_1", "bill_address_line_1");


       /* bill_address_line_2 -  */
       registerAttribute("bill_address_line_2", "bill_address_line_2");


       /* bill_address_country -  */
       registerAttribute("bill_address_country", "bill_address_country");


       /* disputed_indicator -  */
       registerAttribute("disputed_indicator", "disputed_indicator", "IndicatorAttribute");


       /* invoice_outstanding_amount - Outstanding amount. */
       registerAttribute("invoice_outstanding_amount", "invoice_outstanding_amount", "TradePortalDecimalAttribute");


       /* currency_code - The currency of the invoice */
       registerAttribute("currency_code", "currency_code");


       /* invoice_status - Invoice status which could be one of the following:
         OPN	Open 
         DSP	Disputed
         CLD	Closed
         PAD	Paid 
         MTH	Matched
         FNR	Finance requested
CFN	Closed/financed
 */
       registerAttribute("invoice_status", "invoice_status");


       /* invoice_payment_status - Invoice status */
       registerAttribute("invoice_payment_status", "invoice_payment_status");


       /* invoice_payment_amount - The amount that is already paid for this invoice. */
       registerAttribute("invoice_payment_amount", "invoice_payment_amount", "TradePortalDecimalAttribute");


       /* closed_indicator -  */
       registerAttribute("closed_indicator", "closed_indicator", "IndicatorAttribute");


       /* invoice_credit_note_amount - Credit Note Amount */
       registerAttribute("invoice_credit_note_amount", "invoice_credit_note_amount", "TradePortalDecimalAttribute");


       /* invoice_discount_amount - Discount amount of the invoice. */
       registerAttribute("invoice_discount_amount", "invoice_discount_amount", "TradePortalDecimalAttribute");


       /* pending_payment_amount - The payment amount that has been sent to OTL via middleware but the Apply
         Payment activitiy is not released in OTL yet. */
       registerAttribute("pending_payment_amount", "pending_payment_amount", "TradePortalDecimalAttribute");

       /* finance_instrument_oid -  */
       registerAttribute("finance_instrument_oid", "a_finance_instrument_oid", "NumberAttribute");

       /* client_bank_oid -  */
       registerAttribute("client_bank_oid", "a_client_bank_oid", "NumberAttribute");

       /* corp_org_oid - The Corporate Org that is the seller party of the invoice. */
       registerAttribute("corp_org_oid", "a_corp_org_oid", "NumberAttribute");
       
     //Ravindra - Rel800 CR710 - 20th Dec 2011 - Start
       /* invoice_payment_date -  */
       registerAttribute("invoice_payment_date", "invoice_payment_date", "DateAttribute");
       /* disputed_indicator -  */
       registerAttribute("portal_originated_ind", "portal_originated_ind", "IndicatorAttribute");
     //Ravindra - Rel800 CR710 - 20th Dec 2011 - End
		 registerAttribute("supplier_portal_invoice_ind", "supplier_portal_invoice_ind", "IndicatorAttribute");
		 registerAttribute("goods_description", "goods_description");
		 registerAttribute("invoice_offer_group_oid", "a_invoice_offer_group_oid", "NumberAttribute");
		 registerAttribute("future_value_date", "future_value_date", "DateAttribute");
		 registerAttribute("net_amount_offered", "net_amount_offered", "TradePortalDecimalAttribute");
		 registerAttribute("supplier_portal_invoice_status", "supplier_portal_invoice_status");
		 registerAttribute("offer_expiry_date", "offer_expiry_date", "DateAttribute");
		 registerAttribute("document_image_oid", "a_document_image_oid", "NumberAttribute");
                 registerAttribute("old_net_amount_offered", "old_net_amount_offered", "LocalAttribute");
		 registerAttribute("rejection_reason_text", "rejection_reason_text");
		 
		//M Eerupula 05/14/2013 Rel8.3 CR776
		registerAttribute("sp_email_sent_ind", "sp_email_sent_ind", "IndicatorAttribute");
		
		  //CR913 start
		  registerAttribute("send_to_supplier_date_ind", "send_to_supplier_date_ind", "IndicatorAttribute");
		  registerAttribute("send_to_supplier_date", "send_to_supplier_date", "DateAttribute");
		  /* panel_auth_group_oid - Panel group associated to an Invoice. */
	      registerAttribute("panel_auth_group_oid", "a_panel_auth_group_oid", "NumberAttribute");
	        
	      /* panel_auth_range_oid - Panel Range to a InvoicesSummaryData. */
	      registerAttribute("panel_auth_range_oid", "a_panel_auth_range_oid", "NumberAttribute");
	       
	      /* opt_lock - Optimistic lock attribute*/
	      registerAttribute("panel_oplock_val", "panel_oplock_val", "NumberAttribute");
	      
	    //copy of send_to_supplier_date sent from TPS - used while authorising from portal 
	      registerAttribute("send_to_supplier_date_tps", "send_to_supplier_date_tps", "DateAttribute");
	      //copy of invoice_payment_amount sent from TPS - used while authorising from portal 
	      registerAttribute("invoice_payment_amount_tps", "invoice_payment_amount_tps", "TradePortalDecimalAttribute");
	      //copy of invoice_payment_date sent from TPS - used while authorising from portal 
	      registerAttribute("invoice_payment_date_tps", "invoice_payment_date_tps", "DateAttribute");
	      
		  //CR913 end	
	      registerAttribute("total_credit_note_amount", "total_credit_note_amount","TradePortalDecimalAttribute");
	      registerAttribute("inv_amount_after_adj", "inv_amount_after_adj","TradePortalDecimalAttribute");
	      registerAttribute("end_to_end_id", "end_to_end_id");
	      
	    //CR1006 start
	      registerAttribute("pay_method", "pay_method");
	      registerAttribute("ben_acct_num", "ben_acct_num");
	      registerAttribute("ben_add1", "ben_address_one");
	      registerAttribute("ben_add2", "ben_address_two");
	      registerAttribute("ben_add3", "ben_address_three");
	      registerAttribute("ben_country", "ben_country");
	      registerAttribute("ben_bank_name", "ben_bank_name");
	      registerAttribute("ben_branch_code", "ben_branch_code");
	      registerAttribute("ben_email_addr", "ben_email_addr");
	      registerAttribute("ben_bank_sort_code", "ben_bank_sort_code");
	      registerAttribute("ben_branch_add1", "ben_branch_address1");
	      registerAttribute("ben_branch_add2", "ben_branch_address2");
	      registerAttribute("ben_bank_city", "ben_bank_city");
	      registerAttribute("ben_bank_province", "ben_bank_prvnce");
	      registerAttribute("ben_branch_country", "ben_branch_country");
	      registerAttribute("charges", "payment_charges");
	      registerAttribute("central_bank_rep1", "central_bank_rep1");
	      registerAttribute("central_bank_rep2", "central_bank_rep2");
	      registerAttribute("central_bank_rep3", "central_bank_rep3");	
	    //CR1006 end
		registerAttribute("net_amount_paid", "net_amount_paid", "TradePortalDecimalAttribute");
   }
   
   




}
