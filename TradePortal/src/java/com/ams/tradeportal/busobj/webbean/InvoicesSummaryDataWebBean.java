package com.ams.tradeportal.busobj.webbean;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


public class InvoicesSummaryDataWebBean extends InvoicesSummaryDataWebBean_Base {
	private static final Logger LOG = LoggerFactory.getLogger(InvoicesSummaryDataWebBean.class);

	/**
	 * Builds a set of sorted html option tags based on the 6 predefined data
	 * attributes (amount, ben_name, currency, ...) as well as the generic
	 * "otherx" fields.  The option id is the internal identifier (e.g., amount 
	 * or other1) and the displayed value is whatever name the user has 
	 * assigned to that attribute.  Only non-blank attributes are included.
	 * 
	 * @return java.lang.String
	 */
	
	
	
	/**
	 * Identifies and retrieves the Matching Rule for this invoice.
	 * First it looks for a rule with a matching buyer_name.  If no match is
	 * found for the buyer_name, it looks for a rule with a matching buyer_id.
	 * 
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public String getMatchingRule() throws AmsException, RemoteException {
		final String outerSelect = "SELECT DISTINCT ar_matching_rule_oid"
			+ " FROM ar_matching_rule WHERE ";
		final String innerSelect = " OR ar_matching_rule_oid IN"
			+ " (SELECT p_ar_matching_rule_oid FROM";

		String sqlBuilder = null;
		DocumentHandler resultSet = null;
		String tradePartnerOid = null;
        String corpOrgOid = getAttribute("corp_org_oid");
		
        String buyerName = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_name"):getAttribute("seller_name");//SHR CR708 Rel8.1.1 
		
		if (StringFunction.isNotBlank(buyerName)) {
			buyerName = StringFunction.toUnistr(buyerName.toUpperCase());

			sqlBuilder = outerSelect + "p_corp_org_oid = ? and (buyer_name = unistr(?) " + innerSelect + " ar_buyer_name_alias WHERE buyer_name_alias = unistr(?)))"; // Nar -MKUM061235356

			resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, buyerName, buyerName);
			if (resultSet != null) {
				tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
			}
		}

		if (tradePartnerOid == null) {
			String buyerId = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(getAttribute("invoice_classification"))?getAttribute("buyer_id"):getAttribute("seller_id");//SHR CR708 Rel8.1.1
			if (StringFunction.isNotBlank(buyerId)) {
				buyerId = StringFunction.toUnistr(buyerId.toUpperCase());

				sqlBuilder = outerSelect + " p_corp_org_oid = ? and (buyer_id = unistr(?) " + innerSelect + " ar_buyer_id_alias WHERE buyer_id_alias = unistr(?)))"; // Nar -MKUM061235356
				resultSet = DatabaseQueryBean.getXmlResultSet(sqlBuilder, false, corpOrgOid, buyerId, buyerId);
				if (resultSet != null) {
					tradePartnerOid = resultSet.getAttribute("/ResultSetRecord/AR_MATCHING_RULE_OID");
				}
			}
		}

		

		return tradePartnerOid;
	}
	
}
