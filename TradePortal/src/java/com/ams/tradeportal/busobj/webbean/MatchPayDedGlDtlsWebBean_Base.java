package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
  Invoice data information uploaded will be stored here
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MatchPayDedGlDtlsWebBean_Base extends TradePortalBusinessObjectWebBean {

	/*
	 * Register the attributes and associations of the business object
	 */
	  public void registerAttributes()
	   {  

		      /* Register attributes defined in the Ancestor class */
		      super.registerAttributes();
		      /* match_pay_ded_gl_dtls_oid - MATCH_PAY_DED_GL_DTLS_OID - Unique identifier */
		      registerAttribute("match_pay_ded_gl_dtls_oid", "match_pay_ded_gl_dtls_oid", "ObjectIDAttribute");		      
		      /* invoice_reference_id -  */
		      registerAttribute("invoice_reference_id", "invoice_reference_id");
		      /* Transaction date */
		      registerAttribute("transaction_date", "transaction_date", "DateAttribute");
		      /*  Transaction type  */
		      registerAttribute("transaction_type", "transaction_type");
		      /*  This is the Applied Amount.  */
		      registerAttribute("applied_amount", "applied_amount");
		      /*  This is the General_Ledger_Code.  */
		      registerAttribute("general_ledger_code", "general_ledger_code");	      
		      /*  This is the Discount_Code.  */
		      registerAttribute("discount_code", "discount_code");	      
		      /* discount_description - Description of the discount. */
		      registerAttribute("discount_description", "discount_description");
		      /* discount_comments - comments on the discount. */	
		      registerAttribute("discount_comments", "discount_comments");	
		       /* system date time. */
		      registerAttribute("system_date_time", "system_date_time","DateTimeAttribute");
		      /*Set to UOID of Payment/Remittance in the Portal*/
		      registerAttribute("a_initiating_obj", "a_initiating_obj");	
		      /*Identifies the process creating this record - For portal it will be payremit*/
		      registerAttribute("initiating_obj_cls", "initiating_obj_cls");		      
		      /*OTL_INVOICE _OID*/
		      registerAttribute("otl_invoice_oid", "otl_invoice_oid");
		   }
	  
		   		   
	}

