

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotificationRuleCriterionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  

       super.registerAttributes();
		
		      /* criterion_oid - Unique Identifier */
      registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");
      
	  registerAttribute("transaction_type_or_action", "transaction_type_or_action");

	  registerAttribute("instrument_type_or_category", "instrument_type_or_category");	   

      /* setting - Determines if an email or notification message will be sent to the Corporate
         Customer. */
      registerAttribute("send_notif_setting", "send_notif_setting", "NOTIF_RULE_SETTING");

     registerAttribute("send_email_setting", "send_email_setting", "NOTIF_RULE_SETTING");

	  registerAttribute("additional_email_addr", "additional_email_addr");	         

	  registerAttribute("notify_email_freq", "notify_email_freq","NumberAttribute");	    	   
        /* Pointer to the parent NotificationRule */
      registerAttribute("notify_rule_oid", "p_notify_rule_oid", "ParentIDAttribute");

	   registerAttribute("notification_user_ids", "notification_user_ids");		
	   
	   registerAttribute("clear_tran_ind", "clear_tran_ind");	
   }
   
   




}
