

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Defines a rule which determines if a Corporate Customer will receive notification
 * messages and emails after a transaction has been authorized.
 * 
 * A Notification Rule can be established by Client Banks and Bank Group level
 * users.  The Notification Rule can then be assigned to a specific Corporate
 * Customer.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotificationRuleTemplateWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* notification_rule_oid - Unique identifier */
       registerAttribute("notification_rule_oid", "notification_rule_oid", "ObjectIDAttribute");


       /* description - Description of the rule */
       registerAttribute("description", "description");


       /* email_frequency - Determines whether notification emails will be received for each transaction
         or daily. */
       registerAttribute("email_frequency", "email_frequency");

        /* email_for_ar_match_notice - Whether an email will be sent when AR Match Notice is received. */
//       registerAttribute("email_for_ar_match_notice", "email_for_ar_match_notice");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
       
 	    registerAttribute("template_ind", "template_ind", "IndicatorAttribute");
       
       registerAttribute("source_template_notif_oid", "source_template_notif_oid","NumberAttribute");

	    registerAttribute("save_as_flag", "save_as_flag", "LocalAttribute");
    
		 registerAttribute("default_apply_to_all_grp", "default_apply_to_all_grp", "LocalAttribute");

	  	registerAttribute("default_clear_to_all_grp", "default_clear_to_all_grp", "LocalAttribute");		
 
		registerAttribute("default_notify_setting", "default_notify_setting", "LocalAttribute");			
		
	  	registerAttribute("default_email_setting", "default_email_setting", "LocalAttribute");	

		registerAttribute("supplierReadOnly", "supplierReadOnly", "LocalAttribute");	
		
		registerAttribute("apply_to_all_tran0", "apply_to_all_tran0", "LocalAttribute");		
		registerAttribute("apply_to_all_tran1", "apply_to_all_tran1", "LocalAttribute");		
		registerAttribute("apply_to_all_tran2", "apply_to_all_tran2", "LocalAttribute");		
		registerAttribute("apply_to_all_tran3", "apply_to_all_tran3", "LocalAttribute");		
		registerAttribute("apply_to_all_tran4", "apply_to_all_tran4", "LocalAttribute");		
		registerAttribute("apply_to_all_tran5", "apply_to_all_tran5", "LocalAttribute");		
		registerAttribute("apply_to_all_tran6", "apply_to_all_tran6", "LocalAttribute");		
		registerAttribute("apply_to_all_tran7", "apply_to_all_tran7", "LocalAttribute");		
		registerAttribute("apply_to_all_tran8", "apply_to_all_tran8", "LocalAttribute");		
		registerAttribute("apply_to_all_tran9", "apply_to_all_tran9", "LocalAttribute");		
		registerAttribute("apply_to_all_tran10", "apply_to_all_tran10", "LocalAttribute");	
		registerAttribute("apply_to_all_tran11", "apply_to_all_tran11", "LocalAttribute");		
		registerAttribute("apply_to_all_tran12", "apply_to_all_tran12", "LocalAttribute");		
		registerAttribute("apply_to_all_tran13", "apply_to_all_tran13", "LocalAttribute");		
		registerAttribute("apply_to_all_tran14", "apply_to_all_tran14", "LocalAttribute");		
		registerAttribute("apply_to_all_tran15", "apply_to_all_tran15", "LocalAttribute");		
		registerAttribute("apply_to_all_tran16", "apply_to_all_tran16", "LocalAttribute");		
		registerAttribute("apply_to_all_tran17", "apply_to_all_tran17", "LocalAttribute");		
		registerAttribute("apply_to_all_tran18", "apply_to_all_tran18", "LocalAttribute");		
		registerAttribute("apply_to_all_tran19", "apply_to_all_tran19", "LocalAttribute");		
		registerAttribute("apply_to_all_tran20", "apply_to_all_tran20", "LocalAttribute");	
		registerAttribute("apply_to_all_tran21", "apply_to_all_tran21", "LocalAttribute");		
		registerAttribute("apply_to_all_tran22", "apply_to_all_tran22", "LocalAttribute");		
		registerAttribute("apply_to_all_tran23", "apply_to_all_tran23", "LocalAttribute");		
		registerAttribute("apply_to_all_tran24", "apply_to_all_tran24", "LocalAttribute");		
		registerAttribute("apply_to_all_tran25", "apply_to_all_tran25", "LocalAttribute");		
		registerAttribute("apply_to_all_tran26", "apply_to_all_tran26", "LocalAttribute");		
		registerAttribute("apply_to_all_tran27", "apply_to_all_tran27", "LocalAttribute");		
		registerAttribute("apply_to_all_tran28", "apply_to_all_tran28", "LocalAttribute");		
		registerAttribute("apply_to_all_tran29", "apply_to_all_tran29", "LocalAttribute");		
		registerAttribute("apply_to_all_tran30", "apply_to_all_tran30", "LocalAttribute");	
		registerAttribute("apply_to_all_tran31", "apply_to_all_tran31", "LocalAttribute");		
		registerAttribute("apply_to_all_tran32", "apply_to_all_tran32", "LocalAttribute");		
		registerAttribute("apply_to_all_tran33", "apply_to_all_tran33", "LocalAttribute");		
		registerAttribute("apply_to_all_tran34", "apply_to_all_tran34", "LocalAttribute");	

		registerAttribute("clear_to_all_tran0", "clear_to_all_tran0", "LocalAttribute");	
		registerAttribute("clear_to_all_tran1", "clear_to_all_tran1", "LocalAttribute");		
		registerAttribute("clear_to_all_tran2", "clear_to_all_tran2", "LocalAttribute");		
		registerAttribute("clear_to_all_tran3", "clear_to_all_tran3", "LocalAttribute");		
		registerAttribute("clear_to_all_tran4", "clear_to_all_tran4", "LocalAttribute");		
		registerAttribute("clear_to_all_tran5", "clear_to_all_tran5", "LocalAttribute");		
		registerAttribute("clear_to_all_tran6", "clear_to_all_tran6", "LocalAttribute");		
		registerAttribute("clear_to_all_tran7", "clear_to_all_tran7", "LocalAttribute");		
		registerAttribute("clear_to_all_tran8", "clear_to_all_tran8", "LocalAttribute");		
		registerAttribute("clear_to_all_tran9", "clear_to_all_tran9", "LocalAttribute");		
		registerAttribute("clear_to_all_tran10", "clear_to_all_tran10", "LocalAttribute");	
		registerAttribute("clear_to_all_tran11", "clear_to_all_tran11", "LocalAttribute");		
		registerAttribute("clear_to_all_tran12", "clear_to_all_tran12", "LocalAttribute");		
		registerAttribute("clear_to_all_tran13", "clear_to_all_tran13", "LocalAttribute");		
		registerAttribute("clear_to_all_tran14", "clear_to_all_tran14", "LocalAttribute");		
		registerAttribute("clear_to_all_tran15", "clear_to_all_tran15", "LocalAttribute");		
		registerAttribute("clear_to_all_tran16", "clear_to_all_tran16", "LocalAttribute");		
		registerAttribute("clear_to_all_tran17", "clear_to_all_tran17", "LocalAttribute");		
		registerAttribute("clear_to_all_tran18", "clear_to_all_tran18", "LocalAttribute");		
		registerAttribute("clear_to_all_tran19", "clear_to_all_tran19", "LocalAttribute");		
		registerAttribute("clear_to_all_tran20", "clear_to_all_tran20", "LocalAttribute");	
		registerAttribute("clear_to_all_tran21", "clear_to_all_tran21", "LocalAttribute");		
		registerAttribute("clear_to_all_tran22", "clear_to_all_tran22", "LocalAttribute");		
		registerAttribute("clear_to_all_tran23", "clear_to_all_tran23", "LocalAttribute");		
		registerAttribute("clear_to_all_tran24", "clear_to_all_tran24", "LocalAttribute");		
		registerAttribute("clear_to_all_tran25", "clear_to_all_tran25", "LocalAttribute");		
		registerAttribute("clear_to_all_tran26", "clear_to_all_tran26", "LocalAttribute");		
		registerAttribute("clear_to_all_tran27", "clear_to_all_tran27", "LocalAttribute");		
		registerAttribute("clear_to_all_tran28", "clear_to_all_tran28", "LocalAttribute");		
		registerAttribute("clear_to_all_tran29", "clear_to_all_tran29", "LocalAttribute");		
		registerAttribute("clear_to_all_tran30", "clear_to_all_tran30", "LocalAttribute");	
		registerAttribute("clear_to_all_tran31", "clear_to_all_tran31", "LocalAttribute");		
		registerAttribute("clear_to_all_tran32", "clear_to_all_tran32", "LocalAttribute");	
		registerAttribute("clear_to_all_tran33", "clear_to_all_tran33", "LocalAttribute");		
		registerAttribute("clear_to_all_tran34", "clear_to_all_tran34", "LocalAttribute");	

		registerAttribute("send_notif_setting0", "send_notif_setting0", "LocalAttribute");	
		registerAttribute("send_notif_setting1", "send_notif_setting1", "LocalAttribute");		
		registerAttribute("send_notif_setting2", "send_notif_setting2", "LocalAttribute");		
		registerAttribute("send_notif_setting3", "send_notif_setting3", "LocalAttribute");		
		registerAttribute("send_notif_setting4", "send_notif_setting4", "LocalAttribute");		
		registerAttribute("send_notif_setting5", "send_notif_setting5", "LocalAttribute");		
		registerAttribute("send_notif_setting6", "send_notif_setting6", "LocalAttribute");		
		registerAttribute("send_notif_setting7", "send_notif_setting7", "LocalAttribute");		
		registerAttribute("send_notif_setting8", "send_notif_setting8", "LocalAttribute");		
		registerAttribute("send_notif_setting9", "send_notif_setting9", "LocalAttribute");		
		registerAttribute("send_notif_setting10", "send_notif_setting10", "LocalAttribute");	
		registerAttribute("send_notif_setting11", "send_notif_setting11", "LocalAttribute");		
		registerAttribute("send_notif_setting12", "send_notif_setting12", "LocalAttribute");		
		registerAttribute("send_notif_setting13", "send_notif_setting13", "LocalAttribute");		
		registerAttribute("send_notif_setting14", "send_notif_setting14", "LocalAttribute");		
		registerAttribute("send_notif_setting15", "send_notif_setting15", "LocalAttribute");		
		registerAttribute("send_notif_setting16", "send_notif_setting16", "LocalAttribute");		
		registerAttribute("send_notif_setting17", "send_notif_setting17", "LocalAttribute");		
		registerAttribute("send_notif_setting18", "send_notif_setting18", "LocalAttribute");		
		registerAttribute("send_notif_setting19", "send_notif_setting19", "LocalAttribute");		
		registerAttribute("send_notif_setting20", "send_notif_setting20", "LocalAttribute");	
		registerAttribute("send_notif_setting21", "send_notif_setting21", "LocalAttribute");		
		registerAttribute("send_notif_setting22", "send_notif_setting22", "LocalAttribute");		
		registerAttribute("send_notif_setting23", "send_notif_setting23", "LocalAttribute");		
		registerAttribute("send_notif_setting24", "send_notif_setting24", "LocalAttribute");		
		registerAttribute("send_notif_setting25", "send_notif_setting25", "LocalAttribute");		
		registerAttribute("send_notif_setting26", "send_notif_setting26", "LocalAttribute");		
		registerAttribute("send_notif_setting27", "send_notif_setting27", "LocalAttribute");		
		registerAttribute("send_notif_setting28", "send_notif_setting28", "LocalAttribute");		
		registerAttribute("send_notif_setting29", "send_notif_setting29", "LocalAttribute");		
		registerAttribute("send_notif_setting30", "send_notif_setting30", "LocalAttribute");	
		registerAttribute("send_notif_setting31", "send_notif_setting31", "LocalAttribute");	
		registerAttribute("send_notif_setting32", "send_notif_setting32", "LocalAttribute");		
		registerAttribute("send_notif_setting33", "send_notif_setting33", "LocalAttribute");	
		registerAttribute("send_notif_setting34", "send_notif_setting34", "LocalAttribute");		

		registerAttribute("send_email_setting0", "send_email_setting0", "LocalAttribute");	
		registerAttribute("send_email_setting1", "send_email_setting1", "LocalAttribute");		
		registerAttribute("send_email_setting2", "send_email_setting2", "LocalAttribute");		
		registerAttribute("send_email_setting3", "send_email_setting3", "LocalAttribute");		
		registerAttribute("send_email_setting4", "send_email_setting4", "LocalAttribute");		
		registerAttribute("send_email_setting5", "send_email_setting5", "LocalAttribute");		
		registerAttribute("send_email_setting6", "send_email_setting6", "LocalAttribute");		
		registerAttribute("send_email_setting7", "send_email_setting7", "LocalAttribute");		
		registerAttribute("send_email_setting8", "send_email_setting8", "LocalAttribute");		
		registerAttribute("send_email_setting9", "send_email_setting9", "LocalAttribute");		
		registerAttribute("send_email_setting10", "send_email_setting10", "LocalAttribute");			
		registerAttribute("send_email_setting11", "send_email_setting11", "LocalAttribute");		
		registerAttribute("send_email_setting12", "send_email_setting12", "LocalAttribute");		
		registerAttribute("send_email_setting13", "send_email_setting13", "LocalAttribute");		
		registerAttribute("send_email_setting14", "send_email_setting14", "LocalAttribute");		
		registerAttribute("send_email_setting15", "send_email_setting15", "LocalAttribute");		
		registerAttribute("send_email_setting16", "send_email_setting16", "LocalAttribute");		
		registerAttribute("send_email_setting17", "send_email_setting17", "LocalAttribute");		
		registerAttribute("send_email_setting18", "send_email_setting18", "LocalAttribute");		
		registerAttribute("send_email_setting19", "send_email_setting19", "LocalAttribute");		
		registerAttribute("send_email_setting20", "send_email_setting20", "LocalAttribute");	
		registerAttribute("send_email_setting21", "send_email_setting21", "LocalAttribute");		
		registerAttribute("send_email_setting22", "send_email_setting22", "LocalAttribute");		
		registerAttribute("send_email_setting23", "send_email_setting23", "LocalAttribute");		
		registerAttribute("send_email_setting24", "send_email_setting24", "LocalAttribute");		
		registerAttribute("send_email_setting25", "send_email_setting25", "LocalAttribute");		
		registerAttribute("send_email_setting26", "send_email_setting26", "LocalAttribute");		
		registerAttribute("send_email_setting27", "send_email_setting27", "LocalAttribute");		
		registerAttribute("send_email_setting28", "send_email_setting28", "LocalAttribute");		
		registerAttribute("send_email_setting29", "send_email_setting29", "LocalAttribute");		
		registerAttribute("send_email_setting30", "send_email_setting30", "LocalAttribute");	
		registerAttribute("send_email_setting31", "send_email_setting31", "LocalAttribute");
		registerAttribute("send_email_setting32", "send_email_setting32", "LocalAttribute");	
		registerAttribute("send_email_setting33", "send_email_setting33", "LocalAttribute");		
		registerAttribute("send_email_setting34", "send_email_setting34", "LocalAttribute");		

   }
}