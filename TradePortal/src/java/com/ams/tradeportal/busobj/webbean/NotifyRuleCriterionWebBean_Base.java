

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Allows Notification Rules to be defined differently for particular transaction
 * types and separates the decision criteria to send an email or a notification
 * message for each transaction type.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotifyRuleCriterionWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  

       super.registerAttributes();

       /* criterion_oid - Unique Identifier */
       registerAttribute("criterion_oid", "criterion_oid", "ObjectIDAttribute");


       /* setting - Determines if an email or notification message will be sent to the Corporate
         Customer. */
       registerAttribute("setting", "setting");


       /* transaction_type - Determines the transaction type that this rule relates to. */
       registerAttribute("transaction_type", "transaction_type");


       /* instr_notify_category - Represents the instrument category that this criterion relates to which
         allows notification rules to be categorized by transaction types and groups
         these rules based on category on separate tabs. */
       registerAttribute("instr_notify_category", "instr_notify_category");


       /* criterion_type - Indicates whether this criterion relates to an email or a notification. */
       registerAttribute("criterion_type", "criterion_type");

      /* Pointer to the parent NotificationRule */
       registerAttribute("notify_rule_oid", "p_notify_rule_oid", "ParentIDAttribute");
   }
   
   




}
