

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Defines a rule which determines if a Corporate Customer will receive notification
 * messages and emails after a transaction has been authorized.
 * 
 * A Notification Rule can be established by Client Banks and Bank Group level
 * users.  The Notification Rule can then be assigned to a specific Corporate
 * Customer.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NotifyRuleUserListWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  

      super.registerAttributes();
       /* notification_rule_oid - Unique identifier */
       registerAttribute("notify_rule_user_list_oid", "notify_rule_user_list_oid", "ObjectIDAttribute");

		registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");

		registerAttribute("criterion_oid", "p_criterion_oid", "ParentIDAttribute");
   }
}