package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POLineItemWebBean extends POLineItemWebBean_Base
{

  public void loadManualPOLineItemsFromDoc(DocumentHandler poLineItemDoc, Vector poLineItemFieldList) 
  {
    // This method takes a document containing PO Line Item information and 
    // overwrites the data in the web bean with the data in the document.
    String             poLineItemOid;
    String             fieldType;
    String             fieldValue;
    int                fieldIndex          = 0;
    POLineItemField    poLineItemField     = null;

    // Return if the PO Line Item document is empty
    if (poLineItemDoc == null)
    {
      return;
    }
    
    // Loop through the fields defined in the PO Definition, retrieve their values 
    // from the document, and set the corresponding attribute value in the web bean
    poLineItemOid = poLineItemDoc.getAttribute("/po_line_item_oid");
    if (poLineItemOid != null)
    {
      this.setAttribute("po_line_item_oid", poLineItemOid);
    }

    fieldValue = poLineItemDoc.getAttribute("/selected_indicator");
    if (fieldValue != null)
    {
      this.setAttribute("selected_indicator", fieldValue);
    }

    for (fieldIndex = 0; fieldIndex < poLineItemFieldList.size(); fieldIndex++)
    {
      poLineItemField = (POLineItemField) poLineItemFieldList.elementAt(fieldIndex);
      fieldType       = poLineItemField.getFieldType();
      fieldValue      = poLineItemDoc.getAttribute("/" + fieldType);
           
      if (fieldValue != null)
      {
        this.setAttribute(fieldType, fieldValue);
      }
    }
  }

}
