

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Used to keep track of the passwords that have been used by a user.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PasswordHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* password_history_oid - Object identifier */
       registerAttribute("password_history_oid", "password_history_oid", "ObjectIDAttribute");


       /* creation_timestamp - The date and time (stored in the database in the GMT timezone) when the
         password was changed. */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* password - A password entered by the user. */
       registerAttribute("password", "password");

      /* Pointer to the parent User */
       registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");
   }
   
   




}
