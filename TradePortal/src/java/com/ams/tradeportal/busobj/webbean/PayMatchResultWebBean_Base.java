

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayMatchResultWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* pay_match_result_oid - Unique Identifier */
       registerAttribute("pay_match_result_oid", "pay_match_result_oid", "ObjectIDAttribute");


       /* invoice_matching_status - The status of the matched invoice. A-automatched, M-Manually Matched, P-Partial
         Match, U-Unmatched - an association to the Matched Results Type table. 
         THESE SEEM TO CORRELATE TO THE NEW PORTAL matching_status_type - PPD (proposed),
         AMD (automatched), MMD (manual match), NMD- Not matched */
       registerAttribute("invoice_matching_status", "invoice_matching_status");


       /* invoice_paid_status - The status can be pending/applied/none */
       registerAttribute("invoice_paid_status", "invoice_paid_status");


       /* match_explanation - The explanation of the matching
 */
       registerAttribute("match_explanation", "match_explanation");


       /* matched_amount - The amount that was matched */
       registerAttribute("matched_amount", "matched_amount", "TradePortalDecimalAttribute");
       
       /* IR#T36000018260 - save the discount amount applied
        * discount_applied - The discount amount that was applied */
       registerAttribute("discount_applied", "discount_applied", "TradePortalDecimalAttribute");       


       /* manual_indicator - Whether the Invoice is manually picked by TP user for matching.  
         
         Y = The invoice is manually picked by TP user for matching.
         N = The invoice is proposed by OTL for matching. */
       registerAttribute("manual_indicator", "manual_indicator", "IndicatorAttribute");


       /* otl_invoice_uoid - OTL Uoid of the Invoice being matched. */
       registerAttribute("otl_invoice_uoid", "otl_invoice_uoid");


       /* invoice_reference_id - Invoice Reference ID of the invoice being matched. */
       registerAttribute("invoice_reference_id", "invoice_reference_id");


       /* otl_pay_remit_invoice_uoid - OTL Uoid of the Pay Remit Invoice being matched. */
       registerAttribute("otl_pay_remit_invoice_uoid", "otl_pay_remit_invoice_uoid");


       /* invoice_outstanding_amount - The outstanding amount of the invoice at the time of matching on TP.  This
         data will be sent to OTL and in case the outstanding amount is changed on
         invoice and matched_amount > Outsdanding amount on invoice, the match will
         be returned to TP for re-match.  This could happen if this invoice is matched
         to another payment while the current payment is being matched in TP. */
       registerAttribute("invoice_outstanding_amount", "invoice_outstanding_amount", "TradePortalDecimalAttribute");

      /* Pointer to the parent PayRemit */
       registerAttribute("pay_remit_oid", "p_pay_remit_oid", "ParentIDAttribute");

       /* pay_remit_invoice_oid -  */
       registerAttribute("pay_remit_invoice_oid", "a_pay_remit_invoice_oid", "NumberAttribute");

       /* invoice_oid -  */
       registerAttribute("invoice_oid", "a_invoice_oid", "NumberAttribute");

       /* user_oid -  */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
       
       /* IR 22112 - pending_matched_amount - copy of The amount that was matched from TPS*/
       registerAttribute("pending_matched_amount", "pending_matched_amount", "TradePortalDecimalAttribute");
   }
   
   




}
