package com.ams.tradeportal.busobj.webbean;

/**
 * The Payable Invoice Payment Instructions for Bank defined Trading Parter.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PayableInvPayInstWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* payable_inv_pay_inst_oid - Unique Identifier */
       registerAttribute("payable_inv_pay_inst_oid", "payable_inv_pay_inst_oid", "ObjectIDAttribute");

       /* currency_code - The currency for Payable invoice instruction in Trading Partner. */
       registerAttribute("currency_code", "currency_code");
       
       /* inv_pay_instruction_ind - Whether Payable invoice payment instruction exists in TPS for respective currency*/
       registerAttribute("inv_pay_instruction_ind", "inv_pay_instruction_ind", "IndicatorAttribute");

      /* Pointer to the parent ArMatchingRule */
       registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");
   }
   
   




}
