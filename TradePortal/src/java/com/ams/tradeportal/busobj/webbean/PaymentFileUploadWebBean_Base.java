

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The Payment Files that are to be uploaded.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentFileUploadWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* payment_file_upload_oid - Unique Identifider */
       registerAttribute("payment_file_upload_oid", "payment_file_upload_oid", "ObjectIDAttribute");


       /* payment_file_name - Payment File Name. */
       registerAttribute("payment_file_name", "payment_file_name");


       /* validation_status - The status of Payment File upload validation. */
       registerAttribute("validation_status", "validation_status");


       /* number_of_bene_uploaded - The number of beneficiaries successfully uploaded. */
       registerAttribute("number_of_bene_uploaded", "number_of_bene_uploaded", "NumberAttribute");


       /* number_of_bene_failed - Number of beneficiaries that failed for upload. */
       registerAttribute("number_of_bene_failed", "number_of_bene_failed", "NumberAttribute");


       /* creation_timestamp - Date/Time when the file is uploaded. */
       registerAttribute("creation_timestamp", "creation_timestamp", "DateTimeAttribute");


       /* completion_timestamp - Date/Time when the validation is completed. */
       registerAttribute("completion_timestamp", "completion_timestamp", "DateTimeAttribute");


       /* agent_id - The name of the upload agent that processes/validates the file.  This prevents
         multiple agents from accessing the same file. */
       registerAttribute("agent_id", "agent_id");


       /* deleted_ind - Whether the file is deleted. */
       registerAttribute("deleted_ind", "deleted_ind", "IndicatorAttribute");


       /* error_msg - Error message of uploading. */
       registerAttribute("error_msg", "error_msg");


       /* validation_seconds - The time it takes the agent to validate the payment file. */
       registerAttribute("validation_seconds", "validation_seconds", "NumberAttribute");

       /* user_oid - The user who initiates the Payment File Upload. */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");

       /* instrument_oid - The instrument that the Payment File Upload creates. */
       registerAttribute("instrument_oid", "a_instrument_oid", "NumberAttribute");

       /* owner_org_oid - The Corporate Customer who uploaded the file. */
       registerAttribute("owner_org_oid", "a_owner_org_oid", "NumberAttribute");
       
       /* CJR CR-593 gxs_ref */
 	   registerAttribute("gxs_ref", "gxs_ref");
 	  
 	   /* CJR CR-593 customer_file_ref */
 	   registerAttribute("customer_file_ref", "customer_file_ref");

      /* payment_definition_oid The oid of the payment_definition.  */
      registerAttribute("payment_definition_oid", "a_payment_definition_oid", "NumberAttribute");
      
      /* NAR CR-694A Rel9.0.0.0 05-MAY-2014  */
      /* h2h_source - sender of payment file i.e GXS or FLA */
	  registerAttribute("h2h_source", "h2h_source");
	  
	  /* datetime_process_start - Date/Time when the file processing started. */
      registerAttribute("datetime_process_start", "datetime_process_start", "DateTimeAttribute");
      /* total_process_seconds - The total time it takes the agent to process the invoice file. */
      registerAttribute("total_process_seconds", "total_process_seconds", "NumberAttribute");
      /* process_seconds - The time it takes the agent to validate the invoice file. */
      registerAttribute("process_seconds", "process_seconds", "NumberAttribute");
      /* system_name -  */
      registerAttribute("system_name", "system_name");
      /* server_name - */
      registerAttribute("server_name", "server_name");

      /* PMitnala CR-970 Rel9.1.0.0  */
      /* message_id - messageID of payment file i.e GXS or FLA */
	  registerAttribute("message_id", "message_id");
	  
   }
   
   




}
