

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Rule for Payment Method Validation.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentMethodValidationWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* payment_method_validation_oid - Object ID. */
       registerAttribute("payment_method_validation_oid", "payment_method_validation_oid", "ObjectIDAttribute");


       /* payment_method - Payment Method */
       registerAttribute("payment_method", "payment_method");


       /* country - country */
       registerAttribute("country", "country");


       /* offset_days - Number of Offset Days given the payment method and country. */
       registerAttribute("offset_days", "offset_days", "NumberAttribute");


       /* currency - Currency */
       registerAttribute("currency", "currency");


       /* opt_lock - Optimistic lock attribute.  We need this to take advantage the locking in
         the framework of RefDataMediator.  But we are not inheriting from ReferenceData
         since we do not need the other attributes.
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");
   }
   
   




}
