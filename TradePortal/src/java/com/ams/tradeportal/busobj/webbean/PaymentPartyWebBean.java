package com.ams.tradeportal.busobj.webbean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentPartyWebBean extends PaymentPartyWebBean_Base {
	private static final Logger LOG = LoggerFactory.getLogger(PaymentPartyWebBean.class);

    public PaymentPartyWebBean sourcePartyWebBean = null;	//IAZ CR-586 IR-VRUK091652765 10/10/10 Add

	// Vshah CR507 Begin
	public void loadPaymentPartyFromDoc(DocumentHandler doc) {

		if (doc == null) {
			// If the document is null, there is nothing to populate
			return;
		}

		String value;
		String city, province, postalCode;

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/name");
		if (value == null)
			value = "";
		this.setAttribute("bank_name", value);

		value = doc.getAttribute("/address_line_1");
		if (value == null)
			value = "";
		this.setAttribute("address_line_1", value);

		value = doc.getAttribute("/address_line_2");
		if (value == null)
			value = "";
		this.setAttribute("address_line_2", value);

		city = doc.getAttribute("/address_city");
		province = doc.getAttribute("/address_state_province");
		postalCode = doc.getAttribute("/address_postal_code");

		if (city == null)
			city = "";
		if (province == null)
			province = "";
		if (postalCode == null)
			postalCode = "";
		value = city + " " + province + " " + postalCode;
		
		this.setAttribute("address_line_3", value);

		//IAZ IR-PMUK012737723 02/03/10 Begin
		value = doc.getAttribute("/branch_code");
		if (value == null)
			value = "";
		this.setAttribute("bank_branch_code", value);
		this.setAttribute("branch_name", "");

		value = doc.getAttribute("/address_country");
		if (value == null)
			value = "";
		this.setAttribute("country", value);
		//IAZ IR-PMUK012737723 02/03/10 End

	}

	public void loadPaymentPartyFromDocTagsOnly(DocumentHandler doc) {

		if (doc == null) {
			// If the document is null, there is nothing to populate
			return;
		}

		String value;

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/bank_name");
		if (value != null) {
			this.setAttribute("bank_name", value);
		}

		value = doc.getAttribute("/address_line_1");
		if (value != null) {
			this.setAttribute("address_line_1", value);
		}

		value = doc.getAttribute("/address_line_2");
		if (value != null) {
			this.setAttribute("address_line_2", value);
		}

		value = doc.getAttribute("/address_line_3");

		if (value != null) {
			this.setAttribute("address_line_3", value);
		}

		//IAZ IR-PMUK012737723 02/03/10 Begin
		value = doc.getAttribute("/bank_branch_code");

		if (value !=null) {
			this.setAttribute("bank_branch_code", value);
		}

		value = doc.getAttribute("/branch_name");

		if (value !=null) {
			this.setAttribute("branch_name", value);
		}

		value = doc.getAttribute("/country");

		if (value !=null) {
			this.setAttribute("country", value);
		}
		//IAZ IR-PMUK012737723 02/03/10 End

	}

	public void loadPaymentPartyBankFromDoc(DocumentHandler doc) {

		if (doc == null) {
			// If the document is null, there is nothing to populate
			return;
		}

		String value;
		String city, province;

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/bank_name");
		if (value == null)
			value = "";
		//NSX CR-573 07/08/10  Begin
		else  {
			if (value.length() > 35)
				//get first 35 characters
				value = value.substring(0, 35);
		}
		//NSX CR-573 07/08/10  End
		
		this.setAttribute("bank_name", value);

		value = doc.getAttribute("/bank_branch_code");
		if (value == null)
			value = "";
		this.setAttribute("bank_branch_code", value);

		value = doc.getAttribute("/branch_name");
		if (value == null)
			value = "";
		//NSX CR-573 07/08/10  Begin
		else {
			if (value.length() > 35)
				//get first 35 characters
				value = value.substring(0, 35);
		}
		//NSX CR-573 07/08/10  End
		
		this.setAttribute("branch_name", value);

		value = doc.getAttribute("/address_line_1");
		if (value == null)
			value = "";
		this.setAttribute("address_line_1", value);

		value = doc.getAttribute("/address_line_2");
		if (value == null)
			value = "";
		this.setAttribute("address_line_2", value);

		city = doc.getAttribute("/address_city");
		province = doc.getAttribute("/address_state_province");
		// bank branch rule does not have postalCode
		//postalCode = doc.getAttribute("/address_postal_code");
		
		if (province == null)
			province = "";
		//if (postalCode == null)
		//	postalCode = "";
		
		if (city == null) 
			city = "";
		//NSX CR-573 07/08/10  Begin
		else {
			int provinceLen = province.length();
			if (StringFunction.isNotBlank(province)) {
				provinceLen++;   // increase for space delimiter
			}
			
		   int len = 35 - provinceLen;	
		   if (city.length() > len)
			   city = city.substring(0, len);
		}
				
		// bank branch rule does not have postalCode
		//value = city + " " + province + " " + postalCode;
		
		value = city + " " + province;
		value = value.trim();
		//if (value == null)
			//value = "";
		//NSX CR-573 07/08/10  End
		
		this.setAttribute("address_line_3", value);

		value = doc.getAttribute("/address_country");
		if (value == null)
			value = "";
		this.setAttribute("country", value);
	}

	public void loadPaymentPartyBankFromDocTagsOnly(DocumentHandler doc) {

		if (doc == null) {
			// If the document is null, there is nothing to populate
			return;
		}

		String value;

		value = doc.getAttribute("/payment_party_oid");
		if (value != null && !value.equals("")) {
			this.setAttribute("payment_party_oid", value);
		}

		value = doc.getAttribute("/bank_name");
		if (value != null) {
			this.setAttribute("bank_name", value);
		}

		value = doc.getAttribute("/branch_name");
		if (value != null) {
			this.setAttribute("branch_name", value);
		}

		value = doc.getAttribute("/bank_branch_code");
		if (value != null) {
			this.setAttribute("bank_branch_code", value);
		}

		value = doc.getAttribute("/address_line_1");
		if (value != null) {
			this.setAttribute("address_line_1", value);
		}

		value = doc.getAttribute("/address_line_2");
		if (value != null) {
			this.setAttribute("address_line_2", value);
		}

		value = doc.getAttribute("/address_line_3");
		;
		if (value != null) {
			this.setAttribute("address_line_3", value);
		}

		value = doc.getAttribute("/country");

		if (value !=null) {
			this.setAttribute("country", value);
		}

	}
	//Vshah CR507 End

	public void clearPaymentPartyBank() {

			this.setAttribute("bank_name", "");
			this.setAttribute("branch_name", "");
			this.setAttribute("bank_branch_code", "");
			this.setAttribute("address_line_1", "");
			this.setAttribute("address_line_2", "");
			this.setAttribute("address_line_3", "");
			this.setAttribute("party_type", "");
			this.setAttribute("country", "");

	}

	//IAZ CR-586 IR-VRUK091652765 10/05/10 Begin

	public boolean isFixedValue (String propertyToCheck, boolean isTemplate) {

        if (isTemplate)
        	return false;
        LOG.debug("PPWB::source Template in isFixedValue {} [{}]" , sourcePartyWebBean ,this.getAttribute(propertyToCheck));
		if (StringFunction.isBlank(this.getAttribute(propertyToCheck)))
			return false;
		else if (sourcePartyWebBean == null)
			return false;
		else if (StringFunction.isBlank(sourcePartyWebBean.getAttribute(propertyToCheck)))
			return false;
		else
			return true;

	}
	//IAZ CR-586 IR-VRUK091652765 10/05/10 End
}
