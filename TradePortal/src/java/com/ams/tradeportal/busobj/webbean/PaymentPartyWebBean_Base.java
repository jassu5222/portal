



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A party of Payment instrument.  It should be a BankBranch.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentPartyWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* payment_party_oid - Object ID. */
       registerAttribute("payment_party_oid", "payment_party_oid", "ObjectIDAttribute");


       /* bank_name - Bank Name */
       registerAttribute("bank_name", "bank_name");


       /* bank_branch_code - Bank/Branch Code */
       registerAttribute("bank_branch_code", "bank_branch_code");


       /* branch_name - Branch Name */
       registerAttribute("branch_name", "branch_name");


       /* address_line_1 - Address Line 1 */
       registerAttribute("address_line_1", "address_line_1");


       /* address_line_2 - Address Line 2 */
       registerAttribute("address_line_2", "address_line_2");


       /* address_line_3 - Address Line 3 */
       registerAttribute("address_line_3", "address_line_3");


       /* address_line_4 - Address Line 4 */
       registerAttribute("address_line_4", "address_line_4");


       registerAttribute("party_type", "party_type");
	
	 registerAttribute("country", "country"); //IR - SWUK012840187
   }






}
