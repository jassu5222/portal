

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Payment Reporting Code 1
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentReportingCode1WebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* payment_reporting_code_1_oid - Unique Identifier */
       registerAttribute("payment_reporting_code_1_oid", "payment_reporting_code_1_oid", "ObjectIDAttribute");


       /* code - Reporting Code */
       registerAttribute("code", "code");


       /* short_description - Short Description */
       registerAttribute("short_description", "short_description");


       /* description - Description */
       registerAttribute("description", "description");


       /* locale_name - Locale of the language for description. */
       registerAttribute("locale_name", "locale_name");

      /* Pointer to the parent BankOrganizationGroup */
       registerAttribute("bank_group_oid", "p_bank_group_oid", "ParentIDAttribute");
   }
   
   




}
