



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * 
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentTemplateGroupWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* payment_template_group_oid - Unique identifier */
       registerAttribute("payment_template_group_oid", "payment_template_group_oid", "ObjectIDAttribute");

       /* name - Payment Template Group Name */
       registerAttribute("name", "name");

       /* description - Payment Template Group Description */
       registerAttribute("description", "description");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }






}
