

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * This object represents the running of a periodic task, such as purge or
 * sending daily email.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PeriodicTaskHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* periodic_task_history_oid - Unique identifier */
       registerAttribute("periodic_task_history_oid", "periodic_task_history_oid", "ObjectIDAttribute");


       /* run_timestamp - A timestamp (in GMT) of when the task was run */
       registerAttribute("run_timestamp", "run_timestamp", "DateTimeAttribute");


       /* task_type - The type of task that was run */
       registerAttribute("task_type", "task_type");
   }
   
   




}
