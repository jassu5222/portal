

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The history audit of Purchase Order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* purchase_order_history_oid - Unique Identifier */
       registerAttribute("purchase_order_history_oid", "purchase_order_history_oid", "ObjectIDAttribute");


       /* action_datetime - Timestamp (in GMT) when the action is taken. */
       registerAttribute("action_datetime", "action_datetime", "DateTimeAttribute");


       /* action - Action */
       registerAttribute("action", "action");


       /* status - Staus of Purchase Order. */
       registerAttribute("status", "status");

      /* Pointer to the parent PurchaseOrder */
       registerAttribute("purchase_order_oid", "p_purchase_order_oid", "ParentIDAttribute");

       /* user_oid -  */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
   }
   
   




}
