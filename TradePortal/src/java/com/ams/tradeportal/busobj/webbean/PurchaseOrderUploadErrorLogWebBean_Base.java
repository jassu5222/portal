

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The Error Logs of uploading Purchase Order File for each purchase order.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderUploadErrorLogWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* po_upload_error_log_oid - Unique identifier */
       registerAttribute("po_upload_error_log_oid", "po_upload_error_log_oid", "ObjectIDAttribute");


       /* ben_name - Beneficiary Info (Name, customer ref, amount) */
       registerAttribute("ben_name", "ben_name");


       /* error_msg - Error message. */
       registerAttribute("error_msg", "error_msg");


       /* po_number - The line number in the upload file. */
       registerAttribute("po_number", "po_number");

      /* Pointer to the parent PurchaseOrderFileUpload */
       registerAttribute("po_file_upload_oid", "p_po_file_upload_oid", "ParentIDAttribute");
   }
   
   




}
