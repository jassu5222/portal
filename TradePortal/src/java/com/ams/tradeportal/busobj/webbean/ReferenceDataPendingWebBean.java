package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.AmsEntityWebBean;

import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceDataPendingWebBean extends ReferenceDataPendingWebBean_Base
{
	private boolean readOnly = false;
	private DocumentHandler doc = null;

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public AmsEntityWebBean getReferenceData(AmsEntityWebBean refObj, String login_user_oid, boolean approveFlag) {
		
		String oid = refObj.getOid();
		String className = refObj.getEcsgObjectName();
		
		//TODO: skip all this if dual control is not enabled
		
	    this.isValid = true;
	    getDataFromAppServerFromCondtion("class_name like '%" + className + "%' and changed_object_oid =" + oid);
	    if (this.isValid()) {
	    	String changed_user_oid = getAttribute("change_user_oid");
	    	//if (login_user_oid.equals(changed_user_oid) || approveFlag == true) {
	    	if (approveFlag == true) {
	    		String xml = StringFunction.xssHtmlToChars(getAttribute("changed_object_data"));

	    		doc = new DocumentHandler(xml,false);
	    		refObj.populateFromXmlDoc(doc);
	    		setReadOnly(true);
	    	} else {
	    		//TODO: add warning error
	    	    setReadOnly(true);
	    	    refObj.getDataFromAppServer();
	    	}
	    } else {
	    	refObj.getDataFromAppServer();
	    }
		
		return refObj;
		
	}
	
//	public AmsEntityWebBean getReferenceDataComponent(AmsEntityWebBean refObj, String className) {
//	
//	}
	
}
