

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Abstract business object that is the superclass for reference data that
 * can be owned by organizations at multiple levels.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ReferenceDataWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* name - The name of the piece of reference data. */
       registerAttribute("name", "name");


       /* ownership_level - The level at which the reference data is owned (global, client bank, bank
         organization group, or corporate customer */
       registerAttribute("ownership_level", "ownership_level");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* ownership_type - This attribute is used to populate the "Added By" column of listviews. 
         If the ownership level is set to corporate customer, the ownership type
         is set to non-admin.  If ownership level is set to anything else, the ownership
         type is admin. */
       registerAttribute("ownership_type", "ownership_type");
   }
   
   




}
