/**
 * 
 */
package com.ams.tradeportal.busobj.webbean;

/**

 *
 */
public class SPEmailNotificationsWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{
	public void registerAttributes()
	 {  
		 super.registerAttributes();

		 /* sp_email_oid primary key attribute for SPEmailNotifications */
		 registerAttribute("sp_email_oid", "sp_email_oid", "ObjectIDAttribute");
	      
		  /*  next_sp_email_due- A datetime (in GMT)  when next email is due */
	      registerAttribute("next_sp_email_due", "next_sp_email_due", "DateTimeAttribute");
	      
	      /* Pointer to the CorporateOrganization */
	      registerAttribute("p_corp_organization_oid", "p_corp_organization_oid", "NumberAttribute");
	      
	      /* Pointer to the parent NotificationRule */
	      registerAttribute("p_notification_rule_oid", "p_notification_rule_oid", "NumberAttribute");
		
		
	 }
	

}
