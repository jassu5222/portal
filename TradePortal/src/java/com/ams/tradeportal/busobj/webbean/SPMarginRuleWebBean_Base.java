

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Supplier Portal Margin Rule for a Corporate Customer.  These are the margins
 * used to calculate the invoice offer's net amount offer.  See InvoicesSummaryDate.calculateSupplierPortalDiscount().
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SPMarginRuleWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* sp_margin_rule_oid - Unique identifier */
       registerAttribute("sp_margin_rule_oid", "sp_margin_rule_oid", "ObjectIDAttribute");


       /* currency - A matching crierion.  Matches the currency of the invoice. */
       registerAttribute("currency", "currency");


       /* threshold_amt - A matching criterian.  Matches the invoice whose amount is less than this
         threshold amount. */
       registerAttribute("threshold_amt", "threshold_amt", "TradePortalDecimalAttribute");


       /* rate_type - Matching result.  Used to get interest_discount_rate by matching its rate_type. */
       registerAttribute("rate_type", "rate_type");


       /* margin - Matching result.  The margin over the interest discount rate. */
       registerAttribute("margin", "margin", "TradePortalDecimalAttribute");

      /* Pointer to the parent CorporateOrganization */
       registerAttribute("owner_oid", "p_owner_oid", "ParentIDAttribute");
   }
   
   




}
