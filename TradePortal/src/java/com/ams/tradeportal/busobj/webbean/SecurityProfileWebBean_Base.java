

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * Business object to store the Security Profile for a user.  A security profile
 * describes the rights and privledges that a user has within the Trade Portal.
 * These rights are stored in the security_rights attributes.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class SecurityProfileWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* security_profile_oid - Unique identifier */
       registerAttribute("security_profile_oid", "security_profile_oid", "ObjectIDAttribute");


       /* security_profile_type - Indicates whether this security profile is an admin profile (describing
         rights and privledges for admin users) or a regular profile (used by corporate
         customers and by admin users acting on behalf of corporate customers). */
       registerAttribute("security_profile_type", "security_profile_type");


       /* security_rights - Contains the security rights for the user.   The value stored in this field
         will be the string representation of a number.  This number can be converted
         into a string of 1s and 0s.  Each of these bits (a 1 or 0) represents whether
         a right is allowed or disallowed.
         
         For more information on how this attribute is used, see the SecurityAccess
         class. */
       registerAttribute("security_rights", "security_rights");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
   }
   
   




}
