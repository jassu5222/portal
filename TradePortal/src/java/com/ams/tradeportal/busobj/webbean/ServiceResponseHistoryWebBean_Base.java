

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * ServiceResponseHistory is used for tracking the Service Request sent from
 * Tradeportal to an external system.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ServiceResponseHistoryWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* service_response_hist_oid - Unique identifier */
       registerAttribute("service_response_hist_oid", "service_response_hist_oid", "ObjectIDAttribute");


       /* correlation_id - correlation_id sent in the request */
       registerAttribute("correlation_id", "correlation_id");


       /* time_sent - Timestamp in GMT of when the request was sent */
       registerAttribute("time_sent", "time_sent", "DateTimeAttribute");


       /* time_received - Timestamp in GMT of when the response was received */
       registerAttribute("time_received", "time_received", "DateTimeAttribute");


       /* status - Lifecycle status of the message. */
       registerAttribute("status", "status");


       /* message_id - message_id sent in the request xml */
       registerAttribute("message_id", "message_id");


       /* request_msg - Stores a copy of the data sent in a bank request. */
       registerAttribute("request_msg", "request_msg");


       /* response_msg - Stores a copy of the data received from  a bank request. */
       registerAttribute("response_msg", "response_msg");


       /* msg_type - The type of message to be processed.  This indicates what data is contained
         in the XML */
       registerAttribute("msg_type", "msg_type");

       /* transaction_oid - The transaction which initiated the request */
       registerAttribute("transaction_oid", "a_transaction_oid", "NumberAttribute");

       /* user_oid - The user initiated the request */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
   }
   
   




}
