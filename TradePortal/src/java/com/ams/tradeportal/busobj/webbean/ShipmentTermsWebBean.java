

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ShipmentTermsWebBean extends ShipmentTermsWebBean_Base
{
  DocumentHandler shipmentTermsDoc  = null;
  Vector          shipmentTermsList = null;
  int             index             = 0;
  private static final Logger LOG = LoggerFactory.getLogger(ShipmentTermsWebBean.class);
  
  public int loadShipmentTerms(String termsOid, int shipmentNum) 
  {

    // This method loads data into the web bean by retriving the shipment
    // terms from the database using term oid.  The shipment terms are then
    // ordered by creation date which allows us to use the shipment number
    // argument to find the applicable shipment in the list.
    DocumentHandler shipmentTermsResults = null;
    String	    shipmentOid          = null;
    String   sqlQuery             = null;
    int             numShipments         = 1;

    try {
      sqlQuery = "select shipment_oid, creation_date from shipment_terms where p_terms_oid = ? order by creation_date";

      shipmentTermsResults = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{termsOid});

      if (shipmentTermsResults != null)
      {
        shipmentTermsList = shipmentTermsResults.getFragments("/ResultSetRecord");
        numShipments      = shipmentTermsList.size();
        shipmentNum--;
        index             = shipmentNum;
      
        shipmentTermsDoc = (DocumentHandler) shipmentTermsList.elementAt(index);
        shipmentOid = shipmentTermsDoc.getAttribute("/SHIPMENT_OID");
        this.setAttribute("shipment_oid", shipmentOid);
      }
    } catch (Exception e) {
    	LOG.error("ShipmentTermsWebBean: Exception while loading shipment terms: ",e);
    }
    return numShipments;
  }

  public void copy(ShipmentTermsWebBean source)
  {
    // This method takes a shipment terms object and copies its data
    // to a new object
    Vector nameValueArrays = source.getAttributeNameValues();

    // Get the attributes from the source shipment terms and use them to set
    // the attributes on the new shipment terms
    String [] name = (String []) nameValueArrays.elementAt(0);
    String [] value = (String []) nameValueArrays.elementAt(1);

    this.setAttributes(name, value);
    
    // Set the attributes we don't want copied to an empty string
    // Description, latest ship date, goods desc, po's
    this.setAttribute("description", "");
    this.setAttribute("latest_shipment_date", "");
    this.setAttribute("goods_description", "");
    this.setAttribute("po_line_items", "");    
  }

  public Vector retrieveAssignedPOLineItems() 
  {

    // This method retrieves all manually entered PO Line Items 
    // that have been added to a particular shipment
    DocumentHandler poLineItemResults    = null;
    String    sqlQuery             = null;
    Vector          poLineItemList       = null;

    try {
      sqlQuery = "select po_line_item_oid, creation_date_time from po_line_item where p_shipment_oid = ? order by creation_date_time, po_line_item_oid";

      poLineItemResults = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{this.getAttribute("shipment_oid")});

      if (poLineItemResults != null)
      {
        poLineItemList = poLineItemResults.getFragments("/ResultSetRecord");
      }
    } catch (Exception e) {
    	LOG.error("ShipmentTermsWebBean: Exception while copying shipment terms: ",e);
    }
    return poLineItemList;
  } 

}
