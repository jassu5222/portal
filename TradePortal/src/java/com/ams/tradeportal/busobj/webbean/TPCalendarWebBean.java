package com.ams.tradeportal.busobj.webbean;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarWebBean extends TPCalendarWebBean_Base
{
	private static final Logger LOG = LoggerFactory.getLogger(TPCalendarWebBean.class);
   private Map calYearMap = new TreeMap();	
   
   private static final String CLEAN_DAYS_IN_YEAR = getCleanDaysInYear();
   
   /*
    * Generates empty daysInYear string 
    */
   private static String getCleanDaysInYear() {
	   StringBuffer sb = new StringBuffer(366);
	   for (int i=1; i <= 366; i++) {
		   sb.append("0");
	   }
	   return sb.toString();
   }
   
   /*
    * creates TPCalendarYearWebBean list from request 
    */
   public void populateCalendarYear(HttpServletRequest request)  {
	   int counter=0;
	   TPCalendarYearWebBean calYear = null;
	   while (StringFunction.isNotBlank(request.getParameter ("_year"+counter))) {
   	      calYear = beanMgr.createBean(TPCalendarYearWebBean.class, "TPCalendarYearWebBean");
   	      calYear.setAttribute("tp_calendar_year_oid", request.getParameter ("_calendar_id"+counter));
		  calYear.setAttribute("year", request.getParameter ("_year"+counter));
		  calYear.setAttribute("days_in_year", request.getParameter ("_days_in_year"+counter));
		  calYearMap.put(request.getParameter ("_year"+counter),calYear);
		  counter++;
	   }
   }

   /*
    * Retrieves TPCalendarYearWebBean list from database 
    */
   public void retrieveCalendarYear(String serverLocation, ResourceManager resMgr) throws RemoteException, AmsException {
	  
	  if (StringFunction.isBlank(getAttribute("tp_calendar_oid"))) return;  //nothing to retrieve
	  String sql = null;  
	  QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(serverLocation, "QueryListView");
	  sql = "select tp_calendar_year_oid, year, days_in_year from TP_CALENDAR_YEAR where p_tp_calendar_oid = ? order by "+resMgr.localizeOrderBy("year");
	  LOG.debug("calendarYear sql: {}",sql);
	
	  queryListView.setSQL(sql, new Object[]{getAttribute("tp_calendar_oid")});
	  queryListView.getRecords();
	
	  DocumentHandler calendarYearList = new DocumentHandler();
	  calendarYearList = queryListView.getXmlResultSet();
	  LOG.debug("calendarYearList: {}",calendarYearList.toString());
	  
	  Vector vVector = calendarYearList.getFragments("/ResultSetRecord");
	  DocumentHandler aDoc = null;
	  int numItems = vVector.size();
	  TPCalendarYearWebBean calYear = null;
	  for (int iLoop=0;iLoop<numItems;iLoop++) {
		  calYear = beanMgr.createBean(TPCalendarYearWebBean.class, "TPCalendarYearWebBean");
		  aDoc = (DocumentHandler) vVector.elementAt(iLoop);
		  calYear.setAttribute("tp_calendar_year_oid", aDoc.getAttribute("/TP_CALENDAR_YEAR_OID"));
		  calYear.setAttribute("year", aDoc.getAttribute("/YEAR"));
		  calYear.setAttribute("days_in_year", aDoc.getAttribute("/DAYS_IN_YEAR"));
		  calYearMap.put(aDoc.getAttribute("/YEAR"),calYear);
	  }	
   }

   private TPCalendarYearWebBean createNewCalendarYear(String year) {
	   TPCalendarYearWebBean calYear = beanMgr.createBean(TPCalendarYearWebBean.class, "TPCalendarYearWebBean");
	   calYear.setAttribute("year",year);
	   calYear.setAttribute("days_in_year",CLEAN_DAYS_IN_YEAR);
	   return calYear;
   }
	//Srinivasu_D IR#T36000041583 Rel9.4 09/09/2015  - Start
	public String getRetrievedCalendarYear(String year){
	   
	   
	   String key = null;
	   String value= null;
	   TPCalendarYearWebBean calYear = null;
	   if(calYearMap!=null && calYearMap.size()>0){
		   
		   Set st = calYearMap.keySet();
		   Iterator it = st.iterator();
		   while(it.hasNext()){
			   key =(String) it.next();
			  calYear =(TPCalendarYearWebBean) calYearMap.get(key);
			  value = calYear.getAttribute("year");
			  if(!year.equals(value)){
				  return value;
			 }
		   }
		   
	   }
	   
	   return value;
   }
   //Srinivasu_D IR#T36000041583 Rel9.4 09/09/2015  - End

   /*
    * Returns TPCalendarYearWebBean for given year. If the object does not exists, it creates a new object. 
    */
   public TPCalendarYearWebBean getCalendarYear(String year) {
	   TPCalendarYearWebBean calYear = null;
	   if (calYearMap.containsKey(year)) {
		   calYear = (TPCalendarYearWebBean)calYearMap.get(year);
	   }
	   else {
		   calYear = createNewCalendarYear(year);   
		   calYearMap.put(year, calYear);
	   }
	   return calYear;
   }
   
   /*
    * Returns TPCalendarYearWebBean map 
    */
   public Map getCalendarYearComponentsMap() {
	  return calYearMap;
   }
}
