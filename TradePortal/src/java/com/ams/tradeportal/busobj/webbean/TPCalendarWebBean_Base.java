

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The business calendar that stores the holiday information.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* tp_calendar_oid - Unique Identifier. */
       registerAttribute("tp_calendar_oid", "tp_calendar_oid", "ObjectIDAttribute");


       /* opt_lock - Optimistic lock attribute
         See jPylon documentation for details on how this works */
       registerAttribute("opt_lock", "opt_lock", "OptimisticLockAttribute");


       /* calendar_name - Calendar Name */
       registerAttribute("calendar_name", "calendar_name");


       /* weekend_day_1 - The first weekend day.  e.g. java.util.Calendar.SATURDAY.
 */
       registerAttribute("weekend_day_1", "weekend_day_1", "NumberAttribute");


       /* weekend_day_2 - The second weekend day.  e.g. java.util.Calendar.SUNDAY. */
       registerAttribute("weekend_day_2", "weekend_day_2", "NumberAttribute");


       /* creation_date - The date when the calendar is created. */
       registerAttribute("creation_date", "creation_date", "DateAttribute");
   }
   
   




}
