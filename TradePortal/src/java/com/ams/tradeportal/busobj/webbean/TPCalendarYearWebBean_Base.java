

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The holiday informatino for any given year of a TPCalendar.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TPCalendarYearWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* tp_calendar_year_oid - Unique Identifier. */
       registerAttribute("tp_calendar_year_oid", "tp_calendar_year_oid", "ObjectIDAttribute");


       /* year - The year e.g. 2010 */
       registerAttribute("year", "year", "NumberAttribute");


       /* days_in_year - A 366-character string that represents the days in a year.  Each character
         represents one day.  
         
         0 - workday
         1 - holiday */
       registerAttribute("days_in_year", "days_in_year");

      /* Pointer to the parent TPCalendar */
       registerAttribute("tp_calendar_oid", "p_tp_calendar_oid", "ParentIDAttribute");
   }
   
   




}
