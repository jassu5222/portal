



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;
import com.ams.tradeportal.html.*;

/**
 * Represents an instrument template.  The data for an instrument template
 * is stored in the TemplateInstrument business object.  This business object
 * contains the characteristics of the template from a reference data point
 * of view (who owns it, its name, whether or not it is express), where as
 * the TemplateInstrument actually contains the data that comprises the template.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TemplateWebBean_Base extends ReferenceDataWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


       /* template_oid - Unique identifier */
       registerAttribute("template_oid", "template_oid", "ObjectIDAttribute");


       /* express_flag - Indicates whether or not the template is an express template.   When instruments
         are created from express templates, certain fields are read-only.  This
         is to allow for straight-through processing of LCs without human involvement.

         Yes=template is an express template */
       registerAttribute("express_flag", "express_flag", "IndicatorAttribute");


       /* default_template_flag - Indicates whether or not a default template exists.  A default template
         is the template that is used when the user indicates that they want to create
         a transaction from a "blank form".

         Yes = this template is a default template */
       registerAttribute("default_template_flag", "default_template_flag", "IndicatorAttribute");

      /* Pointer to the parent ReferenceDataOwner */
       registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");

       /* TemplateInstrument - The instrument data that represents the real data for the template.  This
         association is used to get from the name of a template to the data that
         is being copied from. */
       registerAttribute("c_TemplateInstrument", "c_TEMPLATE_INSTR_OID", "NumberAttribute");

       /* fixed_flag - Indicates whether or not the template is a fixed payments template.

         Yes=template is a fixed payments template */
       registerAttribute("fixed_flag", "fixed_flag", "IndicatorAttribute");

       /* confidential_indicator - Indicates whether or not the template is confidential.

         Yes=template is a confidential template */
       registerAttribute("confidential_indicator", "confidential_indicator");

     /* Payment_Templ_Grp_OID - Payment Template Group */
      registerAttribute("payment_templ_grp_oid", "Payment_Templ_Grp_OID", "NumberAttribute");
   }






}
