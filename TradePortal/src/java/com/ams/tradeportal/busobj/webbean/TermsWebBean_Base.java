



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The contractual terms that comprise a transaction.  Data entered by the
 * user on the transaction forms pages will be stored in the Terms business
 * object.
 *
 * Attributes for this business object are dynamically registered.  That is,
 * all of the attributes defined here are not registered for all instances.
 * Terms objects have a terms manager object associated with them.  The terms
 * manager object determines which attributes should be registered and handles
 * any instance-specific method calls.
 *
 * For more information, see the "Terms Manager Delegation" section of the
 * Trade Portal Infrastructure Document.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TermsWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();

       /* FirstTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_FirstTermsParty", "c_FIRST_TERMS_PARTY_OID", "NumberAttribute");

       /* SecondTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_SecondTermsParty", "c_SECOND_TERMS_PARTY_OID", "NumberAttribute");

       /* ThirdTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_ThirdTermsParty", "c_THIRD_TERMS_PARTY_OID", "NumberAttribute");

       /* FourthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_FourthTermsParty", "c_FOURTH_TERMS_PARTY_OID", "NumberAttribute");

       /* FifthTermsParty - A Terms business object can be associated to one to six terms parties.
         Instead of creating a one-to-many association, six one-to-one assocaitoins
         were created to make coding simpler.  Within these six terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_FifthTermsParty", "c_FIFTH_TERMS_PARTY_OID", "NumberAttribute");

       /* SixthTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_SixthTermsParty", "c_SIXTH_TERMS_PARTY_OID", "NumberAttribute");

       /* SeventhTermsParty - A Terms business object can be associated to one to seven terms parties.
         Instead of creating a one-to-many association, seven one-to-one assocaitoins
         were created to make coding simpler.  Within these seven terms parties for
         any Terms object, the terms party type of each terms party will be unique.


         On the JSPs, a certain order of terms parties is assumed.  For example,
         the first terms party on an Issue Import LC is the Applicant.   On a standby,
         it might be the advising bank.   See each JSP for specifics.   The proper
         terms party can always be retrieved by examining the terms party type.

         For bank released terms, the applicant is the first terms party and the
         beneficiary is the second terms party. */
       registerAttribute("c_SeventhTermsParty", "c_SEVENTH_TERMS_PARTY_OID", "NumberAttribute");

       /* OrderingParty - Ordering Party for a CM instrument.

         Note this is using the physical column c_first_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
       registerAttribute("c_OrderingParty", "c_ORDERING_PARTY_OID", "NumberAttribute");

       /* OrderingPartyBank - Ordering Party Bank for a CM instrument.

         Note this is using the physical column c_second_terms_party_oid to store
         the component association.  A Terms has either Terms Party component or
         Payment Party component, depending on the instrument type. */
       registerAttribute("c_OrderingPartyBank", "c_ORDERING_PARTY_BANK_OID", "NumberAttribute");
       
       registerAttribute("individual_debit_ind", "individual_debit_ind", "IndicatorAttribute");
       registerAttribute("request_market_rate_ind", "request_market_rate_ind", "IndicatorAttribute"); // DK CR-640 Rel7.1
       
	registerDynamicAttributes();

   }

     /**
      * This method is overriden in the actual Bean class, where it
      * handles registration of dynamic attributes
      */
     protected void registerDynamicAttributes()
      {

      }






}
