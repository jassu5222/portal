

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * The alias for the AR Buyer ID in an ARMatchingRule
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePartnerMarginRulesWebBean_Base extends com.amsinc.ecsg.web.AmsEntityWebBean
{

  public void registerAttributes()
   {  


       /* ar_buyer_id_alias_oid - Unique Identifier */
       registerAttribute("trade_margin_rule_oid", "trade_margin_rule_oid", "ObjectIDAttribute");


       /* Pointer to the parent ArMatchingRule */
       registerAttribute("ar_matching_rule_oid", "p_ar_matching_rule_oid", "ParentIDAttribute");

		registerAttribute("margin_instrument_type", "margin_instrument_type");
	   
	    registerAttribute("currency_code", "currency_code");

		registerAttribute("threshold_amount", "threshold_amount","TradePortalDecimalAttribute");

		registerAttribute("rate_type", "rate_type");

		registerAttribute("margin", "margin","TradePortalDecimalAttribute");
   }
   
   




}
