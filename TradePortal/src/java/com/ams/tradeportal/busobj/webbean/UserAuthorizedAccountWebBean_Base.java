

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * Accounts associated to the User, where the User is authorized to transfer
 * funds.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserAuthorizedAccountWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* authorized_account_oid - Unique Identifier */
       registerAttribute("authorized_account_oid", "authorized_account_oid", "ObjectIDAttribute");


       /* account_description - the user can enter a description or "nickname' for the account. */
       registerAttribute("account_description", "account_description");

      /* Pointer to the parent User */
       registerAttribute("user_oid", "p_user_oid", "ParentIDAttribute");

       /* account_oid -  */
       registerAttribute("account_oid", "a_account_oid", "NumberAttribute");
   }
   
   




}
