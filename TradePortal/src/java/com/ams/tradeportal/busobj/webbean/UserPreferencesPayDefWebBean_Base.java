



package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * A UserPreferencesPayDef consists of a preferences 
 * of user.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserPreferencesPayDefWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {
      super.registerAttributes();


      registerAttribute("user_pref_pay_def_oid", "user_pref_pay_def_oid", "ObjectIDAttribute");
      registerAttribute("owner_org_oid", "p_owner_org_oid", "ParentIDAttribute");
	  registerAttribute("a_pay_upload_definition_oid", "a_pay_upload_definition_oid");
	  registerAttribute("pmt_file_default_ind", "pmt_file_default_ind", "IndicatorAttribute"); 
   }






}
