

  

package com.ams.tradeportal.busobj.webbean;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import javax.servlet.http.*;

/**
 * When a user logs out, a history record is created that represents their
 * session.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UserSessionHistoryWebBean_Base extends TradePortalBusinessObjectWebBean
{

  public void registerAttributes()
   {  
      super.registerAttributes();


       /* session_history_oid - Object identifier of this record */
       registerAttribute("session_history_oid", "session_history_oid", "ObjectIDAttribute");


       /* login_timestamp - The date and time (stored in the database in the GMT timezone) when the
         ActiveUserSession record was created for the user's session. */
       registerAttribute("login_timestamp", "login_timestamp", "DateTimeAttribute");


       /* server_instance_name - The name of the server instance to which the user logged in. */
       registerAttribute("server_instance_name", "server_instance_name");


       /* logout_timestamp - The date and time (stored in the database in the GMT timezone) at which
         the user's session was logged out. */
       registerAttribute("logout_timestamp", "logout_timestamp", "DateTimeAttribute");

       /* user_oid -  */
       registerAttribute("user_oid", "a_user_oid", "NumberAttribute");
   }
   
   




}
