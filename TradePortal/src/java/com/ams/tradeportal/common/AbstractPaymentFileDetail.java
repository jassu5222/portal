package com.ams.tradeportal.common;
import java.rmi.RemoteException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

public abstract class AbstractPaymentFileDetail {
private static final Logger LOG = LoggerFactory.getLogger(AbstractPaymentFileDetail.class);

    public static final int MAX_FILE_NAME_SIZE = 40;

    public static final String FILE_EXTENSION = ".txt";

    public static final String paymentDataDelimiter = "\\|";

    /**
     * Verifies the Payment Line Item. Doing the validation here instead of at
     * the business object user_validate() gives better error message
     * information.
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid Payment Data file
     * (true - valid false - invalid)
     */
    public static boolean validatePaymentLineItemReqFields(String paymentMethod, String paymentCurrency, String debitAccountNo, int paymentCount, ResourceManager resourceManager, MediatorServices mediatorServices)
            throws AmsException {

        boolean invalidFormat = false;
        String[] substitutionValues = {"", ""};

        //Payment Method
        if (paymentMethod == null || paymentMethod.trim().equals("")) {
            substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.PaymentMethod", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields [" + substitutionValues + "]");
            invalidFormat = true;
        }

        //Payment Currency
        if (paymentCurrency == null || paymentCurrency.trim().equals("")) {
            substitutionValues[0] = //"Payment Currency";
                    resourceManager.getText("DomesticPaymentRequest.PaymentCurrency", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }//manohar - 06/06/2011 - RAUL052433703 - Begin
        else {
            try {
                int decimals = Integer.parseInt(ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, paymentCurrency));
            } catch (AmsException e) {
				// If the currency is not a valid currency, there will be no hashtable
                // entry and we get a number format exception.  Set decimals to -1;
                String[] substitutionValue = {""};
                substitutionValue[0] = paymentCurrency;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CURRENCY, substitutionValue, null);
                LOG.debug("PaymentFileUploadServlet: Invalid Currency : " + substitutionValue);
                invalidFormat = true;
            }
        }//manohar - 06/06/2011 - RAUL052433703 - End

        //Debit Account
        if (debitAccountNo == null || debitAccountNo.trim().equals("")) {
            substitutionValues[0] = //"Debit Account No";
                    resourceManager.getText("DomesticPaymentRequest.DebitFrom", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        return invalidFormat;

    }

    /**
     * CR-921 (added fx contract and fx rate to be validated as part of the
     * header validations
     *
     * @deprecated Use
     * {@link #validateHeader(String,String,String,String,String,String,int,ResourceManager,SessionWebBean,MediatorServices,String,String)}
     * instead
     */
    public static boolean validateHeader(String debitAccountNo, String paymentCurrency, String paymentMethod, String currency, String confidentialInd, String executionDate, int paymentCount, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices) throws AmsException, NumberFormatException, RemoteException {
        return validateHeader(debitAccountNo, paymentCurrency, paymentMethod,
                currency, confidentialInd, executionDate, paymentCount,
                resourceManager, userSession, mediatorServices, null,
                null);
    }

    public static boolean validateHeader(String debitAccountNo, String paymentCurrency, String paymentMethod, String currency, String confidentialInd, String executionDate, int paymentCount, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices, String fxContractNumber, String fxRate) throws AmsException, NumberFormatException, RemoteException {
        return validateHeader(debitAccountNo, paymentCurrency, paymentMethod, currency, confidentialInd, executionDate, paymentCount, resourceManager, userSession, mediatorServices, false, fxContractNumber, fxRate);//CR-921
    }

    /**
     * CR-921
     *
     * @deprecated Use
     * {@link #validateHeader(String,String,String,String,String,String,int,ResourceManager,SessionWebBean,MediatorServices,boolean,String,String)}
     * instead
     */
    public static boolean validateHeader(String debitAccountNo, String paymentCurrency, String paymentMethod, String currency, String confidentialInd, String executionDate, int paymentCount, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices, boolean isGXS) throws AmsException, NumberFormatException, RemoteException {
        return validateHeader(debitAccountNo, paymentCurrency, paymentMethod,
                currency, confidentialInd, executionDate, paymentCount,
                resourceManager, userSession, mediatorServices, isGXS,
                null, null);
    }

    public static boolean validateHeader(String debitAccountNo, String paymentCurrency, String paymentMethod, String currency, String confidentialInd, String executionDate, int paymentCount, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices, boolean isGXS, String fxContractNumber, String fxRate) throws AmsException, NumberFormatException, RemoteException {
        boolean invalidFormat = false;
        String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        String[] substitutionValues = {"", ""};
        //manohar 05/27/2011 - PAUL051971162 - Begin
        String userOid = userSession.getUserOid();
        User user = (User) EJBObjectFactory.createClientEJB(serverLocation, "User", Long.parseLong(userOid), resourceManager.getCSDB());

        if (StringFunction.isNotBlank(executionDate)) {
            Date execDate = TPDateTimeUtility.convertDateStringToDate(executionDate.trim(), "dd/MM/yyyy");
            if (execDate == null) {
                invalidFormat = true;
                substitutionValues[0] = String.valueOf(paymentCount + 1);
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_DATE, substitutionValues, null);
            } else if (TPDateTimeUtility.isBackDated(execDate, user.getAttribute("timezone"))) {
                invalidFormat = true;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DOM_PMT_DATE_CANT_BE_BACKDATED);
            }
        }//manohar 05/27/2011 - PAUL051971162 - End

        debitAccountNo = debitAccountNo.trim();
        LOG.debug("PaymentFileUploadServlet: usp oid " + userSession.getOwnerOrgOid());
        String accountSqlStr = "select account_oid, p_owner_oid, currency, deactivate_indicator, available_for_domestic_pymts from account where account_number = ? and p_owner_oid = ? ";

        DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, debitAccountNo, userSession.getOwnerOrgOid());

        if ((acctResultSet == null) || (StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID")))) {
            invalidFormat = true;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_DEBIT_AC);
        } else if (TradePortalConstants.INDICATOR_YES.equals(acctResultSet.getAttribute("/ResultSetRecord(0)/DEACTIVATE_INDICATOR"))) {
            invalidFormat = true;
            String[] substValues = new String[1];
            substValues[0] = debitAccountNo + "(" + paymentCurrency + ")";
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ACCOUNT_DEACTIVATED, substValues);
        } else if (!TradePortalConstants.INDICATOR_YES.equals(acctResultSet.getAttribute("/ResultSetRecord(0)/AVAILABLE_FOR_DOMESTIC_PYMTS"))) {
            invalidFormat = true;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AC_NOTAVAIL_FOR_DPT);
        } else {
            //on behalf of a Subsidiary while using a Subsidiary Access mode.
            if (!userSession.hasSavedUserSession()) {
                String debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID");
                String selectClause = "a_account_oid";
                String fromClause = "USER_AUTHORIZED_ACCT";
                int accountCount = DatabaseQueryBean.getCount(selectClause, fromClause, " p_user_oid = ? and a_account_oid = ? ", false, userSession.getUserOid(), debitAccountOid);
                String userSecurityType = null;

                userSecurityType = userSession.getSecurityType();

                if (accountCount == 0 && !userSecurityType.equals(TradePortalConstants.ADMIN) && !isGXS) {
                    invalidFormat = true;
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AC_NOTVALID_FOR_USER);
                }
            }
        }

        String sqlSelect = "select count (*) as acount from refdata where table_type = ? and code = ? and upper(addl_value) <> ?";
        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[]{"PAYMENT_METHOD", paymentMethod, "COLLECTION"});
        String aReqCount = resultSet.getAttribute("/ResultSetRecord/ACOUNT");

        if (StringFunction.isBlank(aReqCount) || aReqCount.equals("0")) {
            substitutionValues[0] = String.valueOf(paymentCount + 1);
            substitutionValues[1] = paymentMethod;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DP_FILE_UPLD_INVLD_PMT_MTH_CDE, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Payment Method [" + substitutionValues + "] not in RefData [" + aReqCount + "]");
            invalidFormat = true;
        }

        if (TradePortalConstants.INDICATOR_YES.equals(confidentialInd) && !isGXS) {
		  	 if (!TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("confidential_indicator"))) {
                LOG.debug("PaymentFileUploadServlet: User doesn't have rights to upload Confidential Payment Files");
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CANNOT_UPLAOD_CONFIDENTIAL_PAYMENT_FILES);

                invalidFormat = true;
            }
        }
		//Rel 9.0 CR-921
        //FX Contract Number and Rate Validations
        //Validate that both FX Contract Number and FX Rate are either both missing or both available
        //
        invalidFormat = validateFXInfo(fxRate, fxContractNumber, mediatorServices);

        return invalidFormat;
    }
	//Rel 9.0 CR-921
    //FX Contract Number and Rate Validations
    //Validate that both FX Contract Number and FX Rate are either both missing or both available
    //

    protected static boolean validateFXInfo(String fxRate, String fxContractNumber, MediatorServices mediatorServices) throws AmsException {
        boolean invalidFormat = false;
        if (StringFunction.isNotBlank(fxContractNumber) && StringFunction.isBlank(fxRate)) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_RATE_VALUE_MISSING);
            invalidFormat = true;
        } else if (StringFunction.isNotBlank(fxRate) && StringFunction.isBlank(fxContractNumber)) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_CONTRACT_VALUE_MISSING);
            invalidFormat = true;
        } else if (StringFunction.isNotBlank(fxRate) && StringFunction.isNotBlank(fxContractNumber)) {
            //both are available, check the format
            boolean invalidFXRate = false;
            boolean invalidFXContractNumber = false;
			String[] temp = null;

            //T36000029918 - Validate FXRate format 
            temp = fxRate.split("\\.");
            if (temp.length < 3) {
                if (temp[0].length() > 0) {
                    if (InstrumentServices.containsOnlyNumbers(temp[0])) {
                        if (temp[0].length() > 5) {
                            invalidFXRate = true;
                        }
                    } else {
                        invalidFXRate = true;
                    }

                }
                if ((temp.length > 1)) {
                    if (InstrumentServices.containsOnlyNumbers(temp[1])) {
                        if (temp[1].length() > 8) {
                            invalidFXRate = true;
                        }
                    } else {
                        invalidFXRate = true;
                    }
                }

            } else {
                invalidFXRate = true;
            }
         
            //check length
            if (!(fxContractNumber.length() < 15)) {
                invalidFXContractNumber = true; //MEer Rel 9.0 IR-29096 -Show Correct Error message when FXContNumber length exceeds
            }
            //fx contract is alphanumeric
            if (!StringFunction.isBlank(InstrumentServices.isAlphaNumeric(fxContractNumber, false))) {
                invalidFXContractNumber = true;
            }
			
            //If 'FX Rate' is in an invalid format or if FX Contract number exceeded maximum length
            if (invalidFXRate) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_CONTRACT_INVALID_FORMAT);
                invalidFormat = true;
            }
            //MEer Rel 9.0 IR-29096-if FX Contract Number exceeded maximum length or is not alphanumeric
            if (invalidFXContractNumber) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FX_CONTRACT_SIZE_EXCEEDED);
                invalidFormat = true;
            }

        }

        return invalidFormat;

    }

}
