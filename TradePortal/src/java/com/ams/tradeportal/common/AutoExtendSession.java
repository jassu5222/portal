package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

/**
 * This class provides a static method which checks if Portal session needs to
 * be Extended or not. This is check is performed by the javascript based
 * session timeout logic.
 *

 *
 */
public class AutoExtendSession {
private static final Logger LOG = LoggerFactory.getLogger(AutoExtendSession.class);

    /**
     * This method is called to enquire about the new session timeout value. the
     * new timeout value is the difference between the Default TImeout In
     * seconds and (current time in milliseconds - session.lastaccessedtime).
     *
     * @param httpSession
     * @return newtimeoutValue by which the javascript session timeout timer
     * should be initialized in case the server timeout is already happened we
     * send back 0 as new time out value some number greater than 0.
     */
    public String checkIfAutoExtendSession(HttpSession httpSession) {
        LOG.info("CheckSessionTimeOutServlet called at : ====> " + (new java.util.Date()).toGMTString());
        LOG.info("Session ID in CheckSessionTimeOutServlet: ====> " + httpSession.getId());
        LOG.info("Last Accessed Time  in CheckSessionTimeOutServlet: ====> " + httpSession.getLastAccessedTime());
        LOG.info("Current System Time in Milli seconds  in CheckSessionTimeOutServlet: ====> " + System.currentTimeMillis());
        LOG.info("Session Max In Active Interval seconds  in CheckSessionTimeOutServlet: ====> " + httpSession.getMaxInactiveInterval());

        String newTimeoutValue = "0";
        int maxInactiveTimeOut = SecurityRules.getTimeOutPeriodInSeconds();
        int sessionInActiveInterval = httpSession.getMaxInactiveInterval();

        if (sessionInActiveInterval < maxInactiveTimeOut) {
            maxInactiveTimeOut = sessionInActiveInterval;
        }

        String savedSessionId = httpSession.getAttribute("currentSessionId") == null ? null : (String) httpSession.getAttribute("currentSessionId");
        String thisSessionId = httpSession.getId() == null ? null : httpSession.getId();
        long lastAccessedTime = httpSession.getLastAccessedTime();
        long currentTimeInMilis = System.currentTimeMillis();
        long diff = ((currentTimeInMilis - lastAccessedTime) / 1000);
        int newMaxInactiveTimeout = maxInactiveTimeOut - (int) diff;
        if (newMaxInactiveTimeout <= 0) {
            //DO nothing
        } else if (httpSession.isNew() || (savedSessionId == null)) {
            httpSession.invalidate();
        } else {
            httpSession.setMaxInactiveInterval(newMaxInactiveTimeout);
            newTimeoutValue = "" + newMaxInactiveTimeout;
        }

        return newTimeoutValue;
    }

    public String getNextPhysicalPage(HttpServletRequest request, FormManager formMgr, String templateAutoSaved, NavigationManager nif, String navigateToPage) {
        String nextPhysicalPage = null;
        try {
            HttpSession httpSession = request.getSession(false); //don't create a new one, return null if none
            if (httpSession == null) {
                LOG.debug("Http Session is Null so returnig ==> " + navigateToPage);
                return navigateToPage;
            }
            String newMaxInactiveTimeout = checkIfAutoExtendSession(httpSession);
            LOG.debug("AutoExtendSession.getNextPhySicalPage() : newMaxInactiveTimeout ==> " + newMaxInactiveTimeout);
            if (StringFunction.isNotBlank(newMaxInactiveTimeout)) {
                long newMaxInactiveTimeOut = Long.parseLong(newMaxInactiveTimeout);
                LOG.debug("AutoExtendSession.getNextPhySicalPage() :  New Time Out Value ===> " + newMaxInactiveTimeOut);
                request.setAttribute("newTimeoutVal", String.valueOf(newMaxInactiveTimeOut));
                if ((newMaxInactiveTimeOut - 15) <= 20) {
                    LOG.debug("AutoExtendSession.getNextPhySicalPage() :  formMgr.getOriginalPage() == > " + formMgr.getOriginalPage());
                    DocumentHandler xmlDoc = formMgr.getFromDocCache();
                    if (xmlDoc != null) {
                        Vector errorList = xmlDoc.getFragments("/Error/errorlist/error");
                        LOG.info("Error List ===> " + errorList);
                        DocumentHandler errorDoc = null;
                        Vector warningMessageList = new Vector();
                        Vector errorMessageList = new Vector();
                        Vector infoMessageList = new Vector();
                        int severityLevel = 0;
                        int errorListSize = 0;
                        errorListSize = errorList.size();
                        for (int i = 0; i < errorListSize; i++) {
                            errorDoc = (DocumentHandler) errorList.elementAt(i);

                            severityLevel = errorDoc.getAttributeInt("/severity");

                            if (severityLevel == ErrorManager.ERROR_SEVERITY) {
                                errorMessageList.addElement(errorDoc.getAttribute("/message"));
                            } else if (severityLevel == ErrorManager.WARNING_ERROR_SEVERITY) {
                                warningMessageList.addElement(errorDoc.getAttribute("/message"));
                            } else if (severityLevel == ErrorManager.INFO_ERROR_SEVERITY) {
                                infoMessageList.addElement(errorDoc.getAttribute("/message"));
                            }
                        }
                        if (errorMessageList.size() > 0) {
                            nextPhysicalPage = nif.getPhysicalPage("TimeoutWithAutoSaveError", request);
                        } else if ("Y".equals(templateAutoSaved)) {
                            nextPhysicalPage = nif.getPhysicalPage("TimeoutWithAutoSaveTemplate", request);
                        } else if ("N".equals(templateAutoSaved)) {
                            nextPhysicalPage = nif.getPhysicalPage("TimeoutWithAutoSave", request);
                        } else {
                            nextPhysicalPage = nif.getPhysicalPage(formMgr.getNavigateToPage(), request);
                        }
                    } else {
                        nextPhysicalPage = nif.getPhysicalPage(formMgr.getNavigateToPage(), request);
                    }

                } else {
                    nextPhysicalPage = nif.getPhysicalPage(formMgr.getNavigateToPage(), request);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            nextPhysicalPage = navigateToPage;
        }
        LOG.debug("AutoExtendSession.getNextPhySicalPage() : nextPhySicalPage ===> " + nextPhysicalPage);
        return nextPhysicalPage;
    }
}
