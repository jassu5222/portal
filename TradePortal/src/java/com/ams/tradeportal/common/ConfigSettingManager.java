package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.JPylonProperties;
import com.ams.tradeportal.busobj.*;
import java.rmi.RemoteException;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

public class ConfigSettingManager {
private static final Logger LOG = LoggerFactory.getLogger(ConfigSettingManager.class);
	
	/**
	 * Retrieve the config_setting from the DB.  <br><br>
	 * 
	 * if overrideConfigSettingWithPropertiesFile=Y in TradePortal.properties, the value in config_setting
	 * is overridden by the value in the properties file, if the entry is present.
	 * 
	 * @param applicationName - PORTAL_APP or PORTAL_AGENT
	 * @param className - PORTAL or AGENT
	 * @param settingName - Setting Name
	 * @param defaultValue - Default Setting Value if there is no entry in config_setting
	 * @return setting_value of config_setting
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public static String getConfigSetting(String applicationName, String className, String settingName, String defaultValue) 
	throws AmsException, RemoteException
	{
		String overrideConfigSettingWithPropertiesFile = "N";
		String settingValue = null;
		try {
			PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
			overrideConfigSettingWithPropertiesFile = portalProperties.getString("overrideConfigSettingWithPropertiesFile");
		}
		catch (MissingResourceException e){	}
		
		JPylonProperties jPylonProperties = JPylonProperties.getInstance();
		String serverLocation             = jPylonProperties.getString("serverLocation");
        //LOG.info("ConfigSettingManager.getConfigSetting: serverLocation = " + serverLocation);
		ConfigSetting configSetting = (ConfigSetting) EJBObjectFactory.createClientEJB(serverLocation, "ConfigSetting");
		configSetting.getDataFromCache(applicationName, className, settingName);
		settingValue = configSetting.getAttribute("setting_value");
		if (settingValue == null || "".equals(settingValue)) {
			settingValue = defaultValue;
		}			
		
		if ("Y".equals(overrideConfigSettingWithPropertiesFile)) {
			defaultValue = settingValue; // use value from config_setting as default
			String propertiesFile = null;
			if ("AGENT".equals(className)) {
				propertiesFile = "AgentConfiguration";
			}
			else if ("PORTAL".equals(className)) {
				propertiesFile = "TradePortal";
			}
			else {
				propertiesFile = className;
			}
			try {
				PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle(propertiesFile);
				settingValue = portalProperties.getString(settingName);
			}
			catch (MissingResourceException e){	
				settingValue = defaultValue;
			}
		}

			
		return settingValue;

	}

}
