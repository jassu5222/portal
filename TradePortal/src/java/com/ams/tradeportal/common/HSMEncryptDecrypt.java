package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.KeyGenerator;

import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.Enumeration;

import sun.security.pkcs11.*;

import java.security.MessageDigest;

import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

public class HSMEncryptDecrypt {
private static final Logger LOG = LoggerFactory.getLogger(HSMEncryptDecrypt.class);

	// All the settings from the connection string decoded
	private static HashMap settings = null;

	// Loaded cryptographic elements
	private static KeyStore keystore = null;
	private static X509Certificate certChain[] = null;
	private static PrivateKey rsaPrivateKey = null;
	private static PublicKey rsaPublicKey = null;
	private static Provider provider = null;
	private static boolean isHardwareToken = false;
	private static X509Certificate cert = null;
	private static String symmetricAlgorithm = null;
	private static int symmetricKeySize = 0;
	private static int symmetricIVSize = 0;
	private static String storePassword = null;
	
	// Default parameter values
	private static final String defaultProviderType = "JKS";
	private static final String defaultKeystoreFile = "tpKeystore.jks";
	private static final String defaultAliasRSAKey = "rkey";
	private static final int defaultIVSize = 8;
	private static final int defaultPKCS11SlotIndex = 0;
	private static final String defaultPKCSLibrary = "aetpkss1.dll";
	private static final String defaultStreamEncoding = "/CBC/PKCS5Padding";
	private static final String defaultStreamEncodingNoPadding = "/CBC/NoPadding";
	private static final String defaultStreamEncryptionType = "AES";
	private static final int defaultKeySize = 128;

    // TradePortal properties value enabling / disabling HSM encryption for the instance
    private static boolean instanceHSMEnabled = false;
    
    static {
    	loadInstanceHSMEnabledProperty();
    	
    	if (instanceHSMEnabled) {
	    	String connectionString = null;
	
	    	PropertyResourceBundle servletProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
	    	if(servletProperties.getString("HSMConnectionString") != null) {
	    		connectionString = servletProperties.getString("HSMConnectionString");
	    		LOG.debug("[HSMEncryptDecrypt] Loaded HSMConnectionString: "+connectionString);
	    	}
	    	else {
	    		System.err.println("[HSMEncryptDecrypt] HSMConnectionString not specified in TradePortal.properties file");
	    	}
	
	    	try {
	    		connect(connectionString);
	    	} catch (Exception e) {
	    		System.err.println("[HSMEncryptDecrypt] Exception caught while trying to initialize static object.\n" + e.toString());
	    	}
    	}
    } // static

	/*
	 * Connection Strings
	 * 
	 * All values are continuous, single strings without line breaks.
	 * 
	 * Example 1: Hardware using an external smart card reader (AET SafeSign
	 * PKCS#11 Library)
	 * 
	 * PROVIDERTYPE=SunPKCS11;PKCS11LIB=aetpkss1.dll;PKCS11SLOTINDEX=0;TOKENTYPE
	 * =HARD;STORE=ANZProtect;STOREPASSWORD=1234;STOREALIAS=;IV=
	 * abbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabba
	 * 
	 * Example 2: Software using a JKS built with a keystore
	 * PROVIDERTYPE=jceks;STORE
	 * =/Work/LIT/jobs/Anz/ANZProtector/Java/src/ANZProtect
	 * /bin/anzprotector.jks;STOREPASSWORD=password;STOREALIAS=anzprotect;IV=
	 * abbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabbaabbaABBAabba
	 */


	/***
	 * Connect to the cryptographic library
	 * 
	 * @param connectionString
	 *            Detailed parameters for connecting to the keystore. See
	 *            documentation for details
	 * @throws Exception
	 */
	public static synchronized void connect(String connectionString) throws Exception {
		LoadSettings(connectionString);

		// Provider Type
		String providerType = defaultProviderType;
		if (settings.containsKey("PROVIDERTYPE")) {
			providerType = (String) settings.get("PROVIDERTYPE");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : PROVIDERTYPE = "+providerType);
		}

		// Keystore filename
		String storeLocation = defaultKeystoreFile;
		if (settings.containsKey("STORE")) {
			storeLocation = (String) settings.get("STORE");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : STORE = "+storeLocation);
		}

		// Password for JKS and key (must be the same)
		storePassword = "";
		if (settings.containsKey("STOREPASSWORD")) {
			storePassword = (String) settings.get("STOREPASSWORD");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : STOREPASSWORD = "+storePassword);
		}

		// Symmetric Cipher Algorithm
		symmetricAlgorithm = defaultStreamEncryptionType;
		if (settings.containsKey("SYMMETRICKEYALG")) {
			symmetricAlgorithm = (String) settings.get("SYMMETRICKEYALG");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : SYMMETRICKEYALG = "+symmetricAlgorithm);
		}

		// Symmetric Cipher Key Size
		symmetricKeySize = defaultKeySize;
		if (settings.containsKey("SYMMETRICKEYSIZE")) {
			symmetricKeySize = parseInt((String) settings.get("SYMMETRICKEYSIZE"),
					defaultKeySize).intValue();
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : SYMMETRICKEYSIZE = "+symmetricKeySize);
		}

		// Symmetric IV Size
		symmetricIVSize = defaultIVSize;
		if (settings.containsKey("SYMMETRICIVSIZE")) {
			symmetricIVSize = parseInt((String) settings.get("SYMMETRICIVSIZE"),
					defaultIVSize).intValue();
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : SYMMETRICIVSIZE = "+symmetricIVSize);
		}

		// Alias within the keystore of the RSA key
		String storeAliasRSAKey = defaultAliasRSAKey;
		if (settings.containsKey("STOREALIASRKEY")) {
			storeAliasRSAKey = (String) settings.get("STOREALIASRKEY");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : STOREALIASRKEY = "+storeAliasRSAKey);
		}
		
		// PKCS11 Library file
		String pkcs11Library = defaultPKCSLibrary;
		if (settings.containsKey("PKCS11LIB")) {
			pkcs11Library = (String) settings.get("PKCS11LIB");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : PKCS11LIB = "+pkcs11Library);
		}

		// PKCS11 Slot Index
		int slotIndex = defaultPKCS11SlotIndex;
		if (settings.containsKey("PKCS11SLOTINDEX")) {
			if (((String) settings.get("PKCS11SLOTINDEX")).length() > 0)
				slotIndex = parseInt((String) settings.get("PKCS11SLOTINDEX"),
						defaultPKCS11SlotIndex).intValue();
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : PKCS11SLOTINDEX = "+slotIndex);
		}

		// Determine Token Type is hard or soft
		String tokenType = "SOFT";
		if (settings.containsKey("TOKENTYPE")) {
			tokenType = (String) settings.get("TOKENTYPE");
			LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded : TOKENTYPE = "+tokenType);
		}
		isHardwareToken = (tokenType.compareToIgnoreCase("SOFT") != 0);
		LOG.debug("[HSMEncryptDecrypt.connect(String)] isHardwareToken = "+isHardwareToken);

		// Load the provider
		keystore = null;
		if (isHardwareToken) {
			if (!initializeProvider(pkcs11Library, storeLocation, slotIndex))
				throw new Exception(
				"Unable to load the PKCS11 provider specified");

			keystore.load(null, storePassword.toCharArray());
			LOG.debug("[HSMEncryptDecrypt.connect(String)] loaded PKCS11 provider keystore from "
					+pkcs11Library+", "+storeLocation+", "+slotIndex);
		} else {
			keystore = KeyStore.getInstance(providerType);
			provider = keystore.getProvider();

			// Load the file
			try(FileInputStream stream = new FileInputStream(
					new File(storeLocation))){
			keystore.load(stream, storePassword.toCharArray());
			LOG.debug("[HSMEncryptDecrypt.connect(String)] loaded keystore from "+storeLocation
					+" with "+keystore.size()+" entries");
			}
		}
	
		// Search for the alias for the provided value
		rsaPublicKey = null;
		cert = null;
		String alias;
		for (Enumeration e = keystore.aliases(); e.hasMoreElements();) {
			alias = (String) e.nextElement();
			if ((storeAliasRSAKey.length() > 0) && storeAliasRSAKey.equals(alias))
			{
				LOG.debug("[HSMEncryptDecrypt.connect(String)] Found key with alias = "+alias+" in keystore");
				// If the alias found, then load the key and search for the
				// matching cert
				if (keystore.isKeyEntry(alias)) {
					rsaPrivateKey = (PrivateKey) keystore.getKey(alias,
							storePassword.toCharArray());
					LOG.debug("[HSMEncryptDecrypt.connect(String)] Loaded RSA private key with alias = "+alias+" from keystore");
					java.security.cert.Certificate[] chain = keystore.getCertificateChain(alias);
					certChain = new X509Certificate[chain.length];

					// Search for the matching cert.
					// The chain is retained for debug purposes (or tracing in
					// the future)
					for (int i = 0; i < chain.length; i++) {
						certChain[i] = (X509Certificate) chain[i];
						cert = certChain[certChain.length - 1];
						rsaPublicKey = cert.getPublicKey();

						// If the modulus don't match, then the certificate
						// definitely
						// doesn't match. (ignore exponents as they are likely
						// to match.
						// if
						// (rsaPrivateKey.getModulus().compareTo(rsaPublicKey.getModulus())
						// != 0)
						if (rsaPrivateKey.getAlgorithm()!=null && !rsaPrivateKey.getAlgorithm().equals(rsaPublicKey.getAlgorithm())) {
							rsaPublicKey = null;
							cert = null;
						}
					} // for (int i = 0; i < chain.length; i++)
				} // if (keystore.isKeyEntry(alias))
			} // if ((storeAliasRSAKey.length() > 0) && storeAliasRSAKey.equals(alias))
		} // for (java.util.Enumeration e = keystore.aliases(); e.hasMoreElements();)

		// If no certificates found - abort now.
		if (cert == null) {
			cert = null;
			keystore = null;
			rsaPrivateKey = null;
			rsaPublicKey = null;
			throw new Exception("Unable to load the certificate specified");
		}

	}

	/***
	 * Disconnect from the cryptolibrary
	 */
	public static synchronized void disconnect() {
		cert = null;
		keystore = null;
		rsaPrivateKey = null;
		rsaPublicKey = null;
	}

	/***
	 * Get a base64 encoded buffer of random bytes from the crypto provider
	 * established in the connection string
	 * 
	 * @param numberBytes
	 *            number of bytes requested
	 * @return Returns a string with the number of bytes encoded in Base64
	 * @throws Exception 
	 */
	public static synchronized String getRandomBase64(int numberBytes) throws Exception {

		byte[] randomBytes = new byte[numberBytes];

		for (int j = 0; j < numberBytes;) {
			byte[] generatedRandomBytes = new byte[numberBytes];
			try {
				SecureRandom random = SecureRandom.getInstance("SHA1PRNG", provider);
				random.nextBytes(generatedRandomBytes);
			} catch (NoSuchAlgorithmException e) {
				LOG.debug("[HSMEncryptDecrypt.getRandomBase64()SHA1PRNG not available with configured provider. Falling back to default provider ");
				LOG.info("[HSMEncryptDecrypt.getRandomBase64()SHA1PRNG not available with configured provider. Falling back to default provider ");
				throw new Exception("No provider loaded for the name provided");
			}
			// Copy out the random bytes available
			for (int k = 0; j < numberBytes && k < generatedRandomBytes.length; j++, k++)
				randomBytes[j] = generatedRandomBytes[k];
		}

		// return it formatted
		return new String(Base64Coder.encode(randomBytes));
	}

	/***
	 * Return a random IV value
	 * 
	 * @return base64 encoded IV value for the symmetric crypto being used
	 * @throws Exception 
	 */
	public static synchronized String getNewIV() throws Exception {

		int ivSize = symmetricIVSize;
		try {
			Cipher cipher = Cipher.getInstance(symmetricAlgorithm
					+ defaultStreamEncoding, provider);
			ivSize = cipher.getBlockSize();
		} catch (NoSuchAlgorithmException e) {
			System.err.println("[HSMEncrypDecrypt.getNewIV()] NoSuchAlgorithmException caught while trying to get Cipher instance from provider "
					+ provider.getName() + " with algorithm " + symmetricAlgorithm + defaultStreamEncoding
					+ " and set the block size: " + e.getMessage());
		} catch (NoSuchPaddingException e) {
			ivSize = symmetricIVSize;
		}

		return getRandomBase64(ivSize);
	}

	/***
	 * Decrypt the provided Base64 encrypted data and return a plain text
	 * string. The input is a formatted string. The format is"ENC|Base64 formatted IV|Base64 formatted PublicKey wrapped symmetric key|Base64 formatted symmetric key encrypted message"
	 * 
	 * @param EncData
	 *            Base64 string with all encoded information.
	 * @return Plain text string
	 * @throws Exception
	 */
        // W Zhu 8/20/2015 Rel 9.3.5 CR-1033 Cast Exception to AmsException for simpler handling
	public static synchronized String decryptForTransport(String EncData) throws AmsException {
		LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] EncData = "+EncData);
				
		// Split the input into the 4 components.
		String[] encDataStrings = EncData.split("\\|");
		if (encDataStrings.length < 4)
			throw new AmsException("Invalid input parameter format");

		if (!encDataStrings[0].equalsIgnoreCase("ENC"))
			throw new AmsException(
			"The first component of the input parameter (ENC) is incorrectly formatted");

		Cipher cipher = null;
		SecretKey cipherKey = null;
		byte[] initVectorBytes = null;
		try {
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Getting instance of Cipher with parameters "+symmetricAlgorithm+defaultStreamEncodingNoPadding
					+ " and " + provider.getName());
			try {
				cipher = Cipher.getInstance(symmetricAlgorithm
						+ defaultStreamEncodingNoPadding, provider);
			} catch (Exception e) {
				LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] "
					+"Exception caught while trying to get Cipher instance.  Trying again with "+symmetricAlgorithm+defaultStreamEncodingNoPadding
					+" and provider hardcoded to nCipherKM");
				try {
					cipher = Cipher.getInstance(symmetricAlgorithm
							+ defaultStreamEncodingNoPadding, "nCipherKM");
				} catch (Exception e2) {
					LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Caught the following exception while trying to get instance of Cipher again: "
							+e2.getMessage());
					throw new AmsException(e2);
				}
			}
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Obtained instance of Cipher with symmetricAlgorithm = "+symmetricAlgorithm
					+ ", defaultStreamEncodingNoPadding = " + defaultStreamEncodingNoPadding 
					+ ", and provider = " + provider.getName());

			KeyGenerator keyGenerator;
			keyGenerator = KeyGenerator.getInstance(symmetricAlgorithm,
					provider);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Obtained instance of KeyGenerator with symmetricAlgorithm = " +symmetricAlgorithm
					+ " and provider = " + provider.getName());
			keyGenerator.init(symmetricKeySize);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Initialized keyGenerator");

			// Create the IV based on the parameters provided
			int ivSizeBase64 = (cipher.getBlockSize() + 3) / 3 * 4;
			if (encDataStrings[1].length() < ivSizeBase64
					&& encDataStrings[1].length() != 0)
				throw new AmsException("The second component of the input parameter (IV) is incorrect");

			// Load the IV. If the IV has been provided in the settings and not
			// provided in the entry string,
			// then we use that value instead of the provided value.
			String initVector = encDataStrings[1];
			if (settings.containsKey("IV")) {
				if (((String) settings.get("IV")).length() >= ivSizeBase64)
					if (encDataStrings[1].length() == 0)
						initVector = (String) settings.get("IV");
			}
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Using initVector = {}", initVector);
			
			// Convert the IV to bytes
			byte[] initVectorBytesTmp = Base64Coder.decode(initVector
					.substring(0, ivSizeBase64));
			initVectorBytes = new byte[cipher.getBlockSize()];
			System.arraycopy(initVectorBytesTmp, 0, initVectorBytes, 0,
					initVectorBytes.length);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Converted the initVector to bytes");

			// Load the encrypted key data
			if (encDataStrings[2].length() < 60
					&& (encDataStrings[2].length() % 4) != 0)
				throw new AmsException("The third component of the input parameter (EncKey) is incorrect");
			byte[] keyTextBytes = Base64Coder.decode(encDataStrings[2]);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Loaded the encrypted keyText into bytes");
			
			// Load the encrypted data
			if ((encDataStrings[3].length() % 4) != 0)
				throw new AmsException("The fourth component of the input parameter (EncKey) is incorrect");
			byte[] cipherTextBytes = Base64Coder.decode(encDataStrings[3]);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Loaded the encrypted cipherText into bytes");
			
			// Decrypt the exchange key
			Cipher rsaDecrypter = Cipher.getInstance("RSA/ECB/PKCS1Padding", provider);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Obtained instance of rsaDecrypter from provider = {}", provider.getName());
			rsaDecrypter.init(Cipher.UNWRAP_MODE, rsaPrivateKey);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Initialized the rsaEncryptor in UNWRAP_MODE and with the rsaPrivateKey");
			cipherKey = (SecretKey) rsaDecrypter.unwrap(keyTextBytes,
					symmetricAlgorithm, Cipher.SECRET_KEY);
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Unwrapped the keyText using symmetricAlgorithm = {} and Cipher.SECRET_KEY", symmetricAlgorithm);

			cipher.init(Cipher.DECRYPT_MODE, cipherKey, new IvParameterSpec(initVectorBytes));
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Initialized cipher in DECRYPT_MODE");

			// Decrypt
			byte[] plainTextBytes = cipher.doFinal(cipherTextBytes);			
			LOG.debug("[HSMEncryptDecrypt.decryptForTransport(String)] Decrypted cipherText");

			// Workaround as some devices do not support PKCS5 padding in their
			// PKCS#11 provider (just do it manually)
			int trimmedLength = plainTextBytes.length
			- plainTextBytes[plainTextBytes.length - 1];
			byte[] plainTextBytesTrimmed = new byte[trimmedLength];
			System.arraycopy(plainTextBytes, 0, plainTextBytesTrimmed, 0,
					trimmedLength);

			return new String(plainTextBytesTrimmed, "UTF-8");

		} catch (NoSuchAlgorithmException e) {
			throw new AmsException("AES Encryption not supported on this device", e);
		} catch (NoSuchPaddingException e) {
			throw new AmsException("PKCS5 padding not supported on this device", e);
		} catch (Exception e) {
                    // W Zhu 8/20/2015 Rel 9.3.5 CR-1033 Cast Exception to AmsException for simpler handling
                    throw new AmsException(e);
                }
                

	}

	/***
	 * Sign the challenge using the eMACkey provided
	 * 
	 * @param challenge
	 *            Response is a generated MAC on the challenge.
	 * @param eMACKey
	 *            Public Key encrypted MAC key
	 * @return a challengeResponse pair encoded for the validateMACs routine
	 * 
	 * @throws Exception
	 */
	public static synchronized String signMAC(String challenge, String eMACKey) throws Exception {

		LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
				+"challenge = "+challenge);
		LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
				+"eMACKey = "+eMACKey);
		
		Cipher cipher = null;
		SecretKey cipherKey = null;
		byte[] initVectorBytes = null;
		try {
			cipher = Cipher.getInstance(symmetricAlgorithm
					+ defaultStreamEncodingNoPadding, provider);

			// Initialise the IV to Null as the input data should be random for
			// the challenge
			// so the value of a public random IV is of no value.
			initVectorBytes = new byte[cipher.getBlockSize()];
			for (int i = 0; i < cipher.getBlockSize(); i++)
				initVectorBytes[i] = 0;

			// Load the encrypted key data
			if (eMACKey.length() < 60 && (eMACKey.length() % 4) != 0)
				throw new Exception("The encrypted MAC key is incorrect");
			byte[] keyTextBytes = Base64Coder.decode(eMACKey);
			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Loaded the eMACKey into keyTextBytes");
			
			// Decrypt the exchange key
			Cipher rsaEncryptor = Cipher.getInstance("RSA/ECB/PKCS1Padding",
					provider);
			rsaEncryptor.init(Cipher.UNWRAP_MODE, rsaPrivateKey);
			cipherKey = (SecretKey) rsaEncryptor.unwrap(keyTextBytes,
					symmetricAlgorithm, Cipher.SECRET_KEY);
			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Unwrapped the symmetrical key using the RSA Private Key");

			// Initialise the IV vector each time to reset the MAC
			// encryption
			cipher.init(Cipher.ENCRYPT_MODE, cipherKey, new IvParameterSpec(
					initVectorBytes));
			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Initialized the encryption cipher");
					
			// Create the input byte array of the string provided.
			byte[] plainTextBytes = Base64Coder.decode(challenge);
			
			// Pad input with 0 values to block size (no padding as some HSM cannot pad)
			if ((plainTextBytes.length % cipher.getBlockSize()) != 0)
			{
				byte[] padded = new byte[plainTextBytes.length + (cipher.getBlockSize() - (plainTextBytes.length % cipher.getBlockSize()))];
				for (int k = plainTextBytes.length; k < padded.length; k++)
					padded[k] = 0;
				System.arraycopy(plainTextBytes, 0, padded, 0, plainTextBytes.length);
				plainTextBytes = padded;
			}

			// Encrypt
			byte[] encryptedStream = cipher.doFinal(plainTextBytes);
			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Successfully encrypted the challenge");
			
			// Extract the last 8 bytes and convert to base64.
			byte[] macBytes = new byte[cipher.getBlockSize()];
			System.arraycopy(encryptedStream, encryptedStream.length
					- macBytes.length, macBytes, 0, macBytes.length);
			

			// Convert encrypted data into a base64-encoded string.
			String macText = new String(Base64Coder.encode(macBytes));
			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Converted encrypted data into Base64");

			LOG.debug("[HSMEncryptDecrypt.signMAC(String,String)] "
					+"Returning the following: "+(new String(Base64Coder.encode(plainTextBytes)))+"|"+macText);
			return new String(Base64Coder.encode(plainTextBytes))+"|"+macText;

		} catch (NoSuchAlgorithmException e) {
			throw new Exception("AES Encryption not supported on this device");
		} catch (NoSuchPaddingException e) {
			throw new Exception("PKCS5 padding not supported on this device");
		}
	}


	/***
	 * Validate the challenge response pairs against the encrypted MAC key
	 * 
	 * @param challengeResponses
	 *            Challenge / Response pairs encoded as challenge|response. A
	 *            Response is a generated MAC. This is an array of pairs which
	 *            will all be presented to the HSM for validation and success
	 *            returned in the call parameter.
	 * @param eMACKey
	 *            Public Key encrypted MAC key
	 * @return List of return values for each of the comparisons.
	 * 
	 * @throws Exception
	 */
	public static synchronized boolean[] validateMACs(String[] challengeResponses, String eMACKey)
			throws Exception {

		LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
			+"challengeResponses = "+challengeResponses.toString());
		LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
			+"eMACKey = "+eMACKey);
		
		Cipher cipher = null;
		SecretKey cipherKey = null;
		byte[] initVectorBytes = null;
		try {
			cipher = Cipher.getInstance(symmetricAlgorithm
					+ defaultStreamEncodingNoPadding, provider);

			// Initialise the IV to Null as the input data should be random for
			// the challenge
			// so the value of a public random IV is of no value.
			initVectorBytes = new byte[cipher.getBlockSize()];
			for (int i = 0; i < cipher.getBlockSize(); i++)
				initVectorBytes[i] = 0;

			// Load the encrypted key data
			if (eMACKey.length() < 60 && (eMACKey.length() % 4) != 0)
				throw new Exception("The encrypted MAC key is incorrect");
			byte[] keyTextBytes = Base64Coder.decode(eMACKey);
			LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
					+"Loaded the eMACKey");

			// Decrypt the exchange key
			Cipher rsaEncryptor = Cipher.getInstance("RSA/ECB/PKCS1Padding",
					provider);
			rsaEncryptor.init(Cipher.UNWRAP_MODE, rsaPrivateKey);
			cipherKey = (SecretKey) rsaEncryptor.unwrap(keyTextBytes,
					symmetricAlgorithm, Cipher.SECRET_KEY);
			LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
					+"Unwrapped the symmetrical key using the RSA Private Key");

			// Initialise the results to be returned.
			boolean[] results = new boolean[challengeResponses.length];

			LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
					+"About to validate "+challengeResponses.length+" challengeResponses");
			
			for (int i = 0; i < challengeResponses.length; i++) {
				// Initialise the IV vector each time to reset the MAC
				// encryption
				cipher.init(Cipher.ENCRYPT_MODE, cipherKey,
						new IvParameterSpec(initVectorBytes));

				// Result will be false by default.
				results[i] = false;

				// split the challenge response pair
				String[] pair = challengeResponses[i].split("\\|");

				// Create the input byte array of the string provided.
				byte[] plainTextBytes = Base64Coder.decode( pair[0]);
				
				// Pad input with 0 values to block size (no padding as some HSM cannot pad)
				if ((plainTextBytes.length % cipher.getBlockSize()) != 0)
				{
					byte[] padded = new byte[plainTextBytes.length + (cipher.getBlockSize() - (plainTextBytes.length % cipher.getBlockSize()))];
					for (int k = plainTextBytes.length; k < padded.length; k++)
						padded[k] = 0;
					System.arraycopy(plainTextBytes, 0, padded, 0, plainTextBytes.length);
					plainTextBytes = padded;
				}
				
				// Encrypt
				byte[] cipherTextBytes = cipher.doFinal(plainTextBytes);

				// Extract the last 8 bytes and convert to base64.
				byte[] macBytes = new byte[cipher.getBlockSize()];
				System.arraycopy(cipherTextBytes, cipherTextBytes.length
						- macBytes.length, macBytes, 0, macBytes.length);

				// Convert encrypted data into a base64-encoded string.
				String macText = new String(Base64Coder.encode(macBytes));

				// Compare the mac generated with the value provided.
				results[i] = (macText.compareTo(pair[1]) == 0);
				
				LOG.debug("[HSMEncryptDecrypt.validateMACs(String[],String)] "
						+"Validated challengeResponse "+ challengeResponses[i] +" with result "+results[i]);
			}

			return results;

		} catch (NoSuchAlgorithmException e) {
			throw new Exception("AES Encryption not supported on this device");
		} catch (NoSuchPaddingException e) {
			throw new Exception("PKCS5 padding not supported on this device");
		}
	}

	/***
	 * Returns the Base64 formatted, ASN1 encoded public key of the key
	 * referenced in the connection string.
	 * 
	 * @return Base64 string with all encoded information of the public key
	 */
	public static synchronized String getPublicKey() {
		return new String(Base64Coder.encode(cert.getPublicKey().getEncoded()));
	}

	/**
	 * 
	 * @param passwordHash
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public static synchronized boolean validateCombinationPassword(String password,String passwordHash)
	throws Exception {
		
		// T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- START
		boolean isMatching = true;
		
		String computedPasswordHash;
		String decryptedPasswordHash;
		
		// Load the encrypted AES key data
		if (passwordHash.length() < 60 && (passwordHash.length() % 4) != 0)
			throw new Exception("The encrypted MAC key is incorrect");
		
		byte[] keyTextBytes = Base64Coder.decode(passwordHash);
 		
 		// Decrypt the passwordHash into an AES key
		Cipher rsaEncryptor = Cipher.getInstance("RSA/ECB/PKCS1Padding",
				provider);
		rsaEncryptor.init(Cipher.UNWRAP_MODE, rsaPrivateKey);
		SecretKey cipherKey = (SecretKey) rsaEncryptor.unwrap(keyTextBytes,
				symmetricAlgorithm, Cipher.SECRET_KEY);
		
		decryptedPasswordHash = EncryptDecrypt.bytesToBase64String(cipherKey.getEncoded());
		MessageDigest digest = null;
		 
 
		try{
			 digest = MessageDigest.getInstance("SHA-256",provider); 
			 if (null != digest)
				 LOG.debug("[HSMEncryptDecrypt.validateCombinationPassword():Created MessageDigest using provider " + provider.getName()); 
			 
		}catch (NoSuchAlgorithmException nse) {
 			digest = MessageDigest.getInstance("SHA-256");
 			System.err.println("[HSMEncryptDecrypt.validateCombinationPassword():unable to create MessageDigest using provider " + provider.getName() 
					  +" " + nse.getMessage());
 		}
 		
		if (null == digest){
			System.err.println("[HSMEncryptDecrypt.validateCombinationPassword():unable to create MessageDigest available providers");
			return false;
		}
 		byte[] hash = digest.digest(password.getBytes("UTF-8"));
		byte[] truncated = new byte[16];
		
		//arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
		System.arraycopy(hash,0,truncated,0,16);
		computedPasswordHash = EncryptDecrypt.bytesToBase64String(truncated);
 		
 		if (!computedPasswordHash.equals(decryptedPasswordHash))
 			isMatching = false;
		
 		return isMatching;
 		// T36000030120-smanohar--check password complexity at server also for HSMEnabled instances-- END
	}

	/***
	 * Load the settings
	 * 
	 * @param connectionString
	 *            ";" separate list of Name=Value pairs
	 */
	private static synchronized void LoadSettings(String connectionString) {
		settings = new HashMap();

		String[] settingStrings = connectionString.split(";");
		for (int i = 0; i < settingStrings.length; i++) {
			int offset = settingStrings[i].indexOf("=");
			String key = settingStrings[i].substring(0, offset);
			String value = settingStrings[i].substring(offset + 1);
			settings.put(key.toUpperCase(), value);
		}
	}

	/***
	 * Safe parse the Integer string
	 * 
	 * @param data
	 *            a numeric value
	 * @param defaultValue
	 *            default value if it fails to parse the data as an integer
	 * @return numeric value of the string
	 */
	private static synchronized Integer parseInt(String data, int defaultValue) {
		Integer val = new Integer(defaultValue);
		try {
			val = new Integer(data);
		} catch (NumberFormatException nfe) {
			System.err.println("[HSMEncryptDecrypt.parseInt(String,int)] NumberFormatException caught while creating a new Integer from "
					+ data + ": " + nfe.getMessage());
		}
		return val;
	}

	/**
	 * Initialize a PKCS11 provider
	 * 
	 */
	private static synchronized boolean initializeProvider(String pkcs11Library, String keyAlias,
			int slotIndex) {
		try {
			String providerName = "SunPKCS11-"+keyAlias;
			provider = Security.getProvider(providerName);
			if (provider == null)
				throw new Exception("No provider loaded for the name provided");
			keystore = KeyStore.getInstance("PKCS11", provider);
			return true;
		} catch (Exception ex) {
			// Unable to load the provider by name - load using the
			// configuration provided.
		}

		// The configuration parameters for the PKCS11 module
		String configParameters = "library="+pkcs11Library+"\r\nname="+keyAlias+"\r\nslotListIndex="+slotIndex;

		// Load the configuration from a stream instead of a file
		ByteArrayInputStream configStream = new ByteArrayInputStream(
				configParameters.getBytes());

		try {
			provider = new SunPKCS11(configStream);
			// Check that the provider works by initializing a PKCS11 keystore
			keystore = KeyStore.getInstance("PKCS11", provider);
			// Add to the provider list
			Security.addProvider(provider);

		} catch (Exception e) {
			provider = null;
			keystore = null;
			return false;
		}

		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public static synchronized boolean isInstanceHSMEnabled () {
		return instanceHSMEnabled;
	}
	
	/**
	 * 
	 */
	private static synchronized void loadInstanceHSMEnabledProperty() {
		PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
		try {
			if((tpProperties.getString("HSMEnabled") != null) 
					&& (tpProperties.getString("HSMEnabled").equalsIgnoreCase("true")))
			{
				LOG.debug("[HSMEncryptDecrypt.loadInstanceHSMEnabledProperty()] Found HSMEnabled set to true");
				instanceHSMEnabled = true;
			} 
		} catch (MissingResourceException e) {
			instanceHSMEnabled = false;
			System.err.println("MissingResourceException occurred in HSMEncryptDecrypt:  HSMEnabled property not found in the TradePortal.properties file.  Defaulting to \"false\". " + e.toString());
		}
	}	
}
