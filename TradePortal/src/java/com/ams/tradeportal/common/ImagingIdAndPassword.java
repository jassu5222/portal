package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
        
        
/**
 * Structure that contains an ID and password 
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ImagingIdAndPassword
    {
private static final Logger LOG = LoggerFactory.getLogger(ImagingIdAndPassword.class);
        private String id;
        private String password;
               
        /**
         * Constructor.  Accepts an id and password
         *
         * @param id String - the ID
         * @param password String - the password
         */
        public ImagingIdAndPassword(String id, String password)
        {
            this.id = id;
            this.password = password;
        }
         
        /**
         * Getter for ID
         *
         * @return String ID
         */               
        public String getId()
        {
            return id;
        }

        /**
         * Getter for passwod 
         *
         * @return String password
         */
        public String getPassword()
        {
            return password;   
        }
    }