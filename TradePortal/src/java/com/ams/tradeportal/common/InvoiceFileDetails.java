/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class InvoiceFileDetails {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileDetails.class);

    public static final String FILE_EXTENSION = ".txt";
    public static final String AN = "alphaNumeric";
    public static final String ALP = "alpha";
    public static final String NUMERIC = "Numeric";
    public static final String AMTNUM = "AmountNumeric";
    public static final String DATE = "DateFormat";
    public static final Pattern ALPHA = Pattern.compile("[A-Za-z]+");
    public static final Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9 ]+");
    public static final Pattern AMOUNT = Pattern
            .compile("[-]?\\d{1,15}(\\.\\d{1,3})?");
    public static final Pattern QTY = Pattern.compile("\\d{1,13}(\\.\\d{1,5})?"); // Nar IR- LMUM062755984
    public static final Pattern NUM = Pattern.compile("[0-9]+");
    // Nar IR-MNUM061259346- rel 8.0 invoice supporting only REC instrument, next release
    // invoice will support ATP, load request as well. we need to add all supporting insturment type for upload invoice.
    final List<String> ValidInvoiceInstrType = Arrays.asList("REC", "ATP", "LRQ", "PYB");
    final List<String> ValidInvoiceType = Arrays.asList("INT", "RPL");
    public static final String REFDATA = "REFDATA";
    public static final String CREDITNOTE = "CREDITNOTE";
    public static final String CREDITFIELDS = "CREDITFIELDS";
    final List<String> ValidChargeType = Arrays.asList(TradePortalConstants.CHARGE_UPLOAD_FW_OURS, TradePortalConstants.CHARGE_UPLOAD_FW_BEN,
            TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
    public static final String BRANCHCODE = "BRANCHCODE";
    public static final String BANKSORTCODE = "BANKSORTCODE"; //MEer IR-36300 Checking Bank Sort code for unicode characters	

    public static String validDateFormat = null;
    private String dateReplaced = null;      // DK IR T36000014616 Rel8.2 04/02/2013
    private int currentLineNum;
    private String localeName;
    private Map<String, Integer> fieldPositions;
    InvoiceDefinitionWebBean invoiceDefinition;
    private MediatorServices mediatorServices;
    private String loanType;
    private boolean isPayCreditNote;
    private boolean hasSellerID = false;
    private boolean hasSellerName = false;

    private final PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle
            .getBundle(TradePortalConstants.TEXT_BUNDLE);

    static final Hashtable<String, String> delimiters = new Hashtable<>();

    static {
        delimiters.put("COMMA", ",");
        delimiters.put("PIPEDELIMITER", "\\|");
        delimiters.put("SEMICOLON", ";");
        delimiters.put("TAB", "\\t");
    }
    static final ArrayList<String> delimitersEndValue = new ArrayList<>();

    static {
        delimitersEndValue.add(",,");
        delimitersEndValue.add("||");
        delimitersEndValue.add(";;");
        delimitersEndValue.add("		");
    }

    static final ArrayList<String> reqFields = new ArrayList<>();

    static {
        reqFields.add("invoice_id");
        reqFields.add("issue_date");
        reqFields.add("due_date");
        reqFields.add("buyer_name");
        reqFields.add("buyer_id");
        reqFields.add("seller_name");
        reqFields.add("seller_id");
        reqFields.add("currency");
        reqFields.add("amount");
    }

    static final Hashtable<String, Integer> fieldSize = new Hashtable<>();

    static {
        fieldSize.put("invoice_id", 35);
        fieldSize.put("issue_date", 10);
        fieldSize.put("due_date", 10);
        fieldSize.put("buyer_name", 35);
        fieldSize.put("buyer_id", 30);
        fieldSize.put("seller_name", 35);
        fieldSize.put("seller_id", 30);
        fieldSize.put("currency", 3);
        fieldSize.put("amount", 24);
        fieldSize.put("goods_description", 70);
        fieldSize.put("linked_to_instrument_type", 3);
        fieldSize.put("incoterm", 3);
        fieldSize.put("country_of_loading", 35);
        fieldSize.put("country_of_discharge", 35);
        fieldSize.put("vessel", 35);
        fieldSize.put("carrier", 35);
        fieldSize.put("actual_ship_date", 10);
        fieldSize.put("payment_date", 10);
        fieldSize.put("purchase_order_id", 35);
        fieldSize.put("users_def_label", 35);
        fieldSize.put("users_def_value", 140);
        fieldSize.put("line_item_id", 70);
        fieldSize.put("unit_price", 24);
        fieldSize.put("inv_quantity", 24);
        fieldSize.put("unit_of_measure_price", 35);
        fieldSize.put("unit_of_measure_quantity", 35);
        fieldSize.put("prod_chars_ud_type", 35);
        fieldSize.put("prod_chars_ud_val", 35);
        fieldSize.put("invoice_type", 3);//SHR IR 5727
        // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
        fieldSize.put("credit_note", 11);
        fieldSize.put("pay_method", 4);
        fieldSize.put("ben_acct_num", 34);
        fieldSize.put("ben_add1", 35);
        fieldSize.put("ben_add2", 35);
        fieldSize.put("ben_add3", 35);
        fieldSize.put("ben_email_addr", 255);
        fieldSize.put("ben_country", 2);
        fieldSize.put("ben_bank_name", 120);
        fieldSize.put("ben_bank_sort_code", 8);
        fieldSize.put("ben_branch_code", 35);
        fieldSize.put("ben_branch_add1", 35);
        fieldSize.put("ben_branch_add2", 35);
        fieldSize.put("ben_bank_city", 31);
        fieldSize.put("ben_bank_province", 8);
        fieldSize.put("ben_branch_country", 2);
        fieldSize.put("charges", 3);
        fieldSize.put("central_bank_rep1", 35);
        fieldSize.put("central_bank_rep2", 35);
        fieldSize.put("central_bank_rep3", 35);
        // RKAZI Rel 8.2 IR# T36000012087 02/28/2013 END
        fieldSize.put("credit_discount_code_id", 2);
        fieldSize.put("credit_discount_gl_code_id", 20);
        fieldSize.put("credit_discount_comments_id", 100);
        //CR 913 start
        fieldSize.put("send_to_supplier_date", 10);
        fieldSize.put("buyer_acct_num", 30);
        fieldSize.put("buyer_acct_currency", 3);
        //CR 913 end
        fieldSize.put("end_to_end_id", 35);
        //CR1001 Rel9.4 START
        fieldSize.put("reporting_code_1", 3);
        fieldSize.put("reporting_code_2", 3);
        //CR1001 Rel9.4 END

    }
    static final Hashtable<String, String> fieldFormat = new Hashtable<>();

    static {
        fieldFormat.put("issue_date", DATE);
        fieldFormat.put("due_date", DATE);
        fieldFormat.put("currency", "CURR");
        fieldFormat.put("amount", AMTNUM);
        fieldFormat.put("goods_description", "GOODS");
        fieldFormat.put("linked_to_instrument_type", "INSTRUMENT_TYPE");
        fieldFormat.put("invoice_type", "INVOICE_TYPE");
        fieldFormat.put("charges", "CHARGE_TYPE");
        fieldFormat.put("actual_ship_date", DATE);
        fieldFormat.put("payment_date", DATE);
        fieldFormat.put("unit_price", AMTNUM);
        fieldFormat.put("inv_quantity", "QUANTITY");
        fieldFormat.put("amended_invoice_number", NUMERIC);
        fieldFormat.put("ben_acct_num", AN);
        fieldFormat.put("currency", REFDATA);
        fieldFormat.put("ben_country", REFDATA);
        fieldFormat.put("ben_branch_country", REFDATA);
        fieldFormat.put("credit_note", CREDITNOTE);
        fieldFormat.put("credit_discount_code_id", CREDITFIELDS);
        fieldFormat.put("credit_discount_gl_code_id", CREDITFIELDS);
        fieldFormat.put("credit_discount_comments_id", CREDITFIELDS);
        fieldFormat.put("ben_branch_code", BRANCHCODE);
        fieldFormat.put("send_to_supplier_date", DATE);
        fieldFormat.put("buyer_acct_num", AN);
        fieldFormat.put("buyer_acct_currency", "CURR");
        fieldFormat.put("ben_bank_sort_code", BANKSORTCODE);
    }

    static final Hashtable<String, String> replaceDate = new Hashtable<>();

    static {
        replaceDate.put("M-d-yyyy", "MM-dd-yyyy");
        replaceDate.put("M-d-yy", "MM-dd-yy");
        replaceDate.put("M/d/yyyy", "MM/dd/yyyy");
        replaceDate.put("M/d/yy", "MM/dd/yy");
    }

    static final List<String> optioalFieldsToTradeLoan = new ArrayList<>();

    static {
        optioalFieldsToTradeLoan.add("issue_date");
        optioalFieldsToTradeLoan.add("due_date");
    }

    private static String[] expand(String[] array, int size) {
        String[] temp = new String[size];
        System.arraycopy(array, 0, temp, 0, array.length);
        for (int j = array.length; j < size; j++) {
            temp[j] = "";
        }
        return temp;
    }

    /**
     * Parse one invoice line item.
     *
     * @param currentInvoiceLineItem
     * @param resourceManager
     * @param mediatorServices
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    public static String[] parseInvoiceLineItem(String invoiceDataDelimiter,
            String currentInvoiceLineItem, int file_fields_number) {
        if (StringFunction.isNotBlank(invoiceDataDelimiter)) {
            invoiceDataDelimiter = delimiters.get(invoiceDataDelimiter);
            String endValue = currentInvoiceLineItem.substring(currentInvoiceLineItem.length() - 2, currentInvoiceLineItem.length());
            boolean isEndValueBlank = delimitersEndValue.contains(endValue);
            String[] origInvoice = currentInvoiceLineItem
                    .split(invoiceDataDelimiter, file_fields_number);// T36000014557 Rel 8.2 RKAZI - Changed.
            String[] origInvoiceRecord = isEndValueBlank ? expand(origInvoice, origInvoice.length) : origInvoice;// T36000014557 Rel 8.2 RKAZI - Changed
            // If there is only one field, the format cannot be valid. The user
            // probably has used wrong delimiter.
            if (origInvoiceRecord.length == 1) {
                return null;
            }
            String[] invoiceRecord = new String[origInvoiceRecord.length];
            int numberOfFields = origInvoiceRecord.length;
            if (numberOfFields > invoiceRecord.length) {
                numberOfFields = invoiceRecord.length;
            }
            System.arraycopy(origInvoiceRecord, 0, invoiceRecord, 0, numberOfFields);

            int fromIndex = numberOfFields;
            int toIndex = invoiceRecord.length;
            if (fromIndex <= toIndex) {
                Arrays.fill(invoiceRecord, fromIndex, toIndex, "");
            }

            return invoiceRecord;
        } else {
            return null;
        }
    }

    /**
     * Gets the Field count on InvoiceDefinition
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid Invoice Data file
     * (true - valid false - invalid)
     */
    public static boolean getInvoiceDefFieldCount(
            InvoiceDefinitionWebBean invoiceDefinition,
            MediatorServices mediatorServices, HashMap<String, Integer> details)
            throws AmsException, RemoteException {

        validDateFormat = invoiceDefinition.getAttributeWithoutHtmlChars("date_format");

        int index = 1;

        String fieldName = null;
        boolean hasMoreFields = true;
        while (hasMoreFields && index <= TradePortalConstants.INV_SUMMARY_NUMBER_OF_FIELDS) {
            fieldName = invoiceDefinition
                    .getAttributeWithoutHtmlChars("inv_summary_order" + index);
            if ((fieldName != null) && (!fieldName.equals(""))) {
                index++;
            } else {
                hasMoreFields = false;
            }
        }

        int index1 = 1;
        boolean hasMoreGoodsFields = true;
        while (hasMoreGoodsFields && index1 <= TradePortalConstants.INV_GOODS_NUMBER_OF_FIELDS) {
            fieldName = invoiceDefinition.getAttributeWithoutHtmlChars("inv_goods_order"
                    + index1);
            if ((fieldName != null) && (!fieldName.equals(""))) {
                index1++;
            } else {
                hasMoreGoodsFields = false;
            }
        }
        int index2 = 1;
        boolean hasMoreLineFields = true;
        while (hasMoreLineFields && index2 <= TradePortalConstants.INV_LINE_ITEM_NUMBER_OF_FIELDS) {
            fieldName = invoiceDefinition
                    .getAttributeWithoutHtmlChars("inv_line_item_order" + index2);
            if ((fieldName != null) && (!fieldName.equals(""))) {
                index2++;
            } else {
                hasMoreLineFields = false;
            }
        }
		//Rel8.2 CR-741 - AAlubala - Credit Note fields - START
        //INV_CREDIT_LINE_ORDER_FIELDS
        int index3 = 1;
        boolean hasMoreCreditFields = true;
        while (hasMoreCreditFields && index3 <= TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS) {
            fieldName = invoiceDefinition
                    .getAttributeWithoutHtmlChars("inv_credit_order" + index3);
            if ((fieldName != null) && (!fieldName.equals(""))) {
                index3++;
            } else {
                hasMoreCreditFields = false;
            }
        }

        int invDefFieldCount = index + index1 + index2 + index3 - 4;
        //CR-741 - END
        details.put("invDefFieldCount", invDefFieldCount);

        return true;
    }

    /**
     * Validates Size and Format of fields and creates error message if wrong
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid Invoice Data file
     * (true - valid false - invalid)
     */
    private boolean validateFieldSizeAndFormat(String fieldName, String value) throws AmsException {
//		String[] substitutionValues = { "", "" };
//		substitutionValues[0] = bundle.getString("InvoiceUploadRequest."
//				+ fieldName);
//		substitutionValues[1] = String.valueOf(currentLineNum + 1);
        String format = fieldFormat.get(fieldName);
        if (!StringFunction.isBlank(format) && StringFunction.isNotBlank(value)) {
            Matcher m = null;
            if (format.equals(AN)) {
				//IR 33335- start
                //m = ALPHANUMERIC.matcher(value);
                InstrumentServices.checkForInvalidSwiftCharacters(value, mediatorServices.getErrorManager(), fieldName);
                return isInvalidSwiftErrorCode(mediatorServices.getErrorManager());
                //IR 33335- end
            } else if (format.equals(NUMERIC)) {
                m = NUM.matcher(value);
            } else if (format.equals(ALP)) {
                m = ALPHA.matcher(value);
            } else if (StringFunction.isNotBlank(value) && format.equals(AMTNUM)) {
                m = AMOUNT.matcher(value.trim());
            } else if (format.equals("GOODS")) {
                if (value.contains("<>")) {
                    return false;
                }

            } else if (format.equals(DATE)) {
				//SHR IR DAUM110857648 start
                // DK IR T36000014616 Rel8.2 03/12/2013 Starts
                try {
                    //IR T36000017283 - don not replace validDateFormat,instead use newDate variable
                    String newDate = validDateFormat;
                    String oldDate = replaceDate.get(validDateFormat);
                    if (StringFunction.isNotBlank(oldDate) && StringFunction.isNotBlank(value)) {
                        try {
                            if ("M-d-yy".equals(validDateFormat) || "M/d/yy".equals(validDateFormat)) {
                                String temp = value.substring(value.length() - 3, value.length() - 2);
                                if (!("/".equals(temp) || "-".equals(temp))) {
                                    return false;
                                }
                            } else if ("M-d-yyyy".equals(validDateFormat) || "M/d/yyyy".equals(validDateFormat)) {

                                String temp1 = value.substring(value.length() - 5, value.length() - 4);
                                if (!("/".equals(temp1) || "-".equals(temp1))) {
                                    return false;
                                }
                            }
                        } // DK IR T36000014616 Rel8.2 03/12/2013 Ends
                        catch (Exception e) {
                            return false;
                        }
                        dateReplaced = "replaced";
                        newDate = oldDate;
                    }

                    //SHR IR DAUM110857648 end
                    SimpleDateFormat formatter = new SimpleDateFormat(
                            newDate);

					// just validate the format, do not save the changed format
                    //RKAZI REL 8.2 T36000014901 Start - added check not to parse or format in case value is blank and data is not required
                    if (StringFunction.isNotBlank(value)) {
                        // DK IR T36000014985 Rel8.2 03/20/2013 Starts
                        if (StringFunction.isBlank(oldDate) && value.trim().length() != newDate.length() && dateReplaced == null) {
                            logInvalidFieldFormatError(fieldName);
                            return false;
                        }
                        // DK IR T36000014985 Rel8.2 03/20/2013 Ends

                        formatter.setLenient(false);
                        Date valueDate = formatter.parse(value.trim());
                        value = valueDate.toString();
                    }
                    if (StringFunction.isBlank(value) && reqFields.contains(fieldName)) {
							// issue date and due date can be blank for both Payable and Receivables in case of Trade Loan.
                        // so if loan type is Trade Loan and issue and Due Date is blank, then also pass as valid. 
                        //for payables credit note there are optional fields
                        if ((TradePortalConstants.TRADE_LOAN.equals(getLoanType()) || isPayCreditNote) && optioalFieldsToTradeLoan.contains(fieldName)) {
                            return true;
                        }
                        return false;
                    }
                    //RKAZI REL 8.2 T36000014901 Start
                } catch (ParseException e) {
                    return false;
                }
                // Nar IR - MNUM061259346
            } else if ("INSTRUMENT_TYPE".equals(format)) {
                if (StringFunction.isNotBlank(value) && !ValidInvoiceInstrType.contains(value.toUpperCase())) {
                    return false;
                }
            } else if ("INVOICE_TYPE".equals(format)) {
                if (StringFunction.isNotBlank(value) && !ValidInvoiceType.contains(value.toUpperCase())) {
                    return false;
                }
            } else if ("CHARGE_TYPE".equals(format)) {
                if (StringFunction.isNotBlank(value) && !ValidChargeType.contains(value.toUpperCase())) {
                    return false;
                }
            } //IR T36000016317 - check for non-null value
            else if (StringFunction.isNotBlank(value) && "QUANTITY".equals(format)) { // Nar IR- LMUM062755984
                m = QTY.matcher(value.trim());
            } else if (REFDATA.equals(format)) {
                String tableType = "";
                if ("currency".equals(fieldName)) {
                    tableType = "CURRENCY_CODE";

                } else if ("ben_country".equals(fieldName) || "ben_branch_country".equals(fieldName)) {
                    tableType = "COUNTRY";
                } else {
                    tableType = fieldName.toUpperCase();
                }
                if (StringFunction.isNotBlank(value) && !ReferenceDataManager.getRefDataMgr().checkCode(tableType, value, mediatorServices.getCSDB().getLocaleName())) {
                    return false;
                }
            } else if (CREDITNOTE.equals(format)) {
                try {
                    String creditNoteText = invoiceDefinition.getAttributeWithoutHtmlChars("credit_note_ind_text");
                    creditNoteText = StringFunction.isBlank(creditNoteText) ? "" : creditNoteText;
                    if (StringFunction.isNotBlank(value) && !creditNoteText.equalsIgnoreCase(value)) {
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            } else if (BRANCHCODE.equals(format)) {  //DK T36000015537 Rel 8.2 End
                if (StringFunction.isNotBlank(value)) {
                    String sqlQuery = "select BANK_BRANCH_CODE from BANK_BRANCH_RULE  where BANK_BRANCH_CODE = ? ";

                    DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{value});
                    if (result == null) {
                        addError(fieldName, TradePortalConstants.INVALID_BANK_BRANCH_CODE);
                        return false;
                    }
                }
            } //MEer IR-36300 Checking Bank Sort code for unicode characters
            else if (BANKSORTCODE.equals(format)) {
                if (StringFunction.isNotBlank(value)) {
                    InstrumentServices.checkForInvalidSwiftCharacters(value, mediatorServices.getErrorManager(), bundle.getString("InvoiceUploadRequest." + fieldName));
                    return isInvalidSwiftErrorCode(mediatorServices.getErrorManager());
                }
            }
            if (m != null && !m.matches()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validates currency fields and creates error message if wrong
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not all currency fields are valid
     * (true - valid false - invalid)
     */
    public boolean validateCurrencyAmounts(String currency,
            String value, String alias) {
        ErrorManager errMgr = mediatorServices.getErrorManager();
        int previousErrorCount = errMgr.getErrorCount();

        try {
            TPCurrencyUtility.validateAmount(currency.toUpperCase(), value, alias, errMgr);
        } catch (AmsException e) {
            LOG.debug("Error while validateCurrencyAmounts");
            e.printStackTrace();
        }

		// If validateAmount found errors, error count will be higher than
        // before
        return previousErrorCount == errMgr.getErrorCount();
    }

    public int getCurrentLineNum() {
        return currentLineNum;
    }

    public void setCurrentLineNum(int currentLineNum) {
        this.currentLineNum = currentLineNum;
    }

    public InvoiceFileDetails getInstance() {
        return this;
    }

    public String getLocaleName() {
        return localeName;
    }

    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public Map<String, Integer> getFieldPositions() {
        return fieldPositions;
    }

    public int getFieldPosition(String fieldName) {
        return fieldPositions.get(fieldName) != null ? fieldPositions
                .get(fieldName) : -1;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public boolean isPayCreditNote() {
        return isPayCreditNote;
    }

    public void setPayCreditNote(boolean isPayCreditNote) {
        this.isPayCreditNote = isPayCreditNote;
    }

    /**
     * Returns a map with each fields position on the record as defined in
     * PurchaseOrderDefinition.
     *
     * @param purchaseOrderDefinition
     * @return
     */
    public Map<String, Integer> setFieldPositions(InvoiceDefinitionWebBean invoiceDefinition) {
        fieldPositions = new HashMap<>();
        if (invoiceDefinition != null) {
            boolean hasMoreFields = true;
            String fieldName = "";
            int index = 0;
            String attributeName = "inv_summary_order";

            while (hasMoreFields) {
               // try {
                    fieldName = invoiceDefinition
                            .getAttributeWithoutHtmlChars(attributeName + (index + 1));
               // } catch (Exception e) {
                    if(StringFunction.isBlank(fieldName)){
                    if ("inv_summary_order".equals(attributeName)) {
                        attributeName = "inv_goods_order";
                        index = 0;
                        continue;
                    } else if ("inv_goods_order".equals(attributeName)) {
                        attributeName = "inv_line_item_order";
                        index = 0;
                        continue;
                    } else if ("inv_line_item_order".equals(attributeName)) {
                        //AAlubala - Rel8.2 IR T36000015016 03/21/2013 - Add credit order fields
                        attributeName = "inv_credit_order";
                        index = 0;
                        continue;
                    } else if ("inv_credit_order".equals(attributeName)) {
                        hasMoreFields = false;
                    }
                }

                if (hasMoreFields && (fieldName != null)
                        && (!fieldName.equals(""))) {
                    fieldPositions.put(fieldName, fieldPositions.size());
                } else {
                    if ("inv_summary_order".equals(attributeName)) {
                        attributeName = "inv_goods_order";
                        index = 0;
                        continue;
                    } else if ("inv_goods_order".equals(attributeName)) {
                        attributeName = "inv_line_item_order";
                        index = 0;
                        continue;
                    } else if ("inv_line_item_order".equals(attributeName)) {
                        //AAlubala - Rel8.2 IR T36000015016 03/20/2013 - Add credit order fields
                        attributeName = "inv_credit_order";
                        index = 0;
                        continue;
                    } else if ("inv_credit_order".equals(attributeName)) {
                        hasMoreFields = false;
                    }
                }
                index++;
            }

        }
        return fieldPositions;
    }

    public InvoiceFileDetails getInstance(InvoiceDefinitionWebBean invoiceDefinition) {
        this.invoiceDefinition = invoiceDefinition;
        //this.mediatorServices = mediatorServices;
        this.setFieldPositions(invoiceDefinition);
        return this;

    }
    public void setMediatorServices(MediatorServices mediatorServices ){
    	this.mediatorServices= mediatorServices;
    }

    /**
     * Returns a list of fields defined on the PurchaseOrderDefinition.
     *
     * @param in
     * @return
     */
    public List<String> getDefinedFieldNames(String attributeName) {

        List<String> fieldsNamesList = new ArrayList<>();
        if (invoiceDefinition != null) {
            boolean hasMoreFields = true;
            String fieldName = "";
            int index = 1;

            while (hasMoreFields) {
               // try {
                    fieldName = invoiceDefinition
                            .getAttributeWithoutHtmlChars(attributeName + index);
             //   } catch (Exception e) {
                    if (StringFunction.isBlank(fieldName)) {
                    hasMoreFields = false;
                    continue;
                }

                if (StringFunction.isNotBlank(fieldName)) {

                    fieldsNamesList.add(fieldName);
                } else {
                    if ("inv_line_item_order".equals(attributeName)) {
                        hasMoreFields = false;
                    }
                }
                index++;
            }
        }

        return fieldsNamesList;
    }

    /**
     * Adds error to the mediator services. Used when only field name needs to
     * be set on the message.
     *
     * @param fieldName
     * @param errorCode
     */
    public void addError(String fieldName, String errorCode) {

        String[] substitutionValues = {"", ""};
        try {
            try {
                substitutionValues[0] = bundle
                        .getString("InvoiceUploadRequest." + fieldName);
            } catch (MissingResourceException e) {
                logDebug("Error logging erorr ..." + e.getStackTrace());
                substitutionValues[0] = "InvoiceUploadRequest."
                        + fieldName;
            }
            substitutionValues[1] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1, errorCode,
                    substitutionValues, null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    /**
     * Adds error to the mediator services. Used when field name and value needs
     * to be set on the message.
     *
     * @param fieldName
     * @param fieldValue
     * @param errorCode
     */
    public void addError(String fieldName, String fieldValue, String errorCode) {

        String[] substitutionValues = {"", ""};
        try {
            try {
                fieldName = bundle.getString("InvoiceUploadRequest."
                        + fieldName);
                substitutionValues[0] = fieldValue;
            } catch (MissingResourceException e) {
                logDebug("Error logging erorr ..." + e.getStackTrace());
                substitutionValues[0] = "InvoiceUploadRequest."
                        + fieldName;
            }
            substitutionValues[1] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1, errorCode,
                    substitutionValues, null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    /**
     * Adds error to the mediator services. Used when only line number needs to
     * be set on the error.
     *
     * @param errorCode
     */
    public void addError(String errorCode) {

        String[] substitutionValues = {""};
        try {
            substitutionValues[0] = String.valueOf(currentLineNum);
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1, errorCode,
                    substitutionValues, null);
        } catch (AmsException e) {
            logDebug("Error logging erorr ..." + e.getStackTrace());
        }
    }

    private void logMissingFieldError(String fieldName) {
        addError(fieldName, TradePortalConstants.INVOICE_FIELD_MISSING);

    }

    private void logInvalidFieldFormatError(String fieldName) {
        addError(fieldName, TradePortalConstants.INVALID_INVOICE_FIELD_FORMAT);
    }

    private void logInvalidFieldValueSize(String fieldName) {
        addError(fieldName, TradePortalConstants.INVALID_INVOICE_FIELD_FORMAT);
    }

    private void logDebug(String debugMessage) {
        mediatorServices.debug(debugMessage);
    }

    /**
     * Sets the passed in String Array values to the passed in field names on
     * the passed Object 'obj' in the fieldNames list after validating for size
     * and format. If size or format is not as per definition then logs error
     * and set the passed Object 'obj' as invalid. Returns a Map of fields.
     *
     * @param fieldNamesList
     * @param record
     * @param lineNum
     * @param obj
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    public Map<String, Object> setFieldValues(List<String> fieldNamesList,
            String[] record, int lineNum, Object obj) throws AmsException, RemoteException {
        Map<String, Object> fields = new HashMap<>();
        Field field = null;
        for (String fieldName : fieldNamesList) {
            try {
                field = obj.getClass().getDeclaredField(fieldName);

                int position = fieldPositions.get(fieldName) != null ? fieldPositions
                        .get(fieldName) : -1;
                if (position != -1) {
                    if (!validateAndSetFieldValue(field,
                            record[position].trim(), obj)) {
                        setObjectInvalid(field, fieldName, obj);
                    } else {
                        fields.put(fieldName, record[position].trim());
                    }
                }

            } catch (NoSuchFieldException | IllegalArgumentException e) {
                logMissingFieldError(fieldName);
                logDebug("Error logging erorr ..." + e.getStackTrace());
                setObjectInvalid(field, fieldName, obj);
            }
        }
		//Rel9.2 CR 914 - For Credit Notes, either SellerID or SellerName are mandatory. An error is thrown only if neither of them are present.
        //Hence evaludating and logging error after reading all the fields on the line.
        if (isPayCreditNote) {
            if (!(hasSellerID || hasSellerName)) {
                logInvalidFieldValueSize("seller_id");
                logInvalidFieldValueSize("seller_name");
            }
        }
        return fields;
    }

    /**
     * Validates field value format and sets the value to passed in field on the
     * Object 'obj'
     *
     * @param field
     * @param fieldValue
     * @param obj
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    private boolean validateAndSetFieldValue(Field field, String fieldValue,
            Object obj) throws AmsException, RemoteException {
        String fieldName = field.getName();
        if (fieldName.endsWith("_label")) {
            fieldName = "users_def_value";
        } else if (fieldName.endsWith("_value")) {
            fieldName = "users_def_value";
        } else if (fieldName.startsWith("prod") && fieldName.endsWith("_type")) {
            fieldName = "prod_chars_ud_val";
        } else if (fieldName.startsWith("prod") && fieldName.endsWith("_val")) {
            fieldName = "prod_chars_ud_val";
        }
        if (!validateFieldSize(fieldName, fieldValue, field.getName())) {
			//Rel9.2 CR 914 - For Credit Notes, either SellerID or SellerName are mandatory. An error is thrown only if neither of them are present.
            //Hence evaluating and logging error after reading all the fields on the line.
            if (!(isPayCreditNote && (fieldName.equals("seller_id") || fieldName.equals("seller_name")))) {
                logInvalidFieldValueSize(fieldName);
                return false;
            }
        }
        Class cl = field.getType();
        field.setAccessible(true);
        try {
            String type = cl.getSimpleName();
            if (StringFunction.isNotBlank(fieldValue)) {
                if (type.equalsIgnoreCase("long")) {
                    field.set(obj, Long.valueOf(fieldValue));
                } else if (type.equalsIgnoreCase("int")) {
                    field.set(obj, Integer.valueOf(fieldValue));
                } else if (type.equalsIgnoreCase("float")) {
                    field.set(obj, Float.valueOf(fieldValue));
                } else if (type.equalsIgnoreCase("double")) {
                    field.set(obj, Double.valueOf(fieldValue));
                } else if (type.equalsIgnoreCase("BigDecimal")) {
                    field.set(obj, new BigDecimal(fieldValue));
                } else if (type.equalsIgnoreCase("String")) {
                    field.set(obj, fieldValue);
                } else if (type.equalsIgnoreCase("Date")) {
                    String newDate = validDateFormat;
                    if (StringFunction.isNotBlank(replaceDate.get(validDateFormat))) {
                        newDate = replaceDate.get(validDateFormat);

                    }
                    SimpleDateFormat sd = new SimpleDateFormat(newDate);
                    sd.setLenient(false);
                    //rkazi Rel 8.2 T36000014901 Start - Check to avoid parsing empty value.
                    if (StringFunction.isNotBlank(fieldValue)) {
                        Date d = sd.parse(fieldValue);
                        field.set(obj, d);
                    }
                    //rkazi Rel 8.2 T36000014901 End
                }
            }
        } catch (ParseException | IllegalAccessException e) {
            logInvalidFieldFormatError(fieldName);
            logDebug("Error " + e.getMessage());
            return false;
        } catch (NumberFormatException e) {
            logInvalidFieldFormatError(fieldName);
            logDebug("Error " + e.getMessage());
            return false;
        } catch (IllegalArgumentException e) {
            logInvalidFieldFormatError(fieldName);
            logDebug("Error " + e.getMessage());
            return false;
        }

        return true;
    }

    private void setObjectInvalid(Field field, String fieldName, Object obj) {
        try {
            Field field1 = obj.getClass().getDeclaredField("isValid");
            field1.setAccessible(true);
            field1.set(obj, false);
        } catch (IllegalArgumentException e) {
            logInvalidFieldFormatError(fieldName);
            logDebug("Error " + e.getMessage());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            logMissingFieldError(fieldName);
            logDebug("Error " + e.getMessage());
        }

    }

    /**
     * Validates field size against predefined field sizes.
     *
     * @param fieldName
     * @param fieldValue
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    private boolean validateFieldSize(String fieldName, String fieldValue, String actualName) throws AmsException, RemoteException {
        //CR 913 - if default required fields like amount , currency etc are blank then error
        if (reqFields.contains(fieldName) && StringFunction.isBlank(fieldValue)) {
			//Rel9.2 CR 914 - For Credit Notes, either SellerID or SellerName are mandatory. An error is thrown only if neither of them are present.
            //Hence evaluating and logging error after reading all the fields on the line.
            if (isPayCreditNote) {
                if (fieldName.equals("seller_id") || fieldName.equals("seller_name")) {
                    return true;
                }
            }
            if (!((TradePortalConstants.TRADE_LOAN.equals(getLoanType()) || isPayCreditNote) && optioalFieldsToTradeLoan.contains(fieldName))) {
                return false;
            }
        }
        if (fieldValue != null) {

            int size = fieldSize.get(fieldName);
            // NAR IR-DEUM052936950   log error whether it is greater than DB size or doesn't have any data
            //SHR CR 708 Rel8.1.1 - if "data required" checkbox is checked in invoice def, then validate for the value
            if (fieldValue.length() > size || (!reqFields.contains(fieldName) && TradePortalConstants.INDICATOR_YES.equals(invoiceDefinition.getAttributeWithoutHtmlChars(actualName + "_data_req")) && fieldValue.trim().length() < 1)) {
                return false;
            }
			//Rel9.2 CR 914 - For Credit Notes, either SellerID or SellerName are mandatory. An error is thrown only if neither of them are present.
            //Hence storing the validation results for each of them separately to evaluate later.
            boolean result = validateFieldSizeAndFormat(fieldName, fieldValue);
            if (isPayCreditNote) {
                if (fieldName.equals("seller_id")) {
                    hasSellerID = result;
                }
                if (fieldName.equals("seller_name")) {
                    hasSellerName = result;
                }
            }
            return result;
        }

        return true;
    }

 
    /**
     * Verifies the Invoice Line Item. Doing the validation here instead of at
     * the business object user_validate() gives better error message
     * information.
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid INV Data file (true -
     * valid false - invalid)
     */
    public static boolean validateINVLineItem(String[] invRecord, MediatorServices mediatorServices, HashMap details)
            throws AmsException {

        int invDefFieldCount = (Integer) details.get("invDefFieldCount");

        if (invRecord.length != invDefFieldCount) {
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.INVALID_INVOICE_FIELD_COUNT);
            details.put("errText", mediatorServices);
            return false;
        }
        return true;
    }

    private static boolean isInvalidSwiftErrorCode(ErrorManager errMgr) {
        List<IssuedError> errorList = errMgr.getIssuedErrorsList();
        for (IssuedError err : errorList) {
            String code = err.getErrorCode();
            if (TradePortalConstants.INVALID_SWIFT_CHAR_SEMICOLON.equals(code) || TradePortalConstants.INVALID_SWIFT_CHARS.equals(code)) {
                return false;
            }
        }
        return true;
    }
    public static String getAttributeType(Hashtable attrTable, String attributeName) {
        try {
            Attribute attribute = (Attribute) attrTable.get(attributeName);
            // If the attribute is not found, throw a processing exception
            if (attribute == null) {
                throw new AmsException(attributeName);
            }
            return attribute.getAttributeType();
        }  catch (AmsException e) {
            return null;
        }

    }
    public static String getUniqueID(String[] poRecord, MediatorServices mediatorServices, InvoiceDefinitionWebBean object, int count, String orderField, String fieldName)
            throws AmsException {

        boolean hasMoreDataFields = true;
        String currentFieldName = null;
        int index = 1;

        while (hasMoreDataFields) {

            try {
                currentFieldName = object.getAttributeWithoutHtmlChars(orderField + index);
            } catch (Exception e) {
                // do nothing as field is not registered.
                hasMoreDataFields = false;
                continue;
            }
            if (StringFunction.isNotBlank(currentFieldName)) {
                String currentFieldValue = poRecord[index - 1];
                if (currentFieldName.equals(fieldName)) {
                    if (!validateMissingFields(currentFieldName, currentFieldValue, count, mediatorServices)) {
                        return null;
                    }
                    return currentFieldValue;
                }
            }
            index++;
        }
        return null;

    }

    private static boolean validateMissingFields(String filedName, String value, int lineNum, MediatorServices mediatorServices)
            throws AmsException {

        if (StringFunction.isBlank(value)) {
            String[] substitutionValues = {filedName, String.valueOf(lineNum + 1)};
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.INVOICE_FIELD_MISSING,
                    substitutionValues, null);
            return false;
        }
        return true;
    }
}
