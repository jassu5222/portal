package com.ams.tradeportal.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.ejb.EJBObject;
import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.CreditNotesGoods;
import com.ams.tradeportal.busobj.CreditNotesLineItemDetail;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceFileUpload;
import com.ams.tradeportal.busobj.InvoiceHistory;
import com.ams.tradeportal.busobj.InvoiceLineItemDetail;
import com.ams.tradeportal.busobj.InvoiceSummaryGoods;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TradePortalBusinessObject;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InvoiceLineItemObject;
import com.ams.tradeportal.busobj.util.InvoiceSummaryDataObject;
import com.ams.tradeportal.busobj.util.InvoiceSummaryGoodObject;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.ams.tradeportal.mediator.AutoCreateInvoicesMediator;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

public class InvoiceUploadFileProcessor {
private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(InvoiceUploadFileProcessor.class);

    //provide a upper limit for duration seconds to avoid failing the entire record if calculations are out of bounds	
    private static final long MAX_DURATION_SECONDS = 9999999999L;

    private final String ejbServerLocation;
    private final ClientServerDataBridge csdb;
    private final long invoiceFileOid;

    private String process_id = null;
    private File invoiceFile = null;
    private String fileName = null;
    private String corpOrgOID = null;
    private Map uploadLog = new HashMap();
    private Date valStartTime = null;
    private InvoiceFileUpload invoiceUpload = null;
    private ServerDeploy serverDeploy = null;
    private CorporateOrganizationWebBean corpOrg = null;
    private String dateFormat = null;
    private SimpleDateFormat sd = null;
    private String delimiterChar = null;
    private int total_invoices = 0;
    private String buyer_name = null;
    private String buyer_id = null;
    private String tradingPartnerName = null;
    private String currencyUsed = null;
    private BigDecimal totalAmount = BigDecimal.ZERO;
    private boolean isIndividualInvoiceAllowed = false;
    private boolean isSameCurrency = true;
    private int lineNum = 0;
    private int failed = 0;
    private boolean hasError = false;
    private HashMap<String,List<IssuedError>> allERRDetails = new HashMap<>();
    private Map<String, InvoiceSummaryDataObject> invSummDataMap = new HashMap<>();
    private Map<String, Object> invSummObjMap = new HashMap<>();
    Set<String> failedInvSet = new HashSet<>();
    Set<String> endToEndIDSet = new HashSet<>();
    private Map<String, Integer> endToEndIDMap = new HashMap<>();
    private Map<String, Date> dueDateMap = new HashMap<>();
    private Map<String, Date> paymentDateMap = new HashMap<>();
    private Map<String, String> paymentMethodMap = new HashMap<>();
    private SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

    private final String folder = TradePortalConstants.getPropertyValue(
            "AgentConfiguration", "uploadFolder", "c:/temp/");
    protected static final String loggerCategory = TradePortalConstants
            .getPropertyValue("AgentConfiguration", "loggerServerName",
                    "NoLogCategory");
    private boolean hasOneValidInvAtleast = false;
    String invUploadDataOption = "";
    String invUploadInstrOption = null;
    String invLoanType = null;
    String timeZone = null;
    protected boolean _isInvoiceFile = false;
    private String amount = "";
    ArMatchingRule tpMarginRules = null;
    InvoiceDefinitionWebBean invoiceDefinition = null;
    private String securityRights = null;
    private boolean hasOneEndToEndIDAtleast = false;
    private List<String> invSummaryFieldNamesList = null;
	private List<String> invSummaryGoodsFieldNamesList = null;
	private List<String> invLineItemFieldNamesList = null;
	private List<String> invCreditFieldNamesList = null;

    private PropertyResourceBundle errorbundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("ErrorMessages");
    HashMap<String, String> paymentRuleCache = new HashMap<>();
    Map<String, Map<String,String>> reportinCodeCache = new HashMap<>();

    public InvoiceUploadFileProcessor(String process_id, long oidLong,
            String ejbServerLocation, ClientServerDataBridge csdb) {
        this.ejbServerLocation = ejbServerLocation;
        this.csdb = csdb;
        this.invoiceFileOid = oidLong;
        this.process_id = process_id;
        try{
        serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(ejbServerLocation, "ServerDeploy");
        }catch(Exception e){
        	logError("Unable to obtain ServerDeploy", e);
        }
    }

    /**
     * Processes uploaded Invoice file saved in temporary folder.
     *
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws IOException
     */
    public boolean processInvoiceFile() throws AmsException,
            IOException {

        String sLine = null;
        BufferedReader reader = null;
        String[] record = null;
        List<String> currencyList = new ArrayList<>();
        Set<String> invNumSet = new HashSet<>();
        ArrayList<String> endToEndIdList = new ArrayList<>();

        try {
            invoiceUpload = (InvoiceFileUpload) EJBObjectFactory
                    .createClientEJB(ejbServerLocation, "InvoiceFileUpload",
                            invoiceFileOid);
            //capture the pack end date/time
            String datetime_process_start = "";
            try {
                datetime_process_start = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)); 
//                		DateTimeUtility.getGMTDateTime(false, false);
            } catch (AmsException e) {
                //log and ignore - non-critical processing
                logError("Problem obtaining process start date time. Ignoring.", e);
            }
            try {
                invoiceUpload.setAttribute("datetime_process_start", datetime_process_start);
            } catch (AmsException ex) {
                //log and ignore - non-critical processing
                logError("Problem saving pack start date time. Ignoring.", ex);
            }

            fileName = invoiceUpload.getAttribute("invoice_file_name");
            invoiceUpload.setClientServerDataBridge(csdb);
            PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle
                    .getBundle("TextResources");
            reader = getInputReader();
            sLine = reader.readLine();
            init();

            String invoiceFileDefName = invoiceUpload
                    .getAttribute("invoice_file_definition_name");
            corpOrgOID = invoiceUpload.getAttribute("owner_org_oid");
            
            BeanManager beanMgr = new BeanManager();
			 beanMgr.setServerLocation(ejbServerLocation);
            invoiceDefinition = getInvoiceFileDef(invoiceFileDefName, corpOrgOID,beanMgr);
            if(invoiceDefinition==null){
				logError("Invoice Definition is blank for invoiceFileDefName " + invoiceFileDefName);
            	return false;
            }
            
            InvoiceFileDetails.validDateFormat = invoiceDefinition.getAttributeWithoutHtmlChars("date_format");	
           
            corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization",csdb);
            corpOrg.getById(corpOrgOID);
                       String allowed = corpOrg.getAttributeWithoutHtmlChars("failed_invoice_indicator");
            String corpOrgTPSCustomerID = corpOrg.getAttributeWithoutHtmlChars("Proponix_customer_id");

            isIndividualInvoiceAllowed = TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(allowed); //IR 17776

            boolean isEndToEndIdProcessAllowed = TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttributeWithoutHtmlChars("allow_end_to_end_id_process"));
            String invoice_type_indicator = invoiceDefinition.getAttributeWithoutHtmlChars("invoice_type_indicator");
            String invUploadParams = invoiceUpload.getAttribute("invoice_upload_parameters");
            DocumentHandler invUploadParamsDoc = new DocumentHandler(invUploadParams, false);
            invUploadDataOption = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/InvPayDataOption");
            invUploadInstrOption = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/InvUploadInstrOption");
            invLoanType = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/loanType");
            timeZone = invUploadParamsDoc.getAttribute("/In/User/timeZone");
            ErrorManager errMgr = new ErrorManager();
            if (StringFunction.isBlank(invUploadDataOption)) {
                errMgr.issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INV_DATA_REQD);
                //RKAZI T36000014518 Rel 8.2 - Start
                allERRDetails.put("Invoice-Data", errMgr.getIssuedErrorsList());
                setStatusAndErrorMessage(allERRDetails,
                        invoiceUpload.getAttribute("invoice_file_upload_oid"));
                //RKAZI T36000014518 Rel 8.2 - End
                return false;
            }
            //SHR CR708 Rel8.1.1 change end
            securityRights = invUploadParamsDoc.getAttribute("/In/User/securityRights");
            MediatorServices mediatorServices = null;

            InvoiceFileDetails invoiceFileDetail=null;
            
            if (invoiceFileDetail == null) {
    			invoiceFileDetail = new InvoiceFileDetails().getInstance(invoiceDefinition);
    		}
            
            int invoiceIDFieldPosition =invoiceFileDetail.getFieldPosition("invoice_id");
            if (invoiceIDFieldPosition == -1) {
            	mediatorServices = new MediatorServices();
                mediatorServices.setCSDB(csdb);
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INVALID_INVOICE_FIELD_COUNT);
                return false;
            }
            int amountFieldPosition = invoiceFileDetail.getFieldPosition("amount");
            int creditNoteFieldPosition =  invoiceFileDetail.getFieldPosition("credit_note");
            int paymentDateFieldPosition =  invoiceFileDetail.getFieldPosition("payment_date");
            int paymentMethodFieldPosition =  invoiceFileDetail.getFieldPosition("pay_method");
            boolean allowPayCreditNoteUpload = TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttributeWithoutHtmlChars("allow_pay_credit_note_upload"));
            boolean isPayableProgram = TradePortalConstants.FOR_PAY_PROG.equals(invUploadDataOption);
            boolean endToEndIDReqd = false;
            String endToEndDataReq = invoiceDefinition.getAttributeWithoutHtmlChars("end_to_end_id_data_req");

            
            //performance fix start
            InvoiceProperties invProp = new InvoiceProperties();
            Long uploadUserOId = invoiceUpload.getAttributeLong("user_oid");
            Long invDefOid =Long.parseLong(invoiceDefinition.getAttributeWithoutHtmlChars("inv_upload_definition_oid"));
            Long uploadOwnerOrgOid = invoiceUpload.getAttributeLong("owner_org_oid");
            InvoiceSummaryDataObject invSummData = null;
           
    		
    		if (invSummaryFieldNamesList == null) {
    			invSummaryFieldNamesList = invoiceFileDetail.getDefinedFieldNames("inv_summary_order");
    		}
    		
    		if (invSummaryGoodsFieldNamesList == null) {
    			invSummaryGoodsFieldNamesList = invoiceFileDetail.getDefinedFieldNames("inv_goods_order");
    		}
    		
    		if (invLineItemFieldNamesList == null) {
    			invLineItemFieldNamesList = invoiceFileDetail.getDefinedFieldNames("inv_line_item_order");
    		}
    		
    		//AiA Rel8.2 IR T36000015016 - 03/21/2013 - Add Credit Codes list
    		if (invCreditFieldNamesList == null) {
    			invCreditFieldNamesList = invoiceFileDetail.getDefinedFieldNames("inv_credit_order");
    		}
    		String aliasAmount = bundle.getString("InvoiceUploadRequest.amount");
    		Map<String,String> paymentMap = new HashMap<>();
    		String payReq = invoiceDefinition.getAttributeWithoutHtmlChars("pay_method_req");
    		paymentMap.put("pay_method_req", payReq);
    		paymentMap.put("ben_country_req", invoiceDefinition.getAttributeWithoutHtmlChars("ben_country_req"));
    		if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator) && 
            		(TradePortalConstants.FOR_PAY_PROG.equals(invUploadDataOption) || TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS.equals(invUploadDataOption)) &&
            		TradePortalConstants.INDICATOR_YES.equals(payReq)){
    			paymentMap.put("reporting_code_1_data_req", invoiceDefinition.getAttributeWithoutHtmlChars("reporting_code_1_data_req"));
    			paymentMap.put("reporting_code_2_data_req", invoiceDefinition.getAttributeWithoutHtmlChars("reporting_code_2_data_req"));
    			String bankGroupOid = InvoiceUtility.fetchBankGroupForOpOrg(corpOrg.getAttributeWithoutHtmlChars("first_op_bank_org"));
    			paymentMap.put("bankGroupOid", bankGroupOid);
    		}
            //performance fix end
    		String tpName = invoiceUpload.getAttribute("trading_partner_name");
			while ((reader != null) && (sLine != null)) {
                lineNum++;
                mediatorServices = new MediatorServices();
                mediatorServices.setCSDB(csdb);
                // read the fields in the read line into a string array
                record = InvoiceFileDetails.parseInvoiceLineItem(delimiterChar, sLine, 100);

                if (record == null) {
                    mediatorServices.getErrorManager().issueError(
                            TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_FORMAT_NOT_VALID_DELIMITED);
                    return false;
                }

                String invNumber = record[invoiceIDFieldPosition];

                boolean isNewInvoice = invNumSet.add(invNumber);
                
                boolean isPayCreditNote = isPayCreditNote(amountFieldPosition, creditNoteFieldPosition, record);

                if (isPayCreditNote) {
                    int status = validatePayCreditNote(isPayCreditNote, mediatorServices, isPayableProgram, allowPayCreditNoteUpload, isNewInvoice, invNumber);
                    if (status == 1) {
                        break; // break when individual invoice upload is not allowed on a single invoice failure
                    } else if (status == 2) {
                        sLine = reader.readLine();
                        total_invoices++; //IR 35801 - Invoices with credit notes were not getting added to the count. This will ensure it gets added.
                        continue; // do not process the credit note
                    }
                  //  boolean hasOneCreditNoteAtleast = true;
                }
                if (isNewInvoice) {
                	invoiceFileDetail.setMediatorServices(mediatorServices);
                    invSummData = new InvoiceSummaryDataObject(invoiceDefinition, mediatorServices, csdb,invoiceFileDetail,
                            invSummaryFieldNamesList,invSummaryGoodsFieldNamesList,invLineItemFieldNamesList,invCreditFieldNamesList);
                    
                    invSummData.setNewINV(isNewInvoice);
                    invSummData.setP_invoice_file_upload_oid(Long
                            .valueOf(invoiceFileOid));
                    invSummData.setA_corp_org_oid(Long.valueOf(corpOrgOID));
                    invSummData.setA_user_oid(uploadUserOId);
                    invSummData.SetInvoice_classification(invoice_type_indicator); //SHR Cr708 Rel8.1.1
                    invSummData.setA_upload_definition_oid(invDefOid);

                    invSummData.setEjbServerLocation(this.ejbServerLocation);
                    total_invoices++;
                    invSummObjMap.put(invNumber, invSummData);
                    invSummData.setLoan_type(invLoanType);
                    invSummData.newProcessINV(record, lineNum, isPayCreditNote,aliasAmount);
                } else {
                    total_invoices++;
                    invSummData = (InvoiceSummaryDataObject) invSummObjMap
                            .get(invNumber);
                    invSummData.setNewINV(false);
                    invSummData.setEjbServerLocation(this.ejbServerLocation);
                    invSummData.newProcessINV(record, lineNum, mediatorServices, isPayCreditNote,aliasAmount);
                }
                boolean isValidInvoice = invSummData.isValid();
                String invoiceID = invSummData.getInvoice_id();
                if (StringFunction.isNotBlank(invSummData.getEnd_to_end_id())) {
                    endToEndIdList.add(invSummData.getEnd_to_end_id());
                    endToEndIDMap.put(invSummData.getEnd_to_end_id(), 0);
                }

                if (endToEndIdList.size() > 0) {
                    hasOneEndToEndIDAtleast = true;
                }
                if (endToEndIdList.size() > 0 && lineNum > endToEndIdList.size()) {
                    endToEndIDReqd = true;
                }

                if (isPayableProgram) {
                    if ((isEndToEndIdProcessAllowed || (TradePortalConstants.INDICATOR_YES).equals(endToEndDataReq))
                            && StringFunction.isBlank(invSummData.getEnd_to_end_id())) {
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.END_TO_END_ID_REQ, "", null);
                        endToEndIDReqd = true;
                        isValidInvoice = false;
                        isIndividualInvoiceAllowed = false;//error- fail whole file	
                    } else if (endToEndIDReqd) {
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.END_TO_END_ID_REQ, "", null);
                        isValidInvoice = false;
                        isIndividualInvoiceAllowed = false;//error- fail whole file	
                    }
                }

                if (isPayableProgram && !isPayCreditNote && StringFunction.isNotBlank(invSummData.getEnd_to_end_id())) {
                    if (!checkForSameDates(invSummData, paymentDateFieldPosition)) {
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.INVOICE_WITH_DIFF_DUE_OR_PAYMENT_DATE, "", null);
                        isValidInvoice = false;
                        isIndividualInvoiceAllowed = false;//error- fail whole file
                    }
                }
                //CR999 start
                if (isPayableProgram && StringFunction.isNotBlank(invSummData.getEnd_to_end_id()) && paymentMethodFieldPosition != -1) {
                    if (!checkForSamePaymentMethod(invSummData)) {
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.INVOICE_WITH_DIFF_PAYMENT_METHOD, "", null);
                        isValidInvoice = false;
                        isIndividualInvoiceAllowed = false;//error- fail whole file
                    }
                }//CR999 end

                // if it is new invoice, then only look for trading partner.
                if (isNewInvoice) {
                    Set<Boolean> tradePartnerRuleSet = new HashSet<>();
                    buyer_name = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator) ? invSummData.getBuyer_name() : invSummData.getSeller_name();
                    buyer_id = TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator) ? invSummData.getBuyer_id() : invSummData.getSeller_id();
                    // add the invoice ID for checking duplicate IDs.
                    amount = invSummData.getAmount() != null ? invSummData
                            .getAmount().toString() : "";
                    // get the trading partner related stuff start
                    if (StringFunction.isNotBlank(buyer_name)
                            || StringFunction.isNotBlank(buyer_id)) {
                        invSummData.setA_corp_org_oid(uploadOwnerOrgOid); // Nar -// IRKUUM040347622

                        //9.3 performance tuning start- fetch TpRule only if its not in cache                       
                        tpMarginRules =  (ArMatchingRule) invProp.getTradingPartnerRuleMap().get("buyer_name"+buyer_name+"buyer_id"+buyer_id);
                     
                        if( tpMarginRules==null){
                        	tpMarginRules = invSummData.getMatchingRule();
                        	if(tpMarginRules != null)
                        		invProp.getTradingPartnerRuleMap().put("buyer_name"+buyer_name+"buyer_id"+buyer_id,tpMarginRules);
                        	
                        }//9.3 performance tuning end
                        
                        if (tpMarginRules != null) {
                            //SHR CR708 Rel8.1.1 Start - get the isIndividualInvoice allowed from corporate profile not from TP rule.
                            tradingPartnerName = tpMarginRules
                                    .getAttribute("buyer_name");
                            
                            if (StringFunction.isNotBlank(tpName)) {
                                if (tpName.equals(tradingPartnerName)) {
                                    invoiceUpload.setAttribute("trading_partner_name",
                                            tradingPartnerName);
                                } else {
                                    invoiceUpload.setAttribute("trading_partner_name", null);
                                }
                            }
                        }
                        if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator)) {
                            invSummData.setBuyer_tps_customer_id(tpMarginRules != null ? tpMarginRules.getAttribute("tps_customer_id") : "");
                            invSummData.setSeller_tps_customer_id(corpOrgTPSCustomerID);
                        } else {
                            invSummData.setBuyer_tps_customer_id(corpOrgTPSCustomerID);
                            invSummData.setSeller_tps_customer_id(tpMarginRules != null ? tpMarginRules.getAttribute("tps_customer_id") : "");
                        }

                        //IR 22719 -- get fresh tp name always when there is no TP rule.                       
                        if (tpMarginRules == null || StringFunction.isBlank(tradingPartnerName)) {
                            tradingPartnerName = StringFunction.isNotBlank(buyer_name) ? buyer_name : buyer_id;//IR RAUM102980895
                        }
                        invSummData.setTp_rule_name(tradingPartnerName);
                       
                        //Rel 9.0 Code clean up start	- Moved validations to new methods
                        isValidInvoice = uploadValidation(invSummData, mediatorServices, isValidInvoice, invoice_type_indicator,invProp,paymentMap);

                        isValidInvoice = validateDates(invSummData, mediatorServices, isValidInvoice, invoice_type_indicator);
                        //Rel 9.0 Code clean up end

                        if (tradePartnerRuleSet.isEmpty()) {
                            tradePartnerRuleSet.add(isIndividualInvoiceAllowed);
                        }
                        //SHR IR - T36000005927
                    } else {
                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.INVALID_TRADING_PARTNER_RULE, "", null);

                        isValidInvoice = false;
                    }
                }
                //RKAZI Rel 8.2 T36000014506 - START
                if (!invSummData.isPayCreditNote() && !TradePortalConstants.FOR_PAY_PROG.equals(invUploadDataOption) && 
                		!InvoiceUtility.validatePaymentMethodBenCountry(invSummData, mediatorServices,paymentMap)) {
                    isValidInvoice = false;
                }
                //RKAZI Rel 8.2 T36000014506 - END

                if (!isValidInvoice && (hasOneEndToEndIDAtleast || endToEndIDReqd)) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.GENERIC_END_TO_END_ERROR_MSG, "", null);
                }

                if (isValidInvoice) { // if invoiceFile uploaded is valid
                    // check if same invoice
                    // if No then new Inv summary data is stored
                    if (StringFunction.isNotBlank(invoiceID) && isNewInvoice) {
                        hasSameCurrency(currencyList, invSummData);
                        currencyUsed = invSummData.getCurrency();
                    }
                    hasOneValidInvAtleast = true;
                    // if its valid add store it in a map
                    //SHR IR 6564 Start- even if invoice is success we have a message to display
                    invSummData.getErrorList().addAll(mediatorServices.getErrorManager()
                            .getIssuedErrorsList());
                    allERRDetails.put(invSummData.getInvoice_id(), invSummData.getErrorList());
                    //SHR IR 6564 End
                    invSummDataMap.put(invoiceID, invSummData);
                } else if (!isValidInvoice) {
                    //Rel9.2 CR 914A - Adding a Generic error message for files having E2E ID and have failed due to some reason
                    invSummData.getErrorList().addAll(mediatorServices.getErrorManager()
                            .getIssuedErrorsList());
                    allERRDetails.put(invSummData.getInvoice_id(), invSummData.getErrorList());
                    hasError = true;
                    if (failedInvSet.add(invoiceID)) {
                        failed++;  // add only failed invoices
                    }
                    if (isNewInvoice) //SHR IR - T36000005931
                    {
                        invSummDataMap.remove(invoiceID); // if it is not a valid invoice, then don't save in DB.					
                    }					//Srinivasu_D Fix of IR#T36000018079 06/25/2013 - to continue validating invoices even for failure
					/*	If the file contains credit notes and invoices with an End-to-End (E2E) ID,
                     * and one of the credit notes or invoices fails the upload processing validation, then the whole file will always be rejected,
                     *  regardless of the value of the setting
                     */
                    if (!isIndividualInvoiceAllowed || hasOneEndToEndIDAtleast) {
                        currencyUsed = null;
                        invSummDataMap.clear();
                        failed = 0;
                        total_invoices = 0;
                        //Since the whole file gets rejected, the count of total and failed invoices will be set to zero
                        //IR 17776 - break when individual invoice upload is not allowed on a single invoice failure
                        break;
                    }
                }
                // In case of Fail all invoices then need toclear the map to stop uploading in db
                if (failed > 0 && !isIndividualInvoiceAllowed) {
                    invSummDataMap.clear();
                }

                sLine = reader.readLine();

            }
            // if invSummDataMap is blank, it means no any invoce process successfully. so reset value to display nothing in currency column
            // and error staus should file failed.
            if (invSummDataMap.size() < 1) {
                currencyUsed = null;
                isSameCurrency = true;
                hasOneValidInvAtleast = false;
            }
            postProcess(bundle,invProp,invUploadParamsDoc);
            //include values for processing slas
            String datetime_process_end = "";
            try {
                datetime_process_end = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)); 
//                		DateTimeUtility.getGMTDateTime(false, false);
            } catch (AmsException e) {
                //log and ignore - non-critical processing
                logError("Problem obtaining process start date time. Ignoring.", e);
            }
            String dateCreated = invoiceUpload.getAttribute("creation_timestamp");
            String packStart = invoiceUpload.getAttribute("datetime_process_start");
            try {
                long processSeconds = TPDateTimeUtility.calculateDurationSeconds(packStart, datetime_process_end);
                if (processSeconds >= 0) {
                    if (processSeconds <= MAX_DURATION_SECONDS) {
                        String secondsStr = "" + processSeconds;
                        invoiceUpload.setAttribute("process_seconds", secondsStr);
                    } else {
                        //avoid failure if duration is out of bounds
                        logDebug("InvoiceFileProcessor.processInvoiceFile::process duration out of bounds. Ignoring.");
                    }
                } else {
                    //avoid failure if duration is negative
                    logDebug("InvoiceFileProcessor.processInvoiceFile::negative porcess duration. Ignoring.");
                }
            } catch (Exception t) { //i.e. illegal argument exception
                //just write out to the log (i.e. debug)
                logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate process seconds duration."
                        + "Ignoring. Exception:" + t.toString());
            }
            try {
                long totalProcessSeconds = TPDateTimeUtility.calculateDurationSeconds(dateCreated, datetime_process_end);
                if (totalProcessSeconds >= 0) {
                    if (totalProcessSeconds <= MAX_DURATION_SECONDS) {
                        String secondsStr = "" + totalProcessSeconds;
                        invoiceUpload.setAttribute("total_process_seconds", secondsStr);
                    } else {
                        //avoid failure if duration is out of bounds
                        logDebug("InvoiceFileProcessor.processInvoiceFile::total processing duration out of bounds. Ignoring.");
                    }
                } else {
                    //avoid failure if duration is negative
                   logDebug("InvoiceFileProcessor.processInvoiceFile::negative total processing duration. Ignoring.");
                }
            } catch (Exception t) { //i.e. illegal argument exception
                //just write out to the log (i.e. debug)
               logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate total process seconds duration."
                        + "Ignoring. Exception:" + t.toString());
            }

            if (invoiceUpload.save() < 0) {
                //logDebug("InvoiceFileProcessor.processInvoiceFile::Problem saving Invoice File Upload ");
            }

        } catch (Exception e) {
            logError("Error occured processing invoice file...", e);
            e.printStackTrace();
            rejectFileUplaod(false);
            return false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            cleanup();
        }

        return true;
    }

    private int validatePayCreditNote(boolean isPayCN, MediatorServices mediatorServices,
            boolean isPayableProgram, boolean allowPayCreditNoteUpload, boolean isNewInvoice, String invNumber) throws AmsException {

        if (isPayCN && (!isPayableProgram || !allowPayCreditNoteUpload)) {
            String code = !isPayableProgram ? TradePortalConstants.NO_PAY_INVOICES : TradePortalConstants.CREDIT_NOTE_UNABLE_ULOAD;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, code);
            allERRDetails.put(invNumber, mediatorServices.getErrorManager().getIssuedErrorsList());
            hasError = true;
            if (failedInvSet.add(invNumber)) {
                failed++;  // add only failed invoices
            }

            if (!isIndividualInvoiceAllowed) {
                currencyUsed = null;
                invSummDataMap.clear();

                return 1; //break; // break when individual invoice upload is not allowed on a single invoice failure
            } else {
                return 2;//continue; // do not process the credit note
            }
        }
        return 0;
    }

    private boolean isPayCreditNote(int amountPos, int creditNotePos, String[] record) {
        String creditNote = "";
        if (creditNotePos != -1) {
            creditNote = record[creditNotePos];
        }
        String amount = record[amountPos];
        LOG.debug("isPayCreditNote... checking if amount {} is negative ", amount);
        BigDecimal amt = new BigDecimal(amount);
        if (StringFunction.isNotBlank(creditNote) && amt != null && amt.signum() != -1) {
            amt = amt.negate();
        }
        return amt != null && amt.signum() == -1;
    }

    //Rel 9.0 code cleaning start - Moved validations to new methods...
    private boolean uploadValidation(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices,
            boolean isValidInvoice, String invoice_type_indicator,InvoiceProperties invProp,Map<String,String> paymentMap) throws AmsException, RemoteException {
        String linkInstrumentType = invSummData.getLinked_to_instrument_type();
        if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator)) {
            if (TradePortalConstants.INV_LINKED_INSTR_REC.equalsIgnoreCase(linkInstrumentType)
                    && !TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invUploadDataOption)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
                isValidInvoice = false;
            }
            if (InstrumentType.LOAN_RQST.equalsIgnoreCase(linkInstrumentType)
                    && TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invUploadDataOption)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
                isValidInvoice = false;
            }
			if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invUploadDataOption) && !InvoiceUtility.isValidRPMInstrumentAvailable(invSummData.getCurrency(), corpOrgOID, invProp.getInstrRPMMap())) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_REC_INSTRUMENT, corpOrg.getAttribute("name"), invSummData.getCurrency());
				isValidInvoice = false;
            }
        }
        if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator)) {
            if (InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(linkInstrumentType)
                    && InstrumentType.LOAN_RQST.equals(invUploadInstrOption)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
                isValidInvoice = false;
            }
            if (InstrumentType.LOAN_RQST.equalsIgnoreCase(linkInstrumentType)
                    && InstrumentType.APPROVAL_TO_PAY.equals(invUploadInstrOption)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
                isValidInvoice = false;
            }
            //CR 913 start- If user selects Payables Program on the upload screen perform specific validations

            if (TradePortalConstants.FOR_PAY_PROG
                    .equals(invUploadDataOption) && !invSummData.isPayCreditNote()) {

                isValidInvoice = validateIntegratedPayables(invSummData, mediatorServices, isValidInvoice, invProp);
                //Validate Buyer Account data
                isValidInvoice = validateBuyerAccount(invSummData, mediatorServices, isValidInvoice);

            } //IR 27181 - if instrument type is PYB in invoice file and upload option other than payables program is selected then error
            else if (TradePortalConstants.INV_LINKED_INSTR_PYB.equalsIgnoreCase(linkInstrumentType) && !TradePortalConstants.FOR_PAY_PROG
                    .equals(invUploadDataOption)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
                isValidInvoice = false;
            }
            //CR 913 end
        }
        isValidInvoice = validateCreditNote(invSummData, mediatorServices, isValidInvoice, invoice_type_indicator);
        isValidInvoice = validateE2EID(invSummData, mediatorServices, isValidInvoice, invoice_type_indicator);
        
        //Rel9.4 CR 1001 START
        if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator) && 
        		(TradePortalConstants.FOR_PAY_PROG.equals(invUploadDataOption) || TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS.equals(invUploadDataOption) &&
        				TradePortalConstants.INDICATOR_YES.equals(paymentMap.get("pay_method_req"))) ) {
        	isValidInvoice = validateReportingCodes(invSummData, isValidInvoice, mediatorServices,paymentMap);
        }
        //Rel9.4 CR 1001 END

        return isValidInvoice;
    }

    private boolean validateCreditNote(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices, boolean isValidInvoice,
            String invoice_type_indicator) throws AmsException, NumberFormatException, RemoteException {
        if (invSummData.isPayCreditNote()) {
            String sql = "select upload_credit_note_oid from  credit_notes where invoice_id = unistr(?)  and a_corp_org_oid =? ";
          
            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invSummData.getInvoice_id(), corpOrgOID});
          
            if ((resultSet != null) && (StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord(0)/UPLOAD_CREDIT_NOTE_OID")))) {
                isValidInvoice = false;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DUPLICATE_CREDIT_NOTE_ID);
            }
            
     
            sql = "select upload_invoice_oid from  invoices_summary_data where invoice_id = unistr(?)  and a_corp_org_oid =?  ";
             resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invSummData.getInvoice_id(), corpOrgOID});
          
             if ((resultSet != null) && (StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord(0)/UPLOAD_INVOICE_OID")))) {
                isValidInvoice = false;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVOICE_WITH_CREDIT_NOTE_ID_EXISTS);
            }
        }

        String creditNoteInd = null;
        if (StringFunction.isNotBlank(invSummData.getCredit_note())) {
            creditNoteInd = invSummData.getCredit_note();
            if (amount == null) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.CREDIT_NOTE_AMOUNT_MISSING_ERROR, "", null);
                isValidInvoice = false;
            } else if (amount != null) {
                String instType = invSummData.getLinked_to_instrument_type();
                String dbInv = null;
                String dbInstType = null;

                if (StringFunction.isNotBlank(invSummData.getCredit_note()) && (amount != null && !amount.startsWith("-"))) {
                    amount = "-" + amount;
                }

                if (!invSummData.isPayCreditNote() && (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator)
                        || TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator))) {
                    if (StringFunction.isBlank(instType)) {
                        DocumentHandler resultSet = getInstrumentTypeForInvoice(invSummData, invoice_type_indicator, tradingPartnerName);
                        if (resultSet != null) {
                            List<DocumentHandler> list1 = resultSet.getFragmentsList("/ResultSetRecord");
                            int i = list1.size();
                            if (i > 0) {
                                dbInv = (list1.get(0)).getAttribute("/INVOICE_ID");
                                dbInstType = (list1.get(0)).getAttribute("/LINKED_TO_INSTRUMENT_TYPE");
                            }
                        }

                        if (StringFunction.isNotBlank(dbInstType) && (InstrumentType.LOAN_RQST.equals(dbInstType) || InstrumentType.APPROVAL_TO_PAY.equals(dbInstType))) {

                            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.CREDIT_NOTE_LRQ_ATP_ERROR, "", null);
                            isValidInvoice = false;

                        } else if (StringFunction.isNotBlank(dbInstType) && TradePortalConstants.REC.equals(dbInstType)) {

                            if (!validateIfDuplicateInvoiceFoundInDB(invSummData, buyer_name, mediatorServices)) {
                                isValidInvoice = false;
                            }
                        } else if (StringFunction.isBlank(dbInstType) && StringFunction.isBlank(dbInv)) {

                            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.INVOICE_DOESNOT_EXISTS, "", null);
                            isValidInvoice = false;

                        } else if (StringFunction.isBlank(dbInstType)) {

                            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.CREDIT_NOTE_LRQ_ATP_ERROR, "", null);
                            isValidInvoice = false;

                        }
                    } else if (InstrumentType.LOAN_RQST.equals(instType) || InstrumentType.APPROVAL_TO_PAY.equals(instType)) {

                        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.CREDIT_NOTE_LRQ_ATP_ERROR, "", null);
                        isValidInvoice = false;

                    }
                }
            }
        } else if (StringFunction.isBlank(invSummData.getCredit_note())) {
            if (amount != null && amount.startsWith("-")) {

                creditNoteInd = TradePortalConstants.INDICATOR_YES;
                if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator) && TradePortalConstants.REC.equals(invSummData.getLinked_to_instrument_type())) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.CREDIT_NOTE_MISSING_REC_ERROR, invSummData.getInvoice_id(), null);
                    isValidInvoice = false;

                } else if (!invSummData.isPayCreditNote()) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.AMOUNT_NEGATIVE_ERROR, "", null);
                    isValidInvoice = false;

                }
            }
        }
        if (StringFunction.isNotBlank(invSummData.getCredit_note()) && amount != null) {

            if (TradePortalConstants.REC.equals(invSummData.getLinked_to_instrument_type())) {
                if (!validateIfDuplicateInvoiceFoundInDB(invSummData,
                        buyer_name, mediatorServices)) {

                    isValidInvoice = false;

                }
            }
        }
        if (StringFunction.isNotBlank(invSummData.getCredit_note())) {
            if (InstrumentType.LOAN_RQST.equals(invSummData.getLinked_to_instrument_type())
                    && !TradePortalConstants.TRADE_LOAN.equals(invLoanType)) {
                //SHR - IR LUUM050946882 start - check if issue date is greater than due date
                if (invSummData.getIssue_date() != null && invSummData.getDue_date() != null) {
                    if (invSummData.getIssue_date().after(invSummData.getDue_date())) {
                        mediatorServices
                                .getErrorManager()
                                .issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.INVALID_ISSUE_DATE,
                                        "", null);
                        isValidInvoice = false;
                    }
                }
                if (invSummData.getIssue_date() == null || invSummData.getDue_date() == null) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_LOAN_ISSUE_DUE_DATES_MISSING, "", null);
                    isValidInvoice = false;
                }
            }
        }
        return isValidInvoice;
    }

    /**
     * This method will validate if an uploaded file contains E2E ID, which has
     * already been uploaded and processed. If so, an error is thrown.
     *
     * @param invSummData
     * @param mediatorServices
     * @param isValidInvoice
     * @param invoice_type_indicator
     * @return
     * @throws AmsException
     * @throws NumberFormatException
     * @throws RemoteException
     */
    private boolean validateE2EID(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices, boolean isValidInvoice,
            String invoice_type_indicator) throws AmsException, NumberFormatException, RemoteException {
        if (StringFunction.isNotBlank(invSummData.getEnd_to_end_id())) {
            int countE2E = 0;
            StringBuilder sqlE2E = new StringBuilder();
            String e2eID = StringFunction.toUnistr(SQLParamFilter.filter(invSummData.getEnd_to_end_id()));
            sqlE2E.append(" end_to_end_id = ? and a_corp_org_oid = ? ");
            if (invSummData.isPayCreditNote()) {
                countE2E = DatabaseQueryBean.getCount("upload_credit_note_oid", "credit_notes", sqlE2E.toString(), false, new Object[]{e2eID, corpOrgOID});
            } else {
                sqlE2E.append(" and invoice_status not in (?, ?) "); // e2d is valid if existing invoice is either AVA or Deleted status
                countE2E = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", sqlE2E.toString(),
                        false, new Object[]{e2eID, corpOrgOID, "AVAILABLE_FOR_AUTHORIZATION", "DELETED"});
            }
            if (countE2E > 0) {
                isValidInvoice = false;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.END_TO_END_ID_EXISTS_ERROR_MSG);
            }
        }
        return isValidInvoice;
    }

    private boolean validateDates(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices, boolean isValidInvoice,
            String invoice_type_indicator) throws AmsException, RemoteException {
        if (TradePortalConstants.GROUP_PO_LINE_ITEMS.equals(invUploadDataOption)
                && InstrumentType.LOAN_RQST.equals(invUploadInstrOption) && TradePortalConstants.TRADE_LOAN.equals(invLoanType)) {

            //Case 2 ->  Set Issue date if it is null
            if (invSummData.getIssue_date() == null) {
                Date todayDate =  DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate());
//                		GMTUtility.getGMTDateTime();
                invSummData.setIssue_date(todayDate);
            } //IR T36000015470 start - if issue date is either before or after business date then insert a warning message
            else {
                Date todayDate =  DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate()); 
//                		GMTUtility.getGMTDateTime();
                //IR 22691 start- compare dates without time
                if (!TPDateTimeUtility.isSameDay(todayDate, invSummData.getIssue_date())) {
                    //if(invSummData.getIssue_date().before(todayDate) || invSummData.getIssue_date().after(todayDate)){
                    //IR 22691 end
                    IssuedError ie = ErrorManager.findErrorCode(TradePortalConstants.INVALID_ISSUE_DATE_INFO, csdb.getLocaleName());
                    if (ie != null) {
                        String errMsg = ie.getErrorText();
                        insertToInvErrorLog(invSummData.getInvoice_id() + "-" + invSummData.getTp_rule_name(), invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);
                    }
                }
            }
            //IR T36000015470 end
            //Case 3 ->  If temp Date is null then set Due_date = Issue_Date+1
            if (invSummData.getDue_date() == null) {

                Calendar cal = Calendar.getInstance();
                cal.setTime(invSummData.getIssue_date());
                cal.add(Calendar.DATE, 1);
                Date newDate = cal.getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                String fDate = formatter.format(newDate);
                Date dt = TPDateTimeUtility.getLocalDateTime(fDate, timeZone);

                invSummData.setDue_date(dt); //issues_date+1

            }
            //Case 1 -> Determine if payment date is allowed or not else set temp date
            if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttributeWithoutHtmlChars("use_payment_date_allowed_ind")) && invSummData.getPayment_date() != null) {

                if (invSummData.getPayment_date().before(invSummData.getIssue_date())) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_LOAN_PAYMENT_DATE, "", null);
                    isValidInvoice = false;
                }
            } else if (TradePortalConstants.INDICATOR_NO.equals(corpOrg.getAttributeWithoutHtmlChars("use_payment_date_allowed_ind")) && invSummData.getPayment_date() != null) {

                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.PAYAMENT_DATE_NOT_ALLOW_TRADE_LOANS, "", null);
                isValidInvoice = false;
            }

            //Case 4 -> due date not less than issue date
            if (invSummData.getDue_date() != null && invSummData.getIssue_date() != null) {
                if (invSummData.getDue_date().before(invSummData.getIssue_date())) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_LOAN_PAYMENT_DATE, "", null);
                    isValidInvoice = false;
                }
            }
        } else if (InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(invSummData.getLinked_to_instrument_type())) {
			LOG.debug("else issue date validation...invoice_type_indicator {}", invoice_type_indicator);
            //SHR - IR LUUM050946882 - end
            //Srini_D IR#T36000006373 - 10/11/2012 payment_day_allow changed
            if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator) && invSummData.getPayment_date() != null) {
                if (TradePortalConstants.INDICATOR_NO.equals(corpOrg.getAttributeWithoutHtmlChars("payment_day_allow"))) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.PAYMENT_DATE_NOT_ALLOWED, "", null);
                    isValidInvoice = false;
                } //SHR IR 6564 start
                else if (invSummData.getDue_date() != null && invSummData.getPayment_date() != null) {
                    validatePaymentDate(invSummData, mediatorServices);
                }//SHR IR 6564 end
            }

            //SHR - IR MJUM050951373 start - check if issue date is greater than payment date
            if (invSummData.getIssue_date() != null && invSummData.getPayment_date() != null) {
                if (invSummData.getPayment_date().before(invSummData.getIssue_date())) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_INV_PAYMENT_DATE, "", null);
                    isValidInvoice = false;
                }
            }
            if (invSummData.getDue_date() != null && invSummData.getIssue_date() != null) {
                if (invSummData.getDue_date().before(invSummData.getIssue_date())) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_LOAN_PAYMENT_DATE, "", null);
                    isValidInvoice = false;
                }
            }

            //SHR - IR MJUM050951373 end
        }
        return isValidInvoice;
    }
    //code cleaning end

    private boolean validateIntegratedPayables(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices,
            boolean isValidInvoice, InvoiceProperties invProp) throws AmsException, RemoteException {
        //For Payables Prgm, if instrument type is different in invoice file then fail. 
        //if instrument type is blank then set it to "PYB"
        if (StringFunction.isBlank(invSummData.getLinked_to_instrument_type())) {
            invSummData.setLinked_to_instrument_type(TradePortalConstants.INV_LINKED_INSTR_PYB);
        } else if (!TradePortalConstants.INV_LINKED_INSTR_PYB.equalsIgnoreCase(invSummData.getLinked_to_instrument_type())) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.MISMATCH_INST_TYPE_USAGE, "", null);
            isValidInvoice = false;
        }
        isValidInvoice = validateSupplierDate(invSummData, mediatorServices,
                isValidInvoice);

        String sql = "select upload_credit_note_oid from credit_notes where a_corp_org_oid = ? and invoice_id = unistr(?) ";
        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{corpOrgOID, invSummData.getInvoice_id()});
          if ((resultSet != null) && (StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord(0)/UPLOAD_CREDIT_NOTE_OID")))) {
            isValidInvoice = false;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CREDIT_NOTE_WITH_INVOICE_ID_EXISTS);
        }

        //Replacement validation
         sql = "select upload_invoice_oid from invoices_summary_data where  a_corp_org_oid = ? and invoice_id = unistr(?) and invoice_status in (?,?,?)  ";
        List<Object> sqlParams = new ArrayList<>();
        sqlParams.add(corpOrgOID);
        sqlParams.add(invSummData.getInvoice_id());
        sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);
        sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH);
        sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
        
         resultSet = DatabaseQueryBean.getXmlResultSet(sql, false,sqlParams);
       // int count = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", sql, false, sqlParams);
        if ((resultSet != null) && (StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord(0)/UPLOAD_INVOICE_OID")))) {
        //if (count > 0) {
            isValidInvoice = false;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FAIL_REPLACEMENT_INVOCIE, invSummData.getInvoice_id());
        }

        //Payable Invoice instructions validation
        if (StringFunction.isBlank(invSummData.getPay_method())) {

            // Check if payment instruction exists for the invoice currency;
            boolean paymentInstructionsExits = false;
            if (tpMarginRules != null) { // means trading partner exists
                // first check in the cache
                if ("Y".equals(paymentRuleCache.get(tpMarginRules.getAttribute("ar_matching_rule_oid") + invSummData.getCurrency()))) {
                    paymentInstructionsExits = true;
                } else {
                    sql = "select payable_inv_pay_inst_oid from payable_inv_pay_inst where currency_code = ? and inv_pay_instruction_ind ='Y' and p_ar_matching_rule_oid = ? ";
                    resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{ invSummData.getCurrency(), tpMarginRules.getAttribute("ar_matching_rule_oid")});
                    if ((resultSet != null) && (StringFunction.isNotBlank(resultSet.getAttribute("/ResultSetRecord(0)/PAYABLE_INV_PAY_INST_OID")))) {
                        paymentInstructionsExits = true;
                        paymentRuleCache.put(tpMarginRules.getAttribute("ar_matching_rule_oid") + invSummData.getCurrency(), "Y");
                    }
                }
            }
            if (!paymentInstructionsExits) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.NO_PAYABLE_INVOICE_PAY_INSTR);
                isValidInvoice = false;
            }
        } else {
            String country = InvoiceUtility.fetchOpOrgCountryForPYBInstrument(invSummData.getCurrency(), corpOrg.getAttributeWithoutHtmlChars("organization_oid"), invProp.getOpOrgCountryForPYBMap());
            if (StringFunction.isBlank(country)
                    && !TradePortalConstants.PAYMENT_METHOD_CBFT.equals(invSummData.getPay_method())) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INCOMPLETE_PAY_MGMT, invSummData.getCurrency());
                return false;
            }
            boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethod(invSummData.getPay_method(), invSummData.getCurrency(),
                    country, invSummData.getBen_country(), mediatorServices.getErrorManager(), false, invSummData.getInvoice_id(), invProp.getPaymentMethodMap());
            boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformation(invSummData.getPay_method(), invSummData.getBen_branch_code(),
                    country, invSummData.getBen_bank_name(), invSummData.getBen_branch_add1(), mediatorServices.getErrorManager(), false, invSummData.getInvoice_id());
            if (!isPaymentMethodValid || !isBankBranchCodeValid) {
                isValidInvoice = false;
            }
        }
        return isValidInvoice;
    }

    private boolean validateSupplierDate(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices,
            boolean isValidInvoice) throws AmsException, RemoteException {

        Date payDate = invSummData.getPayment_date() != null ? invSummData.getPayment_date() : invSummData.getDue_date();
        if (invSummData.getSend_to_supplier_date() != null && payDate != null) {
            //if 'Send to Supplier Date' is greater  than or equal to the earliest of payment date/due date, OR 
            // if it is less than current system date then error
            if (!invSummData.getSend_to_supplier_date().before(payDate)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INVALID_SUPPLIER_DATE, "", null);
                isValidInvoice = false;
            }
            if (invSummData.getSend_to_supplier_date().before(DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate()))) {
//            		GMTUtility.getGMTDateTime()
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INVALID_SUPPLIER_DATE_WITH_SYS_DATE, "", null);
                isValidInvoice = false;
            }
        }
        return isValidInvoice;

    }

    // check if buyer account is valid customer�s list of accounts with currency
    private boolean validateBuyerAccount(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices,
            boolean isValidInvoice) throws AmsException {
        if ((StringFunction.isNotBlank(invSummData.getBuyerAcctNum()) && StringFunction.isBlank(invSummData.getBuyerAcctCurrency()))
                || (StringFunction.isBlank(invSummData.getBuyerAcctNum()) && StringFunction.isNotBlank(invSummData.getBuyerAcctCurrency()))) {
            isValidInvoice = false;
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.BUYER_ACCOUNT_CURR_REQUIRED);
        } else if (StringFunction.isNotBlank(invSummData.getBuyerAcctNum()) && StringFunction.isNotBlank(invSummData.getBuyerAcctCurrency())) {
            String accountSqlStr = "select account_oid, deactivate_indicator from account where account_number = ? and p_owner_oid = ? and currency = ? ";

            DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, invSummData.getBuyerAcctNum(), corpOrgOID, invSummData.getBuyerAcctCurrency());

            if ((acctResultSet == null) || (StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID")))) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_BUYER_ACCOUNT);
                isValidInvoice = false;
            } else if (TradePortalConstants.INDICATOR_YES.equals(acctResultSet.getAttribute("/ResultSetRecord(0)/DEACTIVATE_INDICATOR"))) {
                isValidInvoice = false;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DEACTIVATED_BUYER_ACCOUNT);
            }
        }
        return isValidInvoice;

    }

    

    private boolean validateIfDuplicateInvoiceFoundInDB(
            InvoiceSummaryDataObject invSummData, String tradingPartnerName,
            MediatorServices mediatorServices)
            throws NumberFormatException {
        try {

            // check in Receive Management Invoice table for duplicate invoice
            String val = validateIfDuplicateInvoiceInRMDB(invSummData
                    .getInvoice_id());
            BigDecimal amt = BigDecimal.ZERO;
            boolean isDuplicate = false;
            if (val != null) {
                String[] values = val.split(TradePortalConstants.PIPELINE);
                isDuplicate = Boolean.valueOf(values[0]);
                amt = new BigDecimal(values[1]);
            }

            // if not, then check in Uploaded invoice DB
            if (!isDuplicate) {
                val = validateIfDuplicateInvoiceInUploadDB(
                        invSummData.getInvoice_id(), tradingPartnerName);

                if (val != null) {
                    String[] values = val.split(TradePortalConstants.PIPELINE);
                    isDuplicate = Boolean.valueOf(values[0]);
                    amt = new BigDecimal(values[1]);
                }
            }

            // if no invoice but we have a credit then throw error
            if (!isDuplicate
                    && (amount != null && amount.startsWith(TradePortalConstants.NEGATIVE))) {
                String[] substitutionValues = {""};
                substitutionValues[0] = invSummData.getInvoice_id();
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.INVOICE_DOESNOT_EXISTS, substitutionValues, null);
                return false;
            }

            // if invoice exists for a credit then check the credit note will
            // not bring the balance of the invoice to zero or negative
            if (isDuplicate && (amount != null && amount.startsWith(TradePortalConstants.NEGATIVE))) {
                BigDecimal invamt = new BigDecimal(amount);

                if (!((amt.add(invamt)).compareTo(BigDecimal.ZERO) == 1)) {
                    String[] substitutionValues = {""};
                    substitutionValues[0] = invSummData.getInvoice_id();
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.INVALID_CREDIT_NOTE, substitutionValues, null);
                    return false;
                }
            }

        } catch (Exception e) {
            logError(
                    "Error occurred during saving Invoice Upload Error log ...",
                    e);
        }
        return true;
    }

    private String validateIfDuplicateInvoiceInRMDB(String invoiceID)
            throws NumberFormatException {

        if (StringFunction.isBlank(invoiceID)) {
            return null;
        }

        try {

            // Nar IR-DEUM062261626 Begin
            String sql = "SELECT INVOICE_OID,INVOICE_TOTAL_AMOUNT from INVOICE where INVOICE_REFERENCE_ID =unistr(?) AND A_CORP_ORG_OID =?";
            // Nar IR-DEUM062261626 End

            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, invoiceID, corpOrgOID);
            if (resultSet != null) {
                List<DocumentHandler> list1 = resultSet.getFragmentsList("/ResultSetRecord");
                int i = list1.size();
                BigDecimal tot = BigDecimal.ZERO;
                if (i > 0) {
                    StringBuilder val = new StringBuilder();
                    val.append(true);
                    val.append("|");
                    for (DocumentHandler aList1 : list1) {
                        BigDecimal amt = BigDecimal.ZERO;
                        if (StringFunction.isNotBlank(aList1
                                .getAttribute("/INVOICE_TOTAL_AMOUNT"))) {
                            amt = aList1.getAttributeDecimal("/INVOICE_TOTAL_AMOUNT");
                        }
                        tot = tot.add(amt);
                    }
                    val.append(tot);
                    return val.toString();
                }
            }

        } catch (AmsException e) {
            logError("Error occurred during saving Invoice Upload Error log ...", e);
        }

        return null;
    }

    private String validateIfDuplicateInvoiceInUploadDB(String invoiceID,
            String tradingPartnerName) throws NumberFormatException {

        if (StringFunction.isBlank(invoiceID)) {
            return null;
        }

        try {

            // ShilpaR- IR #NEUM040353686 modified sql to verify the
            // corporate customer
            // Nar IR-DEUM062261626 Begin
            String sql = "select upload_invoice_oid, amount from invoices_summary_data where invoice_id = unistr(?) and invoice_status != ?"
                    + " and  invoice_classification=? and p_invoice_file_upload_oid  in ( select invoice_file_upload_oid from invoice_file_uploads where ( trading_partner_name=?"
                    + " or trading_partner_name is null ) and a_owner_org_oid = ? )";
            //				 Nar IR-DEUM062261626 End

            List<Object> sqlParams = new ArrayList<>();
            sqlParams.add(invoiceID);
            sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED);
            sqlParams.add("R");
            sqlParams.add(tradingPartnerName);
            sqlParams.add(corpOrgOID);

            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
            if (resultSet != null) {
                List<DocumentHandler> list1 = resultSet.getFragmentsList("/ResultSetRecord");
                int i = list1.size();
                BigDecimal tot = BigDecimal.ZERO;
                if (i > 0) {
                    StringBuilder val = new StringBuilder();
                    val.append(true);
                    val.append("|");

                    for (DocumentHandler aList1 : list1) {
                        BigDecimal amt = BigDecimal.ZERO;
                        if (StringFunction.isNotBlank(aList1.getAttribute("/AMOUNT"))) {
                            amt = aList1
                                    .getAttributeDecimal("/AMOUNT");
                        }
                        tot = tot.add(amt);
                    }
                    val.append(tot);
                    return val.toString();
                }

            }
        } catch (AmsException e) {
            logError(
                    "Error occurred during saving Invoice Upload Error log ...",
                    e);
        }
        return null;
    }

    /**
     * This method returns the instrument type for the passing invoice id
     *
     * @param invSummData
     * @param tradingPartnerName
     * @return instrument type
     * @throws NumberFormatException
     * @throws RemoteException
     */
    private DocumentHandler getInstrumentTypeForInvoice(InvoiceSummaryDataObject invSummData, String invoiceTypeIndicator,
            String tradingPartnerName) throws NumberFormatException {

        String invoiceID = invSummData.getInvoice_id();
        DocumentHandler resultSet = null;

        try {

            if (StringFunction.isNotBlank(invoiceID)) {
                String sql = "select invoice_id,linked_to_instrument_type from invoices_summary_data where invoice_id = unistr(?) and invoice_status != ? "
                        + " and invoice_classification='R' and p_invoice_file_upload_oid  in ( select invoice_file_upload_oid from invoice_file_uploads where ( trading_partner_name = ?"
                        + " or trading_partner_name is null ) and a_owner_org_oid = ? ";
                List<Object> sqlParams = new ArrayList<>();
                sqlParams.add(invoiceID);
                sqlParams.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED);
                sqlParams.add(tradingPartnerName);
                sqlParams.add(corpOrgOID);
                resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, sqlParams);
            }

        } catch (AmsException e) {
            logError("Error occurred during saving Invoice Upload Error log ...", e);
        }

        return resultSet;
    }

    // save all the valid summaryData and lineItemDetail
    private void saveAll(InvoiceProperties invProp) throws AmsException, RemoteException {
        String creditNoteAvaDays = corpOrg.getAttributeWithoutHtmlChars("pay_credit_note_available_days");
        InvoiceSummaryDataObject invSummDataObj = null;
        if (!invSummDataMap.isEmpty()) {
            Collection<InvoiceSummaryDataObject> coll = invSummDataMap.values();
            Object[] invSummDataList = coll.toArray();
            Set<String> currencySet = new HashSet<>();
            Map currencyCalMap= new HashMap();
            Hashtable<String, Attribute> attrTab = new Hashtable<>();
            Hashtable<String, Attribute> crNoteAttTab = new Hashtable<>();
            Hashtable<String, Attribute> invAttTab = new Hashtable<>();
            Hashtable<String, Attribute> crNoteGoodsAttTab = new Hashtable<>();
            Hashtable<String, Attribute> invGoodsAttTab = new Hashtable<>();
            Hashtable<String, Attribute> crNoteLineAttTab = new Hashtable<>();
            Hashtable<String, Attribute> invLineAttTab = new Hashtable<>();
           String ownerOrgOid = invoiceUpload.getAttribute("owner_org_oid");
           String fileUplgOid =invoiceUpload.getAttribute("invoice_file_upload_oid");
           String defOid =invoiceDefinition.getAttributeWithoutHtmlChars("inv_upload_definition_oid");
           String invoice_type_indicator = invoiceDefinition.getAttributeWithoutHtmlChars("invoice_type_indicator");
           String userOid = invoiceUpload.getAttribute("user_oid");
            for (Object invSummDataList1 : invSummDataList) {
                invSummDataObj = (InvoiceSummaryDataObject) invSummDataList1;
                
                Set<InvoiceSummaryGoodObject> invGoodsSet = invSummDataObj
                        .getInvoiceSummaryGood();
                //validate total line amount
                if (!invSummDataObj.validateInvoiceLineItemAmount()) {

                    allERRDetails.put(invSummDataObj.getInvoice_id(),
                            invSummDataObj.getErrorList());
                    failed++;
                    hasError = true;
                    if (isIndividualInvoiceAllowed) {
                        continue;
                    } else {
                        break;
                    }
                }
                TradePortalBusinessObject invSummary = null;
                HashMap removeOids = new HashMap();
                boolean remove = true;
                boolean isPayCreditNote =invSummDataObj.isPayCreditNote();
                try {
                    //RKAZI 09/04/2012 CR-708 Rel 8.1.1 Start

                    String invoice_type = (String) (invSummDataObj.getFields().get("invoice_type"));

                    if (TransactionType.AMEND.equalsIgnoreCase(invoice_type)) {
                        invSummDataObj.setInvoice_type(TradePortalConstants.REPLACEMENT);
                    }

                        if (invSummDataObj.processAmendInvoices(invSummDataObj, removeOids, securityRights,isPayCreditNote,ownerOrgOid)) {
                            boolean isPaymentError = false;
                            if (invSummDataObj.getErrorList().size() == 1) {
                                IssuedError is = (IssuedError) invSummDataObj.getErrorList().get(0);
                                if (TradePortalConstants.ATP_INVOICE_UPLOAD_PAYMENT_DATE_RULE.equals(is.getErrorCode())) {
                                    isPaymentError = true;
                                }
                            }
                            if (!isPaymentError && invSummDataObj.getErrorList().size() > 0) {
                                allERRDetails.put(invSummDataObj.getInvoice_id(),
                                        invSummDataObj.getErrorList());
                                failed++;
                                hasError = true;
                                if (isIndividualInvoiceAllowed) {
                                    continue;
                                } else {
                                    break;
                                }
                            }
                        } else {
                            remove = false;
                        }
                    //RKAZI 09/04/2012 CR-708 Rel 8.1.1 End
                    if (isPayCreditNote) {
                        invSummary = (CreditNotes) EJBObjectFactory.createClientEJB(ejbServerLocation,
                                "CreditNotes", csdb);
                        if(crNoteAttTab==null || crNoteAttTab.isEmpty()){
                        	crNoteAttTab = invSummary.getAttributeHash();
                        }
                        attrTab=crNoteAttTab;
                    } else {
                        invSummary = (InvoicesSummaryData) EJBObjectFactory.createClientEJB(ejbServerLocation,
                                "InvoicesSummaryData", csdb);
                        if(invAttTab==null || invAttTab.isEmpty()){
                        	invAttTab = invSummary.getAttributeHash();
                        }
                        attrTab=invAttTab;
                    }
                    invSummary.newObject();
                    invSummary.setAttribute("corp_org_oid", ownerOrgOid);
                    invSummary.setAttribute("invoice_file_upload_oid", fileUplgOid);
                    invSummary.setAttribute("upload_definition_oid", defOid);
                    Map fields = invSummDataObj.getFields();
                    int intYear=0;
                        try {
                            intYear = Integer.parseInt(fields.get("due_date").toString().substring(6, 10));
                        } catch (Exception any_exc) {
                            intYear=0;
                        }
                         if(intYear!=0 && currencySet.add(invSummDataObj.getCurrency()+intYear)){
                            
                          currencyCalMap.put(invSummDataObj.getCurrency(), InvoiceUtility.getCurrencyCalendar(invSummDataObj.getCurrency(),fields.get("due_date").toString()));
                        }
                    // set all the fields from invoice summary POJO to actual EJB
                    setActaulObjectFields(invSummary, fields,currencyCalMap,attrTab);
                    if (isPayCreditNote) {

                        invSummary.setAttribute("linked_to_instrument_type", TradePortalConstants.INV_LINKED_INSTR_PYB);

                        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

                        Date convertedDate = dateFormatter.parse(DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate()));
//                        		DateTimeUtility.getGMTDateTime());
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(convertedDate);
                        cal.add(Calendar.DATE, (int) Long.parseLong(creditNoteAvaDays));
                        Date newDate = cal.getTime();
                        String a = dateFormatter.format(newDate);
                        Date fDate = dateFormatter.parse(a);

                        invSummary.setAttribute("expiry_date", DateTimeUtility.convertDateToDateString(fDate)); //issues_date+1

                    }
                    
                   
                    // W Zhu 2/25/13 Rel 8.2 CR-708B add buyer_tps_customer_id and seller_tps_customer_id
                    invSummary.setAttribute("buyer_tps_customer_id", invSummDataObj.getBuyer_tps_customer_id());
                    invSummary.setAttribute("seller_tps_customer_id", invSummDataObj.getSeller_tps_customer_id());
                    List<IssuedError> poErrVector = invSummary.getErrorManager()
                            .getIssuedErrorsList();
                    if (poErrVector != null && poErrVector.size() > 0) {
                        invSummDataObj.getErrorList().addAll(poErrVector);
                    }
                    
                    if (StringFunction.isBlank(invSummary.getAttribute("invoice_type"))) {
                        invSummary.setAttribute("invoice_type",
                                TradePortalConstants.INITIAL_INVOICE);
                    }

                    //SHR CR 708 Rel8.1.1 Begin
                    invSummary.setAttribute("invoice_classification", invoice_type_indicator);
                    String instrType = invSummary.getAttribute("linked_to_instrument_type");

                    if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator)) {
                        instrType = invSummary.getAttribute("linked_to_instrument_type");
                    } else if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator)) {
                        if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invUploadDataOption)) {
                            if (StringFunction.isNotBlank(instrType) && !TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrType)) {
                                IssuedError is = ErrorManager.getIssuedError(csdb.getLocaleName(),
                                        TradePortalConstants.LRQ_NOT_ALLOWED_FOR_REC, null, null);
                                invSummDataObj.getErrorList().add(is);
                                allERRDetails.put(invSummDataObj.getInvoice_id(),
                                        invSummDataObj.getErrorList());
                                failed++;
                                hasError = true;
                                if (isIndividualInvoiceAllowed) {
                                    continue;
                                } else {
                                    break;
                                }

                            }
                            instrType = StringFunction.isBlank(instrType) ? TradePortalConstants.INV_LINKED_INSTR_REC : instrType;
                        }
                    }
                    if (TradePortalConstants.GROUP_PO_LINE_ITEMS.equals(invUploadDataOption)) {
                        if (StringFunction.isNotBlank(invUploadInstrOption) && StringFunction.isBlank(instrType)) {
                            instrType = invUploadInstrOption;
                        }
                        if (StringFunction.isNotBlank(invLoanType)) {
                            invSummary.setAttribute("loan_type", invLoanType);
                        }
                    }
                    invSummary.setAttribute("linked_to_instrument_type", instrType);

                    String creditNote = (String) (invSummDataObj.getFields().get("credit_note"));
                    if ((invSummary.getAttribute("amount").startsWith("-") || StringFunction.isNotBlank(creditNote))
                            && (InstrumentType.LOAN_RQST.equals(instrType)
                            || InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(instrType))) {
                        IssuedError is = ErrorManager.getIssuedError(csdb.getLocaleName(),
                                TradePortalConstants.CREDIT_NOTE_NOT_ALLOWED_FOR_LRQ_ATP, null, null);
                        invSummDataObj.getErrorList().add(is);
                        allERRDetails.put(invSummDataObj.getInvoice_id(),
                                invSummDataObj.getErrorList());
                        failed++;
                        hasError = true;
                        if (isIndividualInvoiceAllowed) {
                            continue;
                        } else {
                            break;
                        }
                    }

                    BigDecimal amount = new BigDecimal((String) invSummDataObj.getFields().get("amount"));
                    if (StringFunction.isNotBlank(creditNote) && amount != null && amount.signum() != -1) {
                        amount = amount.negate();
                        invSummDataObj.setAmount(amount);
                        invSummDataObj.getFields().put("amount", amount.toString());
                        invSummary.setAttribute("amount", amount.toString());
                    }
                    
                    // do the calculation
                    BigDecimal amountinDec = invSummary
                            .getAttributeDecimal("amount"); 
                    calculateTotalInvoiceAmount(amountinDec);
                    
                    String name = StringFunction.isBlank(invSummary.getAttribute("buyer_name"))?null:invSummary.getAttribute("buyer_name");
                    String id = StringFunction.isBlank(invSummary.getAttribute("buyer_id"))?null:invSummary.getAttribute("buyer_id");
                    
                    tpMarginRules = (ArMatchingRule)invProp.getTradingPartnerRuleMap().
                    		get("buyer_name"+name+"buyer_id"+id);
                    //calculation should be done only for Receivables Mgmt
                    if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator) && !TradePortalConstants.EXPORT_FIN.equals(invLoanType)) {
                        try {

                            if (!invSummary.getAttribute("amount").startsWith("-")) {
                                if (StringFunction.isNotBlank(instrType) && !InstrumentType.LOAN_RQST.equals(instrType)) {
                                    ((InvoicesSummaryData) invSummary).calculateInterestDiscount(tpMarginRules,corpOrg);
                                    invSummary.setAttribute("invoice_status",
                                            TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN);
                                } else {
                                    invSummary.setAttribute("invoice_status",
                                            StringFunction.isNotBlank(instrType) ? TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT
                                                    : TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE);
                                }
                            } else {
                                String authorizeType = "";
                                if (tpMarginRules != null) {
                                    authorizeType = tpMarginRules.getAttribute("credit_authorise");
                                    //SHR IR MAUM050844263 Start
                                    invSummary.setAttribute("buyer_name", tpMarginRules.getAttribute("buyer_name"));
                                    invSummary.setAttribute("buyer_id", tpMarginRules.getAttribute("buyer_id"));
                                    //SHR IR MAUM050844263 end
                                } else {
                                    invSummary.setAttribute("buyer_name", buyer_name);
                                    invSummary.setAttribute("buyer_id", buyer_id);
                                }
                                if (TradePortalConstants.DUAL_AUTH_REQ_INV_CORPORATE
                                        .equals(authorizeType)) {                                  
                                    authorizeType = corpOrg.getAttributeWithoutHtmlChars("dual_auth_credit_note");
                                }
                                if (TradePortalConstants.DUAL_AUTH_NOT_REQ
                                        .equals(authorizeType)) {
                                    invSummary.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AUTH);
                                } else {
                                    invSummary.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE);
                                }
                            }

                        } catch (AmsException e) {
                            IssuedError is = e.getIssuedError();
                            if (is != null) {
                                String errorCode = is.getErrorCode();
                                if (TradePortalConstants.ERR_UPLOAD_INV_INSTR_TYPE_MISSING.equals(errorCode)) {
									Debug.debug("InvoiceUploadFileProcessor.processInvoiceFile: "
                                            + "Instrument type missing for invoice: " + invSummary.getAttribute("invoice_id"));

                                    invSummary.setAttribute("invoice_status",
                                            TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE);
                                } else if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode)) {
									Debug.debug("InvoiceUploadFileProcessor.processInvoiceFile: "
                                            + "Number of Tenor Days is less than number of days before due date/ payment date for invoice: "
                                            + invSummary.getAttribute("invoice_id"));

                                    invSummary.setAttribute("invoice_status",
                                            TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
                                } else if (TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode)) {
									Debug.debug("InvoiceUploadFileProcessor.processInvoiceFile: "
                                            + "Number of days between due date and payment date is too large for invoice: "
                                            + invSummary.getAttribute("invoice_id"));

                                    invSummary.setAttribute("invoice_status",
                                            TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
                                } // BSL IR NNUM040526786 04/05/2012 BEGIN
                                else if (TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode)) {
									Debug.debug("InvoiceUploadFileProcessor.processInvoiceFile: "
                                            + "Invoice finance % = 0 for invoice: " + invSummary.getAttribute("invoice_id"));

                                    invSummary.setAttribute("invoice_status",
                                            TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
                                } else {
                                    throw e;
                                }
                            } else {
                                throw e;
                            }
                        }
                    } //IR 20829 start -For REC Export finance invoice, trigger calculation using customer profile
                    else if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator) && InstrumentType.LOAN_RQST.equals(invUploadInstrOption)
                            && TradePortalConstants.EXPORT_FIN.equals(invLoanType) && !invSummary.getAttribute("amount").startsWith("-")) {
                        try {
                            invSummary.setAttribute("net_invoice_finance_amount", invSummary.getAttribute("amount"));
                            ((InvoicesSummaryData) invSummary).calculateInterestDiscount(corpOrgOID);
                        } catch (AmsException e) {
                            IssuedError is = e.getIssuedError();
                            if (is != null) {
                                String errorCode = is.getErrorCode();
                                //BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
                                if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode)
                                        || TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode)
                                        || TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode)
                                        || TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode)) {
                                    invSummary
                                            .setAttribute(
                                                    "invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN);
                                } //BSL IR NNUM040526786 04/05/2012 END
                                else {
									Debug.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
                                    e.printStackTrace();
                                    throw e;
                                }
                            } else {
								Debug.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
                                e.printStackTrace();
                                throw e;
                            }
                        }
                    } //IR 20829 end
                    //CR 913 start
                    else if (TradePortalConstants.FOR_PAY_PROG.equals(invUploadDataOption)
                            || TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrType)) {
                        if (isPayCreditNote) {
                            invSummary.setAttribute("credit_note_status", TradePortalConstants.CREDIT_NOTE_STATUS_AVA);
                            invSummary.setAttribute("credit_note_applied_status", TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP);
                            invSummary.setAttribute("credit_note_utilised_status", TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_UNU);
                            invSummary.setAttribute("portal_originated_ind", TradePortalConstants.INDICATOR_YES);
                        } else {
                            invSummary.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
                        }

                        if (StringFunction.isNotBlank(invSummary.getAttribute("end_to_end_id"))) {
                            if ("RPL".equalsIgnoreCase(invSummDataObj.getInvoice_type()) && !isPayCreditNote) {
                                String endToEndIDSeq = getEndToEndIDSeq(corpOrgOID, invSummary.getAttribute("invoice_id"));
                                invSummary.setAttribute("end_to_end_group_num", endToEndIDSeq);
                            } else {
                                Integer grpNum = endToEndIDMap.get(invSummary.getAttribute("end_to_end_id")) + 1;
                                endToEndIDMap.put(invSummary.getAttribute("end_to_end_id"), grpNum);
                                invSummary.setAttribute("end_to_end_group_num", grpNum.toString());
                            }

                        }

                    }//CR 913 end
                    //IR 5213
                    else {
                        // Srinivasu_D Rel-8.1.1 IR# T36000005935 09/28/2012 Start
                        invSummary.setAttribute("status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT);
                        invSummary.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT);
                        // Srinivasu_D Rel-8.1.1 IR# T36000005935 09/28/2012 End
                    }
                    // calculation part end
                    invSummary.setAttribute("tp_rule_name", invSummDataObj.getTp_rule_name());
                    if (TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice_type_indicator)) {
                        invSummary.setAttribute("buyer_name", invSummDataObj.getTp_rule_name());
                    }
                    if (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice_type_indicator)) {
                        invSummary.setAttribute("seller_name", invSummDataObj.getTp_rule_name());
                    }
                    // process goodsObject now
                    
                    if (invGoodsSet != null) {
                    	TradePortalBusinessObject invGoods = null;
                        for (InvoiceSummaryGoodObject invGoodObject : invGoodsSet) {
                            
                            if (isPayCreditNote) {
                                invGoods = (CreditNotesGoods) invSummary
                                        .getComponentHandle(
                                                "CreditNotesGoodsList", invSummary
                                                .newComponent("CreditNotesGoodsList"));
                                if(crNoteGoodsAttTab==null || crNoteGoodsAttTab.isEmpty()){
                                	crNoteGoodsAttTab = invGoods.getAttributeHash();
                                }
                                attrTab=crNoteGoodsAttTab;
                            } else {
                                long goodsOid = invSummary
                                        .newComponent("InvoiceSummaryGoodsList");

                                invGoods = (InvoiceSummaryGoods) invSummary
                                        .getComponentHandle(
                                                "InvoiceSummaryGoodsList", goodsOid);
                                if(invGoodsAttTab==null || invGoodsAttTab.isEmpty()){
                                	invGoodsAttTab = invGoods.getAttributeHash();
                                }
                                attrTab=invGoodsAttTab;
                            }

                            fields = invGoodObject.getFields();
                            // set all the fields from invoice Goods POJO to
                            // actual EJB
                            setActaulObjectFields(invGoods, fields,new HashMap(),attrTab);

                            List<IssuedError> errVector = invGoods.getErrorManager()
                                    .getIssuedErrorsList();
                            if (errVector != null && errVector.size() > 0) {
                                invSummDataObj.getErrorList().addAll(errVector);
                            }
                            // now line items start
                            Set<InvoiceLineItemObject> invlineSet = invGoodObject
                                    .getInvoiceLineItem();
                        
                            if (invlineSet != null) {
                                TradePortalBusinessObject invLineItem = null;
                                for (InvoiceLineItemObject invLineItemObject : invlineSet) {
                                    if (isPayCreditNote) {
                                        invLineItem = (CreditNotesLineItemDetail) invGoods
                                                .getComponentHandle(
                                                        "CreditNotesLineItemDetailList",
                                                        invGoods
                                                        .newComponent("CreditNotesLineItemDetailList"));
                                        if(crNoteLineAttTab==null || crNoteLineAttTab.isEmpty()){
                                        	crNoteLineAttTab = invLineItem.getAttributeHash();
                                        }
                                        attrTab=crNoteLineAttTab;
                                    } else {
                                        long lineItemOid = invGoods
                                                .newComponent("InvoiceLineItemDetailList");

                                        invLineItem = (InvoiceLineItemDetail) invGoods
                                                .getComponentHandle(
                                                        "InvoiceLineItemDetailList",
                                                        lineItemOid);
                                        if(invLineAttTab==null || invLineAttTab.isEmpty()){
                                        	invLineAttTab = invLineItem.getAttributeHash();
                                        }
                                        attrTab=invLineAttTab;
                                    }

                                    fields = invLineItemObject.getFields();
                                    setActaulObjectFields(invLineItem, fields,new HashMap(),attrTab);

                                    List<IssuedError> errVector1 = invGoods
                                            .getErrorManager()
                                            .getIssuedErrorsList();
                                    if (errVector1 != null
                                            && errVector1.size() > 0) {

                                        invSummDataObj.getErrorList().addAll(
                                                errVector1);
                                    }
                                    // line items end

                                }
                            }
                        }
                    }
                    if(remove && isPayCreditNote){
                    	invSummary.setAttribute("remove_crnote_oid",  (String) removeOids.get("crOid"));
                    }
                     
                    String action = isPayCreditNote ? TradePortalConstants.UPLOAD_CR_ACTION_FILE_UPL :TradePortalConstants.UPLOAD_INV_ACTION_FILE_UPL;
                    String status = isPayCreditNote ? invSummary.getAttribute("credit_note_status") : invSummary.getAttribute("invoice_status");
                    if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH
                            .equals(status)) {
                    	 action = TradePortalConstants.UPLOAD_INV_ACTION_AUTH;                    	
                    	 userOid = StringFunction.isNotBlank(corpOrg.getAttributeWithoutHtmlChars("upload_system_user_oid")) ? corpOrg
                                         .getAttributeWithoutHtmlChars("upload_system_user_oid")
                                         : userOid;
                         invoiceUpload.setAttribute("user_oid", userOid);
                    }
                    invSummary.setAttribute("action", action);
                    invSummary.setAttribute("user_oid", userOid);
                   
                    if (invSummary.save() != 1) {

                        addToErrorList(invSummDataObj);
                        allERRDetails.put(invSummDataObj.getInvoice_id(),
                                invSummDataObj.getErrorList());
                        failed++;
                        hasError = true;
                        if (isIndividualInvoiceAllowed) {
                            continue;
                        } else {
                            break;
                        }
                    } else {

                        if (remove && !isPayCreditNote) {
                            removeInvoicesFromDB(removeOids, (InvoicesSummaryData) invSummary, invSummDataObj);
                        }
                        // Only credit note can be authorized./ If authorized, then create an entry in Outgoing_queue
                        // and create Audit log/update InvoiceFileUpload object with Upload System User OID
                        if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH
                                .equals(status)) {
                        	String uplOid = isPayCreditNote ? invSummary.getAttribute("upload_credit_note_oid") : invSummary.getAttribute("upload_invoice_oid");
                             
                            InvoiceUtility.createOutgoingQueueAndIssueSuccess(TradePortalConstants.UPLOAD_INV_STATUS_AUTH, uplOid,ejbServerLocation,csdb);
                        }
                     }
                } catch (RemoteException e) {
                    logError("Error saving Pruchase Order .... "
                            + invSummDataObj.getInvoice_id() + " : "
                            + e.getMessage());
                } catch (AmsException e) {
                    logError("Error saving Pruchase Order .... "
                            + invSummDataObj.getInvoice_id() + " : "
                            + e.getMessage());
                } catch (ParseException e) {
                    e.printStackTrace();
                } finally {
                    removeEJBReference(invSummary);
                }
            }

        }

    }

    private void removeInvoicesFromDB(HashMap removeOid,
            InvoicesSummaryData newInvSummary,
            InvoiceSummaryDataObject invObject) throws NumberFormatException,
            RemoteException, AmsException {

        boolean delete = false;
        String instrumentOid = "";
        String tranOid = null;
        String termsOid = null;
        if ("AI".equals(removeOid.get("RPL_VAL"))) {
            String invOid = newInvSummary.getAttribute("upload_invoice_oid");
            invObject.checkForAuthorizedAmendOrProcessedByBankMatch(securityRights, newInvSummary);
            //IR T36000016945 - create invoice history
            this.createInvoiceHistoryLog(
                    invOid,
                    TradePortalConstants.PO_STATUS_ASSIGNED,
                    invoiceUpload.getAttribute("user_oid"), TradePortalConstants.UPLOAD_INV_ACTION_FILE_UPL);
        } else {
            String invOid = newInvSummary.getAttribute("upload_invoice_oid");
            InvoicesSummaryData oldInvData = null;
            if (StringFunction.isBlank((String) removeOid.get("invOID"))) {
                instrumentOid = invObject.getA_instrument_oid();
                tranOid = invObject.getA_transaction_oid();
                termsOid = invObject.getA_terms_oid();
            } else if (StringFunction.isNotBlank((String) removeOid.get("invOID"))) {
                oldInvData = (InvoicesSummaryData) EJBObjectFactory
                        .createClientEJB(ejbServerLocation, "InvoicesSummaryData",
                                Long.parseLong((String) removeOid.get("invOID")));
                tranOid = oldInvData.getAttribute("transaction_oid");
                termsOid = StringFunction.isBlank(oldInvData.getAttribute("terms_oid")) ? null : oldInvData.getAttribute("terms_oid");
                instrumentOid = oldInvData.getAttribute("instrument_oid");

            }
            // IR T36000016894 start - get transaction types
            String tranType = "";
            Transaction invSummaryTransaction = null;
            if (StringFunction.isNotBlank(tranOid)) {
                invSummaryTransaction = (Transaction) EJBObjectFactory
                        .createClientEJB(ejbServerLocation, "Transaction",
                                Long.parseLong(tranOid),
                                csdb);
                tranType = invSummaryTransaction.getAttribute("transaction_type_code");
            }
            //IR T36000016894 end

            int success = 0;

            if (StringFunction.isNotBlank((String) removeOid.get("RPL_VAL")) && !"UI".equals(removeOid.get("RPL_VAL"))) {
                newInvSummary.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
                //newInvSummary.setAttribute("tp_rule_name",rule.getAttribute("buyer_name"));
                newInvSummary.verifyInvoiceGroup();
                String newGroupOid = newInvSummary.getAttribute("invoice_group_oid");
                success = updateTransactionDetails(instrumentOid, tranOid,
                        termsOid, invOid, newGroupOid, tranType);
            }

            if ((success == 1 || "UI".equals(removeOid.get("RPL_VAL"))) && oldInvData != null) {
                delete = true;
            }
            if (delete) {
                oldInvData.delete();
                // IR 18421 Rel8.3 09/17/2013 starts
                if (StringFunction.isNotBlank(newInvSummary.getAttribute("invoice_group_oid"))) {
                    newInvSummary.updateInvoiceGroup(newInvSummary.getAttribute("invoice_group_oid"), false);
                }
                // IR 18421 Rel8.3 09/17/2013 ends

            }

            if (removeOid.get("updateTransaction") != null
                    && (Boolean) removeOid.get("updateTransaction")) {

                Terms terms = (Terms) invSummaryTransaction
                        .getComponentHandle("CustomerEnteredTerms");
                Vector invList = new Vector();
                Vector invIDList = new Vector();
                invList.add(newInvSummary.getAttribute("upload_invoice_oid"));
                invIDList.add(newInvSummary.getAttribute("invoice_id"));
                //IR T36000017371 start - get all the assigned invoices of the transaction

                DocumentHandler resultSet1 = DatabaseQueryBean.getXmlResultSet(" select invoice_id,upload_invoice_oid from invoices_summary_data where a_transaction_oid = ? ", false, new Object[]{invSummaryTransaction.getAttribute("transaction_oid")});
                if (resultSet1 != null) {
                    List<DocumentHandler> rows = resultSet1.getFragmentsList("/ResultSetRecord");
                    for (DocumentHandler row: rows) {
                        invIDList.addElement(row.getAttribute("/INVOICE_ID"));
                        invList.addElement(row.getAttribute("/UPLOAD_INVOICE_OID"));
                    }
                }

                instrumentOid = invSummaryTransaction
                        .getAttribute("instrument_oid");
                Instrument instrument = (Instrument) EJBObjectFactory
                        .createClientEJB(ejbServerLocation, "Instrument",
                                Long.parseLong(instrumentOid));
                //IR T36000016894 start - If ISSUANCE then always set invoice type as INT
                if (TransactionType.ISSUE.equals(tranType)) {
                    newInvSummary.setAttribute("invoice_type", TradePortalConstants.INITIAL_INVOICE);
                } else {
                    newInvSummary.setAttribute("invoice_type", TradePortalConstants.REPLACEMENT);
                }
                InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
                        invSummaryTransaction, terms, invList, instrument, invIDList, "RPL");
                invSummaryTransaction.setAttribute("transaction_status",
                        TransactionStatus.STARTED);
                int i = invSummaryTransaction.save(false);
                instrument.save(false);

            }
        }

    }

    private int updateTransactionDetails(String instrumentOid, String tranOid,
            String termsOid, String invOid, String newGroupOid, String tranType) throws NumberFormatException,
            AmsException {
        if (StringFunction.isBlank(instrumentOid)) {
            instrumentOid = null;
        }

        StringBuilder previousSql = new StringBuilder();
        previousSql.append("update invoices_summary_data ");
        previousSql.append("set a_instrument_oid = ? ");
        previousSql.append(", a_transaction_oid = ? ");
        previousSql.append(", a_terms_oid = ? ");
        previousSql.append(", a_invoice_group_oid = ?");
        previousSql.append(", invoice_status = ? ");

        List<Object> sqlParams = new ArrayList<>();
        sqlParams.add(instrumentOid);
        sqlParams.add(tranOid);
        sqlParams.add(termsOid);
        sqlParams.add(newGroupOid);
        sqlParams.add(TradePortalConstants.PO_STATUS_ASSIGNED);

        //IR T36000016894 start - If ISSUANCE then always set invoice type as INT
        if (TransactionType.ISSUE.equals(tranType)) {
            previousSql.append(", invoice_type=?");
            sqlParams.add(TradePortalConstants.INITIAL_INVOICE);
        }
        //IR T36000016894 end
        previousSql.append(" where upload_invoice_oid = ? ");
        sqlParams.add(invOid);

        int success = 0;
        try {
            success = DatabaseQueryBean.executeUpdate(previousSql.toString(), false, sqlParams);
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return success;

    }

    protected void setActaulObjectFields(BusinessObject object, Map fields,Map currencyCalMap,Hashtable<String,Attribute> attrTable)
            throws RemoteException, AmsException, ParseException {
        //Rel 9.0 - Performance tuning
        Map<String, String> invAttributes = new HashMap<>();

        if (fields != null) {
        	
            Set fieldNamesSet = fields.keySet();
            for (Object fieldNameObj : fieldNamesSet) {
                String fieldName = (String) fieldNameObj;
                String fieldType = InvoiceFileDetails.getAttributeType(attrTable,
                        fieldName);
                String value = "";
                if (fieldType == null) {
                    logError("Error fetching attribute type for attribute : "
                            + fieldName);
                } else {
                    if ("DateTimeAttribute".equals(fieldType)) {

                        Date d = sd.parse((String) fields.get(fieldName));

                        invAttributes.put(fieldName, dateTimeFormatter.format(d));
                    } else if ("DateAttribute".equals(fieldType)) {

                        String dateVal = (String) fields.get(fieldName);
                        if (StringFunction.isNotBlank(dateVal)) {
                            if (fieldName.equals("due_date")) {
                                try {
                                    String busdateVal = InvoiceUtility
                                            .getCalendarBasedNextBusinessDate(
                                                    object.getAttribute("currency")
                                                    .toUpperCase(),
                                                    dateVal, (Map)currencyCalMap.get(object.getAttribute("currency")));
                                    dateVal = busdateVal != null ? busdateVal
                                            : dateVal;
                                } catch (Exception e) {
									Debug.debug("Error getting.. CalendarBasedNextBusinessDate");
                                }
                            }
                            Date d = sd.parse(dateVal);

                            invAttributes.put(fieldName,dateFormatter.format(d));
                        }
                    } else if ((fieldName.contains("seller_user") | fieldName
                            .contains("buyer_user"))) {

                        if (fieldName.endsWith("_label")) {
                            String actualabel = invoiceDefinition
                                    .getAttributeWithoutHtmlChars(fieldName);
                            // set label
                            if (fieldName.contains("buyer_user") && StringFunction.isBlank(actualabel)) {
                                actualabel = "Buyer User Definition Label";
                            }

                            invAttributes.put(fieldName, actualabel);
                            String valueLabel = fieldName.replace("_label",
                                    "_value");
                            value = (String) fields.get(fieldName);
                           
                            invAttributes.put(valueLabel, value);

                        }
                    } else if (fieldName.contains("prod_chars")) {
                        if (fieldName.endsWith("_type")) {
                            String actualabel = invoiceDefinition
                                    .getAttributeWithoutHtmlChars(fieldName);
                            // set label

                            invAttributes.put(fieldName, actualabel);
                            String valueLabel = fieldName.replace("_type",
                                    "_val");
                            value = (String) fields.get(fieldName);

                            invAttributes.put(valueLabel, value);

                        }
                    } else if (fieldName.contains("invoice_type")) {
                        value = StringFunction.isBlank((String) fields
                                .get(fieldName)) ? TradePortalConstants.INITIAL_INVOICE : (String) fields.get(fieldName);

                        invAttributes.put(fieldName, value.toUpperCase());
                    } else if (fieldName.contains("currency") || fieldName.contains("incoterm")) {
                        value = (String) fields.get(fieldName);

                        invAttributes.put(fieldName, value.toUpperCase());
                    } else if (fieldName.contains("linked_to_instrument_type")) {
                        value = (String) fields.get(fieldName);
                        if (StringFunction.isBlank(value) && TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invUploadDataOption)) {
                            value = StringFunction.isNotBlank((String) fields
                                    .get(fieldName)) ? (String) fields.get(fieldName)
                                            : "REC";

                        }

                        invAttributes.put(fieldName, value.toUpperCase());

                    } else {

                        invAttributes.put(fieldName, (String) fields.get(fieldName));
                    }

                }
            }
            int size = invAttributes.keySet().size();
            String[] keys = new String[size];
            String[] values = new String[size];

            InvoiceUtility.packInArray(invAttributes, keys, values);
            object.setAttributes(keys, values);
        }
    }

    /**
     * Sets InvoiceFileUpload to File Upload Rejected status
     *
     *
     * @param defaultErrorMsg Error message
     * @throws AmsException
     * @throws RemoveException
     * @throws IOException
     */
    protected void rejectFileUplaod(boolean defaultErrorMsg)
            throws RemoteException, AmsException {

        if (invoiceUpload == null) {
            return;
        }

        invoiceUpload.setAttribute(
                "validation_seconds",
                getDurationInSeconds(valStartTime,DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))
//                        GMTUtility.getGMTDateTime(false, false)
                        ));
        invoiceUpload.setAttribute("completion_timestamp",DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false))
//                DateTimeUtility.getGMTDateTime(false, false)
                );
        String errMsg = invoiceUpload.getAttribute("error_msg");

        if (!defaultErrorMsg) {
            defaultErrorMsg = (StringFunction.isBlank(errMsg) && uploadLog.isEmpty());
        }

        try {
            invoiceUpload.rejectFileUplaod(defaultErrorMsg);
        } catch (AmsException e) {
            logError("Error during rejectFileUpload...", e);
            throw e;

        }
    }

    protected void hasSameCurrency(List<String> currencyList,
            InvoiceSummaryDataObject invoiceSummaryData) {
        String curr = invoiceSummaryData.getCurrency();
        if (!currencyList.isEmpty() && !currencyList.contains(curr)) {
            isSameCurrency = false;
        }
        if (currencyList.isEmpty()) {
            currencyList.add(curr);
        }

    }

    protected void calculateTotalInvoiceAmount(BigDecimal amt) throws AmsException, RemoteException {

         totalAmount = totalAmount.add(amt);

    }

    /**
     * This method processes orphan InvoiceFileUploads and set to reject file
     * upload status
     *
     *
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws RemoveException
     * @throws RemoteException
     */
    public boolean processOrphan() throws AmsException, RemoteException {

        invoiceUpload = (InvoiceFileUpload) EJBObjectFactory.createClientEJB(
                ejbServerLocation, "InvoiceFileUpload", invoiceFileOid);
        fileName = invoiceUpload.getAttribute("invoice_file_name");
        invoiceFile = getInvoiceFile();

        invoiceUpload.setClientServerDataBridge(csdb);

        rejectFileUplaod(true);
        cleanup();

        return true;
    }

    /**
     * This method creates InvoiceFileUpload to Validation in Progress status
     *
     * @throws AmsException
     * @throws RemoteException
     */
    private void init() throws RemoteException, AmsException {
        setInvoiceUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS);
        valStartTime = DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false)); 
//        		GMTUtility.getGMTDateTime(false, false);

    }

    /**
     * This method handles post process
     *
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException
     */
    private void postProcess(PropertyResourceBundle bundle, InvoiceProperties invProp,DocumentHandler invUploadParamsDoc)
            throws RemoteException, AmsException {
        String status = null;
        saveAll(invProp);

        int total_failed_invoices = failed;

        if (!isIndividualInvoiceAllowed && failed > 0) {
            total_invoices = 0;
        } else {

            total_failed_invoices = failed;

        }

        invoiceUpload.setAttribute("completion_timestamp",DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false))
//                DateTimeUtility.getGMTDateTime(true, false)
                );

        invoiceUpload.setAttribute("number_of_invoices_uploaded",
                String.valueOf(total_invoices));

        invoiceUpload.setAttribute("number_of_invoices_failed",
                String.valueOf(total_failed_invoices));

        if (!isSameCurrency) {
            invoiceUpload.setAttribute("currency",
                    bundle.getString("InvoiceFileUpload.MixedCurrency"));
        } else {
            // NAR - IR NEUM031340725 save currency code in upper case as USD,
            // CAD
            if (StringFunction.isNotBlank(currencyUsed)) {

                invoiceUpload.setAttribute("currency", currencyUsed.toUpperCase());
                invoiceUpload.setAttribute("amount", totalAmount.toString());
            }
        }
        if (!hasError || (isIndividualInvoiceAllowed && hasOneValidInvAtleast)) {
            //RKAZI REL 8.2 CR 709 - Start
            autoGroupInvs(invUploadParamsDoc);
            //RKAZI REL 8.2 CR 709 - End

        }
        // BSL IR NNUM040229447 04/02/2012 BEGIN - use ref data instead of
        // resource bundle
        if (!hasError) {

            status = TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL;
        } else {
            if (isIndividualInvoiceAllowed && hasOneValidInvAtleast) {
                status = TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS;
            } else {
                status = TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED;
            }
        }
        setInvoiceUploadStatus(status);
        setStatusAndErrorMessage(allERRDetails,
                invoiceUpload.getAttribute("invoice_file_upload_oid"));
    }

    private void autoGroupInvs(DocumentHandler invUploadParamsDoc) throws RemoteException, AmsException {
        String autoCreated = "N";
        boolean success = true;

        String invUploadDataOption = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/InvPayDataOption");
        if (TradePortalConstants.GROUP_PO_LINE_ITEMS.equals(invUploadDataOption)) {
            String loanType = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/loanType");
            String invUploadInstrType = invUploadParamsDoc.getAttribute("/In/InvoiceDataUploadInfo/InvUploadInstrOption");
            if (StringFunction.isBlank(loanType) && InstrumentType.LOAN_RQST.equals(invUploadInstrType)) {
                IssuedError ie = ErrorManager.findErrorCode(TradePortalConstants.AUTO_UPLOAD_INV_LOANTYPE_MISSING, csdb.getLocaleName());
                if (ie != null) {
                    String errMsg = ie.getErrorText();
                    insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);
                    insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), "No Output returned: " + errMsg, "N");
                }
                return;
            }
            invUploadParamsDoc.setAttribute("/In/InvoiceDataUploadInfo/uploadDefinitionOid", invoiceDefinition.getAttributeWithoutHtmlChars("inv_upload_definition_oid"));
            invUploadParamsDoc.setAttribute("/In/InvoiceDataUploadInfo/invoiceClassification", invoiceDefinition.getAttributeWithoutHtmlChars("invoice_type_indicator"));

            if (!TradePortalConstants.INDICATOR_YES.equals(autoCreated)) {
                invUploadParamsDoc.setAttribute("/In/User/uploadSequenceNumber", invoiceUpload.getAttribute("invoice_file_upload_oid"));
            }
            
            DocumentHandler outputDoc = groupINVs(invUploadParamsDoc);
            DocumentHandler errorDoc = outputDoc.getFragment("/Error");
            if (errorDoc != null) {
                String maxErrorSeverity = errorDoc.getAttribute("maxerrorseverity");
                if ("5".equals(maxErrorSeverity)) {
                    List<DocumentHandler> errVector = errorDoc.getFragmentsList("/errorlist/error");
                    for (DocumentHandler errDoc : errVector) {
                        String errMessage = errDoc.getAttribute("message");
                        insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMessage, null);
                        insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), "Error returned: " + errorDoc.toString(), "N");
                    }
                    success = false;
                } else {
                    success = (!hasError || (isIndividualInvoiceAllowed && hasOneValidInvAtleast));
                }

            }
            if (success) {
                DocumentHandler resultsParamDoc = new DocumentHandler();
                resultsParamDoc.addComponent("/", outputDoc.getFragment("/Out"));
                List<DocumentHandler> v = resultsParamDoc.getFragmentsList("/ResultSetRecord");
                // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Begins
                String errorCode = "";
                if (errorDoc != null) {
                    String maxSeverity = errorDoc.getAttribute("/maxerrorseverity");
                    if ("1".equals(maxSeverity)) {
                        List<DocumentHandler> errVector = errorDoc.getFragmentsList("/errorlist/error");
                        for (DocumentHandler errDoc : errVector) {

                            String errCD = errDoc.getAttribute("/code");
                            if ("I182".equals(errCD)) {
                                errorCode = errCD;
                            } else if ("I477".equals(errCD)) {
                                errorCode = errCD;
                            } else if ("I472".equals(errCD)) {
                                errorCode = errCD;
                            } else if ("I473".equals(errCD)) {
                                errorCode = errCD;
                            } else if ("I478".equals(errCD)) {
                                errorCode = errCD;
                            } else if ("I502".equals(errCD) | "I503".equals(errCD) | "I504".equals(errCD) | "I505".equals(errCD) | "I506".equals(errCD) || "I512".equals(errCD)
                            		|| "I530".equals(errCD) || "I531".equals(errCD) || "I532".equals(errCD) || "I533".equals(errCD)) {
                                String errMsg = errDoc.getAttribute("/message");
                                insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);
                                insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), "No Output returned: " + errorDoc.toString(), "N");
                                errorCode = "ignore";
                            } else if ("I525".equals(errCD)) {
                                String errMsg = errDoc.getAttribute("/message");
                                insertToInvErrorLog("Warning- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);                                

                            }
                        }
                    }
                }
                // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Ends
                if (v.size() > 0) {
                    String resultParams = resultsParamDoc.toString();
                    invoiceUpload.setAttribute("inv_result_parameters", resultParams);
                } else if (StringFunction.isBlank(errorCode)) {  // DK IR - RAUM070159323 rel8.1.1 09/14/2012
                    IssuedError ie = ErrorManager.findErrorCode(TradePortalConstants.ERROR_GROUPING_INVS, csdb.getLocaleName());
                    String errMsg = ie.getErrorText();
                    insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);
                    insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), "No Output returned: " + errorDoc!=null?errorDoc.toString():"", "N");
                } else {
                    IssuedError ie = ErrorManager.findErrorCode(errorCode, csdb.getLocaleName());
                    if (ie != null) {
                        String errMsg = ie.getErrorText();
                        insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), errMsg, null);
                        insertToInvErrorLog("groupingErr- ", invoiceUpload.getAttribute("invoice_file_upload_oid"), "No Output returned: " + errorDoc!=null?errorDoc.toString():"", "N");
                    }
                }
            }
        }

    }

    private DocumentHandler groupINVs(DocumentHandler invUploadParamsDoc) {
		logDebug("InvoiceUploadFileProcessor->groupINVs: starting grouping...");

        AutoCreateInvoicesMediator autoCreateInvoicesMediator = null;
        try {
			logDebug("InvoiceUploadFileProcessor->groupINVs: invUploadParamsDoc: " + invUploadParamsDoc);
            autoCreateInvoicesMediator = (AutoCreateInvoicesMediator) EJBObjectFactory.createClientEJB(ejbServerLocation, "AutoCreateInvoicesMediator");
            DocumentHandler outputDoc = autoCreateInvoicesMediator.performService(invUploadParamsDoc, csdb);
			logDebug("InvoiceUploadFileProcessor->groupINVs: outputDoc: " + outputDoc);
            return outputDoc;
        } catch (RemoteException | AmsException e) {
			logError("InvoiceUploadFileProcessor->groupINVs: Error occured while invoking AutoCreateInvoicesMediator...", e);
        }

        return new DocumentHandler();
    }

    private void insertToInvErrorLog(String invoiceID, String invUploadOID, String errMsg, String displayable) {
        try( Connection con = DatabaseQueryBean.connect(false);
        		PreparedStatement pStmt = con
                        .prepareStatement("insert into INVOICE_UPLOAD_ERROR_LOG (INVOICE_UPLOAD_ERROR_LOG_OID, INVOICE_ID, error_msg, TRADING_PARTNER_NAME, "
                        		+ "P_INVOICE_FILE_UPLOAD_UOID, displayable)values(?,unistr(?),unistr(?),unistr(?),?,?)")
        		) {
           
            con.setAutoCommit(false);
            
            String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(false));
            pStmt.setString(1, oid);
            String[] temp = invoiceID.split("-");
            pStmt.setString(2, (StringFunction.isBlank(temp[0]))?temp[0]:StringFunction.toUnistr(SQLParamFilter.filter(temp[0])));
            pStmt.setString(3, (StringFunction.isBlank(errMsg))?errMsg:StringFunction.toUnistr(SQLParamFilter.filter(errMsg)));
            pStmt.setString(4, (temp[1] == null) ? null : (StringFunction.toUnistr(SQLParamFilter.filter(temp[1]).trim())));
            pStmt.setString(5, invUploadOID);
            pStmt.setString(6, displayable);
            pStmt.execute();
            con.commit();
           
        } catch (AmsException | SQLException e) {
            logError("Error occured during saving PO Upload Error log ...", e);
        }
    }

    /**
     * This method gets the input in seconds
     *
     */
    private String getDurationInSeconds(Date start, Date end) {
        if (start == null || end == null) {
            return null;
        }
        long startMillis = start.getTime();
        long endMillis = end.getTime();
        long durationMillis = endMillis - startMillis;
        return Long.toString(durationMillis / 1000);
    }

    /**
     * This method sets the proper validation status
     *
     */
    private void setInvoiceUploadStatus(String status) throws RemoteException,
            AmsException {

        if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS
                .equals(status)) {
            invoiceUpload.setAttribute(
                    "validation_seconds",
                    getDurationInSeconds(valStartTime, DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))
//                            GMTUtility.getGMTDateTime(false, false)
                            ));
            invoiceUpload.setAttribute("completion_timestamp",DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false))
//                    DateTimeUtility.getGMTDateTime(false, false)
                    );
        }
        invoiceUpload.setAttribute("validation_status", status);
        if (invoiceUpload.save() < 0) {
            throw new AmsException("error saving InvoiceFileUpload ....");
        }
        
        

        
        
    }

    /**
     * this method perform audit log to all the processing that has taken place
     * on a invoice. The audit table will provide an audit trail of the statuses
     * that an invoice will go through (for example, uploaded, approved for
     * financing, authorised), with the accompanying name of the user who
     * initiated the update and the date/time of the status update TODO move to
     * common class
     */
    private void createInvoiceHistoryLog(String invoiceOid, String status,
            String userOid, String action) throws RemoteException, AmsException {
        InvoiceHistory invHistory = (InvoiceHistory) EJBObjectFactory
                .createClientEJB(ejbServerLocation, "InvoiceHistory", csdb);
        invHistory.newObject();
        invHistory.setAttribute("action_datetime", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//        		DateTimeUtility.getGMTDateTime(false, false));//BSL IR BLUM041758623 04/19/2012 - do not use override date
        invHistory.setAttribute("action", action);
        invHistory.setAttribute("invoice_status", status);
        invHistory.setAttribute("upload_invoice_oid", invoiceOid);
        invHistory.setAttribute("user_oid", userOid);
        int ret = invHistory.save(false);
        if (ret != 1) {
			Debug.debug("Error occured while creating Invoice History Log...");
        }
    }

    /**
     * This method inserts the error messages to Invoice Error Log
     *
     */
    private void setStatusAndErrorMessage(HashMap details,
            String invoiceUploadOID) {
        if (details != null && !details.isEmpty()) {
            try(Connection con = DatabaseQueryBean.connect(false);
            		PreparedStatement pStmt = con
                            .prepareStatement("insert into invoice_upload_error_log (invoice_upload_error_log_oid, invoice_id, trading_partner_name,error_msg,p_invoice_file_upload_uoid)values(?,unistr(?),unistr(?),unistr(?),?)");
            ) {
                
                con.setAutoCommit(false);
                //Nar IR-DEUM062261626 using unistr() to insert unicode value.
                //PGedupudi IR#T36000029753
                
                boolean isGenericError = false;
                for (Object obj : details.keySet()) {
                    String invoiceID = (String) obj;
                    List detail = (List) details.get(invoiceID);

                    if (detail != null && detail.size() > 0) {
                        for (Object err : detail) {
                            IssuedError errorObject = (IssuedError) err;
                            if (errorObject != null) {

                                isGenericError = isGenericError(errorObject.getErrorText());
                                String oid = Long.toString(ObjectIDCache
                                        .getInstance(AmsConstants.OID)
                                        .generateObjectID(false));
                                pStmt.setString(1, oid);
                                //Rel9.2 CR 914A - For Generic error messages, no Invoice ID will be mapped.
                                if (isGenericError) {
                                    pStmt.setString(2, "-");
                                } else {
                                    pStmt.setString(2, StringFunction.toUnistr(SQLParamFilter.filter(invoiceID))); // Nar IR-DEUM062261626
                                }
                                //PGedupudi IR#T36000029753
                                String tpName = getTradingPartnerName(StringFunction.toUnistr(SQLParamFilter.filter(invoiceID)), invoiceUploadOID);
                                if(StringFunction.isBlank(tpName)){
                            	  tpName=tradingPartnerName;
                                }
                                pStmt.setString(3, StringFunction.toUnistr(tpName));
                                pStmt.setString(4, StringFunction.toUnistr(SQLParamFilter.filter(errorObject.getErrorText()))); // Nar IR-DEUM062261626
                                pStmt.setString(5, invoiceUploadOID);
                                pStmt.addBatch();
                                pStmt.clearParameters();
                            }
                        }
                    }
                }
                pStmt.executeBatch();
                con.commit();
               
            } catch (AmsException | SQLException e) {
                logError("Error occured during saving Invoice Upload Error log ...", e);
            }
        }

    }

    private String getTradingPartnerName(String invoiceID,
            String invoiceUploadOID) {
        String tradingPartnerName = null;
        String sql = "select case when buyer_name is not null then buyer_name when buyer_id is not null then buyer_id  "
                + "when seller_name is not null then seller_name when seller_id is not null  then seller_id end "
                + "as tp_rule_name from invoices_summary_data where  a_corp_org_oid = ? and invoice_id = ? ";

        try {
            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, corpOrgOID, invoiceID);
            if (resultSet != null) {
                tradingPartnerName = resultSet.getAttribute("/ResultSetRecord/TP_RULE_NAME");
            }

        } catch (AmsException e) {
            logError(
                    "Error occured during Fetching trading partner rule name ...",
                    e);

        }
        return tradingPartnerName;
    }

    /**
     * This method gets temporarily stored invoice file
     *
     */
    private File getInvoiceFile() throws RemoteException, AmsException {
        String fname = folder +  invoiceUpload
                .getAttribute("invoice_file_upload_oid")
                + PaymentFileWithInvoiceDetails.FILE_EXTENSION;
        return new File(fname);
    }

    private BufferedReader getInputReader() throws AmsException {

        BufferedReader bReader = null;
        try {
            invoiceFile = getInvoiceFile();
            FileInputStream is = new FileInputStream(invoiceFile);
            InputStreamReader bis = new InputStreamReader(is,
                    AmsConstants.UTF8_ENCODING);
            bReader = new BufferedReader(bis);
           
        } catch (Exception e) {
            logError("Error occured getting invoice file...", e);
            throw new AmsException("Error occured getting invoice file...");
        }

        return bReader;

    }

    /**
     * This method gets InvoiceDefinition instance
     *
     */
    protected InvoiceDefinitionWebBean getInvoiceFileDef(String name, String corOrgID, BeanManager beanMgr)
            throws RemoteException, AmsException {
        String sql = "select INV_UPLOAD_DEFINITION_OID from INVOICE_DEFINITIONS where NAME = ? AND A_OWNER_ORG_OID = ? ";
        InvoiceDefinitionWebBean invDef = null;
        try{
        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{name, corOrgID});
        String invDefOID = null;
        if (resultSet != null) {
            invDefOID = resultSet
                    .getAttribute("/ResultSetRecord/INV_UPLOAD_DEFINITION_OID");
        invDef = beanMgr.createBean(InvoiceDefinitionWebBean.class, "InvoiceDefinition",csdb);
        invDef.getById(invDefOID);
        delimiterChar = invDef.getAttributeWithoutHtmlChars("delimiter_char");
        dateFormat = invDef.getAttributeWithoutHtmlChars("date_format");
         sd = new SimpleDateFormat(dateFormat);
        }
        }
        catch (Exception e) {
            logError("While getting InvoiceFileDef object", e);
            e.printStackTrace();
        }
        return invDef;

    }

    /**
     * This method removes EJB references and deletes invoice file
     */
    protected void cleanup() {

        removeEJBReference(invoiceUpload);
        // delete file
        if (invoiceFile != null && !invoiceFile.delete()) {
            logError("Error: Could not delete file: " + invoiceFile.getName());
        }
    }

    /**
     * This method removes EJB references
     */
    protected void removeEJBReference(EJBObject objRef) {
        try {
            if (objRef != null) {
                objRef.remove();
                objRef = null;
            }
        } catch (Exception e) {
            logError("While removing instance of EJB object", e);
        }
    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText) {
        String str = " : UploadFilename: " + fileName
                + "  :: InvoiceUpload_Oid: " + invoiceFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + str,
                Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText, Exception e) {
        String str = " : UploadFilename: " + fileName
                + "  :: InvoiceUpload_Oid: " + invoiceFileOid + " ";
        Logger.getLogger().log(
                loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + ": " + e.getMessage()
                + str, Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs Debug info
     *
     */
    public void logDebug(String infoText) {
        String str = " : UploadFilename: " + fileName
                + "  :: InvoiceUpload_Oid: " + invoiceFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": INFO: " + infoText + str,
                Logger.DEBUG_LOG_SEVERITY);
    }

    private void addToErrorList(InvoiceSummaryDataObject invOrderObject) {
        String[] substitutionValues = {"Invoice Summary Data",
            invOrderObject.getInvoice_id()};
        IssuedError ie = ErrorManager.getIssuedError(
                csdb.getLocaleName(), TradePortalConstants.ERROR_SAVING_PO,
                substitutionValues, null);
        invOrderObject.getErrorList().add(ie);

    }

    private void validatePaymentDate(InvoiceSummaryDataObject invSummData, MediatorServices mediatorServices) throws RemoteException, AmsException {

        InvoiceUtility.ATPInvoiceRules atpRules = InvoiceUtility.getATPInvoiceRules(corpOrg);
        try {
            InvoiceUtility.validatePaymentDate(invSummData.getPayment_date(), invSummData.getDue_date(), atpRules, mediatorServices);
        } catch (AmsException e) {
            IssuedError is = e.getIssuedError();
            if (is != null) {
                String errorCode = is.getErrorCode();

                if (TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL
                        .equals(errorCode)
                        || TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL
                        .equals(errorCode)) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ATP_INVOICE_UPLOAD_PAYMENT_DATE_RULE,
                            invSummData.getInvoice_id());
                    // clear payment date
                    invSummData.getFields().put("payment_date", null);
                    invSummData.setPayment_date(null);

                    hasError = true;
                } else {
                    mediatorServices.debug("InvoiceUploadFileProcessor.validatePaymentDate: Unexpected error: "
                            + e.toString());
                    throw e;
                }
            } else {
                mediatorServices.debug("InvoiceUploadFileProcessor.validatePaymentDate: Unexpected error: "
                        + e.toString());
                throw e;
            }
        }
    }

    /**
     * Added for CR 914A - Checking if the errorText is a generic error message.
     * Such messages are displayed without an Invoice ID
     *
     * @param errText
     * @return
     */
    private boolean isGenericError(String errText) {
        return (errorbundle.getString(TradePortalConstants.GENERIC_END_TO_END_ERROR_MSG).equals(errText));
    }

    /**
     * This method will check if invoices within an End-to-End group have same
     * due date and payment dates
     *
     * @param invSummData
     * @param paymentDateFieldPosition
     * @return
     */
    private boolean checkForSameDates(InvoiceSummaryDataObject invSummData, int paymentDateFieldPosition) {
        Date dueDateCurr;
        Date dueDateExisting;
        Date paymentDateCurr;
        Date paymentDateExisting;
        boolean checkForSamePaymentDate = false;
        if (paymentDateFieldPosition != -1) {
            checkForSamePaymentDate = true;
        }
        if (StringFunction.isNotBlank(invSummData.getEnd_to_end_id())) {
            if (endToEndIDSet.add(invSummData.getEnd_to_end_id()) || dueDateMap.get(invSummData.getEnd_to_end_id())==null) {
                dueDateMap.put(invSummData.getEnd_to_end_id(), invSummData.getDue_date());
                if (checkForSamePaymentDate) {
                    paymentDateMap.put(invSummData.getEnd_to_end_id(), invSummData.getPayment_date());
                }
            } else {
                dueDateCurr = invSummData.getDue_date();
                dueDateExisting = dueDateMap.get(invSummData.getEnd_to_end_id());
                if (dueDateCurr.compareTo(dueDateExisting) != 0) {
                    return false;
                }
                if (checkForSamePaymentDate) {
                    paymentDateCurr = invSummData.getPayment_date();
                    paymentDateExisting = paymentDateMap.get(invSummData.getEnd_to_end_id());
                    if ((paymentDateCurr == null && paymentDateExisting != null)
                            || (paymentDateExisting == null && paymentDateCurr != null)) {
                        return false;
                    }
                    if (paymentDateCurr != null && paymentDateExisting != null && paymentDateCurr.compareTo(paymentDateExisting) != 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private String getEndToEndIDSeq(String corpOrgOid, String invoiceID)
            throws AmsException {

        String sql = " SELECT end_to_end_group_num FROM invoices_summary_data where a_corp_org_oid = ? AND invoice_id = ? ";
        DocumentHandler invDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{corpOrgOid, invoiceID});
        return invDoc.getAttribute("/ResultSetRecord(0)/END_TO_END_GROUP_NUM");
    }

    /**
     * This method will check if invoices within an End-to-End group have same
     * payment method
     *
     * @param invSummData
     * @param paymentDateFieldPosition
     * @return
     */
    private boolean checkForSamePaymentMethod(InvoiceSummaryDataObject invSummData) {
        String paymentMethodCurr;
        String paymentMethodExisting;
        
        if (endToEndIDSet.add(invSummData.getEnd_to_end_id()) || StringFunction.isBlank(paymentMethodMap.get(invSummData.getEnd_to_end_id()))) {
            paymentMethodMap.put(invSummData.getEnd_to_end_id(), invSummData.getPay_method());

        } else {
            paymentMethodCurr = invSummData.getPay_method();
            paymentMethodExisting = paymentMethodMap.get(invSummData.getEnd_to_end_id());
            if ((StringFunction.isBlank(paymentMethodCurr) && StringFunction.isNotBlank(paymentMethodExisting))
                    || (StringFunction.isBlank(paymentMethodExisting) && StringFunction.isNotBlank(paymentMethodCurr))) {
                return false;
            }
            if (StringFunction.isNotBlank(paymentMethodCurr) && StringFunction.isNotBlank(paymentMethodExisting)
                    && !paymentMethodCurr.equals(paymentMethodExisting)) {
                return false;
            }
        }

        return true;
    }
    
    private static class InvoiceProperties {
    	Map<String,String> opOrgCountryForPYBMap = new HashMap<>();
		Map<String, String> instrRPMMap = new HashMap<>();
        Map<String,String> paymentMethodMap = new HashMap<>();
        Map<String,Object> tradingPartnerRuleMap= new HashMap<>();
    	public Map<String, String> getOpOrgCountryForPYBMap() {
			return opOrgCountryForPYBMap;
		}
		public Map<String, String> getPaymentMethodMap() {
			return paymentMethodMap;
		}
		public Map<String, Object> getTradingPartnerRuleMap() {
			return tradingPartnerRuleMap;
		}

		public Map<String, String> getInstrRPMMap() {
			return instrRPMMap;
		}
		
    }
	
	/**
     * Added for CR-1001 Rel9.4
     * @param invSummaryData
     * @param isValid
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean validateReportingCodes(InvoiceSummaryDataObject invSummaryData, boolean isValid, MediatorServices mediatorServices,Map<String,String> paymentMap) throws RemoteException, AmsException {
         
        String paymentMethod = invSummaryData.getPay_method();
        String reportingCode1DataReq = paymentMap.get("reporting_code_1_data_req");
        String reportingCode2DataReq = paymentMap.get("reporting_code_2_data_req");
        
        String[] reportingCodes = new String[2];
    	reportingCodes[0] = invSummaryData.getReporting_code_1();
    	reportingCodes[1] = invSummaryData.getReporting_code_2();
    	
		String bankGroupOid = paymentMap.get("bankGroupOid");
		
        isValid = InvoiceUtility.validateReportingCodes(bankGroupOid,reportinCodeCache, paymentMethod, reportingCodes,reportingCode1DataReq, reportingCode2DataReq, mediatorServices.getErrorManager(),isValid); 
        
        return isValid;
    }

    
    
}
