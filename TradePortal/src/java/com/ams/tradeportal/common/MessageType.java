package com.ams.tradeportal.common;

public enum MessageType {
	;

	public static final String PAYMENT_MATCH = "PAYINVIN";
	public static final String ACCT_BALANCE_REQUEST = "ACTBALOT";
	public static final String ACCT_TRANSACTION_REQUEST = "ACTTRNOT";
	public static final String FX_ONLINE_RATE_REQUEST = "FXQUOTOT"; // NSX CR-640/581 Rel 7.1.0 09/26/11
	public static final String FX_ONLINE_DEAL_REQUEST = "FXDEALOT"; // NSX CR-640/581 Rel 7.1.0 09/26/11
	public static final String CONFIRMATION = "MSGCONF";
	public static final String TPL = "TPL";
	public static final String TPLH = "TPLH";// Leelavathi PPX-208 29/4/2011 added
	public static final String PURGE = "PURGE";
	public static final String HISTORY = "HISTORY";
	public static final String MAIL = "MAIL";
	public static final String POR = "POR"; // ShilpaR CR-707 Rel 8.0
	public static final String POM = "POM"; // Srinivasu_D CR-707 Rel 8.0 03/14/2012
	public static final String TRIG = "TRIGGER"; // CR-710 Narayan 03/08/2012 Rel 8.0
	public static final String DATAIN = "DATAIN"; // CR-587 MJ 06/17/2011
	public static final String PAYSTAT = "PAYSTAT"; // CR-596 Narayan 11/22/2010
	public static final String PAYFILE = "PAYFILE"; // CR-593 NSX 04/04/11 Rel. 7.0.0
	public static final String PAYBACK = "PAYBACK"; // CR-593 CJR 04/06/2011
	public static final String PAYCOMP = "PAYCOMP"; // CR-593 CJR 04/06/2011
	public static final String DIRECTDEBIT = "DIRDEB";
	public static final String TPLFXRT = "TPLFXRT"; // CR-469 NShrestha 05/18/2009
	public static final String INVSTO = "INVSTO"; // Vshah 12/11/2008 INVSTO UnPackager
	public static final String ARINVOICE = "ARIINV"; // Pramey - 01st Dec 08 - CR-434
	public static final String EODTRNIN = "EODTRNIN"; // peter ng - 26th Nov 08 - CR-451
	public static final String INVSTI = "INVSTI"; // rbhaduri - 24th Nov 08 - CR-434
	public static final String RPMRULE = "RPMRULE"; // Vshah - CR-434
	public static final String PAYINVOT = "PAYINVOT";
	public static final String PAYINVIN = "PAYINVIN";
	public static final String REFDATA = "REFDATA";
	public static final String WORKSTAT = "WORKSTAT";
	public static final String INSSTATR = "INSSTATR";// Leelavathi PPX-208 29/4/2011 added
	public static final String INSSTATH = "INSSTATH";// Leelavathi PPX-208 29/4/2011 added
	public static final String INSSTAT = "INSSTAT";
	public static final String INTEREST_RATE = "TPLINTRT"; // Srini_D CR-713 Rel8.0 10/18/2011
	public static final String BULKINVUPL = "BULKINV"; // ShilpaR- CR-710
	public static final String INVUPL = "INV"; // ShilpaR- CR-710
	public static final String INM = "INM";// SHR CR708 Rel8.1.1.0
	public static final String CUSREF = "CUSREFIN"; // IValavala CR741 Rel 8.2
	public static final String PINCNAPP = "PINCNAPP";// CR1006
	public static final String GT = "GTRSWFT";
	public static final String TRADEBEAM     = "LCPSWFT";

}
