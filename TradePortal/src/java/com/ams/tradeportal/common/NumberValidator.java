package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import java.text.*;
import java.util.*;

/**
 * This class is used to enforce all Trade Portal number formatting rules.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NumberValidator
 {
private static final Logger LOG = LoggerFactory.getLogger(NumberValidator.class);
    /**
	*  This method validates a string representing a number or decimal value.  All Trade Portal
	*  number formatting rules are enforced in this class.  If an error occurs, an exception is thrown.
	*  Typically, this exception is caught by AttributeMgr, which creates an error message from that
	*  exception.
	*
	*  A string is returned that contains the representation of the number passed in, but with
	*  the grouping separators removed and the decimal separator replaced with a '.'  This assures
	*  that the value will succesfully be placed into the database.<p>
	* For example:<br>
	* 12,345.67 returns 12345.67<br>
	* 12.345,67 (for Sweden) returns 12345.67<br>
	* 1,2,3,4.55 throws an exception
	* 
	* @param number String - the string representing the number to be validated
	* @param locale String - the locale used to determine the decimal and grouping separator
	*/
	public static String getNonInternationalizedValue(String number, String localeStr)
	    throws InvalidAttributeValueException
	 {
		 // Default is for an unsigned number.
		 return getNonInternationalizedValue(number, localeStr, false);
   }            
   





   /**
	* This just localizes the throws clause for the InvalidAttributeValueException by having 
	* 1 method that throws the exception.  It should make the resulting code easier to read.
	* 
	* @throws InvalidAttributeValueException    
	*/      
   private static void throwInvalidAttribute( ) throws InvalidAttributeValueException
   {
		throw new InvalidAttributeValueException(AmsConstants.INVALID_NUMBER_ATTRIBUTE);
	
   }   
   
   /**
	* Validates the value against all rules dealing with the grouping separator.
	* (everything left of the decimal place)
	* 
	* @param value char[] - The string to validate
	* @param groupingSeparator char - the grouping separator character
	* @throws InvalidAttributeValueException    
	*/      
   private static void validateLeftOfDecimal(char[] value, char groupingSeparator,
											 char decimalSeparator, boolean signed )
	throws InvalidAttributeValueException
	{
		//No reason to execute code if first character is a decimal separator
		if(value[0] == decimalSeparator )
			return;
	
	    // If the value is signed and has a negative sign,
	    // ignore the negative sign for this method
	    int startingIndex;
	    
	    if(signed && (value[0] == '-'))
	     {
	         startingIndex = 1;
	     }
	    else
	     {
	         startingIndex = 0;  
	     }
		
		// Make sure the first character in the array is NOT a grouping seperator
		// otherwise it fails rule about a digit on both sides of the grouping seperator.
		if( isValidGroupingSeparator( value[0], groupingSeparator ) )
			throwInvalidAttribute( );
		
		int digitCnt = 0;               //Counting the number of digits found
		boolean groupingFlag = false;   //flag to determine if we've found a grouping seperator
		
		for( int i=startingIndex; i<value.length; i++ )
		{
			digitCnt ++;
			
			// Make sure the decimal seperator isn't showing up with less than 3 digits between 
			// the decimal and the grouping seperators...
			if( (i >= 3)        && 
				(digitCnt <= 3) && 
				(value[i] == decimalSeparator)   )
					throwInvalidAttribute( );
			
			// Make sure there's a grouping seperator every 3 digits
			if( (digitCnt == 4)               && 
				(Character.isDigit(value[i])) && 
				(groupingFlag == true)          )
					throwInvalidAttribute( );
				
			if( isValidGroupingSeparator( value[i], groupingSeparator ) )
			{
				// if a grouping seperator turns up after 4 digits then the user is'nt intending
				// on leaving out all the seperators...therefore issue an error.
				if( digitCnt > 4 )
					throwInvalidAttribute( );
				
				// Make sure the grouping seperator isn't showing up when there's less than 3
				// digits and another grouping seperator has already been displayed.
				
				if( (digitCnt <= 3) && 
					(i >= 3)        && 
					(groupingFlag == true) )
						throwInvalidAttribute( ); 
				
				// Make sure there's a digit on both sides of the groupingSeperator
				// The try/catch makes sure we avoid an ArrayIndexOutOfBound error 
				// ...meaning there was a grouping seperator at the end of the string (w/no decimal)
				try
				{
					if( !( Character.isDigit(value[i-1]) && Character.isDigit(value[i+1]) ) )
						  throwInvalidAttribute( );
				
					//Make sure user does'nt try to enter (USD) '1,1' or '1,12' 
					if( (value[i+2] == decimalSeparator) || (value[i+3] == decimalSeparator) )
						  throwInvalidAttribute( );

				}catch( ArrayIndexOutOfBoundsException e )
				{   throwInvalidAttribute( );
				}
						
				digitCnt = 0;            //We found a grouping seperator therefore start the cnt over
				groupingFlag = true;     //We found a grouping Seperator therfore enforce grouping rules
			}
			
			//We pass all the rules and find a decimal seperator
			if(value[i] == decimalSeparator)
				return;
		}
	
	}
	
   /**
	* Validates the value against all rules dealing with the decimal separator.
	*
	* 
	* @param value char[] - The string to validate
	* @param decimalSeparator char - the decimal separator character
	* @throws InvalidAttributeValueException    
	*/    
   private static void validateRightOfDecimal(char[] value, char groupingSeparator, 
											  char decimalSeparator)
	 throws InvalidAttributeValueException
	 {
		int decIndex = -1;
		int decIndexNum = 0;
		
		for( int i=0; i<value.length; i++ )
		{
			if( value[i] == decimalSeparator )
			{   
				decIndex = i;           // found a decimal seperator...keeping track of it's index val
				decIndexNum += 1;       // keep track of how many we find
				
				// Verify there's not more than one decimal seperator
				if( decIndexNum > 1 )
					throwInvalidAttribute( );
			}

			//Validate no groupingSeperators appear after a decimal seperator
			if( (decIndex != -1) && ( isValidGroupingSeparator( value[i], groupingSeparator )) )
				throwInvalidAttribute( );
		}
	 }
 
  
	
    /**
	*  This method validates a string representing a number or decimal value.  All Trade Portal
	*  number formatting rules are enforced in this class.  If an error occurs, an exception is thrown.
	*  Typically, this exception is caught by AttributeMgr, which creates an error message from that
	*  exception.
	*
	*  A string is returned that contains the representation of the number passed in, but with
	*  the grouping separators removed and the decimal separator replaced with a '.'  This assures
	*  that the value will succesfully be placed into the database.<p>
	* For example:<br>
	* 12,345.67 returns 12345.67<br>
	* 12.345,67 (for Sweden) returns 12345.67<br>
	* 1,2,3,4.55 throws an exception<br>
	* -1,234.56 returns -1234.56<br>
	* +1,234.56 throws an exception ('-' is only valid sign character)
	* 
	* @param number String - the string representing the number to be validated
	* @param locale String - the locale used to determine the decimal and grouping separator
	* @param signed boolean - indicates if number is signed (meaning the number
	*                         may contain '-' as the first character)
	*/
	public static String getNonInternationalizedValue(String number, String localeStr, boolean signed)
	    throws InvalidAttributeValueException
	 {
		// In the off chance that the number passed in was null, stop here and return
		if( (number == null) || (number.equals("")) )
			return number;
		
		// Create a character array from the number passed in to be more efficient
		
		// DK IR LMUM050355987 Rel 8.0 05/10/2012 Begin
		char[] unformattedValue = number.toCharArray();
		char[] value = null;
		// DK IR LMUM050355987 Rel 8.0 05/10/2012 End
		
		// In the off chance that a null value for the locale was entered...Default to US
		if( localeStr == null)
			localeStr = "en_US";
			
		Locale locale = new Locale(localeStr.substring(0,2),
			                       localeStr.substring(3,5));        
		
		// DK IR LMUM050355987 Rel 8.0 05/10/2012 Begin
		
		NumberFormat nf = NumberFormat.getInstance(locale);
		try{
			Number num = nf.parse(number);
			value = nf.format(num).toCharArray();
		}
		catch(ParseException pe){
			LOG.error("Error in getNonInternationalizedValue() ",pe);
		}
		
		// DK IR LMUM050355987 Rel 8.0 05/10/2012 End
		// Determine the decimal format symbols for this locale    
		DecimalFormatSymbols dfs = 
			( (DecimalFormat) NumberFormat.getNumberInstance(locale)).getDecimalFormatSymbols();
		
		char decimalSeparator = dfs.getDecimalSeparator();
		char groupingSeparator = dfs.getGroupingSeparator(); 
		
		//9.3.5 IR T36000045191 - validate amount and return empty string if there are morethan one decimalSeparator's - Begin
		int decimalSeparatorCount = 0;
		for(int i=0;i<unformattedValue.length;i++){
			if (Character.isDigit(unformattedValue[i])) continue;
			
			if (unformattedValue[i] == decimalSeparator){
				decimalSeparatorCount++;
				continue;
			}
		}
		if(decimalSeparatorCount > 1) return "";
		//9.3.5 IR T36000045191 - End
		
		// Validate that there are no characters other than the decimal separator,
		// the grouping separator and digits
		validateInvalidCharacters(unformattedValue, groupingSeparator, decimalSeparator, signed);
  
		// Validate rules dealing with the grouping separator
		if (value!=null) validateLeftOfDecimal(value, groupingSeparator, decimalSeparator, signed);
		
		// Validate rules dealing with the decimal separator
		if (value!=null) validateRightOfDecimal(value, groupingSeparator, decimalSeparator);
		
		// If the code has made it this far without throwing an exception,
		// it is a number that satisfies all of the rules.
		// DK IR LKUM051747946 Rel 8.0 06/04/2012 Begin
		return takeOutLocaleSpecificCharacters(unformattedValue, decimalSeparator, signed);
		// DK IR LKUM051747946 Rel 8.0 06/04/2012 Begin
   }

      /**
	* Because the Jsp can send a non-breaking space OR a regular space when validating a number entry,
	* this method handles this situation and returns a boolean to indicate if current data (val) is
	* equal to the grouping Seperator.  In the event the grouping seperator IS a non-breaking space,
	* we need to let the user also allow a space as an entry.  Since this logic is used more than once
	* it was put into it's own method.
	* IE...Avoiding difference in unicode value of non breaking space vs a regular space
	* 
	* @param value char[] - The string to validate
	* @param groupingSeparator char - the grouping separator character   
	*/      
   private static boolean isValidGroupingSeparator(char val, char groupingSeparator )
   {
		final char nonBreakSpace = '\u00a0';
		
		if( groupingSeparator == nonBreakSpace )
		{
			if( (val == nonBreakSpace) || (val == ' ') )
				return true;
			
		}else if( val == groupingSeparator )
			return true;
		
		return false;
   }     

   /**
	* Removes all characters from the string except digits and the decimal
	* separator.  In the resulting string, the decimal separator is converted
	* to a '.'  
	* 
	* @param value char[] - The string to start with when removing locale specific characters
	* @param decimalSeparator char - the decimal separator character
	* @param signed boolean - indicates if number is signed
	* @return String - a string with all locale-specific characters removed.
	*/  
   private static String takeOutLocaleSpecificCharacters(char[] value, char decimalSeparator, boolean signed)
	{  
	   StringBuilder result = new StringBuilder();
	   
	   for(int i=0;i < value.length;i++)
		{
			if(Character.isDigit(value[i]))
				result.append(value[i]);
			else if(value[i] == decimalSeparator)
				result.append('.');
			else if ((i == 0) && (signed) && (value[0] == '-'))
				result.append(value[0]);
		 }
		 
	   return result.toString();  
	}

   /**
	* Validates the value to assure that no characters exist other than the
	* digits, the decimal separator, and the grouping separator.  (and a -
	* for signed value)
	* 
	* @param value char[] - The string to validate
	* @param decimalSeparator char - the decimal separator character
	* @param groupingSeparator char - the grouping separator character
	* @param signed boolean - indicates if the number is signed (this means
	*                         that '-' is accepted in the first position)
	* @throws InvalidAttributeValueException
	*/   
   private static void validateInvalidCharacters(char[] value, 
	                                             char groupingSeparator, 
	                                             char decimalSeparator,
	                                             boolean signed)
	 throws InvalidAttributeValueException
	{
		for(int i=0;i<value.length;i++)
		 {
			if (Character.isDigit(value[i])) continue;
			
			if (value[i] == decimalSeparator){
				continue;
			}

			if (isValidGroupingSeparator(value[i], groupingSeparator)) continue;

			// Negative sign is only supported in the first position and
			// only for signed values.
			if ((i==0) && (signed && value[0] == '-')) continue;

	        throw new InvalidAttributeValueException(AmsConstants.INVALID_NUMBER_ATTRIBUTE);
		 } 
		
	}

	}