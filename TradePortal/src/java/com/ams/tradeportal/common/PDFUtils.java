package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


import java.util.Map;



import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;

import org.apache.avalon.framework.configuration.*;
import org.apache.avalon.framework.service.*;
import org.apache.fop.apps.*;

import com.ams.tradeportal.producer.EmailAttachment;

public class PDFUtils {
private static final Logger LOG = LoggerFactory.getLogger(PDFUtils.class);
	//BSL Cocoon Upgrade 10/06/11 Rel 7.0 BEGIN
//  public static MessageFormat _documentLink = new MessageFormat ("{4}/Document.xml?producer={0}&transaction={1}&locale={2}&bank_prefix={3}&report=document.pdf");
//	public Reader getPDFDoc(String serverLocation, String producer, String transaction, String locale, String bankPrefix){
//	String[] formatArgs = { producer, EncryptDecrypt.encryptAlphaNumericString(transaction), locale, bankPrefix, serverLocation };
//	String urlString = _documentLink.format (formatArgs);
//	
//	LOG.info("URL = " + urlString);
//	BufferedReader rd = null;
//	try{
//		URL url = new URL(urlString);
//  	  	URLConnection conn = url.openConnection();
//  	  	rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//	}catch(MalformedURLException me){
//		me.printStackTrace();
//	} catch(IOException ie){
//		ie.printStackTrace();
//	}
//	  
//	  	return rd;
//	}
//	public void generatePDFDoc(Map parms, Map attributes, OutputStream out) throws Exception {
//		InputStream configStream = this.getClass().getClassLoader().getResourceAsStream("cocoon.properties");
//		Configurations confs = new Configurations(configStream);
//		CocoonWrapper engine = new CocoonWrapper(confs);
//		engine.handle(out, parms, attributes);
//	}
    /**
     * Name of the FOP config file, which must be located on the classpath
     */
	public static final String CONTEXT_PATH = "portal";

    /**
     * Name of the FOP config file, which must be located on the classpath
     */
	public static final String FOP_CONFIG_FILE = "fop-config.xml";

	/**
     * Factory to create fop objects
     */
    private static FopFactory fopFactory = null;
    private static boolean isFopConfigured = false;

    /**
     * Manager to get URLFactory from.
     */
    private static ServiceManager serviceManager = new DefaultServiceManager();

    private static FopFactory getFopFactory() throws FOPException, ConfigurationException {
    	if (fopFactory == null) {
    		fopFactory = FopFactory.newInstance();
    	}

    	if (!isFopConfigured) {
   			fopFactory.setUserConfig(getConfiguration());
   			isFopConfigured = true;
    	}

    	return fopFactory;
    }

    private static Configuration getConfiguration() throws ConfigurationException {
        String configUrl = FOP_CONFIG_FILE;
        try {
            InputStream configStream = PDFUtils.class.getClassLoader().getResourceAsStream(configUrl);
            DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
            return cfgBuilder.build(configStream);
        }
        catch (Exception e) {
        	String errorMsg = "Cannot load configuration from " + configUrl;
            System.err.println(errorMsg);
            throw new ConfigurationException(errorMsg, e);
        }
    }
 // SL - 10/29/2014 R9.1 IR T36000030417  [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation 
	public void generatePDFDoc(Map parms, Map attributes, OutputStream out, String orderingPartyName) throws Exception {
		FOUserAgent foUserAgent = getFopFactory().newFOUserAgent();
		Fop fop = getFopFactory().newFop(MimeConstants.MIME_PDF, foUserAgent, out);
		String xsltName = (String)parms.get("stylesheet");//BSL Cocoon Upgrade 11/15/11 Rel 7.1 - remove xsltName from method signature b/c the stylesheet name is already available in parms

		// Setup XSLT
		InputStream xsltStream = this.getClass().getClassLoader().getResourceAsStream(xsltName);
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(new StreamSource(xsltStream));

		// Setup input for XSLT transformation
		EmailAttachment attachment = new EmailAttachment();
		Reader sourceReader = attachment.getStream(CONTEXT_PATH,
				parms, attributes, orderingPartyName);
		Source source = new StreamSource(sourceReader);

		// Resulting SAX events (the generated FO) must be piped through to FOP
		Result result = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		transformer.transform(source, result);
	}
	//BSL Cocoon Upgrade 10/06/11 Rel 7.0 END
}
