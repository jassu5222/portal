package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



// Java imports
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.util.Date;
import java.util.Iterator;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignature;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.PGPUtil;

import com.amsinc.ecsg.frame.AmsException;
//import sun.misc.BASE64Encoder;
//import sun.misc.BASE64Decoder;
// Bouncy castle imports
import com.amsinc.ecsg.util.StringFunction;
//
/**

 *
 */

public class PGPEncryptDecrypt {
private static final Logger LOG = LoggerFactory.getLogger(PGPEncryptDecrypt.class);
	
	private static final String PGP_KEY_PASS = "PGPKeyPass";

	private static final String PGP_SECRECT_KEY_RING = "PGPSecrectKeyRing";

	private static final String PGP_PUBLIC_KEY_RING = "PGPPublicKeyRing";

	private static final String BOUNCY_CASTLE_PROVIDER = "BC";

	private static String privateKeyPassword = null;
	
	private static PGPSecretKey siginingKey = null;

	private static PGPSecretKeyRingCollection pgpSec = null;
	private static PGPPublicKeyRingCollection pgpPub = null;


	static {
		
			try {
				init();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		
	}

	/**
	 * @param inputData
	 * @return
	 * @throws AmsException 
	 */
	public static InputStream decrypt(byte[] inputData) throws AmsException {

		try {
			LOG.debug("decrypt(): data length=" + inputData.length);
			ByteArrayInputStream bais = new ByteArrayInputStream(inputData);
			return decrypt(bais);
			
		} 
		catch (PGPException ex)		
		{
			System.err.println(ex.getUnderlyingException().toString());
			ex.printStackTrace();
			throw new AmsException(ex.getUnderlyingException().getMessage());
		}
		catch (Exception ex)
		{
			System.err.println(ex.getMessage());
			ex.printStackTrace();
			throw new AmsException("error occured during decryption, Reason: " + ex.getMessage());
		}
		
	
	}


	
	/**
	 * @param inputData
	 * @return
	 * @throws AmsException
	 */
	public static InputStream decrypt(String inputData) throws AmsException {
		if (StringFunction.isBlank(inputData)) {
			throw new IllegalArgumentException("input data is null or blank");
		}
		
		return decrypt(inputData.getBytes());
	}

	
	/**
	 * @param inputData
	 * @return
	 * @throws AmsException 
	 */
	public static InputStream verifyAndDecrypt(String inputData) throws AmsException {
		
		if (StringFunction.isBlank(inputData)) {
			throw new IllegalArgumentException("input data is null or blank");
		}
		
		return verifyAndDecrypt(inputData.getBytes());
	}
	
	
	/**
	 * @param inputData
	 * @return
	 * @throws AmsException 
	 */
	public static InputStream verifyAndDecrypt(byte[] inputData) throws AmsException {
		verify(inputData);
		return decrypt(inputData);
	}
	
	
	/**
	 * @param data
	 * @param armor
	 * @param userID
	 * @return
	 * @throws AmsException
	 */
	public static String encrypt(String data, boolean armor, String userID) throws AmsException {
		byte[] encData = encrypt(data.getBytes(),  armor,  userID, false);
		return new String(encData);
	}
	
	
	/**
	 * @param data
	 * @param armor
	 * @param userID
	 * @return
	 * @throws AmsException
	 */
	public static String encryptAndSign(String data, boolean armor, String userID) throws AmsException {
		byte[] encData = encrypt(data.getBytes(),  armor,  userID, true);
		return new String(encData);
	}

	
	/**
	 * @param data
	 * @param armor
	 * @param userID
	 * @return
	 * @throws AmsException
	 */
	public static String encryptAndSign(byte[] data, boolean armor, String userID) throws AmsException {
		byte[] encData = encrypt(data,  armor,  userID, true);
		return new String(encData);
	}
	
	
	/**
	 * @param data
	 * @param userID
	 * @return
	 * @throws AmsException 
	 */
	public static byte[] encrypt(byte[] data, boolean armor, String userID, boolean sign) throws AmsException {
		long l1 = System.currentTimeMillis();
		
		File tempfile = null;
		try
		{

			// create a temp file 
			tempfile = createTempFile(data);

			PGPPublicKey pkey= getPublicKey(userID);

			// Encrypt passed data
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			if (sign) {
				encryptSignFile(baos, tempfile.getAbsolutePath(), pkey, armor);	
			}
			else {
			    encryptFile(baos, tempfile.getAbsolutePath(), pkey, armor);
			}
			
			LOG.debug("encrypted text length=" + baos.size());			

			time("encrypt " , l1, System.currentTimeMillis());
			return baos.toByteArray();
		}
		catch (PGPException ex)
		{
			System.err.println(ex.getUnderlyingException().toString());
			ex.printStackTrace();
			throw new AmsException(ex.getUnderlyingException().getMessage());
		}
		catch (Exception ex)
		{
			System.err.println(ex.getMessage());
			ex.printStackTrace();
			throw new AmsException(ex.getMessage());
		}
		finally {
			if (tempfile != null) {
				tempfile.delete();
			}
		}
		
	}

	/**
	 * @param data
	 * @return
	 * @throws IOException
	 */
	protected static File createTempFile(byte[] data) throws IOException  {
		long l1 = System.currentTimeMillis();
		File tmpFile = File.createTempFile("tmp", null);
		try(FileOutputStream fos = new FileOutputStream(tmpFile)){
			fos.write(data);
		}
		time("createTempFile " , l1, System.currentTimeMillis());
		return tmpFile;
	}
	
	
	/**
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	protected static PGPPublicKey getPublicKey(String userId) throws PGPException {
		
		LOG.debug("getPublicKey(): lookup public key using id: " + userId);
		
		Iterator it = pgpPub.getKeyRings(userId);
		while (it.hasNext()) {
            PGPPublicKeyRing pkRing = (PGPPublicKeyRing) it.next();
			Iterator pkIt = pkRing.getPublicKeys();
			while (pkIt.hasNext()) {
				PGPPublicKey key = (PGPPublicKey) pkIt.next();
				if (key.isEncryptionKey()) {
					return key;
				}
			}
		}

		throw new PGPException("No public key found for given id: " + userId );
	}



	/**
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	protected static PGPSecretKey getSecretKey(String userId) throws PGPException {
		
		LOG.debug("getPublicKey(): lookup secret key using id: " + userId);

		Iterator it = pgpSec.getKeyRings(userId);
		while (it.hasNext()) {
            PGPSecretKeyRing pkRing = (PGPSecretKeyRing) it.next();
			Iterator pkIt = pkRing.getSecretKeys();
			while (pkIt.hasNext()) {
				PGPSecretKey key = (PGPSecretKey) pkIt.next();
				if (key.isMasterKey()) {
					return key;
				}
			}
		}

		throw new PGPException("No Secret key found for given id: " + userId );
	}
	
	
	/**
     * Returns signing key from secret key ring.
     * 
     * @return
     * @throws PGPException
     */
	private static PGPSecretKey getSigningKey()
	{


		//PGPSecretKey    sKey = null;
		Iterator it = pgpSec.getKeyRings();


		while (it.hasNext())
		{
			PGPSecretKeyRing    skRing = (PGPSecretKeyRing)it.next();    
			Iterator            it1 = skRing.getSecretKeys();

			while (it1.hasNext())
			{
				PGPSecretKey    key = (PGPSecretKey)it1.next();

				if (key.isSigningKey())
				{
					return key;
				}
			}
		}

        // signingKey not found   
		throw new IllegalArgumentException("Can't find signing key in key ring.");
	}
	
	/**
	 * @param os
	 * @param fileName
	 * @param pKey
	 * @param armor
	 * @param integrityCheck
	 * @throws IOException
	 * @throws NoSuchProviderException
	 * @throws PGPException 
	 */
	protected static void encryptSignFile(OutputStream os, String fileName, PGPPublicKey pKey, boolean armor)
	                                throws IOException, NoSuchProviderException, NoSuchAlgorithmException, SignatureException,  PGPException  	{

		if (armor)
		{
			os = new ArmoredOutputStream(os);
		}

		PGPEncryptedDataGenerator encDataGen = new PGPEncryptedDataGenerator(PGPEncryptedData.CAST5, false, new SecureRandom(), BOUNCY_CASTLE_PROVIDER);
		encDataGen.addMethod(pKey);
		OutputStream  outStream = encDataGen.open(os, new byte[1 << 16]);

		PGPPrivateKey  pgpPrivKey = siginingKey.extractPrivateKey(privateKeyPassword.toCharArray(), BOUNCY_CASTLE_PROVIDER);
		PGPSignatureGenerator sGen = new PGPSignatureGenerator(siginingKey.getPublicKey().getAlgorithm(), PGPUtil.SHA1, BOUNCY_CASTLE_PROVIDER);

		sGen.initSign(PGPSignature.BINARY_DOCUMENT, pgpPrivKey);

		Iterator it = siginingKey.getPublicKey().getUserIDs();
		if (it.hasNext())
		{
			PGPSignatureSubpacketGenerator sigPackGen = new PGPSignatureSubpacketGenerator();

			sigPackGen.setSignerUserID(false, (String)it.next());
			sGen.setHashedSubpackets(sigPackGen.generate());
		}

		PGPCompressedDataGenerator  pgpCompGen = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
		BCPGOutputStream bOut = new BCPGOutputStream(pgpCompGen.open(outStream));
		sGen.generateOnePassVersion(false).encode(bOut);

		File file = new File(fileName);
		PGPLiteralDataGenerator  ldGen = new PGPLiteralDataGenerator();
		OutputStream  ldOut = ldGen.open(bOut, PGPLiteralData.BINARY, file.getName(), new Date(),new byte[1<<16]);

		// use of buffering to speed up write
		byte[] buff = new byte[1<<16];
		try(FileInputStream fis = new FileInputStream(file)){

		int totBytesRead;
		while((totBytesRead = fis.read(buff)) != -1) {
			ldOut.write(buff,0,totBytesRead);
			sGen.update(buff,0,totBytesRead);                        
			ldOut.flush();
		}
		}

		ldGen.close();
		sGen.generate().encode(bOut);
		pgpCompGen.close();

		outStream.close();

		if (armor)
		{
			os.close();
		}


	}

	/**
	 * @param os
	 * @param fileName
	 * @param pKey
	 * @param armor
	 * @param integrityCheck
	 * @throws IOException
	 * @throws NoSuchProviderException
	 * @throws PGPException 
	 */
	protected static void encryptFile(OutputStream os, String fileName, PGPPublicKey pKey, boolean armor)
	                                                         throws IOException, NoSuchProviderException, PGPException 	{
		if (armor)
		{
			os = new ArmoredOutputStream(os);
		}

		PGPEncryptedDataGenerator   encDataGen = new PGPEncryptedDataGenerator(PGPEncryptedData.CAST5, false, new SecureRandom(), BOUNCY_CASTLE_PROVIDER);
		encDataGen.addMethod(pKey);

		OutputStream  outStream = encDataGen.open(os, new byte[1 << 16]);
		PGPCompressedDataGenerator  pgpCompGen = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
		PGPUtil.writeFileToLiteralData(pgpCompGen.open(outStream), PGPLiteralData.BINARY, new File(fileName), new byte[1 << 16]);

		pgpCompGen.close();
		outStream.close();

		if (armor)
		{
			os.close();
		}

	}

	/**
	 * @param in
	 * @return
	 * @throws IOException
	 */
	protected static PGPEncryptedDataList getPGPEncDataList(InputStream in) throws IOException {
		
		PGPEncryptedDataList encDataList;
		PGPObjectFactory pgpF = new PGPObjectFactory(in);
		Object obj = pgpF.nextObject();

		// handle if the first object is PGP marker packet
		if (obj instanceof PGPEncryptedDataList) {
			encDataList = (PGPEncryptedDataList) obj;
		} else {
			encDataList = (PGPEncryptedDataList) pgpF.nextObject();
		}

		return encDataList; 
	}

	/**
	 * @param in
	 * @return
	 * @throws Exception
	 */
	protected static InputStream decrypt(InputStream in) throws Exception {
		long l1 = System.currentTimeMillis();
		in = PGPUtil.getDecoderStream(in);


		PGPEncryptedDataList encDataList = getPGPEncDataList(in);

		PGPEncodedData encData = getSecretKey(encDataList);
		InputStream clear = encData.getPublicKeyEncData().getDataStream(encData.getPrivateKey(), BOUNCY_CASTLE_PROVIDER);
		PGPObjectFactory pgpObjF = new PGPObjectFactory(clear);
		Object obj = pgpObjF.nextObject();
        InputStream is;
		if (obj instanceof PGPCompressedData) {
			PGPCompressedData cData = (PGPCompressedData) obj;
			pgpObjF = new PGPObjectFactory(cData.getDataStream());
			obj = pgpObjF.nextObject();


			if (obj instanceof PGPLiteralData) {
				PGPLiteralData pgpLD = (PGPLiteralData) obj;
				is = pgpLD.getInputStream();
			} else {
				if (obj instanceof PGPOnePassSignatureList) {
					//PGPOnePassSignatureList     sigList = (PGPOnePassSignatureList)obj;
					//PGPOnePassSignature         sig = sigList.get(0);
					obj = pgpObjF.nextObject();
					if (obj instanceof PGPLiteralData) {
						PGPLiteralData pgpLD = (PGPLiteralData) obj;
						is = pgpLD.getInputStream();
					}
					else {
						throw new PGPException("cannot decrypt, invalid encrypted data...");
					}
				}
				else {
					throw new PGPException("cannot decrypt, invalid encrypted data...");
				}
			}
		}
		else {
			throw new PGPException("cannot decrypt, invalid encrypted data...");
		}
		time("decrypt " , l1, System.currentTimeMillis());

		return is;

	}

	/**
	 * @param data
	 * @return
	 * @throws AmsException 
	 */
	public static void verify(byte[] data) throws AmsException {
		long l1 = System.currentTimeMillis();
		
		if (data == null) {
			throw new IllegalArgumentException("input data is null");
		}
		
		try {
			LOG.debug("verify(): data length=" + data.length);
			
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			verifyData(bais);
			
			time("verify " , l1, System.currentTimeMillis());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace(); 
			throw new AmsException("Error occured while verifying signature, Reason: " + e.getMessage());
		}
		
	}


	/**
	 * @param in
	 * @throws Exception
	 */
	private static void verifyData(InputStream in)  throws Exception
	{
		in = PGPUtil.getDecoderStream(in);

		PGPEncryptedDataList encDataList = getPGPEncDataList(in);

		PGPEncodedData encData = getSecretKey(encDataList);
		InputStream clear = encData.getPublicKeyEncData().getDataStream(encData.getPrivateKey(), BOUNCY_CASTLE_PROVIDER);
		PGPObjectFactory pgpObjF = new PGPObjectFactory(clear);
		Object obj = pgpObjF.nextObject();
		if (obj instanceof PGPCompressedData) {
			PGPCompressedData           cdata = (PGPCompressedData)obj;

			pgpObjF = new PGPObjectFactory(cdata.getDataStream());

			obj = pgpObjF.nextObject();
			if (obj instanceof PGPOnePassSignatureList) {
				PGPOnePassSignatureList sigList = (PGPOnePassSignatureList)obj;

				PGPOnePassSignature sig = sigList.get(0);

				obj = pgpObjF.nextObject();
				if (obj instanceof PGPLiteralData) {
					PGPLiteralData pgpLD = (PGPLiteralData)obj;
					InputStream  ldIs = pgpLD.getInputStream();

					PGPPublicKey key = pgpPub.getPublicKey(sig.getKeyID());					
					sig.initVerify(key, BOUNCY_CASTLE_PROVIDER);


					// calculate and update signature
					byte[] buff = new byte[1<<16];
					int totBytesRead;
					while((totBytesRead = ldIs.read(buff)) != -1) {
						sig.update(buff,0,totBytesRead);                        
					}

					//verify signature
					PGPSignatureList  vSig = (PGPSignatureList)pgpObjF.nextObject();
					if (!sig.verify(vSig.get(0)))
					{
						throw new PGPException("Signature verification failed...");
					}
				} else {
					throw new PGPException("cannot verify signature, invalid encrypted data...");
				}
			} else {
				throw new PGPException("cannot verify signature, invalid encrypted data...");
			}

		}
		else {
			throw new PGPException("cannot verify signature, invalid encrypted data...");
		}
	}


	/**
	 * @param enc
	 * @return
	 * @throws PGPException
	 * @throws NoSuchProviderException
	 */
	protected static PGPEncodedData getSecretKey(PGPEncryptedDataList enc) throws PGPException, NoSuchProviderException {

		PGPPublicKeyEncryptedData pbe = null;
		
		Iterator it = enc.getEncryptedDataObjects();
		while (it.hasNext()) {
			pbe = (PGPPublicKeyEncryptedData) it.next();
			//sKey = getSecretKey(pbe.getKeyID());
		}

		PGPSecretKey sKey = pbe!=null?pgpSec.getSecretKey(pbe.getKeyID()):null;
		if (sKey == null) {
			throw new IllegalArgumentException("secret key for message not found.");
		}
		
        PGPPrivateKey pKey = sKey.extractPrivateKey(privateKeyPassword.toCharArray(), BOUNCY_CASTLE_PROVIDER);
		
		if (pKey == null) {
			throw new IllegalArgumentException("secret key for message not found.");
		}
	
		return (new PGPEncodedData(pKey,pbe));
	}


	/**
	 * @param keyIn
	 * @throws IOException
	 * @throws PGPException
	 */
	private static void loadSecretKeyRing(InputStream keyIn) throws IOException, PGPException  
	{
		pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
	}

	/**
	 * @param keyIn
	 * @throws IOException
	 * @throws PGPException
	 */
	private static void loadPublicKeyRing(InputStream keyIn) throws IOException, PGPException  
	{
		pgpPub = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
	}


	/**
	 * @throws Exception
	 */
	protected static void init() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		loadParam();
		siginingKey = getSigningKey();

	}

	/**
	 * @throws IOException
	 * @throws PGPException
	 */
	protected static void loadParam() throws IOException, PGPException {
		String publicRing = TradePortalConstants.getPropertyValue("TradePortal", PGP_PUBLIC_KEY_RING, null);
		String secretRing = TradePortalConstants.getPropertyValue("TradePortal", PGP_SECRECT_KEY_RING, null);
		privateKeyPassword = TradePortalConstants.getPropertyValue("TradePortal", PGP_KEY_PASS, null);



		File publicKeyFile = new File(publicRing);
		File privateKeyFile = new File(secretRing);
		
		try(FileInputStream privKey = new FileInputStream(privateKeyFile);
		FileInputStream pubKey = new FileInputStream(publicKeyFile)){
		loadSecretKeyRing(privKey);
		loadPublicKeyRing(pubKey);
		}

	}

	/**
	 * @param s
	 * @param l1
	 * @param l2
	 */
	protected static void time(String s, long l1, long l2) {
		LOG.info (s + ": timed : " + ((l2 -l1)/1000) + " sec" );
	}
}



class PGPEncodedData {


	PGPPrivateKey skey = null;
	PGPPublicKeyEncryptedData pbe = null;

	PGPEncodedData(PGPPrivateKey skey, PGPPublicKeyEncryptedData pbe) {
		this.skey = skey;
		this.pbe = pbe;
	}

	public PGPPrivateKey getPrivateKey() {
		return skey;
	}
	public PGPPublicKeyEncryptedData getPublicKeyEncData() {
		return pbe;
	}



}

