/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.StringFunction;

public class POUploadFileDetails {
private static final Logger LOG = LoggerFactory.getLogger(POUploadFileDetails.class);

    public static final String FILE_EXTENSION = ".txt";

    public static Hashtable<String, String> delimiters = new Hashtable<String, String>();

    static {
        delimiters.put("COMMA", ",");
        delimiters.put("PIPEDELIMITER", "\\|");
        delimiters.put("SEMICOLON", ";");
        delimiters.put("TAB", "\\t");
    }

    /**
     * Parse one po line item.
     *
     * @param poDataDelimiter
     * @param currentPOLineItem
     * @param file_fields_number
     * @return
     * @throws AmsException
     * @throws RemoteException
     */
    public static String[] parsePOLineItem(String poDataDelimiter, String currentPOLineItem) {

        if (StringFunction.isNotBlank(poDataDelimiter)) {
            poDataDelimiter = delimiters.get(poDataDelimiter);

            String[] origPORecord = currentPOLineItem.split(poDataDelimiter);
			// If there is only one field, the format cannot be valid. The user
            // probably has used wrong delimiter.
            if (origPORecord.length == 1) {
                return null;
            }
            String[] PORecord = new String[origPORecord.length];
            int numberOfFields = origPORecord.length;
            if (numberOfFields > PORecord.length) {
                numberOfFields = PORecord.length;
            }
            for (int i = 0; i < numberOfFields; i++) {
                PORecord[i] = origPORecord[i];
            }

            int fromIndex = numberOfFields;
            int toIndex = PORecord.length;
            if (fromIndex <= toIndex) {
                Arrays.fill(PORecord, fromIndex, toIndex, "");
            }

            return PORecord;
        } else {
            return null;
        }
    }

    /**
     * Gets the Field count on InvoiceDefinition
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid PO Data file (true -
     * valid false - invalid)
     */
    public static void getPODefFieldCount(PurchaseOrderDefinition poDef, HashMap details)
            throws AmsException, RemoteException {

        boolean hasMoreFields = true;
        String currentFieldName = null;
        int index = 1;

        while (hasMoreFields && index <= TradePortalConstants.PO_GOODS_NUMBER_OF_FIELDS) {
            currentFieldName = poDef.getAttribute("po_summary_field" + index);
            if ((currentFieldName != null) && (!currentFieldName.equals(""))) {
                index++;
            } else {
                hasMoreFields = false;
            }
        }

        int index1 = 1;
        boolean hasMoreGoodsFields = true;
        while (hasMoreGoodsFields && index1 <= TradePortalConstants.PO_SUMMARY_NUMBER_OF_FIELDS) {
            currentFieldName = poDef.getAttribute("po_data_field" + index1);
            if ((currentFieldName != null) && (!currentFieldName.equals(""))) {
                index1++;
            } else {
                hasMoreGoodsFields = false;
            }
        }
        int index2 = 1;
        boolean hasMoreLineFields = true;
        while (hasMoreLineFields && index2 <= TradePortalConstants.PO_LINE_ITEM_NUMBER_OF_FIELDS) {
            currentFieldName = poDef.getAttribute("po_line_item_field" + index2);
            if ((currentFieldName != null) && (!currentFieldName.equals(""))) {
                index2++;
            } else {
                hasMoreLineFields = false;
            }
        }
        int poDefFieldCount = index + index1 + index2 - 3;

        details.put("poDefFieldCount", poDefFieldCount);

    }

    /**
     * Verifies the PO Line Item. Doing the validation here instead of at the
     * business object user_validate() gives better error message information.
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid PO Data file (true -
     * valid false - invalid)
     */
    public static boolean validatePOLineItem(String[] poRecord, MediatorServices mediatorServices, HashMap details)
            throws AmsException {

        int poDefFieldCount = (Integer) details.get("poDefFieldCount");

        if (poRecord.length != poDefFieldCount) {
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.INVALID_INVOICE_FIELD_COUNT);
            details.put("errText", mediatorServices);
            return false;
        }
        return true;
    }

    public static String getAttributeType(BusinessObject businessObject, String attributeName) {
        try {
            Hashtable attrTable = businessObject.getAttributeHash();
            Attribute attribute = (Attribute) attrTable.get(attributeName);
            // If the attribute is not found, throw a processing exception
            if (attribute == null) {
                throw new AmsException(attributeName);
            }
            return attribute.getAttributeType();
        } catch (RemoteException e) {
            return null;
        } catch (AmsException e) {
            return null;
        }

    }

    //this returns PONum or invoice Id- based on from where its invoked
    public static String getUniqueID(String[] poRecord, MediatorServices mediatorServices, BusinessObject object, int count, String orderField, String fieldName)
            throws AmsException {

        boolean hasMoreDataFields = true;
        String currentFieldName = null;
        int index = 1;

        while (hasMoreDataFields) {

            try {
                currentFieldName = object.getAttribute(orderField + index);
            } catch (Exception e) {
                // do nothing as field is not registered.
                hasMoreDataFields = false;
                continue;
            }
            if (StringFunction.isNotBlank(currentFieldName)) {
                String currentFieldValue = poRecord[index - 1];
                if (currentFieldName.equals(fieldName)) {
                    if (!validateMissingFields(currentFieldName, currentFieldValue, count, mediatorServices)) {
                        return null;
                    }
                    return currentFieldValue;
                }
            }
            index++;
        }
        return null;

    }

    private static boolean validateMissingFields(String filedName, String value, int lineNum, MediatorServices mediatorServices)
            throws AmsException {

        if (StringFunction.isBlank(value)) {
            String[] substitutionValues = {filedName, String.valueOf(lineNum + 1)};
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.INVOICE_FIELD_MISSING,
                    substitutionValues, null);
            return false;
        }
        return true;
    }

}
