package com.ams.tradeportal.common;
import java.io.ByteArrayOutputStream;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.EmailTrigger;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Transaction;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class PaymentBeneficiaryEmailProcessor {
private static final Logger LOG = LoggerFactory.getLogger(PaymentBeneficiaryEmailProcessor.class);

    private static final String PAYMENT_BENEFICIARY_EMAIL_SUBJECT1 = "PaymentBeneficiaryEmail.Subject1";
    private static final String PAYMENT_BENEFICIARY_EMAIL_SUBJECT2 = "PaymentBeneficiaryEmail.Subject2";
    private static final String PAYMENT_BENEFICIARY_EMAIL_LINE1 = "PaymentBeneficiaryEmail.Line1";
    private static final String PAYMENT_BENEFICIARY_EMAIL_LINE2 = "PaymentBeneficiaryEmail.Line2";
    private static final String PAYMENT_BENEFICIARY_EMAIL_LINE3 = "PaymentBeneficiaryEmail.Line3";
    private static final String PAYMENT_BENEFICIARY_EMAIL_LINE4 = "PaymentBeneficiaryEmail.Line4";
    private static final String PAYMENT_BENEFICIARY_EMAIL_LINE5 = "PaymentBeneficiaryEmail.Line5";
    private static final String PAYMENT_BENEFICIARY_EMAIL_FOOTER = "PaymentBeneficiaryEmail.Footer";
    private static final String PAYMENT_BENEFICIARY_DEFAULT_SENDER_NAME = "PaymentBeneficiaryEmail.DefaultSenderName";
    private static final String attchmntName = "InvoiceDetails.pdf";
    private static final String PAYABLE_BENEFICIARY_EMAIL_LINE2 = "PayableBeneficiaryEmail.Line2";
    private static final String PAYABLE_BENEFICIARY_DEFAULT_SENDER_NAME = "PaymentBeneficiaryEmail.PayablesDefaultSenderName";

    private final String ejbServerLocation;
    private final ClientServerDataBridge csdb;
    private final long domesticPaymentOid;
    private DomesticPayment domesticPayment = null;

    private Instrument instrument = null;
    private CorporateOrganization corpOrg = null;
    private BankOrganizationGroup bankOrg = null;
    // SL - 10/29/2014 R9.1 IR T36000030417  [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation 
    private String orderingPartyName = null;

    // SL - 10/29/2014 R9.1 IR T36000030417  [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation 
    public PaymentBeneficiaryEmailProcessor(Transaction transaction, Instrument instrument,
            CorporateOrganization corpOrg, BankOrganizationGroup bankOrg, long domesticPaymentOid,
            String ejbServerLocation, ClientServerDataBridge csdb, String orderingPartyName) {
        // super();
        this.instrument = instrument;
        this.corpOrg = corpOrg;
        this.bankOrg = bankOrg;
        this.ejbServerLocation = ejbServerLocation;
        this.csdb = csdb;
        this.domesticPaymentOid = domesticPaymentOid;
        this.orderingPartyName = orderingPartyName;
    }

	// CR - 597 this method will be called by the PaymentBenEmailAgent which will generate an email message and pdf attachment
    // these will be saved on the email_trigger table
    public void processDomesticPayment() throws AmsException {
        // Build the subject and content of the email

        StringBuilder emailSubject = new StringBuilder();
        StringBuilder emailContent = new StringBuilder();
        StringBuilder emailFooter = new StringBuilder();
        Locale locale = new Locale(csdb.getLocaleName().substring(0, 2), csdb
                .getLocaleName().substring(3, 5));
        PropertyResourceBundle bundle = (PropertyResourceBundle) PropertyResourceBundle
                .getBundle("TextResources", locale);
        try {
            domesticPayment = (DomesticPayment) EJBObjectFactory
                    .createClientEJB(ejbServerLocation, "DomesticPayment",
                            domesticPaymentOid);
            String emailReceiver = domesticPayment.getAttribute("payee_email");
            Calendar cal = Calendar.getInstance();
            String ordrPartyName = corpOrg.getAttribute("name");

            // CJR - 6/27/2011 - IR DEUL062459962 Begin
            String senderEmailAddress = bankOrg.getAttribute("bene_sender_email_address");
            String incldBnkUrlInd = bankOrg.getAttribute("incld_bnk_url_pmnt_ben");
            String bankUrl = bankOrg.getAttribute("bene_bank_url");
            String senderName = bankOrg.getAttribute("bene_sender_name");
            if (StringFunction.isBlank(senderName)) {
                senderName = bundle.getString(PAYMENT_BENEFICIARY_DEFAULT_SENDER_NAME);
            }
			// CJR - 6/27/2011 - IR DEUL062459962 End

            String brandingDirectory = bankOrg.getAttribute("branding_directory");
			// IR P0UL032966481

            //CR 913 start
            
            String tradeSystemName = "";
            
            if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))) {
                senderName = bankOrg.getAttribute("payable_sender_name").trim();
                senderEmailAddress = bankOrg.getAttribute("payable_sender_email_address");
                bankUrl = bankOrg.getAttribute("payable_bank_url");
                incldBnkUrlInd = bankOrg.getAttribute("incld_bnk_url_payable_mgmt");
                tradeSystemName = bankOrg.getAttribute("payable_system_name").trim();
                if (StringFunction.isBlank(senderName)) {
                    senderName = bundle.getString(PAYABLE_BENEFICIARY_DEFAULT_SENDER_NAME);
                }
                if (StringFunction.isBlank(senderEmailAddress)) {
                    senderEmailAddress = senderName;
                }
            }
			//CR 913 end

            // get the static content from textresources.properties file
            emailSubject.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_SUBJECT1));
			//BSL IR POUL032966481 BEGIN
            emailSubject.append(" ");
            emailSubject.append(instrument.getAttribute("complete_instrument_id"));
            emailSubject.append("-");
            emailSubject.append(domesticPayment.getAttribute("sequence_number"));
            //CR913 start
            if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))
                    && StringFunction.isNotBlank(domesticPayment.getAttribute("payable_payment_seq_num"))) {
                emailSubject.append("-");
                emailSubject.append(domesticPayment.getAttribute("payable_payment_seq_num"));
            }
			//CR913 end
            //BSL IR POUL032966481 END
            emailSubject.append(" ");
            emailSubject.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_SUBJECT2));
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_LINE1));
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(TradePortalConstants.NEW_LINE);
            if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))) {
                emailContent.append(bundle
                        .getString(PAYABLE_BENEFICIARY_EMAIL_LINE2));
            } else {
                emailContent.append(bundle
                        .getString(PAYMENT_BENEFICIARY_EMAIL_LINE2));
            }
            emailContent.append(" ");
            emailContent.append(ordrPartyName);
            emailContent.append(" ");
            emailContent.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_LINE3));
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_LINE4));
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_LINE5));
            emailContent.append(TradePortalConstants.NEW_LINE);
            emailContent.append(TradePortalConstants.NEW_LINE);//BSL IR POUL032966481 ADD
            emailFooter.append(bundle
                    .getString(PAYMENT_BENEFICIARY_EMAIL_FOOTER));
            /*PGedupudi CR 1123 Rel 9.5 start*/
			if (TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlInd)) {
				emailFooter.append(StringFunction.asciiToUnicode(bundle.getString("EmailTrigger.LogIn"))).append(" ");
				emailFooter.append(tradeSystemName + " ");
					emailFooter.append(bundle.getString("EmailTrigger.AdditionalDetails")).append(" ");
					
				        if (!StringFunction.isBlank(bankUrl))
			                {
					  emailFooter.append(StringFunction.asciiToUnicode(bundle.getString("EmailTrigger.WebsiteAccess"))).append(" ");
					  emailFooter.append(bankUrl + ".");
			                }
					
			}else{
				emailFooter.append(bundle.getString("EmailTrigger.ForAdditionalDetails")).append(", ")
				.append(bundle.getString("EmailTrigger.PleaseLogIn")).append(" "+tradeSystemName+ " ")
				.append(StringFunction.asciiToUnicode(bundle.getString("EmailTrigger.ByVisitingBankSite")));
			}
			/*PGedupudi CR 1123 Rel 9.5 end*/
            emailContent.append(emailFooter.toString());

            // To attach the pdf
            ByteArrayOutputStream attachment = generatePDF(String.valueOf(domesticPaymentOid), brandingDirectory, locale, orderingPartyName);

			// Create a new email trigger object containing the email content
            // created above
            DocumentHandler emailMessage = new DocumentHandler(
                    "<Email></Email>", false, true);

            emailMessage.setAttribute("/Sent", (cal.getTime()).toString());
            emailMessage.setAttribute("/To", emailReceiver);
            emailMessage
                    .setAttribute("/From/SenderAddress", senderEmailAddress);
            emailMessage.setAttribute("/From/SenderName", senderName);
            emailMessage.setAttribute("/Subject", emailSubject.toString());
            emailMessage.setAttribute("/Content", emailContent.toString());
            if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code"))) {
                emailMessage.setAttribute("/AttachmentName", "Payables Credit Advice.pdf");
            } else {
                emailMessage.setAttribute("/AttachmentName", attchmntName);
            }

            EmailTrigger emailTrigger = (EmailTrigger) domesticPayment
                    .createServerEJB("EmailTrigger");
            emailTrigger.newObject();
            emailTrigger.setAttribute("status",
                    TradePortalConstants.EMAIL_TRIGGER_NEW);
            emailTrigger.setAttribute("email_data", emailMessage.toString());
            //BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
            if (attachment != null) {
                emailTrigger.setAttributeBytes("attachment_data",
                        attachment.toByteArray());
            }
            //BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
            emailTrigger.save();
            domesticPayment.setAttribute("payment_ben_email_sent_flag", "Y");
            domesticPayment.save();
            domesticPayment.remove();
        } catch (AmsException | RemoteException ae) {
            LOG.info(ae.toString());
            ae.printStackTrace();
            throw new AmsException("Error while processing Domestic Payment: " + ae.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new AmsException("Error while processing Domestic Payment: " + e.getMessage());
        }
    }

    // method to generate pdf attachment
    private ByteArrayOutputStream generatePDF(String domesticPaymentOid, String brandingDirectory, Locale locale, String orderingPartyName) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            Map parms = new HashMap();
            parms.put("producer", "EmailAttachment");
            parms.put("transaction", domesticPaymentOid);

            parms.put("locale", locale.toString());
            parms.put("bank_prefix", brandingDirectory);
            parms.put("stylesheet", "EmailAttachment.xsl");

            Map attributes = new HashMap();
            attributes.put("domestic_payment", domesticPayment);
            attributes.put("bank_org", bankOrg);
            attributes.put("corp_org", corpOrg);
            attributes.put("instrument", instrument);

            PDFUtils pdfUtils = new PDFUtils();
            // SL - 10/29/2014 R9.1 IR T36000030417  [START] - Ordering Party name is corrected in Beneficiary Email - Parent child organisation 
            pdfUtils.generatePDFDoc(parms, attributes, outputStream, orderingPartyName);
        } catch (Exception e) {
            /*
             * If an Exception occurs, print out the call stack so we can
             * determine where it came from
             */
            e.printStackTrace();
            throw new AmsException("PDF generation error: " + e.getMessage());
        } finally {
            outputStream.close();
        }
        return outputStream;
    }
}
