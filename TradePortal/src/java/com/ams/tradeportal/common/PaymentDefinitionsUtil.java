package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PaymentDefinitionsUtil {
private static final Logger LOG = LoggerFactory.getLogger(PaymentDefinitionsUtil.class);
	private static HashSet pmtSummOptionalVal = new HashSet();
	private static HashSet pmtSummReqVal = new HashSet();
	private static Map sizeMap = new HashMap();
	static {

		pmtSummOptionalVal.add("customer_reference");
		pmtSummOptionalVal.add("beneficiary_country");
		pmtSummOptionalVal.add("execution_date");
		pmtSummOptionalVal.add("bene_address1");
		pmtSummOptionalVal.add("bene_address2");
		pmtSummOptionalVal.add("bene_address3");
		pmtSummOptionalVal.add("bene_address4");
		pmtSummOptionalVal.add("bene_fax_no");
		pmtSummOptionalVal.add("bene_email_id");
		pmtSummOptionalVal.add("charges");
		pmtSummOptionalVal.add("bene_bank_name");
		pmtSummOptionalVal.add("bene_bnk_brnch_name");
		pmtSummOptionalVal.add("bene_bnk_brnch_addr1");
		pmtSummOptionalVal.add("bene_bnk_brnch_addr2");
		pmtSummOptionalVal.add("bene_bnk_brnch_city");
		pmtSummOptionalVal.add("bene_bnk_brnch_prvnc");
		pmtSummOptionalVal.add("bene_bnk_brnch_cntry");
		pmtSummOptionalVal.add("payable_location");
		pmtSummOptionalVal.add("print_location");
		pmtSummOptionalVal.add("delvry_mth_n_delvrto");
		pmtSummOptionalVal.add("mailing_address1");
		pmtSummOptionalVal.add("mailing_address2");
		pmtSummOptionalVal.add("mailing_address3");
		pmtSummOptionalVal.add("mailing_address4");
		pmtSummOptionalVal.add("details_of_payment");
		pmtSummOptionalVal.add("instruction_number");
		pmtSummOptionalVal.add("f_int_bnk_brnch_cd");
		pmtSummOptionalVal.add("f_int_bnk_name");
		pmtSummOptionalVal.add("f_int_bnk_brnch_nm");
		pmtSummOptionalVal.add("f_int_bnk_brnch_addr1");
		pmtSummOptionalVal.add("f_int_bnk_brnch_addr2");
		pmtSummOptionalVal.add("f_int_bnk_brnch_city");
		pmtSummOptionalVal.add("f_int_bnk_brnch_prvnc");
		pmtSummOptionalVal.add("f_int_bnk_brnch_cntry");
		pmtSummOptionalVal.add("central_bank_rep1");
		pmtSummOptionalVal.add("central_bank_rep2");
		pmtSummOptionalVal.add("central_bank_rep3");
		pmtSummOptionalVal.add("confidential_ind");
		pmtSummOptionalVal.add("indv_acct_entry_ind");
		// pmtSummOptionalVal.add("header_identifier");
		pmtSummOptionalVal.add("file_reference");
		pmtSummOptionalVal.add("beneficiary_code");
		pmtSummOptionalVal.add("reporting_code1");
		pmtSummOptionalVal.add("reporting_code2");
		pmtSummOptionalVal.add("inv_details_header");
		pmtSummOptionalVal.add("inv_details_lineitm");
		pmtSummOptionalVal.add("bene_account_number");
		pmtSummOptionalVal.add("users_def1");
		pmtSummOptionalVal.add("users_def2");
		pmtSummOptionalVal.add("users_def3");
		pmtSummOptionalVal.add("users_def4");
		pmtSummOptionalVal.add("users_def5");
		pmtSummOptionalVal.add("users_def6");
		pmtSummOptionalVal.add("users_def7");
		pmtSummOptionalVal.add("users_def8");
		pmtSummOptionalVal.add("users_def9");
		pmtSummOptionalVal.add("users_def10");
		pmtSummOptionalVal.add("fx_contract_number");
		pmtSummOptionalVal.add("fx_contract_rate");

		pmtSummReqVal.add("execution_date");

		sizeMap.put("payment_method_size", 4);
		sizeMap.put("debit_account_number_size", 30);
		sizeMap.put("beneficiary_name_size", 120);
		sizeMap.put("bene_account_number_size", 34);
		sizeMap.put("bene_bank_branch_code_size", 35);
		sizeMap.put("payment_currency_size", 3);
		sizeMap.put("payment_amount_size", 19);
		sizeMap.put("customer_reference_size", 20);
		sizeMap.put("beneficiary_country_size", 2);
		sizeMap.put("execution_date_size", 10);
		sizeMap.put("bene_address1_size", 35);
		sizeMap.put("bene_address2_size", 35);
		sizeMap.put("bene_address3_size", 35);
		sizeMap.put("bene_address4_size", 35);
		sizeMap.put("bene_fax_no_size", 15);
		sizeMap.put("bene_email_id_size", 255);
		sizeMap.put("charges_size", 3);
		sizeMap.put("bene_bank_name_size", 35);
		sizeMap.put("bene_bnk_brnch_name_size", 35);
		sizeMap.put("bene_bnk_brnch_addr1_size", 35);
		sizeMap.put("bene_bnk_brnch_addr2_size", 35);
		sizeMap.put("bene_bnk_brnch_city_size", 31);
		sizeMap.put("bene_bnk_brnch_prvnc_size", 8);
		sizeMap.put("bene_bnk_brnch_cntry_size", 2);
		sizeMap.put("payable_location_size", 20);
		sizeMap.put("print_location_size", 20);
		sizeMap.put("delvry_mth_n_delvrto_size", 2);
		sizeMap.put("mailing_address1_size", 35);
		sizeMap.put("mailing_address2_size", 35);
		sizeMap.put("mailing_address3_size", 35);
		sizeMap.put("mailing_address4_size", 35);
		sizeMap.put("details_of_payment_size", 140);
		sizeMap.put("instruction_number_size", 10);
		sizeMap.put("f_int_bnk_brnch_cd_size", 35);
		sizeMap.put("f_int_bnk_name_size", 35);
		sizeMap.put("f_int_bnk_brnch_nm_size", 35);
		sizeMap.put("f_int_bnk_brnch_addr1_size", 35);
		sizeMap.put("f_int_bnk_brnch_addr2_size", 35);
		sizeMap.put("f_int_bnk_brnch_city_size", 31);
		sizeMap.put("f_int_bnk_brnch_prvnc_size", 8);
		sizeMap.put("f_int_bnk_brnch_cntry_size", 2);
		sizeMap.put("central_bank_rep1_size", 35);
		sizeMap.put("central_bank_rep2_size", 35);
		sizeMap.put("central_bank_rep3_size", 35);
		sizeMap.put("confidential_ind_size", 1);
		sizeMap.put("indv_acct_entry_ind_size", 1);
		sizeMap.put("header_identifier_size", 3);
		sizeMap.put("file_reference_size", 35);
		sizeMap.put("detail_identifier_size", 3);
		sizeMap.put("beneficiary_code_size", 15);
		sizeMap.put("reporting_code1_size", 3);
		sizeMap.put("reporting_code2_size", 3);
		sizeMap.put("inv_details_header_size", 3);
		sizeMap.put("inv_details_lineitm_size", 80); // DK IR T36000026394
														// Rel8.4 03/19/2014
		sizeMap.put("users_def1_size", 35);
		sizeMap.put("users_def2_size", 35);
		sizeMap.put("users_def3_size", 35);
		sizeMap.put("users_def4_size", 35);
		sizeMap.put("users_def5_size", 35);
		sizeMap.put("users_def6_size", 35);
		sizeMap.put("users_def7_size", 35);
		sizeMap.put("users_def8_size", 35);
		sizeMap.put("users_def9_size", 35);
		sizeMap.put("users_def10_size", 35);
		sizeMap.put("fx_contract_number_size", 14);
		sizeMap.put("fx_contract_rate_size", 14);

	}

	public static Set getPmtSummOtions() {
		return pmtSummOptionalVal;
	}

	public static Set getPmtSummReqVal() {
		return pmtSummReqVal;
	}
	
	public static Map getSizeMap(){
		return sizeMap;
	}
}
