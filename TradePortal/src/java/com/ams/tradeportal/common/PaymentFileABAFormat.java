/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PaymentDefinitions;

import static com.ams.tradeportal.common.PaymentFileFormat.logError;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.ejb.RemoveException;

import java.math.BigDecimal;

/**
 *
 */
public class PaymentFileABAFormat extends PaymentFileFixedFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileABAFormat.class);

    protected static final Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9 ]+");
    protected static List validTransCode = new ArrayList();

    static {
        validTransCode.add("13");
        validTransCode.add("50");
        validTransCode.add("53");
        validTransCode.add("54");
        validTransCode.add("56");
        validTransCode.add("57");
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_HEADER, "header");
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_PAYMENT, "details Section");
        lineTypes.put(PaymentFileFieldFormat.LINE_TYPE_TRAILER, "batch Control records");
    }

    public PaymentFileABAFormat(PaymentDefinitions paymentDefinition) throws RemoteException, AmsException {
        super(paymentDefinition);
        PaymentFileFieldFormat field = new PaymentFileFieldFormat(PaymentFileFieldFormat.COLUMN_AMOUNT_CURRENCY_CODE, PaymentFileFieldFormat.AMOUNT_CURRENCY_CODE, PaymentFileFieldFormat.DESC_AMOUNT_CURRENCY_CODE, PaymentFileFieldFormat.LINE_TYPE_HEADER, headerFieldsArray.size(), false, false, 3, true);
        headerFieldsArray.add(field);
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat(PaymentFileFieldFormat.COLUMN_BANK_CHARGES_TYPE, PaymentFileFieldFormat.BANK_CHARGES_TYPE, PaymentFileFieldFormat.DESC_BANK_CHARGES_TYPE, PaymentFileFieldFormat.LINE_TYPE_HEADER, headerFieldsArray.size(), false, false, 4, true);
        headerFieldsArray.add(field);
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat(PaymentFileFieldFormat.COLUMN_PAYMENT_METHOD_TYPE, PaymentFileFieldFormat.PAYMENT_METHOD_TYPE, PaymentFileFieldFormat.DESC_PAYMENT_METHOD_TYPE, PaymentFileFieldFormat.LINE_TYPE_HEADER, headerFieldsArray.size(), false, false, 4, true);
        headerFieldsArray.add(field);
        fieldNameToFieldMap.put(field.name, field);
        PaymentFileFieldFormat[] dummy = new PaymentFileFieldFormat[0];
        headerFields = headerFieldsArray.toArray(dummy);

        // Customize a few field descriptions
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYEE_ACCOUNT_NUMBER);
        field.description = "Account number to be credited";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.EXECUTION_DATE);
        field.description = "Date to be Processed";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_AMOUNT);
        field.description = "Amount to be credited";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYEE_NAME);
        field.description = "Title of Account to be credited";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.CUSTOMER_REFERENCE);
        field.description = "Lodgement Reference";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO);
        field.description = "Trace Account Number";
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.USER_DEFINED_FIELD_1);
        if (field != null) {
            field.description = "Transaction Code";
        }
        field = fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYEE_BANK_CODE);
        field.description = "BSB of the Account to be credited";

        HEADER_SECTION_IDENTIFIER = "0";
        PAYMENT_SECTION_IDENTIFIER = "1";
        INVOICE_SECTION_IDENTIFIER = null;
        TRAILER_SECTION_IDENTIFIER = "7";
        HEADER_SECTION_DESCRIPTION = "Desciptive Record";
        PAYMENT_SECTION_DESCRIPTION = "Detail Record";
        TRAILER_SECTION_DESCRIPTION = "Batch Control Record";

        // Hard code trailer fields, i.e. batch control record 
        trailerFields = new PaymentFileFieldFormat[9];
        field = new PaymentFileFieldFormat("br_record_type", "br_record_type", "Record type", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 0, true, true, 1, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_reserved1", "br_reserved1", "Reserved", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 1, true, true, 7, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_reserved2", "br_reserved2", "Reserved", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 2, true, "Y".equals(paymentDefinition.getAttribute("br_reserved2_data_req")), 12, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_btch_net_tot_amt", "br_btch_net_tot_amt", "Batch Net Total Amount", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 3, true, true, 10, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_btch_cred_tot_amt", "br_btch_cred_tot_amt", "Batch Credit Total Amount", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 4, true, "Y".equals(paymentDefinition.getAttribute("br_btch_cred_tot_amt_data_req")), 10, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_btch_deb_tot_amt", "br_btch_deb_tot_amt", "Batch Debit Total Amount", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 5, true, "Y".equals(paymentDefinition.getAttribute("br_btch_deb_tot_amt_data_req")), 10, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_reserved3", "br_reserved3", "Reserved", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 6, true, "Y".equals(paymentDefinition.getAttribute("br_reserved3_data_req")), 24, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_total_item_count", "br_total_item_count", "Batch Total Item Count", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 7, true, true, 6, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);
        field = new PaymentFileFieldFormat("br_reserved4", "br_reserved4", "Reserved", PaymentFileFieldFormat.LINE_TYPE_TRAILER, 8, true, "Y".equals(paymentDefinition.getAttribute("br_reserved4_data_req")), 40, false);
        trailerFields[field.order] = field;
        fieldNameToFieldMap.put(field.name, field);

    }

    @Override
    public String[] parseHeaderLine(String headerLine, PaymentFileProcessor proc) throws AmsException, RemoteException {
        String[] headerRecords = super.parseHeaderLine(headerLine, proc);

        // add charges, and payment method.
        headerRecords[headerRecords.length - 2] = ""; // will be translated to SHA in processPaymentLine
        headerRecords[headerRecords.length - 1] = paymentMethod;

        return headerRecords;
    }

    @Override
    public String[] parsePaymentLine(String[] headerRecord, String[] paymentLines, PaymentFileProcessor proc) throws AmsException, RemoteException {
        String[] paymentRecord = super.parsePaymentLine(headerRecord, paymentLines, proc);

        // Currency = debit account currency
        if (StringFunction.isBlank(headerRecord[headerRecord.length - 3])) {
            String accountSqlStr = "select currency from account where account_number = ? and p_owner_oid = ?";

            Object sqlParams[] = new Object[2];
            sqlParams[0] = getValue(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO, null, paymentRecord, null);
            sqlParams[1] = proc.ownerOrgOid;
            DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, sqlParams);

            if (acctResultSet != null && !StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/CURRENCY"))) {
                headerRecord[headerRecord.length - 3] = acctResultSet.getAttribute("/ResultSetRecord(0)/CURRENCY");
            }
        }

        return paymentRecord;
    }

    @Override
    protected boolean validateHeaderLine(String[] headerRecords, PaymentFileProcessor proc) throws AmsException, RemoteException, RemoveException {
        boolean superReturn = super.validateHeaderLine(headerRecords, proc);
        boolean errorsFound = false;

        String fdSequenceNumber = getValue(PaymentFileFieldFormat.FD_SEQUENCE_NUMBER, headerRecords, null, null);
        if (!"01".equals(fdSequenceNumber)) {
            logError(proc.lineNumberForLogging, proc.beneName, "'Sequence Number' field is not valid - it should contain the value 01.", ERROR_SUB, proc);
            errorsFound = true;
        }

        return superReturn && !errorsFound;
    }

    @Override
    protected boolean processPaymentLine(String[] headerRecord, String[] paymentRecord, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {

        // Reformat payment amount.  1021 -> 10.21
        String paymentAmount = getValue(PaymentFileFieldFormat.PAYMENT_AMOUNT, headerRecord, paymentRecord, null);
        PaymentFileFieldFormat paymentAmountField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_AMOUNT);
        if (StringFunction.isNotBlank(paymentAmount)) {
            paymentAmount = paymentAmount.substring(0, paymentAmount.length() - 2) + "." + paymentAmount.substring(paymentAmount.length() - 2);
            paymentRecord[paymentAmountField.order] = paymentAmount;
        }

        return super.processPaymentLine(headerRecord, paymentRecord, proc);

    }

    @Override
    public String[] parseTrailerLine(String trailerLine) {

        String[] trailerRecord = new String[headerFields.length];

        int lineIndex = 0;
        for (int i = 0; i < trailerFields.length; i++) {
            int fieldLength = trailerFields[i].size;
            int endIndex = lineIndex + fieldLength;
            if (lineIndex > trailerLine.length()) {
                trailerRecord[i] = null;
            } else if (endIndex <= trailerLine.length()) {
                if (trailerFields[i].columnName.contains("reserved")) {
                    trailerRecord[i] = trailerLine.substring(lineIndex, endIndex);
                } else {
                    trailerRecord[i] = trailerLine.substring(lineIndex, endIndex).trim();
                }
            } else if (endIndex > trailerLine.length()) {
                if (trailerFields[i].columnName.contains("reserved")) {
                    trailerRecord[i] = trailerLine.substring(lineIndex);
                } else {
                    trailerRecord[i] = trailerLine.substring(lineIndex).trim();
                }
            }
            lineIndex = endIndex;
        }

        return trailerRecord;
    }

    @Override
    protected boolean validateTrailerLine(String[] trailerRecords, PaymentFileProcessor proc) throws AmsException, RemoteException, RemoveException {
        boolean superReturn = super.validateTrailerLine(trailerRecords, proc);
        boolean errorsFound = false;

        String value = getValue("br_reserved1", null, null, trailerRecords);
        if (!"999-999".equals(value)) {
            logError(proc.lineNumberForLogging, proc.beneName, "The 'Reserved field' in the batch control record must have a value of 999-999.", ERROR_SUB, proc);
            errorsFound = true;
        }

        value = getValue("br_reserved2", null, null, trailerRecords);
        if (!StringFunction.isBlank(value)) {
            logError(proc.lineNumberForLogging, proc.beneName, "The 'Reserved field' in the batch control record must have a value of blanks.", ERROR_SUB, proc);
            errorsFound = true;
        }

        value = getValue("br_btch_net_tot_amt", null, null, trailerRecords);
        if (!StringFunction.isBlank(value)) {
            String newValue = value.substring(0, value.length() - 2) + "." + value.substring(value.length() - 2);
            try {
                BigDecimal amount = new BigDecimal(newValue);
                if (amount.compareTo(proc.totAmt) != 0) {
                    logError(proc.lineNumberForLogging, proc.beneName, "Batch Net Total Amount field (" + value + ") does not match the total of all beneficiary amounts (" + proc.totAmt + ").", ERROR_SUB, proc);
                    errorsFound = true;
                }
            } catch (NumberFormatException e) {
                logError(proc.lineNumberForLogging, proc.beneName, "Invalid value '" + value + "' in Batch Net Total Amount field.", ERROR_SUB, proc);
                errorsFound = true;
            }
        }

        value = getValue("br_btch_cred_tot_amt", null, null, trailerRecords);
        if (!StringFunction.isBlank(value)) {
            String newValue = value.substring(0, value.length() - 2) + "." + value.substring(value.length() - 2);
            try {
                BigDecimal amount = new BigDecimal(newValue);
                if (amount.compareTo(proc.totAmt) != 0) {
                    logError(proc.lineNumberForLogging, proc.beneName, "Batch Credit Total Amount field (" + value + ") does not match the total of all beneficiary amounts (" + proc.totAmt + ").", ERROR_SUB, proc);
                    errorsFound = true;
                }
            } catch (NumberFormatException e) {
                logError(proc.lineNumberForLogging, proc.beneName, "Invalid value '" + value + "' in Batch Credit Total Amount field.", ERROR_SUB, proc);
                errorsFound = true;
            }
        }
        value = getValue("br_total_item_count", null, null, trailerRecords);
        if (!StringFunction.isBlank(value)) {
            try {
                int count = Integer.parseInt(value);
                if (count != proc.successCount) {
                    logError(proc.lineNumberForLogging, proc.beneName, "Batch Total Item Count field (" + value + ") does not match the total number of all beneficiaries (" + proc.successCount + ").", ERROR_SUB, proc);
                    errorsFound = true;
                }
            } catch (NumberFormatException e) {
                logError(proc.lineNumberForLogging, proc.beneName, "Invalid value '" + value + "' in Batch Total Item Count field.", ERROR_SUB, proc);
                errorsFound = true;
            }
        }
        return superReturn && !errorsFound;
    }

    @Override
    protected boolean validatePaymentLine(String[] paymentRecords, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        boolean superReturn = super.validatePaymentLine(paymentRecords, proc);
        boolean errorsFound = false;

        String transCode = getValue(PaymentFileFieldFormat.USER_DEFINED_FIELD_1, null, paymentRecords, null);
        if (StringFunction.isNotBlank(transCode) && !validTransCode.contains(transCode)) {
            logError(proc.lineNumberForLogging, proc.beneName, "Invalid value '" + transCode + "' in the Transaction Code field.", ERROR_SUB, proc);
            errorsFound = true;
        }

        return superReturn && !errorsFound;
    }

    @Override
    public void formatAmount(PaymentFileProcessor proc, String[] paymentRecords) {
        String value = paymentRecords[fieldNameToFieldMap.get(PaymentFileFieldFormat.PAYMENT_AMOUNT).order];
        if (!StringFunction.isBlank(value)) {
            String newValue = value.substring(0, value.length() - 2) + "." + value.substring(value.length() - 2);
            try {
                BigDecimal amount = new BigDecimal(newValue);
                proc.totAmt = proc.totAmt.add(amount);
            } catch (NumberFormatException e) {

            }
        }

    }

    @Override
    protected List getMandatoryFields(String paymentMethod) {
        return new ArrayList();
    }

    @Override
    protected boolean validateLine(String[] dataRecords, PaymentFileFieldFormat[] definitionFields, String lineType, PaymentFileProcessor proc) throws RemoveException, AmsException, RemoteException {
        if (definitionFields == null) {
            return true;
        }
        boolean errorFound = false;
        for (int i = 0; i < definitionFields.length; i++) {
            List reqFields = getMandatoryFields(this.paymentMethod);
            if ((definitionFields[i].isFieldRequired && (dataRecords.length < i + 1 || dataRecords[i] == null)) || (reqFields.contains(definitionFields[i].columnName) && StringFunction.isBlank(dataRecords[i]))) {
                logError(proc.lineNumberForLogging, proc.beneName, "The '" + definitionFields[i].description + " field ' is missing.", ERROR_SUB, proc);
                errorFound = true;
            } else if (definitionFields[i].isDataRequired && (dataRecords.length < i + 1 || StringFunction.isBlank(dataRecords[i]))) {
                if (definitionFields[i].columnName.contains("reserved")) {
                    if ((dataRecords[i] != null && dataRecords[i].length() != definitionFields[i].size)) {
                        logError(proc.lineNumberForLogging, proc.beneName, "The '" + definitionFields[i].description + " field ' in the " + lineTypes.get(lineType) + " is missing.", ERROR_SUB, proc);
                    }
                } else {
                    logError(proc.lineNumberForLogging, proc.beneName, "'" + definitionFields[i].description + "' is missing.", ERROR_SUB, proc);
                }
                errorFound = true;
            } else if (dataRecords.length >= i + 1 && dataRecords[i] != null && dataRecords[i].length() > (PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS.equals(definitionFields[i].columnName) ? (definitionFields[i].size * 1000) : definitionFields[i].size)) {
                long fieldSize = dataRecords[i].length();
                if (dataRecords[i].length() > 1000) {
                    dataRecords[i] = dataRecords[i].substring(0, 1000) + "...";
                }
                logError(proc.lineNumberForLogging, proc.beneName, "'" + definitionFields[i].description + "' - \"" + dataRecords[i] + "\" has " + fieldSize + " characters, exceeding the size limit of "
                        + (PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS.equals(definitionFields[i].columnName) ? (definitionFields[i].size * 1000) : definitionFields[i].size) + " characters.", ERROR_SUB, proc);
                errorFound = true;
            }

        }

        return !errorFound;
    }

}
