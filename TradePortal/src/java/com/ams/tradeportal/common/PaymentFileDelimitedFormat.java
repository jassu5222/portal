/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.StringFunction;
import com.ams.tradeportal.busobj.PaymentDefinitions;

import java.util.ArrayList;

import javax.ejb.RemoveException;

public class PaymentFileDelimitedFormat extends PaymentFileFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileDelimitedFormat.class);

    public PaymentFileDelimitedFormat(PaymentDefinitions paymentDefinition) throws RemoteException, AmsException {
        super(paymentDefinition);
        formatType = TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT;
        delimiterChar = paymentDefinition.getAttribute("delimiter_char");
        if ("COMMA".equals(delimiterChar)) {
            delimiterChar = ",";
        } else if ("SEMICOLON".equals(delimiterChar)) {
            delimiterChar = ";";
        } else if ("PIPEDELIMITER".equals(delimiterChar)) {
            delimiterChar = "\\|";
        } else if ("TAB".equals(delimiterChar)) {
            delimiterChar = "\t";
        }
        ArrayList<PaymentFileFieldFormat> headerFields = new ArrayList<>(TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS);
        ArrayList<PaymentFileFieldFormat> paymentFields = new ArrayList<>(TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS);
        ArrayList<PaymentFileFieldFormat> invoiceFields = new ArrayList<>(TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS);
        for (int i = 0; i < 66; i++) { //CR-921 increased to 66 as two fields - FX Contract details added
            String columnName = paymentDefinition.getAttribute("pmt_mthd_summary_order" + (i + 1));
            if (columnName != null && columnName.length() > 0) {
                PaymentFileFieldFormat field = new PaymentFileFieldFormat(paymentDefinition, columnName, i, PaymentFileFieldFormat.LINE_TYPE_PAYMENT);

                paymentFields.add(field);
                fieldNameToFieldMap.put(field.name, field);
            }
        }

        PaymentFileFieldFormat[] dummy = new PaymentFileFieldFormat[0];
        this.headerFields = headerFields.toArray(dummy);
        this.paymentFields = paymentFields.toArray(dummy);
        this.invoiceFields = invoiceFields.toArray(dummy);

    }

    @Override
    protected String readHeaderLine(PaymentFileProcessor proc, boolean[] errorsFound) throws java.io.IOException, AmsException, RemoveException {
        return null;
    }

    @Override
    protected String[] readPaymentLine(String[] readAheadLine, boolean[] errorsFound, PaymentFileProcessor proc) throws java.io.IOException, AmsException, RemoveException {
        errorsFound[0] = false;
        String[] payLine = new String[1];
        payLine[0] = readAheadLine[0];
        readAheadLine[0] = readLine(proc.reader, proc.writer);
        proc.lineNumber++;
        return payLine;
    }

    @Override
    public String[] parseHeaderLine(String headerLine, PaymentFileProcessor proc)
            throws AmsException, RemoteException {
        return new String[0];
    }

    @Override
    public boolean validateHeaderLine(String[] headerRecords, PaymentFileProcessor proc) throws AmsException, RemoteException, RemoveException {
        return true;
    }

    /**
     * Parse one payment line item.
     *
     * @param currentPaymentLineItem
     * @param resourceManager
     * @param mediatorServices
     * @return
     * @throws AmsException
     */
    @Override
    public String[] parsePaymentLine(String[] headerRecord, String[] detailLines, PaymentFileProcessor proc)
            throws AmsException, RemoteException {

        String currentPaymentLineItem = detailLines[0];

        return currentPaymentLineItem.split(this.delimiterChar, 100);
    }

    @Override
    public String[] parseTrailerLine(String trailerLine) {
        return null;
    }

    @Override
    protected boolean processPaymentLine(String[] headerRecord, String[] paymentRecord, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {

        // Reformat payment amount.  1021 -> 10.21
        String charges = getValue(PaymentFileFieldFormat.BANK_CHARGES_TYPE, headerRecord, paymentRecord, null);
        PaymentFileFieldFormat chargesField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.BANK_CHARGES_TYPE);
        if (StringFunction.isNotBlank(charges)) {
            if (charges.equals(TradePortalConstants.CHARGE_UPLOAD_OURS)) {
                charges = TradePortalConstants.CHARGE_UPLOAD_FW_OURS;
            } else if (charges.equals(TradePortalConstants.CHARGE_UPLOAD_BEN)) {
                charges = TradePortalConstants.CHARGE_UPLOAD_FW_BEN;
            } else if (charges.equals(TradePortalConstants.CHARGE_UPLOAD_OTHER)) {
                charges = TradePortalConstants.CHARGE_UPLOAD_FW_SHARE;
            }
            paymentRecord[chargesField.order] = charges;
        }

        return super.processPaymentLine(headerRecord, paymentRecord, proc);

    }

}
