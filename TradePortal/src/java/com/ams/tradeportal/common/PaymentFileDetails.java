/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

public class PaymentFileDetails extends AbstractPaymentFileDetail // manohar - CR 597 created new class AbstractPaymentFileDetail with common functionality
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileDetails.class);

    public static final String FILE_EXTENSION = ".txt";

    public static final String paymentDataDelimiter = "\\|";

    // Payment file layout
    public static final int PAYMENT_METHOD = 0;
    public static final int DEBIT_ACCOUNT_NO = 1;
    public static final int BENEFICIARY_NAME = 2;
    public static final int BENEFICIARY_ACCOUNT_NO = 3;
    public static final int BENEFICIARY_BANK_BRANCH_CODE = 4;
    public static final int PAYMENT_CURRENCY = 5;
    public static final int PAYMENT_AMOUNT = 6;
    public static final int CUSTOMER_REFERENCE = 7;
    public static final int COUNTRY = 8;
    public static final int EXECUTION_DATE = 9;
    public static final int BENEFICIARY_ADDRESS1 = 10;
    public static final int BENEFICIARY_ADDRESS2 = 11;
    public static final int BENEFICIARY_ADDRESS3 = 12;
    public static final int BENEFICIARY_ADDRESS4 = 13;
    public static final int BENEFICIARY_FAX_NO = 14;
    public static final int BENEFICIARY_EMAIL_ID = 15;
    public static final int CHARGES = 16;
    public static final int BENEFICIARY_BANK_NAME = 17;
    public static final int BENEFICIARY_BANK_BRANCH_NAME = 18;
    public static final int BENEFICIARY_BANK_BRANCH_ADDRESS1 = 19;
    public static final int BENEFICIARY_BANK_BRANCH_ADDRESS2 = 20;
    public static final int BENEFICIARY_BANK_BRANCH_CITY = 21;
    public static final int BENEFICIARY_BANK_BRANCH_PROVINCE = 22;
    public static final int BENEFICIARY_BANK_BRANCH_COUNTRY = 23;
    public static final int PAYABLE_LOCATION = 24;
    public static final int PRINT_LOCATION = 25;
    public static final int DELIVERY_METHOD_DELIVER_TO = 26;
    public static final int MAILING_ADDRESS1 = 27;
    public static final int MAILING_ADDRESS2 = 28;
    public static final int MAILING_ADDRESS3 = 29;
    public static final int MAILING_ADDRESS4 = 30;
    public static final int PAYMENT_DETAILS = 31;
    public static final int INSTRUCTION_NUMBER = 32;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_CODE = 33;
    public static final int FIRST_INTERMEDIARY_BANK_NAME = 34;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_NAME = 35;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1 = 36;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2 = 37;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_CITY = 38;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE = 39;
    public static final int FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY = 40;
    public static final int CENTRAL_BANK_REPORTING1 = 41;
    public static final int CENTRAL_BANK_REPORTING2 = 42;
    public static final int CENTRAL_BANK_REPORTING3 = 43;
    public static final int CONFIDENTIAL_INDICATOR = 44; //CR-586
    //CR-921 - start
    public static final int FX_CONTRACT_NUMBER = 45;
    public static final int FX_RATE = 46;
	//end
    //CR-1001 Rel9.4 - start
    public static final int REPORTING_CODE_1 = 47;
    public static final int REPORTING_CODE_2 = 48;
	//end
    
    // max # of fields per line.
    public static final int MAX_PAYMENT_FILE_FIELDS_NUMBER = REPORTING_CODE_2 + 1;
    // Size of each field.  Only applicable to text fields
    public static int[] fieldSize = new int[MAX_PAYMENT_FILE_FIELDS_NUMBER];
    // The TextResrouce key of the name of the fields, used for error message.
    public static String[] fieldNameKey = new String[MAX_PAYMENT_FILE_FIELDS_NUMBER];

    static {
        // Static initialization of fieldSize[]
        fieldSize[PAYMENT_METHOD] = 0;
        fieldSize[DEBIT_ACCOUNT_NO] = 30;
        fieldSize[BENEFICIARY_NAME] = 140;
        fieldSize[BENEFICIARY_ACCOUNT_NO] = 34;
        fieldSize[BENEFICIARY_BANK_BRANCH_CODE] = 35;
        fieldSize[PAYMENT_CURRENCY] = 0;
        fieldSize[PAYMENT_AMOUNT] = 0;
        fieldSize[CUSTOMER_REFERENCE] = 20;
        fieldSize[COUNTRY] = 0;
        fieldSize[EXECUTION_DATE] = 0;
        fieldSize[BENEFICIARY_ADDRESS1] = 35;
        fieldSize[BENEFICIARY_ADDRESS2] = 35;
        fieldSize[BENEFICIARY_ADDRESS3] = 35;
        fieldSize[BENEFICIARY_ADDRESS4] = 35;
        fieldSize[BENEFICIARY_FAX_NO] = 15;
        fieldSize[BENEFICIARY_EMAIL_ID] = 255;
        fieldSize[CHARGES] = 0;
        fieldSize[BENEFICIARY_BANK_NAME] = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_NAME] = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS1] = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS2] = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_CITY] = 0;
        fieldSize[BENEFICIARY_BANK_BRANCH_PROVINCE] = 0;
        fieldSize[BENEFICIARY_BANK_BRANCH_COUNTRY] = 0;
        fieldSize[PAYABLE_LOCATION] = 20;
        fieldSize[PRINT_LOCATION] = 20;
        fieldSize[DELIVERY_METHOD_DELIVER_TO] = 2;
        fieldSize[MAILING_ADDRESS1] = 35;
        fieldSize[MAILING_ADDRESS2] = 35;
        fieldSize[MAILING_ADDRESS3] = 35;
        fieldSize[MAILING_ADDRESS4] = 35;
        fieldSize[PAYMENT_DETAILS] = 140;
        fieldSize[INSTRUCTION_NUMBER] = 10;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_CODE] = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_NAME] = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_NAME] = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1] = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2] = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_CITY] = 0;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE] = 0;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY] = 0;
        fieldSize[CENTRAL_BANK_REPORTING1] = 35;
        fieldSize[CENTRAL_BANK_REPORTING2] = 35;
        fieldSize[CENTRAL_BANK_REPORTING3] = 35;
        fieldSize[CONFIDENTIAL_INDICATOR] = 1;	//CR-586
        //CR-921 - start
        fieldSize[FX_CONTRACT_NUMBER] = 14;
        fieldSize[FX_RATE] = 14;
        //
        //CR-1001 Rel9.4 - start
        fieldSize[REPORTING_CODE_1] = 3;
        fieldSize[REPORTING_CODE_2] = 3;
        //
        fieldNameKey[FX_CONTRACT_NUMBER] = "DomesticPaymentRequest.FXContractNumber";
        fieldNameKey[FX_RATE] = "DomesticPaymentRequest.FXRate";
        //end
        fieldNameKey[PAYMENT_METHOD] = "";
        fieldNameKey[DEBIT_ACCOUNT_NO] = "DomesticPaymentRequest.DebitFrom";
        fieldNameKey[BENEFICIARY_NAME] = "DomesticPaymentRequest.PayeeName";
        fieldNameKey[BENEFICIARY_ACCOUNT_NO] = "DomesticPaymentRequest.PayeeAccountNumber";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CODE] = "DomesticPaymentRequest.PayeeBankCode";
        fieldNameKey[PAYMENT_CURRENCY] = "DomesticPaymentRequest.PaymentCurrency";
        fieldNameKey[PAYMENT_AMOUNT] = "";
        fieldNameKey[CUSTOMER_REFERENCE] = "DomesticPaymentRequest.CustomerReference";
        fieldNameKey[COUNTRY] = "DomesticPaymentRequest.Country";
        fieldNameKey[EXECUTION_DATE] = "DomesticPaymentRequest.ExecutionDate";
        fieldNameKey[BENEFICIARY_ADDRESS1] = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS2] = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS3] = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS4] = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_FAX_NO] = "DomesticPaymentRequest.BeneFaxNum";
        fieldNameKey[BENEFICIARY_EMAIL_ID] = "DomesticPaymentRequest.BeneEmail";
        fieldNameKey[CHARGES] = "";
        fieldNameKey[BENEFICIARY_BANK_NAME] = "DomesticPaymentRequest.PayeeBankName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_NAME] = "DomesticPaymentRequest.PayeeBankBranchName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS1] = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS2] = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CITY] = "";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_PROVINCE] = "";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_COUNTRY] = "";
        fieldNameKey[PAYABLE_LOCATION] = "DomesticPaymentRequest.PayableLocation";
        fieldNameKey[PRINT_LOCATION] = "DomesticPaymentRequest.PrintLocation";
        fieldNameKey[DELIVERY_METHOD_DELIVER_TO] = "DomesticPaymentRequest.DeliveryMethodTo";
        fieldNameKey[MAILING_ADDRESS1] = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS2] = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS3] = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS4] = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[PAYMENT_DETAILS] = "DomesticPaymentRequest.PaymentDetails";
        fieldNameKey[INSTRUCTION_NUMBER] = "DomesticPaymentRequest.BeneInstructionsNum";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_CODE] = "DomesticPaymentRequest.FirstIntBankBranchCode";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_NAME] = "DomesticPaymentRequest.FirstIntBankTheName";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_NAME] = "DomesticPaymentRequest.FirstIntBankBranchName";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1] = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2] = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_CITY] = "";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE] = "";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY] = "";
        fieldNameKey[CENTRAL_BANK_REPORTING1] = "DomesticPaymentRequest.CentraBankReporting";
        fieldNameKey[CENTRAL_BANK_REPORTING2] = "DomesticPaymentRequest.CentraBankReporting";
        fieldNameKey[CENTRAL_BANK_REPORTING3] = "DomesticPaymentRequest.CentraBankReporting";
        fieldNameKey[CONFIDENTIAL_INDICATOR] = "DomesticPaymentRequest.ConfidentialIndicator"; //CR-586
        fieldNameKey[REPORTING_CODE_1] = "DomesticPaymentRequest.ReportingCode1";
        fieldNameKey[REPORTING_CODE_2] = "DomesticPaymentRequest.ReportingCode2";
    }

    /**
     * Parse one payment line item.
     *
     * @param currentPaymentLineItem
     * @param resourceManager
     * @param mediatorServices
     * @return
     * @throws AmsException
     */
    public static String[] parsePaymentLineItem(String currentPaymentLineItem, int file_fields_number) {

        String[] origPaymentRecord = currentPaymentLineItem.split(paymentDataDelimiter);
        // If there is only one field, the format cannot be valid.  The user probably has used wrong delimiter.
        if (origPaymentRecord.length == 1) {
            return null;
        }
        String[] paymentRecord = new String[file_fields_number];
        int numberOfFields = origPaymentRecord.length;
        if (numberOfFields > paymentRecord.length) {
            numberOfFields = paymentRecord.length;
        }
        for (int i = 0; i < numberOfFields; i++) {
            paymentRecord[i] = origPaymentRecord[i];
        }

        int fromIndex = numberOfFields;
        int toIndex = paymentRecord.length;
        if (fromIndex <= toIndex) {
            Arrays.fill(paymentRecord, fromIndex, toIndex, "");
        }

        return paymentRecord;
    }

    /**
     * Verifies the Payment Line Item. Doing the validation here instead of at
     * the business object user_validate() gives better error message
     * information.
     *
     * @exception com.amsinc.ecsg.frame.AmsException
     * @return boolean - indicates whether or not a valid Payment Data file
     * (true - valid false - invalid)
     */
    public static boolean validatePaymentLineItem(String[] paymentRecord, int paymentCount, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices)
            throws AmsException {

        boolean invalidFormat = false;
        String[] substitutionValues = {"", ""};

        if (paymentRecord == null) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_FORMAT_NOT_PIPE_DELIMITED);
            return true;
        }

		//Payment Method
        //manohar CR 597 - starts
        // moved this part as validatePaymentLineItemReqFields in AbstractPaymentDetail.java
        String paymentMethod = paymentRecord[PAYMENT_METHOD];

        //Beneficiary Name
        String beneficiaryName = paymentRecord[BENEFICIARY_NAME];
        if (beneficiaryName == null || beneficiaryName.trim().equals("")) {
            substitutionValues[0] = //"Beneficiary Name";
                    resourceManager.getText("DomesticPaymentRequest.PayeeName", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        //Beneficiary Account Number
        String beneficiaryAccountNo = paymentRecord[BENEFICIARY_ACCOUNT_NO];
        if (!TradePortalConstants.PAYMENT_METHOD_CCHK.equals(paymentMethod)
                && !TradePortalConstants.PAYMENT_METHOD_BCHK.equals(paymentMethod)
                && (beneficiaryAccountNo == null || beneficiaryAccountNo.trim().equals(""))) {
            substitutionValues[0] = //"Beneficiary Account No";
                    resourceManager.getText("DomesticPaymentRequest.PayeeAccountNumber", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        //Payment Amount
        String paymentAmount = paymentRecord[PAYMENT_AMOUNT];
        if (paymentAmount == null || paymentAmount.trim().equals("")) {
            substitutionValues[0] = //"Payment Amount";
                    resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        } else {
            try {
                // Convert the amount from its localized form to en_US form
                paymentRecord[PAYMENT_AMOUNT] = NumberValidator.getNonInternationalizedValue(paymentRecord[PAYMENT_AMOUNT], resourceManager.getResourceLocale(), false);
                if ((StringFunction.isNotBlank(paymentRecord[PAYMENT_AMOUNT])) && (new Float(paymentRecord[PAYMENT_AMOUNT]).intValue() != 0)) {
                    TPCurrencyUtility.validateAmount(paymentRecord[PAYMENT_CURRENCY],
                            paymentRecord[PAYMENT_AMOUNT],
                            resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE),
                            mediatorServices.getErrorManager());
                }
            } catch (InvalidAttributeValueException iae) {
                substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE);
                substitutionValues[1] = paymentRecord[PAYMENT_AMOUNT];
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_ERR, substitutionValues);
                invalidFormat = true;
            }

            if (mediatorServices.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
                invalidFormat = true;
            }

        }

        //Beneficiary Bank/Branch Code
        String beneficiaryBankBranchCode = paymentRecord[BENEFICIARY_BANK_BRANCH_CODE];
        if (!TradePortalConstants.PAYMENT_METHOD_CCHK.equals(paymentMethod)
                && !TradePortalConstants.PAYMENT_METHOD_BCHK.equals(paymentMethod)
                && (beneficiaryBankBranchCode == null || beneficiaryBankBranchCode.trim().equals(""))) {
            substitutionValues[0] = //"Beneficiary Bank Branch Code";
                    resourceManager.getText("DomesticPaymentRequest.PayeeBankCode", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("PaymentFileUploadServlet: Missing Required Beneficiary Bank Branch Code fields : " + substitutionValues);
            invalidFormat = true;
        }

        if ((paymentRecord[BENEFICIARY_BANK_BRANCH_CITY] + " " + paymentRecord[BENEFICIARY_BANK_BRANCH_PROVINCE]).length() > 32
                || (paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_CITY] + " " + paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE]).length() > 32) //VS CR 631
        /*|| (paymentRecord[SECOND_INTERMEDIARY_BANK_BRANCH_CITY] + " " + paymentRecord[SECOND_INTERMEDIARY_BANK_BRANCH_PROVINCE]).length() > 32*/ {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CITY_PROVINCE_MORE_THAN_31);
            invalidFormat = true;
        }

        // Central Bank Reporting 1
        if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod)
                && StringFunction.isBlank(paymentRecord[CENTRAL_BANK_REPORTING1])) {
            substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.CentraBankReporting", TradePortalConstants.TEXT_BUNDLE) + " 1";
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            invalidFormat = true;
        }
        
        // Check the length of text field.
        for (int i = 0; i < MAX_PAYMENT_FILE_FIELDS_NUMBER; i++) {
			//Rel9.0 IR#T36000028324 - Do not do FX Rate and Contract Number validations here.
            //They are validated elsewhere with a more specific error message. - [START]
            if (i == FX_RATE || i == FX_CONTRACT_NUMBER) {
                continue;
            }
            //-[END]			
            if (paymentRecord[i] != null && fieldSize[i] > 0 && paymentRecord[i].length() > fieldSize[i]) {
                String fieldName = resourceManager.getText(fieldNameKey[i], TradePortalConstants.TEXT_BUNDLE);
                switch (i) {
                    case BENEFICIARY_ADDRESS1:
                        fieldName += " 1";
                        break;
                    case BENEFICIARY_ADDRESS2:
                        fieldName += " 2";
                        break;
                    case BENEFICIARY_ADDRESS3:
                        fieldName += " 3";
                        break;
                    case BENEFICIARY_ADDRESS4:
                        fieldName += " 4";
                        break;
                    case BENEFICIARY_BANK_BRANCH_ADDRESS1:
                        fieldName += " 1";
                        break;
                    case BENEFICIARY_BANK_BRANCH_ADDRESS2:
                        fieldName += " 2";
                        break;
                    case FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1:
                        fieldName += " 1";
                        break;
                    case FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2:
                        fieldName += " 2";
                        break;
                    case CENTRAL_BANK_REPORTING1:
                        fieldName += " 1";
                        break;
                    case CENTRAL_BANK_REPORTING2:
                        fieldName += " 2";
                        break;
                    case CENTRAL_BANK_REPORTING3:
                        fieldName += " 3";
                        break;
                }
                substitutionValues[0] = fieldName;
                substitutionValues[1] = String.valueOf(paymentCount + 1);
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FIELD_TOO_LONG, substitutionValues);
                invalidFormat = true;
            }
        }

        // Additional edit for address field length if payment method = CBFT.
        if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentRecord[PAYMENT_METHOD])) {
            if (paymentRecord[BENEFICIARY_ADDRESS4] != null && paymentRecord[BENEFICIARY_ADDRESS4].length() > 0) {
                substitutionValues[0] = String.valueOf(paymentCount + 1);
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CBFT_BENE_ADD_LINE_4_BLANK, substitutionValues);
                invalidFormat = true;
            }
            if (paymentRecord[BENEFICIARY_ADDRESS3] != null && paymentRecord[BENEFICIARY_ADDRESS3].length() > 32) {
                substitutionValues[0] = resourceManager.getText(fieldNameKey[BENEFICIARY_ADDRESS3], TradePortalConstants.TEXT_BUNDLE) + " 3";
                substitutionValues[1] = String.valueOf(paymentCount + 1);
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FIELD_TOO_LONG, substitutionValues);
                invalidFormat = true;
            }
        }

        // Instruction Number can only be number.
        if (!StringFunction.isBlank(paymentRecord[INSTRUCTION_NUMBER]) && !paymentRecord[INSTRUCTION_NUMBER].matches("^\\d*$")) {
            substitutionValues[1] = resourceManager.getText(fieldNameKey[INSTRUCTION_NUMBER], TradePortalConstants.TEXT_BUNDLE);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.INVALID_NUMBER_ATTRIBUTE, substitutionValues);
            invalidFormat = true;
        }

		// Vshah IR-LMUK082538690 [START]
        // Validate First/Second Intermediary Bank Country.
        if (paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY] != null
                && !paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY].trim().equals("")
                && !ReferenceDataManager.getRefDataMgr().checkCode("COUNTRY", paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY])) {
            substitutionValues[0] = paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY];
            substitutionValues[1] = resourceManager.getText("DomesticPaymentRequest.FirstIntBankCountry", TradePortalConstants.TEXT_BUNDLE);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.INVALID_ATTRIBUTE, substitutionValues);
            invalidFormat = true;
        }
		// Vshah IR-LMUK082538690 [END]

        if (paymentRecord[CONFIDENTIAL_INDICATOR] == null || paymentRecord[CONFIDENTIAL_INDICATOR].trim().equals("")) {
            paymentRecord[CONFIDENTIAL_INDICATOR] = TradePortalConstants.INDICATOR_NO;
        }

		//Rel9.0 CR-921 - Start
        //Validate that both FX Contract Number and FX Rate have to be either both missing or both available
        //validations on the first line only
        //IR#T36000028320 - payment count from PaymentFileUploadServlet is initialized to zero
        //so, the check should be < 1 since the first row will be paymentCount=0
        if (paymentCount < 1) {
            invalidFormat = validateFXInfo(paymentRecord[FX_RATE], paymentRecord[FX_CONTRACT_NUMBER], mediatorServices);
        }
        //end

        return invalidFormat;
    }

    protected static boolean validateRequiredField(String[] paymentRecord, int field, int paymentCount, ResourceManager resourceManager, MediatorServices mediatorServices) throws AmsException {
        boolean isValid = true;
        String[] substitutionValues = {"", ""};
        if (paymentRecord[field] == null || paymentRecord[field].trim().equals("")) {
            substitutionValues[0] = resourceManager.getText(fieldNameKey[field], TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount + 1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            isValid = false;
        }
        return isValid;
    }

    /**
     * This method returns the country code for a given account, using either
     * the bank_country_code stored with the account, or the translated ISO
     * country code from REFDATA.
     *
     * @param accountOid primary key to Account
     * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
     * @return String bank branch country if available.
     * @exception com.amsinc.ecsg.frame.AmsException, RemoteException
     */
    public static String getBankBranchCountryForAccount(String accountOid) throws AmsException {

        String country = null;

        if (StringFunction.isNotBlank(accountOid)) {

            String selectCountrySQL = "SELECT ADDL_VALUE FROM REFDATA WHERE CODE = (select a.BANK_COUNTRY_CODE from account a where a.account_oid = ?) "
                    + " AND TABLE_TYPE = ?";

            long startGetDACCode = System.currentTimeMillis();
            DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(selectCountrySQL, false, new Object[]{accountOid, TradePortalConstants.COUNTRY_ISO3116_3});

            LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] "
                    + "\t[PERFORMANCE]"
                    + "\tgetDebitAccountCountryCode"
                    + "\t" + (System.currentTimeMillis() - startGetDACCode)
                    + "\tmilliseconds");

            if (countryDoc != null) {
                country = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");
            }
        }
        return country;
    }
}
