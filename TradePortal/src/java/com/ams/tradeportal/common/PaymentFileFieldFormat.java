/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PaymentDefinitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.amsinc.ecsg.frame.AmsException;

import java.rmi.RemoteException;

/**
 *

 */
public class PaymentFileFieldFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileFieldFormat.class);

    String lineType; // H, P, I
    int order;
    boolean isFieldRequired;
    boolean isDataRequired;
    int size;
    String name;
    String columnName;
    String description;
    boolean isAttribute; // this field maps to an attribute on Domestic Payment object
    static List allReqfields = new ArrayList();

    static List allReqFieldsFF = new ArrayList();
    static List othrReqFields = new ArrayList();
    static List cbftReqFields = new ArrayList();
    static List chkReqFields = new ArrayList();

    static {
        allReqfields.add(PaymentFileFieldFormat.COLUMN_HEADER_IDENTIFIER);
        allReqfields.add(PaymentFileFieldFormat.COLUMN_PAYMENT_IDENTIFIER);
        allReqfields.add(PaymentFileFieldFormat.COLUMN_DEBIT_ACCOUNT_NO);

        allReqfields.add(PaymentFileFieldFormat.COLUMN_PAYMENT_METHOD_TYPE);
        allReqfields.add(PaymentFileFieldFormat.COLUMN_AMOUNT_CURRENCY_CODE);
        allReqfields.add(PaymentFileFieldFormat.COLUMN_PAYMENT_AMOUNT);
        allReqfields.add(PaymentFileFieldFormat.COLUMN_PAYEE_NAME);
        allReqFieldsFF.add(PaymentFileFieldFormat.COLUMN_EXECUTION_DATE);
        othrReqFields.add(PaymentFileFieldFormat.COLUMN_PAYEE_BANK_CODE);
        cbftReqFields.add(PaymentFileFieldFormat.COLUMN_PAYEE_ADDRESS_LINE_1);
        cbftReqFields.add(PaymentFileFieldFormat.COLUMN_PAYEE_COUNTRY);
        cbftReqFields.add(PaymentFileFieldFormat.COLUMN_CENTRAL_BANK_REPORTING_1);
        chkReqFields.add(PaymentFileFieldFormat.COLUMN_DELIVERY_METHOD);
        chkReqFields.add(PaymentFileFieldFormat.COLUMN_PRINT_LOCATION);
        chkReqFields.add(PaymentFileFieldFormat.COLUMN_PAYABLE_LOCATION);

    }
    private static final HashMap<String, String> columnNameToFieldNameMap;
    private static final HashMap<String, String> columnNameToDescriptionMap;
    private static final HashMap<String, PaymentFileFieldFormat> hardCodedFields;

    static final String LINE_TYPE_HEADER = "H";
    static final String LINE_TYPE_PAYMENT = "P";
    static final String LINE_TYPE_TRAILER = "T";

    static final String   COLUMN_HEADER_IDENTIFIER                                = "header_identifier";
    static final String   COLUMN_PAYMENT_IDENTIFIER                               = "detail_identifier";
    static final String   COLUMN_INVOICE_IDENTIFIER			          = "inv_details_header";

    static final String   DEBIT_ACCOUNT_NO                                = "debit_account_no";
    static final String   COLUMN_DEBIT_ACCOUNT_NO                                = "debit_account_number";  
    static final String   DESC_DEBIT_ACCOUNT_NO                                = "Debit Account Number";  
    static final String   EXECUTION_DATE                                  = "execution_date";
    static final String   COLUMN_EXECUTION_DATE                                  = "execution_date";
    static final String   DESC_EXECUTION_DATE                                  = "Execution Date";
    static final String   AMOUNT_CURRENCY_CODE                                = "amount_currency_code";
    static final String   COLUMN_AMOUNT_CURRENCY_CODE                                = "payment_currency";
    static final String   DESC_AMOUNT_CURRENCY_CODE                                = "Payment Currency";
    static final String   PAYMENT_METHOD_TYPE                                  = "payment_method_type";
    static final String   COLUMN_PAYMENT_METHOD_TYPE                                  = "payment_method";
    static final String   DESC_PAYMENT_METHOD_TYPE                                  = "Payment Method";
    static final String   COLUMN_CUSTOMER_FILE_REF                             	  = "file_reference";
    static final String   DESC_CUSTOMER_FILE_REF                             	  = "File Reference";
    static final String   COLUMN_CONFIDENTIAL_INDICATOR                          = "confidential_ind";
    static final String   DESC_CONFIDENTIAL_INDICATOR                          = "Confidential Indicator";
    static final String   COLUMN_INDIVIDUAL_ACCT_ENTRIES                         = "indv_acct_entry_ind";
    static final String   DESC_INDIVIDUAL_ACCT_ENTRIES                         = "Individual Account Entry Indicator";
    static final String   PAYMENT_AMOUNT                                  = "amount";
    static final String   COLUMN_PAYMENT_AMOUNT                                  = "payment_amount";
    static final String   DESC_PAYMENT_AMOUNT                                  = "Payment Amount";
    static final String   CUSTOMER_REFERENCE                              = "customer_reference";
    static final String   COLUMN_CUSTOMER_REFERENCE                              = "customer_reference";
    static final String   DESC_CUSTOMER_REFERENCE                              = "Customer Reference";
    static final String   COLUMN_PAYEE_CODE                    			  = "beneficiary_code";
    static final String   DESC_PAYEE_CODE                    			  = "Beneficiary Code";
    static final String   PAYEE_NAME                                = "payee_name";
    static final String   COLUMN_PAYEE_NAME                                = "beneficiary_name";
    static final String   DESC_PAYEE_NAME                                = "Beneficiary Name";
    static final String   PAYEE_ACCOUNT_NUMBER                          = "payee_account_number";
    static final String   COLUMN_PAYEE_ACCOUNT_NUMBER                          = "bene_account_number";
    static final String   DESC_PAYEE_ACCOUNT_NUMBER                          = "Beneficiary Account Number";
    static final String   PAYEE_ADDRESS_LINE_1                            = "payee_address_line_1";
    static final String   COLUMN_PAYEE_ADDRESS_LINE_1                            = "bene_address1";
    static final String   DESC_PAYEE_ADDRESS_LINE_1                            = "Beneficiary Address 1";
    static final String   PAYEE_ADDRESS_LINE_2                            = "payee_address_line_2";
    static final String   COLUMN_PAYEE_ADDRESS_LINE_2                            = "bene_address2";
    static final String   DESC_PAYEE_ADDRESS_LINE_2                            = "Beneficiary Address 2";
    static final String   PAYEE_ADDRESS_LINE_3                            = "payee_address_line_3";
    static final String   COLUMN_PAYEE_ADDRESS_LINE_3                            = "bene_address3";
    static final String   DESC_PAYEE_ADDRESS_LINE_3                            = "Beneficiary Address 3";
    static final String   PAYEE_ADDRESS_LINE_4                            = "payee_address_line_4";
    static final String   COLUMN_PAYEE_ADDRESS_LINE_4                            = "bene_address4";
    static final String   DESC_PAYEE_ADDRESS_LINE_4                            = "Beneficiary Address 4";
    static final String   PAYEE_COUNTRY                 			  = "country";
    static final String   COLUMN_PAYEE_COUNTRY                 			  = "beneficiary_country";
    static final String   DESC_PAYEE_COUNTRY                 			  = "Country";
    static final String   PAYEE_FAX_NUMBER                              = "payee_fax_number";
    static final String   COLUMN_PAYEE_FAX_NUMBER                              = "bene_fax_no";
    static final String   DESC_PAYEE_FAX_NUMBER                              = "Beneficiary Fax Number";
    static final String   PAYEE_EMAIL                            = "payee_email";
    static final String   COLUMN_PAYEE_EMAIL                            = "bene_email_id";
    static final String   DESC_PAYEE_EMAIL                            = "Beneficiary Email ID";
    static final String   PAYEE_BANK_CODE                    = "payee_bank_code";
    static final String   COLUMN_PAYEE_BANK_CODE                    = "bene_bank_branch_code";
    static final String   DESC_PAYEE_BANK_CODE                    = "Beneficiary Bank Branch Code";
    static final String   PAYEE_BANK_NAME                           = "payee_bank_name";
    static final String   COLUMN_PAYEE_BANK_NAME                           = "bene_bank_name";
    static final String   DESC_PAYEE_BANK_NAME                           = "Beneficiary Bank Name";
    static final String   PAYEE_BRANCH_NAME                    = "payee_branch_name";
    static final String   COLUMN_PAYEE_BRANCH_NAME                    = "bene_bnk_brnch_name";
    static final String   DESC_PAYEE_BRANCH_NAME                    = "Beneficiary Bank Branch Name";
    static final String   ADDRESS_LINE_1                = "address_line_1";
    static final String   COLUMN_ADDRESS_LINE_1                = "bene_bnk_brnch_addr1";
    static final String   DESC_ADDRESS_LINE_1                = "Beneficiary Bank Branch Address 1";
    static final String   ADDRESS_LINE_2                = "address_line_2";
    static final String   COLUMN_ADDRESS_LINE_2                = "bene_bnk_brnch_addr2";
    static final String   DESC_ADDRESS_LINE_2                = "Beneficiary Bank Branch Address 1";
    static final String   COLUMN_CITY                    = "bene_bnk_brnch_city";
    static final String   DESC_CITY                    = "Beneficiary Bank Branch City";
    static final String   COLUMN_PROVINCE                = "bene_bnk_brnch_prvnc";
    static final String   DESC_PROVINCE                = "Beneficiary Bank/Branch Province";
    static final String   ADDRESS_LINE_3                = "address_line_3";
    static final String   COUNTRY                 = "payee_bank_country";
    static final String   COLUMN_COUNTRY                 = "bene_bnk_brnch_cntry";
    static final String   DESC_COUNTRY                 = "Beneficiary Bank/Branch Country";
    static final String   BANK_CHARGES_TYPE                                         = "bank_charges_type";
    static final String   COLUMN_BANK_CHARGES_TYPE                                         = "charges";
    static final String   DESC_BANK_CHARGES_TYPE                                         = "Charges";
    static final String   PAYABLE_LOCATION                                = "payable_location";
    static final String   COLUMN_PAYABLE_LOCATION                                = "payable_location";
    static final String   DESC_PAYABLE_LOCATION                                = "Payable Location";
    static final String   PRINT_LOCATION                                  = "print_location";
    static final String   COLUMN_PRINT_LOCATION                                  = "print_location";
    static final String   DESC_PRINT_LOCATION                                  = "Print Location";
    static final String   DELIVERY_METHOD                      = "delivery_method";
    static final String   COLUMN_DELIVERY_METHOD                      = "delvry_mth_n_delvrto";
    static final String   DESC_DELIVERY_METHOD                      = "Delivery Method and Deliver To";
    static final String   MAILING_ADDRESS_LINE_1                               = "mailing_address_line_1";
    static final String   COLUMN_MAILING_ADDRESS_LINE_1                               = "mailing_address1";
    static final String   DESC_MAILING_ADDRESS_LINE_1                               = "Mailing Address 1";
    static final String   MAILING_ADDRESS_LINE_2                                = "mailing_address_line_2";
    static final String   COLUMN_MAILING_ADDRESS_LINE_2                                = "mailing_address2";
    static final String   DESC_MAILING_ADDRESS_LINE_2                                = "Mailing Address 2";
    static final String   MAILING_ADDRESS_LINE_3                                = "mailing_address_line_3";
    static final String   COLUMN_MAILING_ADDRESS_LINE_3                                = "mailing_address3";
    static final String   DESC_MAILING_ADDRESS_LINE_3                                = "Mailing Address 3";
    static final String   MAILING_ADDRESS_LINE_4                                = "mailing_address_line_4";
    static final String   COLUMN_MAILING_ADDRESS_LINE_4                                = "mailing_address4";
    static final String   DESC_MAILING_ADDRESS_LINE_4                                = "Mailing Address 4";
    static final String   INSTRUCTION_NUMBER                              = "payee_instruction_number";
    static final String   COLUMN_INSTRUCTION_NUMBER                              = "instruction_number";
    static final String   DESC_INSTRUCTION_NUMBER                              = "Instruction Number";
    static final String   PAYMENT_DETAILS                                 = "payee_description";
    static final String   COLUMN_PAYMENT_DETAILS                                 = "details_of_payment";
    static final String   DESC_PAYMENT_DETAILS                                 = "Details of Payment";
    static final String   FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE             = "first_intermediary_bank.bank_branch_code";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE             = "f_int_bnk_brnch_cd";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE             = "First Intermediary Bank/Branch Code";
    static final String   FIRST_INTERMEDIARY_BANK_BANK_NAME                    = "first_intermediary_bank.bank_name";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BANK_NAME                    = "f_int_bnk_name";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BANK_NAME                    = "First Intermediary Bank Name";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_NAME             = "first_intermediary_bank.branch_name";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_NAME             = "f_int_bnk_brnch_nm";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_NAME             = "First Intermediary Bank/Branch Code";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1         = "first_intermediary_bank.address_line_1";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1         = "f_int_bnk_brnch_addr1";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1         = "First Intermediary Bank/Branch Address 1";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2         = "first_intermediary_bank.address_line_2";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2         = "f_int_bnk_brnch_addr2";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2         = "First Intermediary Bank/Branch Address 2";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_CITY             = "first_intermediary_bank.city";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_CITY             = "f_int_bnk_brnch_city";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_CITY             = "First Intermediary Bank/Branch City";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE         = "first_intermediary_bank.province";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE         = "f_int_bnk_brnch_prvnc";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE         = "First Intermediary Bank/Branch Province";
    static final String   FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY          = "first_intermediary_bank.country";
    static final String   COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY          = "f_int_bnk_brnch_cntry";
    static final String   DESC_FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY          = "First Intermediary Bank/Branch Country";
    static final String   CENTRAL_BANK_REPORTING_1                         = "central_bank_reporting_1";
    static final String   COLUMN_CENTRAL_BANK_REPORTING_1                         = "central_bank_rep1";
    static final String   DESC_CENTRAL_BANK_REPORTING_1                         = "Central Bank Reporting 1";
    static final String   CENTRAL_BANK_REPORTING_2                         = "central_bank_reporting_2";
    static final String   COLUMN_CENTRAL_BANK_REPORTING_2                         = "central_bank_rep2";
    static final String   DESC_CENTRAL_BANK_REPORTING_2                         = "Central Bank Reporting 2";
    static final String   CENTRAL_BANK_REPORTING_3                         = "central_bank_reporting_3";
    static final String   COLUMN_CENTRAL_BANK_REPORTING_3                         = "central_bank_rep3";
    static final String   DESC_CENTRAL_BANK_REPORTING_3                         = "Central Bank Reporting 3";

    static final String   REPORTING_CODE_1                         = "reporting_code_1";
    static final String   COLUMN_REPORTING_CODE_1                         = "reporting_code1";
    static final String   DESCBREPORTING_CODE_1                         = "Reporting Code 1";
    static final String   REPORTING_CODE_2                         = "reporting_code_2";
    static final String   COLUMN_REPORTING_CODE_2                         = "reporting_code2";
    static final String   DESCBREPORTING_CODE_2                         = "Reporting Code 2";
    static final String   USER_DEFINED_FIELD_1                     = "user_defined_field_1";
    static final String   COLUMN_USER_DEFINED_FIELD_1                     = "users_def1";
    static final String   DESCBUSER_DEFINED_FIELD_1                     = "User Defined Field 1";
    static final String   USER_DEFINED_FIELD_2                    = "user_defined_field_2";
    static final String   COLUMN_USER_DEFINED_FIELD_2                    = "users_def2";
    static final String   DESCBUSER_DEFINED_FIELD_2                    = "User Defined Field 2";
    static final String   USER_DEFINED_FIELD_3                     = "user_defined_field_3";
    static final String   COLUMN_USER_DEFINED_FIELD_3                     = "users_def3";
    static final String   DESCBUSER_DEFINED_FIELD_3                     = "User Defined Field 3";
    static final String   USER_DEFINED_FIELD_4                     = "user_defined_field_4";
    static final String   COLUMN_USER_DEFINED_FIELD_4                     = "users_def4";
    static final String   DESCBUSER_DEFINED_FIELD_4                     = "User Defined Field 4";
    static final String   USER_DEFINED_FIELD_5                     = "user_defined_field_5";
    static final String   COLUMN_USER_DEFINED_FIELD_5                     = "users_def5";
    static final String   DESCBUSER_DEFINED_FIELD_5                     = "User Defined Field 5";
    static final String   USER_DEFINED_FIELD_6                     = "user_defined_field_6";
    static final String   COLUMN_USER_DEFINED_FIELD_6                     = "users_def6";
    static final String   DESCBUSER_DEFINED_FIELD_6                     = "User Defined Field 6";
    static final String   USER_DEFINED_FIELD_7                     = "user_defined_field_7";
    static final String   COLUMN_USER_DEFINED_FIELD_7                     = "users_def7";
    static final String   DESCBUSER_DEFINED_FIELD_7                     = "User Defined Field 7";
    static final String   USER_DEFINED_FIELD_8                     = "user_defined_field_8";
    static final String   COLUMN_USER_DEFINED_FIELD_8                     = "users_def8";
    static final String   DESCBUSER_DEFINED_FIELD_8                     = "User Defined Field 8";
    static final String   USER_DEFINED_FIELD_9                     = "user_defined_field_9";
    static final String   COLUMN_USER_DEFINED_FIELD_9                     = "users_def9";
    static final String   DESCBUSER_DEFINED_FIELD_9                     = "User Defined Field 9";
    static final String   USER_DEFINED_FIELD_10                    = "user_defined_field_10";
    static final String   COLUMN_USER_DEFINED_FIELD_10                    = "users_def10";
    static final String   DESCBUSER_DEFINED_FIELD_10                    = "User Defined Field 10";
    static final String   PAYEE_INVOICE_DETAILS                    = "payee_invoice_details";

    static final String   INVOICE_DETAIL                    = "invoice_detail";   
    static final String   COLUMN_INVOICE_DETAIL                    = "inv_details_lineitm";  
    //MEerupula Rel 9.0 CR-921
    static final String   COLUMN_FX_CONTRACT_NUMBER                = "fx_contract_number";
    static final String   DESC_FX_CONTRACT_NUMBER                  = "FX Contract Number";
    static final String   COLUMN_FX_CONTRACT_RATE                  = "fx_contract_rate";
    static final String   DESC_FX_CONTRACT_RATE                    = "FX Contract Rate";
    
    // ABA & PNG special fields
    static final String FD_RECORD_TYPE = "fd_record_type";
    static final String FD_BSB = "fd_bsb";
    static final String FD_ACCOUNT = "fd_account";
    static final String FD_RESERVED1 = "fd_reserved1";
    static final String FD_SEQUENCE_NUMBER = "fd_sequence_number";
    static final String FD_USER_FINANCE_INSTITUTION = "fd_user_finance_institution";
    static final String FD_RESERVED2 = "fd_reserved2";
    static final String FD_USER_SUPPLYING_FILE = "fd_user_supplying_file";
    static final String FD_USER_ID_NUMBER = "fd_user_id_number";
    static final String FD_DESC_ENTRIES = "fd_desc_entries";
    static final String FD_TIME = "fd_time";
    static final String FD_RESERVED3 = "fd_reserved3";
    static final String DR_RECORD_TYPE = "dr_record_type";
    static final String DR_WITHHOLD_TAX_IND = "dr_withhold_tax_ind";
    static final String DR_TRACE_BSB_NUMBER = "dr_trace_bsb_number";
    static final String DR_REMITTER_NAME = "dr_remitter_name";
    static final String DR_WITHHOLD_TAX_AMT = "dr_withhold_tax_amt";

    static final String FD_RESERVED4 = "fd_reserved4";
    static final String FD_RESERVED5 = "fd_reserved5";
    static final String DR_ACCOUNT_TYPE = "dr_account_type";
    static final String DR_ACCOUNT_DETAILS = "dr_account_details";
    static final String DR_LODGE_BSB_NUMBER = "dr_lodge_bsb_number";
    static final String DR_LODGE_ACCOUNT_TYPE = "dr_lodge_account_type";
    static final String DR_RESERVED1 = "dr_reserved1";

    private static final PaymentFileFieldFormat FD_RECORD_TYPE_FIELD = new PaymentFileFieldFormat(FD_RECORD_TYPE, "fd_record_type", "Record Type", LINE_TYPE_HEADER, 0, true, true, 1, false);
    private static final PaymentFileFieldFormat FD_BSB_FIELD = new PaymentFileFieldFormat(FD_BSB, "fd_bsb", "BSB", LINE_TYPE_HEADER, 0, true, true, 7, false);
    private static final PaymentFileFieldFormat FD_ACCOUNT_FIELD = new PaymentFileFieldFormat(FD_ACCOUNT, "fd_account", "Account", LINE_TYPE_HEADER, 0, true, true, 9, false);
    private static final PaymentFileFieldFormat FD_RESERVED1_FIELD = new PaymentFileFieldFormat(FD_RESERVED1, "fd_reserved1", "Reserved 1", LINE_TYPE_HEADER, 0, true, false, 1, false);
    private static final PaymentFileFieldFormat FD_SEQUENCE_NUMBER_FIELD = new PaymentFileFieldFormat(FD_SEQUENCE_NUMBER, "fd_sequence_number", "Sequence Number", LINE_TYPE_HEADER, 0, true, false, 2, false);
    private static final PaymentFileFieldFormat FD_USER_FINANCE_INSTITUTION_FIELD = new PaymentFileFieldFormat(FD_USER_FINANCE_INSTITUTION, "fd_user_finance_institution", "Name of User-Financial Institution", LINE_TYPE_HEADER, 0, true, true, 3, false);
    private static final PaymentFileFieldFormat FD_RESERVED2_FIELD = new PaymentFileFieldFormat(FD_RESERVED2, "fd_reserved2", "Reserved 2", LINE_TYPE_HEADER, 0, true, false, 7, false);
    private static final PaymentFileFieldFormat FD_USER_SUPPLYING_FILE_FIELD = new PaymentFileFieldFormat(FD_USER_SUPPLYING_FILE, "fd_user_supplying_file", "Name of User Supplying File", LINE_TYPE_HEADER, 0, true, true, 26, false);
    private static final PaymentFileFieldFormat FD_USER_ID_NUMBER_FIELD = new PaymentFileFieldFormat(FD_USER_ID_NUMBER, "fd_user_id_number", "User Identification Number", LINE_TYPE_HEADER, 0, true, true, 6, false);
    private static final PaymentFileFieldFormat FD_DESC_ENTRIES_FIELD = new PaymentFileFieldFormat(FD_DESC_ENTRIES, "fd_desc_entries", "Description of Entries on File", LINE_TYPE_HEADER, 0, true, true, 12, false);
    private static final PaymentFileFieldFormat FD_TIME_FIELD = new PaymentFileFieldFormat(FD_TIME, "fd_time", "Time", LINE_TYPE_HEADER, 0, true, false, 4, false);
    private static final PaymentFileFieldFormat FD_RESERVED3_FIELD = new PaymentFileFieldFormat(FD_RESERVED3, "fd_reserved3", "Reserved 3", LINE_TYPE_HEADER, 0, true, false, 36, false);
    private static final PaymentFileFieldFormat DR_RECORD_TYPE_FIELD = new PaymentFileFieldFormat(DR_RECORD_TYPE, "dr_record_type", "Record Type", LINE_TYPE_PAYMENT, 0, true, false, 1, false);
    private static final PaymentFileFieldFormat DR_WITHHOLD_TAX_IND_FIELD = new PaymentFileFieldFormat(DR_WITHHOLD_TAX_IND, "dr_withhold_tax_ind", "Withhold Tax ID", LINE_TYPE_PAYMENT, 0, true, false, 1, false);
    private static final PaymentFileFieldFormat DR_TRACE_BSB_NUMBER_FIELD = new PaymentFileFieldFormat(DR_TRACE_BSB_NUMBER, "dr_trace_bsb_number", "Trace BSB Number", LINE_TYPE_PAYMENT, 0, true, true, 7, false);
    private static final PaymentFileFieldFormat DR_REMITTER_NAME_FIELD = new PaymentFileFieldFormat(DR_REMITTER_NAME, "dr_remitter_name", "Name of Remitter", LINE_TYPE_PAYMENT, 0, true, true, 16, false);
    private static final PaymentFileFieldFormat DR_WITHHOLD_TAX_AMT_FIELD = new PaymentFileFieldFormat(DR_WITHHOLD_TAX_AMT, "dr_withhold_tax_amt", "Withhold Tax Amount", LINE_TYPE_PAYMENT, 0, true, false, 1, false);

    private static final PaymentFileFieldFormat FD_RESERVED4_FIELD = new PaymentFileFieldFormat(FD_RESERVED4, "fd_reserved4", "Reserved 1", LINE_TYPE_HEADER, 0, true, false, 17, false);
    private static final PaymentFileFieldFormat FD_RESERVED5_FIELD = new PaymentFileFieldFormat(FD_RESERVED5, "fd_reserved5", "Reserved 2", LINE_TYPE_HEADER, 0, true, false, 50, false);
    private static final PaymentFileFieldFormat DR_ACCOUNT_TYPE_FIELD = new PaymentFileFieldFormat(DR_ACCOUNT_TYPE, "dr_account_type", "Account Type", LINE_TYPE_PAYMENT, 0, true, true, 3, false);
    private static final PaymentFileFieldFormat DR_ACCOUNT_DETAILS_FIELD = new PaymentFileFieldFormat(DR_ACCOUNT_DETAILS, "dr_account_details", "New/Varied BSB/Account Details", LINE_TYPE_PAYMENT, 0, true, false, 1, false);
    private static final PaymentFileFieldFormat DR_LODGE_BSB_NUMBER_FIELD = new PaymentFileFieldFormat(DR_LODGE_BSB_NUMBER, "dr_lodge_bsb_number", "Lodgement Reference BSB Number", LINE_TYPE_PAYMENT, 0, true, true, 7, false);
    private static final PaymentFileFieldFormat DR_LODGE_ACCOUNT_TYPE_FIELD = new PaymentFileFieldFormat(DR_LODGE_ACCOUNT_TYPE, "dr_lodge_account_type", "Lodgement Reference Account Type", LINE_TYPE_PAYMENT, 0, true, true, 3, false);
    private static final PaymentFileFieldFormat DR_RESERVED1_FIELD = new PaymentFileFieldFormat(DR_RESERVED1, "dr_reserved1", "Reserved", LINE_TYPE_PAYMENT, 0, true, false, 8, false);

    static {
        columnNameToFieldNameMap = new HashMap<String, String>();
        columnNameToFieldNameMap.put(COLUMN_DEBIT_ACCOUNT_NO, DEBIT_ACCOUNT_NO);
        columnNameToFieldNameMap.put(COLUMN_EXECUTION_DATE, EXECUTION_DATE);
        columnNameToFieldNameMap.put(COLUMN_AMOUNT_CURRENCY_CODE, AMOUNT_CURRENCY_CODE);
        columnNameToFieldNameMap.put(COLUMN_PAYMENT_METHOD_TYPE, PAYMENT_METHOD_TYPE);
        columnNameToFieldNameMap.put(COLUMN_PAYMENT_AMOUNT, PAYMENT_AMOUNT);
        columnNameToFieldNameMap.put(COLUMN_CUSTOMER_REFERENCE, CUSTOMER_REFERENCE);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_NAME, PAYEE_NAME);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_ACCOUNT_NUMBER, PAYEE_ACCOUNT_NUMBER);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_ADDRESS_LINE_1, PAYEE_ADDRESS_LINE_1);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_ADDRESS_LINE_2, PAYEE_ADDRESS_LINE_2);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_ADDRESS_LINE_3, PAYEE_ADDRESS_LINE_3);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_ADDRESS_LINE_4, PAYEE_ADDRESS_LINE_4);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_COUNTRY, PAYEE_COUNTRY);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_FAX_NUMBER, PAYEE_FAX_NUMBER);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_EMAIL, PAYEE_EMAIL);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_BANK_CODE, PAYEE_BANK_CODE);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_BANK_NAME, PAYEE_BANK_NAME);
        columnNameToFieldNameMap.put(COLUMN_PAYEE_BRANCH_NAME, PAYEE_BRANCH_NAME);
        columnNameToFieldNameMap.put(COLUMN_ADDRESS_LINE_1, ADDRESS_LINE_1);
        columnNameToFieldNameMap.put(COLUMN_ADDRESS_LINE_2, ADDRESS_LINE_2);
        columnNameToFieldNameMap.put(COLUMN_COUNTRY, COUNTRY);
        columnNameToFieldNameMap.put(COLUMN_BANK_CHARGES_TYPE, BANK_CHARGES_TYPE);
        columnNameToFieldNameMap.put(COLUMN_PAYABLE_LOCATION, PAYABLE_LOCATION);
        columnNameToFieldNameMap.put(COLUMN_PRINT_LOCATION, PRINT_LOCATION);
        columnNameToFieldNameMap.put(COLUMN_DELIVERY_METHOD, DELIVERY_METHOD);
        columnNameToFieldNameMap.put(COLUMN_MAILING_ADDRESS_LINE_1, MAILING_ADDRESS_LINE_1);
        columnNameToFieldNameMap.put(COLUMN_MAILING_ADDRESS_LINE_2, MAILING_ADDRESS_LINE_2);
        columnNameToFieldNameMap.put(COLUMN_MAILING_ADDRESS_LINE_3, MAILING_ADDRESS_LINE_3);
        columnNameToFieldNameMap.put(COLUMN_MAILING_ADDRESS_LINE_4, MAILING_ADDRESS_LINE_4);
        columnNameToFieldNameMap.put(COLUMN_INSTRUCTION_NUMBER, INSTRUCTION_NUMBER);
        columnNameToFieldNameMap.put(COLUMN_PAYMENT_DETAILS, PAYMENT_DETAILS);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE, FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BANK_NAME, FIRST_INTERMEDIARY_BANK_BANK_NAME);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_NAME, FIRST_INTERMEDIARY_BANK_BRANCH_NAME);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1, FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2, FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_CITY, FIRST_INTERMEDIARY_BANK_BRANCH_CITY);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE, FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE);
        columnNameToFieldNameMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY, FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY);
        columnNameToFieldNameMap.put(COLUMN_CENTRAL_BANK_REPORTING_1, CENTRAL_BANK_REPORTING_1);
        columnNameToFieldNameMap.put(COLUMN_CENTRAL_BANK_REPORTING_2, CENTRAL_BANK_REPORTING_2);
        columnNameToFieldNameMap.put(COLUMN_CENTRAL_BANK_REPORTING_3, CENTRAL_BANK_REPORTING_3);

        columnNameToFieldNameMap.put(COLUMN_REPORTING_CODE_1, REPORTING_CODE_1);
        columnNameToFieldNameMap.put(COLUMN_REPORTING_CODE_2, REPORTING_CODE_2);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_1, USER_DEFINED_FIELD_1);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_2, USER_DEFINED_FIELD_2);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_3, USER_DEFINED_FIELD_3);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_4, USER_DEFINED_FIELD_4);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_5, USER_DEFINED_FIELD_5);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_6, USER_DEFINED_FIELD_6);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_7, USER_DEFINED_FIELD_7);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_8, USER_DEFINED_FIELD_8);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_9, USER_DEFINED_FIELD_9);
        columnNameToFieldNameMap.put(COLUMN_USER_DEFINED_FIELD_10, USER_DEFINED_FIELD_10);
        columnNameToFieldNameMap.put(COLUMN_INVOICE_DETAIL, INVOICE_DETAIL);

        hardCodedFields = new HashMap<String, PaymentFileFieldFormat>();
        hardCodedFields.put(FD_RECORD_TYPE_FIELD.name, FD_RECORD_TYPE_FIELD);
        hardCodedFields.put(FD_BSB_FIELD.name, FD_BSB_FIELD);
        hardCodedFields.put(FD_ACCOUNT_FIELD.name, FD_ACCOUNT_FIELD);
        hardCodedFields.put(FD_RESERVED1_FIELD.name, FD_RESERVED1_FIELD);
        hardCodedFields.put(FD_SEQUENCE_NUMBER_FIELD.name, FD_SEQUENCE_NUMBER_FIELD);
        hardCodedFields.put(FD_USER_FINANCE_INSTITUTION_FIELD.name, FD_USER_FINANCE_INSTITUTION_FIELD);
        hardCodedFields.put(FD_RESERVED2_FIELD.name, FD_RESERVED2_FIELD);
        hardCodedFields.put(FD_USER_SUPPLYING_FILE_FIELD.name, FD_USER_SUPPLYING_FILE_FIELD);
        hardCodedFields.put(FD_USER_ID_NUMBER_FIELD.name, FD_USER_ID_NUMBER_FIELD);
        hardCodedFields.put(FD_DESC_ENTRIES_FIELD.name, FD_DESC_ENTRIES_FIELD);
        hardCodedFields.put(FD_TIME_FIELD.name, FD_TIME_FIELD);
        hardCodedFields.put(FD_RESERVED3_FIELD.name, FD_RESERVED3_FIELD);
        hardCodedFields.put(DR_RECORD_TYPE_FIELD.name, DR_RECORD_TYPE_FIELD);
        hardCodedFields.put(DR_WITHHOLD_TAX_IND_FIELD.name, DR_WITHHOLD_TAX_IND_FIELD);
        hardCodedFields.put(DR_TRACE_BSB_NUMBER_FIELD.name, DR_TRACE_BSB_NUMBER_FIELD);
        hardCodedFields.put(DR_REMITTER_NAME_FIELD.name, DR_REMITTER_NAME_FIELD);
        hardCodedFields.put(DR_WITHHOLD_TAX_AMT_FIELD.name, DR_WITHHOLD_TAX_AMT_FIELD);
        hardCodedFields.put(FD_RESERVED4_FIELD.name, FD_RESERVED4_FIELD);
        hardCodedFields.put(FD_RESERVED5_FIELD.name, FD_RESERVED5_FIELD);
        hardCodedFields.put(DR_ACCOUNT_TYPE_FIELD.name, DR_ACCOUNT_TYPE_FIELD);
        hardCodedFields.put(DR_ACCOUNT_DETAILS_FIELD.name, DR_ACCOUNT_DETAILS_FIELD);
        hardCodedFields.put(DR_LODGE_BSB_NUMBER_FIELD.name, DR_LODGE_BSB_NUMBER_FIELD);
        hardCodedFields.put(DR_LODGE_ACCOUNT_TYPE_FIELD.name, DR_LODGE_ACCOUNT_TYPE_FIELD);
        hardCodedFields.put(DR_RESERVED1_FIELD.name, DR_RESERVED1_FIELD);

        columnNameToDescriptionMap = new HashMap<String, String>();
        columnNameToDescriptionMap.put(COLUMN_DEBIT_ACCOUNT_NO, DESC_DEBIT_ACCOUNT_NO);
        columnNameToDescriptionMap.put(COLUMN_EXECUTION_DATE, DESC_EXECUTION_DATE);
        columnNameToDescriptionMap.put(COLUMN_AMOUNT_CURRENCY_CODE, DESC_AMOUNT_CURRENCY_CODE);
        columnNameToDescriptionMap.put(COLUMN_PAYMENT_METHOD_TYPE, DESC_PAYMENT_METHOD_TYPE);
        columnNameToDescriptionMap.put(COLUMN_CUSTOMER_FILE_REF, DESC_CUSTOMER_FILE_REF);
        columnNameToDescriptionMap.put(COLUMN_CONFIDENTIAL_INDICATOR, DESC_CONFIDENTIAL_INDICATOR);
        columnNameToDescriptionMap.put(COLUMN_INDIVIDUAL_ACCT_ENTRIES, DESC_INDIVIDUAL_ACCT_ENTRIES);
        columnNameToDescriptionMap.put(COLUMN_PAYMENT_AMOUNT, DESC_PAYMENT_AMOUNT);
        columnNameToDescriptionMap.put(COLUMN_CUSTOMER_REFERENCE, DESC_CUSTOMER_REFERENCE);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_CODE, DESC_PAYEE_CODE);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_NAME, DESC_PAYEE_NAME);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_ACCOUNT_NUMBER, DESC_PAYEE_ACCOUNT_NUMBER);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_ADDRESS_LINE_1, DESC_PAYEE_ADDRESS_LINE_1);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_ADDRESS_LINE_2, DESC_PAYEE_ADDRESS_LINE_2);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_ADDRESS_LINE_3, DESC_PAYEE_ADDRESS_LINE_3);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_ADDRESS_LINE_4, DESC_PAYEE_ADDRESS_LINE_4);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_COUNTRY, DESC_PAYEE_COUNTRY);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_FAX_NUMBER, DESC_PAYEE_FAX_NUMBER);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_EMAIL, DESC_PAYEE_EMAIL);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_BANK_CODE, DESC_PAYEE_BANK_CODE);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_BANK_NAME, DESC_PAYEE_BANK_NAME);
        columnNameToDescriptionMap.put(COLUMN_PAYEE_BRANCH_NAME, DESC_PAYEE_BRANCH_NAME);
        columnNameToDescriptionMap.put(COLUMN_ADDRESS_LINE_1, DESC_ADDRESS_LINE_1);
        columnNameToDescriptionMap.put(COLUMN_ADDRESS_LINE_2, DESC_ADDRESS_LINE_2);
        columnNameToDescriptionMap.put(COLUMN_CITY, DESC_CITY);
        columnNameToDescriptionMap.put(COLUMN_PROVINCE, DESC_PROVINCE);
        columnNameToDescriptionMap.put(COLUMN_COUNTRY, DESC_COUNTRY);
        columnNameToDescriptionMap.put(COLUMN_BANK_CHARGES_TYPE, DESC_BANK_CHARGES_TYPE);
        columnNameToDescriptionMap.put(COLUMN_PAYABLE_LOCATION, DESC_PAYABLE_LOCATION);
        columnNameToDescriptionMap.put(COLUMN_PRINT_LOCATION, DESC_PRINT_LOCATION);
        columnNameToDescriptionMap.put(COLUMN_DELIVERY_METHOD, DESC_DELIVERY_METHOD);
        columnNameToDescriptionMap.put(COLUMN_MAILING_ADDRESS_LINE_1, DESC_MAILING_ADDRESS_LINE_1);
        columnNameToDescriptionMap.put(COLUMN_MAILING_ADDRESS_LINE_2, DESC_MAILING_ADDRESS_LINE_2);
        columnNameToDescriptionMap.put(COLUMN_MAILING_ADDRESS_LINE_3, DESC_MAILING_ADDRESS_LINE_3);
        columnNameToDescriptionMap.put(COLUMN_MAILING_ADDRESS_LINE_4, DESC_MAILING_ADDRESS_LINE_4);
        columnNameToDescriptionMap.put(COLUMN_INSTRUCTION_NUMBER, DESC_INSTRUCTION_NUMBER);
        columnNameToDescriptionMap.put(COLUMN_PAYMENT_DETAILS, DESC_PAYMENT_DETAILS);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE, DESC_FIRST_INTERMEDIARY_BANK_BANK_BRANCH_CODE);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BANK_NAME, DESC_FIRST_INTERMEDIARY_BANK_BANK_NAME);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_NAME, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_NAME);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_1);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS_LINE_2);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_CITY, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_CITY);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE);
        columnNameToDescriptionMap.put(COLUMN_FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY, DESC_FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY);
        columnNameToDescriptionMap.put(COLUMN_CENTRAL_BANK_REPORTING_1, DESC_CENTRAL_BANK_REPORTING_1);
        columnNameToDescriptionMap.put(COLUMN_CENTRAL_BANK_REPORTING_2, DESC_CENTRAL_BANK_REPORTING_2);
        columnNameToDescriptionMap.put(COLUMN_CENTRAL_BANK_REPORTING_3, DESC_CENTRAL_BANK_REPORTING_3);

        columnNameToDescriptionMap.put(COLUMN_REPORTING_CODE_1, REPORTING_CODE_1);
        columnNameToDescriptionMap.put(COLUMN_REPORTING_CODE_2, REPORTING_CODE_2);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_1, USER_DEFINED_FIELD_1);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_2, USER_DEFINED_FIELD_2);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_3, USER_DEFINED_FIELD_3);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_4, USER_DEFINED_FIELD_4);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_5, USER_DEFINED_FIELD_5);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_6, USER_DEFINED_FIELD_6);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_7, USER_DEFINED_FIELD_7);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_8, USER_DEFINED_FIELD_8);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_9, USER_DEFINED_FIELD_9);
        columnNameToDescriptionMap.put(COLUMN_USER_DEFINED_FIELD_10, USER_DEFINED_FIELD_10);
        columnNameToDescriptionMap.put(COLUMN_INVOICE_DETAIL, INVOICE_DETAIL);
        columnNameToDescriptionMap.put(COLUMN_FX_CONTRACT_NUMBER, DESC_FX_CONTRACT_NUMBER);
        columnNameToDescriptionMap.put(COLUMN_FX_CONTRACT_RATE, DESC_FX_CONTRACT_RATE);
    }

    PaymentFileFieldFormat(String columnName, String name, String desc, String lineType, int order, boolean isFieldRequired, boolean isDataRequired, int size, boolean isAttribute) {
        this.columnName = columnName;
        this.name = name;
        this.description = desc;
        this.lineType = lineType;
        this.order = order;
        this.isFieldRequired = isFieldRequired;
        this.isDataRequired = isDataRequired;
        this.size = size;
        this.isAttribute = isAttribute;
    }

    PaymentFileFieldFormat(PaymentDefinitions paymentDefinition, String theColumnName, int order, String lineType)
            throws AmsException, RemoteException {
        boolean isFieldRequired;
        boolean isFieldDataRequired;
        int fieldSize;
        String fileFormatType = paymentDefinition.getAttribute("file_format_type");
        this.columnName = theColumnName;

        PaymentFileFieldFormat hardCodedField = hardCodedFields.get(columnName);
        if (hardCodedField != null) {
            this.name = hardCodedField.name;
            this.description = hardCodedField.description;
            this.lineType = lineType;
            this.order = order;
            this.isFieldRequired = hardCodedField.isFieldRequired;
            this.isDataRequired = hardCodedField.isDataRequired;
            this.size = hardCodedField.size;
            this.isAttribute = hardCodedField.isAttribute;
            return;
        }

        if (COLUMN_PAYMENT_METHOD_TYPE.equals(columnName)) {
            isFieldRequired = true;
            isFieldDataRequired = true;
            if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)) {
                fieldSize = 4;
            } else {
                fieldSize = paymentDefinition.getAttributeInteger(columnName + "_size");
            }
        } else if (COLUMN_DEBIT_ACCOUNT_NO.equals(columnName)) {
            isFieldRequired = true;
            isFieldDataRequired = true;
            if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)) {
                fieldSize = 30;
            } else {
                fieldSize = paymentDefinition.getAttributeInteger(columnName + "_size");
            }
        } else if (COLUMN_PAYEE_NAME.equals(columnName)) {
            isFieldRequired = true;
            isFieldDataRequired = true;
            if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)) {
                fieldSize = 140;
            } else {
                fieldSize = paymentDefinition.getAttributeInteger(columnName + "_size");
            }
        }  else if (COLUMN_PAYMENT_AMOUNT.equals(columnName)) {
            isFieldRequired = true;
            isFieldDataRequired = true;
            if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)) {
                fieldSize = 19;
            } else {
                fieldSize = paymentDefinition.getAttributeInteger(columnName + "_size");
            }
        } else if (TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)
                && (COLUMN_HEADER_IDENTIFIER.equals(columnName)
                || COLUMN_PAYMENT_IDENTIFIER.equals(columnName)
                || COLUMN_INVOICE_IDENTIFIER.equals(columnName))) {
            isFieldRequired = true;
            isFieldDataRequired = true;
            fieldSize = 3;
        } else if (TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)
                && PAYEE_INVOICE_DETAILS.equals(columnName)) {
            isFieldRequired = "Y".equals(paymentDefinition.getAttribute(COLUMN_INVOICE_DETAIL + "_req"));
            isFieldDataRequired = "Y".equals(paymentDefinition.getAttribute(COLUMN_INVOICE_DETAIL + "_data_req"));
            fieldSize = 80;
        } else {
            isFieldRequired = "Y".equals(paymentDefinition.getAttribute(columnName + "_req"));
            isFieldDataRequired = "Y".equals(paymentDefinition.getAttribute(columnName + "_data_req"));
            fieldSize = paymentDefinition.getAttributeInteger(columnName + "_size");
        }
        String fieldName = columnNameToFieldNameMap.get(columnName);
        if (fieldName != null) {
            this.name = fieldName;
            this.isAttribute = true;
        } else {
            this.name = columnName;
            this.isAttribute = false;
        }
        String desc = columnNameToDescriptionMap.get(columnName);
        if (TradePortalConstants.PAYMENT_FILE_TYPE_PNG.equals(fileFormatType)) {
            if (COLUMN_PAYEE_BANK_CODE.equals(columnName)) {
                desc = "BSB of the account to be credited";
            }
            if (COLUMN_EXECUTION_DATE.equals(columnName)) {
                desc = "Date to be Processed";
            }
        }
        if (desc != null) {
            this.description = desc;
        } else {
            this.description = columnName;
        }
        this.lineType = lineType;
        this.order = order;
        this.isFieldRequired = isFieldRequired;
        this.isDataRequired = isFieldDataRequired;
        this.size = fieldSize;

    }
}
