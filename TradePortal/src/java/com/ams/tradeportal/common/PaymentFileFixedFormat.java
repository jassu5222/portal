/**
 *     Copyright  © 2013
 *     AGI, Incorporated
 *     All rights reserved
 *
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.amsinc.ecsg.frame.AmsException;
import com.ams.tradeportal.busobj.PaymentDefinitions;

import static com.ams.tradeportal.common.PaymentFileFormat.logError;

import com.amsinc.ecsg.util.StringFunction;

import java.util.List;

import javax.ejb.RemoveException;

public class PaymentFileFixedFormat extends PaymentFileFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileFixedFormat.class);

       
    public PaymentFileFixedFormat(PaymentDefinitions paymentDefinition) throws RemoteException, AmsException {
        super(paymentDefinition);

        headerFieldsArray = new ArrayList<>(TradePortalConstants.PMT_HDR_SUMMARY_NUMBER_OF_FIELDS);
        paymentFieldsArray = new ArrayList<>(TradePortalConstants.PMT_PAY_SUMMARY_NUMBER_OF_FIELDS);
        invoiceFieldsArray = new ArrayList<>(TradePortalConstants.PMT_INV_SUMMARY_NUMBER_OF_FIELDS);

        for (int i = 0; i < 13; i++) {
            String columnName = paymentDefinition.getAttribute("pmt_hdr_summary_order" + (i + 1));
            if (columnName != null && columnName.length() > 0) {
                PaymentFileFieldFormat field = new PaymentFileFieldFormat(paymentDefinition, columnName, i, "H");
                headerFieldsArray.add(field);
                fieldNameToFieldMap.put(field.name, field);
            }
        }
        for (int i = 0; i < 54; i++) {
            String columnName = paymentDefinition.getAttribute("pmt_pay_summary_order" + (i + 1));
            if (columnName != null && columnName.length() > 0) {
                PaymentFileFieldFormat field = new PaymentFileFieldFormat(paymentDefinition, columnName, i, "P");
                paymentFieldsArray.add(field);
                fieldNameToFieldMap.put(field.name, field);
            }
        }

        // If there is invoice section, add a field to payment fields, which is a concatenation of the invoice lines.
        if (PaymentFileFieldFormat.COLUMN_INVOICE_DETAIL.equals(paymentDefinition.getAttribute("pmt_inv_summary_order2"))) {
            PaymentFileFieldFormat field = new PaymentFileFieldFormat(paymentDefinition, PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS, paymentFieldsArray.size(), "P");
            field.description = "Invoice Detail Line";
            paymentFieldsArray.add(field);
            fieldNameToFieldMap.put(field.name, field);
        }

        PaymentFileFieldFormat[] dummy = new PaymentFileFieldFormat[0];
        this.headerFields = headerFieldsArray.toArray(dummy);
        this.paymentFields = paymentFieldsArray.toArray(dummy);
        this.invoiceFields = invoiceFieldsArray.toArray(dummy);

        HEADER_SECTION_IDENTIFIER = "HDR";
        PAYMENT_SECTION_IDENTIFIER = "PAY";
        INVOICE_SECTION_IDENTIFIER = "INV";
        TRAILER_SECTION_IDENTIFIER = null;

        HEADER_SECTION_DESCRIPTION = "Header";
        PAYMENT_SECTION_DESCRIPTION = "Payment Detail";
        TRAILER_SECTION_DESCRIPTION = "Trailer";

    }

    @Override
    protected String readHeaderLine(PaymentFileProcessor proc, boolean[] errorsFound) throws java.io.IOException, AmsException, RemoveException {
        String sLine = readLine(proc.reader, proc.writer);
        proc.lineNumberForLogging++;
        proc.lineNumber++;
        String headerIdentifier = "";
        if (sLine.length() >= HEADER_SECTION_IDENTIFIER.length()) {
            headerIdentifier = sLine.substring(0, HEADER_SECTION_IDENTIFIER.length());
        } else {
            headerIdentifier = sLine;
        }
        // If the header identifer contains non-ascii, the format must be wrong.
        if (!headerIdentifier.matches("\\A\\p{ASCII}*\\z")) {
            logError(proc.lineNumberForLogging, proc.beneName, "File must be in either a Fixed File Format, or the ABA or PNG File format or a Delimited (by Pipe, Comma, Semi-Colon, Tab) Format. Please correct your file format and upload again.", ERROR_SUB, proc);
            errorsFound[0] = true;
            return null;
        }
        if (!HEADER_SECTION_IDENTIFIER.equals(headerIdentifier)) {
            logError(proc.lineNumberForLogging, HEADER_SECTION_DESCRIPTION, HEADER_SECTION_DESCRIPTION + " must start with " + HEADER_SECTION_IDENTIFIER + ".", ERROR_SUB, proc);
            errorsFound[0] = true;
            return null;
        }
        return sLine;
    }

    @Override
    protected String[] readPaymentLine(String[] readAheadLine, boolean[] errorsFound, PaymentFileProcessor proc) throws java.io.IOException, AmsException, RemoveException {
        // Fixed format may have invoice lines following the payment line.  These invoice lines
        // constitues the payee_invoice_details of the payment.  They are processed as part of the payment line.
        errorsFound[0] = false;
        ArrayList<String> payLines = new ArrayList<String>();
        payLines.add(readAheadLine[0]);
        if (!readAheadLine[0].startsWith(PAYMENT_SECTION_IDENTIFIER)) {
            logError(proc.lineNumberForLogging, PAYMENT_SECTION_DESCRIPTION, PAYMENT_SECTION_DESCRIPTION + " must start with " + PAYMENT_SECTION_IDENTIFIER, ERROR_SUB, proc);
            readAheadLine[0] = readLine(proc.reader, proc.writer);
            proc.lineNumber++;
            errorsFound[0] = true;
            return null;
        }
        readAheadLine[0] = readLine(proc.reader, proc.writer);
        proc.lineNumber++;
        int invoiceNumber = 0;
        while (readAheadLine[0] != null && INVOICE_SECTION_IDENTIFIER != null && readAheadLine[0].startsWith(INVOICE_SECTION_IDENTIFIER)) {
            if (readAheadLine[0] != null && readAheadLine[0].length() > 83) {
                logError(proc.lineNumber, "Invoice", "Invoice detail line '" + proc.lineNumber + "' has " + readAheadLine[0].length() + " characters, exceeding the size limit of 80 characters", ERROR_SUB, proc);
                errorsFound[0] = true;
            }
            invoiceNumber++;
            payLines.add(readAheadLine[0]);
            readAheadLine[0] = readLine(proc.reader, proc.writer);
            proc.lineNumber++; //the invoices are processed with payment line.  Any errors are logged with the payment line number.
        }
        if (invoiceNumber > 1000) {
            logError(proc.lineNumber, "Invoice", "Maximum number of invoice details lines for one beneficiary (1000) has been exceeded.", ERROR_SUB, proc);
            errorsFound[0] = true;
            return null;
        }
        String[] dummy = new String[0];
        return payLines.toArray(dummy);
    }

    @Override
    public String[] parseHeaderLine(String headerLine, PaymentFileProcessor proc) throws AmsException, RemoteException {
        String[] headerRecord = new String[headerFields.length];

        int lineIndex = 0;
        for (int i = 0; i < headerFields.length; i++) {
            int fieldLength = headerFields[i].size;
            int endIndex = lineIndex + fieldLength;
            if (lineIndex > headerLine.length()) {
                    	//Rel9.0 IR#T36000030382
                //headerRecord[i] = null;
                headerRecord[i] = "";

            } else if (endIndex <= headerLine.length()) {
                if (TradePortalConstants.PAYMENT_FILE_TYPE_PNG.equals(formatType) & headerFields[i].name.contains("reserved")) {
                    headerRecord[i] = headerLine.substring(lineIndex, endIndex);
                } else {
                    headerRecord[i] = headerLine.substring(lineIndex, endIndex).trim();
                }
            } else if (endIndex > headerLine.length()) {
                if (TradePortalConstants.PAYMENT_FILE_TYPE_PNG.equals(formatType) & headerFields[i].name.contains("reserved")) {
                    headerRecord[i] = headerLine.substring(lineIndex);
                } else {
                    headerRecord[i] = headerLine.substring(lineIndex).trim();
                }
            }
            lineIndex = endIndex;
        }

        return headerRecord;
    }

    /**
     * Parse one payment line item.
     *
     * @param currentPaymentLineItem
     * @param resourceManager
     * @param mediatorServices
     * @return
     * @throws AmsException
     */
    @Override
    public String[] parsePaymentLine(String[] headerRecord, String[] paymentLines, PaymentFileProcessor proc)
            throws AmsException, RemoteException {

        // First apply the header rows to the payment record.
        String[] paymentRecords = new String[paymentFields.length + 1];

        // Now parse the first line, it should always begin with 'PAY'.  There can only be one 'PAY'
        String paymentLine = paymentLines[0];
        int lineIndex = 0; // Ignore prefix "PAY"
        int numberOfPaymentFields = paymentFields.length;
        boolean hasInvoices = false;
        if (paymentFields.length > 0 && paymentFields[paymentFields.length - 1].name.equals(PaymentFileFieldFormat.PAYEE_INVOICE_DETAILS)) {
            hasInvoices = true;
            numberOfPaymentFields--;
        }
        for (int i = 0; i < numberOfPaymentFields; i++) {
            int fieldLength = paymentFields[i].size;
            int endIndex = lineIndex + fieldLength;
            if (lineIndex > paymentLine.length()) {
                paymentRecords[i] = null;
            } else if (endIndex <= paymentLine.length()) {
                if (paymentFields[i].columnName.contains("reserved")) {
                    paymentRecords[i] = paymentLine.substring(lineIndex, endIndex);
                } else {
                    paymentRecords[i] = paymentLine.substring(lineIndex, endIndex).trim();
                }
            } else if (endIndex > paymentLine.length()) {
                if (paymentFields[i].columnName.contains("reserved")) {
                    paymentRecords[i] = paymentLine.substring(lineIndex);
                } else {
                    paymentRecords[i] = paymentLine.substring(lineIndex).trim();
                }
            }
            lineIndex = endIndex;
        }

        // Now join all the invoice details lines together and apply them to the payment record.  If they exist
        if (hasInvoices) {
            StringBuilder invoiceDetails = new StringBuilder();
            for (int i = 1; i < paymentLines.length; i++) {
                invoiceDetails.append(paymentLines[i].substring(3)).append("\n");
            }
            paymentRecords[paymentFields.length - 1] = invoiceDetails.toString();
        }

        return paymentRecords;
    }

    @Override
    public String[] parseTrailerLine(String trailerLine) {
        return null;
    }

    @Override
    protected List getMandatoryFields(String paymentMethod) {
        List returnArrayList = super.getMandatoryFields(paymentMethod);
        if (StringFunction.isNotBlank(paymentMethod)) {
            returnArrayList.addAll(PaymentFileFieldFormat.allReqFieldsFF);
            if (TradePortalConstants.PAYMENT_METHOD_BCHK.equals(paymentMethod) || TradePortalConstants.PAYMENT_METHOD_CCHK.equals(paymentMethod)) {
                returnArrayList.addAll(PaymentFileFieldFormat.chkReqFields);
            }

        }
        return returnArrayList;

    }

}
