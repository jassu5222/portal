/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.PaymentDefinitions;

import static com.ams.tradeportal.common.PaymentFileFormat.ERROR_SUB;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.StringFunction;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.RemoveException;

/**
 *

 */
public class PaymentFilePNGFormat extends PaymentFileABAFormat {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFilePNGFormat.class);

    static {
        validTransCode = new ArrayList();
        validTransCode.add("13");
        validTransCode.add("53");
    }

    public PaymentFilePNGFormat(PaymentDefinitions paymentDefinition) throws RemoteException, AmsException {
        super(paymentDefinition);
        PaymentFileFieldFormat totalItemCountField = this.fieldNameToFieldMap.get("br_total_item_count");
        totalItemCountField.size = 8;
        PaymentFileFieldFormat reserved4ield = this.fieldNameToFieldMap.get("br_reserved4");
        reserved4ield.size = 50;
        PaymentFileFieldFormat field = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO);
        field.description = "Lodgement Reference Account Number";
        field = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.FD_RESERVED5);
        field.description = "Reserved 3";

    }

    @Override
    protected boolean validateHeaderLine(String[] headerRecords, PaymentFileProcessor proc) throws AmsException, RemoteException, RemoveException {
        boolean superReturn = super.validateHeaderLine(headerRecords, proc);
        boolean errorsFound = false;
        PaymentFileFieldFormat reserved4Field = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.FD_RESERVED4);
        String fdReserved4 = getValue(PaymentFileFieldFormat.FD_RESERVED4, headerRecords, null, null);
        if (!StringFunction.isBlank(fdReserved4) | ((fdReserved4 != null && fdReserved4.length() != reserved4Field.size) | fdReserved4 == null)) {
            logError(proc.lineNumberForLogging, proc.beneName, "A 'Reserved' segment in the header is missing or is incomplete.", ERROR_SUB, proc);
            errorsFound = true;
        }

        PaymentFileFieldFormat reserved2Field = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.FD_RESERVED2);
        String fdReserved2 = getValue(PaymentFileFieldFormat.FD_RESERVED2, headerRecords, null, null);
        if (!StringFunction.isBlank(fdReserved2) | ((fdReserved2 != null && fdReserved2.length() != reserved2Field.size) | fdReserved2 == null)) {
            logError(proc.lineNumberForLogging, proc.beneName, "A 'Reserved' segment in the header is missing or is incomplete.", ERROR_SUB, proc);
            errorsFound = true;
        }

        PaymentFileFieldFormat reserved5Field = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.FD_RESERVED5);
        String fdReserved5 = getValue(PaymentFileFieldFormat.FD_RESERVED5, headerRecords, null, null);
        if (!StringFunction.isBlank(fdReserved5) | ((fdReserved5 != null && fdReserved5.length() != reserved5Field.size) | fdReserved5 == null)) {
            logError(proc.lineNumberForLogging, proc.beneName, "A 'Reserved' segment in the header is missing or is incomplete.", ERROR_SUB, proc);
            errorsFound = true;
        }

        return superReturn && !errorsFound;
    }

    @Override
    protected boolean validatePaymentLine(String[] paymentRecords, PaymentFileProcessor proc)
            throws AmsException, RemoteException, RemoveException {
        boolean superReturn = super.validatePaymentLine(paymentRecords, proc);
        boolean errorsFound = false;

        String accountDetails = getValue(PaymentFileFieldFormat.DR_ACCOUNT_DETAILS, null, paymentRecords, null);
        if (StringFunction.isNotBlank(accountDetails) && !ALPHANUMERIC.matcher(accountDetails).matches()) {
            logError(proc.lineNumberForLogging, proc.beneName, "Invalid value in the New/Varied BSB/Account Details field.", ERROR_SUB, proc);
            errorsFound = true;
        }

        String value = getValue(PaymentFileFieldFormat.DR_RESERVED1, null, paymentRecords, null);
        if (!"        ".equals(value)) {// check for 8 spaces
            logError(proc.lineNumberForLogging, proc.beneName, "A 'Reserved' segment in the Details section is missing or is incomplete.", ERROR_SUB, proc);
            errorsFound = true;
        }
        value = getValue(PaymentFileFieldFormat.DR_ACCOUNT_TYPE, null, paymentRecords, null);
        if (StringFunction.isNotBlank(value) && !"000".equals(value)) {// check for 8 spaces
            logError(proc.lineNumberForLogging, proc.beneName, "Account Type '" + value + "' is invalid � it should be 000", ERROR_SUB, proc);
            errorsFound = true;
        }

        return superReturn && !errorsFound;
    }

    @Override
    protected boolean validateLineForFatalError(String[] dataRecords, PaymentFileFieldFormat[] definitionFields, String lineType, PaymentFileProcessor proc) throws RemoveException, AmsException, RemoteException {
        boolean superReturn = super.validateLineForFatalError(dataRecords, definitionFields, lineType, proc);
        boolean errorsFound = false;
        if (lineType.equals(PaymentFileFieldFormat.LINE_TYPE_PAYMENT)) {
            PaymentFileFieldFormat debitAccountField = this.fieldNameToFieldMap.get(PaymentFileFieldFormat.DEBIT_ACCOUNT_NO);
            String currency = null;
            String debitAccountNo = dataRecords[debitAccountField.order];
            String filePaymentMethod = this.paymentMethod;
            if (!validatePaymentCurrencyCountry(proc, filePaymentMethod, currency, debitAccountNo)) {
                errorsFound = true;
            }
        }
        return !errorsFound;
    }
}
