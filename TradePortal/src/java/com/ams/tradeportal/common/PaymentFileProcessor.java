package com.ams.tradeportal.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import javax.ejb.EJBObject;
import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DmstPmtPanelAuthRange;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.IncomingInterfaceQueue;
import com.ams.tradeportal.busobj.IncomingQueueHistory;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceDetails;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.busobj.PaymentParty;
import com.ams.tradeportal.busobj.SecurityProfile;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.Panel;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.PanelRange;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.mediator.AuthorizeTransactionsMediator;
import com.ams.tradeportal.mediator.CreateTransactionMediator;
import com.ams.tradeportal.mediator.TransactionMediator;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;


public class PaymentFileProcessor {
private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(PaymentFileProcessor.class);

    //provide a upper limit for duration seconds to avoid failing the entire record if calculations are out of bounds	

    private static long MAX_DURATION_SECONDS = 9999999999L;

    final String ejbServerLocation;
    final ClientServerDataBridge csdb;
    private final long paymentFileOid;

    private String process_id = null;
    String creditAccountBankCountryCode = null;
    String debitAccountBankCountryCode = null;
    String debitAccountBankGroupOid = null;
    BigDecimal totAmt = BigDecimal.ZERO;
    // manohar - CR 597 start
    private BigDecimal unsuccessfulTotAmt = BigDecimal.ZERO; // manohar - CR 597 end
    private File paymentFile = null;
    private String fileName = null;
    private Map<String, Map<String, String>> uploadLog = new HashMap<>();
    private Map<String, Map<String, String>> uploadRepLog = new HashMap<>();
    String paymentFileDefFormat = null;

    Transaction trans;
    private Terms terms;
    Hashtable<String, String> pmValueDates = new Hashtable<>();
    private Date valStartTime = null;
    DocumentHandler inputDoc;
    int lineNumberForLogging = 0;
    int lineNumber = 0;
    int paymentCount = 0;
    int successCount = 0;
    DomesticPayment domesticPayment = null;
    InvoiceDetails invoiceDetails = null;
    String beneName = null;
    String currency = null;

    public PaymentFileUpload paymentUpload = null;
    private ServerDeploy serverDeploy = null;
    CorporateOrganization corpOrg = null;
    private String securityRights = null;

    private final String folder = TradePortalConstants.getPropertyValue("AgentConfiguration", "uploadFolder", "c:/temp/");
    protected static final String loggerCategory = TradePortalConstants.getPropertyValue("AgentConfiguration", "loggerServerName", "NoLogCategory");

    protected boolean _isInvoiceFile = false;

    Panel panel = null;
    private Map<String, String> dmstPymtPanelRangeMap = new HashMap<>();
    private String baseCurrency = null;
    private String transferCurrency = null;
    private String fromCurrency = null;
    private boolean isAccountCurrecny = false;
    private ErrorManager transErrorMgr;
    BigDecimal buyRateToConvertFromTransCurr;
    BigDecimal sellRateToConvertFromTransCurr;
    String multiIndToConvertFromTransCurr = null;
    BigDecimal buyRateToConvertFromAcctCurr;
    BigDecimal sellRateToConvertFromAcctCurr;
    String multiIndToConvertFromAcctCurr = null;
    BufferedReader reader = null;
    BufferedWriter writer = null;
    ErrorManager errorMgr = null;
    String userOid = null;
    String ownerOrgOid = null;
    String isAdmin = null;

    public PaymentFileProcessor(String process_id, long oidLong, String ejbServerLocation, ClientServerDataBridge csdb) {
        this.ejbServerLocation = ejbServerLocation;
        this.csdb = csdb;
        this.paymentFileOid = oidLong;
        this.process_id = process_id;
        try{
        	serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(ejbServerLocation, "ServerDeploy");
        }catch (Exception e){
        	  logError("Problem obtaining serverDeploy.", e);
        }
    }

    /**
     * Read the file content from user input stream and save to temporary file.
     * Do file-level validation.
     *
     * @param reader
     * @param writer
     * @param paymentFileDefinitionOid
     * @return
     * @throws RemoteException
     * @throws AmsException
     * @throws IOException
     * @throws RemoveException
     */
    public boolean uploadPaymentFile(BufferedReader reader, BufferedWriter writer, long paymentFileDefinitionOid, ErrorManager errMgr, SessionWebBean userSession)
            throws AmsException, IOException, RemoveException {
        this.reader = reader;
        this.writer = writer;
        this.errorMgr = errMgr;
        this.userOid = userSession.getUserOid();
        this.ownerOrgOid = userSession.getOwnerOrgOid();
        this.isAdmin = getIsAdminUser(this.userOid);
        PaymentFileFormat paymentFileFormat = PaymentFileFormat.getPaymentFileFormat(paymentFileDefinitionOid, this);
        return paymentFileFormat.uploadPaymentFile(this);

    }

    /**
     * Processes uploaded Payment file saved in temporary folder.
     *
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws RemoveException
     * @throws IOException
     */
    public boolean processPaymentFile() throws AmsException, IOException {

        try {
            paymentUpload = (PaymentFileUpload) EJBObjectFactory.createClientEJB(ejbServerLocation, "PaymentFileUpload", paymentFileOid);
            //capture the pack end date/time
            String datetime_process_start = "";
            try {
                datetime_process_start = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)); 
//                		DateTimeUtility.getGMTDateTime(false, false);
            } catch (AmsException e) {
                //log and ignore - non-critical processing
                logError("Problem obtaining process start date time. Ignoring.", e);
            }
            try {
                paymentUpload.setAttribute("datetime_process_start", datetime_process_start);
            } catch (AmsException ex) {
                //log and ignore - non-critical processing
                logError("Problem saving pack start date time. Ignoring.", ex);
            }

            fileName = paymentUpload.getAttribute("payment_file_name");

            paymentUpload.setClientServerDataBridge(csdb);
            inputDoc = getInputDoc();
            reader = getInputReader();

                        // W Zhu 1/9/2014 Rel 8.4 CR-797 begin
            // Process upload with customer payment file definition.
            String paymentFileDefinitionOid = paymentUpload.getAttribute("payment_definition_oid");
            if (StringFunction.isNotBlank(paymentFileDefinitionOid)) {
                this.userOid = paymentUpload.getAttribute("user_oid");
                this.ownerOrgOid = paymentUpload.getAttribute("owner_org_oid");
                this.isAdmin = getIsAdminUser(this.userOid);
                PaymentFileFormat paymentFileFormat = PaymentFileFormat.getPaymentFileFormat(Long.valueOf(paymentFileDefinitionOid), this);
                //MEer CR-934b-save payment method type at transaction level.
                boolean processPaymentFile = paymentFileFormat.processPaymentFile(this);
                if (processPaymentFile) {
                    List<Object> sqlParams = new ArrayList<>();
                    sqlParams.add(trans.getAttribute("transaction_oid"));

                    int numberOfDomPmts = DatabaseQueryBean.getCount("domestic_payment_oid", "domestic_payment", " p_transaction_oid =? ", false, sqlParams);

                    if (numberOfDomPmts > 0) {
                        String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?  order by DP.DOMESTIC_PAYMENT_OID";
                        DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{trans.getAttribute("transaction_oid")});
                        String firstBenePaymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(0)/PAYMENT_METHOD_TYPE");

                        if (StringFunction.isNotBlank(firstBenePaymentMethodType)) {
                            trans.setAttribute("payment_method_type", firstBenePaymentMethodType);
                        }
                    }
                }
                return processPaymentFile;
            }
                        // W Zhu 1/9/2014 Rel 8.4 CR-797 end

            init();

            // Nar CR857 Begin
            if (corpOrg.isPanelAuthEnabled(InstrumentType.DOM_PMT)) {
                validatePanelSetupData();
            }
            //Nar CR857 End

            String sLine = reader.readLine();
            // If the first line starts with "HDR" then it is the invoice file format.            
            if(sLine!=null){
            	_isInvoiceFile = sLine.startsWith("HDR");	
            }            
            String[] headerFields = null;
            // If it's an invoice file then
            if (_isInvoiceFile) {
                headerFields = PaymentFileWithInvoiceDetails.parseHeaderLine(sLine);
				// The customer_file_ref is retrieved from the header.  Since the paymentUpload and trans are already created
                // the customer_file_ref on these objects must be populated now.
                paymentUpload.setAttribute("customer_file_ref", headerFields[PaymentFileWithInvoiceDetails.FILE_REFERENCE]);
                trans.setAttribute("customer_file_ref", headerFields[PaymentFileWithInvoiceDetails.FILE_REFERENCE]);
                sLine = reader.readLine();
            }

            while (sLine != null) {
                if (!StringFunction.isBlank(sLine)) {
                    if (_isInvoiceFile) { // For invoice files must join PAY records with INV records
                        String[] payLines = new String[1001]; // Maximum Number of lines (1 PAY plus 1000 INV)
                        if (!sLine.startsWith("PAY")) {
                            throw new AmsException("Invalid file format"); // Should never happen if initial validation working
                        }
                        int lineIndex = 0;
                        payLines[lineIndex] = sLine;
                        sLine = reader.readLine();
                        while (sLine != null && sLine.startsWith("INV")) {
                            lineIndex++;
                            payLines[lineIndex] = sLine;
                            sLine = reader.readLine();
                        }
                        processPaymentWithInvoiceLines(inputDoc, headerFields, payLines);
                    } else {
                        processPaymentLine(inputDoc, sLine);
                        sLine = reader.readLine();
                    }
                    paymentCount++;
                } else {
                    sLine = reader.readLine();
                }
            }

            // CR 857 Begin - set the awaiting approvals for beneficiaries of transaction.
            if (!dmstPymtPanelRangeMap.isEmpty()) {
                String panelApprovalsList = PanelAuthUtility.getPanelAuthAwaitingApprovalForBene(dmstPymtPanelRangeMap, trans, InstrumentType.DOM_PMT, transErrorMgr);
                trans.setAttribute("panel_auth_next_approvals", panelApprovalsList);
            }
			// CR 857 End

            postProcess();
            saveProcessingTimeData();
            if (paymentUpload.save() < 0) {
                logDebug("InvoiceFileProcessor.processInvoiceFile::Problem saving Invoice File Upload ");
            }

        } catch (Exception e) {
            logError("Error occured processing payment file...", e);
            e.printStackTrace();
            rejectFileUplaod(false);
            return false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            cleanup();
        }

        return true;
    }

    void saveProcessingTimeData() throws RemoteException, AmsException {
        //include values for processing slas
        String datetime_process_end = "";
        try {
            datetime_process_end = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)); 
//            		DateTimeUtility.getGMTDateTime(false, false);
        } catch (AmsException e) {
            //log and ignore - non-critical processing
            logError("Problem obtaining process start date time. Ignoring.", e);
        }
        String dateCreated = paymentUpload.getAttribute("creation_timestamp");
        String packStart = paymentUpload.getAttribute("datetime_process_start");
        try {
            long processSeconds = TPDateTimeUtility.calculateDurationSeconds(packStart, datetime_process_end);
            if (processSeconds >= 0) {
                if (processSeconds <= MAX_DURATION_SECONDS) {
                    String secondsStr = "" + processSeconds;
                    paymentUpload.setAttribute("process_seconds", secondsStr);
                } else {
                    //avoid failure if duration is out of bounds
                    logDebug("InvoiceFileProcessor.processInvoiceFile::process duration out of bounds. Ignoring.");
                }
            } else {
                //avoid failure if duration is negative
                logDebug("InvoiceFileProcessor.processInvoiceFile::negative porcess duration. Ignoring.");
            }
        } catch (RemoteException | AmsException t) { //i.e. illegal argument exception
            //just write out to the log (i.e. debug)
            logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate process seconds duration."
                    + "Ignoring. Exception:" + t.toString());
        }
        try {
            long totalProcessSeconds = TPDateTimeUtility.calculateDurationSeconds(dateCreated, datetime_process_end);
            if (totalProcessSeconds >= 0) {
                if (totalProcessSeconds <= MAX_DURATION_SECONDS) {
                    String secondsStr = "" + totalProcessSeconds;
                    paymentUpload.setAttribute("total_process_seconds", secondsStr);
                } else {
                    //avoid failure if duration is out of bounds
                    logDebug("InvoiceFileProcessor.processInvoiceFile::total processing duration out of bounds. Ignoring.");
                }
            } else {
                //avoid failure if duration is negative
                logDebug("InvoiceFileProcessor.processInvoiceFile::negative total processing duration. Ignoring.");
            }
        } catch (RemoteException | AmsException t) { //i.e. illegal argument exception
            //just write out to the log (i.e. debug)
            logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate total process seconds duration."
                    + "Ignoring. Exception:" + t.toString());
        }
        if (paymentUpload.save() < 0) {
            throw new AmsException("error saving PaymentFileUpload ....");
        }
    }
    private String getIsAdminUser(String userOID) {
        try {
            User user = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", Long.parseLong(userOID));
            if (!TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))) {
                return TradePortalConstants.ADMIN;
            }
        } catch (NumberFormatException | RemoteException | AmsException e) {
            e.printStackTrace();
        }

        return TradePortalConstants.NON_ADMIN;
    }

    protected void processPaymentWithInvoiceLines(DocumentHandler inputDoc, String[] headerFields, String[] paymentLines) throws AmsException, RemoteException, RemoveException {

        // manohar - CR 597 start
        String[] paymentRecord = PaymentFileWithInvoiceDetails.parsePaymentRecord(headerFields, paymentLines);

        DomesticPayment dp = createDP(trans, paymentRecord, paymentCount, paymentUpload.getAttribute("owner_org_oid"), paymentUpload.getAttribute("payment_file_upload_oid"));
        ErrorManager dpErrorMgr = dp.getErrorManager();
        //BSL IR#PUL032965444 04/04/11 - InvoiceDetails record should only be created if invoice details are non-blank
        InvoiceDetails invoiceDetail = null;
        if (StringFunction.isNotBlank(paymentRecord[PaymentFileWithInvoiceDetails.INVOICE_DETAIL])) {
            invoiceDetail = (InvoiceDetails) EJBObjectFactory.createClientEJB(ejbServerLocation, "InvoiceDetails", csdb);
            invoiceDetail.newObject();
            invoiceDetail.setAttribute("domestic_payment_oid", dp.getAttribute("domestic_payment_oid"));
            invoiceDetail.setAttribute("payee_invoice_details", paymentRecord[PaymentFileWithInvoiceDetails.INVOICE_DETAIL]);           
        }

        if (paymentCount == 0) {
            validateHeaders(trans, paymentRecord, paymentUpload.getAttribute("owner_org_oid"));

            //Rel9.0 IR#T36000023751 -If it is a H2H and is STA, skip panel check
            if (!(trans.isH2HSentPayment() && corpOrg.isStraightThroughAuthorize())) {
                // CR857 Begin			
                if (TradePortalConstants.INDICATOR_YES.equals(trans.getAttribute("bene_panel_auth_ind")) && panel == null) {
                    setTransactionPanelData(dp);
                }
                // CR857 End
            }//End IR#T36000023751
            // this method verify whether fx rate is present for base currency and also store multiply indicator and buy rate of
            // account and transaction currency.
            verifyAndSetRateDefineForFXGroup();

            if (trans.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY
                    || transErrorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
                handleError(trans);
                throw new AmsException("Error occured validating header info....");
            }
            debitAccountBankCountryCode = PaymentFileWithInvoiceDetails.getBankBranchCountryForAccount(terms.getAttribute("debit_account_oid"));
            debitAccountBankGroupOid = getBankGroupForAccount(dp.getErrorManager());//CR-1001 Rel9.4 Fetch the BankGroup of the Debit account only once for validating reporting codes
            inputDoc.setAttribute("/Terms/debit_account_oid", terms.getAttribute("debit_account_oid")); // Manohar - 1/4/2011 -added to validate the reporting codes
            inputDoc.setAttribute("/Terms/amount_currency_code/", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY]);
            inputDoc.setAttribute("/Transaction/payment_date", trans.getAttribute("payment_date"));

			// Rel 7.1 - CR-640-581 - manohar - Begin
            Date todaysDate = DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate()); 
//            		GMTUtility.getGMTDateTime();
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");
            String curDateStr = formatter.format(getExecutionDate(paymentRecord));
            String todaysDateStr = formatter.format(todaysDate);
            String fxOnlineInd = PaymentFileWithInvoiceDetails.returnFxOnlineAvailInd(paymentRecord);
            if (null != fxOnlineInd && fxOnlineInd.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES)) {
                try {
                    Date d1 = formatter.parse(todaysDateStr);
                    Date d2 = formatter.parse(curDateStr);

                    if (d1.equals(d2)) {
                        terms.setAttribute("request_market_rate_ind", TradePortalConstants.INDICATOR_YES);
                    }
                } catch (ParseException e) {
                    // Parse error - should never happen
                    e.printStackTrace();
                }

            }	// Rel 7.1 - CR-640-581 - manohar - End
        }

        inputDoc.setAttribute("/Transaction/uploaded_ind", "F"); // manohar IR#SRUL031753965 03/30/11 added
        inputDoc.setAttribute("/OwnerOrg", paymentUpload.getAttribute("owner_org_oid"));

        Vector err = new Vector();
        // CR 857 Begin
        boolean isErrorFoundInPaneProcess = false;
        if (TradePortalConstants.INDICATOR_YES.equals(trans.getAttribute("bene_panel_auth_ind"))) {
            setBeneficiaryPanelData(dp, dpErrorMgr);
            err.addAll(dpErrorMgr.getIssuedErrors());
            if (dpErrorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
                isErrorFoundInPaneProcess = true;
            }
        }

		// CR 857 End
		//TO DO: needs performance tuning...
        inputDoc.setAttribute("/Transaction/uploaded_ind", TradePortalConstants.INDICATOR_YES); // manohar IR#SRUL031753965 03/30/11 added
        dp.verifyDomesticPayment(inputDoc, pmValueDates, creditAccountBankCountryCode, debitAccountBankCountryCode, true, isErrorFoundInPaneProcess);//BSL IR# PBUL042033374 04/29/11 - Add overload
        
        if (paymentCount == 0) {
            pmValueDates.put(paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_METHOD], dp.getAttribute("value_date"));
        }

        err.addAll(dp.getIssuedErrors());
        Map<String, String> tempRepMap = new HashMap<>();
        if (err.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (int index = 0; !(index >= err.size()); index++) {
                com.amsinc.ecsg.frame.IssuedError errorObject = (com.amsinc.ecsg.frame.IssuedError) err.elementAt(index);
                sb.append("- ").append(errorObject.getErrorText());
            }
            String lineNum = paymentCount + 1 + "";
            // manohar - CR 597 start
            if (StringFunction.isNotBlank(paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_AMOUNT])) { //added if condition to check if payment amount is not blank
                unsuccessfulTotAmt = unsuccessfulTotAmt.add((new BigDecimal(paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_AMOUNT])));
            }

            String bene_info = paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_NAME] + ", "
                    + paymentRecord[PaymentFileWithInvoiceDetails.CUSTOMER_REFERENCE] + ", "
                    + paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_AMOUNT];

            Map<String, String> tempMap = new HashMap<>();
            tempMap.put(bene_info, sb.toString());
            uploadLog.put(lineNum, tempMap);

        } else {
            totAmt = totAmt.add((new BigDecimal(paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_AMOUNT])));
            successCount++;
        }

        if (!isErrorFoundInPaneProcess) { //CR857
            dp.save();
            if(err.size() == 0){ //CR-1001 If no beneficiary level errors, then validateReportingCode errors
            	validateReportingCodes(dp);
            }
        }
        //BSL IR#PUL032965444 04/04/11 - InvoiceDetails record should only be created if invoice details are non-blank
        if (invoiceDetail != null) {
            invoiceDetail.save();
            invoiceDetail.remove();
        }
        dp.remove();
    }

    /**
     * Sets Instrument, Transaction and PaymentFileUpload to File Upload
     * Rejected status
     *
     *
     * @param String Error message
     * @throws AmsException
     * @throws RemoveException
     * @throws IOException
     */
    protected void rejectFileUplaod(boolean defaultErrorMsg) throws RemoteException, AmsException {

        if (paymentUpload == null) {
            return;
        }

        paymentUpload.setAttribute("validation_seconds", getDurationInSeconds(valStartTime, DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))));
//        		GMTUtility.getGMTDateTime(false, false)));
        paymentUpload.setAttribute("completion_timestamp", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//        		DateTimeUtility.getGMTDateTime(false, false));
        String errMsg = paymentUpload.getAttribute("error_msg");

        if (!defaultErrorMsg) {
            defaultErrorMsg = (StringFunction.isBlank(errMsg) && uploadLog.size() == 0);
        }

        try {
            paymentUpload.rejectFileUplaod(defaultErrorMsg);
        } catch (AmsException e) {
            logError("Error during rejectFileUpload...", e);
            throw e;

        }
    }

    /**
     * This method processes orphan PaymentFileUploads and set to reject file
     * upload status
     *
     *
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws RemoveException
     * @throws RemoteException
     */
    public boolean processOrphan() throws AmsException, RemoteException {

        logDebug("Processing Orphan PaymentFileUpload...");

        paymentUpload = (PaymentFileUpload) EJBObjectFactory.createClientEJB(ejbServerLocation, "PaymentFileUpload", paymentFileOid);
        fileName = paymentUpload.getAttribute("payment_file_name");
        paymentFile = getPaymentFile();

        paymentUpload.setClientServerDataBridge(csdb);

        rejectFileUplaod(true);
        cleanup();

        return true;
    }

    /**
     * This method creates Instrument, Transaction and sets PaymentFileUpload to
     * Validation in Progress status
     *
     * @throws AmsException
     * @throws RemoteException
     */
    void init() throws RemoteException, AmsException {

        trans = createTransaction(paymentUpload);
        terms = (Terms) trans.getComponentHandle("CustomerEnteredTerms");
        terms.newComponent("FirstTermsParty");

        paymentUpload.setAttribute("instrument_oid", trans.getAttribute("instrument_oid"));
        setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS);
        valStartTime = DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false)); 
//        		GMTUtility.getGMTDateTime(false, false);
    }

    /**
     * This method handles post process
     *
     * @throws AmsException
     * @throws RemoteException
     * @throws RemoveException
     */
    void postProcess() throws RemoteException, AmsException {
        // manohar - CR 597 start
        unsuccessfulTotAmt = unsuccessfulTotAmt.add(totAmt);
        trans.setAttribute("copy_payment_file_amt", unsuccessfulTotAmt.toString());
        // manohar - CR 597 start
        trans.setAttribute("copy_of_amount", totAmt.toString());
        terms.setAttribute("funding_amount", totAmt.toString());
        terms.setAttribute("amount", totAmt.toString());

        paymentUpload.setAttribute("number_of_bene_uploaded", paymentCount + "");
        paymentUpload.setAttribute("number_of_bene_failed", (paymentCount - successCount) + "");

        updateTerms();    // //NSX - PMUL012565883 Re. 6.1.0

        if (trans.save(false) < 0) {
            handleError(trans);
            throw new AmsException("error saving transaction....");

        }

		//cquinton 2/21/2011 Rel 6.1.0 ir#pbul012384451
        //set confidential indicator on instrument from terms
        Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", csdb);
        instrument.getData(trans.getAttributeLong("instrument_oid"));
        String confInd = terms.getAttribute("confidential_indicator");
        instrument.setAttribute("confidential_indicator", confInd);
        if (instrument.save(false) < 0) {
            handleError(instrument);
            throw new AmsException("error saving transaction....");

        }

        if (successCount > 0) {

            DocumentHandler verifyInputDoc = buildDocumentHandlerFromTransaction(instrument);

            TransactionMediator transMediator = (TransactionMediator) EJBObjectFactory.createClientEJB(ejbServerLocation, "TransactionMediator");
            DocumentHandler outputDoc = transMediator.performService(verifyInputDoc, csdb);
            //cquinton 5/16/2011 cleanup ejb
            removeEJBReference(transMediator);
            boolean hasFXContract = true;
            if (TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status"))) {
                hasFXContract = isFXContractPresent(outputDoc);
            }
            if (checkError(outputDoc)) {
				//MDB PKUL110253996 Rel7.1 11/8/11 Begin
                //MDB PRUL112347061 Rel7.1 12/15/11 - Begin
                if ((!TransactionStatus.VERIFIED_PENDING_FX.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status")))
                        && (!TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status")))) {
                    rejectFileUplaod(false);
                } else {
                    if (TransactionStatus.VERIFIED_PENDING_FX.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status"))) {
                        setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL);
                        trans.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
                        if (StringFunction.isNotBlank(outputDoc.getAttribute("/Out/Transaction/authorization_errors"))) {
                            trans.setAttribute("authorization_errors", outputDoc.getAttribute("/Out/Transaction/authorization_errors"));
                        }
                    } else if (TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status"))) {
                        setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS);
                        if (StringFunction.isNotBlank(outputDoc.getAttribute("/Out/Transaction/authorization_errors"))) {
                            trans.setAttribute("authorization_errors", outputDoc.getAttribute("/Out/Transaction/authorization_errors"));
                        }
                        if (paymentUpload.isH2HSentPayment() && paymentUpload.isPartialPayAuthAllowed()) { //Added condition for CR 988 Rel9.2
                            trans.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
                        } else {
                            trans.setAttribute("transaction_status", TransactionStatus.VERIFIED_AWAITING_APPROVAL);
                        }
                        setStatus();
                    }
                    //MDB PRUL112347061 Rel7.1 12/15/11 - End
                    if (trans.save(false) < 0) {
                        handleError(trans);
                        throw new AmsException("error saving transaction....");
                    }
                }
                //MDB PKUL110253996 Rel7.1 11/8/11 End
                createOutgoingMessage(trans);//PMitnala Rel9.1 CR#970 - Create an entry in Outgoing_Queue table
                return;
            } else {
            	//Rel9.4 CR-1001. In case of file upload using Payment file definition (Original fixed width & Delimiter type), if only reporting code errors are present, change the status
            	if (StringFunction.isNotBlank(paymentFileDefFormat) && 
            			(TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(paymentFileDefFormat) || TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(paymentFileDefFormat))){
            		if(successCount == paymentCount && uploadRepLog.size()>0){
            			trans.setAttribute("transaction_status", TransactionStatus.REPORTING_CODES_REQUIRED);
                		if (trans.save(false) < 0) {
                            handleError(trans);
                            throw new AmsException("error saving transaction....");

                        }
            		}
            	}//Else, in case of file upload without definition(i.e, Original pipe delimited format) 
            	else if(StringFunction.isBlank(paymentFileDefFormat)){
	            		//Scenario1: check if only reporting code errors are present
		            	if(uploadLog.size() == 0 && uploadRepLog.size()>0){
		            		trans.setAttribute("transaction_status", TransactionStatus.REPORTING_CODES_REQUIRED);
		            		if (trans.save(false) < 0) {
		                        handleError(trans);
		                        throw new AmsException("error saving transaction....");
		
		                    }
		            	}
	            	
		                //Scenario2: Added condition for CR 988 Rel9.2 - Change the status so that it can be allowed for authorization	- BEGIN
		                if (paymentUpload.isH2HSentPayment() && paymentUpload.isPartialPayAuthAllowed()
		                        && TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(outputDoc.getAttribute("/Out/Transaction/transaction_status")) ) {
		                	//CR-1001 Rel9.4 - If reporting code errors are present, the beneficiary errors get confirmed and moved to ReportingCodes Required status
		                	if(uploadRepLog.size()>0){
		                		trans.setAttribute("transaction_status", TransactionStatus.REPORTING_CODES_REQUIRED);
		                		setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR);
		                	}else{
			                    if (!hasFXContract) {
			                        trans.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
			                    } else if (isCheckerEnabled(outputDoc)) {
			                        trans.setAttribute("transaction_status", TransactionStatus.READY_TO_CHECK);
			                    } else {
			                        trans.setAttribute("transaction_status", TransactionStatus.READY_TO_AUTHORIZE);
			                    }
		                	}
		                    if (trans.save(false) < 0) {
		                        handleError(trans);
		                        throw new AmsException("error saving transaction....");
		
		                    }
		                }
	            	
            	}
            }
            trans.setAttribute("transaction_status", outputDoc.getAttribute("/Out/Transaction/transaction_status"));
			//Added condition for CR 988 Rel9.2 - Change the status so that it can be allowed for authorization	- END

            // CJR CR-593 04/06/2011 BEGIN
            //Added conditions for CR 988 Rel9.2
            if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("straight_through_authorize"))
                    && (TransactionStatus.READY_TO_AUTHORIZE.equals(trans.getAttribute("transaction_status"))
                    || (TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(trans.getAttribute("transaction_status")))
                    && paymentUpload.isPartialPayAuthAllowed())
                    && paymentUpload.isH2HSentPayment() && !isCheckerEnabled(outputDoc) && uploadRepLog.size()==0) {
                // build authorize input document
                DocumentHandler authInputDoc = buildAuthDocument(instrument);
                AuthorizeTransactionsMediator authMediator = (AuthorizeTransactionsMediator) EJBObjectFactory.createClientEJB(ejbServerLocation, "AuthorizeTransactionsMediator");
                DocumentHandler authOutputDoc = authMediator.performService(authInputDoc, csdb);
                //cquinton 5/16/2011 cleanup ejb
                removeEJBReference(authMediator);
                if (checkError(authOutputDoc)) {
                    // Unable to authorize

                }
            }
        }
		// CJR CR-593 04/06/2011 END

        //cquinton 5/16/2011 cleanup ejb
        removeEJBReference(instrument);

        setStatus();

        //moved here so that errors if any are saved before outgoing queue PAYBACK message is saved.
        createOutgoingMessage(trans);//PMitnala Rel9.1 CR#970 - Create an entry in Outgoing_Queue table

    }

    // CJR CR-593 04/06/2011 BEGIN
    private DocumentHandler buildAuthDocument(Instrument instrument) throws RemoteException, AmsException {
        DocumentHandler authDoc = new DocumentHandler();

        authDoc.setAttribute("/In/User/userOid", paymentUpload.getAttribute("user_oid"));
        authDoc.setAttribute("/In/User/securityRights", securityRights);
        StringBuilder transactionData = new StringBuilder();
        transactionData.append(trans.getAttribute("transaction_oid"));
        transactionData.append("/");
        transactionData.append(instrument.getAttribute("instrument_oid"));
        transactionData.append("/");
        transactionData.append(trans.getAttribute("transaction_type_code"));
        authDoc.setAttribute("/In/TransactionList/Transaction/transactionData", transactionData.toString());
        authDoc.setAttribute("/In/Authorization/preAuthorize", TradePortalConstants.INDICATOR_NO);
        return authDoc;
    }
	// CJR CR-593 04/06/2011 END

    /**
     * @throws RemoteException
     * @throws AmsException
     */
    protected void updateTerms() throws RemoteException, AmsException {
        if (successCount > 1) { //if bulk payment

            DomesticPayment domPayment = (DomesticPayment) EJBObjectFactory.createClientEJB(ejbServerLocation, "DomesticPayment");
            DocumentHandler inputDoc = new DocumentHandler();

            inputDoc.setAttribute("/Instrument/instrument_type_code", InstrumentType.DOM_PMT);
            inputDoc = domPayment.processDPTerms(inputDoc, successCount);
            TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
            firstTermsParty.setAttribute("name", inputDoc.getAttribute("/Terms/FirstTermsParty/name"));
            firstTermsParty.setAttribute("contact_name", inputDoc.getAttribute("/Terms/FirstTermsParty/contact_name"));

            firstTermsParty.setAttribute("address_line_1", "");
            firstTermsParty.setAttribute("address_line_2", "");
            firstTermsParty.setAttribute("address_line_3", "");
            firstTermsParty.setAttribute("address_country", "");
            terms.setAttribute("reference_number", inputDoc.getAttribute("/Terms/reference_number"));
        }
    }

    /**
     * Build the xml formatted input document based on data in the Transaction
     * bean. This method really belongs on the TransactionBean, but is part of
     * the agent for the POC.
     *
     * @param trans the instances of the Transaction
     * @param resultDoc the DocumentHandler instance
     * @return resultDoc the DocumentHandler instance
     */
    public DocumentHandler buildDocumentHandlerFromTransaction(Instrument instrument)
            throws AmsException, RemoteException {

        DocumentHandler verifyInputDoc = new DocumentHandler();
        verifyInputDoc.setAttribute("/In/Update/ButtonPressed", TradePortalConstants.BUTTON_VERIFY);
        //add the user oid of the user who uploaded the payment file
        verifyInputDoc.setAttribute("/In/LoginUser/user_oid", paymentUpload.getAttribute("user_oid"));
        populateInputXmlDoc(verifyInputDoc, "/In/Transaction/", trans);

        populateInputXmlDoc(verifyInputDoc, "/In/Terms/", terms);
        populateInputXmlDoc(verifyInputDoc, "/In/PaymentFileUpload/", paymentUpload);

		//for Instrument, only set the instrument oid and type_code
        //cquinton 2/21/2011 Rel 6.1.0 ir#pbul012384451 move instrument creation
        // to before this method
        verifyInputDoc.setAttributeLong("/In/Instrument/instrument_oid", trans.getAttributeLong("instrument_oid"));
        verifyInputDoc.setAttribute("/In/Instrument/instrument_type_code", instrument.getAttribute("instrument_type_code"));
        verifyInputDoc.setAttribute("/In/VerifyAysnc", TradePortalConstants.INDICATOR_YES);

        // CR 857 - include tag to not process panel again in reVerify method
        if (TradePortalConstants.INDICATOR_YES.equals(trans.getAttribute("bene_panel_auth_ind"))) {
            verifyInputDoc.setAttribute("/In/ProcessBenePanelProcess", TradePortalConstants.INDICATOR_NO);
        }

        return verifyInputDoc;

    }

    /**
     * This method takes a document and populates this business object's
     * attributes from the values in the document starting at the specified
     * component path.
     *

     * @param doc The source DocumentHandler object that will be used to
     * populated the object's attributes.
     * @param componentPath The component path to go to to perform the business
     * object population. See the method documentation for a note on how the
     * format of this variable is relevant.
     * @param tagTranslator XMLTagTranslater - an adaptor that translates the
     * XML node tag name to Object attribute/component name, and tag value to
     * attribute valu. If the attribute name is null, this node is ignored.
     */
    public void populateInputXmlDoc(DocumentHandler doc, String componentPath, BusinessObject bean)
            throws RemoteException, AmsException {
        Hashtable attrList = bean.getAttributeHash();
        for (Object o : attrList.keySet()) {
            String attributeComponentName = (String) o;
            if (StringFunction.isNotBlank(bean.getAttribute(attributeComponentName))) {
                doc.setAttribute(componentPath + attributeComponentName, bean.getAttribute(attributeComponentName));
            }
        }
    }

    private DocumentHandler getInputDoc() throws RemoteException, AmsException {
        DocumentHandler inputDoc = new DocumentHandler();
        inputDoc.setAttribute("/LoginUser/user_oid", paymentUpload.getAttribute("user_oid"));
        inputDoc.setAttribute("/Transaction/uploaded_ind", TradePortalConstants.INDICATOR_YES);
        inputDoc.setAttribute("/Instrument/instrument_type_code", InstrumentType.DOM_PMT);
        return inputDoc;
    }

    private String getDurationInSeconds(Date start, Date end) {
        if (start == null || end == null) {
            return null;
        }
        long startMillis = start.getTime();
        long endMillis = end.getTime();
        long durationMillis = endMillis - startMillis;
        return Long.toString(durationMillis / 1000);
    }

    private void setPaymentUploadStatus(String status) throws RemoteException, AmsException {

        if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS.equals(status)) {
            paymentUpload.setAttribute("validation_seconds", getDurationInSeconds(valStartTime, DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))));
//            		GMTUtility.getGMTDateTime(false, false)));
            paymentUpload.setAttribute("completion_timestamp", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//            		DateTimeUtility.getGMTDateTime(false, false));
        }

        paymentUpload.setAttribute("validation_status", status);
        if (paymentUpload.save() < 0) {
            throw new AmsException("error saving PaymentFileUpload ....");
        }
    }

    private void setStatus() throws RemoteException, AmsException {

        if (successCount == 0) {
            rejectFileUplaod(false);
        } else if (successCount == paymentCount) {
        	if(uploadRepLog.size()>0){
        		setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR);
        	}else{
        		setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL);
        	}
        } else {
            setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS);
        }
        
        if (paymentUpload.isH2HSentPayment() && paymentUpload.isPartialPayAuthAllowed()
                && TransactionStatus.VERIFIED_AWAITING_APPROVAL.equals(trans.getAttribute("transaction_status")) ) {
        	//CR-1001 Rel9.4 - If reporting code errors are present, the beneficiary errors get confirmed and moved to ReportingCodes Required status
        	if(uploadRepLog.size()>0){
        		setPaymentUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR);
        	}
        }

        if (uploadLog.size() > 0) {

            String paymentUploadOid = paymentUpload.getAttribute("payment_file_upload_oid");
            try(Connection con = DatabaseQueryBean.connect(false);
            		PreparedStatement pStmt = con.prepareStatement("insert into PAYMENT_UPLOAD_ERROR_LOG "
            				+ "(payment_upload_error_log_oid, line_number, p_payment_file_upload_oid, beneficiary_info,error_msg)values(?,?,?,unistr(?),unistr(?))")	) {
                
                con.setAutoCommit(false);
                //MEer Rel 9.0 IR-18996 using unistr() to insert unicode characters in the database
                
                for (String line_num : uploadLog.keySet()) {
                    Map tmpMap = uploadLog.get(line_num);
                    Iterator temp_it = tmpMap.keySet().iterator(); 
                    String bene_info = (String) temp_it.next();
                    String errMsg = (String) tmpMap.get(bene_info);
                    String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(false));
                    pStmt.setString(1, oid);
                    pStmt.setString(2, line_num);
                    pStmt.setString(3, paymentUploadOid);
                    pStmt.setString(4, StringFunction.toUnistr(bene_info));
                  //  pStmt.setString(5, errMsg);
                    pStmt.setString(5, StringFunction.toUnistr(errMsg));
                    pStmt.addBatch();
                    pStmt.clearParameters();
                }

                pStmt.executeBatch();
                con.commit();
                
            } catch (AmsException | SQLException e) {
                logError("Error occured during saving Payment Upload Error log ...", e);
            }

        } if (uploadRepLog.size() > 0) {

            String paymentUploadOid = paymentUpload.getAttribute("payment_file_upload_oid");
            try(Connection con = DatabaseQueryBean.connect(false);
                    PreparedStatement pStmt = con.prepareStatement("insert into PAYMENT_UPLOAD_ERROR_LOG "
                    		+ "(payment_upload_error_log_oid, line_number, p_payment_file_upload_oid, beneficiary_info,error_msg, p_domestic_payment_oid, REPORTING_CODE_ERROR_IND)values(?,?,?,unistr(?),?,?,?)");) {
                
                con.setAutoCommit(false);
                //MEer Rel 9.0 IR-18996 using unistr() to insert unicode characters in the database
                for (String dp : uploadRepLog.keySet()) {
                    Map tmpMap = uploadRepLog.get(dp);
                    Iterator temp_it = tmpMap.keySet().iterator();
                    String bene_info = (String) temp_it.next();
                    String errMsg = (String) tmpMap.get(bene_info);
					String[] bene_info_Arr = bene_info.split("= ");
                    String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(false));
                    pStmt.setString(1, oid);
                    pStmt.setString(2, bene_info_Arr[3]);
                    pStmt.setString(3, paymentUploadOid);
                    pStmt.setString(4, StringFunction.toUnistr(bene_info_Arr[0]+", "+bene_info_Arr[1]+", "+bene_info_Arr[2]));
                    pStmt.setString(5, errMsg);
                    pStmt.setString(6, dp);
                    pStmt.setString(7, TradePortalConstants.INDICATOR_YES);
                    pStmt.addBatch();
                    pStmt.clearParameters();
                }

                pStmt.executeBatch();
                con.commit();
               

            } catch (AmsException | SQLException e) {
                logError("Error occured during saving Payment Upload Error log ...", e);
            }

        }

    }

    private File getPaymentFile() throws RemoteException, AmsException {
        String payment_oid = paymentUpload.getAttribute("payment_file_upload_oid");
        // manohar - CR 597 start
        String fname = folder + payment_oid + PaymentFileWithInvoiceDetails.FILE_EXTENSION;
        // manohar - CR 597 end
        return new File(fname);
    }

    private BufferedReader getInputReader() throws AmsException {

        logDebug("Getting Paymentfile Reader...");

        BufferedReader bReader = null;
        try {
            paymentFile = getPaymentFile();
            FileInputStream is = new FileInputStream(paymentFile);
            InputStreamReader bis = new InputStreamReader(is, AmsConstants.UTF8_ENCODING);
            bReader = new BufferedReader(bis);
        } catch (Exception e) {
            logError("Error occured getting payment file...", e);
            throw new AmsException("Error occured getting payment file...");
        }

        return bReader;

    }

    private boolean checkError(DocumentHandler doc) throws RemoteException, AmsException {

        boolean hasError = false;
        if (doc != null) {
            StringBuilder sb = new StringBuilder();
            int severityLevel = 0;
            Vector errorList = doc.getFragments("/Error/errorlist/error");
            int errorListSize = errorList.size();

            for (int i = 0; i < errorListSize; i++) {

                DocumentHandler errorDoc = (DocumentHandler) errorList.elementAt(i);
                severityLevel = errorDoc.getAttributeInt("/severity");

                if (severityLevel >= ErrorManager.ERROR_SEVERITY) {
                    hasError = true;
                    sb.append("-").append(errorDoc.getAttribute("/message"));
                }
            }

            if (hasError) {
                paymentUpload.setAttribute("error_msg", sb.toString());
            }

        }
        return hasError;
    }

    private void handleError(BusinessObject obj) throws RemoteException, AmsException {

        StringBuilder sb = new StringBuilder();
        if (obj.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
        	 for (IssuedError errorObject : obj.getIssuedErrors()) {
                sb.append("-").append(errorObject.getErrorText());
            }
        }
        // CR 857 Begin
        if (transErrorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {

            List<IssuedError> errorList = transErrorMgr.getIssuedErrorsList();
            for (IssuedError error : errorList) {
                sb.append("-").append(error.getErrorText());
            }
        }
        // CR 857 End
        paymentUpload.setAttribute("error_msg", sb.toString());
    }

    /**
     * Processes Payment line
     *
     * @param DocumentHandler
     * @param String Payment line
     */
    private void processPaymentLine(DocumentHandler inputDoc, String sline) throws AmsException, RemoteException, RemoveException {

        String[] paymentRecord = PaymentFileDetails.parsePaymentLineItem(sline, PaymentFileDetails.MAX_PAYMENT_FILE_FIELDS_NUMBER);

        DomesticPayment dp = createDP(trans, paymentRecord, paymentCount, paymentUpload.getAttribute("owner_org_oid"), paymentUpload.getAttribute("payment_file_upload_oid"));
        ErrorManager dpErrorMgr = dp.getErrorManager();
        if (paymentCount == 0) {
            validateHeaders(trans, paymentRecord, paymentUpload.getAttribute("owner_org_oid"));

            //Rel9.0 IR#T36000023751 -If it is a H2H and is STA, skip panel check
            if (!(trans.isH2HSentPayment() && corpOrg.isStraightThroughAuthorize())) {
                // CR857 Begin			
                if (TradePortalConstants.INDICATOR_YES.equals(trans.getAttribute("bene_panel_auth_ind")) && panel == null) {
                    setTransactionPanelData(dp);
                }
                // CR857 End
            }//End IR#T36000023751
            // this method verify whether fx rate is present for base currency and also store multiply indicator and buy rate of
            // account and transaction currency.
            verifyAndSetRateDefineForFXGroup();

            if (trans.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY
                    || transErrorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {

                handleError(trans);
                throw new AmsException("Error occured validating header info....");
            }

            debitAccountBankCountryCode = PaymentFileDetails.getBankBranchCountryForAccount(terms.getAttribute("debit_account_oid"));
            debitAccountBankGroupOid = getBankGroupForAccount(dp.getErrorManager());//CR-1001 Rel9.4 Fetch the BankGroup of the Debit account only once for validating reporting codes
            inputDoc.setAttribute("/Terms/amount_currency_code/", paymentRecord[PaymentFileDetails.PAYMENT_CURRENCY]);
            inputDoc.setAttribute("/Transaction/payment_date", trans.getAttribute("payment_date"));

        }

        Vector err = new Vector();
        // CR 857 Begin
        boolean isErrorFoundInPaneProcess = false;
        if (TradePortalConstants.INDICATOR_YES.equals(trans.getAttribute("bene_panel_auth_ind"))) {
            setBeneficiaryPanelData(dp, dpErrorMgr);
            err.addAll(dpErrorMgr.getIssuedErrors());
            if (dpErrorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
                isErrorFoundInPaneProcess = true;
            }
        }

		// CR 857 End
		//TO DO: needs performance tuning...       
        dp.verifyDomesticPayment(inputDoc, pmValueDates, creditAccountBankCountryCode, debitAccountBankCountryCode, true, isErrorFoundInPaneProcess);//BSL IR# PBUL042033374 04/29/11 - Add overload
        if (paymentCount == 0) {
            pmValueDates.put(paymentRecord[PaymentFileDetails.PAYMENT_METHOD], dp.getAttribute("value_date"));
        }

        err.addAll(dp.getIssuedErrors());

        if (err.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (int index = 0; !(index >= err.size()); index++) {
                com.amsinc.ecsg.frame.IssuedError errorObject = (com.amsinc.ecsg.frame.IssuedError) err.elementAt(index);
                sb.append("- ").append(errorObject.getErrorText());
            }
            String lineNum = paymentCount + 1 + "";
            
            String bene_info = paymentRecord[PaymentFileDetails.BENEFICIARY_NAME] + ", "
                    + paymentRecord[PaymentFileDetails.CUSTOMER_REFERENCE] + ", "
                    + paymentRecord[PaymentFileDetails.PAYMENT_AMOUNT];

            // manohar - CR 597 start
            try {
                unsuccessfulTotAmt = unsuccessfulTotAmt.add((new BigDecimal(paymentRecord[PaymentFileDetails.PAYMENT_AMOUNT])));
            } catch (NumberFormatException nfe) {
				//ignore since the amount is not in proper format.

            }
            
            Map<String, String> tempMap = new HashMap<String, String>();
            tempMap.put(bene_info, sb.toString());
            uploadLog.put(lineNum, tempMap);
            
        } else {
            totAmt = totAmt.add((new BigDecimal(paymentRecord[PaymentFileDetails.PAYMENT_AMOUNT])));
            successCount++;

        }
        if (!isErrorFoundInPaneProcess) { //CR857
            dp.save();
            if(err.size() == 0){ //CR-1001 If no beneficiary level errors, then validateReportingCode errors
            	validateReportingCodes(dp);
            }
        }
        dp.remove();
    }

    /**
     * Get the execution date.
     *
     * i. If execution date is found it will be used for all beneficiaries ii.
     * If no execution date is provided, the system will set the execution date
     * to the current date iii. Validate that the date is on the correct format.
     *
     * @return Date execution_date)
     */
    private Date getExecutionDate(String[] paymentRecord)
            throws AmsException {

        logDebug("Getting Execution date...");
        LOG.info("isInvoiceFile");
        // manohar - CR 597 start
        String executionDate = null;
        if (paymentRecord[0].equalsIgnoreCase("HDR")) {
            executionDate = paymentRecord[PaymentFileWithInvoiceDetails.EXECUTION_DATE];
        } else { // manohar - CR 597 end
            executionDate = paymentRecord[PaymentFileDetails.EXECUTION_DATE];
        }

        Date execDate = null;

        if (executionDate == null || executionDate.trim().equals("")) {
            // If no execution date is provided, set the execution date to the current date
        	try{
            execDate = DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(true)); 
//            		GMTUtility.getGMTDateTime(true);
        	}catch(Exception e){
        		e.printStackTrace();
        	}
            execDate = TPDateTimeUtility.getLocalDateTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(execDate), TimeZone.getDefault().toString());
        } else {
            execDate = TPDateTimeUtility.convertDateStringToDate(executionDate.trim(), "dd/MM/yyyy");
            LOG.debug("PaymentFileUploadServlet: getExecutionDate converted [ " + executionDate + "]");
            // 2-digt year is not allowed.  execDate.getYear() returns year based on year 1900.  So any year < 1000 would have getYear() < -900.
            if (execDate == null || execDate.getYear() < -900) {
                logError("invalid payment date, executionDate: " + executionDate);
                execDate = null;
            }
        }
        LOG.debug("getExecutionDate [ " + executionDate + "]");
        return execDate;
    }

    protected String getCorporateOrgName() throws RemoteException, AmsException {
        String corpName = null;
        String sql = "select NAME from CORPORATE_ORG where ORGANIZATION_OID = ?";
        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{paymentUpload.getAttribute("owner_org_oid")});
        if (resultSet != null && !StringFunction.isBlank(resultSet.getAttribute("/ResultSetRecord/NAME"))) {
            corpName = resultSet.getAttribute("/ResultSetRecord/NAME");
        }
        return corpName;
    }

    /**
     * Build the xml formatted input document based on data in the Transaction
     * bean. This method really belongs on the TransactionBean, but is part of
     * the agent for the POC.
     *
     * @param trans the instances of the Transaction
     * @param resultDoc the DocumentHandler instance
     * @return resultDoc the DocumentHandler instance
     */
    protected void validateHeaders(Transaction trans, String[] paymentRecord, String ownerOrgID) throws RemoteException, AmsException {

        String debitAccountNo = null;
        String paymentCurr = null;
        String customerRef = null;
        String partyName = null;
        String bene_add1 = null;
        String bene_add2 = null;
        String bene_add3 = null;
        String bene_country = null;
        String conf_ind = null;
        String ind_acct_ind = null;
        String fxContractNumber = null;
        String fxRate = null;
        Date executionDate = getExecutionDate(paymentRecord);
        //CR-921

        if (executionDate == null) {
            // TO DO: handle error
            logDebug("Invalid Execution date...");
        }
        trans.setAttribute("payment_date", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(executionDate));
        // manohar - CR 597 start
        if (paymentRecord[0].equalsIgnoreCase("HDR")) {
            debitAccountNo = paymentRecord[PaymentFileWithInvoiceDetails.DEBIT_ACCOUNT_NO].trim();
            paymentCurr = paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY];
            customerRef = paymentRecord[PaymentFileWithInvoiceDetails.CUSTOMER_REFERENCE];
            partyName = paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_NAME];
            bene_add1 = paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS1];
            bene_add2 = paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS2];
            bene_add3 = paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS3];
            bene_country = paymentRecord[PaymentFileWithInvoiceDetails.COUNTRY];
            conf_ind = paymentRecord[PaymentFileWithInvoiceDetails.CONFIDENTIAL_INDICATOR];
            //IValavala CR657. set Individual Acct Indicator
            ind_acct_ind = paymentRecord[PaymentFileWithInvoiceDetails.INDIVIDUAL_ACCT_ENTRIES];
            trans.setAttribute("copy_of_currency_code", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY]);
			//CR-921
            //FX Contract Number and FX Rate fields
            fxContractNumber = paymentRecord[PaymentFileWithInvoiceDetails.FX_CONTRACT_NUMBER];
            fxRate = paymentRecord[PaymentFileWithInvoiceDetails.FX_RATE];
        } else {// manohar - CR 597 end
            debitAccountNo = paymentRecord[PaymentFileDetails.DEBIT_ACCOUNT_NO].trim();
            paymentCurr = paymentRecord[PaymentFileDetails.PAYMENT_CURRENCY];
            customerRef = paymentRecord[PaymentFileDetails.CUSTOMER_REFERENCE];
            partyName = paymentRecord[PaymentFileDetails.BENEFICIARY_NAME];
            bene_add1 = paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS1];
            bene_add2 = paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS2];
            bene_add3 = paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS3];
            bene_country = paymentRecord[PaymentFileDetails.COUNTRY];
            conf_ind = paymentRecord[PaymentFileDetails.CONFIDENTIAL_INDICATOR];
            trans.setAttribute("copy_of_currency_code", paymentRecord[PaymentFileDetails.PAYMENT_CURRENCY]);
			//CR-921
            //FX Contract Number and FX Rate fields
            fxContractNumber = paymentRecord[PaymentFileDetails.FX_CONTRACT_NUMBER];
            fxRate = paymentRecord[PaymentFileDetails.FX_RATE];
        }

        // Terms
        Terms terms = (Terms) trans.getComponentHandle("CustomerEnteredTerms");

		// Get Account OID
        String accountSqlStr = "select account_oid from account where account_number = ? and p_owner_oid = ? ";         // NSX - 07/01/10 PDUK030948788
        DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, new Object[]{debitAccountNo, ownerOrgID});
        if (acctResultSet != null && !StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID"))) {
            String debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID");
            terms.setAttribute("debit_account_oid", debitAccountOid);
        }
        terms.setAttribute("amount_currency_code", paymentCurr);
        terms.setAttribute("reference_number", customerRef);
		//CR-921
        //Add the fx contract number and fx rate to Terms if they are present in uploaded data
        if (StringFunction.isNotBlank(fxContractNumber)) {
            terms.setAttribute("covered_by_fec_number", fxContractNumber);
        }
        if (StringFunction.isNotBlank(fxRate)) {
            terms.setAttribute("fec_rate", fxRate);
        }
		//

        // Set Ordering Party
        String corpName = getCorporateOrgName();
        if (StringFunction.isNotBlank(corpName)) {
            terms.newComponent("OrderingParty");
            PaymentParty orderingParty = (PaymentParty) terms.getComponentHandle("OrderingParty");
            orderingParty.setAttribute("bank_name", corpName);
        }

        TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
	    //Beneficiary Name may be upto 140 chars long. Terms only allow upt 35 chars values: use substring with set.
        int termSizeLimit = partyName.length();
        if (termSizeLimit > 35) {
            termSizeLimit = 35;
        }
        firstTermsParty.setAttribute("terms_party_type", TermsPartyType.PAYEE);
        firstTermsParty.setAttribute("name", partyName.substring(0, termSizeLimit));
        firstTermsParty.setAttribute("contact_name", partyName.substring(0, termSizeLimit));
        firstTermsParty.setAttribute("address_line_1", bene_add1);
        firstTermsParty.setAttribute("address_line_2", bene_add2);
        firstTermsParty.setAttribute("address_line_3", bene_add3);
        firstTermsParty.setAttribute("address_country", bene_country);

	
        //Vshah CR-586 [BEGIN]
        if (TradePortalConstants.INDICATOR_YES.equals(conf_ind)) {
            terms.setAttribute("confidential_indicator", TradePortalConstants.CONF_IND_FRM_TMPLT);
        } //Vshah CR-586 [END]
        //RKAZI IR RRUL052435989 05/26/2011 Start
        else {
            terms.setAttribute("confidential_indicator", "");
        }

        //IValavala Rel 7.1 CR 657 Aug 02 2011.
        if (TradePortalConstants.INDICATOR_YES.equals(ind_acct_ind)) {
            terms.setAttribute("individual_debit_ind", TradePortalConstants.INDICATOR_YES);
        } else {
            terms.setAttribute("individual_debit_ind", TradePortalConstants.INDICATOR_NO);
        }
    }

    /**
     * Build the xml formatted input document based on data in the Transaction
     * bean. This method really belongs on the TransactionBean, but is part of
     * the agent for the POC.
     *
     * @param trans the instances of the Transaction
     * @param resultDoc the DocumentHandler instance
     * @return resultDoc the DocumentHandler instance
     */
    private Transaction createTransaction(PaymentFileUpload paymentUpload) throws RemoteException, AmsException {

        logDebug("Creating transaction...");

        String ownerOrgID = paymentUpload.getAttribute("owner_org_oid");
        String userOID = paymentUpload.getAttribute("user_oid");
        long sec_profile_oid = 0;

        User user = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", Long.parseLong(userOID));

		// if not corporate user, use customer access security profile
        //Leelavathi IR#T36000013673 Rel-8300 08/13/2013 Begin
        if (!TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))
                && TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("customer_access_ind"))) {
            sec_profile_oid = user.getAttributeLong("cust_access_sec_profile_oid");
        } //Leelavathi IR#T36000013673 Rel-8300 08/13/2013 End
        else {
            sec_profile_oid = user.getAttributeLong("security_profile_oid");
        }

        SecurityProfile rights = (SecurityProfile) EJBObjectFactory.createClientEJB(ejbServerLocation, "SecurityProfile", sec_profile_oid);
        corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization", Long.parseLong(ownerOrgID));

        CreateTransactionMediator med = (CreateTransactionMediator) EJBObjectFactory.createClientEJB(ejbServerLocation, "CreateTransactionMediator");
        DocumentHandler inputDoc = new DocumentHandler();
        securityRights = rights.getAttribute("security_rights");
        inputDoc.setAttribute("/In/securityRights", securityRights);
        inputDoc.setAttribute("/In/instrumentType", "FTDP");
        inputDoc.setAttribute("/In/copyType", "Blank");
        inputDoc.setAttribute("/In/bankBranch", corpOrg.getAttribute("first_op_bank_org"));
        inputDoc.setAttribute("/In/userOid", userOID);
        inputDoc.setAttribute("/In/mode", "CREATE_NEW_INSTRUMENT");
        inputDoc.setAttribute("/In/validationState", "VALIDATE_STEP_1");
        inputDoc.setAttribute("/In/clientBankOid", user.getAttribute("client_bank_oid"));
        inputDoc.setAttribute("/In/ownerOrg", ownerOrgID);
        inputDoc.setAttribute("/In/uploaded_ind", "Y");
        // CJR CR-593 04/06/2011 BEGIN
        inputDoc.setAttribute("/In/gxs_ref", paymentUpload.getAttribute("gxs_ref"));
        inputDoc.setAttribute("/In/customer_file_ref", paymentUpload.getAttribute("customer_file_ref"));
        // CJR CR-593 04/06/2011 END
        inputDoc.setAttribute("/In/h2h_source", paymentUpload.getAttribute("h2h_source")); // Nar CR694A Rel9.0

        DocumentHandler outputDocument = med.performService(inputDoc, csdb);
        if (checkError(outputDocument)) {
            throw new AmsException("error creating transaction...");
        }

        String transOid = outputDocument.getAttribute("/Out/Transaction/oid");
        Transaction trans = (Transaction) EJBObjectFactory.createClientEJB(ejbServerLocation, "Transaction", Long.parseLong(transOid), csdb);
        transErrorMgr = trans.getErrorManager();

        //cquinton 5/16/2011 cleanup ejb
        removeEJBReference(user);
        removeEJBReference(rights);
        removeEJBReference(med);

        return trans;
    }

    /**
     * Upload one payment line item.
     *
     * @param trans
     * @param paymentRecord
     * @param userSession
     * @param ClientServerDataBridge
     * @return boolean true - success false = failure
     * @throws AmsException
     * @throws RemoteException
     */
    protected DomesticPayment createDP(Transaction trans, String[] paymentRecord, int paymentCount, String ownerOrgID, String paymentFileUploadOid)
            throws AmsException, RemoteException {

        DomesticPayment domesticPayment = (DomesticPayment) EJBObjectFactory.createClientEJB(ejbServerLocation, "DomesticPayment", csdb);
        domesticPayment.newObject();

		// smanohar R8.4 T36000013729. Performance tuning -- start. 
		// Instead of making a remote call for setting value of each attribute, collect the attribute values in a HashMap first 
        // and pass them in bulk to the remote bean using method setAttributes(String[] attributePaths, String[] attributeValues) available
        // BusinessObjectBean. 
        Map<String, String> dpAttributes = new HashMap<>();

        // manohar - CR 597 start
        if (paymentRecord!=null && paymentRecord[0].equalsIgnoreCase("HDR")) {
            dpAttributes.put("sequence_number", Integer.toString(paymentCount + 1));
            dpAttributes.put("transaction_oid", trans.getAttribute("transaction_oid"));
            dpAttributes.put("amount", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_AMOUNT]);

            // Note: do not set value date.  It will be calculated during Verify.
            dpAttributes.put("amount_currency_code", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_CURRENCY]);
            dpAttributes.put("payment_method_type", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_METHOD]);
            dpAttributes.put("payee_name", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_NAME]);
            dpAttributes.put("payee_account_number", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ACCOUNT_NO]);
            dpAttributes.put("payee_address_line_1", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS1]);
            dpAttributes.put("payee_address_line_2", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS2]);
            dpAttributes.put("payee_address_line_3", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS3]);
            dpAttributes.put("payee_address_line_4", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_ADDRESS4]);
            dpAttributes.put("payee_bank_name", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_NAME]);
            dpAttributes.put("payee_bank_code", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_CODE]);
            dpAttributes.put("payee_branch_name", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_NAME]);
            dpAttributes.put("address_line_1", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_ADDRESS1]);
            dpAttributes.put("address_line_2", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_ADDRESS2]);
            dpAttributes.put("address_line_3", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_CITY] + " " + paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_PROVINCE]);
            dpAttributes.put("payee_bank_country", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_BANK_BRANCH_COUNTRY]);
            dpAttributes.put("country", paymentRecord[PaymentFileWithInvoiceDetails.COUNTRY]);
            dpAttributes.put("customer_reference", paymentRecord[PaymentFileWithInvoiceDetails.CUSTOMER_REFERENCE]);
            dpAttributes.put("payee_fax_number", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_FAX_NO]);
            dpAttributes.put("payee_email", paymentRecord[PaymentFileWithInvoiceDetails.BENEFICIARY_EMAIL_ID]);
            dpAttributes.put("payee_instruction_number", paymentRecord[PaymentFileWithInvoiceDetails.INSTRUCTION_NUMBER]);
            dpAttributes.put("payee_description", paymentRecord[PaymentFileWithInvoiceDetails.PAYMENT_DETAILS]);
            if (paymentRecord[PaymentFileWithInvoiceDetails.CHARGES] == null || paymentRecord[PaymentFileWithInvoiceDetails.CHARGES].trim().length() == 0) {
				//IR 23519 - Pass bank_charges_type="O" instead of "S"
                //jgadela IR - T36000013687 Rel 8.3 -  setting the default value for charges to shared
                dpAttributes.put("bank_charges_type", TradePortalConstants.CHARGE_OTHER);
            } else {
				// Translate the for charges type from upload file values to TP code values.
                //BSL IR PAUL051735284 05/18/11 BEGIN - replace CHARGE_UPLOAD_*** constants with corresponding CHARGE_UPLOAD_FW_*** (fixed width) versions; log error for invalid value
                if (paymentRecord[PaymentFileWithInvoiceDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_FW_OURS)) {
                    paymentRecord[PaymentFileWithInvoiceDetails.CHARGES] = TradePortalConstants.CHARGE_OUR_ACCT;
                } else if (paymentRecord[PaymentFileWithInvoiceDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_FW_BEN)) {
                    paymentRecord[PaymentFileWithInvoiceDetails.CHARGES] = TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE;
                } else if (paymentRecord[PaymentFileWithInvoiceDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_FW_SHARE)) {
                    paymentRecord[PaymentFileWithInvoiceDetails.CHARGES] = TradePortalConstants.CHARGE_OTHER;
                } else {
                    // Write error to the log
                    StringBuilder invalidChargeError = new StringBuilder();
                    invalidChargeError.append("The value '").append(paymentRecord[PaymentFileWithInvoiceDetails.CHARGES]);
                    invalidChargeError.append("' is not valid for Charges.  Translating the invalid value to '");
                    invalidChargeError.append(TradePortalConstants.CHARGE_BAD_TRANSLATION).append("'.  Valid values are '");
                    invalidChargeError.append(TradePortalConstants.CHARGE_UPLOAD_FW_OURS).append("', '");
                    invalidChargeError.append(TradePortalConstants.CHARGE_UPLOAD_FW_BEN).append("', and '");
                    invalidChargeError.append(TradePortalConstants.CHARGE_UPLOAD_FW_SHARE).append("'.");
                    logError(invalidChargeError.toString());

                    // Translate invalid Charges value into constant to be rejected during payment verify
                    paymentRecord[PaymentFileWithInvoiceDetails.CHARGES] = TradePortalConstants.CHARGE_BAD_TRANSLATION;
                }
                //BSL IR PAUL051735284 05/18/11 END
                dpAttributes.put("bank_charges_type", paymentRecord[PaymentFileWithInvoiceDetails.CHARGES]);
            }
            dpAttributes.put("delivery_method", paymentRecord[PaymentFileWithInvoiceDetails.DELIVERY_METHOD_DELIVER_TO]);
            dpAttributes.put("payable_location", paymentRecord[PaymentFileWithInvoiceDetails.PAYABLE_LOCATION]);
            dpAttributes.put("print_location", paymentRecord[PaymentFileWithInvoiceDetails.PRINT_LOCATION]);
            dpAttributes.put("mailing_address_line_1", paymentRecord[PaymentFileWithInvoiceDetails.MAILING_ADDRESS1]);
            dpAttributes.put("mailing_address_line_2", paymentRecord[PaymentFileWithInvoiceDetails.MAILING_ADDRESS2]);
            dpAttributes.put("mailing_address_line_3", paymentRecord[PaymentFileWithInvoiceDetails.MAILING_ADDRESS3]);
            dpAttributes.put("mailing_address_line_4", paymentRecord[PaymentFileWithInvoiceDetails.MAILING_ADDRESS4]);
            dpAttributes.put("central_bank_reporting_1", paymentRecord[PaymentFileWithInvoiceDetails.CENTRAL_BANK_REPORTING1]);
            dpAttributes.put("central_bank_reporting_2", paymentRecord[PaymentFileWithInvoiceDetails.CENTRAL_BANK_REPORTING2]);
            dpAttributes.put("central_bank_reporting_3", paymentRecord[PaymentFileWithInvoiceDetails.CENTRAL_BANK_REPORTING3]);
            dpAttributes.put("reporting_code_1", paymentRecord[PaymentFileWithInvoiceDetails.REPORTING_CODE1]);
            dpAttributes.put("reporting_code_2", paymentRecord[PaymentFileWithInvoiceDetails.REPORTING_CODE2]);

            //manohar - CR 597 01/04/2011 begin
            dpAttributes.put("user_defined_field_1", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD1]);
            dpAttributes.put("user_defined_field_2", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD2]);
            dpAttributes.put("user_defined_field_3", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD3]);
            dpAttributes.put("user_defined_field_4", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD4]);
            dpAttributes.put("user_defined_field_5", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD5]);
            dpAttributes.put("user_defined_field_6", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD6]);
            dpAttributes.put("user_defined_field_7", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD7]);
            dpAttributes.put("user_defined_field_8", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD8]);
            dpAttributes.put("user_defined_field_9", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD9]);
            dpAttributes.put("user_defined_field_10", paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD10]);
			//manohar - CR 597 01/04/2011 end

            // set attributes of component FirstIntermediaryBank
            dpAttributes.put("/FirstIntermediaryBank/bank_name", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_NAME]);
            dpAttributes.put("/FirstIntermediaryBank/bank_branch_code", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_CODE]);
            dpAttributes.put("/FirstIntermediaryBank/branch_name", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_NAME]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_1", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_2", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_3", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_CITY] + " " + paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE]);
            dpAttributes.put("/FirstIntermediaryBank/country", paymentRecord[PaymentFileWithInvoiceDetails.FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY]);

            int size = dpAttributes.keySet().size();
            String[] keys = new String[size];
            String[] values = new String[size];

            packInArray(dpAttributes, keys, values);
            domesticPayment.setAttributes(keys, values);

        } else { // manohar - CR 597 end
            dpAttributes.put("sequence_number", Integer.toString(paymentCount + 1));
            dpAttributes.put("transaction_oid", trans.getAttribute("transaction_oid"));
            if(paymentRecord!=null){
            dpAttributes.put("amount", paymentRecord[PaymentFileDetails.PAYMENT_AMOUNT]);
            // Note: do not set value date.  It will be calculated during Verify.
            dpAttributes.put("amount_currency_code", paymentRecord[PaymentFileDetails.PAYMENT_CURRENCY]);
            dpAttributes.put("payment_method_type", paymentRecord[PaymentFileDetails.PAYMENT_METHOD]);
            dpAttributes.put("payee_name", paymentRecord[PaymentFileDetails.BENEFICIARY_NAME]);
            dpAttributes.put("payee_account_number", paymentRecord[PaymentFileDetails.BENEFICIARY_ACCOUNT_NO]);
            dpAttributes.put("payee_address_line_1", paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS1]);
            dpAttributes.put("payee_address_line_2", paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS2]);
            dpAttributes.put("payee_address_line_3", paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS3]);
            dpAttributes.put("payee_address_line_4", paymentRecord[PaymentFileDetails.BENEFICIARY_ADDRESS4]);
            dpAttributes.put("payee_bank_name", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_NAME]);
            dpAttributes.put("payee_bank_code", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_CODE]);
            dpAttributes.put("payee_branch_name", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_NAME]);
            dpAttributes.put("address_line_1", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_ADDRESS1]);
            dpAttributes.put("address_line_2", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_ADDRESS2]);
            dpAttributes.put("address_line_3", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_CITY] + " " + paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_PROVINCE]);
            dpAttributes.put("payee_bank_country", paymentRecord[PaymentFileDetails.BENEFICIARY_BANK_BRANCH_COUNTRY]);
            dpAttributes.put("country", paymentRecord[PaymentFileDetails.COUNTRY]);
            dpAttributes.put("customer_reference", paymentRecord[PaymentFileDetails.CUSTOMER_REFERENCE]);
            dpAttributes.put("payee_fax_number", paymentRecord[PaymentFileDetails.BENEFICIARY_FAX_NO]);
            dpAttributes.put("payee_email", paymentRecord[PaymentFileDetails.BENEFICIARY_EMAIL_ID]);
            dpAttributes.put("payee_instruction_number", paymentRecord[PaymentFileDetails.INSTRUCTION_NUMBER]);
            dpAttributes.put("payee_description", paymentRecord[PaymentFileDetails.PAYMENT_DETAILS]);
            if (paymentRecord[PaymentFileDetails.CHARGES] == null || paymentRecord[PaymentFileDetails.CHARGES].trim().length() == 0) {
				//jgadela R 8.4 IR 24031 - Pass bank_charges_type="O" instead of "S", backout T36000013687
                //jgadela IR - T36000013687 Rel 8.3 -  setting the default value for charges to shared
                dpAttributes.put("bank_charges_type", TradePortalConstants.CHARGE_OTHER);
            } else {
                // Translate the for charges type from upload file values to TP code values.
                if (paymentRecord[PaymentFileDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_OURS)) {
                    paymentRecord[PaymentFileDetails.CHARGES] = TradePortalConstants.CHARGE_OUR_ACCT;
                } else if (paymentRecord[PaymentFileDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_BEN)) {
                    paymentRecord[PaymentFileDetails.CHARGES] = TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE;
                } else if (paymentRecord[PaymentFileDetails.CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_OTHER)) {
                    paymentRecord[PaymentFileDetails.CHARGES] = TradePortalConstants.CHARGE_OTHER;
                }
                dpAttributes.put("bank_charges_type", paymentRecord[PaymentFileDetails.CHARGES]);
            }
            dpAttributes.put("delivery_method", paymentRecord[PaymentFileDetails.DELIVERY_METHOD_DELIVER_TO]);
            dpAttributes.put("payable_location", paymentRecord[PaymentFileDetails.PAYABLE_LOCATION]);
            dpAttributes.put("print_location", paymentRecord[PaymentFileDetails.PRINT_LOCATION]);
            dpAttributes.put("mailing_address_line_1", paymentRecord[PaymentFileDetails.MAILING_ADDRESS1]);
            dpAttributes.put("mailing_address_line_2", paymentRecord[PaymentFileDetails.MAILING_ADDRESS2]);
            dpAttributes.put("mailing_address_line_3", paymentRecord[PaymentFileDetails.MAILING_ADDRESS3]);
            dpAttributes.put("mailing_address_line_4", paymentRecord[PaymentFileDetails.MAILING_ADDRESS4]);
            dpAttributes.put("central_bank_reporting_1", paymentRecord[PaymentFileDetails.CENTRAL_BANK_REPORTING1]);
            dpAttributes.put("central_bank_reporting_2", paymentRecord[PaymentFileDetails.CENTRAL_BANK_REPORTING2]);
            dpAttributes.put("central_bank_reporting_3", paymentRecord[PaymentFileDetails.CENTRAL_BANK_REPORTING3]);
            dpAttributes.put("reporting_code_1", paymentRecord[PaymentFileDetails.REPORTING_CODE_1]);
            dpAttributes.put("reporting_code_2", paymentRecord[PaymentFileDetails.REPORTING_CODE_2]);


            dpAttributes.put("/FirstIntermediaryBank/bank_name", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_NAME]); // DK IR T36000023898 Rel8.4 01/07/2014
            dpAttributes.put("/FirstIntermediaryBank/bank_branch_code", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_CODE]);
            dpAttributes.put("/FirstIntermediaryBank/branch_name", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_NAME]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_1", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_2", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2]);
            dpAttributes.put("/FirstIntermediaryBank/address_line_3", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_CITY] + " " + paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE]);
            dpAttributes.put("/FirstIntermediaryBank/country", paymentRecord[PaymentFileDetails.FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY]);
            }
            int size = dpAttributes.keySet().size();
            String[] keys = new String[size];
            String[] values = new String[size];
            packInArray(dpAttributes, keys, values);
            domesticPayment.setAttributes(keys, values);

        }
		// smanohar R8.4 T36000013729. Performance tuning -- end.

        return domesticPayment;
    }

	// smanohar R8.4 T36000013729. Performance tuning -- start. private method which takes HashMap containing attribute keys and values
    // and pack them in an array so that those attribute values can be passed in one remote call from agent process
    private void packInArray(Map<String, String> attributes, String[] keys, String[] values) {

        HashMap<String, String> attributesMap = (HashMap<String, String>) attributes;
        Iterator<String> ikeys = attributesMap.keySet().iterator();
        int index = 0;

        while (ikeys.hasNext()) {
            String oneKey = ikeys.next();
            keys[index] = oneKey;
            values[index] = attributesMap.get(oneKey);
            index++;
        }

    }

	// smanohar R8.4 T36000013729. Performance tuning -- end. 

    protected void cleanup() {

        removeEJBReference(trans);
        removeEJBReference(paymentUpload);
        removeEJBReference(corpOrg);

        //delete file
        if (paymentFile != null && !paymentFile.delete()) {
            logError("Error: Could not delete file: " + paymentFile.getName());

        }

    }

    protected void removeEJBReference(EJBObject objRef) {
        try {
            if (objRef != null) {
                objRef.remove();
                objRef = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logError("Error removing instance of EJB object " + objRef.toString(), e);
        }
    }

    public void logError(String errorText) {
        String str = " : UploadFilename: " + fileName + "  :: PaymentUpload_Oid: " + paymentFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id, process_id + ": ERROR: " + errorText + str, Logger.ERROR_LOG_SEVERITY);

    }

    public void logError(String errorText, Exception e) {
        String str = " : UploadFilename: " + fileName + "  :: PaymentUpload_Oid: " + paymentFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id, process_id + ": ERROR: " + errorText + ": " + e.getMessage() + str, Logger.ERROR_LOG_SEVERITY);

    }

    public void logDebug(String infoText) {
        String str = " : UploadFilename: " + fileName + "  :: PaymentUpload_Oid: " + paymentFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id, process_id + ": INFO: " + infoText + str, Logger.DEBUG_LOG_SEVERITY);
    }

    // CR 857 Begin
    /**
     *
     * @throws NumberFormatException
     * @throws RemoteException
     * @throws AmsException
     */
    protected void validatePanelSetupData() throws NumberFormatException,
            RemoteException, AmsException {

		// if instrument owner is subsidiary customer, then parent should also
        // be set for same panel setting
        User user = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", Long.parseLong(paymentUpload.getAttribute("user_oid")));
        if (TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))
                && !corpOrg.getAttribute("organization_oid").equals(user.getAttribute("owner_org_oid"))) {

            CorporateOrganization loggedUserCorpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation, "CorporateOrganization",
                    Long.parseLong(user.getAttribute("owner_org_oid")));

            if (!loggedUserCorpOrg.isPanelAuthEnabled(InstrumentType.DOM_PMT)) {
                transErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);
            } else if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("pmt_bene_panel_auth_ind"))) {

                if (!TradePortalConstants.INDICATOR_YES.equals(loggedUserCorpOrg.getAttribute("pmt_bene_panel_auth_ind"))) {
                    transErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);
                } else {
                    trans.setAttribute("bene_panel_auth_ind", TradePortalConstants.INDICATOR_YES);
                }
            }
            removeEJBReference(loggedUserCorpOrg);
            removeEJBReference(user);
        } else if (TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("pmt_bene_panel_auth_ind"))) {
            trans.setAttribute("bene_panel_auth_ind", TradePortalConstants.INDICATOR_YES);
        }
    }

    /**
     * this method set panel data related to transaction.
     *
     * @param dp
     * @throws NumberFormatException
     * @throws RemoteException
     * @throws AmsException
     */
    void setTransactionPanelData(DomesticPayment dp)
            throws NumberFormatException, RemoteException, AmsException {

        String panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(InstrumentType.DOM_PMT, corpOrg, transErrorMgr, trans.getAttribute("transaction_type_code"),
                dp.getAttribute("payment_method_type"), terms.getAttribute("confidential_indicator"), terms.getAttribute("debit_account_oid"));
        PanelAuthorizationGroup panelAuthGroup = getPanelAuthGroup(panelGroupOid, transErrorMgr);

        if (panelAuthGroup != null) {
            String panelGroupOId = panelAuthGroup.getAttribute("panel_auth_group_oid");
            String panelOptVal = panelAuthGroup.getAttribute("opt_lock");
            trans.setAttribute("panel_auth_group_oid", panelGroupOId);
            trans.setAttribute("panel_oplock_val", panelOptVal);

            panel = new Panel(panelGroupOId, panelOptVal, InstrumentType.DOM_PMT, transErrorMgr);

            Account account = (Account) EJBObjectFactory.createClientEJB(ejbServerLocation, "Account", Long.parseLong(terms.getAttribute("debit_account_oid")));
            baseCurrency = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, account, InstrumentType.DOM_PMT);

            if ("ACCT_CCY".equals(panel.getCcyBasis())) {
                isAccountCurrecny = true;
            }
            transferCurrency = terms.getAttribute("amount_currency_code");
            fromCurrency = account.getAttribute("currency");

            removeEJBReference(account);
            removeEJBReference(panelAuthGroup);
        }
    }

    /**
     *
     * @param panelGroupOid
     * @param transErrorMgr
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private PanelAuthorizationGroup getPanelAuthGroup(String panelGroupOid, ErrorManager transErrorMgr) throws RemoteException, AmsException {

        PanelAuthorizationGroup panelAuthorizationGroup = null;
        if (StringFunction.isNotBlank(panelGroupOid)) {
            panelAuthorizationGroup = (PanelAuthorizationGroup) EJBObjectFactory.createClientEJB(ejbServerLocation, "PanelAuthorizationGroup");
            try {
                panelAuthorizationGroup.find("panel_auth_group_oid", panelGroupOid);
            } catch (RemoteException exe) {
                transErrorMgr.issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.PANEL_GROUP_NOT_PRESENT);
            }
        }

        return panelAuthorizationGroup;
    }

    /**
     * this method set panel authorization data related to beneficiary depend on
     * amount.
     *
     * @param dp
     * @throws RemoteException
     * @throws AmsException
     */
    private void setBeneficiaryPanelData(DomesticPayment dp, ErrorManager dpErrorMgr)
            throws RemoteException, AmsException {

        BigDecimal amountToPanelGroup = getAmountToPanelGroup(dp.getAttribute("amount"), dpErrorMgr);
        if (amountToPanelGroup.compareTo(BigDecimal.ZERO) < 0) {
            return;
        }
        PanelRange panelRange = panel.getRange(amountToPanelGroup.toString());

        if (panelRange == null) {
            String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
                    TradePortalConstants.INSTRUMENT_TYPE,
                    InstrumentType.DOM_PMT,
                    dpErrorMgr.getLocaleName());
            String formattedAmountInBase = TPCurrencyUtility.getDisplayAmount(dp.getAttribute("amount"), transferCurrency,
                    dpErrorMgr.getLocaleName());

            dpErrorMgr.issueError(
                    getClass().getName(),
                    TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
                    formattedAmountInBase, instrumentTypeDesc);

        } else {

            String panelRangeOid = panelRange.getPanelRangeOid();
            if (!dmstPymtPanelRangeMap.containsKey(panelRangeOid)) {
                long dmstPmtPanelAuthRangeOid = trans.newComponent("DmstPmtPanelAuthRangeList");
                DmstPmtPanelAuthRange dmstPmtPanelAuthRange = (DmstPmtPanelAuthRange) trans.getComponentHandle(
                        "DmstPmtPanelAuthRangeList",
                        dmstPmtPanelAuthRangeOid);
                dmstPmtPanelAuthRange.setAttribute("panel_auth_range_oid", panelRangeOid);
                dmstPymtPanelRangeMap.put(panelRangeOid, Long.toString(dmstPmtPanelAuthRangeOid));
            }
            dp.setAttribute("dmst_pmt_panel_auth_range", dmstPymtPanelRangeMap.get(panelRangeOid));
        }

    }

    /**
     * This method convert Transaction amount into equal amount of base currency
     * amount or account currency amount.
     *
     * @param amount - transaction amount
     * @param dpErrorMgr
     * @return - amount to find panel group
     * @throws AmsException
     */
    private BigDecimal getAmountToPanelGroup(String amount, ErrorManager dpErrorMgr) throws AmsException {

        BigDecimal amountForPanelGroup = new BigDecimal(-1.0f);

        if (isAccountCurrecny) {
            amountForPanelGroup = getDebitAmount(amount, dpErrorMgr);
        } else {
            amountForPanelGroup = getAmountInBaseCurrency(amount, buyRateToConvertFromTransCurr, dpErrorMgr);
        }

        return amountForPanelGroup;
    }

    /**
     * This Method convert transaction amount into account currency amount.
     *
     * @param amount
     * @param dpErrorMgr
     * @return
     * @throws AmsException
     */
    private BigDecimal getDebitAmount(String amount, ErrorManager dpErrorMgr) throws AmsException {
        BigDecimal debit_amt = null;

        if (transferCurrency.equals(fromCurrency)) {
            debit_amt = new BigDecimal(amount).abs();
        } else if (transferCurrency.equals(baseCurrency)) {
            debit_amt = getAmounInFXCurrency(amount, buyRateToConvertFromAcctCurr, dpErrorMgr);

        } else if (fromCurrency.equals(baseCurrency)) {
            debit_amt = getAmountInBaseCurrency(amount, sellRateToConvertFromTransCurr, dpErrorMgr);
        } else {
            BigDecimal amountInBase = getAmountInBaseCurrency(amount, sellRateToConvertFromTransCurr, dpErrorMgr);
            debit_amt = getAmounInFXCurrency(amountInBase.toString(), buyRateToConvertFromAcctCurr, dpErrorMgr);
        }
        return debit_amt;
    }

    /**
     * this method convert base amount into required Fxurrency amount.
     *
     * @param baseAmount
     * @param dpErrorMgr
     * @return
     * @throws AmsException
     */
    private BigDecimal getAmounInFXCurrency(String baseAmount, BigDecimal rate, ErrorManager dpErrorMgr) throws AmsException {

        BigDecimal amountInBase = (new BigDecimal(baseAmount)).abs();

        if (baseCurrency.equals(fromCurrency)) {
            return amountInBase;
        }

        BigDecimal amountInForeign = new BigDecimal(-1.0f);
        if (StringFunction.isBlank(multiIndToConvertFromAcctCurr)) {
            dpErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, fromCurrency);
        } else {

            int dec = TPCurrencyUtility.getDecimalPrecision(fromCurrency);
            if (TradePortalConstants.DIVIDE.equals(multiIndToConvertFromAcctCurr)) {
                amountInForeign = amountInBase.multiply(rate).setScale(dec, BigDecimal.ROUND_HALF_UP);

            } else {
                amountInForeign = amountInBase.divide(rate, dec, BigDecimal.ROUND_HALF_UP);
            }
        }

        return amountInForeign;
    }

    /**
     * this method convert transaction amount into customer base currency
     * amount.
     *
     * @param amount
     * @param dpErrorMgr
     * @return
     * @throws AmsException
     */
    private BigDecimal getAmountInBaseCurrency(String amount, BigDecimal rate, ErrorManager dpErrorMgr) throws AmsException {

        BigDecimal amountInForeign = (new BigDecimal(amount)).abs();
        BigDecimal amountInBase = new BigDecimal(-1.0f);
        if (this.baseCurrency.equals(this.transferCurrency)) {
            amountInBase = amountInForeign;
        } else {
            if (StringFunction.isBlank(multiIndToConvertFromTransCurr)) {
                dpErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_FOREX_DEFINED, transferCurrency);
            } else {
                if (StringFunction.isBlank(rate.toString())) {
                    dpErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_SELL_FOREX_DEFINED);
                } else {
                    int dec = TPCurrencyUtility.getDecimalPrecision(baseCurrency);
                    if (TradePortalConstants.DIVIDE.equals(multiIndToConvertFromTransCurr)) {
                        amountInBase = amountInForeign.divide(rate, dec, BigDecimal.ROUND_HALF_UP);
                    } else {
                        amountInBase = amountInForeign.multiply(rate);
                    }
                }
            }
        }

        return amountInBase;
    }

    /**
     * this method check whether basecurrency is defined in FX Rate group or
     * not.
     *
     * @throws NumberFormatException
     * @throws RemoteException
     * @throws AmsException
     */
    void verifyAndSetRateDefineForFXGroup() throws NumberFormatException, RemoteException, AmsException {

        CorporateOrganization corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation,
                "CorporateOrganization", Long.parseLong(paymentUpload.getAttribute("owner_org_oid")));

        boolean isDefined = false;
        String baseCurrency = terms.getAttribute("amount_currency_code");
        String fxrategroup = corpOrg.getAttribute("fx_rate_group");
        String clientbankid = corpOrg.getAttribute("client_bank_oid");

        if (StringFunction.isNotBlank(fxrategroup)) {
            Hashtable<String, String> currencies = getFxCurrenciesForGroup(fxrategroup, clientbankid);
            if (currencies != null) {
                Iterator<String> it = currencies.keySet().iterator();
                Iterator<String> bc = currencies.values().iterator();

                while (it.hasNext()) {
                    String key = it.next();
                    String basecurrency = bc.next();
                    if (key.equals(baseCurrency) || basecurrency.equals(baseCurrency)) {
                        isDefined = true;
                        break;
                    }
                }
            }
        }
        if (!isDefined) {
            // throw error if not present 
            transErrorMgr.issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_BUY_FOREX_DEFINED);
        }

        removeEJBReference(corpOrg);
    }

    /**
     * This method has been added for IR # T36000015559 This method will get the
     * currencies defined for a particular FX Group.
     *
     * @param fxGroup
     * @param organizationOid
     * @throws AmsException
     */
    private Hashtable<String, String> getFxCurrenciesForGroup(String fxGroup,
            String clientbankOid) throws AmsException {
        String fxGroupQuery = "select currency_code,base_currency_code, buy_rate, sell_rate, multiply_indicator from fx_rate where fx_rate_group = ? and p_owner_org_oid = ? ";
        Hashtable<String, String> currencyList = null;
        DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(fxGroupQuery, false, new Object[]{fxGroup, clientbankOid});
        if (resultDoc != null) {
            currencyList = new Hashtable<>();
            List<DocumentHandler> cusrrencies = resultDoc.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler doc : cusrrencies) {
                currencyList.put(doc.getAttribute("/CURRENCY_CODE"), doc.getAttribute("/BASE_CURRENCY_CODE"));
                // store the multiply indicator and buy rate of account currency. it will be used to get panel group if required.
                if (doc.getAttribute("/CURRENCY_CODE") != null && doc.getAttribute("/CURRENCY_CODE").equals(this.fromCurrency)) {
                    this.buyRateToConvertFromAcctCurr = new BigDecimal(doc.getAttribute("/BUY_RATE"));
                    this.sellRateToConvertFromAcctCurr = new BigDecimal(doc.getAttribute("/SELL_RATE"));
                    this.multiIndToConvertFromAcctCurr = doc.getAttribute("/MULTIPLY_INDICATOR");
                    // store the multiply indicator and buy rate of transaction currency. it will be used to get panel group if required.
                } else if (doc.getAttribute("/CURRENCY_CODE") != null && doc.getAttribute("/CURRENCY_CODE").equals(this.transferCurrency)) {
                    this.buyRateToConvertFromTransCurr = new BigDecimal(doc.getAttribute("/BUY_RATE"));
                    this.sellRateToConvertFromTransCurr = new BigDecimal(doc.getAttribute("/SELL_RATE"));
                    this.multiIndToConvertFromTransCurr = doc.getAttribute("/MULTIPLY_INDICATOR");
                }
            }
        }
        return currencyList;
    }

    /**
     * PMitnala Rel9.1 CR 970 Added to create an entry in Outgoing_queue table
     *
     * @param trans
     */
    private void createOutgoingMessage(Transaction trans) {
        try {
            if (trans.isH2HSentPayment()) {

                OutgoingInterfaceQueue outgoing_queue = (OutgoingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "OutgoingInterfaceQueue");
                outgoing_queue.newObject();
                outgoing_queue.setAttribute("date_created", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate()));
//                		DateTimeUtility.getGMTDateTime());
                outgoing_queue.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
                outgoing_queue.setAttribute("msg_type", MessageType.PAYBACK);
                outgoing_queue.setAttribute("message_id", InstrumentServices.getNewMessageID());

                String queue_oid = "";
                String msg_text = "";
                String sql = "Select queue_oid from incoming_queue where message_id=?";
                String sqlHistory = "Select queue_history_oid from incoming_q_history where message_id=?";

                DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{paymentUpload.getAttribute("message_id")});

                if (resultSet != null) {
                    queue_oid = resultSet.getAttribute("/ResultSetRecord/QUEUE_OID");
                    IncomingInterfaceQueue incoming_queue = (IncomingInterfaceQueue) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingInterfaceQueue", Long.parseLong(queue_oid));
                    msg_text = incoming_queue.getAttribute("msg_text");
                } else {
                    //Query in Incoming_q_history
                    DocumentHandler resultSetHistory = DatabaseQueryBean.getXmlResultSet(sqlHistory, false, new Object[]{paymentUpload.getAttribute("message_id")});
                    if (resultSetHistory != null) {
                        queue_oid = resultSetHistory.getAttribute("/ResultSetRecord/QUEUE_HISTORY_OID");
                        IncomingQueueHistory incoming_queue_history = (IncomingQueueHistory) EJBObjectFactory.createClientEJB(ejbServerLocation, "IncomingQueueHistory", Long.parseLong(queue_oid));
                        msg_text = incoming_queue_history.getAttribute("msg_text");
                    }
                }

                DocumentHandler messageDoc = new DocumentHandler(msg_text, true, true);
                DocumentHandler processDoc = messageDoc.getFragment("/Proponix/SubHeader");
                processDoc.setAttribute("/SenderID", messageDoc.getAttribute("/Proponix/Header/SenderID"));

                outgoing_queue.setAttribute("reply_to_message_id", paymentUpload.getAttribute("message_id"));    //reply_to_message_id translates to message_id
                outgoing_queue.setAttribute("process_parameters", processDoc.toString());
                outgoing_queue.setAttribute("transaction_oid", trans.getAttribute("transaction_oid"));

                if (outgoing_queue.save() < 0) {
                    throw new AmsException("Cannot update outgoing_queue data.");
                }

            }
        } catch (Exception e) {
            logError("PaymentFileProcessor::postProcess:: Excception While setting attributes of outgoing_queue table", e);
        }
    }

    /**
     * Added for Rel 9.2 CR 988
     *
     * @param outputDoc
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean isCheckerEnabled(DocumentHandler outputDoc) throws RemoteException, AmsException {
        String checkerAuth = corpOrg.getAttribute("ftdp_checker_auth_ind");
        if (TradePortalConstants.INDICATOR_YES.equals(checkerAuth)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Added for Rel 9.2 CR 988 This method checks if the payment is future
     * dated which requires FX Contract but no FX Contract is present in the
     * file
     *
     * @param outputDoc
     * @return boolean
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean isFXContractPresent(DocumentHandler outputDoc) throws RemoteException, AmsException {

        User user = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", Long.parseLong(paymentUpload.getAttribute("user_oid")));
        String fromAcctOID = outputDoc.getAttribute("/Out/Terms/debit_account_oid");
        String transferCurrency = outputDoc.getAttribute("/Out/Terms/amount_currency_code");
        String fromCcy = "";
        if (StringFunction.isNotBlank(fromAcctOID)) {
            Account account = (Account) EJBObjectFactory.createClientEJB(ejbServerLocation, "Account", Long.parseLong(fromAcctOID));
            fromCcy = account.getAttribute("currency");
        }
        boolean isFXContractPresent = true;
        if ((StringFunction.isBlank(outputDoc.getAttribute("/Out/Terms/fec_rate")))
                && (!(transferCurrency.equalsIgnoreCase(fromCcy))) && TPDateTimeUtility.isFutureDate(outputDoc.getAttribute("/Out/Transaction/payment_date"), user.getAttribute("timezone"))) {
            isFXContractPresent = false;
        }

        return isFXContractPresent;
    }
    
    public void validateReportingCodes(DomesticPayment dp) 
    		throws RemoteException, AmsException
    		{ 
    			String paymentMethod = dp.getAttribute("payment_method_type");
    			Vector err = new Vector();
    			ErrorManager dpErrMgr = dp.getErrorManager();
    	        if (StringFunction.isBlank(debitAccountBankGroupOid))
    			{
    				dpErrMgr.issueError(getClass().getName(),
    						TradePortalConstants.REP_CODE_BANK_GRP_MISSING);
    				return;
    			}

    			//Rel9.4 CR-1001 Adding additional validations START
    			int currentReportCodeIndex = 0;
    			String reportingCodes[] = new String[2];
    			reportingCodes[0] = dp.getAttribute("reporting_code_1");
    			reportingCodes[1] = dp.getAttribute("reporting_code_2");
    			
    			
    			String reportingCode1ReqdForACH=null, reportingCode1ReqdForRTGS=null, reportingCode2ReqdForACH=null, reportingCode2ReqdForRTGS = null;
    			boolean rc1ReqdForACH=false, rc1ReqdForRTGS=false, rc2ReqdForACH=false, rc2ReqdForRTGS = false;
    			String sqlQuery = "select reporting_code1_reqd_for_ach, reporting_code1_reqd_for_rtgs, reporting_code2_reqd_for_ach, reporting_code2_reqd_for_rtgs"+
    						" from bank_organization_group where organization_oid=?";
    			DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{debitAccountBankGroupOid}); 
    	       	if(resultDoc!=null){
    				reportingCode1ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_ACH");
    				reportingCode1ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_RTGS");
    				reportingCode2ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_ACH");
    				reportingCode2ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_RTGS");
    	       	}
    	       	
    			if(TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)){
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForACH) && StringFunction.isBlank(reportingCodes[0])){
    					rc1ReqdForACH = true;
    				}
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForACH) && StringFunction.isBlank(reportingCodes[1])){
    					rc2ReqdForACH = true;
    				}
    				if(rc1ReqdForACH && rc2ReqdForACH){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_AND_2_REQUIRED , paymentMethod);
    				}else if(rc1ReqdForACH){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
    				}else if(rc2ReqdForACH){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
    				}
    			}
    			else if(TradePortalConstants.PAYMENT_METHOD_RTGS.equals(paymentMethod)){
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForRTGS) && StringFunction.isBlank(reportingCodes[0])){
    					rc1ReqdForRTGS = true;
    				}
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForRTGS) && StringFunction.isBlank(reportingCodes[1])){
    					rc2ReqdForRTGS = true;
    				}
    				if(rc1ReqdForRTGS && rc2ReqdForRTGS){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_AND_2_REQUIRED , paymentMethod);
    				}else if(rc1ReqdForRTGS){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
    				}else if(rc2ReqdForRTGS){
    					dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
    				}
    			}
    			

    			//Rel9.4 CR-1001 End
    			currentReportCodeIndex = 0;
    			List<String> repCodeErrors = new ArrayList<String>();
    			while (currentReportCodeIndex < TradePortalConstants.MAX_REP_CODE_IDX)
    			{
    				validateReportingCode(debitAccountBankGroupOid, reportingCodes[currentReportCodeIndex], Integer.toString(currentReportCodeIndex+1), dpErrMgr, repCodeErrors);
    				currentReportCodeIndex++;
    			}
    			if(repCodeErrors.size() == TradePortalConstants.MAX_REP_CODE_IDX)
    			{
    				dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE_1_AND_2 , reportingCodes[0], reportingCodes[1]);
    			}else if(repCodeErrors.contains(TradePortalConstants.INVALID_REPORTING_CODE1)){
    				dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE1 , reportingCodes[0]);
    			}else if(repCodeErrors.contains(TradePortalConstants.INVALID_REPORTING_CODE2)){
    				dpErrMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE2 , reportingCodes[1]);
    			}
    			
    			err.addAll(dpErrMgr.getIssuedErrors());

                if (err.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int index = 0; !(index >= err.size()); index++) {
                        com.amsinc.ecsg.frame.IssuedError errorObject = (com.amsinc.ecsg.frame.IssuedError) err.elementAt(index);
                        sb.append("- ").append(errorObject.getErrorText());
                    }
                    String lineNum = paymentCount + 1 + "";
			String bene_info = dp.getAttribute("payee_name") + "= " + dp.getAttribute("customer_reference") + "= " + dp.getAttribute("amount") + "= "
                            + lineNum;

                    Map<String, String> tempMap = new HashMap<String, String>();
                    tempMap.put(bene_info, sb.toString());
                    uploadRepLog.put(dp.getAttribute("domestic_payment_oid"), tempMap);

                } 
    		}


    		/**
    		 * This method validates payment's Reporting Code(s) entered on the screen
    		 * @param inputDoc com.amsinc.ecsg.util - the input doc
    		 * @param bankGroupOid String - Bank Group OID to check for codes
    		 * @param isForVerify boolean - If true, gets code values from bean rather than inputDoc
    		 * @param repCodeIndex String - Reporting Code Index.
    		 * @return void -- nothing is return; in case of error, an error condition is raised via ErrorManager
    		 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
    		 */
    		private void validateReportingCode(String bankGroupOid, String reportingCode, String repCodeIndex, ErrorManager errMgr, List<String> repCodeErrors)
    		throws RemoteException, AmsException
    		{
    			boolean repSelected = false;
    			//build sql: if reporting code was entered for this payment, validate it against reporting codes table for given bank group
    			// if it was not entered, verify it is not required (no codes stored for this bank group in the database)
    			String whereSqlStr = " p_bank_group_oid = ?";
    			List<Object> temp = new ArrayList<Object>();
    			Object intOb = new Object();
    			intOb = (bankGroupOid !=null)?Long.parseLong(bankGroupOid):bankGroupOid;
    			temp.add(intOb);
    			if (StringFunction.isNotBlank(reportingCode))
    			{
    				repSelected = true;
    				whereSqlStr += " and code = ?";
    				temp.add(reportingCode);
    			}

    			//obtain count.
    			int reportCount = DatabaseQueryBean.getCount("code", "PAYMENT_REPORTING_CODE_" + repCodeIndex, whereSqlStr, false, temp);

    			//if reporting code was selected on payment but was not found in the database, issue an error.
    			if (repSelected && reportCount < 1)
    			{
    				if("1".equals(repCodeIndex)){
    					repCodeErrors.add(TradePortalConstants.INVALID_REPORTING_CODE1);
    				}else if("2".equals(repCodeIndex)){
    					repCodeErrors.add(TradePortalConstants.INVALID_REPORTING_CODE2);
    				}
    				return ;
    			}
    			
    			return ;
    			
    		}
    		
    		/**
    		 * Added for CR-1001 Rel9.4 Fetch the BankGroup of the Debit account only once for validating reporting codes
    		 * @param dpErrMgr
    		 * @return
    		 * @throws AmsException
    		 * @throws RemoteException
    		 */
    		public String getBankGroupForAccount(ErrorManager dpErrMgr) throws AmsException,RemoteException{
    			
    			String debitAccountOid = terms.getAttribute("debit_account_oid");
    			String bankGroupOid = null;
    			if (StringFunction.isBlank(debitAccountOid))
    			{
    				return null;
    			}

    			Account debitAccount = (Account) EJBObjectFactory.createClientEJB(ejbServerLocation,"Account");
    			debitAccount.getData(Long.parseLong(debitAccountOid));
    			String opBankOrgOid = debitAccount.getAttribute("op_bank_org_oid");
    			if (StringFunction.isBlank(opBankOrgOid))
    			{
    				dpErrMgr.issueError(getClass().getName(),
    						TradePortalConstants.REP_CODE_OP_ORG_ID_MISSING);
    				return null;
    			}

    			OperationalBankOrganization opBankOrg = (OperationalBankOrganization) EJBObjectFactory.createClientEJB(ejbServerLocation,"OperationalBankOrganization",
    														  Long.parseLong(opBankOrgOid));
    			bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");

    			return bankGroupOid;
    		}

}
