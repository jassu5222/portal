/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.TimeZone;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;

public class PaymentFileWithInvoiceDetails extends AbstractPaymentFileDetail
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileWithInvoiceDetails.class);
	// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
	public static final int MAX_INV_DTL_CHAR = 83;
	public static final int MAX_INV_LINES = 1000; //manohar-6/15/2011 - CR-597 Scope Change- udpated to accept 1000 inv lines

    // Payment file header layout
    public static final int   HEADER_INDICATOR                                = 0;
    public static final int   DEBIT_ACCOUNT_NO                                = 1;
    public static final int   EXECUTION_DATE                                  = 2;
    public static final int   PAYMENT_CURRENCY                                = 3;
    public static final int   PAYMENT_METHOD                                  = 4;
    public static final int   FILE_REFERENCE                             	  = 5;
    public static final int   CONFIDENTIAL_INDICATOR                          = 6;
    public static final int   INDIVIDUAL_ACCT_ENTRIES                         = 7;

    //TODO: adjust the numbers accordingly to fit in these fields.
    //CR-921
    public static final int   FX_CONTRACT_NUMBER                              = 8;
    public static final int   FX_RATE                                         = 9;
    //end
    
    public static final int   PAYMENT_DETAIL_INDICATOR                        = 10;
    public static final int   PAYMENT_AMOUNT                                  = 11;
    public static final int   CUSTOMER_REFERENCE                              = 12;
    public static final int   BENEFICIARY_CODE                    			  = 13;
    public static final int   BENEFICIARY_NAME                                = 14;
    public static final int   BENEFICIARY_ACCOUNT_NO                          = 15;
    public static final int   BENEFICIARY_ADDRESS1                            = 16;
    public static final int   BENEFICIARY_ADDRESS2                            = 17;
    public static final int   BENEFICIARY_ADDRESS3                            = 18;
    public static final int   BENEFICIARY_ADDRESS4                            = 19;
    public static final int   COUNTRY                 			  			  = 20;
    public static final int   BENEFICIARY_FAX_NO                              = 21;
    public static final int   BENEFICIARY_EMAIL_ID                            = 22;
    public static final int   BENEFICIARY_BANK_BRANCH_CODE                    = 23;
    public static final int   BENEFICIARY_BANK_NAME                           = 24;
    public static final int   BENEFICIARY_BANK_BRANCH_NAME                    = 25;
    public static final int   BENEFICIARY_BANK_BRANCH_ADDRESS1                = 26;
    public static final int   BENEFICIARY_BANK_BRANCH_ADDRESS2                = 27;
    public static final int   BENEFICIARY_BANK_BRANCH_CITY                    = 28;
    public static final int   BENEFICIARY_BANK_BRANCH_PROVINCE                = 29;
    public static final int   BENEFICIARY_BANK_BRANCH_COUNTRY                 = 30;
    public static final int   CHARGES                                         = 31;
    public static final int   PAYABLE_LOCATION                                = 32;
    public static final int   PRINT_LOCATION                                  = 33;
    public static final int   DELIVERY_METHOD_DELIVER_TO                      = 34;
    public static final int   MAILING_ADDRESS1                                = 35;
    public static final int   MAILING_ADDRESS2                                = 36;
    public static final int   MAILING_ADDRESS3                                = 37;
    public static final int   MAILING_ADDRESS4                                = 38;
    public static final int   INSTRUCTION_NUMBER                              = 39;
    public static final int   PAYMENT_DETAILS                                 = 40;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_CODE             = 41;
    public static final int   FIRST_INTERMEDIARY_BANK_NAME                    = 42;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_NAME             = 43;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1         = 44;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2         = 45;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_CITY             = 46;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE         = 47;
    public static final int   FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY          = 48;
    public static final int   CENTRAL_BANK_REPORTING1                         = 49;
    public static final int   CENTRAL_BANK_REPORTING2                         = 50;
    public static final int   CENTRAL_BANK_REPORTING3                         = 51;

    public static final int   REPORTING_CODE1                         = 52;
    public static final int   REPORTING_CODE2                         = 53;
    public static final int   USER_DEFINED_FIELD1                     = 54;
    public static final int   USER_DEFINED_FIELD2                     = 55;
    public static final int   USER_DEFINED_FIELD3                     = 56;
    public static final int   USER_DEFINED_FIELD4                     = 57;
    public static final int   USER_DEFINED_FIELD5                     = 58;
    public static final int   USER_DEFINED_FIELD6                     = 59;
    public static final int   USER_DEFINED_FIELD7                     = 60;
    public static final int   USER_DEFINED_FIELD8                     = 61;
    public static final int   USER_DEFINED_FIELD9                     = 62;
    public static final int   USER_DEFINED_FIELD10                    = 63;

    public static final int   INVOICE_IND			                  = 64;
    public static final int   INVOICE_DETAIL                          = 65;    

    //CR-921 - Adding two more header fields to make a total of 10 header fields.
    //public static final int NUM_HEADER_FIELDS = 8;
    public static final int NUM_HEADER_FIELDS = 10;
    //
    public static final int NUM_PAYMENT_FIELDS = USER_DEFINED_FIELD10 - NUM_HEADER_FIELDS + 1;


    // max # of fields per line.
	public static final int   MAX_PAYMENT_FILE_FIELDS_NUMBER                  = INVOICE_DETAIL + 1;
    // Size of each field.  Only applicable to text fields
	public static int[] fieldSize = new int[MAX_PAYMENT_FILE_FIELDS_NUMBER];
	// The TextResrouce key of the name of the fields, used for error message.
	public static String[] fieldNameKey = new String[MAX_PAYMENT_FILE_FIELDS_NUMBER];

	static {
        // Static initialization of fieldSize[]

		fieldSize[HEADER_INDICATOR]                                = 3;
		fieldSize[DEBIT_ACCOUNT_NO]                                = 30;
		fieldSize[EXECUTION_DATE]                                  = 10;
		fieldSize[PAYMENT_CURRENCY]                                = 3;
        fieldSize[PAYMENT_METHOD]                                  = 4;
        fieldSize[FILE_REFERENCE]                                  = 35;
        fieldSize[CONFIDENTIAL_INDICATOR]						   = 1;
        fieldSize[INDIVIDUAL_ACCT_ENTRIES]						   = 1;
        //CR-921
        fieldSize[FX_CONTRACT_NUMBER]						       = 14;
        fieldSize[FX_RATE]						   				   = 14;       
        //

        fieldSize[PAYMENT_DETAIL_INDICATOR]						   = 3;
        fieldSize[PAYMENT_AMOUNT]                                  = 19;
        fieldSize[CUSTOMER_REFERENCE]                              = 20;
        fieldSize[BENEFICIARY_CODE]                                = 15;
        fieldSize[BENEFICIARY_NAME]                                = 120;
        fieldSize[BENEFICIARY_ACCOUNT_NO]                          = 34;
        fieldSize[BENEFICIARY_ADDRESS1]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS2]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS3]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS4]                            = 35;
        fieldSize[COUNTRY]                             			   = 2;
        fieldSize[BENEFICIARY_FAX_NO]                              = 15;
        fieldSize[BENEFICIARY_EMAIL_ID]                            = 255;
        fieldSize[BENEFICIARY_BANK_BRANCH_CODE]                    = 35;
        fieldSize[BENEFICIARY_BANK_NAME]                           = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_NAME]                    = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS1]                = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS2]                = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_CITY]                    = 31;
        fieldSize[BENEFICIARY_BANK_BRANCH_PROVINCE]                = 8;
        fieldSize[BENEFICIARY_BANK_BRANCH_COUNTRY]                 = 2;
        fieldSize[CHARGES]                                         = 3;
        fieldSize[PAYABLE_LOCATION]                                = 20;
        fieldSize[PRINT_LOCATION]                                  = 20;
        fieldSize[DELIVERY_METHOD_DELIVER_TO]                      = 2;
        fieldSize[MAILING_ADDRESS1]                                = 35;
        fieldSize[MAILING_ADDRESS2]                                = 35;
        fieldSize[MAILING_ADDRESS3]                                = 35;
        fieldSize[MAILING_ADDRESS4]                                = 35;
        fieldSize[INSTRUCTION_NUMBER]                              = 10;
        fieldSize[PAYMENT_DETAILS]                                 = 140;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_CODE]             = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_NAME]                    = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_NAME]             = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1]         = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2]         = 35;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_CITY]             = 31;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE]         = 8;
        fieldSize[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY]          = 2;
        fieldSize[CENTRAL_BANK_REPORTING1]                         = 35;
        fieldSize[CENTRAL_BANK_REPORTING2]                         = 35;
        fieldSize[CENTRAL_BANK_REPORTING3]                         = 35;

        fieldSize[REPORTING_CODE1]                         = 3;
        fieldSize[REPORTING_CODE2]                         = 3;
        fieldSize[USER_DEFINED_FIELD1]                         = 35;
        fieldSize[USER_DEFINED_FIELD2]                         = 35;
        fieldSize[USER_DEFINED_FIELD3]                         = 35;
        fieldSize[USER_DEFINED_FIELD4]                         = 35;
        fieldSize[USER_DEFINED_FIELD5]                         = 35;
        fieldSize[USER_DEFINED_FIELD6]                         = 35;
        fieldSize[USER_DEFINED_FIELD7]                         = 35;
        fieldSize[USER_DEFINED_FIELD8]                         = 35;
        fieldSize[USER_DEFINED_FIELD9]                         = 35;
        fieldSize[USER_DEFINED_FIELD10]                        = 35;
        fieldSize[INVOICE_IND]                        = 3;
        fieldSize[INVOICE_DETAIL]                        = 81000; // manohar - 6/15/2011 - updated from 8000 to 80000 to accept invoice details from payment page
        															//cjr - 8/3/2011 - updated to 81000 to include counting of newline characters in the fixed file format.



        fieldNameKey[HEADER_INDICATOR]                                = "";
        fieldNameKey[DEBIT_ACCOUNT_NO]                                = "DomesticPaymentRequest.DebitFrom";
        fieldNameKey[EXECUTION_DATE]                                  = "DomesticPaymentRequest.ExecutionDate";
        fieldNameKey[PAYMENT_CURRENCY]                                = "DomesticPaymentRequest.PaymentCurrency";
        fieldNameKey[PAYMENT_METHOD]                                  = "DomesticPaymentRequest.PaymentMethod";
        fieldNameKey[FILE_REFERENCE]                                  = "DomesticPaymentRequest.CustUploadFileRef";
        fieldNameKey[CONFIDENTIAL_INDICATOR]						  = "DomesticPaymentRequest.ConfidentialIndicator";
        fieldNameKey[INDIVIDUAL_ACCT_ENTRIES]						  = "";
        
        //CR-921 start
        fieldNameKey[FX_CONTRACT_NUMBER]                              = "DomesticPaymentRequest.FxContractNumber";
        fieldNameKey[FX_RATE]						  				  = "DomesticPaymentRequest.FxRate";        
        //CR-921 end

        fieldNameKey[PAYMENT_DETAIL_INDICATOR]						   = "";
        fieldNameKey[PAYMENT_AMOUNT]                                  = "DomesticPaymentRequest.PaymentAmount";
        fieldNameKey[CUSTOMER_REFERENCE]                              = "DomesticPaymentRequest.CustomerReference";;
        fieldNameKey[BENEFICIARY_CODE]                                = "DomesticPaymentRequest.Beneficiary";
        fieldNameKey[BENEFICIARY_NAME]                                = "DomesticPaymentRequest.PayeeName";
        fieldNameKey[BENEFICIARY_ACCOUNT_NO]                          = "DomesticPaymentRequest.PayeeAccountNumber";
        fieldNameKey[BENEFICIARY_ADDRESS1]                            = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS2]                            = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS3]                            = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[BENEFICIARY_ADDRESS4]                            = "DomesticPaymentRequest.PayeeAddress";
        fieldNameKey[COUNTRY]                            			  = "DomesticPaymentRequest.Country";
        fieldNameKey[BENEFICIARY_FAX_NO]                              = "DomesticPaymentRequest.BeneFaxNum";
        fieldNameKey[BENEFICIARY_EMAIL_ID]                            = "DomesticPaymentRequest.BeneEmail";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CODE]                    = "DomesticPaymentRequest.PayeeBankCode";
        fieldNameKey[BENEFICIARY_BANK_NAME]                           = "DomesticPaymentRequest.PayeeBankName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_NAME]                    = "DomesticPaymentRequest.PayeeBankBranchName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS1]                = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS2]                = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CITY]                    = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_PROVINCE]                = "DomesticPaymentRequest.PayeeBankAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_COUNTRY]                 = "DomesticPaymentRequest.Country";
        fieldNameKey[CHARGES]                                         = "DomesticPaymentRequest.BankCharges";
        fieldNameKey[PAYABLE_LOCATION]                                = "DomesticPaymentRequest.PayableLocation";
        fieldNameKey[PRINT_LOCATION]                                  = "DomesticPaymentRequest.PrintLocation";
        fieldNameKey[DELIVERY_METHOD_DELIVER_TO]                      = "DomesticPaymentRequest.DeliveryMethodTo";
        fieldNameKey[MAILING_ADDRESS1]                                = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS2]                                = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS3]                                = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[MAILING_ADDRESS4]                                = "DomesticPaymentRequest.MailingAdress";
        fieldNameKey[INSTRUCTION_NUMBER]                              = "DomesticPaymentRequest.BeneInstructionsNum";
        fieldNameKey[PAYMENT_DETAILS]                                 = "DomesticPaymentRequest.PaymentDetails";


        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_CODE]             = "DomesticPaymentRequest.FirstIntBankBranchCode";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_NAME]                    = "DomesticPaymentRequest.FirstIntBankTheName";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_NAME]             = "DomesticPaymentRequest.FirstIntBankBranchName";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1]         = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2]         = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_CITY]             = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_PROVINCE]         = "DomesticPaymentRequest.FirstIntBankAddress";
        fieldNameKey[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY]          = "DomesticPaymentRequest.Country";

        fieldNameKey[CENTRAL_BANK_REPORTING1]                         = "DomesticPaymentRequest.CentraBankReporting";
        fieldNameKey[CENTRAL_BANK_REPORTING2]                         = "DomesticPaymentRequest.CentraBankReporting";
        fieldNameKey[CENTRAL_BANK_REPORTING3]                         = "DomesticPaymentRequest.CentraBankReporting";

        fieldNameKey[REPORTING_CODE1]                         = "DomesticPaymentRequest.ReportingCode1";
        fieldNameKey[REPORTING_CODE2]                         = "DomesticPaymentRequest.ReportingCode2";
        fieldNameKey[USER_DEFINED_FIELD1]                         = "DomesticPaymentRequest.UserDefinedField1";
        fieldNameKey[USER_DEFINED_FIELD2]                         = "DomesticPaymentRequest.UserDefinedField2";
        fieldNameKey[USER_DEFINED_FIELD3]                         = "DomesticPaymentRequest.UserDefinedField3";
        fieldNameKey[USER_DEFINED_FIELD4]                         = "DomesticPaymentRequest.UserDefinedField4";
        fieldNameKey[USER_DEFINED_FIELD5]                         = "DomesticPaymentRequest.UserDefinedField5";
        fieldNameKey[USER_DEFINED_FIELD6]                         = "DomesticPaymentRequest.UserDefinedField6";
        fieldNameKey[USER_DEFINED_FIELD7]                         = "DomesticPaymentRequest.UserDefinedField7";
        fieldNameKey[USER_DEFINED_FIELD8]                         = "DomesticPaymentRequest.UserDefinedField8";
        fieldNameKey[USER_DEFINED_FIELD9]                         = "DomesticPaymentRequest.UserDefinedField9";
        fieldNameKey[USER_DEFINED_FIELD10]                        = "DomesticPaymentRequest.UserDefinedField10";

        fieldNameKey[INVOICE_IND]                        = "";
        fieldNameKey[INVOICE_DETAIL]                        = "DomesticPaymentRequest.BeneInvoiceDetails";
    }


	public static String[] parseHeaderLine(String headerLine){
		String[] headerRecord = new String[NUM_HEADER_FIELDS];

		int lineIndex = 0;
		for(int i = 0; i < NUM_HEADER_FIELDS; i++){
			int fieldLength = fieldSize[i];
			int endIndex = lineIndex + fieldLength;
			if(endIndex > headerLine.length()){
				//Rel9.0 IR#T36000028275
				//endIndex can be greater than the headerLine length in a case where
				//the header field length can vary. Example: max length is say 10, but only a field of length 4 is entered.
				//in this case, the endindex will be 6 longer. So, in such a case, just assign headerLine length as the endindex
				endIndex = headerLine.length();
				//headerRecord[i] = "";
			}//else{
				if(fieldLength > 0){
					try {
						headerRecord[i] = headerLine.substring(lineIndex, endIndex).trim();
					} catch (Exception e) {
						//default to empty header record in case of an error
						//then fail gracefully
						headerRecord[i] = "";
						LOG.debug("Error in PaymentFileWithInvoiceDetails.parseHeaderLine: "+e);
						e.printStackTrace();
					}
				}
			//}
			lineIndex = endIndex;
		}
        return headerRecord;
	}



	/**
	 * Parse one payment line item.
	 *
	 * @param currentPaymentLineItem
	 * @param resourceManager
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 */
	public static String[] parsePaymentRecord(String[] headerRecord, String[] paymentLines) throws AmsException {

		// First apply the header rows to the payment record.
		String[] paymentRecord = new String[MAX_PAYMENT_FILE_FIELDS_NUMBER];
		for(int i = 0; i < NUM_HEADER_FIELDS; i++){
			paymentRecord[i] = headerRecord[i];
		}

		// Now parse the first line, it should always begin with 'PAY'.  There can only be one 'PAY'
		String paymentLine = paymentLines[0];
		int lineIndex=0;
		for(int i = NUM_HEADER_FIELDS; i < (NUM_PAYMENT_FIELDS + NUM_HEADER_FIELDS); i++){
			int fieldLength = fieldSize[i];
			int endIndex = lineIndex + fieldLength;
			if(endIndex > paymentLine.length()){
				paymentRecord[i] = "";
			}else{
				if(fieldLength > 0){
					paymentRecord[i] = paymentLine.substring(lineIndex, endIndex).trim();

				}
			}
			lineIndex = endIndex;
		}

		// Now join all the invoice details lines together and apply them to the payment record.  If they exist
		if(paymentLines[1] != null){
			StringBuilder invoiceDetails = new StringBuilder(1000);
			int i = 1;
			while(i<1001 && paymentLines[i] != null){ // manohar - SEUL041149384 - to check max of 100 invoice details
				invoiceDetails.append(paymentLines[i].substring(3)).append("\n");
				i++;
			}

			paymentRecord[INVOICE_DETAIL] = invoiceDetails.toString();
		}else{
			paymentRecord[INVOICE_DETAIL] = "";
		}

		return paymentRecord;
	}

	
	/**
	 * Verifies the Payment Line Item.  Doing the validation here instead of
	 * at the business object user_validate() gives better error message information.
	 *
	 * @exception  com.amsinc.ecsg.frame.AmsException
	 * @return     boolean - indicates whether or not a valid Payment Data file
	 *                       (true  - valid
	 *                        false - invalid)
	 */
	public static boolean validatePaymentLineItem(String[] paymentRecord, int paymentCount,  ResourceManager resourceManager, MediatorServices mediatorServices)
	throws AmsException {

		boolean invalidFormat = false;
		String[] substitutionValues = {"", ""};

		if (paymentRecord == null) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_FORMAT_NOT_PIPE_DELIMITED);
			return true;
		}

		//MANOHAR REL 7.1 CR 640 - BEGINS
		//boolean fxOnlineInd = validateFxOnlineAvailInd(paymentRecord);
		if(paymentCount==0){
		String fxOnlineInd = returnFxOnlineAvailInd(paymentRecord);
			if(null != fxOnlineInd && fxOnlineInd.equalsIgnoreCase(TradePortalConstants.INDICATOR_NO)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	                    TradePortalConstants.MARKET_RATE_NOT_AVAILABLE_FOR_ACCOUNT);
				invalidFormat = true;
			}//MANOHAR REL 7.1 CR 640 - ENDS
		}
		
		//Payment Amount
		String paymentAmount = paymentRecord[PAYMENT_AMOUNT];
		if (StringFunction.isNotBlank(paymentAmount))  {
			try
			{
				// Convert the amount from its localized form to en_US form
				paymentRecord[PAYMENT_AMOUNT] =  NumberValidator.getNonInternationalizedValue(paymentRecord[PAYMENT_AMOUNT], resourceManager.getResourceLocale(), false);
				if ((StringFunction.isNotBlank(paymentRecord[PAYMENT_AMOUNT]))&&(new Float(paymentRecord[PAYMENT_AMOUNT]).intValue() != 0))
				{
					TPCurrencyUtility.validateAmount(paymentRecord[PAYMENT_CURRENCY],
							paymentRecord[PAYMENT_AMOUNT],
							resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE),
							mediatorServices.getErrorManager() );
				}
			}
			catch(InvalidAttributeValueException iae)
			{
				substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE);
				substitutionValues[1] = paymentRecord[PAYMENT_AMOUNT];
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_ERR, substitutionValues);
				invalidFormat = true;
			}

			if (mediatorServices.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
			{
				invalidFormat = true;
			}

		}

		// Check the length of text field.
		for (int i = 0; i< MAX_PAYMENT_FILE_FIELDS_NUMBER; i++) {
			//Rel9.0 IR#T36000028324 - Do not do FX Rate and Contract Number validations here.
			//They are validated elsewhere with a more specific error message. - [START]
			if(i == FX_RATE || i == FX_CONTRACT_NUMBER) continue;
			//-[END]
			if (paymentRecord[i] != null && fieldSize[i] > 0 && paymentRecord[i].length() > fieldSize[i]) {
				String fieldName = resourceManager.getText(fieldNameKey[i], TradePortalConstants.TEXT_BUNDLE);
				switch (i) {
				case BENEFICIARY_ADDRESS1: fieldName += " 1"; break;
				case BENEFICIARY_ADDRESS2: fieldName += " 2"; break;
				case BENEFICIARY_ADDRESS3: fieldName += " 3"; break;
				case BENEFICIARY_ADDRESS4: fieldName += " 4"; break;
				case BENEFICIARY_BANK_BRANCH_ADDRESS1: fieldName += " 1"; break;
				case BENEFICIARY_BANK_BRANCH_ADDRESS2: fieldName += " 2"; break;

				case FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS1: fieldName += " 1"; break;
				case FIRST_INTERMEDIARY_BANK_BRANCH_ADDRESS2: fieldName += " 2"; break;


				case CENTRAL_BANK_REPORTING1: fieldName += " 1"; break;
				case CENTRAL_BANK_REPORTING2: fieldName += " 2"; break;
				case CENTRAL_BANK_REPORTING3: fieldName += " 3"; break;
				}
				substitutionValues[0] = fieldName;
				substitutionValues[1] = String.valueOf(paymentCount + 1);
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FIELD_TOO_LONG, substitutionValues);
				invalidFormat = true;
			}
		}

		// Additional edit for address field length if payment method = CBFT.
		if (TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentRecord[PAYMENT_METHOD])) {
			if (paymentRecord[BENEFICIARY_ADDRESS4] != null && paymentRecord[BENEFICIARY_ADDRESS4].length() > 0) {
				// CJR 04/25/2011 - IR SBUL040836986 Begin
				substitutionValues[0] = String.valueOf(paymentCount + 1);
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CBFT_BENE_ADD_LINE_4_BLANK, substitutionValues);
				// CJR 04/25/2011 - IR SBUL040836986 End
				invalidFormat = true;
			}
			if (paymentRecord[BENEFICIARY_ADDRESS3] != null && paymentRecord[BENEFICIARY_ADDRESS3].length() > 32) {
				substitutionValues[0] = resourceManager.getText(fieldNameKey[BENEFICIARY_ADDRESS3], TradePortalConstants.TEXT_BUNDLE) + " 3";
				substitutionValues[1] = String.valueOf(paymentCount + 1);
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FIELD_TOO_LONG, substitutionValues);
				invalidFormat = true;
			}
		}

		// Instruction Number can only be number.
		if (!StringFunction.isBlank(paymentRecord[INSTRUCTION_NUMBER]) && !paymentRecord[INSTRUCTION_NUMBER].matches("^\\d*$")) {
			substitutionValues[1] = resourceManager.getText(fieldNameKey[INSTRUCTION_NUMBER], TradePortalConstants.TEXT_BUNDLE);;
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.INVALID_NUMBER_ATTRIBUTE, substitutionValues);
			invalidFormat = true;
		}


		// Validate First/Second Intermediary Bank Country.
		if(paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY]!=null
				&& !paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY].trim().equals("")
				&& !ReferenceDataManager.getRefDataMgr().checkCode("COUNTRY", paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY])){
			substitutionValues[0] = paymentRecord[FIRST_INTERMEDIARY_BANK_BRANCH_COUNTRY];
			substitutionValues[1] = resourceManager.getText("DomesticPaymentRequest.FirstIntBankCountry", TradePortalConstants.TEXT_BUNDLE);
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.INVALID_ATTRIBUTE, substitutionValues);
			invalidFormat = true;
		}

		if (paymentRecord[CONFIDENTIAL_INDICATOR] == null || paymentRecord[CONFIDENTIAL_INDICATOR].trim().equals("")) {
			// CJR - 04/25/2011 - IR PAUL042052906 BEGIN

			paymentRecord[CONFIDENTIAL_INDICATOR] = TradePortalConstants.INDICATOR_NO;
			//CJR - IR PAUL042052906 END
		}else if(!(InstrumentServices.validateIndicator(paymentRecord[CONFIDENTIAL_INDICATOR].trim(), false))){ //RKAZI IR RRUL052435898 05/26/2011 
																												//- boolean added to avoid ignore case
			substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.ConfidentialIndicator", TradePortalConstants.TEXT_BUNDLE);
			substitutionValues[1] = paymentRecord[CONFIDENTIAL_INDICATOR];
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_VALUE, substitutionValues);
			invalidFormat = true;
		}
		
		if (paymentRecord[INDIVIDUAL_ACCT_ENTRIES] == null || StringFunction.isBlank(paymentRecord[INDIVIDUAL_ACCT_ENTRIES])) {
			paymentRecord[INDIVIDUAL_ACCT_ENTRIES]=TradePortalConstants.INDICATOR_NO;
		}else if(!(InstrumentServices.validateIndicator(paymentRecord[INDIVIDUAL_ACCT_ENTRIES].trim(),true))){ //RKAZI IR RRUL052435898 05/26/2011 
																											   //- boolean added to use ignorecase
			substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.IndAccountingEntries", TradePortalConstants.TEXT_BUNDLE);
			substitutionValues[1] = paymentRecord[INDIVIDUAL_ACCT_ENTRIES];
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_VALUE, substitutionValues);
			invalidFormat = true;
		}
		
		return invalidFormat;
	}

	protected static boolean validateRequiredField(String[] paymentRecord, int field,int paymentCount,ResourceManager resourceManager, MediatorServices mediatorServices) throws AmsException {
		boolean isValid = true;
		String[] substitutionValues = {"", ""};
		if (paymentRecord[field] == null || paymentRecord[field].trim().equals("")) {
			substitutionValues[0] = resourceManager.getText(fieldNameKey[field], TradePortalConstants.TEXT_BUNDLE);
			substitutionValues[1] = String.valueOf(paymentCount+1);
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
			isValid = false;
		}
		return isValid;
	}


	/**
	 * This method returns the country code for a given account, using either the bank_country_code
	 * stored with the account, or the translated ISO country code from REFDATA.
	 *
	 * @param accountOid primary key to Account
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return String bank branch country if available.
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	public static String getBankBranchCountryForAccount(String accountOid) throws RemoteException, AmsException {

		String country = null;

		if (StringFunction.isNotBlank(accountOid))
		{
			String selectCountrySQL = "SELECT ADDL_VALUE FROM REFDATA WHERE CODE = (select a.BANK_COUNTRY_CODE from account a where a.account_oid = ? )AND TABLE_TYPE = ?";

            long startGetDACCode = System.currentTimeMillis();
			DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(selectCountrySQL.toString(), false,new Object[]{accountOid, TradePortalConstants.COUNTRY_ISO3116_3});

			LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] "
							+"\t[PERFORMANCE]"
							+"\tgetDebitAccountCountryCode"
							+"\t"+(System.currentTimeMillis()-startGetDACCode)
				+"\tmilliseconds");

			if (countryDoc != null)
				country = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");
		}
		return country;
     }
	//manohar - SBUL040548203 - 04/14/2011 -  begin	
	/**
	 * Get the execution date.
	 *
	 * i.   If execution date is found it will be used for all beneficiaries
	 * ii.  If no execution date is provided, the system will set the execution date to the current date
	 * iii. Validate that the date is on the correct format.
	 *
	 * @return     Date execution_date)
	 */
	public static boolean validateExecutionDate(String executionDate, int paymentCount,ResourceManager resourceManager, MediatorServices mediatorServices) throws AmsException {
		
		Date execDate = null;
		boolean invalidFormat = false;
		String[] substitutionValues = {"", ""};

		if (executionDate == null || executionDate.trim().equals("")) {
			// If no execution date is provided, set the execution date to the current date
			execDate = GMTUtility.getGMTDateTime(true);
			execDate = TPDateTimeUtility.getLocalDateTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(execDate), TimeZone.getDefault().toString());
		} else {
			execDate = TPDateTimeUtility.convertDateStringToDate(executionDate.trim(), "dd/MM/yyyy");
			LOG.debug("PaymentFileUploadServlet: getExecutionDate converted [ " + executionDate + "]");
			// 2-digt year is not allowed.  execDate.getYear() returns year based on year 1900.  So any year < 1000 would have getYear() < -900.
			if (execDate == null || execDate.getYear() < -900) {
				execDate = null;
			}
		}
		if (execDate == null) {
			//substitutionValues[0] = resourceManager.getText("DomesticPaymentRequest.ExecutionDate", TradePortalConstants.TEXT_BUNDLE);
			substitutionValues[0] = String.valueOf(paymentCount+1);
			substitutionValues[1] = resourceManager.getText("DomesticPaymentRequest.ExecutionDate", TradePortalConstants.TEXT_BUNDLE);
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_DATE, substitutionValues, null);
			invalidFormat = true;
			
		}
		return invalidFormat;
		
	}
	//manohar - SBUL040548203 - 04/14/2011 -  end

	
	public static boolean validatePaymentCurrencyCountry(MediatorServices mediatorServices, String[] paymentRecord) throws AmsException{
		boolean invalidFormat = false;
		StringBuilder whereClause = new StringBuilder();
		
		String paymentMethod = paymentRecord[PAYMENT_METHOD];
		String paymentCurrency = paymentRecord[PAYMENT_CURRENCY];
		String debitAccountNo = paymentRecord[DEBIT_ACCOUNT_NO];

		
		String debitAccountBankCountryCode =null;
		String debitAccountOid = null;
		
		String accountSqlStr = "select account_oid from account where account_number = ? ";         // NSX - 07/01/10 PDUK030948788
		DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, new Object[]{debitAccountNo.trim()});
		if ( acctResultSet != null && !StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID"))) {
			debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID");
		}
		
		try {
			debitAccountBankCountryCode = PaymentFileWithInvoiceDetails.getBankBranchCountryForAccount(debitAccountOid);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Object> sqlParams = new ArrayList<>();
		whereClause.append(" PAYMENT_METHOD = ? AND ");
		sqlParams.add(paymentMethod);
		if (StringFunction.isNotBlank(debitAccountBankCountryCode)) {
		   whereClause.append("COUNTRY = ? AND ");
		   sqlParams.add(debitAccountBankCountryCode);
		}
		whereClause.append("CURRENCY = ?");
		sqlParams.add(paymentCurrency); //Rel9.3 IR 39182
		StringBuffer selectOffsetdays = (new StringBuffer("select offset_days from PAYMENT_METHOD_VALIDATION where")).append(whereClause);

		DocumentHandler paymentMethodRec = DatabaseQueryBean.getXmlResultSet(selectOffsetdays.toString(), false, sqlParams);
		if (paymentMethodRec == null)
		{
			String[] substitutionValue = {"", "", ""};
			substitutionValue[0] = paymentMethod;
			substitutionValue[1] = paymentCurrency;
			substitutionValue[2] = debitAccountBankCountryCode;
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_METHOD_RULE_COMBINATION,
					substitutionValue);
			invalidFormat = true;
		}
		//manohar - SBUL040548203 - 04/14/2011 -  end

		return invalidFormat;
	}
	
	// Rel 7.1 - CR-640-581 - manohar - Begin
	public static String returnFxOnlineAvailInd(String[] paymentRecord) throws AmsException{
		DocumentHandler results = null;
		String fxOnlineInd = null;
		String opOrgId = null;
		if(StringFunction.isNotBlank(paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD1]) && paymentRecord[PaymentFileWithInvoiceDetails.USER_DEFINED_FIELD1].equalsIgnoreCase(TradePortalConstants.INDICATOR_YES)){
			String sqlStatement = "select A_OP_BANK_ORG_OID from ACCOUNT where account_number = ? ";
			results = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{paymentRecord[PaymentFileWithInvoiceDetails.DEBIT_ACCOUNT_NO].trim()});

				if(results!=null){
				opOrgId = results.getAttribute("/ResultSetRecord(0)/A_OP_BANK_ORG_OID");
				String sqlQuery = "select FX_ONLINE_AVAIL_IND from BANK_ORGANIZATION_GROUP where ORGANIZATION_OID = "+
					"(select A_BANK_ORG_GROUP_OID from OPERATIONAL_BANK_ORG where ORGANIZATION_OID = ?)";
				results = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, new Object[]{opOrgId});
					if ( results != null){
						fxOnlineInd = results.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND");
					}
				}
		}

		return fxOnlineInd;
	}// Rel 7.1 - CR-640-581 - manohar - End
}
