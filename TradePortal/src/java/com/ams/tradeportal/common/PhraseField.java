package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a fill-in-the-blank field on a bank standard wording
 * phrase.  Each field consists of a label and a length.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PhraseField {
private static final Logger LOG = LoggerFactory.getLogger(PhraseField.class);
	private java.lang.String label = "";
	private java.lang.String length = "";
	public final static java.lang.String MAX_LENGTH = "35";
/**
 * PhraseField constructor comment.
 */
public PhraseField() {
	super();
}
/**
 * Getter for the label attribute
 * 
 * @return java.lang.String
 */
public java.lang.String getLabel() {
	return label;
}
/**
 * Getter for the length attribute
 *
 * @return java.lang.String
 */
public java.lang.String getLength() {
	return length;
}
/**
 * Setter for the label attribute
 * 
 * @param newLabel java.lang.String
 */
public void setLabel(java.lang.String newLabel) {
	label = newLabel;
}
/**
 * Setter for the length attribute.
 * 
 * @param newLength java.lang.String
 */
public void setLength(java.lang.String newLength) {
	length = newLength;
}
}
