package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * PhraseParser is a specific version of a StringTokenizer for use with
 * bank standard wording phrases that contain fill-in-the-blank fields.
 * After creating the class, the getNextField method can be called 
 * repeatedly (until null is returned) to get the next field in the phrase.
 * Fields are identifed by [label;length].  '[' represents the start of the
 * field, 'label' is the label to display to the user on the web page,
 * ';length' is the optional separator and max allowed length for the
 * user entered value, and ']' represents the end of the field.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public class PhraseParser {
private static final Logger LOG = LoggerFactory.getLogger(PhraseParser.class);
	private StringTokenizer tokenizer;
/**
 * Creates an instance of the PhraseParser with the given text.
 * 
 * @param text java.lang.String
 */
public PhraseParser(String text) {
	// Set up the tokenizer with the given text.  The delimiters are the
	// [ and ] characters.  We will returns the delimiters as tokens
	// because this allows us to determine the start and end of the 
	// fields.	
	tokenizer = new StringTokenizer(text, "[]", true);
}
/**
 * Returns the next field in the phrase text.  If no more fields exist,
 * null is returned.
 *
 * @return com.ams.tradeportal.common.PhraseField
 */
public PhraseField getNextField() {

	String token;
	boolean fieldFound = false;
	PhraseField field = new PhraseField();
	int semicolon = 0;
		
	while (tokenizer.hasMoreTokens()) {
		token = tokenizer.nextToken();

		if (token.equals("[")) {
			// We have found the start of a field.
			fieldFound = true;
		} else if (token.equals("]")) {
			// We have found the end of field marker
			fieldFound = false;
		} else if (fieldFound) {
			// The token is a field.  Attempt to parse for the ;
			// character indicating the length.  If not found,
			// default to the max length.
			semicolon = token.indexOf(";");
			if (semicolon > -1) {
				field.setLabel(token.substring(0,semicolon));
				field.setLength(token.substring(semicolon+1));
			} else {
				field.setLabel(token);
				field.setLength(PhraseField.MAX_LENGTH);
			}

			// Since we found a field, return it.  
			return field;
		}
	}

	// No more fields (i.e., end of text) so return null;
	return null;
}
}
