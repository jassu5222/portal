package com.ams.tradeportal.common;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.ejb.EJBObject;

import com.ams.tradeportal.busobj.PurchaseOrder;
import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.ams.tradeportal.busobj.PurchaseOrderFileUpload;
import com.ams.tradeportal.busobj.PurchaseOrderLineItem;
import com.ams.tradeportal.mediator.StructuredPOCreateMediator;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.Logger;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

public class PurchaseOrderFileProcessor {
private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(PurchaseOrderFileProcessor.class);

    //provide a upper limit for duration seconds to avoid failing the entire record if calculations are out of bounds	

    private static long MAX_DURATION_SECONDS = 9999999999L;
    private String datetime_process_start = "";
    private final String ejbServerLocation;
    private final ClientServerDataBridge csdb;
    private final long purchaseOrderFileOid;
    private PropertyResourceBundle bundle = null;

    private String process_id = null;
    private File purchaseOrderFile = null;
    private String fileName = null;
    private Map uploadLog = new HashMap();
    private Date valStartTime = null;
    private PurchaseOrderFileUpload purchaseOrderFileUpload = null;
    private ServerDeploy serverDeploy = null;
    private PurchaseOrderDefinition purchaseOrderDefinition = null;
    private String delimiterChar = null;
    private int total_purchaseOrders = 0;
    private int total_failed_purchaseOrders = 0;
    private int ctr = 0;
    private int failed = 0;
    private final String folder = TradePortalConstants.getPropertyValue("AgentConfiguration", "uploadFolder", "c:/temp/");
    protected static final String loggerCategory = TradePortalConstants.getPropertyValue("AgentConfiguration", "loggerServerName", "NoLogCategory");
    private transient String securityRights = null;

    public PurchaseOrderFileProcessor(String process_id, long oidLong,
            String ejbServerLocation, ClientServerDataBridge csdb) {
        this.ejbServerLocation = ejbServerLocation;
        this.csdb = csdb;
        this.purchaseOrderFileOid = oidLong;
        this.process_id = process_id;
        try {
			serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(ejbServerLocation, "ServerDeploy");
		} catch (Exception e) {
			logError("Error obtaining ServerDeploy." , e);
		} 
    }

    /**
     * Processes uploaded PurchaseOrder file saved in temporary folder.
     *
     * @return boolean true - success false = failure
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws javax.ejb.RemoveException
     * @throws java.io.IOException
     */
    public boolean processPurchaseOrderFile() throws AmsException,
            IOException {

        boolean success = false;
        String sLine = null;
        BufferedReader reader = null;
        String[] record = null;
        Set poNumSet = new HashSet<String>();
        Set savedPOs = new HashSet<String>();
        boolean isPartiallySuccessful = false;
        try {

            init();

            String poUploadParams = purchaseOrderFileUpload.getAttribute("po_upload_parameters");
            DocumentHandler poUploadParamsDoc = new DocumentHandler(poUploadParams, false);
            securityRights = poUploadParamsDoc.getAttribute("/In/User/securityRights");
            String autoCreated = poUploadParamsDoc.getAttribute("/In/PurchaseOrderUploadInfo/AutoCreated");

            if (!(TradePortalConstants.INDICATOR_YES.equals(autoCreated))) {

                // get the Purchase Order Definition for the uploaded file.
                purchaseOrderDefinition = getPOUploadDef(purchaseOrderFileUpload.getAttribute("a_upload_definition_oid"));

                // read the uploaded file.
                reader = getInputReader();
                // read the first line.
                sLine = reader.readLine();

                MediatorServices mediatorServices = new MediatorServices();
                mediatorServices.setCSDB(csdb);

                PurchaseOrderUtility poUtility = new PurchaseOrderUtility(purchaseOrderDefinition, mediatorServices);
                int poNumberFieldPosition = poUtility.getFieldPosition("purchase_order_num");
                if (poNumberFieldPosition == -1) {
                    // issue error
                }

                PurchaseOrderObject purchaseOrderObject = null;
                int lineNum = 0;

                while ((reader != null) && (sLine != null)) {
                    lineNum++;

                    // if read line is empty continue to next line.
                    if (StringFunction.isBlank(sLine)) {
                        sLine = reader.readLine();
                        continue;
                    }

				// TODO the MAX_PO_FILE_FIELDS_NUMBER
                    // record = PurchaseOrderFileDetails.parsePaymentLineItem(sLine,
                    // PurchaseOrderFileDetails.MAX_PO_FILE_FIELDS_NUMBER);
                    // read the fields in the read line into a string array
                    record = POUploadFileDetails.parsePOLineItem(delimiterChar, sLine);

                    if (record == null) {
                        mediatorServices.getErrorManager().issueError(
                                TradePortalConstants.ERR_CAT_1,
                                TradePortalConstants.FILE_FORMAT_NOT_VALID_DELIMITED);
                        return false;
                    }

				//Ravindra B - 18th Sep 2012 - Rel810 IR-KAUM091833385 - Start
                    //Added trim to avoid leading spaces issue
                    String poNumber = record[poNumberFieldPosition].trim();
                    //Ravindra B - 18th Sep 2012 - Rel810 IR-KAUM091833385 - End

                    boolean isNewPO = poNumSet.add(poNumber);

                    if (isNewPO) {
                        if (purchaseOrderObject != null) {
                            if (purchaseOrderObject.isValid()) {
                                if (!copyToEJBAndCommit(purchaseOrderObject, poUtility)) {
                                    addToErrorList(purchaseOrderObject, mediatorServices, "1");
                                    purchaseOrderObject.logError();
                                    failed++;
                                } else {
                                    savedPOs.add(purchaseOrderObject.getPurchase_order_num());
                                }
                            } else if (purchaseOrderObject.getErrorList().size() > 0) {
                                purchaseOrderObject.logError();
                                failed++;
                            } else {
                                addToErrorList(purchaseOrderObject, mediatorServices, "2");
                                purchaseOrderObject.logError();
                                failed++;
                            }
                        }
                        ctr++;
                        purchaseOrderObject = new PurchaseOrderObject(purchaseOrderDefinition, mediatorServices, poUtility);
                        purchaseOrderObject.setNewPO(true);
                        purchaseOrderObject.setLocalName(csdb.getLocaleName());
                        purchaseOrderObject.setEjbServerLocation(this.ejbServerLocation);
                        purchaseOrderObject.setA_upload_definition_oid(Long.parseLong(purchaseOrderFileUpload.getAttribute("a_upload_definition_oid")));
                        purchaseOrderObject.setA_po_file_upload_oid(Long.parseLong(purchaseOrderFileUpload.getAttribute("po_file_upload_oid")));
                        purchaseOrderObject.setA_owner_org_oid(Long.parseLong(purchaseOrderFileUpload.getAttribute("owner_org_oid")));
                        purchaseOrderObject.setA_user_oid(Long.parseLong(purchaseOrderFileUpload.getAttribute("user_oid")));
                        purchaseOrderObject.setPoUploadUserSecurityRights(securityRights);
                        purchaseOrderObject.setUploadSequenceNumber(purchaseOrderFileUpload.getAttribute("po_file_upload_oid"));
                        purchaseOrderObject.processPO(record, lineNum);
                        if (!purchaseOrderObject.isValid()) {

                            List<IssuedError> errList = mediatorServices.getErrorManager().getIssuedErrorsList();
                            if (errList.size() > 0) {
                                purchaseOrderObject.getErrorList().addAll(errList);
                            }
                        }
                    } else {
                        if (purchaseOrderObject!=null && purchaseOrderObject.isValid()) {
                            //RKAZI IR RWUM041040948 Rel 8.0 04/16/2012 Start
                            if (purchaseOrderObject.getPurchase_order_num().equals(poNumber)) {
                                //if line_item_detail_provided = 'Y' then same poNumber on consecutive lines means duplicates po's
                                String lineItemNotProvided = purchaseOrderDefinition.getAttribute("line_item_detail_provided");
                                if (TradePortalConstants.INDICATOR_YES.equals(lineItemNotProvided) || savedPOs.contains(poNumber)) {
                                    String[] substitutionValues = {poNumber, String.valueOf(lineNum)};
                                    IssuedError ie = mediatorServices.getErrorManager().getIssuedError(csdb.getLocaleName(), TradePortalConstants.PO_DUPLICATE_ERR, substitutionValues, null);
                                    String sellerName = record[poUtility.getFieldPosition("seller_name")];
                                    insertToPOErrorLog(poNumber + "-" + sellerName, purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), ie.getErrorText(), null);
                                    failed++;
                                    total_purchaseOrders++;
                                    total_failed_purchaseOrders++;
                                } else {
                                    purchaseOrderObject.processPO(record, lineNum);
                                }
                                if (!purchaseOrderObject.isValid()) {
                                    List<IssuedError> errList = mediatorServices.getErrorManager().getIssuedErrorsList();
                                    if (errList.size() > 0) {
                                        purchaseOrderObject.getErrorList().addAll(errList);
                                    }
                                }
                            } else {
                            //duplicate poNumber but if current po is not saved then first save it
                                // then log duplicate po error.
                                if (!savedPOs.contains(purchaseOrderObject.getPurchase_order_num())) {
                                    if (!copyToEJBAndCommit(purchaseOrderObject, poUtility)) {
                                        addToErrorList(purchaseOrderObject, mediatorServices, "3");
                                        purchaseOrderObject.logError();
                                        failed++;
                                    } else {
                                        savedPOs.add(purchaseOrderObject.getPurchase_order_num());
                                        purchaseOrderObject = null; //SHR IR KAUM091833385
                                    }
                                }
                                String[] substitutionValues = {poNumber, String.valueOf(lineNum)};
                                IssuedError ie = mediatorServices.getErrorManager().getIssuedError(csdb.getLocaleName(), TradePortalConstants.PO_DUPLICATE_ERR, substitutionValues, null);
                                String sellerName = record[poUtility.getFieldPosition("seller_name")];
                                insertToPOErrorLog(poNumber + "-" + sellerName, purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), ie.getErrorText(), null);
                                failed++;
                                total_purchaseOrders++;
                                total_failed_purchaseOrders++;
                            }
                            //RKAZI IR RWUM041040948 Rel 8.0 04/16/2012 End
                        }
                    }

                    sLine = reader.readLine();

                }
                //Finally save the last PO processed successfully.
                if (purchaseOrderObject != null) {
                    if (!savedPOs.contains(purchaseOrderObject.getPurchase_order_num())) {
                        if (purchaseOrderObject.isValid()) {
                            if (!copyToEJBAndCommit(purchaseOrderObject, poUtility)) {
                                addToErrorList(purchaseOrderObject, mediatorServices, "4");
                                purchaseOrderObject.logError();
                                failed++;
                            } else {
                                savedPOs.add(purchaseOrderObject.getPurchase_order_num());
                            }
                        } else if (purchaseOrderObject.getErrorList().size() > 0) {
                            purchaseOrderObject.logError();
                            failed++;
                        } else {
                            addToErrorList(purchaseOrderObject, mediatorServices, "5");
                            purchaseOrderObject.logError();
                            failed++;
                        }

                    }
                }
                total_purchaseOrders = total_purchaseOrders + poNumSet.size();
                total_failed_purchaseOrders = total_failed_purchaseOrders + (poNumSet.size() - savedPOs.size());
                //failed;
                success = failed < 1;
                isPartiallySuccessful = (poNumSet.size() > savedPOs.size()) && (savedPOs.size() > 0) || ((savedPOs.size() > 0) && failed > 0);

                logDebug("PurchaseOrderFileProcessor->processPurchaseOrderFile: success " + success + " isPartiallySuccessful: " + isPartiallySuccessful);
            }

            String poUploadDataOption = poUploadParamsDoc.getAttribute("/In/PurchaseOrderUploadInfo/POUploadDataOption");
            if ("G".equals(poUploadDataOption)) {
                poUploadParamsDoc.setAttribute("/In/PurchaseOrderUploadInfo/uploadDefinitionOid", purchaseOrderFileUpload.getAttribute("a_upload_definition_oid"));
                if (!TradePortalConstants.INDICATOR_YES.equals(autoCreated)) {
                    poUploadParamsDoc.setAttribute("/In/User/uploadSequenceNumber", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"));
                }
                DocumentHandler outputDoc = groupPOs(poUploadParamsDoc);
                DocumentHandler errorDoc = outputDoc.getFragment("/Error");
                if (errorDoc != null) {
                    String maxErrorSeverity = errorDoc.getAttribute("maxerrorseverity");
                    if ("5".equals(maxErrorSeverity)) {
                        Vector errVector = errorDoc.getFragments("/errorlist/error");
                        for (Object err : errVector) {
                            DocumentHandler errDoc = (DocumentHandler) err;
                            String errMessage = errDoc.getAttribute("message");
                            insertToPOErrorLog("groupingErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), errMessage, null);
                            insertToPOErrorLog("groupingErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), "Error returned: " + errorDoc.toString(), "N");
                        }
                        success = false;
                    } else {
                        success = failed < 1;
                    }

                }
                if (success | isPartiallySuccessful) {
                    DocumentHandler resultsParamDoc = new DocumentHandler();
                    resultsParamDoc.addComponent("/", outputDoc.getFragment("/Out"));
                    Vector v = resultsParamDoc.getFragments("/ResultSetRecord");
                    // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Begins
                    String errorCode = "";
                    if (errorDoc != null) {
                        String maxSeverity = errorDoc.getAttribute("/maxerrorseverity");
                        if ("1".equals(maxSeverity)) {
                            Vector errVector = errorDoc.getFragments("/errorlist/error");
                            for (Object err : errVector) {
                                DocumentHandler errDoc = (DocumentHandler) err;
                                if ("I182".equals(errDoc.getAttribute("/code"))) {
                                    errorCode = errDoc.getAttribute("/code");
                                }
                            }
                        }
                    }
                    // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Ends
                    if (v.size() > 0) {
                        String resultParams = resultsParamDoc.toString();
                        purchaseOrderFileUpload.setAttribute("po_result_parameters", resultParams);
                    } else if ("".equals(errorCode) || errorCode == null) {  // DK IR - RAUM070159323 rel8.1.1 09/14/2012
                        IssuedError ie = ErrorManager.findErrorCode(TradePortalConstants.ERROR_GROUPING_POS, csdb.getLocaleName());
                        String errMsg = ie.getErrorText();
                        insertToPOErrorLog("groupingErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), errMsg, null);
                        insertToPOErrorLog("groupingErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), "No Output returned: " + errorDoc.toString(), "N");
                    }
                }
            }
            // update the status
            postProcess(success, bundle, isPartiallySuccessful);
            //include values for processing slas
            String datetime_process_end = "";
            try {
                datetime_process_end = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)); 
//                		DateTimeUtility.getGMTDateTime(false, false);
            } catch (AmsException e) {
                //log and ignore - non-critical processing
                logError("Problem obtaining process start date time. Ignoring.", e);
            }
            String dateCreated = purchaseOrderFileUpload.getAttribute("creation_timestamp");
            String packStart = purchaseOrderFileUpload.getAttribute("datetime_process_start");
            try {
                long processSeconds = TPDateTimeUtility.calculateDurationSeconds(packStart, datetime_process_end);
                if (processSeconds >= 0) {
                    if (processSeconds <= MAX_DURATION_SECONDS) {
                        String secondsStr = "" + processSeconds;
                        purchaseOrderFileUpload.setAttribute("process_seconds", secondsStr);
                    } else {
                        //avoid failure if duration is out of bounds
                        logDebug("InvoiceFileProcessor.processInvoiceFile::process duration out of bounds. Ignoring.");
                    }
                } else {
                    //avoid failure if duration is negative
                    logDebug("InvoiceFileProcessor.processInvoiceFile::negative porcess duration. Ignoring.");
                }
            } catch (Throwable t) { //i.e. illegal argument exception
                //just write out to the log (i.e. debug)
                logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate process seconds duration."
                        + "Ignoring. Exception:" + t.toString());
            }
            try {
                long totalProcessSeconds = TPDateTimeUtility.calculateDurationSeconds(dateCreated, datetime_process_end);
                if (totalProcessSeconds >= 0) {
                    if (totalProcessSeconds <= MAX_DURATION_SECONDS) {
                        String secondsStr = "" + totalProcessSeconds;
                        purchaseOrderFileUpload.setAttribute("total_process_seconds", secondsStr);
                    } else {
                        //avoid failure if duration is out of bounds
                        logDebug("InvoiceFileProcessor.processInvoiceFile::total processing duration out of bounds. Ignoring.");
                    }
                } else {
                    //avoid failure if duration is negative
                    logDebug("InvoiceFileProcessor.processInvoiceFile::negative total processing duration. Ignoring.");
                }
            } catch (Throwable t) { //i.e. illegal argument exception
                //just write out to the log (i.e. debug)
                logDebug("InvoiceFileProcessor.processInvoiceFile::unable to calculate total process seconds duration."
                        + "Ignoring. Exception:" + t.toString());
            }

            if (purchaseOrderFileUpload.save() < 0) {
                logDebug("InvoiceFileProcessor.processInvoiceFile::Problem saving Invoice File Upload ");
            }

        } catch (Exception e) {
            logError("Error occured processing Purchase Order file...", e);
            IssuedError ie = ErrorManager.findErrorCode(TradePortalConstants.ERROR_UPLOADING_FILE, csdb.getLocaleName());
            insertToPOErrorLog("uploadErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), ie.getErrorText(), null);
            insertToPOErrorLog("uploadErr- ", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"), ie.getErrorText() + "ErrorStack: " + getStackTrace(e), "N");
            rejectFileUplaod(false);
            return false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logError("Error occured processing Purchase Order file...", e);
                }
            }
            cleanup();
        }

        return true;
    }

    private DocumentHandler groupPOs(DocumentHandler poUploadParamsDoc) {
        logDebug("PurchaseOrderFileProcessor->groupPOs: starting grouping...");

        StructuredPOCreateMediator structuredPOCreateMediator = null;
        try {
            logDebug("PurchaseOrderFileProcessor->groupPOs: poUploadParamsDoc: " + poUploadParamsDoc);
            structuredPOCreateMediator = (StructuredPOCreateMediator) EJBObjectFactory.createClientEJB(ejbServerLocation, "StructuredPOCreateMediator");
            DocumentHandler outputDoc = structuredPOCreateMediator.performService(poUploadParamsDoc, csdb);
            logDebug("PurchaseOrderFileProcessor->groupPOs: outputDoc: " + outputDoc);
            return outputDoc;
        } catch (RemoteException e) {
            logError("PurchaseOrderFileProcessor->groupPOs: Error occured while invoking StructuredPOCreateMediator...", e);
        } catch (AmsException e) {
            logError("PurchaseOrderFileProcessor->groupPOs: Error occured while invoking StructuredPOCreateMediator...", e);
        }

        return new DocumentHandler();
    }

    private void addToErrorList(PurchaseOrderObject purchaseOrderObject, MediatorServices mediatorServices, String debugCode) {
        if (purchaseOrderObject.getErrorList().size() < 1) {    // add generic error message if no specific error
            String[] substitutionValues = {"Purchase Order", purchaseOrderObject.getPurchase_order_num()};
            IssuedError ie = mediatorServices.getErrorManager().getIssuedError(csdb.getLocaleName(), TradePortalConstants.ERROR_SAVING_PO, substitutionValues, null);
            purchaseOrderObject.getErrorList().add(ie);

        }
    }

    private boolean copyToEJBAndCommit(PurchaseOrderObject purchaseOrderObject, PurchaseOrderUtility poUtility) {
        boolean success = true;
        if (purchaseOrderObject != null) {
            String purchaseOrderType = purchaseOrderObject.getPurchase_order_type();
            if ("AMD".equals(purchaseOrderType)) {
                checkStatusAndUpdatePOType(purchaseOrderObject);
                boolean amendSuccess = purchaseOrderObject.processAmendPurchaseOrder(purchaseOrderObject);
                if (purchaseOrderObject.getErrorList().size() > 0) {
                    return false;
                } else {
                    return amendSuccess;
                }

            }
            Set<PurchaseOrderLineItemObject> purchaseOrderLineItems = purchaseOrderObject.getPurchaseOrderLineItems();
            PurchaseOrder purchaseOrder = null;
            try {
                purchaseOrder = (PurchaseOrder) EJBObjectFactory.createClientEJB(ejbServerLocation, "PurchaseOrder", csdb);
                purchaseOrder.newObject();
                purchaseOrder.setAttribute("action", TradePortalConstants.PO_ACTION_SYS_UPLOAD);
                purchaseOrder.setAttribute("status", TradePortalConstants.PO_STATUS_UNASSIGNED);
                purchaseOrder.setAttribute("user_oid", purchaseOrderFileUpload.getAttribute("user_oid"));
                purchaseOrder.setAttribute("owner_org_oid", purchaseOrderFileUpload.getAttribute("owner_org_oid"));
                purchaseOrder.setAttribute("upload_definition_oid", purchaseOrderFileUpload.getAttribute("a_upload_definition_oid"));
                purchaseOrder.setAttribute("po_file_upload_oid", purchaseOrderFileUpload.getAttribute("po_file_upload_oid"));
                Map fields = purchaseOrderObject.getFields();
                if (fields != null) {
                    Set fieldNamesSet = fields.keySet();

                    for (Object fieldNameObj : fieldNamesSet) {
                        String fieldName = (String) fieldNameObj;
                        String fieldType = POUploadFileDetails.getAttributeType(purchaseOrder, fieldName);
                        String value = (String) fields.get(fieldName);
                        if (fieldType == null) {
                            logError("Error fetching attribute type for attribute : " + fieldName);
                        } else {
                            if ("DateTimeAttribute".equals(fieldType)) {
                                Date d = poUtility.getDateFormat().parse(value);
                                purchaseOrder.setAttribute(fieldName, DateTimeUtility.convertDateToDateTimeString(d));
                            } else if ("DateAttribute".equals(fieldType)) {
                                Date d = poUtility.getDateFormat().parse(value);
                                purchaseOrder.setAttribute(fieldName, DateTimeUtility.convertDateToDateString(d));
                            } else {
                                if (fieldName.contains("seller_user") | fieldName.contains("buyer_user")) {
                                    String labelValue = purchaseOrderDefinition.getAttribute(fieldName.replace("user", "users"));
                                    purchaseOrder.setAttribute(fieldName, labelValue);
                                    purchaseOrder.setAttribute(fieldName.replace("label", "value"), value);
                                } else {
                                    purchaseOrder.setAttribute(fieldName, value);
                                }
                            }
                        }
                    }
                }
                List<IssuedError> poErrVector = purchaseOrder.getErrorManager().getIssuedErrorsList();
                if (poErrVector.size() > 0) {
                    purchaseOrderObject.getErrorList().addAll(poErrVector);
                }
                if (purchaseOrderLineItems == null) {
                    //Create derived Purchase Order Line Item and set derivedIndicator to 'Y'
                    PurchaseOrderLineItemObject purchaseOrderLineItemObject = new PurchaseOrderLineItemObject();
                    purchaseOrderLineItemObject.setValid(true);
                    purchaseOrderLineItemObject.setAttribute("line_item_num", "1");
                    purchaseOrderLineItemObject.setAttribute("unit_price", purchaseOrderObject.getAmount() != null ? purchaseOrderObject.getAmount().toString() : "0");
                    purchaseOrderLineItemObject.setAttribute("quantity", "1");
                    purchaseOrderLineItemObject.setAttribute("derived_line_item_ind", TradePortalConstants.INDICATOR_YES);
                    purchaseOrderLineItems = new HashSet<>();
                    purchaseOrderLineItems.add(purchaseOrderLineItemObject);
                }

                BigDecimal totalLineItemAmount = PurchaseOrderObject.checkTotalPOLineItemAmount(purchaseOrderLineItems, purchaseOrderObject.getAmount());
                if (totalLineItemAmount.compareTo(BigDecimal.ZERO) != 0) {
                    String[] substitutionValues = {totalLineItemAmount.toString(), purchaseOrderObject.getAmount().toString()};
                    IssuedError ie = purchaseOrder.getErrorManager().getIssuedError(csdb.getLocaleName(), TradePortalConstants.LINE_ITEM_AMT_NOT_EQUAL_TO_PO_AMT, substitutionValues, null);
                    purchaseOrderObject.getErrorList().add(ie);
                    success = false;
                    return success;
                } else {
                    PurchaseOrderLineItem purchaseOrderLineItem = null;
                    for (PurchaseOrderLineItemObject purchaseOrderLineItemObject : purchaseOrderLineItems) {
                        long lineItemOid = purchaseOrder.newComponent("PurchaseOrderLineItemList");

                        purchaseOrderLineItem = (PurchaseOrderLineItem) purchaseOrder.getComponentHandle("PurchaseOrderLineItemList", lineItemOid);

                        Map lineItemFields = purchaseOrderLineItemObject.getFields();
                        if (lineItemFields != null) {
                            Set fieldNamesSet = lineItemFields.keySet();
                            for (Object fieldNameObj : fieldNamesSet) {
                                String fieldName = (String) fieldNameObj;
                                if (fieldName.contains("prod_chars")) {
                                    String labelValue = purchaseOrderDefinition.getAttribute(fieldName);
                                    String value = (String) lineItemFields.get(fieldName);
                                    purchaseOrderLineItem.setAttribute(fieldName, labelValue);
                                    purchaseOrderLineItem.setAttribute(fieldName.replace("label", "value"), value);
                                } else {
                                    purchaseOrderLineItem.setAttribute(fieldName, (String) lineItemFields.get(fieldName));
                                }
                            }
                        }
                        List<IssuedError> errList = purchaseOrderLineItem.getErrorManager().getIssuedErrorsList();
                        if (errList.size() > 0) {
                            purchaseOrderObject.getErrorList().addAll(errList);
                        }
                    }
                }

                if (purchaseOrder.save() != 1) {
                    logError("Error saving  PurchaseOrder " + purchaseOrderObject.getPurchase_order_num() + " Errors: " + getErrors(purchaseOrder));
                    success = false;
                }

            } catch (RemoteException e) {
                success = false;
                logError("Error saving Pruchase Order .... "
                        + purchaseOrderObject.getPurchase_order_num() + " : "
                        + e.getMessage(), e);
            } catch (AmsException e) {
                success = false;
                logError("Error saving Pruchase Order .... "
                        + purchaseOrderObject.getPurchase_order_num() + " : "
                        + e.getMessage(), e);
            } catch (ParseException e) {
                success = false;
                logError("Error saving Pruchase Order .... "
                        + purchaseOrderObject.getPurchase_order_num() + " : "
                        + e.getMessage(), e);
            } finally {
                removeEJBReference(purchaseOrder);
            }

        }
        return success;
    }

    private void checkStatusAndUpdatePOType(PurchaseOrderObject purchaseOrderObject) {
    	
    	String whereClause = " purchase_order_num = ? and status in (?,?,?) and purchase_order_type = ? and A_OWNER_ORG_OID = ?";
        int count = 0;
        List<Object> sqlParams = new ArrayList<Object>();
        sqlParams.add(purchaseOrderObject.getPurchase_order_num());
        sqlParams.add(TradePortalConstants.PO_STATUS_UNASSIGNED);
        sqlParams.add(TradePortalConstants.PO_STATUS_ASSIGNED);
        sqlParams.add(TradePortalConstants.PO_STATUS_CANCELLED);
        sqlParams.add("INT");
        sqlParams.add(purchaseOrderObject.getA_owner_org_oid());
        try {
            count = DatabaseQueryBean.getCount("purchase_order_num", "PURCHASE_ORDER",
                    whereClause, true, sqlParams);
        } catch (AmsException e) {
            logError("Error executing count query for status check.... "
                    + " : "
                    + e.getMessage(), e);

        }
        if (count != 0) {
            purchaseOrderObject.getFields().put("purchase_order_type", TradePortalConstants.INITIAL_PO);
        }

    }

    /**
     * Sets POFileUpload to File Upload Rejected status
     *
     *
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws javax.ejb.RemoveException
     */
    protected void rejectFileUplaod(boolean defaultErrorMsg)
            throws RemoteException, AmsException {

        if (purchaseOrderFileUpload == null) {
            return;
        }

        purchaseOrderFileUpload.setAttribute("validation_seconds", getDurationInSeconds(valStartTime, DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))
//        		GMTUtility.getGMTDateTime(false, false)
        		));
        purchaseOrderFileUpload.setAttribute("completion_timestamp", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//        		DateTimeUtility.getGMTDateTime(false, false));
        String errMsg = purchaseOrderFileUpload.getAttribute("error_msg");

        if (!defaultErrorMsg) {
            defaultErrorMsg = (StringFunction.isBlank(errMsg) && uploadLog.size() == 0);
        }

        try {
            purchaseOrderFileUpload.rejectFileUplaod(defaultErrorMsg);
        } catch (AmsException e) {
            logError("Error during rejectFileUpload...", e);
            throw e;

        }
    }

    /**
     * This method processes orphan POFileUploads and set to reject file upload
     * status
     *
     *
     * @return boolean true - success false = failure
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws javax.ejb.RemoveException
     * @throws java.rmi.RemoteException
     */
    public boolean processOrphan() throws AmsException, RemoteException {

        purchaseOrderFileUpload = (PurchaseOrderFileUpload) EJBObjectFactory.createClientEJB(ejbServerLocation, "PurchaseOrderFileUpload", purchaseOrderFileOid);
        purchaseOrderFileUpload.setClientServerDataBridge(csdb);

        rejectFileUplaod(true);
        cleanup();

        return true;
    }

    /**
     * This method creates POFileUpload to Validation in Progress status
     *
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws java.rmi.RemoteException
     */
    private void init() throws RemoteException, AmsException {

        // get the Purchase Order File Upload instance.
        purchaseOrderFileUpload = (PurchaseOrderFileUpload) EJBObjectFactory.createClientEJB(ejbServerLocation, "PurchaseOrderFileUpload", purchaseOrderFileOid);
		//capture the pack end date/time

        try {
            datetime_process_start = DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate());
//            		DateTimeUtility.getGMTDateTime(false, false);
        } catch (AmsException e) {
            //log and ignore - non-critical processing
            logError("Problem obtaining process start date time. Ignoring.", e);
        }
        try {
            purchaseOrderFileUpload.setAttribute("datetime_process_start", datetime_process_start);
        } catch (AmsException ex) {
            //log and ignore - non-critical processing
            logError("Problem saving pack start date time. Ignoring.", ex);
        }

        purchaseOrderFileUpload.setClientServerDataBridge(csdb);
        fileName = purchaseOrderFileUpload.getAttribute("po_file_name");

        //set the status to VALIDATION_IN_PROGRESS before starting processing the uploaded file.
        setPOUploadStatus(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS, bundle);

        bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TextResources");

        valStartTime = DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false));
//        		GMTUtility.getGMTDateTime(false, false);

    }

    /**
     * This method handles post process
     *
     * @throws com.amsinc.ecsg.frame.AmsException
     * @throws java.rmi.RemoteException
     * @throws javax.ejb.RemoveException
     */
    private void postProcess(boolean validFile, PropertyResourceBundle bundle, boolean partiallySuccessful) throws RemoteException, AmsException {
        String status = null;
        purchaseOrderFileUpload.setAttribute("completion_timestamp", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//        		DateTimeUtility.getGMTDateTime(true, false));
        purchaseOrderFileUpload.setAttribute("number_of_po_uploaded", String.valueOf(total_purchaseOrders));
        purchaseOrderFileUpload.setAttribute("number_of_po_failed", String.valueOf(total_failed_purchaseOrders));
        if (partiallySuccessful) {
            status = TradePortalConstants.PYMT_UPLOAD_VALIDATED_WITH_ERRORS;
        } else if (validFile) {
            status = TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL;
        } else {
            status = TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED;
        }
        setCCYAndAmount();
        // update the PO_FILE_UPLOADS table with status
        setPOUploadStatus(status, bundle);
    }

    private void setCCYAndAmount() {
        try {
            String sql = "select currency, sum(amount) amount from PURCHASE_ORDER where A_PO_FILE_UPLOAD_OID = ?   group by currency";
            DocumentHandler resutlsDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{purchaseOrderFileOid});
            if (resutlsDoc == null) {
                return;
            }
            Vector resultsVector = resutlsDoc.getFragments("/ResultSetRecord");
            if (resultsVector == null) {
                return;
            }
            if (resultsVector.size() > 1) {
                purchaseOrderFileUpload.setAttribute("currency", "Mixed");
            } else if (resultsVector.size() == 1) {
                DocumentHandler recordDoc = (DocumentHandler) resultsVector.elementAt(0);
                String currencyCode = recordDoc.getAttribute("/CURRENCY");
                String amount = recordDoc.getAttribute("/AMOUNT");
                purchaseOrderFileUpload.setAttribute("currency", currencyCode);
                purchaseOrderFileUpload.setAttribute("amount", amount);
            }

        } catch (Exception e) {
            logError("Error setting currency and amount on Purchase Oder File Upload for OID .. " + purchaseOrderFileOid, e);
        }
    }

    /**
     * This method gets the input in seconds
	 *
     */
    private String getDurationInSeconds(Date start, Date end) {
        if (start == null || end == null) {
            return null;
        }
        long startMillis = start.getTime();
        long endMillis = end.getTime();
        long durationMillis = endMillis - startMillis;
        return Long.toString(durationMillis / 1000);
    }

    /**
     * This method sets the proper validation status
	 *
     */
    private void setPOUploadStatus(String status, PropertyResourceBundle bundle)
            throws RemoteException, AmsException {

        if (!TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS.equals(status)) {
            purchaseOrderFileUpload.setAttribute("validation_seconds", getDurationInSeconds(valStartTime, DateTimeUtility.convertDateToGMTDateTime(serverDeploy.getServerDate(false))));
//            		GMTUtility.getGMTDateTime(false, false)));
            purchaseOrderFileUpload.setAttribute("completion_timestamp", DateTimeUtility.convertDateToGMTDateTimeString(serverDeploy.getServerDate(false)));
//            		DateTimeUtility.getGMTDateTime(false, false));
        }

        purchaseOrderFileUpload.setAttribute("validation_status", status);

        if (purchaseOrderFileUpload.save() < 0) {
            List<IssuedError> errors = purchaseOrderFileUpload.getErrorManager().getIssuedErrorsList();

            if (errors.size() > 0) {
                IssuedError ie = errors.get(0);
                AmsException ae = new AmsException("error saving PurchaseOrderFileUpload ....");
                ae.setIssuedError(ie);
                throw ae;
            } else {
                int errorSeverity = purchaseOrderFileUpload.getErrorManager().getMaxErrorSeverity();
                throw new AmsException("error saving PurchaseOrderFileUpload .... with error severity --> " + errorSeverity);
            }
        }

    }

 
    private void insertToPOErrorLog(String poNumber, String poUploadOID, String errMsg, String displayable) {
        try( Connection con = DatabaseQueryBean.connect(false);
        	PreparedStatement pStmt = con
                    .prepareStatement("insert into po_upload_error_log (po_upload_error_log_oid, po_number, "
                    		+ "error_msg, ben_name, p_po_file_upload_oid, displayable)values(?,?,?,?,?,?)")){
                    
            con.setAutoCommit(false);
            
            String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(false));
            pStmt.setString(1, oid);
            String[] temp = poNumber.split("-");
            pStmt.setString(2, temp[0]);
            pStmt.setString(3, errMsg);
            pStmt.setString(4, (temp[1] == null) ? null : (temp[1]).trim());
            pStmt.setString(5, poUploadOID);
            pStmt.setString(6, displayable);
            pStmt.execute();
            con.commit();
           
        } catch (AmsException e) {
            logError("Error occured during saving PO Upload Error log ...", e);
        } catch (SQLException e) {
            logError("Error occured during saving PO Upload Error log ...", e);
        }
    }

    /**
     * This method gets temporarily stored PO file
	 *
     */
    private File getPurChaseOrderFile() throws RemoteException, AmsException {
        String po_file_upload_oid = purchaseOrderFileUpload.getAttribute("po_file_upload_oid");
        String fname = folder + po_file_upload_oid + POUploadFileDetails.FILE_EXTENSION;
        return new File(fname);
    }

    private BufferedReader getInputReader() throws AmsException {

        BufferedReader bReader = null;
        try {
            purchaseOrderFile = getPurChaseOrderFile();
            FileInputStream is = new FileInputStream(purchaseOrderFile);
            InputStreamReader bis = new InputStreamReader(is,
                    AmsConstants.UTF8_ENCODING);
            bReader = new BufferedReader(bis);
        } catch (Exception e) {
            logError("Error occured getting purchase order file...", e);
            throw new AmsException(
                    "Error occured getting purchase order file...");
        }

        return bReader;

    }

    /**
     * This method gets PurchaseOrderDefinition instance
	 *
     */
    protected PurchaseOrderDefinition getPOUploadDef(String oid) throws RemoteException, AmsException {

        PurchaseOrderDefinition purchaseOrderDefinition = (PurchaseOrderDefinition) EJBObjectFactory.createClientEJB(ejbServerLocation, "PurchaseOrderDefinition", new Long(oid));
        delimiterChar = purchaseOrderDefinition.getAttribute("delimiter_char");
        return purchaseOrderDefinition;

    }

    /**
     * This method removes EJB references and deletes PO file
     */
    protected void cleanup() {

        removeEJBReference(purchaseOrderFileUpload);
        removeEJBReference(purchaseOrderDefinition);

		// TODO commented for now, uncomment after testing.
        // delete file
        if (purchaseOrderFile != null && !purchaseOrderFile.delete()) {
            logError("Error: Could not delete file: " + purchaseOrderFile.getName());
        }
    }

    /**
     * This method removes EJB references
     */
    protected void removeEJBReference(EJBObject objRef) {
        try {
            if (objRef != null) {
                objRef.remove();
                objRef = null;
            }
        } catch (Exception e) {
            logError("While removing instance of EJB object", e);
        }
    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText) {
        String str = " : UploadFilename: " + fileName + "  :: POUpload_Oid: "
                + purchaseOrderFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + str,
                Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs error
     *
     */
    public void logError(String errorText, Exception e) {
        String str = " : UploadFilename: " + fileName
                + "  :: PurchaseOrderUpload_Oid: " + purchaseOrderFileOid + " ";
        Logger.getLogger().log(
                loggerCategory, process_id,
                process_id + ": ERROR: " + errorText + ": " + e.getMessage() + " - StackTrace - " + getStackTrace(e)
                + str, Logger.ERROR_LOG_SEVERITY);

    }

    /**
     * This method Logs Debug info
     *
     */
    public void logDebug(String infoText) {
        String str = " : UploadFilename: " + fileName
                + "  :: PurchaseOrderUpload_Oid: " + purchaseOrderFileOid + " ";
        Logger.getLogger().log(loggerCategory, process_id,
                process_id + ": INFO: " + infoText + str,
                Logger.DEBUG_LOG_SEVERITY);
    }

    private String getStackTrace(Exception ex) {
        final Writer writer = new StringWriter();
        ex.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    private String getErrors(BusinessObject obj) throws RemoteException {

        if (obj.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {

            StringBuilder sb = new StringBuilder();
            for (IssuedError errorObject : obj.getIssuedErrors()) {
                sb.append("-").append(errorObject.getErrorText());
            }

            return sb.toString();
        }
        return null;
    }

}
