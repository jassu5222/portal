package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.Party;
import com.ams.tradeportal.busobj.PurchaseOrder;
import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.ams.tradeportal.busobj.PurchaseOrderLineItem;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.util.StructuredPOLogger;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * User: rizwan.kazi
 * Date: 3/9/12
 * Time: 12:02 PM
 * Rel 8.0 CR-707.
 * POJO to hold Purchase Order values.
 */
public class PurchaseOrderObject {
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderObject.class);
    //Fields declaration.
   
    private long a_owner_org_oid;
    private long a_upload_definition_oid;
    private long a_po_file_upload_oid;
    private long a_user_oid;

    private PurchaseOrderDefinition purchaseOrderDefinition;
    private Set<PurchaseOrderLineItemObject> purchaseOrderLineItems;
    private List<IssuedError> errorList = new ArrayList<>();
    private boolean isValid = true;
    private final MediatorServices mediatorServices;

    private boolean isNewPO = false;
    private Set<String> lineItemNumSet = new HashSet<>();
    private List<String> poFieldNamesList = null;
    private List<String> liFieldNamesList = null;
    private PurchaseOrderUtility purchaseOrderUtility = null;
    private Map<String, Object> fields = new HashMap<>();
    //private int lineNum;
    private boolean isDuplicate;
    private String localName;

    private Transaction poLineItemTransaction        = null;
    private PurchaseOrder purchaseOrder = null;
    private PurchaseOrder copyOfpurchaseOrder = null;
    private String ejbServerLocation = null;
    
    private String             instrumentType               = null;
    private String             poUploadUserSecurityRights   = null;
    private transient StructuredPOLogger logger = null;
    private transient String uploadSequenceNumber = null;
    private transient String logSequence = null;
    private boolean isAmendTransaction = false;
    private boolean noPoLineItems = false;

    public PurchaseOrderObject(PurchaseOrderDefinition purchaseOrderDefinition, MediatorServices mediatorServices, PurchaseOrderUtility purchaseOrderUtility) {
        this.mediatorServices = mediatorServices;
        this.purchaseOrderDefinition = purchaseOrderDefinition;
        this.purchaseOrderUtility = purchaseOrderUtility;


        
        if (poFieldNamesList == null) {
            poFieldNamesList = purchaseOrderUtility.getDefinedFieldNames("");
        }
        if (liFieldNamesList == null) {
            liFieldNamesList = purchaseOrderUtility.getDefinedFieldNames("lineItem");
        }

    }

    //Getter and Setters for all declared fields. START

    public String getPurchase_order_num() {
        return (String)fields.get("purchase_order_num");
    }


    public String getPurchase_order_type() {
        return (String)fields.get("purchase_order_type");
    }

    public void setA_upload_definition_oid(long a_upload_definition_oid) {
        this.a_upload_definition_oid = a_upload_definition_oid;
    }   
    
    public void setA_po_file_upload_oid(long a_po_file_upload_oid) {
        this.a_po_file_upload_oid = a_po_file_upload_oid;
    }

    public Date getLatest_shipment_date() {
        Date d = null;
        try {
            String latestShipmentDate = (String) fields.get("latest_shipment_date");
            if (latestShipmentDate == null){
                return d;
            }
            d = purchaseOrderUtility.getDateFormat().parse(latestShipmentDate);
        } catch (ParseException e) {
           return d;
        }
        return d;
    }


    public String getCurrency() {
        return (String)fields.get("currency");
    }


    public BigDecimal getAmount() {
        String amt = (String)fields.get("amount");
        return StringFunction.isBlank(amt)? BigDecimal.ZERO: new BigDecimal(amt);
    }

    public String getSeller_name() {
        return (String)fields.get("seller_name");
    }


    public long getA_owner_org_oid() {
        return a_owner_org_oid;
    }

    public void setA_owner_org_oid(long a_owner_org_oid) {
        this.a_owner_org_oid = a_owner_org_oid;
    }

    public long getA_upload_definition_oid() {
        return a_upload_definition_oid;
    }


    public long getA_po_file_upload_oid() {
        return a_po_file_upload_oid;
    }


    public PurchaseOrderDefinition getPurchaseOrderDefinition() {
        return purchaseOrderDefinition;
    }

    public void setPurchaseOrderDefinition(
            PurchaseOrderDefinition purchaseOrderDefinition) {
        this.purchaseOrderDefinition = purchaseOrderDefinition;
    }

    public Set<PurchaseOrderLineItemObject> getPurchaseOrderLineItems() {
        return purchaseOrderLineItems;
    }

    public List<IssuedError> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<IssuedError> errorList) {
        this.errorList = errorList;
    }

    public boolean isNewPO() {
        return isNewPO;
    }

    public void setNewPO(boolean isNewPO) {
        this.isNewPO = isNewPO;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public boolean isDuplicate() {
        return isDuplicate;
    }

    public void setDuplicate(boolean duplicate) {
        isDuplicate = duplicate;
    }

    public Map<String, Object> getFields() {
        return fields;
    }


    public String getEjbServerLocation() {
        return ejbServerLocation;
    }

    public void setEjbServerLocation(String ejbServerLocation) {
        this.ejbServerLocation = ejbServerLocation;
    }

    public long getA_user_oid() {
        return a_user_oid;
    }

    public void setA_user_oid(long a_user_oid) {
        this.a_user_oid = a_user_oid;
    }

    public StructuredPOLogger getLogger() {
        return logger;
    }

    public void setLogger(StructuredPOLogger logger) {
        this.logger = logger;
    }

    public String getPoUploadUserSecurityRights() {
        return poUploadUserSecurityRights;
    }

    public void setPoUploadUserSecurityRights(String poUploadUserSecurityRights) {
        this.poUploadUserSecurityRights = poUploadUserSecurityRights;
    }

    public String getUploadSequenceNumber() {
        return uploadSequenceNumber;
    }

    public void setUploadSequenceNumber(String uploadSequenceNumber) {
        this.uploadSequenceNumber = uploadSequenceNumber;
    }

    public String getLogSequence() {
        return logSequence;
    }

    public void setLogSequence(String logSequence) {
        this.logSequence = logSequence;
    }

    //Getter and Setters for all declared fields. END


    /**
     * ProcessPO method is called to process a passed in upload file line. It parses the record and maps all the values
     * to appropriate fields on the object. Once all fields as mapped it validates the values and marks this object
     * as valid or invalid. It also create, validates and adds any LineItems as child.
     *
     * @param record
     * @param lineNum
     */
    public void processPO(String[] record, int lineNum) {

        purchaseOrderUtility.setCurrentLineNum(lineNum);
        if (record != null && record.length > 0) {
            if (record.length != purchaseOrderUtility.getFieldPositions().size()) {
                purchaseOrderUtility.addError(TradePortalConstants.INVALID_PO_FIELD_COUNT);
                isValid = false;
                isNewPO = false;
            } else {
                if (!isNewPO & isValid) {
                    int lineItemNumPosition = purchaseOrderUtility.getFieldPosition("line_item_num");
                    if (lineItemNumPosition == -1) {
                        String[] substitutionValues = {"Purchase Order", getPurchase_order_num()};
						IssuedError ie = ErrorManager.getIssuedError( this.localName, TradePortalConstants.PO_FIELD_MISSING, substitutionValues, null);
                        getErrorList().add(ie);
                        this.isValid = false;
                    } else {
                        String lineItemNum = record[lineItemNumPosition];
                        boolean isDuplicateLineItem = lineItemNumSet.add(lineItemNum);
                        if (!isDuplicateLineItem) {

                            this.isValid = false;
                            purchaseOrderUtility.addError("line_item_num", lineItemNum, TradePortalConstants.PO_LINE_ITEM_DUPLICATE_ERR);
                        } else {
                            PurchaseOrderLineItemObject purchaseOrderLineItemObject = new PurchaseOrderLineItemObject();
                            purchaseOrderLineItemObject.processLineItem(liFieldNamesList, record,purchaseOrderUtility, lineNum);
                            if (!purchaseOrderLineItemObject.isValid()) {
                                this.isValid = false;
                            } else {
                                if (purchaseOrderLineItems == null) {
                                    purchaseOrderLineItems = new HashSet<>();
                                }
                                purchaseOrderLineItems.add(purchaseOrderLineItemObject);

                            }
                        }
                    }
                } else {
                    purchaseOrderLineItems = null;
                    fields = purchaseOrderUtility.setFieldValues(poFieldNamesList, record, lineNum, this);
                 	 if (TradePortalConstants.INITIAL_PO.equals(getPurchase_order_type())) {
        				getFields().put("amend_seq_no", "0");
        			 }
                    if (isValid) {
                        checkDuplicateOrMissingPO();
                        isNewPO = false;
                        if (liFieldNamesList != null && liFieldNamesList.size() > 0 && isValid) {
                            this.processPO(record, lineNum);
                        }
                    }
                }
            }
        }
    }

    public void logError() {
    	String stm="insert into po_upload_error_log (po_upload_error_log_oid, po_number, error_msg, ben_name, p_po_file_upload_oid)values(?,?,?,?,?)";
            try (Connection con = DatabaseQueryBean.connect(false);
            		PreparedStatement pStmt = con.prepareStatement(stm)){
                con.setAutoCommit(false);
               
                    List<IssuedError> detail = this.errorList;

                    if (detail != null && detail.size() > 0) {
                        for (IssuedError errorObject : detail) {
                            StringBuilder sb = new StringBuilder();
                            if (errorObject != null) {
                                sb.append(errorObject.getErrorText());
                                String errMsg = sb.toString();
                                String oid = Long.toString(ObjectIDCache.getInstance(
                                        AmsConstants.OID).generateObjectID(false));
                                pStmt.setString(1, oid);
                                pStmt.setString(2, getPurchase_order_num());
                                pStmt.setString(3, errMsg);
                                pStmt.setString(4, getSeller_name());
                                pStmt.setString(5, String.valueOf(a_po_file_upload_oid));
                                pStmt.addBatch();
                                pStmt.clearParameters();
                            }
                        }

                    }

                pStmt.executeBatch();
                con.commit();
                mediatorServices.getErrorManager().initialize();

            } catch (AmsException | SQLException e) {
//                logError("Error occured during saving PO Upload Error log ...",
//                        e);
            }
    }
	
    /**
     * This method is used to identify if the currect PurchaseOrder object being processed is already present in the
     * DB if the Purchase Order Type is INT, If true then it logs error and marks this Purchase Order object as invalid.
     * This method also check if the Purchase Order Type is 'AMD' then the Purchase Order should be present in DB and
     * should  not have a status of 'Authorised', if not it logs error and marks the Purchase Order Object being
     * processed as inValid.
     */
    private void checkDuplicateOrMissingPO() {
        try {
            int count = DatabaseQueryBean.getCount("purchase_order_num", "PURCHASE_ORDER",
                    " deleted_ind = ? and purchase_order_num = ? and A_OWNER_ORG_OID = ?"  , true, "N", getPurchase_order_num(), a_owner_org_oid);
            if (count != 0 & TradePortalConstants.INITIAL_PO.equals(getPurchase_order_type())) {
                purchaseOrderUtility.addError("purchase_order_num", getPurchase_order_num(), TradePortalConstants.PO_DUPLICATE_ERR);
                isDuplicate = true;
                isValid = false;
            } else if (count == 0 & TradePortalConstants.AMEND_PO.equals(getPurchase_order_type())) {
                purchaseOrderUtility.addError("purchase_order_num", getPurchase_order_num(), TradePortalConstants.PO_MISSING_ERR);
                isValid = false;
            } else if (TradePortalConstants.AMEND_PO.equals(getPurchase_order_type())) {
                String whereClause = " purchase_order_num = ? and status = ? and A_OWNER_ORG_OID = ? ";

                count = DatabaseQueryBean.getCount("purchase_order_num", "PURCHASE_ORDER",
                        whereClause, true, getPurchase_order_num(), TradePortalConstants.PO_STATUS_AUTHORISED, a_owner_org_oid);
                if (count != 0) {
                    purchaseOrderUtility.addError("purchase_order_num", getPurchase_order_num(), TradePortalConstants.ERROR_CANNOT_AMEND_AUTHORIZED_PO);// IR ROUM041205034 Add.
                    isValid = false;
                }
            }
        } catch (AmsException e) {
            mediatorServices.debug("Error checking duplicate ... for Purchase Order : " + getPurchase_order_num());
        }

    }

    public boolean processAmendPurchaseOrder(PurchaseOrderObject purchaseOrderObject) {

        Set<PurchaseOrderLineItemObject> purchaseOrderLineItemObjects = purchaseOrderObject.getPurchaseOrderLineItems();
        //RKAZI IR#KRUM092851904 Rel 8.1 UAT 10/04/2012 - Start - Added logic to check for invalid PO due to incorrect
        // PO Line Items amount total.
        //Before processing PO Amend check if PO is valid.
        if(purchaseOrderLineItemObjects!=null){
        BigDecimal totalLineItemAmount = PurchaseOrderObject.checkTotalPOLineItemAmount(purchaseOrderLineItemObjects,purchaseOrderObject.getAmount());
        if (totalLineItemAmount.compareTo(BigDecimal.ZERO)!= 0){
            String[] substitutionValues = {totalLineItemAmount.toString(), purchaseOrderObject.getAmount().toString()};
			IssuedError ie = ErrorManager.getIssuedError(this.localName, TradePortalConstants.LINE_ITEM_AMT_NOT_EQUAL_TO_PO_AMT, substitutionValues, null);
            purchaseOrderObject.getErrorList().add(ie);
            return false;
        }}
        if (purchaseOrderObject.getErrorList().size() > 0){
            return false;
        }
        //RKAZI IR#KRUM092851904 Rel 8.1 UAT 10/04/2012 - End

        try
        {
            // The log sequence needs to be
            // generated.  Otherwise, we use the upload sequence of the log sequence
            if (uploadSequenceNumber == null) {
                long newSeq = ObjectIDCache.getInstance(
                        TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID();
                setLogSequence(String.valueOf(newSeq));
            } else {
                setLogSequence(uploadSequenceNumber);
            }

            setLogger(StructuredPOLogger.getInstance(String.valueOf(getA_owner_org_oid())));

            if (checkForUnassignedPOLineItemMatch() == true)
            {
                return true;
            }

            if (checkForAssignedPOLineItemMatch(mediatorServices.getCSDB().getLocaleName()) == true) //logu & amit
            {
                return true;
            }

            if (checkForAuthorizedIssuePOLineItemMatch(mediatorServices.getCSDB().getLocaleName()) == true)
            {
                return true;
            }

            if (checkForAuthorizedAmendOrProcessedByBankMatch(mediatorServices.getCSDB().getLocaleName()) == true) //logu & amit
            {
                return true;
            }
        }
        catch (AmsException ex)
        {
            LOG.info("AmsException occurred in PurchaseOrderObject.processAmendPurchaseOrder(): " + ex.getMessage());
            ex.printStackTrace();
            logException(ex);
        }
        catch (RemoteException ex)
        {
            LOG.info("RemoteException occurred in PurchaseOrderObject.processAmendPurchaseOrder(): " + ex.getMessage());
            ex.printStackTrace();
            logException(ex);
        }

        return false;
    }

    private void logException(Exception ex) {
    	try {
			getLogger().addLogMessageText(String.valueOf(a_owner_org_oid), logSequence,
			       "Exception Occured - PO Num: " +  getPurchase_order_num() + " StackTrace: " + getStackTrace(ex));
		} catch (AmsException e) {
			// TODO Auto-generated catch block
		}
    }
    
    
    private String getStackTrace(Exception ex) {
	    final Writer writer = new StringWriter();
	    ex.printStackTrace(new PrintWriter(writer));
	    return writer.toString();
	  }
    /**
     * This method is used to determine whether or not the current PO line item
     * has the same PO number and item number as an unassigned PO line item in
     * the PO line item database table. If a match is found, the method will
     * call methods to validate the current PO line item data, update the
     * existing record's data if successful, and add a message to the Auto LC
     * log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a PO line item match was found
     *                       (true  - a PO line item match was found, so stop processing the PO line item
     *                        false - a PO line item match was not found, so continue processing the PO line item)
     */
    private boolean checkForUnassignedPOLineItemMatch() throws AmsException, RemoteException
    {

        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are NOT currently assigned to a transaction
        String sqlQuery = "select purchase_order_oid from purchase_order where a_owner_org_oid = ? and purchase_order_num = ? and a_transaction_oid is null";
        
        Long orgOidLong = getA_owner_org_oid();
        Object[] params = new Object[]{orgOidLong,getPurchase_order_num()} ;
        
        // Retrieve any PO line items that satisfy the SQL that was just constructed
        DocumentHandler matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, params );

        // If a match was found, try to validate the current PO line item's data and
        // save it to the matching PO line item.
        if (matchingPOLineItemsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, getPurchase_order_num());

                return true;
            }


            // Update the existing PO line item's data with the data from the current
            // PO line item and log a message to the Auto LC log file
            savePOLineItemData(matchingPOLineItemsDoc);

            cleanupPOLineItemData("checkForUnassignedPOLineItemMatch");

            getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                    TradePortalConstants.LCLOG_LINE_ITEM_UPDATED, getPurchase_order_num());

            return true;
        }

        return false;
    }

    /**
     * This method is used as a wrapper to call the main savePOLineItemData
     * method, which takes a boolean save flag as a parameter. Unlike the
     * wrapper method that doesn't take any parameters, this method receives an
     * xml document that contains the OID of the PO line item that the current
     * PO line item matched. It passes this xml document to a method to
     * instantiate the existing PO line item before the main savePOLineItemData
     * is called to update it.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void savePOLineItemData(DocumentHandler matchingPOLineItemsDoc) throws AmsException, RemoteException
    {
        getMatchingPOLineItem(matchingPOLineItemsDoc);

        savePOLineItemData(true);
    }

    /**
     * This method is used to instantiate the existing PO line item that matches
     * the current PO line item being processed. Once this object has been
     * instantiated, it can be accessed in other methods like saving, accessing
     * the transaction that the PO line item is assigned to, etc.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void getMatchingPOLineItem(DocumentHandler matchingPOLineItemsDoc) throws AmsException, RemoteException
    {

        List<DocumentHandler> purchaseOrders = matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord");
        DocumentHandler purchaseOrderDoc =  purchaseOrders.get(0);
        long purchaseOrderOid = purchaseOrderDoc.getAttributeLong("PURCHASE_ORDER_OID");

        purchaseOrder = (PurchaseOrder) EJBObjectFactory.createClientEJB(ejbServerLocation,
                                                           "PurchaseOrder", purchaseOrderOid, mediatorServices.getCSDB());
    }

    /**
     * This method is used to create a new PO line item and save its data or
     * update an existing PO line item with new data. A boolean flag passed in
     * determines whether or not the save method will be called for the PO line
     * item business object. If this parameter is set to false, the save method
     * must be called elsewhere for all the data set in this method to be saved.
     *

     * @param      saveFlag - indicator of whether or not to save the PO line item in this method
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     */
    public void savePOLineItemData(boolean saveFlag) throws AmsException, RemoteException
    {


        boolean           isPOLineItemNew               = false;
        // If we're dealing with an existing PO line item, get its PO upload definition OID and
        // compare it to the PO upload definition OID that was selected for the current PO
        // upload. If they aren't the same, it's possible that some fields that previously
        // existed in the old definition no longer exist in the new one; in this case, we need
        // to clear their values so that we don't have bad data.
        if (!isPOLineItemNew)
        {
            String previousPOUploadDefinitionOid = purchaseOrder.getAttribute("upload_definition_oid");

            if (!previousPOUploadDefinitionOid.equals(String.valueOf(a_upload_definition_oid)))
            {
                resetPurchaseOrder(previousPOUploadDefinitionOid);
            }
        }
        //RKAZI 06-18-2012 IR-RSUM051645477 Start
        //Need to check if the sum of Amended PurchaseOrderLineItems amount matches the updated PurchaseOrder Amount
        //if not then we should not update the PurchaseOrder.
        boolean updatePO = true;
        Set<PurchaseOrderLineItemObject> purchaseOrderLineItemObjects =
                getPurchaseOrderLineItems();
        if (purchaseOrderLineItemObjects != null){
            BigDecimal totalLineItemAmount = checkTotalPOLineItemAmount(purchaseOrderLineItems,getAmount());
            if (totalLineItemAmount.compareTo(BigDecimal.ZERO)!= 0){
                String[] substitutionValues = {"", ""};
                substitutionValues[0] = totalLineItemAmount.toString();
                substitutionValues[1] = getAmount().toString();

                IssuedError ie = ErrorManager.getIssuedError(
                        mediatorServices.getCSDB().getLocaleName(),
                        TradePortalConstants.LINE_ITEM_AMT_NOT_EQUAL_TO_PO_AMT, substitutionValues,
                        null);
                getErrorList().add(ie);
                updatePO = false ;
            }
        }
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - START
        else {
            //Create derived Purchase Order Line Item and set derivedIndicator to 'Y'
            //this is needed if there are no Line Items for uploaded PO's.

            PurchaseOrderLineItemObject purchaseOrderLineItemObject = new PurchaseOrderLineItemObject();
            purchaseOrderLineItemObject.setValid(true);
            purchaseOrderLineItemObject.setAttribute("line_item_num","1");
            purchaseOrderLineItemObject.setAttribute("unit_price", getAmount().toString());
            purchaseOrderLineItemObject.setAttribute("quantity", "1");
            purchaseOrderLineItemObject.setAttribute("derived_line_item_ind",TradePortalConstants.INDICATOR_YES);
            purchaseOrderLineItems = new HashSet<>();
            purchaseOrderLineItems.add(purchaseOrderLineItemObject);
            noPoLineItems = true;

        }
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - END
        if (updatePO){

            updatePurchaseOrder();

            updatePurchaseOrderLineItems();
            // If the save flag is true, save the PO line item; otherwise, this save call needs to be
            // made elsewhere (somewhere after this method call)
            if (saveFlag)
            {
                purchaseOrder.save();

                // If a copy of the PO line item has been created, save it too
                if(copyOfpurchaseOrder != null)
                    copyOfpurchaseOrder.save();
            }
        }
        //RKAZI 06-18-2012 IR-RSUM051645477 End

    }

    private void updatePurchaseOrderLineItems() throws AmsException, RemoteException {

        List<String> fieldNames = purchaseOrderUtility.getDefinedFieldNames("lineItem");
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - START
        if (noPoLineItems){
            fieldNames = getDefaultFieldNames();
        }
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - END
        Set<PurchaseOrderLineItemObject> purchaseOrderLineItemObjects =
                getPurchaseOrderLineItems();
        if (purchaseOrderLineItemObjects != null){
            BigDecimal totalLineItemAmount = checkTotalPOLineItemAmount(purchaseOrderLineItems,getAmount());
            if (totalLineItemAmount.compareTo(BigDecimal.ZERO)!= 0){
                String[] substitutionValues = {"", ""};
                substitutionValues[0] = totalLineItemAmount.toString();
                substitutionValues[1] = getAmount().toString();

                IssuedError ie = ErrorManager.getIssuedError(
                		mediatorServices.getCSDB().getLocaleName(),
                        TradePortalConstants.LINE_ITEM_AMT_NOT_EQUAL_TO_PO_AMT, substitutionValues,
                        null);
                getErrorList().add(ie);
                return ;
            }

        for (PurchaseOrderLineItemObject purchaseOrderLineItemObject : purchaseOrderLineItemObjects){
            long poLineItemOid = getPurchaseOrderLineItemOid(purchaseOrderLineItemObject.getLine_item_num(),
                    purchaseOrder.getAttribute("purchase_order_oid"));
            PurchaseOrderLineItem purchaseOrderLineItem = null;
            Map fields = purchaseOrderLineItemObject.getFields();
            if (poLineItemOid != 0){
                purchaseOrderLineItem =
                        (PurchaseOrderLineItem)purchaseOrder.getComponentHandle("PurchaseOrderLineItemList", poLineItemOid);

            } else {
                long lineItemOid = purchaseOrder.newComponent("PurchaseOrderLineItemList");
                purchaseOrderLineItem = (PurchaseOrderLineItem)purchaseOrder.getComponentHandle("PurchaseOrderLineItemList", lineItemOid);
            }
            for (String fieldName : fieldNames){
            	if (fieldName.contains("prod_chars")){
                    String labelValue = purchaseOrderDefinition.getAttribute(fieldName) ;
                    String value = (String)fields.get(fieldName);
                    purchaseOrderLineItem.setAttribute(fieldName, labelValue);
                    purchaseOrderLineItem.setAttribute(fieldName.replace("label","value"), value);
                } else{
                    purchaseOrderLineItem.setAttribute(fieldName, (String)fields.get(fieldName));
                }
            }

        }
        }
    }

    //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - START
    private List<String> getDefaultFieldNames() {
        List<String> fieldNamesList = new ArrayList<>();
        fieldNamesList.add("line_item_num");
        fieldNamesList.add("unit_price");
        fieldNamesList.add("quantity");
        fieldNamesList.add("derived_line_item_ind");
        return fieldNamesList;
    }
    //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - END

    public static BigDecimal checkTotalPOLineItemAmount(Set<PurchaseOrderLineItemObject> purchaseOrderLineItems, BigDecimal amount) {

    	BigDecimal totalPoAmount = BigDecimal.ZERO;
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - START
        if (purchaseOrderLineItems == null){
            return totalPoAmount;
        }
        //RKAZI 11/06/2012 IR RAUM110579604 Release 8.1 UAT - END
     	for (PurchaseOrderLineItemObject purchaseOrderLineItemObject : purchaseOrderLineItems) {
    		if (purchaseOrderLineItemObject.getUnit_price() != null && purchaseOrderLineItemObject.getQuantity() != null) {
    			BigDecimal totLineAmount = purchaseOrderLineItemObject.getUnit_price().multiply(purchaseOrderLineItemObject.getQuantity());
    			totalPoAmount = totalPoAmount.add(totLineAmount);
    		}
    	}

    	return amount.compareTo(totalPoAmount) == 0 ? BigDecimal.ZERO: totalPoAmount;
    }

    private void updatePurchaseOrder() throws AmsException, RemoteException {

        List<String> fieldNames = purchaseOrderUtility.getDefinedFieldNames("");
        for (String fieldName : fieldNames){
            String fieldType = POUploadFileDetails.getAttributeType(purchaseOrder,fieldName);
            if (fieldType == null){

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid),logSequence,
                        "Error fetching attribute type for attribute : " + fieldName);
            } else {
            	String value = (String)fields.get(fieldName);
                if ( "DateTimeAttribute".equals(fieldType)){
                    Date d = null;
                    try {
                        d = purchaseOrderUtility.getDateFormat().parse(value);
                    }  catch (ParseException e) {

                        getLogger().addLogMessage(String.valueOf(a_owner_org_oid),logSequence,
                        "Error saving Pruchase Order .... "
                                + getPurchase_order_num() + " : "
                               + e.getMessage());
                        e.printStackTrace();
                    }
                    purchaseOrder.setAttribute(fieldName, DateTimeUtility.convertDateToDateTimeString(d));
                }else if ( "DateAttribute".equals(fieldType)){
                    Date d = null;
                    try {
                        d = purchaseOrderUtility.getDateFormat().parse(value);
                    } catch (ParseException e) {

                        getLogger().addLogMessage(String.valueOf(a_owner_org_oid),logSequence,
                                "Error saving Pruchase Order .... "
                                        + getPurchase_order_num() + " : "
                                        + e.getMessage());
                        e.printStackTrace();
                    }
                    purchaseOrder.setAttribute(fieldName, DateTimeUtility.convertDateToDateString(d));
                }else {
                	 if (fieldName.contains("seller_user") | fieldName.contains("buyer_user")){
                         String labelValue = purchaseOrderDefinition.getAttribute(fieldName.replace("user", "users")) ;
                         purchaseOrder.setAttribute(fieldName, labelValue);
                         purchaseOrder.setAttribute(fieldName.replace("label","value"), value);
                     } else{
                         purchaseOrder.setAttribute(fieldName, value);
                     }
                }
            }

        }

        // Update all non-PO line item data values in the object
        String[] attributeValues = new String[6];
        String[] attributePaths = new String[6];

        attributePaths[0]  = "upload_definition_oid";
        attributePaths[1]  = "po_file_upload_oid";
        attributePaths[2]  = "creation_date_time";
        attributePaths[3]  = "owner_org_oid";
        attributePaths[4]  = "user_oid";
        attributePaths[5]  = "action";

        attributeValues[0] = String.valueOf(getA_upload_definition_oid());
        attributeValues[1] = String.valueOf(getA_po_file_upload_oid());
        attributeValues[2] = DateTimeUtility.getGMTDateTime(false, false);//BSL IR BLUM041758623 04/19/2012 - do not use override date
        attributeValues[3] = String.valueOf(getA_owner_org_oid());
        attributeValues[4] = String.valueOf(getA_user_oid());
        attributeValues[5] = TradePortalConstants.PO_ACTION_AMEND_UPLOAD;

        purchaseOrder.setAttributes(attributePaths, attributeValues);

    }

    private void resetPurchaseOrder(String previousPOUploadDefinitionOid) throws AmsException, RemoteException {

        PurchaseOrderDefinition tempPODef = (PurchaseOrderDefinition) EJBObjectFactory
                .createClientEJB(ejbServerLocation,
                        "PurchaseOrderDefinition", Long.parseLong(previousPOUploadDefinitionOid) );
        PurchaseOrderUtility poUtility = new PurchaseOrderUtility(tempPODef, mediatorServices);

        List<String> fieldNames = poUtility.getDefinedFieldNames("");

        for (String fieldName : fieldNames){
            purchaseOrder.setAttribute(fieldName, null);
        }

        // Clear the Last Shipment Date field and Item Number field if they were removed from
        // the PO upload definition and we're updating an existing PO line item
        if (getLatest_shipment_date() == null)
        {
            purchaseOrder.setAttribute("latest_shipment_date", null);
        }

        fieldNames = poUtility.getDefinedFieldNames("lineItem");
        DocumentHandler poLineItemsDoc = getMatchingPoLineItems(purchaseOrder.getAttribute("purchase_order_oid"));
        if (poLineItemsDoc != null){
            List<DocumentHandler> poLineItems = poLineItemsDoc.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler poLineItemDoc : poLineItems){
                long poLineItemOid = poLineItemDoc.getAttributeLong("PO_LINE_ITEM_OID");
                PurchaseOrderLineItem purchaseOrderLineItem =
                        (PurchaseOrderLineItem)purchaseOrder.getComponentHandle("PurchaseOrderLineItemList", poLineItemOid);
                if (purchaseOrderLineItem != null){
                    for (String fieldName : fieldNames){
                        purchaseOrderLineItem.setAttribute(fieldName, null);
                    }
                }
            }
        }
    }

    private long getPurchaseOrderLineItemOid(String line_item_num, String purchase_order_oid) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT PO_LINE_ITEM_OID from PURCHASE_ORDER_LINE_ITEM where P_PURCHASE_ORDER_OID = ? and LINE_ITEM_NUM = ? ");
        long poLineItemOid = 0;
        try {
            DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sb.toString(), true, purchase_order_oid, line_item_num);
            if (resultSetDoc != null){
               poLineItemOid = resultSetDoc.getAttributeLong("/ResultSetRecord(0)/PO_LINE_ITEM_OID");
            }

        } catch (AmsException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return poLineItemOid;

    }


    private DocumentHandler getMatchingPoLineItems(String purchase_order_oid) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT PO_LINE_ITEM_OID from PURCHASE_ORDER_LINE_ITEM where P_PURCHASE_ORDER_OID = ? ");

        try {
            return DatabaseQueryBean.getXmlResultSet(sb.toString(), true, new Object[]{purchase_order_oid});
        } catch (AmsException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    /**
     * This method is used to remove the PO line item and PO line item
     * transaction EJBs that were used during the processing of the current PO
     * line item. The calling method parameter is used simply for debugging
     * purposes if an error occurs while removing these objects.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void cleanupPOLineItemData(String methodOrigin) throws RemoteException
    {
        removePOLineItem(methodOrigin);

        if (poLineItemTransaction != null)
        {
            removePOLineItemTransaction(methodOrigin);
        }
    }
    /**
     * This method is used to remove the PO Line Item EJB that was used during
     * the processing of the current PO line item. The calling method parameter
     * is used simply for debugging purposes if an error occurs while removing
     * this object.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removePOLineItem(String methodOrigin) throws RemoteException
    {
        try
        {
            if(purchaseOrder != null)
            {
                purchaseOrder.remove();
                purchaseOrder = null;
            }

            if(copyOfpurchaseOrder != null)
            {
                copyOfpurchaseOrder.remove();
                copyOfpurchaseOrder = null;
            }

        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("Purchase Order", methodOrigin);
        }
    }

    /**
     * This method is used to remove the PO Line Item transaction EJB that was
     * used during the processing of the current PO line item. The calling
     * method parameter is used simply for debugging purposes if an error occurs
     * while removing this object.
     *

     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removePOLineItemTransaction(String methodOrigin) {
        try
        {
            if(poLineItemTransaction != null)
            {
                poLineItemTransaction.remove();
            }
        }
        catch (Exception ex)
        {
            issueRemoveErrorDebugMessage("PO line item transaction", methodOrigin);
        }
    }

    /**
     * This method is used to determine whether or not the current PO line item
     * has the same PO number and item number as a PO line item assigned to a
     * transaction having a pending status (something other than Authorized, Deleted,
     * Cancelled By Bank, and Processed By Bank). If a match is found, the method
     * will validate the current PO line item's data. Next it will determine
     * whether or not several of the current PO line item's fields match the
     * corresponding fields in the matching PO line item. If the values are the
     * same, it will call methods to update the existing record's data, derive
     * several PO line item fields, set various transaction attributes, and add
     * a message to the Auto LC log file. If the values are not the same, the
     * method will unassign the matching PO line item from the transaction it's
     * assigned to, update the existing record's data, and add a message to the
     * Auto LC log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a PO line item match was found
     *                       (true  - a PO line item match was found, so stop processing the PO line item
     *                        false - a PO line item match was not found, so continue processing the PO line item)
     */
    private boolean checkForAssignedPOLineItemMatch(String userLocale) throws AmsException, RemoteException //logu & amit
    {

        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to a transaction that has a
        // status other than Authorized,Partially Authorized,Rejected by Bank,
        // Processed By Bank, Cancelled By Bank, or Deleted
        // Only look for a match where the PO is active for the instrument
         List<Object> sqlParams = new ArrayList<>();
        String sqlQuery = "select a.purchase_order_oid, a.a_shipment_oid from purchase_order a, transaction b where a.a_owner_org_oid = ? "
        	+" and a.purchase_order_num = ? and a.a_transaction_oid = b.transaction_oid  and a.a_instrument_oid is not null "
        	+" and b.transaction_status not in (?,?,?,?,?,?)";
        
        sqlParams.add(a_owner_org_oid);
        sqlParams.add(getPurchase_order_num());
        sqlParams.add(TransactionStatus.AUTHORIZED);
        sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
        sqlParams.add(TransactionStatus.CANCELLED_BY_BANK);
        sqlParams.add(TransactionStatus.DELETED);
        sqlParams.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        sqlParams.add(TransactionStatus.REJECTED_BY_BANK);

        // Retrieve any PO line items that satisfy the SQL that was just constructed
        DocumentHandler matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);

        // If a match was found, try to validate the current PO line item's data and
        // update the matching PO line item.
        if (matchingPOLineItemsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, getPurchase_order_num());

                return true;
            }

//            // Validate the current PO line item's data; if any field is invalid for
//            // whatever reason, stop processing the PO line item and return
//            if (validatePOLineItemData() == false)
//            {
//                return true;
//            }

            // Reorder the following code to derive transaction data
            // regardless whether poLineItemMatchFields = true or false.
            // It used to be done only if poLineItemMatchFields = true.

            // If the PO upload definition, beneficiary, and currency are the same for the
            // current PO line item and matching PO line item, update the existing PO line
            // item with the new data, derive several transaction, terms, and PO line item
            // fields, set several PO line item transaction attributes, and log a message
            // to the Auto LC log file; otherwise, unassign the PO line item, update it with
            // the new PO line item data, and log a message to the Auto LC log file.
            boolean poLineItemMatchFields = poLineItemFieldsMatchTransactionFields(matchingPOLineItemsDoc);
            if (poLineItemMatchFields)
            {
                // Popoulate poLineItem with the uploaded data
                savePOLineItemData(false);

                // Save poLineItem
                // Do not blank out previous_purchase_order_oid and auto_added_to_amend_ind
                // We are just updating PO information.
                //poLineItem.setAttribute("previous_purchase_order_oid", null);
                //poLineItem.setAttribute("auto_added_to_amend_ind",      TradePortalConstants.INDICATOR_NO);
                purchaseOrder.save();

                // Retrieve the transaction that the existing PO line item is currently assigned
                // to so that we can update attributes on it and access the terms associated to it
                getMatchingPOLineItemTransaction();
            }
            else
            {

                // If the bene or currency or po definition is changed then ...
                // If this is a po auto-added to amendment, give error
                //TODO Need to discuss with Nabin how to handle this so commenting for now.
//                if (TradePortalConstants.INDICATOR_YES.equals(purchaseOrder.getAttribute("auto_added_to_amend_ind"))) {
                if (TradePortalConstants.INDICATOR_YES.equals(TradePortalConstants.INDICATOR_NO)) {
                    getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);
                    getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                            TradePortalConstants.LCLOG_LINE_ITEM_DATA_CHANGED,
                            getInstrumentSubstitutionValues(true));
//
                    cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");
                    return true;
                }
                else {
                    // If this is a manually added PO, reset the matching PO line item so that it's no longer assigned to a
                    // transaction
                    // Note within unassignPOLineItem we first get the poLineItemTransaction before
                    // we clear the association on poLineItem.
                    unassignPOLineItem();

                    // Update the existing PO line item's data with the data from the current
                    // PO line item and log a message to the Auto LC log file
                    savePOLineItemData();
                }
            }

            // Derive transaction fields...

            Terms terms = (Terms) poLineItemTransaction.getComponentHandle("CustomerEnteredTerms");
            ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
            int numShipments = shipmentTermsList.getObjectCount();

            List<DocumentHandler> results   = matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord");
            long shipmentTermsOid = results.get(0).getAttributeLong("A_SHIPMENT_OID");

            ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentTermsOid);

            // Get a list of the POs already associated with this instrument
            // Note we have updated the matching PO and saved to db.
            Vector<String> shipmentPOsList = new Vector<>();
            Vector<String> shipmentPONumList = new Vector<>();
            shipmentPONumList.add(getPurchase_order_num());
            String alreadyAssociatedSql = "select purchase_order_oid,purchase_order_num from purchase_order where a_shipment_oid = ? ";
            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new ArrayList().add(shipmentTermsOid));

            if (resultSet == null)  resultSet = new DocumentHandler();

            List<DocumentHandler> alreadyAssociatedList = resultSet.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler anAlreadyAssociatedList : alreadyAssociatedList) {
                String poOid =  anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_OID");
                shipmentPOsList.addElement(poOid);
                shipmentPONumList.addElement(anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_NUM"));
            }
            long instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");
            Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", instrumentOid);
            // Derive transaction fields
            if(TransactionType.AMEND.equals(poLineItemTransaction.getAttribute("transaction_type_code")))
            {
                // Instantiate the instrument so that we can perform a bunch of validations on it
                // and save the new Amend transaction
                

                // Add new logic to get matchPreviousPODoc to replace matchingPOLineItemsDoc
                // If all the POs on the amendment are new (i.e. do not exist on previous transaction),
                // then will derive transaction amount by passing in non-null matchPreviousPODoc.
                // If any PO on the amendment is not new, then do not derive transaction amount by
                // passing in null matchPreviousPODoc.  We need to do this because this scenario
                // requires calculating the differential amounts of the PO and the logic that
                // derives transaction amount does not handle it.
                // This is the same SQL as in checkForAuthorizedAmendOrProcessedByBankMatch().
                sqlQuery = "select a.purchase_order_oid from purchase_order a, transaction b where a.a_owner_org_oid = ? and a.purchase_order_num = ? "
                	+" and a.a_transaction_oid = b.transaction_oid "
                	+" and (b.transaction_type_code =? and (b.transaction_status = ? or b.transaction_status = ?)) ";
                sqlParams = new ArrayList<>();
                sqlParams.add(a_owner_org_oid);
                sqlParams.add(getPurchase_order_num());
                sqlParams.add(TransactionType.AMEND);
                sqlParams.add(TransactionStatus.AUTHORIZED);
                sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
                
                DocumentHandler matchPreviousPODoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,false,sqlParams);

                POLineItemUtility.deriveTranDataFromStruPO(true, poLineItemTransaction, shipmentTerms,
                        numShipments, null, 0, shipmentPOsList,0,null, instrument,null, matchPreviousPODoc, true, null,shipmentPONumList);
                //numShipments, shipmentPOsList, 0, sessionContext, instrument,matchingPOLineItemsDoc, userLocale);


            }
            else
            {
                if (poLineItemMatchFields)
                {
                    // Update the Import LC - Issue transaction's Beneficiary Name and Address
                    updateTransactionBeneficiary(terms);

                    // Update the Import LC - Issue transaction's Currency
                    if (ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", getCurrency()))
                    {
                        poLineItemTransaction.setAttribute("copy_of_currency_code", getCurrency());
                        terms.setAttribute("amount_currency_code", getCurrency());
                    }
                }
                // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
                // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
                // Note all the PO Line Items have been saved into database by now.
                POLineItemUtility.deriveTranDataFromStruPO(true,poLineItemTransaction, shipmentTerms, numShipments,null,0, shipmentPOsList, 0, null,
                		instrument,null, null, true, null,shipmentPONumList);
            }

            poLineItemTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
            poLineItemTransaction.save(false);

            // Log the action we have taken.
            if (poLineItemMatchFields)
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_LINE_ITEM_UPDATED, getPurchase_order_num());

            }
            else
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_LINE_ITEM_REMOVED,
                        getInstrumentSubstitutionValues(true));
            }


            cleanupPOLineItemData("checkForAssignedPOLineItemMatch");

            return true;
        }

        return false;
    }


    /**
     * This method is used to determine whether or not the PO upload definition
     * OID, beneficiary name, and currency for the current PO line item are the
     * same as the existing (i.e., matching) PO line item's fields. It will
     * initially call a method to instantiate the PO line item that was matched,
     * so its data can be retrieved for comparison.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not the current PO line item fields are the same as the existing PO line
     *                       item fields
     *                       (true  - the PO line item fields are the same
     *                        false - the PO line item fields are not the same)
     */
    private boolean poLineItemFieldsMatchTransactionFields(DocumentHandler matchingPOLineItemsDoc)
            throws AmsException, RemoteException
    {

        getMatchingPOLineItem(matchingPOLineItemsDoc);

        long transactionPOUploadDefinitionOid = purchaseOrder.getAttributeLong("upload_definition_oid");
        String transactionBeneficiaryName       = purchaseOrder.getAttribute("seller_name");
        String transactionCurrency              = purchaseOrder.getAttribute("currency");

        if ((a_upload_definition_oid == transactionPOUploadDefinitionOid) &&
                (getSeller_name().equals(transactionBeneficiaryName)) &&
                (getCurrency().equals(transactionCurrency)))
        {
            return true;
        }

        return false;
    }

    /**
     * This method is used to instantiate the transaction that an existing PO
     * line item is assigned to. Once this object has been instantiated, it can
     * be accessed in other methods like deriving fields, updating, etc.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void getMatchingPOLineItemTransaction() throws AmsException, RemoteException
    {
        long   transactionOid  = purchaseOrder.getAttributeLong("transaction_oid");

        poLineItemTransaction = (Transaction) EJBObjectFactory.createClientEJB(ejbServerLocation, "Transaction",
                transactionOid, mediatorServices.getCSDB());
    }

    /**
     * This method is used to instantiate the transaction that an existing PO
     * line item is assigned to. It first calls a method to instantiate the PO
     * line item. Once the transaction object has been instantiated, it can
     * be accessed in other methods like deriving fields, updating, etc.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void getMatchingPOLineItemTransaction(DocumentHandler matchingPOLineItemsDoc)
            throws AmsException, RemoteException
    {

        getMatchingPOLineItem(matchingPOLineItemsDoc);
        getMatchingPOLineItemTransaction();
    }

    /**
     * This method is used to unassign a PO line item from a transaction. It
     * first needs to retrieve the transaction that the PO line item is assigned
     * to so that we can access info from it for an Auto LC log message later.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void unassignPOLineItem() throws AmsException, RemoteException
    {
        getMatchingPOLineItemTransaction();

        purchaseOrder.setAttribute("status", TradePortalConstants.PO_STATUS_UNASSIGNED);
        purchaseOrder.setAttribute("previous_purchase_order_oid", null);
        purchaseOrder.setAttribute("transaction_oid",        null);
        purchaseOrder.setAttribute("instrument_oid",        null);
        purchaseOrder.setAttribute("shipment_oid", null);
    }

    /**
     * This method is used as a wrapper to call the main savePOLineItemData
     * method, which takes a boolean save flag as a parameter.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     */
    public void savePOLineItemData() throws AmsException, RemoteException
    {
        savePOLineItemData(true);
    }

    /**
     * This method is used to update the beneficiary for the transaction that
     * an existing (i.e., matching) PO line item is assigned to. It runs a SQL
     * query to determine if a party exists that matches the beneficiary name
     * currently set in the PO line item. If a match is found, the first terms
     * party associated to the transaction is updated with the beneficiary
     * party info. If the party has a designated bank, the method will update
     * the third terms party as well.
     *

     * @param      terms - the terms associated to the transaction that the existing PO
     *                                                      line item is assigned to
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     void
     */
    private void updateTransactionBeneficiary(Terms terms) throws AmsException, RemoteException
    {

        // Compose the SQL to retrieve the party that matches the beneficiary name currently
        // set in the PO line item and its designated bank (if one exists)
        String sqlQuery = "select party_oid, a_designated_bank_oid from party where p_owner_org_oid = ? and name = ?";

        // Retrieve any parties that satisfy the SQL that was just constructed
        DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, a_owner_org_oid, getSeller_name());

        // If a party was found, instantiate it and update the first terms party for the transaction
        // associated to the PO line item; otherwise, do nothing and return
        if (resultSet != null)
        {
            long partyOid = resultSet.getAttributeLong("/ResultSetRecord(0)/PARTY_OID");

            Party party = (Party) EJBObjectFactory.createClientEJB(ejbServerLocation, "Party", partyOid);

            TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");

            termsParty.copyFromParty(party);

            // If the party found has a designated bank, update the third terms party as well
            String designatedBankOid = resultSet.getAttribute("/ResultSetRecord(0)/A_DESIGNATED_BANK_OID");

            if ((designatedBankOid != null) && (!designatedBankOid.equals("")))
            {
                party = (Party) EJBObjectFactory.createClientEJB(ejbServerLocation, "Party", Long.parseLong(designatedBankOid));

                if(StringFunction.isBlank(terms.getAttribute("c_ThirdTermsParty")))
                {
                    terms.newComponent("ThirdTermsParty");
                    termsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");
                }
                else
                    termsParty = (TermsParty) terms.getComponentHandle("ThirdTermsParty");

                termsParty.copyFromParty(party);
            }

            removeParty(party, "updateTransactionBeneficiary");
        }
    }

    /**
     * This method is used to remove a Party EJB that was used during the
     * processing of the current PO line item. The calling method parameter is
     * used simply for debugging purposes if an error occurs while removing this
     * object.
     *

     * @param      party - the party EJB to remove from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeParty(Party party, String methodOrigin) throws RemoteException
    {
        try
        {
            if(party != null)
            {
                party.remove();
            }
        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("party", methodOrigin);
        }
    }

    /**
     * This method is used to issue a debug error message indicating that an
     * error occurred while trying to remove a particular EJB object from
     * memory.
     *

     * @param      objectToRemove - description of the EJB object being removed from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void issueRemoveErrorDebugMessage(String objectToRemove, String methodOrigin)
    {
        StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Error removing ");
        errorMessage.append(objectToRemove);
        errorMessage.append(" in POLineItemData.");
        errorMessage.append(methodOrigin);
        errorMessage.append("()!");

        LOG.info(errorMessage.toString());
    }

    /**
     * This method is used to determine whether or not the current PO line item
     * has the same PO number and item number as a PO line item assigned to an
     * Import LC - Issue transaction having a status of Authorized. If a match
     * is found, the method will retrieve the Issue transaction associated with
     * the matching PO line item and use it for logging an error message to the
     * Auto LC log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a PO line item match was found
     *                       (true  - a PO line item match was found, so stop processing the PO line item
     *                        false - a PO line item match was not found, so continue processing the PO line item)
     */
    private boolean checkForAuthorizedIssuePOLineItemMatch(String userLocale) throws AmsException, RemoteException
    {
        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to an Issue transaction with a
        // status of Authorized,Partially Authorized or Rejected by Bank
        // Only look for a match where the PO is active for the instrument
        //Krishna IR 12/06/2007 PUH091837811 (added here b.transaction_status)
        String sqlQuery = "select a.purchase_order_oid, a.a_shipment_oid, b.transaction_status from purchase_order a, transaction b where a.a_owner_org_oid = ? "
        	+" and a.purchase_order_num = ? and a.a_transaction_oid = b.transaction_oid  and a.a_instrument_oid is not null "
        	+" and (b.transaction_type_code = ? and b.transaction_status in (?,?,?) or b.transaction_type_code = ? and b.transaction_status = ?)";
        List<Object> sqlParams = new ArrayList<Object>();
        sqlParams.add(a_owner_org_oid);
        sqlParams.add(getPurchase_order_num());
        sqlParams.add(TransactionType.ISSUE);
        sqlParams.add(TransactionStatus.READY_TO_AUTHORIZE);
        sqlParams.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        sqlParams.add(TransactionStatus.REJECTED_BY_BANK);
        sqlParams.add(TransactionType.AMEND);
        sqlParams.add(TransactionStatus.REJECTED_BY_BANK);
        
        // Retrieve any PO line items that satisfy the SQL that was just constructed
        DocumentHandler matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);

        if (matchingPOLineItemsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                //TODO LOG ERROR
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, getPurchase_order_num());

                return true;
            }

            // Retrieve the Issue transaction associated with the matching PO line
            // item and use it for logging an error message to the Auto LC log file
            getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);

            String transStatus= matchingPOLineItemsDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_STATUS");
            if(TransactionStatus.PARTIALLY_AUTHORIZED.equals(transStatus) |
                    TransactionStatus.READY_TO_AUTHORIZE.equals(transStatus))
            {
                // Reorder the following code to derive transaction data
                // regardless whether poLineItemMatchFields = true or false.
                // It used to be done only if poLineItemMatchFields = true.

                // If the PO upload definition, beneficiary, and currency are the same for the
                // current PO line item and matching PO line item, update the existing PO line
                // item with the new data, derive several transaction, terms, and PO line item
                // fields, set several PO line item transaction attributes, and log a message
                // to the Auto LC log file; otherwise, unassign the PO line item, update it with
                // the new PO line item data, and log a message to the Auto LC log file.
                boolean poLineItemMatchFields = poLineItemFieldsMatchTransactionFields(matchingPOLineItemsDoc);
                if (poLineItemMatchFields)
                {
                    // Popoulate poLineItem with the uploaded data
                    savePOLineItemData(false);

                    // Save poLineItem
                    // Do not blank out previous_purchase_order_oid and auto_added_to_amend_ind
                    // We are just updating PO information.
                    //poLineItem.setAttribute("previous_purchase_order_oid", null);
                    //poLineItem.setAttribute("auto_added_to_amend_ind",      TradePortalConstants.INDICATOR_NO);
                    purchaseOrder.save();

                    // Retrieve the transaction that the existing PO line item is currently assigned
                    // to so that we can update attributes on it and access the terms associated to it
                    getMatchingPOLineItemTransaction();
                }
                else
                {

                    // If the bene or currency or po definition is changed then ...
                    // If this is a po auto-added to amendment, give error
                    //TODO Need to discuss with Nabin how to handle this so commenting for now.
//                if (TradePortalConstants.INDICATOR_YES.equals(purchaseOrder.getAttribute("auto_added_to_amend_ind"))) {
                    if (TradePortalConstants.INDICATOR_YES.equals(TradePortalConstants.INDICATOR_NO)) {
                        getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);
                        getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                                TradePortalConstants.LCLOG_LINE_ITEM_DATA_CHANGED,
                                getInstrumentSubstitutionValues(true));
//
                        cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");
                        return true;
                    }
                    else {
                        // If this is a manually added PO, reset the matching PO line item so that it's no longer assigned to a
                        // transaction
                        // Note within unassignPOLineItem we first get the poLineItemTransaction before
                        // we clear the association on poLineItem.
//                        unassignPOLineItem();

                        // Update the existing PO line item's data with the data from the current
                        // PO line item and log a message to the Auto LC log file
                        savePOLineItemData();
                    }
                }

                // Derive transaction fields...
                Terms terms = (Terms) poLineItemTransaction.getComponentHandle("CustomerEnteredTerms");
                ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
                int numShipments = shipmentTermsList.getObjectCount();

                List<DocumentHandler> results   = matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord");
                long shipmentTermsOid = results.get(0).getAttributeLong("A_SHIPMENT_OID");

                ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentTermsOid);

                // Get a list of the POs already associated with this instrument
                // Note we have updated the matching PO and saved to db.
                Vector<String> shipmentPOsList = new Vector<>();
                Vector<String> shipmentPONumList = new Vector<>();
                shipmentPONumList.addElement(getPurchase_order_num());
                String alreadyAssociatedSql = "select purchase_order_oid,purchase_order_num from purchase_order where a_shipment_oid = ? ";
                DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{shipmentTermsOid});

                if (resultSet == null)  resultSet = new DocumentHandler();

                List<DocumentHandler> alreadyAssociatedList = resultSet.getFragmentsList("/ResultSetRecord");
                for (DocumentHandler anAlreadyAssociatedList : alreadyAssociatedList) {
                    String poOid = anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_OID");
                    shipmentPOsList.addElement(poOid);
                    shipmentPONumList.addElement(anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_NUM"));
                }

                // Derive transaction fields
                long instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");
                Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", instrumentOid);
                if(TransactionType.AMEND.equals(poLineItemTransaction.getAttribute("transaction_type_code")))
                {
                    // Instantiate the instrument so that we can perform a bunch of validations on it
                    // and save the new Amend transaction
                    

                    // Add new logic to get matchPreviousPODoc to replace matchingPOLineItemsDoc
                    // If all the POs on the amendment are new (i.e. do not exist on previous transaction),
                    // then will derive transaction amount by passing in non-null matchPreviousPODoc.
                    // If any PO on the amendment is not new, then do not derive transaction amount by
                    // passing in null matchPreviousPODoc.  We need to do this because this scenario
                    // requires calculating the differential amounts of the PO and the logic that
                    // derives transaction amount does not handle it.
                    // This is the same SQL as in checkForAuthorizedAmendOrProcessedByBankMatch().
                    sqlQuery = "select a.purchase_order_oid from purchase_order a, transaction b where a.a_owner_org_oid = ? and a.purchase_order_num = ?"
                    	+" and a.a_transaction_oid = b.transaction_oid"
                    	+" and (b.transaction_type_code = ? and (b.transaction_status = ? or b.transaction_status = ?))";
                    
                    sqlParams = new ArrayList<Object>();
                    sqlParams.add(a_owner_org_oid);
                    sqlParams.add(getPurchase_order_num());
                    sqlParams.add(TransactionType.AMEND);
                    sqlParams.add(TransactionStatus.AUTHORIZED);
                    sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);

                    DocumentHandler matchPreviousPODoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);

                    POLineItemUtility.deriveTranDataFromStrucPO(true,poLineItemTransaction, shipmentTerms, numShipments, shipmentPOsList, 0,
                    		null,instrument, userLocale, null,shipmentPONumList);

                }
                else
                {
                    if (poLineItemMatchFields)
                    {
                        // Update the Import LC - Issue transaction's Beneficiary Name and Address
                        updateTransactionBeneficiary(terms);

                        // Update the Import LC - Issue transaction's Currency
                        if (ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", getCurrency()))
                        {
                            poLineItemTransaction.setAttribute("copy_of_currency_code", getCurrency());
                            terms.setAttribute("amount_currency_code", getCurrency());
                        }
                    }
                    // Derive the amounts (Transaction.amount etc), dates (Terms.expirty_date,
                    // ShipmentTerms.latest_shipment_date etc) and ShipmentTerms.po_line_item.
                    // Note all the PO Line Items have been saved into database by now.
                    POLineItemUtility.deriveTranDataFromStrucPO(true,poLineItemTransaction, shipmentTerms, numShipments, shipmentPOsList, 0,
                    		null,instrument, userLocale, null,shipmentPONumList);
                    
                } 

                poLineItemTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
                poLineItemTransaction.save(false);

                // Log the action we have taken.
                if (poLineItemMatchFields)
                {

                    getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                            TradePortalConstants.LCLOG_LINE_ITEM_UPDATED, getPurchase_order_num());

                }
                else
                {

                    getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                            TradePortalConstants.LCLOG_LINE_ITEM_REMOVED,
                            getInstrumentSubstitutionValues(true));
                }


            }
            else if(TransactionStatus.REJECTED_BY_BANK.equals(transStatus))
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_REJ_BY_BANK_CANT_UPD_PO_DAT,getCompleteInstrumentId());
            }

            else
            {

                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_CANT_UPDATE_PO_DATA,
                        getInstrumentSubstitutionValues(false));
            }

            cleanupPOLineItemData("checkForAuthorizedIssuePOLineItemMatch");

            return true;
        }

        return false;
    }

    /**
     * This method is used to determine whether or not the current PO line item
     * has the same PO number and item number as a PO line item assigned to an
     * Amend transaction having a status of Authorized or a transaction having a
     * status of Processed By Bank. If a match is found, the method will
     * validate the current PO line item's data. If validation is successful,
     * it will then call a method to create a new Import LC - Amend transaction.
     * Next, if the Amend transaction is created successfully, it will call
     * methods to update the existing record's data and add a message to the
     * Auto LC log file.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not a PO line item match was found
     *                       (true  - a PO line item match was found, so stop processing the PO line item
     *                        false - a PO line item match was not found, so continue processing the PO line item)
     */
    private boolean checkForAuthorizedAmendOrProcessedByBankMatch(String userLocale) throws AmsException, RemoteException //logu & amit
    {


        // Compose the SQL to retrieve PO Line Items that match the PO number and line item
        // number passed in and that are currently assigned to either an Amend transaction
        // with a status of Authorized or any transaction with a status of Processed By Bank
        // Only look for a match where the PO is active for the instrument
        // Order by transaction_status_date and transaction_oid descendingly so that the po
        // line item belonging to the latest transaction comes up first.

        //sqlQuery.append("select a.po_line_item_oid, a.a_active_for_instrument from po_line_item a, transaction b where a.a_owner_org_oid = ");
        String sqlQuery = "select a.purchase_order_oid, a.a_instrument_oid, b.transaction_status  from purchase_order a, transaction b where a.a_owner_org_oid =? "
        	+" and a.purchase_order_num = ? and a.a_transaction_oid = b.transaction_oid  and a.a_instrument_oid is not null "
        	+" and (b.transaction_type_code = ? and (b.transaction_status = ?))";
        
        List<Object> sqlParams = new ArrayList<>();
        sqlParams.add(a_owner_org_oid);
        sqlParams.add(getPurchase_order_num());
        sqlParams.add(TransactionType.AMEND);
        sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
        
      //13510 start - we just need processed POs to be considered.
        /*  sqlQuery.append(TradePortalConstants.TRANS_STATUS_AUTHORIZED);

        sqlQuery.append("' or b.transaction_status = '");
        sqlQuery.append(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED);
        
       //Ravindra 19thJun2012 Rel800 IR# RRUM052453512 Start
        //2nd Amendment not Auto Created on PO Upload when 1st Amendment cancelled
        sqlQuery.append("' or b.transaction_status = '");
        sqlQuery.append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK);
        sqlQuery.append("' or b.transaction_status = '");
        sqlQuery.append(TradePortalConstants.TRANS_STATUS_DELETED);
        //Ravindra 19thJun2012 Rel800 IR# RRUM052453512 End 
        sqlQuery.append("' or b.transaction_status = '"); */

        // Retrieve any PO line items that satisfy the SQL that was just constructed
        DocumentHandler matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);
        //if no Amended transactions check if ISS is processed.
        if(matchingPOLineItemsDoc == null){
	        sqlQuery = "select a.purchase_order_oid, a.a_instrument_oid, b.transaction_status  from purchase_order a, transaction b where a.a_owner_org_oid = ? "
	        		+" and a.purchase_order_num = ? and a.a_transaction_oid = b.transaction_oid  and a.a_instrument_oid is not null "
	        		+" and b.transaction_type_code = ?  and b.transaction_status = ?";
	        sqlParams = new ArrayList<Object>();
	        sqlParams.add(a_owner_org_oid);
	        sqlParams.add(getPurchase_order_num());
	        sqlParams.add(TransactionType.ISSUE);
	        sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
	        matchingPOLineItemsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);
        
        }
      //13510 end
        
        
        
        if (matchingPOLineItemsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (matchingPOLineItemsDoc.getFragmentsList("/ResultSetRecord").size() > 1)
            {
                //TODO LOG ERROR
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_MULTIPLE_MATCHES_FOUND, getPurchase_order_num());

                return true;
            }

//            // Validate the current PO line item's data; if any field is invalid for
//            // whatever reason, stop processing the PO line item and return
//            if (validatePOLineItemData() == false)
//            {
//                return true;
//            }

            // Checking matching fields.  If the key fields (po definition oid,
            // beneficiary, currency) are changed, give an error and do not upload.
            if (!poLineItemFieldsMatchTransactionFields(matchingPOLineItemsDoc))
            {
                getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);
                //TODO LOG ERROR
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_LINE_ITEM_DATA_CHANGED,
                        getInstrumentSubstitutionValues(true));

                cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

                return true;
            }



            // Check whether there is pending amendment for the instrument.
            // If there is pending amendment, then add PO to amendment, otherwise create a new amendment.
            // If multiple POs are amended, the first amended PO may create a new amendment
            // or be appended to an existing amendment.  The following amended PO will be appended
            // to the new amendment or appended to the same existing amendment.
            String instrumentOid = matchingPOLineItemsDoc.getAttribute("/ResultSetRecord(0)/A_INSTRUMENT_OID");
            long pendingAmendmentOid = getPendingAmendmentTransaction(instrumentOid);

            // Attempt to create or append to an Import LC - Amend transaction; if an error occurs
            // during this process, stop processing the PO line item and return
            // logu & amit
            if (createOrAppendAmendmentTransaction(matchingPOLineItemsDoc,pendingAmendmentOid, userLocale) == false)
            {
                cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

                return true;
            }

            // After POs have been created and the PO updated with the proper data,
            // save it.
            // DK IR-RAUM051653353 Rel8.1.1 10/16/2012 - purchase order already saved in createOrAppendAmendmentTransaction, this was creating duplicate history
            //purchaseOrder.save();

            // If a copy of the PO line item has been created, save it too
            if(copyOfpurchaseOrder != null)
                copyOfpurchaseOrder.save();


            // Log the creation of amendment or appending PO to existing amendment.
            if (pendingAmendmentOid != 0)
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_ADDED_TO_AMENDMENT,
                        getPurchase_order_num(), getCompleteInstrumentId());
            }
            else
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.LCLOG_AMENDMENT_STARTED,
                        getCompleteInstrumentId(), getPurchase_order_num());
            }

            cleanupPOLineItemData("checkForAuthorizedAmendOrProcessedByBankMatch");

            return true;
        }

        return false;
    }


    /**
     * This method is used to get pending amendment for an instrument
     * If there are multiple amendment, use the one created earliest.
     *

     * @param      instrumentOid
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     long - Pending Amendment transaction Oid.
     *                0 if none if found.
     */
    private long getPendingAmendmentTransaction(String instrumentOid)
            throws AmsException
    {

        String sqlQuery  = "select transaction_oid from transaction"
        		+" where p_instrument_oid = ?  AND transaction_status not in (?,?,?,?,?,?)  AND  transaction.transaction_type_code = ?  order by date_started";
        
        List<Object> sqlParams = new ArrayList<>();
        sqlParams.add(instrumentOid);
        sqlParams.add(TransactionStatus.CANCELLED_BY_BANK);
        sqlParams.add(TransactionStatus.PROCESSED_BY_BANK);
        sqlParams.add(TransactionStatus.DELETED);
        sqlParams.add(TransactionStatus.AUTHORIZED);
        sqlParams.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        sqlParams.add(TransactionStatus.REJECTED_BY_BANK);
        sqlParams.add("AMD");

        DocumentHandler pendingAmdTransactionsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, sqlParams);

        long pendingAmendmentOid = 0;
        if (pendingAmdTransactionsDoc != null)
        {
            // If more than one record is found, log an error message and return
            if (pendingAmdTransactionsDoc.getFragmentsList("/ResultSetRecord").size() > 0)
            {
                pendingAmendmentOid = Long.parseLong(pendingAmdTransactionsDoc.getAttribute("/ResultSetRecord(0)/TRANSACTION_OID"));
            }
        }
        return pendingAmendmentOid;
    }


    /**
     * This method is used to create a new Import LC - Amend transaction for the
     * PO line item currently being processed, or to append the PO to an existing
     * and pending Amendment. It is only called when a PO line
     * item matches an existing PO line item that belong to an Authorized Amendment
     * or some other Processed transaction.  If a new Amend transaction is created,
     * it is created in the same way a normal Import LC - Amend transaction is created.
     *

     * @param      matchingPOLineItemsDoc - xml document containing the OID that
     *                                                                           uniquely identifies the PO line item that
     *                                                                           the current PO line item matched
     * @param     pendingAmendmentOid - Pending Amendment Oid.  Append the PO to this amendment
     *                                        if this parameter is not 0.
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     boolean - indicates whether or not the Import LC - Amend transaction was created successfully
     *                       (true  - the Import LC - Amend transaction was created successfully
     *                        false - the Import LC - Amend transaction was not created successfully)
     */
    private boolean createOrAppendAmendmentTransaction(DocumentHandler matchingPOLineItemsDoc, long pendingAmendmentOid, String userLocale) // logu & amit
            throws AmsException, RemoteException
    {

        // First, retrieve the transaction associated to the existing PO line item that was
        // matched so that we can retrieve its instrument
        getMatchingPOLineItemTransaction(matchingPOLineItemsDoc);

        // Since the PO is now going to be active for the amendment,
        // a copy must be made of its data so that we can maintain data
        // at the transaction level after the PO is no longer active.

        // Populate a copy
        copyOfpurchaseOrder = createCopy(purchaseOrder);
        String[] attributeValues = new String[1];
        String[] attributePaths = new String[1];

        // Clear out some of the associations on the copy.
        // Keep in mind that the transactional and shipment association (assigned_to_trans_oid, shipment_oid) are preserved
        // so that the previous transaction and shipment still own the old PO data
        // Do you really need to null out previous_purchase_order_oid and auto_added_to_amend_ind? Need investigation.  -- Weian Zhu 12/22/04
        attributePaths[0]  = "instrument_oid";
        attributeValues[0] = "";

        copyPOLineItems(purchaseOrder,copyOfpurchaseOrder);
        isAmendTransaction = true;
        // Update the existing PO line item's data with the data from the current
        // PO line item, but don't save yet.
        savePOLineItemData(false);

        // Instantiate the instrument so that we can perform a bunch of validations on it
        // and save the new Amend transaction
        long instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");

        Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", instrumentOid);

           instrumentType = instrument.getAttribute("instrument_type_code");

        if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
            if (!SecurityAccess.canCreateModInstrument(poUploadUserSecurityRights, InstrumentType.APPROVAL_TO_PAY,
                    TransactionType.AMEND))
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
                        InstrumentType.APPROVAL_TO_PAY,
                        TransactionType.AMEND);

                removeInstrument(instrument, "createNewATPAmendmentTransaction");

                return false;
            }
        } else {
            if (!SecurityAccess.canCreateModInstrument(poUploadUserSecurityRights, InstrumentType.IMPORT_DLC,
                    TransactionType.AMEND))
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.NTM_UNAUTH_TO_CREATE_TRANS,
                        InstrumentType.IMPORT_DLC,
                        TransactionType.AMEND);

                removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

                return false;
            }
        }

        // If the status of the instrument is Pending, add an error message to the Auto LC log
        // and stop processing the PO line item
        if (instrument.getAttribute("instrument_status").equals(TradePortalConstants.INSTR_STATUS_PENDING))
        {

            getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence, TradePortalConstants.NTM_INSTR_PENDING);

            removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

            return false;
        }


        // Try locking the instrument so that it's not updated by more than one person at the same
        // time; if someone else is currently updating the instrument, add an error message to the
        // Auto LC log and stop processing the PO line item. If the same user is trying to update
        // the instrument somehow, he/she already has the instrument locked so continue processing.
        try
        {
            LockingManager.lockBusinessObject(instrumentOid, a_user_oid, true);
        }
        catch (InstrumentLockException e)
        {
            long lockingUserOid = e.getUserOid();

            if (!String.valueOf(a_user_oid).equals(String.valueOf(lockingUserOid)))
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.NTM_INSTRUMENT_LOCKED,
                        getInstrumentLockSubstitutionValues(lockingUserOid));

                removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

                return false;
            }
        }

        Terms terms = null;
        long shipmentOid = 0;
        Vector<String> shipmentPOsList = new Vector<>();

        Vector<String> poNum = new Vector<>(2);
        // If there is pending amendment passed in,
        // then add PO to amendment, otherwise create a new amendment.
        Transaction amendTransaction = null;
        long transactionOid = 0;
        if ( pendingAmendmentOid!= 0)
        {
            transactionOid = pendingAmendmentOid;
            amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
            terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");
           shipmentOid = terms.getAttributeLong("first_shipment_oid");

            // Get the list of existing POs.  This is used later to derive transaction data.
            String alreadyAssociatedSql = "select purchase_order_oid,purchase_order_num from purchase_order where a_transaction_oid = ? ";
            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{transactionOid});

            if (resultSet != null)
            {
                List<DocumentHandler> alreadyAssociatedList = resultSet.getFragmentsList("/ResultSetRecord");
                for (DocumentHandler anAlreadyAssociatedList : alreadyAssociatedList) {
                    String poOid = anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_OID");
                    shipmentPOsList.addElement(poOid);
                    poNum.addElement(anAlreadyAssociatedList.getAttribute("/PURCHASE_ORDER_NUM"));
                }
            }
            amendTransaction.setAttribute("transaction_status", TransactionStatus.STARTED);
            amendTransaction.save(false);
        }
        else
        {
            // If no pending amendment is found, create a new amendment and save it
            // First, if the instrument has Pending transactions, add a warning message to the Auto LC log
            // but continue processing
            if (instrument.pendingTransactionsExist())
            {
                getLogger().addLogMessage(String.valueOf(a_owner_org_oid), logSequence,
                        TradePortalConstants.NTM_OTHER_TRANS_PENDING);
            }

            // Create the new amendment transaction and save it
            transactionOid = instrument.newComponent("TransactionList");

            amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);


    			//Needed for AMEND PO's which do have instrument type set.
    			instrumentType = instrument.getAttribute("instrument_type_code");
    			//RKAZI T36000019379 REL 8.2 08/20/213 - End

            if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
                amendTransaction.createNewBlankStructPOTran(TransactionType.AMEND, String.valueOf(a_user_oid), String.valueOf(a_owner_org_oid),
                        InstrumentType.APPROVAL_TO_PAY, false, "", null, purchaseOrder);
            } else {
                amendTransaction.createNewBlankStructPOTran(TransactionType.AMEND, String.valueOf(a_user_oid), String.valueOf(a_owner_org_oid),
                        InstrumentType.IMPORT_DLC, false, "", null, purchaseOrder);
            }



            terms = (Terms) amendTransaction.getComponentHandle("CustomerEnteredTerms");
            shipmentOid = terms.getAttributeLong("first_shipment_oid");

            // Save the new transaction and PO Line Items before trying to derive the data
            // since the derivation assumes the data are saved in the database.
            instrument.save(false);
        }

        shipmentPOsList.addElement(purchaseOrder.getAttribute("purchase_order_oid"));
        ComponentList shipmentTermsList = (ComponentList) terms.getComponentHandle("ShipmentTermsList");
        ShipmentTerms shipmentTerms = (ShipmentTerms)shipmentTermsList.getComponentObject(shipmentOid);


        // Set several transaction-related attributes in the PO line item and save it.
        attributeValues = new String[6];
        attributePaths  = new String[6];

        attributePaths[0]  = "previous_purchase_order_oid";
        attributePaths[1]  = "transaction_oid";
        attributePaths[2]  = "instrument_oid";
        attributePaths[3]  = "shipment_oid";
        attributePaths[4]  = "status";
        attributePaths[5]  = "amend_seq_no";

        attributeValues[0] = copyOfpurchaseOrder.getAttribute("purchase_order_oid");
        attributeValues[1] = String.valueOf(transactionOid);
        attributeValues[2] = String.valueOf(instrumentOid);
        attributeValues[3] = String.valueOf(shipmentOid);
        attributeValues[4] = TradePortalConstants.PO_STATUS_ASSIGNED;
        String seq=copyOfpurchaseOrder.getAttribute("amend_seq_no");
        if (seq != null) {
        	seq=String.valueOf(Integer.parseInt(seq) + 1);
        }
        else {
        	seq="0";
        }
        attributeValues[5] = seq;
        
        purchaseOrder.setAttributes(attributePaths, attributeValues);
       
        poNum.addElement(purchaseOrder.getAttribute("purchase_order_num"));
        purchaseOrder.save();
        copyOfpurchaseOrder.save();
        

        POLineItemUtility.deriveTranDataFromStrucPO(true,amendTransaction, shipmentTerms,
                1, shipmentPOsList, 0, null, instrument, userLocale, null,poNum);
        String goodsDescription = PropertyResourceBundle
                .getBundle("TextResources").getString("PurchaseOrders.POItemsAmended");
        shipmentTerms.setAttribute("goods_description", goodsDescription);
        // Save any change of transaction data derived from PO.
        instrument.save(false);

        removeInstrument(instrument, "createNewImportLCAmendmentTransaction");

        return true;
    }

    private void copyPOLineItems(PurchaseOrder purchaseOrder, PurchaseOrder copyOfPurchaseOrder) throws AmsException, RemoteException {
        String copyOfPurchaseOrderOid = copyOfPurchaseOrder.getAttribute("purchase_order_oid");
        ComponentList lineItemsList = (ComponentList) purchaseOrder
                .getComponentHandle("PurchaseOrderLineItemList");
        BusinessObject busObj = null;
        for (int i = 0 ; i < lineItemsList.getObjectCount(); i++){
            lineItemsList.scrollToObjectByIndex(i);
            busObj = lineItemsList.getBusinessObject( );
            busObj.setAttribute("purchase_order_oid", copyOfPurchaseOrderOid);
            busObj.save();
        }


    }

    /**
     * Creates a copy of the passed-in purchase order.   The copy is identical except
     * for the OID.
     *
     * @param po - the PO line item that is being copied
     * @return a copy of the PO line item with all fields the same except for the OID
     *
     */
    private PurchaseOrder createCopy (PurchaseOrder po) throws AmsException, RemoteException
    {
        // Dump data to XML first, then use that to populate the new object
        DocumentHandler xml = new DocumentHandler();
        xml = po.populateXmlDoc(xml);

        PurchaseOrder copy = (PurchaseOrder) EJBObjectFactory.createClientEJB(ejbServerLocation, "PurchaseOrder");
        copy.newObject();
        copy.populateFromXmlDoc(xml);

        return copy;
    }

    /**
     * This method is used to remove an Instrument EJB that was used during the
     * processing of the current PO line item. The calling method parameter is
     * used simply for debugging purposes if an error occurs while removing this
     * object.
     *

     * @param      instrument - the instrument EJB to remove from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeInstrument(Instrument instrument, String methodOrigin) throws RemoteException
    {
        try
        {
            if(instrument != null)
            {
                instrument.remove();
            }
        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("instrument", methodOrigin);
        }
    }

    /**
     * This method is used to retrieve an array of substitution values
     * containing instrument data for use in several Auto LC error log messages.
     * The flag determines whether or not the PO line item number should be
     * included as a substitution parameter.
     *

     * @param      includePOLineItemNumberFlag - indicates whether or not the PO line item number should be
     *                                                   included as a substitution parameter
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     java.lang.String [] - an array of Auto LC log error message substitution values containing instrument
     *                                   data such as the instrument ID and transaction type
     */
    private String[] getInstrumentSubstitutionValues(boolean includePOLineItemNumberFlag)
            throws AmsException, RemoteException
    {

        String[]     substitutionValues;
        String       transactionType;
        int          index = 0;

        if (includePOLineItemNumberFlag)
        {
            substitutionValues = new String[3];
            substitutionValues[index++] = getPurchase_order_num();
        }
        else
        {
            substitutionValues = new String[2];
        }

        transactionType = poLineItemTransaction.getAttribute("transaction_type_code");

        substitutionValues[index++] = getCompleteInstrumentId();
        substitutionValues[index]   = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
                transactionType);

        return substitutionValues;
    }

    /**
     * This method is used to retrieve the complete instrument ID of the
     * instrument that the user is trying to create a transaction for.
     *

     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     java.lang.String - the complete instrument ID of the instrument that the user is trying to create a
     *                                transaction for
     */
    private String getCompleteInstrumentId() throws AmsException, RemoteException
    {
        long instrumentOid = poLineItemTransaction.getAttributeLong("instrument_oid");

        Instrument instrument = (Instrument) EJBObjectFactory.createClientEJB(ejbServerLocation, "Instrument", instrumentOid);

        String completeInstrumentId = instrument.getAttribute("complete_instrument_id");

        removeInstrument(instrument, "getCompleteInstrumentId");

        return completeInstrumentId;
    }

    /**
     * This method is used to retrieve an array of substitution values
     * containing instrument lock data for use in an Auto LC error log message.
     * The oid passed in is used to lookup the name of the user who currently
     * has the instrument locked that the current user is trying to create a
     * transaction for.
     *

     * @param      lockingUserOid - unique OID of the user who currently has the instrument locked
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @exception  java.rmi.RemoteException
     * @return     java.lang.String [] - an array of Auto LC log error message substitution values containing instrument
     *                                   lock data such as the instrument type, transaction type, and user name
     */
    private String[] getInstrumentLockSubstitutionValues(long lockingUserOid) throws AmsException, RemoteException
    {
        String[]   substitutionValues = new String[4];
        User       lockUser = (User) EJBObjectFactory.createClientEJB(ejbServerLocation, "User", lockingUserOid);


        if (instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) {
            substitutionValues[0] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                    InstrumentType.APPROVAL_TO_PAY);
        } else {
            substitutionValues[0] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
                    InstrumentType.IMPORT_DLC);
        }


        substitutionValues[1] = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
                TransactionType.AMEND);
        substitutionValues[2] = lockUser.getAttribute("first_name");
        substitutionValues[3] = lockUser.getAttribute("last_name");

        removeUser(lockUser, "getInstrumentLockSubstitutionValues");

        return substitutionValues;
    }

    /**
     * This method is used to remove a User EJB that was used during the
     * processing of the current PO line item. The calling method parameter is
     * used simply for debugging purposes if an error occurs while removing this
     * object.
     *

     * @param      user - the user EJB to remove from memory
     * @param      methodOrigin - the name of the calling method
     * @return     void
     */
    private void removeUser(User user, String methodOrigin) throws RemoteException
    {
        try
        {
            if(user != null)
            {
                user.remove();
            }
        }
        catch (RemoveException ex)
        {
            issueRemoveErrorDebugMessage("user", methodOrigin);
        }
    }

}
