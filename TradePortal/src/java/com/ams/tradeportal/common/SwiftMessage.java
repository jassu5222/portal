package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * This class is used for representing a Swift message in the Trade Portal. It
 * is used entirely for the purpose of downloading Export Letter of Credit -
 * Advise transaction data, and therefore only handles the following Swift
 * message types: 700, 701, 710, 711, 720, and 721. The class contains methods
 * that return the ID of the Swift message, the source (or origin) of the Swift
 * message, and the relevant data to be downloaded from the Swift message. The
 * class will definitely need to be modified/enhanced to make it applicable for
 * all Swift message types or for other Trade Portal purposes that do not
 * include the downloading of Export Letter of Credit - Advise data.
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 *

 */
public class SwiftMessage {
private static final Logger LOG = LoggerFactory.getLogger(SwiftMessage.class);

    private String swiftMessageSource = null;
    private String swiftMessageData = null;
    private String swiftMessageId = null;
    private static final String LINE_DELIMITER = ":   :";
    private static final int SWIFT_TAG_SIZE = 5;
    private final char NEW_LINE_CHAR = '\n';//Added by LOGANATHAN - 05/13/2005 - IR LNUF031779896

    /**
     * Creates an instance of the SwiftMessage with the given text.
     *

     * @param text java.lang.String
     */
    public SwiftMessage(String text) {
        int index = 0;

        swiftMessageData = text;
        //LOGANATHAN - 08/18/2005 - IR NBUF081764129
        if (swiftMessageData != null && swiftMessageData.charAt(0) == '\n') {
            swiftMessageData = swiftMessageData.substring(1, swiftMessageData.length());
        }

		// Extract fields from the SWIFT header
        // The SWIFT header will always be formatted something like this:
        // SWIFT I700 RECEIVED FROM SWIFT ID BNAPFRPP XXX ON Jan 01, 2001
		// "700" is the ID we are looking for
        // "BNAPFRPP XXX" is the source we are looking for
        if(swiftMessageData != null){
        	swiftMessageId = swiftMessageData.substring(7, 10);
            swiftMessageSource = swiftMessageData.substring(34, 47);	
        }
    }

    /**
     * This method is used to return the Swift message's ID.
     *

     * @return java.lang.String - the ID of the Swift message
     */
    public String getSwiftMessageId() {
        return swiftMessageId;
    }

    /**
     * This method is used to return the source where the Swift message
     * originated from.
     *

     * @return java.lang.String - the source of the Swift message
     */
    public String getSwiftMessageSource() {
        return swiftMessageSource;
    }

    /**
     * This method is used to return a hashtable of Swift message data. The
     * hashtable's keys are the names associated to Swift line numbers with data
     * in them (i.e., "CreditNumber" = ':20 :'). This method only applies to the
     * following Swift message IDs: 700, 701, 710, 711, 720, and 721.
     *

     * @return java.util.Hashtable - hashtable containing all relevant data from
     * the Swift message
     */
    public Hashtable getSwiftMessageData() {

        if ((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700))
                || (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710))
                || (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720))) {
            return getSwiftMessageMainData();
        } else if ((swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_701))
                || (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_711))
                || (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_721))) {
            return getSwiftMessageAttachmentData();
        } else {
            LOG.debug("SwiftMessage: Invalid SWIFT Message ID!");

            return null;
        }
    }

    /**
     * Overload of other getValueForSwiftTag() method. Defaults line number
     * being obtained to 1.
     *
     * @param tagString - the string denoting the SWIFT tag (i.e. ":20 :")
     */
    private String getValueForSwiftTag(String tagString) {
        return getValueForSwiftTag(tagString, 1);
    }

    /**
     * Retrieves the data that is contained in the SWIFT message for a
     * particular tag An example of a portion of a message is:
	 *
	 * :59 :Beneficiary
	 * :   :Centre For Marine and Coastal Study
	 * :   :The University of NSW
	 * :   :Sydney NSW 2052
	 * :   :
	 * :32B:Currency Code,  Amount
	 *
     * This method looks for data on a particular line. The line with the actual
     * SWIFT tag and the label is considered line 1
     *
     * @param tagString - the string denoting the SWIFT tag (i.e. ":20 :")
     * @param lineNumber - the line number within the SWIFT tag data to retrieve
     */
    private String getValueForSwiftTag(String tagString, int lineNumber) {

        // Get index of where the SWIFT tag is located
        int tagLineIndex = swiftMessageData.indexOf(tagString);

        if (tagLineIndex > 0) {
            // If the SWIFT tag is found, begin looping through the lines
            int currentIndex = tagLineIndex;
            int beginIndex = 0;
            int endIndex = 0;

            for (int i = 1; i <= lineNumber; i++) {

                // Determine the starting point for the entire line (including first five characters)
                beginIndex = swiftMessageData.indexOf(LINE_DELIMITER, currentIndex);
                // Determine where the next delimiter is in the line
                endIndex = swiftMessageData.indexOf(LINE_DELIMITER, beginIndex + SWIFT_TAG_SIZE);

                // Move up the pointer
                currentIndex = endIndex;

                // W Zhu 11/3/2010 NIUK102059836 Check end of file.
                if (beginIndex < 0 || beginIndex + SWIFT_TAG_SIZE >= swiftMessageData.length()) {
                    return "";
                }

                //START - Added by LOGANATHAN - 05/25/2005 - IR LNUF031779896
                //check for '\n' included messages
                if (swiftMessageData.charAt(beginIndex + SWIFT_TAG_SIZE) == '\n'
                        && (beginIndex + SWIFT_TAG_SIZE + 1 >= swiftMessageData.length()
                        || swiftMessageData.charAt(beginIndex + SWIFT_TAG_SIZE + 1) == ':')) {
                    return "";
                }
				//END - Added by LOGANATHAN - 05/25/2005 - IR LNUF031779896

				// Check that we have not looped past the end of the data for the
                // line.  When that happens, the data will always begin with a colon
                if (swiftMessageData.charAt(beginIndex + SWIFT_TAG_SIZE) == ':') {
					// We've gone beyond the data for the desired tag and into another
                    // one.   Basically, this means that no data could be found for the line number
                    return "";
                }
            }
			//START - Added by LOGANATHAN - 05/25/2005 - IR LNUF031779896
            //Check for removing the new line character(s) from the parsed data.			
            if (swiftMessageData.charAt(endIndex - 1) == NEW_LINE_CHAR) {
                return swiftMessageData.substring(beginIndex + SWIFT_TAG_SIZE, endIndex - 1);
            } else {
                return swiftMessageData.substring(beginIndex + SWIFT_TAG_SIZE, endIndex);
            }
            //END - Added by LOGANATHAN - 05/25/2005 - IR LNUF031779896
        } else {
            return "";
        }
    }

    /**
     * Parses through the SWIFT message data to find a tag and then places that
     * value into a passed in hashtable.
     *
     * @param hashtable - the hashtable to which the key and value are added
     * @param hashkey - the key of the hashtable
     * @param tagString - the string denoting the SWIFT tag (i.e. ":20 :")
     * @param allowBlanks - determines whether or not to put an entry in the
     * hashtable when there is no value for the tag
     * @param lineNumber - the line number within the SWIFT tag data to retrieve
     */
    private void putValueIntoHash(Hashtable hashtable, String hashkey, String tagString, boolean allowBlanks, int lineNumber) {
        String valueForTag = getValueForSwiftTag(tagString, lineNumber);

        if (allowBlanks) {
            hashtable.put(hashkey, valueForTag);
        } else {
            if (!valueForTag.equals("")) {
                hashtable.put(hashkey, valueForTag);
            }
        }
    }

    /**
     * Overload of above method. Defaults line number to 1.
     *
     * @param hashtable - the hashtable to which the key and value are added
     * @param hashkey - the key of the hashtable
     * @param tagString - the string denoting the SWIFT tag (i.e. ":20 :")
     * @param allowBlanks - determines whether or not to put an entry in the
     * hashtable when there is no value for the tag
     */
    private void putValueIntoHash(Hashtable hashtable, String hashkey, String tagString, boolean allowBlanks) {
        putValueIntoHash(hashtable, hashkey, tagString, allowBlanks, 1);
    }

    /**
     * Parses through the SWIFT message data to find a tag and then places that
     * value into a passed in hashtable. All lines for that value are stored in
     * the hashtable, but the delimiters are removed.
     *
     * @param hashtable - the hashtable to which the key and value are added
     * @param hashkey - the key of the hashtable
     * @param tagString - the string denoting the SWIFT tag (i.e. ":20 :")
     * @param allowBlanks - determines whether or not to put an entry in the
     * hashtable when there is no value for the tag
     */
    private void putMultiLineValueIntoHash(Hashtable hashtable, String hashkey, String tagString,
            boolean allowBlanks, int maxLineNumber) {
        StringBuilder completeValue = new StringBuilder();

        for (int lineNumber = 1; lineNumber <= maxLineNumber; lineNumber++) {
            String lineValue = getValueForSwiftTag(tagString, lineNumber);
            completeValue.append(lineValue);
        }

        if (allowBlanks) {
            hashtable.put(hashkey, completeValue.toString());
        } else {
            if (!completeValue.toString().equals("")) {
                hashtable.put(hashkey, completeValue.toString());
            }
        }
    }

    /**
     * This method is used to return a hashtable of Swift message data for the
     * main Swift message types (i.e., 700, 710, and 720). It will check the
     * Swift message for numerous types of data, and if the data is found in the
     * Swift message, it will be placed in the hashtable using a key that
     * describes the data (i.e., for ':20 :LC/IN30' in the Swift message, the
     * key would be "CreditNumber" and the corresponding value would be
     * 'LC/IN30').
     *

     * @return java.util.Hashtable - hashtable containing all relevant data from
     * the Swift message
     */
    private Hashtable getSwiftMessageMainData() {
        Hashtable swiftMessageMainData = new Hashtable();

        // Retrieve the Credit Form data
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700)) {
            putValueIntoHash(swiftMessageMainData, "CreditFormLine1", ":40A:", true);
            swiftMessageMainData.put("CreditFormLine2", "");
        } else {
            putValueIntoHash(swiftMessageMainData, "CreditFormLine1", ":40B:", true, 1);
            putValueIntoHash(swiftMessageMainData, "CreditFormLine2", ":40B:", true, 2);
        }

        // Retrieve the Credit Number
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700)) {
            putValueIntoHash(swiftMessageMainData, "CreditNumber", ":20 :", true);
        } else {
            putValueIntoHash(swiftMessageMainData, "CreditNumber", ":21 :", true);
        }

        // Retrieve the Issue Date
        putValueIntoHash(swiftMessageMainData, "IssueDate", ":31C:", true);

        // Retrieve the Expiry Date and Expiry Place
        String expiryDateAndPlace = getValueForSwiftTag(":31D:");

        swiftMessageMainData.put("ExpiryDate", expiryDateAndPlace.substring(0, 6));
        swiftMessageMainData.put("ExpiryPlace", expiryDateAndPlace.substring(6, expiryDateAndPlace.length()));

        // Retrieve the Issuing Bank
        if (!swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700)) {
            putValueIntoHash(swiftMessageMainData, "IssuingBankName", ":52A:", false, 1);
            putValueIntoHash(swiftMessageMainData, "IssuingBankAddressLine1", ":52A:", false, 2);
            putValueIntoHash(swiftMessageMainData, "IssuingBankAddressLine2", ":52A:", false, 3);
            putValueIntoHash(swiftMessageMainData, "IssuingBankAddressLine3", ":52A:", false, 4);

            //IR GOUH051562143 Rajat - start
            putValueIntoHash(swiftMessageMainData, "NonBankIssuerName", ":50B:", false, 1);
            putValueIntoHash(swiftMessageMainData, "NonBankIssuerAddressLine1", ":50B:", false, 2);
            putValueIntoHash(swiftMessageMainData, "NonBankIssuerAddressLine2", ":50B:", false, 3);
            putValueIntoHash(swiftMessageMainData, "NonBankIssuerAddressLine3", ":50B:", false, 4);
            //IR GOUH051562143 Rajat - end

        }

        // Retrieve the Applicant's Bank
        putValueIntoHash(swiftMessageMainData, "ApplicantBankName", ":51 :", false, 1);
        putValueIntoHash(swiftMessageMainData, "ApplicantBankAddressLine1", ":51 :", false, 2);
        putValueIntoHash(swiftMessageMainData, "ApplicantBankAddressLine2", ":51 :", false, 3);
        putValueIntoHash(swiftMessageMainData, "ApplicantBankAddressLine3", ":51 :", false, 4);

        // Retrieve the Applicant
        putValueIntoHash(swiftMessageMainData, "ApplicantName", ":50 :", false, 1);
        putValueIntoHash(swiftMessageMainData, "ApplicantAddressLine1", ":50 :", false, 2);
        putValueIntoHash(swiftMessageMainData, "ApplicantAddressLine2", ":50 :", false, 3);
        putValueIntoHash(swiftMessageMainData, "ApplicantAddressLine3", ":50 :", false, 4);

        // Retrieve the Beneficiary 1
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720)) {
            putValueIntoHash(swiftMessageMainData, "Beneficiary1Name", ":50 :", false, 1);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine1", ":50 :", false, 2);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine2", ":50 :", false, 3);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine3", ":50 :", false, 4);
        } else {
            putValueIntoHash(swiftMessageMainData, "Beneficiary1Name", ":59 :", false, 1);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine1", ":59 :", false, 2);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine2", ":59 :", false, 3);
            putValueIntoHash(swiftMessageMainData, "Beneficiary1AddressLine3", ":59 :", false, 4);
        }

        // Retrieve the Beneficiary 2
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_720)) {
            putValueIntoHash(swiftMessageMainData, "Beneficiary2Name", ":59 :", false, 1);
            putValueIntoHash(swiftMessageMainData, "Beneficiary2AddressLine1", ":59 :", false, 2);
            putValueIntoHash(swiftMessageMainData, "Beneficiary2AddressLine2", ":59 :", false, 3);
            putValueIntoHash(swiftMessageMainData, "Beneficiary2AddressLine3", ":59 :", false, 4);
        }

        // Retrieve the Credit Currency
        String currencyAndAmount = getValueForSwiftTag(":32B:");

        swiftMessageMainData.put("CreditCurrency", currencyAndAmount.substring(0, 3));
        swiftMessageMainData.put("CreditAmount", currencyAndAmount.substring(3, currencyAndAmount.length()));

        // Retrieve the Amount Tolerance values
        String amountTolerance = getValueForSwiftTag(":39A:");
        if (!amountTolerance.equals("")) {
            swiftMessageMainData.put("PlusTolerance", amountTolerance.substring(0, 2));
            swiftMessageMainData.put("MinusTolerance", amountTolerance.substring(2, amountTolerance.length()));
        }

        // Retrieve the Maximum Amount
        putValueIntoHash(swiftMessageMainData, "MaximumAmount", ":39A:", false);

        // Retrieve the Additional Cover
        putMultiLineValueIntoHash(swiftMessageMainData, "AdditionalCover", ":39C:", false, 4);

        // Retrieve the Available With ... By values
        putValueIntoHash(swiftMessageMainData, "AvailableWith", ":41D:", true, 1);
        putValueIntoHash(swiftMessageMainData, "AvailableBy", ":41D:", true, 2);

        // Retrieve the Tenor
        putMultiLineValueIntoHash(swiftMessageMainData, "Tenor", ":42C:", true, 3);

        // Retrieve the Drawee Bank
        String draweeBankId = getValueForSwiftTag(":42A:");
        if (!draweeBankId.equals("")) {
            swiftMessageMainData.put("DraweeBankIdentifierCode", draweeBankId);
        } else {
            putValueIntoHash(swiftMessageMainData, "DraweeBankName", ":42D:", false, 1);
            putValueIntoHash(swiftMessageMainData, "DraweeBankAddressLine1", ":42D:", false, 2);
            putValueIntoHash(swiftMessageMainData, "DraweeBankAddressLine2", ":42D:", false, 3);
            putValueIntoHash(swiftMessageMainData, "DraweeBankAddressLine3", ":42D:", false, 4);
        }

        // Retrieve the Mixed Payment Details
        putMultiLineValueIntoHash(swiftMessageMainData, "MixedPaymentDetails", ":42M:", false, 4);

        // Retrieve the Deferred Payment Details
        putMultiLineValueIntoHash(swiftMessageMainData, "DeferredPaymentDetails", ":42P:", false, 4);

        // Retrieve the Partial Shipment
        putValueIntoHash(swiftMessageMainData, "PartialShipment", ":43P:", false);

        // Retrieve the Transshipment
        putValueIntoHash(swiftMessageMainData, "Transshipment", ":43T:", false);

        // Retrieve the Ship From location
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710)) {
            putValueIntoHash(swiftMessageMainData, "ShipFrom", ":44E:", false); //pcutrone - IR-SDUH032346296 - Look for extra ship from tag for MT710
        } else {
            putValueIntoHash(swiftMessageMainData, "ShipFrom", ":44A:", false);
        }

        // Retrieve the Ship To location
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_710)) {
            putValueIntoHash(swiftMessageMainData, "ShipTo", ":44F:", false); //pcutrone - IR-SDUH032346296 - Look for extra ship to tag for MT710
        } else {
            putValueIntoHash(swiftMessageMainData, "ShipTo", ":44B:", false);
        }

        // Retrieve the Latest Ship Date
        putValueIntoHash(swiftMessageMainData, "LatestShipDate", ":44C:", false);

        // Retrieve the Shipment Period
        putMultiLineValueIntoHash(swiftMessageMainData, "ShipmentPeriod", ":44D:", false, 6);

		//IR GOUH051562143 Rajat - start
        // Retrieve the Port of Loading
        putValueIntoHash(swiftMessageMainData, "PortOfLoading", ":44E:", false);

        // Retrieve the Port of Discharge
        putValueIntoHash(swiftMessageMainData, "PortOfDischarge", ":44F:", false);

        // Retrieve the Applicable Rules
        putValueIntoHash(swiftMessageMainData, "ApplicableRules", ":40E:", false);

		//IR GOUH051562143 Rajat - end
        // Retrieve the Goods Description
        putMultiLineValueIntoHash(swiftMessageMainData, "GoodsDescription", ":45A:", false, 65);

        // Retrieve the Documents Required
        putMultiLineValueIntoHash(swiftMessageMainData, "DocumentsRequired", ":46A:", false, 65);

        // Retrieve the Additional Conditions
        putMultiLineValueIntoHash(swiftMessageMainData, "AdditionalConditions", ":47A:", false, 65);

		// Narayan IR-NIUK102059836 31-Oct-10 Begin
        // Retrieve the Goods Description
        putMultiLineValueIntoHash(swiftMessageMainData, "GoodsDescriptionPart2", ":45B:", false, 200);

        // Retrieve the Documents Required
        putMultiLineValueIntoHash(swiftMessageMainData, "DocumentsRequiredPart2", ":46B:", false, 200);

        // Retrieve the Additional Conditions
        putMultiLineValueIntoHash(swiftMessageMainData, "AdditionalConditionsPart2", ":47B:", false, 200);

		//Narayan IR-NIUK102059836 31-Oct-10 End
        // Retrieve the Charges
        putMultiLineValueIntoHash(swiftMessageMainData, "Charges", ":71B:", false, 6);

        // Retrieve the Presentation Period
        putMultiLineValueIntoHash(swiftMessageMainData, "PresentationPeriod", ":48 :", false, 4);

        // Retrieve the Confirmation Instructions
        putValueIntoHash(swiftMessageMainData, "ConfirmationInstructions", ":49 :", false);

        // Retrieve the Sending Bank
        if (!swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_700)) {
            putValueIntoHash(swiftMessageMainData, "SendingBankName", ":52A:", false, 1);
            putValueIntoHash(swiftMessageMainData, "SendingBankAddressLine1", ":52A:", false, 2);
            putValueIntoHash(swiftMessageMainData, "SendingBankAddressLine2", ":52A:", false, 3);
            putValueIntoHash(swiftMessageMainData, "SendingBankAddressLine3", ":52A:", false, 4);
        }

        return swiftMessageMainData;
    }

    /**
     * This method is used to return a hashtable of Swift message data for the
     * attachment Swift message types (i.e., 701, 711, and 721). It will check
     * the Swift message for several types of data, and if the data is found in
     * the Swift message, it will be placed in the hashtable using a key that
     * describes the data (i.e., for ':20 :LC/IN30' in the Swift message, the
     * key would be "CreditNumber" and the corresponding value would be
     * 'LC/IN30'). Attachment messages are used when the maximum amount of
     * characters in a free form field in a main message has been met; the field
     * is simply continued in an attachment message.
     *

     * @return java.util.Hashtable - hashtable containing all relevant data from
     * the Swift message
     */
    private Hashtable getSwiftMessageAttachmentData() {
        Hashtable swiftMessageAttachmentData = new Hashtable();

        // Retrieve the Credit Number
        if (swiftMessageId.equals(TradePortalConstants.SWIFT_MESSAGE_701)) {
            putValueIntoHash(swiftMessageAttachmentData, "CreditNumber", ":20 :", true);
        } else {
            putValueIntoHash(swiftMessageAttachmentData, "CreditNumber", ":21 :", true);
        }

        // Retrieve the Goods Description
        putValueIntoHash(swiftMessageAttachmentData, "GoodsDescription", ":45A:", false);

        // Retrieve the Documents Required
        putValueIntoHash(swiftMessageAttachmentData, "DocumentsRequired", ":46A:", false);

        // Retrieve the Additional Conditions
        putValueIntoHash(swiftMessageAttachmentData, "AdditionalConditions", ":47A:", false);

        return swiftMessageAttachmentData;
    }
}
