package com.ams.tradeportal.common;
import org.slf4j.Logger;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.TPCalendarYear;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;
/**
 * TPDateTimeUtility contains several methods for working with dates.
 * These include methods to format dates, convert dates from one format
 * to another and to get the currency local time based on GMT time.
 * <p>
 * Some of the methods require a formatType parameter.  There are two
 * constants defined for this class (SHORT and LONG) that should be
 * used for this parameter.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class TPDateTimeUtility {
private static final Logger LOG = LoggerFactory.getLogger(TPDateTimeUtility.class);

    public static final int    NUMBER_OF_EURO_DATE_CHARS = 10;
	public static final String LONG                      = "2";
	public static final String SHORT                     = "1";

/**
 * TPDateTimeUtility constructor.
 */
public TPDateTimeUtility() {
	super();
}

    /**
     * Converts time HH:mm to HH:mm AM or PM
     *
     * @param timeZone
     * @return
     */
    public static String getCurrentTimeForRequestTimeZone(String timeZone) {
        SimpleDateFormat timeZoneFormat = null;
        Date date;

        try {
            String time = getLocalDateTime(DateTimeUtility.getCurrentDateTime(), timeZone).toString();
            time = time.substring(10, 16);
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            timeZoneFormat = new SimpleDateFormat("HH:mm a");
            date = timeFormat.parse(time);
        } catch (ParseException ex) {
            return timeZone;
        }

        String returnTime = timeZoneFormat.format(date);

        return returnTime;
    }

    /**
     * Converts an ISO Date (i.e. yyyy-mm-dd) to JPylon format mm/dd/yyyy
     * 00:00:00.
     *
     * @param isoDate
     * @return
     */
    public static String convertISODateToJPylonDate(String isoDate) {
        String jpylonDate = isoDate; //default
        if (isoDate != null && isoDate.contains("-")) { //is it in iso format?
            SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat jpylonDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date;
            try {
                date = isoDateFormat.parse(isoDate);
            } catch (ParseException ex) {
                return isoDate;
            }
            jpylonDate = jpylonDateFormat.format(date);
        }
        return jpylonDate;
    }

    public static String convertISODateToJPylonDateWithoutTime(String isoDate) {
        String jpylonDate = isoDate; //default
        if (isoDate != null && isoDate.contains("-")) { //is it in iso format?
            SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat jpylonDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date;
            try {
                date = isoDateFormat.parse(isoDate);
            } catch (ParseException ex) {
                return isoDate;
            }
            jpylonDate = jpylonDateFormat.format(date);
        }
        return jpylonDate;
    }

    public static String convertJPylonDateToISODate(String jpylonDate) {
        String isoDate = jpylonDate; //default
        if (jpylonDate != null && jpylonDate.contains("/")) { //is it in jpylon format?
            SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat jpylonDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date;
            try {
                date = jpylonDateFormat.parse(jpylonDate);
            } catch (ParseException ex) {
                return jpylonDate;
            }
            isoDate = isoDateFormat.format(date);
        }
        return isoDate;
    }

/**
 * Builds a jPylon timestamp from a day (1-31), month (1-12), and year
 * (whatever).  The result is of the form mm/dd/yy HH:mm:ss.  The time is
 * set to 00:00:00.  If any of the day, month, or year values is -1
 * (indicating no value was selected in the dropdown), we convert it
 * to "" to force an error.
 * <br>
 * Recent update: checking length of the day / month strings.  If they're
 * 1 digit in length, then they're pre-pended with a '0' to make them 2
 * digits in length.  Some of the error checking that is called, requires
 * the data to be at least 2 digits in length. -RAS
 *
 * @return java.lang.String
 * @param day java.lang.String
 * @param month java.lang.String
 * @param year java.lang.String
 */
public static String buildJPylonDateTime(String day, String month, String year) {
	// Need to construct a jPylon timestamp.  This is of the form
	// mm/dd/yyyy HH:mm:ss.

	if (day == null || month == null || year==null)
	    return "";

	// If no value was chosen from the dropdown, the returned value
	// is -1.  Convert this into "" to force an error to occur.
	if (month.equals("-1") && day.equals("-1") && year.equals("-1")) {
		return "";
	}

	if (day.equals("-1")) day = "";
	if (month.equals("-1")) month = "";
	if (year.equals("-1")) year = "";

	if (day.length() == 1) day = "0" + day;         //added code for error
	if (month.length() == 1) month = "0" + month;   //checking reasons

	StringBuilder newDate = new StringBuilder(month);
	newDate.append("/");
	newDate.append(day);
	newDate.append("/");
	newDate.append(year);
	newDate.append(" 00:00:00");

	return newDate.toString();
}

/**
 * Converts a date in iso format (yyyy-MM-dd HH:mm:ss.S) into a formatted date
 * and time. The input date (assumed to be GMT time) is converted to the local
 * timezone and then formatted (see formatDateTime method) for display. The
 * result contains the date only.
 *
 * @return java.lang.String - the formatted result
 * @param ISODate java.lang.String - the date to format
 * @param formatType java.lang.String - SHORT or LONG
 * @param locale - locale to format for
 * @param timezone String - The timezone to convert for.  May be any valid
 *                          java.util.TimeZoneData key
 */
public static String convertGMTDateForTimezoneAndFormat(String ISODate, String formatType, String locale, String timezone)
 {
        if(ISODate.equals(""))
           return "";

	// First, convert ISO date to proper format to call getLocalDateTime
	SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	Date date = null;

	try
	 {
	       // Try to convert the ISO date as a String to a Java Date
	       date = isoDateFormatter.parse(ISODate);
	 }
	catch (ParseException e)
	 {
	        // Parse error - should never happen
	        e.printStackTrace();
	 }

	 // Now, convert that date to the proper format to call getLocalDateTime
	 SimpleDateFormat otherFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");

	 // Convert the date to the specified timezone
	 Date convertedDate = TPDateTimeUtility.getLocalDateTime(otherFormatter.format(date), timezone);


	 return formatDate(isoDateFormatter.format(convertedDate), formatType, locale);
 }

/**
 * Converts a date in iso format (yyyy-MM-dd HH:mm:ss.S) into a formatted date
 * and time. The input date (assumed to be GMT time) is converted to the local
 * timezone and then formatted (see formatDateTime method) for display.  The
 * result contains the date and time.
 *
 * @return java.lang.String - the formatted result
 * @param ISODate java.lang.String - the date to format
 * @param formatType java.lang.String - SHORT or LONG
 * @param locale - locale to format for
 * @param timezone String - The timezone to convert for.  May be any valid
 *                          java.util.TimeZoneData key
 */
 public static String convertGMTDateTimeForTimezoneAndFormat(String ISODate, String formatType, String locale,
															 String timeZone)
 {

	 

	 Date date = null;
     // First, convert ISO date to proper format to call getLocalDateTime
	 SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	 try
	{
	   // Try to convert the ISO date as a String to a Java Date
	   date = isoDateFormatter.parse(ISODate);
	}
	catch (ParseException e)
	{
	   // Parse error - should never happen
	   e.printStackTrace();
	}

	// Now, convert that date to the proper format to call getLocalDateTime
	 SimpleDateFormat otherFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");

	 // Convert the date to the specified timezone
	 Date convertedDate = TPDateTimeUtility.getLocalDateTime(otherFormatter.format(date), timeZone);

	 return formatDateTime(isoDateFormatter.format(convertedDate), formatType, locale);
 }

 /*
  * Converts given date in string in given format to Date
  */

 public static Date convertDateStringToDate(String sDate, String format){
	 SimpleDateFormat formatter = new SimpleDateFormat(format);
     formatter.setLenient(false); //SRUK01876919 Strict interpretation of date.  e.g. convertDateStringToDate("01/32/2000", "MM/dd/yyyy") = null since it is invalid.
     Date date=null;
	try {
		date = formatter.parse(sDate);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     return date;
 }

/**
 * Similar to the formatDate(String, String, String) method except
 * only LONG formats are supported and rather than formatting from an ISO
 * date string, it formats from a date.
 * <p>
 * @param Date               - This is the the date that needs to be formatted.
 * @param locale             - The locale to use for formatting (specifically for
 *                             the month name).
 * @return formattedDateTime - The string value of the formatted date..If the method
 *                             fails, then we return the date value that was passed in.
 */
public static String formatDate(Date myDate, String locale)
{
   String result = myDate.toString();  //if this method fails, return what was passed in.

   if( locale == null )
	   locale = "en_US";

   try{
		Locale myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", myLocale);
		result = formatter.format(myDate);

   }catch(Exception e) { }

   return result;
}

/**
 * Similar to the formatDate(String, String, String) method except
 * only LONG formats are supported and rather than formatting from an ISO
 * date string, it formats from a date.
 * <p>
 * @param Date               - This is the the date that needs to be formatted.
 * @param locale             - The locale to use for formatting (specifically for
 *                             the month name).
 * @return formattedDateTime - The string value of the formatted date..If the method
 *                             fails, then we return the date value that was passed in.
 */
public static String formatMessageDate(Date myDate, String locale)
{
   String result = myDate.toString();  //if this method fails, return what was passed in.

   if( locale == null )
	   locale = "en_US";
   try{

		Locale myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", myLocale);
		result = formatter.format(myDate);

   }catch(Exception e) { }

   return result;
}

/**
 * Formats a given ISO date (yyyy-MM-dd HH:mm:ss.S, the format returned
 * from the database) into a presentation format (dd MMM yyyy or
 * dd MMM yyyy) depending on the formatType.
 * <p>
 * A format type of SHORT prints months as abbrevations.  A format type of
 * LONG prints the full month name (e.g., Jan vs January)
 * <p>
 * @return java.lang.String - The formatted date (only)
 * @param ISODate java.lang.String - the date to format in ISO format
 * @param formatType java.lang.String - SHORT or LONG
 * @param locale - the locale to use for formatting the date (provides the
 *                 localized month names.)
 */
public static String formatDate(String ISODate, String formatType, String locale) {
	String result = ISODate;
	String pattern;
	if (formatType.equals(SHORT)) pattern = "dd MMM yyyy";
	else pattern = "dd MMMM yyyy";

	Locale myLocale;

	try {
		myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
	} catch (Exception e) {
		myLocale = Locale.US;
	}

	SimpleDateFormat sdf = new SimpleDateFormat(pattern, myLocale);
	SimpleDateFormat iso = null;
    if (result.length() == 10) {
		iso = new SimpleDateFormat("yyyy-MM-dd");
    }else {
    	iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    }
	try {
		Date date = iso.parse(ISODate);

		result = sdf.format(date);
	} catch (Exception e) {
	}

	return result;
}

/**
 * Formats a given ISO date (yyyy-MM-dd HH:mm:ss.S, the format returned
 * from the database) into a presentation format (dd MMM yyyy hh:mma or
 * dd MMM yyyy hh:mma) depending on the formatType.
 * <p>
 * A format type of SHORT prints months as abbrevations.  A format type of
 * LONG prints the full month name (e.g., Jan vs January)
 * <p>
 * @return java.lang.String - The formatted date and time
 * @param ISODate java.lang.String - the date to format in ISO format
 * @param formatType java.lang.String - SHORT or LONG
 * @param locale - the locale to use for formatting the date (provides the
 *                 localized month names.)
 */
public static String formatDateTime(String ISODate, String formatType, String locale)
{
	String             pattern  = null;
   String             result   = ISODate;

	if (formatType.equals(SHORT))
   {
	  pattern = "dd MMM yyyy hh:mm a";
   }
   else
   {
	  pattern = "dd MMMM yyyy hh:mm a";
   }

	Locale myLocale = null;
	try
   {
	  myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
   }
   catch (Exception e)
   {
	  myLocale = Locale.US;
   }

	SimpleDateFormat sdf = new SimpleDateFormat(pattern, myLocale);
	SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

	try
   {
	   Date date = iso.parse(ISODate);

	   result = sdf.format(date);
   }
   catch (Exception e)
   {

   }

   return result;
}

/**
 * Similar to the formatDateTime(String, String, String) method except
 * only LONG formats are supported and rather than formatting from an ISO
 * date string, it formats from a date.
 * <p>
 * @param Date               - This is the the date that needs to be formatted.
 * @param locale             - The locale to use for formatting (specifically for
 *                             the month name).
 * @return formattedDateTime - The string value of the formatted date..If the method
 *                             fails, then we return the date value that was passed in.
 */
public static String formatDateTime(Date myDate, String locale)
{

	String result = myDate.toString();

	if (locale == null)
   {
	  locale = "en_US";
   }

   try
   {

	   Locale myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
	   SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy hh:mma", myLocale);
	   result    = formatter.format(myDate);

   }
   catch (Exception e)
   {

   }

   return result;
}


/**
 * Given string representation of year, month, and day (month is 1-12),
 * build a date of the format dd-MMM-yyyy which can be used for querying
 * against Oracle date fields.   (MMM is a three letter English abbrev
 * for the month).
 * <p>
 * If any value is -1 (i.e., a blank option), no date is returned.
 * <p>
 * @return java.lang.String - the 'dd-MMM-yyyy' formatted date
 * @param year java.lang.String - the year
 * @param month java.lang.String - the month (1-12 or -1)
 * @param day java.lang.String - the day (1-31 or -1)
 */
public static String formatOracleDate(String year, String month, String day) {
	if ((year != null) && (month != null) && (day != null)) {
		if (year.equals("-1") || month.equals("-1") || day.equals("-1")) {
			return "";
		}
		return day + "-" + getEnglishMonthAbbrev(month) + "-" + year;
	} else {
		return "";
	}
}

/**
 * Returns an HTML dropdown containing day values (1-31) with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).
 *
 * @return java.lang.String - generated HTML
 * @param name String - name of the dropdown field
 * @param selection String - day to mark as selected
 * @param firstOptionText String - default option text
 */
public static String getDayDropDown(String name, String selection,
							   	    String firstOptionText) {

	return getDayDropDown(name, selection, firstOptionText, "");
}

/**
 * Returns an HTML dropdown containing day values (1-31) with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).
 *
 * @return java.lang.String - generated HTML
 * @param name String - name of the dropdown field
 * @param selection String - day to mark as selected
 * @param firstOptionText String - default option text
 * @param extraTags - This is any additional features that need to be
 *                    coded for this given dropdown box...ie: onChange
 *                    event definition(s).
 */
public static String getDayDropDown(String name, String selection,
							   	    String firstOptionText, String extraTags) {

	StringBuilder html = new StringBuilder("<select name=");
	html.append(name);
	html.append(" class=ListText");
	if ((extraTags != null) &&
	    (!extraTags.equals("")))
	{       html.append(" ");
	        html.append(extraTags);
	}
	html.append(">");

	if (firstOptionText != null) {
		html.append("<option value=-1>");
		html.append(firstOptionText);
		html.append("</option>");
	}

	int day = -1;
	if ((selection == null) || (selection.equals("")))
		day = -1;
	else
		day = Integer.parseInt(selection);

	for (int i = 1; i <= 31; i++) {
		html.append("<option");
		if (day == i) {
			html.append(" selected");
		}
		if (i < 10)
		{
		    html.append(" value=0" + i + ">" + i + "</option>");
		}
		else
		{
		    html.append(" value=" + i + ">" + i + "</option>");
		}
	}
	html.append("</select>");
	return html.toString();
}

/**
 * Returns number of days in Feb for given year.  Feb has 29 days in
 * years divisible by 4 except for centurial years which are not also
 * divisibile by 400
 *
 * @return int - 28 or 29 indicating the number of days in Feb.
 * @param year int - value representing the year
 */
private static int getDaysInFebruary(int year) {
	if ( (year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0) ) )
	   return 29;
	 else
	   return 28;
}

/**
 * This method is used for the date fields found in the document preparation XML file.
 * All of these dates are of the following two formats: 'YYYYMMDD' and 'YYMMDD'. The
 * formatType parameter passed in will indicate which one to use. This method calls an
 * overloaded getDocPrepFormattedDate method with 'null' passed as the date parameter,
 * which indicates the date doesn't already exist and should be generated from the
 * system.
 *
 * @param formatType java.lang.String - indicator of which year format to use when
 *                                      formatting the date (i.e., LONG  = 'YYYYMMDD'
 *                                                                 SHORT = 'YYMMDD')
 * @param timeZone java.lang.String - the user's time zone that should be used when
 *                                    formatting the date
 * @return java.lang.String - the formatted doc prep date
 *
 */
public static String getDocPrepFormattedDate(String formatType, String timeZone)
{
   return getDocPrepFormattedDate(null, formatType, timeZone);
}

/**
 * This method is used for the date fields found in the document preparation XML file.
 * All of these dates are of the following two formats: 'YYYYMMDD' and 'YYMMDD'. The
 * formatType parameter passed in will indicate which one to use. The method first
 * determines whether or not to format an existing date or a newly generated system
 * date. It then places the date into a Java Date object, adjusts the date according
 * to the user's time zone, and converts it using the appropriate date format.
 *
 * @param date - the date (in 'MM/dd/yyyy' format)to format according to doc prep
 *               format
 * @param formatType java.lang.String - indicator of which year format to use when
 *                                      formatting the date (i.e., LONG  = 'YYYYMMDD'
 *                                                                 SHORT = 'YYMMDD')
 * @param timeZone java.lang.String - the user's time zone that should be used when
 *                                    formatting the date
 * @return java.lang.String - the formatted doc prep date
 *
 */
public static String getDocPrepFormattedDate(String date, String formatType, String timeZone)
{

	

	Date convertedDate = null;
     // Set up the date formatter to parse the date
	SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	try
   {
	  // First, check to see whether a date was passed into the method to format; if this
	  // parameter is null, retrieve the current system date
	  if (date == null)
	   {
		  date = DateTimeUtility.getGMTDateTime();
	   }

	  // Try to convert the date as a String to a Java Date
	  convertedDate = dateFormatter.parse(date);
   }
   catch (Exception e)
   {
	  // Parse error - should never happen
	  e.printStackTrace();
   }

   // Adjust the date according to the user's time zone
	GregorianCalendar localCalendar = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
	localCalendar.setTime(convertedDate);
	int localOffset = localCalendar.get(Calendar.ZONE_OFFSET) + localCalendar.get(Calendar.DST_OFFSET);
	Date localDate = new Date(convertedDate.getTime() + localOffset);

	// Now, determine which year format to use for the date and format it accordingly
	SimpleDateFormat docPrepDateFormatter = null;
	if (formatType.equals(TPDateTimeUtility.SHORT))
   {
	  docPrepDateFormatter = new SimpleDateFormat("yyMMdd");
   }
   else
   {
	  docPrepDateFormatter = new SimpleDateFormat("yyyyMMdd");
   }

   return docPrepDateFormatter.format(localDate);
}

/**
 * This method is used for the time fields found in the document preparation XML file.
 * All of these dates are of the format 'HHmm'. This method calls an overloaded
 * getDocPrepFormattedTime method with 'null' passed as the date parameter,
 * which indicates the date (including time) doesn't already exist and should be
 * generated from the system.
 *
 * @param timeZone java.lang.String - the user's time zone that should be used when
 *                                    formatting the time
 * @return java.lang.String - the formatted doc prep time
 *
 */
public static String getDocPrepFormattedTime(String timeZone)
{
   return getDocPrepFormattedTime(null, timeZone);
}

/**
 * This method is used for the time fields found in the document preparation XML file.
 * All of these dates are of the format 'HHmm'. The method first determines whether or
 * not to format an existing time or a newly generated system time. It then places the
 * date into a Java Date object, adjusts the date and time according to the user's time
 * zone, and converts it using the 'HHmm' time format.
 *
 * @param date - the date (in 'MM/dd/yyyy' format)to format according to doc prep
 *               format
 * @param timeZone java.lang.String - the user's time zone that should be used when
 *                                    formatting the date
 * @return java.lang.String - the formatted doc prep date
 *
 */
public static String getDocPrepFormattedTime(String date, String timeZone)
{


	

	Date convertedDate = null;
    // Set up the date formatter to parse the date
	SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	try
   {

	   // First, check to see whether a date was passed into the method to format; if this
	   // parameter is null, retrieve the current system date
	   if (date == null)
	   {
		  date = DateTimeUtility.getGMTDateTime();
	   }

	  // Try to convert the date as a String to a Java Date
	  convertedDate = dateFormatter.parse(date);
   }
   catch (Exception e)
   {
	  // Parse error - should never happen
	  e.printStackTrace();
   }

   // Adjust the date according to the user's time zone
	GregorianCalendar localCalendar = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
	localCalendar.setTime(convertedDate);
	int localOffset = localCalendar.get(Calendar.ZONE_OFFSET) + localCalendar.get(Calendar.DST_OFFSET);
	Date localDate = new Date(convertedDate.getTime() + localOffset);

	// Now, set up the year format to use for the date and format it accordingly
	SimpleDateFormat docPrepDateFormatter = new SimpleDateFormat("HHmm");

	return docPrepDateFormatter.format(localDate);
}

/**
 * Returns a month abbreviation given a month number (1-12).  Abbrev
 * is the English version (used for construction Oracle date value).
 *
 * @return java.lang.String - the 3 letter English month abbrevation
 * @param monthNum java.lang.String - 1 based value representing the month
 */
private static String getEnglishMonthAbbrev(String monthNum) {
	  String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN",
						 "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
	  return months[Integer.parseInt(monthNum) -1];
}

/**
 * Converts a given GMT date (in MM/dd/yyyy HH:mm:ss format) into a
 * local date time based on the given timezone.
 *
 * @return Date - the converted date.
 * @param gmtDateTime java.util.Date - the GMT date to convert
 * @param localTimezone String - The timezone to convert for.  May be any valid
 *                               java.util.TimeZoneData key
 */
public static Date getLocalDateTime(String gmtDateTime, String localTimeZone) {

	Date gmtDate = null;

	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	try
	{
	    gmtDate = formatter.parse(gmtDateTime);
	}
	catch (Exception e)
	{
	    LOG.info(e.toString());
	}

	GregorianCalendar localCal = new GregorianCalendar(TimeZone.getTimeZone(localTimeZone));
	localCal.setTime(gmtDate);
	int localOffset = localCal.get(Calendar.ZONE_OFFSET) + localCal.get(Calendar.DST_OFFSET);

	// Add the offset to the gmt to get the local time
	Date localDate = new Date(gmtDate.getTime() + localOffset);

	return localDate;
}

   /**
    * This method is used to convert a date in European date format (i.e.,
    * 'dd/MM/yyyy') to jPylon date-time format (i.e., 'MM/dd/yyyy hh:mm:ss').
    * If the date passed in is not in the correct format or is an invalid
    * date, the unmodified date string will be returned. It is up to the class
    * that calls this method to issue validation errors. Currently, this
    * method is only called by the class POLineItemData.
    *

    * @param      java.lang.String europeanDate - the european date to convert
    * @return     java.lang.String - the converted PO line item date in jPylon format (i.e., 'MM/dd/yyyy hh:mm:ss')
    */
   public static String convertFromEuroToJPylonDateTimeFormat(String europeanDate)
   {

	   // First, determine whether the date has the same length as "dd/mm/yyyy" (i.e., 10 chars);
      // if it doesn't, return and validate later (validation will catch it)
      if (europeanDate.length() != NUMBER_OF_EURO_DATE_CHARS)
      {
         return europeanDate;
      }

      // Set up the date formatter to parse the date
	   SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	   try
      {
         // Try to convert the date as a String to a Java Date; don't worry about invalid
         // formats and values here---date validation will catch it later
		  Date date = dateFormatter.parse(europeanDate);
	  }
      catch (ParseException e)
      {
         return europeanDate;
      }

      // If the date was in the correct format, convert to a jPylon date (i.e., MM/dd/yyyy hh:mm:ss).
	   String month = europeanDate.substring(3, 5);
	   String year = europeanDate.substring(6);
	   String day = europeanDate.substring(0, 2);

	   StringBuilder convertedDate = new StringBuilder();
	   convertedDate.append(month);
      convertedDate.append("/");
      convertedDate.append(day);
      convertedDate.append("/");
      convertedDate.append(year);
      convertedDate.append(" 00:00:00");

      return convertedDate.toString();
   }

/**
 * Returns an HTML dropdown containing month text values with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).  The locale is used
 * to get the full month names for the given language.
 *
 * Note: value for each month is 1-12.
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - month to mark as selected
 * @param firstOptionText String - default option text
 */
public static String getMonthDropDown(String name, String selection,
							String locale, 	String firstOptionText) {

	return getMonthDropDown(name, selection, locale, firstOptionText, "");
}

/**
 * Returns an HTML dropdown containing month text values with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).  The locale is used
 * to get the full month names for the given language.
 *
 * Note: value for each month is 1-12.
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - month to mark as selected
 * @param firstOptionText String - default option text
 * @param extraTags - This is any additional features that need to be
 *                    coded for this given dropdown box...ie: onChange
 *                    event definition(s).
 */
public static String getMonthDropDown(String name, String selection, String locale,
									  String firstOptionText, String extraTags) {
	StringBuilder html = new StringBuilder("<select name=");
	html.append(name);
	html.append(" class=ListText");
	if ((extraTags != null) &&
	    (!extraTags.equals("")))
	{       html.append(" ");
	        html.append(extraTags);
	}
	html.append(">");

	if (firstOptionText != null) {
		html.append("<option value=-1>");
		html.append(firstOptionText);
		html.append("</option>");
	}

	String[] displayMonths = getMonthNames(locale);

	int month = -1;
	if ((selection == null) || (selection.equals("")))
		month = -1;
	else
		month = Integer.parseInt(selection);

	for (int i = 0; i < 12; i++) {
		html.append("<option");
		if (month == i + 1) {
			html.append(" selected");
		}

		if ((i+1) < 10)
		{
  		    html.append(" value=0" + (i + 1) + ">" + displayMonths[i] + "</option>");
  		}
		else
		{
  		    html.append(" value=" + (i + 1) + ">" + displayMonths[i] + "</option>");
  		}
	}
	html.append("</select>");
	return html.toString();

}

/**
 * For the given locale (in xx_xx format), returns an array of month names
 *
 * @return java.lang.String[] - internationalized list of month names
 * @param locale java.lang.String - the locale in xx_xx format (e.g., en_US)
 */
public static String[] getMonthNames(String locale) {

	Locale myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
	DateFormatSymbols symbols = new DateFormatSymbols(myLocale);
	return symbols.getMonths();
}

/**
 * Returns an HTML dropdown containing year values (from current year - 1 to
 * current year + 20) with a given value selected.  If firstOptionText is not
 * null, this becomes the default value in the first row (with value -1).
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - year to mark as selected
 * @param firstOptionText String - default option text
 */
public static String getYearDropDown(String name, String selection,
									String firstOptionText) {

	return getYearDropDown(name, selection, firstOptionText, -1, "");
}


/**
 * Returns an HTML dropdown containing year values (current year +
 * startYearOffset to current year + 20) with a given value selected.  If
 * firstOptionText is not null, this becomes the default value in the first row
 * (with value -1).
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - year to mark as selected
 * @param firstOptionText String - default option text
 * @param startYearOffset int - offset of the starting year relative to current
 * year. e.g.  -1
 */
public static String getYearDropDown(String name, String selection,
									String firstOptionText, int startYearOffset) {

	return getYearDropDown(name, selection, firstOptionText, startYearOffset, "");
}

/**
 * Returns an HTML dropdown containing year values (from current year - 1 to
 * current year + 20) with a given value selected.  If firstOptionText is not
 * null, this becomes the default value in the first row (with value -1).
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - year to mark as selected
 * @param firstOptionText String - default option text
 * @param extraTags - extra tags
 */
public static String getYearDropDown(String name, String selection,
									String firstOptionText,  String extraTags) {

	return getYearDropDown(name, selection, firstOptionText, -1, extraTags);
}


/**
 * Returns an HTML dropdown containing year values (from current year +
 * startYearOffset to current year + 20) with a given value selected.  If
 * firstOptionText is not null, this becomes the default value in the first row
 * (with value -1).
 *
 * @return java.lang.String - the generated HTML
 * @param name String - name of the dropdown field
 * @param selection String - the year to mark as selected
 * @param firstOptionText String - default option text
 * @param extraTags - This is any additional features that need to be
 *                    coded for this given dropdown box...e.g.: onChange
 *                    event definition(s).
 */
public static String getYearDropDown(String name, String selection,
									String firstOptionText, int startYearOffset, String extraTags) {

	StringBuilder html = new StringBuilder("<select name=");
	html.append(name);
	html.append(" class=ListText");
	if ((extraTags != null) &&
	    (!extraTags.equals("")))
	{       html.append(" ");
	        html.append(extraTags);
	}
	html.append(">");

	if (firstOptionText != null) {
		html.append("<option value=-1>");
		html.append(firstOptionText);
		html.append("</option>");
	}

	int year = -1;
	if ((selection == null) || (selection.equals("")))
		year = -1;
	else
		year = Integer.parseInt(selection);

    //cquinton 3/11/2011 Rel 7.0.0 ir#nbuk120941165 start
    //base current year off of override date if applicable
    Date today = null;
    try {
        today = GMTUtility.getGMTDateTime();
    } catch (AmsException e) {
        today = new Date(); //a backup plan :.)
    }
    Calendar todayCalendar = new GregorianCalendar();
    todayCalendar.setTime(today);

    int currentYear = todayCalendar.get(Calendar.YEAR);
    //cquinton 3/11/2011 Rel 7.0.0 ir#nbuk120941165 end
	int startYear = currentYear + startYearOffset;
	int endYear = currentYear + 20;
	for (int i = startYear; i <= endYear; i++) {
		html.append("<option");
		if (year == i) {
			html.append(" selected");
		}
		html.append(" value=" + i + ">" + i + "</option>");
	}
	html.append("</select>");
	return html.toString();
}

//NSX CR-507 01/06/10 Begin

/**
 * Returns an HTML dropdown containing week day text values with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).  The locale is used
 * to get the week day text for the given language.
 *
 * Note: value for week days is 1-7.
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - week day to mark as selected
 * @param firstOptionText String - default option text
 */
public static String getWeekDayDropDown(String name, String selection,
							String locale, 	String firstOptionText) {

	return getWeekDayDropDown(name, selection, locale, firstOptionText, "");
}

/**
 * Returns an HTML dropdown containing week day text values with a given
 * value selected.  If firstOptionText is not null, this becomes the
 * default value in the first row (with value -1).  The locale is used
 * to get the week day text for the given language.
 *
 * Note: value for week days is 1-7.
 *
 * @return java.lang.String
 * @param name String - name of the dropdown field
 * @param selection String - week day to mark as selected
 * @param firstOptionText String - default option text
 * @param extraTags - This is any additional features that need to be
 *                    coded for this given dropdown box...ie: onChange
 *                    event definition(s).
 */
public static String getWeekDayDropDown(String name, String selection, String locale,
									  String firstOptionText, String extraTags) {
	StringBuilder html = new StringBuilder("<select name=");
	html.append(name);
	html.append(" class=ListText");
	if ((extraTags != null) &&
	    (!extraTags.equals("")))
	{       html.append(" ");
	        html.append(extraTags);
	}
	html.append(">");

	if (firstOptionText != null) {
		html.append("<option value=-1>");
		html.append(firstOptionText);
		html.append("</option>");
	}

	String[] days = getWeekDayNames(locale);

	int day = -1;
	if (StringFunction.isNotBlank(selection)) {
		day = Integer.parseInt(selection);
	}

	for (int i = 1; i < days.length; i++) {
		html.append("<option");
		if (day == i) {
			html.append(" selected");
		}
	    html.append(" value=" + (i) + ">" + days[i] + "</option>");
	}
	html.append("</select>");
	return html.toString();

}

/**
 * For the given locale (in xx_xx format), returns an array of week days
 *
 * @return java.lang.String[] - internationalized list of week days
 * @param locale java.lang.String - the locale in xx_xx format (e.g., en_US)
 */
private static String[] getWeekDayNames(String locale) {

	Locale myLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
	DateFormatSymbols symbols = new DateFormatSymbols(myLocale);
	return symbols.getWeekdays();
}


/**
 * For the given locale (in xx_xx format) and week day index, returns week day name
 *
 * @return java.lang.String - internationalized week day name
 * @param locale java.lang.String - the locale in xx_xx format (e.g., en_US)
 * @param locale java.lang.String - the index of week day
 */
public static String getWeekDayName(String locale, String dayIdx) {

 	int idx = Integer.parseInt(dayIdx);
 	if (!(idx >= 1 && idx <= 7)) return "";

	String days[] = getWeekDayNames(locale);

	return days[idx];
}

// NSX CR-507 01/06/10 End

/**
 * Returns true if year, month, and day make a valid date; false otherwise.
 * If year, month, and day are ALL -1, this is a valid "empty" date.
 * Likewise, all null values is a valid "empty" date.  The parms are
 * assumed to be numbers (1-31 for day; 1-12 for month).  This method
 * was written primarily for validation of date dropdowns.
 *
 * @return boolean - true: is valid or empty date; false: invalid date
 * @param year java.lang.String - value representing the year.  Note that
 *                '99' is not equivalent to '1999' but rather the year 99.
 * @param month java.lang.String - value representing the month, 1-12 or -1
 * @param day java.lang.String - value representing the day, 1-31 or -1
 */
public static boolean isGoodDate(String year, String month, String day) {
	if (year == null && month == null && day == null)
		return true;

	int[] daysInMonth = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int yr = -1;
	int mo = -1;
	int dy = -1;

	try {
		yr = Integer.parseInt(year);
		mo = Integer.parseInt(month);
		dy = Integer.parseInt(day);
	} catch (NumberFormatException e) {
		return false;
	}

	if (yr == -1 && mo == -1 && dy == -1) {
		return true;
	} else
		if (yr == -1 || mo == -1 || dy == -1) {
			return false;
		}

	/* GGAYLE - IR GYUD091744322 - 09/22/03 */
	if (mo > 12)
		return false;
	/* GGAYLE - IR GYUD091744322 - 09/22/03 */

	if (dy > daysInMonth[mo])
		return false;

	if ((mo == 2) && (dy > getDaysInFebruary(yr)))
		return false;

	return true;
}

/**
 * Parses a day from a date in the format yyyy-mm-dd
 *
 * @param date java.lang.String - date in the format yyyy-mm-dd
 * @return String - the day portion of the date, "" if the day
 *                  cannot be obtained.
 */
public static String parseDayFromDate(String date) {
	if (date == null) return "";

	if (date.length() > 9) {
		return date.substring(8,10);
	} else {
		return "";
	}

}

/**
 * Parses a month from a date in the format yyyy-mm-dd
 *
 * @param date java.lang.String - date in the format yyyy-mm-dd
 * @return String - the month portion of the date, "" if the month
 *                  cannot be obtained.
 */
public static String parseMonthFromDate(String date) {
	if (date == null) return "";

	if (date.length() > 9) {
		return date.substring(5,7);
	} else {
		return "";
	}

}

/**
 * Parses a year from a date in the format yyyy-mm-dd
 *
 * @param date java.lang.String - date in the format yyyy-mm-dd
 * @return String - the year portion of the date, "" if the year
 *                  cannot be obtained.
 */
public static String parseYearFromDate(String date) {
	if (date == null) return "";

	if (date.length() > 9) {
		return date.substring(0,4);
	} else {
		return "";
	}

}


/**
 * This method gets the start of the local day in GMT time.
 * <p>
 * Using the timezone, the local date/time is obtained (as well as the
 * offset from GMT (base on timezone and daylight savings time info).
 * We construct a new day based on the local date (but not the time) to
 * get the start of the user's local day (i.e., midnight).  Subtracting
 * the offset from midnight results in the start of the current local
 * day in GMT time.
 * <p>
 * For example, suppose that we have a user in Adelaide, Australia
 * and the current time there is 11:30 AM on June 20, 2002.  Adelaide is
 * 9 and 1/2 hours (yes!) ahead of GMT.  This means it is currently 2:00 am
 * on June 20, 2002 in London.  We want to get the start of the day for
 * the Adelaide user in GMT.  The start of the date for the user is
 * just after midnight on June 20, 2002.  Therefore, the equivalent
 * date/time in GMT for the user's "start of day" is 9.5 hours earlier
 * or 2:30 pm on June 19, 2002.
 * <p>
 * @return Date - the date and time representing the start of the local
 *                day in GMT.
 * @param timeZone java.lang.String - timezone to use for determining local time
 */
public static Date getStartOfLocalDayInGMT(String timeZone) {

	// Find out what time it is for the user.  We need current GMT time
	// and the user's timezone to do this.
	Date localDateTime;
	try {
		localDateTime = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(), timeZone);
	} catch (AmsException e) {
		localDateTime = new Date();
	}

   	// Create a calendar with the user's local date/time.  This allows us to
   	// get the offset for the zone and DST.
	GregorianCalendar localCal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));

	localCal.setTime(localDateTime);
	int localOffset = localCal.get(Calendar.ZONE_OFFSET) + localCal.get(Calendar.DST_OFFSET);


	// Now create a calendar using just the month, day, year.  This results
	// in a calendar for the start of the local day (i.e., just past midnight)
	GregorianCalendar startOfDayCal = new GregorianCalendar(localCal.get(Calendar.YEAR),
	    localCal.get(Calendar.MONTH), localCal.get(Calendar.DATE));

	// Finally, using the start of day, subtract the offset to get the
	// start of day time in GMT.  (
	long gmtStartOfLocalDay = startOfDayCal.getTime().getTime() - localOffset;

	Date gmtDate = new Date(gmtStartOfLocalDay);
	LOG.debug("gmt start is " + gmtDate);

	return gmtDate;
}

/**
 * This method returns the start of the local day in GMT time for
 * the passed in localDateTime.
 * <p>
 * Using the timezone, the local date/time is obtained (as well as the
 * offset from GMT (base on timezone and daylight savings time info).
 * We construct a new day based on the local date (but not the time) to
 * get the start of the user's local day (i.e., midnight).  Subtracting
 * the offset from midnight results in the start of the current local
 * day in GMT time.
 * <p>
 * @param localDateTimeString java.lang.String - a local date/time in standard string format
 * @param timeZone java.lang.String - timezone to use for determining local time
 * @return Date - the date and time representing the start of the local
 *                day in GMT.
 */
public static Date getStartOfLocalDayInGMT(String localDateTimeString, String timeZone)
        throws AmsException {
	int year = -1;

	//stuff the date components in a calendar with the local timezone
    //assumes localDateTimeString is format "MM/dd/yyyy ...."

    try {
        year = Integer.parseInt(localDateTimeString.substring(6, 10));
    } catch (Exception ex) {
        throw new AmsException("Problem getting start of local day in gmt: year not valid");
    }
	int month = -1;
	try {
        month = Integer.parseInt(localDateTimeString.substring(0,2));
    } catch (Exception ex) {
        throw new AmsException("Problem getting start of local day in gmt: month not valid");
    }
	int day = -1;
	try {
        day = Integer.parseInt(localDateTimeString.substring(3,5));
    } catch (Exception ex) {
        throw new AmsException("Problem getting start of local day in gmt: day not valid");
    }

    GregorianCalendar startOfDayCal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
    startOfDayCal.set(Calendar.YEAR,year);
    startOfDayCal.set(Calendar.MONTH,month-1); //MONTH is zero based
    startOfDayCal.set(Calendar.DAY_OF_MONTH,day); //DAY_OF_MONTH is 1 based
    startOfDayCal.set(Calendar.HOUR_OF_DAY,0);
    startOfDayCal.set(Calendar.MINUTE,0);
    startOfDayCal.set(Calendar.SECOND,0);

	Date gmtDate = startOfDayCal.getTime();
	LOG.debug("gmt start is " + gmtDate);

    return gmtDate;
}

/**
 * This method compares two dates received as DateTime objects in the specified timezone
 * <p>
 * Both DateTimes are converted to the date in the timezone.  1 is returned if first date is
 * newer in the tiomezone. 2 is returned is second date is newer in the timezone.
 * 0 is returned if dates are the same.
 * <p>
 * For example, if datetimes received are:
 *  1st date: Tue Jun 16 00:59:42 UTC 2009
 *  2nd date: Mon Jun 15 15:55:44 UTC 2009
 * Then, if TimeZone specified as UTC, this method returns 1 (Jun 16th > June 15th).
 *       if TimeZone specified as "Australia/Sydney", then as Australia is GMT+10:
 *       the 1st date is Tue June 16 10:59:42 AST 2009 and
 *       the 2nd date is Tue June 16 01:55:44 AST 2009 and method returns 0 (June 16th = June 16th).
 * <p>
 * @return int - indicates which date is newer
 * @param Date1 java.util.Date - first DateTime to compare
 * @param Date2 java.util.Date - second DateTime to compare
 * @param timeZone java.lang.String - timezone to use for determining local time.
 */

public static int compareDatesInGivenTimeZone(java.util.Date Date1, java.util.Date Date2, String aTimeZone) {
	try {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd 00:00:00");
		if ((aTimeZone != null) && (!aTimeZone.equals("")))
			formatter.setTimeZone(java.util.TimeZone.getTimeZone(aTimeZone));
		LOG.debug("Date1 " + formatter.format(Date1));
		LOG.debug("Date2 " + formatter.format(Date2));
		if ((formatter.format(Date1)).compareTo(formatter.format(Date2)) > 0)
			return 1;
		if ((formatter.format(Date1)).compareTo(formatter.format(Date2)) < 0)
			return 2;
		return 0;
	}
	catch (Exception e_any) {
		LOG.info("compareDatesInGivenTimeZone::Exception comparing Dates" + e_any);
		return -1;
	}

}

/**
 * Given a start and end jpylon datetime, calculate the duration in seconds.
 * @param dateStart
 * @param dateEnd
 * @return Integer value if duration can be calculated, null if there is a problem
 * @throws IllegalArgumentException if startDateTime or endDateTime is missing or invalid
 */
public static long calculateDurationSeconds(String startDateTime, String endDateTime) {
	//short-circuit if start or end not present
	if ( startDateTime == null || startDateTime.length()<=0 ) {
		throw new IllegalArgumentException("Cannot calculate duration: startDateTime missing");
	}
	if ( endDateTime == null || endDateTime.length()<=0 ) {
		throw new IllegalArgumentException("Cannot calculate duration: endDateTime missing");
	}
	//convert string to date types
    Date start = null;
	try {
		start = DateTimeUtility.convertStringDateTimeToDate(startDateTime);
	} catch (Exception ex) {
		throw new IllegalArgumentException("Cannot calculate duration: startDateTime invalid: " + startDateTime);
	}
	Date end = null;
	try {
		end = DateTimeUtility.convertStringDateTimeToDate(endDateTime);
	} catch (Exception ex) {
		throw new IllegalArgumentException("Cannot calculate duration: endDateTime invalid: " + endDateTime);
	}

	long startMillis = start.getTime();
	long endMillis = end.getTime();
	long durationMillis = endMillis - startMillis;
	long durationSeconds = durationMillis / 1000;

	return durationSeconds;
}

/**
 * This method determines whether execution date is a future date
 * @param Sting dateStr future date to compare
 * @return boolean true: is a future date payment
 * @return boolean false: is not a future date payment
 */
public static boolean isFutureDate(String dateStr)
{
 return isFutureDate(dateStr,null);
}
/**
 * This method determines whether execution date is a future date
 * @param Sting dateStr future date to compare
 * @return boolean true: is a future date payment
 * @return boolean false: is not a future date payment
 */
public static boolean isFutureDate(String dateStr, String timeZone)
{
	if (dateStr == null)
		throw new IllegalArgumentException("Cannot determine if future date: future date is null");

	boolean returnValue = false;

	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	try
	{
		java.util.Date todaysDate = null;

		if (StringFunction.isNotBlank(timeZone))
		{
			todaysDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(),timeZone);
		}
		else
		{
			todaysDate = GMTUtility.getGMTDateTime();
		}
		Date execDate = df.parse(dateStr);
		LOG.debug("isFutureDate, future date compare:: future date: " + dateStr + " vs. today's date: " + todaysDate + "Result is: " + execDate.compareTo(todaysDate));

		if (execDate.compareTo(todaysDate) > 0)
			returnValue = true;
		else
			returnValue = false;
	}
	catch(Exception any_exc)
	{
		throw new IllegalArgumentException("Cannot determine if future date: future date is " + dateStr);
	}

	return returnValue;
}

/**
 * This method determines whether given date & timeZone is back dated
 * @param Date        date: date to compare
 * @return String timeZone: time zone
 * @return boolean
 */
public static boolean isBackDated(Date date, String timeZone) throws AmsException
{

	java.util.Date todaysDate = null;

	if (StringFunction.isNotBlank(timeZone))
	{
		todaysDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(),timeZone);
	}
	else
	{
		todaysDate = GMTUtility.getGMTDateTime();
	}

	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd");
	String curDateStr = formatter.format(date);
	String todaysDateStr = formatter.format(todaysDate);

	boolean backDated = false;
	if (curDateStr.compareTo(todaysDateStr) < 0)
	{
		backDated = true;
	}

	return backDated;

}
/**
 * This method gets Value Date for Payment/DDI tarnsaction.
 * It obtains necessary records and invokes a method on TPCalendarYear to obtain correct next business day
 * providing execute date, number of offset days, weekend pattern.
 * TPCalendarYear object maintain holiday calendar for given year.      .
 * @param inputDoc com.amsinc.ecsg.util - the input/output doc
 * @param valueDateOffset - number of offset days as defined in Payment Method record.
 * @return String -- the value is passed back in inputDoc.
 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
 */
    public static String getCalendarBasedNextBusinessDate(String currenyCode, String inputDate, int valueDateOffset, MediatorServices mediatorServices)
            throws RemoteException, AmsException {

        String returnDate = null;
        //Get Currency Calendar Rule, TP Calendar and TP Calendar Year records to obtain nececssary input
        String sqlSelect = "Select CC.A_TP_CALENDAR_OID, TC.WEEKEND_DAY_1, TC.WEEKEND_DAY_2 from CURRENCY_CALENDAR_RULE CC, TP_CALENDAR TC"
                + " where CC.CURRENCY = ? "
                + " and CC.A_TP_CALENDAR_OID = TC.TP_CALENDAR_OID";

        DocumentHandler calendarCur = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[]{currenyCode});
        if (calendarCur == null) {
            mediatorServices.getErrorManager().issueError("",
                    TradePortalConstants.CURRENCY_CALENDAR_NOT_DEFINED,
                    currenyCode);
            return returnDate;
        }
        String tpCalOid = calendarCur.getAttribute("/ResultSetRecord(0)/A_TP_CALENDAR_OID");
        if (StringFunction.isBlank(tpCalOid)) {
            LOG.info("Unexpected Error: Can't retrive Calendar Record OID form the Found Record");
            mediatorServices.getErrorManager().issueError("",
                    TradePortalConstants.CURRENCY_CALENDAR_NOT_DEFINED,
                    currenyCode);
            return returnDate;
        }

        LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::A_TP_CALENDAR_OID " + tpCalOid);

        int intYear;
        try {
            intYear = Integer.parseInt(inputDate.substring(6, 10));
        } catch (Exception any_exc) {
            LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::Execute Date is not entered/ is in invalid format. Value Date will be set to none");
            return returnDate;
        }

        sqlSelect = "select TP_CALENDAR_YEAR_OID FROM TP_CALENDAR_YEAR WHERE P_TP_CALENDAR_OID = ?  and year = ? ";

        DocumentHandler calendarYear = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, new Object[]{tpCalOid, intYear});
        if (calendarYear == null) {
            mediatorServices.getErrorManager().issueError("",
                    TradePortalConstants.CALENDAR_NOT_DEFINED_FOR_YEAR,
                    currenyCode,
                    inputDate.substring(6, 10));
            return returnDate;
        }
        String calendarYearOid = calendarYear.getAttribute("/ResultSetRecord(0)/TP_CALENDAR_YEAR_OID");
        if (StringFunction.isBlank(calendarYearOid)) {
            LOG.info("Unexpected Error: Can't retrive Calendar Year Oid form the Found Record");
            mediatorServices.getErrorManager().issueError("",
                    TradePortalConstants.CALENDAR_NOT_DEFINED_FOR_YEAR,
                    currenyCode,
                    inputDate.substring(6, 10));
            return returnDate;
        }

        //Obtain weekend days pattern, if defined
        int weekend1 = 0;

        String strWeekend1 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_1");
        if (StringFunction.isNotBlank(strWeekend1)) {
            weekend1 = Integer.parseInt(strWeekend1);
        }
        String strWeekend2 = calendarCur.getAttribute("/ResultSetRecord(0)/WEEKEND_DAY_2");
        int weekend2 = 0;
        if (StringFunction.isNotBlank(strWeekend2)) {
            weekend2 = Integer.parseInt(strWeekend2);
        }

        LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::weekends " + weekend1 + " " + weekend2);

		//Create TP Calendar Year Object for given year/currency.
        //Invoke method to obtain next business day provided execution date, number of offset days, and weekend dats pattern.
        //Shilpa R CR710 Rel 8.0 start
        String serverLocation = JPylonProperties.getInstance().getString(
                "serverLocation");
        TPCalendarYear tpCalendarYear = (TPCalendarYear) EJBObjectFactory
                .createClientEJB(serverLocation, "TPCalendarYear");
		//TPCalendarYear tpCalendarYear = (TPCalendarYear) mediatorServices.createServerEJB("TPCalendarYear");
        //Shilpa R CR710 Rel 8.0 end
        tpCalendarYear.getData(Long.parseLong(calendarYearOid));
        String valueDate = tpCalendarYear.getNextBusinessDay(inputDate,
                valueDateOffset, weekend1, weekend2);
        LOG.debug("TPDateTimeUtility::getCalendarBasedNextBusinessDate::this is value date " + valueDate);
        returnDate = valueDate;

        return returnDate;
    }

   public static String getDateFromDay (int day, int year, String locale) {
	   int[] spec = {31,getDaysInFebruary(year),31,30,31,30,31,31,30,31,30,31};
	   int balance = day;
	   int month =0;
	   for (month =0; month <12; month ++) {
		   if (balance > spec[month]){
			   balance = balance - spec[month];
		   } else {
			   break;
		   }
	   }

	   Date givenDate = new Date (year-1900, month, balance);

	   LOG.info("Given date ="+ givenDate.toGMTString());

	   String date = formatDate(givenDate, null);
	   LOG.info(date);
	   return date;
   }

   public static String getYearOptions(String selection,String firstOptionText,int startYearOffset){

	   StringBuilder html = new StringBuilder();

		if (firstOptionText != null) {
			html.append("<option value=-1>");
			html.append(firstOptionText);
			html.append("</option>");
		}

		int year = -1;
		if ((selection == null) || (selection.equals("")))
			year = -1;
		else
			year = Integer.parseInt(selection);

	    //cquinton 3/11/2011 Rel 7.0.0 ir#nbuk120941165 start
	    //base current year off of override date if applicable
	    Date today = null;
	    try {
	        today = GMTUtility.getGMTDateTime();
	    } catch (AmsException e) {
	        today = new Date(); //a backup plan :.)
	    }
	    Calendar todayCalendar = new GregorianCalendar();
	    todayCalendar.setTime(today);

	    int currentYear = todayCalendar.get(Calendar.YEAR);
	    //cquinton 3/11/2011 Rel 7.0.0 ir#nbuk120941165 end
		int startYear = currentYear + startYearOffset;
		int endYear = currentYear + 20;
		for (int i = startYear; i <= endYear; i++) {
			html.append("<option");
			if (year == i) {
				html.append(" selected");
			}
			html.append(" value=" + i + ">" + i + "</option>");
		}

		return html.toString();


   }

	// Shilpa R - CR 710 Rel 8.0 start
	public static String formatToDBDate(String value, String format)
			throws InvalidAttributeValueException {
		String month = null;
		String day = null;
		String year = null;
		if ("M/D/YY".equalsIgnoreCase(format)
				|| "M-D-YY".equalsIgnoreCase(format)) {
			month = value.substring(0, 1);
			day = value.substring(2, 3);
			year = value.substring(4, 6);
		} else if ("M/D/YYYY".equalsIgnoreCase(format)
				|| "M-D-YYYY".equalsIgnoreCase(format)) {
			month = value.substring(0, 1);
			day = value.substring(2, 3);
			year = value.substring(4, 8);
		} else if ("MM/DD/YY".equalsIgnoreCase(format)
				|| "MM-DD-YY".equalsIgnoreCase(format)) {
			month = value.substring(0, 2);
			day = value.substring(3, 5);
			year = value.substring(6, 8);
		} else if ("MM/DD/YYYY".equalsIgnoreCase(format)
				|| "MM-DD-YYYY".equalsIgnoreCase(format)) {
			month = value.substring(0, 2);
			day = value.substring(3, 5);
			year = value.substring(6, 10);
		} else if ("YY/MM/DD".equalsIgnoreCase(format)
				|| "YY-MM-DD".equalsIgnoreCase(format)) {
			month = value.substring(3, 5);
			day = value.substring(6, 8);
			year = value.substring(0, 2);
		} else if ("YYYY/MM/DD".equalsIgnoreCase(format)
				|| "YYYY-MM-DD".equalsIgnoreCase(format)) {
			month = value.substring(5, 7);
			day = value.substring(2, 3);
			year = value.substring(0, 4);
		} else if ("DD/MM/YY".equalsIgnoreCase(format)
				|| "DD-MM-YY".equalsIgnoreCase(format)) {
			month = value.substring(3, 5);
			day = value.substring(0, 2);
			year = value.substring(6, 8);
		} else if ("DD/MM/YYYY".equalsIgnoreCase(format)
				|| "DD-MM-YYYY".equalsIgnoreCase(format)) {
			month = value.substring(3, 5);
			day = value.substring(0, 2);
			year = value.substring(6, 10);
		}

		return getValidFormat(year, month, day);
	}

	public static String getValidFormat(String year, String month, String day)
			throws InvalidAttributeValueException {
		if (day == null || month == null || year == null)
			return "";

		// If no value was chosen from the dropdown, the returned value
		// is -1. Convert this into "" to force an error to occur.
		if (month.equals("-1") && day.equals("-1") && year.equals("-1")) {
			return "";
		}

		if (day.equals("-1"))
			day = "";
		if (month.equals("-1"))
			month = "";
		if (year.equals("-1"))
			year = "";

		if (day.length() == 1)
			day = "0" + day; // added code for error
		if (month.length() == 1)
			month = "0" + month; // checking reasons

		StringBuilder newDate = new StringBuilder(month);
		newDate.append("/");
		newDate.append(day);
		newDate.append("/");
		newDate.append(year);

		DateFormat informat = new SimpleDateFormat("MM/dd/yy");
		DateFormat outformat = new SimpleDateFormat("MM/dd/yyyy");

		String date = "";

		try {
			if (year.length() == 2) {
				date = outformat.format(informat.parse(newDate.toString()));
			} else {
				// Parse the value and then format it again.
				Date parsedDate = outformat.parse(newDate.toString());
				date = outformat.format(parsedDate).trim();
			}
		} catch (Exception e) {
			throw new InvalidAttributeValueException(
					AmsConstants.INVALID_DATE_ATTRIBUTE);
		}
		return date;
	}

	//for packaging invoice file dates convert MM/dd/yyyy to yyyy-MM-dd format

	public static String packagingInvoiceDate(String value){
		String month = value.substring(0, 2);
		String	day = value.substring(3, 5);
		String year = value.substring(6, 10);

		StringBuilder newDate = new StringBuilder(year);
		newDate.append("-");
		newDate.append(month);
		newDate.append("-");
		newDate.append(day);
		return newDate.toString();
	}


    /**
     * This method converts the passed in date to ISO date format
     *
     * @param value
     * @param format
     * @return
     */
    public static String convertToISODate(String value, String format) {
        if (StringFunction.isBlank(value)) {
            return value;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(format);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String formattedDate = null;
        try {
            Date inDate = inputFormat.parse(value);
            formattedDate = outputFormat.format(inDate);
        } catch (ParseException e) {
            formattedDate = value;
        }

        return formattedDate;
    }
    public static String convertToDBDate(String value, String format){
        if (StringFunction.isBlank(value)){
            return value;
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(format);
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy");
        String formattedDate = null;
        try {
			Date inDate = inputFormat.parse(value);
			formattedDate = outputFormat.format(inDate);
        } catch (ParseException e) {
            formattedDate = value;
        }

        return formattedDate;
    }
    
    //IR T36000017596 start- convert from yyyy-MM-dd HH:mm:ss.S to MM/dd/yyyy HH:mm:ss
    public static String convertDBISODateToJPylonDate(String isoDate) {
        String jpylonDate = isoDate; //default
        if ( isoDate != null && isoDate.contains("-") ) { //is it in iso format?
            SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            SimpleDateFormat jpylonDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date date;
            try {
                date = isoDateFormat.parse(isoDate);
            }
            catch (ParseException ex) {
                return isoDate;
            }
            jpylonDate = jpylonDateFormat.format(date);
        }
        return jpylonDate;
    }
  //IR T36000017596 end
    
    //IR 22691 Start- Check if both dates are same without time 
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            return false;
        }
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH));
    }
    //IR 22691 End

    public static String getXMLFormattedDate(String date, String formatType) {

        // Set up the date formatter to parse the date
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

        Date convertedDate = null;
        try {
    	  // First, check to see whether a date was passed into the method to format; if this
            // parameter is null, retrieve the current system date
            if (date == null) {
                date = DateTimeUtility.getGMTDateTime();
            }

            // Try to convert the date as a String to a Java Date
            convertedDate = dateFormatter.parse(date);
        } catch (Exception e) {
            // Parse error - should never happen
            e.printStackTrace();
        }

        // Now, determine which year format to use for the date and format it accordingly
        SimpleDateFormat docPrepDateFormatter = null;
        if (formatType.equals(TPDateTimeUtility.SHORT)) {
            docPrepDateFormatter = new SimpleDateFormat("yyMMdd");
        } else {
            docPrepDateFormatter = new SimpleDateFormat("yyyyMMdd");
        }

        return docPrepDateFormatter.format(convertedDate);
    }

    
  //MEer Rel 9.3  Added new method to validate DATE is in given date format.
  	 public static boolean isValidDateFormat(String date, String dateFormat) {
  		 boolean isValid = false;
  		 try {
  			 SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
  			 sdf.setLenient(false);
  			 sdf.parse(date);
  			 isValid = true;
  		 }
  		 catch (Exception e) {
  			 e.printStackTrace();
  		 }
  		 return isValid;
  	 }


}