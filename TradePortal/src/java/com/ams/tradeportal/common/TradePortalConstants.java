package com.ams.tradeportal.common;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Global constants for the Trade Portal
 *
 *     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TradePortalConstants
{
private static final Logger LOG = LoggerFactory.getLogger(TradePortalConstants.class);
	// Error Messages (in alpha order)
	public final static String AUTO_UPLOAD_INSTR_TYPE_MISSING = "E275";
	public final static String AUTO_ATP_PO_UPLOAD             = "ATP";
	public final static String AUTO_LC_PO_UPLOAD              = "IMP_DLC";
	public final static String AUTO_LC_PO_UPLOAD_TPS          = "DLC"; //BSL IR RSUM041136929 04/11/2012 ADD

//	public final static String INVOICE_APPROVED    = "IVAP";
//	public final static String INVOICE_REFUSED    = "IVRF";

	public final static String ACCOUNTS_NOT_ENTERED           = "W173";
	public final static String ACTIVE_BOGS                    = "E014";
	public final static String ACTIVE_CORP_CUSTOMERS          = "E017";
	public final static String ACTIVE_LC_CREATION_RULE        = "E020";
	public final static String ACTIVE_OP_BANK_ORGS            = "E018";
	public final static String ACTIVE_USERS                   = "E016";
	public final static String ADD_PO_LINE_ITEMS_SUCCESS      = "I208";
	public final static String ADMIN_USER_CANNOT_AUTHORIZE    = "E079";
	public final static String AIR_WAYBILL_NUMBER_MISSING     = "W010a";
//	public final static String ALREADY_AUTHORIZED             = "E077";
	public final static String ALREADY_EXISTS                 = "E002";
	public final static String ALREADY_LOGGED_IN              = "E128";
//	public final static String AMOUNT_DETAILS_REQUIRED        = "E243";
	public final static String AMOUNT_IS_ZERO                 = "E286";
	public final static String ASCT_MESSAGE_PROCESSED         = "I026";
	public final static String AUTHORIZED_USERS_NOT_SELECTED  = "E101";
	public final static String AUTOLC_PROCESS_RUNNING         = "E152";
	public final static String AUTOLC_PROCESS_UPLOADING       = "I011";
	public final static String BANK_BRANCH_RULE_ALREADY_EXISTS= "E508";
	public final static String BILL_OF_LADING_NUMBER_MISSING  = "W010b";
	public final static String CANT_DELETE_PO_UPLD_DEFN       = "E147";
	public final static String CANT_CREATE_EXPRESS_TEMPLATE   = "E168";
	public final static String CANT_CREATE_NON_PMT_FIXD_TMLTE = "E433";			//IAZ CR-586 08/16/10
	public final static String CANT_SELECT_FIXED_AND_EXPRS    = "E432";			//IAZ CR-586 08/18/10
	public final static String CANT_PROCESS_CONFID_PMTS       = "E431";			//IAZ CR-586 08/18/10
	public final static String CARRIER_NAME_MISSING           = "W010c";
	public final static String CERTIFICATE_NOT_VALID          = "E133";
	public final static String CERT_REQD_LOAN_AND_FUND        = "E203";
	//cr498 change W174 to an error so it displays
	//public final static String CHALLENGE_PASSWORD_2FA         = "W174";
	public final static String CHALLENGE_PASSWORD_2FA         = "E539";  //cr498
	public final static String CITY_PROVINCE_MORE_THAN_31     = "E519";
	public final static String COMM_CHRG_INFO_MISSING         = "E096a";
	public final static String COMMIT_TO_IMAGE_DB_FAILED      = "E256";
	public final static String COMPLETE_ALL_INFO              = "E141";
	public final static String CONTACT_BANK_PMT_INSTR         = "E086";
	public final static String CONTAINER_NUMBER_MISSING       = "W010d";
	public final static String CORP_SERVICES_URL_NOT_ESTD     = "E293";
	public final static String CORP_SERVICES_URL_MSG          = "W017";
	public final static String CORP_SERVICES_URL_MSG1         = "W018";
//	public final static String CURR_OF_ACCOUNT_MISSING        = "E212";
	public final static String CURRENTLY_ASSIGNED             = "E005";
	public final static String CURRENT_PASSWORD_INVALID       = "E127";
	public final static String CUST_TEXT_OR_BANK_WORDING      = "E097";
	public final static String DAILY_LIMIT_EXCEEDED           = "E024";

	//cquinton 12/6/2012 the following generic error is used for those limited
	// cases where data in the system is out of synch and processing cannot continue,
	// but the user can't do anything about it. the specific error message
	// should include more detailed info about the problem so a support person
	// can resolve if necessary
//	public final static String DATA_ERROR                     = "E003";

	public final static String DECIMAL_PLACE                  = "E188";
	public final static String DECIMAL_PLACE_LEFT             = "E009";
	public final static String DEFAULT_DEFINITION             = "E146";
	public final static String DELETE_DEFAULT_TEMPLATE        = "E129";
	public final static String DELETE_SUCCESSFUL              = "I003";
	public final static String DELETED_BY_ANOTHER_USER        = "E136";
	public final static String DIFFERENT_AUTHORIZER_REQUIRED  = "E040";
	public final static String DIFFERENT_SECOND_AUTH_REQRUIED = "E088";
	public final static String DOC_PREP_USERNAME              = "W005";
	public final static String DRAWINGS_PERCENTAGE_MISSING    = "E116";
	public final static String DRAWN_ON_PARTY_REQD            = "E241";
	public final static String DUPLICATE_REPT_CATEGS          = "E032"; //Suresh CR-603 03/15/2011 Add
	public final static String DUPLICATE_OP_BANK_ORGS         = "E033";
	public final static String DUPLICATES_NOT_ALLOWED         = "E160";
	public final static String EMAIL_ADDRESS_REQUIRED         = "E214";
	public final static String EMAIL_DETAILS_REQUIRED         = "E237";
	public final static String EMAIL_LANGUAGE_REQUIRED        = "E238";
//	public final static String EMAIL_RECEIVER_REQUIRED        = "E216";
	public final static String ENTER_INSTRUCTIONS             = "E132";
	public final static String ENTER_VALID_INSTRUMENT         = "E019";
	public final static String ENTRIES_DONT_MATCH             = "E118";
	public final static String ERROR_VALIDATE_2FA             = "E505";
	public final static String EXEC_DATE_LESS_THAN_CURRENT    = "E518";
	//cquinton 10/21/2013 Rel 8.3 ir#21748 - this is a warning, not an error
	// it was mistakenly made an error in release 8.2
	public final static String EXISTING_LINE_ITEMS_W_THIS_DEF = "W162"; //
	public final static String FAILED_DOCUMENT_VALIDATION     = "E255";
	public final static String FIELD_ALREADY_SELECTED         = "E150";
	public final static String FIELD_NAME_REQUIRED            = "E240";
    public final static String FIELD_TOO_LONG                 = "E529";
    public final static String FILE_FORMAT_NOT_PIPE_DELIMITED = "E530";
	public final static String FILE_NOT_FOUND                 = "E153";
    public final static String FILE_TYPE_NOT_SUPPORTED        = "E254";
	public final static String FILE_UPLOAD_EXCEPTION          = "E257";
	public final static String FINANCE_INFO_REQD              = "E054";
	public final static String FINANCE_TYPE_REQUIRED          = "E249";
	public final static String FIXED_MATURITY_DATE_BEFORE_TRANS_DATE = "W016";
	public final static String FIXED_MATURITY_DATE_REQUIRED   = "E181";
	public final static String FROM_PORT_MISSING              = "W010e";
	public final static String FX_RATE_OPT_LOCK_EXCEPTION     = "E055";
	public final static String GOODS_DESCRIPTION_MISSING      = "W010f";
	public final static String IN_CASE_MISSING_AUTHORITY      = "E104";
	public final static String IN_CASE_MISSING_TEXT           = "E114";
	public final static String INS_DOC_INFO_MISSING           = "E049";
	public final static String INSERT_SUCCESSFUL              = "I001";
	public final static String INSTRUMENT_LOCKED              = "I190";
	public final static String IN_USE_FOR_LC_CREATION_RULE    = "E159";
	public final static String IN_USE_FOR_GOODS_DESCRIPTION   = "E158";
    public final static String INSTRUMENT_AUTHENTICATION_FAILED = "E540"; //cr498
	public final static String INVALID_CLIENT_BANK_ORG        = "E102";
	public final static String INVALID_COMB_AVAILBY_PMTTERMS  = "E247";
	public final static String INVALID_COMB_PMT_COLL_INSTR    = "W106";
	public final static String INVALID_CONSIGN_TO_PARTY_DATA  = "E239";
	public final static String INVALID_CURRENCY_FORMAT        = "E189";
	public final static String INVALID_DOCUMENT_NAME          = "E258";
	public final static String INVALID_FROM_DATE              = "E080a";
	public final static String INVALID_FX_RATE                = "E095";
	public final static String INVALID_INSTRUMENT_CURRENCY    = "E115";
	public final static String INVALID_INSTRUMENT_ID          = "E070";
	public final static String INVALID_ISSUE_TRANSFER_INSTR   = "E112";
	public final static String INVALID_PASSWORD               = "E126";
	//BSL 08/30/11 CR663 Rel 7.1 Begin
	public final static String INVALID_REGISTRATION_PASSWORD  = "E123";
	public final static String UNABLE_TO_REGISTER             = "E117";
	//BSL 08/30/11 CR663 Rel 7.1 End
	//Ravindra - 03/29/2011 - CLUL032349513 - Start
	public final static String INCOMPLETE_USER_PROVISIONING   = "E562";//CR541
	//Ravindra - 03/29/2011 - CLUL032349513 - End
	//Suresh Penke 08/08/2011 IR-VVUJ051144191 Begin
	public final static String INVALID_DATE_MATURITY_DATE     ="E582";
	//Suresh Penke 08/08/2011 IR-VVUJ051144191 End
	public final static String INVALID_PASSWORD_2FA           = "E507";
	public final static String INVALID_PO_DEFINITION_TYPE     = "E236";
	public final static String INVALID_SHIP_TERM_COMBO        = "W011";
	public final static String INVALID_SWIFT_CHARS            = "E103";
	public final static String INVALID_TO_DATE                = "E080b";
	public final static String INVALID_REL_INSTR_EXPORT_LC    = "E259";
	public final static String INVALID_REL_INSTR_EXPORT_COL   = "E260";
	public final static String INVALID_REL_INSTR_IMPORT       = "E261";
    public final static String INVOICE_AUTHENTICATION_FAILED  = "E542"; //cr498
	public final static String ITEM_DELETED                   = "E191";
	public final static String ISSUE_DATE_REQUIRED            = "E242";
	public final static String LC_RULE_VALUE_TOO_BIG          = "E165";
	public final static String LCLOG_ADDED_TO_AMENDMENT       = "I024";
	public final static String LCLOG_AMENDMENT_STARTED        = "I020";
	public final static String LCLOG_AMOUNT_TOO_BIG           = "I169";
	public final static String LCLOG_CANT_UPDATE_PO_DATA      = "E164";
	public final static String LCLOG_CREATE_LC_START          = "I170";
	public final static String LCLOG_CREATE_LC_STOP           = "I171";
	public final static String LCLOG_DATA_MISSING             = "E167";
	public final static String LCLOG_GROUPING_START           = "I178";
//	public final static String LCLOG_INSTRUMENT_CREATED       = "I019";
	public final static String LCLOG_INVALID_CURRENCY         = "W172";
	public final static String LCLOG_INVALID_DATE_FORMAT      = "E173";
	public final static String LCLOG_INVALID_DELIMITED_FORMAT = "E174";
	public final static String LCLOG_INVALID_FIXED_FORMAT     = "E174";
	public final static String LCLOG_INVALID_NUMBER_FORMAT    = "E176";
	public final static String LCLOG_INVALID_SWIFT_CHARS_2PARM = "E177";
	public final static String LCLOG_INVALID_SWIFT_CHARS_3PARM = "E178";
	public final static String LCLOG_PART_AUTH_CANT_UPD_PO_DAT      = "E288";
	public final static String LCLOG_REJ_BY_BANK_CANT_UPD_PO_DAT    = "E289";
	public final static String LCLOG_LINE_ITEM_ASSIGNED       = "I177";
	public final static String LCLOG_LINE_ITEM_DATA_CHANGED   = "I025";
//	public final static String LCLOG_LINE_ITEM_NOT_GROUPED    = "I021";
	public final static String LCLOG_LINE_ITEM_REMOVED        = "I018";
	public final static String LCLOG_LINE_ITEM_UPDATED        = "I017";
	public final static String LCLOG_MULTIPLE_MATCHES_FOUND   = "E179";
	public final static String LCLOG_NO_FX_RATE               = "E180";
	public final static String LCLOG_NO_LC_RULES              = "I181";
	public final static String LCLOG_NO_ITEMS_FOR_LC_RULE     = "I182";
//	public final static String LCLOG_PARSE_START              = "I183";
	public final static String LCLOG_PROCESSING_LC_RULE       = "I184";
	public final static String LCLOG_TOTAL_ITEMS_UNASSIGNED   = "I185";
	public final static String LCLOG_UNASSIGNED_ITEMS_UPLOADED  = "I210";
	public final static String LCLOG_UPLOAD_ITEMS_UNASSIGNED  = "I186";
	public final static String LCLOG_UPLOAD_START             = "I012";
	public final static String LCLOG_UPLOAD_STOP              = "I193";
	public final static String LCLOG_VALUE_TOO_BIG            = "E163";
//	public final static String LINE_LENGTH_EXCEEDED           = "E144";
	public final static String LINE_LENGTH_EXCEEDED_SHORT     = "E192";
	public final static String LOAN_TERMS_NUM_DAYS_REQD         = "E182";
	public final static String LOAN_PAYMENT_AMOUNT_REQD         = "E183";
	public final static String LOAN_TYPE_IS_REQUIRED          = "E252";

	public final static String SEL_BANK_REQD_IF_SEL             = "E290";
	public final static String SEL_REQD_IF_BUY_FIN_SEL          = "E291";
	public final static String BEN_BANK_REQD_IF_BEN             = "E292";

	public final static String LOAN_REQ_CANNOT_RELATE_TRANSFER = "E262";
	public final static String LOAN_REQ_WITH_DIFF_CURRENCY_CODE = "E263";

	public final static String MATCH_AMT_GREATER_THAN_PAYMENT = "E343";
	public final static String MATURITY_DATE_REQUIRED         = "E231";
	public final static String MESSAGE_PROCESSED              = "I014";
	public final static String MISSING_PO_FIELDS              = "W014";
	public final static String MULTIPLE_INSTRUMENT_AUTHORIZE  = "E085";
	public final static String MULTIPLE_INSTRUMENT_AMEND  	  = "E1104";
	public final static String MULTI_TENOR_NOT_IND            = "E046";
	public final static String MUST_AMEND                     = "E110a";
	public final static String MUST_BE_LESS_THAN              = "E008";
	public final static String NEGATIVE_AMOUNT                = "E111";
	public final static String NO_ACTION_MESSAGE_AUTH         = "E130";
	public final static String NO_ACTION_NOTIFICATION_AUTH    = "E135";
	public final static String NO_ACTION_TRANSACTION_AUTH     = "E057";
//	public final static String NO_AMEND_INFO_IND              = "E110b";
	public final static String NO_CUSTOMER_SELECTED           = "E004";
	public final static String NO_DOCUMENTS_SELECTED          = "E107";
	public final static String NO_FOREX_DEFINED               = "E039";
	public final static String NO_ITEM_SELECTED               = "E076";
	public final static String NO_SUBSIDIARY_SELECTED         = "E230";
	public final static String NO_WORKGROUP_ASSIGNED          = "W006";
	public final static String NOT_INDICATED_UNDER            = "E096";
	public final static String NOT_MULTI_AND_RELATED_INST_ID  = "E184";
	public final static String NTM_CREATE_EXPORT_DLC          = "E091";
	public final static String NTM_DUPLICATE_INSTR_NUM        = "E066";
	public final static String NTM_INSTR_BAD_STATUS           = "E157";
	public final static String NTM_INSTR_DEACTIVATED          = "E218";
	public final static String NTM_INSTR_NUM_POOL_EXHAUSTED   = "E042";
	public final static String NTM_INSTR_PENDING              = "E071";
	public final static String NTM_INSTRUMENT_LOCKED          = "E041";
	public final static String NTM_INVALID_EXIST_INSTR_ID     = "E195";
	public final static String NTM_INVALID_EXIST_INSTR_TYPE   = "E087";
	public final static String NTM_INVALID_INSTR_TYPE         = "E063";
	public final static String NTM_INVALID_INSTR_OID          = "E067";
	public final static String NTM_INVALID_TEMPLATE_TYPE      = "E089";
	public final static String NTM_INVALID_TRANS_TYPE         = "E083";
	public final static String NTM_NO_DEFAULT_TEMPLATE_BANK   = "I005";
	public final static String NTM_NO_DEFAULT_TEMPLATE_CORP   = "E034";
	public final static String NTM_OTHER_TRANS_PENDING        = "W008";
	public final static String NTM_UNAUTH_TO_CREATE_TRANS     = "E063";
	public final static String NTM_WRONG_OP_BANK_ORG          = "E069";
	public final static String NTS1_BANK_BRANCH_NULL          = "E062";
	public final static String NTS1_COPY_FROM_NULL            = "E007";
	public final static String NTS1_INSTR_TYPE_NULL           = "E006";
	public final static String NTS1_MODE_NULL                 = "E026";
	public final static String NOTIFICATION_RULE_ASSIGNED     = "E215";
	public final static String NOTIFICATION_PROCESSED         = "I016";
	public final static String NOTIFICATION_PROCESSED_ALL     = "I027";
	public final static String NOTIFICATION_PROCESSED_MULTIPLE   = "I028";
//	public final static String NOTIF_RULE_REQUIRED            = "E217";
	public final static String NUM_DAYS_REQD                  = "E029";
	public final static String NUMBER_OF_DOCS_MISSING         = "W001";
	public final static String OTHER_DOCS_MISSING             = "E108";
	public final static String ORG_NO_DEFAULT_USER            = "E059";
	public final static String ORG_NOT_EXIST                  = "E074";
	public final static String OTHER_CONSIGNEE_MISSING        = "E051";
	public final static String PARENT_TO_CORP_CUSTOMERS       = "E011";
	public final static String ACCT_NUMBER_MISSING            = "E211";
	public final static String PARTY_MISSING                  = "E052";
	public final static String PARTY_TYPE_BANK_BRANCH_CODE    = "W015";
	public final static String PASSWORD_CONSECUTIVE           = "E125";
	public final static String PASSWORD_SAME_AS               = "E119";
	public final static String PASSWORD_SAME_AS_CURRENT       = "E120";
	public final static String PASSWORD_SPACE                 = "E122";
	public final static String PASSWORD_TOO_SHORT             = "E124";
	public final static String PASSWORD_UPPER_LOWER_DIGIT     = "E208";
	public final static String PASSWORD_HISTORY               = "E209";
	public final static String PASSWORD_HISTORY_COUNT         = "E210";
    public final static String PAY_CURRENCY_NOT_SAME_FOR_BENE = "E557";
    public final static String PAY_MATCH_AUTHENTICATION_FAILED = "E541"; //cr498
	public final static String PAY_METHOD_RULE_ALREADY_EXISTS = "E509";
	public final static String PLEASE_SPECIFY_INSTR_TO_BANK   = "E109";
	public final static String PMT_TERM_NOT_VALID_FOR_TENOR   = "E030";
	public final static String PO_CURR_NOT_SAME_ACROSS_SHIPS  = "E235";
	public final static String PO_LINE_ITEM_CURR_NOT_SAME     = "E232";
	public final static String PO_LINE_ITEM_MISMATCH          = "E166";
	public final static String PO_LINE_ITEMS_EXCEED_MAX_CHARS = "E072";
	public final static String PO_LINE_ITEM_MATCH_FOUND       = "E233";
	public final static String PO_ITEM_UPLOADED_WITH_THIS_DEF = "E015";
	public final static String PO_ITEM_CURR_NOT_SAME_AS_TRANS = "E012";
//	public final static String PO_UPLOAD_DEF_ASSOC_W_LC_RULE  = "E194";
	public final static String PREFIXES_SUFFIXES              = "E021";
	public final static String PRESENT_DATE_AFTER_EXPIRY      = "W012";
	public final static String PRESENT_DATE_BEFORE_EXPIRY     = "W009";
	public final static String REMOVE_PO_LINE_ITEMS_SUCCESS   = "I209";
	public final static String REQD_DOC_INFO_MISSING          = "E047";
	public final static String REQUIRED_ATTRIBUTE             = "E023";
	public final static String DIFFERENCE_IN_EXPIRY_DATE_AND_SHIP_DATE = "E559"; //VS IR SDUH021456070 01/13/11 Rel 7.0

	public final static String RESET_USER_SUCCESSFUL          = "I022";

	public final static String REL_INSTRUMENT_ID_NOT_FOUND_FOR_CORP_CUS = "E251";
	public final static String REL_INSTRUMENT_ID_NOT_FOUND_FOR_BANK_ORG = "E264";
	public final static String REL_INSTRUMENT_ID_NOT_ACTIVE = "E265";

	public final static String SAVE_SUCCESSFUL                = "I006";
	public final static String SECURITY_CORP_CUST_NO_2FA      = "E506";
	public final static String SECURITY_CORP_CUST_NO_CERT     = "E138";
	public final static String SECURITY_CORP_CUST_NO_SSO      = "E245";
	//BSL 09/07/11 CR663 Rel 7.1 Begin
	public final static String SECURITY_CORP_CUST_NO_REGISTERED_SSO = "E267";
	public final static String SECURITY_CORP_USER_NO_REGISTERED_SSO = "E268";
	//BSL 09/07/11 CR663 Rel 7.1 End
	public final static String SECURITY_CORP_USER_NO_2FA      = "E504";
	public final static String SECURITY_CORP_USER_NO_CERT     = "E121";
	public final static String SECURITY_CORP_USER_NO_SSO      = "E244";
	public final static String SECURITY_CORP_USER_NO_PASSWORD = "E134";
	public final static String SECURITY_DETAILS_LIMITS        = "E098";
	public final static String SECURITY_LRFT_CORP_NO_CERT   = "E213";
	public final static String SECURITY_MISSING_2FA           = "E504";
	public final static String SECURITY_MISSING_PASSWORDS     = "E139";
	public final static String SECURITY_MISSING_CERTIFICATES  = "E149";
	public final static String SECURITY_MISSING_SSO           = "E244";
	public final static String SECURITY_NEED_CERT_MAINT_USER  = "I194";
	public final static String SECURITY_USER_DOES_NOT_HAVE_RIGHTS  = "E246";
	public final static String SELECT_ADMIN_USER_ORG          = "E137";
	public final static String SELECT_DATAITEM_FOR_VALUE      = "E201";
	public final static String SELECT_ORG_TO_ROUTE            = "E044";
	public final static String SELECT_PMT_INSTR               = "E061";
//	public final static String SELECT_PMT_INSTR_IN_CUR_TRANS  = "E202";
	public final static String SELECT_PO_DEFNITION            = "E154";
	public final static String SELECT_PO_DEFN_FOR_UPLOAD      = "E151";
	public final static String SELECT_USER_OR_ORG_TO_ROUTE    = "E073";
	public final static String SELECT_USER_TO_ROUTE           = "E043";
	public final static String SELECT_VALUE_FOR_DATAITEM      = "E148";
	public final static String SENT_TO_BANK_SUCCESSFUL        = "I203";
	public final static String SENT_TO_CUST_SUCCESSFUL        = "I204";
	

    //cquinton 12/6/2012 the following generic error allows the UI to
	// display an error when there is a problem in the mediator -
	// rather than just displaying nothing -- so the user knows that
	// something is wrong.  ideally this would never be displayed
	// as it is the exception case.
    public final static String SERVER_ERROR                   = "E001";

	public final static String SETTLE_INFO_MISSING            = "E096b";
	public final static String SHIP_DATE_PAST_EXPIRY_DATE     = "E053";
	public final static String SHIP_INFO_MISSING              = "W010";
	public final static String SHIP_FROM_MISSING              = "W003";
	public final static String SHIP_TO_MISSING                = "W004";
	//Leelavathi IR#T36000017675 Rel8400 25/11/2013 Begin
	public final static String SHIP_DET_FROM_MISSING          = "E979";
	public final static String SHIP_DET_TO_MISSING            = "E980";
	public final static String SHIP_DET_INC_LOC_MISSING       = "E981";
	public final static String SHIP_DET_INCOTERN_MISSING      = "E982";
	//Leelavathi IR#T36000017675 Rel8400 25/11/2013 End
	public final static String SHIPMENT_ATTRIBUTE_EXISTS      = "E234";
	public final static String SHIPMENT_DAYS_NOT_NUMERIC      = "E155";
	public final static String SHIPMENT_DAYS_BOTH_REQUIRED    = "E156";
	public final static String SHIPMENT_DAYS_WRONG_ORDER      = "E204";
	public final static String SIZE_CANNOT_BE                 = "E161";
//	public final static String SIZE_NOT_NUMERIC               = "E145";
//	public final static String SIZE_REQUIRED                  = "E140";
	public final static String SPECIFY_ADDITIONAL_CHARGES     = "E105";
	public final static String SPECIFY_CHARACTER_TYPE         = "E143";
	public final static String SPECIFY_PO_FILE_FORMAT         = "E142";
	public final static String STARTDATE_CANNOT_BE_IN_PAST    = "E824";
	public final static String SUBSIDIARY_ACCESS_BEGIN        = "I004";
	public final static String SUBSIDIARY_ACCESS_END          = "I007";
	public final static String TENOR_AMOUNTS_NOT_EQUAL        = "E058";
	public final static String TENOR_REQD                     = "E045";
	public final static String TEXT_TOO_LONG                  = "E013";
	public final static String TEXT_TOO_LONG_SHOULD_END       = "E205";
	public final static String TO_PORT_MISSING                = "W010h";
//	public final static String TOO_MANY_CHARS_IN_COMB         = "E248";
	/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - Start */
	public final static String TOO_MANY_CHARS_IN_COMB_PARTYBEAN         = "E2015";
	/*KMehta IR-T36000034134 Rel 9300 on 27-Apr-2015 Change - End */
	public final static String TRANS_DIFF_WORK_GROUP_REQUIRED = "E090";
	public final static String TRANS_DOC_INFO_MISSING         = "E050";
	public final static String TRANS_DOC_INVALID_COMBO        = "E200";
	public final static String TRANS_PARTIALLY_AUTHORIZED     = "I013";
	public final static String TRANS_WORK_GROUP_REQUIRED      = "E092";
	public final static String TRANSACTION_CANNOT_PROCESS     = "E056";
	public final static String TRANSACTION_LIMIT_EXCEEDED     = "E010";
	public final static String TRANSACTION_PROCESSED          = "I010";
	public final static String TRANSACTION_IN_USE             = "E035";
    public final static String UNICODE_NOT_ALLOWED            = "E533";
	public final static String INV_DUE_DATE_REQD              = "E276";
	public final static String INV_ONLY_REQD                  = "E277";
	public final static String PO_INV_ONLY                    = "E278";
	public final static String INV_DATE_PAST_EXPIRY_DATE      = "E279";
//	public final static String FUND_AMT_GREATER_PMT_AMT       = "E280";
//	public final static String FUND_AMT_REQD                  = "E281";
//	public final static String INT_PAID_REQD                  = "E282";
//	public final static String LOAN_TERMS_REQD                = "E283";
	public final static String OTH_TXT_REQD                   = "E284";
	public final static String DEBIT_ACT_REQD                 = "E285";
//	public final static String NO_PO_DETAILS                  = "E287";

	public final static String CANT_DISPUTE_INV_WITH_FINREQUESTED   = "E294";
	public final static String INVOICE_AMT_GT_CLOSE_THRESHOLD_AMT   = "E295";
	public final static String INVOICE_AMT_GT_DISPUTE_THRESHOLD_AMT = "E298";
	public final static String DISPUTED_MATCHING_IS_IN_PROGRESS     = "E299";
	public final static String CANT_FINANCE_DISPUTED_INVOICE        = "E300";
	public final static String INVOICE_ALREADY_CLOSED               = "E301";
	public final static String INVOICE_AMT_GT_FINANCE_THRESHOLD_AMT = "E302";
	public final static String INVOICE_ALREADY_FINANCED             = "E303";
	public final static String INVOICE_CANNOT_BE_FINANCED           = "E304";
	public final static String INVOICE_HAS_PENDING_PAYMENT          = "E305";
	public final static String CLOSE_MATCHING_IS_IN_PROGRESS		= "E306";
//	public final static String INVOICE_ALREADY_PAID                 = "E307";
	public final static String CANT_CLOSE_FINANCE      				= "E308";
	public final static String CANT_DISPUTE                			= "E309";
	//Leelavathi IR#T36000021281 11/21/2013 Rel8400 Begin
	public final static String ADDL_DOC_INFO_MISSING				="E978";
	//Leelavathi IR#T36000021281 11/21/2013 Rel8400 End
	public final static String CANNOT_BE_GREATER             = "E317";
	public final static String INCLUDE_IN_PARAMETER         = "_inclInParameter";
	public final static String ACCT_NUMBER_NAME_MISSING    = "E296";
	public final static String ACCT_DESC_MISSING    = "E297";
//	public final static String NOT_ENOUGH_FUND    = "E480";
//	public final static String ERR_QUERYING_ACCT_BAL    = "E481";
	public final static String FROM_TO_ACCT_CANNOT_BE_SAME    = "E482";
	public final static String DUPLICATE_ACCT_SELECTED    = "E484";
//	public final static String EXCH_RATE_VAR_REQD    = "E488";
//	public final static String TRANSFER_DATE_IS_FUTURE_DATE     = "W020";
	public final static String ACCT_BAL_NOT_AVAILABLE           = "W021";
	public final static String RECENT_TRANS_NOT_AVAILABLE       = "E489";
	public final static String NO_RECENT_TRANS_RETURNED         = "E490";
//	public final static String ACCT_BAL_QUERY_MISSING_FIELD     = "E492";
	public final static String ACCT_TRAN_QYERY_MISSING_FIELD    = "E493";
	public final static String BAL_QUERY_MISSING_FIELD          = "W022";
	public final static String MISSING_REQUIRED_FIELDS    = "REQD_FLD_MISSING";
	public final static String ACTIVE_ACCOUNT          = "E494";
	public final static String BUY_SELL_RATE_REQD      = "E495";

	public final static String FX_RATE_GROUP_REQD      = "E496";
	public final static String DUPLICATE_ACCT_DESCRIPTION_SELECTED = "E491";

	public final static String PAYMENT_AMT_NOT_EQUAL_MATCH_AMT       = "E314";
	public final static String MORE_THAN_ONE_MATCH_FOR_INVOICE       = "E315";
//	public final static String APPROVE_DISCOUNT_NEEDED               = "E316";
	public final static String UNABLE_TO_SEND_TO_IMG_SERVER   = "E253";
	public final static String UNAUTH_PHRASE_TYPE             = "E094";
	public final static String UPDATE_SUCCESSFUL              = "I002";
	public final static String USER_NOT_EXIST                 = "E075";
	public final static String VESSEL_NAME_MISSING            = "W010i";
	public final static String PANEL_AUTH_COMBINATION = "E336A";
//	public final static String PANEL_AMOUNT = "E336C";

	public final static String PANEL_AMOUNT_RANGE_CHECK    = "E334";
	public final static String PANEL_AMOUNT_RANGE_DOES_NOT_EXIST    = "E500";
	public final static String DAILY_LIMIT_AMT_NOT_ENTERED  = "E335A";
//	public final static String USER_THRESHOLD_DETAILS_NOT_ENTERED = "E501";
//	public final static String TOT_NUM_OF_AUTHORIZERS_EXCEEDED    = "E336";
//	public final static String IDENTICAL_PANEL_AMOUNT_RANGE    = "E337";
	public final static String NEGATIVE_DAILY_LIMIT_AMT_ENTERED    = "E338";
	public final static String INSTRUMENT_TYPE_NOT_SELECTED    = "E339";
	public final static String AUTH_METHOD_NOT_SELECTED    = "E340";
//	public final static String PANEL_DETAILS_NOT_ENTERED	= "E341";
	public final static String PANEL_AMOUNT_RANGE_NOT_IN_SEQUENCE = "E342";
	public final static String TIMEZONE_REQUIRED   = "E339A";
//	public final static String ACCT_OP_ORG_EMPTY			  = "E421";			//IAZ IR-HAUL061553153 06/30/11 Add
	public final static String REP_FIELD_HAS_NON_ALPHA_NM	  = "E422";			//IAZ IR-DNUL061462150 06/18/11 Add
	public final static String MAX_NUM_PDF_PAGES_EXD		  = "E423"; 		//IAZ IR RRUL061611265 06/16/2011 Add
	public final static String FXRATE_GROUP_BASE_IS_NOT_VALID = "E424"; 		//IAZ IR PIUL051734251 05/17/2011 Add
	public final static String REP_CODE_NOT_UNIQUE  = "E425"; //BSL IR #VIUL032443438 CR-655 03/28/11 Add
	//BSL CR-655 03/04/11 Begin
//	public final static String REP_CODE_NOT_VALID_FOR_BOPORG  = "E426";
	//Ravindra - Rel710 - IR ASUL080179246 - 7th Sep 2011 - Start
	//CR655 Optional/Required Field Change ? The bank has requested that we undo the logic implemented in CR655
	//for release 7.0 and no longer require reporting codes as per the original requirements/design doc.
	//public final static String REP_CODE_NOT_SELECTED   	  	  = "E427";
	//Ravindra - Rel710 - IR ASUL080179246 - 7th Sep 2011 - End
	public final static String REP_CODE_OP_ORG_ID_MISSING     = "E428";
	public final static String REP_CODE_BANK_GRP_MISSING   	  = "E429";
	//BSL CR-655 03/04/11 End
	public final static String CANT_ADD_DOM_PMT_FIXED_TR	  = "E430";			//IAZ CR-586 IR-VRUK091652765 10/09/10 Add
    public final static String CANT_DELETE_DOM_PMT_FIXED_TR	  = "E555";			//rkazi - IR SAUK121782011 - 01/15/2011 - Add
    public final static String DOM_PMT_MTHD_NOT_UNIQUE	  = "E556";			//rkazi - IR RAUL011683765 - 01/15/2011 - Add
	public final static String CALENDAR_NOT_DEFINED_FOR_YEAR  = "E435";
	public final static String CURRENCY_CALENDAR_NOT_DEFINED  = "E436";
	public final static String NO_PANEL_GROUP_SET_FOR_DEB_ACCOUNT = "E437";
	public final static String DIFFERENT_AUTH_METHOD_SELECTED = "E438";
	public final static String DP_FILE_UPLD_INVLD_PMT_MTH_CDE = "E439";
//	public final static String DP_FILE_UPLOAD_INVALID_CCY_CDE = "E440";
//	public final static String DP_FILE_UPLOAD_CCY_INCONSIST   = "E441";
	public final static String ACCOUNT_DEACTIVATED	  		  = "E442";
//	public final static String CANT_AUTH_SUBS_PANEL_CHILD	  = "E443";
//	public final static String CANT_AUTH_SUBS_PANEL_PARENT	  = "E444";
	public final static String DP_FU_CANT_DELETE_DPS_WRN	  = "W445";
	public final static String CUST_ACCTS_DATA_DUPLICATED 	  = "E446";
//	public final static String CM_PMT_DATE_CNT_BE_BCKDTD_W_ID = "E447";
	public final static String NO_BUY_FOREX_DEFINED           = "E448";
	public final static String AUTH_URL_NOT_SPECIFIED		  = "W663"; // DK IR T36000028485 Rel8.4 BTMU 05/27/2014
	public final static String PAYEE_DELETE_SUCCESSFUL		  = "I450";
	public final static String DP_OID_NOT_PROVIDED_FOR_DELETE = "E451";
	public final static String CANNOT_UPDATE_CUST_DAILY_LIMIT = "E452";
	public final static String USER_CANT_USE_ACCOUNTS		  = "E453";
	public final static String DOM_PMT_TRAN_NOT_UPDATED       = "E454";
	public final static String CERTIFICATE_AUTH_REQ_ERR		  = "E455";
//	public final static String CERTIFICATE_AUTH_REQ			  = "W456";
	public final static String RECERTIFICATION_FAILED		  = "E457";
	public final static String INCORRECT_CERTIFICATE_ID       = "E458";
//	public final static String CUST_DAILY_LIMIT_UNKNOWN       = "W458";
	public final static String CUST_DAILY_LIMIT_EXCEEDED      = "W459";
	public final static String PROVIDE_ALL_REQ_PARMS          = "E460";
	public final static String DOM_PMT_DATE_CANT_BE_BACKDATED = "E461";
	public final static String DOM_PMT_DATE_IS_FUTURE_DATE    = "W462";
	public final static String NO_SELL_FOREX_DEFINED          = "E463";
	public final static String DOM_PMT_AMOUNT_IS_ZERO         = "E464";
//	public final static String DOM_PMT_AMOUNT_IS_INVALID      = "W465";
	public final static String DOM_PMT_AMOUNT_IS_INVALID_WRN  = "W465";
	public final static String DOM_PMT_AMOUNT_IS_INVALID_ERR  = "E465";
//	public final static String MESSAGE_TO_PAYER_IS_MISSING    = "E466";
	public final static String DOM_PMT_OID_NOT_SUPPLIED_WRN   = "W467";
	public final static String DOM_PMT_GEN_ERR_WRN            = "W468";
	public final static String INV_DET_GEN_ERR_WRN            = "W468"; //BSL IR#PUL032965444 04/04/11
	public final static String NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE 	   = "E469";
	public final static String PANEL_AUTH_REQS_NOT_MET		   = "W470";
//	public final static String PANEL_AUTH_FOR_LEVEL_ALREADY_MET_WRN	   = "W471";
//	public final static String PANEL_AUTH_FOR_LEVEL_ALREADY_MET_ERR	   = "E471";
	public final static String USER_ALREADY_ATTEMPT_AUTHRZ_PANEL   	   = "W472";
//	public final static String USER_IS_NOT_PAN_AUTH_WRN   	  = "W473";
//	public final static String USER_IS_NOT_PAN_AUTH_ERR   	  = "E473";
//	public final static String MAX_NUMBER_OF_PAN_AUTH_REACHED = "E474";
//	public final static String USER_CANNOT_AUTHORISE_SELF	  = "E475";
//	public final static String PANEL_AUTH_SUCCESS             = "I476";
//	public final static String CERTIFICATE_AUTH_FAILED		  = "E477";
	public final static String DOM_PMT_GEN_UPD_ERR_WRN        = "W478";
	public final static String PAYEE_DATA_MISSING_ERR         = "E479";
	public final static String NO_PANEL_GROUPS_SPECIFIED	  = "W479"; // PDUK010441815
	public final static String NO_ACCOUNT_SELECTED			  = "W480"; // PDUK010441815
	public final static String NO_SUBSIDARY_THRESHOLD         = "W481"; // RIUK062552727
	public final static String NO_SUBSIDARY_WORKGRP           = "W482"; // RIUK062552727
	public final static String MAX_SPOT_RATE_AMOUNT_EXCEEDED_WARN = "W483"; // MDB Rel7.1 CR-640 09/26/2011
	public final static String MAX_DEAL_AMOUNT_EXCEEDED_WARN = "W489"; //MDB PRUL112347061 Rel7.1 12/15/11

	public final static String NO_AUTH_ACCESS_AT_PARENT_LEVEL = "E434"; // PYUK071981074


	public final static String CUST_ACCTS_DATA_MISSING_ERR    = "E470";
	public final static String CUST_ACCTS_DATA_MISSING_WRN    = "W474";
//	public final static String FILE_NOT_IN_SPECIFIC_FORMAT    = "E185";
	public final static String INVALID_PAYMENT_DATE    		  = "E186";
//	public final static String INVALID_CURRERNCY    		  = "E190";
//	public final static String CURRERNCY_NOTMATCH_DEBIT_AC    = "E193";
	public final static String INVALID_DEBIT_AC               = "E219";
	public final static String INVALID_CREDIT_AC   			  = "E525";
//	public final static String AC_NOTAVAIL_DOM_PMT    		  = "E220";
	public final static String MISSING_REQUIRED_FILES    	  = "E524";
//	public final static String AC_NOTAVAIL_DOM_PMT_TRANS      = "E222";
//	public final static String AC_NOTAVAIL_DEBIT    	      = "E223";
//	public final static String NO_OF_PAYEE_NOTMATCH    	      = "W224";
//	public final static String PAYEES_AMT_NOTMATCH    	      = "W225";
//	public final static String PAYEES_AMT_TOOBIG    	      = "E226";
//	public final static String HEADER_NOT_IN_SPECIFIC_FORMAT  = "E227";
	public final static String AC_NOTVALID_FOR_USER 		  = "E228";
	public final static String ORDERING_PARTY_INVALID         = "E516";
	public final static String AC_NOTAVAIL_FOR_DPT 		  	  = "E229";
	public final static String AC_NOTAVAIL_FOR_DDI            = "E517";
	public final static String AC_NOT_SAME_FOR_PAYER          = "E526";
//	public final static String AC_NOT_SAME_FOR_BENE           = "E527";
	public final static String CURRENCY_NOT_SAME              = "E528";


//	public final static String PAYEES_AMT_NON_NUM_FORMAT 	  = "E250";
//	public final static String PAYEES_BANK_CHRGS_WRONG_FORMAT = "E266";

	public final static String TRANSFER_DATE_CANNOT_BE_BACKDATED   = "E483";
//	public final static String TRANSACTION_INVOLVES_TWO_CURRENCIES = "E462";
//	public final static String PAYMENT_DATE_IS_FUTURE_DATE         = "W019";


	public final static String INVALID_AMOUNT_TYPE                 = "E485";
	public final static String INVALID_DATE_TYPE                   = "E486";
	public final static String AUTHORIZE_INVALID_UNMATCHED_AMOUNT  = "E487";

	public final static String CALENDAR_NAME_NOT_UNIQUE            = "E520";
	public final static String WEEKEND_DAY1_SAME_AS_WEEKEND_DAY1   = "E521";
	public final static String CALENDAR_IS_REFERECED               = "E522";
	public final static String TPCALENDAR_WORK_DAY                 = "0";
	public final static String TPCALENDAR_HOLIDAY                  = "1";

	public final static String PAYMENT_FILENAME_SIZE_EXCEEDED      = "E549";
	public final static String ERR_SAVING_PAYMENTUPLOAD_FILE       = "E550";
	public final static String ERR_PROCESSING_PAYMENTUPLOAD_FILE   = "E551";
	public final static String PAY_MTHD_NOT_SAME_FOR_BENE          = "E552";
    public final static String PAYMENT_FILE_BENES_EXCEEDED         = "E558";
	public final static String PAYMENT_FILE_SIZE_EXCEEDED          = "E553";

	public final static String FXRATE_GROUP_COMPARISON			   = "E561"; //CR610 Rel7.0.0.0 - AAlubala FX Rate Group Comparison

	public final static String USER_REFERECED_AS_SYSTEM_USER 	   = "E563";// CJR CR-593 Rel7.0.0.0
    // CR-593 NSX 04/04/11  Rel. 7.0.0 - Begin
	public final static String INVALID_CUSTOMER_ID  			   = "E565";
	public final static String CUSTOMER_NO_ACCESS_PAYMENT		   = "E566";
	public final static String SYSTEM_USER_NOT_DEFINED			   = "E567";
	public final static String MAX_LENGTH_EXCEDDED  			   = "E568";
	public final static String INVALID_PAYMENT_FILE_FORMAT  	   = "E569";
	public final static String ERROR_DECRYPTING_FILE        	   = "E570";
    // CR-593 NSX 04/04/11  Rel. 7.0.0 - End

	public final static String INV_LINE_EXCEEDED          = "E572";			// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011
	public final static String INV_CHAR_EXCEEDED          = "E573";			// IR SEUL041149384 Rel7.0.0.0 - 04/18/2011


	// CJR 4/25/2011 IR SBUL040836986
	public final static String CBFT_BENE_ADD_LINE_4_BLANK = "E574";

	public final static String FXRATE_GROUP_BASE_CURRECY_COMPARISON			= "E577"; //RKazi IR BLUL040459173 04/11/2011.
	public final static String ACCOUNTS_NOT_SAME_BOPORG = "E578";//BSL IR ACUL042681341 05/03/11

	public final static String INVALID_EMAIL                        = "E583";   //IR KIUL080237339 - 08/11/11

	//MDB Rel7.1 CR-640 Begin
	public final static String MAX_DEAL_AMOUNT_EXCEEDED             = "E595";
	public final static String MAX_SPOT_RATE_AMOUNT_EXCEEDED        = "E596";
	public final static String MARKET_RATE_UNAVAILABLE				= "E597";
	//MDB Rel7.1 CR-640 End

	//NSX CR-640/581 Rel 7.1.0  09/26/11 Start -
	public final static String FX_GET_RATE_GENERAL_ERR               = "E600";
	public final static String FX_GET_RATE_REQUEST_DENIED_ERR        = "E601";
	public final static String FX_GET_RATE_SERVICE_NOT_ENABLED_ERR   = "E602";

	public final static String FX_GET_DEAL_GENERAL_ERR               = "E605";
	public final static String FX_GET_DEAL_FX_ONLINE_TIMEDOUT_ERR    = "E606";
	public final static String FX_GET_DEAL_SERVICE_NOT_ENABLED_ERR   = "E607";
	public final static String FX_GET_DEAL_EXPIRED                   = "E608";

	public final static String FX_GET_RATE_NO_RESPONSE               = "E610";
	public final static String FX_GET_DEAL_NO_RESPONSE               = "E611";
	public final static String FX_GET_RATE_NO_ITEM_SELECTED          = "E612";
	public final static String FX_GET_RATE_MULTIPLE_ITEMS_SELECTED   = "E613";
	public final static String FX_GET_RATE_INVALID_TRANS_STATUS      = "E614";
	public final static String FX_GET_RATE_NOT_AVAIL_FOR_BRANCH      = "E615";
	//public final static String FX_GET_RATE_SERVICE_NOT_ENABLED_ERR   = "E616";
	public final static String FX_RATE_CHANGED                       = "E617";
	public final static String NO_PERMISSION_TO_AUTH                 = "E618";
	public final static String MARKET_RATE_OR_FX_CONTRACT_REQD       = "E619";
	public final static String CROSS_RATE_MISSING_REQD_FIELDS        = "E620";
	public final static String CROSS_RATE_IS_REFERENCED              = "E621";
	public final static String DUPLICATE_CROSS_RATE_ENTRY            = "E622";
	public final static String CROSS_RATE_FROM_AND_TO_CCY_SAME       = "E623";
	public final static String ATLEAST_ONE_CCY_COMB_REQD             = "E624";
//	public final static String TITLE_EMPTY							 = "E626";
//	public final static String SUBJECT_EMPTY						 = "E627";
//	public final static String STARTDATE_EMPTY						 = "E628";
//	public final static String ENDDATE_EMPTY						 = "E629";
	public final static String OP_BANK_OR_EXTERNAL_BANK_REQ			 = "E1069"; //Kyriba CR 268
	public final static String DUPLICATE_EXTERNAL_BANKS				 = "E1070"; //Kyriba CR 268
	// jgadela R8.3 Cr 501 START
	public final static String PENDING_REC_EXISTS_UPDATE      = "E900";
//	public final static String PENDING_REC_EXISTS_DELETE      = "E901";
	// jgadela R8.3 Cr 501 START

	public final static String FX_DEAL_SUCCESSFULY_BOOKED            = "W484";
	public final static String MARKET_RATE_REQRD_UPON_AUTH           = "W485";
	public final static String FX_SPOT_RATE_THREDSHOLD_EXCEED_MARKETRATE  = "W486";
//	public final static String FX_SPOT_RATE_THREDSHOLD_EXCEED      = "W487";

	//smanohar Kyriba PPX268 start
	public final static String SWIFT_ADDRESS_REQUIRED    = "E1100";
 	public final static String TPS_ID_REQUIRED    = "E1101";
	public final static String BANK_GROUP_REQUIRED    = "E1102";
	//smanohar Kyriba PPX268 end

	//NSX CR-640/581 Rel 7.1.0  09/26/11 End -

	// Error Messages for Middleware (in alpha order)
	public final static String CONFIRM_PACKAGER_GENERAL      = "MID001";
	public final static String CONFIRM_PACKAGER_TIMESTAMP    = "MID002";
	public final static String CONFIRM_UNPACK_GENERAL        = "MID003";
	public final static String CONFIRM_UNPACK_REDUNDANT_MSG  = "MID004";
	public final static String CONFIRM_UNPACK_UNEXPECTED_MSG = "MID005";
	public final static String CONFIRM_UNPACK_UNRECOGN_MSG   = "MID006";
//	public final static String TPL_MESSAGE_PACKAGING_ERROR   = "MID007";

//	public final static String COMPONENT_ALREADY_EXISTS      = "MID008";
	public final static String VALUE_MISSING                 = "MID009";
	public final static String NO_MATCHING_VALUE_FOUND       = "MID010";
	public final static String MORE_THAN_ONE_VALUE_FOUND     = "MID011";
	public final static String NUMBER_OF_ENTRIES_MORE        = "MID012";
	public final static String NO_TRANSACTION_TO_UPDATE      = "MID013";
	public final static String UNPACKAGING_MISMATCH_DATA     = "MID014";
	public final static String TASK_NOT_SUCCESSFUL           = "MID015";
	public final static String NOT_ORIGINATING_TRANSACTION   = "MID016";
	public final static String VALUE_DOESNOT_MATCH           = "MID017";
	public final static String NO_ASSOCIATED_VALUE           = "MID018";
	public final static String EOD_ACCOUNT_NOTFOUND          = "MID019";
	public final static String EOD_FAIL_ROUTE                = "MID020";
	public final static String PAYFILE_FAIL_ROUTE            = "MID021";
	public final static String SEND_TO_CUST_REQ           	 = "E2028";


        // NSX - PRUK080636392 - 08/16/10 - Begin
	public final static String STATUS_ALL                    = "All";
	public final static String STATUS_STARTED                = "Started";
	public final static String STATUS_READY                  = "Ready to Authorise";
	public final static String STATUS_PARTIAL                = "Partially Authorised";
	public final static String STATUS_AUTH_FAILED            = "Authorise Failed";
	public final static String STATUS_REJECTED_BY_BANK       = "Rejected by Bank";

	//Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start he
	// changed the the constant values in the part of bug fixing
	//
	public final static String STATUS_READY_TO_CHECK         = "READY_TO_CHECK";
	public final static String STATUS_REPAIR			     = "REPAIR";
	//Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - End

	public final static String STATUS_ACTIVE                 = "Active";
	public final static String STATUS_INACTIVE               = "Inactive";
	public final static String STATUS_PAID                   = "Paid";
	public final static String STATUS_PENDING                = "Pending";
        // NSX - PRUK080636392 - 08/16/10 - End

	// Default Branding - used for pages (such as the exception page) where
	// the system cannot determine the user's branding.
	public final static String DEFAULT_BRANDING = "default";

	// Error Categories
	public final static String ERR_CAT_1 = "CAT1";
	public final static String ERR_CAT_MIDDLEWARE = "Middleware Error";

	// Increase / Decrease
	public final static String INCREASE = "Increase";
	public final static String DECREASE = "Decrease";
	public final static String NOAMOUNT = "NoAmount";

	// Refdata Table Type Constants
	public final static String ACCT_OWNER_TYPE          = "ACCT_OWNER_TYPE";
	public final static String ACTIVATION_STATUS        = "ACTIVATION_STATUS";
	public final static String AVAILABLE_BY             = "AVAILABLE_BY";
	public final static String AVAILABLE_WITH_PARTY     = "AVAILABLE_WITH_PARTY";
	public final static String AUTHENTICATION_METHOD    = "AUTHENTICATION_METHOD";
	public final static String AMOUNT_TYPE    		= "AMOUNT_TYPE";
	public final static String BANK_ACTION_REQ          = "BANK_ACTION_REQ";
	public final static String BANK_CHARGES_TYPE        = "BANK_CHARGES_TYPE";
	public final static String CHANGE_TYPE              = "CHANGE_TYPE";
	public final static String CHARACTERISTIC_TYPE      = "CHARACTERISTIC_TYPE";
	public final static String CHARGE_TYPE              = "CHARGE_TYPE";
	public final static String CONFIRMATION_TYPE        = "CONFIRMATION_TYPE";
	public final static String CONSIGNMENT_TYPE         = "CONSIGNMENT_TYPE";
	public final static String CONSIGNMENT_PARTY_TYPE   = "CONSIGNMENT_PARTY_TYPE";
	public final static String COUNTRY                  = "COUNTRY";
	public final static String CURRENCY_CODE            = "CURRENCY_CODE";
	public final static String DATE_TYPE    		= "DATE_TYPE";
	public final static String DELIMITER_CHAR           = "DELIMITER_CHAR";
	public final static String DRAWING_TYPE             = "FINANCE_DRAWING_TYPE";
	public final static String DRAWN_ON_PARTY           = "DRAWN_ON_PARTY";
	public final static String FORM_TYPE                = "FORM_TYPE";
	public final static String ICC_GUIDELINES           = "ICC_GUIDELINES";
	public final static String IN_ADVANCE_DISP_TYPE     = "IN_ADVANCE_DISP_TYPE";
	public final static String IN_ARREARS_DISP_TYPE     = "IN_ARREARS_DISP_TYPE";
	public final static String INCOTERM                 = "INCOTERM";
	public final static String INSURANCE_RISK_TYPE      = "INSURANCE_RISK_TYPE";
	public final static String INSTRUMENT_LANGUAGE      = "INSTRUMENT_LANGUAGE";
	public final static String INSTRUMENT_STATUS        = "INSTRUMENT_STATUS";
	public final static String VALIDATION_STATUS        = "VALIDATION_STATUS";
	public final static String INSTRUMENT_TYPE          = "INSTRUMENT_TYPE";
	public final static String INST_TYPE_SLC_AS_GUA     = "INST_TYPE_SLC_AS_GUA";
	public final static String INVOICE_STATUS           = "INVOICE_STATUS";
	public final static String INVOICE_SEARCH_STATUS	= "INVOICE_SEARCH_STATUS";
	public final static String INVOICE_FINANCE_STATUS   = "INVOICE_FINANCE_STATUS";
	public final static String INVOICE_PAYMENT_STATUS   = "INVOICE_PAYMENT_STATUS";
	public final static String LOAN_TERMS_TYPE          = "LOAN_TERMS_TYPE";
	public final static String LOCALE                   = "LOCALE";
	public final static String MATCHING_STATUS_TYPE     = "MATCHING_STATUS_TYPE";
	public final static String MARKED_FREIGHT_TYPE      = "MARKED_FREIGHT_TYPE";
	public final static String MESSAGE_SOURCE           = "MESSAGE_SOURCE";
	public final static String MESSAGE_STATUS           = "MESSAGE_STATUS";
	public final static String MULTIPLY_DIVIDE_IND      = "MULTIPLY_DIVIDE_IND";
	public final static String OWNERSHIP_LEVEL          = "OWNERSHIP_LEVEL";
	public final static String PANEL_AUTH_TYPE          = "PANEL_AUTH_TYPE";
	public final static String PARTY_TYPE               = "PARTY_TYPE";
	public final static String PAYMENT_TERMS            = "PAYMENT_TERMS";
	public final static String PAYMENT_SOURCE_TYPE      = "PAYMENT_SOURCE_TYPE";
	public final static String PHRASE_CATEGORY          = "PHRASE_CATEGORY";
	public final static String PHRASE_TYPE              = "PHRASE_TYPE";
	public final static String PLACE_OF_EXPIRY_TYPE     = "PLACE_OF_EXPIRY_TYPE";
	public final static String PMT_INSTR_TYPE           = "PMT_INSTR_TYPE";
	public final static String PMT_TERMS_DAYS_AFTER     = "TENOR_TYPE";
	public final static String PMT_TERMS_TYPE           = "PAYMENT_TERMS_TYPE";
	public final static String PO_FIELD_DATA_TYPE       = "PO_FIELD_DATA_TYPE";
//	public final static String PO_UPLOAD_FILE_FORMAT    = "PO_UPLOAD_FILE_FORMAT";
//	public final static String QUEUE_MESSAGE_TYPE       = "QUEUE_MESSAGE_TYPE";
//	public final static String SEC_PROFILE_TYPE         = "SEC_PROFILE_TYPE";
	public final static String SETTLEMENT_HOW_TYPE      = "SETTLE_HOW";
//	public final static String TRACER_SEND_TYPE         = "TRACER_SEND_TYPE";
	public final static String TRANS_DOC_ORIGINALS_TYPE = "TRANS_DOC_ORIGINALS_TYPE";
	public final static String TRANS_DOC_TYPE           = "TRANS_DOC_TYPE";
	public final static String TRANSACTION_STATUS       = "TRANSACTION_STATUS";
	public final static String TRANSACTION_TYPE         = "TRANSACTION_TYPE";
	public final static String WIP_VIEW                 = "WIP_VIEW";
	public final static String MATCHED_PAYMENT_STATUS_TYPE = "MATCHED_PAYMENT_STATUS_TYPE";
//	public static final String MSG_TYPE_SCFTPREPORTING  = "SCFTP";
//	public static final String SCF_REPORTING_UPDATE_TYPE_PORUPL = "PORUPL";
//	public static final String SCF_REPORTING_UPDATE_TYPE_INVUPL = "INVUPL";
//	public static final String SCF_REPORTING_UPDATE_TYPE_IMDUPL = "IMDUPL";
//	public static final String SCF_REPORTING_UPDATE_TYPE_PAMUPL = "PAMUPL";
//	public static final String SCF_REPORTING_UPDATE_TYPE_PORTACP= "PORTACP";
//	public static final String SCF_REPORTING_UPDATE_TYPE_PORTACI= "PORTACI";
//	public static final String SCF_REPORTING_UPDATE_TYPE_PORTACU= "PORTACU";

	// Button actions.  Use these in submit buttons and mediators.
	// It helps match up what button was pressed on the form.
	public final static String BUTTON_ADD_PO_LINE_ITEMS         = "AddPOLineItems";
	public final static String BUTTON_ADD_PO_LINE_ITEM          = "AddPOLineItem";
	public final static String BUTTON_ADD_PO_LINE_ITEMS_MANUAL  = "AddPOLineItemsManuallyButton";
	public final static String BUTTON_ADD_FIVE_PO_LINE_ITEMS    = "Add5POLineItems";
	public final static String BUTTON_ADD_SHIPMENT              = "AddShipment";
//	public final static String BUTTON_ADD_UPLOADED_PO_LINE_ITEMS = "AddUploadedPOLineItemsButton";
	public final static String BUTTON_ADDRESSSEARCH             = "AddressSearch";

	public final static String BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS = "DisputeUndisputeSelectedItems";
	public final static String BUTTON_FINANCE_SELECTED_ITEMS         = "FinanceSelectedItems";
	public final static String BUTTON_CLOSE_SELECTED_ITEMS           = "CloseSelectedItems";


	public final static String BUTTON_ATTACH_DOCUMENTS          = "AttachDocuments";
	public final static String BUTTON_AUTHORIZE                 = "Authorize";
	public final static String BUTTON_PROXY_AUTHORIZE                 = "ProxyAuthorize"; //AAlubala CR-711 Rel8.0 - 10/14/2011
//	public final static String BUTTON_CALCULATE                 = "CalcNewAmount";
	public final static String BUTTON_CHECK_LINE_LENGTH         = "CheckLength";
//	public final static String BUTTON_CLEAR                     = "Clear";
	public final static String BUTTON_CONTINUE                  = "Continue";
	public final static String BUTTON_AUTHORIZEDELETE           = "AuthorizeAndDelete";
	public final static String BUTTON_APPROVE_DISCOUNT_AUTHORIZE= "ApproveDiscountAuthorize";

	public final static String BUTTON_CREATE_ATP_FROM_PO        = "CreateATPFromPO";
	public final static String BUTTON_CREATE_LC_FROM_PO         = "CreateLCFromPO";

	public final static String BUTTON_CREATE_LC_CREATION_RULE   = "CreateLCCreationRule";
	public final static String BUTTON_DELETE                    = "Delete";
    public final static String BUTTON_DELETE_ALL                = "DeleteAll";
	public final static String BUTTON_DELETE_ATTACHED_DOCUMENTS = "DeleteAttachedDocuments";
	public final static String BUTTON_DELETE_SELECTED_PO_ITEMS  = "DeleteSelectedPOLineItems";
	public final static String BUTTON_DELETE_SHIPMENT           = "DeleteShipment";
	public final static String BUTTON_DELETRANS                 = "DeleteTrans";
	public final static String BUTTON_DELETE_PO                 = "DeletePO";
	public final static String BUTTON_DEACTIVATE                 = "Deactivate";
	//MDB CR-564 Rel6.1 12/21/10 Begin
	public final static String BUTTON_CONFIRM				 	= "Confirm";
	public final static String BUTTON_CONFIRM_FX 			 	= "ConfirmFX";    //NSX CR-640/581 Rel 7.1.0  09/26/11
	public final static String BUTTON_REJECT				 	= "Reject";
	public final static String BUTTON_DELETE_PAYMENT_UPLOAD 	= "DeletePaymentUpload";
	public final static String BUTTON_DELETE_REJECTED__ITEMS 	= "DeleteRejectedItems";

	//MDB CR-564 Rel6.1 12/21/10 End
	public final static String BUTTON_EDIT                      = "Edit";
	public final static String BUTTON_EDIT_PO_LINE_ITEMS        = "EditPOLineItemsButton";
	public final static String BUTTON_FILTER                    = "Filter";
	public final static String BUTTON_INSTRUMENT_SEARCH         = "InstrumentSearch";
	public final static String BUTTON_LOGIN                     = "Login";
//	public final static String BUTTON_NEW_TRANSACTION           = "NewTransaction";
//	public final static String BUTTON_PARTYSEARCH               = "PartySearch";
	public final static String BUTTON_PHRASELOOKUP              = "PhraseLookup";
	public final static String BUTTON_PHRASECLEAR               = "PhraseClear";
	public final static String BUTTON_PHRASEFILLIN              = "PhraseFillin";
//	public final static String BUTTON_REMOVE_PO_LINE_ITEMS      = "RemovePOLineItemsButton";
//	public final static String BUTTON_REPLYTOBANK               = "ReplyToBank";
	public final static String BUTTON_RESET_ACCOUNT             = "ResetUserAccount";
	public final static String BUTTON_ROUTE                     = "Route";
	public final static String BUTTON_ROUTEANDSAVE              = "RouteAndSave";
	public final static String BUTTON_SAVE                      = "Save";
	public final static String BUTTON_SAVEANDCLOSE              = "SaveAndClose";
	public final static String BUTTON_SAVEASDRAFT               = "SaveAsDraft";
	public final static String BUTTON_SAVECLOSETRANS            = "SaveCloseTrans";
	public final static String BUTTON_SAVEROUTETRANS            = "SaveRouteTrans";
	public final static String BUTTON_SAVETRANS                 = "SaveTrans";
	public final static String BUTTON_SEARCH                    = "Search";
	public final static String BUTTON_SELECT                    = "Select";
	public final static String BUTTON_SENDTOBANK                = "SendToBank";
	public final static String BUTTON_SENDTOCUST                = "SendToCust";
	public final static String BUTTON_TIMEOUT                   = "Timeout";
	public final static String BUTTON_TEMPLATETIMEOUT           = "TemplateTimeout";
	public final static String BUTTON_UPLOAD_FILE               = "UploadFile";
	public final static String BUTTON_VERIFY                    = "Verify";
	public final static String BUTTON_ADD_PAYEE					= "AddPayee";
	public final static String BUTTON_MODIFY_PAYEE				= "ModifyPayee";
	public final static String BUTTON_ADD_PAYEE_TRAN			= "AddPayeeToTrans";
	public final static String BUTTON_DELETE_PAYEE				= "DeletePayee";
	public final static String BUTTON_SELECT_TEMPLATE			= "SelectTemplate";
	public final static String BUTTON_UNREGISTER_ACCOUNT		= "UnregisterAccount";      //BSL 09/01/11 CR663 Rel 7.1 Add
	public final static String BUTTON_SAVE_FORMATTING			= "SaveFormattingButton";
	public final static String DOM_PMT_VERIFY_SUCCESSFUL		= "DomPmtVerifyOK";
//	public final static String MULTIPLE_PAYEES_ENTERED			= "Multiple Payees";		//IAZ CM-451 01/29/09 Add
//	public final static String MULTIPLE_REFERENCES_ENTERED		= "Multiple References";	//IAZ CM CR-507 12/18/09 Add
//	public final static String MULTIPLE_PAYERS_ENTERED			= "Multiple Payers";		//IAZ CM CR-509 01/07/10 Add
//	public final static String MULTIPLE_BENES_ENTERED			= "Multiple Beneficiaries";	//IAZ CM CR-507 01/18/10 Add
//	public final static String PANEL_AUTH_LEVEL_A				= "A";
//	public final static String PANEL_AUTH_LEVEL_B				= "B";
//	public final static String PANEL_AUTH_LEVEL_C				= "C";
//	public final static String PANEL_AUTH_LEVEL_D				= "D";
	public final static String AUTHFALSE						= "FALSE";
	public final static String AUTHTRUE							= "TRUE";
	public final static String AUTHPARTL						= "PARTIAL";
//	public final static String AUTHPREVSTAT      				= "PREVPARTL";
	public final static String AUTHCONTINUE      				= "ACONTINUE";
	public final static int	   NUMBER_OF_TENORS 			    = 6;
	public final static int	   MAX_TRAN_AUTH_NUM			    = 6;
	public final static int    MAX_CORP_ORG_RETRY				= 3;
	public final static int    MAX_REP_CODE_IDX                 = 2;  //BSL CR-655 03/04/11 Add
	public final static int    MAX_NUM_PDF_PAGES                = 999;//IAZ IR RRUL061611265 06/16/11 Add
	public final static String USE_BUY_RATE						= "FXBUY";
	public final static String USE_SELL_RATE					= "FXSELL";
	public final static String USE_MID_RATE						= "FXMID";
	public final static String PMT_METHOD						= "PAYMENT_METHOD";
	public final static String SHIPMENT_TAB           = "ShipmentTab";

	public final static String BUTTON_APPROVEDISCOUNTAUTHORIZE  = "ApproveDiscountAuthorize";
	public final static String BUTTON_PROXY_APPROVEDISCOUNTAUTHORIZE  = "ProxyApproveDiscountAuthorize"; //AAlubala - CR711 Rel8.0 02/07/2011 - Proxy Authorize Button
	public final static String BUTTON_BUYERSEARCH				= "BuyerSearch";
	// jgadela R8.3 CR 501 begin
	public final static String BUTTON_APPROVE_PENDING_REF_DATA	= "ApprovePendingRefData";
	public final static String BUTTON_REJECT_PENDING_REF_DATA	= "RejectPendingRefData";
	// jgadela R8.3 CR 501 end

	// NSX CR-574 09/21/10 - Begin -
	public final static String TRAN_ACTION				        = "Action";
	public final static String TRAN_ACTION_CREATE		        = "Create";
	public final static String TRAN_ACTION_SAVE		            = "Save";
	public final static String TRAN_ACTION_EDIT		            = "Edit";   // NSX - IR# HNUL021063380
	public final static String TRAN_ACTION_VERIFY               = "Verify";
	public final static String TRAN_ACTION_AUTHORIZE            = "Authorize";
	public final static String TRAN_ACTION_BANK                 = "Bank";
	public final static String TRAN_ACTION_AUTOMATED_RELEASE    = "AutomatedRelease";  //NSX IR# HAUL022462166  Rel. 6.1.0
	public final static String TRAN_ACTION_CONFIRM_FXDEAL       = "ConfirmFXDeal";     //NSX CR-640/581 Rel 7.1.0  09/26/11
	public final static String TRAN_ACTION_SEND_FOR_AUTH        = "Send_Authorize";      // Nar CR 821 Rel 8.2.0.0 05/21/13
	public final static String TRAN_ACTION_REPAIR               = "Repair";     // Nar CR 821 Rel 8.2.0.0 05/21/13

		// NSX CR-574 09/21/10 - End -
	//MDB CR-564 Rel6.1 12/29/10 Begin
	public final static String TRAN_ACTION_CONFIRM              = "Confirm";
	public final static String TRAN_ACTION_REJECT               = "Reject";
	//MDB CR-564 Rel6.1 12/29/10 End
	//Rel 951 CR 1160
	public final static String TRAN_ACTION_UPDATE_RECEIVED = "UPD_RCV";
	// Activation Status
	public final static String ACTIVE   = "ACTIVE";
	public final static String INACTIVE = "INACTIVE";

	// Number of Milliseconds in a day
	public final static long   MILLISECONDS_IN_DAY = 86400000;

	// Default Number of Days for Document Presentation
	public final static long   DEFAULT_PRESENTATION_DAYS = 21;

	// Security Profile Types
	public final static String ADMIN     = "ADMIN";
	public final static String NON_ADMIN = "NON_ADMIN";

	// Authentication Method Types
	public final static String AUTH_PASSWORD    = "PASSWORD";
	public final static String AUTH_CERTIFICATE = "CERTIFICATE";
	public final static String AUTH_SSO  = "SSO";
	public final static String AUTH_PERUSER    = "PERUSER";
	public final static String AUTH_2FA = "2FA";
	public final static String AUTH_2FA_SUBTYPE = "2FASUBTYPE";
	public final static String AUTH_2FA_SUBTYPE_OTP = "O";
	public final static String AUTH_2FA_SUBTYPE_SIG = "S";
	//Ravindra - 10/01/2011 - CR-541 - Start
	/*
	 *  CMA specific Authentication Method
	 */
	public final static String AUTH_AES256  = "AES256";
	//Ravindra - 10/01/2011 - CR-541 - End
	//BSL 08/19/11 CR-663 Begin
	public final static String AUTH_REGISTERED_SSO    = "REGISTERED_SSO";
	public final static String AUTH_SSO_REGISTRATION  = "SSO_REGISTRATION";
    //BSL 08/19/11 CR-663 End
	//cquinton 4/29/2013 Rel PR ir#16473 new auth method for smartcard over sso
	//note that this is converted to actual CERTICATE authentication so this is not stored in db
	public final static String AUTH_CERTIFICATE_SSO = "CERTIFICATE_SSO";
//	public final static String CAAS_AUTHLEVEL_HARDWARE_PINPAD_TOKEN_OTP = "40";

	//Final Authroize Authentication Pages
	public final static String AUTHENT_PAGE_DUMMY    = "EnterRecertificationPassword";

	public final static String REPORTING_TYPE_CASH_MGMT    = "C";
	public final static String REPORTING_TYPE_TRADE_SRV    = "T";
	public final static String BUTTON_ADD_4_MORE_ACCOUNTS = "Add4MoreAccounts";
	public final static String BUTTON_ADD_ACCOUNTS = "AddAccounts";

	// Organization ID for global organization (passed in URL to identify the user as
	// being owned by the global organization)
	public final static String GLOBAL_ORG_ID = "ASP";

	// Ownership Levels
	public final static String OWNER_BANK      = "BANK";
	public final static String OWNER_BOG       = "BOG";
	public final static String OWNER_CORPORATE = "CORPORATE";
	public final static String OWNER_GLOBAL    = "GLOBAL";

	// Ownership Types
	public final static String OWNER_TYPE_NON_ADMIN = "NON_ADMIN";
	public final static String OWNER_TYPE_ADMIN = "ADMIN";

	// Account Ownership Types
	public final static String CORPORATE_ORG = "CORPORATE_ORG";
	public final static String PARTY       = "PARTY";

	// Indicator Constants
	public final static String INDICATOR_YES = "Y";
	public final static String INDICATOR_NO  = "N";
	public final static String INDICATOR_A   = "A";
	public final static String INDICATOR_X   = "X";
	public final static String INDICATOR_T   = "T";		//RKAZI CR709 Rel 8.2 01/25/2013
	public final static String INDICATOR_ON  = "on";
	public final static String INDICATOR_CANCEL  = "C";
	public final static String INDICATOR_MULTIPLE  = "M";
	public final static String CONF_IND_FRM_TMPLT = "T";	//IAZ CR-586 08/18/10 Add

	// Return codes for Routing
	public final static String ROUTE_SELECTION_ERROR = "S";
	public final static String ROUTE_GENERAL_ERROR = "E";
	public final static String ROUTE_SUCCESS = "C";

	// Text Resources Constants
	public final static String TEXT_BUNDLE = "TextResources";

	// path for images
	// this is used for pdf creation
	public final static String IMAGES_PATH = "/portal/themes/";
	// FXRates - Multiply/Divide Indicator
	public final static String MULTIPLY = "MULTIPLY";
	public final static String DIVIDE = "DIVIDE";

	// Constants for multi-part pages
	public final static String PART_ONE = "Part1";
	public final static String PART_TWO = "Part2";
	public final static String PART_THREE = "Part3";
	public final static String PART_FOUR = "Part4";
	public final static String READONLY_PART = "ReadOnlyPart";

	// Tells how new instrument/template is being created
	public final static String FROM_INSTR     = "Instr";
	public final static String FROM_TEMPL     = "Templ";
	public final static String FROM_BLANK     = "Blank";
	public final static String TRANSFER_ELC   = "TRANSFER";

	// Transaction Modes
	public final static String NEW_INSTR      = "CREATE_NEW_INSTRUMENT";
	public final static String NEW_TEMPLATE   = "CREATE_NEW_TEMPLATE";
	public final static String EXISTING_INSTR = "USE_EXISTING_INSTR";

	// Default cancel action for the entire create transaction area
	public final static String TRANSACTION_CANCEL_ACTION = "cleanUpDoc";

	// Standard document names for documents that are placed in cache
	public final static String PHRASE_LIST_DOC = "PhraseLists";

	// Session key for value representing where to return to upon instrument close.
	public final static String AUTHORIZATION_ERRORS_CLOSE_ACTION = "AuthorizationErrorsCloseAction";
	public final static String INSTRUMENT_SUMMARY_CLOSE_ACTION   = "InstrumentSummaryCloseAction";
	public final static String INSTRUMENT_CLOSE_ACTION           = "InstrumentCloseAction";
	public final static String INSTRUMENT_SUMMARY_OID            = "InstrumentOid";
	public final static String INSTRUMENT_SUMMARY                = "InstrumentSummary";

	// Session key for value representing where to return to upon message close.
	public final static String MAIL_MESSAGE_CLOSE_RETURN_PAGE = "fromPage";

	// Session key for value representing which search to use
	public final static String INSTRUMENT_HISTORY_SEARCH_TYPE = "InstrumentHistorySearchType";
	public final static String INSTRUMENT_SEARCH_SEARCH_TYPE  = "InstrumentSearchSearchType";

	// Error.jsp related information
	public final static String AUTHORIZATION_ERRORS_HTML = "Authorization_Errors_Html";

	// Instrument search options
	public final static String ADVANCED = "A";
	public final static String SIMPLE   = "S";
	public final static String SEARCH_ALL_INSTRUMENTS = "ALL_INSTRUMENTS";
	public final static String SEARCH_EXP_DLC_ACTIVE  = "EXPORT_DLC_ACTIVE";
	public final static String SEARCH_FOR_NEW_TRANSACTION = "NEW_TRANSACTION";
	public final static String SEARCH_FOR_COPY_INSTRUMENT = "NEW_INSTRUMENT";
	public final static String SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES = "ALL_INSTRUMENTS_MESSAGES";
	public final static String IMPORTS_ONLY = "IMPORTS_ONLY";

	//TLE - 01/30/07 - IR#AYUG101742823 - Add Begin
	public final static String ALL_EXPORT_DLC = "ALL_EXP_DLC";
	//TLE - 01/30/07 - IR#AYUG101742823 - Add End


	// Chandrakanth CR 451 CM 10/21/2008 End
	// Fake instrument types used for the Standby as Guarantee logic.
	public final static String SIMPLE_SLC     = "SimpleSLC";
	public final static String DETAILED_SLC   = "DetailedSLC";
	public final static String SLC_IS_GUA     = "SLC-is-GUA";

	// Chandrakanth CR 451 CM 11/10/2008 Begin

	public final static String BANK_COUNTRY_CODE = "COUNTRY_ISO3116_3";
	// Chandrakanth CR 451 CM 11/10/2008 End
	public final static String REFINANCE_RATE_TYPE = "REFINANCE_RATE_TYPE"; //Suresh CR-713 Rel 8.0 Add
	// Chandrakanth IR ACUI120149991 02/12/2008 Begin
	public final static String ADD_MODIFY_PANEL_LEVEL_BUTTON = "AddModifyPanelLevel";
	// Chandrakanth IR ACUI120149991 02/12/2008 End
	//Instrument Statuses
	public final static String INSTR_STATUS_ACTIVE               = "ACT";
	public final static String INSTR_STATUS_CLOSED               = "CLO";
	public final static String INSTR_STATUS_PENDING              = "PND";
	public final static String INSTR_STATUS_EXPIRED              = "EXP";
	public final static String INSTR_STATUS_CANCELLED            = "CAN";
	public final static String INSTR_STATUS_DEACTIVATED          = "DEA";
	public final static String INSTR_STATUS_LIQUIDATE_ALL        = "LIQ";
	public final static String INSTR_STATUS_LIQUIDATE_BANK_SIDE  = "LQB";
	public final static String INSTR_STATUS_LIQUIDATE_CUST_SIDE  = "LQC";
	public final static String INSTR_STATUS_DELETED              = "DEL";
	//Narayan-IR RAUL011841908 Begin(CR 596 changes)
	public final static String INSTR_STATUS_REJECTED_PAYMENT     = "RPI";
	public final static String INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT    = "PRP";
	//Narayan-IR RAUL011841908 End

	//instrument groups
    public final static String INSTR_GROUP__ALL = "ALL";
	public final static String INSTR_GROUP__TRADE = "TRD";
    public final static String INSTR_GROUP__PAYMENT = "PMT";
    public final static String INSTR_GROUP__DIRECT_DEBIT = "DD";
    public final static String INSTR_GROUP__RECEIVABLES = "RCV";

	// Instrument dual authorization requirement
	public final static String DUAL_AUTH_REQ_ONE_USER      = "N";
	public final static String DUAL_AUTH_REQ_TWO_USERS       = "Y";
	public final static String DUAL_AUTH_REQ_TWO_WORKGROUPS        = "G";
	public final static String DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS   = "I";
	public final static String DUAL_AUTH_REQ_PANEL_AUTH   = "P";			//IAZ CR-511 11/09/09 Add
	public final static String DUAL_AUTH_NOT_REQ  =	"R";				//Leelavathi CR-710 Rel - 8.0.0 23/11/2011 ADD
	public final static String DUAL_AUTH_REQ_CORPORATE = "C";//BSL CR710 03/01/2012 Rel8.0 Add
	//IAZ CM-451 12/01/08 Begin
	public final static String PANEL_USER_AUTH_OWN_TRAN			 = "Y";
	//IAZ CM-451 12/01/08 End
	//Leelavathi CR-710 Rel - 8.0.0.0 03/11/2011 Begin
	public final static String INVOICES_NOT_UPLOAD_FILE   = "N";
	public final static String INVOICES_UPLOAD_FILE         = "Y";
	//Leelavathi CR-710 Rel - 8.0.0.0 03/11/2011 End
	//Leelavathi CR-710 Rel - 8.0.0.0 23/11/2011 Begin
	public final static String INVOICES_NOT_GROUPED_TPR   = "N";
	public final static String INVOICES_GROUPED_TPR         = "Y";
	//Leelavathi CR-710 Rel - 8.0.0.0 23/11/2011 End
	//Leelavathi CR-710 Rel - 8.0.0.0 24/11/2011 Begin
	public final static String INVOICE_UPLOAD_NOT_RESTRICTED_DIRECTORY   = "N";
	public final static String INVOICE_UPLOAD_RESTRICTED_DIRECTORY         = "Y";
	//Leelavathi CR-710 Rel - 8.0.0.0 24/11/2011 End
	//Leelavathi CR-707 Rel - 8.0.0.0 01/02/2012 Begin
	public final static String PURCHASE_ORDER_STRUCTURED_FORMAT   = "S";
	public final static String PURCHASE_ORDER_DESCRIPTION_TEXT   = "T";
	public final static String PURCHASE_ORDER_NOT_RESTRICTED_DIRECTORY = "N";
	public final static String PURCHASE_ORDER_RESTRICTED_DIRECTORY = "Y";
	//Leelavathi CR-707 Rel - 8.0.0.0 01/02/2012 End
	//Leelavathi CR-710 Rel - 8.0.0.0 29/11/2011 Begin
	public final static String INTEREST_COLLECTED_FINANCE_AMOUNT = "I";
	public final static String DISCOUNT_NETTED_FINANCE_AMOUNT = "D";
	public final static String STRAIGHT_DISCOUNT = "S";
	public final static String DISCOUNT_YIELD = "D";
	// Leelavathi CR-710 Rel 8.0.0 29/11/2011 End
	public final static String CHANGE_TYPE_CHANGE     = "C";
	public final static String CHANGE_TYPE_DELETE     = "D";
	public final static String CHANGE_TYPE_UPDATE     = "U";
	public final static String CHANGE_TYPE_DISPUTE    = "DISPUTE";
	public final static String CHANGE_TYPE_UNDISPUTE  = "UNDISPUTE";
	public final static String CHANGE_TYPE_CLOSE      = "CLOSE";
	public final static String CHANGE_TYPE_MATCH      = "MATCH";
	public final static String CHANGE_TYPE_FINANCE    = "FINANCE";

	public final static String PAYMENT_SOURCE_TYPE_BPY = "BPY";
	public final static String PAYMENT_SOURCE_TYPE_CCD = "CCD";
	public final static String PAYMENT_SOURCE_TYPE_DIR = "DIR";
	public final static String PAYMENT_SOURCE_TYPE_HPA = "HPA";
	public final static String PAYMENT_SOURCE_TYPE_LBX = "LBX";
	public final static String PAYMENT_SOURCE_TYPE_RTG = "RTG";
	public final static String PAYMENT_SOURCE_TYPE_SMT = "SMT";
	public final static String PAYMENT_SOURCE_TYPE_SWF = "SWF";


	///////////// MIDDLEWARE SECTION OF CONSTANTS BEGIN ///////////////////

	// Outgoing Interface Queue Status Constants
	public final static String OUTGOING_STATUS_STARTED    = "S";
	public final static String OUTGOING_STATUS_INITIALIZED= "I";
	public final static String OUTGOING_STATUS_PACKAGED   = "P";
	public final static String OUTGOING_STATUS_COMPLETED  = "C";
	public final static String OUTGOING_STATUS_ERROR    = "E";

	// Incoming Interface Queue Status Constants
	public final static String INCOMING_STATUS_RECEIVED   = "R";
	public final static String INCOMING_STATUS_INITIALIZED= "I";
	public final static String INCOMING_STATUS_UNPACKAGED = "U";
	public final static String INCOMING_STATUS_COMPLETED  = "C";
	public final static String INCOMING_STATUS_ERROR    = "E";

	// Queue Confirmation Status Constants
	public final static String CONFIRM_STATUS_PENDING = "P";
	public final static String CONFIRM_STATUS_COMPLETED = "C";
	public final static String CONFIRM_STATUS_ERROR   = "E";

	// Processing Status Constants:
	public static final String PROCESSING_STATUS_FAIL = "MSGFAIL";
	public static final String PROCESSING_STATUS_OKAY = "MSGOKAY";


	public static final String MSG_SUBTYPE_DISPUTED   		= "DISP";
	public static final String MSG_SUBTYPE_UNDISPUTED   		= "UNDP";
	public static final String MSG_SUBTYPE_CLOSED   		= "CLOS";
	public static final String MSG_SUBTYPE_FINANCEREQUESTED   	= "FINR";
	public static final String MSG_SUBTYPE_PPY   	= "PPY";
	//rbhaduri - 16th Oct 08 - CR-374 - Add End

	//Vshah 12/11/2008 INVSTO UnPackager
	public static final String MSG_SUBTYPE_CREDITNOTE   		= "CRED";
	public static final String MSG_SUBTYPE_CREATEUSANCE   	= "CUS";
	public static final String MSG_SUBTYPE_AUTOMATCH   		= "AUTO";
	public static final String MSG_SUBTYPE_MANUALMATCH   	= "MMAT";
	public static final String MSG_SUBTYPE_APPLIEDPAYMENT   	= "PAY";
	public static final String MSG_SUBTYPE_LIQUIDATE   		= "LIQ";
	//Vshah 12/11/2008 INVSTO UnPackager


	// Collection of destination id's:
	public static final String DESTINATION_ID_PROPONIX   = "PRO";
	public static final String DESTINATION_ID_ANZ        = "ANZ";
	public static final String DESTINATION_ID_OTL        = "OTL";
	public static final String DESTINATION_ID_TPL        = "TPL";
	public static final String DESTINATION_ID_TRADEBEAM  = "LCP";
	public static final String DESTINATION_ID_GXS        = "GXS";
	public static final String DESTINATION_ID_FLA        = "FLA"; //Nar CR-694A 05-MAY-2014 Rel 9.0

	// Collection of sender id's:
	public static final String SENDER_ID_PROPONIX        = "PRO";
	public static final String SENDER_ID_ANZ             = "ANZ";
	public static final String SENDER_ID_OTL             = "OTL";
	//public static final String SENDER_ID_TPL           = "TPL";    // NSX CR-542 03/04/10
	public static final String SENDER_ID                 = getPropertyValue("TradePortal","senderServerType","TPL"); // NSX CR-542 03/04/10
	public static final String SENDER_ID_UBC             = "UBC";


	//MailMessage Constants
	public static final String TRANSACTION_IS_PROCESSED_BY_BANK = "Y";

	//Transaction constants
	public final static String SHOW_ON_NOTIFICATIONS_TAB_Y     = "Y";

	//DocumentImage Form Types
	public static final String DOC_IMG_FORM_TYPE_SWIFT700 = "E700S";
	public static final String DOC_IMG_FORM_TYPE_SWIFT710 = "E710S";
	public static final String DOC_IMG_FORM_TYPE_SWIFT720 = "E720S";
	// [BEGIN] IR-YYUH032255414 - jkok
	public static final String DOC_IMG_FORM_TYPE_PORTAL_ATTACHED = "PORTAL";
	public static final String DOC_IMG_FORM_TYPE_OTL_ATTACHED = "USER";
	// [BEGIN] IR-YYUH032255414 - jkok

	public static final String TPL_MESSAGE_UNPACKAGING_ERROR = "Unpackaging Error";
	public static final String DRAFTS_REQUIRED            = "Y";

	///////////// MIDDLEWARE SECTION OF CONSTANTS END /////////////////////

	// Periodic Task Types
	public final static String DAILY_EMAIL_TASK_TYPE = "DAILY_EMAIL";
	public final static String HISTORY_PURGE_TASK_TYPE = "HISTORY_PURGE";
	public final static String BROWSE_CM_MQQUEUE_TASK_TYPE = "BROWSE_CM_REQUEST_QUEUE";
	public final static String ARC_GEN_INC_DATA_MESSAGE_TASK_TYPE = "GEN_INC_DATA_MSG"; // CR 587
	public final static String ARC_EOD_TRANSACTION_MESSAGE_TASK_TYPE = "EOD_TRANS_MSG"; // CR 587
	public final static String SUPPLIER_PORTAL_EMAIL_TASK_TYPE = "SP_EMAIL";//CR  776
	
	public final static String EOD_PURGE_CRNOTES_INVOICES="EOD_PURGE_CRNOTES_INVOICES"; //CR914A-R9.2
	
	//MEer Rel 9.3 CR-1006 STARTS HERE
	public final static String PINA_EMAIL_TASK_TYPE = "PAYABLES_INV_APPROVAL_EMAIL"; 
	public final static String PCNA_EMAIL_TASK_TYPE = "PAYABLES_CN_APPROVAL_EMAIL"; 
	public final static String RINA_EMAIL_TASK_TYPE = "RECEIVABLES_INV_APPROVAL_EMAIL"; 
	//MEer Rel 9.3 CR-1006 ENDS HERE

	// Queue Message Type Constants
	public final static String MSG_TRANSACTION = "TPL";

	// Discrepancy Instructions
	public final static String EXP_SEND_APPR    = "DISA";
	public final static String EXP_SWIFT_AND_HOLD = "DISW";
	public final static String EXP_SWIFT_AND_SEND = "DISN";
	public final static String EXP_OTHER    = "OTHR";
	public final static String IMP_ACC_NO_DCR     = "APVP";
	public final static String IMP_REJ      = "DREJ";
	public final static String IMP_OTHER    = "OTHR";

	// Guarantee Issuing Bank
	public final static String GUAR_ISSUE_APPLICANTS_BANK = "APB";
	public final static String GUAR_ISSUE_OVERSEAS_BANK = "OVB";

	// Guarantee Date Types
	public static final String DATE_OF_ISSUE            = "ID";
	public static final String OTHER_VALID_FROM_DATE    = "OD";
	public static final String VALIDITY_DATE            = "VD";
	public static final String OTHER_EXPIRY_DATE        = "OE";
	//Krishna IR-AVUF122339652 Begin
	public static final String CURRENT_VALIDITY_FROM_DATE     = "VC";
	public static final String CURRENT_VALIDITY_TO_DATE       = "CD";
	//Krishna IR-AVUF122339652 End

	// Guarantee "Deliver To" Types
	public static final String DELIVER_TO_APPLICANT     = "APP";
	public static final String DELIVER_TO_BENEFICIARY   = "BEN";
	public static final String DELIVER_TO_AGENT         = "AGT";
	public static final String DELIVER_TO_OTHER         = "OTH";

	// Guarantee "Deliver By" Types
	public static final String DELIVER_BY_TELEX         = "T";
	public static final String DELIVER_BY_SWIFT         = "S";
	public static final String DELIVER_BY_REG_MAIL      = "R";
	public static final String DELIVER_BY_MAIL          = "M";
	public static final String DELIVER_BY_COURIER       = "O";


	//<Papia Dastidar IR#BKUF080335338 & BIUF080336121 25thAug 2005>
	//Modified the following constant name as per Weian's suggestion
	//from ACCOUNTS_REC to ACCOUNTS_RECEIVABLE_UPDATE
	public final static String ACCOUNTS_RECEIVABLE_UPDATE = "ARU";
	//Added the following constant
	public final static String ACCOUNTS_RECEIVABLE_PAYMENT = "ARP";
	//</Papia Dastidar IR#BKUF080335338 & BIUF080336121 25thAug 2005>

	public final static String CORRESPONDENCE = "COR";
	//rkrishna CR 375-D ATP-Response 07/26/2007 Begin
	public final static String ATP_INVOICE_APPROVED    = "APVP";
	public final static String ATP_INVOICE_REFUSED    = "DREJ";
	public final static String ATP_OTHER          ="OTHR";
	//rkrishna CR 375-D ATP-Response 07/26/2007 End
	//  Pratiksha CR-434 09/24/2008 ADD BEGIN
	public final static String RECEIVABLES_UPDATE 			= "UPD";
	public final static String APPLY_RECEIVABLES_PAYMENT 	= "PAR";
	public final static String NEW_RECEIVABLES				= "ARM";
	public final static String CHANGE_RECEIVABLES 			= "CHM";
	
	public final static String CROSS_BORDER_FIN_TRANSFER    = "CBFT"; //IAZ CR-507 12/18/2009 Add
	public final static String PAYMENT_METHOD_CBFT          = "CBFT"; //IAZ CR-507 12/18/2009 Add
	public final static String PAYMENT_METHOD_ACH_GIRO      = "ACH";  //IAZ CR-507 12/18/2009 Add
	public final static String PAYMENT_METHOD_RTGS          = "RTGS"; //IAZ CR-507 12/18/2009 Add
	public final static String PAYMENT_METHOD_BKT           = "BKT";  //IAZ CR-507 12/18/2009 Add
	public final static String PAYMENT_METHOD_BCHK          = "BCHK"; //IAZ CR-507 01/26/2010 Add
	public final static String PAYMENT_METHOD_CCHK          = "CCHK"; //IAZ CR-507 01/26/2010 Add

	public final static String PAYMENT_METHOD_TYPE_ELLECTR  = "Electronic"; 	//IAZ CR-507 SRUK012081362 01/21/10 CHG
	public final static String PAYMENT_METHOD_TYPE_INTERNTL = "International"; 	//IAZ CR-507 SRUK012081362 01/21/10 CHG
	public final static String PAYMENT_METHOD_TYPE_CHEQUE   = "Cheque"; 		//IAZ CR-507 SRUK012081362 01/21/10 CHG
	public final static String PAYMENT_METHOD_TYPE_COLLECTN = "Collection"; 	//IAZ CR-507 SRUK012081362 01/21/10 CHG

	// Instrument Languages of Issue
	public final static String INSTRUMENT_LANGUAGE_ENGLISH = "01";
	public final static String INSTRUMENT_LANGUAGE_FRENCH  = "02";

	// Instrument Purge Types
	public final static String PURGE_DELETE_INSTRUMENT = "PURGE_ALL";
	public final static String PURGE_KEEP_INSTRUMENT   = "RETAIN";

	// Transaction statuses
	/**
	 * @deprecated Use {@link TransactionStatus#AUTHORIZED} instead
	 */
	public final static String TRANS_STATUS_AUTHORIZED           = TransactionStatus.AUTHORIZED;
	/**
	 * @deprecated Use {@link TransactionStatus#AUTHORIZE_FAILED} instead
	 */
	public final static String TRANS_STATUS_AUTHORIZE_FAILED     = TransactionStatus.AUTHORIZE_FAILED;
	/**
	 * @deprecated Use {@link TransactionStatus#AUTHORIZE_PENDING} instead
	 */
	public final static String TRANS_STATUS_AUTHORIZE_PENDING    = TransactionStatus.AUTHORIZE_PENDING;
	/**
	 * @deprecated Use {@link TransactionStatus#CANCELLED_BY_BANK} instead
	 */
	public final static String TRANS_STATUS_CANCELLED_BY_BANK    = TransactionStatus.CANCELLED_BY_BANK;
	/**
	 * @deprecated Use {@link TransactionStatus#DELETED} instead
	 */
	public final static String TRANS_STATUS_DELETED              = TransactionStatus.DELETED;
	/**
	 * @deprecated Use {@link TransactionStatus#PROCESSED_BY_BANK} instead
	 */
	public final static String TRANS_STATUS_PROCESSED_BY_BANK    = TransactionStatus.PROCESSED_BY_BANK;
	/**
	 * @deprecated Use {@link TransactionStatus#PARTIALLY_AUTHORIZED} instead
	 */
	public final static String TRANS_STATUS_PARTIALLY_AUTHORIZED = TransactionStatus.PARTIALLY_AUTHORIZED;
	/**
	 * @deprecated Use {@link TransactionStatus#READY_TO_AUTHORIZE} instead
	 */
	public final static String TRANS_STATUS_READY_TO_AUTHORIZE   = TransactionStatus.READY_TO_AUTHORIZE;
	/**
	 * @deprecated Use {@link TransactionStatus#STARTED} instead
	 */
	public final static String TRANS_STATUS_STARTED              = TransactionStatus.STARTED;
	/**
	 * @deprecated Use {@link TransactionStatus#REJECTED_BY_BANK} instead
	 */
	public final static String TRANS_STATUS_REJECTED_BY_BANK     = TransactionStatus.REJECTED_BY_BANK;
	/**
	 * @deprecated Use {@link TransactionStatus#FVD_AUTHORIZED} instead
	 */
	public final static String TRANS_STATUS_FVD_AUTHORIZED       = TransactionStatus.FVD_AUTHORIZED; //VS CR 609 11/23/10
	/**
	 * @deprecated Use {@link TransactionStatus#READY_TO_CHECK} instead
	 */
	public final static String TRANS_STATUS_READY_TO_CHECK       = TransactionStatus.READY_TO_CHECK; // Nar CR 821 Rel 8.2.0.0 05/21/13
	/**
	 * @deprecated Use {@link TransactionStatus#REPAIR} instead
	 */
	public final static String TRANS_STATUS_REPAIR               = TransactionStatus.REPAIR; // Nar CR 821 Rel 8.2.0.0 05/21/13
	/**
	 * @deprecated Use {@link TransactionStatus#REPORTING_CODES_REQUIRED} instead
	 */
	public final static String TRANS_STATUS_REPORTING_CODES_REQUIRED = TransactionStatus.REPORTING_CODES_REQUIRED; // CR 1001 Rel9.4 08/29/15

	//MDB CR-564 R6.1 Begin
	/**
	 * @deprecated Use {@link TransactionStatus#VERIFIED} instead
	 */
	public final static String TRANS_STATUS_VERIFIED		             = TransactionStatus.VERIFIED;
	/**
	 * @deprecated Use {@link TransactionStatus#VERIFIED_PENDING_FX} instead
	 */
	public final static String TRANS_STATUS_VERIFIED_PENDING_FX          = TransactionStatus.VERIFIED_PENDING_FX;
	/**
	 * @deprecated Use {@link TransactionStatus#FILE_UPLOAD_REJECT} instead
	 */
	public final static String TRANS_STATUS_FILE_UPLOAD_REJECT           = TransactionStatus.FILE_UPLOAD_REJECT;
	/**
	 * @deprecated Use {@link TransactionStatus#VERIFIED_AWAITING_APPROVAL} instead
	 */
	public final static String TRANS_STATUS_VERIFIED_AWAITING_APPROVAL   = TransactionStatus.VERIFIED_AWAITING_APPROVAL;
	//MDB CR-564 R6.1 End
	//DK CR-640 Rel7.1 BEGINS
	/**
	 * @deprecated Use {@link TransactionStatus#FX_THRESH_EXCEEDED} instead
	 */
	public final static String TRANS_STATUS_FX_THRESH_EXCEEDED = TransactionStatus.FX_THRESH_EXCEEDED;
	/**
	 * @deprecated Use {@link TransactionStatus#AUTH_PEND_MARKET_RATE} instead
	 */
	public final static String TRANS_STATUS_AUTH_PEND_MARKET_RATE = TransactionStatus.AUTH_PEND_MARKET_RATE;
	//DK CR-640 Rel7.1 ENDS

	//  Pratiksha CR-434 09/24/2008 ADD BEGIN
	public final static String TRANS_STATUS_MATCH_REQUIRED     = "MATCH_REQUIRED";
	public final static String TRANS_STATUS_DISC_APPROVAL      = "DISC_APPROVAL";
	public final static String TRANS_STATUS_PAID     		   = "PAID";
	public final static String TRANS_STATUS_IN_PROGRESS        = "IN_PROGRESS";
	public final static String TRANS_STATUS_DISC_APPR_FAILED   = "DISC_APPR_FAILED";
	//IAZ 12/01/08 Begin
	/**
	 * @deprecated Use {@link TransactionStatus#TRANS_STATUS_REQ_AUTHENTICTN} instead
	 */
	public final static String TRANS_STATUS_REQ_AUTHENTICTN    = TransactionStatus.TRANS_STATUS_REQ_AUTHENTICTN;
	public final static String TRANS_STATUS_FAILED_AUTHENTN    = "FAILED_AUTHENTN";
	//IAZ 12/01/05 End
	//CR-596 11/08/2010 Begin
	
	/**
	 * @deprecated Use {@link TransactionStatus#REJECTED_PAYMENT} instead
	 */
	public final static String TRANS_STATUS_REJECTED_PAYMENT    = TransactionStatus.REJECTED_PAYMENT;
	/**
	 * @deprecated Use {@link TransactionStatus#PARTIALLY_REJECTED_PAYMENT} instead
	 */
	public final static String TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT    = TransactionStatus.PARTIALLY_REJECTED_PAYMENT;
	

	//Narayan-IR RAUL011841908 Begin
	//public final static String INSTRUMENT_STATUS_REJECTED_PAYMENT    = "REJECTED_PAYMENT";
	//public final static String INSTRUMENT_STATUS_PARTIALLY_REJECTED_PAYMENT    = "PARTIALLY_REJECTED_PAYMENT";
	//Narayan-IR RAUL011841908End
	public final static String INSTRUMENT_STATUS_FILE_UPLOAD_REJECTED = "FUR"; //MDB CR-564 Rel6.1 1/19/11 IR# RSUL011855108

	public final static String BENEFICIARY_PAYMENT_STATUS    = "BENEFICIARY_PAYMENT_STATUS";
	public final static String BENEFICIARY_PAYMENT_STATUS_COMPLETE    = "COMPLETE";
	public final static String BENEFICIARY_PAYMENT_STATUS_REJECTED    = "REJECTED";
	public final static String BENEFICIARY_PAYMENT_STATUS_PROCESSED_BY_BANK    = "PROCESSED_BY_BANK";
	public final static String BENEFICIARY_PAYMENT_STATUS_PENDING    = "PENDING";
	public final static String BENEFICIARY_PAYMENT_STATUS_REJECTED_UNPAID    = "REJECTED_UNPAID";
	//CR-596 11/08/2010 End

	//Invoice Status Types
	public final static String INVOICE_STATUS_CLOSED_AND_FINANCED  = "CFN";
	public final static String INVOICE_STATUS_ASSIGNED        = "ASN";
	public final static String INVOICE_STATUS_ASSIGN_AND_FINANCE   = "ASF";
	public final static String INVOICE_STATUS_CLOSED     = "CLO";
	public final static String INVOICE_STATUS_FINANCE_REQUESTED    = "FNR";
	public final static String INVOICE_STATUS_OPEN     		   = "OPN";
	public final static String INVOICE_STATUS_DISPUTED    	   = "DSP";

	public final static String INVOICE_STATUS_UNDISPUTED       = "UNDP";

	// Invoice Search Status
	public final static String INVOICE_SEARCH_STATUS_UNDISPUTE = "UNDISPUTE";
	public final static String INVOICE_SEARCH_STATUS_OPEN      = "OPEN";
	public final static String INVOICE_SEARCH_STATUS_DISPUTE      = "DISPUTE";//Vshah - IR#PNUI121736772
	//Matching Status Types
	public final static String MATCHING_STATUS_PARTIALLY_MATCHED   = "PMD"; //Jyoti - IR#5155
	public final static String MATCHING_STATUS_PROPOSED     	   = "PPD";
	public final static String MATCHING_STATUS_AUTO_MATCHED    	   = "AMD";
	public final static String MATCHING_STATUS_MANUAL_MATCH        = "MMD";
	public final static String MATCHING_STATUS_NOT_MATCHED     	   = "NMD";

	//Payment/Remit Source Types
	public final static String PAY_SOURCE_STATEMENT     		   = "SMT";
	public final static String PAY_SOURCE_BILL_PAY    	   		   = "BPY";
	public final static String PAY_SOURCE_LOCK_BOX			   = "LBX";
	public final static String PAY_SOURCE_CREDIT_CARD     		   = "CCD";
	public final static String PAY_SOURCE_DIRECT_ENTRY     		   = "DIR";
	public final static String PAY_SOURCE_REAL_TIME_GROSS_SETTLEMENT = "RTG";
	public final static String PAY_SOURCE_HPA_DATE_CAPTURE		   = "HPA";
	public final static String PAY_SOURCE_SWIFT_MT_PAY     		   = "SWF";
	//  Pratiksha CR-434 09/24/2008 ADD END

	//rbhaduri CR-434 16th Oct 08 - Add Begin
	//Finance Status Type
	public final static String INVOICE_APPROVED_FOR_FINANCE    = "AFF";
	public final static String INVOICE_EXTERNAL_FINANCING    	= "EFS";
	public final static String INVOICE_FINANCED    		= "FIN";
	public final static String INVOICE_REPAID_LIQUIDATE    	= "LIQ";
	public final static String INVOICE_NOT_FINANCED    	= "NFN";
	//rbhaduri CR-434 16th Oct 08 - Add End

	// Party Types
	public final static String PARTY_TYPE_BANK          = "BANK";
	public final static String PARTY_TYPE_CORP          = "CORP";

	// Phrase categories
	public final static String PHRASE_CAT_MISC          = "MISC";
	public final static String PHRASE_CAT_DOCREQD       = "REQDOC";
	public final static String PHRASE_CAT_GOODS         = "GOODS";
	public final static String PHRASE_CAT_TRANSPORT     = "TRANSP";
	public final static String PHRASE_CAT_ADDL_COND     = "CONDIT";
	public final static String PHRASE_CAT_SPCL_INST     = "SPECINT";
	public final static String PHRASE_CAT_PMT_INST      = "PMTINST";
	public final static String PHRASE_CAT_GUAR_CUST     = "GUARCUST";
	public final static String PHRASE_CAT_GUAR_BANK     = "GUARBANK";
	public final static String PHRASE_CAT_PRES_DOC      = "PRESDOC";
	public final static String PHRASE_CAT_COLL_INSTR    = "COLLINSTR";
	public final static String PHRASE_CAT_NEED          = "NEED";
	public final static String PHRASE_CAT_COLL_CHARGE   = "COLLCHARGE";
	public final static String PHRASE_CAT_COLL_INTEREST = "COLLINTEREST";
	public final static String PHRASE_CAT_SETTLEMENT    = "SETTLEMENT";
	public final static String PHRASE_CAT_DCR_INST      = "DCRINST";
	public final static String PHRASE_CAT_POTEXT        = "POTEXT";

	// Phrase types


	public final static String PHRASE_MODIFIABLE        = "MODIF";
	public final static String PHRASE_PARTLY_MODIFIABLE = "PART_MODIF";
	public final static String PHRASE_STATIC            = "STATIC";
	// Although not a true phrase type, 'FILLED' indicates a partially
	// modifiable phrase which has already been filled in.
	public final static String PHRASE_PARTLY_MODIF_FILLED = "FILLED";

	// Wip View Constants
	public final static String WIP_VIEW_ME              = "ME";
	public final static String WIP_VIEW_MY_ORG          = "MY_ORG";
	public final static String WIP_VIEW_MY_ORG_CHILDREN = "MY_ORG_CHILDREN";

	// Instrument Tab Constants
	public final static String PURCHASE_ORDERS          = "O";
	public final static String PENDING_TRANSACTIONS     = "P";
	public final static String AUTHORIZED_TRANSACTIONS  = "A";
	public final static String INSTRUMENT_HISTORY       = "I";

	public final static String FUTURE_TRANSACTIONS      = "F";//VS CR 609 11/23/10
	public final static String PAYMENT_FILE_UPLOAD      = "U";//MDB CR 564 12/6/10
	//rbhaduri - 14th Oct 08 CR-374 - Invoice Tab Page
	public final static String INVOICES         	= "V";


	// Messages Tab Constants
	public final static String NOTIFICATIONS = "N";
	public final static String MAIL          = "M";
	public final static String PRE_DEBIT_FUNDING = "D";

	//-- CR-451 NShrestha 11/03/2008 Begin --
	// Cash Management Tab Constants
	public final static String ACCT_BALANCE    = "AB";
	public final static String CASH_MGMT_TRANSACTION    = "T";
	public final static String APP_VERSION    = "5.0";
	public final static String TRF_BTW_ACCOUNT = "ATA";
	public final static String DTC_PAYMENT= "ADP";
	public final static String INTL_PAYMENT = "AIP";
	public final static String REPORTS = "ARE";


	//-- CR-451 NShrestha 11/03/2008 End --

	//-------- Navigation constants start ------------------
	// common transaction navigations
	public final static String NAV__PENDING_TRANSACTIONS     = "P";
	public final static String NAV__AUTHORIZED_TRANSACTIONS  = "A";
    public final static String NAV__FUTURE_TRANSACTIONS      = "F";
    public final static String NAV__INQUIRIES                = "I";
    // some specifics to receivables
    public final static String NAV__RECEIVABLE_MATCH_NOTICES = "N";
    public final static String NAV__INVOICES                 = "V";
    //accounts navigations
    public final static String NAV__ACCT_BALANCE             = "AB";
    //upload navigations
    public final static String NAV__PURCHASE_ORDERS          = "O";
    public final static String NAV__PAYMENT_FILE_UPLOAD      = "U";
    public final static String NAV__INVOICE_FILE_UPLOAD      = "I";
    public final static String NAV__STRUCTURE_PURCHASE_ORDER = "S";
    //messages navigations
    public final static String NAV__MAIL                     = "M";
    public final static String NAV__NOTIFICATIONS            = "N";
    public final static String NAV__PRE_DEBIT_FUNDING        = "D";
    //-------- Navigation constants end --------------------

    //-------- RefData constants start ------------------
    public final static String REFDATA__PARTIES              = "PRT";
    public final static String REFDATA__PHRASES              = "PHR";
    public final static String REFDATA__THRESHOLD            = "THR";
    public final static String REFDATA__TEMPLATE             = "TMP";
    public final static String REFDATA__USERS                = "USR";
    public final static String REFDATA__SECURITY             = "SEC";
    public final static String REFDATA__FXRATES              = "FXR";
    public final static String REFDATA__LCCREATERULES        = "LCR";
    public final static String REFDATA__POUPLOADDEFN         = "POD";
    public final static String REFDATA__NOTIFRULES           = "NTR";
    public final static String REFDATA__WORKGROUPS           = "WKG";
    public final static String REFDATA__PANELGROUPS          = "PNG";
    public final static String REFDATA__CALENDARS            = "CAL";
    public final static String REFDATA__PAYTEMPLATEGROUP     = "PTG";
    public final static String REFDATA__GENERICMSGCATEGORIES = "GMC";
    public final static String REFDATA__ATPCREATERULES       = "ATP";
    public final static String REFDATA__ARMMATCHRULES        = "AMR";
    public final static String REFDATA__BANKBRANCHRULES      = "BBR";
    public final static String REFDATA__PAYMENTMETHVAL       = "PMV";
    public final static String REFDATA__CURRENCYCALENDARRULE = "CCR";
    public final static String REFDATA__CROSSRATERULE        = "CRR";
    public final static String REFDATA__ANNOUNCEMENT         = "ANN";
    public final static String REFDATA__INVOICEDEFN          = "IND";
    public final static String REFDATA__INTDISRATE           = "IRT";
    //AAlubala - Rel8.2 CR741 - ERP - BEGIN
    public final static String REFDATA__ERPGLCODES           = "ERP";
    public final static String REFDATA__DISCOUNTCODES        = "DIS";
    //CR741 - END
    public final static String REFDATA__INVONLY        		 = "INO";
    //pgedupudi - Rel9.3 CR976A 03/09/2015
    public final static String REFDATA__BANKGRPRESTRICTRULES   = "BGRR";
    //SSikhakolli - Rel9.5 CR927B - Adding new RefData constant
    public final static String REFDATA__NOTIFRULE_TEMPLATES  = "NRT";
    //-------- RefData constants start ------------------

    //-------- Dashboard Sections start -----------------
    public final static String DASHBOARD_SECTION__MAIL              = "MAIL";
    public final static String DASHBOARD_SECTION__NOTIFICATIONS     = "NOTIFS";
  //Ravindra - CR-708B - Start
    public final static String DASHBOARD_SECTION__INVOICE_OFFERED  = "INVOFFER";
    //Ravindra - CR-708B - End
    public final static String DASHBOARD_SECTION__ALL_TRANSACTIONS  = "ALLTRANS";
    public final static String DASHBOARD_SECTION__RCV_MATCH_NOTICES = "MATCHNOT";
    public final static String DASHBOARD_SECTION__ACCOUNT_BALANCES  = "ACCTBAL";
    public final static String DASHBOARD_SECTION__ANNOUNCEMENTS     = "ANNOUNCE";
    public final static String DASHBOARD_SECTION__LOCKED_OUT_USERS  = "LOCKUSER";
    //CR 821 - Rel 8.3 START
    public final static String DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS = "PANELTRANS";
    //CR 821 - Rel 8.3 END
    public final static String DASHBOARD_NO_SECTIONS_DISPLAY  = "NONE"; //Rel 9.2 IR-34829
    //-------- Dashboard Sections end -------------------

	//Payment file upload validaiton status
	public final static String  PYMT_UPLOAD_VALIDATION_PENDING ="VALIDATION_PENDING";
	public final static String  PYMT_UPLOAD_VALIDATION_IN_PROGRESS = "VALIDATION_IN_PROGRESS";
	public final static String  PYMT_UPLOAD_VALIDATION_SUCCESSFUL = "VALIDATION_SUCCESSFUL";
	public final static String  PYMT_UPLOAD_VALIDATED_WITH_ERRORS = "VALIDATED_WITH_ERRORS";
	public final static String  PYMT_UPLOAD_VALIDATION_FAILED = "VALIDATION_FAILED";
	public final static String  PYMT_UPLOAD_FILE_CONFIRMED = "FILE_CONFIRMED";
	public final static String  PYMT_UPLOAD_FILE_REJECTED = "FILE_REJECTED";
	public final static String  PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR = "VALIDATED_AWAITING_REPAIR";//CR-1001 Rel9.4
	public final static String  PYMT_UPLOAD_FILE_REPAIRED = "FILE_REPAIRED";//CR-1001 Rel9.4

    // CR-597 crhodes 3/1/20011 Begin
	// Payment Beneficiary Email Status
	public final static String PYMT_BEN_EMAIL_IN_PROGRESS = "IN_PROGRESS";
	public final static String PYMT_BEN_EMAIL_PENDING = "PENDING";
    // CR-597 crhodes 3/1/20011 End

	//Message Mode Types
	public final static String MESSAGE_CREATE = "C";
	public final static String MESSAGE_UPDATE = "U";

	// Mail Folder Constants
	public final static String SENT_TO_BANK_FOLDER = "S";
	public final static String DRAFTS_FOLDER       = "D";
	public final static String INBOX_FOLDER        = "I";

	// Message Source Types
	public final static String BANK     = "BANK";
	public final static String PORTAL   = "PORTAL";

	// Message Status Constants
	public final static String SENT_TO_BANK_DELETED = "SENT_TO_BANK_DELETED";
	public final static String SENT_TO_CUST_DELETED = "SENT_TO_CUST_DELETED";
	public final static String NOT_SENT_DELETED     = "NOT_SENT_DELETED";
	public final static String REC                  = "REC";
	public final static String REC_ASSIGNED         = "REC_ASSIGNED";
	public final static String REC_DELETED          = "REC_DELETED";
	public final static String DRAFT                = "DRAFT";
	public final static String SENT_ASSIGNED        = "SENT_ASSIGNED";
	public final static String SENT_TO_BANK         = "SENT_TO_BANK";
	public final static String SENT_TO_CUST_FOLDER  = "S";
	public final static String SENT_TO_CUST			= "SENT_TO_CUST";

	// Notification Rule Criterion Types
	public final static String NOTIF_RULE_NOTIFICATION    = "N";
	public final static String NOTIF_RULE_EMAIL       = "E";
	public final static String NOTIF_RULE_EMAIL_FREQ       = "F";

	// Notification Rule Email Frequencies
	public final static String NOTIF_RULE_TRANSACTION   = "T";
	public final static String NOTIF_RULE_DAILY     = "D";
	public final static String NOTIF_RULE_BOTH      = "B";
	public final static String NOTIF_RULE_NOEMAIL     = "N";
	public final static String NOTIF_RULE_NONOTIFICATION    = "E";

	// Notification Rule Setting Types
	public final static String NOTIF_RULE_ALWAYS        = "A";
	public final static String NOTIF_RULE_NONE          = "N";
	public final static String NOTIF_RULE_CHRGS_DOCS_ONLY = "C";

	// Notfication Rule Tab Types
//	public final static String NOTIF_RULE_GENERAL   = "GEN";
//	public final static String NOTIF_RULE_IMP       = "IMP";
//	public final static String NOTIF_RULE_EXP       = "EXP";
//	public final static String NOTIF_RULE_OTH       = "OTH";
//	public final static String NOTIF_RULE_MAIL      = "MAI";
//	public final static String NOTIF_RULE_BILL      = "BIL";

	// Instrument Category Types
	public final static String IMPORT       = "IMP";
	public final static String EXPORT       = "EXP";
//	public final static String INCOMING       = "INC";
//	public final static String OUTGOING       = "OUT";
	public final static String OTHER        = "OTH";
	public final static String RELATED        = "REL";

	// Email Trigger Types
	public final static String EMAIL_TRIGGER_TRANSACTION  = "TRANSACTION";
	public final static String EMAIL_TRIGGER_MAIL_MSG   = "MAIL_MESSAGE";
	public final static String EMAIL_TRIGGER_DISCREPANCY  = "DISCREPANCY";
	public final static String EMAIL_TRIGGER_DAILY    = "DAILY";
	public final static String EMAIL_TRIGGER_FUNDING  = "FUNDING"; // DK IR-SAUM053159077 Rel8.0 06/18/2012
	public final static String EMAIL_TRIGGER_SETTLEMENT = "SETTLEMENT";
	public final static String EMAIL_TRIGGER_SUPPLIER_PORTAL   = "SP_EMAIL"; //CR 776
	public final static String EMAIL_TRIGGER_INVOICE_CRN  ="INV_CRN_APPROVAL_MAIL"; //Rel 9.3 CR-1006
	public final static String EMAIL_TRIGGER_AR_MATCH  = "MATCH";
	// Email Trigger Characters
	public final static String NEW_LINE   = "\n";
	public final static String LINE_SEPERATOR   = "\r\n";

	// Email Trigger Statuses
	public final static String EMAIL_TRIGGER_NEW  = "N";
	public final static String EMAIL_TRIGGER_STARTED  = "S";
	public final static String EMAIL_TRIGGER_ERROR  = "E";
	public final static String EMAIL_TRIGGER_RETRY  = "R";

	// # of PO Definition fields
	public final static int PO_NUMBER_OF_FIELDS    = 31;

	// PO Date Formats
	public final static String PO_DATE_FORMAT_EURO    = "DDMMYYYY";
	public final static String PO_DATE_FORMAT_US      = "MMDDYYYY";
	public final static String PO_FORMATTED_EURO_DATE = "dd/MM/yyyy";
	public final static String PO_FORMATTED_US_DATE   = "MM/dd/yyyy";

	// PO Definition Types
	public final static String PO_DEFINITION_TYPE_MANUAL = "MANUAL";
	public final static String PO_DEFINITION_TYPE_UPLOAD = "UPLOAD";
	public final static String PO_DEFINITION_TYPE_BOTH   = "UPLOAD_AND_MANUAL";

	// PO Delimiter Types
	public final static String PO_COMMA     = "COMMA";
	public final static String PO_SEMICOLON = "SEMICOLON";
	public final static String PO_TAB       = "TAB";

	// PO Field Data Types
	public final static String PO_FIELD_DATA_TYPE_DATE     = "DATE";
	public final static String PO_FIELD_DATA_TYPE_NUMBER   = "NUMBER";
	public final static String PO_FIELD_DATA_TYPE_TEXT     = "TEXT";
	public final static String PO_FIELD_DATA_TYPE_LARGETEXT     = "LARGETEXT";

	// PO Line Item Field Types
	public final static String PO_FIELD_TYPE_PO_NUMBER          = "po_num";
	public final static String PO_FIELD_TYPE_ITEM_NUMBER        = "item_num";
	public final static String PO_FIELD_TYPE_BENEFICIARY_NAME   = "ben_name";
	public final static String PO_FIELD_TYPE_CURRENCY           = "currency";
	public final static String PO_FIELD_TYPE_AMOUNT             = "amount";
	public final static String PO_FIELD_TYPE_LAST_SHIPMENT_DATE = "last_ship_dt";
	public final static String PO_FIELD_PO_TEXT                 = "po_text";
	public final static String PO_FIELD_TYPE_OTHER              = "other";
	public final static String PO_FIELD_TYPE_DATA_ITEM          = "Data Item ";

	// PO Source Types
	public final static String PO_SOURCE_MANUAL   = "MANUAL";
	public final static String PO_SOURCE_UPLOAD   = "UPLOAD";

	// PO Tab Types
	public final static String PO_GENERAL   = "General";
	public final static String PO_FILE      = "File";
	public final static String PO_GOODS     = "Goods";
	public final static String PO_DEFINITION_PAGE     = "Po_Definition_Page";

	// PO Upload File Format Types
	public final static String PO_DELIMITED     = "DELIMITED";
	public final static String PO_FIXED_LENGTH  = "FIXED_LENGTH";

	// Purchase Order Add/Remove ListView column grouping
	public final static String BY_PO_NUMBER = "PO_ONLY";
	public final static String BY_PO_AND_LINEITEM_NUMBER = "PO_AND_LINEITEM";
	public final static String PO_REMOVE = "REMOVE";
	public final static String PO_ADD = "ADD";


	// Purchase Order Constants
	public final static String GROUP_PO_LINE_ITEMS         = "G";
	public final static String MANUALLY_ADD_PO_LINE_ITEMS  = "M";
	public final static String INCLUDE_ALL_PO_LINE_ITEMS   = "I";
	public final static String USE_ONLY_FILE_PO_LINE_ITEMS = "U";

	// Queue Message Types
	public final static String QUEUE_MESSAGE_TYPE_ACT   = "ACT";
	public final static String QUEUE_MESSAGE_TYPE_MAIL  = "MAIL";

	// Routing Rule Types
	public final static String ROUTE_ALL            = "ALL";
	public final static String CHILDREN_PARENT_ONLY = "CHILDREN_PARENT_ONLY";

	// Download Advice Terms Format
	public final static String DNLD_SWIFT = "SWIFT";
	public final static String DNLD_XML   = "XML";
	public final static String SWIFT_DELIM  = ":";

	// Validation States for CreateTransactionMediator
	public final static String VALIDATE_NEW_TRANS_STEP1 = "VALIDATE_STEP_1";
	public final static String VALIDATE_NEW_TEMPLATE    = "VALIDATE_NEW_TEMP";
	public final static String VALIDATE_NOTHING         = "DONT_VALIDATE_NUTHIN";

	// Used as a radio button value to indicate a user entered account number
	// was provided rather than one of the prefined accounts
	public final static String USER_ENTERED_ACCT        = "-+USER_ENTERED+-";

	//********************************************************************
	//        Constants for Terms object fields
	//********************************************************************

	//rkrishna CR 375-D ATP-ISS 07/26/2007 Begin

	//For ATP following 2 Places of Expiry Types are to be excluded from the dropdown in ATP-Issue transaction.
	//The value here is the values in the column 'Code' in the DB table 'refdata' with table_type as PLACE_OF_EXPIRY_TYPE
	// Terms - Place of Expiry Types
	public final static String COUNTRY_OF_BENEFICIARY        = "Country of Beneficiary";
	public final static String AT_CONFIRMATION_BANK_COUNTERS = "At Confirmation Bank Counters";
	public final static String COUNTRY_OF_SELLER             = "Country of Seller";
	//rkrishna CR 375-D ATP-ISS 07/26/2007 End

	// Terms - Bank Charge Types
	public final static String CHARGE_OUR_ACCT        = "A";
	public final static String CHARGE_BEN_FOR_OUTSIDE = "I";
	public final static String CHARGE_OTHER           = "O";
	public final static String CHARGE_OUR	          = "U";	//IAZ CR-509 01/07/10 Add
	public final static String CHARGE_PAYER           = "P";	//IAZ CR-509 01/07/10 Add
	public final static String CHARGE_SHARED          = "S";    //IAZ CR-509 01/07/10 Add

    // Bank Charge Types used in upload file
    public final static String CHARGE_UPLOAD_OURS     = "O";
    public final static String CHARGE_UPLOAD_BEN      = "B";
    public final static String CHARGE_UPLOAD_OTHER    = "S";
    //BSL IR PAUL051735284 05/18/11 BEGIN
    public final static String CHARGE_UPLOAD_FW_OURS  = "OUR";
    public final static String CHARGE_UPLOAD_FW_BEN   = "BEN";
    public final static String CHARGE_UPLOAD_FW_SHARE = "SHA";
    public final static String CHARGE_BAD_TRANSLATION = "?";
    //BSL IR PAUL051735284 05/18/11 END

	// Terms - Confirmation Types
	public final static String CONFIRM_NOT_REQD   = "WOUT";
	public final static String BANK_TO_ADD        = "CONF";
	public final static String BANK_MAY_ADD       = "MYAD";
	public final static String SILENT_CONFIRMATION = "SCON";
	public final static String NOT_ADDING_CONFIRMATION = "NCON";
	public final static String ADDING_CONFIRMATION = "ACON";


	// Terms - Consignment Types
	public final static String CONSIGN_ORDER      = "O";
	public final static String CONSIGN_PARTY      = "C";


	// Terms - Consignment Party Types
	public final static String CONSIGN_APPLICNT   = "A";
	public final static String CONSIGN_ISS_BANK   = "I";
	public final static String CONSIGN_SHIPPER    = "S";
	public final static String CONSIGN_OTHER      = "O";
	//rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	public final static String CONSIGN_BUYER      = "B";
	public final static String CONSIGN_BUYERS_BANK= "C";
	// rkrishna CR 375-D ATP-ISS 07/21/2007 End


	// Terms - Exchange Rate Variations
	public final static String ADJUST_YOUR_ACCT   = "YOU";
	public final static String ADJUST_BEN_ACCT    = "BNA";

	// Terms - Funds Transfer Settlement type
	public final static String XFER_SETTLE_CREATE_FINANCE   = "CFP";
	public final static String XFER_SETTLE_DEBIT_PAY_ACCT   = "DPA";
	public final static String XFER_SETTLE_OTHER            = "OTH";


	// Terms - Funds Trasnfer Settlement type
	public final static String RELATED_INSTRUMENT = "RIN";
	public final static String DEBIT_APP_ACCT     = "DAA";

	// Terms - Loan Request Settlement type
	public final static String CREDIT_REL_INST     = "REL";
	public final static String CREDIT_OUR_ACCT     = "CAY";
	public final static String CREDIT_BEN_ACCT     = "CBA";
	public final static String CREDIT_OTHER_ACCT   = "COB";
	public final static String CREDIT_MULTI_BEN_ACCT   = "CMB"; 		//RKAZI CR709 Rel 8.2 01/25/2013

	// Terms - Loan Request Finance Types
	public final static String SELLER_REQUESTED_FINANCING   = "SEL_REQ";
	public final static String BUYER_REQUESTED_FINANCING    = "BUY_REQ";
	public final static String OTHER_RECEIVABLES_FINANCING  = "OTH_REC";
	public final static String PRESHIPMENT_FINANCING        = "PRE_SHP";
	//ALREADY DEFINED IN INSTRUMENT CATEGORIES AND TYPES.
	//public final static String ALL_EXPORT_DLC               = "ALL_EXP_DLC";
	//public final static String EXPORT_COL                   = "EXP_COL";
	//public final static String EXPORT_DLC                   = "EXP_DLC";
	//public final static String OTHER                        = "OTH";
	//JUST KEEPING TRACK OF ALL FINANCE TYPES ABOVE

	// Terms - Loan Request Maturity type
	public final static String DEBIT_APP           = "OAN";
	public final static String DEBIT_OTHER         = "OAD";
	// [BEGIN] IR-KTUG091864248 - jkok - added new code for Offset Against Export LC/Collection Instrument
	public final static String DEBIT_EXP           = "OAE";
	// [END] IR-KTUG091864248 - jkok

	// Terms - Marked Feight Types
	public final static String PREPAID            = "P";
	public final static String COLLECT            = "C";

	// Shipment Terms Party Types
	public final static String SHIPMENT_NOTIFY_PARTY       = "NP";
	public final static String SHIPMENT_CONSIGNEE_PARTY    = "CON";

	// Terms Party types
	/**
	 * @deprecated Use {@link TermsPartyType#ADVISING_BANK} instead
	 */
	public final static String ADVISING_BANK       = TermsPartyType.ADVISING_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#AGENT} instead
	 */
	public final static String AGENT               = TermsPartyType.AGENT;
	/**
	 * @deprecated Use {@link TermsPartyType#APPLICANT} instead
	 */
	public final static String APPLICANT           = TermsPartyType.APPLICANT;
	/**
	 * @deprecated Use {@link TermsPartyType#ASSIGNEE_01} instead
	 */
	public final static String ASSIGNEE_01         = TermsPartyType.ASSIGNEE_01;
	/**
	 * @deprecated Use {@link TermsPartyType#ASSIGNEE_BANK} instead
	 */
	public final static String ASSIGNEE_BANK       = TermsPartyType.ASSIGNEE_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#BENEFICIARY} instead
	 */
	public final static String BENEFICIARY         = TermsPartyType.BENEFICIARY;
	/**
	 * @deprecated Use {@link TermsPartyType#BENEFICIARY_BANK} instead
	 */
	public final static String BENEFICIARY_BANK    = TermsPartyType.BENEFICIARY_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#BORROWER} instead
	 */
	public final static String BORROWER            = TermsPartyType.BORROWER;
	/**
	 * @deprecated Use {@link TermsPartyType#CASE_OF_NEED} instead
	 */
	public final static String CASE_OF_NEED        = TermsPartyType.CASE_OF_NEED;
	/**
	 * @deprecated Use {@link TermsPartyType#COLLECTING_BANK} instead
	 */
	public final static String COLLECTING_BANK     = TermsPartyType.COLLECTING_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#DESIG_BANK} instead
	 */
	public final static String DESIG_BANK          = TermsPartyType.DESIG_BANK; //Not a refdata code
	/**
	 * @deprecated Use {@link TermsPartyType#DRAWEE_BUYER} instead
	 */
	public final static String DRAWEE_BUYER        = TermsPartyType.DRAWEE_BUYER;
	/**
	 * @deprecated Use {@link TermsPartyType#DRAWER_SELLER} instead
	 */
	public final static String DRAWER_SELLER       = TermsPartyType.DRAWER_SELLER;
	/**
	 * @deprecated Use {@link TermsPartyType#FREIGHT_FORWARD} instead
	 */
	public final static String FREIGHT_FORWARD     = TermsPartyType.FREIGHT_FORWARD;
	/**
	 * @deprecated Use {@link TermsPartyType#NOTIFY_PARTY} instead
	 */
	public final static String NOTIFY_PARTY        = TermsPartyType.NOTIFY_PARTY;
	/**
	 * @deprecated Use {@link TermsPartyType#OPENING_BANK} instead
	 */
	public final static String OPENING_BANK        = TermsPartyType.OPENING_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#OTHER_CONSIGNEE} instead
	 */
	public final static String OTHER_CONSIGNEE     = TermsPartyType.OTHER_CONSIGNEE;
	/**
	 * @deprecated Use {@link TermsPartyType#OVERSEAS_BANK} instead
	 */
	public final static String OVERSEAS_BANK       = TermsPartyType.OVERSEAS_BANK; //Note GUAR_ISSUE_OVERSEAS_BANK also exits
	/**
	 * @deprecated Use {@link TermsPartyType#RELEASE_TO_PARTY} instead
	 */
	public final static String RELEASE_TO_PARTY    = TermsPartyType.RELEASE_TO_PARTY;
	/**
	 * @deprecated Use {@link TermsPartyType#TRANSFEREE} instead
	 */
	public final static String TRANSFEREE          = TermsPartyType.TRANSFEREE;
	/**
	 * @deprecated Use {@link TermsPartyType#TRANSFEREE_NEGOT} instead
	 */
	public final static String TRANSFEREE_NEGOT    = TermsPartyType.TRANSFEREE_NEGOT;
	/**
	 * @deprecated Use {@link TermsPartyType#THIRD_PARTY} instead
	 */
	public final static String THIRD_PARTY         = TermsPartyType.THIRD_PARTY;
	/**
	 * @deprecated Use {@link TermsPartyType#APPLICANT_BANK} instead
	 */
	public final static String APPLICANT_BANK      = TermsPartyType.APPLICANT_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#ADVISE_THROUGH_BANK} instead
	 */
	public final static String ADVISE_THROUGH_BANK = TermsPartyType.ADVISE_THROUGH_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#REIMBURSING_BANK} instead
	 */
	public final static String REIMBURSING_BANK    = TermsPartyType.REIMBURSING_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#ISSUING_BANK} instead
	 */
	public final static String ISSUING_BANK        = TermsPartyType.ISSUING_BANK;
	//  rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	/**
	 * @deprecated Use {@link TermsPartyType#INSURING_PARTY} instead
	 */
	public final static String INSURING_PARTY      = TermsPartyType.INSURING_PARTY;
	/**
	 * @deprecated Use {@link TermsPartyType#ATP_BUYER} instead
	 */
	public final static String ATP_BUYER           = TermsPartyType.ATP_BUYER;
	/**
	 * @deprecated Use {@link TermsPartyType#ATP_SELLER} instead
	 */
	public final static String ATP_SELLER          = TermsPartyType.ATP_SELLER;
	//  rkrishna CR 375-D ATP-ISS 07/21/2007 End
	/**
	 * @deprecated Use {@link TermsPartyType#PAYER} instead
	 */
	public final static String PAYER               = TermsPartyType.PAYER;
	/**
	 * @deprecated Use {@link TermsPartyType#PAYEE} instead
	 */
	public final static String PAYEE               = TermsPartyType.PAYEE;
	/**
	 * @deprecated Use {@link TermsPartyType#PAYEE_BANK} instead
	 */
	public final static String PAYEE_BANK          = TermsPartyType.PAYEE_BANK;
	//IAZ CM CR-507 12/22/09 BEGIN
	/**
	 * @deprecated Use {@link TermsPartyType#ORDERING_PARTY} instead
	 */
	public final static String ORDERING_PARTY      = TermsPartyType.ORDERING_PARTY;
	/**
	 * @deprecated Use {@link TermsPartyType#ORDERING_PARTY_BANK} instead
	 */
	public final static String ORDERING_PARTY_BANK = TermsPartyType.ORDERING_PARTY_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#FIRST_INTERMED_BANK} instead
	 */
	public final static String FIRST_INTERMED_BANK = TermsPartyType.FIRST_INTERMED_BANK;
	/**
	 * @deprecated Use {@link TermsPartyType#SECND_INTERMED_BANK} instead
	 */
	public final static String SECND_INTERMED_BANK = TermsPartyType.SECND_INTERMED_BANK;
	//public final static String PAYER_BANK          = "PYB";	// IAZ 01/30/10 CHF
	/**
	 * @deprecated Use {@link TermsPartyType#PAYER_BANK} instead
	 */
	public final static String PAYER_BANK          = TermsPartyType.PAYER_BANK;		// IAZ 01/30/10 CHT
	//IAZ CM CR-507 12/22/09 END

	//IR BKUL040547648 Rel 7000 - AAlubala 04/06/2011 - Add a new Party type
	/**
	 * @deprecated Use {@link TermsPartyType#PAYMENT_OWNER} instead
	 */
	public final static String PAYMENT_OWNER = TermsPartyType.PAYMENT_OWNER;
	// Peter Ng - IR INUJ012743663 Begin
	public final static String MULTIPLE            = "MUL";
	// Peter Ng - INUJ012743663 End

	// Terms - Bank Action Required
	public final static String REQUIRED               = "REQ";
	public final static String NOT_REQUIRED           = "NRQ";

	// Terms - Collection_Charges_Types
	public final static String COLLECTION_CHARGES_TYPE_ALL      = "DRAE";
	public final static String COLLECTION_CHARGES_TYPE_DRAWEE   = "ODRE";
	public final static String COLLECTION_CHARGES_TYPE_DRAWER   = "DRAR";

	// Terms - In_Case_of_Need Types
	public final static String IN_CASE_OF_NEED_ACCEPT   = "ACC";
	public final static String IN_CASE_OF_NEED_GUIDANCE = "GUI";

	// Terms - Interest to be Paid Types (interest_to_be_paid)
	public final static String IN_ADVANCE = "ADV";
	public final static String IN_ARREARS = "ARR";

	// Terms - Loan Terms Types
	public final static String LOAN_DAYS_AFTER = "AFS";
	public final static String LOAN_FIXED_MATURITY = "AFM";

	// Terms - Partial Payment Types
	public final static String PARTIAL_PAYMENT_TYPE_FULL_AMOUNT = "F";
	public final static String PARTIAL_PAYMENT_TYPE_PERCENTAGE  = "P";

	// Terms - Payment Instructions Types
	public final static String PMT_INSTR_YES      = "Y";
	public final static String PMT_INSTR_NO       = "N";
	public final static String PMT_INSTR_CURR     = "CURR";     //Currency Code changed

	// Terms - Payment Terms Types
	public final static String PMT_SIGHT          = "SGT";
	public final static String PMT_DAYS_AFTER     = "DAY";
	public final static String PMT_OTHER          = "OTH";
	public final static String PMT_FIXED_MATURITY = "AFM";
	public final static String PMT_CASH_AGAINST   = "CAD";

	// Terms - Shipping term types (incoterms)
	public final static String SHIPPING_EXW       = "EXW";
	public final static String SHIPPING_FCA       = "FCA";
	public final static String SHIPPING_FAS       = "FAS";
	public final static String SHIPPING_FOB       = "FOB";

	// Terms - Tracer Send types
	public final static String NO_ACTION_REQUIRED = "TNAR";
	public final static String BANK_TO_SEND       = "TBTS";

	// Terms - Transport document types
	public final static String MARINE_OCEAN       = "M";
	public final static String MULTI_MODAL        = "C";
	public final static String AIR_TRANSPORT      = "A";

	// Terms - Transport Original types
	public final static String ONE_UPON_3         = "1/3";
	public final static String TWO_UPON_3         = "2/3";
	public final static String THREE_UPON_3       = "3/3";
	public final static String FULL_SET           = "F/S";



	//TP refresh Change
		public final static String FEC   = "F";
		public final static String RMR   = "Y";
		public final static String NA    = "N";

		//END TP Refresh Change

	//MDB - Portal Refresh Routing Functionality
	public final static String FROM_MESSAGES = "M";
	public final static String FROM_PAYREMIT = "P";
	public final static String FROM_CM       = "C";
	public final static String FROM_CM_FVD   = "F";
	public final static String FROM_DDI	     = "D";
	public final static String FROM_TRADE    = "T";
	public final static String FROM_MESSAGES_DEBIT_FUND = "DF";
	public final static String FROM_MESSAGES_NOTIFICATIONS = "MN";

	//********************************************************************
	//        End Constants for Terms object fields
	//********************************************************************


	//Reports
	public final static String STANDARD_REPORTS   = "S";
	public final static String CUSTOM_REPORTS   = "C";
	public final static String NEW_CUSTOM_REPORTS   = "NEW_CUSTOM_REPORTS";
	public final static String NEW_STANDARD_REPORTS   = "NEW_STANDARD_REPORTS";
	public final static String REPORTFLAG   = "Y";
	public final static String REPORT_DOWNLOAD   = "REPORT_DOWNLOAD";
	public final static String REPORT_EDIT  = "EDIT";
	public final static String REPORT_USER_NOT_SETUP = "E198";
	public final static String REPORT_NAME_MISSING   = "E196";
	//Ravindra - Rel7000 - IR# REUL070646127  - 07/12/2011 - Start
	public final static String REPORT_CAT_IN_USE   = "E581";
	//Ravindra - Rel7000 - IR# REUL070646127  - 07/12/2011 - End
	//Suresh - Rel7100 - IR# WZUK102660022 - 09/09/2011 - Start
	public final static String SIMPLESLC_OR_SIMPLEDETAILEDSLC = "E591";
	//Suresh - Rel7100 - IR# WZUK102660022 - 09/09/2011 - End
	public final static String REPORT_NEW   = "NEW";
	public final static String REPORT_DESC_MISSING = "E197";
	public final static String REPORT_CAT_MISSING = "E131";
	public final static String REPORT_OTL_CATEGORY = "OTL";
	public final static String REPORT_TP_CATEGORY = "TP";
	public final static String DUPLICATE_REPORT_NAME = "E187";
	public final static String REPORT_EDIT_CUSTOM = "EDIT_CUSTOM_REPORT";
	public final static String REPORT_EDIT_STANDARD = "EDIT_STANDARD_REPORT";
	public final static String REPORT_COPY_CUSTOM = "COPY_CUSTOM_REPORT";
	public final static String REPORT_COPY_STANDARD = "COPY_STANDARD_REPORT";
	public final static String REPORT_SAVEAS_STANDARD = "SAVEAS_STANDARD_REPORT";
	public final static String REPORT_SAVEAS_CUSTOM = "SAVEAS_CUSTOM_REPORT";
	public final static String REPORT_SERVER_DOWN = "E113";
	public final static String WEBI_SERVER_ERROR = "E206";
	public final static String WEBI_SESSION_ERROR = "E207";
	public final static String WEBI_REPORT_DELETE = "WEBI_REPORT_DELETE";
	public final static String REPORT_PUBLISH_ALL = "REPORT_PUBLISH_ALL";
	public final static String REPORT_PUBLISH_SELECT = "REPORT_PUBLISH_SELECT";
	public final static String REPORT_PUBLISH_HIERARCHY = "E199";
	public final static String SAVE_CUSTOM_ERROR = "SAVE_CUSTOM_ERROR";
	public final static String SAVE_STANDARD_ERROR = "SAVE_STANDARD_ERROR";
	public final static String REPORT_ALL_CAT = "All Categories";
	public final static String REPORT_CATEGORY = "REPORTING_CATEGORY";


	//XI Reports
	public final static String SORTD = "DESC";
	public final static String SORTA = "ASC";
	public final static String DEFAULT_PERSONAL_CATEGORY = "All Categories";
	public final static String DEFAULT_CORPORATE_CATEGORY = "All Categories";
	public final static String DEFAULT_FOLDER = "public";
	public final static String WEBI_INVALID_DATE_ERROR = "E502";


	// Trade Portal Download Constants
	public final static String DOWNLOAD_REQUEST_ADVISE_ISSUE_DATA = "DOWNLOAD_REQUEST_ADVISE_ISSUE_DATA";
	public final static String DOWNLOAD_IMPORT_LC_ISSUE_DATA = "I";
	public final static String DOWNLOAD_GUARANTEE_ISSUE_DATA = "DOWNLOAD_GUARANTEE_ISSUE_DATA";
	public final static String DOWNLOAD_OUTGOING_STANDBY     = "S";
	public final static String DOWNLOAD_ADVICE_TERMS_DATA    = "A";
	public final static String DOWNLOAD_REQUEST_TYPE         = "DownloadRequestType";
	public final static String DOWNLOAD_TXT_CONTENT_TYPE     = "application/x-TradePortal-txt";
	public final static String DOWNLOAD_XML_CONTENT_TYPE     = "application/x-TradePortal-xml";
	public final static String DOWNLOAD_CSV_CONTENT_TYPE     = "application/x-TradePortal-csv";
	// rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	public final static String DOWNLOAD_ATP_ISSUE_DATA = "DOWNLOAD_ATP_ISSUE_DATA";
	// rkrishna CR 375-D ATP-ISS 07/21/2007 End

	// DOC PREP Constants
	public final static String DOC_PREP = "DOCPREP";

	// SWIFT Message Constants
	public final static String SWIFT_MESSAGE_700 = "700";
	public final static String SWIFT_MESSAGE_701 = "701";
	public final static String SWIFT_MESSAGE_710 = "710";
	public final static String SWIFT_MESSAGE_711 = "711";
	public final static String SWIFT_MESSAGE_720 = "720";
	public final static String SWIFT_MESSAGE_721 = "721";

	// Sequence Types (key to the SEQUENCE table)
	public final static String MESSAGE_SEQUENCE = "MESSAGE";
	public final static String AUTOLC_SEQUENCE  = "AUTOLC";

	// Trade Portal Upload Constants
	public final static String UPLOAD_PURCHASE_ORDER_DATA = "PO";
	public final static String UPLOAD_REQUEST_TYPE        = "UploadRequestType";

	// Trigger value to cause an invalid number error
	public final static String BAD_AMOUNT = "BAD AMOUNT!";

	// Amount Tolerance Constant Values
	public final static String NOT_EXCEEDING = "Not Exceeding";
	public final static String AMOUNT_TOLERANCE = "AMOUNT_TOLERANCE";

	// PDF Document Types for ExportCollection Pages
	public final static String PDF_EXPORT_COLLECTION = "ExportCollection";
	public final static String PDF_NEW_EXPORT_COLLECTION = "NewExportCollection";
	public final static String PDF_BILL_OF_EXCHANGES = "BillOfExchanges";
	// PDF Document Types for DLC/SLC/RQA Issue/Amend Application
	public final static String PDF_DLC_ISSUE_APPLICATION     = "DLCIssueApplication";
	public final static String PDF_DLC_AMEND_APPLICATION     = "DLCAmendmentApplication";
	public final static String PDF_SLC_ISSUE_APPLICATION_S   = "SLCIssueApplicationS";
	public final static String PDF_SLC_ISSUE_APPLICATION_D   = "SLCIssueApplicationD";
	public final static String PDF_SLC_AMEND_APPLICATION     = "SLCAmendmentApplication";
	public final static String PDF_RQA_ISSUE_APPLICATION     = "RQAIssueApplication";
	public final static String PDF_RQA_AMEND_APPLICATION     = "RQAAmendmentApplication";
	// rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	public final static String PDF_ATP_AMEND_APPLICATION     = "ATPAmendmentApplication";
	public final static String PDF_ATP_ISSUE_APPLICATION     = "ATPIssueApplication";
	//rkrishna CR 375-D ATP-ISS 07/21/2007 Begin
	
	//PDF Document Types for Settlement Instructions
	public final static String PDF_SETTLE_INSTR_ADVANCE     = "SettlementInstructionsAdvance";
	public final static String PDF_SETTLE_INSTR_BNK_ACC     = "SettlementInstructionsBankersAcceptance";
	public final static String PDF_SETTLE_INSTR_DIFF_PAY    = "SettlementInstructionsDeferredPayment";
	public final static String PDF_SETTLE_INSTR_DISC_RES    = "SettlementInstructionsDiscrepancyResponse";
	public final static String PDF_SETTLE_INSTR_FINANCE     = "SettlementInstructionsFinance";
	public final static String PDF_SETTLE_INSTR_IMP_LC      = "SettlementInstructionsImportLC";
	public final static String PDF_SETTLE_INSTR_INW_COL     = "SettlementInstructionsInwardCollection";
	public final static String PDF_SETTLE_INSTR_LOAN_REQ    = "SettlementInstructionsLoanRequest";
	
	//MEer CR-1027
	public final static String PDF_AIRWAY_BILL_REL_APPLICATION = "AIRReleaseApplication";
	public final static String PDF_SHIPPING_GUAR_ISS_APPLICATION = "SHPIssueApplication";
	public final static String PDF_LRQ_INVFINANCE_ISS_APPLICATION = "LRQIssueApplication";
	public final static String PDF_SLC_ISSUE_EXPANDED_APPLICATION_D   = "SLCIssueExpandedApplicationD";
	public final static String PDF_DLC_ISSUE_EXPANDED_APPLICATION   = "DLCIssueApplicationExpanded";
	public final static String PDF_SLC_ISSUE_EXPANDED_APPLICATION_S   = "SLCIssueApplicationSExpanded";


	// Indicates that the user wants all attributes of a terms manager
	public final static String ALL_ATTRIBUTES = "ALL_ATTRIBUTES";

	// Constants related to Terms' reference to TermsParty
	public final static int MAX_NUM_TERMS_PARTIES = 7;
	public static final int CITY_STATE_ZIP_LENGTH = 32;
	/*KMehta IR-T36000034134 Rel 9300 on 14-May-2015 Add CITY_STATE_ZIP_PARTYBEAN_LENGTH*/
	public static final int CITY_STATE_ZIP_PARTYBEAN_LENGTH = 29;
	public final static String[] TERMS_PARTY_COMPONENT_NAMES = {"FirstTermsParty","SecondTermsParty","ThirdTermsParty","FourthTermsParty","FifthTermsParty","SixthTermsParty", "SeventhTermsParty"};
	public final static String[] TERMS_PARTY_ATTRIBUTE_NAMES  = {"c_FirstTermsParty","c_SecondTermsParty","c_ThirdTermsParty","c_FourthTermsParty","c_FifthTermsParty","c_SixthTermsParty", "c_SeventhTermsParty"};
	public final static String[] TERMS_PARTY_COLUMN_NAMES  = {"c_FIRST_TERMS_PARTY_OID","c_SECOND_TERMS_PARTY_OID","c_THIRD_TERMS_PARTY_OID","c_FOURTH_TERMS_PARTY_OID","c_FIFTH_TERMS_PARTY_OID","c_SIXTH_TERMS_PARTY_OID", "c_SEVENTH_TERMS_PARTY_OID"};

	// Image Format Constants CR-186 - pcutrone
	public final static String TIF_IMAGE = "TIF";
	public final static String PDF_IMAGE = "PDF";

	//IValavala CR 742 add external source type for images
	public final static String EXTERNAL_IMAGE_TYPE = "X";

	// The tabs under Receivables Management
//	public final static String RECEIVABLES_INSTRUMENTS_TAB = "I";
//	public final static String RECEIVABLES_MATCH_NOTICES_TAB = "N";
//	public final static String RECEIVABLES_INVOICES_TAB = "V";
//	public final static String RECEIVABLES_DIRECT_DEBIT_TAB = "D";     //NSX CR-509 12/07/2009
//	public final static String DIRECT_DEBIT_TRANSACTION_HISTORY = "H";     //NSX CR-509 12/07/2009

	//Shilpa R CR-710 REL 8.0 start
//	public final static String INVOICE_UPLOADS_TAB = "IU";
	public final static String INVOCIE_FILE_UPLOAD_EXCEEDED = "E645";
	public final static String INVOICE_FILE_SIZE_EXCEEDED = "E650";
	public final static String ERR_SAVING_INVOICEUPLOAD_FILE = "E649";
	public final static String FILE_FORMAT_NOT_VALID_DELIMITED = "E643";
	public final static String INVALID_INVOICE_FIELD_COUNT = "E644";
	public final static String AVAILABLE_FOR_FINANCING = "AVAILABLE_FOR_FINANCING";
	//BSL IR NNUM040229447 04/02/2012 BEGIN DELETE
	//public final static String UPLOADED_WITH_ERRORS = "UploadedWithErrors";
	//public final static String INVOICE_STATUS_FAILED = "Failed";
	//BSL IR NNUM040229447 04/02/2012 END DELETE
	public final static String INVOICE_FIELD_MISSING = "E646";
	public final static String INVALID_INVOICE_FIELD_FORMAT = "E647";
	public final static String INVALID_USER_DEFINED_LABEL = "E648";
	public final static String BUTTON_DELETE_INVOICE_UPLOAD = "DeleteInvoiceUpload";
	public final static String INVOICE_UPLOAD_DELETE_FILE = "E652";
	public final static String INVALID_TRADING_PARTNER_RULE = "E662";
	public final static String DUPLICATE_INVOICE = "E663";
	public final static String INVOICE_DOESNOT_EXISTS = "E695";
	public final static String INVALID_CREDIT_NOTE = "E696";
	public final static String INITIAL_INVOICE = "INT";
	public final static String INVALID_LINE_ITEMS_AMT = "E670";
	public final static String NO_TRADING_PARTNER_RULE = "E682";
	public final static String PIPELINE = "\\|";
	public final static String NEGATIVE = "-";

	//Shilpa R CR-710 REL 8.0 END

	//trudden CR 451 Caching Constants 12/17/08 Begin

	//Caching Constants
	public final static String CLIENT_BANK_CACHE = "clientbankcache";
	public final static String FX_RATE_CACHE = "fxRateCache";
	//trudden CR 451 Caching Constants 12/17/08 End
	//trudden CR 564 Caching Constants 08/31/10 Performance enhancement for verify domestic payments
	public final static String BANK_BRANCH_RULE_CACHE = "bankBranchRuleCache";
	//trudden CR 564 Caching Constants 08/31/10 End
	public final static String CONFIG_SETTING_CACHE = "ConfigSettingCache";


	//PPRAKASH IR ALUJ011377704 Add Begin
//	public final static String INSTRUMENT_CASH_TAB_VISIBLE= "INSTRUMENT_CASH";
//	public final static String ONLY_INSTRUMENT_INT_TAB_VISIBLE= "INSTRUMENT_INT";
//	public final static String ONLY_CASH_INT_TAB_VISIBLE= "CASH_INT";
//	public final static String INSTRUMENT_TAB_VISIBLE= "INSTRUMENT";
	//PPRAKASH IR ALUJ011377704 Add End

	public final static String CREATE_REPLY_NOT_SELECTED  = "E503"; //IR - GAUI022069369
	//Vshah CR507
	public final static String INTERMEDIARY_PARTY_BANK = "IP1";
	public final static String SECOND_INTERMEDIARY_PARTY_BANK = "IP2";
	public final static String BUTTON_BANKSEARCH = "BankSearch";
	public final static String SELECT_PAYMENT_METHOD = "E510";
	public final static String SELECT_DEBIT_ACCOUNT = "E511";
	public final static String NO_MATCH_FOR_PAYMENTMETHOD_DEBITACCOUNT = "E512";
	public final static String COUNTRY_ISO3116_3 = "COUNTRY_ISO3116_3";
	public final static String INVALID_PAYMENT_METHOD_RULE_COMBINATION = "E513";
	public final static String INVALID_BANK_BRANCH_RULE_COMBINATION = "E514";
	public final static String NO_MATCH_FOR_SELECTED_DEBITACCOUNT = "E515";
	public final static String CURRENCY_CALENDAR_RULE_ALREADY_EXISTS = "E523"; //IR - SAUK011906527
	//Vshah CR507 End

	//VS CR580 Begin
	public final static String INVALID_BANK_BRANCH_RULE = "E531";
	public final static String MULTIPLE_BANK_BRANCH_RULE = "E532";
	//VS CR580 End

	// Vshah CR-586 [BEGIN]
	public final static String PAYMENT_LINES_NOT_CLASSIFIED_AS_CONFIDENTIAL_IND = "E534";
	public final static String CANNOT_UPLAOD_CONFIDENTIAL_PAYMENT_FILES = "E535";
	// Vshah CR-586 [END]

	public final static String PAYMENT_TEMPLATES_CANNOT_BE_IN_SAME_GROUP = "E536"; //Vshah - PKUK092358952

	// VS VSUK090955243 Begin
	public final static String DUPLICATE_TEMPLATEGROUP_SELECTED    = "E537";
	// VS VSUK090955243 End
	public final static String TEMPLATE_GROUP_CANNOT_BE_LINK = "E538";//Vshah - 10/13/2010 - IR#VIUK101259666

	//MDB CR609 11/29/2010 Begin
	public final static String DOM_FUTURE_DEB_CRED_DIFF = "E543";
	public final static String FVD_AUTH_USER_INACTIVE = "E544";
	//MDB CR609 11/29/2010 End

	// CR-543
	public final static String ENCRYPTION_SALT_BLANK = "#####BLANK#####";

	// Narayan CR-596 Payment Status Begin

	public final static String PAY_STATUS_COMPLETED          = "COMPLETE";
	public final static String PAY_STATUS_FAILED             = "FAILED";
	public final static String PAY_STATUS_PARTIAL_COMPLETE   = "PARTIALCOMPLETE";
	public final static String PAY_STATUS_SINGLE_COMPLETE    = "SINGLECOMPLETE";
	public final static String PAY_STATUS_SINGLE_FAILED      = "SINGLEFAILED";
	public final static String PAY_STATUS_REJECTED           = "REJECTED";

	// Narayan CR-596 PaymentStatus End

	//VShah - CR564 - 03/01/2011 - BEGIN
//	public final static String TRANS_STATUS_VERIFIED_PENDING_FX	            			= "VERIFIED_PENDING_FX";
//	public final static String TRANS_STATUS_VERIFIED	          						= "VERIFIED";
//	public final static String TRANS_STATUS_VERIFIED_AWAITING_APPROVAL	    			= "VERIFIED_AWAITING_APPROVAL";
	public final static String BUTTON_VERIFY_FX                    						= "VerifyFX";
	//public final static String TRAN_ACTION_VERIFY_PENDING_FX               				= "Verify-Pending FX";
	public final static String SEMICOLONS_NOT_ALLOWED			   						= "E545";
	public final static String TRANSACTION_INVOLVES_TWO_CURRENCIES_ENTER_EXCHANGE_RATE 	= "E546";
	public final static String FILE_MUST_BE_CONFIRMED									= "I451";
	//VShah - CR564 - 03/01/2011 - END

	public final static String PYMT_UPLOAD_DELETE_FILE_NOT_REJECTED = "E554"; //MDB CR-564 1/5/2011 Rel6.1.0.0
	public final static String ADDL_DOCS_MISSING = "E560"; // VS IR LIUK031741938 01/26/11 Rel 7.0
	public final static String OTHER_ACCOUNT_OID = "OtheracctOid";
	public final static String OTHER_ACCOUNT_CHECKBOX = "otherCorpsAccount";
 	public static final String INDICATOR_PASSED = "P";

	//manohar - 04/14/2011 - CR597 - Start
	public final static String INVALID_VALUE   = "E575";
	public final static String INVALID_PREFIX   = "E576";
	//manohar - 04/14/2011 - CR597 - end
	public final static String INVOICE_DETAILS_EXCEEDS_80000_CHAR   = "E579"; // manohar  - 05/24/2011 - MOUL051953162
	public final static String INVALID_CURRENCY   = "E580"; // manohar  - 6/6/2011 - RAUL052433703
	//manohar - PSUL052447378 - 06/28/2011 - BEGIN
	public final static String INFO_TRANSACTION_INVOLVES_TWO_CURRENCIES_ENTER_EXCHANGE_RATE 	= "I452";
	public final static String INFO_DOM_FUTURE_DEB_CRED_DIFF = "I453";
	//manohar - PSUL052447378 - 06/28/2011 - END

 	public static final String PAYBACK_STATUS_SUCCESS = "Successful";       // CR-593 NSX 04/04/11  Rel. 7.0.0
 	public static final String PAYBACK_STATUS_REJECTED = "Rejected";        // CR-593 NSX 04/04/11  Rel. 7.0.0
 	public static final String PAYBACK_STATUS_FILE_REJECTED = "FILE_UPLOAD_REJECTED";        // CR-970 Rel9.1

	public static final String ROW_LOCKED = "ROW_LOCKED"; //MDB Rel6.1 IR# PDUL012842185

	public final static int	   MAX_CITY_PROVINCE_SIZE			    = 31; 	//CR-597 manohar 04/21/2011
	// manohar - IR SEUL041149384 Rel7.0.0.0 - 05/18/2011 - Begin
	public final static String	   INVOICE			    = "INV";
	public final static String	   INVOICE_LINE			    = "Invoice Line";
	public final static String	   PAYMENT_LINE			    = "Payment Line";
	// manohar - IR SEUL041149384 Rel7.0.0.0 - 05/18/2011 - END

	//Shilpa R- PPX166 -Begin
	public static final String ISRA_JNDI = "ISRA_JNDI";
	public static final String DOCUMENT_ID = "doc_id";
	public static final String PAGE_NUMBER = "page_number";
	//Shilpa R- PPX166 -End

	public final static String	   NOT_APPLICABLE		    = "NA"; // Manohar - IR#HNUL070449897 -7/05/2011
	public final static String	   ALIAS_MSGCAT_PAIR_REQUIRED		    = "E584"; // DK CR-587 Rel7.1
	public final static String	   DUPLICATE_ALIAS_MSGCAT_PAIR		    = "E585"; // DK CR-587 Rel7.1
	public final static String INVALID_SWIFT_CHAR_SEMICOLON            = "E586";
	public final static String	   MUST_BE_ALPHA_NUMERIC		    = "E587"; // DK IR DOUL081056897 Rel7.1

	//MDB CR-640 Rel7.1 10/13/11 Begin
	public final static String	CONV_RATE_TYPE_INSTRUMENT_TO_THIRD 	= "ITOT";
	public final static String	CONV_RATE_TYPE_INSTRUMENT_TO_BASE 	= "ITOB";
	public final static String	CONV_RATE_TYPE_BASE_TO_THIRD 		= "BTOT";
	//MDB CR-640 Rel7.1 10/13/11 End

	//DK CR-640 Rel7.1 BEGINS
	public final static String DEFAULT_FX_ONLINE_ACCT_ID_REQUIRED  = "E588";
	public final static String DOMAIN_ORG_URL_REQUIRED = "E589";
	public final static String FX_AVL_ON = "E590";
	//DK CR-640 Rel7.1 ENDS
	public final static String 	   USER_NOT_CONFIGURED_FOR_PASSSWORD			= "E592"; //Vshah - IR#AOUL063030265 - Rel7.1 - 09/12/2011

	//MANOHAR CR-640 Rel7.1 BEGINS
	public final static String MARKET_RATE_NOT_AVAILABLE_FOR_ACCOUNT = "E598";
	//MANOHAR CR-640 Rel7.1 ENDS

	public final static String CALC_METHOD_NOT_DEFINED_FOR_CURRENCY_PAIR = "E625"; 	//MDB CR-640 Rel7.1 10/28/11
	//Leelavathi CR-707 Rel800 14th Feb 2012 Begin
	public final static String MISSING_RESTRICTED_PO_UPLOAD_DIRECTORY = "E672";
	public final static String PO_UPLOAD_METHOD_WARN = "W660";
	//Leelavathi CR-707 Rel800 22nd Feb 2012 End
	//Leelavathi CR-707 Rel800 5th Mar 2012 Begin
	public final static String MISSING_PO_UPLOAD_FROM_LC_AND_ATP = "E680";
	//Leelavathi CR-707 Rel800 5th Mar 2012 End
	//Suresh CR-713 Rel8.0 Begin
	public final static String 	   MARGIN_REQUIRED								= "E593";
	public final static String 	   CURRENCY_REQUIRED							= "E594";
	public final static String 	   DUPLICATE_MARGIN_RULE						= "E599";
        public final static String	   MARGIN_INVALID							= "E603";
	//Suresh CR-713 Rel8.0 End
        public final static String	   SP_INCORRECT_STATUS							= "E854";
        public final static String	   SP_INCORRECT_FUTURE_VALUE_DATE                                        = "E855";
        public final static String	   SP_MARGIN_INVALID							= "E856";
	public final static String 	   SP_MARGIN_REQUIRED							= "E857";
	public final static String 	   SP_CURRENCY_REQUIRED							= "E858";
	public final static String 	   DUPLICATE_SP_MARGIN_RULE						= "E859";
	public final static String 	   MISSING_SP_MARGIN_RULE						= "E950";
	public final static String 	   MISSING_SP_DIS_CALC_METHOD					= "E871";
	public final static String 	   INVALID_INTEREST_DISCOUNT_RATE_GROUP					= "E872";

	//cquinton 9/9/2011 ppx240 Rel 7.1
	//this is an arbitrary value that must be matched to indicate a hash value was not
	// generated for the given document.  the odd value below is used becuase it is passed as a url
	// parm and we want the encrypted value to resemble an actual encrypted hash value.
	public final static String NO_HASH = "AZBYCXDWEVFUGTHSIRJQKPLOMN192837";

	//Srini_D CR-710 11/25/2011 Rel8.0 Start
	public final static String TPR_ALIASID   = "General";
	public final static String TPR_INVOICE      = "Invoices";
	public final static String TPR_RECVMGMT     = "ReceivablesMgmt";
	public final static String TPR_AUTHORIZATION     = "Authorisations";
	public final static String TPR_BANKDEFRULES     = "Bank-DefinedRules";

	//Invoice Tab
	public final static String INV_UPLOAD_FILES      = "N";

	//BSL CR710 03/01/2012 Rel8.0 BEGIN - These constants should not be defined separate from the original DUAL_AUTH constants
	//Authorization tab variables
//	public final static String DUAL_AUTH_REQ_INV_ONE_USER      = "P";
//	public final static String DUAL_AUTH_REQ_INV_TWO_USERS       = "Y";
//	public final static String DUAL_AUTH_REQ_INV_TWO_WORKGROUPS     = "G";
//	public final static String DUAL_AUTH_REQ_INV_CORPORATE     = "I";
//
//	public final static String DUAL_AUTH_REQ_CREDIT_NONE      = "N";
//	public final static String DUAL_AUTH_REQ_CREDIT_ONE_USER       = "Y";
//	public final static String DUAL_AUTH_REQ_CREDIT_TWO_USER     = "G";
//	public final static String DUAL_AUTH_REQ_CREDIT_TWO_WORKGROUPS     = "I";
//	public final static String DUAL_AUTH_REQ_CREDIT_CORPORATE     = "P";

	// The following constants can be deleted after UploadInvoiceMediatorBean
	// is updated to reference the original constants.
	public final static String DUAL_AUTH_REQ_INV_ONE_USER = DUAL_AUTH_REQ_ONE_USER;
	public final static String DUAL_AUTH_REQ_INV_TWO_USERS = DUAL_AUTH_REQ_TWO_USERS;
	public final static String DUAL_AUTH_REQ_INV_TWO_WORKGROUPS = DUAL_AUTH_REQ_TWO_WORKGROUPS;
	public final static String DUAL_AUTH_REQ_INV_CORPORATE = DUAL_AUTH_REQ_CORPORATE;
	//BSL CR710 03/01/2012 Rel8.0 END

	//Bank Def Rules

	public final static String BANK_DEF_PAYMENT_ALLOW_NO      = "N";
	public final static String BANK_DEF_PAYMENT_ALLOW_YES      = "Y";

	public final static String 	DAYS_BEFORE_AFTER_REQUIRED	= "E595";
	public final static String 	TRADE_MARGIN_REQUIRED	= "E596";
	public final static String 	CURRENCY_CODE_REQUIRED	= "E597";

	 //Srini_D CR-710 11/25/2011 Rel8.0 End

	//Vshah - IR#LMUL112544447 - Rel7.1 - 11/28/2011 - <BEGIN>
	public final static String BUY_INDICATOR = "Buy";
	public final static String SELL_INDICATOR = "Sell";
	//Vshah - IR#LMUL112544447 - Rel7.1 - 11/28/2011 - <END>

	//AAlubala CR-711 Rel8.0 - Token Indicator (RSA - R or VASCO - V)
	public final static String 	RSA_TOKEN = "R";
	public final static String 	VASCO_TOKEN = "V";
	public final static String ERROR_TRANSACTION_SIGNING = "E626";
	public final static String ERROR_PROXY_AUTHORIZING = "E653";
	//CR-711 - end
	//RavindraB - Rel800 IR#LKUM021755721 - 7th Mar 2012 - Start
	public final static String MISSING_SECURITY_TYPE_DEVICE = "E684";
	//RavindraB - Rel800 IR#LKUM021755721 - 7th Mar 2012 - End
    //NAR CR710 rel8.0 12/12/2011 Begin
	public final static String INV_GENERAL                             = "Gen";
	public final static String INV_FILE                                = "Fil";
	public final static String INV_DELIMITED                           = "DELIMITED";
	public final static String DATE_FORMAT                             = "Date Format";
	public final static String INV_DATE_FORMAT                         = "INVOICE_DATE_FORMAT";
	public final static String INV_DELIMITER_CHAR                      = "INVOICE_DELIMITER_CHAR";
	public final static String PAY_DEF_DLM                      	   = "PAY_DEF_DLM"; // KMehta IR-T36000026010 for Rel8400 on 11th Mar 2014
	public final static int INV_SUMMARY_NUMBER_OF_FIELDS               = 34; //Srinivasu_D Rel-8.1.1 IR# T36000005217 09/17/2012 - RKAZI Rel 8.2 IR# T36000012087 02/28/2013 START
	public final static int INV_GOODS_NUMBER_OF_FIELDS                 = 28;
	public final static int INV_LINE_ITEM_NUMBER_OF_FIELDS             = 12;
	public final static int INV_BUYER_OR_SELLER_USER_DEF_FIELD         = 10;
	public final static int INV_LINE_ITEM_TYPE_OR_VALUE_FIELD          = 7;
	//Rel8.2 CR-741 - Discount Inv order total items
	public final static int INV_CREDIT_LINE_ORDER_FIELDS          = 3;
	public final static String INV_LINE_ITEM_FIELD_ALREADY_SELECTED    = "E627";
	public final static String LABEL_REQUIRED                          = "E628";
	public final static String COMPLETE_INV_COUNTRY_INFO               = "E629";
	public final static String IN_USE_FOR_INV_FILE_DESCRIPTION         = "E630";
	public final static String MISSING_INV_SUMMARY_ORDER_FIELDS        = "E631";
	public final static String BUYER_ID_AND_NAME_NOT_REQUIRED          = "E632";
	public final static String LINE_ITEM_REQUIRED                      = "E633";
	public final static String INV_SUMMARY_FIELD_ALREADY_SELECTED      = "E634";
	//NAR CR710 rel8.0 12/12/2011 End

	//Leelavathi CR-710 Rel 8.0 06/12/2011 Added
	public final static String MISSING_INVOICE_GROUP_INDICATOR       = "E635";
	public final static String MISSING_SPECIFIC_RESTRICTED_DIRECTORY_INDICATOR   = "E636";
	public final static String MISSING_RESTRICTED_INVOICE_UPLOAD_DIRECTORY       = "E637";
	public final static String MISSING_DISCOUNT_CAT                              = "E638";
	public final static String DISCOUNT_IS_NOT_APPLICABLE                        = "E639";
	//Leelavathi CR-710 Rel 8.0 06/12/2011 End
	//NAR CR710 rel8.0 12/12/2011 Begin
	public final static String USER_REFERECED_AS_SYSTEM_UPLOAD_USER 	   = "E640";
	//NAR CR710 rel8.0 12/12/2011 End
	//Srinivasu IR-KIUL121454215 12/28/2011 Start
	public final static String DISCOUNT_IS_MISSING = "E641";
	// refactor public final static String DISCOUNT_TEMP = "X";
	//Srinivasu IR-KIUL121454215 12/28/2011 Start
    //NAR CR710 rel8.0 01/12/2012 Begin
	public final static String INV_GOODS_FIELD_ALREADY_SELECTED        = "E651";
	//NAR CR710 rel8.0 01/12/2012 End
	//BSL CR710 01/23/2012 Rel8.0 BEGIN
	public final static String UPLOAD_INV_OPT_LOCK_EXCEPTION       = "E660";
	public final static String SELECT_ONLY_ONE_ITEM                = "E669";
	public final static String ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF    = "E671";
	public final static String PARTNER_RULES_RESTRICT_PAYMENT_DATE = "E708";
	public final static String INV_GROUP_TAB = "G";
	//BSL CR710 01/23/2012 Rel8.0 END
	public final static String ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO  = "E710";//BSL IR NNUM040526786 04/05/2012 ADD
	//NAR CR710 rel8.0 01/05/2012 Begin
	public final static String INVOICE_LIST_TAB = "V";

	// invoice pressed button
	public final static String BUTTON_INV_ATTACH_DOCUMENT                = "AttachDocument";
	public final static String BUTTON_INV_DELETE_DOCUMENT                = "DeleteDocument";
	public final static String BUTTON_INV_ACCEPT_FINANCING               = "AcceptFinancing";
	public final static String BUTTON_INV_REMOVE_FINANCING               = "RemoveFinancing";
	public final static String BUTTON_INV_APPLY_PAYMENT_DATE             = "ApplyPaymentDate";
	public final static String BUTTON_INV_ASSIGN_INSTR_TYPE              = "AssignInstrType";
	public final static String BUTTON_INV_AUTHORIZE_INVOICES             = "AuthorizeInvoices";
	public final static String BUTTON_INV_PROXYAUTHORIZE_INVOICES             = "ProxyAuthorizeInvoices"; //CR710 - Offline/VASCO Auth
	public final static String BUTTON_INV_PROXYAUTHORIZE_CREDIT_NOTE          = "ProxyAuthorizeCreditNote"; //CR710 - Offline/VASCO Auth
	public final static String BUTTON_INV_CLOSE_INVOICE                  = "CloseInvoice";
	public final static String BUTTON_INV_AUTHORIZE_CREDIT_NOTE          = "AuthorizeCreditNote";
	public final static String BUTTON_INV_DELETE_INVOICES                = "DeleteInvoices";
                public final static String BUTTON_INV_RETURN_TO_GROUPS                = "ReturnToGroups";
    public final static String BUTTON_INV_RESET_TO_AUTHORIZE                = "ResetToAuthorize";

	// Added invoice change type to audit log
	public final static String CHANGE_TYPE_APPROAL_FOR_FINANCE     = "AFIN";
	public final static String CHANGE_TYPE_INTEREST_AND_DISCOUNT   = "IDA";
	public final static String CHANGE_TYPE_AUTHORIZED              = "AUTH";
	public final static String CHANGE_TYPE_PARTIAL_AUTHORIZED      = "PAUTH";
	public final static String CHANGE_TYPE_APPLY_PAYMENT_DATE      = "APT";
	public final static String CHANGE_TYPE_ASSIGN_INSTR_TYPE       = "AIT";
	public final static String CHANGE_TYPE_REMOVE_FOR_FINANCING    = "RFIN";
	public final static String CHANGE_TYPE_FILE_UPL           = "UPL";

	// invoice instrument type
	public final static String INV_LINKED_INSTR_REC                      = "REC";

	//	Added invoice status
	public final static String UPLOAD_INV_STATUS_DELETED          = "DELETED";
	public final static String UPLOAD_INV_STATUS_AVL_FOR_FIN      = "AVAILABLE_FOR_FINANCING";
	public final static String UPLOAD_INV_STATUS_AVL_FOR_AUTH     = "AVAILABLE_FOR_AUTHORIZATION";
	public final static String UPLOAD_INV_STATUS_AUTH_FAILED      = "AUTHORIZATION_FAILED";
	public final static String UPLOAD_INV_STATUS_PAR_AUTH         = "PARTIALLY_AUTHORIZED";
	public final static String UPLOAD_INV_STATUS_AUTH             = "AUTHORIZED";
	public final static String UPLOAD_INV_STATUS_FAILED           = "FAILED";
	public final static String UPLOAD_INV_STATUS_PEND_ACT         = "PENDING_ACTION";
	public final static String UPLOAD_INV_STATUS_CLOSED           = "CLOSED"; // Nar IR-MAUM051043378
	//BSL CR710 02/01/2012 Rel8.0 BEGIN
	public final static String UPLOAD_INV_STATUS_FIN_AUTH_FAILED  = "FINANCE_AUTHORIZATION_FAILED";
	public final static String UPLOAD_INV_STATUS_FIN_PAR_AUTH     = "FINANCE_PARTIALLY_AUTHORIZED";
	public final static String UPLOAD_INV_STATUS_FIN_AUTH         = "FINANCE_AUTHORIZED";
	//BSL CR710 02/01/2012 Rel8.0 END
	//Ravindra - Rel800 CR710 - 20th Dec 2011 - End
	public final static String UPLOAD_INV_STATUS_FINANCED         = "FINANCED";
	//Ravindra - Rel800 CR710 - 20th Dec 2011 - End
	public final static String UPLOAD_INVOICE_STATUS                     = "UPLOAD_INVOICE_STATUS";

	//Sandeep - Rel8.2 CR708B IR#T36000015624 - 04/16/2013 - Start
	public final static String SUPPLIER_PORTAL_INVOICE_STATUS =  "SUPPLIER_PORTAL_INVOICE_STATUS";
	public final static String PDF_INVOICE_OFFERED_DETAILS  = "InvoicesOfferedDetailPDFGenerator";
	//Sandeep - Rel8.2 CR708B IR#T36000015624 - 04/16/2013 - End

	// added invoice action to invoice history log
	public final static String UPLOAD_INV_ACTION_DELETE                  = "DEL";
	public final static String UPLOAD_INV_ACTION_CLOSE                   = "CLO";
	public final static String UPLOAD_INV_ACTION_FIN_APP                 = "AFIN";
	public final static String UPLOAD_INV_ACTION_AUTH                    = "AUTH";
	public final static String UPLOAD_INV_ACTION_INTREST_DIS_UPDATE      = "IDA";
	public final static String UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE      = "APT";
	public final static String UPLOAD_INV_ACTION_ASSIGN_INSTR_TYPE       = "AIT";
	public final static String UPLOAD_INV_ACTION_RMV_FINANCING           = "RFIN";
	public final static String UPLOAD_INV_ACTION_FILE_UPL                = "UPL";
	public final static String UPLOAD_INV_ACTION_AUTH_FAILED             = "FLD";
	public final static String UPLOAD_INV_ACTION_DELETED                 = "DEL";
	public final static String UPLOAD_INV_ACTION_PAUTH                   = "PAUTH";
	public final static String UPLOAD_INV_ACTION_BANK_REJECTED           = "REJ";
	public final static String UPLOAD_INV_ACTION_BANK_DECLINED           = "DEC";
	public final static String UPLOAD_INV_ACTION_BANK_PROCESSED          = "PRC";
	public final static String UPLOAD_INV_ACTION_BANK_PROCESSED_CN       = "REC_TPS";


	public final static String INV_INSTR_ASSIGNED_SUCCESSFUL		     = "I454";
	public final static String INV_AUTH_SUCCESSFUL                       = "I455";
	public final static String NO_ACTION_INVOICE_AUTH                    = "E654";

	//NAR CR710 rel8.0 01/05/2012 End
	//ShilpaR CR710 rel8.0 Start
	public final static String UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE    = "PENDING_ACTION_CREDIT_NOTE";
	public final static String UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE  = "PENDING_ACTION_NO_INSTR_TYPE";
	public final static String UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN  = "PENDING_ACTION_UNABLE_TO_FIN";
	public final static String DUPLICATE_GOODS_LINEITEMS  = "E720";
	//ShilpaR CR710 rel8.0 End

	//SHR - IR LUUM050946882
	public final static String INVALID_ISSUE_DATE = "E725";
	//SHR - IR MJUM050951373
	public final static String INVALID_INV_PAYMENT_DATE = "E726";
	//AAlubala - IR#LSUM051041577
	//Only one doc attachment allowed per invoice group
	public final static String ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED_TO_GRP			= "E727";
	//
	//IRLSUM051041577 - end

	//RKAZI CR-710 Rel 8.0 01/25/2012 START
	public final static String INVOICE_MGMT								 = "InvoiceManagement";
	public final static String INV_FIN_AMNT_STAT_RECALC_PENDING			 = "RECALC_PENDING";
	public final static String INV_FIN_AMNT_STAT_RECALC_COMPLETED		 = "RECALC_COMPLETED";
	public final static String INV_FIN_AMNT_STAT_RECALC_INPROGRESS		 = "RECALC_INPROGRESS";
	public final static String INV_FIN_AMNT_STAT_RECALC_FAILED			 = "RECALC_FAILED";
	public final static String INV_PAYMENT_DATE_ALLOWED					 = "N";
	//cquinton 4/3/2013 Rel 8.1.0.4 ir#15381 remove dup/invalid indicators
	//public final static String INV_INTEREST_IND 						 = "I";
	//public final static String INV_DISCOUNT_IND 						 = "D";
	//public final static String INV_STRAIGHT_DISCOUNT_IND 				 = "S";
	//public final static String INV_DTY_DISCOUNT_IND 					 = "N";
	public final static String UPLOAD_INV_SEARCH_STATUS_FIN_ACCPTD      = "FINANCING_ACCEPTED";
	public final static String ERR_UPLOAD_INV_INSTR_TYPE_MISSING		= "E655";
	public final static String ERR_UPLOAD_INV_TENOR_DAYS_LESS			= "E656";
	public final static String NO_ACTION_INVOICE_APPROVE                = "E657";
	public final static String ADMIN_USER_CANNOT_ACCEPT_FINANCING		= "E658";
	public final static String ADMIN_USER_CANNOT_REMOVE_FINANCING		= "E659";
	public final static String INV_FINANCING_ACCEPTED		    		= "I456";
	public final static String INV_FINANCING_REMOVED                    = "I457";
	public final static String PDF_INVOICE_DETAILS	     				= "InvoiceDetailsPDFGenerator";
	public final static String PDF_CREDIT_NOTE_DETAILS	     				= "CreditNoteDetailsPDFGenerator";
	public final static String PDF_LIST     							= "ListViewPDFGenerator";
	public final static String ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED		= "E666";
	public final static String ERR_UPLOAD_INV_CANNOT_ATTACH_DOC			= "E667";
	public final static String ERR_UPLOAD_INV_CANNOT_DELETE_DOC			= "E668";
	//RKAZI CR-710 Rel 8.0 01/25/2012 END
    //NAR CR710 rel8.0 01/05/2012 Begin
	public final static String INV_ASSIGN_PAYMENT_DATE                    = "I458";
    //NAR CR710 rel8.0 01/05/2012 End

	//Srinivasu IR-KIUL121454215 01/28/2012 Start
	public final static String MISSING_INVOICE_MGMT_OPTIONS                 = "E661";
	public final static String  MISSING_INVOICE_UPLOAD                        = "E664";
	//Srinivasu IR-KIUL121454215 01/28/2012 End

	//Srinivasu_D IR-KRUM010338190 01/27/12 Start
		public final static String  AUTHORIZATION_MANDATORY			    = "E665";
		public final static String  INV_AUTHORIZATION_MANDATORY			= "E711";
		public final static String  CRED_AUTHORIZATION_MANDATORY		= "E712";
	//Srinivasu_D IR-KRUM010338190 01/27/12 End

	//Srini_d KRUM010338190 Rel8.0 02/01/12 Start
		public final static String UPLOAD_INV_LINKED_INSTR_TY = "UPLOAD_INV_LINKED_INSTR_TY";
	//Srini_d KRUM010338190 Rel8.0 02/01/12 Start

	//Srinivasu IR-KIUL121454215 01/28/2012 End - Refactor
/*		public final static String INTEREST_DISCOUNT_INDICATOR		 = "Z";
		public final static String DEFAULT_INDICATOR				 = "X";
		public final static String NO_DISCOUNT_INDICATOR			 = "V";
		public final static String NET_FINANCE_INDICATOR			 = "Y";
		public final static String CAL_DISCOUNT_INDICATOR			 = "W";*/
	//Srinivasu IR-KIUL121454215 01/19/2012 End


		//ShilpaR CR707 Rel8.0 Start
		public final static String STRUCTURED_PO       = "SPO";
		public final static String BUTTON_DELETE_PO_UPLOAD = "DeletePOUpload";
		public final static String ERR_SAVING_POUPLOAD_FILE = "E687";
		public final static String PO_FILE_UPLOAD_EXCEEDED = "E688";
		public final static String PO_FILE_SIZE_EXCEEDED = "E689";
		public final static String ADD_PURCHASE_ORDER_SUCCESS    = "I459";
		public final static String REMOVE_PURCHASE_ORDER_SUCCESS = "I460";
		public final static String PURCHASE_ORDER_MISMATCH          = "E707";
		public final static String REMOVE_PO          = "RemovePOLineItems";
		public static final String OTLID 		= "OTLID-";
		public final static String PO_STRUCT_ADD = "PO_STRUCT_ADD";
		public final static String PO_ALREADY_ASSOCIATED = "E697";
		public final static String PO_FILE_LINE_ITEMS_EXCEEDED         = "E698";
		public final static String POS_PER_FILE_EXCEEDED         = "E699";
		public final static String UNV_AMD         = "UNA";
		public final static String UNV_INT         = "UNI";
		public static final String PO_ACTION_UNLINK ="Unlinked";
		public static final String PO_ACTION_CAN_BY_BANK ="Cancelled by Bank";
		public static final String PO_ACTION_PROCESSED_BY_BANK ="Processed by Bank";


		//ShilpaR CR707 Rel8.0 end

		//CR 707 Rel 8.0 02/09/2012 Start
		public final static int PO_SUMMARY_NUMBER_OF_FIELDS               = 7;
		public final static int PO_GOODS_NUMBER_OF_FIELDS                 = 25;
		public final static int PO_LINE_ITEM_NUMBER_OF_FIELDS             = 12;
		public final static int PO_BUYER_OR_SELLER_USER_DEF_FIELD         = 10;
		public final static int PO_LINE_ITEM_TYPE_OR_VALUE_FIELD          = 7;
		public final static String PO_LINE_ITEM_REQUIRED				  = "E673";

		public final static String PO_GOODS_FIELD_ALREADY_SELECTED		  = "E674";
		public final static String PO_SUMMARY_FIELD_ALREADY_SELECTED	  = "E675";
		public final static String PO_LINE_ITEM_FIELD_ALREADY_SELECTED    = "E676";
		public final static String MISSING_PO_SUMMARY_ORDER_FIELDS        = "E677";
		public final static String PO_USE_FOR_INV_FILE_DESCRIPTION        = "E678";
		public final static String INCOTERM_SELECTION					  = "E679";
		public final static String PO_UPLOAD_STRUCTURED					  = "S";
		public final static String PO_UPLOAD_TEXT						  = "T";
		public final static String SPO_DATE_FORMAT                        = "SPO_DATE_FORMAT";
		// Srinivasu CR 707 Rel 8.0 02/09/2012 End

		//Srinivasu CR-707 Rel 8.0 03/06/2012 Start
		public final static String PURCHASE_ORDERS_STRUCTURED				= "S";
		public final static String BUTTON_DELETE_PO_STRUCTURED              = "DeletePOStructured";
		//Srinivasu CR-707 Rel 8.0 03/06/2012 End
		//Srinivasu CR-707 Rel 8.0 02/26/2012 Start
		public final static String BUTTON_CREATE_ATP_FROM_PO_STRUCTURED        = "CreateATPFromPOStructured";
		public final static String BUTTON_CREATE_LC_FROM_PO_STRUCTURED         = "CreateLCFromPOStructured";
		public final static String BUTTON_VIEW_STRUCTURED_PO			       = "ViewStructuredPOButton";
		public final static String BUTTON_REMOVE_STRUCTURED_PO			       = "RemoveStructuredPOButton";
		public final static String BUTTON_ADD_STRUCTURED_PO			       = "AddStructuredPOButton";
		//Srinivasu CR-707 Rel 8.0 02/26/2012 End
        // Nar CR-710 Rel 8.0 03/16/2012 Start
		public final static String INV_CRED_TRIGGER_TYPE                   = "CRED";
        // Nar CR-710 Rel 8.0 03/16/2012 End
		public final static String INV_FINR_TRIGGER_TYPE                   = "FINR";
		//Srinivasu CR-707 Rel 8.0 03/19/2012 Start
		public final static String PO_STRUCTURED_ITEM_UPLOADED_WITH_THIS_DEF   = "E685";
		//Srinivasu CR-707 Rel 8.0 03/19/2012 End
		//Srinivasu CR-707 Rel 8.0 03/20/2012 Start
		public static final String PO_MSG   = "POR";
		public static final String PO_GROUPREF_TYPE   = "P";
		//Srinivasu CR-707 Rel 8.0 03/20/2012 End

       //RKAZI CR-707 Rel 8.0 03/06/2012 Start
        public final static String PO_DUPLICATE_ERR                       = "E690";
        public final static String PO_MISSING_ERR                         = "E691";
        public final static String UPLOAD_PO_ACTION_FILE_UPL              = "UPL";
        public static final String PO_STATUS_FAILED                       = "Failed";
        public final static String INVALID_PO_FIELD_FORMAT                = "E702";
        public final static String INVALID_PO_FIELD_COUNT                 = "E701";
        public final static String PO_FIELD_MISSING                       = "E693";
        public final static String PO_LINE_ITEM_DUPLICATE_ERR             = "E694";
        public final static String ERROR_SAVING_PO                        = "E703";
        public final static String LINE_ITEM_AMT_NOT_EQUAL_TO_PO_AMT      = "E704";
        public final static String ERROR_GROUPING_POS                     = "E705";

        public final static String PO_ACTION_SYS_UPLOAD                  = "System Upload";
        public final static String PO_ACTION_AUTOCREATE                  = "Autocreate";
        public final static String PO_ACTION_AUTHORISE                   = "Authorise";
        public final static String PO_ACTION_DELETE			 			 = "Delete";
        public static final String PO_ACTION_LINK_TO_INSTR 				 = "Linked to Instr";
        public static final String PO_ACTION_AMEND_UPLOAD 				 = "Amendment Upload";


        public final static String PO_STATUS_UNASSIGNED                  = "Unassigned";
        public final static String PO_STATUS_CANCELLED                   = "Cancelled";
        public final static String PO_STATUS_ASSIGNED                    = "Assigned";
        public final static String PO_STATUS_AUTHORISED                  = "Authorised";
        public final static String PO_STATUS_PROCESSED_BY_BANK           = "Processed By Bank";
        public final static String PO_STATUS_DELETE				   		 = "Deleted";
        public final static String BUTTON_CREATE_ATP_FROM_STRUCT_PO      = "CreateATPFromPOStructured";
        public final static String BUTTON_CREATE_LC_FROM_STRUCT_PO       = "CreateLCFromPOStructured";
        public final static String PDF_PO_DETAILS	     				 = "PODetailsPDFGenerator";
        public final static String ERROR_UPLOADING_FILE                  = "E717";
        public final static String ERROR_CANNOT_AMEND_AUTHORIZED_PO      = "E721";  //IR ROUM041205034 Add.
        public final static String ERROR_FIELD_VALUE_EXCEEDS_MAX_LEN     = "E722";  //IR SOUM041631984 Add.
        public final static String INITIAL_PO                            = "INT";
        public final static String AMEND_PO                              = "AMD";
    //RKAZI CR-707 Rel 8.0 03/06/2012 End

		//Srinivasu CR-707 Rel 8.0 03/24/2012 Start
		public final static String PO_ATP_LC_CREATE_SUCCESS				 = "I461";
		//Srinivasu CR-707 Rel 8.0 03/24/2012 Start

		//Srinivasu_D Rel8.0 IR# BEUM030834162 03/27/2012 Start
		public final static String MISSING_PO_SELECTION					 = "E700";
		//Srinivasu_D Rel8.0 IR# BEUM030834162 03/27/2012 End

  		//Srinivasu_D Rel8.0 IR# BRUM032665368 03/29/2012 Start
		public final static String PO_DEF_DEFAULT_DEFINITION			 = "E706";
		//Srinivasu_D Rel8.0 IR# BRUM032665368 03/29/2012 End

		//Srinivasu_D Rel8.0 IR# BUM030950033 04/02/2012 Start
		public final static String PO_DEF_UPLOAD_DRIVE_SELECTION		 = "E709";
		//Srinivasu_D Rel8.0 IR# BUM030950033 04/02/2012 End
		public final static String ERROR_EMPTY_VASCO_SIGNATURE = "E715";
                public final static String ERROR_VASCO_SIGNATURE_NOT_6_DIGIT = "E1110";
		public final static String ERROR_EMPTY_OFFLINE_AUTHORIZER = "E716";
		public final static String ERROR_NONVASCO_FOR_OFFLINE_AUTH = "E713";
		public final static String ERROR_VASCO_OTP_FOR_OFFLINE_AUTH = "E714";
		public final static String ERROR_2FA_NO_ID = "E718"; //CR710 Rel8.0. 4/17/2012. Vasco missing security id message
		public final static String ERROR_RULE_IS_USED = "E719"; //BSL IR NEUM041853493 04/23/2012 Rel 8.0 ADD
		public final static String ERROR_NONALPHANUMERIC_INT_GRP_RATE = "E723"; //AAlubala IR MIUM050239929 05/02/2012 Rel8.0 - Add a new msg
		//Srinivasu Rel8.0 IR-DEUM050357968  05/10/2012 Start
		public final static String VIEW_UPLOAD_MISSING		 = "E724";
		//Srinivasu Rel8.0 IR-DEUM050357968  05/10/2012 End
		public final static String CLOSE_SUCCESSFUL		 = "I462"; //Nar IR MAUM051043378
		public final static String INV_FILE_LINE_ITEMS_EXCEEDED = "E728"; //IR MMUM052582934
		public final static String ERROR_CERTUSER_FOR_OFFLINE_AUTH = "E729"; //AAlubala IR#MJUM061351814
	    // NAR IR MMUM050954397 rel8.0 06/19/2012 Begin
		public final static String DOC_DELETED_SUCCESSFULLY                    = "I463";
		public final static String DOC_NOT_PRESENT_TO_DEL                      = "I464";
		public final static String DOC_ATTACHED_SUCCESSFULLY                   = "I465";
	    // NAR IR MMUM050954397 rel8.0 06/19/2012 End

	// Srinivasu_D CR-708 Rel8.1 05/11/2012 Start
		public final static String LOAN_TYPE_REQUIRED						 =	"E800";
		public final static String LOAN_REQUEST_REQUIRED					 =	"E801";
		public final static String APPROVAL_TO_PAY_REQUIRED					 =	"E802";
		public final static String PROCESS_INV_FILES_REQUIRED				 =	"E803";

		public final static String PM_DUAL_AUTH_REQ_ONE_USER				 =	 "O";
		public final static String PM_DUAL_AUTH_REQ_TWO_USERS				 =	 "T";
		public final static String PM_DUAL_AUTH_REQ_TWO_WORKGROUPS			 =	 "G";
		public final static String PM_DUAL_AUTH_NOT_REQ					 =	 "N";
		public final static String INVOICE_TYPE_REC_MGMT					 =   "R";
		public final static String INVOICE_TYPE_PAY_MGMT					 =   "P";
		public final static String SELLER_ID_AND_NAME_NOT_REQUIRED           =  "E805";
		public final static String ATP_LOANREQ_INVOICE_REQUIRED				 =  "E804";
		public final static String INSTRUMENT_TYPE_MISSING_WARN				 =  "W661";


		// Srinivasu_D CR-708 Rel8.1 05/11/2012 End

		//SHR CR 708 Rel8.1.1 Begin
		public final static String WARNING_NO_DEFAULT_INSTR_TYPE = "W490";
		public final static String PAYABLES_CREATE_INSTR = "C";
		public final static String PAYABLES_MGMT = "PYB";
		public final static String PAYABLES_CREDIT_NOTE_ERROR = "E730";
		public final static String ADD_INVOICE_MISMATCH = "E731";
		public final static String INVOICE_ALREADY_ASSOCIATED = "E732";
		public static final String INV_GROUPREF_TYPE   = "I";
		public final static String ADD_INVOICE_SUCCESS    = "I466";
		public final static String REMOVE_INVOICE_SUCCESS = "I467";
		public final static String REPLACEMENT          = "RPL";
		//SHR CR 708 Rel8.1.1 End
		public final static int RECENT_TRANSACTION_MAX_ROWS                    = 300;  //RKAZI 07/26/2012 Rel 8.1 CQ-T36000003623 max no. of rows = 300

		//Srinivasu_D CR-708 Rel8.1.1 08/25/2012 Start
		public final static String BUTTON_INV_CREATE_APPROVAL_PAY         = "CreateApprovalToPay";
		public final static String PAYABLE_INV_GROUP_TAB				  = "GP";
		public final static String INVALID_PAYABLE_INVOICE_GROUP		  =  "E806";
		public final static String PAYABLE_INV_ATP_FAILED				  =  "E807";
		public final static String PAYABLE_INV_ATP_NO_ACCESS			  =  "E808";
		public final static String PAYABLE_INV_NO_CREDIT_PROCESSING		  =  "E809";
		//Srinivasu_D CR-708 Rel8.1.1 08/25/2012 End

		public final static String NO_MATCHING_RULE_ERROR		      =  "E955";

        //RKAZI CR-708 REL 8.1.1 09/03/2012 Start
        public final static String NO_MATCHING_INVOICE_ERROR		      =  "E810";
        public final static String UPDATING_AUTHORIZED_INVOICE_ERROR	  =  "E811";
        public final static String AUTHORIZED_INVOICE_DUE_DATE_DIFF_ERROR =  "E812";
        public final static String INVOICE_TYPE_RPL                 	  =  "RPL";
        //RKAZI CR-708 REL 8.1.1 09/03/2012 Start
        public final static String INSTRUMENT_EXISTS                 	  =  "I468";

        public final static String DUPLICATE_INVOICE_ID  = "E813"; //SHR IR - T36000005931
		public final static String PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED   = "Unassigned"; //Srinivasu_D Rel-8.1.1 IR# T36000005935 09/28/2012 Start
 		public final static String BUTTON_INV_DETAIL_APPLY_PAYMENT_DATE             = "InvApplyPaymentDate";
        public final static String BUTTON_INV_DETAIL_ATTACH_DOCUMENT             = "InvAttachDocument";
        public final static String BUTTON_INV_DETAIL_DELETE_DOCUMENT                = "InvDeleteDocument";
        public final static String BUTTON_INV_DETAIL_DELETE_INVOICES                = "InvDeleteInvoices";
        public final static String BUTTON_INV_DETAIL_CREATE_APPROVAL_PAY         = "InvCreateApprovalToPay";
        public final static String PAYMENT_DATE_NOT_ALLOWED				  = "E814";
		//Srinivasu_D CR-708 Rel8.1.1 10/10/2012 Start
		public final static String PAYABLE_UPLOAD_INV_STATUS_ASSIGNED     = "Assigned";
        public final static String PAYABLE_UPLOAD_INV_ACTION_UNLINKED     = "Unlinked";
        public final static String PAYABLE_UPLOAD_INV_ACTION_LINK_INST    = "Linked to Instr";
		public final static String ATP_INVOICE_RULES_REQUIRED			  =  "E815";
		public final static String ATP_INVOICE_PAYMENT_DATE_RULE_FAIL	  =  "E816";
        public final static String ATP_INVOICE_PAYMENT_DATE_REQUIRED	  =  "E817";
		public final static String PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION     = "PENDING_ACTION"; //"Pending Action";
		public final static String ATP_INVOICE_INVALID_AMT_CURRENCY	       =  "E818";
		//Srinivasu_D CR-708 Rel8.1.1 10/10/2012 End
	   public final static String ATP_INVOICE_UPLOAD_PAYMENT_DATE_RULE	   =  "E819";
		public final static String INVOICE_DEF_DATA_REQ_VALIDATION		   =  "E820";
		public final static String PAYABLE_UPLOAD_INV_STATUS_DELETED 			= "DELETED";
        public static final String PAYABLE_UPLOAD_INV_STATUS_CAN_BY_BANK		= "Cancelled by Bank";
		public static final String PAYABLE_UPLOAD_INV_STATUS_PROCESSED_BY_BANK  =  "Processed By Bank";
		public final static String PAYABLE_UPLOAD_INV_STATUS_CANCELLED          =  "Cancelled";
		public final static String PAYABLE_UPLOAD_INV_STATUS_REL_BY_BANK        =  "Released by Bank";
		public final static String PAYABLE_UPLOAD_INV_STATUS_AUTHORISED         =  "Authorised";
     	public final static String PAYABLE_UPLOAD_INV_STATUS_PENDING			=  "PENDING";
        public final static String SYSTEM_DATE_AFTER_EXPIRY     = "W491";
		public final static String UPDATING_REC_AUTHORIZED_INVOICE_ERROR	  =  "E821";
		public final static String PAYABLE_INVOICE_PAYMENT_DATE_REQ			  =  "E822";
		public final static String PAYABLE_INVOICE_BANK_BRANCH_REQ			  =  "E823";
		public final static String FAILED_TO_COPY_INSTRUMENT			  =  "E825";

		//Ravindra - CR-708B - Start
		public final static String INTR_DISC_GRP_REQ		  =  "E860";
		public final static String SP_STATUS_BUYER_APPROVED			  =  "BUYER_APPROVED";
		public final static String SP_STATUS_OFFER_ACCEPTED			  =  "OFFER_ACCEPTED";
		public final static String SP_STATUS_PARTIALLY_AUTHORIZED		  =  "PARTIALLY_AUTHORIZED";
		public final static String SP_STATUS_FVD_ASSIGNED	  =  "FVD_ASSIGNED";
		public final static String SP_STATUS_OFFER_DECLINED			  =  "OFFER_DECLINED";
		public final static String SP_STATUS_AUTHORIZED			  =  "AUTHORIZED";
        public final static String SP_STATUS_FVD_AUTHORIZED		=  "FVD_AUTHORIZED";
        public final static String SP_STATUS_INELLIGIBLE		=  "INELIGIBLE";
        public final static String SP_STATUS_REJECTED_BY_BANK		=  "REJECTED_BY_BANK";
        public final static String SP_STATUS_DECLINTED_BY_BANK		=  "DECLINED_BY_BANK";
        public final static String SP_STATUS_PROCESSED_BY_BANK		=  "PROCESSED_BY_BANK";
        public final static String SP_STATUS_INVOICE_PAID		=  "INVOICE_PAID";//Rel 9.0 CR 913

		public final static String DISPLAY_INVOICE_OFFERED			  	=  "N";
		public final static String DISPLAY_FUTURE_VALUE_OFFERS		  	=  "F";
		public final static String DISPLAY_INVOICE_OFFERS_HISTORY		=  "Y";
		public final static String BUTTON_SP_ACCEPT_OFFER               		= "AcceptOffer";
		public final static String BUTTON_SP_DECLINE_OFFER               		= "DeclineOffer";
		public final static String BUTTON_SP_RESET_OFFER               		= "ResetOffer";
		public final static String BUTTON_SP_AUTHORIZE_OFFER             	= "AuthoriseOffer";
		public final static String BUTTON_SP_ASSIGN_FVD              			= "AssignFVD";
		public final static String BUTTON_SP_REMOVE_FVD              			= "RemoveFVD";
		public final static String BUTTON_SP_GROUP_INVOICES           			= "GroupInvoices";
		public final static String BUTTON_SP_ATTACH_DOCUMENT                	= "AttachDocument";
		public final static String BUTTON_SP_DELETE_DOCUMENT                	= "DeleteDocument";
		public final static String BUTTON_SP_ACCEPT_INVOICE_GROUPING            = "AcceptInvoiceGrouping";
		public final static String BUTTON_SP_REMOVE_INVOICES           			= "RemoveInvoices";
		public final static String BUTTON_SP_RESET_INVOICES_OFFER               = "ResetInvoicesOffer";


		//Ravindra - CR-708B - End

		//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
        public final static String AUTO_EXTEND_PERIOD_TYPE     = "AUTO_EXTEND_PER_TYPE";
        public final static String AVAILABLE_BY_TYPE     = "AVAIL_BY_TYPE";
        public final static String TENOR     = "TENOR";
        public final static String PMT_IN_PERCENT     = "P";
        public final static String PMT_IN_AMOUNT     = "A";
        public final static String ATTRIBUTE_VLAUE_RANGE                  = "E826";
        public final static String ATTRIBUTE_REQUIRED_BASED_CONDITION     = "E827";
        public final static String ROW_ATTRIBUTE_REQUIRED_BASED_CONDITION     = "E828";
        public final static String ROW_ATTRIBUTE_MIN_CONDITION     = "E829";
        public final static String ROW_ATTRIBUTE_REQUIRED_EITHER_OF_TWO    = "E830";
        public final static String PMT_MATURITY_DATE_CHECK = "E831";
        public final static String PMT_TENOR_DTL_REQ = "E832";
        public final static String ROW_COLUMN_TOT_CHECK = "E833";
        public final static String PMT_AMOUNT_TOT_IS_NOTEQUAL_TO_INSTRUMENT_AMOUNT = "E834";
      //Leelavathi IR#T36000015641 CR-737 Rel-8.2 04/12/2013 Begin
        public final static String ROW_ATTRIBUTE_REQUIRED_CONDITION     = "E922";
       //Leelavathi IR#T36000015641 CR-737 Rel-8.2 04/12/2013 End
        //Leelavathi IR#T36000017713 Rel-8.2 05/31/2013 Begin
        public final static String TENOR_TYPE_REQUIRED_CONDITION     = "E959";
        //Leelavathi IR#T36000017713 Rel-8.2 05/31/2013 End
        //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End

	//IValavala CR 741
	public static final String CUSREF = "CUSREFIN";

	 //-------- Favorite Task Section start -----------------
    public final static String FAV_TYPE_TASK                    = "T";
    public final static String FAV_TYPE_REPORT                  = "R";

	public final static String FAV_TASK__CREATE_MAIL            = "CreateMail";
    public final static String FAV_TASK__VIEW_NOTIFS            = "ViewNotifs";

    //Naveen 30-August-2012 IR-T36000004151- START
    public final static String FAV_TASK__COPY_TRADE              = "CopyTrade";
    public final static String FAV_TASK__TRANSFER_EXPORT_LC      = "TransferExportLC";
    public final static String FAV_TASK__CREATE_AIRWAY_BILL      = "CreateAirWaybill";
    public final static String FAV_TASK__CREATE_APPROVAL_TO_PAY  = "CreateApprovalToPay";
    public final static String FAV_TASK__CREATE_DIRECT_SEND_COLL = "CreateDirectSendColl";
    //cquinton 2/25/2013 add create export collection task
    public final static String FAV_TASK__CREATE_EXPORT_COLL      = "CreateExportColl";
    public final static String FAV_TASK__CREATE_IMPORT_LC        = "CreateImportLC";
    public final static String FAV_TASK__CREATE_LOAN_REQUEST     = "CreateLoanRequest";
    public final static String FAV_TASK__CREATE_OUTGOING_GUAR    = "CreateOutgoingGuar";
    public final static String FAV_TASK__CREATE_OUT_SLC_DETAILED = "CreateOutSLCDetailed";
    public final static String FAV_TASK__CREATE_OUT_SLC_SIMPLE   = "CreateOutSLCSimple";
    public final static String FAV_TASK__CREATE_REQUEST_TO_ADVISE = "CreateRequestToAdvise";
    public final static String FAV_TASK__CREATE_SHIPPING_GUAR    = "CreateShippingGuar";

    public final static String FAV_TASK__COPY_PAYMENT            = "CopyPayment";
    public final static String FAV_TASK__CREATE_TRNSFR_BTN_ACCT  = "CreateTBA";
    public final static String FAV_TASK__CREATE_PMT              = "CreatePayment";
    public final static String FAV_TASK__CREATE_INT_PMT          = "CreateIntPayment";

    public final static String FAV_TASK__COPY_DIRECT_DEBIT       = "CopyDirectDebit";
    public final static String FAV_TASK__CREATE_DIRECT_DEBIT     = "CreateDirectDebit";

    public final static String FAV_TASK__VIEW_TRADE_PEND_TRANS   = "ViewTradePendTrans";
    public final static String FAV_TASK__VIEW_TRADE_AUTH_TRANS   = "ViewTradeAuthTrans";
    public final static String FAV_TASK__VIEW_TRADE_HISTORY      = "ViewTradeHistory";
    public final static String FAV_TASK__TRADE_CREATE_AMENDMENT  = "CreateAmendment";
    public final static String FAV_TASK__TRADE_CREATE_TRACER     = "CreateTracer";
    public final static String FAV_TASK__TRADE_CREATE_ASSIGNMENT = "CreateAssignment";

    public final static String FAV_TASK__VIEW_PMT_PEND_TRANS = "ViewPmtPendTrans";
    public final static String FAV_TASK__VIEW_PMT_FUTURE_TRANS  = "ViewPmtFutureTrans";
    public final static String FAV_TASK__VIEW_PMT_AUTH_TRANS    = "ViewPmtAuthTrans";
    public final static String FAV_TASK__VIEW_PMT_HISTORY       = "ViewPmtHistory";

    public final static String FAV_TASK__VIEW_DD_PEND_TRANS     = "ViewDDPendTrans";
    public final static String FAV_TASK__VIEW_DD_HISTORY        = "ViewDDHistory";

    public final static String FAV_TASK__VIEW_RCV_MATCHING      = "ViewRcvMatching";
    public final static String FAV_TASK__VIEW_RCV_INVOICES      = "ViewRcvInvoices";
    public final static String FAV_TASK__VIEW_RCV_HISTORY       = "ViewRcvHistory";

    public final static String FAV_TASK__VIEW_ACCT_BALANCES     = "ViewAccountBalances";

    public final static String FAV_TASK__VIEW_PARTIES           = "ViewParties";
    public final static String FAV_TASK__VIEW_PHRASES           = "ViewPhrases";
    public final static String FAV_TASK__VIEW_USERS             = "ViewUsers";
    public final static String FAV_TASK__VIEW_THRESHOLDGROUPS   = "ViewThresholdGroups";
    public final static String FAV_TASK__VIEW_SECURITYPROFILES	= "ViewSecurityProfiles";
    public final static String FAV_TASK__VIEW_PODEFINITIONS		= "ViewPODefinitions";
    //cquinton 2/25/2013 add view invoice definitions
    public final static String FAV_TASK__VIEW_INV_DEFINITIONS   = "ViewInvDefinitions";
    public final static String FAV_TASK__VIEW_CREATIONRULES		= "ViewCreationRules";
    public final static String FAV_TASK__VIEW_REC_MATCH_RULES	= "ViewReceivablesMatchingRules";
    public final static String FAV_TASK__VIEW_WORKGROUPS		= "ViewWorkGroups";
    public final static String FAV_TASK__VIEW_PANEL_AUTH_GRPS	= "ViewPanelAuthorizationGroups";
    public final static String FAV_TASK__VIEW_TEMPLATE_GRPS		= "ViewTemplateGroups";
    public final static String FAV_TASK__VIEW_TEMPLATES		    = "ViewTemplates";
    public final static String FAV_TASK__VIEW_FX_RATES		    = "ViewFXRates";

    public final static String FAV_TASK__UPLOAD_PURCHASE_ORDER	= "UploadPurchaseOrder";
    public final static String FAV_TASK__UPLOAD_INVOICE_FILE    = "UploadInvoices";
    public final static String FAV_TASK__UPLOAD_PAYMENT_FILE	= "UploadPaymentFile";

    public final static String FAV_REPORT_SUMM_LIABILITY        = "SUMMLIABILITY";
    public final static String FAV_REPORT_DETAIL_LIABILITY      = "DETAILIABILITY";
    public final static String FAV_REPORT_WORK_IN_PROGRESS      = "WORKINPROGRSS";

    //Naveen 30-August-2012 IR-T36000004151- END

    public final static String INV_INVALID_ACTION_PAY_DATE     = "EPR001";//SHR
    // Srini_D IR# MNUM121350532 Rel 8.1 12/28/2012 - Added new button constant
	public final static String BUTTON_ADDSAVETRANS                 = "AddSaveTrans";
    //-------- Favorite TAsk Section end -------------------

    //AAlubala - Rel8.2 CR741 - BEGIN
	public final static String BANK_AND_CUSTOMER_ERP_CODES   = "B";
	public final static String CUSTOMER_ONLY_ERP_CODES   = "C";
	public final static String ERP_PAYMENT = "Payment";
	public final static String ERP_DISCOUNT = "Discount";
	public final static String ERP_OVERPAYMENT = "Over-Payment";
    //CR741 - END
    // DK CR709 Rel8.2 10/31/2012 BEGINS
    public final static String LOAN_TYPE_REQD				 = "E835";
    public final static String TRADE_LOAN_RULES_REQD				 = "E836";
    public final static String INV_FILE_TYPE                         = "INVOICE_FILE_TYPE";
    public final static String LOAN_TYPE				 = "LOAN_TYPE";
    public final static String TRADE_LOAN			 = "TLN";
    public final static String EXPORT_FIN				 = "EFN";
    public final static String INV_FIN_REC				 = "IFR";
    public final static String INV_FIN_BR				 = "IFBR";
    public final static String INV_FIN_BB				 = "IFBB";
    public final static String WARNING_NO_LOAN_TYPE = "W492";
    public final static String BUTTON_INV_CLEAR_PAYMENT_DATE             = "ClearPaymentDate";
    public final static String LOAN_REQUEST_PROCESS_INV_FILES_REQUIRED				 =	"E837";

	public final static String BUTTON_INV_ASSIGN_LOAN_TYPE          = "AssignLoanType";
	public final static String BUTTON_INV_CREATE_LOAN_REQ  = "CreateLRQ";
	public final static String BUTTON_INV_CREATE_LOAN_REQ_BY_RULES  = "CreateLoanReqByRules";
	public final static String REC_INVOICE_CLEAR_PAYMENT_NOT_ALLOW  = "E838";
	public final static String UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE = "CPD";
	public final static String INV_CLEAR_PAYMENT_DATE               = "I469";
	public final static String REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_ALLOW  = "E839";
	public final static String REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_AVAIL  = "E840";
	public final static String INV_ASSIGN_LOAN_TYPE                 = "I470";
	public final static String UPLOAD_INV_ACTION_ASSIGN_LOAN_TYPE   = "ALT";
	public final static String REC_INV_LRQ_NO_ACCESS			  =  "E843";
	public final static String INVALID_REC_INVOICE_GROUP		  =  "E844";
	public final static String INV_DATA_REQD		  =  "E845";
    // DK CR709 Rel8.2 10/31/2012 ENDS
	//Srinivasu_D CR-709 Rel8.2 12/17/2012 Start
//	public final static String REC_INVOICE_CLEAR_PAYMENT_NOT_ALLOW			 =  "E838";
//	public final static String REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_ALLOW		 =  "E839";
//	public final static String REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_AVAIL		 =  "E840";
//	public final static String INV_CLEAR_PAYMENT_DATE						 =  "I469";
//	public final static String INV_ASSIGN_LOAN_TYPE							 =  "I470";
	public final static String AUTO_LOAN_REQ_RULE_CREATION_SUCCESS			 =  "I471";
	public final static String AUTO_LOAN_REQ_BY_RULES_NO_RULE_EXISTS		 =  "I472";
	public final static String AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE  =  "I473";
//	public final static String UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE			 =  "CPD";
//	public final static String UPLOAD_INV_ACTION_ASSIGN_LOAN_TYPE			 =  "ALT";
	public final static String UPLOAD_INV_ACTION_LOAN_REQ_BY_RULE			 =  "LRR";
	public final static String REC_INVOICE_LOAN_REQ_RULES_NOT_ALLOW			 =  "E846";
	public final static String REC_INVOICE_ASSIGN_INSTRUMENT_TYPE_NOT_ALLOW  =  "E847";
	//public final static String LOANREQ_LOG_NO_FX_RATE			 			 =  "W493";
	public final static String INV_CLEAR_PAYMENT_DATE_NOT_AVAILABLE			 =  "E849";
	public final static String RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED		 =  "Assigned";
	public final static String INVOICE_ACTION_AUTOCREATE					 =  "Autocreate";
    //public final static String LOANREQ_LOG_INVALID_CURRENCY			 		 =  "W495";
	public final static String REC_LIST_CLEAR_PAYMENTDATE					 =  "ListClearPaymentDate";
	public final static String REC_LIST_CREATE_LOAN_REQ_BY_RULES			 =  "ListCreateLoanReqByRules";
	public final static String REC_LIST_INV_ATTACH_DOCUMENT					 =  "ListAttachDocument";
	public final static String REC_LIST_INV_DELETE_DOCUMENT					 =  "ListDeleteDocument";
    //Srinivasu_D CR-709 Rel8.2 12/17/2012 End
    public final static String DEBIT_OUR_ACCOUNTS     = "DA";//RKAZI
    public final static String DEBIT_LOAN_PROCEEDS     = "DL";//RKAZI
    public final static String DEBIT_ACCT_REC     = "DR";//RKAZI
    public final static String TRADE_LOAN_REC    = "TRL_REC";//RKAZI
    public final static String TRADE_LOAN_PAY    = "TRL_PAY";//RKAZI
    public final static String BUTTON_DELETE_INV_PAYEE				= "DeleteInvPayee";
 // DK IR - T36000011727 Rel8.2 02/14/2013 Starts
    public final static String BEN_BANK_NAME_BRANCH_REQD = "E850";
    public final static String BEN_BANK_NAME_ADDR_DATA_REQD = "E851";
    public final static String BEN_BANK_BRANCH_DATA_REQD = "E852";
    public final static String BEN_ADDR_BANK_NAME_DATA_REQD = "E853";
 // DK IR - T36000011727 Rel8.2 02/14/2013 Ends

	public final static String BUTTON_ADD_TRANS_INVOICES         = "AddTransInvoices";
	public final static String BUTTON_VIEW_TRANS_INVOICES         = "ViewTransInvoices";
	public final static String BUTTON_REMOVE_TRANS_INVOICES         = "RemoveTransInvoices";
	public final static String BUTTON_ASSIGN_TRANS_INVOICES         = "AssignTransInvoices";
	public final static String INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD = "E863";//RKAZI T36000014518 Rel 8.2 Added
	public final static String COUNTRY_REQD_FOR_CBFT_PAY_METHOD = "E864";//RKAZI T36000014518 Rel 8.2 Added
	//Srinivasu_D Fix of IR#11999 Rel8.2 02/25/2013 start
	 public final static String INVALID_LOAN_REQ_EXP_FINANCE_GROUP		  =  "E861";
     public final static String INVALID_LOAN_REQ_TRADE_LOAN_GROUP		  =  "E862";
	 public final static String REC_LIST_INV_ASSIGN_PAYMENTDATE			  =  "ListAssignInstrType";
	 public final static String REC_LIST_INV_CREATE_LOAN_REQ			  =  "ListCreateLRQ";
	 public final static String INV_MGMT_LOAN_REQ_PAGE	   			      =  "InvoiceGroupLoanReqDetail";
	  public final static String REC_INVOICE_DELETE_SUCCESSFUL			  =  "I474";
	//Srinivasu_D Fix of IR#11999 Rel8.2 02/25/2013 end
	// DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Starts
	public final static String FOR_REC_PROG  = "R";
	// DK Rel8.2 IR T36000011671/T36000011677/T36000012151/T36000012149 02/25/2013 Ends
	public final static String BUTTON_UNASSIGN_TRANS_INVOICES         = "UnAssignTransInvoices";
	public final static String DIFFERENT_INVOICE_CURRENCY = "E865";//RKAZI T36000014518 Rel 8.2 Added
	public final static String INV_BEN_BANK_ADD_SIZE_EXCEED			  =  "E866";
	public final static String BUTTON_CALC_DIST_AND_INTRST              =  "CalcDistAndIntrst";
	public final static String LOAN_DETAIL_REQ_TO_CALC_DIST_AND_INST    =  "E867";
	public final static String LRQ_INV_TENOR_DAYS_LESS                  =  "E868";

	//jgadela R8.3 CR 501 16/04/2013 [start]
	public final static String PENDING_USER_ALREADY_EXISTS = "E990";
	public final static String PENDING_USER_ALREADY_EXISTS2 = "E991";
	public final static String PENDING_USER_ALREADY_APPROVED = "E992";
	public final static String DUPLICATE_PENDING_USER = "E993";
	public final static String INSERT_SUCCESSFUL_PENDING = "I300";
	public final static String UPDATE_SUCCESSFUL_PENDING = "I301";
	public final static String DELETE_SUCCESSFUL_PENDING = "I302";
	public final static String UPDATES_REJECTED = "I303";
	public final static String PENDING_EXISTS = "E1050";
	//jgadela R8.3 CR 501 16/04/2013 [end]

	public final static String LRQ_INSTRUMENT_EXISTS                 			 =  "I475";
	public final static String PAYBLE_INVOICE_PAGE           					 =  "yes";
	public final static String PAY_GROUP_INV_CREATE_ATP_BYRULES				     =  "CreateATPByRules";
    public final static String PAY_GROUP_DETAIL_INV_ATP_BYRULES				     =  "ListATPByRules";
	public final static String PAY_INVOICE_ATP_RULES_NOT_ALLOW			 		 =  "E869";
	public final static String AUTO_ATP_REQ_RULE_CREATION_SUCCESS				 =  "I476";
	public final static String AUTO_LOAN_REQ_BY_RULES_PAYMENT_DATE_NOT_BLANK  	 =  "I477";
	public final static String ERROR_GROUPING_INVS                    			 = "E877";
	public final static String INVALID_LOAN_PAYMENT_DATE 						 =  "E873";
	public final static String INVALID_LOAN_ISSUE_DUE_DATES_MISSING				 =  "E874";
	public final static String INVALID_ISSUE_DUE_NOT_LESS_THAN_CURRENT_DATE		 =  "E875";

	//M Eerupula Rel8.3 CR 776
	public final static String EMAIL_FREQUENCY_MISSING							 =  "E968";
	public final static String INVALID_EMAIL_FREQUENCY							 =  "E969";
	public final static String TRADE_LOAN_NO_PAYMENT_DATE					     =  "E882";
public final static String MUST_BE_LESS_THAN_EQUAL_TO              = "E878";
             	public final static String PAY_INV_ATP_GROUP_DETAIL_PAGE			         =  "PayableInvoiceGroupDetail";
	public final static String PAY_INV_LRQ_GROUP_DETAIL_PAGE	                                    =  "PayableInvoiceGroupLRDetail";
	public final static String TRADE_LOAN_RULES_RESTRICT_PAYMENT_DATE			 = "E880";
	public final static String ERR_TRADE_INV_DUE_PMT_DAYS_DIFF    				= "E881";
	//AAlubala Rel8.2 CR-741 - Start
	public final static String  ERP_CODES_REQUIRED 								= "E883";
	public final static String  ERP_CODES_EXISTS 								= "E884";
	public final static String  ERP_GL_CODE_FOR_PAYMENT_ONLY_ONE 				= "E885";
	public final static String  ERP_GL_CODE_FOR_OVERPAYMENT_ONLY_ONE 			= "E886";
	public final static String  ERP_GL_CODE_FOR_DISCOUNT_ONLY_ONE 				= "E887";
	public final static String  ERP_GL_USED_ON_TRADE_PARTNER 					= "E888";
	public final static String  ERP_GL_USED_ON_TRADE_PARTNER_W 					= "W496";
	public final static String  TRANSACTION_TYPE_REQUIRED 						= "E889";
	public final static String  APPLIED_AMOUNT_REQUIRED 						= "E890";
	public final static String  APPLIED_AMT_BE_GREATER_THAN_ZERO 				= "E891";
	public final static String  DISCOUNT_CODE_REQUIRED_FOR_DISC_TRANSACTION 	= "E892";
	public final static String  DISCOUNT_CODE_NOT_ALLOWED_FOR_PAY 				= "E893";
	public final static String  TOTAL_AMT_EXCEEDS_OUTSTANDING_AMT 				= "E894";
	public final static String  EXCESS_PAY_NOT_EXCEED_OUTSTANDING_AMT 			= "E895";
	public final static String  CHANGES_NOT_SAVED 								= "E896";
	public final static String  DISCOUNT_CODES_REQUIRED 						= "E897";
	public final static String  DISCOUNT_CODES_TWO_CHARS 						= "E898";
	public final static String  DISCOUNT_CODES_EXISTS 							= "E899";
	public final static String  INDUSTRY_DISCOUNT_CODES_EXISTS 					= "E900";
	public final static String  ERP_GL_CODE_DESCRIPTION_LEN 					= "E903";
	public final static String  DISCOUNT_CODES_THIRTY_CHARS 					= "E902";
	//CR-741 - End
	public final static String  AUTO_UPLOAD_INV_LOANTYPE_MISSING 				= "E901";
	public final static String AUTO_UPLOAD_INV_INSTR_TYPE_MISSING			    = "E904";
	public final static String MISSING_LOAN_TYPE_FOR_ADD_INVOICES			    = "E905";
	public  final static String INST_LOAN_TYPE_REQUIRED                                 = "E906";
	public  final static String ERROR_CREATING_ISTRUMENT                                = "E907";
	public  final static String LOAN_TYPE_NO_STATUS_CHANGE						= "E908";
        public final static String IS_WEEKEND                                               ="E909";
        public final static String IS_HOLIDAY                                               ="E910";
      public final static String BUTTON_INV_DELETE_LRQ_INVOICES					= "DeleteLRQInvoices";
	public final static String TRADE_LOAN_ISSUE_DATE_WARNING 					= "W494";
	public final static String RECEIVABLE_INV_ASSIGN_INST						= "Receivable Invoices";
	public final static String AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE_OR_INVS_ALRDY_ATTCHD  =  "I478";
		public final static String LOANREQ_LOG_NO_FX_RATE			 			 =  "E912";
	    public final static String LOANREQ_LOG_INVALID_CURRENCY			 		 =  "E911";
		public final static String FX_LOAN_REQUEST								="Loan Request";
		public final static String FX_APPROVAL_TO_PAY							="Approval To Pay";
	    public final static String CREDIT_NOTE_NOT_ALLOWED_FOR_LRQ_ATP				= "E913";
		public  final static String LOAN_REQ_MULTIPLE_LOAN_TYPES_SELECTED            = "E914";
		public  final static String INST_LOAN_TYPE_MISSED_GROUP                      = "E915";
		public  final static String REC_OR_PAY_TYPE_MISSING                      = "E916";
		public  final static String MISSING_ATTACHED_INVS_OR_INVOICES_DETAILS                    = "E917";
		public final static String RPL_NOT_ALLOWED_FOR_LRQ				= "E918";
		public final static String INVALID_BANK_BRANCH_CODE		= "E919";
		public final static String LRQ_NOT_ALLOWED_FOR_REC		= "E920";
		public  final static String PAYAMENT_DATE_NOT_ALLOW_TRADE_LOANS                      = "E921";
		public final static String DIFFERENT_INVOICE_TRADINGPARTNERS = "E924";
		public final static String DIFFERENT_INVOICE_PAYMENTORDUEDATES = "E923";
		public final static String RPL_ATP_ISSUE = "E925"; //IR T36000016161
		public final static String INVOICE_LINE_ITEMS_DATA_REQ = "E926"; //IR T36000016317
		public final static String INVALID_BANK_DETAILS_FOR_INV_UPLOAD = "E927";
		public final static String UPDATING_AUTHORIZED_LRQ_INVOICE_ERROR = "E928";
        public final static String 	   NO_MARGIN								= "W497"; 		// DK IR T36000016513 Rel 8.2 05/03/2012
		public final static String DIFFERENT_INVOICE_PAYMENTMETHOD = "E929";
		public final static String DIFFERENT_PAYMENTBENE_PAYMENTMETHOD = "E930";
		public final static String DIFFERENT_PAYMENTBENE_CURRENCY = "E931";
		public  final static String CREDIT_NOTE_AMOUNT_MISSING_ERROR                      = "E932";
		public  final static String CREDIT_NOTE_LRQ_ATP_ERROR							  = "E933";
		public  final static String CREDIT_NOTE_MISSING_REC_ERROR						  = "E934";
		public  final static String AMOUNT_NEGATIVE_ERROR								  = "E935";
		public  final static String CREDIT_NOTE_INVOICE_NOT_FOUND_ERROR						  = "E936";
		public final static String NO_ADD_INV_TO_EXP_LOAN = "E937";
		public final static String NO_INSTR_TYPE_ATP = "E956";
                           public final static String INVF_FINANCE_TYPE_REQUIRED          = "E957";
		//CR 821 - Rel 8.3
		public final static String PANEL_AUTHORIZATION_CHECK_FOR_SUBSIDIARY_CUSTOMER = "W500";
		public final static String PANEL_GROUP_REQUIRED_FOR_PANEL_AUTHORIZATION = "W501";
		public final static String PANEL_GROUP_ASSIGNED_FOR_INSTRUMENTS = "W502";
		public final static String MULTIPLE_PAYMENT_METHODS_ASSOC_TO_PANEL_GROUPS = "E970";
		public final static String DUPLICATE_PANEL_GROUPS = "E971";
		 // Rel 8.3 CR 776
		public final static String MY_ORGANIZATION_PROFILE   = "My Organisation's Profile";
		//Narayan Rel8.3 CR 821 Begin
		public final static String BUTTON_SEND_FOR_AUTH   = "SendForAuth";
		public final static String BUTTON_SEND_FOR_REPAIR = "SendForRepair";
		public final static String SEND_FOR_REPAIR		  =  "I304";
		public final static String SEND_FOR_AUTHORIZATION =  "I305";
		public final static String PERAIR_REASON_REQ      =  "E997";
		public final static String NOT_VALID_STATUS_FOR_ACTION      =  "E998";
		//Narayan Rel8.3 CR 821 End

		//Prateep Rel8.3 CR 821 Begin
		public final static String PANEL_INSTRUMENT_TYPE_CHECK = "E994";
		public final static String PANEL_RANGE_RULES_CHECK = "E995";
		public final static String PANEL_RULE_SEQUENCE_APPROVER_CHECK = "E996";
		public final static String PANEL_RULE_APPROVER_SEQUENCE_CHECK = "E1031";
		public final static String PANEL_GROUP_DELETE_ASSIGNED_INSTRUMENTS = "W502";
		//Prateep Rel8.3 CR 821 End

		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
		public final static String SLC_OTHER    = "OTHR";
		public final static String GUA_OTHER    = "OTHR";
		public final static String RQA_OTHER    = "OTHR";

		//Adding New Errors for ICC Publication Validations
		public final static String ICC_PUBLICATION_DETAILS_CHECK = "E1001";

		//Adding new Table Types for Ref Data Table
		public final static String ICC_GUIDELINES_DLC           = "ICC_GUIDELINES_DLC";
		public final static String ICC_GUIDELINES_SLC           = "ICC_GUIDELINES_SLC";
		public final static String ICC_GUIDELINES_GUA          = "ICC_GUIDELINES_GUA";
		public final static String ICC_GUIDELINES_RQA           = "ICC_GUIDELINES_RQA";
		//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End

		//PMitnala Rel 8.3 CR 821 - START
		public final static String BUTTON_NOTIFY_PANEL_AUTH_USER = "NotifyPanelAuthUser";
		//PMitnala Rel 8.3 CR 821 - END

		public final static String ACCEPTANCE 	= "ACCP";
		public final static String DEFERED_PAY 	= "DEFP";
		public final static String MIXED_PAY 	= "MIXP";
		public final static String NEGIOTIATION = "NEGO";
		public final static String SIGHT 		= "PAYM";
		public final static String SPECIAL 		= "SPEC";
		public final static String LRQ_INV_ASSIGN_INST	= "Loan Request";  // DK IR T36000016416 Rel8.2 05/14/2013
		public final static String INVALID_DUE_DATE          = "E958";
		public final static String EXP_DUE_DATE_UNMATCH          = "E967";
		public final static String DUPL_RESP_TRAN          = "E960";
		public final static String INVALID_LOAN_REQ_PAYABLE_GROUP		  =  "E961";
		public final static String INVALID_BANK_BRNCH_CODE = "E962";
		public final static String INVALID_SUPP_AMT = "E963";

		public final static String INFO_INVALID_BANK_BRNCH_CODE = "I502";
	    public final static String INFO_BEN_BANK_NAME_BRANCH_REQD = "I503";
	    public final static String INFO_BEN_BANK_NAME_ADDR_DATA_REQD = "I504";
		public final static String INFO_INVALID_PAYMENT_METHOD_RULE_COMBINATION = "I505";
		public final static String INFO_INVALID_PAYMENT_METHOD_FOR_INV_UPLOAD = "I506";

		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
		public final static String DUPLICATE_INSTRUMENT_ID          = "E1064";
		public final static String REQUIRED_CC_INSTRUMENT_ID		= "E1065";
		public final static String CHARGE_CURRENCY_CODE_REQ			= "E1066";
		public final static String CHARGE_TYPE_REQ					= "E1067";
		public final static String CHARGE_AMOUNT_REQ				= "E1068";
		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - end

		public final static String CONVERTED_TRANSACTION_PAYMENT      = "PAY";
		public final static String BUTTON_CONVERT                 	= "Convert";
		public static final String CONVERTED 						= "CONVERTED";
	    public final static String PANEL_AUTH_NOT_SETUP_FOR_PARENT = "E972";
		public final static String PANEL_AUTH_NOT_FOR_FTDP_OR_FTBA = "E973";
		public final static String PANEL_AUTH_NOT_FOR_INTERNATIONAL = "E974";
		public final static String PANEL_AUTH_NOT_FOR_CONF_FTDP_OR_FTBA = "E975";
		public final static String PANEL_AUTH_NOT_FOR_CONF_INTERNATIONAL = "E976";
		public final static String PANEL_GROUP_NOT_PRESENT = "E977";

		public final static String APPROVER_SEQ_FIX_FIRST   =  "FIX_FIRST";
		public final static String APPROVER_SEQ_FIX_LAST   =  "FIX_LAST";
		public final static String APPROVER_SEQ_FIX_ALL   =  "FIX_ALL";
		public final static String APPROVER_SEQ_NOT_FIXED   =  "NOT_FIXED";
		public final static String AUTHORISER_NOT_SPECIFIED = "E1020";
		public final static String INVOICES_SUMMARY_DATA = "INVOICES_SUMMARY_DATA";
		public final static String PAY_REMIT = "PAY_REMIT";
		public final static String TRANSACTION = "TRANSACTION";
		public final static String INVOICE_OFFER_GROUP = "INVOICE_OFFER_GROUP";

		public final static String PANEL_REF_DATA_CHANGED = "E1021";
		public final static String SUPPLIER_PORTAL = "SUPPLIER_PORTAL";
		public final static String RECEIVABLES_CREDIT_NOTE = "RECEIVABLES_CREDIT_NOTE";

		public final static String INV_RESET_TO_AUTHORIZE		    		= "I510";
		public final static String MULTIPLE_PANEL_GROUP_FOUND		        = "E1022";
		public final static String PAYMENT_INSTR_GROUP_TYPE = "PYMTS";
		public final static String PAYMENT_PANEL_GROUP_TYPE = "PYMT";
		public final static String INT_PANEL_GROUP_TYPE = "INT_PYMT";

		   
		// T36000013446- use custom http header to communicate CMA specific status codes - start
		public final static String CUSTOM_STATUS_HEADER = "Custom-Status";
		   
		// T36000013446- use custom http header to communicate CMA specific status codes - end

		//Panel instrument groups
	    public final static String PANEL_INSTR_GROUP__ALL = "ALL";
		public final static String PANEL_INSTR_GROUP__TRADE = "TRADE";
	    public final static String PANEL_INSTR_GROUP__PAYMENT = "PYMTS";
	    public final static String PANEL_INSTR_GROUP__RECEIVABLES = "RECV";

	    //IR T36000019854 Rel 8.3
	    public final static String PANEL_AUTH_BULK_PAYMENTS_NOT_ALLOWED = "E1023";
	    public final static String PANEL_AUTH_CAN_NOT_PROCESS = "E1024";
	    public final static String INVALID_ISSUE_DATE_INFO		 =  "I511"; //IR T36000015470
	    public final static int NUMBER_OF_PANEL_GROUP_FOR_ACCT   = 16;
	    //IR 18534 - start
	    public final static String RATE_TYPE_REQUIRED			= "E1025";
	    public final static String SP_RATE_TYPE_REQUIRED		= "E1026";
	    public final static String TP_RATE_TYPE_REQUIRED		= "E1027";
	    //IR 18534 - end
	    public final static String MISSING_ATTACHED_INVS = "E1028";
	    public final static String LRQ_INV_TENOR_DAYS_GREATER = "E1029"; //IR 21174

	    public final static String PENDING_USER_PREFERENCES = "E1052"; //Rel 8.3 IR T36000021112

	    //SSikhakolli - Rel-8.3 SysTest IR#T36000021156 on 10/03/2013 - Begin
	    public final static String PANEL_AUTH_GROUP_CANNOT_DELETE = "E1030";
	    //SSikhakolli - Rel-8.3 SysTest IR#T36000021156 on 10/03/2013 - End
                 public final static String PANEL_GROUP_ACCT_CURR_BASIS = "ACCT_CCY";
	    public final static String PANEL_GROUP_BASE_CURR_BASIS = "BASE_CCY";

	  //JGADELA REL 8.3 IR T36000019854
	  	    public final static String CAN_NOT_TURN_OFF_DUAL_CONTROL = "E1040";
	   public final static String MISMATCH_INST_TYPE_USAGE = "E1033";


	  	  public final static String PASSWORD_CONTAINS_ERROR               = "E1041";	 //RKAZI REL 8.3 10/09/2013 T36000021563 - Added
	  	 public final static String PASSWORD_WEAK_ERROR               = "E1042";	 //RKAZI REL 8.3 10/09/2013 T36000021563 - Added
	  	public final static String NO_MATCH_RULE_INV     = "I512";
	  	public final static String BUTTON_VIEW_INV = "ViewInvoiceButton";
	    //@KMehta 10/15/2013 Rel8400 CR-910 Start
	    public final static String EXPIRY_END_DATE_REQUIRED = "E1043";
	    // DK CR-886 Rel8.4 10/15/2013 starts
		public final static String PMT_BENE_ACCT_NUM = "A";
		public final static String PMT_BENE_ORDER = "O";
		// DK CR-886 Rel8.4 10/15/2013 ends


		//Srinivasu_D CR-599 Rel8.4 11/20/2013  - Start
		public final static String PAY_MGMT_DIR_REQUIRED				 = "E1060";
		public final static String PAYMENT_MGMT_FILE_DEF				 = "P";
		public final static String PAYMENT_MGMT_PILE_FIXED_FORMAT		 = "O";
		public final static String PAYMENT_MGMT_NOT_RESTRICTED_DIRECTORY = "N";
	    public final static String PAYMENT_MGMT_RESTRICTED_DIRECTORY	 = "Y";
		public final static String PAY_DEF_DUPLICATE_FOUND				 = "E1061";
		//Srinivasu_D CR-599 Rel8.4 11/20/2013  - End

		//Rel-9.2 IR#T36000021894 Vsarkary uncommenting to validate and load delivery instructions dynamically- Start
		//Rel-8.4.0.1 IR# T36000028107. Backing out changes done under T36000021894 - Begin
		//Rel-8.4 IR# T36000021894 on 12/04/2013 - Begin
		public final static String DELIVARY_TO_APPLICANT_OPTION = "Applicant";
		public final static String DELIVARY_TO_BENEFICIARY_OPTION = "Beneficiary";
		public final static String DELIVARY_TO_AGENT_OPTION = "Agent";
		public final static String DELIVARY_TO_OTHER_OPTION = "Other";
		public final static String DELIVARY_BY_TELEX_SWIFT_OPTION = "Telex/Swift";
		public final static String DELIVARY_BY_SWIFT_OPTION = "Swift";
		public final static String DELIVARY_BY_REGISTERED_MAIL_OPTION = "Registered Mail";
		public final static String DELIVARY_BY_MAIL_OPTION = "Mail";
		public final static String DELIVARY_BY_COURIER_OPTION = "Courier";
		//Rel-8.4 IR# T36000021894 on 12/04/2013 - End
		//Rel-8.4.0.1 IR# T36000028107. Backing out changes done under T36000021894 - end
		//Rel-9.2 IR#T36000021894 Vsarkary uncommenting to validate and load delivery instructions dynamically- End
		// DK CR-804 Rel8.4 12/31/2013 starts
		public final static String INVALID_PIN           = "E983";
		public final static String NEW_PIN_FAIL           = "E984";
		public final static String NEW_PIN_REJECT           = "E985";
		// DK CR-804 Rel8.4 12/31/2013 ends

	    public final static String REFDATA__PAYMENTDEFN = "PMD";

        public final static String PAYMENT_FILE_TYPE_FIX       = "FIX";
        public final static String PAYMENT_FILE_TYPE_DELIMIT       = "DLM";
        public final static String PAYMENT_FILE_TYPE_ABA       = "ABA";
        public final static String PAYMENT_FILE_TYPE_PNG       = "PNG";
        public final static int PMT_MTHD_SUMMARY_NUMBER_OF_FIELDS               = 49;
        public final static int PMT_HDR_SUMMARY_NUMBER_OF_FIELDS               = 10;//CR-921 Rel9.0 Added 2 new FX fields to the header
        public final static int PMT_PAY_SUMMARY_NUMBER_OF_FIELDS               = 54;
        public final static int PMT_INV_SUMMARY_NUMBER_OF_FIELDS               = 2;
    	public final static String PAYMENT_DATE_FORMAT                         = "PAYMENT_DATE_FORMAT";
    	public final static String NO_PAYMENT_DEFINITION_IDENTIFIED            = "E1071";
    	public final static String NEW_PIN_LOGIN            = "E1072"; // DK IR T36000024146 Rel8.4 02/11/2014
    	public final static String PAY_DEF_MAX_FIELD_SIZE_EXCEEDED				="E1073";
		//Narayan CR-831 Rel9.0 01/09/2014  - Start
		public final static String EXPIRY_DATE_MUST_BE_LESS_THAN            = "E999";
		public final static String EXPIRY_DATE_OR_MAX_NUM_NOT_PRESENT_WARN  = "W661";
		public final static String DAY    = "DAY";
		public final static String MAX_AUTO_EXTENSION_ALLOWED    = "999";
		//Narayan CR-831 Rel9.0 01/09/2014  - End
		//Narayan CR-913 Rel9.0 01/28/2014  - Start
		public final static String DUPLICATE_PAY_INV_CURR            = "E986";
		public final static String BUTTON_MODIFY_SUPPLIER_DATE         = "ModifySupplierDate";
		public final static String BUTTON_RESET_SUPPLIER_DATE          = "ResetSupplierDate";
		//Narayan CR-913 Rel9.0 01/28/2014  - End
	//Rel9.0 CR 913 Start
	public final static String ERROR_SELECT_PAYABLE_INVOICES_UPLOAD_FILE = "E2000a";
	public final static String ERROR_SELECT_RECEIVABLE_INVOICES_UPLOAD_FILE = "E2000b";
	public final static String ERROR_SELECT_INVOICES_UPLOAD_FILE_TYPE = "E2000c";
	//Rel9.0 CR 913 End
		// CR 913 start
		public final static String INV_LINKED_INSTR_PYB                      =  "PYB";
		public final static String PAY_PGM_BUTTON_AUTHORIZ				     =  "PayPgmAuthorise";
		public final static String PAY_PGM_BUTTON_MODIFY_SUPP_DATE		     =  "PayPgmModifySupplierDate";
		public final static String PAY_PGM_BUTTON_RESET_SUPP_DATE			 =  "PayPgmResetSupplierDate";
		public final static String PAY_PGM_BUTTON_APPLY_PAYMENT_DATE		 =  "PayPgmApplyPayDate";
		public final static String PAY_PGM_BUTTON_CLEAR_PAYMENT_DATE		 =  "PayPgmClearPayDate";
		public final static String PAY_PGM_BUTTON_DELETE_INVOICES			 =  "PayPgmDeleteInvoice";
		public final static String PAY_PGM_BUTTON_ATTACH_DOCUMENT			 =  "PayPgmAttachDocument";
		public final static String PAY_PGM_BUTTON_DELETE_DOCUMENT			 =  "PayPgmDeleteDocument";
		public final static String PAY_PGM_BUTTON_RESET_TO_AUTHORIZE		 =  "PayPgmResetToAuth";
		public final static String INV_LINKED_INSTR_PAY                      =  "PAY";
		public final static String PAYABLES_TRANSACTION_TYPE                 =  "PPY";
		public final static String PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE                =  "PAYB";
		public final static String SELLER_FINANCE_INSTRUMENT_TYPE            =  "SFN";
		public final static String DASHBOARD_SECTION_DEBIT_FUNDING_MAIL     = "DEBITFUND";
		public final static String PAYABLES_PROGRAM_INVOICE     = "PPI";
		public final static String PAYABLES_MANAGEMENT_INVOICE     = "PMI";
		public final static String EMAIL_TRIGGER_PAYABLES_INVOICE   = "PAYABLES_EMAIL";
		public final static String EMAIL_PRE_DEBIT_FUNDING   = "PRE_DEBIT";

		public final static String PAYABLES_INVOICE_ACTION_RECEIVED   = "INV_RECV";
		public final static String INVALID_BUYER_ACCOUNT   = "E1074";
		public final static String DEACTIVATED_BUYER_ACCOUNT   = "E1075";
		public final static String BUYER_ACCOUNT_CURR_REQUIRED   = "E1076";
		public final static String FAIL_REPLACEMENT_INVOCIE   = "E1077";
		public final static String NO_PAYABLE_INVOICE_PAY_INSTR = "E1078";
		public final static String FOR_PAY_PROG  = "P";
		public final static String PAYABLE_MGMT_INVALID_STATUS = "E1079";
		public final static String PAY_MGMT_BUTTON_RESET_PAY_AMOUNT = "ResetEarlyPayAmt";
		public final static String PAY_MGMT_BUTTON_RESET_SUPP_DATE = "ResetSupplierDate";
		public final static String PAY_INV_BUTTON_MODIFY_SUPP_DATE	=  "ModifySupplierDate";
		public final static String PAY_INV_BUTTON_APPLY_PAY_AMT	=  "ApplyEarlyPayAmt";
		public final static String PAY_INV_INFO_CLEAR_FIELD_VALUE	=  "I513";
		public final static String PAY_INV_INFO_APPLY_FIELD_VALUE	=  "I514";
		public final static String INVALID_SUPPLIER_DATE	=  "E1081";
		public final static String UPLOAD_INV_ACTION_MODIFY_SUPPLIER_DATE      = "MSD";
		public final static String UPLOAD_INV_ACTION_RESET_SUPPLIER_DATE      = "RSD";
		public final static String UPLOAD_INV_ACTION_APPLY_PAYMENT_AMT      = "APA";
		public final static String UPLOAD_INV_ACTION_RESET_PAYMENT_AMT      = "RPA";
		public final static String PAY_MGMT_INVALID_AUTHORISATION	=  "E1082";
		public final static String NAV__PAY_INVOICES                 = "PI";
		public final static String NAV__PAY_INQUIRIES                 = "PH";
		public final static String NAV__INSTR_INQUIRIES                 = "PHI";
		public final static String NAV__PYMTS_INQUIRIES                 = "PHP";
		public final static String INVOICE_STATUS_AUTHORISED       = "AUTH";
		public final static String INVALID_SUPPLIER_DATE_WITH_SYS_DATE	=  "E1083";
		// R 94 CR 932
		public final static String NAV__BILLING_INSTR_HISTORY_INQUIRIES                 = "BHI";
		// CR 913 end
		public final static String PAY_PRGM_RESET_SUPP_DATE = "I516";
		public final static String PAY_PRGM_RESET_PAYMENT_AMT = "I518";
		public final static String PAY_PRGM_MODIFY_SUPP_DATE = "I515";
		public final static String PAY_PRGM_APPLY_PAYMENT_AMT = "I517";
		public final static String PAYABLE_INV_LIST_TAB				  = "LP";
		public final static String UPLOAD_INV_ACTION_RESET_AUTH      = "RESET_AUTH";
		public final static String INVALID_INSTRUMENT_TYPE      = "E1084";
		public final static String PAYABLE_MGMT_INVALID_ACTION = "E1085";
		public final static String INVALID_PAYMENT_AMOUNT	=  "E1086";
		public final static String INVALID_GROUP_AUTH	=  "E1087";
		public final static String PAY_PGM_BUTTON_AUTH_DETAIL_LIST    =  "PayPgmDetailListAuthorise";
		public final static String PANEL_GROUP_DELETE_ASSIGNED_INVOICES = "W662";
		//Rel 9.0 - CR 913 - Start
		public final static String PAYABLES_CREATE = "APM";
		public final static String PAYABLES_CHANGE = "CHP";
		public final static String PAYABLES_PAYMENT = "PPY";
		public final static String SUPPLIER_PORTAL_CREATE = "CUS";
		public final static String SUPPLIER_PORTAL_LIQUIDATE = "LIQ";

		public final static String PAYABLE_MANAGEMENT     = "PYB";
		public final static String SUPPLIER_PORTAL_MANAGEMENT     = "LRQ";
		//Rel 9.0 - CR 913 - End
		//Rel9.0 CR-921
		public final static String FX_RATE_VALUE_MISSING		 = "E1089";
		public final static String FX_CONTRACT_VALUE_MISSING	 = "E1090";
		public final static String FX_CONTRACT_INVALID_FORMAT	 = "E1091";
		public final static String FX_REQUIRED					 = "E1092";
		public final static String FX_CONTRACT_SIZE_EXCEEDED	 = "E1093";

		//Rel9.0 IR-T36000027060
		public final static String PANEL_AUTH_CAN_NOT_PROCESS_INVOICE = "E2000d";

		public final static String PAY_PRGM_DETAIL_PAGE     =  "PayableProgramInvoiceDetail";
		public final static String PAYABLE_MGMT_INVALID_FIN_STATUS = "E1094";

		public final static String ALL_TRANS_HOME     =  "AllTransactionsHome"; // DK IR T36000024626 Rel9.0 05/05/2014

		public final static String PAY_PRGM_MODIFY_PAYMENT_DATE = "I519";
		public final static String PAY_PRGM_RESET_PAYMENT_DATE = "I520";

		public final static String TRADE_PORTAL_HOME     =  "TradePortalHome"; // DK IR T36000026794 Rel9.0 05/29/2014


		public final static String INVALID_SENDER_ID  			   = "E1097"; //MEer Rel9.0 IR-28809
		public final static String SP_STATUS_BUYER_APPROVED_ALLELIGIBLE			  =  "BUYER_APPROVED_ALLELIGIBLE";
		
		public final static String INSTR_GROUP__PAYABLES = "PAYB";
		public final static String PAYMENT_METHOD_IACC = "IACC";
		//Rel9.0 IR-T36000029301
		public final static String MISSING_PAYABLES_INVOICE_GROUP_INDICATOR = "E2000e";
		public final static String AUTH_TRANS_NOT_UPDATEABLE	 = "E1098";
		public final static String PAY_INV_LIST_BUTTON_APPLY_PAY_AMT	=  "ListApplyEarlyPayAmt";
		public final static String PAY_INV_LIST_BUTTON_RESET_PAY_AMT	=  "ListResetEarlyPayAmt";
		public final static String CANNOT_BE_ZERO	 = "E1103"; // DK IR T36000013357 Rel9.1
    	public final static String PERF_STAT_CONFIG_CACHE = "perfStatConfigCache";
		public final static String NO_ACTION_MATCH_NOTICE_AUTH     = "E1105";
		
		public final static String NO_PAY_INVOICES     = "E1201";
		public final static String CREDIT_NOTE_UNABLE_ULOAD     = "E1202";
		public final static String END_TO_END_ID_REQ     = "E1203";
		public final static String DUPLICATE_CREDIT_NOTE_ID   = "E1205";
		
		public final static String CREDIT_NOTE_STATUS_AVA     = "AVA";
		public final static String CREDIT_NOTE_STATUS_PBB     = "PBB";
		public final static String CREDIT_NOTE_STATUS_PBB_BAL  = "PBL";
		public final static String CREDIT_NOTE_STATUS_AWB     = "AWB";
		public final static String CREDIT_NOTE_STATUS_DEC    = "DEC";
		public final static String CREDIT_NOTE_STATUS_AUT    = "AUT";
		public final static String CREDIT_NOTE_STATUS_PAUT    = "PAUT";
		public final static String CREDIT_NOTE_STATUS_FAUT    = "FAUT";
		public final static String CREDIT_NOTE_STATUS_CLO    = "CLO";
		public final static String CREDIT_NOTE_STATUS_DEL    = "DEL";
		public final static String CREDIT_NOTE_STATUS_STB    = "STB";
		public final static String CREDIT_NOTE_STATUS_ASN    = "ASN";
		
		public final static String DELETE_INVOICES_FROM_INST   = "E1108";

		//R92-CR914
		public final static String CREDIT_NOTE_SEQUENTIAL_TYPE_INVOICE_ID     = "IL";
		public final static String CREDIT_NOTE_SEQUENTIAL_TYPE_DATES   		  = "DL";
		
		public final static String CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_HIGHTOLOW      = "AH";
		public final static String CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_LOWTOHIGH      = "AL";
		
		public final static String CREDIT_NOTE_APPLICATION_SEQUENTIAL     = "S";
		public final static String CREDIT_NOTE_APPLICATION_PROPOTIONATE     = "P";
		

		public final static String UNUTILIZED_CREDIT_NOTE_AVAILABLE_DAYS_REQUIRED     = "E1800";
		public final static String CREDIT_NOTE_MANUAL_ID_PROCESS_MUTUALLY_EXCL     = "E1801";
		public final static String CREDIT_NOTE_MANUAL_NOT_AVAILBLE     = "E1802";
		public final static String CREDIT_NOTE_SEQUENTIAL_METHOD_REQUIRED     = "E1803";
		public final static String UNUTILIZED_CREDIT_NOTE_AVAILABLE_DAYS_INVALID     = "E1804";
		public final static String CREDIT_NOTE_END_TO_END_OR_MANUAL_MANDATORY     = "E1805";
		public final static String CREDIT_NOTE_PROPORTINATE_SEQ_MANDATORY     = "E1806";
 		public final static String CREDIT_NOTE_INV_AUTH_NOT_MATCHING     = "E1807";
 
 		public final static String CREDIT_NOTE_APPLIED_STATUS_UAP    = "UAP";
		public final static String CREDIT_NOTE_APPLIED_STATUS_PAP    = "PAP";
		public final static String CREDIT_NOTE_APPLIED_STATUS_FAP    = "FAP";
		public final static String CREDIT_NOTE_UTILIZED_STATUS_UNU   = "UNU";
		public final static String CREDIT_NOTE_UTILIZED_STATUS_PUT   = "PUT";
		public final static String CREDIT_NOTE_UTILIZED_STATUS_FUT   = "FUT";
		public final static String SELECT_SINGLE_CREDIT_NOTE    = "E1206";
		public final static String CANNOT_APPLY_PAY_AMT    = "E1207";
		public final static String NAV__PAY_CREDIT_NOTES = "PC";
		public final static String CREDIT_NOTE_APPLIED_STATUS_TYPE   = "CNA";
		public final static String CREDIT_NOTE_UTILIZED_STATUS_TYPE   = "CNU";
		public final static String CANNOT_APPLY_CR_NOTE    = "E1214";
		public final static String CANNOT_UNAPPLY_CR_NOTE    = "E1215";
		public final static String FULLY_APPLIED_CR_NOTE    = "E1210";
		public final static String END_TO_END_CR_NOTE    = "E1211";
		public final static String EXPIRED_CR_NOTE    = "E1212";
		public final static String INCOMPLETE_PAY_MGMT    = "E1213";
		public final static String BUTTON_DELETE_PAY_CREDIT_NOTE   = "DeletePayCreditNote";
		public final static String BUTTON_CLOSE_PAY_CREDIT_NOTE   = "ClosePayCreditNote";
		public final static String BUTTON_RESET_TO_AUTHORIZE_CREDIT_NOTE		 =  "ResetToAuthPayCreditNote";
		public final static String BUTTON_ATTACH_DOCUMENT_CREDIT_NOTE            = "AttachDocumentPayCreditNote";
		public final static String BUTTON_DELETE_DOCUMENT_CREDIT_NOTE            = "DeleteDocumentPayCreditNote";
		public final static String BUTTON_AUTHORIZE_CREDIT_NOTE                  = "AuthorizePayCreditNote";
        public final static String CREDIT_NOTE_EXPIRY_DATE_EXPIRED   = "E1208";
		public final static String CREDIT_NOTE_EXPIRY_DATE_NOT_EXPIRED   = "E1209";
		public final static String PAYABLES_CREDIT_NOTE     = "PCN";

		public final static String CREDIT_NOTE_STATUS = "CREDIT_NOTE_STATUS";
		public final static String CREDIT_NOTE_UTILISED_STATUS = "CREDIT_NOTE_UTILISED_STATUS";
		public final static String CREDIT_NOTE_APPLIED_STATUS = "CREDIT_NOTE_APPLIED_STATUS";
		public final static String CREDIT_NOTE_PURGE= "PurgeCrNotes";
		public final static String INVOICE_PURGE= "PurgeInvoices";

		public final static String INVALID_PAY_AMT_WITH_CRNOTE    = "E1216";
		public final static String CRNOTE_APPLIED_SUCCESSFULLY   = "I521";
		public final static String CRNOTE_UNAPPLIED_SUCCESSFULLY   = "I522";
		public final static String PARTIALLY_AUTH_CRNOTE_ERROR    = "E1217";
		public final static String PARTIALLY_AUTH_INV_ERROR    = "E1218";
		public final static String CRNOTE_INVALID_STATUS_ERROR    = "E1219";
		public final static String UPLOAD_CR_ACTION_FILE_UPL                = "UPL_CR";
		public final static String UPLOAD_CR_ACTION_CLOSE                = "CLO_CR";
		public final static String INVALID_DATE_FOR_END_TO_END_GROUP    = "E1220";
		public final static String UPLOAD_CR_ACTION_APPLY         = "APL_CR";
		public final static String UPLOAD_CR_ACTION_UNAPPLY       = "UAP_CR";
		public final static String CREDIT_NOTE_NEW                = "N";
		public final static String CREDIT_NOTE_UPDATE             = "U";
		public final static String AUTH_END_TO_END_ID_GROUP             = "I523";
		public final static String PARTIALLY_AUTH_END_TO_END_ID_GROUP   = "I524";
		public final static String USER_AUTH_LEVEL_FIRST  = "F";
		public final static String USER_AUTH_LEVEL_SECOND = "S";
		public final static String CREDIT_NOTE = "C";
		public final static String PAYABLE_INVOICE = "I";
		public final static String PAYABLE_UPDATE_ACTIVY = "UPD";
		public final static String PAYABLE_ASSIGN_ACTIVY = "ACN";
		public final static String DUPLICATE_CREDIT_NOTE = "E1221";
		public final static String GENERIC_END_TO_END_ERROR_MSG = "E1222";
		public final static String END_TO_END_ID_EXISTS_ERROR_MSG = "E1223";
		public final static String CREDIT_NOTE_WITH_INVOICE_ID_EXISTS = "E1224";
		public final static String INVOICE_WITH_CREDIT_NOTE_ID_EXISTS = "E1225";
		public final static String INVOICE_WITH_DIFF_DUE_OR_PAYMENT_DATE = "E1226";
		public final static String NO_INVOICE_TO_APPLY = "MID022";
		
		//CR914A-R9.2
		public final static String INV_CRNOTE_PURGE_ERROR = "INV_CRNOTE_PURGE_ERROR";
		
		public final static String CREDIT_NOTE_AMOUNT = "CNA";
		public final static String REMAINING_AMOUNT = "RMA";
		//XSS Rel 9.2 Added Blank constant
		public final static String BLANK                = "";
		public final static String INFO_SHARED_BANK_CHARGES ="I525";
		/*pgedupudi - Rel-9.3 CR-976A 03/26/2015 Start*/
		public final static String USER_ORG_IS_RESTRICTED ="E1808";
		public final static String ONE_BANK_GROUP_MUST_SELECTED ="E1809";
		public final static String BANK_GROUP_SELECTED_ONCE ="E1810";
		public final static String BANK_GROUP_RESTRICT_RULE_NOT_DELETED ="E1811";
		/*pgedupudi - Rel-9.3 CR-976A 03/26/2015 End*/
		//CR1006 start
		public final static String UPLOAD_INV_STATUS_RAA     = "RAA";
		public final static String UPLOAD_INV_STATUS_DBC     = "DBC";
		public final static String UPLOAD_INV_STATUS_PDC     = "PDC";
		public final static String UPLOAD_INV_STATUS_DCF     = "DCF";
				

		// Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added Error Code
		public final static String  DEFAULT_DISCOUNT_RATE_DUPLICATE		   = "E2010";
		public final static String REMIT_IND_TRUE   	 	 = "true";
//		public final static String REMIT_IND_FALSE   	 	 = "false";
		public final static String AUTO_PAY_MATCH			 = "Match";
		public final static String AUTO_PAY_UNMATCH			 = "UnMatch";
		public final static String AUTO_PAY_SEARCH			 = "Search";
		// Srinivasu_D CR#997 Rel9.3 03/10/2015 - End
		//CR999 start
		public final static String INVOICE_WITH_DIFF_PAYMENT_METHOD = "E2013";
		//CR99 end
		public final static String INVALID_INV_CN_EMAIL_FREQUENCY	 =  "E2011";
		public final static String EMAIL_INV_CN_FREQUENCY_MISSING	 =  "E2012";

		//pgedupudi - Rel9.3 CR1006 05/06/2015
		public static final String INVOICE_PAYMENT_STATUS_UAP = "UAP";
		public static final String INVOICE_PAYMENT_IS_UNAPPROVED = "E2014";
		public final static String DELETED_INVOICE = "E2019";
		public final static String DIFFERENT_ENDTOENDID = "E2020";
		public final static String PAUT_INVOICE = "E2021";
		public final static String APPLIED_INVOICE = "E2022";
		public final static String INVOICE_IND = "I";
		public final static String DEC_WORK_GROUP_REQUIRED      = "E2023";
		public final static String PAY_PGM_BUTTON_DECLINE		 =  "DeclinePayInv";
		public final static String REC_PGM_BUTTON_DECLINE		 =  "DeclineRecInv";
		public final static String PAY_PGM_DETAIL_BUTTON_DECLINE		 =  "DeclinePayInvDetail";
		public final static String REC_PGM_DETAIL_BUTTON_DECLINE		 =  "DeclineRecInvDetail";
		public static final String BUTTON_DECLINE_INVOICE = "DeclinePayInvoice";
		public static final String PAYMENT_DATE_CANNOT_BE_MODIFIED = "E2016";
		public static final String H2H_CREDIT_NOTES_CANNOT_BE_APPLIED = "E2017";
		public static final String CANNOT_DECLINE = "E2024";
		public static final String BUTTON_CR_NOTE = "DeclineCRNote";
		public final static String REC_GROUP_BUTTON_DECLINE		 =  "DeclineRecGroupInv";
		public final static String DECLINED_INVOICE = "I526";
		public final static String DECLINED_CRNOTE = "I527";
		public final static String TP_RULE_FOR_AUTH = "E2018";
        public final static String SP_INVOICE_OFFER_FUTURE_DATE							= "E2025";
        public final static String DPT_CBFT_BENE_NAME_MAX_LENGTH							= "E2065";		// DK IR T36000046030 Rel9.5
                           //Nar CR 1029 06/12/2015 Rel 9.3.5.0 Begin
		// Bank Update Transaction status
		public static final String BANK_TRANS_UPDATE_STATUS_APPLIED       = "APPLIED";
		public static final String BANK_TRANS_UPDATE_STATUS_NOT_STARTED   = "NOT_STARTED";
		public static final String BANK_TRANS_UPDATE_STATUS_IN_PROGRESS   = "IN_PROGRESS";
		public static final String BANK_TRANS_UPDATE_STATUS_REPAIR        = "REPAIR";
		public static final String BANK_TRANS_UPDATE_STATUS_APPROVED      = "APPROVED";   
		// Approve status for Bank Transaciton history
		public static final String BANK_TRANS_H_UPDATE_STATUS_APPROVED_PBB      = "APPROVED_PBB"; 
		public static final String BANK_TRANS_H_UPDATE_STATUS_APPROVED_RBB      = "APPROVED_RBB"; 
		// Bank User Action
		public static final String BANK_ADMIN_ACTION_SAVE                 = "SAVE";
		public static final String BANK_ADMIN_ACTION_SENT_FOR_REPAIR      = "SENT_FOR_REPAIR";
		public static final String BANK_ADMIN_ACTION_APPROVE              = "APPROVE";
		public static final String BANK_ADMIN_ACTION_APPLIED              = "APPLIED";
		public static final String BANK_ADMIN_ACTION_AUTHORISED_BY_CUST   = "AUTHORISED_BY_CUST";
		//Nar CR 1029 06/12/2015 Rel 9.3.5.0 End
		
		//MEer Rel 9.3.5 CR-1027
		public final static String APPLICATION_PDF_BASIC_TYPE		 =  "B";
		public static final String APPLICATION_PDF_EXPANDED_TYPE = "E";
		public final static String ONLY_ONE_PDF_TYPE_ALLOWED  = "E2052";
		
		//Rel 9.3.5 - CR 1029 Start
		public final static String BANK_UPDATE_STATUS_TYPE		 =  "BANK_UPDATE_STATUS_TYPE";
		public final static String BANK_TRANSACTION_STATUS		 =  "BANK_TRANSACTION_STATUS";
		public final static String BANK_TRANSACTION_STATUS_REJECTED_BY_BANK		 =  "REJECTED_BY_BANK";
		public final static String BANK_TRANSACTION_STATUS_PROCESSED_BY_BANK		=  "PROCESSED_BY_BANK";
		public final static String BUTTON_APPLY_UPDATES		 =  "ApplyUpdatesButton";
		public final static String BUTTON_APPROVE		 =  "ApproveButton";
		public final static String BUTTON_BANK_SEND_FOR_REPAIR		 =  "SendForRepairButton";
		public final static String BUTTON_BANK_EDIT		 =  "BankEditButton";	
		public final static String BUTTON_BANK_ATTACH_DOCUMENTS		 =  "AttachDocuments";	
		public final static String DIFFERENT_APPROVER_REQUIRED  = "E2029";
		public final static String APPLY_UPDATES  = "I528";
		public final static String APPROVED  = "I529";
		public final static String BANK_TASK_NOT_SUCCESSFUL  = "E2030";
		public final static String STRING_LENGTH_EXCEED  = "E2031";
		//Rel 9.3.5 - CR 1029 End
        public static final String DOWNLOAD_AUTHORISED_TRANSACTION_XML   = "DOWNLOAD_AUTHORISED_TRANSACTION_XML";
        public final static String DOWNLOAD_ZIP_CONTENT_TYPE     = "application/zip";
        
        //SSikhakolli - Rel 9.3.5 CR 1029
        public final static String CANNOT_UNCHECK_BANK_ADMIN_UPDATE_SYNC  = "E2026";
        public final static String CANNOT_UNCHECK_ENABLE_ADMIN_UPDATE_CENTRE  = "E2027";
        public final static String AUTHENTICATION_FAILED  = "E1109";
        public final static String ADMIN_AUTH_METHOD_NOT_MATCH = "W664";
//Rpasupulati IR T36000043200
        public final static String   FROM_OTHER_DATE_NOT_ENTERED				= "W665";  
        public final static String   TO_NEW_VALIDITY_DATE_NOT_ENTERED  			= "W666";
        public final static String   TO_END_DATE_NOT_ENTERED  			= "W667";
        public final static String BANK_ACCEPTANCE 	= "ACC";
		public final static String BANK_DEFERED_PAY 	= "DFP";
		public final static String BANK_DAYS 	= "DAY";
		public final static String COLUMN_LENGTH_IN_HEADER_EXCEEDS        = "E2079";
		
		public static final String BANK_IN_PROGRESS       = "BANK_IN_PROGRESS";
		public static final String BANK_APPLIED   = "BANK_APPLIED";
		public static final String BANK_APPROVED   = "BANK_APPROVED";
		public static final String BANK_REPAIR        = "BANK_REPAIR";
		public static final String BANK_INSTRUMENT_LOCKED      = "BANK_INSTRUMENT_LOCKED"; 
		public final static String LOAN_AMOUNT_REQD         = "E2033";


        public final static String SETTLEMENT_REQ_PAYMENT   = "P";
        public final static String SETTLEMENT_REQ_LIQUIDATE = "L";
        public final static String SETTLEMENT_REQ_ROLLOVER  = "R";
        
        //SURREWSH - Rel 9.4 CR 932
        public final static String BILLING        = "BIL";
        public final static String NEWBILL        = "BNW";
        public final static String CHANGEBILL     = "BCH";
        public final static String COLLECTBILL    = "BCA";
        public final static String SETTLEMENT_WORK_ITEM_TYPE_ROLLOVER   = "WRO";
        public final static String SETTLEMENT_WORK_ITEM_TYPE_LIQUIDATE  = "WSL";
        public final static String SETTLEMENT_WORK_ITEM_TYPE_PAYMENT    = "WSP";
        public final static String SETTLEMENT_WORK_ITEM_TYPE_RESPONSE   = "WSR";
        public final static String SETTLEMENT_ACCOUNT_REMIT_TYPE_ACCOUNT   = "A";
        public final static String SETTLEMENT_ACCOUNT_REMIT_TYPE_REMITTED  = "R";
        public final static String SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_FULL     = "F";
        public final static String SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL  = "P";
        public final static String SETTLEMENT_DAILY_RATE_FEC_TYPE_DAILY      = "D";
        public final static String SETTLEMENT_DAILY_RATE_FEC_TYPE_FXCONTRACT = "F";
        public final static String SETTLEMENT_DAILY_RATE_FEC_TYPE_OTHER      = "O";
        public final static String SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL = "P";
        public final static String SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE = "F";
        public final static String SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL = "R";
        public final static String SETTLEMENT_ROLLOVER_TERMS_DAYS = "D";
        public final static String SETTLEMENT_ROLLOVER_TERMS_MATURITY_DATE = "M";
        public final static String SETTLEMENT_PAYMENT_DATE_REQ = "E2031";
        public final static String SETTLEMENT_PAYMENT_ACCT_MODE_REQ = "E2032";
        public final static String SETTLEMENT_DEBIT_ACCT_REQ = "E2033";
        public final static String SETTLEMENT_FX_RATE_REQ = "E2034";
        public final static String SETTLEMENT_FX_CONTRACT_DETAIL_REQ = "E2035";
        public final static String SETTLEMENT_OTHER_FEC_TEXT_REQ = "E2036";
        public final static String SETTLEMENT_FINANCE_CURR_REQ = "E2037";
        public final static String SETTLEMENT_FIN_FULL_OR_PARTIAL_IND_REQ = "E2038";
        public final static String SETTLEMENT_PARTIAL_PAY_AMT_REQ = "E2039";
        public final static String SETTLEMENT_FIN_DAYS_OR_DATE_REQ = "E2040";
        public final static String SETTLEMENT_FIN_DAYS_REQ = "E2041";
        public final static String SETTLEMENT_FIN_MATURITY_DATE_REQ = "E2042";
        public final static String SETTLEMENT_ADDL_INSTR_TEXT_REQ = "E2043";
        public final static String SETTLEMENT    = "SIT";
        public final static String IMP_COL    = "imp_col";
        public final static String SETTLEMENT_INSTR_REQ    = "E2049";
        public final static String SETTLEMENT_ROLL_FULL_OR_PARTIAL_IND_REQ = "E2053";
        public final static String SETTLEMENT_ROLL_DAYS_OR_DATE_REQ = "E2054";
        public final static String SETTLEMENT_ROLL_DAYS_REQ = "E2055";
        public final static String SETTLEMENT_ROLL_MATURITY_DATE_REQ = "E2056";
        public final static String SETTLEMENT_FX_RATE_REQ_FOR_FIN = "E2057";
        public final static String SETTLEMENT_PAYMENT_DATE_CAN_NOT_PAST_DATE= "E2058";
        public final static String SETTLEMENT_FIXED_MATURITY_DATE_CAN_NOT_PAST_DATE = "E2059";

        //CR-1001 Rel9.4 START
        public final static String REPORTING_CODE_1_REQUIRED  = "E2044";
        public final static String REPORTING_CODE_2_REQUIRED  = "E2045";
        public final static String INVALID_REPORTING_CODE1  = "E2046";
        public final static String INVALID_REPORTING_CODE2  = "E2047";
        public final static String REPORTING_CODE_1_AND_2_REQUIRED  = "E2050";
        public final static String INVALID_REPORTING_CODE_1_AND_2  = "E2051";
        public final static String REPORTING_CODE_1_REQUIRED_INFO  = "I530";
        public final static String REPORTING_CODE_2_REQUIRED_INFO  = "I531";
        public final static String INVALID_REPORTING_CODE1_INFO  = "I532";
        public final static String INVALID_REPORTING_CODE2_INFO  = "I533";
        public final static String FILE_REPORTING_ERRORS_MUST_BE_CONFIRMED = "E2048";
        public final static String BUTTON_APPLY_UPDATES_REPORTING_CODES = "ApplyUpdates";
        //CR-1001 Rel9.4 END

         public final static String CERTIFICATE_ID = "C";
         public final static String GEMALTO = "G";
         public final static String SSO_REQUIRED_FOR_GEMALTO = "E1227";
         public final static String SSO_PASSWORD_SELECTED = "E1228";
         public final static String MISSING_CERTIFICATE_TYPE = "E2060";
         public final static String CAAS_AUTHLEVEL_PASSWORD = "5";
         
         /*CR-1123 Rel 9.5*/
         public final static String URL_REQD_TRD = "E2061";
         public final static String URL_REQD_PMNT_BEN = "E2062";
         public final static String URL_REQD_REPR_PANEL = "E2063";
         public final static String URL_REQD_PAYABLE_MGMT = "E2064";
		 //dpatra Rel-9.5 -added for regulatory account type CR-1051
         public final static String COPS_REGULATORY_ACCOUNT_TYPE = "REGULATORY_ACCOUNT_TYPE";
         
		 //Rel9.5 CR-927B START	
         public final static String MSG_INST_TYPE	      						= "MESSAGES";
         public final static String SUPP_PORT_INST_TYPE      					= "LRQ_INV";
         public final static String HTWOH_INV_INST_TYPE      					= "H2H_INV_CRN";
         public final static String PIN_TRANS_TYPE      					= "PIN";
         public final static String PCN_TRANS_TYPE      					= "PCN";
         public final static String RIN_TRANS_TYPE      					= "RIN";
		 public final static String MESSAGES      = "MESSAGES";
         public final static String NOTIF_RULE_YES        = "Y";
         public final static String DEFAULT_NOTIFY_SETTINGS_MISSING         = "E2070";
         public final static String APPLY_CLEAR_BUTTON_NOTIFY_MISSING       = "E2071";
		 public final static String SEND_MAIL_NOTIFICATION_MISSING          = "E2072";
         public final static String INSTRUMENT_NOTIFY_MISSING       		= "E2073";
		 public final static String SEND_MAIL_MISSING				        = "E2074";
         public final static String APPLY_CLEAR_BUTTON_MISSING		        = "E2075";
         public final static String USER_ID_MISSING                         = "E2076";
         public final static String DEFAULT_USER_ID_MISSING                 = "E2077";
         public final static String NOTIFICATION_INST_TYPE = "NOTIFICATION_INST_TYPE";
         public final static String IMPORT_BANKER_ACCEPTANCE = "IMP_DBA";
         public final static String EXPORT_BANKER_ACCEPTANCE = "EXP_DBA";
         public final static String IMPORT_TRADE_ACCEPTANCE  = "IMP_TAC";
         public final static String EXPORT_TRADE_ACCEPTANCE  = "EXP_TAC";
         public final static String IMPORT_DEFERRED_PAYMENT  = "IMP_DFP";
         public final static String EXPORT_DEFERRED_PAYMENT  = "EXP_DFP";
         public final static String INVALID_EMAIL_FORMAT           = "E2078";
         public final static String ADDITIONAL_EMAIL_LENGTH_EXCEEDS = "E2082";
         public final static String NOTIF_RULE_INCOMPLETE_DATA = "E2083";
		 //Rel9.5 CR-927B END
	public final static String INVALID_ISO_ONLY_8859_CHARS_ERR = "E2080";
	public final static String INVALID_ISO_ONLY_8859_CHARS_ERR_1 = "E2081";
	public final static String INVALID_REC_INSTRUMENT = "E2084";
	
	public final static String LINE_ITEMS_EXCEED_MAX_CHARS = "E2085";
	public final static String FEC_NEGATIVE_AMOUNT = "E2086";

 /** 
	 * TradePortalConstants constructor comment.
	 */
	public TradePortalConstants()
	{
		super();
	}

	public static String getPropertyValue(String propertyBundle, String propertyName, String defaultValue) {
		try {
			return ResourceBundle.getBundle(propertyBundle).getString(propertyName);
		} catch (MissingResourceException e) {
			LOG.trace("<=== WARNING: ===>  {}.properties -> " + propertyName + " not found, defualting to: {}",
					propertyBundle, defaultValue);
			return defaultValue;
		}
	}
	
}
