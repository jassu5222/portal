package com.ams.tradeportal.common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class TradePortalSignedDecimalAttribute extends TradePortalDecimalAttribute
 {
private static final Logger LOG = LoggerFactory.getLogger(TradePortalSignedDecimalAttribute.class);
	public TradePortalSignedDecimalAttribute ()
	 {
     }

	public TradePortalSignedDecimalAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public TradePortalSignedDecimalAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
	 

 }