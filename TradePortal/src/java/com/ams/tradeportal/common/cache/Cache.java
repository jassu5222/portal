/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;

/**
 * Represents the generic methods that all caches must use
 */
public interface Cache
{
	Object nullObject = new Object();
	/**
	 * Places the current object in the cache
	 * @param key The key used to identify the object wihtin the cache
	 * @param value The cached object
	 */
	void put(String key, Object value);

	/**
	 * Retrieves a cached value.
	 * @param key The key used to identify the object to retrieve wihtin
	 * the cache
	 * @return The cached object or null if that object does not exist
	 */
	Object get(String key);

	/**
	 * Retrieves a cached value, checking for the age of the cached entry.
	 *
	 * @param key
	 * @param refreshPeriod
	 * @return
	 */
	Object get(String key, int refreshPeriod);

	/**
	 * Removes an entry from the cache.
	 *
	 * @param key The key used to identify the object that will be removed
	 * from the cache.
	 */
	void remove(String key);

	/**
	 * Flushes the cache of all objects.
	 *
	 */
	void flush();

	/**
	 * Get the native cache object.
	 * @return Object containing the native cache object.
	 */
	Object getImplementation();

	/**
	 * Set the maximum capacity of the cache.
	 *
	 * @param size int specifying the maximum size that the
	 * cache should use.  Depending on the implementation,
	 * this may either be an item count or a memory size.
	 */
	void setCapacity(int size);

	/**
	 * Set the resolver used to retrieve objects when the cache
	 * does not contain an object.
	 *
	 * @param resolver CacheResolver that will be called
	 * to retrieve an object.
	 */
	void setResolver(CacheResolver resolver);

	/**
	 * Get the current resolver.
	 *
	 * @return CacheResolver used to retrieve an item if it not
	 * found in the cache
	 */
	CacheResolver getResolver();

	/**
	 * Set a default lifetime for all objects in the cache. When
	 * an object is retrieved from the cache, if it is older
	 * than refreshPeriod an ExpiredCacheEntryException will be
	 * thrown. If a CacheResolver has been specified it will
	 * be called first to load a fresh object.
	 *
	 * @param refreshPeriod int specifying the maximum time
	 * (in seconds) that an object in the cache is considered
	 * valid.
	 */
	void setRefreshPeriod(int refreshPeriod);

	/**
	 * Get the current value of the refresh period.
	 * @return int
	 */
	int getRefreshPeriod();

	/**
	 * Set the name of the Cache.
	 *
         * @param name
	 */
	void setCacheName(String name);

	/**
	 * Get the name of the Cache.
	 *
	 * @return String
	 */
	String getCacheName();

    /**
     * returns the number of entries stored in this Cache instance
     *
     * @return size
     */
	int getSize();

}
