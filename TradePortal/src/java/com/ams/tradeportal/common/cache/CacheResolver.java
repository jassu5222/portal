/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group 
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;

/**
 * CacheResolver is used to load an object when the Cache
 * does not contain the object.
 */
public interface CacheResolver
{
	/**
	 * Get an object specified by key.
	 * 
	 * @param key Object containing the key of an object to retrieve.
	 * 
	 * @return Object corresponding to the key.
	 */
	Object get(String key);
}
