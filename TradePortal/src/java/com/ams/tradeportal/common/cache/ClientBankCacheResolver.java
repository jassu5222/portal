/**
 * Copyright � 2003 American Management Systems, Incorporated All rights
 * reserved
 *
 * OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 * This product includes software developed by the OpenSymphony Group
 * (http://www.opensymphony.com/).
 *
 * _______________________________________________________________________________
 *
 */
package com.ams.tradeportal.common.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Resolver responsible for retrieving ClientBank records by organization OID, so the result can be cached.
 */
public class ClientBankCacheResolver implements CacheResolver {
	private static final Logger LOG = LoggerFactory.getLogger(ClientBankCacheResolver.class);

	/**
	 * Retrieve a row from the Client_Bank table and return it as a DocumentHandler
	 *
	 * @param key
	 *            organization oid
	 *
	 * @return object the result of the retrieval
	 *
	 */
	@Override
	public Object get(String key) {

		DocumentHandler resultDoc = new DocumentHandler();

		if (StringFunction.isBlank(key)) {
			// not found
			return resultDoc;
		}
		String sqlQuery = "select * from client_bank where organization_oid = ? ";

		try {

			resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[] { key });
		} catch (Exception e) {
			LOG.error("Exception from ClientBank database retrieval", e);

		}

		return resultDoc;
	}

}
