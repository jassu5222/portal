/**
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 *OSCache Copyright (c) 2001-2004 The OpenSymphony Group. All rights reserved.
 *
 *This product includes software developed by the OpenSymphony Group 
 *(http://www.opensymphony.com/).
 *
 *_______________________________________________________________________________
 * */

package com.ams.tradeportal.common.cache;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Resolver responsible for retrieving ClientBank records by organization OID, so the result can be cached.
 */
public class PerfStatConfigCacheResolver implements CacheResolver {
	private static final Logger LOG = LoggerFactory.getLogger(PerfStatConfigCacheResolver.class);

	/**
	 * Retrieve a row from the config_setting table and return it as a DocumentHandler
	 *
	 * @param sql
	 *            the SQL to retrieve the config_setting entry
	 *
	 * @return the DocumentHandle of the retrieval result
	 *
	 */

	@Override
	public Object get(String sql) {

		DocumentHandler resultDoc = new DocumentHandler();
		if (StringFunction.isBlank(sql)) {
			// not found
			return resultDoc;
		}

		try {
			List<Object> keyParams = new ArrayList<>();
			resultDoc = (DocumentHandler) DatabaseQueryBean.getXmlResultSet(sql, false, keyParams);
		} catch (Exception e) {
			LOG.error("Exception from ConfigSetting database retrieval", e);
		}

		return resultDoc;
	}

}
