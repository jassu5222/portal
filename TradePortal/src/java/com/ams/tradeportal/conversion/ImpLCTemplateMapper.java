/*
 * @(#)ImpLCTemplateMapper.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Import LC Template Mapper class.  This class is
 * used to map a row of data into an XML document for use with
 * a Trade Portal Mediator. This mapper uses Import LC Template mapper data
 * to translate the data fields into document attributes.
 *
 * @version		1.0 29 Oct 2002

 */
public class ImpLCTemplateMapper extends TemplateMapper {
private static final Logger LOG = LoggerFactory.getLogger(ImpLCTemplateMapper.class);
	/*
	 * Constants
	 */

	/** Template Mapper Field Name */
	public static final String TEMPLATE_MAPPER_FIELD_NAME	= "Template Name";

	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Import LC Template Mapper given a mapper name,
	 * an ArrayList of specific Template mapper data,
	 * a Trade Portal User ID, and valid Client Bank.
	 * Also takes a Message Log object to log errors.
	 *
	 * @param name - Mapper name.
	 *
	 * @param mapperData - ArrayList of ArrayList rows of mapper data to
	 * translate data fields into attribute values in an XML document.
	 *
	 * @param userID - Trade Portal User ID.
	 *
	 * @param clientBank - An existing Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public ImpLCTemplateMapper(String name,
							   MapperData mapperData,
							   String userID,
							   String clientBank,
							   MessageLog errorLog) {
		// Call Ancestor constructor
		super(name, mapperData, userID, clientBank, errorLog);
	}

	// Validate incoming Reference Data.
	// Populate Mapper with incoming row of data.
	// Return XML document if successful, otherwise null.
	public DocumentHandler populate(ArrayList dataRow)
	{
		DocumentHandler document;
		Query query;
		String field;
		String value;

		// Validate incoming Data row.  Exit if validation fails.
		if (!validate(dataRow)) {
			return null;
		}

		// Log populate Import LC Template document with Mapper
		errorLog.debug("Populating Import LC Template document...");

		// Call Ancestor to create XML document with auto-populated fields
		document = super.populate(dataRow);

		// If null document returned, return null
		if (document == null) {
			return null;
		}

		// Initialize Query object
		query = new Query(errorLog);

		// Get Owner Op Org and Client Bank oids
		// Obtain from Database using Corporate Customer Name
		field = dataRow.get(DATA_FIELD_CUSTOMER).toString();
		// Use where clause for specific Customer Name
		value = query.getQueryResult("customer_oid",
			"co.organization_oid customer_oid, cb.organization_oid clientbank_oid",
			"corporate_org co, client_bank cb",
			Conversion.createCustomerWhereClause(field, clientBank));
		if (value.length() == 0)
		{
			errorLog.error("Unable to locate Corporate Customer: " + field);
			return null;
		}
		// Set Owner Org Oid
		document.setAttribute("/ownerOrg", value);

		// Set Client Bank Oid
		value = query.getQueryResult("clientbank_oid");
		if (value.length() == 0)
		{
			errorLog.error("Unable to locate Client Bank: " + clientBank);
			return null;
		}
		document.setAttribute("/clientBankOid", value);

		// Login Info
		if (!query.performQuery("user_oid, security_rights",
			"users u, security_profile sp",
			Conversion.createGlobalUserWhereClause(userID)))
		{
			errorLog.error("Unable to locate Trade Portal User ID: " + userID);
			return null;
		}
		// Login User oid
		// Set User Oid
		document.setAttribute("/userOid", query.getQueryResult("user_oid"));

		// Security Rights
		// Set Security Rights
		document.setAttribute("/securityRights",
			query.getQueryResult("security_rights"));

		try
		{
			// Get Template Name using the mapper
			value = dataRow.get(mapperData.findRow(
				TEMPLATE_MAPPER_FIELD_NAME)).toString();
		}
		catch(IndexOutOfBoundsException e)
		{
			errorLog.error("Unable to locate Template Name field from "
				+ mapperName + " mapper");
			return null;
		}
		// Set Template Name
		document.setAttribute("/name", value);

		// Log populate Import LC Template complete
		errorLog.debug("Populating Import LC Template document complete.");

		// Return populated XML document
		return document;
	}
}
