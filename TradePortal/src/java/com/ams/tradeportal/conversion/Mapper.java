/*
 * @(#)Mapper.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Base Mapper class.  This abstract class is the foundation
 * used to map a row of field data into an XML document for use with
 * a Trade Portal Mediator. This mapper uses a mapper data array to
 * translate the data fields into document attributes.  Specific types of
 * mappers must inherit from this class in order to be instantiated.
 *
 * @version		1.0 29 Oct 2002

 */
public abstract class Mapper {
private static final Logger LOG = LoggerFactory.getLogger(Mapper.class);
	/*
	 * Constants
	 */

	/*
	 * Mapper Population Fields
	 */

	/** Mapper Document Attribute Value Index */
	public static final int MAPPER_DOC_ATTRIBUTE			= 0;

	/** Population Type Indicator Index */
	public static final int MAPPER_POPULATE_TYPE			= 1;

	/** Mapper Data Manual Entry Value Index */
	public static final int MAPPER_MANUAL_VALUE				= 2;

	/** Mapper Field List Max Fields */
	public static final int MAPPER_MAXFIELDS				= 3;

	/*
	 * Mapper Population Types
	 */

	/** No Mapper Population */
	public static final String POPULATE_NO					= "N";

	/** Mapper Attribute Auto-Population from Data Row */
	public static final String POPULATE_AUTO				= "A";

	/** Mapper Attribute Manual Entry from Mapper Data */
	public static final String POPULATE_MANUAL				= "M";


	/*
	 * Members
	 */

	/** Mapper Name */
	protected String mapperName = "";

	/** Mapper Data List */
	protected MapperData mapperData = null;

	/** Trade Portal User ID */
	protected String userID = "";

	/** Trade Portal Client Bank */
	protected String clientBank = "";

	/** Mapper is Valid Flag */
	protected boolean objectIsValid = false;

	/** Error Log */
	protected MessageLog errorLog = null;

	/**
	 * Mapper Data Field population list.  These will be populated by the
	 * descendant class with specified mapper data tables.
	 */
	protected ArrayList mapperFieldList = null;

	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Mapper given an ArrayList of mapper data,
	 * a Trade Portal User ID, and a Client Bank.
	 * Also takes a Message Log object to log errors.
	 * Any inheriting classes should specify a mapper name for the type
	 * of mapper.  This is used for logging purposes.
	 *
	 * @param name - Mapper name.
	 *
	 * @param mapperData - ArrayList of ArrayList rows of mapper data to
	 * translate data fields into attribute values in an XML document.
	 *
	 * @param userID - Trade Portal User ID.
	 *
	 * @param clientBank - An existing Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public Mapper(String name,
				  MapperData mapperData,
				  String userID,
				  String clientBank,
				  MessageLog errorLog) {
		// Initialize Mapper field location list
		mapperFieldList = new ArrayList();

		// Check for null Error Log
		if (errorLog == null) {
			LOG.info("Error: " + mapperName
				+ " Mapper Error Log is invalid");
			return;
		}
		// Store Error Log
		this.errorLog = errorLog;

		// Check for name
		if ((name == null)
			|| (name.length() == 0)) {
			// Cannot have blank name
			errorLog.error("Mapper name is blank");
			return;
		}

		// Check for User ID and Client Bank
		if ((userID == null)
			|| (userID.length() == 0)
			|| (clientBank == null)
			|| (clientBank.length() == 0)) {
			// Invalid User ID and/or Client Bank
			this.errorLog.error("User ID and/or Client Bank is invalid: " +
				userID + ", " + clientBank);
			return;
		}

		// Check for valid mapper data
		if (!mapperData.isValid()) {
			// Invalid Mapper
			this.errorLog.error(mapperName + " Data array is empty");
			return;
		}

		// Store mapper name
		mapperName = name;

		// Store User ID and Client Bank
		this.userID = userID;
		this.clientBank = clientBank;

		// Store the mapper data list
		this.mapperData = mapperData;

		// Mapper is valid
		objectIsValid = true;
	}


	/*
	 * Public Methods
	 */

	/**
	 * Validate incoming data row.
	 * Attempt to populate XML document with incoming data row using
	 * the mapper data.  Return XML document if successful, else null.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return DocumentHandler, XML document generated using data row with
	 * the stored mapper data.
	 */
	public DocumentHandler populate(ArrayList dataRow) {
		ArrayList mapperDataRow;
		DocumentHandler document;
		int i;
		int docAttributeIndex;
		int fieldIndex;
		String populateField;

		// Validate incoming data row, exit if failure
		if (!validate(dataRow)) {
			return null;
		}

		// Log populate XML document with mapper data
		errorLog.debug("Auto-Populating XML document with mapper data...");

		// Create a new XML document
		document = new DocumentHandler();

		/*
		 * Populate XML Document attributes using data row and mapper.
		 * Loop through mapper rows check for type of population to determin
		 * whether to extract data field and set attribute in document.
		 */
		for (i = 0; i < mapperData.size(); i++) {
			// Get mapper data row
			mapperDataRow = mapperData.getRow(i);

			// Check to automatically populate the field into the document
			try {
				// Get mapper populate type field index
				fieldIndex = Integer.parseInt(
					mapperFieldList.get(MAPPER_POPULATE_TYPE).toString());

				// Get mapper populate type
				populateField = mapperDataRow.get(fieldIndex).toString();

				// If auto-populate, then set the attribute in the document
				if (populateField.equals(POPULATE_AUTO)) {
					// Check if data field exists to populate into document
					if ((dataRow.size() > i)
						&& (dataRow.get(i).toString().length() > 0)) {
						// Get mapper document attribute field index
						docAttributeIndex = Integer.parseInt(
							mapperFieldList.get(MAPPER_DOC_ATTRIBUTE).toString());

						// Store data field attribute in document
						document.setAttribute(
							mapperDataRow.get(docAttributeIndex).toString(),
							dataRow.get(i).toString());
					}
				} else if (populateField.equals(POPULATE_MANUAL)) {
					// Manual field entry from Mapper Data
					// Get mapper document attribute field index
					docAttributeIndex = Integer.parseInt(
						mapperFieldList.get(MAPPER_DOC_ATTRIBUTE).toString());
					// Get manual entry field index
					fieldIndex = Integer.parseInt(
						mapperFieldList.get(MAPPER_MANUAL_VALUE).toString());

					// Store manual field entry value
					document.setAttribute(
						mapperDataRow.get(docAttributeIndex).toString(),
						mapperDataRow.get(fieldIndex).toString());
				} else if (populateField.equals(POPULATE_NO)) {
					// Do Nothing
				} else {
					// Invalid Population Type
					errorLog.error("Invalid Mapper Population Type code: "
						+ populateField + ", for data field: " + (i + 1));
					return null;
				}
			} catch (IndexOutOfBoundsException e) {
				// Invalid field index
				errorLog.error("Unable to translate mapper data field: "
					+ (i + 1));
				return null;
			} catch (NumberFormatException e) {
				// Invalid field index
				errorLog.error("Unable to translate mapper data field: "
					+ (i + 1));
				return null;
			}
		}

		// Log populate Reference Data document with Mapper complete
		errorLog.debug("Auto-Populating mapper data to document complete.");

		// Return populated XML document
		return document;
	}

	/**
	 * Validate incoming data row.
	 * Attempt to populate XML document with incoming data row using
	 * the mapper data.  Return XML document if successful, else null.
	 * The Template Document can be used to extract attributes for use when
	 * populating the new document, but is not needed in this class.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @param templateDoc - Template document whose data can be used to
	 * populate the new document.
	 *
	 * @return DocumentHandler, XML document generated using data row with
	 * the stored mapper data.
	 */
	public DocumentHandler populate(ArrayList dataRow,
									DocumentHandler templateDoc)
	{
		// Populate the document, the Template document is not needed here.
		return populate(dataRow);
	}

	/**
	 * Validate the incoming data row against the mapper data.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return boolean, whether data row is valid.
	 */
	protected boolean validate(ArrayList dataRow) {
		String field;

		// Check for valid Mapper
		if (!objectIsValid) {
			// Attempt to log error to file
			try {
				errorLog.error(mapperName + " Mapper is invalid");
			} catch (NullPointerException e) {
				LOG.info(mapperName + " Mapper is invalid");
			}
			return false;
		}

		// Verify Mapper fields list population for correct data mapping
		if (mapperFieldList.size() < MAPPER_MAXFIELDS) {
			// Not all fields populated
			errorLog.error("Mapper field location list not populated");
			return false;
		}

		// Check for empty data
		if (dataRow == null) {
			errorLog.error("Incoming data row has no data to create XML Document");
			return false;
		}

		// Validation Successful
		return true;
	}
}
