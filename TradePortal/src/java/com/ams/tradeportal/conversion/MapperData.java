/*
 * @(#)MapperData.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Trade Portal Mapper Data Store class.  This is used to hold imported
 * Reference Data and Template mapper field data for converting
 * field-delimited rows of data into attribute-populated XML documents.
 *
 * @version		1.0 29 Oct 2002

 */
public class MapperData {
private static final Logger LOG = LoggerFactory.getLogger(MapperData.class);
	/*
	 * Constants
	 */

	/* Reference Data Mapper Fields */

	/** Reference Data Mapper Field Name Index */
	protected static final int REFDATA_FIELD_NAME			= 0;


	/*
	 * Members
	 */

	/** Mapper Data Store Name */
	protected String mapperName = "";

	/**
	 * Mapper Data Storage object, stored as an ArrayList of rows of data
	 * stored as ArrayLists of fields.
	 */
	protected ArrayList mapperData = null;

	/** Error Log */
	protected MessageLog errorLog = null;

	// Mapper is Valid Flag
	protected boolean objectIsValid = false;


	/*
	 * Constructors
	 */

	/**
	 * Mapper Data Store class constructor.
	 * Takes in a mapper name and a 2-dimensional list of mapper data.
	 *
	 * @param name - Mapper name.
	 *
	 * @param data - Mapper data used to translate parsed fields into an
	 * XML document.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public MapperData(String name,
					  ArrayList data,
					  MessageLog errorLog) {
		// Check for null Error Log
		if (errorLog == null)
		{
			LOG.info("Error: MapperData Error Log is invalid");
			return;
		}

		// Check for name
		if ((name == null)
			|| (name.length() == 0)) {
			errorLog.error("No name specified for Mapper");
			return;
		}

		// Check for empty list
		if ((data == null)
			|| (data.size() == 0)) {
			errorLog.error("Mapper data parameter is empty");
			return;
		}

		// Store Name
		mapperName = name;

		// Store reference data
		mapperData = data;

		// Reference data is valid
		objectIsValid = true;
	}


	/*
	 * Public Methods
	 */

	/**
	 * Return whether MapperData object is valid.
	 *
	 * @return boolean, whether MapperData object is valid.
	 */
	public boolean isValid() {
		return objectIsValid;
	}

	/**
	 * Return mapper data storage list.
	 *
	 * @return ArrayList, mapper data array.
	 */
	public ArrayList getData() {
		return mapperData;
	}

	/**
	 * Return Mapper data row count.
	 *
	 * @return int, mapper data row count
	 */
	public int size() {
		// Check for valid reference data
		if (!objectIsValid) {
			return 0;
		}

		return mapperData.size();
	}

	/**
	 * Return specified mapper data row from the mapper data store.
	 *
	 * @param index - Specified row in mapper data.
	 *
	 * @return ArrayList, list of mapper fields for the specified row.
	 */
	public ArrayList getRow(int index) {
		// Check for valid reference data
		if (!objectIsValid) {
			errorLog.error("MapperData object is invalid, unable to get row: "
				+ index);
			return new ArrayList();
		}

		try {
			// Return specified row
			return (ArrayList)mapperData.get(index);
		} catch (IndexOutOfBoundsException e) {
			// Invalid row selected
			errorLog.error("Unable to get Mapper data Row: " + index);

			// Return empty list
			return new ArrayList();
		} catch (ClassCastException e) {
			// Mapper data field is not an ArrayList
			errorLog.error("Mapper Data is invalid, unable to extract row: "
				+ index);

			// Return empty list
			return new ArrayList();
		}
	}

	/**
	 * Locate a field value given a Row key and a specified column.
	 * If search fails, return empty string.
	 *
	 * @param key - Name of the first field of desired mapper data row.
	 *
	 * @param column - Index of desired column within mapper data row.
	 *
	 * @return String, field value of specified row and column.
	 */
	public String getField(String key,
						   int column) {
		ArrayList dataRow;
		int row;

		// Locate mapper row
		row = findRow(key);

		// If row not found, return empty string
		if (row < 0) {
			errorLog.error(mapperName + " Mapper data row not found for key: "
				+ key);

			// No Match found
			return "";
		}

		try {
			// Get data row
			dataRow = (ArrayList)mapperData.get(row);

			// Return value at specified column
			return dataRow.get(column).toString();
		} catch (ClassCastException e) {
			// Invalid mapper data row, keep searching
			errorLog.error("Invalid mapper row format for " + mapperName +
				" mapper data at row: " + (row + 1) +
				".  Searching for record: '" + key);

			// No Match found
			return "";
		} catch (IndexOutOfBoundsException e) {
			// Desired column in  storage, keep searching
			errorLog.error("Unable to extract data from " + mapperName +
				" mapper data storage at row: " + (row + 1) +
				".  Searching for record/column: '" + key + ", " + column);

			// No Match found
			return "";
		}
	}

	/**
	 * Locate a matching row in the mapper data given a key value.
	 * If search fails, return -1.
	 *
	 * @param key - Name of the first field of desired mapper data row.
	 *
	 * @return int, index of matching row.
	 */

	public int findRow(String key) {
		ArrayList dataRow;
		int i;

		// Check for valid Reference Data
		if (!objectIsValid) {
			LOG.info("Error: searching failed, reference data is invalid");
			return -1;
		}

		// Validate key and column
		if (key == null || key.length() == 0) {
			// Invalid key or column
			LOG.info("Error, invalid key specified for Reference Data search: " +
				key);
			return -1;
		}

		// Loop through Reference Data to find matching field
		for (i = 0; i < mapperData.size(); i++) {
			try
			{
				// Get data row
				dataRow = (ArrayList)mapperData.get(i);

				// Check for matching key
				if (!key.equals(dataRow.get(REFDATA_FIELD_NAME).toString())) {
					// No match, skip to next row
					continue;
				}

				// Return row
				return i;
			} catch (Exception e) {
				// Incorrect data storage, keep searching
				LOG.info("Error, unable to extract data from " + mapperName +
					"Reference Data storage at row " + (i + 1) +
					".  Searching for record '" + key);
			}
		}

		// No Match found
		return -1;
	}
}