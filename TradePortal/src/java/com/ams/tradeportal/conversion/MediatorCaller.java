/*
 * @(#)MediatorCaller.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.mediator.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import javax.ejb.*;

/**
 * Trade Portal Mediator Caller class.  This class is
 * used to call a specified Mediator to submit data from an XML document
 * into the Trade Portal Database.
 *
 * @version		1.0 29 Oct 2002

 */
public class MediatorCaller {
private static final Logger LOG = LoggerFactory.getLogger(MediatorCaller.class);
	/*
	 * Constants
	 */

	/* Document Literals */

	/** Input Document Tag */
	public static final String TAG_INPUT_DOC			= "/In";

	/** Output Document Tag */
	public static final String TAG_OUTPUT_DOC			= "/Out";


	/*
	 * Members
	 */

	/** Mediator Name*/
	protected String mediatorName = "";

	/** Mediator is Valid Flag */
	protected boolean objectIsValid = false;

	/** Error Log */
	protected MessageLog errorLog = null;

	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Mediator Caller given a specific Mediator.
	 * Also takes a Message Log object to log errors.
	 *
	 * @param mediatorName - Name of specific Mediator.to use to process
	 * the XML document into the Trade Portal.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public MediatorCaller(String mediatorName,
						  MessageLog errorLog) {
		// Check for null Error Log
		if (errorLog == null) {
			LOG.info("Error, Import LC Template Mediator Error Log is invalid");
			return;
		}

		// Store Error Log
		this.errorLog = errorLog;

		// Check for empty name
		if ((mediatorName == null)
			|| mediatorName.length() == 0) {
			errorLog.error("Unable to initialize Mediator Caller, name is blank");
			return;
		}

		// Store Caller name
		this.mediatorName = mediatorName;

		// Mediator is valid
		objectIsValid = true;
	}

	/*
	 * Public Methods
	 */

	/**
	 * Submit XML document using the Mediator and process the results
	 * document that is returned from the Mediator.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return DocumentHandler, Mediator output document if processing
	 * complete, else null.
	 */
	public DocumentHandler processDoc(DocumentHandler inputDoc) {
		ClientServerDataBridge cSDB;
		DocumentHandler mediatorDoc;
		DocumentHandler outputDoc=null;
		DocumentHandler errorDoc;
		JPylonProperties jPylonProperties;
		Mediator mediator;
		Vector errorList;
		boolean hasError;
		int i;
		int severityLevel;
		int errorListSize;
		String ejbServerLocation;
		String errorCode;
		String message;

		// Check for valid Mediator Caller
		if (!objectIsValid) {
			// Mediator invalid
			LOG.info("Error: " + mediatorName
				+ " Caller is invalid");
			return null;
		}

		// Log Processing Mediator document
		errorLog.debug("Submitting document through " + mediatorName
			+ " Mediator Caller...");

		// Verify input document
		if (inputDoc == null) {
			errorLog.error("Input document is empty.");
			return null;
		}

		try {
			// Determine the server location
			jPylonProperties = JPylonProperties.getInstance();
			ejbServerLocation = jPylonProperties.getString ("serverLocation");
		} catch (AmsException e) {
			// Failed to locate server
			errorLog.error("Unable to determine server location");
			return null;
		}

		// Log server location
		errorLog.debug("Server Location: " + ejbServerLocation);

		// Create the CSDB - needed to create the mediator
		cSDB = new ClientServerDataBridge();
		// Set locale for US english language
		cSDB.setLocaleName(Conversion.LOCALE_ENGLISH_US);

		try {
			// Create the mediator using the specified type and locale
			mediator = (Mediator)EJBObjectFactory.createClientEJB(
				ejbServerLocation, mediatorName, cSDB);
			// Check that mediator was created
			if (mediator == null) {
				errorLog.error("Unable to create " + mediatorName);
				return null;
			}
		} catch (AmsException e) {
			errorLog.error("Unable to create " + mediatorName);

			// Clear mediator
			mediator = null;
		} catch (RemoteException e) {
			errorLog.error("Unable to connect to server using the "
				+ mediatorName);

			// Clear mediator
			mediator = null;
		}

		// Create document to send to the mediator
		mediatorDoc = new DocumentHandler();
		// Move the document to the Input section of the document for the mediator
		mediatorDoc.addComponent(TAG_INPUT_DOC, inputDoc);

		// Log document sent to Transaction Mediator
		errorLog.debug("Sending Document to " + mediatorName + ":\n"
			+ mediatorDoc.toString(true) + "\n");

		try {
			// Execute the mediator, passing in XML and receiving XML back
			if(mediator!=null)
			outputDoc = mediator.performService(mediatorDoc, cSDB);
			
		} catch (AmsException e) {
			errorLog.error("Failed while trying to execute " + mediatorName);

			// Clear Output Document to exit
			outputDoc = null;
		} catch (RemoteException e) {
			errorLog.error("Failed while trying to connect the " + mediatorName
				+ " to the server");

			// Clear Output Document to exit
			outputDoc = null;
		}

		try {
			// Remove the mediator EJB if it exists
			if (mediator != null) {
				mediator.remove();
			}
		} catch (RemoteException e) {
			// Log warning and continue
			errorLog.warning("Unable to close " + mediatorName
				+ " on the server, continuing...");
		} catch (RemoveException e) {
			// Log warning and continue
			errorLog.warning("Unable to close " + mediatorName
				+ ", continuing...");
		}

		// Verify output document
		if (outputDoc == null) {
			errorLog.error(mediatorName + " output document is empty.");
			return null;
		}

		// Get a list of the returned errors
		severityLevel = 0;
		errorList = outputDoc.getFragments("/Error/errorlist/error");
		errorListSize = errorList.size();

		// Log returned error document
		errorLog.debug(mediatorName + " output Error Document:\n" +
			outputDoc.toString(true) + "\n");

		// Initialize to no Errors
		hasError = false;

		// Loop to log errors
		for (i = 0; i < errorListSize; i++) {
			errorDoc = (DocumentHandler)errorList.elementAt(i);

			try {
				// Get severity and message
				errorCode = errorDoc.getAttribute("/code");
				message = errorDoc.getAttribute("/message");
				severityLevel = errorDoc.getAttributeInt("/severity");
			} catch (AmsException e) {
				// Log error and leave
				errorLog.error("Unable to retrieve error message and severity for error "
					+ (i + 1) + ", Number of errors: " + errorListSize);
				return null;
			} catch (NullPointerException e) {
				// Log error and leave
				errorLog.error("Unable to retrieve error message and severity for error "
					+ (i + 1) + ", Number of errors: " + errorListSize);
				return null;
			}

			// Log severe error message
			if (severityLevel >= ErrorManager.ERROR_SEVERITY) {
				// Error found
				hasError = true;

				// Log message
				errorLog.message("Input Document Error " + (i + 1)
					+ ":\n\tCode: " + errorCode + "\n\tMessage: "
					+ message + "\n\tSeverity: " + severityLevel);
			}
		}

		// If there were errors, return null
		if (hasError) {
			return null;
		}

		// Log Processing Mediator document complete
		errorLog.debug("Submitting document through "
			+ mediatorName + " Mediator Caller complete.");

		/*
		 * Document processed completely, return output section of document
		 * returned from Mediator
		 */
		return outputDoc.getFragment(TAG_OUTPUT_DOC);
	}
}
