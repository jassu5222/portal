/*
 * @(#)MessageLog.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import com.amsinc.ecsg.util.StringFunction;

/**
 * Trade Portal Message Log class to log messages to a file.  This can be used
 * for error loggingto a file, etc.
 *
 * @version		1.0 29 Oct 2002

 */
public class MessageLog {
private static final Logger LOG = LoggerFactory.getLogger(MessageLog.class);
	/*
	 * Members
	 */

	/** Results Data Storage */
	protected PrintWriter logWriter = null;

	/** Log File Path */
	protected String logFilePath = "";

	/** Current Log Message buffer */
	protected StringBuffer messageBuffer = null;

	/** Flag of whether to log Debug Messages */
	protected boolean debugOn = false;

	/** Is Log File Valid Flag */
	protected boolean objectIsValid = false;


	/*
	 * Constructors
	 */

	/**
	 * Default unvalidated Trade Portal Message Log class Constructor.
	 * Messages will be printed to the console instead of a log file.
	 */
	public MessageLog() {
	}

	/**
	 * Create a Trade Portal Message Log object given a log file path.
	 * This log file will append to an existing file or start a new one.
	 * If debugging is turned on, then debug messages will be logged.
	 *
	 * @param filePath - New log file path
	 *
	 * @param doDebug - Flag of whether to log debug messages.
	 */
	public MessageLog(String filePath,
					  boolean doDebug) {
		// Set debug flag
		debugOn = doDebug;

		// Attempt to open log file
		openLog(filePath);
	}

	/*
	 * Public Methods
	 */

	/**
	 * Return if log file is open.
	 *
	 * @return boolean, whether log file is valid and open for writing.
	 */
	public boolean isOpen() {
		// Return if file open
		return objectIsValid;
	}

	/**
	 * Return log file path.
	 *
	 * @return String, Log file path.
	 */
	public String getFilePath() {
		return logFilePath;
	}

	/**
	 * Append debug message to log message buffer, if debugging on.
	 *
	 * @param message - Debug message.
	 */
	public void debug(String message) {
		// Add debug text w/o printing
		debug(message, false);
	}

	/**
	 * Append debug message to log message buffer, if debugging on.
	 * Log message to file if print flag is true.
	 *
	 * @param message - Debug message.
	 *
	 * @param logMessage - Flag of whether to log message to file now.
	 */
	public void debug(String message,
					  boolean logMessage) {
		// Check for debugging on
		if (debugOn) {
			// Add debug message
			message(message, "");

			// Check to print message
			if (logMessage) {
				// Print message to log
				log();
			}
		}
	}

	/**
	 * Append an informational message to log message buffer.
	 *
	 * @param message - Message.
	 */
	public void message(String message) {
		message(message, "");
	}

	/**
	 * Append warning message to log message buffer.
	 *
	 * @param message - Warning message.
	 */
	public void warning(String message) {
		warning(message, false);
	}

	/**
	 * Append warning message to log message buffer, check flag for printing
	 * current message buffer to the log file..
	 *
	 * @param message - Debug message.
	 *
	 * @param logMessage - Flag of whether to log message to file now.
	 */
	public void warning(String message,
						boolean logMessage) {
		// Add warning message
		message(message, "Warning: ");

		// Check to print message to log
		if (logMessage) {
			// Log message
			log();
		}
	}

	/**
	 * Append error message to log message buffer.
	 *
	 * @param message - Error message.
	 */
	public void error(String message) {
		error(message, false);
	}

	/**
	 * Append error message to log message buffer, check flag for printing
	 * current message buffer to the log file..
	 *
	 * @param message - Debug message.
	 *
	 * @param logMessage - Flag of whether to log message to file now.
	 */
	public void error(String message,
					  boolean logMessage) {
		// Add error message
		message(message, "Error: ");

		// Check to print message to log
		if (logMessage) {
			// Log message
			log();
		}
	}

	/**
	 * Log message buffer to file.
	 */
	public void log() {
		log("");
	}

	/**
	 * Log message buffer to file with a specified header message.
	 *
	 * @param header - Log message header text.
	 */
	public void log(String header) {
		String messageText;
		StringBuffer fullMessage;

		// Get message text
		messageText = messageBuffer.toString();

		// Reset message buffer
		messageBuffer = new StringBuffer();

		// Initialize full message
		fullMessage = new StringBuffer();

		// Check for populated header
		if ((header != null)
			&& (header.length() > 0)) {
			// Append header
			fullMessage.append(header);
		}

		// Check for message text in buffer
		if (messageText.length() > 0) {
			// Append new line if header exists
			if (fullMessage.length() > 0) {
				fullMessage.append('\n');
			}

			// Append message text
			fullMessage.append(messageText);
		}

		// If no text to print, exit
		if (fullMessage.length() == 0) {
			return;
		}

		// Check for open file
		if (!openLog()) {
			// File not open, print message to screen and exit
			LOG.info(fullMessage.toString());

			return;
		}

		// Print message to log file
		logWriter.println(fullMessage.toString());

		// Check for print error
		if (logWriter.checkError()) {
			// Print message to screen
			LOG.info("Error while writing message to log file: " +
				logFilePath);
			LOG.info(fullMessage.toString());
		}
	}


	/*
	 * Local Methods
	 */

	/**
	 * Check for an existing open log file.  If log file not open, then open
	 * the file at the currently stored log file path to write messages.
	 *
	 * @return boolean, whether log file was opened successfully.
	 */
	protected boolean openLog() {
		// Check for open file
		if (isOpen()) {
			return true;
		}

		// Try to open existing file
		return openLog(logFilePath);
	}

	/**
	 * Attempt to open the specified log file to write messages.
	 *
	 * @return boolean, whether log file was opened successfully.
	 */
	protected boolean openLog(String filePath) {
		// Initialize message buffer
		messageBuffer = new StringBuffer();
		
		// Check for empty path
		if ((filePath == null)
			|| (filePath.length() == 0)) {
			LOG.info("Error: No Log File Path specified");
			return false;
		}

		// Store new File Path
		logFilePath = filePath;

		// Close an existing writer
		if (logWriter != null) {
			logWriter.close();
		}

		// Open the file for writing
		String filepathcheck=StringFunction.validateFilePath(logFilePath);
		if(filepathcheck!=null){
		try {
			// Create new Printer writer with text auto-flushing
			logWriter = new PrintWriter(new BufferedWriter(
				new FileWriter(logFilePath, true)), true);
		} catch (IOException e) {
			// Unable to open file
			LOG.info("Error: Unable to open file for writing: "
				+ logFilePath);

			// Clear writer
			logWriter = null;

			return false;
		}
		}else{
			LOG.info("Error: Unable to open file for writing: "
					+ logFilePath);
			logWriter = null;
			return false;
		}
		// File open
		objectIsValid = true;

		return objectIsValid;
	}

	/**
	 * Append message to buffer, adding message prefix (ex.,"Error: ").
	 *
	 * @return boolean, whether log file was opened successfully.
	 */
	protected void message(String message,
						   String prefix) {
		// Check for empty message
		if ((message == null)
			|| (message.length() == 0)) {
			// No message, do nothing
			return;
		}

		// Append message prefix
		messageBuffer.append(prefix);
		// Append new message
		messageBuffer.append(message);
		// Append new line
		messageBuffer.append('\n');
	}
}
