/*
 * @(#)Query.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Query class.  This is used to perform user-defined
 * Database queries and facilitate resulting data extraction.
 *
 * @version		1.0 29 Oct 2002

 */
public class Query {
private static final Logger LOG = LoggerFactory.getLogger(Query.class);
	/*
	 * Member Objects
	 */

	/** Results data storage */
	protected Vector queryResults = null;

	/** Query successful flag */
	protected boolean querySuccess = false;

	/** Query object is Valid Flag */
	protected boolean objectIsValid = false;

	/** Error Log */
	protected MessageLog errorLog = null;


	/*
	 * Constructors
	 */

	/**
	 * Trade Portal Query object constructor.
	 *
	 * @param errorLog - Error log to print messages to a file.
	 */
	public Query(MessageLog errorLog)
	{
		// Check for null Error Log
		if (errorLog == null)
		{
			LOG.info("Error, Party Validator Error Log is invalid");
			return;
		}
		// Store Error Log
		this.errorLog = errorLog;

		// Initialize results storage
		queryResults = new Vector(1);

		// Query object is valid
		objectIsValid = true;
	}


	/*
	 * Public Methods
	 */

	/**
	 * Return current successful query flag.
	 *
	 * @return boolean, is current query successful.
	 */
	public boolean isSuccessful() {
		return querySuccess;
	}

	/**
	 * Get query results set.
	 *
	 * @return Vector, query results set.
	 */
	public Vector getResults() {
		// Return empty vector if query failed
		if (!querySuccess) {
			return new Vector(1);
		}

		// Return query results
		return queryResults;
	}

	/**
	 * Get Query Results Row Count.
	 *
	 * @return int, number of rows of data returned from successful query.
	 */
	public int count() {
		// Return empty vector if query failed
		if (!querySuccess) {
			return 0;
		}

		// Return results row count
		return queryResults.size();
	}

	/**
	 * Perform Database query using incoming fields and tables.
	 * Multiple fields or tables are comma delimited, as in SQL.
	 * A where clause can be specified, or left blank.
	 * Return whether query completed successfully.
	 * Takes a Message Log object to log errors.
	 *
	 * @param fields - SQL query fields, multiple separated by commas.
	 *
	 * @param tables - Database tables, multiple separated by commas.
	 *
	 * @param whereClause - SQL query where clause.  Can be empty string.
	 *
	 * @return boolean, whether query completed successfully.
	 */
	public boolean performQuery(String fields,
								String tables,
								String whereClause) {
		DocumentHandler resultsDoc;
		Vector results;
		StringBuffer query;

		// Check for valid Query object
		if (!objectIsValid) {
			// Attempt to log error to file
			try {
				errorLog.debug("Query object is invalid");
			} catch (NullPointerException e) {
				LOG.info("Query object is invalid");
			}

			return false;
		}

		// Field(s) and Table(s) must be specified
		if ((fields == null)
			|| (fields.length() == 0)
			|| (tables == null)
			|| (tables.length() == 0)) {
			// Fields and/or tables parameter is empty
			errorLog.debug("Database Query Fields and/or Table not specified");

			return false;
		}

		// Clear previous query results
		querySuccess = false;
		queryResults = null;
		query = new StringBuffer();

		// Compile query using incoming parameters
		query.append("SELECT " + fields);
		query.append(" FROM " + tables);

		// Append where clause if one exists
		if ((whereClause == null)
			|| (whereClause.length() > 0)) {
			query.append(" WHERE " + whereClause);
		}

		// Log query SQL
		errorLog.debug("SQL Query:\n" + query.toString() + "\n");

		try {
			// Execute query
			resultsDoc = DatabaseQueryBean.getXmlResultSet(query.toString(),false, new ArrayList<Object>());
		} catch (AmsException e) {
			// Query failed
			errorLog.debug("Failure while performing Database Query:");
			// Log Query
			if (query != null) {
				errorLog.message(query.toString());
			}

			return false;
		}

		// Extract results
		if(resultsDoc != null) {
			// Extract any records
			results = resultsDoc.getFragments("/ResultSetRecord/");

			// Log SQL Query output
			errorLog.debug("SQL Query Document Output:\n" +
				resultsDoc.toString(true) + "\n");
		} else {
			// Log no query results
			errorLog.debug("No results from query");

			// No returning data
			results = new Vector(1);
		}

		// Store parsed results data
		queryResults = results;

		// Query successful
		querySuccess = true;

		return querySuccess;
	}

	/**
	 * Get specific results Document from Query object.
	 *
	 * @param index - Index of desired row in results set
	 *
	 * @return DocumentHandler, document containing results from query.
	 */
	public DocumentHandler getResultsDoc(int index) {
		DocumentHandler document;

		// Check for valid Query object
		if (!objectIsValid) {
			// Attempt to log error to file
			try {
				errorLog.debug("Query object is invalid");
			} catch (NullPointerException e) {
				LOG.info("Query object is invalid");
			}

			return new DocumentHandler();
		}

		// Check for failed query
		if (!querySuccess) {
			errorLog.debug("Latest Query was not successful.  No results to return.");
			return new DocumentHandler();
		}

		// Check for any rows returned
		if (queryResults.size() == 0) {
			// No data returned from query, return empty document
			errorLog.debug("No rows returned from query results document");
			return new DocumentHandler();
		}

		// Get chosen document
		try {
			document = (DocumentHandler)queryResults.get(index);
		} catch (ArrayIndexOutOfBoundsException e) {
			// Unable to locate specified results record, return empty document
			errorLog.debug("Unable to retrieve query results document at Row: " +
				index + ", Max Rows: " + queryResults.size());
			return new DocumentHandler();
		}

		// Return document
		return document;
	}

	/**
	 * Get single attribute value from specified query.
	 * All of the query results are cached in results set.
	 *
	 * @param attribute - Desired field value of first row to be returned.
	 *
	 * @param fields - SQL query fields, multiple separated by commas.
	 *
	 * @param tables - Database tables, multiple separated by commas.
	 *
	 * @param whereClause - SQL query where clause.  Can be empty string.
	 *
	 * @return String, desired attribute from first row in query results.
	 */
	public String getQueryResult(String attribute,
								 String fields,
								 String tables,
								 String whereClause) {
		String value;

		// Check for valid Query object
		if (!objectIsValid) {
			// Attempt to log error to file
			try {
				errorLog.debug("Query object is invalid");
			} catch (NullPointerException e) {
				LOG.info("Query object is invalid");
			}

			return "";
		}

		// Check flag on whether to execute new query
		if (!performQuery(fields, tables, whereClause)) {
			return "";
		}

		// Check for empty attribute
		if ((attribute == null)
			|| (attribute.length() == 0)) {
			// Invalid attribute
			errorLog.debug("No retrieval attribute specified for query");

			return "";
		}

		// Get field from first record
		value = getResultsDoc(0).getAttribute(Conversion.DOC_ROOT
			+ attribute.toUpperCase());
		if (value == null) {
			return "";
		}

		// Return result
		return value;
	}

	/**
	 * Get result value from existing query results.
	 *
	 * @param field - Results field of the desired column from which to get
	 * the results value.
	 *
	 * @return String, results value.
	 */
	public String getQueryResult(String field)
	{
		String value;

		// Check for failed query
		if (!querySuccess) {
			errorLog.debug("Latest Query was not successful.  No results to return.");

			return "";
		}

		// Get field from first record
		value = getResultsDoc(0).getAttribute(Conversion.DOC_ROOT
			+ field.toUpperCase());

		// Check for invalid field -> null value
		if (value == null) {
			return "";
		}

		// Return result
		return value;
	}
}
