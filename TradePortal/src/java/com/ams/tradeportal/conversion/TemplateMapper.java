/*
 * @(#)TemplateMapper.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Template Mapper class.  This class is
 * used to map a row of data into an XML document for use with
 * a Trade Portal Mediator. This mapper uses a mapper data Template to
 * translate the data fields into document attributes.
 *
 * @version		1.0 29 Oct 2002

 */
public class TemplateMapper extends Mapper {
private static final Logger LOG = LoggerFactory.getLogger(TemplateMapper.class);
	/*
	 * Constants
	 */

	/* Template Mapper Columns */

	/** Field Name */
	public static final int TEMPLATE_COL_NAME				= 0;

	/** Field Type */
	public static final int TEMPLATE_COL_TYPE				= 1;

	/** Field Size */
	public static final int TEMPLATE_COL_SIZE				= 2;

	/** Preset Category */
	public static final int TEMPLATE_COL_CATEGORY			= 3;

	/** Is Required Field */
	public static final int TEMPLATE_COL_REQUIRED			= 4;

	/** XML Document Attribute */
	public static final int TEMPLATE_COL_DOC_ATTRIBUTE		= 5;

	/** Field Population Type Indicator */
	public static final int TEMPLATE_COL_POPULATE_TYPE		= 6;

	/** Manual Field Entry Value */
	public static final int TEMPLATE_COL_MANUAL_VALUE		= 7;

	/** Max Fields */
	public static final int TEMPLATE_COL_MAXFIELDS			= 8;

	/* Required Incoming Data Fields */

	/** Corporate Customer Name */
	public static final int DATA_FIELD_CUSTOMER				= 0;

	/** Minimum number of data fields */
	public static final int DATA_FIELD_MINFIELDS			= 1;


	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Template Mapper given a mapper name, an
	 * ArrayList of specific Template mapper data,
	 * a Trade Portal User ID, and valid Client Bank.
	 * Also takes a Message Log object to log errors.
	 *
	 * @param name - Mapper name.
	 *
	 * @param mapperData - ArrayList of ArrayList rows of mapper data to
	 * translate data fields into attribute values in an XML document.
	 *
	 * @param userID - Trade Portal User ID.
	 *
	 * @param clientBank - An existing Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public TemplateMapper(String name,
						  MapperData mapperData,
						  String userID,
						  String clientBank,
						  MessageLog errorLog) {
		// Call Ancestor constructor
		super(name, mapperData, userID, clientBank, errorLog);

		// Set Mapper field locations for data extraction
		// Mapper Document Attribute
		mapperFieldList.add(String.valueOf(TEMPLATE_COL_DOC_ATTRIBUTE));

		// Mapper Field Population Type
		mapperFieldList.add(String.valueOf(TEMPLATE_COL_POPULATE_TYPE));

		// Mapper Data Manual Entry Value
		mapperFieldList.add(String.valueOf(TEMPLATE_COL_MANUAL_VALUE));
	}


	/*
	 * Public Methods
	 */

	/**
	 * Validate the incoming data row against the Template mapper data.
	 *
	 * @param dataRow - Row of parsed data fields.
	 *
	 * @return boolean, whether data row is valid.
	 */
	protected boolean validate(ArrayList dataRow) {
		// Call Ancestor Validate
		if (!super.validate(dataRow)) {
			return false;
		}

		// Check for minimum required number of fields
		if (dataRow.size() < DATA_FIELD_MINFIELDS) {
			errorLog.error("Incoming data requires at least "
				+ DATA_FIELD_MINFIELDS + " fields, fields found: "
				+ dataRow.size());
			return false;
		}

		// Verify specified required fields
		// Corporate Customer Name
		if (dataRow.get(DATA_FIELD_CUSTOMER).toString().length() == 0) {
			errorLog.error("Corporate Customer Name cannot be blank");
			return false;
		}

		// Validation Successful
		return true;
	}
}
