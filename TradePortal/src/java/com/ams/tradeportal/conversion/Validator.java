/*
 * @(#)Validator.java	1.0 2002/10/29
 *
 * Copyright 2002 American Management Systems.
 */


package com.ams.tradeportal.conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;

/**
 * Trade Portal Base Validator class.  This class is
 * used to validate an XML document prior to submission to the Mediator.
 *
 * @version		1.0 29 Oct 2002

 */
public class Validator {
private static final Logger LOG = LoggerFactory.getLogger(Validator.class);
	/*
	 * Members
	 */

	/** Parent Client Bank */
	protected String clientBank = "";

	/** Validator is Valid Flag */
	protected boolean objectIsValid = false;

	/** Error Log */
	protected MessageLog errorLog = null;

	/*
	 * Constructors
	 */

	/**
	 * Create a Trade Portal Validator given a parent Client Bank.
	 * Takes a Message Log object to log errors.
	 *
	 * @param clientBank - Parent Client Bank.
	 *
	 * @param errorLog - Message Log object to log errors.
	 */
	public Validator(String clientBank,
					 MessageLog errorLog) {
		// Check for null Error Log
		if (errorLog == null) {
			LOG.info("Error, Validator Error Log is invalid");
			return;
		}

		// Store Error Log
		this.errorLog = errorLog;

		// Check for empty Client Bank
		if ((clientBank == null)
			|| (clientBank.length() == 0)) {
			errorLog.error("No Client Bank specified, Validator is invalid");
			return;
		}

		// Store Client Bank
		this.clientBank = clientBank;

		// Validator is valid
		objectIsValid = true;
	}

	/*
	 * Public Methods
	 */

	/**
	 * Perform validations on the incoming data row and document.
	 *
	 * @param dataRow - Array of parsed data fields.
	 *
	 * @param document - XML Document to be validated.
	 */
	public boolean validate(ArrayList dataRow,
							DocumentHandler document) {
		// Check for valid Validator
		if (!objectIsValid) {
			// Attempt to log error to file
			try {
				errorLog.error("Validator is invalid");
			} catch (NullPointerException e) {
				LOG.info("Validator is invalid");
			}

			return false;
		}

		// Check for empty document
		if (document == null) {
			errorLog.error("Validation failed, ocument is empty");
			return false;
		}

		// Document validated
		return true;
	}
}
