/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;
import java.util.Map;

/**

 *
 */
public class ARMMatchingRuleDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(ARMMatchingRuleDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public ARMMatchingRuleDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        String includeSubAccessUserOrg = null;
        String subAccessUserOrgID = null;
        if (getUserSession().showOrgDataUnderSubAccess()) {
            includeSubAccessUserOrg = "Y";
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }

        String buyerId = parameters.get("buyerId");
        String buyerName = parameters.get("buyerName");
        sql.append("where m.p_corp_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
            sql.append(",?");
            sqlParams.add(clientBankID);
            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
                sql.append(",?");
                sqlParams.add(bogID);
            }
        }

        // Possibly include the subsidiary access user org's data also
        if (includeSubAccessUserOrg != null) {
            sql.append(",?");
            sqlParams.add(subAccessUserOrgID);
        }

        sql.append(")");

	       // Search by the Buyer Name
        //	if ((buyerId.equals("")) && !buyerName.equals("")) {
        if (StringFunction.isBlank(buyerId) && StringFunction.isNotBlank(buyerName)) {
            sql.append(" and UPPER(buyer_name) like (?) ");
            sqlParams.add("%" + buyerName + "%");
        }
        // Search by the Buyer ID
        if (StringFunction.isNotBlank(buyerId) && StringFunction.isBlank(buyerName)) {
            sql.append(" and UPPER(buyer_id ) like (?)");
            sqlParams.add("%" + buyerId + "%");
        }
        // Search by the Buyer ID and the Buyer Name
        if (StringFunction.isNotBlank(buyerId) && StringFunction.isNotBlank(buyerName)) {
            sql.append(" and (UPPER(buyer_id) like ? or UPPER(buyer_name) like ?)");
            sqlParams.add("%" + buyerId + "%");
            sqlParams.add("%" + buyerName + "%");
        }
        // Build the where clause - All fields are empty
        if (StringFunction.isBlank(buyerId) && StringFunction.isBlank(buyerName)) {
            sql.append("");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
