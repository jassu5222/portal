/**
 *
 */
package com.ams.tradeportal.dataview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import java.text.ParseException;

/**
 * 
 *
 */
public abstract class AbstractDBDataView extends AbstractDataView {
	private static final Logger LOG = LoggerFactory.getLogger(AbstractDBDataView.class);

	public String fullSQL = "";
	public String fullCountSQL = "";
	private ResultSet resultSet = null;

	private java.sql.ResultSetMetaData rsmd = null;
	// create a mapping between column names and index
	private Map<String, Integer> columnNameMap = null;

	/**
	 * Get data view data.
	 *
	 * @param viewName
	 *            - view name which have a data view object of same name
	 * @param parameters
	 *            - all url parameters passed to the data view servlet
	 * @param fmt
	 *            - the format to return. by default JSON
	 * @return
	 * @throws java.lang.Exception
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt) throws Exception {
		return getData(viewName, parameters, fmt, true);
	}

	/**
	 * Get data view data.
	 *
	 * @param viewName
	 *            - view name which have a data view object of same name
	 * @param parameters
	 *            - all url parameters passed to the data view servlet
	 * @param fmt
	 *            - the format to return. by default JSON
	 * @return
	 * @throws java.lang.Exception
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt, Boolean encrypt) throws Exception {
		super.getData(viewName, parameters, fmt, encrypt); // IR 16481

		// ensure all the basic stuff is passed in
		if (null == viewName || null == parameters || null == fmt) {
			throw new Exception("Required arguments are missing");
		}
		LOG.debug("Parameters for DataView {} are  {} ", viewName, parameters);
		DataViewManager.DataViewMetadata metadata = getDataViewMetadata(viewName);

		setFormatType(fmt);

		String dynamicWhereClause = buildDynamicSQLCriteria(parameters);

		setSortColumn(metadata, parameters);
		String querySQL;
		if (fullSQL != null && fullSQL.trim().length() > 1) {
			querySQL = fullSQL;
		} else {
			querySQL = buildQuery(metadata, dynamicWhereClause);
		}

		if (null == querySQL) {
			throw new Exception("Problem building query sql.");
		}

		LOG.debug("querySQL in getData = {}", querySQL);
		
		String countSQL;
		if (fullCountSQL != null && fullCountSQL.trim().length() > 1) {
			countSQL = fullCountSQL;
		} else {
			countSQL = buildCountQuery(metadata, dynamicWhereClause);
		}

		if (null == countSQL) {
			throw new Exception("Problem building count sql.");
		}
		
		LOG.debug("countSQL in getData = {}", countSQL);

		String output;
		try {
			output = executeQuery(metadata, parameters, querySQL, countSQL);
		} catch (Exception e) {
			LOG.error("Exception occured getting data in dataview {}  ", viewName,e);
			throw e;

		}
		
		LOG.debug("OutputJSON from DataView {} : {}",viewName , output);
		return output;
	}

	/**
	 * Subclasses must implement this. This method is used to generate the dynamic sql that is also applied to the query to filter
	 * by search criteria.
	 *
	 * Note that this sql is used in 2 queries: once for the actual query limited by a specific # of rows, and second for a count of
	 * all rows.
	 * 
	 * @param parameters
	 * @return
	 * @throws com.ams.tradeportal.dataview.DataViewException
	 */
	abstract protected String buildDynamicSQLCriteria(Map<String, String> parameters) throws DataViewException;

	/**
	 * Constructs the SQL used to query the database. This consists of the select clause, the from clause, and two where clauses.
	 * The select, from, and first where clause were previously loaded via loaddataViewMetadata. The second where clause is passed
	 * in via the request. This resulting sql will retrieve only rowsPerPage rows and begins with startRow. ie. select
	 * <columns> from (select rownum as row_num, <columns> from (select <columns> from where order by )) where row_num > and row_num
	 * < Use decode() in select clause with caution due to the limited capability of the parser used here.
	 *
	 * @param metadata
	 * @param dynamicWhereClause
	 * @return java.lang.String
	 */
	public String buildQuery(DataViewManager.DataViewMetadata metadata, String dynamicWhereClause) {

		StringBuilder innerSql = new StringBuilder();
		StringBuilder middleSql = new StringBuilder();
		StringBuilder outerSql = new StringBuilder();
		StringBuilder middleOuterColumns = new StringBuilder();
		String sortColDataType = "";
		if (sortColumn != -1) { // note this is not the metadata!
			sortColDataType = metadata.columns[sortColumn].dataType;
		}

		// First build inner SQL statement. This is the one that actually retrieves the data in appropriate sort column
		// and order. If we're sorting on a refdata column we need to add extra selection, from, and where criteria to allow
		// sorting on the refdata code's description (in refdata table) rather than on the refdata code.
		if (sortColDataType.equals(DataViewManager.REFDATA)) {
			innerSql.append(metadata.selectClause);
			innerSql.append(", refdata.descr as RefDataDescr");
			// cquinton 10/25/2013 Rel 8.3 ir#21594. sort by row_num on outer query
			// outerSortRefdata="RefDataDescr"; // put innerSql RefDataDescr column to use it in outerSql sorting
			innerSql.append(' ');
			innerSql.append(metadata.fromClause);
			innerSql.append(", refdata");
			innerSql.append(' ');
			innerSql.append(metadata.whereClause);
			innerSql.append(' ');
			innerSql.append(dynamicWhereClause);
			if (StringFunction.isBlank(metadata.whereClause) && StringFunction.isBlank(dynamicWhereClause)) {
				innerSql.append(" WHERE ");
			} else {
				innerSql.append(" AND ("); // PCUTRONE-(IR-GNUG100258356)[18-SEP-07]-REMOVE QUOTE IN "AND ('"
			}
			// We need to join the refdata table using the code column. However, an alias does not work in a WHERE clause so we'll
			// parse out the db column name for the given alias.
			String dbSortColName = getDbColumnForAlias(metadata, metadata.columns[sortColumn].columnKey);
			innerSql.append(dbSortColName);
			innerSql.append(" = refdata.code AND refdata.table_type ='");
			innerSql.append(metadata.columns[sortColumn].tableType);

			// Try to join in by the user's locale
			// If description does not exist for that locale, use the null default locale data

			innerSql.append("' AND ((refdata.locale_name = '");
			innerSql.append(getResourceManager().getResourceLocale());
			innerSql.append("') OR");
			innerSql.append("  ((refdata.locale_name is null)) and"); // PCUTRONE-(IR-GNUG100258356)[18-SEP-07]-ADD PARENTHESIS IN "
																		// ((refdata.locale_name is null) and"

			innerSql.append("   ((( select count(*)");
			innerSql.append("     from refdata rrr");
			innerSql.append("     where rrr.code=refdata.code and ");
			innerSql.append("           rrr.table_type = '");
			innerSql.append(metadata.columns[sortColumn].tableType);
			innerSql.append("' and ");
			innerSql.append("           rrr.locale_name = '");
			innerSql.append(getResourceManager().getResourceLocale());
			innerSql.append("') = 0) ");

			innerSql.append("or (select count(*)");
			innerSql.append("     from refdata rrr");
			innerSql.append("     where rrr.code=refdata.code and ");
			innerSql.append("           rrr.table_type = '");
			innerSql.append(metadata.columns[sortColumn].tableType);
			innerSql.append("' and ");
			innerSql.append("           rrr.locale_name is null");
			innerSql.append(") = 0)) ");
			if (StringFunction.isNotBlank(metadata.whereClause) || StringFunction.isNotBlank(dynamicWhereClause)) {
				innerSql.append(')');
			}

		} else {
			innerSql.append(metadata.selectClause);
			innerSql.append(' ');
			innerSql.append(metadata.fromClause);
			innerSql.append(' ');
			innerSql.append(metadata.whereClause);
			innerSql.append(' ');
			innerSql.append(dynamicWhereClause);
		}

		if (sortColDataType.equals(DataViewManager.DATE) || sortColDataType.equals(DataViewManager.DECIMAL)
				|| sortColDataType.equals(DataViewManager.NUMBER) || sortColDataType.equals(DataViewManager.GMTDATE)
				|| sortColDataType.equals(DataViewManager.GMTDATETIME) || sortColDataType.equals(DataViewManager.DATETIME) || // Srinivasu_D IR#T36000040369 Rel9.3(WS) 06/29/2015 -to handle datetime
				sortColDataType.equals(DataViewManager.CURRENCY)) {
			innerSql.append(" order by ");
			innerSql.append(metadata.columns[sortColumn].columnKey);
		} else if (sortColDataType.equals(DataViewManager.REFDATA)) {
			innerSql.append(" order by ");
			innerSql.append(getResourceManager().localizeOrderBy("upper(RefDataDescr)"));
		}

		else if (sortColumn != -1) {
			innerSql.append(" order by ");
			innerSql.append(getResourceManager().localizeOrderBy("upper(" + metadata.columns[sortColumn].columnKey + ')'));
		}
		if (sortColumn != -1) {
			if (SORT__DESCENDING.equals(sortOrder))
				innerSql.append(" DESC");
		}

		// build the select statement used in the middle and outer
		// SQL statement by extracting the column name that is extracted from the inner SQL.

		StringTokenizer columnPhrases = new StringTokenizer(metadata.selectClause, ",");

		while (columnPhrases.hasMoreElements()) {
			String columnPhrase = columnPhrases.nextToken();

			columnPhrase = columnPhrase.trim();

			int wordIndex = columnPhrase.lastIndexOf(' ');

			String columnName = columnPhrase.substring(wordIndex + 1);
			columnName = columnName.trim();

			int dotIndex = columnName.indexOf('.');
			if (dotIndex > -1) {
				columnName = columnName.substring(dotIndex + 1);
			}

			// When the syntax gets a little complicated, the parser may mistaken some token for column name when they are not.
			// Suffix such tokens with /*not_column_name*/. For example,
			// decode(a.copy_of_currency_code, null, active_tran.copy_of_currency_code, a.copy_of_currency_code) as Currency,
			// should be written as
			// decode(a.copy_of_currency_code/*not_column_name*/, null/*not_column_name*/,
			// active_tran.copy_of_currency_code/*not_column_name*/, a.copy_of_currency_code/*not_column_name*/) as Currency,
			if (!columnName.endsWith("/*not_column_name*/")) {
				middleOuterColumns.append(columnName);
				if (columnPhrases.hasMoreElements())
					middleOuterColumns.append(", ");
			}
		}

		// build the middle sql that selects the row number as row_num and all the columns and rows selected from innerSql, ie.
		//select columns, row as row_num from (innerSql)

		middleSql.append("select ");
		middleSql.append(middleOuterColumns);

		middleSql.append(", rownum row_num from (");
		middleSql.append(innerSql);
		middleSql.append(" ) ");

		// build the outer sql that consists of all data previously selected and limit the rows selected

		outerSql.append("select ");
		outerSql.append(middleOuterColumns);

		// MEerupula Rel 8.3 T36000021433/20624 sql tuning STARTS HERE
		// ctq added for sql performance
		if (StringFunction.isNotBlank(metadata.outerSelectClause)) {
			outerSql.append(',');
			outerSql.append(metadata.outerSelectClause);
		}

		outerSql.append(" from (");
		outerSql.append(middleSql);
		outerSql.append(')');
		if (StringFunction.isNotBlank(metadata.outerFromClause)) {
			outerSql.append(", ");
			outerSql.append(metadata.outerFromClause);
		}
		outerSql.append(" where row_num > ?");
		outerSql.append(" and row_num <= ?");
		if (StringFunction.isNotBlank(metadata.outerWhereClause)) {
			outerSql.append(" and ");
			outerSql.append(metadata.outerWhereClause);

			// cquinton 10/25/2013 Rel 8.3 ir#21594. sort by row_num on outer query
			// this seems to help with the optimizer rather than ordering on the original column again
			outerSql.append(" order by row_num");
		}

		return outerSql.toString();
	}

	/**
	 * This method returns the physical database column name for a given alias in the select clause. For example, if the select
	 * clause is select fx_rate_oid, currrency_code as Currency, rate as Rate and we need the db column for 'Currency', the result
	 * will be currency_code.
	 *
	 * @param metadata
	 * @return String - physical name for the given alias
	 * @param alias
	 *            String - an alias in the select clause to find the physical name for.
	 */
	@Override
	public String getDbColumnForAlias(DataViewManager.DataViewMetadata metadata, String alias) {
		String clause = metadata.selectClause;

		// First find the alias in the select clause and then strip off everything from the preceding " AS " to the end.
		int end = clause.indexOf(' ' + alias);
		clause = clause.substring(0, end - 3);

		// The clause should now end with the name of the column we are attempting to get. Find the space before this name.
		int start = clause.lastIndexOf(',');
		// modified below code as previously it was expecting that every SQL will have space after comma but in this IR
		// column, which is getting sort, don't have space after comma. even if to include space also in query no issue.
		return clause.substring(start + 1);
	}

	/**
	 * Constructs the SQL used to query the database for the number of rows. This consists of the select count clause of the first
	 * column of the sql (ie select count(first_column), the from clause, and two where clauses. The select, from, and first where
	 * clause were previously loaded via loaddataViewMetadata. The second where clause is passed in via the request. It assumes that
	 * the first column of the select clause is unique and not empty for all rows selected.
	 *
	 * @param metadata
	 * @param dynamicWhereClause
	 * @return java.lang.String
	 */
	public String buildCountQuery(DataViewManager.DataViewMetadata metadata, String dynamicWhereClause) {

		StringBuilder sql = new StringBuilder();

		// generate the select count(first column) to produce the total number of rows with a given criteria
		int deCodeIndex = metadata.selectClause.indexOf("decode");
		int commaIndex = metadata.selectClause.indexOf(',');

		String selectFirstColumn;
		String firstColumn;
		int spaceIndex;
		if (deCodeIndex > 0 && deCodeIndex < commaIndex) {
			int perntIndex = metadata.selectClause.indexOf(')', deCodeIndex);
			selectFirstColumn = metadata.selectClause.substring(0, perntIndex + 2);
			selectFirstColumn = selectFirstColumn.trim();
			spaceIndex = selectFirstColumn.lastIndexOf(' ');
			firstColumn = selectFirstColumn.substring(spaceIndex + 1);

		} else {
			selectFirstColumn = metadata.selectClause.substring(0, commaIndex);
			selectFirstColumn = selectFirstColumn.trim();
			spaceIndex = selectFirstColumn.lastIndexOf(' ');
			firstColumn = selectFirstColumn.substring(spaceIndex + 1);
		}

		if (dynamicWhereClause.toUpperCase().indexOf("GROUP BY") > 0) {
			sql.append("select count(count(");
			sql.append(firstColumn);
			sql.append(")) as count ");
		} else {
			sql.append("select count(");
			sql.append(firstColumn);
			sql.append(") as count ");
		}

		/*
		 * If the outerFromClause is not empty then (wrap the inner sql in outer tags)select from inner tables and then join with
		 * outer table else just select from inner tables
		 */
		if (StringFunction.isNotBlank(metadata.outerFromClause)) {
			sql.append("from (");
			sql.append(metadata.selectClause);
			sql.append(' ');
		}
		sql.append(metadata.fromClause);
		sql.append(' ');
		sql.append(metadata.whereClause);

		sql.append(' ');
		sql.append(dynamicWhereClause);

		if (StringFunction.isNotBlank(metadata.outerFromClause)) {
			sql.append(") ");
			int dotIndex = firstColumn.indexOf('.');
			if (dotIndex > 0) {
				String firstColumnTableAlias = firstColumn.substring(0, dotIndex);
				sql.append(firstColumnTableAlias);
			}
			sql.append(", ");
			sql.append(metadata.outerFromClause);
		}

		if (StringFunction.isNotBlank(metadata.outerWhereClause)) {
			sql.append(" where ");
			sql.append(metadata.outerWhereClause);
		}
		// MEerupula Rel 8.3 T36000021433/20624 sql tuning ENDS HERE

		return sql.toString();
	}

	/**
	 * @return
	 * @throws AmsException
	 */
	protected Map<String, Integer> getColumnMap() throws AmsException {
		Map<String, Integer> columnNameMap = new HashMap<>();
		try {
			int numRSColumns = rsmd.getColumnCount();

			for (int i = 1; i < numRSColumns + 1; i++) {
				String columnName = rsmd.getColumnName(i);
				columnNameMap.put(columnName, i);
			}
		} catch (SQLException ex) {
			throw new AmsException(ex);
		}
		return columnNameMap;
	}

	/**
	 * @param metadata
	 * @param parameters
	 * @param querySql
	 * @param countSql
	 * @return
	 * @throws Exception
	 */
	private String executeQuery(DataViewManager.DataViewMetadata metadata, Map<String, String> parameters, String querySql, String countSql) throws Exception {

		String parentId = parameters.get("parentId");
		String outputJSON = null;
		try (Connection connection = DatabaseQueryBean.connect(false)) {
			if (null != connection) {
				// first get the count query results
				PreparedStatement countStatement = connection.prepareStatement(countSql);
				countStatement = setCountQueryParameters(countStatement, parameters);
				int totalRecordCount = 0;
				try {
					resultSet = countStatement.executeQuery();
					if (resultSet.next()) {
						String count = resultSet.getString(1);
						totalRecordCount = Integer.parseInt(count);
					} else {
						// log an error, but move on
						LOG.warn("Error executing count query in dataview: {} - no resultSet rows.", parameters.get("vName"));
					}
					LOG.debug("Total Record Count - {}", totalRecordCount);

				} catch (Exception e) {
					// issue with count sql while executing,catching here so atleast it will display grid data.
					//LOG.error("Exception occured in view {} executing countSQL==> {} ",  parameters.get("vName"), countSql, e);
					LOG.error("Exception occured in view " + parameters.get("vName") + " executing countSQL==> {} ",  countSql, e);

				}

				PreparedStatement prepareQueryStatement = connection.prepareStatement(querySql);
				prepareQueryStatement = setQueryParameters(prepareQueryStatement, parameters);

				resultSet = prepareQueryStatement.executeQuery();

				if (null != resultSet) {
					rsmd = resultSet.getMetaData();
					columnNameMap = getColumnMap();
					outputJSON = renderJSON(metadata, totalRecordCount, parentId, parameters, true);
				}

			}
		} catch (Exception e) {
			//LOG.error("Exception occured in view {} executing querySQL==> {} ",  parameters.get("vName"), querySql, e);
 			  LOG.error("Exception occured in view " + parameters.get("vName") + " executing querySQL==> {} ",  querySql, e);
		}

		
		return outputJSON;
	}

	/**
	 * @param statement
	 * @param parameters
	 * @return
	 * @throws SQLException
	 * @throws DataViewException
	 */
	private PreparedStatement setQueryParameters(PreparedStatement statement, Map<String, String> parameters)
			throws SQLException, DataViewException {
		int varIdx = 1;

		int dynamicVarCount = setDynamicSQLValues(statement, varIdx, parameters);
		varIdx += dynamicVarCount;

		String startStr = parameters.get("start");
		int start = Integer.parseInt(startStr);
		statement.setInt(varIdx, start);
		varIdx++;

		String countStr = parameters.get("count");
		int count = Integer.parseInt(countStr);
		statement.setInt(varIdx, start + count);

		return statement;
	}

	/**
	 * @param statement
	 * @param parameters
	 * @return
	 * @throws SQLException
	 * @throws DataViewException
	 */
	private PreparedStatement setCountQueryParameters(PreparedStatement statement, Map<String, String> parameters)
			throws SQLException, DataViewException {

		int varIdx = 1;
		setDynamicSQLValues(statement, varIdx, parameters);

		return statement;
	}

	/**
	 * Method to be implemented by subclass -- Note that this is called twice - once for the actual query limited by a specific # of
	 * rows, and second for a count of all rows.
	 *
	 * @param statement
	 * @param startVarIdx
	 * @param parameters
	 * @return
	 * @throws SQLException
	 * @throws com.ams.tradeportal.dataview.DataViewException
	 */
	abstract protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, Map<String, String> parameters)
			throws SQLException, DataViewException;

	@Override
	protected Object getDataValue(String col) throws AmsException {
		String dataValue = null;

		try {
			// below just mimics what we do in DatabaseQueryBean, but
			// does not put to xml
			Integer columnIndexInt = columnNameMap.get(col.toUpperCase());
			if (columnIndexInt != null) {
				int rsIdx = columnIndexInt;
				int sqlType = rsmd.getColumnType(rsIdx);
				Object rawColumnData = resultSet.getObject(rsIdx);

				if (sqlType == Types.DATE && rawColumnData != null) {
					String date = resultSet.getDate(rsIdx).toString();
					String time = resultSet.getTime(rsIdx).toString();

					rawColumnData = date + ' ' + time + ".0";
				}
				if (rawColumnData == null) {
					dataValue = "";
				} else {
					dataValue = rawColumnData.toString();
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception in getDataValue", e);
			throw new AmsException(e);
		}

		return dataValue;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ams.tradeportal.dataview.AbstractDataView#hasNext()
	 */
	@Override
	protected boolean hasNext() throws AmsException {
		try {
			return resultSet.next();
		} catch (SQLException e) {

			throw new AmsException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ams.tradeportal.dataview.AbstractDataView#hasNext()
	 */
	protected int setSqlVarsToPreparedStmt(PreparedStatement statement, int startVarIdx, List<Object> paramList)
			throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, paramList.toArray());
	}

	protected int setSqlVarsToPreparedStmt(PreparedStatement statement, int startVarIdx, Object... sqlVars) throws SQLException {
		int varCount;
		// now set the inputs!
		for (varCount = 0; varCount < sqlVars.length; varCount++) {
			if (sqlVars[varCount] != null) {
				statement.setObject(startVarIdx + varCount, sqlVars[varCount]);
			} else {
				statement.setNull(startVarIdx + varCount, Types.NULL);
			}
		}

		return varCount;
	}

	/**
	 * This method will take ',' separated string value, and construct string with place holder(?,?) string for in clause, and adds
	 * the values to List
	 * 
	 * @param inParams
	 * @param sqlParams
	 * @return String
	 */
	protected String prepareDynamicSqlStrForInClause(String inParams, List<Object> sqlParams) {

		return prepareDynamicSqlStrForInClause(inParams, sqlParams, true);
	}

	/**
	 * This method will take ',' separated string value, and construct string with place holder(?,?) string for in clause, and adds
	 * the values to List
	 * 
	 * @param inParams
	 * @param sqlParams
	 * @param addToParamList
	 * @return String
	 */
	protected String prepareDynamicSqlStrForInClause(String inParams, List<Object> sqlParams, boolean addToParamList) {

		StringBuilder placeHolderStr = new StringBuilder("");
		inParams = inParams.replaceAll("'", "");

		String[] parts = inParams.split(",");
		for (int i = 0; i < parts.length; i++) {
			placeHolderStr.append('?');
			if (parts.length - 1 > i) {
				placeHolderStr.append(',');
			}
			if (addToParamList) {
				sqlParams.add(parts[i].trim());
			}
		}
		return placeHolderStr.toString();
	}

	/**
	 * This method return restricted bank groups that the user has associated to.
	 * 
	 * @param bankGrpRestrictRuleOid
	 * @return String
	 */
	protected List<String> filterBankGroups(String bankGrpRestrictRuleOid) {
		List<String> filteredBankGroups = new ArrayList<>();

		if (StringFunction.isNotBlank(bankGrpRestrictRuleOid)) {
			try {
				List<Object> params = new ArrayList<Object>();
				params.add(bankGrpRestrictRuleOid);
				String query = "select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ";
				DocumentHandler bankGrpList = DatabaseQueryBean.getXmlResultSet(query, false, params);
				List<DocumentHandler> bgList = bankGrpList.getFragmentsList("/ResultSetRecord");
				for (DocumentHandler bankGrpforrestricDoc : bgList) {
					filteredBankGroups.add(bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
				}
			} catch (AmsException e) {
				LOG.error("Exception in filterBankGroups", e);
			}
		}

		return filteredBankGroups;
	}

	/**
	 * SSikhakolli - Rel 9.3.5 IR#T36000045191 Adding below util method to validate 'User entered value in date field in UI' with
	 * 'date format driven from user session'
	 * 
	 * @param format
	 * @param value
	 * @return boolean
	 */
	protected boolean isValidDateFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			LOG.error("User entered value {} in date field dosen't match with required date format {}", value, format);
			LOG.error("Exception occured while validating date in isValidDateFormat() of AbstractDBDataView.java: ", ex);
		}
		return date != null;
	}
}
