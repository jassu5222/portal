/**
 *
 */
package com.ams.tradeportal.dataview;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.InstrumentWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.html.AbstractCustomListViewColumnFormatter;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

/**
 *
 */
public abstract class AbstractDataView implements DataView {
	private static final Logger LOG = LoggerFactory.getLogger(AbstractDataView.class);

	public static final String SORT__ASCENDING = "A";
	public static final String SORT__DESCENDING = "D";

	// the following vars just pull stuff from session for formatting presentation things
	// not sure if this is best, but its easiest with existing architecture
	private ResourceManager resMgr = null;
	private SessionWebBean userSession = null;
	// cquinton 11/11/2013 Rel 8.4 web services -
	// form manager, bean manager, and http servlet response are specific to dataviews used by the user interface.
	// for use by web service, these will be null and we use an indicator to avoid use of them - ideally these would not
	// be here at all but we simplify ui by doing some parts here
	private FormManager formMgr = null;
	private BeanManager beanMgr = null;
	// response particularly is just used by the format manager to encode url parameters -- assuming its
	// ok although the response we are using is the ajax response rather than the actual page response...
	private HttpServletResponse response = null;

	private String outputTarget = OUTPUT_TARGET__USER_INTERFACE; // default

	// cquinton 1/25/2013 add error manager for user errors
	// error manager collects user errors that are to be returned
	private final ErrorManager errMgr = new ErrorManager();

	private String formatType = "JSON";

	// this is the currently selected sorting column and order
	protected int sortColumn = 0; // by default the first. -1 means none
	protected String sortOrder = SORT__ASCENDING;

	public ResourceManager getResourceManager() {
		return this.resMgr;
	}

	public SessionWebBean getUserSession() {
		return this.userSession;
	}

	public FormManager getFormManager() {
		return this.formMgr;
	}

	public BeanManager getBeanManager() {
		return this.beanMgr;
	}

	public HttpServletResponse getHttpResponse() {
		return this.response;
	}

	@Override
	public String getOutputTarget() {
		return outputTarget;
	}

	@Override
	public void setOutputTarget(String x) {
		this.outputTarget = x;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	/**
	 * Must do this.
	 * 
	 * @param resourceManager
	 */
	@Override
	public void setResourceManager(ResourceManager resourceManager) {
		this.resMgr = resourceManager;
	}

	/**
	 * Must do this.
	 * 
	 * @param swb
	 */
	@Override
	public void setUserSession(SessionWebBean swb) {
		this.userSession = swb;
	}

	/**
	 * Must do this.
	 * 
	 * @param fm
	 */
	@Override
	public void setFormManager(FormManager fm) {
		this.formMgr = fm;
	}

	/**
	 * Must do this.
	 */
	@Override
	public void setBeanManager(BeanManager bm) {
		this.beanMgr = bm;
	}

	/**
	 * Must do this.
	 */
	@Override
	public void setHttpResponse(HttpServletResponse r) {
		this.response = r;
	}

	/**
	 * The error manager for user errors
	 */
	@Override
	public ErrorManager getErrorManager() {
		return errMgr;
	}

	// the following getters represent set of user session variables that are directly accessible by the dataview class
	// these exist because userSession is probably not something that will be retained in future for non-presentation access,
	// but we need these values to be available (but not passed in via url parameter for security!!!)
	public String getGlobalOrgOid() {
		if (userSession != null) {
			return userSession.getGlobalOrgOid();
		}
		return null;
	}

	public String getClientBankOid() {
		if (userSession != null) {
			return userSession.getClientBankOid();
		}
		return null;
	}

	public String getBogOid() {
		if (userSession != null) {
			return userSession.getBogOid();
		}
		return null;
	}

	public String getOwnerOrgOid() {
		if (userSession != null) {
			return userSession.getOwnerOrgOid();
		}
		return null;
	}

	public String getOwnershipLevel() {
		if (userSession != null) {
			return userSession.getOwnershipLevel();
		}
		return null;
	}

	public String getSecurityType() {
		if (userSession != null) {
			return userSession.getSecurityType();
		}
		return null;
	}

	public String getUserOid() {
		if (userSession != null) {
			return userSession.getUserOid();
		}
		return null;
	}

	/**
	 * Get data view data.
	 *
	 * @param viewName
	 *            - view name which have a data view object of same name
	 * @param parameters
	 *            - all url parameters passed to the data view servlet
	 * @param fmt
	 *            - the format to return. by default JSON
	 * @return
	 */
	/*
	 * IR 16481 start public abstract String getData(String viewName, HashMap parameters, String fmt) throws Exception;
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt) throws Exception {
		return getData(viewName, parameters, fmt, true);
	}

	/**
	 * Get data view data.
	 *
	 * @param viewName
	 *            - view name which have a data view object of same name
	 * @param parameters
	 *            - all url parameters passed to the data view servlet
	 * @param fmt
	 *            - the format to return. by default JSON
	 * @return
	 */
	/*
	 * IR 16481 start public abstract String getData(String viewName, HashMap parameters, String fmt) throws Exception;
	 */
	@Override
	public String getData(String viewName, Map<String, String> parameters, String fmt, Boolean encrypt) throws Exception {
		DataViewManager.DataViewMetadata metaData = getDataViewMetadata(viewName);
		// IR 20278 -- if this is triggered on expanding tree view on list page then do not store
		if (metaData.isPreserveSearchCriteriaEnabled() && !ignorePassedInParam(parameters)) {

			HashMap<String, String> map = new HashMap<>(parameters);
			map.remove("loginLocale");
			map.remove("start");
			map.remove("count");
			getUserSession().setSavedSearchQueryData(viewName, map);
		}
		return null;

	}

	protected boolean ignorePassedInParam(Map<String, String> parameters) {
		return false;
	}

	/**
	 * This method gets the data view metadata. It does this by using the DataViewManager class (which reads in an xml file for a
	 * given data view). The DataViewManager returns a hashtable of the parameters. We extract these into local variables. Any
	 * errors found while reading in the parameters are printed to the console.
	 *
	 * @param dataView
	 *            java.lang.String - dataview to load
	 * @return
	 */

	protected DataViewManager.DataViewMetadata getDataViewMetadata(String dataView) {

		DataViewManager.DataViewMetadata metadata = DataViewManager.getDataViewMetadata(dataView);

		LOG.debug("Data view parms for {} are:\n {}" ,dataView, metadata);

		if (metadata.hasErrors) {
			LOG.warn("*********************************************");
			LOG.warn("The following errors were found for {} \n\r {}" ,dataView, metadata.errors);
			LOG.warn("*********************************************");
		}

		return metadata;
	}

	/**
	 * This method returns the physical database column name for a given alias in the select clause. For example, if the select
	 * clause is select fx_rate_oid, currrency_code as Currency, rate as Rate and we need the db column for 'Currency', the result
	 * will be currency_code.
	 *
	 * @return String - physical name for the given alias
	 * @param alias
	 *            String - an alias in the select clause to find the physical name for.
	 */
	public String getDbColumnForAlias(DataViewManager.DataViewMetadata metadata, String alias) {
		String clause = metadata.selectClause;

		// First find the alias in the select clause and then strip off everything from the preceding " AS " to the end.
		int end = clause.indexOf(' ' + alias);
		clause = clause.substring(0, end - 3);

		// The clause should now end with the name of the column we are attempting to get. Find the space before this name.
		int start = clause.lastIndexOf(',');
		return clause.substring(start + 2);
	}
	//RKAZI IR 47582 24-03-2016 REL 9.5 - START
	public int getSortColumn (){
		return sortColumn;
	}
	//RKAZI IR 47582 24-03-2016 REL 9.5 - END
	/**
	 * setSortColumn determines if a sort parameter has been passed and what to do with it.
	 *
	 * @param metadata
	 * @param parameters
	 */
	protected void setSortColumn(DataViewManager.DataViewMetadata metadata, Map<String, String> parameters) {
		// default sort values
		sortColumn = 0;
		sortOrder = SORT__ASCENDING;

		String sortColumnName = parameters.get("sort");
		if (sortColumnName != null && sortColumnName.length() > 0) {
			// QueryReadStore prepends a hyphen to the json field name when the column should be displayed descending
			if (sortColumnName.startsWith("-")) {
				sortOrder = SORT__DESCENDING;
				sortColumnName = sortColumnName.substring(1);
			} else {
				sortOrder = SORT__ASCENDING;
			}

			for (int i = 0; i < metadata.columns.length; i++) {
				if (sortColumnName.equals(metadata.columns[i].columnKey)) {
					sortColumn = i;
				}
			}
		} else if (metadata.sortColumn != 0) {
			sortColumn = metadata.sortColumn;
		}
	}

	/**
	 * @return
	 * @throws AmsException
	 */
	protected abstract boolean hasNext() throws AmsException;

	/**
	 * @param metadata
	 * @param totalRecordCount
	 * @param parentId
	 * @param parameters
	 * @param encrypt
	 * @return
	 * @throws Exception
	 */
	protected String renderJSON(DataViewManager.DataViewMetadata metadata, int totalRecordCount, String parentId,
			Map<String, String> parameters, Boolean encrypt) throws Exception {

		JSONArray json = new JSONArray();

		try {

			// spin through the rows
			while (hasNext()) {

				// create return object
				JSONObject jsonObj = new JSONObject();

				// first create a rowkey if there is one
				if (metadata.rowKeyValues != null) {
					String rowKeyValue = buildRowKeyValue(metadata, encrypt);
					jsonObj.put("rowKey", rowKeyValue);
					jsonObj.put("id", rowKeyValue);
				}

				// spin through dataview columns
				for (DataViewManager.DataViewColumn column : metadata.columns) {
					Object displayValue;
					Object value = getDataValue(column.columnKey);

					// format the data value
					// note that if this is a link, the value returned here is simply the display value
					Object formattedValue = formatDataValue(column, value, parentId);
					// Changes done for including suppressValue
					String strDataType = column.dataType;
					String strFormattedValue = formattedValue.toString();
					String suppressValueParam = "";

					StringBuilder additionalParms = new StringBuilder();

					if (parameters.get("suppressValue") != null) {
						suppressValueParam = parameters.get("suppressValue");
					}
					// IR T36000005764 PortalRefresh - PavaniMitnala BEGIN
					if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(column.getRelatedInstrID)) {
						InstrumentWebBean relatedInstrument = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
						relatedInstrument.getById(formattedValue.toString());
						displayValue = relatedInstrument.getAttribute("complete_instrument_id");
						// IR T36000005764 PortalRefresh - PavaniMitnala END
					} else {
						if (strDataType.equalsIgnoreCase("suppress_for_value")) {
							if (strFormattedValue.equalsIgnoreCase(suppressValueParam)) {
								displayValue = "";
							} else {
								displayValue = formattedValue;
							}
						} else if (strDataType.equalsIgnoreCase("custom")) {
							try {
								Class customFormatterClass = Class.forName(column.customFormatter);
								AbstractCustomListViewColumnFormatter customFormatter = (AbstractCustomListViewColumnFormatter) customFormatterClass.newInstance();
								customFormatter.setUserSession(userSession);
								customFormatter.setResourceManager(resMgr);
								displayValue = customFormatter.format(formattedValue);
							} catch (Exception ex) {
								// if there is any issue just return the select data value
								displayValue = formattedValue;
							}
							// NOTE: the above instantiates a new custom formatter for every column row.
							// if performance is an issue look at a single formatter for the column
						} else {
							displayValue = formattedValue;
						}
					}
					jsonObj.put(column.columnKey, displayValue);

					// if this is a link, we add a new data item to the json that contains the linkUrl. the presentation layer will user this
					// to generate the link itself
					// cquinton 11/11/2013 Rel 8.4 web services --
					// only add link column is output target is the UI
					if (column.isLink && OUTPUT_TARGET__USER_INTERFACE.equals(getOutputTarget())) {

						// CR914- Support link to generated PDF -- start
						if (column.isGeneratedDoc && StringFunction.isNotBlank(column.docGenerator)) {

							String linkArgs[] = new String[4];

							linkArgs[0] = column.docGenerator;
							String rowKey = buildRowKeyValueForGeneratedDoc(metadata);
							linkArgs[1] = rowKey;
							linkArgs[2] = resMgr.getResourceLocale();
							linkArgs[3] = userSession.getBrandingDirectory();

							String linkUrl = formMgr.getDocumentLinkAsURLInFrame(linkArgs[0], "_blank", linkArgs, response);

							String linkUrlColumn = column.linkUrlColumn;

							if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
								linkUrlColumn = column.columnKey + "_linkUrl";
							}

							jsonObj.put(linkUrlColumn, linkUrl);
							// CR914- Support link to generated PDF -- end

						} else if (column.getRelatedInstrID.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES)) {
							// IR T36000005764 PortalRefresh - PavaniMitnala BEGIN

							additionalParms.append("&instrument_oid=");
							additionalParms.append(EncryptDecrypt.encryptStringUsingTripleDes(formattedValue.toString(), userSession.getSecretKey()));
							String linkUrl = formMgr.getLinkAsUrl("goToInstrumentSummary", additionalParms.toString(), response);
							String linkUrlColumn = column.linkUrlColumn;
							if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
								linkUrlColumn = column.columnKey + "_linkUrl";
							}
							jsonObj.put(linkUrlColumn, linkUrl);
							// IR T36000005764 PortalRefresh - PavaniMitnala END
						} else {
							String linkUrlColumn = column.linkUrlColumn;
							// IR-T36000004272 PortalRefresh Change Begin - PavaniMitnala(Adding implementation for dataType suppress_for_value)
							if (!(strDataType.equalsIgnoreCase("suppress_for_value"))) {
								String linkUrl = buildColumnLinkUrl(column, encrypt);
								if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
									linkUrlColumn = column.columnKey + "_linkUrl";
								}
								// IR-T36000003569 PortalRefresh Change Begin - PavaniMitnala (This is to create no link incase the linkUrl is blank)
								if (linkUrl.equals("")) {
									linkUrlColumn = "";
								}
								// IR-T36000003569 PortalRefresh Change End
								jsonObj.put(linkUrlColumn, linkUrl);
							} else if (strDataType.equals("suppress_for_value") && !(strFormattedValue.equalsIgnoreCase(suppressValueParam))) {
								String linkUrl = buildColumnLinkUrl(column, encrypt);
								if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
									linkUrlColumn = column.columnKey + "_linkUrl";
								}
								jsonObj.put(linkUrlColumn, linkUrl);
							} else if (strDataType.equalsIgnoreCase("suppress_for_value")
									&& strFormattedValue.equalsIgnoreCase(suppressValueParam)) {
								if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
									linkUrlColumn = "";
								}
								jsonObj.put(linkUrlColumn, "");
							}
						}
					}
				}
				// IR-T36000004272 PortalRefresh Change End - PavaniMitnala
				json.add(jsonObj);
			}

		} catch (JSONException ex) {
			throw new Exception("Problem creating json data.", ex);
		}

		// get Application errors from error manager object. Add the same to grid in json format.
		List<IssuedError> errorList = this.errMgr.getIssuedErrorsList();

		JSONArray errorArray = new JSONArray();
		for (IssuedError err : errorList) {

			JSONObject errors = new JSONObject();
			errors.put("ErrorCode", err.getErrorCode());
			errors.put("ErrorText", err.getErrorText());
			errorArray.add(errors);
		}

		JSONObject serviceResult = new JSONObject();
		serviceResult.put("numRows", totalRecordCount);
		serviceResult.put("items", json.toArray());
		serviceResult.put("errors", errorArray); // //vdesingu Rel 8.4 CR-590
		// To add identifier to TreeDatagrid data
		for (DataViewManager.DataViewColumn column : metadata.columns) {
			if (column.columnKey.equals("rowKey")) {
				/* Added for Identifier in LazyTreeGrid */
				serviceResult.put("identifier", "rowKey");
			}
		}

		return serviceResult.toString();
	}

	/**
	 * Build the row key value for the row. This is the set of row key values using a "/" delimiter and encrypted.
	 *
	 * @param metadata
	 * @return
	 * @throws SQLException
	 */
	public String buildRowKeyValue(DataViewManager.DataViewMetadata metadata) // these last 3 are to get other column data
			// there must be a better way...
			throws Exception {
		return buildRowKeyValue(metadata, true);
	}

	/**
	 * Build the row key value for the row. This is the set of row key values using a "/" delimiter and encrypted.
	 *
	 * @param metadata
	 * @param encrypt
	 * @return
	 * @throws SQLException
	 */
	public String buildRowKeyValue(DataViewManager.DataViewMetadata metadata, Boolean encrypt) // these last 3 are to get other column data there must be a better way...
			throws Exception {
		StringBuilder rowKeyValueSB = new StringBuilder();

		for (int i = 0; i < metadata.rowKeyValues.length; i++) {

			String otherColumnKey = metadata.rowKeyValues[i];
			String otherColumnValue = (String) getDataValue(otherColumnKey);

			rowKeyValueSB.append(otherColumnValue);

			// the delimiter between row key values
			if (i < metadata.rowKeyValues.length - 1) {
				rowKeyValueSB.append('/');
			}
		}

		// encrypt it
		String rowKeyValue = rowKeyValueSB.toString();
		if (encrypt) {
			rowKeyValue = EncryptDecrypt.encryptStringUsingTripleDes(rowKeyValueSB.toString(), getUserSession().getSecretKey());
		}
		return rowKeyValue;
	}

	// maybe this should be string?
	protected abstract Object getDataValue(String col) throws AmsException;

	/**
	 * @param column
	 * @param valueObj
	 * @param parentId
	 * @return
	 * @throws Exception
	 */
	public Object formatDataValue(DataViewManager.DataViewColumn column, Object valueObj, String parentId) // these last 3 are to get other column data there must be a better way...
			// Added parameters for child refdata tabletypeC
			throws Exception {

		Object formattedValue;

		if (column == null) {
			return "";
		}

		// for now assume this - should check it...
		String value = (String) valueObj;
		// Added for rowKey encryption for treeGrid
		if (column.dataType.equals(DataViewManager.ENCRYPT)) {
			formattedValue = EncryptDecrypt.encryptStringUsingTripleDes(value, getUserSession().getSecretKey());
		} else if (column.dataType.equals(DataViewManager.STRING)) {
			// Prateep 31/01/2013 changing this to normal while this causing problem at the time of getting the values from grid
			// formattedValue = StringFunction.xssCharsToHtml(value);
			formattedValue = value;
		} else if (column.dataType.equals(DataViewManager.DATE)) {
			formattedValue = formatDate(value, DateFormat.SHORT);
		} else if (column.dataType.equals(DataViewManager.DATETIME)) {
			if ((value == null) || (value.equals(""))) {
				formattedValue = "";
			} else {
				formattedValue = formatDateTime(value, resMgr.getResourceLocale());
			}
		} else if (column.dataType.equals(DataViewManager.NUMBER)) {
			formattedValue = formatNumber(value);
		} else if (column.dataType.equals(DataViewManager.DECIMAL)) {
			formattedValue = formatDecimal(value, column.decimals, column.trailingZeros);
		} else if (column.dataType.equals(DataViewManager.CURRENCY)) {
			formattedValue = formatCurrency(value, column.currencyColumn, column.prefixCurrencyCode, resMgr.getResourceLocale());
		} else if (column.dataType.equals(DataViewManager.GMTDATE)) {
			if ((value == null) || (value.equals(""))) {
				formattedValue = "";
			} else {
				/* KMehta Rel8400 @ 02/21/2014 IR T36000023290 */
				/* Modified formatGMTDates to formatGMTDate */
				formattedValue = formatGMTDate(value, userSession.getTimeZone(), resMgr.getResourceLocale());
			}
		} else if (column.dataType.equals(DataViewManager.GMTDATETIME)) {
			if ((value == null) || (value.equals(""))) {
				formattedValue = "";
			} else {
				formattedValue = formatGMTDateTime(value, userSession.getTimeZone(), resMgr.getResourceLocale());
			}
		} else if (column.dataType.equals(DataViewManager.REFDATA)) {

			if (parentId != null && column.tableTypeC != null) {
				formattedValue = formatRefData(value, column.tableTypeC, resMgr.getResourceLocale());
			} else {
				formattedValue = formatRefData(value, column.tableType, resMgr.getResourceLocale());
			}

		} else if (column.dataType.equals(DataViewManager.BOOLEAN)) {
			formattedValue = formatBoolean(value, column.suppressNo);
		} else if (column.dataType.equals(DataViewManager.CUSTOM)) {
			try {
				Class customFormatterClass = Class.forName(column.customFormatter);
				AbstractCustomDataViewColumnFormatter customFormatter = (AbstractCustomDataViewColumnFormatter) customFormatterClass.newInstance();
				customFormatter.setResourceManager(this.resMgr);
				customFormatter.setUserSession(this.userSession);
				formattedValue = customFormatter.format(value);
			} catch (Exception ex) {
				// if there is any issue just return the select data value
				formattedValue = value;
			}
			// NOTE: the above instantiates a new custom formatter for every column row.
			// if performance is an issue look at a single formatter for the column
		} else {
			formattedValue = value;
		}

		if (null == formattedValue || "".equals(formattedValue)) {
			formattedValue = "";
		}

		return formattedValue;
	}

	/**
	 * Formats a number string. The number is assumed to be a long value and is formatted using the resource manager's number
	 * formatter. If the number cannot be formatted for some reason, the unformatted value is returned and an error printed to the
	 * console.
	 *
	 * @return java.lang.String - the formatted number
	 * @param value
	 *            java.lang.String - the number string to format
	 */
	public String formatNumber(String value) {
		String formattedValue;

		try {
			NumberFormat lf = resMgr.getNumberFormatter();

			formattedValue = lf.format(Long.parseLong(value));
		} catch (NumberFormatException e) {
			LOG.info("Unable to format {} as a number ", value, e);
			formattedValue = value;
		}

		return formattedValue;
	}

	/**
	 * Formats a decimal number string with a certain number of decimals. It does not round. The number is formatted using the
	 * resource manager's number formatter. Also see formatDecimalWithoutTrailingZeros and formatDecimalWithTrailingZeros.
	 *
	 * @param value
	 *            String - the number string to format
	 * @param numDecimals
	 *            int - the number of decimals to display
	 * @param trailingZeros
	 *            boolean - flag indicating whether trailing zeros should be inserted if the number of decimals is less than the
	 *            number of decimal digits in the value passed in
	 * @return String - the formatted number
	 */
	public String formatDecimal(String value, int numDecimals, boolean trailingZeros) {
		if (!trailingZeros) {
			return TPCurrencyUtility.getDisplayAmount(value, "", resMgr.getResourceLocale());
		} else {
			return TPCurrencyUtility.getDecimalDisplayAmount(value, numDecimals, resMgr.getResourceLocale());
		}
	}

	/**
	 * Formats a date string (in ISO format, i.e., from the database). The format is based on the given format type (either SHORT or
	 * LONG from DateFormat). The locale for formatting comes from the resource manager.
	 *
	 * @return java.lang.String - the formatted date
	 * @param date
	 *            java.lang.String - the ISO date string to format
	 * @param formatType
	 *            int - one of the DateFormat format types
	 */
	public String formatDate(String date, int formatType) {
		return TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, this.resMgr.getResourceLocale());
	}

	/**
	 * Formats a refdata code by translating it according to the given refdata table type and locale.
	 * <p>
	 * If the value cannot be formatted (e.g., invalid table type), the code is returned and an error printed to the console
	 * <p>
	 * 
	 * @return java.lang.String - the translated refdata code
	 * @param value
	 *            java.lang.String - the refdata code to translate
	 * @param tableType
	 *            java.lang.String - the refdata table type to use
	 * @param locale
	 *            java.lang.String - the locale to use
	 */
	public String formatRefData(String value, String tableType, String locale) {
		ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();
		String formattedValue = "";

		// return empty string if value is blank
		if (StringFunction.isBlank(value))
			return formattedValue;

		try {
			formattedValue = rdm.getDescr(tableType, value, locale);
			// When Description is not fetched let the code be used instead of description
			if (formattedValue == null || (formattedValue != null && formattedValue.equals(""))) {
				formattedValue = value;
			}

		} catch (AmsException e) {
			LOG.info("Error looking up refdata code: {} " , value, e);
			formattedValue = value;
		}
		return formattedValue;
	}

	/**
	 * Converts a boolean string (Y or N) into displayed text (based on resource bundle values for yes and no).
	 *
	 * @return java.lang.String - translated text
	 * @param value
	 *            java.lang.String - boolean value
	 * @param suppressNo
	 *            java.lang.String - suppress No indicator
	 */
	public String formatBoolean(String value, String suppressNo) {

		String displayValue;

		// todo - put this in refdata rather than resource bundle
		if (TradePortalConstants.INDICATOR_YES.equals(value)) {
			displayValue = this.getResourceManager().getText("listview.Yes", TradePortalConstants.TEXT_BUNDLE);
		} else {
			if (TradePortalConstants.INDICATOR_YES.equals(suppressNo)) {
				displayValue = "";
			} else {
				displayValue = this.getResourceManager().getText("listview.No", TradePortalConstants.TEXT_BUNDLE);
			}
		}

		return StringFunction.asciiToUnicode(displayValue); // T36000019054 Rel 8.2 08/07/2013 - Added
	}

	/**
	 * Formats a GMT date. Converts from the GMT date to the user's date by using the current user's timezone and locale.
	 *
	 * @return java.lang.String - the formatted date value
	 * @param value
	 *            java.lang.String - the date to format in ISO form
	 * @param timeZone
	 *            - the current user's timezone
	 * @param userLocale
	 *            - the current user's locale
	 */
	// Jyoti added 04-09-2012 IR5161
	public String formatGMTDates(String value, String timeZone, String userLocale) {

		// format without converting to timezone
		return TPDateTimeUtility.formatDate(value, TPDateTimeUtility.SHORT, userLocale);

	}

	/**
	 * Formats a GMT date. Converts from the GMT date to the user's date by using the current user's timezone and locale.
	 *
	 * @return java.lang.String - the formatted date value
	 * @param value
	 *            java.lang.String - the date to format in ISO form
	 * @param timeZone
	 *            - the current user's timezone
	 * @param userLocale
	 *            - the current user's locale
	 */

	public String formatGMTDate(String value, String timeZone, String userLocale) {
		if (timeZone.equals("")) {
			// format without converting to timezone
			return TPDateTimeUtility.formatDate(value, TPDateTimeUtility.SHORT, userLocale);
		}
		if (value.length() < 13) {
			return TPDateTimeUtility.formatDate(value, TPDateTimeUtility.SHORT, userLocale);
		}
		return TPDateTimeUtility.convertGMTDateForTimezoneAndFormat(value, TPDateTimeUtility.SHORT, userLocale, timeZone);
	}

	/**
	 * Formats a GMT date time value. Converts from the GMT date and time to the user's date by using the current user's timezone
	 * and locale.
	 *
	 * @return java.lang.String - the formatted date time value
	 * @param value
	 *            java.lang.String - the date and time to format in ISO form
	 * @param timeZone
	 *            - the current user's timezone
	 * @param userLocale
	 *            - the current user's locale
	 */

	public String formatGMTDateTime(String value, String timeZone, String userLocale) {

		if (timeZone.equals("")) {
			// format without converting to timezone
			return TPDateTimeUtility.formatDateTime(value, TPDateTimeUtility.SHORT, userLocale);
		}

		return TPDateTimeUtility.convertGMTDateTimeForTimezoneAndFormat(value, TPDateTimeUtility.SHORT, userLocale, timeZone);
	}

	/**
	 * Formats a date time value.
	 *
	 * @return java.lang.String - the formatted date time value
	 * @param value
	 *            java.lang.String - the date and time to format in ISO form
	 * @param userLocale
	 *            - the current user's locale
	 */

	public String formatDateTime(String value, String userLocale) {

		return TPDateTimeUtility.formatDateTime(value, TPDateTimeUtility.SHORT, userLocale);
	}

	/**
	 * Formats a given currency string value. In order to format the value, several other pieces of data are required. First the
	 * currency code is retrieved from the result set using the given row and currencyColumn attribute. Lastly the locale (from the
	 * resource manager) is used to format the number portion. If errors are found (like bad currency or bad currency column), the
	 * unformatted value is returned.
	 *
	 * @param currencyColumn
	 * @param prefixCurrencyCode
	 * @return java.lang.String - the formatted currency
	 * @param value
	 *            java.lang.String - the currency string to format
	 * @throws java.lang.Exception
	 */

	public String formatCurrency(String value, String currencyColumn, boolean prefixCurrencyCode, String userLocale) // these last 3 are to get other column data, there must be a better way...
			throws Exception {

		// Retrieve the currency code associated with the current amount
		String currencyCode = (String) getDataValue(currencyColumn);

		// If the currency code was not found (likely due to a bad column or xml tag name),
		// simply return the value; otherwise errors will occur during formatting
		if (value == null || value.equals("")) {
			return "";
		}

		// Format the currency amount based on the currency code associated with it and the user's locale
		String formattedValue = TPCurrencyUtility.getDisplayAmount(value, currencyCode, userLocale);

		if (prefixCurrencyCode) {
			formattedValue = currencyCode + ' ' + formattedValue;
		}

		return formattedValue;
	}

	/**
	 * Returns an HTML HREF tag by constructing a link using the formatted value of the listview column, the link action associated
	 * with the column, and the parameters to pass along in the link. By default, all link parameters will be passed encrypted in
	 * the URL
	 *
	 * @param column
	 * @return java.lang.String - the formatted HREF tag
	 * @throws java.lang.Exception
	 */
	public String buildColumnLinkUrl(DataViewManager.DataViewColumn column) // these last 3 are to get other column data, there must be a better way...
			throws Exception {
		return buildColumnLinkUrl(column, true);
	}

	/**
	 * Returns an HTML HREF tag by constructing a link using the formatted value of the listview column, the link action associated
	 * with the column, and the parameters to pass along in the link. By default, all link parameters will be passed encrypted in
	 * the URL
	 *
	 * @param column
	 * @param encrypt
	 * @return java.lang.String - the formatted HREF tag
	 * @throws java.lang.Exception
	 */
	public String buildColumnLinkUrl(DataViewManager.DataViewColumn column, Boolean encrypt) // these last 3 are to get other column
																								// data
			// there must be a better way...
			throws Exception {

		String linkUrl = "";
		String parameterValue = null;
		StringBuilder urlParameters = new StringBuilder();
		int numberOfParameters = column.linkColumnNames.length;

		if (column.linkAction.startsWith("javascript:")) {
			linkUrl = column.linkAction;
		} else {
			for (int i = 0; i < numberOfParameters; i++) {
				String parameterColumnName = column.linkColumnNames[i];
				String secureParameterName = column.linkSecureParmNames[i];

				// get other column value
				parameterValue = (String) getDataValue(parameterColumnName);

				// Encrypt the value so that it can be placed into the URL
				String urlParameterValue = parameterValue;
				if (encrypt) {
					urlParameterValue = EncryptDecrypt.encryptStringUsingTripleDes(parameterValue, getUserSession().getSecretKey());
				}

				// Create name=value pair for the url
				urlParameters.append('&');
				urlParameters.append(secureParameterName);
				urlParameters.append('=');
				urlParameters.append(urlParameterValue);
			}

			String additionalUrlParm = getAdditionalURLParm();
			if (StringFunction.isNotBlank(additionalUrlParm)) {
				urlParameters.append(additionalUrlParm);
			}
			if (!StringFunction.isBlank(parameterValue)) {
				linkUrl = formMgr.getLinkAsUrl(getLinkAction(column), urlParameters.toString(), this.getHttpResponse());
			}
		}

		return linkUrl;
	}

	/**
	 * @return
	 */
	protected String getAdditionalURLParm() {
		return null;
	}

	/**
	 * @param column
	 * @return
	 * @throws com.amsinc.ecsg.frame.AmsException
	 */
	protected String getLinkAction(DataViewManager.DataViewColumn column) throws AmsException {

		return column.linkAction;
	}

	/**
	 * Generate an xml error document for list of errors.
	 * 
	 * @return
	 */
	@Override
	public DocumentHandler getErrorDoc() {

		return errMgr.getErrorDoc();
	}

	// MEerupula Rel 8.4 IR-23915 STARTS Since the confidentail indicator is not user control field, this field is not supposed to be passed in
	// as parameter from browser to the dataviews...instead this field is set in user's session on login and use it from the session.
	// This method is used to retrieve the confidentail indicator of the logged in user from the session.
	public String getConfidentialInd() {
		String confInd;

		if (getUserSession().getSavedUserSession() == null) {
			confInd = getUserSession().getConfInd();
		} else {
			if (TradePortalConstants.ADMIN.equals(getUserSession().getSavedUserSession().getSecurityType())) {
				confInd = TradePortalConstants.INDICATOR_YES;
			} else {
				confInd = getUserSession().getSubsidConfInd();
			}
		}
		if (StringFunction.isBlank(confInd)) {
			confInd = TradePortalConstants.INDICATOR_NO;
		}

		return confInd;

	}

	// CR914- Support link to generated PDF

	public String buildRowKeyValueForGeneratedDoc(DataViewManager.DataViewMetadata metadata) throws Exception {

		StringBuilder rowKeyValueSB = new StringBuilder();

		for (int i = 0; i < metadata.rowKeyValues.length; i++) {

			String otherColumnKey = metadata.rowKeyValues[i];
			String otherColumnValue = (String) getDataValue(otherColumnKey);

			rowKeyValueSB.append(otherColumnValue);

			// the delimiter between row key values
			if (i < metadata.rowKeyValues.length - 1) {
				rowKeyValueSB.append('/');
			}
		}

		// encrypt it
		return EncryptDecrypt.encryptStringUsingTripleDes(rowKeyValueSB.toString());

	}

}
