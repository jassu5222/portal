/**
 * Account Data View shows all of the user's accounts.
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import java.util.Map;

/**

 *
 */
public class AccountDataView extends AbstractAccountDataView {
private static final Logger LOG = LoggerFactory.getLogger(AccountDataView.class);

    //store the value which will be the same for the user
    boolean allowPayByAnotherAcctInitialized = false;
    String allowPayByAnotherAcct = null;

    /**
     * Constructor
     */
    public AccountDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters)
            throws DataViewException {
        StringBuilder sql = new StringBuilder();

        //cquinton 12/20/2012 - get user/org info from session rather than
        // passed in vars for security and to encapsulate for common use on home page
        String allowPayAcc = getAllowPayByAnotherAcct();

        sql.append(" and b.p_user_oid = ?");

        if (allowPayAcc != null) {
            if (TradePortalConstants.INDICATOR_NO.equals(allowPayAcc)) {
                sql.append("and a.othercorp_customer_indicator != ?");
            }
        }
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException, DataViewException {

        //cquinton 12/20/2012 - get user/org info from session rather than
        // passed in vars for security and to encapsulate for common use on home page
        String allowPayAcc = getAllowPayByAnotherAcct();
        String userOid = this.getUserOid();

        int varCount = 0;
        statement.setString(startVarIdx + varCount, userOid);
        varCount++;

        if (allowPayAcc != null) {
            if (TradePortalConstants.INDICATOR_NO.equals(allowPayAcc)) {
                statement.setString(startVarIdx + varCount, TradePortalConstants.INDICATOR_YES);
                varCount++;
            }
        }

        return varCount;
    }

    private String getAllowPayByAnotherAcct()
            throws DataViewException {
        if (!allowPayByAnotherAcctInitialized) {
            try {
                String clientBankOid = this.getClientBankOid();

                DocumentHandler result = DatabaseQueryBean.getXmlResultSet(
                        "select allow_pay_by_another_accnt from client_bank where organization_oid = ? ", false, new Object[]{clientBankOid});

                if (result != null) {
                    this.allowPayByAnotherAcct = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
                }
                allowPayByAnotherAcctInitialized = true;
            } catch (AmsException ex) {
                throw new DataViewException("Problem getting allowPayByAnotherAcct from client bank", ex);
            }
        }

        return this.allowPayByAnotherAcct;
    }
}
