/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import java.util.Map;

/**
 *
 *
 */
public class AddInvoicesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AddInvoicesDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public AddInvoicesDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String sellerName = parameters.get("sellerName");
        String invID = parameters.get("InvID");
        String currency = parameters.get("Currency");
        String invCurrency = parameters.get("InvCurrency");//IR T36000016868
        String beneName = parameters.get("beneName");
        String uploadInvDefOid = parameters.get("uploadDefinitionOid");
        String hasInvoices = parameters.get("hasInvoices");
        String amountFrom = parameters.get("AmountFrom");
        String amountTo = parameters.get("AmountTo");
        String fromDate = parameters.get("DateFrom");
        String toDate = parameters.get("DateTo");
        String validDate = parameters.get("validDate");
        String transactionTypeCode = parameters.get("transactionTypeCode");
        String tranOid = parameters.get("tranOid");
        String instrOid = parameters.get("instrOid");

        String datePattern = null;
        try {
            if (null != parameters.get("dPattern")) {
                datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
            } else {
                datePattern = "";
            }
        } catch (UnsupportedEncodingException e) {
            LOG.debug("Exceptiom in DateFormat Decoding : " + e);
        }

        dynamicWhereClause.append(" where deleted_ind !='Y'");

        dynamicWhereClause.append(" and a_corp_org_oid =  ? ");
        sqlParams.add(userOrgOid);
        dynamicWhereClause.append(" and (linked_to_instrument_type = '");
        dynamicWhereClause.append(InstrumentType.APPROVAL_TO_PAY + "')");
        dynamicWhereClause.append(" and invoice_status  in ('");
        dynamicWhereClause.append(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT + "','");
        dynamicWhereClause.append(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED + "','");
        dynamicWhereClause.append(TradePortalConstants.PO_STATUS_CANCELLED + "')");
        if (TransactionType.ISSUE.equals(transactionTypeCode)) {
            dynamicWhereClause.append("and invoice_type = '");
            dynamicWhereClause.append(TradePortalConstants.INITIAL_PO).append("' ");
        } //IR T36000016951 start - for amendment activity make sure RPL invoices matches with the invoices in the ISS activity.
        else if (TransactionType.AMEND.equals(transactionTypeCode)) {
            dynamicWhereClause.append("and (invoice_id in (select invoice_id from invoices_summary_data where a_instrument_oid = ?)");
            dynamicWhereClause.append("or invoice_type = '");
            dynamicWhereClause.append(TradePortalConstants.INITIAL_PO).append("') ");
            sqlParams.add(instrOid);
        }
      //IR T36000016951 end

     // IR T36000013870 Start - check if Issue activity already has an invoice if so get the invoice definition oid 
        String whereClause = " a_instrument_oid = (select p_instrument_oid from transaction where transaction_oid= ?)";
        String invDefinitionOid = getObjectID("invoices_summary_data", "A_UPLOAD_DEFINITION_OID", whereClause, tranOid);
        // get invoice def oid

        if (StringFunction.isNotBlank(invDefinitionOid) && !Boolean.valueOf(hasInvoices)) {
            dynamicWhereClause.append(" and a_upload_definition_oid = ? ");
            sqlParams.add(invDefinitionOid);
        }
		// IR T36000013870 End

        if (Boolean.valueOf(hasInvoices)) {
            dynamicWhereClause.append(" and a_upload_definition_oid = ?");
            dynamicWhereClause.append(" and currency = ?");
            dynamicWhereClause.append(" and seller_name = ?");
            dynamicWhereClause.append(" and a_transaction_oid is null");

            sqlParams.add(uploadInvDefOid);
            sqlParams.add(currency);
            sqlParams.add(sellerName);

            //SHR IR T36000005856 Start
            if (!StringFunction.isBlank(validDate)) {

                dynamicWhereClause.append(" and (due_date = NVL(?,'0') ");
                dynamicWhereClause.append(" or payment_date = NVL(?,'0'))");
                sqlParams.add(validDate);
                sqlParams.add(validDate);
            }
    		//SHR IR T36000005856 End

        } else {
            dynamicWhereClause.append(" and a_transaction_oid is null");
            if (StringFunction.isNotBlank(currency) && "null" != currency) {
                dynamicWhereClause.append(" and currency = ? ");
                sqlParams.add(currency);
            }
        }

        if (StringFunction.isNotBlank(invID) && "null" != invID) {
            dynamicWhereClause.append(" and upper(invoice_id) like ? ");
            sqlParams.add("%" + invID + "%");
        }
        //IR T36000016868 - currency used in search functionality
        if (StringFunction.isNotBlank(invCurrency) && "null" != invCurrency) {
            dynamicWhereClause.append(" and upper(currency) = ? ");
            sqlParams.add(invCurrency);
        }
        if (StringFunction.isNotBlank(beneName) && "null" != beneName) {
            dynamicWhereClause.append(" and upper(seller_name) like ? ");
            sqlParams.add("%" + beneName.toUpperCase() + "%");
        }

        if (StringFunction.isNotBlank(amountFrom) && StringFunction.isNotBlank(amountTo)) {
            dynamicWhereClause.append(" and amount between ? ");
            dynamicWhereClause.append(" and ? ");
            sqlParams.add(amountFrom);
            sqlParams.add(amountTo);
        }
        if (StringFunction.isNotBlank(fromDate)) {
            if (StringFunction.isNotBlank(toDate)) {
                dynamicWhereClause.append(" and ((");
                dynamicWhereClause.append(" due_date" + " >= TO_DATE(?,'" + datePattern + "')");

                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append("  due_date" + " <= TO_DATE(?,'" + datePattern + "'))");
                dynamicWhereClause.append(" or (");
                dynamicWhereClause.append(" payment_date" + " >= TO_DATE(?,'" + datePattern + "')");

                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(" payment_date" + " <= TO_DATE(?,'" + datePattern + "')))");

                sqlParams.add(fromDate);
                sqlParams.add(toDate);

                sqlParams.add(fromDate);
                sqlParams.add(toDate);

            } else {
                dynamicWhereClause.append(" and (");
                dynamicWhereClause.append(" due_date" + " >= TO_DATE(?,'" + datePattern + "')");
                dynamicWhereClause.append(" or ");
                dynamicWhereClause.append(" payment_date" + " >= TO_DATE(?,'" + datePattern + "')");
                sqlParams.add(fromDate);
                sqlParams.add(fromDate);

            }
        } else if (StringFunction.isNotBlank(toDate)) {
            dynamicWhereClause.append(" and (");
            dynamicWhereClause.append("due_date" + " <= TO_DATE(?,'" + datePattern + "'))");
            dynamicWhereClause.append("or ");
            dynamicWhereClause.append("payment_date" + " <= TO_DATE(?,'" + datePattern + "')))");
            sqlParams.add(toDate);
            sqlParams.add(toDate);

        }

        dynamicWhereClause.append(" group by invoice_id,seller_name, currency,amount,issue_date,creation_date_time,due_date,payment_date,upload_invoice_oid");

        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

    private static String getObjectID(String tableName, String objectIdField, String whereClause, String tranOid) {
        String queryForOID = "SELECT distinct " + objectIdField + " FROM " + tableName + " WHERE " + whereClause;
        String objectID = null;
        try {

            DocumentHandler resultsDocXML = DatabaseQueryBean.getXmlResultSet(queryForOID, false, new Object[]{tranOid});
            if (resultsDocXML != null) {
                List<DocumentHandler> list = resultsDocXML.getFragmentsList("/ResultSetRecord");

                if (list.size() > 1) {
                    objectID = resultsDocXML.getAttribute("/ResultSetRecord[0]/" + objectIdField);
                } else if (list.size() == 1) {
                    objectID = resultsDocXML.getAttribute("/ResultSetRecord/" + objectIdField);
                }
            }
        } catch (AmsException e) {
            e.printStackTrace();
        }
        return objectID;
    }
}
