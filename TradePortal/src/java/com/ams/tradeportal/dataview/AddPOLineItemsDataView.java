/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class AddPOLineItemsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AddPOLineItemsDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public AddPOLineItemsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String uploadDefinitionOid = parameters.get("uploadDefinitionOid");
        String currency = parameters.get("currency");
        String beneficiaryName = parameters.get("beneficiaryName");
        String beneName = parameters.get("beneName");
        String sourceType = parameters.get("sourceType");
        String poNumber = parameters.get("poNumber");
        String itemNumber = parameters.get("itemNumber");

        boolean hasPOLineItems = Boolean.parseBoolean(parameters.get("hasPOLineItems"));

        //parameter check start
        if (userOrgOid == null) {
            userOrgOid = "";
        }
        if (uploadDefinitionOid == null) {
            uploadDefinitionOid = "";
        }
        if (currency == null) {
            currency = "";
        }
        if (beneficiaryName == null) {
            beneficiaryName = "";
        }
        if (beneName == null) {
            beneName = "";
        }
        if (sourceType == null) {
            sourceType = "";
        }
        if (poNumber == null) {
            poNumber = "";
        }
        if (itemNumber == null) {
            itemNumber = "";
        }

        sql.append("where a_owner_org_oid = ? ");
        sqlParams.add(userOrgOid);

        if (!"".equals(sourceType)) {
            sql.append(" and source_type = ? ");
            sqlParams.add(sourceType);
        }

        if (hasPOLineItems) {
            if (!"".equals(uploadDefinitionOid)) {
                sql.append(" and a_source_upload_definition_oid = ? ");
                sqlParams.add(uploadDefinitionOid);
            }

            if (!"".equals(currency)) {
                sql.append(" and currency = ? ");
                sqlParams.add(currency);
            }

            if (!"".equals(beneficiaryName)) {
                // PMitnala IR#T36000020360 Rel 8.3- Search results not obtained when beneficiary name contains apostrophe. Hence removing SQLParamFilter as it is already applied in Line53. 
                sql.append(" and ben_name = ? ");
                sqlParams.add(beneficiaryName);
            }

            sql.append(" and a_assigned_to_trans_oid is null ");

        } else {

            if (StringFunction.isNotBlank(beneficiaryName)) {
                sql.append(" and upper(ben_name) like upper(?)");
                sqlParams.add(beneficiaryName);
            }
            sql.append(" and a_assigned_to_trans_oid is null");

        	// If no uploaded po line items are assigned to this shipment check if a currency
            // was passed in.  The currency passed in would be the transaction's currency if it
            // exists or would be the currency of manual po line items assigned to other
            // shipments of this transaction
            //currency = xmlDoc.getAttribute("/In/SearchForPO/currency");
            if (!StringFunction.isBlank(currency)) {
                sql.append(" and currency = ? ");
                sqlParams.add(currency);
            }
        }

        // Search by the PO Number
        if (!poNumber.equals("") && itemNumber.equals("") && beneName.equals("")) {
            sql.append(" and upper(po_num) like upper(?)");
            sqlParams.add(poNumber + "%");
        }

        // Search by the PO Number and the Line Item Number
        if ((!poNumber.equals("")) && !itemNumber.equals("") && beneName.equals("")) {
            sql.append(" and upper(po_num) like upper(?) and upper(item_num) like upper(?)");
            sqlParams.add(poNumber + "%");
            sqlParams.add(itemNumber + "%");
        }

        // Search by the PO Number and the Beneficiary Name
        if (!poNumber.equals("") && itemNumber.equals("") && !beneName.equals("")) {
            sql.append(" and upper(po_num) like upper(?) and upper(ben_name) like upper(?)");
            sqlParams.add(poNumber + "%");
            sqlParams.add(beneName + "%");
        }

        // Search by the PO Number and the Line Item Number and the Beneficiary Name
        if (!poNumber.equals("") && !itemNumber.equals("") && !beneName.equals("")) {
            sql.append(" and upper(po_num) like upper(?) and upper(item_num) like upper(?) and upper(ben_name) like upper(?)");
            sqlParams.add(poNumber + "%");
            sqlParams.add(itemNumber + "%");
            sqlParams.add(beneName + "%");
        }

        // Search by the Beneficiary Name
        if (poNumber.equals("") && itemNumber.equals("") && !beneName.equals("")) {
            sql.append(" and upper(ben_name) like upper(?)");
            sqlParams.add(beneName + "%");
        }

        // Search by the Line Item Number
        if ((poNumber.equals("")) && !itemNumber.equals("") && beneName.equals("")) {

            sql.append(" and upper(item_num) like upper(?)");
            sqlParams.add(itemNumber + "%");
        }
        // Search by the Line Item Number and the Beneficiary Name
        if (poNumber.equals("") && !itemNumber.equals("") && !beneName.equals("")) {
            sql.append(" and upper(item_num) like upper(?) and upper(ben_name) like upper(?)");
            sqlParams.add(itemNumber + "%");
            sqlParams.add(beneName + "%");
        }

        // Build the where clause - All fields are empty
        if (poNumber.equals("") && itemNumber.equals("") && beneName.equals("")) {
            sql.append("");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
