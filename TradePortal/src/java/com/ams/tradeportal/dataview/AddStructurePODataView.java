/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.util.StringFunction;
import java.util.Map;

/**
 *
 *
 */
public class AddStructurePODataView
        extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AddStructurePODataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public AddStructurePODataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String transactionTypeCode = parameters.get("transactionTypeCode");
        String beneficiaryName = parameters.get("beneficiaryName");
        String poNumber = parameters.get("poNumber");
        String currency = parameters.get("currency");
        String beneName = parameters.get("beneName");
        String uploadDefinitionOid = parameters.get("uploadDefinitionOid");
        String hasStructuredPOs = parameters.get("hasStructuredPOs");

        sql.append(" where deleted_ind != ?");
        sqlParams.add(TradePortalConstants.INDICATOR_YES);

        if (TransactionType.ISSUE.equals(transactionTypeCode)) {
            sql.append(" and purchase_order_type = ? ");
            sqlParams.add(TradePortalConstants.INITIAL_PO);
        }
        sql.append(" and a_owner_org_oid = ? ");
        sqlParams.add(userOrgOid);

        if ("true".equals(hasStructuredPOs)) {
            if (StringFunction.isNotBlank(uploadDefinitionOid)) {
                sql.append(" and a_upload_definition_oid = ? ");
                sqlParams.add(uploadDefinitionOid);
            }
            if (StringFunction.isNotBlank(uploadDefinitionOid)) {
                sql.append(" and currency = ? ");
                sqlParams.add(currency);
            }
            sql.append("and a_transaction_oid is null");
        } else {
            sql.append(" and a_transaction_oid is null");
            if (!StringFunction.isBlank(currency)) {
                sql.append(" and currency = ? ");
                sqlParams.add(currency);
            }
        }
        if (StringFunction.isNotBlank(beneName)) {
            sql.append(" and upper(seller_name) like upper(?)");
            sqlParams.add(beneName);
        }
        if (StringFunction.isNotBlank(poNumber)) {
            sql.append(" and upper(purchase_order_num) like ? ");
            sqlParams.add(poNumber.toUpperCase() + "%");
        }

        if (StringFunction.isNotBlank(beneficiaryName)) {
            sql.append(" and upper(seller_name) like ? ");
            sqlParams.add(beneficiaryName.toUpperCase() + "%");
        }

        sql.append(" group by purchase_order_num, seller_name, currency,amount, creation_date_time, purchase_order_oid");

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
