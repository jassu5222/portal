/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class AdminSecurityProfileDataView
    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(AdminSecurityProfileDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public AdminSecurityProfileDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder where = new StringBuilder();
        
    	String bogID = getUserSession().getBogOid();
	    String clientBankID = getUserSession().getClientBankOid();
		String ownershipLevel = getUserSession().getOwnershipLevel();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();


        boolean preVal = false;
        where.append(" and p_owner_org_oid in (");
        if(StringFunction.isNotBlank(parentOrgID)){
        	where.append(" ? ");
        	sqlParams.add(parentOrgID);
        	preVal = true;
        }
        if(StringFunction.isNotBlank(globalID)){
        	if(preVal)
        		where.append(",");
        	where.append(" ? ");
        	sqlParams.add(globalID);
        	preVal = true;
        }

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
    	  if(StringFunction.isNotBlank(clientBankID)){
    		  if(preVal)
          		where.append(",");
    		  where.append("?");
    		  sqlParams.add(clientBankID);
    		  preVal = true;
          }
      }
      if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
    	  if(StringFunction.isNotBlank(clientBankID)){
    		  if(preVal)
            		where.append(",");
    		  where.append("?");
    		  sqlParams.add(clientBankID);
    		  preVal = true;
          }
    	  if(StringFunction.isNotBlank(bogID)){
    		  if(preVal)
          		where.append(",");
    		  where.append("?");
    		  sqlParams.add(bogID);
    		  preVal = true;
          }
      }

  	    where.append(")");
        return where.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
