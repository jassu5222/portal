/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class AdminUserDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(AdminUserDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public AdminUserDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String bogID = getUserSession().getBogOid();
        String clientBankID = getUserSession().getClientBankOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        String selectedBankOid = parameters.get("selectedBankOid");
        String filterTextFirst =parameters.get("filterTextFirst");
        String filterTextLast =parameters.get("filterTextLast");


        if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
           sql.append("and (a.ownership_level = '");
           sql.append(TradePortalConstants.OWNER_GLOBAL);
           sql.append("' or a.ownership_level = '");
           sql.append(TradePortalConstants.OWNER_BANK);
           sql.append("')");

        }
        else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
            sql.append(" and a.a_client_bank_oid = ? ");
           sql.append(" and (a.ownership_level = '");
           sql.append(TradePortalConstants.OWNER_BANK);
           sql.append("' or a.ownership_level = '");
           sql.append(TradePortalConstants.OWNER_BOG);
           sql.append("')");
           sqlParams.add(clientBankID);
        }
        else {
        	sql.append(" and a.p_owner_org_oid = ? and a.ownership_level = ?");
            sqlParams.add(bogID);
            sqlParams.add(ownershipLevel);
        }



        if (StringFunction.isNotBlank(selectedBankOid)&& selectedBankOid!=null) {
        	selectedBankOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedBankOid, getUserSession().getSecretKey());
   			sql.append(" and a.p_owner_org_oid = ? ");
   			sqlParams.add(selectedBankOid);
   		}

   		if (StringFunction.isNotBlank(filterTextFirst)&& filterTextFirst!=null) {
   			sql.append(" and upper(a.first_name) like ?");
   			sqlParams.add("%"+ filterTextFirst.toUpperCase() + "%");
   		}
   		if (StringFunction.isNotBlank(filterTextLast)&& filterTextLast!=null) {
   			sql.append(" and upper(a.last_name) like ?");
   			sqlParams.add("%" + filterTextLast.toUpperCase() + "%");
   		}

   		return sql.toString();
    }


    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);


    }
}
