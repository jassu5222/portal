/**
 *
 */
package com.ams.tradeportal.dataview;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class AllTransactionsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(AllTransactionsDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
    final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
    final static String ALL_INSTRUMENTS = "common.all";

	/**
	 * Constructor
	 */
	public AllTransactionsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String selectedWorkOrgOid = parameters.get("work");
        String unencryptedWorkOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkOrgOid, getUserSession().getSecretKey());
        
        if (ALL_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append("and b.a_corp_org_oid in (");
            sql.append(" select organization_oid");
            sql.append(" from corporate_org");
            sql.append(" where activation_status = ? start with organization_oid = ?"); //ownerOrgOid
            sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            sqlParams.add(TradePortalConstants.ACTIVE);
            sqlParams.add(this.getOwnerOrgOid());
            
        }
        else if (MY_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append("and a.a_assigned_to_user_oid = ?"); //userOrgOid
            sqlParams.add(this.getUserOid());
        }
        else {
            sql.append(" and b.a_corp_org_oid = ?");
            sqlParams.add(unencryptedWorkOrgOid);
        }
        

        //instrument groups
        //todo: this really should be ref data
        String selectedInstrGrp = parameters.get("grp");
        if (TradePortalConstants.INSTR_GROUP__TRADE.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
            sqlParams.add(InstrumentType.STANDBY_LC);
            sqlParams.add(InstrumentType.EXPORT_DLC);
            sqlParams.add(InstrumentType.EXPORT_COL);
            sqlParams.add(InstrumentType.EXPORT_DLC);
            sqlParams.add(InstrumentType.NEW_EXPORT_COL);
            sqlParams.add(InstrumentType.GUARANTEE);
            sqlParams.add(InstrumentType.AIR_WAYBILL);
            sqlParams.add(InstrumentType.SHIP_GUAR);
            sqlParams.add(InstrumentType.IMPORT_DLC);
            sqlParams.add(InstrumentType.INCOMING_SLC);
            sqlParams.add(InstrumentType.CLEAN_BA);
            sqlParams.add(InstrumentType.DOCUMENTARY_BA);
            sqlParams.add(InstrumentType.REFINANCE_BA);
            sqlParams.add(InstrumentType.INDEMNITY);
            sqlParams.add(InstrumentType.DEFERRED_PAY);
            sqlParams.add(InstrumentType.COLLECT_ACCEPT);
            
            sqlParams.add(InstrumentType.INCOMING_GUA);
            sqlParams.add(InstrumentType.IMPORT_COL);
            sqlParams.add(InstrumentType.LOAN_RQST);
            sqlParams.add(InstrumentType.REQUEST_ADVISE);
            sqlParams.add(InstrumentType.APPROVAL_TO_PAY);
        }
        else if (TradePortalConstants.INSTR_GROUP__PAYMENT.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,?,?) ");
            sqlParams.add(InstrumentType.FUNDS_XFER);
            sqlParams.add(InstrumentType.DOM_PMT);
            sqlParams.add(InstrumentType.XFER_BET_ACCTS);
        }
        else if (TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?) ");
            sqlParams.add(InstrumentType.DIRECT_DEBIT_INSTRUCTION);
        }
        else if (TradePortalConstants.INSTR_GROUP__RECEIVABLES.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,?) ");
            sqlParams.add(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
            sqlParams.add(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT);
        }
        else if (TradePortalConstants.INSTR_GROUP__PAYABLES.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?)");
            sqlParams.add(InstrumentType.PAYABLE_MANAGEMENT);
           
        }
      
        //instrument types
        //todo: if not given just do those instrument types the user can view
        String selectedInstrType = parameters.get("iType");
        if (AllTransactionsDataView.ALL_INSTRUMENTS.equals(selectedInstrType)) {
            //do nothing
        }
        else if (selectedInstrType!=null && selectedInstrType.length()>0 ) {
            sql.append(" and b.instrument_type_code = ?");
            sqlParams.add(selectedInstrType);
        }

        String selectedStatus = parameters.get("status");
        if (AllTransactionsDataView.ALL_INSTRUMENTS.equals(selectedStatus)) {
        	sql.append(" And a.transaction_status NOT IN (?)");
        	sqlParams.add("DELETED");
        }
        else if ( selectedStatus != null && selectedStatus.length()>0 ) {
            sql.append(" and a.transaction_status=?"); //selected status
            sqlParams.add(selectedStatus);
        }
        else {
            //throw exception!!! missing required parm
        }

 
        String confInd  = getConfidentialInd();
        
 	
        if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
            sql.append(" and ( b.confidential_indicator = ? OR ");
            sql.append(" b.confidential_indicator is null) ");
            sqlParams.add(TradePortalConstants.INDICATOR_NO);
        }
        
        //Added by Sandeep 12/06/2012 - Start 
        //As per the design change a new field Instrument Id is added, 
        //so appending another AND condition to filter data based on Instrument ID also.
        String instrumentId = parameters.get("instrumentId");
        if(instrumentId == null)instrumentId="";
       
        if(!"".equals(instrumentId)){
        	sql.append(" and b.complete_instrument_id like ? ");
        	sqlParams.add("%"+instrumentId.toUpperCase()+"%");
        }
        
        String bankInstrumentId = parameters.get("bankInstrumentId");
        if(bankInstrumentId == null)bankInstrumentId="";        
       
        if(!"".equals(bankInstrumentId)){
			sql.append(" and upper(b.bank_instrument_id) like ? ");
        	sqlParams.add("%"+bankInstrumentId.toUpperCase()+"%");
        }
      
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
    
    public String buildQuery(DataViewManager.DataViewMetadata metadata,
            String dynamicWhereClause ) {

    	StringBuilder builtSQL = new StringBuilder(super.buildQuery(metadata, dynamicWhereClause));
    	if (metadata.columns[sortColumn].columnKey.equalsIgnoreCase("Amount")){
    	String a = "decode(b.instrument_type_code/*not_column_name*/, 'BIL'/*not_column_name*/, null/*not_column_name*/, a.copy_of_amount/*not_column_name*/) ";
    	builtSQL.replace(builtSQL.indexOf(a),builtSQL.indexOf(a)+a.length()," a.copy_of_amount ");
    	a = a.replace("b.instrument_type_code", "InstrumentType");
    	a = a.replace("a.copy_of_amount", "Amount");
    	builtSQL.replace(builtSQL.lastIndexOf("Amount, Status"),builtSQL.lastIndexOf("Amount, Status")+6,a + " AS Amount " );
    	}

    	return builtSQL.toString();
    }
}
