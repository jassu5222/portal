/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

import java.util.Date;
import java.text.SimpleDateFormat;

/**

 *
 */
public class AuthorisedTransactionXMLDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(AuthorisedTransactionXMLDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public AuthorisedTransactionXMLDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String showDownloadedXMLInd = parameters.get("showDownloadedXMLInd");
        String customer = parameters.get("customer");
        String instrumentID = parameters.get("instrumentID");
        String bankInstrumentID = parameters.get("bankInstrumentID");
        String transactionID = parameters.get("transactionID");
        String transaction = parameters.get("transaction");
        String instrumentType = parameters.get("instrumentType");
        String dateFrom = parameters.get("dateFrom");
        String dateTo = parameters.get("dateTo");
        String timeFrom = " 00:00:00";
        String timeTo = " 23:59:59";
        
        String datePattern = null;
        try {
            if (null != parameters.get("datePattern")) {
                datePattern = java.net.URLDecoder.decode(parameters.get("datePattern"), "UTF-8");
            } else {
                datePattern = "";
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            LOG.debug("Exceptiom in DateFormat Decoding : " + e);
        }
        
        //if Client Bank
        if( TradePortalConstants.OWNER_BANK.equals(this.getOwnershipLevel()) ){
        	sql.append(" AND c.a_client_bank_oid = ?");
        	sqlParams.add(this.getClientBankOid());
        	//If any restricted bank org group, then eliminate them
        	if( StringFunction.isNotBlank(this.getUserSession().getBankGrpRestrictRuleOid()) ){
        		sql.append(" AND c.a_bank_org_group_oid NOT IN ");
        		sql.append("(select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ?)");
        		sqlParams.add(this.getUserSession().getBankGrpRestrictRuleOid());
        	} 
        	
        }else if( TradePortalConstants.OWNER_BOG.equals(this.getOwnershipLevel()) ){
        	sql.append(" AND c.a_bank_org_group_oid = ?");
        	sqlParams.add(this.getBogOid());
        }

        sql.append(" and b.instrument_type_code in (?,?,?,?,?,?,?) ");
        sqlParams.add(InstrumentType.STANDBY_LC);
        sqlParams.add(InstrumentType.GUARANTEE);
        sqlParams.add(InstrumentType.AIR_WAYBILL);
        sqlParams.add(InstrumentType.SHIP_GUAR);
        sqlParams.add(InstrumentType.IMPORT_DLC);
        sqlParams.add(InstrumentType.INCOMING_SLC);
        sqlParams.add(InstrumentType.LOAN_RQST);
        
        if(StringFunction.isNotBlank(showDownloadedXMLInd)){
        	sql.append(" and a.downloaded_xml_ind= ?");
        	sqlParams.add(showDownloadedXMLInd);
        }else{
        	sql.append(" and a.downloaded_xml_ind='");
        	sql.append(TradePortalConstants.INDICATOR_NO);
        	sql.append("'");
        }
        
        if(StringFunction.isNotBlank(customer)){
        	sql.append(" and c.organization_oid=?");
        	sqlParams.add(customer);
        }	
        
        if(StringFunction.isNotBlank(instrumentID)){
        	sql.append(" and b.complete_instrument_id like upper(?)");
        	sqlParams.add("%"+instrumentID+"%");
        }
        
        if(StringFunction.isNotBlank(bankInstrumentID)){
        	sql.append(" and b.bank_instrument_id like upper(?)");
        	sqlParams.add("%"+bankInstrumentID+"%");
        }
        
        if(StringFunction.isNotBlank(transactionID)){
        	sql.append(" and a.display_transaction_oid= ?");
        	//Check if the given transactionID is numeric.  If not return -1.
        	sqlParams.add(SQLParamFilter.filterNumber(transactionID));
        }
        
        if(StringFunction.isNotBlank(transaction)){
        	sql.append(" and a.transaction_type_code= ?");
        	sqlParams.add(transaction);
        }
        
        if(StringFunction.isNotBlank(instrumentType)){
        	sql.append(" and b.instrument_type_code= ?");
        	sqlParams.add(instrumentType);
        }
        
        if (StringFunction.isNotBlank(dateFrom)) {
        	boolean isValidDateFrom = isValidDateFormat(datePattern, dateFrom);
        	
        	if(isValidDateFrom){
        		sql.append(" and a.transaction_status_date >= TO_DATE(?,'" + datePattern+" HH24:MI:SS" + "')");
                sqlParams.add(dateFrom+timeFrom);
        	}else{
        		sql.append(" and 1 != 1 ");
        	}
        }

        if (StringFunction.isNotBlank(dateTo)) {
        	boolean isValidDateTo = isValidDateFormat(datePattern, dateTo);
        	
        	if(isValidDateTo){
        		sql.append(" and a.transaction_status_date <= TO_DATE(?,'" + datePattern+" HH24:MI:SS" + "')");
        		sqlParams.add(dateTo+timeTo);
        	}else{
        		sql.append(" and 1 != 1 ");
        	}
        }
        sql.append(" and exists (select 1 from outgoing_q_history ");
        sql.append("where A_TRANSACTION_OID=a.TRANSACTION_OID and MSG_TYPE='TPL') ");
        
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
