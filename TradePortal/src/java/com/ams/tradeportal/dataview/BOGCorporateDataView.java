/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class BOGCorporateDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BOGCorporateDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public BOGCorporateDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();
        /**
         * vName=CorporateDataView&selectedClientBankID=pEKhUlxxTQ0=&filterText=abcd&clientBankID=728526&bogID=null&isClientBankUser=true
         *
         */
        String bogID = getUserSession().getBogOid();
        String clientBankID = getUserSession().getClientBankOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        boolean isClientBankUser = ownershipLevel.equals(TradePortalConstants.OWNER_BANK);

        String filterText = parameters.get("filterText");

        if (clientBankID == null) {
            clientBankID = "";
        }
        if (bogID == null) {
            bogID = "";
        }

        if (filterText == null) {
            filterText = "";
        }

        if (isClientBankUser && !"".equals(clientBankID)) {
            sql.append(" and b.p_client_bank_oid = ? ");
            sqlParams.add(clientBankID);
        } else {
            if (!"".equals(bogID)) {
                sql.append(" and a_bank_org_group_oid = ? ");
                sqlParams.add(bogID);
            }
        }

        sql.append(" and a.activation_status = ?");
        sqlParams.add(TradePortalConstants.ACTIVE);

        if (!"".equals(filterText)) {
            sql.append(" and upper(a.name) like upper(?) ");
            sqlParams.add(filterText + "%");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
