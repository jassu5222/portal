/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class BankBranchRulesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRulesDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public BankBranchRulesDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();

        String paymentMethod = parameters.get("PaymentMethod");
        String bankBranchCode = parameters.get("BankBranchCode");
        String bankName = parameters.get("BankName");
        String branchName = parameters.get("BranchName");
        String city = parameters.get("City");
        String country = parameters.get("Country");

        if (StringFunction.isNotBlank(paymentMethod)) {
            if (!StringFunction.isBlank(paymentMethod)) {
                sql.append(" and n.payment_method = ?");
                sqlParams.add(paymentMethod);
            }

            if (!StringFunction.isBlank(bankBranchCode)) {
                sql.append(" and upper(n.bank_branch_code) like unistr(?)");
                sqlParams.add(StringFunction.toUnistr(bankBranchCode.toUpperCase()) + "%");
            }

            if (!StringFunction.isBlank(bankName)) {
                sql.append(" and upper(n.bank_name) like unistr(?)");
                sqlParams.add(StringFunction.toUnistr(bankName.toUpperCase()) + "%");
            }

            if (!StringFunction.isBlank(branchName)) {
                sql.append(" and upper(n.branch_name) like unistr(?)");
                sqlParams.add(StringFunction.toUnistr(branchName.toUpperCase()) + "%");
            }

            if (!StringFunction.isBlank(city)) {
                sql.append(" and upper(n.address_city) like unistr(?)");
                sqlParams.add(StringFunction.toUnistr(city.toUpperCase()) + "%");
            }
            
            if (!StringFunction.isBlank(country)) {
                sql.append(" and upper(n.address_country) like unistr(?)");
                sqlParams.add(StringFunction.toUnistr(country.toUpperCase()) + "%");
            }            
        } else {
            sql.append(" and n.payment_method = 'null'");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
