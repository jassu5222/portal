/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

/**
 *
 *
 */
public class BankGrpRestrnRulesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BankGrpRestrnRulesDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public BankGrpRestrnRulesDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        sqlParams.add(getUserSession().getClientBankOid());

        return "where P_CLIENT_BANK_OID in (?)";
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
