/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class BankSearchDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BankSearchDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public BankSearchDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();
        
            String bankType = parameters.get("bankType");
            String paymentMethodCode = parameters.get("paymentMethodCode");
            String accCountry = parameters.get("accCountry");
            String unicodeIndicator = parameters.get("unicodeIndicator");

            if (TermsPartyType.ORDERING_PARTY_BANK.equals(bankType)) {
                String comparInd = "<>";
                if (InstrumentType.DIRECT_DEBIT_INSTRUCTION
                        .equals(paymentMethodCode)) {
                    comparInd = "=";
                }

                sql.append(" where bank_branch_rule.PAYMENT_METHOD ").append(comparInd).append("?");
                sqlParams.add(InstrumentType.DIRECT_DEBIT_INSTRUCTION);

            } else {
                sql.append(" where bank_branch_rule.PAYMENT_METHOD = ? ");
                sqlParams.add(paymentMethodCode);
            }

            if (!TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethodCode)) {
                sql.append(" and bank_branch_rule.address_country = ? ");
                sqlParams.add(accCountry);
            }
            /* 	KMehta IR-T36000027080 Rel 9400 on 20-August-2015 Change - Begin */
           /* if (!TradePortalConstants.ORDERING_PARTY_BANK.equals(bankType)
                    && !TradePortalConstants.BENEFICIARY_BANK.equals(bankType)) {
                sql.append(" and (unicode_indicator = 'N' or unicode_indicator is null)");
            }*/
            if (TradePortalConstants.INDICATOR_NO.equals(unicodeIndicator) && unicodeIndicator != null) {
                sql.append(" and unicode_indicator = 'N'");
            }
            /* 	KMehta IR-T36000027080 Rel 9400 on 20-August-2015 Change - End */
            
            // 11/18/2014 - R91 IR T36000034694 - Fixed the issue by adding wild cards in below parameters
            if (parameters.get("BankName") != null) {
                sql.append(" AND upper(bank_branch_rule.bank_name) like  unistr (?) ");
                sqlParams.add("%" + StringFunction.toUnistr(parameters.get("BankName").toUpperCase()) + "%");
            }
            String bankCode = parameters.get("BankBranchCode");
            if (StringFunction.isNotBlank(bankCode)) {
                // 11/18/2014 - R91 IR T36000034694   
                if (!"null".equals(bankCode.trim())) {
                    sql.append(" AND upper(bank_branch_rule.bank_branch_code) like unistr (?)  ");
                    sqlParams.add("%" + StringFunction.toUnistr(parameters.get("BankBranchCode").toUpperCase()) + "%");
                }
            }
            if (parameters.get("BranchName") != null) {
                sql.append(" AND upper(bank_branch_rule.branch_name) like unistr (?)  ");
                sqlParams.add("%" + StringFunction.toUnistr(parameters.get("BranchName").toUpperCase()) + "%");
            }
            if (parameters.get("City") != null) {
                sql.append(" AND upper(bank_branch_rule.address_city) like unistr (?)  ");
                sqlParams.add("%" + StringFunction.toUnistr(parameters.get("City").toUpperCase()) + "%");
            }
            if (parameters.get("Province") != null) {
                sql.append(" AND upper(bank_branch_rule.address_state_province) like unistr (?) ");
                sqlParams.add(StringFunction.toUnistr(parameters.get("Province").toUpperCase()) + "%");
            }

       
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement,
            int startVarIdx, Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
