/**
 *
 */
package com.ams.tradeportal.dataview;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class BankTransactionHistoryLogDataView extends AbstractDBDataView {
	private static final Logger LOG = LoggerFactory.getLogger(BankTransactionHistoryLogDataView.class);

	/**
	 * Constructor
	 */
	public BankTransactionHistoryLogDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
		StringBuilder sql = new StringBuilder();
		String encryptedTranOid = parameters.get("origTranOid");
		if (encryptedTranOid != null && encryptedTranOid.length() > 0) {
			sql.append("and t.a_transaction_oid = ?");
		} else {
			// origTranOid is required
			sql.append(" and 1=0");
		}

		return sql.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, Map<String, String> parameters)
			throws SQLException {
		int varCount = 0;
		String encryptedTranOid = parameters.get("origTranOid");
		if (encryptedTranOid != null && encryptedTranOid.length() > 0) {
			String tranOid = EncryptDecrypt.decryptStringUsingTripleDes(encryptedTranOid, getUserSession().getSecretKey());
			LOG.debug("***tranOid: {}", tranOid);
			statement.setString(startVarIdx + varCount, tranOid);
			varCount++;
		}
		return varCount;
	}
}
