/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.ams.tradeportal.common.NumberValidator;

/**

 *
 */
public class BankTransactionUpdateDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(BankTransactionUpdateDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public BankTransactionUpdateDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();
        
        String loginLocale = getUserSession().getUserLocale();
        MediatorServices medService = new MediatorServices();
        
        String bankUpdateStatus = parameters.get("bankUpdateStatusType");
        String bankXMLDownloadInd = parameters.get("bankXMLDownloadInd");        
        String instrumentID = parameters.get("instrumentID");
        String customer = parameters.get("customer");        
        String bankInstrumentID = parameters.get("bankInstrumentID");
        String instrumentType = parameters.get("instrumentType");
        String transactionType = parameters.get("transactionType");        
        String currency = parameters.get("currency");       
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        
        //if Client Bank
        if( TradePortalConstants.OWNER_BANK.equals(this.getOwnershipLevel()) ){
        	sql.append(" AND co.a_client_bank_oid = ?");
        	sqlParams.add(this.getClientBankOid());
        	//If any restricted bank org group, then eliminate them
        	if( StringFunction.isNotBlank(this.getUserSession().getBankGrpRestrictRuleOid()) ){
        		sql.append(" AND co.a_bank_org_group_oid NOT IN ");
        		sql.append("(select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ?)");
        		sqlParams.add(this.getUserSession().getBankGrpRestrictRuleOid());
        	} 
        	
        }else if( TradePortalConstants.OWNER_BOG.equals(this.getOwnershipLevel()) ){
        	sql.append(" AND co.a_bank_org_group_oid = ?");
        	sqlParams.add(this.getBogOid());
        }      
        
        if(StringFunction.isNotBlank(bankUpdateStatus)){
        	if(TradePortalConstants.STATUS_ALL.equals(bankUpdateStatus)){
        		sql.append(" and btu.BANK_UPDATE_STATUS IN (?,?,?,?,?) ");        		
        		sqlParams.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPLIED);
        		sqlParams.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_NOT_STARTED);
        		sqlParams.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_IN_PROGRESS);
        		sqlParams.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_REPAIR);
        		sqlParams.add(TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPROVED);        		
        	}else{
        		sql.append(" and btu.BANK_UPDATE_STATUS = ? ");
        		sqlParams.add(bankUpdateStatus);
        	}
        }
        
        if(StringFunction.isNotBlank(bankXMLDownloadInd)){
        	if("true".equals(bankXMLDownloadInd)){
        		sql.append(" and a.DOWNLOADED_XML_IND='");
            	sql.append(TradePortalConstants.INDICATOR_YES);
            	sql.append("' ");
        	}else{
        		sql.append(" and a.DOWNLOADED_XML_IND='");
            	sql.append(TradePortalConstants.INDICATOR_NO);
            	sql.append("' ");
        	}        		
        }
        
        if(StringFunction.isNotBlank(instrumentID)){
        	sql.append(" and b.COMPLETE_INSTRUMENT_ID like upper(?) ");
        	sqlParams.add("%"+instrumentID+"%");
        }
        
        if(StringFunction.isNotBlank(customer)){
        	sql.append(" and co.ORGANIZATION_OID=? ");
        	sqlParams.add(customer);
        }
        
        if(StringFunction.isNotBlank(bankInstrumentID)){
        	sql.append(" and b.BANK_INSTRUMENT_ID like upper(?) ");
        	sqlParams.add("%"+bankInstrumentID+"%");
        }
        
        if(StringFunction.isNotBlank(instrumentType)){
        	sql.append(" and b.INSTRUMENT_TYPE_CODE= ? ");
        	sqlParams.add(instrumentType);
        }
        
        if(StringFunction.isNotBlank(transactionType)){
        	sql.append(" and a.TRANSACTION_TYPE_CODE= ? ");
        	sqlParams.add(transactionType);
        }else{
        	sql.append(" and a.TRANSACTION_TYPE_CODE IN (?,?,?) ");        		
    		sqlParams.add(TransactionType.ISSUE);
    		sqlParams.add(TransactionType.AMEND);
    		sqlParams.add(TransactionType.RELEASE);    
        }
        
        if(StringFunction.isNotBlank(currency)){
        	sql.append(" and a.COPY_OF_CURRENCY_CODE= ? ");
        	sqlParams.add(currency);
        }
        
        if (StringFunction.isNotBlank(amountFrom)) {
            try {
                String amount = NumberValidator.getNonInternationalizedValue(amountFrom, loginLocale);
                if (!amount.equals("")) {
                	sql.append(" and (a.COPY_OF_AMOUNT >= ?) ");
           		 	sqlParams.add(amountFrom);
                }else{
                	sql.append(" and 1 != 1 ");
                }
            } catch (InvalidAttributeValueException e) {
                try {
                    medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CURRENCY_FORMAT, amountFrom);
                    sql.append(" and 1 != 1 ");
                } catch (AmsException ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (StringFunction.isNotBlank(amountTo)) {
            try {
                String amount = NumberValidator.getNonInternationalizedValue(amountTo, loginLocale);
                if (!amount.equals("")) {
                	sql.append(" and (a.COPY_OF_AMOUNT <= ?) ");
                    sqlParams.add(amountTo);
                }else{
                	sql.append(" and 1 != 1 ");
                }
            } catch (InvalidAttributeValueException e) {
                try {
                    medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CURRENCY_FORMAT, amountFrom);
                    sql.append(" and 1 != 1 ");
                } catch (AmsException ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        /*
        if (StringFunction.isNotBlank(amountFrom)) {
        	if (StringFunction.isNotBlank(amountTo)) {
        		sql.append(" and (a.COPY_OF_AMOUNT >= ? and a.COPY_OF_AMOUNT <= ?) ");
        		sqlParams.add(amountFrom);
        		sqlParams.add(amountTo);
        	}else{
        		 sql.append(" and (a.COPY_OF_AMOUNT >= ?) ");
        		 sqlParams.add(amountFrom);
        	}            
        }else if (StringFunction.isNotBlank(amountTo)) {
            sql.append(" and (a.COPY_OF_AMOUNT <= ?) ");
            sqlParams.add(amountTo);
        }*/
        
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
