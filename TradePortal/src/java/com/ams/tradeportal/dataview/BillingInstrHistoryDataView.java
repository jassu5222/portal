/**
 * CR 932
 */
package com.ams.tradeportal.dataview;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

/**
 *
 *
 */
public class BillingInstrHistoryDataView extends AbstractDBDataView {
    private static final Logger LOG = LoggerFactory.getLogger(BillingInstrHistoryDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    static final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

    /**
     * Constructor
     */
    private static final String INNER_SELECT = " select i.instrument_oid as rowKey,i.instrument_oid AS instrument_oid,o.transaction_oid as transaction_oid,i.complete_instrument_id as InstrumentId,"
            + "i.instrument_type_code as InstrumentType,i.instrument_type_code as InstrumentTypeCode, o.copy_of_currency_code as CurrencyCode,'0.00' as ChargeAmount,"
            + "i.instrument_status as Status,i.instrument_status as STATUSFORQVIEW,"
            + "p.name as Party,'true' as CHILDREN, i.copy_of_confirmation_ind as Confirmed,i.copy_of_ref_num as ApplicantsRefNo";

    private static final String OUTER_SELECT = " select rowKey,instrument_oid,transaction_oid,InstrumentId,InstrumentType,InstrumentTypeCode,CurrencyCode,ChargeAmount,Status,STATUSFORQVIEW,Party,CHILDREN,Confirmed,ApplicantsRefNo";

    private static final String INNER_SELECT_CHILD = " SELECT a.transaction_oid AS rowKey,DECODE(a.transaction_type_code ,'ARM',NULL,'CHM',NULL,a.transaction_oid ) AS transaction_oid,"
            + " b.instrument_oid AS instrument_oid,to_char(a.transaction_status_date, 'DD Mon YYYY') AS InstrumentId,a.transaction_type_code AS InstrumentType,"
            + "  (select   SETTLEMENT_CURR_CODE from fee f where  f.p_transaction_oid = a.transaction_oid and rownum=1) as CurrencyCode"
            + ","
            + "(select sum(settlement_curr_amount) from fee f where  f.p_transaction_oid = a.transaction_oid) as ChargeAmount, a.transaction_status AS Status,a.transaction_status AS STATUSFORQVIEW, NULL AS Party,'false' AS CHILDREN";

    private static final String OUTER_SELECT_CHILD = " SELECT rowKey,transaction_oid,instrument_oid,InstrumentId,InstrumentType, CurrencyCode, ChargeAmount,Status,STATUSFORQVIEW,Party,CHILDREN";

    private static final String INNER_FROM = " from instrument i,transaction o,transaction t,terms_party p, terms r";

    private static final String INNER_FROM_CHILD = " FROM transaction a,instrument b,transaction active_tran";

    private static final String INNER_WHERE = " where i.original_transaction_oid = o.transaction_oid "
            + "and i.active_transaction_oid = t.transaction_oid(+) and t.c_bank_release_terms_oid = r.TERMS_OID(+) and "
            + "r.C_THIRD_TERMS_PARTY_OID = p.terms_party_oid(+) and i.template_flag='N' and i.instrument_status != 'DEL' and i.instrument_type_code != 'REC'";

    public BillingInstrHistoryDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder whereClause = new StringBuilder();
        StringBuilder countClause = new StringBuilder();
        String parentId = SQLParamFilter.filterNumber(parameters.get("parentId"));

        if (StringFunction.isNotBlank(parentId)) {
            whereClause.append(buildDynamicSQLCriteriaChild(parameters));
            countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
        } else {
            whereClause.append(buildDynamicSQLCriteriaParent(parameters));
            countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
        }

        super.fullSQL = whereClause.toString();
        super.fullCountSQL = countClause.toString();
        
        return null;
    }

    protected String buildDynamicSQLCriteriaParentFilter(Map<String, String> parameters, boolean addToParamList) {

        StringBuilder sql = new StringBuilder();

        String userOrgOid = getUserSession().getOwnerOrgOid();
        String selectedOrg = parameters.get("selectedOrg");
        String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus"));

        BeanManager beanMgr = getBeanManager();
        String confInd = getConfidentialInd();

        CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
        corpOrg.getById(getUserSession().getOwnerOrgOid());

        String searchType = parameters.get("searchType");
        //Search parameters
        String instrumentId = parameters.get("instrumentId");
        String refNum = parameters.get("refNum");
        String otherParty = parameters.get("otherParty");
        String startToDate = parameters.get("startDateTo");
        String startFromDate = parameters.get("startDateFrom");
        String closeDateTo = parameters.get("closeDateTo");
        String closeDateFrom = parameters.get("closeDateFrom");
        String datePattern = null;
        
        try {
            if (null != parameters.get("dPattern")) {
                datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
            } else {
                datePattern = "";
            }
        } catch (UnsupportedEncodingException e) {
            LOG.debug("Exceptiom in DateFormat Decoding : " , e);
        }

        if (instrumentId == null) {
            instrumentId = "";
        }
        if (refNum == null) {
            refNum = "";
        }
        
        if (otherParty == null) {
            otherParty = "";
        }
        List<String> statuses = new ArrayList<>();

        if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus)) {
            statuses.add(TradePortalConstants.INSTR_STATUS_ACTIVE);
            statuses.add(TradePortalConstants.INSTR_STATUS_PENDING);
            statuses.add(TradePortalConstants.INSTR_STATUS_EXPIRED);
        } else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus)) {
            statuses.add(TradePortalConstants.INSTR_STATUS_CANCELLED);
            statuses.add(TradePortalConstants.INSTR_STATUS_CLOSED);
            statuses.add(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
        } else if (TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            statuses.add(TradePortalConstants.INSTR_STATUS_ACTIVE);
            statuses.add(TradePortalConstants.INSTR_STATUS_PENDING);
            statuses.add(TradePortalConstants.INSTR_STATUS_EXPIRED);
            statuses.add(TradePortalConstants.INSTR_STATUS_CANCELLED);
            statuses.add(TradePortalConstants.INSTR_STATUS_CLOSED);
            statuses.add(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
            statuses.add(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
        } else // default is ALL (actually not all since DELeted instruments never show up (handled by the SQL in the listview XML)
        {
            selectedStatus = "''";
        }

        if (selectedOrg != null && selectedOrg.length() > 0) {
            selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, getUserSession().getSecretKey());
        } else {
            //org is required, build some dummy sql to return nothing
            sql.append(" and 1=0");
        }

        if (ALL_ORGS.equals(selectedOrg)) {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("select organization_oid, name");
            sqlQuery.append(" from corporate_org");
            sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
            sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
            sqlQuery.append(" order by ");
            sqlQuery.append(getResourceManager().localizeOrderBy("name"));

            DocumentHandler hierarchyDoc = null;
            try {
                hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
            } catch (AmsException e) {
                LOG.error("Exception caught in InstrumentsInquiriesDataView.java while calling DatabaseQueryBean.getXmlResultSet(). This exception is cirrently not caught." , e);
            }

            StringBuilder orgList = new StringBuilder();
            List<DocumentHandler> orgListDoc = hierarchyDoc.getFragmentsList("/ResultSetRecord");
            int totalOrganizations = orgListDoc.size();

            DocumentHandler orgDoc;
            for (int i = 0; i < totalOrganizations; i++) {
                orgDoc = orgListDoc.get(i);
                orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
                if (i < (totalOrganizations - 1)) {
                    orgList.append(", ");
                }
            }
            // Build a comma separated list of the orgs
            sql.append(" and i.a_corp_org_oid in (" + prepareDynamicSqlStrForInClause(orgList.toString(), sqlParams, addToParamList) + ")");
        } else {
            sql.append(" and i.a_corp_org_oid = ? ");
            if (addToParamList) {
                sqlParams.add(selectedOrg);
            }
        }

        if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) || TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) || TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            StringBuilder statusStr = new StringBuilder("");
            int numStatuses = statuses.size();
            for (int i = 0; i < numStatuses; i++) {
                statusStr.append(statuses.get(i));

                if (i < numStatuses - 1) {
                    statusStr.append(", ");
                }
            }

            sql.append(" and i.instrument_status in (");
            sql.append(prepareDynamicSqlStrForInClause(statusStr.toString(), sqlParams, addToParamList));
            sql.append(") ");
        } else {
            sql.append(" and i.instrument_status in ('') ");
        }

        if (TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(confInd)) //DK IR T36000024537 Rel8.4 02/04/2014
        {
            sql.append(" AND (i.confidential_indicator  = 'N' OR i.confidential_indicator IS NULL)");
        }
        sql.append(" and i.instrument_type_code in ('BIL')");
       
        if (searchType.equals(TradePortalConstants.ADVANCED)) {
        	 if (startFromDate != null && !startFromDate.equals("")) {
                 sql.append(" and i.issue_date >= TO_DATE(?,'" + datePattern + "')");
                 if (addToParamList) {
                     sqlParams.add(startFromDate);
                 }
             }

             if (startToDate != null && !startToDate.equals("")) {
                 sql.append(" and i.issue_date <= TO_DATE(?,'" + datePattern + "')");
                 if (addToParamList) {
                     sqlParams.add(startToDate);
                 }
             }

            if (closeDateFrom != null && !closeDateFrom.equals("")) {
                sql.append(" and i.copy_of_expiry_date >= TO_DATE(?,'" + datePattern + "')");
                if (addToParamList) {
                    sqlParams.add(closeDateFrom);
                }
            }

            if (closeDateTo != null && !closeDateTo.equals("")) {
                sql.append(" and i.copy_of_expiry_date <= TO_DATE(?,'" + datePattern + "')");
                if (addToParamList) {
                    sqlParams.add(closeDateTo);
                }
            }
            
        } else {

            // Unlike the ADVANCED search, the SIMPLE search values are ANDed together.
            // To handle this we'll add an 'AND (simple conditions ored together)'
            // clause.  This is only added if at least one of the search fields has a
            // value.
            if (!instrumentId.equals("") || !refNum.equals("") ||  !otherParty.equals("")) {
                // At least one search field has a value.

                boolean addAnd = false;
                sql.append(" and (");

                if (!instrumentId.equals("")) {

                    sql.append("upper(i.complete_instrument_id) like ? "); // WebLogic 6.1 SP6 does not treat jsp:param as URLEncoded any more.
                    addAnd = true;
                    if (addToParamList) {
                        sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                    }
                }
                if (!refNum.equals("")) {
                    if (addAnd) {
                        sql.append(" and ");
                    }
                    sql.append("upper(i.copy_of_ref_num) like ? ");
                    if (addToParamList) {
                        sqlParams.add("%" + refNum.toUpperCase() + "%");
                    }
                    addAnd = true;
                }
                

                    if (!otherParty.equals("")) {
                    	 if (addAnd) {
                             sql.append(" and ");
                         }
                        sql.append("  upper(p.name) like upper(?)");
                        if (addToParamList) {
                            sqlParams.add("%" + otherParty.toUpperCase() + "%");
                        }
                    }

               
                sql.append(")");

            }  // end if-else (ADVANCED vs BASIC)
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

       private String buildDynamicSQLCriteriaParent(Map<String, String> parameters) {

        String sort = SQLParamFilter.filter(parameters.get("sort"));
        StringBuilder basicquery = new StringBuilder();

        basicquery.append(OUTER_SELECT).append(" from (");
        basicquery.append(OUTER_SELECT).append(" ,rownum row_num from ( ");

       
        if (sort != null && !sort.equals("")) {
            if (sort.contains("InstrumentType")) {

                basicquery.append(INNER_SELECT).append(", refdata.descr AS RefDataDescr");
                basicquery.append(INNER_FROM).append(",refdata");
                basicquery.append(INNER_WHERE);
                basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, true));

                basicquery.append(buildSortQuery(parameters));
                if (sort.startsWith("-")) {
                    basicquery.append(" desc ");
                } else {
                    basicquery.append(" asc ");
                }
            } else {
                basicquery.append(INNER_SELECT).append(INNER_FROM).append(INNER_WHERE);
                basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, true));
                if (sort.startsWith("-")) {
                    sort = sort.substring(1);
                    sort = "'" + SQLParamFilter.filter(sort) + "'";
                    basicquery.append(" order by ").append(sort).append(" desc ");
                } else {
                    basicquery.append(" order by ").append(sort).append(" asc ");
                }
            }
        }
      
        basicquery.append(" ) ) where row_num > ? ").append(" and row_num <= ? ");
        return basicquery.toString();
    }

    public String buildSortQuery(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();
        
        sql.append("and (i.instrument_type_code = refdata.code ");
        sql.append(" and refdata.table_type ='");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        // Try to join in by the user's locale
        // If description does not exist for that locale, use the null default locale data

        sql.append("' AND ((refdata.locale_name = '");
        sql.append(getResourceManager().getResourceLocale());
        sql.append("') OR");
        sql.append("  ((refdata.locale_name is null)) and"); //PCUTRONE-(IR-GNUG100258356)[18-SEP-07]-ADD PARENTHESIS IN "  ((refdata.locale_name is null) and"

        sql.append("   ((( select count(*)");
        sql.append("     from refdata rrr");
        sql.append("     where rrr.code=refdata.code and ");
        sql.append("           rrr.table_type = '");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        sql.append("' and ");
        sql.append("           rrr.locale_name = '");
        sql.append(getResourceManager().getResourceLocale());
        sql.append("') = 0) ");

        sql.append("or (select count(*)");
        sql.append("     from refdata rrr");
        sql.append("     where rrr.code=refdata.code and ");
        sql.append("           rrr.table_type = '");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        sql.append("' and ");
        sql.append("           rrr.locale_name is null");
        sql.append(") = 0))) ");

        sql.append(" order by ");
        sql.append(getResourceManager().localizeOrderBy("upper(RefDataDescr)"));
        return sql.toString();
    }

    private String buildDynamicSQLCriteriaParentCount(Map<String, String> parameters) {

        StringBuilder basicquery = new StringBuilder();
        basicquery.append("select count(*) from( ");
        basicquery.append(INNER_SELECT).append(" ,rownum as row_num").append(INNER_FROM).append(INNER_WHERE);
        basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, false));
        basicquery.append(" ) ");

        return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChild(Map<String, String> parameters) {

        String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
        String innerWhereChild = " WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND "
                + "a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND "
                + "a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE "
                + "a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ? )";

        sqlParams.add(parentId);
        sqlParams.add(parentId);

        StringBuilder basicquery = new StringBuilder();
        basicquery.append(OUTER_SELECT_CHILD).append(" from (");
        basicquery.append(INNER_SELECT_CHILD).append(" ,rownum as row_num").append(INNER_FROM_CHILD).append(innerWhereChild);
        basicquery.append(" ) where row_num > ? ").append(" and row_num <= ? ");

        return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChildCount(Map<String, String> parameters) {

        String innerWhereChild = " WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND "
                + "a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND "
                + "a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE "
                + "a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ? )";

        StringBuilder basicquery = new StringBuilder();
        basicquery.append("select count(*) from( ");
        basicquery.append(INNER_SELECT_CHILD).append(" ,rownum as row_num").append(INNER_FROM_CHILD).append(innerWhereChild);
        basicquery.append(" ) ");

        return basicquery.toString();
    }

    @Override
    protected boolean ignorePassedInParam(Map<String,String> parameters) {
        return StringFunction.isNotBlank( parameters.get("parentId"));

    }
}
