/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class CalendarsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CalendarsDataView.class);

    /**
     * Constructor
     */
    public CalendarsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        return "";
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
