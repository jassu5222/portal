/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class CashMgmtPaymentFileUploadErrorsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CashMgmtPaymentFileUploadErrorsDataView.class);

    /**
     * Constructor
     */
    public CashMgmtPaymentFileUploadErrorsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append(" where p_payment_file_upload_oid = ? ");
    	sbQuery.append(" and reporting_code_error_ind = ? ");
        return sbQuery.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        statement.setString(startVarIdx + varCount, parameters.get("uploadOid"));
        varCount++;
        
        statement.setString(startVarIdx + varCount, TradePortalConstants.INDICATOR_NO);
        varCount++;
        return varCount;
    }
}
