/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;

/**
 *
 *
 */
public class CorpCustAddressSearchDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CorpCustAddressSearchDataView.class);

    /**
     * Constructor
     */
    public CorpCustAddressSearchDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        return " where p_corp_org_oid = ?";

    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        String corpOrgOid = SQLParamFilter.filterNumber(parameters.get("corp_org_oid"));
        statement.setString(startVarIdx + varCount, corpOrgOid);
        varCount++;
        return varCount;
    }
}
