/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import java.util.Map;

/**
 *
 *
 */
public class CorporateCustAccountSetupDataView extends AbstractEJBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CorporateCustAccountSetupDataView.class);

    /**
     * Constructor
     */
    public CorporateCustAccountSetupDataView() {
        super();
    }

    @Override
    protected DocumentHandler getDataFromEjbSource(Map<String, String> parameters) throws Exception {

        StringBuilder sql = new StringBuilder();

        String corporateOrgOid = SQLParamFilter.filterNumber(parameters.get("corporateOrgOid"));
        String allowPayByAnotherAccnt = SQLParamFilter.filter(parameters.get("allowPayByAnotherAccnt"));
        String serverLocation = SQLParamFilter.filter(parameters.get("serverLocation"));
        String clientBankOid = SQLParamFilter.filterNumber(parameters.get("clientBankOid"));

        QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(serverLocation, "QueryListView");

        ArrayList sqlParams = new ArrayList();

        sql.append("select account_oid, account_number, currency,bank_country_code,account_type,account_name,source_system_branch,available_for_internatnl_pay,available_for_xfer_btwn_accts,a_panel_auth_group_oid,available_for_direct_debit,available_for_loan_request,available_for_domestic_pymts,available_for_settle_instr,pending_transaction_balance,fx_rate_group,a_op_bank_org_oid,othercorp_customer_indicator,proponix_customer_id, deactivate_indicator, pending_transac_credit_balance");
        sql.append(" from account");
        sql.append(" where p_owner_oid = ? ");
        sqlParams.add(corporateOrgOid);

        if (StringFunction.isBlank(allowPayByAnotherAccnt)) {
            String sqlQuery = "select allow_pay_by_another_accnt from client_bank where organization_oid = ? ";

            queryListView.setSQL(sqlQuery, new Object[]{clientBankOid});
            queryListView.getRecords();

            DocumentHandler result = queryListView.getXmlResultSet();
            allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
        }
        //cquinton 9/23/2011 Rel 7.1 account for null values

        if (!TradePortalConstants.INDICATOR_YES.equals(allowPayByAnotherAccnt)) {
            sql.append(" and (othercorp_customer_indicator != ? or othercorp_customer_indicator is null) ");
            sqlParams.add(TradePortalConstants.INDICATOR_YES);
        }

        sql.append(" order by ");
        sql.append("account_number");

        queryListView.setSQL(sql.toString(), sqlParams);
        queryListView.getRecords();

        DocumentHandler acctList = queryListView.getXmlResultSet();

        return acctList;
    }

}
