/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;

public class CustAccountDataView extends AbstractAccountDataView {
private static final Logger LOG = LoggerFactory.getLogger(CustAccountDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    //store the value which will be the same for the user
    boolean allowPayByAnotherAcctInitialized = false;


    /**
     * Constructor
     */
    public CustAccountDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters)
            throws DataViewException {
        StringBuilder sql = new StringBuilder();

        //cquinton 12/20/2012 - get user/org info from session rather than
        // passed in vars for security and to encapsulate for common use on home page
        String allowPayAcc = getAllowPayByAnotherAcct();

        sql.append("and a.p_owner_oid = ?");
        sqlParams.add(this.getOwnerOrgOid());

        if (TradePortalConstants.INDICATOR_NO.equals(allowPayAcc)) {
                sql.append("and a.othercorp_customer_indicator != ?");
                sqlParams.add(TradePortalConstants.INDICATOR_YES);
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException, DataViewException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

    private String getAllowPayByAnotherAcct()    throws DataViewException {
    	String allowPayByAnotherAcct = null;
    	
        if (!allowPayByAnotherAcctInitialized) {
            try {
                String clientBankOid = this.getClientBankOid();
                String sql = "select allow_pay_by_another_accnt from client_bank where organization_oid = ? ";

                DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{clientBankOid});
                if (result != null) {
                    allowPayByAnotherAcct = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
                }
                allowPayByAnotherAcctInitialized = true;
            } catch (AmsException ex) {
                throw new DataViewException("Problem getting allowPayByAnotherAcct from client bank", ex);
            }
        }

        return allowPayByAnotherAcct;

    }

}
