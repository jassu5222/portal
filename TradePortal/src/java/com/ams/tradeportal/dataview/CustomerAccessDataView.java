/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class CustomerAccessDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(CustomerAccessDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public CustomerAccessDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();

        String ownershipLevel = this.getOwnershipLevel();
        String selectedOrgOid = parameters.get("org");
        String unencryptedOrgOid
                = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
        if (TradePortalConstants.OWNER_GLOBAL.equals(ownershipLevel)) {
            //if a selection has been made
            if (selectedOrgOid != null && selectedOrgOid.length() > 0) {
                sql.append(" and a_client_bank_oid = ?"); //selectedOrgOid
                sqlParams.add(unencryptedOrgOid);
            }
        } else if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            sql.append(" and a_client_bank_oid = ?");
            sqlParams.add(this.getClientBankOid());
            //if a selection has been made
            if (selectedOrgOid != null && selectedOrgOid.length() > 0) {
                sql.append(" and a_bank_org_group_oid = ?"); //selectedOrgOid
                sqlParams.add(unencryptedOrgOid);
            }
        } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
            sql.append(" and a_bank_org_group_oid = ?"); //bgOrg
            sqlParams.add(this.getBogOid());
        } else //The user does not have the rights to this dataview!!!
        {
            //throw DataViewException("invalid user level");
        }

        /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start */
        if (StringFunction.isNotBlank(getUserSession()
                .getBankGrpRestrictRuleOid())) {
            sql.append(" and b.organization_oid not in ( ");
            List<String> filteredBankGRoups = filterBankGroups(getUserSession()
                    .getBankGrpRestrictRuleOid());
            for (int iLoop = 0; iLoop < filteredBankGRoups.size(); iLoop++) {
                sql.append(" ? ");
                sqlParams.add(filteredBankGRoups.get(iLoop));
                if (iLoop != filteredBankGRoups.size() - 1) {
                    sql.append(" , ");
                }
            }
            sql.append(" ) ");
        }
        /* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End */

        String orgName = parameters.get("name");
        if (orgName != null && orgName.length() >= 0) {
            sql.append(" and upper(a.name) like ? ");
            sqlParams.add("%" + orgName.toUpperCase() + "%");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {

        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
