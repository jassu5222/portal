/**
 *
 */
package com.ams.tradeportal.dataview;

import java.util.Map;

import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.web.FormManager;
import com.amsinc.ecsg.web.BeanManager;

import javax.servlet.http.HttpServletResponse;

/**

 *
 */
public interface DataView {

    String OUTPUT_TARGET__USER_INTERFACE = "UI";
    String OUTPUT_TARGET__WEB_SERVICE = "WS";

    //todo: should just include this in the constructor
    void setResourceManager(ResourceManager resourceManager);

    void setUserSession(SessionWebBean userSession);

    void setFormManager(FormManager formManager);

    void setBeanManager(BeanManager beanManager);

    void setHttpResponse(HttpServletResponse response);

    ErrorManager getErrorManager();

    String getOutputTarget();

    int getSortColumn(); //RKAZI IR 47582 24-03-2016 REL 9.5 - ADD
    
    void setOutputTarget(String x);

    /**
	 * This method will get the data for the requested DataView
	 *
	 * @param viewName
	 * @param parameters
	 * @param formatType
	 * @return
	 * @throws java.lang.Exception
	 */
	String getData(String viewName, Map<String, String> parameters, String formatType)
	        throws Exception; //todo - use a better exception type

	/**
     * This method will get the data for the requested DataView
     *
     * @param viewName
     * @param parameters
     * @param formatType
     * @param encrypt TODO
     * @return
     * @throws java.lang.Exception
     */
    String getData(String viewName, Map<String, String> parameters, String formatType, Boolean encrypt)
            throws Exception; //todo - use a better exception type

    /**
     * Returns an xml document with list of errors.
     *
     * @return
     */
    DocumentHandler getErrorDoc();
}
