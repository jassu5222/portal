package com.ams.tradeportal.dataview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.XmlConfigFileCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * DataViewManager
 *
 * Manages data view meta data.
 *
 * Data view metadata is specified in an xml file in the configxml/dataview directory.
 *
 * Note: this class provides a subset of functionality from previous ListViewManager - primarily the data specific portion - leaving
 * presentation specifics to the datagrid. See DataGridManager for presentation specifics. For ease of implementation, some arguably
 * presentation items are still included here - for example, data formatting. For example, reference data lookups are done here
 * becuase the reference data descriptions are in the database. Similarly the urls for links rendered on the data grid are also
 * generated here, although note that the actual html is not. Arguably - the specific data be returned and the data grid could
 * format into a url. Its easier to do here so thats how we've implemented currently.
 *
 * The following capabilities previously handled by listviews are not supported here -- you should look to datagrid presentation for
 * support. admin vs. nonadmin - solution: define 2 different data grids/views justify - implement in presentation/datagrid width -
 * implement in presentation/datagrid extended - look to presentation for how to deal with this related instrument - this is custom
 * logic for instruments do this on the page itself transaction type - this is custom logic for transactions use a custom formatter
 * instead transaction status - this is custom logic for transactions use a custom formatter instead
 *
 * As requests are made to load the xml file, the xml is read in, parsed and placed in a hashtable. This hashtable is then stored in
 * another hashtable, keyed by the xml file name. Efficiency is improved by caching the data rather than reading it again.
 * <p>
 * Request for the data view metadata are returned as a hashtable. The keys to the data values are defined as static constants in
 * this class (for example, ROWS_PER_PAGE). Since each column displayed in the list view contains several pieces of information, the
 * data value stored in the hashtable is an instance of the inner class DataViewColumn. Each column is keyed within the hashtable by
 * COLUMN + the zero based column number (for example, use the key COLUMN + 0 to get the first column).
 * <p>
 *
 * This is sample xml to define a DataView.
 *
 * <pre>
 *   <dataView>
 *     <SQL>
 *       <selectClause>select oid,
 *               name as bankName,
 *               assets as bankAssets,
 *               foundDate as bankFoundDate</selectClause>
 *       <fromClause>from bank</fromClause>
 *       <whereClause>where name > 'A'</whereClause>
 *     </SQL>
 *     <rowKey>
 *       <rowKeyValue>oid</rowKeyValue>
 *       <rowKeyValue>bankName</rowKeyValue>
 *     </rowKey>
 *     <column>
 *       <displayNameKey>bankName</displayNameKey>
 *       <dataType>string</dataType>
 *       <linkInfo>
 *          <linkAction>selectBank</linkAction>
 *          <linkParameters>
 *             <linkParameter>
 *                <columnName>oid</columnName>
 *                <secureParameterName>oid</secureParameterName>
 *             </linkParameter>
 *          </linkParameters>
 *       </linkInfo>
 *     </column>
 *     <column>
 *       <displayNameKey>bankAssets</displayNameKey>
 *       <dataType>number</dataType>
 *       <decimals>0</decimals>
 *       <getRelatedInstrID>Y</getRelatedInstrID>
 * </column>
 *     <column>
 *       <displayNameKey>bankFoundDate</displayNameKey>
 *       <dataType>date</dataType>
 *     </column>
 *   </distView>
 *
 * The following table describes each metadata option.
 *
 *     Tag                    Description
 * ---------------- -----------------------------------------------------------
 * selectClause     The is the Select clause that is used to select data to
 *                  display.  Each column selected must be given a logical name
 *                  via the 'AS' reserved word.  This logical name serves two
 *                  purposes.  First, it is used as a lookup name in to the
 *                  external text resource bundle to support international-
 *                  ization.  Second, it is used as the key for each of the
 *                  column definitions provided in the <columns> section.
 *                  Also note that if code values are to display with text
 *                  descriptions, the text description must be selected rather
 *                  than the code value (this also means a join is done).  You
 *                  may use carriage returns within the text to make it more
 *                  readable in the parameter file.  This value is required.
 *                  Note: The select, from, and where clauses, when appended
 *                  together should form a valid working SQL statement.
 *
 * fromClause       This is the From clause used to retrieve the data.  List all
 *                  tables used in the query.  This value is required.
 *
 * WhereClause      This is the static part of the Where clause.  Use it to
 *                  provide any known where conditions.  Usually these are join
 *                  conditions.  This value is optional.  (Additional dynamic
 *                  where clause conditions can be provided on the DataView
 *                  class itself when evaluating url parameters.
 *
 * rowKey           Indicates that the data returned should include a
 *                  special data item that represents the row key.
 *                  This allows the client side to identify the row when
 *                  acting upon row data.
 *
 * rowKeyValue      A column name from the selectClause above, usually an oid field but not
 *                  always. More than one rowKeyValue tag can be specified,
 *                  each will be separated by the '/' character.
 *
 * column           Container tag that holds other tags that define a column.
 *                  At least one column must be specified.
 *
 * displayNameKey   This is a logical column name (see selectClause above).
 *                  It is a key into the text resource bundle and matches a
 *                  logical name in the selectClause.  This value is required.
 *
 * dataType         This case insensitive value indicates the data type of the
 *                  value to display.  Allowed values are 'string', 'number',
 *                  'decimal', 'currency', 'transactionType', 'date', 'GMTDate', and 'GMTDateTime'.
 *                  Numbers are formatted according to the default locale.
 *                  Dates are formatted in dd mmm yyyy format for the default
 *                  locale. Time is specified by a 7 character string in the
 *                  format: 'hh:mm' + 'AM' or 'PM' (Ex: '12:30PM').
 *
 * decimals         Only valid for columns which are 'decimal's.  It indicates
 *                  the number of decimal places to display in the formatted
 *                  number.  The default is 2.
 *
 * trailingZeros    Only valid for columns which are 'decimal's.  It indicates
 *                  whether or not trailing zeros should be used to fill in
 *                  a decimal number that doesn't completely fill in the number
 *                  of decimal places to display.  Allowed values are "true"
 *                  and "false".  The default is "false".
 *
 * tableType        Only valid for columns which are 'refdata' type.
 *                  This is the table type to lookup in refdata to translate
 *                  a ref data code into a description.
 *
 * currencyColumn   Only valid for columns which are 'currency's. It indicates
 *                  the column in the dataview containing the currency code to
 *                  use for formatting the currency amount.  If an invalid or
 *                  no column is specified, the original unformatted amount
 *                  value will be returned.
 *
 * suppressNo       Only valid for columns which are boolean's.  It indicates
 *                  whether or not 'No' values should be suppressed.  Allowed
 *                  values are 'Y' and 'N'.  The default is 'N'.
 *
 * customFormatter  Specify a custom formatter classname.  Should include the full
 *                  package name.  Custom formatters should implement the
 *                  CustomDataViewColumnFormatter interface.
 * </pre>
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */
public class DataViewManager {
	private static final Logger LOG = LoggerFactory.getLogger(DataViewManager.class);

	// The allDataViewMetadata hashtable is a hashtable of hashtables. As each list view parameter file is read in, the data is
	// placed in a hashtable (see loadDataViewMetadata). Each hashtable is then placed in this 'global' hashtable keyed by the listview name.
	private static final Map<String, DataViewMetadata> allDataViewMetadata = new HashMap<>();

	/**
	 * A representation of the data view metadata.
	 *
	 */
	public static class DataViewMetadata {

		public boolean hasErrors = false;
		public String errors = "";
		public String selectClause = "";
		public String fromClause = "";
		public String whereClause = "";
		// MEerupula Rel 8.3 T36000021433/20624 sql tuning
		public String outerSelectClause = "";
		public String outerFromClause = "";
		public String outerWhereClause = "";
		public int sortColumn = 0; // zero based index - default to first column
		public String[] rowKeyValues = null;
		public DataViewManager.DataViewColumn columns[] = null;
		// IR 16481
		private boolean isPreserveSearchCriteriaEnabled = false;

		public boolean isPreserveSearchCriteriaEnabled() {
			return isPreserveSearchCriteriaEnabled;
		}

		public void setPreserveSearchCriteriaEnabled(boolean isPreserveSearchCriteriaEnabled) {
			this.isPreserveSearchCriteriaEnabled = isPreserveSearchCriteriaEnabled;
		}
	}

	/**
	 * A DataViewColumn consists of several pieces of data that help describe it. These are grouped together into an inner class for
	 * ease of use.
	 */
	public static class DataViewColumn {

		/**
		 * columnKey is the resource bundle key used to lookup the value to display for the column header
		 */
		public String columnKey;

		/**
		 * dataType is the type of data to display. dataType indicates how the data should be formatted. See the various constants
		 * defined in ListViewManager.
		 */
		public String dataType;

		/**
		 * decimals indicates the number of decimal places for decimal data types.
		 */
		public int decimals;

		/**
		 * trailingZeros indicates if trailing zeroes should be added to the decimal value being formatted.
		 */
		public boolean trailingZeros;

		/**
		 * tableType is a reference data table name for refdata columns
		 */
		public String tableType;

		/**
		 * currencyColumn is used with currency data types. It represents the column that holds the currency code associated with
		 * the currency value.
		 */
		public String currencyColumn;

		/**
		 * prefixCurrencyCode is a flag that determines if the currency code should be pre-pended to a currency column.
		 */
		public boolean prefixCurrencyCode;

		/**
		 * suppressNo indicates that boolean values of false should be suppressed from display.
		 */
		public String suppressNo;

		/**
		 * a custom formatter if data type is "custom"
		 */
		public String customFormatter;

		/**
		 * indicates whether a column is RelatedInstrID
		 */
		public String getRelatedInstrID;

		public boolean isLink = false; // default

		public boolean isGeneratedDoc = false;
		public String docGenerator = "";
		public String linkAction;
		public String[] linkColumnNames = null;
		public String[] linkSecureParmNames = null;

		// linkUrlColumn is the name of the column that should be auto generated by the dataview if the column is meant to be a link
		// on the display. the linkUrlColumn will hold the contents of the url that should be navigated to when the link is selected
		public String linkUrlColumn;

		/**
		 * tableTypeC is a reference data table name for refdata child columns in TreeGrid
		 */
		public String tableTypeC;

		@Override
		public String toString() {
            return ("[ColumnKey=" + columnKey
                    + " DataType=" + dataType
                    + " decimals=" + decimals
                    + " tableType=" + tableType
                    + " tableTypeC=" + tableTypeC
                    + " trailingZeros=" + trailingZeros
                    + " suppressNo=" + suppressNo
                    + " currencyColumn=" + currencyColumn
                    + " prefixCurrencyCode=" + prefixCurrencyCode
                    + " customFormatter=" + customFormatter
                    + " getRelatedInstrID=" + getRelatedInstrID
                    + "]");
		}
	}

	// All data type constants are defined below
	/**
	 * Indicates the type of the column is a boolean. Associated with the 'dataType' listview xml tag. Prints as 'Yes' or 'No'
	 * (based on resource bundle values). 'No' values may be suppressed when the 'suppressNo' tag is given a value of 'Y'.
	 */
	public final static java.lang.String BOOLEAN = "boolean";

	/**
	 * Indicates the type of the column is a currency value. Associated with the 'dataType' listview xml tag. It is formatted based
	 * on the resource manager's locale. Must be used in conjunction with the 'currencyColumn' tag
	 */
	public final static java.lang.String CURRENCY = "currency";

	/**
	 * Indicates the type of the column is a date. Associated with the 'dataType' listview xml tag. Dates are formatted using the
	 * TPDateTimeUtility with a format type of SHORT and the resource manager's locale.
	 */
	public final static java.lang.String DATE = "date";

	/**
	 * Indicates the type of the column is a decimal. Associated with the 'dataType' listview xml tag. Decimal values are formatted
	 * with a certain number of decimal positions (based on the 'decimals' tag) and with or without trailing zeroes (based on the
	 * 'trailingZeros' tag). The default is 2 decimal places and no trailing zeroes.
	 */
	public final static java.lang.String DECIMAL = "decimal";

	/**
	 * Indicates the type of the column is a GMT Date. Associated with the 'dataType' listview xml tag. GMT Dates are converted to
	 * GMT dates based on the <u>request</u> parameter userTimezone and the locale of the resource manager.
	 */
	public final static java.lang.String GMTDATE = "gmtdate";

	/**
	 * Indicates the type of the column is a GMT DateTime. Associated with the 'dataType' listview xml tag. GMT Date Times are
	 * converted to GMT datetimes based on the <u>request</u> parameter userTimezone and the locale of the resource manager.
	 */
	public final static java.lang.String GMTDATETIME = "gmtdatetime";

	/**
	 * Indicates the type of the column is a DateTime. Associated with the 'dataType' listview xml tag. DateTime are formatted using
	 * the TPDateTimeUtility with a format type of SHORT and the resource manager's locale.
	 */
	public final static java.lang.String DATETIME = "datetime";

	/**
	 * Indicates the type of the column is a number. Associated with the 'dataType' listview xml tag. Numbers are assumed to be long
	 * values and are formatted with the resource manager number formatter.
	 */
	public final static java.lang.String NUMBER = "number";

	/**
	 * Indicates the type of the column is refdata. Associated with the 'dataType' listview xml tag. Must be used in conjuction with
	 * the 'tableType' tag to provide the refdata table to use for translation a code.
	 */
	public final static java.lang.String REFDATA = "refdata";

	/**
	 * Indicates the type of the column is a string. Associated with the 'dataType' listview xml tag.
	 */
	public final static java.lang.String STRING = "string";

	/**
	 * Indicates the type of the column is a encrypted of actual. Associated with the 'dataType' listview xml tag.
	 */
	public final static java.lang.String ENCRYPT = "encrypt";

	/**
	 * Indicates custom column. If specified, the customFormatter tag should also be specified to indicate the class used to format
	 * the data.
	 */
	public final static java.lang.String CUSTOM = "custom";

	/**
	 * Indicates the type of the column is a string suppression Associated with the 'dataType' listview xml tag. Works like a string
	 * data type except when the value is the same as that provided in the '<u>request</u> parameter 'suppressValue'. When there is
	 * an exact match, the value does not display in the listview.
	 */
	public final static java.lang.String SUPPRESS_FOR_VALUE = "suppress_for_value";

	/**
	 * Default constructor
	 *
	 */
	private DataViewManager() {
	}

	/**
	 * This method returns the hashtable containing the parameters for a given listview xml file. If the data was previously loaded,
	 * the hashtable is simply returned. Otherwise, the file is loaded first and then returned.
	 *
	 * @param dataViewName
	 * @return java.util.Hashtable - The hashtable containing the listview parameters
	 */
	public static DataViewMetadata getDataViewMetadata(String dataViewName) {

		DataViewMetadata metadata = allDataViewMetadata.get(dataViewName);

		if (metadata == null) {
			loadDataViewMetadata(dataViewName);
			metadata = allDataViewMetadata.get(dataViewName);
		}

		return metadata;
	}

	/**
	 * This method reads in the XML metadata for a dataView and populates the internal hashtable storage of the data view
	 * information. Each dataview's hashtable is then stored in the 'global' hashtable of dataviews for this class. This method is
	 * only called to initially load or refresh a dataview's data. The dataview xml file is described in the class description.
	 * <p>
	 * 
	 * @param dataViewName
	 *            String - The name of the dataview parameter file to load.
	 */
	private static synchronized void loadDataViewMetadata(String dataViewName) {

		DataViewMetadata metadata = new DataViewMetadata();

		// Default the HAS_ERRORS value to N.
		metadata.hasErrors = false;// only if there are errors then hasErrors is set to true

		try {
			// Create a document handler based on the passed in file.
			DocumentHandler doc = XmlConfigFileCache.readConfigFile("/dataview/".concat(dataViewName).concat(".xml"));
			// todo: should return an error if can find the file!!!!

			// Now get each attribute from the documents and put it in the hashtable. Perform edits as necessary.

			// 'selectClause' is required.
			String value = doc.getAttribute("/SQL/selectClause");
			if (value == null || value.equals("")) {
				postError("/SQL/selectClause is missing or blank", metadata);
			} else {
				metadata.selectClause = value;
			}

			// 'fromClause' is required.
			value = doc.getAttribute("/SQL/fromClause");
			if (value == null || value.equals("")) {
				postError("/SQL/fromClause is missing or blank", metadata);
			} else {
				metadata.fromClause = value;
			}

			// 'whereClause' is optional.
			value = doc.getAttribute("/SQL/whereClause");
			if (value == null) {
				value = "";
			}
			metadata.whereClause = value;

			// MEerupula Rel 8.3 T36000021433/20624 sql tuning. STATRS HERE
			// ctq added for sql performance
			value = doc.getAttribute("/SQL/outerSelectClause");
			if (value == null) {
				value = "";
			}
			metadata.outerSelectClause = value;
			value = doc.getAttribute("/SQL/outerFromClause");
			if (value == null) {
				value = "";
			}
			metadata.outerFromClause = value;
			value = doc.getAttribute("/SQL/outerWhereClause");
			if (value == null) {
				value = "";
			}
			metadata.outerWhereClause = value;
			// MEerupula Rel 8.3 T36000021433/20624 sql tuning ENDS HERE

			value = doc.getAttribute("/sortColumn");
			if (value != null) {
				try {
					metadata.sortColumn = Integer.parseInt(value);
				} catch (Exception ex) {
					metadata.sortColumn = 0;
				}
			}
			// IR 16481 start
			value = doc.getAttribute("/preserveSearchCriteria");
			metadata.isPreserveSearchCriteriaEnabled = (StringFunction.isNotBlank(value) && "Y".equalsIgnoreCase(value));
			// IR 16481 end
			// todo: add a sortOrder capability

			// 'showCheckBox' defaults to N if not provided. Otherwise the value is taken as is. The user of this value
			// decides how to interpret/default the value.  Y/N is expected. If Y is provided, retrieve the column
			// names to use for the check box's value (each separated by a '/').
			DocumentHandler rowKeyDoc = doc.getFragment("/rowKey");
			if (rowKeyDoc != null) {
				// List<String> rowKeyValueList = new ArrayList<>();

				List<DocumentHandler> rowKeyValues = rowKeyDoc.getFragmentsList("/rowKeyValue");
				metadata.rowKeyValues = new String[rowKeyValues.size()];
				for (int j = 0; j < rowKeyValues.size(); j++) {
					DocumentHandler rowKeyValueDoc = rowKeyValues.get(j);
					value = rowKeyValueDoc.getAttribute("/");
					metadata.rowKeyValues[j] = value;

				}
			}

			// At least one 'column' is required.
			List<DocumentHandler> columns = doc.getFragmentsList("/column");
			int columnCount = columns.size();
			if (columnCount < 1) {
				postError("No columns were specified!", metadata);
			}
			metadata.columns = new DataViewManager.DataViewColumn[columnCount];

			DocumentHandler linkParameterDoc = null;
			DocumentHandler linkInfoDoc = null;
			DocumentHandler columnDoc = null;
			Map<String, List<String>> linkSecureParmNames = new HashMap<>();
			Map<String, List<String>> linkColumnNames = new HashMap<>();
			Map<String, String> linkActions = new HashMap<>();
			List<String> linkSecureParmNameList = null;
			List<String> linkColumnNameList = null;
			List<DocumentHandler> linkParameters = null;
			String linkAction = null;
			String isGenerated = null;
			String docGenerator = null;
			DataViewColumn aColumn = null;

			// Loop through each of the column definitions.
			for (int i = 0; i < columnCount; i++) {
				columnDoc = (DocumentHandler) columns.get(i);

				aColumn = buildColumnFromDoc(columnDoc, metadata);

				// If the column is a link column, retrieve the link action and link parameters.
				linkInfoDoc = columnDoc.getFragment("/linkInfo");
				if (linkInfoDoc != null) {
					aColumn.isLink = true;

					// CR914-Support generation of link to generated PDF documents-start
					docGenerator = linkInfoDoc.getAttribute("/docGenerator");
					isGenerated = linkInfoDoc.getAttribute("/isGeneratedDoc");

					// CR914-Support generation of link to generated PDF documents-end
					if (TradePortalConstants.INDICATOR_YES.equals(isGenerated)) {
						aColumn.isGeneratedDoc = true;
					}

					if (StringFunction.isNotBlank(docGenerator)) {
						aColumn.docGenerator = docGenerator;
					}

					linkAction = linkInfoDoc.getAttribute("/linkAction");
					aColumn.linkAction = linkAction;

					linkParameters = linkInfoDoc.getFragmentsList("/linkParameters/linkParameter");

					linkColumnNameList = new ArrayList<>();
					linkSecureParmNameList = new ArrayList<>();

					aColumn.linkColumnNames = new String[linkParameters.size()];
					aColumn.linkSecureParmNames = new String[linkParameters.size()];

					for (int k = 0; k < linkParameters.size(); k++) {
						linkParameterDoc = (DocumentHandler) linkParameters.get(k);

						String columnName = linkParameterDoc.getAttribute("/columnName");
						String secureParmName = linkParameterDoc.getAttribute("/secureParameterName");

						linkColumnNameList.add(columnName);
						linkSecureParmNameList.add(secureParmName);

						aColumn.linkColumnNames[k] = columnName;
						aColumn.linkSecureParmNames[k] = secureParmName;
					}

					linkSecureParmNames.put(aColumn.columnKey, linkSecureParmNameList);
					linkColumnNames.put(aColumn.columnKey, linkColumnNameList);
					if (null == linkAction) {
						linkAction = "";
					}

					linkActions.put(aColumn.columnKey, linkAction);

					aColumn.linkUrlColumn = linkInfoDoc.getAttribute("/linkUrlColumn");

				}

				metadata.columns[i] = aColumn;

			} // end for each column loop

			// After loading all the parameters, put the hashtable in
			// the classes 'allDataViewMetadata' table, keyed by the name of the listview.
			allDataViewMetadata.put(dataViewName, metadata);

		} catch (Exception e) {
			LOG.error("Exception occured while loading data view metadata ", e);
		}
	}

	/**
	 * Appends the given error to the current list of error messages (which is stored in the hashtable under the keyword ERRORS.
	 * Messages are separated by a carriage return/line feed. Also sets the HAS_ERRORS hash value to Y.
	 *
	 * @param message
	 *            java.lang.String - The error message to post
	 */
	private static void postError(String message, DataViewMetadata metadata) {
		metadata.hasErrors = true;

		if (metadata.errors == null) {
			metadata.errors = "";
		}

		metadata.errors += "\n\r" + message;
	}

	/**
	 * This method clears all the data from the global hashtable. This method can be called to refresh the listview parameters
	 * without restarting the web server.
	 *
	 */
	public synchronized void refresh() {

		try {
			allDataViewMetadata.clear();

		} catch (Exception e) {
			LOG.error("Error occured refreshing Data View Metadata", e);
		}
	}

	/**
	 * Build column from xml.
	 * <p>
	 */
	private static synchronized DataViewColumn buildColumnFromDoc(DocumentHandler columnDoc,
			DataViewManager.DataViewMetadata dataViewMetadata) {

		DataViewColumn aColumn = new DataViewColumn();

		try {

			// 'displayNameKey' is required for each column.
			String value = columnDoc.getAttribute("/displayNameKey");

			if (value == null || value.equals("")) {
				postError("/column/displayNameKey is missing or blank", dataViewMetadata);
				value = "";
			}
			aColumn.columnKey = value;

			// Valid justify values are 'string', 'number', 'decimal', 'date', 'gmtdate', 'gmtdatetime', 'boolean',
			// or 'refdata'. If missing, default to 'string'. If not valid, post an error and default to 'string'.
			value = columnDoc.getAttribute("/dataType");
			if (value == null || value.equals("")) {
				value = STRING;
			} else {
				value = value.toLowerCase();
                if (!(value.equals(STRING)
                        || value.equals(DATE)
                        || value.equals(GMTDATE)
                        || value.equals(GMTDATETIME)
					    || value.equals(DATETIME)	//Srinivasu_D IR#T36000040369 Rel9.3(WS) 06/29/2015 - to handle datetime
                        || value.equals(NUMBER)
                        || value.equals(DECIMAL)
                        || value.equals(CURRENCY)
                        || value.equals(REFDATA)
                        || value.equals(ENCRYPT)
                        || value.equals(BOOLEAN)
                        || value.equals(CUSTOM)
                        || value.equals(SUPPRESS_FOR_VALUE))) {
					postError("/column/dataType value of " + value + " is invalid; changing to 'string'", dataViewMetadata);
					value = STRING;
				}
			}
			aColumn.dataType = value;

			// If the decimals attribute is given, it must be numeric. (This test only applies if the datatype
			// is decimal.). If the test fails, it defaults to 2.
			value = columnDoc.getAttribute("/decimals");
			int intValue = 0;
			if (aColumn.dataType.equals(DECIMAL)) {
				try {
					intValue = Integer.parseInt(value);
				} catch (NumberFormatException e) {
					postError("/column/decimals value of " + value + " is not a valid number; defaulting to 2", dataViewMetadata);
					intValue = 2;
				}
			} else {
				intValue = 0;
			}
			aColumn.decimals = intValue;

			// If the trailingZeros attribute is given, it must be either 'true' or 'false'. (This test only applies if the datatype
			// is decimal.). If the test fails, it defaults to 'false'.
			value = columnDoc.getAttribute("/trailingZeros");
			if (aColumn.dataType.equals(DECIMAL)) {
				if (value == null || value.equals("") || (!value.equals("true") && !value.equals("false"))) {
					postError("/column/trailingZeros is missing, blank, or invalid", dataViewMetadata);
					value = "false";
				}
			} else {
				value = "false";
			}
			aColumn.trailingZeros = Boolean.parseBoolean(value);

			// If the suppressNo attribute is given, it must be either 'Y' or 'N'. (This test only applies if the datatype is boolean.).
			// If the test fails, it defaults to 'N'.
			value = columnDoc.getAttribute("/suppressNo");
			if (aColumn.dataType.equals(BOOLEAN)) {
				if (value == null || value.equals("")) {
					value = TradePortalConstants.INDICATOR_NO;
				} else if (!value.equals(TradePortalConstants.INDICATOR_YES) && !value.equals(TradePortalConstants.INDICATOR_NO)) {
					postError("/column/suppressNo is invalid - defaulting to N", dataViewMetadata);
					value = TradePortalConstants.INDICATOR_NO;
				}
			} else {
				value = TradePortalConstants.INDICATOR_NO;
			}
			aColumn.suppressNo = value;

			// If the currencyColumn attribute is given, it must correspond to a column that contains valid currency codes. This column
			// only applies if the data type is 'currency'.
			value = columnDoc.getAttribute("/currencyColumn");
			if (aColumn.dataType.equals(CURRENCY)) {
				if (value == null || value.equals("")) {
					value = "";
				}
			} else {
				value = "";
			}
			aColumn.currencyColumn = value;

			// If the prefixCurrencyCode attribute is given, it must coorespond to a column that contains valid currency codes. This column
			// only applies if the data type is 'currency'
			value = columnDoc.getAttribute("/prefixCurrencyCode");
			if (aColumn.dataType.equals(CURRENCY)) {
				if (value == null || value.equals("")) {
					value = "";
				}
			} else {
				value = "";
			}
			aColumn.prefixCurrencyCode = value.equals("Y");

			// If the datatype is refdata, the tabletype or tabletypeC attribute is required.
			if (columnDoc.getAttribute("/tabletype") != null) {
				value = columnDoc.getAttribute("/tabletype");
			}

			if (columnDoc.getAttribute("/tabletypeC") != null) {
				aColumn.tableTypeC = columnDoc.getAttribute("/tabletypeC");
			}
			// value = columnDoc.getAttribute("/tabletype");
			if (aColumn.dataType.equals(REFDATA)) {
				if (value == null || value.equals("")) {
					postError("/column/tabletype is required for column " + aColumn.columnKey, dataViewMetadata);
				}
			}
			aColumn.tableType = value;

			// custom formatter
			value = columnDoc.getAttribute("/customFormatter");
			aColumn.customFormatter = value;

			value = columnDoc.getAttribute("/getRelatedInstrID");
			if (value == null || value.equals("")) {
				value = TradePortalConstants.INDICATOR_NO;
			} else if (!value.equals(TradePortalConstants.INDICATOR_YES) && !value.equals(TradePortalConstants.INDICATOR_NO)) {
				postError("/column/getRelatedInstrID is invalid - defaulting to N", dataViewMetadata);
				value = TradePortalConstants.INDICATOR_NO;
			}
			aColumn.getRelatedInstrID = value;

		} catch (Exception e) {
			LOG.error("Exception occured in buildColumnFromDoc ", e);
		}

		return aColumn;
	}

}
