/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.util.EncryptDecrypt;
/**
 *
 *
 */
public class DirectDebitPendingDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(DirectDebitPendingDataView.class);
    final static String ALL_WORK = "PendingTransactions.AllWork";
    final static String MY_WORK  = "PendingTransactions.MyWork";
        
	/**
	 * Constructor
	 */
	public final static String INVOICE_SEARCH_STATUS_CLOSED    = "CLOSED";
	public DirectDebitPendingDataView() {

		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();

        List<String> statuses = new ArrayList<>();

        String selectedStatus = SQLParamFilter.filter(parameters.get("transStatusType"));
    	//cquinton 3/26/2013 Rel PR ir#15207 ensure parameters associated with access
    	// are encrypted or used from session
    	String encryptedSelectedWorkflow = parameters.get("Workflow");

        if (TradePortalConstants.STATUS_READY.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.READY_TO_AUTHORIZE);
    
        }
        else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        }
        else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.AUTHORIZE_FAILED);
        }
        else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus) )
        {
           statuses.add(TransactionStatus.STARTED);
        }
        else // default is ALL
        {
           statuses.add(TransactionStatus.STARTED);
           statuses.add(TransactionStatus.AUTHORIZE_FAILED);
           statuses.add(TransactionStatus.PARTIALLY_AUTHORIZED);
           statuses.add(TransactionStatus.READY_TO_AUTHORIZE);
           selectedStatus = TradePortalConstants.STATUS_ALL;
        }

        // Build a where clause using the vector of statuses we just set up.
        dynamicWhereClause.append(" and t.transaction_status in (");

        int numStatuses = statuses.size();
        for (int i=0; i<numStatuses; i++) {
            dynamicWhereClause.append("'");
            dynamicWhereClause.append(statuses.get(i) );
            dynamicWhereClause.append("'");

            if (i < numStatuses - 1) {
              dynamicWhereClause.append(", ");
            }
        }

        dynamicWhereClause.append(") ");

        //cquinton 3/26/2013 Rel PR ir#15207 ensure parameters associated with access
        // are encrypted or used from session
        if ( encryptedSelectedWorkflow!=null && encryptedSelectedWorkflow.length()>0 ) {
            String selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedSelectedWorkflow, getUserSession().getSecretKey());
            if (MY_WORK.equals(selectedWorkflow)) {
               dynamicWhereClause.append("and t.a_assigned_to_user_oid = ?"); //userOid
            }
            else if (ALL_WORK.equals(selectedWorkflow)) {
               dynamicWhereClause.append("and i.a_corp_org_oid in (");
               dynamicWhereClause.append(" select organization_oid");
               dynamicWhereClause.append(" from corporate_org");
               dynamicWhereClause.append(" where activation_status = '");
               dynamicWhereClause.append(TradePortalConstants.ACTIVE);
               dynamicWhereClause.append("' start with organization_oid = ?"); //userOrgOid
               dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            }
            else
            {
               dynamicWhereClause.append("and i.a_corp_org_oid = ?"); //selectedWorkflow
            }
        }
        else {
            //workflow is required
            dynamicWhereClause.append("and 1=0");
        }

        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        //cquinton 3/26/2013 Rel PR ir#15207 ensure parameters associated with access
        // are encrypted or used from session
        String encryptedSelectedWorkflow = parameters.get("Workflow");
        if ( encryptedSelectedWorkflow!=null && encryptedSelectedWorkflow.length()>0 ) {
            String selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedSelectedWorkflow, getUserSession().getSecretKey());
            
            if (MY_WORK.equals(selectedWorkflow)) {
                String userOid = this.getUserOid();
                statement.setString(startVarIdx+varCount, userOid);
                varCount++;
            }
            else if (ALL_WORK.equals(selectedWorkflow)) {
                String userOrgOid = this.getOwnerOrgOid();
                statement.setString(startVarIdx+varCount, userOrgOid);
                varCount++;
            }
            else
            {
                statement.setString(startVarIdx+varCount, selectedWorkflow);
                varCount++;
            }
        }

        return varCount;
    }
}
