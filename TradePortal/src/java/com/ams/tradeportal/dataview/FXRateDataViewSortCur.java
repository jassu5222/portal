/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class FXRateDataViewSortCur     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(FXRateDataViewSortCur.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public FXRateDataViewSortCur() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder dynamicWhere = new StringBuilder();

         String clientBankID = getUserSession().getClientBankOid();
         String parentOrgID = getUserSession().getOwnerOrgOid();
         String ownershipLevel = getUserSession().getOwnershipLevel();
         String filterText = parameters.get("filterText");

        //AAlubala - IR#IAUL050662274 -04/27/2011 - If it is a corpoate user, get the rates for the client bank as well
         if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
         {
        	 dynamicWhere.append(" where p_owner_org_oid in (?,?)");
             sqlParams.add(clientBankID);
             sqlParams.add(parentOrgID);
         }  else {
        	 dynamicWhere.append(" where p_owner_org_oid = ? ");
        	 sqlParams.add(parentOrgID);
         }
         if (filterText != null && !filterText.equals(""))
	     {
        	 dynamicWhere.append (" and upper(fx_rate_group) like unistr(?)");
        	 sqlParams.add("%" + StringFunction.toUnistr(filterText)+ "%");
	     }

        return dynamicWhere.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
