package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;

public class FXRateRequestVisibilityCondition extends AbstractVisibilityCondition {
private static final Logger LOG = LoggerFactory.getLogger(FXRateRequestVisibilityCondition.class);

	@Override
	public boolean execute(SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr) {
		boolean visible=false;

		CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		corpOrg.getById(userSession.getOwnerOrgOid());
		BankOrganizationGroupWebBean bankOrgGrp  = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
		bankOrgGrp.getById(corpOrg.getAttribute("bank_org_group_oid"));

		if (TradePortalConstants.INDICATOR_YES.equals(bankOrgGrp.getAttribute("fx_online_avail_ind")) && userSession.hasAccessToLiveMarketRate()) {

			String userSecurityRights = userSession.getSecurityRights();

			if	(SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.DOM_PMT, TransactionType.ISSUE) ||
					SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.XFER_BET_ACCTS, TransactionType.ISSUE) ||
					SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.FUNDS_XFER, TransactionType.ISSUE)) {
				String loginSecurityType = null;
				boolean subsidiaryAccessPanelAuth = false;

				if (userSession.hasSavedUserSession()) {
					loginSecurityType = userSession.getSavedUserSessionSecurityType();
					subsidiaryAccessPanelAuth=isSubsidiaryAccessPanelAuth(corpOrg, beanMgr); 
				} else {
					loginSecurityType = userSession.getSecurityType();
				}

				if (TradePortalConstants.NON_ADMIN.equals(loginSecurityType) &&
						!subsidiaryAccessPanelAuth) {
					visible = true;
				}
			}
		}

		return visible;
	}
	
	private boolean isSubsidiaryAccessPanelAuth(CorporateOrganizationWebBean corpOrg, BeanManager beanMgr){
		boolean access=TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("allow_panel_auth_for_pymts"));
		 if (!access) {
				String parentCorpOrgOid = corpOrg.getAttribute("parent_corp_org_oid");
		    	if (StringFunction.isNotBlank(parentCorpOrgOid))
		    	{
			    	CorporateOrganizationWebBean parentCorpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		    	    parentCorpOrg.getById(parentCorpOrgOid);
		    	    if (TradePortalConstants.INDICATOR_YES.equals(parentCorpOrg.getAttribute("allow_panel_auth_for_pymts")))
		    	    {
		    	    	access = true;	    	
		    	    }
		    	}
		   	}
		 return access;
	}

}
