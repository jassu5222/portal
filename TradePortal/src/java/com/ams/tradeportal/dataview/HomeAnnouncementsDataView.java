/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DateTimeUtility;

/**
 *
 *
 */
public class HomeAnnouncementsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(HomeAnnouncementsDataView.class);
	/**
	 * Constructor
	 */
	public HomeAnnouncementsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters)
            throws DataViewException {

        StringBuilder sql = new StringBuilder();

        String ownershipLevel = this.getOwnershipLevel();

        if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            sql.append("where p_owner_org_oid = ? "); //ownerOrgOid
        }
        else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
            sql.append("where (p_owner_org_oid = ?"); //ownerOrgOid
            sql.append(  " or (p_owner_org_oid = ?"); //clientBankOid
            sql.append(    " and (all_bogs_ind='Y'");
            sql.append(      " or exists (select 1 from announcement_bank_group");
            sql.append(                  " where p_announcement_oid=a.announcement_oid");
            sql.append(                  " and a_bog_oid = ?)))) "); //ownerOrgOid
        }
        else  //The user does not have the rights to this dataview!!!
        {
            
            sql.append("where 1=0 ");
        }

        //cquinton 9/21/2012 ir#4158
        //on the admin home page only the currently active announcements are displayed
        //get the user date - which is specific to the user timezone!
        sql.append("and announcement_status = 'ACTIVE' ");
        sql.append("and start_date <= to_date(?, 'YYYY-MM-DD')");
        sql.append("and end_date >= to_date(?, 'YYYY-MM-DD')");

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException, DataViewException {
        int varCount = 0;

        String ownershipLevel = this.getOwnershipLevel();

        if (TradePortalConstants.OWNER_BANK.equals(ownershipLevel)) {
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }
        else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
            statement.setString(startVarIdx+varCount, this.getClientBankOid());
            varCount++;
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }

        //cquinton 9/21/2012 ir#4158
        //on the admin home page only the currently active announcements are displayed
        //get the user date - which is specific to the user timezone!
        String currentGMTTime = "";
        try {
            currentGMTTime = DateTimeUtility.getGMTDateTime(true,false);//do not use override date
        }
        catch (AmsException ex) {
            throw new DataViewException("Problem getting current time", ex);
        }
        Date myDate = TPDateTimeUtility.getLocalDateTime(
          currentGMTTime, this.getUserSession().getTimeZone());
        SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String myIsoDate = isoDateFormatter.format(myDate);
        statement.setString(startVarIdx+varCount, myIsoDate);
        varCount++;
        statement.setString(startVarIdx+varCount, myIsoDate);
        varCount++;

        return varCount;
    }
}
