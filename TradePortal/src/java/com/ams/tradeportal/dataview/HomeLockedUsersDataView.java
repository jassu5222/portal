/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class HomeLockedUsersDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(HomeLockedUsersDataView.class);

    /**
     * Constructor
     */
    public HomeLockedUsersDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder sql = new StringBuilder();

        // Show users that are locked out, but only in the appropriate organizatiosn
        if (TradePortalConstants.OWNER_GLOBAL.equals(this.getOwnershipLevel())) {
            // Don't filter the users at all - show all locked users in the
            // database
        } else if (TradePortalConstants.OWNER_BANK.equals(this.getOwnershipLevel())) {
            // Show all users under the client bank
            sql.append(" and a.a_client_bank_oid = ?"); //clientBankOid
        } else {
            // For BOG users, display the same set of users as for a client
            // bank, but leave out the bank level admin users
            sql.append(" and a.a_client_bank_oid = ?"); //clientBankOid
            sql.append(" and a.ownership_level <> '");
            sql.append(TradePortalConstants.OWNER_BANK);
            sql.append("'");
        }

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        // Show users that are locked out, but only in the appropriate organizatiosn
        if (TradePortalConstants.OWNER_GLOBAL.equals(this.getOwnershipLevel())) {
            // Don't filter the users at all - show all locked users in the
            // database
        } else if (TradePortalConstants.OWNER_BANK.equals(this.getOwnershipLevel())) {
            statement.setString(startVarIdx + varCount, this.getClientBankOid());
            varCount++;
        } else {
            statement.setString(startVarIdx + varCount, this.getClientBankOid());
            varCount++;
        }

        return varCount;
    }
}
