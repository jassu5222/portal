/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class HomeMailMessagesDataView    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(HomeMailMessagesDataView.class);
    //some options for the org search parameter
    final static String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
    final static String ALL_MAIL = "Mail.AllMail";
    public final static String ALL_MAIL_MESS = "Mail.AllMailMESS";

	/**
	 * Constructor
	 */
	public HomeMailMessagesDataView() {
		super();
	}

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String selectedOrgOid = parameters.get("org");
        String unencryptedOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
        if (HomeMailMessagesDataView.MY_MAIL_AND_UNASSIGNED.equals(unencryptedOrgOid)) {
            sql.append("and (a.a_assigned_to_user_oid = ?"); //userOid
            sql.append(" or (a.a_assigned_to_corp_org_oid = ?"); //ownerOrgOid
            sql.append(" and a.a_assigned_to_user_oid is null))");
        }
        else if (HomeMailMessagesDataView.ALL_MAIL.equals(unencryptedOrgOid)) {
            sql.append("and a.a_assigned_to_corp_org_oid in (");
            sql.append( "select organization_oid");
            sql.append( " from corporate_org");
            sql.append( " where activation_status = '");
            sql.append( TradePortalConstants.ACTIVE);
            sql.append( "' start with organization_oid = ?"); //ownerOrgOid
            sql.append( " connect by prior organization_oid = p_parent_corp_org_oid)");
        }
        else if ( unencryptedOrgOid != null && unencryptedOrgOid.length()>0 ) {
            sql.append("and a.a_assigned_to_corp_org_oid = ?"); //selected org
        }
        //cquinton 4/9/2013 Rel PR ir#15704 begin
        else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
        //cquinton 4/9/2013 Rel PR ir#15704 end
        
        String unreadFlag = SQLParamFilter.filter(parameters.get("UnreadFlag"));
                
        if (StringFunction.isNotBlank(unreadFlag) && !ALL_MAIL.equals(unreadFlag)) 
        {
        	if("N".equals(unreadFlag))
        	{
           		sql.append(" and a.unread_flag = 'N'");
        	}
        	else 
        	{
        		sql.append(" and (a.unread_flag is null or a.unread_flag <> 'N')");
        	}
        	
        }

        //inbox folder
        sql.append(" and message_status in ('");
        sql.append(TradePortalConstants.REC);
        sql.append("', '");
        sql.append(TradePortalConstants.REC_ASSIGNED);
        sql.append("', '");
        sql.append(TradePortalConstants.SENT_ASSIGNED);
        sql.append("') ");
        
        /*CR 913 - Rel 9.0 - vdesingu
         *Filter mail messages for funding_amount and funding_date is NULL*/
        sql.append(" and a.funding_amount is NULL");
        sql.append(" and a.funding_date is NULL ");

        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        String selectedOrgOid = parameters.get("org");
        String unencryptedOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
        if (HomeMailMessagesDataView.MY_MAIL_AND_UNASSIGNED.equals(unencryptedOrgOid)) {
            statement.setString(startVarIdx+varCount, this.getUserOid());
            varCount++;
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }
        else if (HomeMailMessagesDataView.ALL_MAIL.equals(unencryptedOrgOid)) {
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }
        else if ( unencryptedOrgOid != null && unencryptedOrgOid.length()>0 ) {
            statement.setString(startVarIdx+varCount, unencryptedOrgOid);
            varCount++;
        }

        String mailRead = SQLParamFilter.filter(parameters.get("Read/Unread"));
        if (HomeMailMessagesDataView.ALL_MAIL.equals(mailRead)) {
            //do nothing
        }
        else if ( mailRead != null && mailRead.length()>0 ) {
            statement.setString(startVarIdx+varCount, mailRead);
            varCount++;
        }
        return varCount;
    }
}
