/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class HomeNotificationsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(HomeNotificationsDataView.class);
    //some options for the org and status search parameters
    final static String ALL_NOTIFS = "Notifications.All";
    final static String ALL_NOTIFS_RUN = "Notifications.AllRUN";

	/**
	 * Constructor
	 */
	public HomeNotificationsDataView() {
		super();
	}

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();
              
        String selectedOrgOid = parameters.get("org");
        String confInd  = getConfidentialInd();
       
     
        String unencryptedOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
      
        if (HomeNotificationsDataView.ALL_NOTIFS.equals(unencryptedOrgOid)) {
            sql.append(" and b.a_corp_org_oid in (select organization_oid ");
            sql.append(" from corporate_org where activation_status = 'ACTIVE' ");
            sql.append(" start with organization_oid = ?"); //ownerOrgOid
            sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
        }
        else if ( unencryptedOrgOid != null && unencryptedOrgOid.length()>0 ) {
            sql.append(" and b.a_corp_org_oid=?"); //selected org
        }
         else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
    

        String selectedStatus = SQLParamFilter.filter(parameters.get("status"));
        if (HomeNotificationsDataView.ALL_NOTIFS.equals(selectedStatus)) {
            //do nothing
        }
        else if ( selectedStatus != null && selectedStatus.length()>0 ) {
            sql.append(" and a.transaction_status=?"); //selected status
        }
        else {
            //throw exception!!! missing required parm
        }
      
       
        if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
           sql.append(" and (b.confidential_indicator = 'N' OR ");
           sql.append(  "b.confidential_indicator is null) ");
        }
      //Added by Jaya Mamidala - CR49930 -  Start
        
        String unreadFlag = SQLParamFilter.filter(parameters.get("UnreadFlag"));
        if (StringFunction.isNotBlank(unreadFlag) && !ALL_NOTIFS_RUN.equals(unreadFlag)) 
        {
        	if("N".equals(unreadFlag))
        	{
           		sql.append(" and a.unread_flag = 'N'");
        	}
        	else 
        	{
        		sql.append(" and (a.unread_flag is null or a.unread_flag <> 'N')");
        	}
        	
        }
      //Jaya Mamidala - CR49930 -  END
        return sql.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        String selectedOrgOid = parameters.get("org");
        String unencryptedOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
        if (HomeNotificationsDataView.ALL_NOTIFS.equals(unencryptedOrgOid)) {
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }
        else if ( unencryptedOrgOid != null && unencryptedOrgOid.length()>0 ) {
            statement.setString(startVarIdx+varCount, unencryptedOrgOid);
            varCount++;
        }

        String selectedStatus = SQLParamFilter.filter(parameters.get("status"));
        if (HomeNotificationsDataView.ALL_NOTIFS.equals(selectedStatus)) {
            //do nothing
        }
        else if ( selectedStatus != null && selectedStatus.length()>0 ) {
            statement.setString(startVarIdx+varCount, selectedStatus);
            varCount++;
        }
        
        String selectedRead = SQLParamFilter.filter(parameters.get("Read/Unread"));
        if (HomeNotificationsDataView.ALL_NOTIFS.equals(selectedRead)) {
            //do nothing
        }
        else if ( selectedRead != null && selectedRead.length()>0 ) {
            statement.setString(startVarIdx+varCount, selectedRead);
            varCount++;
        }

        return varCount;
    }
   
    public String buildQuery(DataViewManager.DataViewMetadata metadata,
            String dynamicWhereClause ) {

    	StringBuilder builtSQL = new StringBuilder(super.buildQuery(metadata, dynamicWhereClause));
    	if (metadata.columns[sortColumn].columnKey.equalsIgnoreCase("Amount")){
    	String a = "decode(b.instrument_type_code/*not_column_name*/, 'BIL'/*not_column_name*/, null/*not_column_name*/, a.copy_of_amount/*not_column_name*/) ";
    	builtSQL.replace(builtSQL.indexOf(a),builtSQL.indexOf(a)+a.length()," a.copy_of_amount ");
    	a = a.replace("b.instrument_type_code", "InstrumentType");
    	a = a.replace("a.copy_of_amount", "Amount");
    	builtSQL.replace(builtSQL.lastIndexOf("Amount, Status"),builtSQL.lastIndexOf("Amount, Status")+6,a + " AS Amount " );
    	}

    	return builtSQL.toString();
    }
    
}
