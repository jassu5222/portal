/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class HomeReceivablesMatchingDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(HomeReceivablesMatchingDataView.class);
    static final String ALL_WORK = "common.allWork";
    static final String MY_WORK = "common.myWork";
    static final String ALL = "common.all";

	/**
	 * Constructor
	 */
	public HomeReceivablesMatchingDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String selectedWorkOrgOid = parameters.get("work");
        String unencryptedWorkOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkOrgOid, getUserSession().getSecretKey());
        if (ALL_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append(" and a.a_seller_corp_org_oid in (");
            sql.append(" select organization_oid");
            sql.append(" from corporate_org");
            sql.append(" where activation_status = '");
            sql.append(TradePortalConstants.ACTIVE);
            sql.append("' start with organization_oid = ?"); //ownerOrgOid
            sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
        }
        else if (MY_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append(" and a.a_assigned_to_user_oid = ?"); //userOid
        }
        else if ( unencryptedWorkOrgOid!=null && unencryptedWorkOrgOid.length()>0 ){
            sql.append(" and a.a_seller_corp_org_oid = ?"); //selectedWorkOrgOid
        }
     
        else {
            //selectedWorkOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }
             

        String selectedStatus = SQLParamFilter.filter(parameters.get("status"));
        if (TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            //do nothing
        }
        else if (TradePortalConstants.STATUS_PENDING.equals(selectedStatus)) {
            sql.append(" and a.transaction_status != '" );
            sql.append(TradePortalConstants.TRANS_STATUS_PAID);
            sql.append("'");
        }
        else if (TradePortalConstants.STATUS_PAID.equals(selectedStatus)) {
            sql.append(" and a.transaction_status = '" );
            sql.append(TradePortalConstants.TRANS_STATUS_PAID);
            sql.append("'");
        }

        String selectedPaySrc = parameters.get("pSrc");
        if (ALL.equals(selectedPaySrc)) {
            //do nothing
        }
        else if (selectedPaySrc != null && !selectedPaySrc.equals("")) {
            sql.append(" and a.payment_source_type = ?"); //selectedPaySrc
        }

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        String selectedWorkOrgOid = parameters.get("work");
        String unencryptedWorkOrgOid =
            EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkOrgOid, getUserSession().getSecretKey());
        if (ALL_WORK.equals(unencryptedWorkOrgOid)) {
            statement.setString(startVarIdx+varCount, this.getOwnerOrgOid());
            varCount++;
        }
        else if (MY_WORK.equals(unencryptedWorkOrgOid)) {
            statement.setString(startVarIdx+varCount, this.getUserOid());
            varCount++;
        }
        else if ( unencryptedWorkOrgOid!=null && unencryptedWorkOrgOid.length()>0 ){
            statement.setString(startVarIdx+varCount, unencryptedWorkOrgOid);
            varCount++;
        }

        String selectedPaySrc = parameters.get("pSrc");
        if (ALL.equals(selectedPaySrc)) {
            //do nothing
        }
        else if (selectedPaySrc != null && !selectedPaySrc.equals("")) {
            statement.setString(startVarIdx+varCount, selectedPaySrc);
            varCount++;
        }

        return varCount;
    }
}
