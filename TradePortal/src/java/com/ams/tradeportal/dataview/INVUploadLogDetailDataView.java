/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Map;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import java.util.List;
/**
 *
 *
 */
public class INVUploadLogDetailDataView
    extends AbstractEJBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(INVUploadLogDetailDataView.class);
	/**
	 * Constructor INV 
	 */
	public INVUploadLogDetailDataView() {
		super();
	}

	@Override
	protected DocumentHandler getDataFromEjbSource(Map<String,String> parameters)throws Exception {
        String xmlStr = parameters.get("xmlStr");
        DocumentHandler docHandler = new DocumentHandler(xmlStr,false);
        List<DocumentHandler> vVector = docHandler.getFragmentsList("/ResultSetRecord");
        
        for(int iLoop=0; iLoop<vVector.size(); iLoop++){
        	DocumentHandler instDoc = vVector.get(iLoop);
        	docHandler.setAttribute("/ResultSetRecord("+iLoop+")/INS_TYP", ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,instDoc.getAttribute("/INS_TYP")));
        	docHandler.setAttribute("/ResultSetRecord("+iLoop+")/TRAN_TYP", ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,instDoc.getAttribute("/TRAN_TYP")));
        }
     
        return docHandler;
    }

}
