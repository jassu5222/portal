/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class INVUploadLogGroupDetailDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(INVUploadLogGroupDetailDataView.class);

    /**
     * Constructor
     */
    public INVUploadLogGroupDetailDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        return " and p_invoice_file_upload_uoid = ? ";
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;
        statement.setString(startVarIdx + varCount, parameters.get("uploadOid"));
        varCount++;
        return varCount;
    }
}
