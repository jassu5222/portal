/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class InstrumentMailDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InstrumentMailDataView.class);

	/**
	 * Constructor
	 */
	public InstrumentMailDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

      
        //InstrumentOid is encrypted for security
        String encryptedInstrumentOid = parameters.get("InstrumentOid");
        if (encryptedInstrumentOid!=null && encryptedInstrumentOid.length()>0) {
            sql.append("and a.a_related_instrument_oid = ?");
        }
        else {
            //instrumentOid is required
            sql.append(" and 1=0");
        }
     


        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;
        
     
        //InstrumentOid is encrypted for security
        String encryptedInstrumentOid = parameters.get("InstrumentOid");        
        if (encryptedInstrumentOid!=null && encryptedInstrumentOid.length()>0) {
            String instrumentOid = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedInstrumentOid, getUserSession().getSecretKey());
            instrumentOid = SQLParamFilter.filterNumber(instrumentOid);

            statement.setString(startVarIdx+varCount, instrumentOid);
            varCount++;
        }
      
        
        return varCount;
    }
}
