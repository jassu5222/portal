/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.JPylonProperties;

/**

 *
 */
public class InstrumentTemplateSearchDataView  extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InstrumentTemplateSearchDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
/**
 * Constructor
 */
public InstrumentTemplateSearchDataView() {
	super();
}

protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
   
   StringBuilder sql = new StringBuilder();
   
   String slc="";
   int dbcount = 0;
    
    String pUserOid = getUserSession().getUserOid();
    String clientBankID = getUserSession().getClientBankOid();
    String bogID = getUserSession().getBogOid();
    String globalID = getUserSession().getGlobalOrgOid();
    String OwnerOrgOid = getUserSession().getOwnerOrgOid();
    String ownershipLevel = getUserSession().getOwnershipLevel();
    String parentOrgID = getUserSession().getOwnerOrgOid();
   
    String  loginRights    = getUserSession().getSecurityRights();
    JPylonProperties         jPylonProperties     = null;
    CorporateOrganization corporateOrg = null;
    
    String confInd  = getConfidentialInd();

   String standbys_use_guar_form_only = parameters.get("standbys_use_guar_form_only");
   String standbys_use_either_form = parameters.get("standbys_use_either_form");
   
   String templateName =  parameters.get("templateName");

   boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
   String subAccessUserOrgID = null;
   if(includeSubAccessUserOrg) {
       subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
   }
  
   if(null != (parameters.get("dbCount")))
	   dbcount = Integer.parseInt(parameters.get("dbCount"));
   
   boolean isAllowSLC=false;
   
   if (dbcount != 0)
   {

    sql.append(" and t.payment_templ_grp_oid in ( select payment_template_group_oid from payment_template_group,user_authorized_template_group " +
	   		"where payment_template_group_oid = a_template_group_oid and p_user_oid= ? ) ");
    sqlParams.add(pUserOid);

   }
   else	   {

	   	sql.append(" and t.p_owner_org_oid in (?,?");
	   	sqlParams.add(parentOrgID);
	   	sqlParams.add(globalID);
	   	if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
	   	  sql.append(",?");
	  	  sqlParams.add(clientBankID);
	   	  if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
	         sql.append(",?");
	         sqlParams.add(bogID);
	   	  }
	   	}
	   	// Also include templates from the user's actual organization if using subsidiary access
	   	if(includeSubAccessUserOrg){
	         sql.append(",?"); 
	         sqlParams.add(subAccessUserOrgID);
	   	}

	   	sql.append(")");
    
	   	if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
	
	   	sql.append(" and i.instrument_type_code in (" );
	   	try {
	   		jPylonProperties = JPylonProperties.getInstance();
			String serverLocation   = jPylonProperties.getString("serverLocation");
		   corporateOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(serverLocation, "CorporateOrganization", Long.parseLong(OwnerOrgOid));
		 isAllowSLC=  corporateOrg.getAttribute("allow_SLC").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.SLC_CREATE_MODIFY);
		   
	   	//air_waybill
		if(corporateOrg.getAttribute("allow_airway_bill").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.AIR_WAYBILL_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.AIR_WAYBILL);
			
		}
		//direct send collection
		if(corporateOrg.getAttribute("allow_export_collection").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.EXPORT_COLL_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.EXPORT_COL);
		}
		//export collection
		if(corporateOrg.getAttribute("allow_new_export_collection").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.NEW_EXPORT_COL);
		}
	   	//guarantee
		if(corporateOrg.getAttribute("allow_guarantee").equals(TradePortalConstants.INDICATOR_YES)
			&& (SecurityAccess.hasRights(loginRights,SecurityAccess.GUAR_CREATE_MODIFY) || SecurityAccess.hasRights(loginRights,SecurityAccess.CC_GUA_CREATE_MODIFY)))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.GUARANTEE);
		}
		//import DLC
		if(corporateOrg.getAttribute("allow_import_DLC").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.DLC_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.IMPORT_DLC);
		}
		//standyby LC
		if(isAllowSLC)
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.STANDBY_LC);
							
			// Based on the indicators from corpOrg, add the necessary "fake"
			// codes or the actual SLC code.
	
		}
		//shipping guarantee
		if(corporateOrg.getAttribute("allow_shipping_guar").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.SHIP_GUAR_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.SHIP_GUAR);
		
		}
		//corporateOrg request
		if(corporateOrg.getAttribute("allow_loan_request").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.LOAN_RQST_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.LOAN_RQST);
		
		}
		//funds transfer
		if((corporateOrg.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES) ||
		    corporateOrg.getAttribute("allow_funds_transfer_panel").equals(TradePortalConstants.INDICATOR_YES))
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.FUNDS_XFER_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.FUNDS_XFER);
		
		}
		//domestic pay
		if((corporateOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES) ||
			corporateOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES))
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.DOMESTIC_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.DOM_PMT);
			
		}
		//request advise
		if(corporateOrg.getAttribute("allow_request_advise").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.REQUEST_ADVISE);
			
		}
		//ATP
		if(corporateOrg.getAttribute("allow_approval_to_pay").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.APPROVAL_TO_PAY);
		}
		// xfer between accts
		if((corporateOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES) ||
			corporateOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES))
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.TRANSFER_CREATE_MODIFY))
		{
			sql.append("?,");
			sqlParams.add(InstrumentType.XFER_BET_ACCTS);
			
		}
		// direct debit
		if(corporateOrg.getAttribute("allow_direct_debit").equals(TradePortalConstants.INDICATOR_YES) &&
				SecurityAccess.hasRights(loginRights,SecurityAccess.DDI_CREATE_MODIFY)) {
			sql.append("?,");
			sqlParams.add(InstrumentType.DIRECT_DEBIT_INSTRUCTION);
		
		}
		
		
		
	   	}
	   	catch (Exception e)
	   	{
	   		e.printStackTrace();
	   	}
	   	finally{
	   		if (sql.charAt(sql.length()-1) == ',')
	   			sql.deleteCharAt(sql.length()-1);
	   		sql.append(")");
	   		
	   		sql.append(slc);
	   					
	   	}
	 
	   	  }
	   }


   if (null != templateName) {
     templateName = templateName.trim().toUpperCase();
     if (!templateName.equals("")) {
       sql.append(" and upper(t.name) like ? ");
       sqlParams.add("%" + templateName+"%");
     }
   } else {
     templateName = "";
   }

   if (!TradePortalConstants.INDICATOR_YES.equals(confInd)){
		sql.append(" and t.confidential_indicator = ?");
		sqlParams.add(TradePortalConstants.INDICATOR_NO);
   }
   
  
  //Make sure of all rights for SLC
   if(isAllowSLC)
		{
    //This means Detailed SLC
	   if (TradePortalConstants.INDICATOR_YES.equals(standbys_use_guar_form_only) && 
			   TradePortalConstants.INDICATOR_NO.equals(standbys_use_either_form)){
		  sql.append(" and ((n.standby_using_guarantee_form = ? and i.instrument_type_code = ?) or not(i.instrument_type_code = ?))");
		  sqlParams.add(TradePortalConstants.INDICATOR_YES);
		  sqlParams.add(InstrumentType.STANDBY_LC);
		  sqlParams.add(InstrumentType.STANDBY_LC);
	   }
	 //This means Simple SLC
	   else if (TradePortalConstants.INDICATOR_NO.equals(standbys_use_guar_form_only) && 
			   TradePortalConstants.INDICATOR_NO.equals(standbys_use_either_form)){
		  sql.append(" and ((n.standby_using_guarantee_form =? and i.instrument_type_code = ?) or not(i.instrument_type_code = ?))");
		  		  sqlParams.add(TradePortalConstants.INDICATOR_NO);
		  		  sqlParams.add(InstrumentType.STANDBY_LC);
		  		  sqlParams.add(InstrumentType.STANDBY_LC);
	   		}
		}

   
 
   return sql.toString();
}

protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
        Map<String,String> parameters)
        throws SQLException {
	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
}
}
