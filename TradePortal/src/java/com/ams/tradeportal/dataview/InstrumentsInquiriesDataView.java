/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;

import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.html.Dropdown;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

import java.util.Map;

/**
 *
 *
 */
public class InstrumentsInquiriesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InstrumentsInquiriesDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    static final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

    /**
     * Constructor
     */
    private final String innerSelect = " select i.instrument_oid as rowKey,i.instrument_oid AS instrument_oid,o.transaction_oid as transaction_oid,i.complete_instrument_id as InstrumentId,"
            + "i.instrument_type_code as InstrumentType,i.instrument_type_code as InstrumentTypeCode, i.bank_instrument_id as BankInstrumentID, o.copy_of_currency_code as CurrencyCode,i.copy_of_instrument_amount as Amount,"
            + "i.instrument_status as Status,i.instrument_status as STATUSFORQVIEW,decode(i.instrument_type_code, 'RFN', null,i.a_related_instrument_oid)  as RelatedInstr,"
            + "p.name as Party,'true' as CHILDREN, i.copy_of_confirmation_ind as Confirmed,i.copy_of_ref_num as ApplicantsRefNo,i.opening_bank_ref_num as OrigBanksRefNo,"
            + "i.vendor_id as VendorId";

    private final String outerSelect = " select rowKey,instrument_oid,transaction_oid,InstrumentId,InstrumentType,InstrumentTypeCode,BankInstrumentID,CurrencyCode,Amount,Status,STATUSFORQVIEW,RelatedInstr,Party,CHILDREN,Confirmed,ApplicantsRefNo,OrigBanksRefNo,VendorId";

    private final String innerSelectChild = " SELECT a.transaction_oid AS rowKey,DECODE(a.transaction_type_code ,'ARM',NULL,'CHM',NULL,a.transaction_oid ) AS transaction_oid,"
            + " b.instrument_oid AS instrument_oid,to_char(a.transaction_status_date, 'DD Mon YYYY') AS InstrumentId,a.transaction_type_code AS InstrumentType,"
            + "b.bank_instrument_id AS BankInstrumentID, DECODE(a.copy_of_currency_code, NULL, active_tran.copy_of_currency_code , a.copy_of_currency_code ) AS CurrencyCode,"
            + "a.copy_of_amount AS Amount, a.transaction_status AS Status,a.transaction_status AS STATUSFORQVIEW,NULL as RelatedInstr, NULL AS Party,'false' AS CHILDREN";

    private final String outerSelectChild = " SELECT rowKey,transaction_oid,instrument_oid,InstrumentId,InstrumentType, BankInstrumentID, CurrencyCode, Amount,Status,STATUSFORQVIEW,RelatedInstr,Party,CHILDREN";

    private final String innerFrom = " from instrument i,transaction o,transaction t,terms_party p";

    private final String innerFromChild = " FROM transaction a,instrument b,transaction active_tran";

    private final String innerWhere = " where i.original_transaction_oid = o.transaction_oid "
            + "and i.active_transaction_oid = t.transaction_oid(+) and "
            + "i.a_counter_party_oid = p.terms_party_oid(+) and i.template_flag='N' and i.instrument_status != 'DEL' and i.instrument_type_code != 'REC'";

    public InstrumentsInquiriesDataView() {
        super();
    }

    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder whereClause = new StringBuilder();
        StringBuilder countClause = new StringBuilder();
        String parentId = SQLParamFilter.filterNumber(parameters.get("parentId"));

        if (StringFunction.isNotBlank(parentId)) {
            whereClause.append(buildDynamicSQLCriteriaChild(parameters));
            countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
        } else {
            whereClause.append(buildDynamicSQLCriteriaParent(parameters));
            countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
        }

        super.fullSQL = whereClause.toString();
        super.fullCountSQL = countClause.toString();
        
        return null;
    }

    protected String buildDynamicSQLCriteriaParentFilter(Map<String, String> parameters, boolean addToParamList) {

        StringBuilder sql = new StringBuilder();

        MediatorServices medService = new MediatorServices();

        //cquinton 3/11/2013 pull secure data from the session
        String userOid = getUserSession().getUserOid();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String loginLocale = getUserSession().getUserLocale();
        String loginRights = getUserSession().getSecurityRights();
        String userSecurityType = getUserSession().getSecurityType();
        String userOwnerShipLevel = getUserSession().getOwnershipLevel();

        String selectedOrg = parameters.get("selectedOrg");
        String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus"));

        boolean filterAllCashMgmtTrans = false;
        BeanManager beanMgr = getBeanManager();
        String confInd = getConfidentialInd();

          // Set the filterAllCashMgmtTrans flag.  This is used to filter out all the cash management transactions or all but International payments.
        // We do not filter the international payments if the corporate organization does not use transfer between accounts and domestic payments.
        CorporateOrganizationWebBean corpOrg =  beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
        corpOrg.getById(getUserSession().getOwnerOrgOid());

        if (corpOrg.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)) {
            boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);
            boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);
            filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        } else {
            boolean allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
            boolean allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);
            filterAllCashMgmtTrans = (allowXferBtwnAccts || allowDomesticPayments) && SecurityAccess.canViewDomesticPaymentOrTransfer(getUserSession().getSecurityRights());
        }

        String searchType = parameters.get("searchType");
        String searchCondition = parameters.get("searchCondition");
        //Search parameters
        String instrumentId = parameters.get("instrumentId");
        String refNum = parameters.get("refNum");
        String bankRefNum = parameters.get("bankRefNum");
        String instrumentType = parameters.get("instrumentType");
        String vendorId = parameters.get("vendorId");
        String otherParty = parameters.get("otherParty");
        String currency = parameters.get("currency");
        String externalBankOid = parameters.get("externalBankOid"); //Kyriba CR 268
        String toDate = parameters.get("toDate");
        String fromDate = parameters.get("fromDate");

        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");

        String startToDate = parameters.get("startDateTo");
        String startFromDate = parameters.get("startDateFrom");
        String bankInstrumentId = parameters.get("bankInstrumentId");
        
      
        String datePattern = null;
        try {
            if (null != parameters.get("dPattern")) {
                datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
            } else {
                datePattern = "";
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            LOG.debug("Exceptiom in DateFormat Decoding : " + e);
        }

        if (instrumentId == null) {
            instrumentId = "";
        }
        if (refNum == null) {
            refNum = "";
        }
        if (bankRefNum == null) {
            bankRefNum = "";
        }
        if (instrumentType == null) {
            instrumentType = "";
        }
        if (vendorId == null) {
            vendorId = "";
        }
        if (otherParty == null) {
            otherParty = "";
        }
        if (currency == null) {
            currency = "";
        }
        if (amountFrom == null) {
            amountFrom = "";
        }
        if (amountTo == null) {
            amountTo = "";
        }
        if (null == externalBankOid) {
            externalBankOid = "";
        } //Kyriba CR 268
        
        if (bankInstrumentId == null) {
        	bankInstrumentId = "";
        }

        Vector statuses = new Vector();

        if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus)) {
            statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
        } else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus)) {
            statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
        } else if (TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
            statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
        } else // default is ALL (actually not all since DELeted instruments never show up (handled by the SQL in the listview XML)
        {
            selectedStatus = "''";
        }

        if (selectedOrg != null && selectedOrg.length() > 0) {
            selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, getUserSession().getSecretKey());
        } else {
            //org is required, build some dummy sql to return nothing
            sql.append(" and 1=0");
        }

        if (ALL_ORGS.equals(selectedOrg)) {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("select organization_oid, name");
            sqlQuery.append(" from corporate_org");
            sqlQuery.append(" where activation_status = ? start with organization_oid = ? ");
            sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
            sqlQuery.append(" order by ");
            sqlQuery.append(getResourceManager().localizeOrderBy("name"));

            DocumentHandler hierarchyDoc = null;
            try {
                hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
            } catch (AmsException e) {
                LOG.debug("Exception caught in InstrumentsInquiriesDataView.java while calling DatabaseQueryBean.getXmlResultSet(). This exception is cirrently not caught.");
            }

            StringBuilder orgList = new StringBuilder();
            List<DocumentHandler> orgListDoc = hierarchyDoc.getFragmentsList("/ResultSetRecord");
            int totalOrganizations = orgListDoc.size();

            DocumentHandler orgDoc = null;
            for (int i = 0; i < totalOrganizations; i++) {
                orgDoc = orgListDoc.get(i);
                orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
                if (i < (totalOrganizations - 1)) {
                    orgList.append(", ");
                }
            }
            // Build a comma separated list of the orgs
            sql.append(" and i.a_corp_org_oid in (" + prepareDynamicSqlStrForInClause(orgList.toString(), sqlParams, addToParamList) + ")");
        } else {
            sql.append(" and i.a_corp_org_oid = ? ");
            if (addToParamList) {
                sqlParams.add(selectedOrg);
            }
        }

        if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) || TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) || TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            StringBuilder statusStr = new StringBuilder("");
            int numStatuses = statuses.size();
            for (int i = 0; i < numStatuses; i++) {
                statusStr.append((String) statuses.elementAt(i));

                if (i < numStatuses - 1) {
                    statusStr.append(", ");
                }
            }

            sql.append(" and i.instrument_status in (");
            sql.append(prepareDynamicSqlStrForInClause(statusStr.toString(), sqlParams, addToParamList));
            sql.append(") ");
        } else {
            sql.append(" and i.instrument_status in ('') ");
        }

        // Add additional where clause to remove Cash Management transactions if necessary.
        if (filterAllCashMgmtTrans) {
            sql.append(" and i.instrument_type_code not in('FTBA','FTDP','FTRQ','DDI', 'PYB')");   //SRUK011182777 - added 'DDI' transactions exclusion
        } else {
            sql.append(" and i.instrument_type_code not in('FTBA','FTDP','DDI','PYB')");         //SRUK011182777 - added 'DDI' transactions exclusion
        }

        if (TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(confInd)) //DK IR T36000024537 Rel8.4 02/04/2014
        {
            sql.append(" AND (i.confidential_indicator  = 'N' OR i.confidential_indicator IS NULL)");
        }

        /**
         * Adding search conditions
         */
        Vector instrumentTypes = null;
        try {
            fetchInstrumentTypes(searchCondition, userOrgOid, userOid, loginRights, userSecurityType, userOwnerShipLevel);
        } catch (ServletException se) {
            se.printStackTrace();
        }
        
      //TODO looks like this condition never happens as searchCondition will always be ALL_INSTRUMENTS- WE can remove this whole block with searchCondition criteria
        if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE)) {
            //TODO instrumentType = InstrumentType.EXPORT_DLC;
            sql.append(" and i.instrument_type_code='");
            sql.append(InstrumentType.EXPORT_DLC);
            sql.append("' and i.instrument_oid in ");
            sql.append("(select p_instrument_oid from transaction where ");
            sql.append("p_instrument_oid = i.instrument_oid and ");
            sql.append("transaction_type_code = '");
            sql.append(TransactionType.ADVISE);
            sql.append("')");
        } else if (!searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS)) {
        	
            StringBuilder instrumentTypeSql = new StringBuilder();
            int vectorSize = instrumentTypes!=null?instrumentTypes.size():0;
            if (vectorSize == 0) {
             //CM security setup could prevent the user from having access to any instrument types,
                //so SQL statement needs an empty string
                instrumentTypeSql.append("''");
            }

            for (int i = 0; i < vectorSize; i++) {
                instrumentTypeSql.append((String) instrumentTypes.elementAt(i));
                if (i < vectorSize - 1) {
                    instrumentTypeSql.append(", ");
                }
            }

            sql.append(" and i.instrument_type_code in (");
            sql.append(prepareDynamicSqlStrForInClause(instrumentTypeSql.toString(), sqlParams, addToParamList));
            sql.append(")");

        }

        // if to create new transaction for an existing instrument,
        // instrument must have a status of Active
        if (searchCondition.equals(TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION)) {
            sql.append(" and i.instrument_status <> '");
            sql.append(TradePortalConstants.INSTR_STATUS_PENDING);
            sql.append("' and i.instrument_status <> '");
            sql.append(TradePortalConstants.INSTR_STATUS_DELETED);
            sql.append("' and i.instrument_status <> '");
            sql.append(TradePortalConstants.INSTR_STATUS_CANCELLED);
            sql.append("'");
        }

        if (searchCondition.equals(TradePortalConstants.IMPORTS_ONLY)) {
            sql.append(" and i.instrument_type_code in ('");
            sql.append(InstrumentType.IMPORT_DLC);
            sql.append("','");
            sql.append(InstrumentType.STANDBY_LC);
            sql.append("','");
            sql.append(InstrumentType.IMPORT_COL);
            sql.append("') ");
        }

        // If we are searching for an instrument to copy from, only those with customer
        // enetered terms should appear
        if (searchCondition.equals(TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT)) {
            sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
        }

        if (searchType.equals(TradePortalConstants.ADVANCED)) {
        	if (StringFunction.isNotBlank(fromDate)) {
            	boolean isValidFromDate = isValidDateFormat(datePattern, fromDate);
            	
            	if(isValidFromDate){
            		sql.append(" and i.copy_of_expiry_date >= TO_DATE(?,'" + datePattern + "')");
                    if (addToParamList) {
                        sqlParams.add(fromDate);
                    }
            	}else{
            		sql.append(" and 1 != 1 ");
            	}
            }
        	
        	if (StringFunction.isNotBlank(toDate)) {
            	boolean isValidToDate = isValidDateFormat(datePattern, toDate);
            	
            	if(isValidToDate){
            		sql.append(" and i.copy_of_expiry_date <= TO_DATE(?,'" + datePattern + "')");
                    if (addToParamList) {
                        sqlParams.add(toDate);
                    }
            	}else{
            		sql.append(" and 1 != 1 ");
            	}
            }
        
        	//IR-45903-Start
        	if (StringFunction.isNotBlank(startFromDate)) {
            	boolean isValidStartFromDate = isValidDateFormat(datePattern, startFromDate);
            	
            	if(isValidStartFromDate){	
                   sql.append(" and i.issue_date >= TO_DATE(?,'" + datePattern + "')");
             if (addToParamList) {
                 sqlParams.add(startFromDate);
             }  }else{
            	 sql.append(" and 1 != 1 ");
            	 }
             
            }

        	if (StringFunction.isNotBlank(startToDate)) {
        		boolean isValidStartToDate = isValidDateFormat(datePattern, startToDate);
        		if(isValidStartToDate){
             sql.append(" and i.issue_date <= TO_DATE(?,'" + datePattern + "')");
             if (addToParamList) {
                 sqlParams.add(startToDate);
             }}else{
            	 sql.append(" and 1 != 1 ");
            	 }
           }
      
        	//IR-45903-End
            if (instrumentType != null && !instrumentType.trim().equals("")) {
                sql.append(" and i.instrument_type_code=?");
                if (addToParamList) {
                    sqlParams.add(instrumentType);
                }
            }

            if (otherParty != null) {
                if (!otherParty.equals("")) {
                    sql.append(" and upper(p.name) like upper(?)");
                    if (addToParamList) {
                        sqlParams.add("%" + otherParty.toUpperCase() + "%");
                    }
                }
            }

            if (currency != null) {
                if (!currency.equals("")) {
                    sql.append(" and o.copy_of_currency_code=?");
                    if (addToParamList) {
                        sqlParams.add(currency);
                    }
                }
            }
            if (StringFunction.isNotBlank(amountFrom)) {
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountFrom, loginLocale);
                    if (!amount.equals("")) {
                        sql.append(" and i.copy_of_instrument_amount >= ? ");
                        if (addToParamList) {
                            sqlParams.add(amount);
                        }
                    }else{
                    	sql.append(" and 1 != 1 ");
                    }
                } catch (InvalidAttributeValueException e) {
                    try {
                        medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CURRENCY_FORMAT, amountFrom);
                        sql.append(" and 1 != 1 ");
                    } catch (AmsException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            if (StringFunction.isNotBlank(amountTo)) {
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountTo, loginLocale);
                    if (!amount.equals("")) {
                        sql.append(" and i.copy_of_instrument_amount <=? ");
                        if (addToParamList) {
                            sqlParams.add(amount);
                        }
                    }else{
                    	sql.append(" and 1 != 1 ");
                    }
                } catch (InvalidAttributeValueException e) {
                    try {
                        medService.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CURRENCY_FORMAT, amountFrom);
                        sql.append(" and 1 != 1 ");
                    } catch (AmsException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            if (vendorId != null) {
                if (!vendorId.equals("")) {
                    sql.append("and upper(i.vendor_id) like ? ");
                    if (addToParamList) {
                        sqlParams.add("%" + vendorId + "%");
                    }
                }
            }
            //Kyriba CR 268 - Add external Bank oid
            if (null != externalBankOid) {
                if (!"".equals(externalBankOid)) {
                    sql.append(" and i.A_OP_BANK_ORG_OID=?");
                    if (addToParamList) {
                        sqlParams.add(externalBankOid);
                    }
                }
            }
        } else {

            // Unlike the ADVANCED search, the SIMPLE search values are ANDed together.
            // To handle this we'll add an 'AND (simple conditions ored together)'
            // clause.  This is only added if at least one of the search fields has a
            // value.
            if (!instrumentId.equals("") || !instrumentType.equals("")
                    || !refNum.equals("") || !bankRefNum.equals("") || !vendorId.equals("")|| !"".equals(bankInstrumentId)) {
                // At least one search field has a value.

                boolean addAnd = false;
                sql.append(" and (");

                if (!instrumentId.equals("")) {

                    sql.append("upper(i.complete_instrument_id) like ? "); // WebLogic 6.1 SP6 does not treat jsp:param as URLEncoded any more.
                    addAnd = true;
                    if (addToParamList) {
                        sqlParams.add("%" + instrumentId.toUpperCase() + "%");
                    }
                }
                if (!refNum.equals("")) {
                    if (addAnd) {
                        sql.append(" and ");
                    }
                    sql.append("upper(i.copy_of_ref_num) like ? ");
                    if (addToParamList) {
                        sqlParams.add("%" +refNum.toUpperCase() + "%");
                    }
                    addAnd = true;
                }
                if (!bankRefNum.equals("")) {
                    if (addAnd) {
                        sql.append(" and ");
                    }
                    sql.append("upper(i.opening_bank_ref_num) like ? "); //RPasupulati IR T36000037888 removing extra braces
                    if (addToParamList) {
                        sqlParams.add("%" +bankRefNum.toUpperCase() + "%");
                    }
                    addAnd = true;
                }
                if (!instrumentType.equals("")) {
                    if (addAnd) {
                        sql.append(" and ");
                    }
                 // if this search originates from Export DLC transaction page,
                    // ALWAYS filter based on instrument type Export LC that originate from
                    // advise
                    sql.append("i.instrument_type_code=?");
                    if (addToParamList) {
                        sqlParams.add(instrumentType);
                    }
                    addAnd = true;
                }
                if (!vendorId.equals("")) {
                    if (addAnd) {
                        sql.append(" and ");
                    }
                    sql.append("upper(i.vendor_id) like ? ");
                    if (addToParamList) {
                        sqlParams.add("%" + vendorId.toUpperCase() + "%");
                    }
                    addAnd = true;
                }
               
                if(!"".equals(bankInstrumentId)){
                	if (addAnd) {
                		sql.append(" and ");
                	}
                	sql.append("upper(i.bank_instrument_id) like ? ");                  
                	if (addToParamList) {
                		sqlParams.add( "%"+bankInstrumentId.toUpperCase() + "%");
                	}
                	addAnd = true;
                }
                sql.append(")");

            }  // end if-else (ADVANCED vs BASIC)
        }
      
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

    public Vector fetchInstrumentTypes(String searchCondition, String userOrgOid, String userOid, String loginRights, String userSecurityType, String userOwnerShipLevel) throws ServletException {

        BeanManager beanMgr = new BeanManager();
        FormManager formMgr = new FormManager();
        CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
        corpOrg.getById(userOrgOid);
        Vector instrumentTypes = null;
    	  		// Determine the instrument type code(s) for display and search criteria there are three options: display all instrument types,
        // display only instrument types that can be created and display only the Export LC instrument

        if (searchCondition.equals(TradePortalConstants.SEARCH_ALL_INSTRUMENTS)) {
            instrumentTypes = Dropdown.getTradeInstrumentTypes();
        } else if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.EXPORT_DLC);
        } else if (searchCondition.equals(TradePortalConstants.ALL_EXPORT_DLC)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.EXPORT_DLC);
        } else if (searchCondition.equals(InstrumentType.EXPORT_COL)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.EXPORT_COL);
        } else if (searchCondition.equals(TradePortalConstants.IMPORT)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.IMPORT_DLC);
            instrumentTypes.addElement(InstrumentType.SHIP_GUAR);
            instrumentTypes.addElement(InstrumentType.IMPORT_COL);
            instrumentTypes.addElement(InstrumentType.AIR_WAYBILL);
            instrumentTypes.addElement(InstrumentType.APPROVAL_TO_PAY);
        } else if (searchCondition.equals(InstrumentType.LOAN_RQST)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.LOAN_RQST);
        } else if (searchCondition.equals(TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT)
                || searchCondition.equals(TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION)) {
            Long organization_oid = Long.valueOf(userOrgOid);
            Long userOID = Long.valueOf(userOid);
            instrumentTypes = Dropdown.getCreateModifyInstrumentTypes(formMgr, loginRights, organization_oid.longValue(), userSecurityType, userOwnerShipLevel, userOID.longValue());
        } else if (searchCondition.equals(TradePortalConstants.IMPORTS_ONLY)) {
            instrumentTypes = new Vector();
            instrumentTypes.addElement(InstrumentType.IMPORT_DLC);
            instrumentTypes.addElement(InstrumentType.STANDBY_LC);
            instrumentTypes.addElement(InstrumentType.IMPORT_COL);
        } else {
            instrumentTypes = Dropdown.getTradeInstrumentTypes(); // IR NGUJ013070808
        }

        return instrumentTypes;
    }

    private String buildDynamicSQLCriteriaParent(Map<String, String> parameters) {

        String sort = SQLParamFilter.filter(parameters.get("sort"));
        StringBuilder basicquery = new StringBuilder();

        basicquery.append(outerSelect).append(" from (");
        basicquery.append(outerSelect).append(" ,rownum row_num from ( ");

        /*KMehta IR T36000015673 Start */
        //Added for sorting in Grid
        if (sort != null && !sort.equals("")) {
            if (sort.contains("InstrumentType")) {

                basicquery.append(innerSelect).append(", refdata.descr AS RefDataDescr");
                basicquery.append(innerFrom).append(",refdata");
                basicquery.append(innerWhere);
                basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, true));

                basicquery.append(buildSortQuery(parameters));
                if (sort.startsWith("-")) {
                    sort = sort.substring(1);
                    basicquery.append(" desc ");
                } else {
                    basicquery.append(" asc ");
                }
            } else {
                basicquery.append(innerSelect).append(innerFrom).append(innerWhere);
                basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, true));
                if (sort.startsWith("-")) {
                    sort = sort.substring(1);
					//Srinivasu_D IR#T36000042039 Rel9.3 07/25/2015 - allowed to sort on inputs
					sort =  SQLParamFilter.filter(sort);
                   // sort = "'" + SQLParamFilter.filter(sort) + "'";
				   //Srinivasu_D IR#T36000042039 Rel9.3 07/25/2015 - End
                    basicquery.append(" order by ").append(sort).append(" desc ");
                } else {
                    basicquery.append(" order by ").append(sort).append(" asc ");
                }
            }
        }
        /*KMehta IR T36000015673 End */
        basicquery.append(" ) ) where row_num > ? ").append(" and row_num <= ? ");
        return basicquery.toString();
    }

    public String buildSortQuery(Map<String, String> parameters) {
        StringBuilder sql = new StringBuilder();
        
        sql.append("and (i.instrument_type_code = refdata.code ");
        sql.append(" and refdata.table_type ='");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        // Try to join in by the user's locale
        // If description does not exist for that locale, use the null default locale data

        sql.append("' AND ((refdata.locale_name = '");
        sql.append(getResourceManager().getResourceLocale());
        sql.append("') OR");
        sql.append("  ((refdata.locale_name is null)) and"); //PCUTRONE-(IR-GNUG100258356)[18-SEP-07]-ADD PARENTHESIS IN "  ((refdata.locale_name is null) and"

        sql.append("   ((( select count(*)");
        sql.append("     from refdata rrr");
        sql.append("     where rrr.code=refdata.code and ");
        sql.append("           rrr.table_type = '");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        sql.append("' and ");
        sql.append("           rrr.locale_name = '");
        sql.append(getResourceManager().getResourceLocale());
        sql.append("') = 0) ");

        sql.append("or (select count(*)");
        sql.append("     from refdata rrr");
        sql.append("     where rrr.code=refdata.code and ");
        sql.append("           rrr.table_type = '");
        sql.append(TradePortalConstants.INSTRUMENT_TYPE);
        sql.append("' and ");
        sql.append("           rrr.locale_name is null");
        sql.append(") = 0))) ");

        sql.append(" order by ");
        sql.append(getResourceManager().localizeOrderBy("upper(RefDataDescr)"));
        return sql.toString();
    }

    private String buildDynamicSQLCriteriaParentCount(Map<String, String> parameters) {

        StringBuilder basicquery = new StringBuilder();
        basicquery.append("select count(*) from( ");
        basicquery.append(innerSelect).append(" ,rownum as row_num").append(innerFrom).append(innerWhere);
        basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, false));
        basicquery.append(" ) ");

        return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChild(Map<String, String> parameters) {

        String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
        String innerWhereChild = " WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND "
                + "a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND "
                + "a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE "
                + "a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ? )";

        sqlParams.add(parentId);
        sqlParams.add(parentId);

        StringBuilder basicquery = new StringBuilder();
        basicquery.append(outerSelectChild).append(" from (");
        basicquery.append(innerSelectChild).append(" ,rownum as row_num").append(innerFromChild).append(innerWhereChild);
        basicquery.append(" ) where row_num > ? ").append(" and row_num <= ? ");

        return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChildCount(Map<String, String> parameters) {

        //String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());

        String innerWhereChild = " WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND "
                + "a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND "
                + "a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE "
                + "a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ? )";

        StringBuilder basicquery = new StringBuilder();
        basicquery.append("select count(*) from( ");
        basicquery.append(innerSelectChild).append(" ,rownum as row_num").append(innerFromChild).append(innerWhereChild);
        basicquery.append(" ) ");

        return basicquery.toString();
    }

    @Override
    protected boolean ignorePassedInParam(Map<String,String> parameters) {
        return StringFunction.isNotBlank( parameters.get("parentId"));

    }

}
