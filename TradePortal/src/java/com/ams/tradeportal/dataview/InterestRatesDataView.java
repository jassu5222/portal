/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class InterestRatesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InterestRatesDataView.class);
	private static final String mainQuery = "select r.currency_code as Currency_Code, r.rate_type as RateType, r.call_rate as CallRate, "
			+ "r.rate_30_days as Rate30Days, r.rate_60_days as Rate60Days,r.rate_90_days as Rate90Days, r.rate_120_days as Rate120Days,"
			+ " r.rate_180_days as Rate180Days,  r.rate_360_days as Rate360Days,  "
			+ " r.effective_date as EffectiveDate %refDataDescColumn% from (select currency_code, rate_type, p_interest_disc_rate_group_oid,"
			+ " MAX(effective_date) as effective_date from interest_discount_rate  where p_interest_disc_rate_group_oid = ? ";
	String refDataDescColumn = "";
	String refdataTable = "";

	/**
	 * Constructor
	 */
	public InterestRatesDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

		StringBuilder sql = new StringBuilder();
		StringBuilder sqlJoin = new StringBuilder();
		StringBuilder sqlRefData = new StringBuilder();



		sqlJoin.append(" group by currency_code, rate_type, p_interest_disc_rate_group_oid) x  %refdataTable%");
		sqlJoin.append(" ,interest_discount_rate r");
		sqlJoin.append(" where x.currency_code = r.currency_code");
		sqlJoin.append(" and x.rate_type = r.rate_type");
		sqlJoin.append(" and x.effective_date = r.effective_date");
		sqlJoin.append(" and x.p_interest_disc_rate_group_oid = r.p_interest_disc_rate_group_oid ");

		String sortColumn = parameters.get("sort");
		boolean sortAsc = true;
		if (sortColumn.indexOf("-") == -1) {
			sortAsc = false;
		}
		sortColumn = sortColumn.replace("-", "");

		if ("Currency_Code".equals(sortColumn)) {
			sqlRefData.append("AND (r.currency_code = refdata.code AND    	refdata.table_type ='CURRENCY_CODE' AND ((refdata.locale_name = '"
							+ getResourceManager().getResourceLocale()+ "') OR  ((refdata.locale_name is null)) and ");
			sqlRefData.append("((( select count(*)     from refdata rrr     where rrr.code=refdata.code and            rrr.table_type = 'CURRENCY_CODE' and  ");
			sqlRefData.append("rrr.locale_name = '"+ getResourceManager().getResourceLocale()+ "') = 0) or (select count(*)     from refdata rrr     where rrr.code=refdata.code and   ");
			sqlRefData.append("rrr.table_type = 'CURRENCY_CODE' and    	rrr.locale_name is null) = 0))) ");
			refDataDescColumn = ", refdata.descr as RefDataDescr ";
			refdataTable = ", refdata ";
		}

		if ("RateType".equals(sortColumn)) {
			sqlRefData.append("AND (r.rate_type = refdata.code AND refdata.table_type ='REFINANCE_RATE_TYPE' AND ((refdata.locale_name = '"
							+ getResourceManager().getResourceLocale()+ "') OR ((refdata.locale_name is null)) and ");
			sqlRefData.append("((( select count(*)     from refdata rrr     where rrr.code=refdata.code and    				rrr.table_type = 'REFINANCE_RATE_TYPE' and ");
			sqlRefData.append("rrr.locale_name = '"+ getResourceManager().getResourceLocale()
							+ "') = 0) or (select count(*)     from refdata rrr     where rrr.code=refdata.code and   ");
			sqlRefData.append("rrr.table_type = 'REFINANCE_RATE_TYPE' and    	rrr.locale_name is null) = 0))) ");
			refDataDescColumn = ", refdata.descr as RefDataDescr ";
			refdataTable = ", refdata ";
		}



		sql.append("select Currency_Code, RateType, CallRate, Rate30Days, Rate60Days, Rate90Days, Rate120Days, Rate180Days, Rate360Days, EffectiveDate from ");
		sql.append("(select Currency_Code, RateType, CallRate, Rate30Days, Rate60Days, Rate90Days, Rate120Days, Rate180Days, Rate360Days, EffectiveDate, rownum row_num from(");
		sql.append(mainQuery);
		sql.append(sqlJoin.toString());
		sql.append(sqlRefData.toString());

		if ("Currency_Code".equals(sortColumn) || "RateType".equals(sortColumn)) {
			sql.append(" order by ");
			sql.append(getResourceManager().localizeOrderBy("upper(RefDataDescr)"));
		} else {
			sql.append(" order by ");
			sql.append(SQLParamFilter.validateDBColumns(sortColumn, new String[]{"Currency_Code", "RateType", "CallRate", "Rate30Days", "Rate60Days", "Rate90Days", "Rate120Days","Rate180Days", "Rate360Days", "EffectiveDate"}));
		}

		if (!sortAsc) {
			sql.append(" DESC");
		}

		sql.append(" ) )");
		sql.append("where row_num > ? and row_num <= ?");

		fullSQL = sql.toString();

		fullSQL = fullSQL.replaceAll("%refDataDescColumn%", refDataDescColumn);
		fullSQL = fullSQL.replaceAll("%refdataTable%", refdataTable);

		getCountSql(parameters);

		return fullSQL;
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		int varCount = 0;
		//jgadela 02/24/2015 - T36000036839 - Fixed Sensitive Data Disclosure issue
		String groupOid=EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("groupOid"), getUserSession().getSecretKey());
		statement.setString(startVarIdx+varCount, groupOid);
        varCount++;
		return varCount;
	}

	protected void getCountSql(Map<String,String> parameters) {
		StringBuilder countSql = new StringBuilder();
		
		countSql.append("select count(*) as countfrom (select currency_code, rate_type, call_rate, p_interest_disc_rate_group_oid, ");
		countSql.append("rate_30_days, rate_60_days, rate_90_days,rate_120_days, rate_180_days ,rate_360_days, MAX(effective_date) as effective_date ");
		countSql.append("from interest_discount_rate  where p_interest_disc_rate_group_oid = ? ");
		countSql.append("   group by currency_code, rate_type,call_rate, p_interest_disc_rate_group_oid, rate_30_days, rate_60_days, rate_90_days,rate_120_days, rate_180_days ,rate_360_days) ");
		fullCountSQL = countSql.toString();
	}
}
