/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

/**
 * 
 *
 */
public class InvoiceDefinitionDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceDefinitionDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();

	/**
	 * Constructor
	 */
	public InvoiceDefinitionDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();


         String parentOrgID = getUserSession().getOwnerOrgOid();
         String includeSubAccessUserOrg = null;
         String subAccessUserOrgID = null;
         if(getUserSession().showOrgDataUnderSubAccess()) {
            includeSubAccessUserOrg = "Y";
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
         }

          // Build the dynamic part of the where clause.  Only show those
	      // rules for the user's corporate org.
         sql.append("i.a_owner_org_oid in (?");
         sqlParams.add(parentOrgID);
	     // Possibly include the subsidiary access user org's data also
	      if(includeSubAccessUserOrg!=null && "Y".equals(includeSubAccessUserOrg)){
	    	  sql.append(",?");
		      sqlParams.add(subAccessUserOrgID);
	      }
	      sql.append(",?");
	      sqlParams.add(subAccessUserOrgID);
	      sql.append( ")");

		  return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
