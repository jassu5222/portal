/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class InvoiceGroupSummaryDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceGroupSummaryDataView.class);

    /**
     * Constructor
     */
    public InvoiceGroupSummaryDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        String userOrgOid = getUserSession().getOwnerOrgOid();

        StringBuilder dynamicWhereClause = new StringBuilder();
        dynamicWhereClause.append(" ").append(userOrgOid);
        dynamicWhereClause.append(" ) GROUP BY Ccy ");
        return dynamicWhereClause.toString();

    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
