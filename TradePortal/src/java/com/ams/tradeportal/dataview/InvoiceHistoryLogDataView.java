/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.amsinc.ecsg.util.EncryptDecrypt;


/**

 *
 */
public class InvoiceHistoryLogDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceHistoryLogDataView.class);
	/**
	 * Constructor
	 */
	public InvoiceHistoryLogDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		
		return "and a.p_upload_invoice_oid = ? " ;

	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
					throws SQLException {
		int varCount = 0;
		//MEer IR-36419 Transaction Log issue 
		String uploadInvoiceOid = parameters.get("upload_invoice_oid");		
		String unencryptedInvoiceOid =	EncryptDecrypt.decryptStringUsingTripleDes(uploadInvoiceOid, getUserSession().getSecretKey());         
		statement.setString(startVarIdx+varCount, unencryptedInvoiceOid);
		varCount++;

		return varCount;

	}
	
	
}

