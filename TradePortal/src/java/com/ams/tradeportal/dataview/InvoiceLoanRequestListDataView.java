/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import java.util.HashMap;

/**
 *
 *
 */
public class InvoiceLoanRequestListDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceLoanRequestListDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    /**
     * Constructor
     */
    public InvoiceLoanRequestListDataView() {
        super();
    }

    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();
        String userOrgOid = parameters.get("userOrgOid");
        String selectedStatus = parameters.get("selectedStatus");
        String invoiceGroupOid = parameters.get("invoiceGroupOid");

        if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            dynamicWhereClause.append(" and i.invoice_status = ? ");
            sqlParams.add(selectedStatus);
        }
        dynamicWhereClause.append(" and i.a_corp_org_oid = ? ");
        sqlParams.add(userOrgOid);
        dynamicWhereClause.append(" and i.a_invoice_group_oid = ? ");
        sqlParams.add(invoiceGroupOid);

        HashMap map = new HashMap();
        map.put("InvoiceLoanRequestListDataView", dynamicWhereClause.toString());
        map.put("params", parameters);
        map.put("paramsDynamicSqlPlaceHolder", sqlParams);
        InvoiceDataViewUtil.getMoreSQL().setMap(map);
        return dynamicWhereClause.toString();

    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
