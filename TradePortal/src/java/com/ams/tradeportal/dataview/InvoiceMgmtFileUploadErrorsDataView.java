/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class InvoiceMgmtFileUploadErrorsDataView    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceMgmtFileUploadErrorsDataView.class);
	/**
	 * Constructor
	 */
	public InvoiceMgmtFileUploadErrorsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        return " and p_invoice_file_upload_uoid = ? ";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    		int varCount = 0;
    	    statement.setString(startVarIdx+varCount, parameters.get("uploadOid"));
    	    varCount++;
    	    return varCount;
    }
}
