/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.SQLParamFilter;

/**
 *
 *
 */
public class InvoicePendingActionDataView extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoicePendingActionDataView.class);
	/**
	 * Constructor
	 */
	public InvoicePendingActionDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();
    	String userOrgOid = getUserSession().getOwnerOrgOid();
    	String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus"));

        if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus)) {
            dynamicWhereClause.append(" and i.invoice_status = '");
            dynamicWhereClause.append(selectedStatus).append("' ");
         }
         dynamicWhereClause.append(" and i.a_corp_org_oid = ").append(userOrgOid);

        return dynamicWhereClause.toString();

    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;



        return varCount;
    }
}
