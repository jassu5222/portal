package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class InvoicesForCreditNoteUnApplyDataView   extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(InvoicesForCreditNoteUnApplyDataView.class);

	
	public InvoicesForCreditNoteUnApplyDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        return " and r.a_upload_credit_note_oid = ? ";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	int varCount = 0;
		statement.setString(startVarIdx+varCount, parameters.get("creditNoteOid"));
        varCount++;
		return varCount;
    }
}
 


