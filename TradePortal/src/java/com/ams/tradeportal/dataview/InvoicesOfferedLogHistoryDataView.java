/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class InvoicesOfferedLogHistoryDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesOfferedLogHistoryDataView.class);

	/**
	 * Constructor
	 */
	public InvoicesOfferedLogHistoryDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        return " AND ih.p_upload_invoice_oid = ? ";
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	int varCount = 0;
    	String invoiceOid = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("invoiceOid"), getUserSession().getSecretKey());
	    statement.setString(startVarIdx+varCount, invoiceOid);
	    varCount++;
	    return varCount;
    }
}
