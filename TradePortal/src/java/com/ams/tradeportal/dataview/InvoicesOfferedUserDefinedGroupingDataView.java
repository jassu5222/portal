/**
 * Ravindra - Rel8200 CR-708B - Initial Version
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class InvoicesOfferedUserDefinedGroupingDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(InvoicesOfferedUserDefinedGroupingDataView.class);

    private final List<Object> sqlParams = new ArrayList<>();

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();
        

        String invoiceOidList = parameters.get("InvoiceOidList");
        if (StringFunction.isNotBlank(invoiceOidList)) {
            invoiceOidList = EncryptDecrypt.decryptStringUsingTripleDes(invoiceOidList, getUserSession().getSecretKey());
        }

        if (StringFunction.isNotBlank(invoiceOidList)) {
            invoiceOidList = invoiceOidList.replaceAll("'", "");
            dynamicWhereClause.append(" and invoice_oid in ( ").append(prepareDynamicSqlStrForInClause(invoiceOidList, sqlParams)).append(" )");
        } else {
            dynamicWhereClause.append(" and 1 = 0");
        }
        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

}
