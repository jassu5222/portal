/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class MatchInvoicesDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(MatchInvoicesDataView.class);

    /**
     * Constructor
     */
    public final static String INVOICE_SEARCH_STATUS_CLOSED = "CLOSED";

    public MatchInvoicesDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();

        String userOrgOid = SQLParamFilter.filterNumber(parameters.get("userOrgOid"));

        /*Params for search*/
        String invoiceID = SQLParamFilter.filter(parameters.get("invoiceID"));
        String finance = SQLParamFilter.filter(parameters.get("finance"));

        String dateType = SQLParamFilter.filter(parameters.get("dateType"));
        String fromDate = SQLParamFilter.filter(parameters.get("fromDate"));
        String toDate = SQLParamFilter.filter(parameters.get("toDate"));

        String amountType = SQLParamFilter.filter(parameters.get("amountType"));
        String amountFrom = SQLParamFilter.filterNumber(parameters.get("amountFrom"));
        String amountTo = SQLParamFilter.filterNumber(parameters.get("amountTo"));
        String partyFilterText = SQLParamFilter.filter(parameters.get("partyFilterText"));
        String poNumber = SQLParamFilter.filter(parameters.get("poNumber"));
        String loginLocale = SQLParamFilter.filter(parameters.get("loginLocale"));
        String currency = SQLParamFilter.filter(parameters.get("currency"));
        //R 9.3 T36000034280 Rpasupulati adding datePattern start
        String datePattern = null;
        try {
            if (null != parameters.get("dPattern")) {
                datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
            } else {
                datePattern = "";
            }
        } catch (UnsupportedEncodingException e) {
            LOG.debug("Exceptiom in DateFormat Decoding : " + e);
        }

      //R 9.3 T36000034280 Rpasupulati adding datePattern end
        dynamicWhereClause.append(" and ");

        if (currency != null && !currency.equals("")) {
            dynamicWhereClause.append(" i.currency_code = '");
            dynamicWhereClause.append(currency);
            dynamicWhereClause.append("' ");
            dynamicWhereClause.append(" and ");
        }

        dynamicWhereClause.append(" i.a_corp_org_oid = ");
        dynamicWhereClause.append(userOrgOid);

        /*Start comment*/
        // Search by Finance
        if (finance != null && !finance.equals("")) {
            if (TradePortalConstants.INDICATOR_YES.equals(finance)) {
                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(" finance_status in ('FIN','AFF') ");// PPRAKASH IR ALUJ031649735

            }

        }

        // Search by the invoice ID
        if (invoiceID != null && !invoiceID.equals("")) {
            dynamicWhereClause.append(" and ");
            dynamicWhereClause.append(" upper(invoice_reference_id) like '%");
            dynamicWhereClause.append(invoiceID);
            dynamicWhereClause.append("%'");

        }

           // Search by date type
        if (dateType != null && !dateType.equals("")) {
            String expandedDateType = "";
            if (dateType.equals("DDT")) {
                expandedDateType = "invoice_due_date";
            } else if (dateType.equals("IDT")) {
                expandedDateType = "invoice_issue_datetime";
            } else {
                expandedDateType = "";
            }

            if (fromDate != null && !fromDate.equals("")) {
                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(expandedDateType + " >= TO_DATE('");
                dynamicWhereClause.append(fromDate);
                dynamicWhereClause.append("','" + datePattern + "')");//R 9.3 T36000034280 Rpasupulati adding datePattern
            }

            if (toDate != null && !toDate.equals("")) {
                dynamicWhereClause.append(" and ");
                dynamicWhereClause.append(expandedDateType + " <= TO_DATE('");
                dynamicWhereClause.append(toDate);
                dynamicWhereClause.append("','" + datePattern + "')");//R 9.3 T36000034280 Rpasupulati adding datePattern

            }

        }

        // Search by Amount
        if (amountType != null && !StringFunction.isBlank(amountType)) {
            String expandedAmountType = "";
            if (amountType.equals("INA")) {
                expandedAmountType = "invoice_total_amount";
            } else if (amountType.equals("DSA")) {
                expandedAmountType = "invoice_discount_amount";
            } else if (amountType.equals("CNA")) {
                expandedAmountType = "invoice_credit_note_amount";
            } else if (amountType.equals("FNA")) {
                expandedAmountType = "finance_amount";
            } else {
                expandedAmountType = "";
            }
            if (amountFrom != null && !amountFrom.equals("")) {
                amountFrom = amountFrom.trim();
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountFrom, loginLocale);
                    if(StringFunction.isNotBlank(amount)){
                        dynamicWhereClause.append(" and ");
                        dynamicWhereClause.append(expandedAmountType + " >= ");
                        dynamicWhereClause.append(amount);

                    }
                } catch (InvalidAttributeValueException e) {

                }

            }

            if (amountTo != null && !amountTo.equals("")) {

                amountTo = amountTo.trim();
                try {
                    String amount = NumberValidator.getNonInternationalizedValue(amountTo, loginLocale);
                    if (!amount.equals("")) {
                        dynamicWhereClause.append(" and ");
                        dynamicWhereClause.append(expandedAmountType + " <= ");
                        dynamicWhereClause.append(amount);

                    }
                } catch (InvalidAttributeValueException e) {

                }

            }

        }

        if (partyFilterText != null && !partyFilterText.equals("")) {

            partyFilterText = partyFilterText.trim();
            dynamicWhereClause.append(" and ");
            dynamicWhereClause.append(" buyer_party_identifier like '");
            dynamicWhereClause.append(SQLParamFilter.filter(partyFilterText.toUpperCase()));
            dynamicWhereClause.append("%'");

        }

        if (poNumber != null && !poNumber.equals("")) {
            dynamicWhereClause.append(" and ");
            dynamicWhereClause.append(" i.invoice_oid in (select p_invoice_oid from invoice_goods where upper(po_reference_id) like '");
            dynamicWhereClause.append(poNumber.toUpperCase());
            dynamicWhereClause.append("%')");

        }

        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
