/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class MatchNoticesDataView  extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(MatchNoticesDataView.class);
	/**
	 * Constructor
	 */
	public final static String INVOICE_SEARCH_STATUS_CLOSED    = "CLOSED";
	final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
	public MatchNoticesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhereClause     = new StringBuilder();

    	String encryptedSelectedWorkflow = parameters.get("selectedWorkflow");
        String selectedStatusPending = SQLParamFilter.filter(parameters.get("pending"));
        String selectedStatusPaid = SQLParamFilter.filter(parameters.get("paid"));

        String paymentReference = SQLParamFilter.filter(parameters.get("paymentReference"));
        String buyerID = SQLParamFilter.filter(parameters.get("buyerID"));
        String buyerName = SQLParamFilter.filter(parameters.get("buyerName"));
        String paymentSource = SQLParamFilter.filter(parameters.get("paymentSource"));

        //cquinton 3/26/2013 Rel PR ir#15207 workflow is always encrypted
        if ( encryptedSelectedWorkflow!=null && encryptedSelectedWorkflow.length()>0 ) {
            String selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedSelectedWorkflow, getUserSession().getSecretKey());
    		selectedWorkflow = SQLParamFilter.filter(selectedWorkflow);

            if (MY_WORK.equals(selectedWorkflow)) {
               dynamicWhereClause.append(" and a.a_assigned_to_user_oid = ?"); //userOid
            }
            else if (ALL_WORK.equals(selectedWorkflow)) {
               dynamicWhereClause.append(" and a.a_seller_corp_org_oid in (");
               dynamicWhereClause.append(" select organization_oid");
               dynamicWhereClause.append(" from corporate_org");
               dynamicWhereClause.append(" where activation_status = '");
               dynamicWhereClause.append(TradePortalConstants.ACTIVE);
               dynamicWhereClause.append("' start with organization_oid = ?"); //userOrgOid
               dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            }
            else {
               dynamicWhereClause.append(" and a.a_seller_corp_org_oid = ?"); //selectedWorkflow
            }
        }
        else {
            //workflow is required
            dynamicWhereClause.append(" and 1=0");
        }

         //IR 25387 8.4 ER start
		    if(StringFunction.isBlank(selectedStatusPaid) || StringFunction.isBlank(selectedStatusPending)){
		        if (TradePortalConstants.STATUS_PENDING.equals(selectedStatusPending)){
		        	dynamicWhereClause.append(" and a.transaction_status != '" );
		        	dynamicWhereClause.append(TransactionStatus.AUTHORIZED);
		        	dynamicWhereClause.append("'");
		        }
		        if (TradePortalConstants.STATUS_PAID.equals(selectedStatusPaid)){
		        	dynamicWhereClause.append(" and a.transaction_status = '" );
		        	dynamicWhereClause.append(TransactionStatus.AUTHORIZED);
		        	dynamicWhereClause.append("'");
		        }
		     }
        //IR 25387 8.4 ER End

        //PR ANZ-813 - Pavani - Else condition added since the list should not display anything when neither 'Paid' nor 'Pending' are checked.
        else if ((selectedStatusPaid == null || selectedStatusPaid.equals("")) && (selectedStatusPending == null || selectedStatusPending.equals("")))
        {
        	dynamicWhereClause.append(" and a.transaction_status IS NULL" );
        }

        if (paymentReference != null && !paymentReference.equals("")) {
	   		dynamicWhereClause.append(" and upper(a.payment_invoice_reference_id) like '%");
	   		dynamicWhereClause.append(paymentReference.toUpperCase());
	   		dynamicWhereClause.append("%'");
    	}
    	if (buyerID != null && !buyerID.equals("")) {
    	   		dynamicWhereClause.append(" and upper(a.seller_id_for_buyer) like '%");
    	  		dynamicWhereClause.append(buyerID.toUpperCase());
    	  		dynamicWhereClause.append("%'");
    	}
    	if (buyerName != null && !buyerName.equals("")) {
    	   		dynamicWhereClause.append(" and upper(a.seller_name_for_buyer) like '%");
    	  		dynamicWhereClause.append(buyerName.toUpperCase());
    	  		dynamicWhereClause.append("%'");
    	}
    	if (paymentSource != null && !paymentSource.equals("")) {
    	  		dynamicWhereClause.append(" and a.payment_source_type = '");
    	  		dynamicWhereClause.append(paymentSource);
    	  		dynamicWhereClause.append("'");
    	}

        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        //cquinton 3/26/2013 Rel PR ir#15207 workflow is always encrypted
        String encryptedSelectedWorkflow = parameters.get("selectedWorkflow");
        if ( encryptedSelectedWorkflow!=null && encryptedSelectedWorkflow.length()>0 ) {
            String selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedSelectedWorkflow, getUserSession().getSecretKey());
            selectedWorkflow = SQLParamFilter.filter(selectedWorkflow);

            if (MY_WORK.equals(selectedWorkflow)) {
                String userOid = getUserSession().getUserOid();
                statement.setString(startVarIdx+varCount, userOid);
                varCount++;
            }
            else if (ALL_WORK.equals(selectedWorkflow)) {
                String userOrgOid = getUserSession().getOwnerOrgOid();
                statement.setString(startVarIdx+varCount, userOrgOid);
                varCount++;
            }
            else {
                statement.setString(startVarIdx+varCount, selectedWorkflow);
                varCount++;
            }
        }

        return varCount;
    }
}
