/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class NotifyPendingPanelAuthUsersDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(NotifyPendingPanelAuthUsersDataView.class);
	/**
	 * Constructor
	 */
	public NotifyPendingPanelAuthUsersDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	return " and p_owner_org_oid = ? and panel_authority_code = ? and user_oid != ? ";// PMitnala IR#T36000023237 - User List to exclude the logged in user
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	String parentOrgID = getUserSession().getOwnerOrgOid();
        String userOid = getUserSession().getUserOid();
        String panel_level_code = parameters.get("panelLevel");
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, parentOrgID, panel_level_code, userOid);
    }
}
