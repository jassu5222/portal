/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class OperationalBankOrgsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(OperationalBankOrgsDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	
	/**
	 * Constructor
	 */
	public OperationalBankOrgsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();
        String clientBankID = getUserSession().getClientBankOid();


        	sql.append(" and a.p_owner_bank_oid = ? ");
        	sql.append(" and a.activation_status = ? ");
        	sqlParams.add(clientBankID);
        	sqlParams.add(TradePortalConstants.ACTIVE);
        	
		/* pgedupudi - Rel-9.3 CR-976A 03/30/2015 Start */
		if (StringFunction.isNotBlank(getUserSession()
				.getBankGrpRestrictRuleOid())) {
			sql.append(" and b.organization_oid not in ( ");
			List filteredBankGRoups = filterBankGroups(getUserSession()
					.getBankGrpRestrictRuleOid());
			for (int iLoop = 0; iLoop < filteredBankGRoups.size(); iLoop++) {
				sql.append("?");
				sqlParams.add(filteredBankGRoups.get(iLoop));
				if (iLoop != filteredBankGRoups.size() - 1) {
					sql.append(" , ");
				}
			}
			sql.append(" )");
		}
		/* pgedupudi - Rel-9.3 CR-976A 03/30/2015 End */
        	
        	//SSikhakolli - Rel 8.4 UAT IR# T36000026822 - 04/02/2014 - Begin
        	//Moving below line from OperationalBankOrgsDataView.xml which is added as part of Kyriba.
        	//This code is fetching wrong data from database if it is in where clause
        	sql.append(" and external_bank_ind ='N' or external_bank_ind IS NULL");
        	//SSikhakolli - Rel 8.4 UAT IR# T36000026822 - 04/02/2014 - End
   
        return sql.toString();
    }
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
    
    
}
