/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class PanelAuthTransactionsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthTransactionsDataView.class);
    final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
    final static String ALL_INSTRUMENTS = "common.all";
    
    private final List<Object> sqlParams = new ArrayList<>();

	/**
	 * Constructor
	 */
	public PanelAuthTransactionsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        StringBuilder sql = new StringBuilder();

        String selectedWorkOrgOid = parameters.get("work");
        String selectedMyTransactions = parameters.get("myTransactions");
        String unencryptedWorkOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkOrgOid, getUserSession().getSecretKey());
        
        if (ALL_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append("and b.a_corp_org_oid in (");
            sql.append(" select organization_oid");
            sql.append(" from corporate_org");
            sql.append(" where activation_status = ? start with organization_oid = ?"); //ownerOrgOid
            sql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            sqlParams.add(TradePortalConstants.ACTIVE);
            sqlParams.add(this.getOwnerOrgOid());
        }
        else if (MY_WORK.equals(unencryptedWorkOrgOid)) {
            sql.append("and a.a_assigned_to_user_oid = ?"); //userOrgOid
            sqlParams.add(this.getUserOid());
        }
        else {
            sql.append(" and b.a_corp_org_oid = ?");
            sqlParams.add(unencryptedWorkOrgOid);
        }
       

        //instrument groups
        //todo: this really should be ref data
        String selectedInstrGrp = SQLParamFilter.filter(parameters.get("grp"));
        if (TradePortalConstants.PANEL_INSTR_GROUP__TRADE.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
            
            sqlParams.add(InstrumentType.STANDBY_LC);
            sqlParams.add(InstrumentType.EXPORT_COL);
            sqlParams.add(InstrumentType.EXPORT_DLC);
            sqlParams.add(InstrumentType.NEW_EXPORT_COL);
            sqlParams.add(InstrumentType.GUARANTEE);
            sqlParams.add(InstrumentType.AIR_WAYBILL);
            
            sqlParams.add(InstrumentType.SHIP_GUAR);
            sqlParams.add(InstrumentType.IMPORT_DLC);
            sqlParams.add(InstrumentType.INCOMING_SLC);
            sqlParams.add(InstrumentType.CLEAN_BA);
            sqlParams.add(InstrumentType.DOCUMENTARY_BA);
            sqlParams.add(InstrumentType.REFINANCE_BA);
            
            sqlParams.add(InstrumentType.INDEMNITY);
            sqlParams.add(InstrumentType.DEFERRED_PAY);
            sqlParams.add(InstrumentType.COLLECT_ACCEPT);
            sqlParams.add(InstrumentType.INCOMING_GUA);
            sqlParams.add(InstrumentType.IMPORT_COL);
            sqlParams.add(InstrumentType.LOAN_RQST);
            
            sqlParams.add(InstrumentType.REQUEST_ADVISE);
            sqlParams.add(InstrumentType.APPROVAL_TO_PAY);
            
            
        }
        else if (TradePortalConstants.PANEL_INSTR_GROUP__PAYMENT.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,?,?) ");
            sqlParams.add(InstrumentType.FUNDS_XFER);
            sqlParams.add(InstrumentType.DOM_PMT);
            sqlParams.add(InstrumentType.XFER_BET_ACCTS);
        }
        else if (TradePortalConstants.PANEL_INSTR_GROUP__RECEIVABLES.equals(selectedInstrGrp) ) {
            sql.append(" and b.instrument_type_code in (?,? ) ");
            sqlParams.add(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
            sqlParams.add(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT);
		//IR T36000020457 Rel 8.3
        }else if(!TradePortalConstants.PANEL_INSTR_GROUP__ALL.equals(selectedInstrGrp) && 
        		 !TradePortalConstants.PANEL_INSTR_GROUP__TRADE.equals(selectedInstrGrp) &&
        		 !TradePortalConstants.PANEL_INSTR_GROUP__PAYMENT.equals(selectedInstrGrp) && 
        		 !TradePortalConstants.PANEL_INSTR_GROUP__RECEIVABLES.equals(selectedInstrGrp)){
        	sql.append(" and b.instrument_type_code = null");
        }

        //instrument types
        //todo: if not given just do those instrument types the user can view
        String selectedInstrType = parameters.get("iType");
        if (PanelAuthTransactionsDataView.ALL_INSTRUMENTS.equals(selectedInstrType)) {
            //do nothing
        }
        else if (selectedInstrType!=null && selectedInstrType.length()>0 ) {
            sql.append(" and b.instrument_type_code = ? ");
            sqlParams.add(selectedInstrType);
        }//IR T36000020457 Rel 8.3
		else if (selectedInstrType==null){
        	 sql.append(" and b.instrument_type_code = null");
        }

        sql.append(" And a.transaction_status IN (?,?,?)");
        sqlParams.add(TransactionStatus.AUTHORIZE_FAILED);
        sqlParams.add(TransactionStatus.PARTIALLY_AUTHORIZED);
        sqlParams.add(TransactionStatus.READY_TO_AUTHORIZE);
        
		//IR T36000020457 Rel 8.3  - searching if any one of the next approvers is the user's panel authority       
        if(MY_WORK.equals(selectedMyTransactions)){
        	sql.append(" and INSTR(a.panel_auth_next_approvals,");
        	sql.append(" (select panel_authority_code from users where user_oid=?)");
        	sql.append(",1) > 0");
        	sqlParams.add(this.getUserOid());
        }else if(!MY_WORK.equals(selectedMyTransactions) && !ALL_WORK.equals(selectedMyTransactions)){
        	sql.append(" and a.a_assigned_to_user_oid = null ");
        }
        
        sql.append(" AND a.a_panel_auth_group_oid IS NOT NULL ");
        //MEerupula Rel 8.4 IR-23915 
        String confInd  = getConfidentialInd();
      
        if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
            sql.append(" and ( b.confidential_indicator = 'N' OR ");
            sql.append(" b.confidential_indicator is null) ");
        }
        
        //Added by Sandeep 12/06/2012 - Start 
        //As per the design change a new field Instrument Id is added, 
        //so appending another AND condition to filter data based on Instrument ID also.
        String instrumentId = parameters.get("instrumentId");
        if(instrumentId == null)instrumentId="";
        
        if(!"".equals(instrumentId)){
        	sql.append(" and b.complete_instrument_id like UPPER(?) ");
        	 sqlParams.add(instrumentId);
        }
        
     
        //Added by Sandeep 12/06/2012 - End 
        
        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
