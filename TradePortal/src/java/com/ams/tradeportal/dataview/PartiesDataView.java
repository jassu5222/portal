/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class PartiesDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PartiesDataView.class);
		
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PartiesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

        
		StringBuilder where = new StringBuilder();
       
        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if(includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }
        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        String filterText = parameters.get("filterText");

        where.append("where p.p_owner_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
        {
        	where.append(",?");
            sqlParams.add(clientBankID);
            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
            	where.append(",? ");
            	sqlParams.add(bogID);
            }
        }

        // Possibly include the subsidiary access user org's data also
        if(includeSubAccessUserOrg){
		where.append(",?");
    	sqlParams.add(subAccessUserOrgID);
         }
        where.append(")");

        if (filterText != null && !filterText.equals(""))
        {
        	where.append(" and upper(name) like ?");
        	sqlParams.add("%" + StringFunction.toUnistr(filterText) + "%");
        }

        return where.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
