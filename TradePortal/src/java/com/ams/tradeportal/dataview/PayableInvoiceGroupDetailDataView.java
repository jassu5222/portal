/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

import com.ams.tradeportal.common.SQLParamFilter;
import java.util.HashMap;

/**
 *
 *
 */
public class PayableInvoiceGroupDetailDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PayableInvoiceGroupDetailDataView.class);

    /**
     * Constructor
     */
    public PayableInvoiceGroupDetailDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String invoiceGroupOid = SQLParamFilter.filterNumber(parameters.get("invoiceGroupOid"));

        dynamicWhereClause.append("and i.a_corp_org_oid='").append(userOrgOid).append("'");
        dynamicWhereClause.append(" and i.a_invoice_group_oid = ").append(invoiceGroupOid);
        //This piece of code is needed to print(view pdf) the invoice list
        HashMap map = new HashMap();
        map.put("PayableInvoiceGroupDetailDataView", dynamicWhereClause.toString());
        map.put("params", parameters);
        InvoiceDataViewUtil.getMoreSQL().setMap(map);
        //End

        return dynamicWhereClause.toString();

    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;
    }
}
