/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 *
 */
public class PayablesMgmtCreditNoteDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PayablesMgmtCreditNoteDataView.class);

    private final List<Object> sqlParams = new ArrayList();

    /**
     * Constructor
     */
    public PayablesMgmtCreditNoteDataView() {

        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder whereClause = new StringBuilder();
        StringBuilder countClause = new StringBuilder();
        String parentId = parameters.get("parentId");
        if (StringFunction.isNotBlank(parentId)) {
            whereClause.append(buildDynamicSQLCriteriaChild(parameters));
            countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
        } else {
            whereClause.append(buildDynamicSQLCriteriaParent(parameters));
            countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
        }

        super.fullSQL = whereClause.toString();
        super.fullCountSQL = countClause.toString();

        return null;
    }

    private String buildDynamicSQLCriteriaParent(Map<String, String> parameters) {
        StringBuilder dynamicWhereClause = new StringBuilder();
        StringBuilder where = new StringBuilder();
        String creditNoteID = SQLParamFilter.filter(parameters.get("creditNoteID"));
        String tradingPartner = SQLParamFilter.filter(parameters.get("tradingPartner"));
        String statusType = SQLParamFilter.filter(parameters.get("statusType"));
        String status = SQLParamFilter.filter(parameters.get("status"));
        String currency = SQLParamFilter.filter(parameters.get("currency"));
        String amountType = SQLParamFilter.filter(parameters.get("amountType"));
        String amountFrom = SQLParamFilter.filter(parameters.get("amountFrom"));
        String amountTo = SQLParamFilter.filter(parameters.get("amountTo"));
        String endToEndID = SQLParamFilter.filter(parameters.get("endToEndID"));
        String sort = SQLParamFilter.filter(parameters.get("sort"));
        if (amountFrom == null) {
            amountFrom = "0";
        }
        //if(amountTo==null)amountTo="0";
        if (amountFrom != null && amountFrom.contains("-")) {
            amountFrom = amountFrom.replaceFirst("-", "");
        }
        if (amountTo != null && amountTo.contains("-")) {
            amountTo = amountTo.replaceFirst("-", "");
        }
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String selectouter = " SELECT rowKey,upload_invoice_oid,InvoiceID,Ccy,TradingPartner,CHILDREN,Amount,AppliedAmount,AppliedStatus,UtilisedStatus,ViewPDF,RemainingAmount,EndToEndID, DocName,Hash";
        String select = "SELECT DISTINCT o.upload_credit_note_oid AS rowKey,o.upload_credit_note_oid AS upload_invoice_oid, o.invoice_id AS InvoiceID,o.currency AS Ccy,"
                + "o.seller_name AS TradingPartner,'true' AS CHILDREN, o.amount AS Amount,NVL(o.credit_note_applied_amount,0)*(-1) as AppliedAmount,o.end_to_end_id AS EndToEndID, "
                + "NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0)  as RemainingAmount, o.credit_note_applied_status as AppliedStatus,o.credit_note_utilised_status as UtilisedStatus, d.IMAGE_ID as ViewPDF, d.DOC_NAME as DocName,d.HASH as Hash";
        String from = " FROM CREDIT_NOTES o left outer join Document_image d on o.upload_credit_note_oid = d.p_transaction_oid";
        where.append(" WHERE o.a_corp_org_oid = ? ");
        sqlParams.add(userOrgOid);
        if (StringFunction.isNotBlank(creditNoteID)) {
            where.append(" and upper(o.invoice_id) like ?");
            sqlParams.add("%" + creditNoteID + "%");
        }
        if (StringFunction.isNotBlank(tradingPartner)) {
            where.append(" and (upper(o.seller_name) like ? or upper(o.seller_id) like ?) ");
            sqlParams.add("%" + tradingPartner + "%");
            sqlParams.add("%" + tradingPartner + "%");
        }
        if (StringFunction.isNotBlank(statusType)) {
            if (TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_TYPE.equals(statusType) && StringFunction.isNotBlank(status)) {
                where.append(" and o.credit_note_applied_status = ?");
                sqlParams.add(status);
            } else if (TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_TYPE.equals(statusType) && StringFunction.isNotBlank(status)) {
                where.append(" and o.credit_note_utilised_status = ?");
                sqlParams.add(status);
            }
        }
        if (StringFunction.isNotBlank(currency)) {
            where.append(" and o.currency=?");
            sqlParams.add(currency);
        }

        if (StringFunction.isNotBlank(amountType) && TradePortalConstants.CREDIT_NOTE_AMOUNT.equals(amountType)) {

            where.append(" and abs(NVL(o.amount,0)) >= ?");
            sqlParams.add(amountFrom);

            if (StringFunction.isNotBlank(amountTo)) {
                where.append(" and abs(o.amount) <= ?");
                sqlParams.add(amountTo);
            }
        } else if (StringFunction.isNotBlank(amountType) && TradePortalConstants.REMAINING_AMOUNT.equals(amountType)) {
            where.append(" and abs(NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0) )>= ?");
            sqlParams.add(amountFrom);
            if (StringFunction.isNotBlank(amountTo)) {
                where.append(" and abs(NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0) ) <= ?");
                sqlParams.add(amountTo);
            }
        }

        if (StringFunction.isNotBlank(endToEndID)) {
            where.append(" and upper(o.end_to_end_id) like ?");
            sqlParams.add("%" + endToEndID + "%");
        }

        dynamicWhereClause.append(selectouter).append(" from ( ").append(selectouter).append(" , rownum row_num from ( ").append(select).append(from).append(where);
        //Added for sorting in Grid
        if (StringFunction.isNotBlank(sort)) {
            if (sort.startsWith("-")) {
                sort = sort.substring(1);
                dynamicWhereClause.append(" order by ").append(sort).append(" desc ");
            } else {
                dynamicWhereClause.append(" order by ").append(sort).append(" asc ");
            }
        }
        dynamicWhereClause.append(" ) ) where row_num > ? and row_num <= ?");

        return dynamicWhereClause.toString();

    }

    private String buildDynamicSQLCriteriaParentCount(Map<String, String> parameters) {
        StringBuilder dynamicWhereClause = new StringBuilder();
        StringBuilder where = new StringBuilder();
        String creditNoteID = SQLParamFilter.filter(parameters.get("creditNoteID"));
        String tradingPartner = SQLParamFilter.filter(parameters.get("tradingPartner"));
        String statusType = SQLParamFilter.filter(parameters.get("statusType"));
        String status = SQLParamFilter.filter(parameters.get("status"));
        String currency = SQLParamFilter.filter(parameters.get("currency"));
        String amountType = SQLParamFilter.filter(parameters.get("amountType"));
        String amountTo = SQLParamFilter.filter(parameters.get("amountTo"));
        String endToEndID = SQLParamFilter.filter(parameters.get("endToEndID"));
        String select = " SELECT count (DISTINCT o.upload_credit_note_oid) ";
        String from = " FROM CREDIT_NOTES o ";
        where.append(" WHERE o.a_corp_org_oid = ?");
        if (StringFunction.isNotBlank(creditNoteID)) {
            where.append(" and upper(o.invoice_id) like ?");
        }
        if (StringFunction.isNotBlank(tradingPartner)) {
            where.append(" and (upper(o.seller_name) like ? or upper(o.seller_id) like ?)");
        }
        if (StringFunction.isNotBlank(statusType)) {
            if (TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_TYPE.equals(statusType) && StringFunction.isNotBlank(status)) {
                where.append(" and o.credit_note_applied_status = ?");
            } else if (TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_TYPE.equals(statusType) && StringFunction.isNotBlank(status)) {
                where.append(" and o.credit_note_utilised_status = ?");
            }
        }
        if (StringFunction.isNotBlank(currency)) {
            where.append(" and o.currency=?");
        }
        if (StringFunction.isNotBlank(amountType) && TradePortalConstants.CREDIT_NOTE_AMOUNT.equals(amountType)) {
            where.append(" and abs(NVL(o.amount,0)) >= ?");
            if (StringFunction.isNotBlank(amountTo)) {
                where.append(" and abs(NVL(o.amount,0)) <= ?");
            }
        } else if (StringFunction.isNotBlank(amountType) && TradePortalConstants.REMAINING_AMOUNT.equals(amountType)) {
            where.append(" and abs(NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0) )>= ?");
            if (StringFunction.isNotBlank(amountTo)) {
                where.append(" and abs(NVL(o.amount,0) -  NVL(o.credit_note_applied_amount,0) ) <= ?");
            }
        }
        if (StringFunction.isNotBlank(endToEndID)) {
            where.append(" and upper(o.end_to_end_id like ?)");
        }
        dynamicWhereClause.append(select).append(from).append(where);

        return dynamicWhereClause.toString();
    }

    private String buildDynamicSQLCriteriaChild(Map<String, String> parameters) {
        String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
        StringBuilder dynamicWhereClause = new StringBuilder();
        String select1 = " SELECT  InvoiceID ,rowKey,upload_invoice_oid,InvoiceSummaryID,CRInvoiceID,Ccy,TradingPartner,CHILDREN,Amount,AppliedAmount,AppliedStatus,UtilisedStatus,ViewPDF,RemainingAmount,EndToEndID,DocName,Hash FROM( SELECT i.invoice_reference_id AS InvoiceID,i.invoice_oid AS rowKey, i.invoice_oid AS upload_invoice_oid,i.invoice_reference_id AS InvoiceSummaryID,i.invoice_reference_id AS CRInvoiceID,i.currency_code AS Ccy,"
                + "i.seller_party_identifier AS TradingPartner,'INV' AS CHILDREN, rownum as row_num, case when (i.invoice_payment_amount is null or i.invoice_payment_amount=0) then i.invoice_total_amount else i.invoice_payment_amount end as Amount,"
                + "c.credit_note_applied_amount as  AppliedAmount,'' as AppliedStatus,'' as UtilisedStatus, '' as ViewPDF, '' as DocName,'' as Hash, "
                + " i.inv_amount_after_adj as RemainingAmount, o.end_to_end_id as EndToEndID";
        String from1 = " FROM invoice i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
        String where1 = " WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.invoice_oid";
        dynamicWhereClause.append(select1).append(from1).append(where1);
        if (StringFunction.isNotBlank(parentId)) {
            dynamicWhereClause.append(" AND o.upload_credit_note_oid =?");
            sqlParams.add(parentId);
        }

        dynamicWhereClause.append(" UNION ");

        String select2 = " SELECT i.invoice_id AS InvoiceID, i.upload_invoice_oid AS rowKey, i.upload_invoice_oid AS upload_invoice_oid,i.invoice_id AS InvoiceSummaryID,i.invoice_id AS CRInvoiceID,i.currency AS Ccy,"
                + "i.seller_name AS TradingPartner,'INVSum' AS CHILDREN, rownum as row_num, case when (i.payment_amount is null or i.payment_amount=0) then i.amount else i.payment_amount end as Amount,"
                + "c.credit_note_applied_amount as  AppliedAmount,'' as AppliedStatus,'' as UtilisedStatus, '' as ViewPDF, '' as DocName,'' as Hash, "
                + " (CASE WHEN i.payment_amount is NULL THEN ( NVL(i.amount,0) +  NVL(i.total_credit_note_amount,0)) "
                + " ELSE (NVL(i.payment_amount,0) + NVL(i.total_credit_note_amount,0)) END) as RemainingAmount, i.end_to_end_id as EndToEndID";
        String from2 = " FROM invoices_summary_data i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
        String where2 = " WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.upload_invoice_oid and i.invoice_id not in (select invoice_reference_id from invoice where a_corp_org_oid = ? )";
        sqlParams.add(getUserSession().getOwnerOrgOid());
        dynamicWhereClause.append(select2).append(from2).append(where2);
        if (StringFunction.isNotBlank(parentId)) {
            dynamicWhereClause.append(" AND o.upload_credit_note_oid =?");
            sqlParams.add(parentId);
        }
        dynamicWhereClause.append(" )WHERE row_num > ? AND row_num  <= ?");
        return dynamicWhereClause.toString();

    }

    private String buildDynamicSQLCriteriaChildCount(Map<String, String> parameters) {
        String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
        StringBuilder dynamicWhereClause = new StringBuilder();
        String select1 = " SELECT count(invoiceOid) FROM (SELECT i.invoice_oid AS invoiceOid,i.invoice_reference_id as invId";
        String from1 = " FROM invoice i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
        String where1 = " WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.invoice_oid ";
        dynamicWhereClause.append(select1).append(from1).append(where1);
        if (StringFunction.isNotBlank(parentId)) {
            dynamicWhereClause.append(" AND o.upload_credit_note_oid  =?");
        }
            dynamicWhereClause.append(" UNION ");

            String select2 = " SELECT i.upload_invoice_oid AS invoiceOid,i.invoice_id as invId";
            String from2 = " FROM invoices_summary_data i,CREDIT_NOTES o,INVOICES_CREDIT_NOTES_RELATION c";
            String where2 = " WHERE c.a_upload_credit_note_oid = o.upload_credit_note_oid and c.a_upload_invoice_oid = i.upload_invoice_oid and i.invoice_id not in (select invoice_reference_id from invoice where a_corp_org_oid = ?) ";
            dynamicWhereClause.append(select2).append(from2).append(where2);
            if (StringFunction.isNotBlank(parentId)) {
                dynamicWhereClause.append(" AND o.upload_credit_note_oid  =?");
            }
            dynamicWhereClause.append(" ) ");

        

        return dynamicWhereClause.toString();

    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }

    @Override
    protected boolean ignorePassedInParam(Map<String, String> parameters) {
        return StringFunction.isNotBlank(parameters.get("parentId"));

    }

}
