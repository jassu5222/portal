/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 *
 */
public class PayablesMgmtInstrumentDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PayablesMgmtInstrumentDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PayablesMgmtInstrumentDataView() {

		super();
	}

	   protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

		StringBuilder      whereClause     = new StringBuilder();
    	StringBuilder 	  countClause     = new StringBuilder();
    	String parentId = parameters.get("parentId");
    	if(StringFunction.isNotBlank(parentId)){
    		whereClause.append(buildDynamicSQLCriteriaChild(parameters));
    		countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
    	}else{
    		whereClause.append(buildDynamicSQLCriteriaParent(parameters));
    		countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
    	}
    	
    	super.fullSQL = whereClause.toString();
    	super.fullCountSQL = countClause.toString();

        return null;
    }

    private String buildDynamicSQLCriteriaParent(Map<String,String> parameters) {
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	StringBuilder      where     = new StringBuilder();
    	String Active=parameters.get("Active");
    	String InActive=parameters.get("InActive");
    	String Currency=parameters.get("Currency");
    	String InstrumentID=parameters.get("InstrumentID");
    	String sort=parameters.get("sort");
        String UserOrgOid=getUserSession().getOwnerOrgOid();
    	String selectouter=" SELECT rowKey,instrument_oid,transaction_oid,INSTRUMENT,Type,STATUS,STATUSFORQVIEW,CCY,AMOUNT,REFERENCE,CHILDREN";
    	String select="SELECT i.instrument_oid AS rowKey,i.instrument_oid AS instrument_oid, o.transaction_oid as transaction_oid,i.complete_instrument_id AS INSTRUMENT,i.instrument_type_code AS Type," +
    			"i.instrument_status AS STATUS,i.instrument_status AS STATUSFORQVIEW,o.copy_of_currency_code AS CCY,i.copy_of_instrument_amount AS AMOUNT,i.complete_instrument_id AS REFERENCE," +
    			"'true' AS CHILDREN";
    	String from=" FROM instrument i,transaction o,transaction t";
    	where.append(" WHERE i.original_transaction_oid = o.transaction_oid AND i.active_transaction_oid = t.transaction_oid(+) AND i.template_flag ='N' AND i.instrument_status!= 'DEL' AND i.instrument_type_code IN ('PYB')");

    	if(StringFunction.isNotBlank(Active) && Active.equals("Y")){
    		if(StringFunction.isNotBlank(InActive) && InActive.equals("Y")){
    			where.append(" and i.instrument_status in ('ACT','PND','EXP','CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    		}
    		else {
    			where.append(" and i.instrument_status in ('ACT','PND','EXP')");
    		}	
    	}
    	else if(StringFunction.isNotBlank(InActive) && InActive.equals("Y")){
    		where.append(" and i.instrument_status in ('CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    	}
    	else if((StringFunction.isBlank(Active) || Active.equals("N")) && (StringFunction.isBlank(InActive) || InActive.equals("N"))) {
    		where.append(" and i.instrument_status in ('') ");
    	}
    	
    	if(StringFunction.isNotBlank(InstrumentID)){
    		where.append(" and upper(i.complete_instrument_id) like ? ");
    		sqlParams.add("%"+InstrumentID.toUpperCase()+"%");
    	}
    	if(StringFunction.isNotBlank(Currency)){
    		where.append(" and o.copy_of_currency_code=?");
    		sqlParams.add(Currency);
    	}
    
    	if(StringFunction.isNotBlank(UserOrgOid)){
    		where.append(" and i.a_corp_org_oid = ? ");
    		sqlParams.add(UserOrgOid);
    	}
    

    	dynamicWhereClause.append(selectouter).append(" from ( ").append(selectouter).append(" , rownum row_num from ( ").append(select).append(from).append(where);
    	//Added for sorting in Grid
    	if(StringFunction.isNotBlank(sort)){
    		if ( sort.startsWith("-")) {
    			sort = sort.substring(1);
    			sort = "'"+ SQLParamFilter.filter(sort)+"'";
    			dynamicWhereClause.append(" order by ").append(sort).append(" desc ");
            } else {
            	dynamicWhereClause.append(" order by ").append(sort).append(" asc ");
            }
        }
    	dynamicWhereClause.append(" ) ) where row_num > ? and row_num <= ?");




    	return dynamicWhereClause.toString();


	}

    private String buildDynamicSQLCriteriaParentCount(Map<String,String> parameters) {
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	StringBuilder      where     = new StringBuilder();
    	String Active=parameters.get("Active");
    	String InActive=parameters.get("InActive");
    	String Currency=parameters.get("Currency");
    	String InstrumentID=parameters.get("InstrumentID");
    	
    	String UserOrgOid=parameters.get("userOrgOid");
    
    	

    	String select=" SELECT count (i.instrument_oid) ";
    	String from=" FROM instrument i,transaction o,transaction t ";
    	where.append(" WHERE i.original_transaction_oid = o.transaction_oid AND i.active_transaction_oid = t.transaction_oid(+) AND i.template_flag ='N' AND i.instrument_status!= 'DEL' AND i.instrument_type_code IN ('PYB')");

    	if(StringFunction.isNotBlank(Active) && Active.equals("Y")){
    		if(StringFunction.isNotBlank(InActive) && InActive.equals("Y")){
    			where.append(" and i.instrument_status in ('ACT','PND','EXP','CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    		}
    		else {
    			where.append(" and i.instrument_status in ('ACT','PND','EXP')");
    		}	
    	}
    	else if(StringFunction.isNotBlank(InActive) && InActive.equals("Y")){
    		where.append(" and i.instrument_status in ('CLO','CAN','DEA','LIQ','LQB','LQC','DEL')");
    	}
    	else if((StringFunction.isBlank(Active) || Active.equals("N")) && (StringFunction.isBlank(InActive) || InActive.equals("N"))) {
    		where.append(" and i.instrument_status in ('') ");
    	}
    		
    	if(StringFunction.isNotBlank(InstrumentID)){
    		where.append(" and upper(i.complete_instrument_id) = ?");
    	}
    	if(StringFunction.isNotBlank(Currency)){
    		where.append(" and o.copy_of_currency_code=?");
    	}
    
    	if(StringFunction.isNotBlank(UserOrgOid)){
    		where.append(" and i.a_corp_org_oid = ?");
    	}
    

    	dynamicWhereClause.append(select).append(from).append(where);

    	return dynamicWhereClause.toString();


	}

    private String buildDynamicSQLCriteriaChild(Map<String,String> parameters) {
    	String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	
    	String select=" SELECT rowKey,transaction_oid,instrument_oid,INSTRUMENT,Type,STATUS,STATUSFORQVIEW,CCY,AMOUNT,REFERENCE,CHILDREN FROM( select a.transaction_oid as rowKey,DECODE(a.transaction_type_code ,'ARM',NULL,'CHM',NULL,a.transaction_oid ) AS transaction_oid,b.instrument_oid  as instrument_oid, to_char(a.transaction_status_date, 'DD Mon YYYY') AS INSTRUMENT,a.transaction_type_code AS Type, a.transaction_status AS STATUS, a.transaction_status AS STATUSFORQVIEW,DECODE(a.copy_of_currency_code, NULL, active_tran.copy_of_currency_code , a.copy_of_currency_code ) AS CCY, a.copy_of_amount AS AMOUNT, null AS REFERENCE,'false' AS CHILDREN, rownum as row_num";
    	String from=" FROM transaction a, instrument b, transaction active_tran";
    	String where=" WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND a.display_change_transaction = 'N') AND b.active_transaction_oid     = active_tran.transaction_oid(+)";
    	dynamicWhereClause.append(select).append(from).append(where);
    	if(StringFunction.isNotBlank(parentId)){
    		String anWhere=" AND a.transaction_oid IN (SELECT original_transaction_oid AS transaction_oid FROM instrument  WHERE a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid =?)";
    		sqlParams.add(parentId);
    		sqlParams.add(parentId);
    		dynamicWhereClause.append(anWhere);
    	}
    	dynamicWhereClause.append(" )WHERE row_num > ? AND row_num  <= ?");

    	return dynamicWhereClause.toString();
	}

    private String buildDynamicSQLCriteriaChildCount(Map<String,String> parameters) {
    	String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
    	StringBuilder      dynamicWhereClause     = new StringBuilder();
    	String select=" SELECT count(b.instrument_oid) ";
    	String from=" FROM transaction a, instrument b, transaction active_tran";
    	String where=" WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND a.display_change_transaction = 'N') AND b.active_transaction_oid     = active_tran.transaction_oid(+)";
    	dynamicWhereClause.append(select).append(from).append(where);
    	if(StringFunction.isNotBlank(parentId)){
    			String anWhere=" AND a.transaction_oid IN (SELECT original_transaction_oid AS transaction_oid FROM instrument  WHERE a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ?)";
    			dynamicWhereClause.append(anWhere);
    	}
    	return dynamicWhereClause.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
	
	protected boolean ignorePassedInParam(Map<String,String> parameters){
    	return StringFunction.isNotBlank(parameters.get("parentId"));
		
	}
}
