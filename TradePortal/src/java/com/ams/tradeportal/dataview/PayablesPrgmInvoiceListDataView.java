package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ams.tradeportal.common.SQLParamFilter;

import com.ams.tradeportal.common.TradePortalConstants;
import java.util.HashMap;

public class PayablesPrgmInvoiceListDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PayablesPrgmInvoiceListDataView.class);

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {

        StringBuilder dynamicWhereClause = new StringBuilder();
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String invoiceStatusType = SQLParamFilter.filter(parameters.get("invoiceStatusType"));
        dynamicWhereClause.append("and i.a_corp_org_oid='").append(userOrgOid).append("'");
        if (!TradePortalConstants.STATUS_ALL.equals(invoiceStatusType)) {
            dynamicWhereClause.append(" and (i.invoice_status = '");
            dynamicWhereClause.append(invoiceStatusType);
            dynamicWhereClause.append("'");
            if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(invoiceStatusType)) {
                dynamicWhereClause.append(" or i.invoice_status = '");
                dynamicWhereClause.append(TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_AUTHORISED);
                dynamicWhereClause.append("'");
            }
            dynamicWhereClause.append(")");
        }
        //This piece of code is needed to print(view pdf) the invoice list
        HashMap map = new HashMap();
        map.put("PayablesPrgmInvoiceListDataView", dynamicWhereClause.toString());
        map.put("params", parameters);
        InvoiceDataViewUtil.getMoreSQL().setMap(map);
        //end
        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;
        return varCount;
    }
}
