/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 *
 *
 */
public class PaymentAuthorizedTransactionsDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentAuthorizedTransactionsDataView.class);
	/**
	 * Constructor
	 */
	public PaymentAuthorizedTransactionsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String selectedOrgOid = parameters.get("selectedOrgOid");
        if (selectedOrgOid!=null) {
            selectedOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
            String allOrganizations =
                this.getResourceManager().
                    getText("AuthorizedTransactions.AllOrganizations",
                    TradePortalConstants.TEXT_BUNDLE);
            if (allOrganizations.equals(selectedOrgOid)) {
                sql.append("and b.a_corp_org_oid in (");
                sql.append(" select organization_oid");
                sql.append(" from corporate_org");
                sql.append(" where activation_status = '");
                sql.append(TradePortalConstants.ACTIVE);
                sql.append("' start with organization_oid = ? "); //user userOrgOid here!
                sql.append("connect by prior organization_oid = p_parent_corp_org_oid)");
            }
            else {
                sql.append("and b.a_corp_org_oid = ? "); //selectedOrgOid
            }
        }
        //cquinton 4/9/2013 Rel PR ir#15704 begin
        else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
        //cquinton 4/9/2013 Rel PR ir#15704 end

        //For Users where the User Profile level
        // "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No),
        // no transactions will appear that are designated as Confidential Payments

      //MEerupula Rel 8.4 IR-23915 
        String confInd  = getConfidentialInd();
        if (confInd ==null || TradePortalConstants.INDICATOR_NO.equals(confInd) ) {
            sql.append(" and ( t.confidential_indicator = 'N' OR ");
            sql.append(" t.confidential_indicator is null) ");
        }

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;

        String selectedOrgOid = parameters.get("selectedOrgOid");
        if (selectedOrgOid!=null) {
            selectedOrgOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrgOid, getUserSession().getSecretKey());
            String allOrganizations =
                this.getResourceManager().
                    getText("AuthorizedTransactions.AllOrganizations",
                    TradePortalConstants.TEXT_BUNDLE);
            if (allOrganizations.equals(selectedOrgOid)) {
                String userOrgOid = SQLParamFilter.filterNumber(parameters.get("userOrgOid"));
                statement.setString(startVarIdx+varCount, userOrgOid);
                varCount++;
            }
            else {
                statement.setString(startVarIdx+varCount, selectedOrgOid);
                varCount++;
            }
        }

        return varCount;
    }
}
