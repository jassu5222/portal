/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.EncryptDecrypt;
/**
 *
 *
 */
public class PaymentFileUploadDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadDataView.class);
	
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PaymentFileUploadDataView() {
		super();
	}

	final static String ALL_WORK = "common.allWork";
	final static String MY_WORK = "common.myWork";

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
    	StringBuilder dynamicWhereClause = new StringBuilder();

        String selectedWorkflow = parameters.get("selectedWorkflow");
        String userOid = parameters.get("userOid");
        String userOrgOid = parameters.get("userOrgOid");
        String displayedValue = parameters.get("displayedValue");
		 String confInd  = getConfidentialInd();
		
        if ( displayedValue != null){
        	selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, getUserSession().getSecretKey());
        	
        }

        if (selectedWorkflow.equals(MY_WORK)) {
           dynamicWhereClause.append("and a_assigned_to_user_oid = ? "); 
           sqlParams.add(userOid);
        }else if (selectedWorkflow.equals(ALL_WORK))
        {
           dynamicWhereClause.append("and a_owner_org_oid in (");
           dynamicWhereClause.append(" select organization_oid");
           dynamicWhereClause.append(" from corporate_org");
           dynamicWhereClause.append(" where activation_status = ? start with organization_oid = ? ");
           dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
           sqlParams.add(TradePortalConstants.ACTIVE);
           sqlParams.add(userOrgOid);
        }
        else
        {
           dynamicWhereClause.append("and a_owner_org_oid  = ? ");
           sqlParams.add(selectedWorkflow);
        }
		if(TradePortalConstants.INDICATOR_NO.equals(confInd)){
			dynamicWhereClause.append(" and nvl(confidential_indicator,'N') NOT IN (?,?) ");
			sqlParams.add(TradePortalConstants.INDICATOR_YES);
			sqlParams.add(TradePortalConstants.CONF_IND_FRM_TMPLT);
		}

        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
