/**
 *
 */
package com.ams.tradeportal.dataview;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class PaymentInquiriesDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PaymentInquiriesDataView.class);
	private final List<Object> sqlParams = new ArrayList<Object>();

	/**
	 * Constructor
	 */

	private final String innerSelect=" select i.instrument_oid as rowKey,i.instrument_oid as instrument_oid,o.transaction_oid as transaction_oid,i.complete_instrument_id as InstrumentId,i.instrument_type_code as Type,o.copy_of_currency_code as CCY, i.copy_of_instrument_amount as Amount,i.instrument_status as Status, i.instrument_status as STATUSFORQVIEW,i.copy_of_ref_num as PrimaryReference,p.name as Party,'true' as CHILDREN,i.instrument_type_code as InstrumentTypeCode, (CASE WHEN o.C_CUST_ENTER_TERMS_OID is null THEN 'T'  ELSE 'F' END) as TPSGen";

	private final String outerSelect=" select rowKey,instrument_oid,transaction_oid,InstrumentId,Type,CCY,Amount,Status,STATUSFORQVIEW,PrimaryReference,Party,CHILDREN,InstrumentTypeCode,TPSGen";

	private final String innerSelectChild=" SELECT a.transaction_oid as rowKey,DECODE(a.transaction_type_code ,'ARM',NULL,'CHM',NULL,a.transaction_oid )" +
			" AS transaction_oid, b.instrument_oid, to_char(a.transaction_status_date, 'DD Mon YYYY') AS InstrumentId, a.transaction_type_code AS Type," +
			" DECODE(a.copy_of_currency_code, NULL, active_tran.copy_of_currency_code , a.copy_of_currency_code ) AS CCY, a.copy_of_amount AS Amount," +
			" a.transaction_status AS Status,a.transaction_status as STATUSFORQVIEW,NULL as PrimaryReference, NULL AS Party,'false' AS CHILDREN ";

	private final String outerSelectChild=" SELECT rowKey,transaction_oid,instrument_oid,InstrumentId,Type,CCY,Amount,Status,STATUSFORQVIEW,PrimaryReference,Party,CHILDREN";

	private final String innerFrom=" from instrument i,transaction o,transaction t,terms_party p ";

	private final String innerFromChild=" FROM transaction a,instrument b,transaction active_tran";

	private final String innerWhere="where i.original_transaction_oid = o.transaction_oid and i.active_transaction_oid = t.transaction_oid(+) and" +
			"  i.a_counter_party_oid = p.terms_party_oid(+) and i.template_flag='N' and i.instrument_status != 'DEL' " +
			"and i.instrument_type_code in ('FTBA','FTDP','FTRQ') and NOT EXISTS (select NULL from transaction o1 where o1.transaction_status " +
			"IN ('STARTED','VERIFIED_AWAITING_APPROVAL') and  o1.uploaded_ind IS NOT NULL and o1.uploaded_ind = 'Y' and " +
			"o.transaction_oid=o1.transaction_oid)";



	public PaymentInquiriesDataView() {
		super();
	}

        @Override
	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

		StringBuilder whereClause     = new StringBuilder();
    	StringBuilder countClause     = new StringBuilder();
    	String parentId = SQLParamFilter.filterNumber(parameters.get("parentId"));

    	if(StringFunction.isNotBlank(parentId)){
    		whereClause.append(buildDynamicSQLCriteriaChild(parameters));
    		countClause.append(buildDynamicSQLCriteriaChildCount(parameters));
    	}else{
    		whereClause.append(buildDynamicSQLCriteriaParent(parameters));
    		countClause.append(buildDynamicSQLCriteriaParentCount(parameters));
    	}

    	super.fullSQL = whereClause.toString();
    	super.fullCountSQL = countClause.toString();

        return null;
    }

    protected String buildDynamicSQLCriteriaParentFilter(Map<String,String> parameters, boolean addToParamList) {
    	StringBuilder orgList = new StringBuilder();
        StringBuilder sql = new StringBuilder();
        int numStatuses = -1;

        MediatorServices medService = new MediatorServices();
        String selectedOrg = parameters.get("selectedOrg");

        String instrStatusType = parameters.get("instrStatusType");
        String confInd  = getConfidentialInd();
    
        String searchType = parameters.get("searchType");
        String instrumentId = parameters.get("instrumentId");
        String instrumentType = parameters.get("instrumentType");
        String refNum = parameters.get("refNum");

        String currency = parameters.get("currency");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String otherParty = parameters.get("otherParty");
        String externalBankOid = parameters.get("externalBankOid"); //Kyriba CR 268
        
    
        String payDateFrom = parameters.get("payDateFrom");
        String payDateTo = parameters.get("payDateTo");  
        String datePattern = null;
        try{
        	if(null != parameters.get("dPattern")){
        		datePattern = java.net.URLDecoder.decode(parameters.get("dPattern"), "UTF-8");
        	}else{
        		datePattern = "";
        	}
        }catch (UnsupportedEncodingException e) {
			LOG.debug("Exceptiom in DateFormat Decoding : "+e);
		}
         


        String  ALL_ORGANIZATIONS = getResourceManager().getText("AuthorizedTransactions.AllOrganizations",TradePortalConstants.TEXT_BUNDLE);

        Vector statuses = new Vector();

        if ( selectedOrg!=null ) {
            selectedOrg= EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, getUserSession().getSecretKey());

            if (ALL_ORGANIZATIONS.equalsIgnoreCase(selectedOrg)) {
            	  StringBuilder sqlQuery = new StringBuilder();
           	   sqlQuery.append("select organization_oid, name");
           	   sqlQuery.append(" from corporate_org");
           	   sqlQuery.append(" where activation_status = ? start with organization_oid = ? connect by prior organization_oid = p_parent_corp_org_oid");
           	   sqlQuery.append(" order by ");
           	   sqlQuery.append(getResourceManager().localizeOrderBy("name"));
    
           	   try{
           		DocumentHandler hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, TradePortalConstants.ACTIVE, getOwnerOrgOid());
    
           		   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
           		   int totalOrganizations = orgListDoc.size();
    
           		   DocumentHandler orgDoc = null;
           		   for (int i = 0; i < totalOrganizations; i++) {
           			   orgDoc = (DocumentHandler) orgListDoc.get(i);
           			  
           			   orgList.append("?");
           			if(addToParamList){
         			   sqlParams.add(orgDoc.getAttribute("ORGANIZATION_OID"));
           			}

           			   if (i < (totalOrganizations - 1) ) {
           				   orgList.append(", ");
           			   }
           		   }
           	   }catch(AmsException e){
           		   e.printStackTrace();
           	   }
       		   // Build a comma separated list of the orgs
       		   sql.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
    
            }  else {
            	sql.append(" and i.a_corp_org_oid = ? ");
            	if(addToParamList){
            		sqlParams.add(selectedOrg);
            	}
            }
        }
        //cquinton 4/9/2013 Rel PR ir#15704 begin
        else {
            //selectedOrgOid is required. if not found return nothing
            sql.append(" and 1=0");
        }        
        //cquinton 4/9/2013 Rel PR ir#15704 end

     	if (TradePortalConstants.STATUS_ACTIVE.equalsIgnoreCase(instrStatusType) )
     	{
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
     	}

     	else if (TradePortalConstants.STATUS_INACTIVE.equalsIgnoreCase(instrStatusType) )
     	{
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT);
     		statuses.addElement(TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT);
     		statuses.addElement(TradePortalConstants.INSTRUMENT_STATUS_FILE_UPLOAD_REJECTED);
     		
     	 }else if (TradePortalConstants.STATUS_ALL.equals(instrStatusType)){
         	statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
             statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
      		 statuses.addElement(TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT);
      		 statuses.addElement(TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT);
      		 statuses.addElement(TradePortalConstants.INSTRUMENT_STATUS_FILE_UPLOAD_REJECTED);
         }

     	 numStatuses = statuses.size();
     
     	 if (numStatuses > 0)   	{
     		sql.append(" and i.instrument_status in (");
    	     for (int i=0; i<numStatuses; i++)
    	     {
    	    	 sql.append("?");
    	    	 if(addToParamList){
    	    		 sqlParams.add( (String) statuses.elementAt(i) );
             	}
    	    	 
	             if (i < numStatuses - 1)
	             {
	            	 sql.append(", ");
             	 }
         	 }

    	     sql.append(") ");
      	}

     	if (TradePortalConstants.INDICATOR_NO.equalsIgnoreCase(confInd))
    	{
     		 //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
     		sql.append(" and ( i.confidential_indicator = 'N' OR ");
     		sql.append(" i.confidential_indicator is null) ");
    	}



     	//Advance & Basic Search
            
            
            if ("A".equals(searchType)){
            	if (StringFunction.isNotBlank(instrumentType)){
                	sql.append(" and i.instrument_type_code= ? ");
                	if(addToParamList){
                		sqlParams.add(instrumentType);
                	}
            	}

            	if (StringFunction.isNotBlank(otherParty)){
            		sql.append(" and upper(p.name) like ? ");
            		if(addToParamList){
                		sqlParams.add("%"+otherParty.toUpperCase()+"%");
                	}
            	}

            	if (StringFunction.isNotBlank(currency)){
            		sql.append(" and o.copy_of_currency_code= ? ");
            		if(addToParamList){
                		sqlParams.add(currency);
                	}
            	}

            	if (StringFunction.isNotBlank(amountFrom)) {
            		amountFrom = amountFrom.trim();
            		try {
            			String amount = NumberValidator.getNonInternationalizedValue(amountFrom, getResourceManager().getResourceLocale());
            			if (!amount.equals("")) {
            				sql.append(" and i.copy_of_instrument_amount >=? ");  
            				if(addToParamList){
            					/* KMehta IR-T36000028449 Rel 9300 on 16-Jun-2015 Change - Begin*/
                        		sqlParams.add(amount);
                        	}
            			}
            		} catch (InvalidAttributeValueException e) {
            			try {
            				medService.getErrorManager().issueError(
            						TradePortalConstants.ERR_CAT_1,
            						TradePortalConstants.INVALID_CURRENCY_FORMAT,
            						amountFrom);
            			} catch (AmsException e1) {
            				// TODO Auto-generated catch block
            				e1.printStackTrace();
            			}


            		}
            	}
            	
            	

            	if (StringFunction.isNotBlank(amountTo)) {
            		amountTo = amountTo.trim();
            		try {
            			String amount = NumberValidator.getNonInternationalizedValue(amountTo, getResourceManager().getResourceLocale());
            			if (!amount.equals("")) {
            				sql.append(" and i.copy_of_instrument_amount <=? ");      
            				if(addToParamList){
            					/* KMehta IR-T36000028449 Rel 9300 on 16-Jun-2015 Change - Begin*/
                        		sqlParams.add(amount);
                        	}
            			}
            		} catch (InvalidAttributeValueException e) {
            			try {
            				medService.getErrorManager().issueError(
            						TradePortalConstants.ERR_CAT_1,
            						TradePortalConstants.INVALID_CURRENCY_FORMAT,
            						amountTo);
            			} catch (AmsException e1) {
            				// TODO Auto-generated catch block
            				e1.printStackTrace();
            			}
            		}
            	}
            	//Kyriba CR 268 - Add external Bank oid
                if (StringFunction.isNotBlank(externalBankOid)) {
                       sql.append(" and i.A_OP_BANK_ORG_OID=?");
                       if(addToParamList){
                   		sqlParams.add(externalBankOid);
                       }
                    }
                       
                if (StringFunction.isNotBlank(payDateFrom)) {
           		 sql.append(" and o.payment_date >= TO_DATE(?"); // DK IR T36000024022 Rel8.4 01/10/2014
           		 sql.append(",?)");
           		 if(addToParamList){
               		sqlParams.add(payDateFrom);
               		sqlParams.add(datePattern);
                 }
                }

                if( StringFunction.isNotBlank(payDateTo)) {
               	 sql.append(" and o.payment_date <= TO_DATE(?"); //  DK IR T36000024022 Rel8.4 01/10/2014
               	 sql.append(",?)");
               	 if(addToParamList){
               		sqlParams.add(payDateTo);
               		sqlParams.add(datePattern);
                 }
                }             
                   	
            	
            } else if ("S".equals(searchType)){

              if (StringFunction.isNotBlank(instrumentId)){
                 sql.append(" and upper(i.complete_instrument_id) like ? ");
                 if(addToParamList){
                		sqlParams.add("%"+instrumentId.toUpperCase()+"%");
                  }
              }
              if (StringFunction.isNotBlank(refNum)){
                 sql.append(" and upper(i.copy_of_ref_num) like ? ");
                 if(addToParamList){
                		sqlParams.add("%"+refNum.toUpperCase()+"%");
                  }
              }
              if (StringFunction.isNotBlank(instrumentType)){
          		sql.append(" and i.instrument_type_code=? ");
          		if(addToParamList){
               		sqlParams.add(instrumentType);
                 }
          	  }

           }  // end if-else (ADVANCED vs BASIC)
          

        return sql.toString();
    }


	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }


private String buildDynamicSQLCriteriaParent(Map<String,String> parameters){

		String sort=parameters.get("sort");
    	StringBuilder basicquery=new StringBuilder();
    	basicquery.append(outerSelect).append(" from ( ");
    	basicquery.append(outerSelect).append(" ,rownum row_num from ( ");
    	basicquery.append(innerSelect).append(innerFrom).append(innerWhere);
    	basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, true));

    	//Added for sorting in Grid
    	if(StringFunction.isNotBlank(sort)){

    		if ( sort.startsWith("-")) {
    			sort = sort.substring(1);
    			basicquery.append(" order by ").append(sort).append(" desc ");
            } else {
            	basicquery.append(" order by ").append(sort).append(" asc ");
            }
        }

    	basicquery.append(" ) ) where row_num > ? ").append(" and row_num <= ? ");

    	return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaParentCount(Map<String,String> parameters){

    	StringBuilder basicquery=new StringBuilder();
    	basicquery.append("select count(*) from( ");
    	basicquery.append(innerSelect).append(" ,rownum as row_num").append(innerFrom).append(innerWhere);
    	basicquery.append(buildDynamicSQLCriteriaParentFilter(parameters, false));
    	basicquery.append(" ) ");

    	return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChild(Map<String,String> parameters){

    	String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());
    	String innerWhereChild=" WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND " +
    			"a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND " +
    			"a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE " +
    			"a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid = ? )";
    	sqlParams.add(parentId);
    	sqlParams.add(parentId);
    	StringBuilder basicquery=new StringBuilder();
    	basicquery.append(outerSelectChild).append(" from (");
    	basicquery.append(innerSelectChild).append(" ,rownum as row_num").append(innerFromChild).append(innerWhereChild);
    	basicquery.append(" ) where row_num > ? ").append(" and row_num <= ? ");

    	return basicquery.toString();
    }

    private String buildDynamicSQLCriteriaChildCount(Map<String,String> parameters){

    	//String parentId = EncryptDecrypt.decryptStringUsingTripleDes(parameters.get("parentId"), getUserSession().getSecretKey());

    	String innerWhereChild=" WHERE a.p_instrument_oid = b.instrument_oid AND NOT (a.transaction_type_code = 'CHG' AND " +
    			"a.display_change_transaction = 'N') AND b.active_transaction_oid = active_tran.transaction_oid(+) AND " +
    			"a.transaction_oid  IN (SELECT original_transaction_oid AS transaction_oid FROM instrument WHERE " +
    			"a_related_instrument_oid = ? UNION SELECT transaction_oid FROM transaction WHERE p_instrument_oid =? )";

    	StringBuilder basicquery=new StringBuilder();
    	basicquery.append("select count(*) from( ");
    	basicquery.append(innerSelectChild).append(" ,rownum as row_num").append(innerFromChild).append(innerWhereChild);
    	basicquery.append(" ) ");

    	return basicquery.toString();
    }
  
        @Override
	protected boolean ignorePassedInParam(Map<String,String> parameters){
		return StringFunction.isNotBlank(parameters.get("parentId"));
		
	}

}
