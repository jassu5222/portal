/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;


/**
 *
 *
 */
public class PaymentMatchRemittanceDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PaymentMatchRemittanceDataView.class);
	/**
	 * Constructor
	 */
	public PaymentMatchRemittanceDataView () {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		StringBuilder sql = new StringBuilder();
		try {
			  String payRemitOid = parameters.get("payRemitOid");
			  
		      if(payRemitOid != null && payRemitOid.trim().length() > 0){
		    	  sql.append("and pay_remit.pay_remit_oid = ? ");
		      }


		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sql.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		String payRemitOid = parameters.get("payRemitOid");

		int varCount = 0;
		 if(payRemitOid != null && payRemitOid.trim().length() > 0){
			 statement.setString(startVarIdx+varCount, payRemitOid);
        	varCount++;	
		 }

		return varCount;
	}

}
