/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Vector;

import java.sql.SQLException;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class PaymentPendingTransactionsDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PaymentPendingTransactionsDataView.class);

    final static String ALL_WORK = "common.allWork";
    final static String MY_WORK = "common.myWork";
    final static String ALL_INSTRUMENTS = "common.all";

    /**
     * Constructor
     */
    public PaymentPendingTransactionsDataView() {
        super();
    }

    @Override
    protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
        StringBuilder dynamicWhereClause = new StringBuilder();

        Vector statuses = new Vector();
        int numStatuses = 0;
        String selectedStatus = SQLParamFilter.filter(parameters.get("selectedStatus"));
        String userOid = SQLParamFilter.filterNumber(parameters.get("userOid"));
        String userOrgOid = SQLParamFilter.filterNumber(parameters.get("userOrgOid"));
        //MEerupula Rel 8.4 IR-23915 
        String confInd = getConfidentialInd();
        if (selectedStatus != null) {
            if (TradePortalConstants.STATUS_READY.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.READY_TO_AUTHORIZE);
                statuses.addElement(TransactionStatus.AUTHORIZE_PENDING);
            } else if (TradePortalConstants.STATUS_PARTIAL.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.PARTIALLY_AUTHORIZED);
            } else if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.AUTHORIZE_FAILED);
            } else if (TradePortalConstants.STATUS_STARTED.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.STARTED);
            } else if (TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.REJECTED_BY_BANK);
            } else if (TransactionStatus.VERIFIED.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.VERIFIED);
            } else if (TransactionStatus.VERIFIED_PENDING_FX.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.VERIFIED_PENDING_FX);
            } else if (TransactionStatus.FX_THRESH_EXCEEDED.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.FX_THRESH_EXCEEDED);
            } else if (TransactionStatus.AUTH_PEND_MARKET_RATE.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.AUTH_PEND_MARKET_RATE);
            } //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start
            else if (TradePortalConstants.STATUS_READY_TO_CHECK.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.READY_TO_CHECK);
            } else if (TradePortalConstants.STATUS_REPAIR.equals(selectedStatus)) {
                statuses.addElement(TransactionStatus.REPAIR);
            } //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - End
            else // default is ALL
            {
                statuses.addElement(TransactionStatus.STARTED);
                statuses.addElement(TransactionStatus.AUTHORIZE_FAILED);
                statuses.addElement(TransactionStatus.AUTHORIZE_PENDING);
                statuses.addElement(TransactionStatus.PARTIALLY_AUTHORIZED);
                statuses.addElement(TransactionStatus.READY_TO_AUTHORIZE);
                statuses.addElement(TransactionStatus.TRANS_STATUS_REQ_AUTHENTICTN);   //IAZ CM-451 01/22/09
                statuses.addElement(TransactionStatus.REJECTED_BY_BANK);
                statuses.addElement(TransactionStatus.VERIFIED);
                statuses.addElement(TransactionStatus.VERIFIED_PENDING_FX);
                statuses.addElement(TransactionStatus.FX_THRESH_EXCEEDED);
                statuses.addElement(TransactionStatus.AUTH_PEND_MARKET_RATE);
                //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - Start
                statuses.addElement(TransactionStatus.READY_TO_CHECK);
                statuses.addElement(TransactionStatus.REPAIR);
              //Sandeep Rel-8.3 CR-821 Dev 05/28/2013 - End

                selectedStatus = TradePortalConstants.STATUS_ALL;
            }
        }
        // Build a where clause using the vector of statuses we just set up.
        numStatuses = statuses.size();
        if (numStatuses > 0) {
            dynamicWhereClause.append(" and a.transaction_status in (");
            for (int i = 0; i < numStatuses; i++) {
                dynamicWhereClause.append("'");
                dynamicWhereClause.append((String) statuses.elementAt(i));
                dynamicWhereClause.append("'");

                if (i < numStatuses - 1) {
                    dynamicWhereClause.append(", ");
                }
            }

            dynamicWhereClause.append(") ");
        }

        String selectedWorkflow = parameters.get("selectedWorkflow");

        if (selectedWorkflow != null) {
            selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, getUserSession().getSecretKey());
            if (selectedWorkflow.equals(MY_WORK)) {
                dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
                dynamicWhereClause.append(userOid);
            } else if (selectedWorkflow.equals(ALL_WORK)) {
                dynamicWhereClause.append("and b.a_corp_org_oid in (");
                dynamicWhereClause.append(" select organization_oid");
                dynamicWhereClause.append(" from corporate_org");
                dynamicWhereClause.append(" where activation_status = '");
                dynamicWhereClause.append(TradePortalConstants.ACTIVE);
                dynamicWhereClause.append("' start with organization_oid = ");
                dynamicWhereClause.append(userOrgOid);
                dynamicWhereClause.append(" connect by prior organization_oid = p_parent_corp_org_oid)");
            } else {
                dynamicWhereClause.append("and b.a_corp_org_oid = ");
                dynamicWhereClause.append(selectedWorkflow);
            }
        } //cquinton 4/9/2013 Rel PR ir#15704 begin
        else {
            //selectedWorkflow is required. if not found return nothing
            dynamicWhereClause.append(" and 1=0");
        }
        //cquinton 4/9/2013 Rel PR ir#15704 end

	    	      //CR-586 Vshah [BEGIN]
        //For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No),
        //on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
        if (TradePortalConstants.INDICATOR_NO.equals(confInd)) {
            dynamicWhereClause.append(" and ( t.confidential_indicator = 'N' OR ");
            dynamicWhereClause.append(" t.confidential_indicator is null) ");
        }

        return dynamicWhereClause.toString();
    }

    @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String, String> parameters)
            throws SQLException {
        int varCount = 0;

        return varCount;

    }
}
