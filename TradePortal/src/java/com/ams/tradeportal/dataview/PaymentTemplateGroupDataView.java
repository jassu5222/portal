/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class PaymentTemplateGroupDataView    extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PaymentTemplateGroupDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PaymentTemplateGroupDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {



        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();

        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if(includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }
        String where = "where p.p_owner_org_oid in (?,?";
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
        {
        	where = where +",?";
            sqlParams.add(clientBankID);
            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
                where = where +",?";
                sqlParams.add(bogID);
            }
        }

        // Possibly include the subsidiary access user org's data also
        if(includeSubAccessUserOrg){
            where +=",?";
            sqlParams.add(subAccessUserOrgID);
        }

        where = where +")";

        return where;
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
