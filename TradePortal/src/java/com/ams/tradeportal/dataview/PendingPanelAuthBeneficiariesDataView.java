/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 *
 */
public class PendingPanelAuthBeneficiariesDataView
    extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(PendingPanelAuthBeneficiariesDataView.class);
	/**
	 * Constructor
	 */
	public PendingPanelAuthBeneficiariesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	String dynamicWhere;
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
        dynamicWhere = " where p_transaction_oid = ? "
        			+ " and a_dmst_pmt_panel_auth_range in (select dmst_pmt_panel_auth_range_oid from dmst_pmt_panel_auth_range where p_transaction_oid = ? " 
        			+ " and a_panel_auth_range_oid = ? ) ";

        return dynamicWhere;
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	//jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
        String transactionOid = parameters.get("transactionOid");
        String panelAuthRangeOid = parameters.get("panelAuthRangeOid");
		return setSqlVarsToPreparedStmt(statement, startVarIdx, transactionOid, transactionOid, panelAuthRangeOid);
    }
}
