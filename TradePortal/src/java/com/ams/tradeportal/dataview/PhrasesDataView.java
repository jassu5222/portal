/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class PhrasesDataView    extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(PhrasesDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public PhrasesDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


        // W Zhu 10/24/2012 T36000006807 #333 get parameters internally to avoid parameter tampering
        String clientBankID = getUserSession().getClientBankOid();
        String bogID = getUserSession().getBogOid();
        String globalID = getUserSession().getGlobalOrgOid();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String ownershipLevel = getUserSession().getOwnershipLevel();
        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
        String subAccessUserOrgID = null;
        if(includeSubAccessUserOrg) {
            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
        }

        StringBuilder where = new StringBuilder("where p.p_owner_org_oid in (?,?");
        sqlParams.add(parentOrgID);
        sqlParams.add(globalID);

        if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG) || ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
        {
            where.append(",? " );
            sqlParams.add(clientBankID);
            if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)){
                where.append(",?");
                sqlParams.add(bogID);
            }
        }

        // Possibly include the subsidiary access user org's data also
        if(includeSubAccessUserOrg){
        	where.append(",?");
            sqlParams.add(subAccessUserOrgID);
        }

        where.append(")");

        return where.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
