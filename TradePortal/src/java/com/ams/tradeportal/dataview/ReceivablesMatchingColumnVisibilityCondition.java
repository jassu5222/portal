package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.BeanManager;

public class ReceivablesMatchingColumnVisibilityCondition extends AbstractVisibilityCondition {
private static final Logger LOG = LoggerFactory.getLogger(ReceivablesMatchingColumnVisibilityCondition.class);

	@Override
	
	public boolean execute(SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr) {
		boolean visible=false;

		CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		corpOrg.getById(userSession.getOwnerOrgOid());
		if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corpOrg.getAttribute("dual_auth_arm"))){
			visible=true;
		}

		return visible;
	}
	
}
