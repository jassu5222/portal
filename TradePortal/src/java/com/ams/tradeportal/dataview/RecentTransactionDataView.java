/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.agents.*;
import com.ams.tradeportal.busobj.*;

/**
 *
 *
 */
public class RecentTransactionDataView extends AbstractEJBDataView {
private static final Logger LOG = LoggerFactory.getLogger(RecentTransactionDataView.class);

    /**
     * Constructor
     */
    public RecentTransactionDataView() {
        super();
    }

    @Override
    protected DocumentHandler getDataFromEjbSource(Map<String, String> parameters) throws Exception {

        //String accountNo = null;
       // String currency = null;
        DocumentHandler xmlDoc = null;

        String userOID = getUserSession().getUserOid();
        String client_bankOID = getUserSession().getClientBankOid();
        String corp_oid = getUserSession().getOwnerOrgOid();
        String serverLocation = this.getFormManager().getServerLocation();

        String oid = parameters.get("oid");
        String acct_oid  = parameters.get("acct_oid");

        if (acct_oid != null) {
            acct_oid = EncryptDecrypt.decryptStringUsingTripleDes(acct_oid, getUserSession().getSecretKey());

        } else {
            if (oid != null && !("null".equals(oid))) {
                acct_oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, getUserSession().getSecretKey());
            }
        }
        if (StringFunction.isNotBlank(acct_oid)) {
            // Call the mediator
            ClientBank clientBank = (ClientBank) EJBObjectFactory.createClientEJB(serverLocation, "ClientBank");
            clientBank.getDataFromReferenceCache(Long.parseLong(client_bankOID));

            CorporateOrganization corporateOrganization = (CorporateOrganization) EJBObjectFactory.createClientEJB(serverLocation, "CorporateOrganization", Long.parseLong(corp_oid));

            Account account = (Account) EJBObjectFactory.createClientEJB(serverLocation, "Account", Long.parseLong(acct_oid));

            //accountNo = account.getAttribute("account_number");
            //currency = account.getAttribute("currency");

            AcctTransactionQuery tran_query = new AcctTransactionQuery();
            xmlDoc = tran_query.queryTransaction(userOID, clientBank, corporateOrganization, account);

        }

        return xmlDoc;
    }

}
