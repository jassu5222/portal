/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

/**
 *
 *
 */
public class RepairTransactionUsersDataView     extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(RepairTransactionUsersDataView.class);
	/**
	 * Constructor
	 */
	public RepairTransactionUsersDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {

    	StringBuilder dynamicWhere = new StringBuilder();
        String parentOrgID = getUserSession().getOwnerOrgOid();
        String userOid = getUserSession().getUserOid();
        dynamicWhere.append(" AND p_owner_org_oid = ").append(parentOrgID);
        dynamicWhere.append(" AND user_oid != ").append(userOid);// NAR IR-T36000021889 15 Oct 2013
        return dynamicWhere.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;



        return varCount;
    }
}
