package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;

/**
 * RequireAuthenticationFormatter writes out 'Y' or 'N' to specify
 * at runtime whether the given instrument transaction requires
 * re-authentication.
 */
public class RequireAuthenticationFormatter extends AbstractCustomDataViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(RequireAuthenticationFormatter.class);

	/**
	 * Format data.
	 * @param obj - assumes a string concatenation of
	 * instrument type and transaction type with underline (e.g. AIR_REL).
	 * @return 'Y' if authentication is required, 'N' otherwise.
	 */
    public String format(Object obj) {
        String formattedData = "N"; //default

        SessionWebBean userSession = getUserSession();
        Cache clientBankCache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
        DocumentHandler CBCResult = (DocumentHandler) clientBankCache.get(userSession.getClientBankOid());
        String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");

        String value = (String) obj;
        if (InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth,value)) {
            formattedData = "Y";
        }

        return formattedData;
    }
}
