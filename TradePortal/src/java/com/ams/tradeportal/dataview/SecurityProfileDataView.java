/**
 *
 */
package com.ams.tradeportal.dataview;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;

/**
 *
 *
 */
public class SecurityProfileDataView extends AbstractDBDataView {
	private static final Logger LOG = LoggerFactory.getLogger(SecurityProfileDataView.class);

	/**
	 * Constructor
	 */
	public SecurityProfileDataView() {
		super();
	}

	@Override
	protected String buildDynamicSQLCriteria(Map<String, String> parameters) {
		StringBuilder sql = new StringBuilder();
		try {
			String ownershipLevel = parameters.get("ownershipLevel");
			boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();

			sql.append(" and p_owner_org_oid in ( ?, ?");
			if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
				sql.append(", ?");
			}

			if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
				sql.append(", ?, ?");
			}

			// Possibly include the subsidiary access user org's data also
			if (includeSubAccessUserOrg) {
				sql.append(", ?");
			}
			sql.append(")");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sql.toString();
	}

	@Override
	protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx, Map<String, String> parameters) throws SQLException {

		int varCount = 0;
		try {

			// W Zhu 10/24/2012 T36000006807 #333 get parameters internally to avoid parameter tampering
			String clientBankID = getUserSession().getClientBankOid();
			String bogID = getUserSession().getBogOid();
			String globalID = getUserSession().getGlobalOrgOid();
			String parentOrgID = getUserSession().getOwnerOrgOid();
			String ownershipLevel = getUserSession().getOwnershipLevel();
			boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
			String subAccessUserOrgID = null;
			if (includeSubAccessUserOrg) {
				subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
			}

			statement.setString(startVarIdx + varCount, parentOrgID);
			varCount++;
			statement.setString(startVarIdx + varCount, globalID);
			varCount++;

			if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
				statement.setString(startVarIdx + varCount, clientBankID);
				varCount++;
			}
			if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
				statement.setString(startVarIdx + varCount, clientBankID);
				varCount++;
				statement.setString(startVarIdx + varCount, bogID);
				varCount++;
			}
			if (includeSubAccessUserOrg) {
				statement.setString(startVarIdx + varCount, subAccessUserOrgID);
				varCount++;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return varCount;
	}

}
