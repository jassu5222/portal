/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import com.amsinc.ecsg.util.EncryptDecrypt;


/**
 *
 *
 */
public class StructureCreationRuleDataView extends AbstractDBDataView  {
private static final Logger LOG = LoggerFactory.getLogger(StructureCreationRuleDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public StructureCreationRuleDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		StringBuilder sql = new StringBuilder();
		try {

                        // W Zhu 10/24/2012 T36000006807 #333 get parameters internally to avoid parameter tampering
                        String parentOrgID = getUserSession().getOwnerOrgOid();
                        boolean includeSubAccessUserOrg = getUserSession().showOrgDataUnderSubAccess();
                        String subAccessUserOrgID = null;
                        if(includeSubAccessUserOrg) {
                            subAccessUserOrgID = getUserSession().getSavedUserSession().getOwnerOrgOid();
                        }


			String ruletype =  parameters.get("ruletype");
			String podef = parameters.get("podef");
			if (ruletype != null && ruletype.trim().length() > 0){
				sql.append(" and upper(l.instrument_type_code) = ? ");
				sqlParams.add(ruletype.toUpperCase());
			}
			if (podef != null && podef.trim().length() > 0){
				sql.append(" and p.purchase_order_definition_oid =? ");
				 sqlParams.add(EncryptDecrypt.decryptStringUsingTripleDes(podef, getUserSession().getSecretKey()));
			}

			sql.append(" and l.a_owner_org_oid in (? ");
			sqlParams.add(parentOrgID);

		      // Possibly include the subsidiary access user org's data also
		      if(includeSubAccessUserOrg){
		    	  sql.append(",");
		    	  sqlParams.add(subAccessUserOrgID);
		      }

		      sql.append(")");



		} catch (Exception ex) {
			ex.printStackTrace();
		}
				return sql.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {

		return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
	}
}
