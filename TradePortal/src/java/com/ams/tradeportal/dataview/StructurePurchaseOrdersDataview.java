/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

/**
 *
 *
 */
public class StructurePurchaseOrdersDataview     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(StructurePurchaseOrdersDataview.class);

	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public StructurePurchaseOrdersDataview() {
		super();
	}

        @Override
    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();
      
        String poNumber = parameters.get("poNumber");
        String beneName = parameters.get("beneName");
        
        if (beneName == null) {
        	beneName = "";            
        }
        else {
        	beneName = beneName.toUpperCase(); 
        }

        if (poNumber == null) {
            poNumber = "";
        }
        else {
            poNumber = poNumber.toUpperCase();
        }

        if(poNumber == null)poNumber="";
        if(beneName == null)beneName="";

        sql.append("and p.a_owner_org_oid = ? ");
        sqlParams.add(parameters.get("userOrgOid"));

        // Search by the PO Number

        if (!poNumber.equals("") && beneName.equals("")) {
        	sql.append(" and upper(purchase_order_num) like ? ");
        	sqlParams.add("%" + poNumber+"%");
        }

     // Search by the PO Number and the Beneficiary Name
        if (!poNumber.equals("") && !beneName.equals("")) {
        	sql.append(" and upper(purchase_order_num) like ? and upper(seller_name) like ? ");
        	sqlParams.add("%" + poNumber+"%");
        	sqlParams.add("%" + beneName+"%");
        }

        // Search by the Beneficiary Name
        if (poNumber.equals("") && !beneName.equals("")) {
        	sql.append(" and upper(seller_name) like ? ");
        	sqlParams.add("%" + beneName+"%");
        }

        // Build the where clause - All fields are empty
        if (poNumber.equals("") &&  beneName.equals("")) {
        	sql.append("");
        }


        return sql.toString();
    }

        @Override
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
