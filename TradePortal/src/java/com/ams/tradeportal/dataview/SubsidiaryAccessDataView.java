/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.TradePortalConstants;

/**

 *
 */
public class SubsidiaryAccessDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(SubsidiaryAccessDataView.class);

	private final List<Object> sqlParams = new ArrayList<>();

	/**
	 * Constructor
	 */
	public SubsidiaryAccessDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        sql.append(" AND organization_oid <> ?"); //ownerOrgOid
        sql.append(" AND organization_oid in (");
        sql.append("    select organization_oid");
        sql.append("      from corporate_org");
        sql.append("     where activation_status = ? START WITH organization_oid = ?"); //ownerOrgOid
        sql.append(" CONNECT BY PRIOR organization_oid = p_parent_corp_org_oid)");
        
        sqlParams.add(this.getOwnerOrgOid());
        sqlParams.add(TradePortalConstants.ACTIVE);
        sqlParams.add(this.getOwnerOrgOid());

        String orgName = parameters.get("name");
        if (orgName!=null && orgName.length()>=0) {
            sql.append(" and upper(name) like ? ");
            sqlParams.add("%" + orgName.toUpperCase() + "%");
        }

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {

    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
}
