/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class TradeSearchDataView     extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(TradeSearchDataView.class);
	private final List<Object> sqlParams = new ArrayList<>();
	/**
	 * Constructor
	 */
	public TradeSearchDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
        StringBuilder sql = new StringBuilder();

        String confInd  = getConfidentialInd();
    	
        String status = "";
        String showChildInstruments = parameters.get("showChildInstruments");
        String userOrgOid = getUserSession().getOwnerOrgOid();
        String statusActive = parameters.get("statusActive");
        String statusInactive = parameters.get("statusInactive");
        String instrumentType = parameters.get("instrumentType");
        String currency = parameters.get("currency");
        String amountFrom = parameters.get("amountFrom");
        String amountTo = parameters.get("amountTo");
        String dateFrom = parameters.get("dateFrom");
        String dateTo = parameters.get("dateTo");
        String instrumentId = parameters.get("instrumentId");
        String refNum = parameters.get("refNum");
        String bankRefNum = parameters.get("bankRefNum");
        String vendorId = parameters.get("vendorId");
        String otherParty = parameters.get("otherParty");
        String custID = parameters.get("custID");
        String searchType = parameters.get("searchType");
        String createAmendmentFlag = parameters.get("createAmendmentFlag");
        String createAssignmentFlag = parameters.get("createAssignmentFlag");
        String createTracerFlag = parameters.get("createTracerFlag");
        String createSettlementInstructionsFlag = parameters.get("createSettlementInstructionsFlag");
        String requestRolloverFlag = parameters.get("requestRolloverFlag");
        boolean allowImpCol =  "Y".equals(parameters.get("allowImpCol"));
        boolean allowImportDLC = "Y".equals(parameters.get("allowImportDLC"));
        boolean allowLoanRequest =  "Y".equals(parameters.get("allowLoanRequest"));
        String transferExportLCFlag = parameters.get("transferExportLCFlag");
        boolean isInstrIDSearch = Boolean.parseBoolean(parameters.get("instrumentIDSearchFlag"));
        String tradeInstrTypes = parameters.get("tradeInstrTypes");
        String addEXPOCO = parameters.get("addEXPOCO");
		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - added below param
        String conversionCenterFlag = parameters.get("conversionCenter");
        String bankInstrumentId = parameters.get("bankInstrumentId");
      //parameter check start
        if(confInd == null)confInd="";
        if(statusActive == null)statusActive="";
        if(statusInactive == null)statusInactive="";
        if(instrumentType == null || instrumentType.equals(" "))instrumentType="";
        if(currency == null || currency.equals(" "))currency="";
        if(amountFrom == null)amountFrom="";
        if(amountTo == null)amountTo="";
        if(dateFrom == null)dateFrom="";
        if(dateTo == null)dateTo="";
        if(instrumentId == null)instrumentId="";
        if(refNum == null)refNum="";
        if(bankRefNum == null)bankRefNum="";
        if(vendorId == null)vendorId="";
        if(otherParty == null)otherParty="";

        if(searchType == null)searchType="";
        if(showChildInstruments == null)showChildInstruments="";
        if(createAssignmentFlag == null)createAssignmentFlag="";
        if(createAmendmentFlag == null)createAmendmentFlag="";
        if(createTracerFlag == null)createTracerFlag="";
        if(createSettlementInstructionsFlag == null) createSettlementInstructionsFlag="";
        if(requestRolloverFlag == null) requestRolloverFlag="";
        if(transferExportLCFlag == null)transferExportLCFlag="";
        if(tradeInstrTypes == null)tradeInstrTypes="";
        if(addEXPOCO == null)addEXPOCO="";
		if(conversionCenterFlag == null)conversionCenterFlag="";
		if(bankInstrumentId == null)bankInstrumentId="";
		
      //parameter check end

      //constructing the string of instruments types which is passing as a set of types to be filtered.
      //These instrument types are prepared based on user security/access rights and sent to this place as a req pereameter from jsp.
        //tradeInstrTypes = tradeInstrTypes.replaceAll(",", "','");
        //tradeInstrTypes = "'"+tradeInstrTypes+"'";
		
		
        //if called from template creation then display payment instruments also, if not just trade instruments
        String isFromTemplateCreation = parameters.get("isFromTemplateCreation");
        if(!"Y".equals(isFromTemplateCreation)){
        	sql.append("and i.instrument_type_code not in ('FTBA','FTDP','FTRQ')");
        }
        if (statusActive.equals("") && statusInactive.equals("")){
        	sql.append(" and i.instrument_status not in (");
            sql.append("'ACT', 'PND', 'EXP','CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC'");
            sql.append(")");
        }else if(TradePortalConstants.STATUS_ACTIVE.equals(statusActive) && TradePortalConstants.STATUS_INACTIVE.equals(statusInactive)){
        	if("Y".equals(createAssignmentFlag)){
	        	sql.append(" and i.instrument_status in (");
	            sql.append("'ACT', 'EXP'");
	            sql.append(")");
        	} else if ("Y".equals(createSettlementInstructionsFlag) || "Y".equals(requestRolloverFlag)) {
        		sql.append(" and i.instrument_status in (");
	            sql.append("'ACT'");
	            sql.append(")");
        	}else{
        		sql.append(" and i.instrument_status in (");
                sql.append("'ACT', 'PND', 'EXP','CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC'");
                sql.append(")");
        	}
        }else if(TradePortalConstants.STATUS_ACTIVE.equals(statusActive)) {
        	if("Y".equals(createAssignmentFlag)){
	        	sql.append(" and i.instrument_status in (");
	            sql.append("'ACT', 'EXP'");
	            sql.append(")");
        	} else if ("Y".equals(createSettlementInstructionsFlag) || "Y".equals(requestRolloverFlag)) {
        		sql.append(" and i.instrument_status in (");
	            sql.append("'ACT'");
	            sql.append(")");
        	} else{
        		sql.append(" and i.instrument_status in (");
        		sql.append("'ACT', 'PND', 'EXP'");
                sql.append(")");
        	}
        }else if(TradePortalConstants.STATUS_INACTIVE.equals(statusInactive)) {
        	if("Y".equals(createAssignmentFlag)){
        		sql.append(" and i.instrument_status not in (");
                sql.append("'ACT', 'PND', 'EXP','CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC'");
                sql.append(")");
        	}else{
        		sql.append(" and i.instrument_status in (");
            	sql.append("'CAN', 'CLO', 'DEA', 'LIQ', 'LQB', 'LQC'");
            	sql.append(")");
        	}
        }
        if(StringFunction.isNotBlank(custID)){
        	custID = EncryptDecrypt.decryptStringUsingTripleDes(custID, getUserSession().getSecretKey());
        	userOrgOid = custID;
        }

        //SPenke T36000006422 10/15/2012 Begin
        if(showChildInstruments.equals(TradePortalConstants.INDICATOR_YES)){
          // Append the dynamic where clause to view the instruments of the user's
          // organizations and all of its children.
        	sql.append(" and i.a_corp_org_oid in ( ");
        	sql.append(" select organization_oid ");
        	sql.append(" from corporate_org ");
        	sql.append(" start with organization_oid = ?  ");
        	sqlParams.add(userOrgOid);
        	sql.append(" connect by prior organization_oid = p_parent_corp_org_oid ");
        	sql.append(" ) ");
        } else {
          // Append the dynamic where clause to view only the user
          // organization's instruments
    	   sql.append(" and i.a_corp_org_oid = ? ");
    	   sqlParams.add(userOrgOid);
        }
      
        if (!"".equals(status)){
        	sql.append(" and i.instrument_status in (" + prepareDynamicSqlStrForInClause(status, sqlParams)+ ") ");
        }

        if(!"".equals(confInd) && TradePortalConstants.INDICATOR_NO.equals(confInd)){
			sql.append(" and i.confidential_indicator = ?");
			sqlParams.add(TradePortalConstants.INDICATOR_NO);
        }
		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - start
		if (StringFunction.isNotBlank(conversionCenterFlag) && TradePortalConstants.INDICATOR_YES.equals(conversionCenterFlag)){
			
        		sql.append(" and i.converted_transaction_ind = ? ");
        		sqlParams.add(TradePortalConstants.INDICATOR_YES);
		}
		//Srinivasu_D CR#269 Rel8.4 09/02/2013 - end
         //SPenke T36000006422 10/15/2012 Begin
        if (!"".equals(searchType) && searchType.equals("Advanced")){
        	if (!"".equals(currency)){
        		sql.append(" and o.copy_of_currency_code = ? ");
        		sqlParams.add(currency);
        	}

        	if (!"".equals(amountFrom)){
        		sql.append(" and i.copy_of_instrument_amount >=? ");
        		sqlParams.add(amountFrom);
        	}

        	if (!"".equals(amountTo)){
        		sql.append(" and i.copy_of_instrument_amount <=? ");
        		sqlParams.add(amountTo);
        	}

        	if (!"".equals(dateFrom)) {
        		sql.append(" and i.copy_of_expiry_date >= TO_DATE(?,'mm/dd/yyyy')");
        		sqlParams.add(dateFrom);
        	}
        	if (!"".equals(dateTo)) {
        		sql.append(" and i.copy_of_expiry_date <= TO_DATE(?,'mm/dd/yyyy')");
        		sqlParams.add(dateTo);
        	}

        	if (!"".equals(otherParty)){
        		sql.append(" and upper(p.name) like ? ");
        		sqlParams.add("%" + otherParty.toUpperCase()+"%");
        	}

        	if (!"".equals(vendorId)){
        		sql.append(" and upper(i.vendor_id) like ? ");
        		sqlParams.add("%" + vendorId.toUpperCase()+"%");
        	}
        	
        	
        	if(("737542").equals(userOrgOid) || ("737553").equals(userOrgOid)){
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){
        			if(!"Y".equals(createTracerFlag)){
        				
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams) );
	        			sql.append(")");
        			}else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
        			
        			if(!isInstrIDSearch){
	        			if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && !"Y".equals(createSettlementInstructionsFlag) && !"Y".equals(requestRolloverFlag)){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	}
        	else {
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){
        			if ("Y".equals(createSettlementInstructionsFlag) || "Y".equals(requestRolloverFlag)) {
        				if ( allowImpCol || allowImportDLC || allowLoanRequest ) { 
        				  if ( "Y".equals(createSettlementInstructionsFlag) ) {
            				sql.append(" and i.instrument_type_code in (");
        					if ( allowImpCol ) {
        						sql.append("'IMC'");
        					}
        					if ( allowImportDLC ) {
        						if(allowImpCol) {
        						  sql.append(", ");
        						}
        						sql.append("'DBA', 'DFP'");
        					}
        					if (allowLoanRequest) {
        						if ( allowImpCol || allowImportDLC ) {
        							sql.append(", ");
        						}
        						sql.append("'LRQ'");
        					}
        					sql.append(")");
        				  } else {
        					  if ( allowLoanRequest ) {
                				sql.append(" and i.instrument_type_code in (");
        					    sql.append("'LRQ'");
        					    sql.append(")");
        					  } else {
        						  sql.append(" and i.instrument_type_code is null ");
        					  }
        				  }	        			  
        			   } else {
        				   sql.append(" and i.instrument_type_code is null ");
        			   }
                	} else if(!"Y".equals(createTracerFlag)){
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams));
	        			sql.append(")");
        			} else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
        			
        			if(!isInstrIDSearch){
	        			if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && 
	        					!"Y".equals(createSettlementInstructionsFlag) && !"Y".equals(requestRolloverFlag)){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	} //SPenke T36000006422 10/15/2012 End
        	if(InstrumentType.EXPORT_DLC.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code = ? ");
            	sqlParams.add(instrumentType);
            	
            	if("Y".equals(transferExportLCFlag)){
            		//sql.append(" AND i.instrument_oid IN ");
            		//sql.append(" (SELECT p_instrument_oid FROM transaction WHERE p_instrument_oid    = i.instrument_oid AND transaction_type_code = 'ADV') ");
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - Begin*/  
            		sql.append(" AND ((t.c_bank_release_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y'))");
            		sql.append(" OR (t.c_cust_enter_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y')))");
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - End*/
            	}else if(!"Y".equals(transferExportLCFlag) && !"Y".equals(createAssignmentFlag) && !isInstrIDSearch){
            		sql.append(" AND o.c_CUST_ENTER_TERMS_OID  IS NOT NULL ");
            	}
            }
        	else if(TradePortalConstants.ALL_EXPORT_DLC.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code='"+InstrumentType.EXPORT_DLC+"'");
        	}
        	else if(InstrumentType.EXPORT_COL.equals(instrumentType)){
        		if (!TradePortalConstants.INDICATOR_YES.equals(addEXPOCO)){
        			sql.append(" and i.instrument_type_code = ?");
        			sqlParams.add(instrumentType);
        		}
            }
        	else if(TradePortalConstants.IMPORT.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code in (");
            	sql.append("'IMP_DLC', 'SHP', 'IMC', 'AIR', 'ATP'");
            	sql.append(")");
            }
        	else {
        		if(!TradePortalConstants.SEARCH_ALL_INSTRUMENTS.equals(instrumentType) && !"".equals(instrumentType)){
        			sql.append(" and i.instrument_type_code = ? ");
        			sqlParams.add(instrumentType);
        		}
        	}        	        	


        }else if (!"".equals(searchType) && searchType.equals("Basic")){
        	if (!"".equals(instrumentId)){
        		sql.append(" and upper(i.complete_instrument_id) like ? ");
        		sqlParams.add("%"+instrumentId.toUpperCase()+"%");
        	}

        	if (!"".equals(refNum)){
        		sql.append(" and upper(i.copy_of_ref_num) like ? ");
        		sqlParams.add("%" + refNum.toUpperCase()+"%");
        	}

        	if (!"".equals(bankRefNum)){
        		sql.append(" and upper(i.opening_bank_ref_num) like ? ");
        		sqlParams.add("%" + bankRefNum.toUpperCase()+"%");
        	}

        	if (!"".equals(vendorId)){
        		sql.append(" and upper(i.vendor_id) like ? ");
        		sqlParams.add("%" + vendorId.toUpperCase()+"%");
        	}
        	if (StringFunction.isNotBlank(bankInstrumentId)){
        		sql.append(" and upper(i.bank_instrument_id) like ? ");
        		sqlParams.add("%"+bankInstrumentId.toUpperCase()+"%");
        	}
        	
        	//SPenke T36000006422 10/15/2012 Begin
        	if(("737542").equals(userOrgOid) || ("737553").equals(userOrgOid)){
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){
        			if(!"Y".equals(createTracerFlag)){
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams));
	        			sql.append(")");
        			}else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
        			
        			if(!isInstrIDSearch){
	        			if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && !"Y".equals(createSettlementInstructionsFlag) && !"Y".equals(requestRolloverFlag)){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	} else {
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){
        			if(!"Y".equals(createTracerFlag)){
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams));
	        			sql.append(")");
        			}else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
        			
        			if(!isInstrIDSearch){
	        			if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && !"Y".equals(createSettlementInstructionsFlag) && !"Y".equals(requestRolloverFlag) ){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	} //SPenke T36000006422 10/15/2012 End
        	
        	if(InstrumentType.EXPORT_DLC.equals(instrumentType)){
        		sql.append(" and i.instrument_type_code=? ");
        		sqlParams.add(instrumentType);

        		if("Y".equals(transferExportLCFlag)){
/*            		sql.append(" AND i.instrument_oid IN ");
            		sql.append(" (SELECT p_instrument_oid FROM transaction WHERE p_instrument_oid    = i.instrument_oid AND transaction_type_code = 'ADV') "); */
        			/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - Begin*/  
            		sql.append(" AND ((t.c_bank_release_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y'))");
            		sql.append(" OR (t.c_cust_enter_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y')))");
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - End*/
        		}else if(!"Y".equals(transferExportLCFlag) && !"Y".equals(createAssignmentFlag) && !isInstrIDSearch){
            		sql.append(" AND o.c_CUST_ENTER_TERMS_OID  IS NOT NULL ");
            	}
            }
        	else if(TradePortalConstants.ALL_EXPORT_DLC.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code = ? ");
            	sqlParams.add(InstrumentType.EXPORT_DLC);
        	}
        	else if(InstrumentType.EXPORT_COL.equals(instrumentType)){
        		if (!TradePortalConstants.INDICATOR_YES.equals(addEXPOCO)){
        			sql.append(" and i.instrument_type_code = ? ");
        			sqlParams.add(instrumentType);
        		}
            }
        	else if(TradePortalConstants.IMPORT.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code in (");
            	sql.append("'IMP_DLC', 'SHP', 'IMC', 'AIR', 'ATP'");
            	sql.append(")");
            }
        	else {
        		if(!TradePortalConstants.SEARCH_ALL_INSTRUMENTS.equals(instrumentType) && !"".equals(instrumentType)){
        			sql.append(" and i.instrument_type_code= ? ");
        			sqlParams.add(instrumentType);
        		}
        	}       	
        	//SPenke T36000006422 10/15/2012 Begin
        }else if (!"".equals(searchType) && searchType.equals("Bank")){
        	if (!"".equals(instrumentId)){
        		sql.append(" and upper(i.complete_instrument_id) like ? ");
        		sqlParams.add("%"+instrumentId.toUpperCase()+"%");
        	}

        	if (StringFunction.isNotBlank(bankInstrumentId)){
        		sql.append(" and upper(i.bank_instrument_id) like ? ");
        		sqlParams.add("%"+bankInstrumentId.toUpperCase()+"%");
        	}
        	
        	
        	if(InstrumentType.EXPORT_DLC.equals(instrumentType)){
        		sql.append(" and i.instrument_type_code=? ");
        		sqlParams.add(instrumentType);

        		if("Y".equals(transferExportLCFlag)){
            	/*	sql.append(" AND i.instrument_oid IN ");
            		sql.append(" (SELECT p_instrument_oid FROM transaction WHERE p_instrument_oid    = i.instrument_oid AND transaction_type_code = 'ADV') ");*/
        			/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - Begin*/  
            		sql.append(" AND ((t.c_bank_release_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y'))");
            		sql.append(" OR (t.c_cust_enter_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y')))");
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - End*/
        		}else if(!"Y".equals(transferExportLCFlag) && !"Y".equals(createAssignmentFlag) && !isInstrIDSearch){
            		sql.append(" AND o.c_CUST_ENTER_TERMS_OID  IS NOT NULL ");
            	}
            }
        	else if(TradePortalConstants.ALL_EXPORT_DLC.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code = ? ");
            	sqlParams.add(InstrumentType.EXPORT_DLC);
        	}
        	else if(InstrumentType.EXPORT_COL.equals(instrumentType)){
        		if (!TradePortalConstants.INDICATOR_YES.equals(addEXPOCO)){
        			sql.append(" and i.instrument_type_code = ? ");
        			sqlParams.add(instrumentType);
        		}
            }
        	else if(TradePortalConstants.IMPORT.equals(instrumentType)){
            	sql.append(" and i.instrument_type_code in (");
            	sql.append("'IMP_DLC', 'SHP', 'IMC', 'AIR', 'ATP'");
            	sql.append(")");
            }
        	else {
        		if(!TradePortalConstants.SEARCH_ALL_INSTRUMENTS.equals(instrumentType) && !"".equals(instrumentType)){
        			sql.append(" and i.instrument_type_code= ? ");
        			sqlParams.add(instrumentType);
        		}
        	}       	
        } else if("".equals(searchType)){
        	if(("737542").equals(userOrgOid) || ("737553").equals(userOrgOid)){
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){        	
        			if(!"Y".equals(createTracerFlag)){
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams));
	        			sql.append(")");
        			}else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
		        	
        			if(!isInstrIDSearch){
			        	if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && !"Y".equals(createSettlementInstructionsFlag) && !"Y".equals(requestRolloverFlag)){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	//SPenke T36000006422 10/15/2012 End
        	} else {
        		if(!InstrumentType.EXPORT_DLC.equals(instrumentType)){
        			if ("Y".equals(createSettlementInstructionsFlag) || "Y".equals(requestRolloverFlag)) {
        				if ( allowImpCol || allowImportDLC || allowLoanRequest ) {          				 
          				  if ( "Y".equals(createSettlementInstructionsFlag) ) {
          					sql.append(" and i.instrument_type_code in (");
          					if ( allowImpCol ) {
          						sql.append("'IMC'");
          					}
          					if ( allowImportDLC ) {
          						if(allowImpCol) {
          						  sql.append(", ");
          						}
          						sql.append("'DBA', 'DFP'");
          					}
          					if (allowLoanRequest) {
          						if ( allowImpCol || allowImportDLC ) {
          							sql.append(", ");
          						}
          						sql.append("'LRQ'");
          					}
          					sql.append(")");
          				  } else {
          					  if ( allowLoanRequest ) {
          						sql.append(" and i.instrument_type_code in (");
          					    sql.append("'LRQ'");
          					    sql.append(")");
          					  } else {
          						  sql.append(" and i.instrument_type_code is null ");
          					  }
          				  }  	        			  
          			   } else {
          				   sql.append(" and i.instrument_type_code is null ");
          			   }
        			} else if(!"Y".equals(createTracerFlag)){
	        			sql.append(" and i.instrument_type_code in (");
	        			//sql.append("'AIR', 'EXP_COL', 'EXP_OCO', 'GUA', 'IMP_DLC', 'SLC', 'EXP_DLC', 'SHP', 'LRQ', 'RQA', 'ATP', 'FTRQ'");
	        			sql.append(prepareDynamicSqlStrForInClause(tradeInstrTypes, sqlParams));
	        			sql.append(")");
        			}else if("Y".equals(createTracerFlag)){
        				sql.append(" and i.instrument_type_code in (");
	        			sql.append("'EXP_COL', 'EXP_OCO'");
	        			sql.append(")");
        			}
		        	
        			if(!isInstrIDSearch){
			        	if(!"Y".equals(createAmendmentFlag) && !"Y".equals(createTracerFlag) && !"Y".equals(createSettlementInstructionsFlag) 
			        			&& !"Y".equals(requestRolloverFlag)){
	        				sql.append(" and o.c_CUST_ENTER_TERMS_OID is not null ");
	        			}else{
	        				sql.append(" AND i.instrument_status       <> 'PND' ");
	        				sql.append(" AND i.instrument_status       <> 'DEL' ");
	        				sql.append(" AND i.instrument_status       <> 'CAN' ");
	        			}
        			}
        		}
        	} 
        	if(InstrumentType.EXPORT_DLC.equals(instrumentType)){
        		sql.append(" and i.instrument_type_code = ? ");
        		sqlParams.add(instrumentType);
        		
        		
        		if("Y".equals(transferExportLCFlag)){
            		/*sql.append(" AND i.instrument_oid IN ");
            		sql.append(" (SELECT p_instrument_oid FROM transaction WHERE p_instrument_oid    = i.instrument_oid AND transaction_type_code = 'ADV') ");*/
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - Begin*/  
            		sql.append(" AND ((t.c_bank_release_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y'))");
            		sql.append(" OR (t.c_cust_enter_terms_oid IN");
            		sql.append("(SELECT m.terms_oid FROM terms m WHERE m.transferrable = 'Y')))");
            		/* KMehta IR-T36000021952 Rel 9400 on 10-Sep-2015 Add - End*/
        		}else if(!"Y".equals(transferExportLCFlag) && !"Y".equals(createAssignmentFlag) && !isInstrIDSearch){
            		sql.append(" AND o.c_CUST_ENTER_TERMS_OID  IS NOT NULL ");
            	}
        	} else if(TradePortalConstants.ALL_EXPORT_DLC.equals(instrumentType)){
        		sql.append(" and i.instrument_type_code = ?");
        		sqlParams.add(InstrumentType.EXPORT_DLC);
        	} else if(InstrumentType.EXPORT_COL.equals(instrumentType)){
        		if (!TradePortalConstants.INDICATOR_YES.equals(addEXPOCO)){
        			sql.append(" and i.instrument_type_code=? ");
        			sqlParams.add(instrumentType);
        		}
        	}else if(InstrumentType.NEW_EXPORT_COL.equals(instrumentType)){
        		sql.append(" and i.instrument_type_code=? ");
        		sqlParams.add(instrumentType);
        	}else if(TradePortalConstants.IMPORT.equals(instrumentType)){
	        	sql.append(" and i.instrument_type_code in (");
	        	sql.append("'IMP_DLC', 'SHP', 'IMC', 'AIR', 'ATP'");
	        	sql.append(")");
        	}
        }

        return sql.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
    	return setSqlVarsToPreparedStmt(statement, startVarIdx, sqlParams);
    }
    
    
     
}
