/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;
import java.sql.SQLException;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class TransactionFTDPISSDataView extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(TransactionFTDPISSDataView.class);
	/**
	 * Constructor
	 */
	public TransactionFTDPISSDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


    	StringBuilder dynamicWhereClause = new StringBuilder();


        String transaction_oid = SQLParamFilter.filterNumber(parameters.get("transaction_oid"));
        String benificiaryName = SQLParamFilter.filter(parameters.get("benificiaryName"));
        String benificiaryBankName = SQLParamFilter.filter(parameters.get("benificiaryBankName"));
        String paymentStatusSearch = SQLParamFilter.filter(parameters.get("paymentStatusSearch"));
        String amount = SQLParamFilter.filterNumber(parameters.get("amount"));

        dynamicWhereClause.append ("and dp.p_transaction_oid = ");
 		dynamicWhereClause.append(transaction_oid);

        dynamicWhereClause.append(" and (1=1");

        if (StringFunction.isNotBlank(benificiaryName)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("upper(dp.payee_name) like '%");
           dynamicWhereClause.append(benificiaryName);
           dynamicWhereClause.append("%'"); //"%'" is safe for URLdecoding.
        }

        if (StringFunction.isNotBlank(benificiaryBankName)) {
           dynamicWhereClause.append(" and ");
           //dynamicWhereClause.append("upper(dp.payee_bank_name) like '");//BSL IR RSUL060252944 06/30/11 DELETE
           dynamicWhereClause.append("upper(dp.payee_bank_name) like '%"); //BSL IR RSUL060252944 06/30/11 ADD
           dynamicWhereClause.append(benificiaryBankName);
           dynamicWhereClause.append("%'");
        }

        if (StringFunction.isNotBlank(paymentStatusSearch)) {
           dynamicWhereClause.append(" and ");
           //BSL IR LMUL052437851 05/31/11 BEGIN - paymentStatusSearch value 'REJECTED' should also match 'REJECTED_UNPAID'
           //dynamicWhereClause.append("dp.payment_status='");
           dynamicWhereClause.append("dp.payment_status like '");
           dynamicWhereClause.append(paymentStatusSearch);
           //dynamicWhereClause.append("'");
           dynamicWhereClause.append("%'");
           //BSL IR LMUL052437851 05/31/11 END
        }

        if (StringFunction.isNotBlank(amount)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("dp.amount='");
           dynamicWhereClause.append(amount);
           dynamicWhereClause.append("'");
        }

        dynamicWhereClause.append(")");

        String countSQL;
        countSQL = buildCountQuery(dynamicWhereClause);
        super.fullCountSQL = countSQL;

        return dynamicWhereClause.toString();
    }

    private String buildCountQuery(StringBuilder dynamicWhereClause) {
    	String selectClause, fromClause, whereClause;
    	StringBuilder countSQL = new StringBuilder();
    	
    	selectClause = " SELECT COUNT(dp.domestic_payment_oid) AS COUNT ";
    	fromClause = " from domestic_payment dp, dmst_pmt_panel_auth_range dppar, domestic_payment sdp, invoice_details invd ";
    	whereClause = " where dp.a_dmst_pmt_panel_auth_range = dppar.dmst_pmt_panel_auth_range_oid(+) " +  
    		      " and  dp.source_template_dp_oid = sdp.domestic_payment_oid(+) " +
    		      " and  sdp.domestic_payment_oid = invd.domestic_payment_oid (+) ";
    	
        countSQL.append(selectClause).append(fromClause).append(whereClause).append(dynamicWhereClause);
        return countSQL.toString();
	}
    
    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;



        return varCount;
    }
}
