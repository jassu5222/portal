/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**

 *
 */
public class TransactionHistoryLogDataView extends AbstractDBDataView {
private static final Logger LOG = LoggerFactory.getLogger(TransactionHistoryLogDataView.class);

	/**
	 * Constructor
	 */
	public TransactionHistoryLogDataView() {
		super();
	}

	protected String buildDynamicSQLCriteria(Map<String,String> parameters) {
		StringBuilder sql = new StringBuilder();
		
        //cquinton 3/26/2013 Rel PR ir#15207 start
        //origTranOid is encrypted for security
        String encryptedTranOid = parameters.get("origTranOid");
        if (encryptedTranOid!=null && encryptedTranOid.length()>0) {
			sql.append("and a.p_transaction_oid = ?");
		}
        else {
            //origTranOid is required
            sql.append(" and 1=0");
        }
        //cquinton 3/26/2013 Rel PR ir#15207 end
        
		return sql.toString();
	}

	protected int setDynamicSQLValues(PreparedStatement statement,
			int startVarIdx, Map<String,String> parameters)
			throws SQLException {
		int varCount = 0;
		
        //cquinton 3/26/2013 Rel PR ir#15207 start
        //origTranOid is encrypted for security
        String encryptedTranOid = parameters.get("origTranOid");
        if (encryptedTranOid!=null && encryptedTranOid.length()>0) {
            String tranOid = EncryptDecrypt.decryptStringUsingTripleDes(
                encryptedTranOid, getUserSession().getSecretKey());
            tranOid = SQLParamFilter.filterNumber(tranOid);
            
            statement.setString(startVarIdx+varCount, tranOid);
            varCount++;
        }
        //cquinton 3/26/2013 Rel PR ir#15207 end
        
		return varCount;
	}
}
