/**
 *
 */
package com.ams.tradeportal.dataview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Map;

import java.sql.SQLException;

import com.ams.tradeportal.common.SQLParamFilter;
import com.amsinc.ecsg.util.StringFunction;

/**
 *
 *
 */
public class TransactionLRQTradeLoanISSMultiBenINVsDataView extends AbstractDBDataView
{
private static final Logger LOG = LoggerFactory.getLogger(TransactionLRQTradeLoanISSMultiBenINVsDataView.class);
	/**
	 * Constructor
	 */
	public TransactionLRQTradeLoanISSMultiBenINVsDataView() {
		super();
	}

    protected String buildDynamicSQLCriteria(Map<String,String> parameters) {


    	StringBuilder dynamicWhereClause = new StringBuilder();


        String transaction_oid = SQLParamFilter.filterNumber(parameters.get("transaction_oid"));
        String benificiaryName = SQLParamFilter.filter(parameters.get("benificiaryName"));
        String benificiaryBankName = SQLParamFilter.filter(parameters.get("benificiaryBankName"));
      
        String amount = SQLParamFilter.filterNumber(parameters.get("beneficiaryAmount"));

        dynamicWhereClause.append ("isd.a_transaction_oid = ");
 		dynamicWhereClause.append(transaction_oid);

        dynamicWhereClause.append(" and (1=1");

        if (StringFunction.isNotBlank(benificiaryName)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("upper(isd.seller_name) like '%");
           dynamicWhereClause.append(benificiaryName.toUpperCase());
           dynamicWhereClause.append("%'"); //"%'" is safe for URLdecoding.
        }

        if (StringFunction.isNotBlank(benificiaryBankName)) {
           dynamicWhereClause.append(" and ");
    
           dynamicWhereClause.append("upper(isd.ben_bank_name) like '%"); //BSL IR RSUL060252944 06/30/11 ADD
           dynamicWhereClause.append(benificiaryBankName.toUpperCase());
           dynamicWhereClause.append("%'");
        }


        if (StringFunction.isNotBlank(amount)) {
           dynamicWhereClause.append(" and ");
           dynamicWhereClause.append("isd.amount like '");
           dynamicWhereClause.append(amount);
           dynamicWhereClause.append("%'");
        }

        dynamicWhereClause.append(")");

        

        return dynamicWhereClause.toString();
    }

    protected int setDynamicSQLValues(PreparedStatement statement, int startVarIdx,
            Map<String,String> parameters)
            throws SQLException {
        int varCount = 0;



        return varCount;
    }
}
