package com.ams.tradeportal.devtool;

import java.io.*;
import java.util.*;

/**
 * This class is used during the build process to create
 * the ANT buildfile used to build EJBs.
 */
public class EjbBuildFileCreate
 {

    /**
     * Looks at the list of deployment descriptors to determine all of the 
     * EJBs that need to be built.  It then goes and creates an Ant buildfile
     * to build all of those EJBs.
     *
     */ 
    public static void main(String argc[])
     {
       if(argc.length < 2)
        {
         System.out.println("The following paths must be specified:");
         System.out.println("  1. Path to EJB deployment descriptor directory"); 
         System.out.println("  2. Path to place the ANT build files that are created");
         return;
        }

       String pathToDescriptors = argc[0];
       String pathToBuildFiles  = argc[1];

       Vector boNames       = getEjbNames(new File(pathToDescriptors+"/busobj").list());
       Vector mediatorNames = getEjbNames(new File(pathToDescriptors+"/mediator").list());

       writeOutEjbNames(pathToBuildFiles+"/build_bos.xml",       "busobj_build.xml",   boNames);
       writeOutEjbNames(pathToBuildFiles+"/build_mediators.xml", "mediator_build.xml", mediatorNames);    
     }

    /**
     * Given a list of EJBs, creates an ant file that calls ant targets
     * for each EJB.
     *
     * @param filePath - path to where to build the ant file
     * @param antFileToCall - path to the ant file that will be called for each EJB
     */    
    private static void writeOutEjbNames(String filePath, String antFileToCall, Vector ejbNames)
     {
      try
       {
          Enumeration enumer = ejbNames.elements();
        
	  FileOutputStream file = new FileOutputStream(filePath);
	  System.out.println("ANT filePath:"+filePath);	
	  PrintWriter writer = new PrintWriter(file);

          writer.println("<project name=\"BuildBusObjEJB\" default=\"all\" basedir=\".\">");
          writer.println(" <target name=\"all\">");

          while(enumer.hasMoreElements())
           {
               writer.print("  <ant antfile=\"");
               writer.print(antFileToCall);
               writer.println("\" dir=\"../..\">");
               writer.print("     <property name=\"ejb.name\" value=\"");
               writer.print(enumer.nextElement());
               writer.println("\"/>");
               writer.println("  </ant>");                          
           }

          writer.println(" </target>"); 
          writer.println("</project>"); 
	  writer.flush();
	  writer.close();
	  file.close(); 
       }
      catch(IOException ioe)
       {
          ioe.printStackTrace();
       }
     }


    /**
     * Takes a full file name (such as AuditLog.ejb-jar.xml) and
     * determines the EJB name (AuditLog in the above case)
     *
     * @param fullFileNames - array of full deployment descriptor file names
     * @return Vector - contains EJB names from the list
     */
    private static Vector getEjbNames(String[] fullFileNames)
     {
        
       Vector returnValue = new Vector();

       for(int i=0; i<fullFileNames.length; i++)
        {
          String fileName = fullFileNames[i];
          System.out.println("fileName:"+fileName);
          // Find the extension to parse on
          int index = fileName.indexOf(".ejb-jar.xml");

          // If it's not there (such as for the WebLogic descriptor), ignore it
          if(index > 0)
           {
              returnValue.add(fileName.substring(0,index));
           }
        }
      
       return returnValue;
     }
 }