
To Compile the file:
* Copy the ojdbc14.jar to the source location (where the java file is) and run the below command.
* javac -classpath "<path of the sourcefile>;ojdbc14.jar" ImportArchivedXMLs.java

example: If the source is in c:\temp, copy the ojdbc14.jar to that folder and run the below command.
javac -classpath "c:\temp;ojdbc14.jar" ImportArchivedXMLs.java

To Execute the file:
* Preferred is to execute this program on the server where JBoss Agent is running for that Bank.If NOT, make sure the EXTENDED_ARCHIVEDIR config_setting path is valid from whereever the program is executed.
* The EXTENDED_ARCHIVEDIR config_setting should be pointing to the Extended Archive Storage.
* Update the properties file with the correct values. Refer the sample properties file importarchivedxmls.properties for more details to update the properties.
* Copy the ojdbc14.jar to the location where the .class files are and run the below command.
* java -classpath "<path of the class file>;ojdbc14.jar" ImportArchivedXMLs "<Property file name with path>"

example: If the class file and property file  is in c:\temp, copy the ojdbc14.jar to that folder and run the below command.
java -classpath "c:\temp;ojdbc14.jar" ImportArchivedXMLs "c:\temp\importarchivedxmls.properties"


Assumptions:
* The XMLs in the DVD are in the year/month folder (folder names like 2014_12, 2015_01 etc) - When saving this zip files, a folder with same name will be created in the destination folder path.

* To create the destination folder path the client bank will be from the properties files.

* The files and folders are not zipped/compressed

* File name starting with WI_ is the work item else it is an instrument.

* File name format for work item: WI_<work item number>_etc

* File name format for instrument: <InstrumentID>_etc


Program logic (High level):
* Get the properties file name and path as a parameter while executing the program.
	* The Property file has the source file path, db connection info and the client bank

* The destination path is derived as:
	Setting value from Config_setting for setting_name = 'EXTENDED_ARCHIVEDIR' + client bank from properties file + 	month/year folder name that the xml files are in the source path.
	ex: If the XMLs are in E:\CB1\2015_01 folder.
             * The path to be provided in the properties file is E:\CB1 
             * The program will read the files like 2015_01\xml file name
             * If the EXTENDED_ARCHIVEDIR path in the config_setting table is C:\OTL\Archive
             * If the client bank provided in the properties file is ANZ
             * The zip files will be stored in C:\OTL\Archive\ANZ\2015_01\zip file name

* Zip the xml file, validate it thru MD5Hash and save it in the destination path

* If the file name starts with WI_ then, get the work item number from the file name and update xml_file column of the archive_work_item table with the zip file name and path for the record where the work item number matches.

* If the file name does NOT start with WI_ then, get the instrument id from the file name and update xml_file column of the archive_instrument table with the zip file name and path for the records where the instrument id OR parent instrument id matches. (Here multiple records may be updated for a single file).



 