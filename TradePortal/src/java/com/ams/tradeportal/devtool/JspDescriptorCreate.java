package com.ams.tradeportal.devtool;

import java.io.*;
import java.util.*;
import com.amsinc.ecsg.util.*;

/**
 * This class is used during the build process to insert JSP information
 * into the web.xml WAR deployment descriptor.
 */
public class JspDescriptorCreate
 {
    private static Vector jspList = new Vector();

    /**
     * Inner class to hold information about a JSP
     *
     */
    private static class Jsp
     {
          private String path;
          private String fileName;
          private String fileNameWithoutExtension;
          private String url;
          private String servletClass;

          /**
           * Constructor.  Based on the path and file name, it builds a bunch of other stuff.
           *
           * @param path - path to the JSP, relative to the document root
           * @param fileName - JSP filename         
           */
          protected Jsp(String path, String fileName)
           {
             // Includes forward quotes, but does not include trailing
             this.path = path;
             this.fileName = fileName;

             // Determine JSP name
             int index = fileName.indexOf(".jsp");
             this.fileNameWithoutExtension = fileName.substring(0,index);

             // Determine URL
             this.url = StringService.change(path, ".", "") + "/" + fileName;

             // Determine Servlet Class
             // This needs to match the fully qualified name of the servlet
             // class that weblogic generates for the JSP
             this.servletClass = "com.ams.tradeportal.jsp." + 
                                 StringService.change(path, "/", "_") +
                                 "__" +
                                 // Any dashes in the filename are converted to _45_
                                 StringService.change(fileNameWithoutExtension, "-", "_45_");
           }
        
        

          /**
           * Writes the servlet definition for this JSP
           * to the print writer that was passed in.
           *
           * @param pw - where to write the text
           */
          protected void writeServletXml(PrintWriter pw)
           {
              pw.println("   <servlet>");
              pw.print("      <servlet-name>");
              pw.print("jsp."+fileNameWithoutExtension);
              pw.println("</servlet-name>");
              pw.print("      <servlet-class>");
              pw.print(servletClass);
              pw.println("</servlet-class>");
              pw.println("   </servlet>");
           }

          /**
           * Writes the servlet mapping for this JSP
           * to the print writer that was passed in.
           *
           * @param pw - where to write the text
           */
          protected void writeServletMappingXml(PrintWriter pw)
           {
              pw.println("   <servlet-mapping>");
              pw.print("      <servlet-name>");
              pw.print("jsp."+fileNameWithoutExtension);
              pw.println("</servlet-name>");
              pw.print("      <url-pattern>");
              pw.print(url);
              pw.println("</url-pattern>");
              pw.println("   </servlet-mapping>");
           }


     }


    /**
     * Looks at the list of deployment descriptors to determine all of the 
     * JSPs to include in the descriptor.  Builds web.xml file that includes
     * the servlet definition and servlet mapping for all of the JSPs.
     *
     */ 
    public static void main(String argc[])
     {
       if(argc.length < 2)
        {
         System.out.println("The following paths must be specified:");
         System.out.println("  1. Path to JSP root directory"); 
         System.out.println("  2. Path to web-xml descriptor");
         return;
        }

       String jspRootDirectory = argc[0];
       String pathToWebXml  = argc[1];

       // Build the list
       getListOfJsps(new File(jspRootDirectory).listFiles(), "");

       // Create servlet definition and mapping from the list
       writeOutJspNames(pathToWebXml, jspList);
     }


     /**
      * Recursively goes through the list of JSPs to build a complete list
      *
      * @param fileList - list of files (including directories) to loop through
      * @param path - the path to the file relative to the staring point
      */
     private static void getListOfJsps(File[] fileList, String path)
      {
    	 if(fileList!=null){
          for(int i=0; i<fileList.length; i++)
           {
               File theFile = fileList[i];
               String fileName = theFile.getName();

               if(fileList[i].isDirectory())
                {
                    // If this file is a directory, recurse through it, passing along the
                    // directory name as the path
                    getListOfJsps(theFile.listFiles(), path+"/"+fileName+".");
                }
               else
                {
                    // If a JSP file, add it to the list
                    if(fileName.endsWith(".jsp"))
                        jspList.addElement(new Jsp(path, fileName));
                }
           }
          }
      }


     /**
      * Loops through the list of JSPs.  It inserts the servlet definition
      * and servlet mapping for each JSP into the web.xml file.   
      *
      * @param descriptorPath - path to the web.xml file
      * @param jspList - the list of JSPs
      */
    private static void writeOutJspNames(String descriptorPath,  Vector jspList)
     {
      try
       {

        
          // Read the web.xml file
          File f = new File(descriptorPath);
	  try(FileReader fr = new FileReader(f)){
	  char[] c = new char[(int)f.length()];
	  fr.read(c,0,(int)f.length());  
	  String fileData = new String(c);
      

          // Insert the JSP information
	  try(FileOutputStream fileOutStream = new FileOutputStream(descriptorPath);
          PrintWriter writer = new PrintWriter(fileOutStream)){


          String tagToLookFor = "<!-- JspDescriptorCreate: HERE -->";

          // Find the place in the web.xml file to insert JSP information
          int indexOfSpot = fileData.indexOf(tagToLookFor) + tagToLookFor.length();
          
          String everythingBeforeJsps = fileData.substring(0, indexOfSpot);
          String everythingAfterJsps =  fileData.substring(indexOfSpot +1, (fileData.length() - 1));

          writer.println(everythingBeforeJsps);
          writer.println("");

          Enumeration enumer = jspList.elements();

          // Write out servlet definition for each JSP
          while(enumer.hasMoreElements())
           {
               Jsp theJsp = (Jsp) enumer.nextElement();    
               theJsp.writeServletXml(writer);
               writer.println("");
           }        

          enumer = jspList.elements();

          // Write out servlet mapping for each JSP
          while(enumer.hasMoreElements())
           {
               Jsp theJsp = (Jsp) enumer.nextElement();    
               theJsp.writeServletMappingXml(writer);
               writer.println("");
           }         

          // Write out the ending tag
          writer.println(everythingAfterJsps);
	  writer.flush();
	  }}
	  
       }
      catch(IOException ioe)
       {
          ioe.printStackTrace();
       }




     }
 }