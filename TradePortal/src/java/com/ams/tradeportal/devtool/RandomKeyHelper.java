package com.ams.tradeportal.devtool;

import com.amsinc.ecsg.util.RandomKey;
import com.amsinc.ecsg.util.EncryptDecrypt;

/**
 * Nar IR-T36000032592 Rel9.2 10/13/2014 
 * Utility to generate keys at the command line.  This should be used
 * to generate keys prior to going into production.  
 */

public class RandomKeyHelper {

	public static void main(String argc[])
    {
       if( (argc.length == 0) || (argc[0] == null) )
        {
           System.out.println("Please enter the number of bits in the key as a command line parameter...");
           return;
        }
       
       System.out.println("Generating a random set of "+argc[0]+" bits");
       
       byte[] randomBytes = RandomKey.getRandomBytes(Integer.parseInt(argc[0]));
       
       System.out.println("\n\nRandom bits (in base64 format): ");

       System.out.println(EncryptDecrypt.bytesToBase64String(randomBytes));
    }
}
