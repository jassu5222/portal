package com.ams.tradeportal.devtool;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.Attribute;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DataSchemaFactory;
import com.amsinc.ecsg.frame.DataSourceFactory;
import com.amsinc.ecsg.frame.DateAttribute;
import com.amsinc.ecsg.frame.DateTimeAttribute;
import com.amsinc.ecsg.frame.DecimalAttribute;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.LocalAttribute;
import com.amsinc.ecsg.frame.NumberAttribute;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.StringService;
import com.workingdogs.village.Schema;
    
/**
 * This tool moves data from one database to another.   A query is
 * run against the source database that retrieves information about all 
 * the data to be moved.  This query provides the flexibility to move any
 * set of data that you want.  The information is used to retrieve all of
 * the data to be moved.  SQL*Loader control files are created containing
 * the majority of the data.   For data that contains newlines that cannot 
 * be added using SQL*Loader, a script containing update statements is 
 * produced.   
 *
 * OIDs are kept in synch in the target database by adding a fixed amount to 
 * each OID and each attribute value that represents an OID.  The fixed amount
 * added to the OIDs should be the current sequence value on the target database.
 * This ensures that OIDs in the target database will be unique.   An update 
 * statement updates the sequence of the target database.
 *
 * A batch file named run.bat is created by this tool.   This batch file
 * performs all actions necessary to load the data.
 *
 * If there are any errors, a rollback SQL script is also produced.
 *
 */
public class RefDataCopyUtility implements Runnable
{
   // Delimiter to separate data fields in the control file
   private static final String DELIMITER = "|..|";
   
   // Indicates to SQL*Loader that the CLOB field is empty
   private static final String EMPTY_CLOB_INDICATOR = "!@#$";    
   
   // Name of SQL file containing update statement
   private static final String UPDATE_SEQUENCE_FILE_NAME = "updateSequence.sql";   
   
   // Name of SQL file to roll back all changes made 
   private static final String ROLLBACK_SCRIPT_NAME = "rollbackEverything.sql";
   
   // Name of SQL file containing update statements to enter any non-CLOB fields
   // that contain newlines in their data
   private static final String UPDATE_NEWLINE_FIELDS = "updateNewLineFields.sql";
   
   // Holds results of the query that is run against the source database
   // to retrieve all business objects.  Keyed by business object name
   // and contains a list of OIDS
   private HashMap allBusinessObjects;
   
   // For each business object, contains a list of attributes that hold OIDs
   private HashMap oidAttributes;
   
   // Keeps track of all the control files that have been created
   private ArrayList controlFilesCreated;
   
   // Path to the directory where all generated files will go
   private String destinationFilePath;
   
   // Server location of the application server for the source data
   private String sourceServerLocation;
 
   // Username, password and ID of the target database
   // Used when creating the batch file
   private String targetDatabaseUserNameAndPassword;
   
   // Indicates whether the program is in debug mode
   private boolean debugFlag;
   
   // Full file path to the file containing the SQL query used to populate 
   // the allBusinessObjects hashmap
   private String queryFilePath;
   
   // Counter of the number of data files created
   // Used to determine name of file also
   private int dataFileCounter = 0;
   
   // The constant amount by which all OIDs will be adjusted
   private long oidAdjustmentAmount;
   
   // Keeps track of the starting time to determine elapsed time
   private long startingTime;
   
   // Keeps track of the new client bank OID so that a reminder
   // can be issued to manually create a reporting user
   private long newClientBankOid;
 
   // Holds a list of all the supervisor commands to be imported
   private HashMap corpSupervisorCommands;
   private ArrayList bogSupervisorCommands;

   // Path to the log file created by this process
   private String logFilePath;
   
   // Holds a list of update statements to be run against the target
   // database (usually for fields containing newline characters)
   private ArrayList updateStatements;
   
   // Holds all table names that are updated
   private Hashtable allTableNames;
   
   // List of errors to display to the user
   private ArrayList errors;

   // Accumulates messages to be written to the log
   StringBuffer logFile;

   // Reporting User Suffix of target database
   String reportingUserSuffix;
   
   public RefDataCopyUtility(String sourceServerLocation, String queryFilePath, String destinationFilePath,
                                 String oidAdjustment, String targetDatabaseUserNameAndPassword,
                                 String logFilePath, String reportingUserSuffix)
    {
          debugFlag = true;        
          allBusinessObjects = new HashMap(60);
          oidAttributes = new HashMap(60);
          controlFilesCreated = new ArrayList(60);
          startingTime = System.currentTimeMillis();       
          setListOfOidFields();
          corpSupervisorCommands = new HashMap(75);
          bogSupervisorCommands = new ArrayList(25);
          updateStatements = new ArrayList(300);
          allTableNames = new Hashtable(50);
          errors = new ArrayList(20);

          logFile = new StringBuffer();

          // Pull values from the passed in data
          this.sourceServerLocation = sourceServerLocation;
          this.queryFilePath = queryFilePath;
          this.destinationFilePath = destinationFilePath;
          this.oidAdjustmentAmount = Long.parseLong(oidAdjustment);           
          this.targetDatabaseUserNameAndPassword = targetDatabaseUserNameAndPassword;
          this.logFilePath = logFilePath;
          this.reportingUserSuffix = reportingUserSuffix;
    }
   
   /**
    * The main method - runs all the different steps of the process.
    *
    * See class comments for details on the algorithm
    *
    */
   public void run ()
    {
       try
       {                                                          
           System.out.println("Starting to run Reference Data Copy Utility...");          
 
           writeToLog("Source Server Location: "+sourceServerLocation);
           writeToLog("Query File Path: "+queryFilePath);
           writeToLog("File Destination Path: "+destinationFilePath);
           writeToLog("Maximum Oid in Target DB: "+oidAdjustmentAmount);           
           writeToLog("Target Database Username and Password:" + targetDatabaseUserNameAndPassword);        
           
           // Run the query in the query file to read in a list of all the relevant data
           // in the database.  Populates a hash table (keyed by businessObjectName) of array lists (containing OIDs)
           getListOfBusinessObjects();   

           // Create a control file for each business object
           // The control file contains information about the data being inserted and the data
           Set allEjbs = allBusinessObjects.keySet();
           
           for(int i=0; i<allEjbs.size(); i++)
            {
                 createControlFileForBusinessObject((String) allEjbs.toArray()[i]);
            }

           // Add Supervisor Command entries to the hashtable, then create
           // the control file for supervisor commands           
           addSupervisorCommands();
           createControlFileForBusinessObject("SupervisorCommand");
           
           // An update SQL script needs to be created to update the sequence table
           // of the target database
           createUpdateScriptForSequence();
           
           // Because non-CLOB data containing newlines is not properly 
           // handled by SQL*Loader, create update statements to load that data
           createUpdateScriptForNewLineFields();
           
           // Create a script to rollback all changes (except for the 
           // sequence number update that have been made by this tool
           createRollbackScript();
           
           // Generate a batch file that will load all of the data into the target database
           createBatchFileToRun();
           
           // Display any errors that occurred
           if(errors.size() > 0)
           {
             writeToLog("###############################################");               
             writeToLog("ERRORS:");
             for(int i=0;i<errors.size();i++)
             {
               writeToLog((String)errors.get(i));   
             }
             writeToLog("");
           }
           
           writeToLog("###############################################");
           writeToLog("#  Export of data is now complete          ");
           writeToLog("#  Run the run.bat script to load data     ");
           writeToLog("#  into the target database                ");
           writeToLog("#                                             ");
           writeToLog("#  REMINDER: The ClientBank reporting user    ");
           writeToLog("#     must be created manually in Supervisor! ");
           writeToLog("#     New OID of Client Bank is: "+newClientBankOid);
           writeToLog("#");
           writeToLog("#  Reporting users for Corporate Orgs and ");
           writeToLog("#    Bank Groups will not be added until the ");
           writeToLog("#    Supervisor Update Utility is run (usually nightly)");
           writeToLog("#");
           writeToLog("#   Total Elapsed Time (milliseconds): "+(System.currentTimeMillis() - startingTime));
           writeToLog("###############################################");

       }
       catch(Exception e)
       {
         // All exception catching is done here for the entire process
         e.printStackTrace();
       }   
       finally
        {
    	   String filepathcheck=StringFunction.validateFilePath(logFilePath);
    	   if(filepathcheck!=null){
           try
            {
               FileOutputStream file = new FileOutputStream(logFilePath);
               PrintWriter writer = new PrintWriter(file);        
               writer.print(logFile.toString());
               writer.flush();
               writer.close();
               file.close(); 
            }
           catch(IOException ioe)
            {
                ioe.printStackTrace();
            }
    	   }else{
    		    System.out.println("Invalid Path.........");
    	   }
           System.out.println("Finished running Reference Data Copy Utility...");    
        }    
    }


   /**
    * This method runs a query stored in a file.  The query must be structured
    * so that it returns one column named EJB and another named OID.  The EJB
    * column contains the name of the EJB that represents a row of data.  The OID
    * column is the OID of that data.   For example, the query might look something 
    * like this:
    *     select 'AuditLog' as EJB, a.audit_log_oid as OID
    *     from audit_log a, corporate_org o
    *     where a.owner_org_oid = o.organization_oid AND
    *           o.a_client_bank_oid = 7031
    *     union
    *     select 'Phrase' as EJB, p.phrase_oid as OID
    *     from phrase p, client_bank o
    *     where p.p_owner_org_oid = o.organization_oid AND
    *     o.organization_oid = 7031
    * Data from several tables must be 'union'ed together to create one result set.
    * 
    * The result set is examined and the data placed into a hashmap.
    *
    */
   private void getListOfBusinessObjects() throws Exception
    {
       // Read the query and trim off any trailing spaces
       String query = readFile(queryFilePath).trim();
      
       QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				                        sourceServerLocation,
                                                        "QueryListView"); 
       queryListView.setSQL(query);
       queryListView.getRecords();
            
       Vector oidsList = new Vector(20000);
       int rowsRetrieved = queryListView.getReturnCount();
       int rowCount = 0;
       
       // Read data in chunks of 50 at a time.
       // If data is read in one large chunk, the XML gets too big
       // and sometimes causes problems.
       while(rowCount <= rowsRetrieved)
       {
           writeToLog(" Retrieving records "+rowCount+" through "+(rowCount+50));
           DocumentHandler xml = queryListView.getRecordsAsXml(rowCount, 50);
           
           // Place each record into a list
           oidsList.addAll(xml.getFragments("/ResultSetRecord"));
           rowCount +=50;
       }
       
       queryListView.remove();       
       
       writeToLog("Total records returned from source database: "+oidsList.size());
       
       // For each record in the list, place the data into the hashmap
       for(int i=0; i<oidsList.size(); i++)
       {
         String businessObjectName = ((DocumentHandler)oidsList.elementAt(i)).getAttribute("/EJB");
         String oid = ((DocumentHandler)oidsList.elementAt(i)).getAttribute("/OID");         
         addBusObjInstance(businessObjectName, oid);
        }
    }
      
   /**
    * A control file is created for each business object.  It contains a header
    * that describes the table's columns and how to determine the data that goes in
    * each.  It also contains the data that goes into the columns, separated by a
    * delimiter.  For some fields, like CLOBs, the data will not actually be 
    * placed in the control file; instead it will be placed in separate files.
    *
    * @param businessObjectName - the business object whose control file is being created
    *
    */   
   private void createControlFileForBusinessObject(String businessObjectName) throws Exception
    {
       
        String connectionPoolName = null;
        Schema schema = null;
        String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\"+businessObjectName+".ctl");
        if(filepathcheck!=null){
        writeToLog("Starting to create control file for: "+businessObjectName);
        try(FileOutputStream file = new FileOutputStream(destinationFilePath+"\\"+businessObjectName+".ctl");
        PrintWriter writer = new PrintWriter(file)){
       
        // Get the list of oids for the business object
        ArrayList oidList = (ArrayList) allBusinessObjects.get(businessObjectName);
        Object[] oids = oidList.toArray();
        
        // Loop through each OID
        for(int i=0; i<oids.length; i++)        
        {
           // If there has been a bad association found, looping through
           // attributes is stopped
           boolean badAssociationFound = false;
           
           String oid = (String) oids[i];

           // Create a default CSDB for each business object
           ClientServerDataBridge csdb = new ClientServerDataBridge();    
           csdb.setLocaleName("en_US");             
           
           // For a Terms business object, force the inclusion of all attributes
           // by putting a special value in the CSDB
           // Each Terms instance needs to have the same number (all) of attributes
           // If the number of attributes were different (as it normally is), then
           // the delimiters could not be properly used.
           if(businessObjectName.equals("Terms"))
           {
               csdb.setCSDBValue("instrument_type", TradePortalConstants.ALL_ATTRIBUTES);               
           }
           
           BusinessObject source;
           
           // Create the EJB
           
           // For Supervisor Command business objects, their data is determined by this program,
           // not by reading it from the database
           // Therefore, their "oid" is not really an OID but the appropriate 
           // supervisor command.  
           if(businessObjectName.equals("SupervisorCommand"))
           {
               // A timestamp is set when each SupervisorCommand is created
               // This timestamp is used to order the commands when they are processed
               // sleep so that each of the commands will have a timestamp that
               // can be used to determine the order relative to the other commands
               Thread.sleep(500);

               // Create an empty object
               source = (BusinessObject)EJBObjectFactory.createClientEJB( sourceServerLocation,
                                                                          businessObjectName,
                                                                          csdb);
               source.newObject();
               
               // Populate the command attribute with the data from the "OID" field
               source.setAttribute("command", oid);
           }
           else
           {
               // For any other EJB, just use the OID to read data from the database
                source = (BusinessObject)EJBObjectFactory.createClientEJB( sourceServerLocation,
                                                                          businessObjectName,
                                                                          Long.parseLong(oid),
                                                                          csdb);
           }

              
           // If this is the first OID processed, the header must be added      
           if(i==0)
           {
             writeToLog(" Creating control file header for: "+businessObjectName);  
             // Get a cached schema for this business object's table
             // so that the column sizes can be determined
             try
              {
                 connectionPoolName = JPylonProperties.getInstance().getString("dataSourceName");
              }
             catch(AmsException e) {}
             try(Connection c = DataSourceFactory.getDataSource(connectionPoolName, false).getConnection()){
             schema = DataSchemaFactory.getSchema(c, source.getDataTableName());             
             createControlFileHeader(writer, source, schema);  
             }
           }
 
           // Perform special processing to create SupervisorCommand rows 
           if(businessObjectName.equals("BankOrganizationGroup"))
           {
               addSupervisorCommandsForBankGroup((BankOrganizationGroup)source);
           }
                   
           // Perform special processing to create SupervisorCommand rows
           if(businessObjectName.equals("CorporateOrganization"))
           {
               addSupervisorCommandsForCorporateCustomer((CorporateOrganization)source);
           }

           // Keep track of the ClientBank's new OID to create a reminder
           if(businessObjectName.equals("ClientBank"))
           {
               newClientBankOid = source.getAttributeLong("organization_oid") + oidAdjustmentAmount;
           }
           
          
           // Get the list of attributes for this business object that are really OIDs
           ArrayList attributesWithOids = (ArrayList) oidAttributes.get(businessObjectName);           
                
           Hashtable attributes = source.getAttributeHash(); 
           Enumeration enumer = attributes.keys();

           StringBuffer businessObjectData = new StringBuffer();
           
           // Loop through each attribute and add its data to the control file's data
           while(enumer.hasMoreElements() && !badAssociationFound)
            {
                  String attributeName = (String) enumer.nextElement();
                  Attribute theAttribute = (Attribute) attributes.get(attributeName);

                  // LocalAttributes aren't stored in the database, so do nothing
                  if(theAttribute instanceof LocalAttribute)
                       continue;                  
                  
                  String storedValue = theAttribute.getAttributeValue();
                  String value = null;
                                   
                  // If this attribute stores an OID value and is not blank,
                  // adjust the OID by the oidAdjustmentAmount
                  if( storedValue.equals("") || 
                      (attributesWithOids == null) ||
                      (!attributesWithOids.contains(attributeName)) )   
                  {
                         // Attribute does not contain an OID
                         value = storedValue;
                  }
                  else
                  {
                         // Attribute contains an association to an OID
                      
                         // Don't do for SupervisorCommand because their records
                         // never existed in the source database.  AuditLog records
                         // will sometimes contain associations to deleted data, so 
                         // don't validate them
                         if(!checkAssociatedObject(storedValue) &&
                            !businessObjectName.equals("SupervisorCommand") &&
                            !businessObjectName.equals("AuditLog"))
                         {
                            // Associated data is not valid
                            StringBuffer sb = new StringBuffer();
                            sb.append("!! Bad Association - Could not Migrate!! : ");
                            sb.append(businessObjectName);
                            sb.append(" (OID=");
                            sb.append(oid);
                            sb.append(") is related using attribute ");
                            sb.append(attributeName);
                            sb.append(" to an object (OID=");
                            sb.append(storedValue);
                            sb.append(") that is not being migrated.");
                            
                            // Add the error to the list (to be displayed later)
                            // Remove the EJB and skip entering its data into the control file
                            errors.add(sb.toString());
                            badAssociationFound = true;
                            continue;
                         }
                         else
                         {                
                             long oidValue = Long.parseLong(storedValue);   
                             // Adjust the OID
                             value = String.valueOf(oidValue + oidAdjustmentAmount);
                         }
                  }
               
                  // If the data is a clob, its data must be placed in a
                  // separate file instead of in the main control/data file.  A reference 
                  // to the separate file is included in the main control/data file.
                  if(schema.getColumn(theAttribute.getPhysicalName()).isClob())
                  {
                     if(!value.equals(""))
                      {
                          // Put the file name in the control/data file, then write out the file
                          String dataFileName = "data"+(++dataFileCounter)+".dat";
                          businessObjectData.append(dataFileName);
                          businessObjectData.append(DELIMITER); 
                          writeOutDataFile(dataFileName, value);
                      }
                      else
                      {
                          // If its empty, no need to create a separate file, just mark it as empty
                          // and the mark it with a delimiter
                          businessObjectData.append(EMPTY_CLOB_INDICATOR);
                          businessObjectData.append(DELIMITER); 
                      }
                  }
                  else
                  {   
                      // Because SQL*Loader uses a newline for a record terminator,
                      // we must handle data that contains newlines as a special case
                      // Here an update statement is built and then stored
                      // All of the update statements are placed into a script
                      //
                      // Format is (example):
                      //   update phrase set phrase_text = 'Hello, my 
                      //   name is Homer' where phrase_oid = 12345;
                      if(value.indexOf("\n") > 0)
                       {
                          StringBuffer updateStatement = new StringBuffer();
                          updateStatement.append("update ");
                          updateStatement.append(source.getDataTableName());
                          updateStatement.append(" set ");
                          updateStatement.append(attributeName);
                          updateStatement.append(" = '");

                          // If the value contains any ' characters, change them
                          // to ''  so that Oracle will understand them
                          if(value.indexOf("'") > 0)
                            value = StringService.change(value, "'", "''");   

                          // Make it so that each newline is concated together
                          // This prevents problems when there are two consective newlines
                          value = StringService.change(value, "\n", "\n'||'"); 

                          updateStatement.append(value);
                          updateStatement.append("' where ");
                          updateStatement.append(source.getIDAttributeName());
                          updateStatement.append(" = ");
                          updateStatement.append(source.getAttributeLong(source.getIDAttributeName())+oidAdjustmentAmount);
                          updateStatement.append(";");
                          
                          // Add this update statement to the list so that
                          // it will be placed in a script later
                          updateStatements.add(updateStatement.toString());
                          
                          // Set the value to be blank in the control file data
                          value="";
                       }
                      
                      // For regular fields, just put the data in the control/data file and mark it
                      // with a delimiter
                      
                      // For fields that contain the delimiter, enclose them
                      // so that SQL Loader will not misinterpret
                      businessObjectData.append(value);                          
                      businessObjectData.append(DELIMITER);  
                  }
             }
           
           if(!badAssociationFound)
              writer.println(businessObjectData.toString());  
           
           source.remove();     
        }

        writer.flush();
       
        controlFilesCreated.add(businessObjectName);
        writeToLog("Done creating control file for: "+businessObjectName+" ("+oids.length+" records)"); 
        	}
        }else{
        	System.out.println("Error: Unable to open file for writing: "
    				+ destinationFilePath);

        }
    }
 
   /**
    * Writes the control file header to a file.   The header tells SQL*Loader what
    * to do with the data that the file contains.  The output that is placed in the
    * file for the header is similar to this:
    *     
    *     LOAD DATA
    *     INFILE *
    *     APPEND
    *     INTO TABLE FX_RATE
    *     FIELDS TERMINATED BY '|..|'
    *     (
    *       opt_lock INTEGER EXTERNAL,
    *       rate DECIMAL EXTERNAL,
    *       p_corporate_org_oid INTEGER EXTERNAL,
    *       last_updated_date DATE (19) "mm/dd/yyyy HH24:MI:SS",
    *       currency_code CHAR,
    *       multiply_indicator CHAR,
    *       fx_rate_oid INTEGER EXTERNAL)
    * 
    *     BEGINDATA
    *
    * The business object is examined and each attribute name is placed into the file.
    * After the header section, the delimited data is included in the file.
    *
    * @param writer - the PrintWriter for the file being written to
    * @param source - the business object used to determine the attributes
    * @param schema - cached schema that describes this business object
    */
   private void createControlFileHeader(PrintWriter writer, BusinessObject source, Schema schema) throws Exception
    {       
       Hashtable attributes = source.getAttributeHash();

       writer.println("LOAD DATA");
       writer.println("INFILE *");
       writer.println("APPEND");
       writer.print("INTO TABLE ");
       writer.println(source.getDataTableName());
       writer.print("FIELDS TERMINATED BY '");
       writer.print(DELIMITER);
       writer.println("'");       
       writer.println("(");

       // Loop through each of the attributes of the business object
       Enumeration enumer = attributes.keys();
       while(enumer.hasMoreElements())
        {
           String attributeName = (String) enumer.nextElement();
           Attribute theAttribute = (Attribute) attributes.get(attributeName);

           // Skip local attributes - they aren't stored in the database
           if(theAttribute instanceof LocalAttribute)
               continue;

           // CLOBs needed to be treated differently
           // Data for CLOB fields is pulled from separate data files 
           // (one for each field in each row) 
           // The column needs to be defined in the header file to 
           // indicate this.
           // An example of how it would look is:
           //      transaction_as_text LOBFILE (FILE_transaction_as_text) TERMINATED BY EOF NULLIF FILE_transaction_as_text = '!@#$'
           // In the actual data, the file name is placed between the delimiters instead of the data
           if(schema.getColumn(theAttribute.getPhysicalName()).isClob()) 
           {
               String clobFileId = "FILE_"+theAttribute.getPhysicalName();
               writer.print("  ");
               writer.print(clobFileId);
               writer.println(" FILLER CHAR,");
               writer.print("  ");
               writer.print(theAttribute.getPhysicalName());
               writer.print(" LOBFILE (");                  
               writer.print(clobFileId);
               writer.print(") TERMINATED BY EOF NULLIF ");
               writer.print(clobFileId);
               writer.print(" = '");
               writer.print(EMPTY_CLOB_INDICATOR);
               if(enumer.hasMoreElements())               
                 writer.println("',");
           }
           else
           {
               // Determine the length of this column from the schema
               int length = 
                     schema.getColumn(theAttribute.getPhysicalName()).length();
               
               // If it's not a CLOB, just write out the attribute's physical name
               // and then the appropriate type
               writer.print("  ");
               writer.print(theAttribute.getPhysicalName());
               writer.print(" ");
               writer.print(getAttributeType(theAttribute, length));
               if(enumer.hasMoreElements())
                  writer.println(",");
           }
         }               

       writer.println(")");
       writer.println("");
       writer.println("BEGINDATA");        
       
       // Store the table name and its OID attribute (used for the rollback
       // script)
       allTableNames.put(source.getDataTableName(), source.getIDAttributeName());
    }   
   
    /**
     * Checks to see if an association is valid in the data that is being 
     * migrated.  It is possible that data related to data being migrated will
     * not be migrated.   This method checks for that.
     * 
     * @param associatedObjectType - the EJB name of the associated object
     * @param associatedObjectOid - the OID of the associated object
     * @return true if the assoication is valid, false if it is not
     */
  private boolean checkAssociatedObject(String associatedObjectOid) throws Exception
  {
       // If the OID is blank , there is no association
       // Therefore, the association cannot be bad
       if(associatedObjectOid.equals(""))
         return true;   

       // Loop through all OIDs, searching for the OID passed in
       Object[] keys = allBusinessObjects.keySet().toArray();
              
       for(int i=0; i<keys.length;i++)
       {
           ArrayList oids = (ArrayList) allBusinessObjects.get(keys[i]);
           
           if(oids.contains(associatedObjectOid))
             return true;
       }
       
       // If it hasn't been found, return false;
       return false;
  }
    
    
  /**
   * Given a corporate organization bean, determines what commands must be
   * run by the Supervisor program to add the organization to the reporting
   * repository.   
   *
   * @param source - the corporate organization for which the commands are being determined
   */      
  private void addSupervisorCommandsForCorporateCustomer(CorporateOrganization source) throws Exception
   {
        // The OIDs must be adjusted to their new values
        String orgOID = 
                  String.valueOf(source.getAttributeLong("organization_oid") + oidAdjustmentAmount);
        String bankorggroupOID = 
                  String.valueOf(source.getAttributeLong("bank_org_group_oid") + oidAdjustmentAmount);

        String pclientbankOID = source.getAttribute("parent_corp_org_oid"); 
        String parentOID;

        if (StringFunction.isNotBlank(pclientbankOID) )
        {
            parentOID =  String.valueOf(Long.parseLong(pclientbankOID) + oidAdjustmentAmount);
        }
        else
            parentOID = bankorggroupOID;

        ArrayList commands = new ArrayList(2);

        parentOID = parentOID+reportingUserSuffix;
        String realOrgOID = orgOID;
        orgOID = orgOID+reportingUserSuffix;
	
	//rbhaduri - 14th May 09 - PPX-051 - get reporting type and pass it to the supervisor command function
	String reportingType = source.getAttribute("reporting_type");
	
        corpSupervisorCommands.put(orgOID, commands);
    }

  /**
   * Given a corporate organization bean, determines what commands must be
   * run by the Supervisor program to add the organization to the reporting
   * repository.   
   *
   * @param source - the corporate organization for which the commands are being determined
   */         
   private void addSupervisorCommandsForBankGroup(BankOrganizationGroup source) throws Exception
    {
        // The OIDs must be adjusted to their new values        
        String orgOID = String.valueOf(source.getAttributeLong("organization_oid") + oidAdjustmentAmount);
        String pclientbankOID = String.valueOf(source.getAttributeLong("client_bank_oid") + oidAdjustmentAmount);

        pclientbankOID = pclientbankOID+reportingUserSuffix;

        String realOrgOID = orgOID;
        orgOID = orgOID+reportingUserSuffix;
   }   
   
   /**
    * Adds a business object's OID to the full list of OIDs for that business object.
    * OIDs are stored in the allBusinessObjects hashtable.  This hashtable is keyed
    * by the business object name and contains Arraylists of OIDs.
    *
    * @param businessObjectName - String - the name of the business object
    * @param oid - the OID of the object being referred to
    */
   private void addBusObjInstance(String businessObjectName, String oid)
    {
       // Look to see if there is already an entry for this business object
       ArrayList instances = (ArrayList) allBusinessObjects.get(businessObjectName);
       
       if(instances != null)
        {
           // If there is already an entry for the business object, add the OID to its list
           instances.add(oid);
        }
       else
        {
           // Create a new list for the business object, then add the OID to the list
           instances = new ArrayList(5000);
           instances.add(oid);
           allBusinessObjects.put(businessObjectName, instances);
        }
     }

   /**
    * Adds SupervisorCommand data to the hashtable.
    * These SupervisorCommand rows are created to ensure that the reporting
    * software will be set up properly for the migrated data.
    *
    * When BOGs and CorporateCustomers are added, data is stored about them
    * in order to create the supervisor command data.
    *
    * Each SupervisorCommand row is stored with a timestamp.  That timestamp
    * is used to order the supervisor commands as they are executed.  BOGs
    * must be placed in the file first. Then, corporate customers must be placed
    * in the file in order so that any parent corporate customer is listed prior
    * to its children.   This is because the reporting software creates representations
    * of these relationships, and the parent must be created prior to the child
    * in order for these relationships to be set up.
    *
    */
    public void addSupervisorCommands() throws Exception
     {
           // Add all of the bog supervisor commands first
           for(int i=0; i<bogSupervisorCommands.size();i++)
               addBusObjInstance("SupervisorCommand", (String)bogSupervisorCommands.get(i)); 

           
           // Query to read in all corporate orgs, ordered by level, so that the highest
           // level corporate orgs are listed first.  Therefore, the parent is always before its child.
           // Note: This query retrieves all corporate orgs, so it will probably contain more
           //       corporate orgs than are being migrated.  This is taken care of below.
           String query = "select organization_oid " +
                          " from corporate_org "+
                          " start with organization_oid in "+
                          "           (  select organization_oid "+
                          "              from corporate_org "+
                          "              where p_parent_corp_org_oid is null ) "+              
                          "       connect by prior organization_oid = p_parent_corp_org_oid "+
                          "       order by level ";
 

           // Run the query and get the results
           QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				                        sourceServerLocation,
                                                        "QueryListView"); 
           queryListView.setSQL(query);
           queryListView.getRecords();

           DocumentHandler xml = queryListView.getRecordsAsXml();
               
           queryListView.remove();       
  
           ArrayList corporateOrgOids = new ArrayList(35);      
           Vector corpOrgOidsList = xml.getFragments("/ResultSetRecord");
       
           // Put the results of the query into corporateOrgOids
           for(int i=0; i<corpOrgOidsList.size(); i++)
            {
               long oid = ((DocumentHandler)corpOrgOidsList.elementAt(i)).getAttributeLong("/ORGANIZATION_OID");      

               // OID must be adjusted so that it will match what is stored in corpSupervisorCommands
               oid += oidAdjustmentAmount;

               corporateOrgOids.add(String.valueOf(oid));
            }

           // For each oid read in, determine if that corporate org is being
           // migrated.  If it is, create a supervisor command for it. 
           // The order that these are processed is in order from highest level to lowest level
           for(int i=0; i<corporateOrgOids.size();i++)
            {
               // Oid of the corporate org 
               String oid = (String) corporateOrgOids.get(i);

               // Get the command associated with that OID (if it exists)
               // If it doesn't exist, it means that the corporate customer is not being migrated
               ArrayList commands = (ArrayList) corpSupervisorCommands.get(oid);

               if(commands != null)
                { 
                   // Create a row in the database for each command associated with the corporate org
                   for(int j=0; j<commands.size(); j++)
                    {
                      addBusObjInstance("SupervisorCommand", (String)commands.get(j));
                    }
                }
            }
     }

   /**
    * This method creates a batch file to run that inserts data
    * into the target database.
    * 
    * First, it runs the update script on the sequence table of the 
    * target database using SQL*Plus.
    *
    * Then, it runs the SQL*Loader program for each control file that
    * was created.
    *
    * @exception Exception throws any exception to the calling method
    */
   private void createBatchFileToRun() throws Exception
    {
        
        String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\run.bat");
		if(filepathcheck!=null){
			writeToLog("Starting to create batch file");
		FileOutputStream file = new FileOutputStream(destinationFilePath+"\\run.bat");
        PrintWriter writer = new PrintWriter(file);              
        
        // first, run SQL script to update sequence
        // Final output in file will look like this:
            // REM Run script to update the OID sequence on the target database
            // sqlplus scott/tiger @updateSequence.sql
        writer.println("REM Run script to update the OID sequence on the target database");
        writer.print("start sqlplus ");
        writer.print(targetDatabaseUserNameAndPassword);        
        writer.print(" @");
        writer.println(UPDATE_SEQUENCE_FILE_NAME);
        writer.println("");
        
        // run sqlldr on each of the BO files
        // Final output in file for each business object will look like this:        
            //REM call SQL*Loader for User
            //start sqlldr scott/tiger control=User.ctl log=User.log bad=User.bad discard=User.discard
        Object[] controlFiles = controlFilesCreated.toArray();        
        
        for(int i=0; i<controlFiles.length;i++)
         {
            writer.print("REM call SQL*Loader for ");
            writer.println((String)controlFiles[i]);
            writer.print("start sqlldr ");
            writer.print(targetDatabaseUserNameAndPassword);
            writer.print(" control=");
            writer.print((String)controlFiles[i]);
            writer.print(".ctl log=");
            writer.print((String)controlFiles[i]);         
            writer.print(".log bad=");
            writer.print((String)controlFiles[i]);         
            writer.print(".bad discard=");
            writer.print((String)controlFiles[i]); 
            writer.print(".discard");
            writer.println("");
         }
        
        // Add statement to run the script to update fields with new lines
        writer.println("REM Run script to update fields containing new liness");
        writer.print("sqlplus ");
        writer.print(targetDatabaseUserNameAndPassword);        
        writer.print(" @");
        writer.println(UPDATE_NEWLINE_FIELDS);
        writer.println("");       
        
        writer.flush();
        writer.close();
        file.close();
        writeToLog("Done creating batch file");
    }else{
    	System.out.println("Error: Unable to open file for writing: "
				+ destinationFilePath);

    }  
    }
    
   /**
    * The sequence of the target database must be increased by the 
    * highest OID found in the source database.  This ensures that
    * there will be no collisions of OID values in the target database.
    *
    * @exception Exception throws any exception to the calling method
    */
   private void createUpdateScriptForSequence() throws Exception
    {
	   String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\"+UPDATE_SEQUENCE_FILE_NAME);
		if(filepathcheck!=null){
			    FileOutputStream file = new FileOutputStream(destinationFilePath+"\\"+UPDATE_SEQUENCE_FILE_NAME);
		        PrintWriter writer = new PrintWriter(file);          
		
		        // Create an update statement for the sequence table
		        writer.print("update SEQUENCE set SEQUENCE_VALUE = ");
		        
		        // The new amount is computed by taking the current number in the sequence table
		        // (which is the oid adjustment amount), adding the highest OID found in the 
		        // source, and then adding 1000 (as a buffer just in case)
		        writer.print(String.valueOf(oidAdjustmentAmount + getMaximumOidFromSource() + 1000));
		        writer.println(" where SEQUENCE_NAME = 'ECSG';");
		        writer.println("exit;");
		
		        writeToLog("Just created update script for sequence table");
		        
		        writer.flush();
		        writer.close();
		        file.close();
        }else{
        		System.out.println("Error: Unable to open file for writing: "
    				+ destinationFilePath);
        }
		
    }    
    
   /**
    * This method determines the highest OID out of all the OIDs retrieved from
    * the source database.   The OID sequence of the target database will be 
    * increased by the value determined from this method.
    *
    * @return the highest OID found in the source data
    * @exception Exception throws any exception to the calling method
    */
   private long getMaximumOidFromSource() throws Exception
    {
        writeToLog("Looking for highest OID found in source data");
        long highestSoFar = -1;
        Object[] ejbNames = allBusinessObjects.keySet().toArray();      
 
        // Loop through all OIDs for all business objects, keep track of the highest one found
        for(int i=0;i<ejbNames.length;i++)
        {
           Object[] oids = ((ArrayList) allBusinessObjects.get(ejbNames[i])).toArray();
           
           for(int j=0; j<oids.length; j++)
           {
               try
               {
                   long oidLongValue = Long.parseLong((String)oids[j]);

                   if(oidLongValue > highestSoFar)
                        highestSoFar = oidLongValue;
               }
               catch (NumberFormatException nfe)
               {
                   // For SupervisorCommand objects, the oid is not actually stored
                   // Ignore these items
               }
           }
        }
        writeToLog("The highest OID found in source data is: "+highestSoFar);
        return highestSoFar;
    }    

    /**
     * Builds a script that will delete the added data from all tables
     * touched by the data migration tool.  It doesn't roll back the changes
     * to the sequence on the target database.
     *
     * This script should be run if there are any major problems.
     */
    private void createRollbackScript() throws Exception
    {
    	String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\"+ROLLBACK_SCRIPT_NAME);
		if(filepathcheck!=null){
			       FileOutputStream file = new FileOutputStream(destinationFilePath+"\\"+ROLLBACK_SCRIPT_NAME);
			       PrintWriter writer = new PrintWriter(file);       
			       
			       long maxOid = getMaximumOidFromSource();
			
			       Enumeration enumer = allTableNames.keys();
			     
			       while(enumer.hasMoreElements())  
			       {
			           String tableName = (String) enumer.nextElement();    
			           String oidColumn = (String) allTableNames.get(tableName);
			           writer.print("delete from ");
			           writer.print(tableName);
			           writer.print(" where ");
			           writer.print(oidColumn);
			           writer.print(" > ");
			           writer.print(oidAdjustmentAmount);
			           writer.print(" and ");
			           writer.print(oidColumn);
			           writer.print(" <= ");
			           writer.print(maxOid);
			           writer.println(";");
			       }
			       
			       writer.println("exit;");
			       writer.flush();
			       writer.close();
			       file.close();            
		}else{
					System.out.println("Error: Unable to open file for writing: "+ destinationFilePath);
		}
    }
    
  /**
   * Some fields cannot be loaded with SQL*Loader (such as those
   * containing newlines.  The control file data enters null values
   * for these fields and then an update statement must be run
   *
   * This method creates the script that has those updates tatements in it.
   */
   private void createUpdateScriptForNewLineFields() throws Exception
    {
	   String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\"+UPDATE_NEWLINE_FIELDS);
			if(filepathcheck!=null){
									   FileOutputStream file = new FileOutputStream(destinationFilePath+"\\"+UPDATE_NEWLINE_FIELDS);
									   PrintWriter writer = new PrintWriter(file);       
								
								       for(int i=0; i<updateStatements.size(); i++)
								           writer.println(updateStatements.get(i));
								
								       writer.println("exit;");
								       writer.flush();
								       writer.close();
								       file.close();          
								}else{
									System.out.println("Error: Unable to open file for writing: "
											+ destinationFilePath);
								}
    }
    
   /**
    * Utility method determines the appropriate type for 
    * the control file data definition based on the attribute type.
    *
    * @return String - the control file data type
    * @param attribute Attribute - the attribute for which the type is being determined
    * @param length - the maximum length of the column
    */
   private String getAttributeType(Attribute attribute, int length)
    {
      // For date attributes, the format must be specified (use jPylon date format)
      if( (attribute instanceof DateAttribute) ||
          (attribute instanceof DateTimeAttribute) )
        return "DATE (19) \"mm/dd/yyyy HH24:MI:SS\"";

      // Handle decimal attributes
      if(attribute instanceof DecimalAttribute)
        return "DECIMAL EXTERNAL";

      // Handle numbers that aren't decimals
      if(attribute instanceof NumberAttribute)
        return "INTEGER EXTERNAL";

      return "CHAR("+length+")";
    }    
   
   /**
    * Utility method to write out a String to a file.  Used mainly to populate
    * the files that contain CLOB data or long character data.
    *
    * @param clobFileName String - the filename
    * @param value String - the data to be placed in the file.
    */
   private void writeOutDataFile(String dataFileName, String value) throws Exception
    {
	   String filepathcheck=StringFunction.validateFilePath(destinationFilePath+"\\"+dataFileName);
	   if(filepathcheck!=null){
					   FileOutputStream file = new FileOutputStream(destinationFilePath+"\\"+dataFileName);
				       PrintWriter writer = new PrintWriter(file);        
				       writer.print(value);
				       writer.flush();
				       writer.close();
				       file.close();       
	   		}else{
    					System.out.println("Error: Unable to open file for writing: "+ dataFileName);
    		}
    }
    
   /**
    * Utility method to read data from a file and read it into
    * a string.  
    *
    * @param path - String - path to the file
    * @return String - the data in the file
    */ 
   private String readFile(String path) throws Exception
    {
	   char[] c ={};
	   String filepathcheck=StringFunction.validateFilePath(path);
	   if(filepathcheck!=null){
			   File f = new File(path);
			   try(FileReader fr = new FileReader(f)){
				   c = new char[(int)f.length()];
				   fr.read(c,0,(int)f.length());
			   }
		}
	   return new String(c);
    }    

   /**
    * For each business object, the process needs to know which attributes
    * contain OIDs.   This method sets up that data by adding to the 
    * oidAttributes hashtable.   The hashtable is keyed by the business object
    * name and contains an ArrayList of the OID attribute names.
    */
   private void setListOfOidFields()
    {
       ArrayList temp = new ArrayList();
       temp.add("account_oid"); 
       temp.add("owner_oid"); 
       oidAttributes.put("Account", temp);

       temp = new ArrayList();
       temp.add("audit_log_oid"); 
       temp.add("owner_org_oid"); 
       temp.add("changed_object_oid"); 
       temp.add("change_user_oid"); 
       oidAttributes.put("AuditLog", temp);

       temp = new ArrayList();
       temp.add("organization_oid"); 
       temp.add("default_user_oid"); 
       temp.add("client_bank_oid");
       oidAttributes.put("BankOrganizationGroup", temp);

       temp = new ArrayList();
       temp.add("organization_oid"); 
       temp.add("default_user_oid"); 
       temp.add("guar_def_tmplt_oid"); 
       temp.add("export_coll_def_tmplt_oid"); 
       temp.add("new_export_coll_def_tmplt_oid"); //Vasavi CR 524 03/31/2010 Add
       temp.add("shipping_guar_def_tmplt_oid"); 
       temp.add("air_waybill_def_tmplt_oid"); 
       temp.add("standby_lc_def_tmplt_oid"); 
       temp.add("import_lc_def_tmplt_oid"); 
       temp.add("c_InstrumentNumberSequence");
       oidAttributes.put("ClientBank", temp);

       temp = new ArrayList();
       temp.add("organization_oid"); 
       temp.add("default_user_oid"); 
       temp.add("parent_corp_org_oid"); 
       temp.add("bank_org_group_oid"); 
       temp.add("first_op_bank_org");
       temp.add("second_op_bank_org");
       temp.add("third_op_bank_org");
       temp.add("fourth_op_bank_org");
       temp.add("client_bank_oid");
       oidAttributes.put("CorporateOrganization", temp);

       temp = new ArrayList();
       temp.add("fx_rate_oid"); 
       temp.add("corporate_org_oid");
       oidAttributes.put("FXRate", temp);

       temp = new ArrayList();
       temp.add("instrument_oid"); 
       temp.add("a_counter_party_oid"); 
       temp.add("original_transaction_oid"); 
       temp.add("active_transaction_oid"); 
       temp.add("corp_org_oid"); 
       temp.add("op_bank_org_oid"); 
       temp.add("related_instrument_oid"); 
       temp.add("client_bank_oid");
       oidAttributes.put("Instrument", temp);

       temp = new ArrayList();
       temp.add("instr_num_seq_oid");
       oidAttributes.put("InstrNumberSequence", temp);

       temp = new ArrayList();
       temp.add("lc_creation_rule_oid"); 
       temp.add("template_oid"); 
       temp.add("op_bank_org_oid"); 
       temp.add("owner_org_oid"); 
       temp.add("po_upload_def_oid");
       oidAttributes.put("LCCreationRule", temp);

       temp = new ArrayList();
       temp.add("organization_oid"); 
       temp.add("bank_org_group_oid"); 
       temp.add("owner_bank_oid");
       oidAttributes.put("OperationalBankOrganization", temp);

       temp = new ArrayList();
       temp.add("party_oid"); 
       temp.add("owner_org_oid"); 
       temp.add("designated_bank_oid");
       oidAttributes.put("Party", temp);

       temp = new ArrayList();
       temp.add("password_history_oid"); 
       temp.add("user_oid");
       oidAttributes.put("PasswordHistory", temp);

       temp = new ArrayList();
       temp.add("phrase_oid");
       temp.add("owner_org_oid");
       oidAttributes.put("Phrase", temp);

       temp = new ArrayList();
       temp.add("po_upload_definition_oid"); 
       temp.add("owner_org_oid");
       oidAttributes.put("POUploadDefinition", temp);

       temp = new ArrayList();
       temp.add("security_profile_oid"); 
       temp.add("owner_org_oid");
       oidAttributes.put("SecurityProfile", temp);

       temp = new ArrayList();
       temp.add("supervisor_cmd_oid"); 
       oidAttributes.put("SupervisorCommand", temp);       
       
       temp = new ArrayList();
       temp.add("template_oid"); 
       temp.add("owner_org_oid"); 
       temp.add("c_TemplateInstrument"); 
       oidAttributes.put("Template", temp);

       temp = new ArrayList();
       temp.add("terms_oid");
       for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES; i++) {
         	temp.add(TradePortalConstants.TERMS_PARTY_ATTRIBUTE_NAMES[i]); 
       }
       oidAttributes.put("Terms", temp);

       temp = new ArrayList();
       temp.add("terms_party_oid"); 
       oidAttributes.put("TermsParty", temp);

       temp = new ArrayList();
       temp.add("threshold_group_oid"); 
       temp.add("corp_org_oid"); 
       oidAttributes.put("ThresholdGroup", temp);

       temp = new ArrayList();
       temp.add("transaction_oid"); 
       temp.add("assigned_to_user_oid"); 
       temp.add("instrument_oid"); 
       temp.add("first_authorizing_user_oid"); 
       temp.add("second_authorizing_user_oid"); 
       temp.add("c_CustomerEnteredTerms");
       temp.add("c_BankReleasedTerms");
       temp.add("last_entry_user_oid"); 
       oidAttributes.put("Transaction", temp);

       temp = new ArrayList();
       temp.add("user_oid"); 
       temp.add("owner_org_oid"); 
       temp.add("security_profile_oid"); 
       temp.add("threshold_group_oid"); 
       temp.add("cust_access_sec_profile_oid"); 
       temp.add("client_bank_oid"); 
       oidAttributes.put("User", temp);
    }    
    
   /**
    * Writes to the log string buffer
    *
    * @param msg - the debug message
    */
   public void writeToLog(String msg)
    {
          logFile.append("\n");
          logFile.append(msg);
    }    
}