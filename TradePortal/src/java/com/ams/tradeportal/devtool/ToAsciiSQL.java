package com.ams.tradeportal.devtool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**

 * 
 */
public class ToAsciiSQL {

	private static final String PRE_FILL = "0000";
	private static int UNICODE_SIZE = 4;
	private static int HEX_BUFFER_SIZE = UNICODE_SIZE+1;

	
	// Pattern used for parsing sql parameter values
	private static Pattern pattern = Pattern.compile("(['])(?:(?!\1)[^']|'')*\\1");
	private StringBuilder hexBuffer = new StringBuilder(HEX_BUFFER_SIZE);
	private String encoding = null;
	private String outputEncoding = null;
	private String infile = null;
	private String outfile = null;

	
	/**
	 * @param infile
	 * @param outfile
	 */
	public ToAsciiSQL(String infile, String outfile) {
   	    this (infile,outfile,"US-ASCII");
	}
	
	/**
	 * @param infile
	 * @param outfile
	 */
	public ToAsciiSQL(String infile, String outfile, String outputEncoding) {
		this.infile = infile;
		this.outfile = outfile;
   	    this.outputEncoding = outputEncoding;
	}

	/**
	 * 
	 */
	public void process() {

		try {
			
		    //validateFiles();
			
			ByteBuffer buf = readFile();
			
			//get encoded charbuffer from bytebuffer
 			Charset charset = Charset.forName(getEncodingType(buf));
			CharBuffer cbuf = charset.newDecoder().decode(buf.slice());
			
			// convert to unistr() function and write to outfile
			convert(cbuf);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getOutputEncoding() {
		return outputEncoding;
	}

	public void setOutputEncoding(String outputEncoding) {
		this.outputEncoding = outputEncoding;
	}
	
	private String getEncodingType(ByteBuffer buf) throws Exception {

    	byte first = buf.get(0);
    	byte second = buf.get(1);
    	byte third = buf.get(2);
    	
    	if ((first == (byte)0xFE) && (second == (byte)0xFF )) {
    		setEncoding("UTF-16BE");
    		buf.position(2);
    	} 
    	else if ((first == (byte)0xFF) && (second == (byte)0xFE )) {
    		setEncoding("UTF-16LE");
    		buf.position(2);
    	} 
    	else if ((first == (byte)0xEF) && (second == (byte)0xBB ) && (third == (byte)0xBF )) {
    		setEncoding("UTF-8");
    		buf.position(3);
    	}
    	else {
    		throw new Exception("Error: invalid file format, Unknown file encoding...");
    	}

    	
    	return encoding;
    }
	
	/**
	 * @return
	 * @throws IOException 
	 * @throws IOException
	 */
	public ByteBuffer readFile() throws Exception {

		File inFile = new File(infile);

		// Open the file as input and get channel
		FileChannel fc=null;
		try {
			fc = new FileInputStream(inFile).getChannel();

			// Get the file into memory
			MappedByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0, (int)fc.size());
			
			// return the file content as ByteBuffer
			return buf;

		} catch (IOException e) {
			System.out.println("Error occurreding reading file: " + infile);
			throw e;
		}
		finally  {
			// Close the file channel 
			if (fc!=null) {
				fc.close();
			}
		}


	}

	/**
	 * @throws IOException
	 */
	private void convert(CharBuffer cb) throws IOException {
		BufferedWriter out = null;
		int start = 0;
		char[] arr = cb.array();
		try  {
			FileOutputStream outFile = new FileOutputStream(outfile);
			out = new BufferedWriter(new OutputStreamWriter(outFile, getOutputEncoding()));

			Matcher lm = pattern.matcher(cb); 
			while (lm.find()) {
				out.write(arr,start,lm.start()-start);
				out.write(toUnistrFunction(lm.group().toString()));
				start = lm.end();
			}
			out.write(arr, start, cb.remaining() - start);
		}
		finally {
			//close buffered writer
			if (out != null) {
				out.close();
			}
		}
	} 

	/**
	 * @param str
	 * @return
	 */
	private String toUnistrFunction(String str) {
		StringBuilder  sb = new StringBuilder (str.length());
		if (toASCII(sb,str)) {
			sb.insert(0,"unistr(").append(")");
		}
		return  sb.toString();
	}
	
	/**
	 * @param str
	 * @return
	 */
	private boolean toASCII(StringBuilder sb, String str) {
		boolean hasNonASCII = false;
		int len = str.length();
		char ch;
		for (int i = 0; i < len; i++) {
			ch = str.charAt(i);
			if (ch <= 0x7E) {
				sb.append(ch);
			} else {
				hasNonASCII = true;
				sb.append(getHexValue(ch));
			}
		}

		return  hasNonASCII;
	}

	/**
	 * @param ch
	 * @return
	 */
	private String getHexValue(char ch) {
		String hex = Integer.toHexString(ch);
		hexBuffer.delete(0, HEX_BUFFER_SIZE).append("\\");
		hexBuffer.append(PRE_FILL.substring(0, UNICODE_SIZE - hex.length())).append(hex);
		return hexBuffer.toString();
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		if (args.length == 0) {
			dispUsage();
			System.exit(1);
		}

		String infile = args[0];
		String outfile = null;
		
		if (args.length == 2) {
			 outfile = args[1];
		}
		else {
			outfile = infile + "_out";
		}
			

		long t1 = System.currentTimeMillis();
		System.out.println("Input file: " + infile);
		System.out.println("Output file: " + outfile);
		System.out.println("Converting.... ");
		
		ToAsciiSQL toAsciiSQL = new ToAsciiSQL(infile, outfile);
		toAsciiSQL.process();
  	    
  	    System.out.println("Completed.... ");
		System.out.println("Input file encoding: " + toAsciiSQL.getEncoding());
		System.out.println("Output file encoding: " + toAsciiSQL.getOutputEncoding());
		System.out.println("Time elapsed (in sec): " + (System.currentTimeMillis()-t1)/1000);
	}

	/**
	 * 
	 */
	public static void dispUsage() {
		System.out.println("-------------------------------------------------------");
		System.out.println("Usage: java ToAsciiSQL inputSqlFile [OutputFileName] ");
		System.out.println("Example: java ToAsciiSQL c:/sqls/bankbranch.sql c:/sqls/converted_bank_branch.sql ");
		System.out.println("Example: java ToAsciiSQL c:/sqls/bankbranch.sql");
		System.out.println("-------------------------------------------------------");

	}
	
}
