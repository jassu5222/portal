/**
 * 
 */
package com.ams.tradeportal.html;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.ams.tradeportal.dataview.AbstractVisibilityCondition;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

/**
 *
 */
public class DataGridFactory {
	private static final Logger LOG = LoggerFactory.getLogger(DataGridFactory.class);
	private ResourceManager resMgr;
	private FormManager formMgr = null;
	private SessionWebBean userSession = null;
	private BeanManager beanMgr = null;
	private HttpServletResponse response = null;

	private ConditionalDisplay condDisplay = null;

	public DataGridFactory(ResourceManager resMgr, SessionWebBean userSession, FormManager formMgr, HttpServletResponse response) {
		this.resMgr = resMgr;
		this.userSession = userSession;
		this.formMgr = formMgr;
		this.response = response;
	}

	public DataGridFactory(ResourceManager resMgr, SessionWebBean userSession, FormManager formMgr, BeanManager beanMgr,
			HttpServletResponse response) {
		this.resMgr = resMgr;
		this.userSession = userSession;
		this.formMgr = formMgr;
		this.beanMgr = beanMgr;
		this.response = response;

		condDisplay = new ConditionalDisplay();
		condDisplay.setUserSession(userSession);
		condDisplay.setBeanMgr(beanMgr);
	}

	/*
	 * This version avoids recreation of conditional display, using that passed in from jsp instead.
	 */
	public DataGridFactory(ResourceManager resMgr, SessionWebBean userSession, FormManager formMgr, BeanManager beanMgr,
			HttpServletResponse response, ConditionalDisplay condDisplay) {
		this.resMgr = resMgr;
		this.userSession = userSession;
		this.formMgr = formMgr;
		this.beanMgr = beanMgr;
		this.response = response;
		this.condDisplay = condDisplay;
	}

	/**
	 * Read the dataGrid xml and create the appropriate html for the grid.
	 * 
	 * @param dataGridName
	 * @return
	 */
	public String createDataGrid(String dataGridId, String dataGridName, Map displayRights) {
		return createDataGrid(dataGridId, dataGridName);
	}

	public String createDataGrid(String dataGridId, String dataGridName) {
		StringBuilder output = new StringBuilder();

		DataGridManager.DataGridMetadata metadata = getDataGridMetaData(dataGridName);

		output.append("<div id=\"").append(dataGridId).append("\" class=\"dataGrid\"></div>\n");

		if (metadata.showFooter) {

			output.append("<div class=\"gridFooter\">\n");

			if (metadata.footerItems != null) {
				int itemCount = metadata.footerItems.length;
				for (int i = 0; i < itemCount; i++) {
					DataGridManager.FooterItem footerItem = metadata.footerItems[i];

					String displayKey = metadata.resourcePrefix + "." + footerItem.displayNameKey;

					if (shouldDisplay(footerItem.conditionalDisplay)) {

						String displayText = "";
						
							// jgadela 02-02-2014 Rel 8.4 CR-854 [BEGIN]- Modified to get the headers in Reporting Language.
                        	if("StandardReportDataGrid".equalsIgnoreCase(dataGridName) || "CustomReportDataGrid".equalsIgnoreCase(dataGridName)){
                        		displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE, userSession.getReportingLanguage());

							} else {
								displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);
							}
							// 12-15-2013 Rel 8.4 CR-854 [END]- Modified to get the headers in Reporting Language.
						
						if (displayText.equals(displayKey)) {
							// try the with 'common prefix'
							String commonDisplayKey = "common." + footerItem.displayNameKey;
							displayText = resMgr.getText(commonDisplayKey, TradePortalConstants.TEXT_BUNDLE);
							
							if (displayText.equals(commonDisplayKey)) {
								displayText = displayKey;
							}
						}

						if (DataGridManager.FooterItem.BUTTON.equals(footerItem.type)) {
							String id = formatId(displayKey);
							output.append("  <button");
							output.append(" id=\"").append(id).append("\"");
							output.append(" data-dojo-type=\"dijit.form.Button\"");
							output.append(" type=\"button\" class=\"gridFooterAction\"");
							if (footerItem.initDisabled) {
								output.append(" disabled");
							}

							output.append(">\n").append(displayText).append("\n");

							if (footerItem.onClick != null && footerItem.onClick.length() > 0) {
								output.append("    <script type=\"dojo/method\" ");
								output.append("data-dojo-event=\"onClick\" data-dojo-args=\"evt\">\n");
								output.append(footerItem.onClick).append("\n");
								output.append("    </script>\n");
							}
							output.append("  </button>\n");

							// cquinton 11/28/2012 - only add a tooltip if hoverhelp text is found
							boolean addTooltip = false;
							String hoverHelpText = resMgr.getText(displayKey, "HoverHelp");
							
							if (hoverHelpText != null && !hoverHelpText.equals(displayKey)) {
								addTooltip = true;
							} else {
								// try with 'common prefix'
								String commonDisplayKey = "common." + footerItem.displayNameKey;
								hoverHelpText = resMgr.getText(commonDisplayKey, "HoverHelp");
								
								if (hoverHelpText != null && !hoverHelpText.equals(commonDisplayKey)) {
									addTooltip = true;
								} else {
									hoverHelpText = displayKey;
								}
							}

							if (addTooltip) {
								output.append("<span data-dojo-type=\"t360.widget.Tooltip\" data-dojo-props=\"saveConnId:'");
								output.append(id);
								output.append("'\">");
								output.append(hoverHelpText);
								output.append("</span>");
							}
						} else if (DataGridManager.FooterItem.BUSYBUTTON.equals(footerItem.type)) {
							createBusyButton(output, footerItem, displayText, displayKey, metadata);
						} else if (DataGridManager.FooterItem.LINK.equals(footerItem.type)) {
							output.append("  <a");
							output.append(" id=\"").append(formatId(displayKey)).append("\"");
							if (footerItem.linkAction != null && footerItem.linkAction.length() > 0) {
								String linkHref = getHrefLink(footerItem);
								output.append(" href=\"").append(linkHref).append("\"");
							} else if (footerItem.onClick != null && footerItem.onClick.length() > 0) {
								output.append(" href=\"javascript:").append(footerItem.onClick).append("\"");
							}
							output.append(" class=\"gridFooterAction\">").append(displayText).append("</a>\n");
						}
					}
				}
			}

			if (metadata.showSelectedCount) {
				String selCountText = resMgr.getText("dataGrid.selected", TradePortalConstants.TEXT_BUNDLE);

				// this is updated by a data grid callback. id is important
				output.append("<span class=\"gridSelectedCount\">");
				output.append(" <span id=\"").append(dataGridId).append("_selCount\">0</span>");
				output.append(" <span>&nbsp;").append(selCountText).append("</span>");
				output.append("</span>\n");
			}

			if (metadata.showTotalCount) {

				// jgadela 02-02-2014 Rel 8.4 CR-854 [BEGIN]- Modified to get the total count label in Reporting Language.
				String totalCountText;
            	if("StandardReportDataGrid".equalsIgnoreCase(dataGridName) || "CustomReportDataGrid".equalsIgnoreCase(dataGridName)){
            		totalCountText = resMgr.getText("dataGrid.totalCount", TradePortalConstants.TEXT_BUNDLE, userSession.getReportingLanguage());
				} else {
					totalCountText = resMgr.getText("dataGrid.totalCount", TradePortalConstants.TEXT_BUNDLE);
				}
				// 12-15-2013 Rel 8.4 CR-854 [END]- Modified to get the total count label in Reporting Language.

				// this is updated by a data grid callback. id is important
				output.append("<span class=\"gridTotalCount\">");
				output.append(" <span>").append(totalCountText).append("&nbsp;</span>");
				output.append(" <span id=\"").append(dataGridId).append("_totalCount\">0</span>");
				output.append("</span>\n");
			}

			// cquinton 9/19/2012 ir#5393 add clear:both div to fix issue with ie7 float:right
			output.append("<div style=\"clear:both;\"></div>");

			output.append("</div>\n");
		}

		return output.toString();
	}

	private String getHrefLink(DataGridManager.FooterItem item) {
		String linkHref = "";
		StringBuilder paramStr = new StringBuilder();

		if (item.linkAction != null && item.linkAction.length() > 0) {
			// add the link parameters
			if (item.linkParameters != null) {
                            for (DataGridManager.LinkParameter linkParameter : item.linkParameters) {
                                String parmName = linkParameter.name;
                                String parmValue = linkParameter.value;
                                paramStr.append("&").append(parmName).append("=").append(parmValue);
                            }
			}

			linkHref = this.formMgr.getLinkAsUrl(item.linkAction, paramStr.toString(), this.response);
		}

		return linkHref;
	}

	/* this is deprecated and will be removed!!! */
	public String createGridLayout(String dataGridName) {
		return createGridLayout(dataGridName, dataGridName);
	}

	/**
	 * Read the dataGrid xml and create the appropriate gridLayout javascript necessary.
	 * 
	 * @param dataGridId
	 * @param dataGridName
	 * @return String representation of the grid layout
	 */
	public String createGridLayout(String dataGridId, String dataGridName) {

		DataGridManager.DataGridMetadata metadata = getDataGridMetaData(dataGridName);

		StringBuilder output = new StringBuilder();
		output.append("[\n");

		boolean chkOrRadio = false;
		if (metadata.showCheckBox) {
			output.append("{type: \"dojox.grid._CheckBoxSelector\"},[\n");
			chkOrRadio = true;
		}
		if (metadata.showRadioButton) {
			output.append("{type: \"dojox.grid._RadioSelector\"},[\n");
			chkOrRadio = true;
		}

		// create a hidden column for the row key if necessary
		if (chkOrRadio) {
			String name = "rowKey";

			output.append("{");
			output.append("name:\"").append(name).append("\"");

			output.append(", field:\"rowKey\"");

			output.append(", hidden:\"true\"");

			output.append("}");
			output.append(" ,\n");
		}

		// cquinton 8/14/2012 Rel portal refresh start
		// read in the grid customization data. if data exists, it will override the defaults
		// any columns that are not listed in the db will still be included in the grid layout as hidden
		// columns after the visible ones so they can be added back in later
		boolean gridCustomized = false;
		Set<String> customColumnSet = new HashSet<>(); // a hash of all customized columns found
		int columnCount = 0;
		String userOid = userSession.getUserOid();
		// jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
		String gridCustSql = "select b.column_id, b.width from grid_customization a, grid_column_customization b "
				+ "where a.grid_cust_oid = b.p_grid_cust_oid and a.a_user_oid = ? and a.grid_id = ? order by display_order";
		try {
			DocumentHandler gridCustListDoc = DatabaseQueryBean.getXmlResultSet(gridCustSql, false, userOid, dataGridId);
			if (gridCustListDoc != null) {
				List<DocumentHandler> gridCustList = gridCustListDoc.getFragmentsList("/ResultSetRecord");
				for (DocumentHandler doc : gridCustList) {
					String columnId = doc.getAttribute("/COLUMN_ID");

					// find the column metadata
					for (int j = 0; j < metadata.columns.length; j++) {
						if (columnId.equals(metadata.columns[j].columnKey)) {
							// set a flag to indicate we found customized columns
							gridCustomized = true;
							customColumnSet.add(columnId);
							columnCount++;
							String width = doc.getAttribute("/WIDTH");
							// go ahead and create the layout for the column
							boolean genarated = createGridColumnLayout(metadata, j, width, true, // override the metadata width and make the column visible
									output, dataGridName);
							// Added for ","(genarated==true, add , else skip)
							if (columnCount < metadata.columns.length && genarated) {
								output.append(" ,\n");
							}
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			// if there is a problem, just report in log and move on with default metadata
			LOG.error("Problem retrieving user grid customization for user oid " + userOid + " and grid id " + dataGridId
					+ ". Continuing...  Exception:", ex);
		}

		if (gridCustomized) {
			// spin through metadata columns and add as hidden any that
			// where not in customization data
			for (int i = 0; i < metadata.columns.length; i++) {
				if (!customColumnSet.contains(metadata.columns[i].columnKey)) {
					columnCount++;
					boolean genarated = createGridColumnLayout(metadata, i, null, false, // no custom width and make the column non-visible
							output, dataGridName);
					// Added for ","(genarated==true, add , else skip)
					if (columnCount < metadata.columns.length && genarated) {
						output.append(" ,\n");
					} else if (columnCount == metadata.columns.length) { // Rel9.0 Added for IR#T36000028013
						if (output.charAt(output.length() - 2) == ',') {
							output.setCharAt(output.length() - 2, ' ');
						}
					}
				}
			}
		} else { // no customization
					// spin through metadata columns and make all columns visible
			for (int i = 0; i < metadata.columns.length; i++) {
				columnCount++;
				boolean genarated = createGridColumnLayout(metadata, i, null, true, // no custom width and make the column visible
						output, dataGridName);
				// Added for ","(genarated==true, add , else skip)
				if (columnCount < metadata.columns.length && genarated) {
					output.append(" ,\n");
				} else if (columnCount == metadata.columns.length) { // Rel9.0 Added for IR#T36000028013
					if (output.charAt(output.length() - 2) == ',') {
						output.setCharAt(output.length() - 2, ' ');
					}
				}
			}
		}


		if (chkOrRadio) {
			output.append(" ]]");
		} else {
			output.append(" ]");
		}

		String layoutDefinition = output.toString();
		return layoutDefinition;
	}

	/**
	 * Create layout for a single grid column. Return true if layout was generated, false otherwise.
	 * 
	 * @param metadata
	 * @param metadataColumnIdx
	 * @param dataGridModuleName
	 * @param customWidth
	 * @param visible
	 * @param output
	 * @return
	 */
	private boolean createGridColumnLayout(DataGridManager.DataGridMetadata metadata, int metadataColumnIdx, String customWidth,
			boolean visible, StringBuilder output, String dataGridName) {

		DataGridManager.DataGridColumn column = metadata.columns[metadataColumnIdx];

		// first check the security type to see if we should display
		boolean display;
		if (userSession != null) {
			if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType())
					&& (DataGridManager.SECURITY_TYPE__ADMIN.equals(column.securityType)
							|| DataGridManager.SECURITY_TYPE__BOTH.equals(column.securityType))) {
				display = true;
			} else if (!TradePortalConstants.ADMIN.equals(userSession.getSecurityType())
					&& (DataGridManager.SECURITY_TYPE__NON_ADMIN.equals(column.securityType)
							|| DataGridManager.SECURITY_TYPE__BOTH.equals(column.securityType))) {
				display = true;
			} else {
				display = false;
			}
		} else {
			// temporary!!! this should not happen
			display = true;
		}

		if (StringFunction.isNotBlank(column.visibilityCondition)) {
			display = isVisible(column.visibilityCondition);
		}

		boolean layoutGenerated = false;
		if (display) {
			String name = getColumnDisplayText(metadata.resourcePrefix, column.columnKey, dataGridName);

			output.append("{");
			output.append("name:\"").append(name).append("\"");

			// specify the json field to display in the case of fields formatted in some other fashion,
			// this is still needed, as it is used to tell the server which column to sort on when sorting is in effect.
			// so if a column is displayed and uses some complex formatting,
			// this field should point to some actual server column that sorting should apply to
			output.append(", field:\"").append(column.columnKey).append("\"");

			if (column.isLink) {
				// add a fields reference that is used for input to the formatter
				// note that this is in addition to 'field' which is used for sorting

				// 1st one is formatted display,
				// 2nd is the column holding url link info
				output.append(", fields:[\"").append(column.columnKey).append("\",");
				String linkUrlColumn = column.linkUrlColumn;
				if (linkUrlColumn == null || linkUrlColumn.length() <= 0) {
					linkUrlColumn = column.columnKey + "_linkUrl";
				}
				output.append("\"").append(linkUrlColumn).append("\"]");

				// add the data grid link formatter
				// cquinton 8/16/2012 - use a global variable reference defined in t360/datagrid.js for all common formatters. this is
				// not ideal, but makes repainting the grid (when customizing) possible
				String formatter = column.formatter;
				if (formatter == null || formatter.length() == 0) {
					formatter = "t360gridFormatters.formatGridLink";
				}
				output.append(", formatter:").append(formatter);
			}

			/* Custom Formatter by Prateep */
			if (column.isCustomFormatter) {
				output.append(", fields:[");
				// Add fields from column.fields
				if (column.fields != null) {
					for (int f = 0; f < column.fields.size(); f++) {
						output.append("\"").append(column.fields.get(f)).append("\"");
						if (f != column.fields.size() - 1) {
							output.append(",");
						}
					}
				}
				output.append("]");
				output.append(", formatter:").append(column.customFormatterName);
			}

			// if width came from user customization, use it
			if (customWidth != null && customWidth.length() > 0) {
				output.append(", width:\"").append(customWidth).append("\"");
			} else {
				String width = column.width + "px";
				output.append(", width:\"").append(width).append("\"");
			}

			if (column.isHidden) {
				output.append(", hidden:\"true\"");
			} else {
				// if user customization hides the column, hide it
				if (!visible) {
					output.append(", hidden:\"true\"");
				}
			}

			if (DataGridManager.RIGHT.equals(column.justifyType)) {
				output.append(", cellClasses:\"gridColumnRight\"");
			} else if (DataGridManager.CENTER.equals(column.justifyType)) {
				output.append(", cellClasses:\"gridColumnCenter\"");
			}

			output.append("}");
			layoutGenerated = true;
		}

		return layoutGenerated;
	}

	private boolean isVisible(String clazz) {
		boolean visible = false;
		try {
			Class vClass = Class.forName(clazz);
			AbstractVisibilityCondition vCondition = (AbstractVisibilityCondition) vClass.newInstance();
			visible = vCondition.execute(userSession, beanMgr, resMgr);
		} catch (Exception ex) {
			LOG.error("Error occured executing visibility Class: {}", clazz,ex);
		}
		return visible;
	}

	/**
	 * This method gets the parameters for the list view. It does this by using the DataGridManager class (which reads in an xml
	 * file for a given list view). The DataGridManager returns a hashtable of the parameters. We extract these into local
	 * variables. Any errors found while reading in the parameters are printed to the console.
	 *
	 * @param listView
	 *            java.lang.String - listview to load
	 */

	private DataGridManager.DataGridMetadata getDataGridMetaData(String dataGridName) {

		DataGridManager.DataGridMetadata metadata = DataGridManager.getDataGridMetadata(dataGridName);

		LOG.debug("Data grid metadata for {} are:\n {} ", dataGridName, metadata);

		if (metadata.hasErrors) {
			LOG.debug("*********************************************");
			LOG.debug("The following errors were found for {} \n\r {}", dataGridName, metadata.errors);
			LOG.debug("*********************************************");
		}

		return metadata;
	}

	public String getColumnDisplayText(String prefix, String columnKey, String dataGridName) {
		String columnDisplayText;
		try {

			// jgadela 02-02-2014 Rel 8.4 CR-854 [BEGIN]- Modified to get the header display label in Reporting Language.
			if ("StandardReportDataGrid".equalsIgnoreCase(dataGridName) || "CustomReportDataGrid".equalsIgnoreCase(dataGridName)) {
				String cbReportingLangOptionInd = null;
				Cache CBCache = TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
				DocumentHandler CBCResult = (DocumentHandler) CBCache.get(userSession.getClientBankOid());
				if (!StringFunction.isBlank(userSession.getClientBankOid())) {
					cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854
				}

				String reportingPageLabelLan;
				if (TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)) {
					reportingPageLabelLan = userSession.getReportingLanguage();
				} else {
					reportingPageLabelLan = userSession.getUserLocale();
				}

				columnDisplayText = resMgr.getText(prefix + "." + columnKey, TradePortalConstants.TEXT_BUNDLE,
						reportingPageLabelLan);
			} else {
				columnDisplayText = resMgr.getText(prefix + "." + columnKey, TradePortalConstants.TEXT_BUNDLE);
			}
			// 12-15-2013 Rel 8.4 CR-854 [END]- .
		} catch (Exception e) {
			LOG.debug("**Unable to find key in resource bundle: {}", columnKey, e);
			columnDisplayText = columnKey;
		}
		return columnDisplayText;
	}

	public String formatId(String value) {
		String id = "";
		if (value != null) {
			id = value.replace('.', '_');
		}
		return id;
	}

	/**
	 * Determines whether to display the item or not.
	 * 
	 * @param displayKey
	 * @param securityKey
	 * @param securityRights
	 * @param displayRights
	 * @return
	 */
	private boolean shouldDisplay(String conditionalDisplay) {
		boolean display = true;
		// only go conditional if xml specified a value
		if (conditionalDisplay != null && conditionalDisplay.length() > 0) {
			// and we have a conditionalDisplay instance
			if (condDisplay != null) { // we have enough info to construct
				display = condDisplay.shouldDisplay(conditionalDisplay);
			}
		}
		return display;
	}

	public String createDataGridMultiFooter(String dataGridId, String dataGridName, String numOfFooters) {
		StringBuffer output = new StringBuffer();

		DataGridManager.DataGridMetadata metadata = getDataGridMetaData(dataGridName);

		output.append("<div id=\"").append(dataGridId).append("\" class=\"dataGrid\"></div>\n");

		if (metadata.showFooter) {

			output.append("<div class=\"gridInvFooter\" id=\"gridInvFooterId\">\n");

			if (metadata.footerItems != null) {
				int itemCount = metadata.footerItems.length;
				for (int i = 0; i < itemCount; i++) {
					DataGridManager.FooterItem footerItem = metadata.footerItems[i];

					String displayKey = metadata.resourcePrefix + "." + footerItem.displayNameKey;

					if (shouldDisplay(footerItem.conditionalDisplay)) {

						String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

						if (displayText.equals(displayKey)) {
							// try the with 'common prefix'
							String commonDisplayKey = "common." + footerItem.displayNameKey;
							displayText = resMgr.getText(commonDisplayKey, TradePortalConstants.TEXT_BUNDLE);
							
							if (displayText.equals(commonDisplayKey)) {
								displayText = displayKey;
							}
						}

						if (DataGridManager.FooterItem.BUTTON.equals(footerItem.type)) {
							String id = formatId(displayKey);
							output.append("  <button");
							output.append(" id=\"").append(id).append("\"");
							output.append(" data-dojo-type=\"dijit.form.Button\"");
							output.append(" type=\"button\" class=\"gridInvFooterAction\"");
							if (footerItem.initDisabled) {
								output.append(" disabled");
							}
							output.append(">\n").append(displayText).append("\n");

							if (footerItem.onClick != null && footerItem.onClick.length() > 0) {
								output.append("    <script type=\"dojo/method\" ");
								output.append("data-dojo-event=\"onClick\" data-dojo-args=\"evt\">\n");
								output.append(footerItem.onClick).append("\n");
								output.append("    </script>\n");
							}
							output.append("  </button>\n");

							// cquinton 11/28/2012 - only add a tooltip if hoverhelp text is found
							boolean addTooltip = false;
							String hoverHelpText = resMgr.getText(displayKey, "HoverHelp");
							
							if (hoverHelpText != null && !hoverHelpText.equals(displayKey)) {
								addTooltip = true;
							} else {
								// try with 'common prefix'
								String commonDisplayKey = "common." + footerItem.displayNameKey;
								hoverHelpText = resMgr.getText(commonDisplayKey, "HoverHelp");
								if (hoverHelpText != null && !hoverHelpText.equals(commonDisplayKey)) {
									addTooltip = true;
								} else {
									hoverHelpText = displayKey;
								}
							}

							if (addTooltip) {
								output.append("<span data-dojo-type=\"dijit.Tooltip\" data-dojo-props=\"connectId:'");
								output.append(id);
								output.append("'\">");
								output.append(hoverHelpText);
								output.append("</span>");
							}
						} else if (DataGridManager.FooterItem.LINK.equals(footerItem.type)) {
							output.append("  <a");
							output.append(" id=\"").append(formatId(displayKey)).append("\"");
							if (footerItem.linkAction != null && footerItem.linkAction.length() > 0) {
								String linkHref = getHrefLink(footerItem);
								output.append(" href=\"").append(linkHref).append("\"");
							} else if (footerItem.onClick != null && footerItem.onClick.length() > 0) {
								output.append(" href=\"javascript:").append(footerItem.onClick).append("\"");
							}
							output.append(" class=\"gridInvFooterAction\">").append(displayText).append("</a>\n");
						}
						// Narayan CR-913 Rel9.0 09-Feb-2014 Begin
						else if (DataGridManager.FooterItem.DROPDOWNBUTTON.equals(footerItem.type)) {
							createDropDownButtonItem(output, displayKey, displayText, footerItem, metadata);
						}
						// Narayan CR-913 Rel9.0 09-Feb-2014 End
					}
				}
			}

			if (metadata.showSelectedCount) {
				output.append("<span class=\"gridCount\">");
				String selCountText = resMgr.getText("dataGrid.selected", TradePortalConstants.TEXT_BUNDLE);

				// this is updated by a data grid callback. id is important
				output.append("<span class=\"gridInvSelectedCount\">");
				output.append(" <span id=\"").append(dataGridId).append("_selCount\">0</span>");
				output.append(" <span>&nbsp;").append(selCountText).append("&nbsp;</span>");
				output.append("</span>\n");
				output.append("</span>\n");
			}

			if (metadata.showTotalCount) {
				output.append("<span class=\"gridCount\">");
				String totalCountText = resMgr.getText("dataGrid.totalCount", TradePortalConstants.TEXT_BUNDLE);
				// this is updated by a data grid callback. id is important
				output.append("<span class=\"gridInvTotalCount\">");
				output.append(" <span>").append(totalCountText).append("&nbsp;</span>");
				output.append(" <span id=\"").append(dataGridId).append("_totalCount\">0</span>");
				output.append("</span>\n");
				output.append("</span>\n");
			}
			// cquinton 9/19/2012 ir#5393 add clear:both div to fix issuee with ie7 float:right
			output.append("<div style=\"clear:both;\"></div>");

			output.append("</div>\n");
		}

		String gridHtml = output.toString();
		return gridHtml;
	}

	// Narayan CR-913 Rel9.0 09-Feb-2014 Begin
	/**
	 * This method is used to create DropDownButton and it's menu item.
	 * 
	 * @param output
	 * @param displayText
	 * @param footerItem
	 * @param metadata
	 */
	private void createDropDownButtonItem(StringBuffer output, String groupDisplayKey, String displayText,
			DataGridManager.FooterItem footerItem, DataGridManager.DataGridMetadata metadata) {
		String id = formatId(groupDisplayKey);
		output.append("<div ");
		output.append(" id=\"").append(id).append("\"");
		if (footerItem.initDisabled) {
			output.append(" disabled");
		}
		output.append(" data-dojo-type=\"dijit/form/DropDownButton\" class=\"dijitButton gridInvFooterAction ").append(id).append(" \">");
		output.append("<span>").append(displayText).append("</span>\n");
		int menuItemCount = 0;
		if (footerItem.dropDownItems != null) {
			menuItemCount = footerItem.dropDownItems.length;
		}
		if (menuItemCount > 0) {
			output.append("<div data-dojo-type=\"dijit/DropDownMenu\" class=\"dijitButton dijitMenu \">");

			for (DataGridManager.DropDownItem item : footerItem.dropDownItems) {
				if (shouldDisplay(item.conditionalDisplay)) {
					String displayKey = metadata.resourcePrefix + "." + item.displayNameKey;
					displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

					if (displayText.equals(displayKey)) {
						// try the with 'common prefix'
						String commonDisplayKey = "common." + item.displayNameKey;
						displayText = resMgr.getText(commonDisplayKey, TradePortalConstants.TEXT_BUNDLE);

						if (displayText.equals(commonDisplayKey)) {
							displayText = displayKey;
						}
					}
					output.append(" <div data-dojo-type=\"dijit/MenuItem\" class=\"dijitButton dijitButtonNode dijitMenuItem\"> ");
					output.append("<span>").append(displayText).append("</span>\n");
					if (item.onClick != null && item.onClick.length() > 0) {
						output.append("    <script type=\"dojo/method\" ");
						output.append("data-dojo-event=\"onClick\" data-dojo-args=\"evt\">\n");
						output.append(item.onClick).append("\n");
						output.append("    </script>\n");
					}
					output.append("</div>\n");
				}
			}
			output.append("</div>\n");
		}
		output.append("</div>\n");
	}
	// Narayan CR-913 Rel9.0 09-Feb-2014 End

	// Refactor in future to use common code in button type and busy button type without redundancy
	// MEer Rel 9.0 IR-19423 STARTS HERE
	/**
	 * This method is used to create BusyButton to avoid double form submission.
	 * 
	 * @param output
	 * @param footerItem
	 * @param displayText
	 * @param displayKey
	 * @param metadata
	 */
	private void createBusyButton(StringBuilder output, DataGridManager.FooterItem footerItem, String displayText,
			String displayKey, DataGridManager.DataGridMetadata metadata) {
		String id = formatId(displayKey);
		output.append("  <button");
		output.append(" id=\"").append(id).append("\"");
		output.append(" data-dojo-type=\"dojox.form.BusyButton\"");
		output.append(" data-dojo-props=\"busyLabel:'");
		String busyLabel = resMgr.getText("BusyLabel." + displayText, TradePortalConstants.TEXT_BUNDLE);
		if (StringFunction.isNotBlank(busyLabel)) {
			output.append(busyLabel);
		} else {
			output.append(displayText);
		}
		output.append("', useIcon:false\" class=\"gridFooterAction\"");

		if (footerItem.initDisabled) {
			output.append(" disabled");
		}
		output.append(">\n").append(displayText).append("\n");

		if (footerItem.onClick != null && footerItem.onClick.length() > 0) {
			output.append("    <script type=\"dojo/method\" ");
			output.append("data-dojo-event=\"onClick\" data-dojo-args=\"evt\">\n");
			output.append(footerItem.onClick).append("\n");
			output.append("    </script>\n");
		}
		output.append("  </button>\n");

		// cquinton 11/28/2012 - only add a tooltip if hoverhelp text is found
		boolean addTooltip = false;
		String hoverHelpText = resMgr.getText(displayKey, "HoverHelp");
		if (hoverHelpText != null && !hoverHelpText.equals(displayKey)) {
			addTooltip = true;
		} else {
			// try with 'common prefix'
			String commonDisplayKey = "common." + footerItem.displayNameKey;
			hoverHelpText = resMgr.getText(commonDisplayKey, "HoverHelp");
			
			if (hoverHelpText != null && !hoverHelpText.equals(commonDisplayKey)) {
				addTooltip = true;
			} else {
				hoverHelpText = displayKey;
			}
		}

		if (addTooltip) {
			output.append("<span data-dojo-type=\"t360.widget.Tooltip\" data-dojo-props=\"saveConnId:'");
			output.append(id);
			output.append("'\">");
			output.append(hoverHelpText);
			output.append("</span>");
		}

	}
	

}
