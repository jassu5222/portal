package com.ams.tradeportal.html;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.crypto.SecretKey;
import javax.ejb.RemoveException;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;


public class Dropdown {
private static final Logger LOG = LoggerFactory.getLogger(Dropdown.class);

/**
 * Dropdown constructor comment.
 *
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public Dropdown() {
	super();
}
/**
  *
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedOptions(formMgr, "SortByName",
  *                                       doc, "name", "description", "itemx")%>
  *  </select>
  *
  * @param inputDoc DocumentHandler - The document to read values from
  * @param valueAttribute String - The name of the attribute in the input document
  *                                that represents the "value" for each option.
  * @param textAttribute String  - The name of the attribute in the input document
  *                                that represents the "text" for each option.
  * @param selectedOption String - The value of the option to default as selected.
  *                                If null, the first value defaults to selected.
  * @param localeName - the locale of the user
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedOptions(DocumentHandler inputDoc,
	                                     String valueAttribute,
								         String textAttribute,
								         String selectedOption,
                                                                         String localeName)
	   throws ServletException {

	// Call getDropdownOptions, don't pass in the encryptionFlag
	DropdownOptions options = getDropdownOptions(inputDoc, valueAttribute, textAttribute, selectedOption, null);

	return getSortedOptions(options, localeName);

}

/**
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedOptions(formMgr, "SortByName",
  *                                       doc, "name", "description", "itemx")%>
  *  </select>
  *
  * @param inputDoc DocumentHandler - The document to read values from
  * @param valueAttribute String - The name of the attribute in the input document
  *                                that represents the "value" for each option.
  * @param textAttribute String  - The name of the attribute in the input document
  *                                that represents the "text" for each option.
  * @param selectedOption String - The value of the option to default as selected.
  *                                If null, the first value defaults to selected.
  * @param encryptFlag indicates whether or not to encrypt the value in the HTML using
  * the EncryptDecrypt class  AlphaNumericEncryption
  * @param localeName - the locale of the user
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedOptions(DocumentHandler inputDoc,
	                                     String valueAttribute,
								         String textAttribute,
								         String selectedOption,
								         SecretKey secretKey,
                                                                         String localeName)
	   throws ServletException {

	// Call getDropdownOptions, passing in the encryptionFlag
	DropdownOptions options = getDropdownOptions(inputDoc, valueAttribute, textAttribute, selectedOption, secretKey);

	return getSortedOptions(options, localeName);

}

/**
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedRefDataOptions(formMgr, "usage", "", "")%>
  *  </select>
  *
  * @param tableType String - The table type to get reference data for.
  * @param selectedOption String - Value to make the seleted item in the list box.
  * @param localeName String - Locale to retrieve ref data for, may be blank
  * @param codesToExclue - reference codes whose data should be excluded from the dropdown
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedRefDataOptions(String tableType,
												String selectedOption,
												String localeName,
												Vector codesToExclude)
	   throws ServletException {

	DropdownOptions options = getRefDataDropdownOptions(tableType, selectedOption, localeName, codesToExclude);

	return getSortedOptions(options, localeName);
}

/**
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedRefDataOptions(formMgr, "usage", "", "")%>
  *  </select>
  *
  * @param tableType String - The table type to get reference data for.
  * @param selectedOption String - Value to make the seleted item in the list box.
  * @param localeName String - Locale to retrieve ref data for, may be blank
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedRefDataOptions(String tableType,
												String selectedOption,
												String localeName)
	   throws ServletException {

	DropdownOptions options = getRefDataDropdownOptions(tableType, selectedOption, localeName);

	return getSortedOptions(options, localeName);
}


/**
  *
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedOptions(formMgr, "SortByName",
  *                                       doc, "name", "description", "itemx")%>
  *  </select>
  *
  * @param inputDoc DocumentHandler - The document to read values from
  * @param valueAttribute String - The name of the attribute in the input document
  *                                that represents the "value" for each option.
  * @param textAttribute String  - The name of the attribute in the input document
  *                                that represents the "text" for each option.
  * @param selectedOption String - The value of the option to default as selected.
  *                                If null, the first value defaults to selected.
  * @param encryptFlag indicates whether or not to encrypt the value in the HTML using
  *         the EncryptDecrypt class AlphaNumericEncryption
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String getUnsortedOptions(DocumentHandler inputDoc,
	                                    String valueAttribute,
								        String textAttribute,
								        String selectedOption,
								        SecretKey secretKey)
								        throws ServletException
  {
	DropdownOptions optionList =  getDropdownOptions(inputDoc, valueAttribute, textAttribute,
								                  selectedOption, secretKey);

	return optionList.createOptionListInHtml();
  }

/**
  *
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedOptions(formMgr, "SortByName",
  *                                       doc, "name", "description", "itemx")%>
  *  </select>
  *
  * @param inputDoc DocumentHandler - The document to read values from
  * @param valueAttribute String - The name of the attribute in the input document
  *                                that represents the "value" for each option.
  * @param textAttribute String  - The name of the attribute in the input document
  *                                that represents the "text" for each option.
  * @param prependText          - text to prepend to every item
  * @param selectedOption String - The value of the option to default as selected.
  *                                If null, the first value defaults to selected.
  * @param encryptFlag indicates whether or not to encrypt the value in the HTML using
  *         the EncryptDecrypt class AlphaNumericEncryption
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String getUnsortedOptions(DocumentHandler inputDoc,
	                                    String valueAttribute,
								        String textAttribute,
								        String prependText,
								        String selectedOption,
								        SecretKey secretKey)
								       throws ServletException
  {
	DropdownOptions optionList =  getDropdownOptions(inputDoc, valueAttribute, textAttribute,
								          prependText, selectedOption, secretKey);

	return optionList.createOptionListInHtml();
  }


/**
	* This method returns the OPTION tags that can be incorporated into an
	* an HTML SELECT statement. The OPTION values are based on the inputDoc
	* and the other input parameters.<p>
	* <p>
	* Each option is of the form <OPTION VALUE=value>text<p>
	* <p>
	* The values used for the options are taken from the input doc.  This
	* XML document is assumed to be a set of /ResultSetRecords.  The
	* valueAttribute and textAttribute are the names of the attributes to
	* retrieve from each result set record for the OPTION's value and text.<p>
	* <p>
	* If a selectedOption is provided (i.e., not null), then as the OPTION
	* tags are created, if a match of the value is made with the selectedOption,
	* the SELECTED tag is added as in <OPTION SELECTED VALUE=value>text<p>
	* <p>
	* This variation does not create the SELECT tag to allow the caller
	* to define the SELECT himself (e.g., to use style sheet, set width, etc.)
	*
	* @return The HTML Select statement
	* @param The document to read values from
	* @param valueAttribute The name of the attribute in the input document
	* that represents the "value" for each option.
	* @param textAttribute The name of the attribute in the input document
	* that represents the "text" for each option.
	* @param selectedOption The value of the option to default as selected.
	* @param encryptFlag indicates whether or not to encrypt the value in the HTML using
	* the EncryptDecrypt class AlphaNumericEncryption
	* If null, the first value defaults to selected.
	*/
private static DropdownOptions getDropdownOptions(DocumentHandler inputDoc,
	                            String valueAttribute,
								String textAttribute,
								String selectedOption,
								SecretKey secretKey)
	throws ServletException {

	    DropdownOptions options = new DropdownOptions();
	    if(secretKey != null)
	    {
	        options.setSecretKeyForEncryption(secretKey);
	    }
	    options.setSelected(selectedOption);

		try {
		        Vector v = inputDoc.getFragments("/ResultSetRecord");
		        int numItems = v.size();
	            String value;
	            String text;

		        for (int i = 0; i < numItems; i++) {
			        DocumentHandler doc = (DocumentHandler) v.elementAt(i);
			        // Extract the value and text attributes from the document.
			        try {
				        value = doc.getAttribute("/" + valueAttribute);
			        } catch (Exception e) {
				        value = "0";
			        }
			        try {
				        text = doc.getAttribute("/" + textAttribute);
			        } catch (Exception e) {
				        text = "";
			        }

			        options.addOption(value, text);




		        }
	        } catch (Exception e) {
		        LOG.info("Error in Dropdown.getDropdownOptions " + e.toString());
	        }

		return options;
   }

/**
	* This method returns the OPTION tags that can be incorporated into an
	* an HTML SELECT statement. The OPTION values are based on the inputDoc
	* and the other input parameters. Optional text may be prepended to
	* each text value retrieved from the xml document.<p>
	* <p>
	* Each option is of the form <OPTION VALUE=value>text<p>
	* <p>
	* The values used for the options are taken from the input doc.  This
	* XML document is assumed to be a set of /ResultSetRecords.  The
	* valueAttribute and textAttribute are the names of the attributes to
	* retrieve from each result set record for the OPTION's value and text.<p>
	* <p>
	* If a selectedOption is provided (i.e., not null), then as the OPTION
	* tags are created, if a match of the value is made with the selectedOption,
	* the SELECTED tag is added as in <OPTION SELECTED VALUE=value>text<p>
	* <p>
	* This variation does not create the SELECT tag to allow the caller
	* to define the SELECT himself (e.g., to use style sheet, set width, etc.)
	*
	* @return The HTML Select statement
	* @param The document to read values from
	* @param valueAttribute The name of the attribute in the input document
	* that represents the "value" for each option.
	* @param textAttribute The name of the attribute in the input document
	* that represents the "text" for each option.
	* @param prependText The text to prepend to each text value.
	* @param selectedOption The value of the option to default as selected.
	* @param encryptFlag indicates whether or not to encrypt the value in the HTML using
	* the EncryptDecrypt  class  AlphaNumericEncryption
	* If null, the first value defaults to selected.
	*/
private static DropdownOptions getDropdownOptions(DocumentHandler inputDoc,
	                            String valueAttribute,
								String textAttribute,
								String prependText,
								String selectedOption,
								SecretKey secretKey)
	throws ServletException
{
	DropdownOptions options = new DropdownOptions();
	if(secretKey != null)
	 {
	    options.setSecretKeyForEncryption(secretKey);
	 }
	options.setSelected(selectedOption);

	DocumentHandler   xmlDoc     = null;
	StringBuffer      text       = null;
	String            value      = null;
	Vector            optionList = null;
	int               numItems   = 0;

	try
	{
	   optionList = inputDoc.getFragments("/ResultSetRecord");
	   numItems   = optionList.size();

	   for (int i = 0; i < numItems; i++)
	   {
		  xmlDoc = (DocumentHandler) optionList.elementAt(i);

		  // Extract the value and text attributes from the document.
		  try
		  {
			 value = xmlDoc.getAttribute("/" + valueAttribute);
		  }
		  catch (Exception e)
		  {
			 value = "0";
		  }

		  text = new StringBuffer(prependText);

		  try
		  {
			 text.append(xmlDoc.getAttribute("/" + textAttribute));
		  }
		  catch (Exception e)
		  {
			 text.append("");
		  }

	      options.addOption(value, text.toString());
	   }
	}
	catch (Exception e)
	{
	   LOG.info("Error in Dropdown.getDropdownOptions " + e.toString());
	}

	return options;
}


/**
* This is a specialized version of the createSortedOptions method that works on a
* ResultSetRecord document of accounts.  The form of the document must be:
*
* <ResultSetRecord ID="1">
*    <ACCOUNT_NUMBER>acct1</ACCOUNT_NUMBER>
*    <CURRENCY>xxx</CURRENCY>
* </ResultSetRecord>
* <ResultSetRecord ID="2">
*    <ACCOUNT_NUMBER>acct1</ACCOUNT_NUMBER>
*    <CURRENCY>xxx</CURRENCY>
* </ResultSetRecord>
* etc.
*
* It returns OPTION tags that can be incorporated into an HTML SELECT.
*
* Each option is off the form:
*
*     <OPTION VALUE=acctOid>acctnum (currency)</OPTION>
*
* So, an option tag might be return as <OPTION VALUE=123-456>123-456 (USD)
*
*
* @return DropdownOptions          A set of OPTION tags
* @param inputDoc                  The document to read values from
* @param selectedOption            The acct_number of the option to default as selected.
*                                  If null, the first value defaults to selected.
*/

public static String createSortedAcctOptionsOid(DocumentHandler inputDoc,
						         String selectedOption,
							   String localeName)
	   throws ServletException {

	String acct_oid, acct_num, acct_currency, acct_disc;
	String text;

	DropdownOptions options = new DropdownOptions();
	options.setSelected(selectedOption);

	try {
		Vector v = inputDoc.getFragments("/DocRoot/ResultSetRecord");
		int numItems = v.size();

		for (int i = 0; i < numItems; i++) {
			DocumentHandler doc = (DocumentHandler) v.elementAt(i);

			// Extract the acct_num and acct_currency attributes from the document.
                  // Note that the first "get" from the doc is without a /.  (This has
                  // something to do with the doc pointer and the way the doc was created.)
			try {
				acct_oid = doc.getAttribute("/ACCOUNT_OID");
			} catch (Exception e) {
				acct_oid = "";
			}
			try {
				acct_num = doc.getAttribute("/ACCOUNT_NUMBER");
			} catch (Exception e) {
				acct_num = "";
			}
			try {
				acct_currency = doc.getAttribute("/CURRENCY");
			} catch (Exception e) {
				acct_currency = "";
			}
			//IAZ 02/07/09 Begin: Add Account_Description
			try {
				acct_disc = doc.getAttribute("/ACCOUNT_DESCRIPTION");
				LOG.debug("acct_disc is " + acct_disc);
			} catch (Exception e) {
				acct_disc = "";
			}

			// Now compute the TEXT value.
			if (!StringFunction.isBlank(acct_disc)) {
				acct_num = acct_num + "-" + acct_disc;
			}
			//IAZ 02/07/09 End

			if (acct_currency.equals("")) {
				text = acct_num;
			} else {
				text = acct_num + " (" + acct_currency + ")";
			}

			options.addOption(acct_oid, text);

		}

	} catch (Exception e) {
		LOG.info("Error in Dropdown.getSortedAcctOptions ", e);
	}

	return getSortedOptions(options, localeName);
   }





/**
* This is a specialized version of the createSortedOptions method that works on a
* ResultSetRecord document of accounts.  The form of the document must be:
*
* <ResultSetRecord ID="1">
*    <ACCOUNT_NUMBER>acct1</ACCOUNT_NUMBER>
*    <CURRENCY>xxx</CURRENCY>
* </ResultSetRecord>
* <ResultSetRecord ID="2">
*    <ACCOUNT_NUMBER>acct1</ACCOUNT_NUMBER>
*    <CURRENCY>xxx</CURRENCY>
* </ResultSetRecord>
* etc.
*
* It returns OPTION tags that can be incorporated into an HTML SELECT.
*
* Each option is off the form:
*
*     <OPTION VALUE=acctnum>acctnum (currency)</OPTION>
*
* So, an option tag might be return as <OPTION VALUE=123-456>123-456 (USD)
*
*
* @return DropdownOptions          A set of OPTION tags
* @param inputDoc                  The document to read values from
* @param selectedOption            The acct_number of the option to default as selected.
*                                  If null, the first value defaults to selected.
*/

public static String createSortedAcctOptions(DocumentHandler inputDoc,
						         String selectedOption,
							   String localeName)
	   throws ServletException {

	String acct_num, acct_currency;
	String text;

	DropdownOptions options = new DropdownOptions();
	options.setSelected(selectedOption);

	try {
		Vector v = inputDoc.getFragments("/DocRoot/ResultSetRecord");
		int numItems = v.size();

		for (int i = 0; i < numItems; i++) {
			DocumentHandler doc = (DocumentHandler) v.elementAt(i);

			// Extract the acct_num and acct_currency attributes from the document.
                  // Note that the first "get" from the doc is without a /.  (This has
                  // something to do with the doc pointer and the way the doc was created.)
			try {
				acct_num = doc.getAttribute("ACCOUNT_NUMBER");
			} catch (Exception e) {
				acct_num = "";
			}
			try {
				acct_currency = doc.getAttribute("/CURRENCY");
			} catch (Exception e) {
				acct_currency = "";
			}

			// Now compute the TEXT value.
			if (acct_currency.equals("")) {
				text = acct_num;
			} else {
				text = acct_num + " (" + acct_currency + ")";
			}

			//options.addOption(acct_num, text);
			options.addOption(text, text);

		}

	} catch (Exception e) {
		LOG.info("Error in Dropdown.getSortedAcctOptions " + e.toString());
	}

	return getSortedOptions(options, localeName);
   }


/**
  * Creates an xml string of <option> tags using reference
  * data for a specific table and local.  The resulting options are
  * not sorted (use createSortedRefDataOptions instead).
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * @param tableType - The table type to get reference data for.
  * @param selectedOption - Value to make the seleted item in the list box.
  * @param localeName - Locale to retrieve ref data for, may be blank
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
private static DropdownOptions getRefDataDropdownOptions(String tableType,
									   String selectedOption,
									   String localeName)
	   throws ServletException
	   {
	      return getRefDataDropdownOptions(tableType, selectedOption, localeName, null);
	   }


/**
  * Creates an xml string of <option> tags using reference
  * data for a specific table and local.  The resulting options are
  * not sorted (use createSortedRefDataOptions instead).
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * @param tableType - The table type to get reference data for.
  * @param selectedOption - Value to make the seleted item in the list box.
  * @param localeName - Locale to retrieve ref data for, may be blank
  * @param codesToExclude - codes whose data will be excluded from the dropdown
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
private static DropdownOptions getRefDataDropdownOptions(String tableType,
									   String selectedOption,
									   String localeName,
									   Vector codesToExclude)
	   throws ServletException {

	DropdownOptions options = new DropdownOptions();
	options.setSelected(selectedOption);

	try {
		// Determine what reference data to use
		if (tableType == null)
			throw new AmsException("Need to send the tableType parameter");

		// Get the reference data and iterate through it to create the list
		// box html
		Hashtable refData;
		refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(tableType, localeName);

		Enumeration enumer = refData.keys();
		while (enumer.hasMoreElements())
		 {
			String code = (String) enumer.nextElement();
			String descr = (String) refData.get(code);

			// Only include the code if it has not been placed in the list of
			// codes to exclude
			if( (codesToExclude == null) ||
			    !codesToExclude.contains(code) )
			 {
				 options.addOption(code, descr);
			 }
		 }
	} catch (AmsException e) {
		throw new ServletException(e.getMessage());
	}
	return options;
}




/**
 * This method builds a list of all the Instrument types for a non-admin user.
 *
 * @return Vector - A list of viewable Instrument types.
 */

 public static Vector getAllInstrumentTypes() throws ServletException {

	Vector codeVector = new Vector();         //list of all codes for the Admin user.

	codeVector.addElement(InstrumentType.AIR_WAYBILL);
	codeVector.addElement(InstrumentType.EXPORT_COL);
	codeVector.addElement(InstrumentType.NEW_EXPORT_COL); //Vasavi CR 524 03/31/2010 Add
	codeVector.addElement(InstrumentType.GUARANTEE);
	codeVector.addElement(InstrumentType.IMPORT_DLC);
	codeVector.addElement(InstrumentType.STANDBY_LC);
	codeVector.addElement(InstrumentType.EXPORT_DLC);
	codeVector.addElement(InstrumentType.INCOMING_SLC);
	codeVector.addElement(InstrumentType.CLEAN_BA);
	codeVector.addElement(InstrumentType.DOCUMENTARY_BA);
	codeVector.addElement(InstrumentType.REFINANCE_BA);
	codeVector.addElement(InstrumentType.INDEMNITY);
	codeVector.addElement(InstrumentType.DEFERRED_PAY);
	codeVector.addElement(InstrumentType.COLLECT_ACCEPT);
	codeVector.addElement(InstrumentType.INCOMING_GUA);
	codeVector.addElement(InstrumentType.IMPORT_COL);
	codeVector.addElement(InstrumentType.SHIP_GUAR);
	codeVector.addElement(InstrumentType.LOAN_RQST);
	codeVector.addElement(InstrumentType.FUNDS_XFER);
	codeVector.addElement(InstrumentType.DOM_PMT);
	codeVector.addElement(InstrumentType.REQUEST_ADVISE);
	codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
	codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
	codeVector.addElement(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT);
	return codeVector;

 }



/**
  * Creates an xml string of <option> tags using reference
  * data for instrument types which is based on the passed in List of codes.
  * The result is sorted.
  *
  * If the selectedType is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * This Method was broken up from it's original format to modularize out
  * the work of getting the List of Instrument Types based on business
  * Rules.  There are two basic types of vectors that are created and
  * passed in:<br>
  * 1) Viewable Instrument types - those types that are defined as viewable
  * by the definition of the Corporate Organization. <br> 2) Viewable AND
  * Create/Modifiable as defined by the corporate organization definitions
  * AND the users current security settings.  The code Vector is typically
  * passed in from: getViewableInstrumentTypes(long organization_Oid) -OR-
  * getCreateModifyInstrumentTypes(String securityRights, long
  * organization_Oid).
  *
  * Special logic is included for the Standby As Guarantee logic.  In this
  * case "fake" codes have been added to the vector.  These are actual codes
  * for the INST_TYPE_SLC_AS_GUA type.  If we find one of these codes in the
  * vector,we'll use the description for that code and then replace the
  * code with the correct one (e.g, XGUA becomes GUA, XSLC becomes SLC)
  *
  * @param selectedType - Value to make the seleted item in the list box.
  * @param localeString - Locale to retrieve ref data for
  * @param codes - A vector of instrument codes to help build the optionList.
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String getInstrumentList(     String selectedType,
									        String localeString,
									        Vector codes)
	   throws ServletException {

	//StringBuffer optionList = new StringBuffer();
	DropdownOptions options = new DropdownOptions();
	options.setSelected(selectedType);

	String descr = "";
	String code = "";

	try
	{   //Loop through the codes List and build the option list.

		for( int x = 0; x < codes.size(); x++ )
		{

			try {
			    descr = ReferenceDataManager.getRefDataMgr().
			    			getDescr(TradePortalConstants.INSTRUMENT_TYPE,
										(String) codes.elementAt(x),
										localeString);
			    code = (String)codes.elementAt(x);
			    //IR - FDUH121457510 & GYUH060564111 12/19/2007 Krishna Add Begin
			    //The following code is moved from Catch block to this place as the the changes are
                //made to getDescr(..) function to return 'null' rather than throwing exception when descrption is not found
				if(descr == null)
				{
			    descr = ReferenceDataManager.getRefDataMgr().
    			getDescr(TradePortalConstants.INST_TYPE_SLC_AS_GUA,
							(String) codes.elementAt(x),
							localeString);
                code = (String)codes.elementAt(x);
				}
				//IR - FDUH121457510 & GYUH060564111 12/19/2007 Krishna Add End
			} catch (AmsException e) {
				// We failed because the code isn't a valid
				// INSTRUMENT_TYPE.  Assume it is one of our "fake"
				// codes of type INST_TYPE_SLC_AS_GUA and look that
				// up.  If it fails, allow the exception to propogate
				// out.  Otherwise, "translate" the code.
          

			    // Convert the fake code into its actual code.
			    //      SIMPLE_SLC   ==> SLC
			    //		DETAILED_SLC ==> GUA
			    //      SLC_IS_GUA   ==> SLC
   
			}

			options.addOption(code, descr);
		}

	} catch (Exception e) {
		LOG.info("Error attempting to get instrument types " +
							e.toString()); 
		throw new ServletException(e.getMessage());
	}

	return getSortedOptions(options, localeString);

}

/**
 * Creates an <option value=x>text</option> string buffer.  If the
 * selectedOption matches the value, a selected=true attribute is added.
 *
 * @return java.lang.StringBuffer
 * @param value java.lang.String - value in the option tag
 * @param text java.lang.String - text to display to user
 * @param selectedOption java.lang.String - option to check
 */
protected static StringBuffer getOptionTag(String value, String text, String selectedOption, SecretKey secretKey) {

	StringBuffer option = new StringBuffer("<option");

	if (value.equals(selectedOption)) {
		option.append(" selected=\"selected\"");
	}
	option.append(" value=\"");
//	 W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
	if(secretKey != null)
	    // Encrypt the value
	    // Can be decrypted by setting flag in mapfile
	    option.append(EncryptDecrypt.encryptStringUsingTripleDes(value, secretKey));
	else
	    option.append(value);
	option.append("\">");
	option.append(StringFunction.xssCharsToHtml(text));
	option.append("</option>");

	return option;
}

/**
  * Creates an xml string of <option> tags using reference
  * data for a specific table and local.  The resulting options are
  * not sorted (use createSortedRefDataOptions instead).
  *
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedRefDataOptions(formMgr, "usage", "", "")%>
  *  </select>
  *
  * @param selectedOption String - Value to make the seleted item in the list box.
  * @param localeName String - Locale to retrieve ref data for, may be blank
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedCurrencyCodeOptions(String selectedOption,
												String localeName) throws ServletException
 {
	return createSortedCurrencyCodeOptions(selectedOption, localeName, null);
 }

/**
  * Creates an xml string of <option> tags using reference
  * data for a specific table and local.  The resulting options are
  * not sorted (use createSortedRefDataOptions instead).
  *
  * The purpose of this method is to provide the option tags to
  * include within an HTML select.
  *
  * If the selectedOption is provided, it attempts to match a code
  * with this value.  If found, that option includes the
  * selected="true" attribute.
  *
  * An example of how this might be used within a JSP
  *  <select>
  *    <option value="0"<-- default option -->/option>
  *    <%=Dropdown.createSortedRefDataOptions(formMgr, "usage", "", "")%>
  *  </select>
  *
  * @param selectedOption String - Value to make the seleted item in the list box.
  * @param localeName String - Locale to retrieve ref data for, may be blank
  * @param codesToExclude Vector - a list of codes that should not be included in the output
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createSortedCurrencyCodeOptions(String selectedOption,
												String localeName,
												Vector codesToExclude)
	   throws ServletException {

	DropdownOptions options = new DropdownOptions();
	options.setSelected(selectedOption);

	try {


		// Get the reference data and iterate through it to create the list
		// box html
		Hashtable refData;
		refData = ReferenceDataManager.getRefDataMgr().
					getAllCodeAndDescr(TradePortalConstants.CURRENCY_CODE, localeName);

		Enumeration enumer = refData.keys();
		while (enumer.hasMoreElements()) {
			String code = (String) enumer.nextElement();

			// Only add the code if it has not been included in the list of codes
			// to be excluded
			if( (codesToExclude == null) ||
			    !codesToExclude.contains(code))
			 {
			   // Use code for both value and displayed text
			   options.addOption(code, code);
			 }
		}
	} catch (AmsException e) {
		throw new ServletException(e.getMessage());
	}

	return getSortedOptions(options, localeName);
}
	   /**
 * Using the given option list, returns a sorted string of HTML.
 * If there are no options, a blank string is
 * returned.
 *
 * @return java.lang.String - sorted options
 * @param optionList java.lang.String - well-formed HTML options
 * @param localeName - the locale of the user
 */
private static String getSortedOptions(DropdownOptions optionList, String localeName)
 {
   return optionList.createSortedOptionListInHtml(localeName);
 }

/**
  * Creates an xml string of <option> tags for the Increase By
  * and Decrease By dropdown.  This is a very speciialized method that
  * hard codes the data and order of data in the dropdown.
  *
  * The two entries are the constants INCREASE and DECREASE in that
  * order.  The display text is from the resource bundle using keys
  * transaction.IncreaseBy and transaction.DecreaseBy.
  *
  * The selected item is based on the value of the passed in amount.
  * For negative amounts the DECREASE option is selected.  Otherwise
  * the INCREASE option is selected.
  *
  * @param resMgr ResourceManager - the resource manager
  * @param selectedOption String - Value to make the seleted item in the list box.
  * @param localeName String - Locale to retrieve ref data for, may be blank
  *
  * @return string - HTML options tags that can be wrappered by a select
  */
public static String createIncreaseDecreaseOptions(ResourceManager resMgr,
	                                            String selectedOption,
												String localeName)
	   throws ServletException {

	StringBuilder options = new StringBuilder();

	// Now add the two options Increase and Decrease in that order
	options.append(getOptionTag(TradePortalConstants.INCREASE,
		                        resMgr.getText("transaction.IncreaseBy",
											   TradePortalConstants.TEXT_BUNDLE),
		                        selectedOption, null));
	options.append(getOptionTag(TradePortalConstants.DECREASE,
		                        resMgr.getText("transaction.DecreaseBy",
											   TradePortalConstants.TEXT_BUNDLE),
		                        selectedOption, null));

	return (options.toString());
}

	public static StringBuffer createCashMgmtTransStatusOptions(ResourceManager resMgr, 
			                                               String selectedOption) throws ServletException {

		// Build the dropdown options (hardcoded in ALL, STARTED, READY,PARTIALLY_AUTH,AUTH_FAILED,REJECTED_BY_BANK,VERIFIED,VERIFIED-PENDING FX),
		// re-selecting the most recently selected option.		 
		  StringBuffer options = createTransStatusOptions(resMgr, selectedOption);
		
		// Verified
		options.append("<option value='");
		options.append(TransactionStatus.VERIFIED);
		options.append("' ");
		if (TransactionStatus.VERIFIED.equals(selectedOption)) {
			options.append(" selected ");
		}
		options.append(">");
		options.append(resMgr.getText("common.StatusVerified",TradePortalConstants.TEXT_BUNDLE));
		options.append("</option>");

		// Verified-Pending FX
		options.append("<option value='");
		options.append(TransactionStatus.VERIFIED_PENDING_FX);
		options.append("' ");
		if (TransactionStatus.VERIFIED_PENDING_FX.equals(selectedOption)) {
			options.append(" selected ");
		}
		options.append(">");
		options.append(resMgr.getText("common.StatusVerifiedPendingFX",TradePortalConstants.TEXT_BUNDLE));
		options.append("</option>");
		// FX Threshold Exceeded
		options.append("<option value='");
		options.append(TransactionStatus.FX_THRESH_EXCEEDED);
		options.append("' ");
		if (TransactionStatus.FX_THRESH_EXCEEDED.equals(selectedOption)) {
			options.append(" selected ");
		}
		options.append(">");
		options.append(resMgr.getText("common.StatusFXThreshExceeded",TradePortalConstants.TEXT_BUNDLE));
		options.append("</option>");

		// Authorize-Pending Market Rate
		options.append("<option value='");
		options.append(TransactionStatus.AUTH_PEND_MARKET_RATE);
		options.append("' ");
		if (TransactionStatus.AUTH_PEND_MARKET_RATE.equals(selectedOption)) {
			options.append(" selected ");
		}
		options.append(">");
		options.append(resMgr.getText("common.StatusAuthPendingMarketRate",TradePortalConstants.TEXT_BUNDLE));
		options.append("</option>");
   	  
	    
		return options;
	}

//Narayan-SRUL051855063-05/23/11-end
// NSX - PRUK080636392 - 08/16/10 - Begin
/**
 * Creates HTML string of <option> tags for the Transaction status
 * display dropdown for CashManagement and Trade.
 *
 * @param resMgr ResourceManager - the resource manager
 * @param selectedOption String - Value to make the seleted item in the list box.
 *
 * @return StringBuffer - HTML options tags that can be wrappered by a select
 */
public static StringBuffer createTransStatusOptions(ResourceManager resMgr,
	                                            String selectedOption)   throws ServletException {

	// Build the dropdown options (hardcoded in ALL, STARTED, READY, PARTIALLY_AUTH, AUTH_FAILED,REJECTED_BY_BANK), re-selecting the
    // most recently selected option.
	StringBuffer options = createRcvMgmtTransStatusOptions(resMgr,selectedOption);

	// Rejected by Bank
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_REJECTED_BY_BANK);
    options.append("' ");
    if (TradePortalConstants.STATUS_REJECTED_BY_BANK.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusRejectedByBank", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");
	
	// Ready to Check
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_READY_TO_CHECK);
    options.append("' ");
    if (TradePortalConstants.STATUS_READY_TO_CHECK.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.ReadyToCheck", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");
    
    // Repair
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_REPAIR);
    options.append("' ");
    if (TradePortalConstants.STATUS_REPAIR.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.Repair", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");
    
    return options;
}

/**
 * Creates HTML string of <option> tags for the Transaction status
 * display dropdown for Receivable Management.
 *
 *
 * @param resMgr ResourceManager - the resource manager
 * @param selectedOption String - Value to make the seleted item in the list box.
 *
 * @return StringBuffer - HTML options tags that can be wrappered by a select
 *
 */
public static StringBuffer createRcvMgmtTransStatusOptions(ResourceManager resMgr,
	                                            String selectedOption)   throws ServletException {

	StringBuffer options = new StringBuffer();

	// Build the dropdown options (hardcoded in ALL, STARTED, READY, PARTIALLY_AUTH, AUTH_FAILED), re-selecting the
    // most recently selected option.
    options = new StringBuffer();

    // All
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_ALL);
    options.append("' ");
    if (TradePortalConstants.STATUS_ALL.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusAll", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");

    // Started
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_STARTED);
    options.append("' ");
    if (TradePortalConstants.STATUS_STARTED.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusStarted", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");

    // Ready to Authorize
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_READY);
    options.append("' ");
    if (TradePortalConstants.STATUS_READY.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusReady", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");

    // Partially Authorized
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_PARTIAL);
    options.append("' ");
    if (TradePortalConstants.STATUS_PARTIAL.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusPartial", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");

    // Authorize Failed
    options.append("<option value='");
    options.append(TradePortalConstants.STATUS_AUTH_FAILED);
    options.append("' ");
    if (TradePortalConstants.STATUS_AUTH_FAILED.equals(selectedOption)) {
       options.append(" selected ");
    }
    options.append(">");
    options.append(resMgr.getText("common.StatusAuthFailed", TradePortalConstants.TEXT_BUNDLE));
    options.append("</option>");

 
	return options;
}


/**
 * Creates HTML string of <option> tags for Active/Inactive status dropdown.
 *
 *
 * @param resMgr ResourceManager - the resource manager
 * @param selectedOption String - Value to make the seleted item in the list box.
 *
 * @return StringBuffer - HTML options tags that can be wrappered by a select
 */
public static StringBuffer createActiveInActiveStatusOptions(ResourceManager resMgr,
	                                            String selectedOption)  throws ServletException {

	StringBuffer options = new StringBuffer();

	// Build the status dropdown options (hardcoded in ACTIVE (default), INACTIVE, ALL order),
	   // re-selecting the most recently selected option.

	   options.append("<option value='");
	   options.append(TradePortalConstants.STATUS_ACTIVE);
	   options.append("' ");
	   if (TradePortalConstants.STATUS_ACTIVE.equals(selectedOption)) {
	      options.append(" selected ");
	   }
	   options.append(">");
	   options.append(resMgr.getText("common.StatusActive", TradePortalConstants.TEXT_BUNDLE));
	   options.append("</option>");

	   options.append("<option value='");
	   options.append(TradePortalConstants.STATUS_INACTIVE);
	   options.append("' ");
	   if (TradePortalConstants.STATUS_INACTIVE.equals(selectedOption)) {
	      options.append(" selected ");
	   }
	   options.append(">");
	   options.append(resMgr.getText("common.StatusInactive", TradePortalConstants.TEXT_BUNDLE));
	   options.append("</option>");

	   options.append("<option value='");
	   options.append(TradePortalConstants.STATUS_ALL);
	   options.append("' ");
	   if (TradePortalConstants.STATUS_ALL.equals(selectedOption)) {
	      options.append(" selected ");
	   }
	   options.append(">");
	   options.append(resMgr.getText("common.StatusAll", TradePortalConstants.TEXT_BUNDLE));
	   options.append("</option>");

	return options;
}




/**
 * This method builds a list of all the Instrument types for an admin user.
 *
 * @return Vector - A list of viewable Instrument types.
 * @param ownerLevel String - The ownership level of the admin user
 */

 public static Vector getAllInstrumentTypesForAdmin(String ownerLevel)
 	throws ServletException {

	Vector codeVector = new Vector();         //list of all codes for the Admin user.

	codeVector.addElement(InstrumentType.AIR_WAYBILL);
	codeVector.addElement(InstrumentType.EXPORT_COL);
	codeVector.addElement(InstrumentType.NEW_EXPORT_COL); //Vasavi CR 524 03/31/2010 Add
	codeVector.addElement(InstrumentType.GUARANTEE);
	codeVector.addElement(InstrumentType.IMPORT_DLC);
	codeVector.addElement(InstrumentType.EXPORT_DLC);
	codeVector.addElement(InstrumentType.SHIP_GUAR);
	codeVector.addElement(InstrumentType.LOAN_RQST);
	codeVector.addElement(InstrumentType.FUNDS_XFER);
	codeVector.addElement(InstrumentType.DOM_PMT);
	codeVector.addElement(InstrumentType.REQUEST_ADVISE);

	codeVector.addElement(InstrumentType.XFER_BET_ACCTS);

	codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);

	codeVector.addElement(InstrumentType.DIRECT_DEBIT_INSTRUCTION); //NSX CR-509 12/15/09
	if (ownerLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
		codeVector.addElement(InstrumentType.STANDBY_LC);
	} else {
		// It's a bank or bog user, add the simple and detail slc options
		codeVector.addElement(TradePortalConstants.SIMPLE_SLC);
		codeVector.addElement(TradePortalConstants.DETAILED_SLC);
	}
	// Nar CR-818 added instrument type IMC, DBA and DFP to display in dropdown
	codeVector.addElement(InstrumentType.IMPORT_COL);
	codeVector.addElement(InstrumentType.DOCUMENTARY_BA);
	codeVector.addElement(InstrumentType.DEFERRED_PAY);
	return codeVector;


 }   /** This method builds A list of instrument types that can be created or modified based on
  * the passed in security rights And a passed in list of viewable Instrument types for the
  * current user.  The result will be a returned Vector of strings (Instrument Types).
  * Essentially a list of codes is returned to be used to help build dropdown list(s) of
  * instrument types.  The logic follows the idea, if you have the security right to edit
  * this instrument type, and it is in the list of viewable Instruments, then add this
  * Instrument type to a list to be sent out.
  *
  * @param securityRights   - The security rights of the user.
  * @param organization)Oid - The Corporate Org Oid (to build the list of 'Viewable' Inst. types)
  * @param String securityProfileType - Admin or Non-admin
  * @param String ownerLevel - Bank, Bog, Global, or Corporate
  *
  * @return Vector - A list of create/modify Instrument types.
  */
public static Vector getCreateModifyInstrumentTypes(FormManager formMgr,
														 String securityRights,
														   long organization_Oid,
														 String securityProfileType,
														 String ownerLevel,
														 long userOid)//PPRAKASH IR PRUJ022438083
	   throws ServletException {

	Vector codeVector = new Vector();       //list of codes that are modifyable based on Bus. rules
											//list of viewable types
	Vector viewableVector = getViewableInstrumentTypes(formMgr, organization_Oid, securityProfileType, ownerLevel,userOid);//PPRAKASH IR PRUJ022438083

	//If the user has the security right to modify an air waybill, and the corporation has
	//defined that the airwaybill to be viewable...Then add the air waybill to the list of
	//codes to be returned.
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.AIR_WAYBILL_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.AIR_WAYBILL ) )
			codeVector.addElement(InstrumentType.AIR_WAYBILL);
	}

	//Direct Send Collection
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.EXPORT_COLL_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.EXPORT_COL ) )
			codeVector.addElement(InstrumentType.EXPORT_COL);
	}

	// Export Collection Vasavi CR 524 03/31/2010
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.NEW_EXPORT_COL ) )
			codeVector.addElement(InstrumentType.NEW_EXPORT_COL);
	}

	//Guarantee
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.GUAR_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.GUARANTEE ) )
			codeVector.addElement(InstrumentType.GUARANTEE);
	}

	//Import DLC
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.DLC_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.IMPORT_DLC ) )
			codeVector.addElement(InstrumentType.IMPORT_DLC);
	}

	//Standby Letter of Credit
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.SLC_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.STANDBY_LC ) )
			codeVector.addElement(InstrumentType.STANDBY_LC);
	}

	//Export Letter of credit
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.EXPORT_LC_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.EXPORT_DLC ) )
			codeVector.addElement(InstrumentType.EXPORT_DLC);
	}

	//Shipping Guarantee
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.SHIP_GUAR_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.SHIP_GUAR ) )
			codeVector.addElement(InstrumentType.SHIP_GUAR);
	}

	//Loan Request
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.LOAN_RQST_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.LOAN_RQST ) )
			codeVector.addElement(InstrumentType.LOAN_RQST);
	}

	//Funds Transfer
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.FUNDS_XFER_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.FUNDS_XFER ) )
			codeVector.addElement(InstrumentType.FUNDS_XFER);
	}

	if( SecurityAccess.hasRights(securityRights, SecurityAccess.DOMESTIC_CREATE_MODIFY ) )
	{

		if( viewableVector.contains( InstrumentType.DOM_PMT ) )
			codeVector.addElement(InstrumentType.DOM_PMT);
	}

	//Request Advise
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.REQUEST_ADVISE ) )
			codeVector.addElement(InstrumentType.REQUEST_ADVISE);
	}
	//	Approval to Pay
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY ) )
	{
		if( viewableVector.contains( InstrumentType.APPROVAL_TO_PAY ) )
			codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
	}
    //	Transfer between accounts
	if( SecurityAccess.hasRights(securityRights, SecurityAccess.TRANSFER_CREATE_MODIFY  ) )
	{
		if( viewableVector.contains( InstrumentType.XFER_BET_ACCTS ) )
			codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
	}




	return codeVector;
}/**
 * This method builds a list of editable Instrument types based on
 * how the corporate organization is defined in the database and
 * the user's security profile.
 *
 * Special logic exists to support the Standby as Guarantee.  Based on
 * corporate org indicators standbys_use_either_form and
 * standbys_use_guar_form_only, we replace the standby option with one or
 * two different options (based on "fake" INST_TYPE_SLC_AS_GUA codes).
 *
 * @param CorporateOrganization corp
 * @param String loginRights
 *
 * @return Vector - A list of editable Instrument types.
 */

 public static Vector getEditableInstrumentTypes(CorporateOrganization corpOrg,
												 String loginRights)
	    throws ServletException
 {

	Vector codeVector = new Vector();//list of codes that are editable

	try
	{
		//air_waybill
		if(corpOrg.getAttribute("allow_airway_bill").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.AIR_WAYBILL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.AIR_WAYBILL);
		}
		//direct send collection
		if(corpOrg.getAttribute("allow_export_collection").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.EXPORT_COLL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.EXPORT_COL);
		}
		//export collection Vasavi CR 524 03/31/2010 Add
		if(corpOrg.getAttribute("allow_new_export_collection").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.NEW_EXPORT_COL);
		}
		//guarantee
		if(corpOrg.getAttribute("allow_guarantee").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.GUAR_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.GUARANTEE);
		}
	    //import DLC
		if(corpOrg.getAttribute("allow_import_DLC").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.DLC_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.IMPORT_DLC);
		}
		//standyby LC
		if(corpOrg.getAttribute("allow_SLC").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.SLC_CREATE_MODIFY))
		{
			// Based on the indicators from corpOrg, add the necessary "fake"
			// codes or the actual SLC code.
			String indicator = corpOrg.getAttribute("standbys_use_either_form");
			if (indicator.equals(TradePortalConstants.INDICATOR_YES)) {
				codeVector.addElement(TradePortalConstants.SIMPLE_SLC);
				codeVector.addElement(TradePortalConstants.DETAILED_SLC);
			} else {
				indicator = corpOrg.getAttribute("standbys_use_guar_form_only");
				if (indicator.equals(TradePortalConstants.INDICATOR_YES)) {
					codeVector.addElement(TradePortalConstants.SLC_IS_GUA);
				} else {
					codeVector.addElement(InstrumentType.STANDBY_LC);
				}
			}
		}
		//shipping guarantee
		if(corpOrg.getAttribute("allow_shipping_guar").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.SHIP_GUAR_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.SHIP_GUAR);
		}
		//loan request
		if(corpOrg.getAttribute("allow_loan_request").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.LOAN_RQST_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.LOAN_RQST);
		}

		//funds transfer
		if((corpOrg.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES) ||
			// IR PRUJ012162023 Peter Ng 2/24/2009 Begin
		    corpOrg.getAttribute("allow_funds_transfer_panel").equals(TradePortalConstants.INDICATOR_YES))
		    // IR PRUJ012162023 Peter Ng 2/24/2009 End
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.FUNDS_XFER_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.FUNDS_XFER);
		}

		if((corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES) ||
		    corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES))
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.DOMESTIC_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.DOM_PMT);
		}

		//request advise
		if(corpOrg.getAttribute("allow_request_advise").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.REQUEST_ADVISE);
		}
		//		Approval to Pay
		if(corpOrg.getAttribute("allow_approval_to_pay").equals(TradePortalConstants.INDICATOR_YES)
			&& SecurityAccess.hasRights(loginRights,SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
		}

		if((corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES) ||
		    corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES))
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.TRANSFER_CREATE_MODIFY))
		{
		    codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
		}

		if(corpOrg.getAttribute("allow_direct_debit").equals(TradePortalConstants.INDICATOR_YES) && 
				SecurityAccess.hasRights(loginRights,SecurityAccess.DDI_CREATE_MODIFY)) {
			codeVector.addElement(InstrumentType.DIRECT_DEBIT_INSTRUCTION);
		}

	}catch( java.rmi.RemoteException e ) {
  	    LOG.info("Error attempting to build list of editable " +
	                       "(in Dropdown class) instrument types " + e.toString());
	    throw new ServletException(e.getMessage());

	}catch( com.amsinc.ecsg.frame.AmsException e ) {
	    LOG.info("Error attempting to build list of editable " +
	                       "(in Dropdown class) instrument types " + e.toString());
	    throw new ServletException(e.getMessage());

	}
		return codeVector;
	}//Always return the list regardless if it has data or not.

 /**
 * This method builds a list of editable Instrument types based on how
 * the corporate organization is defined in the database and the user's
 * security profile.
 *
 * @param CorporateOrganization corp
 * @param String loginRights
 * @param String securityProfileType - Admin or Non-admin
 * @param String ownerLevel - Bank, Bog, Global, or Corporate
 *
 * @return Vector - A list of editable Instrument types.
 */

public static Vector getEditableInstrumentTypes(FormManager formMgr,
												long organization_Oid,
												String loginRights,
												String securityProfileType,
												String ownerLevel)
	throws ServletException {


	Vector codeVector = new Vector(); //list of codes that are editable

	if (securityProfileType.equals(TradePortalConstants.ADMIN) )
	{
		codeVector = getAllInstrumentTypesForAdmin(ownerLevel);
		return codeVector;
	}

	try {
		CorporateOrganization corporateOrg; //Setup this object to get data from Db
		corporateOrg =
			(CorporateOrganization) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(),
				"CorporateOrganization",
				organization_Oid);
		codeVector = getEditableInstrumentTypes(corporateOrg, loginRights);
		corporateOrg.remove();

	} catch (java.rmi.RemoteException e) {
		LOG.info(
			"Error attempting to build list of editable "
				+ "(in Dropdown class) instrument types "
				+ e.toString());
		throw new ServletException(e.getMessage());

	} catch (com.amsinc.ecsg.frame.AmsException e) {
		LOG.info(
			"Error attempting to build list of editable "
				+ "(in Dropdown class) instrument types "
				+ e.toString());
		throw new ServletException(e.getMessage());

	} catch (javax.ejb.RemoveException e) {
		LOG.info(
			"Error removing EJB in getEditableInstrumentTypes");
		throw new ServletException(e.getMessage());
	}

		return codeVector;

}

/**
 * Adding acutal SLC codes: For Simplified and Detailed SLC's, the instrument type code SLC is added. And for SLC-is-GUA, instrument type code GUA is added.
 */

public static Vector getActualEditableInstrumentTypes(CorporateOrganization corpOrg,
		String loginRights)
throws ServletException
{

	Vector codeVector = new Vector();//list of codes that are editable

	try
	{
		//air_waybill
		if(corpOrg.getAttribute("allow_airway_bill").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.AIR_WAYBILL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.AIR_WAYBILL);
		}
		//direct send collection
		if(corpOrg.getAttribute("allow_export_collection").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.EXPORT_COLL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.EXPORT_COL);
		}
		//export collection Vasavi CR 524 03/31/2010 Add
		if(corpOrg.getAttribute("allow_new_export_collection").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.NEW_EXPORT_COL);
		}
		//guarantee
		if(corpOrg.getAttribute("allow_guarantee").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.GUAR_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.GUARANTEE);
		}
		//import DLC
		if(corpOrg.getAttribute("allow_import_DLC").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.DLC_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.IMPORT_DLC);
		}
		//standyby LC
		if(corpOrg.getAttribute("allow_SLC").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.SLC_CREATE_MODIFY))
		{
				codeVector.addElement(InstrumentType.STANDBY_LC);
		}
		//shipping guarantee
		if(corpOrg.getAttribute("allow_shipping_guar").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.SHIP_GUAR_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.SHIP_GUAR);
		}
		//loan request
		if(corpOrg.getAttribute("allow_loan_request").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.LOAN_RQST_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.LOAN_RQST);
		}

		//funds transfer
		if((corpOrg.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES) ||
				// IR PRUJ012162023 Peter Ng 2/24/2009 Begin
				corpOrg.getAttribute("allow_funds_transfer_panel").equals(TradePortalConstants.INDICATOR_YES))
				// IR PRUJ012162023 Peter Ng 2/24/2009 End
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.FUNDS_XFER_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.FUNDS_XFER);
		}

		if((corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES) ||
				// IR PRUJ012162023 Peter Ng 2/24/2009 Begin
				corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES))
				// IR PRUJ012162023 Peter Ng 2/24/2009 End
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.DOMESTIC_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.DOM_PMT);
		}

		//request advise
		if(corpOrg.getAttribute("allow_request_advise").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.REQUEST_ADVISE);
		}
		//		Approval to Pay
		if(corpOrg.getAttribute("allow_approval_to_pay").equals(TradePortalConstants.INDICATOR_YES)
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
		}

		if((corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES) ||
				// IR PRUJ012162023 Peter Ng 2/24/2009 Begin
				corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES))
				// IR PRUJ012162023 Peter Ng 2/24/2009 End
				&& SecurityAccess.hasRights(loginRights,SecurityAccess.TRANSFER_CREATE_MODIFY))
		{
			codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
		}

		if(corpOrg.getAttribute("allow_direct_debit").equals(TradePortalConstants.INDICATOR_YES)) {
			codeVector.addElement(InstrumentType.DIRECT_DEBIT_INSTRUCTION);
		}

	}catch( java.rmi.RemoteException e ) {
		LOG.info("Error attempting to build list of editable " +
				"(in Dropdown class) instrument types " + e.toString());
		throw new ServletException(e.getMessage());

	}catch( com.amsinc.ecsg.frame.AmsException e ) {
		LOG.info("Error attempting to build list of editable " +
				"(in Dropdown class) instrument types " + e.toString());
		throw new ServletException(e.getMessage());

	}
		return codeVector;

}
/**
 * Adding acutal SLC codes: For Simplified and Detailed SLC's, the instrument type code SLC is added. And for SLC-is-GUA, instrument type code GUA is added.
 */
public static Vector getActualEditableInstrumentTypes(FormManager formMgr,
		long organization_Oid,
		String loginRights,
		String securityProfileType,
		String ownerLevel)
throws ServletException {


	Vector codeVector = new Vector(); //list of codes that are editable

	if (securityProfileType.equals(TradePortalConstants.ADMIN) )
	{
		codeVector = getAllInstrumentTypesForAdmin(ownerLevel);
		return codeVector;
	}

	try {
		CorporateOrganization corporateOrg; //Setup this object to get data from Db
		corporateOrg =
			(CorporateOrganization) EJBObjectFactory.createClientEJB(
					formMgr.getServerLocation(),
					"CorporateOrganization",
					organization_Oid);
		codeVector = getActualEditableInstrumentTypes(corporateOrg, loginRights);
		corporateOrg.remove();

	} catch (java.rmi.RemoteException e) {
		LOG.info(
				"Error attempting to build list of editable "
				+ "(in Dropdown class) instrument types "
				+ e.toString());
		throw new ServletException(e.getMessage());

	} catch (com.amsinc.ecsg.frame.AmsException e) {
		LOG.info(
				"Error attempting to build list of editable "
				+ "(in Dropdown class) instrument types "
				+ e.toString());
		throw new ServletException(e.getMessage());

	} catch (javax.ejb.RemoveException e) {
		LOG.info(
		"Error removing EJB in getEditableInstrumentTypes");
		throw new ServletException(e.getMessage());
	}

		return codeVector;

}

/**
 * MEer Rel 9.4 CR-932
 * @param formMgr
 * @param organization_Oid
 * @param securityProfileType
 * @param ownerLevel
 * @param userOid
 * @return
 * @throws ServletException
 */

public static Vector getViewableInstrumentTypes(FormManager formMgr,
		long organization_Oid,
		String securityProfileType,
		String ownerLevel,
		long userOid)
				throws ServletException {

	String fieldName = "";
	return getViewableInstrumentTypes(formMgr, organization_Oid, securityProfileType, ownerLevel, userOid, fieldName);
	
}
 /**
 * This method builds a list of viewable Instrument types based on how the corporate organization
 * is defined in the database.  If the allow_<InstrumentType> == 'Y' then we add that instrument
 * to the outgoing vector.  To do this we need a valid Organization_oid to create a corporate org
 * so that we can retrieve the data from the Database.  Null Checking was enforced to be safe,
 * is this necessary? That's been a subject of debate.
 *
 * If the user is an admin user, then return a list with all possible instrument types.
 *
 * @param (long)Organization_Oid - The Corporate Org Oid to create the Corporate Org object
 * @param String securityProfileType - Admin or Non-admin
 * @param String ownerLevel - Bank, Bog, Global, or Corporate
 *
 * @return Vector - A list of viewable Instrument types.
 */

public static Vector getViewableInstrumentTypes(FormManager formMgr,
		long organization_Oid,
		String securityProfileType,
		String ownerLevel,
		long userOid, 
		String fieldName)//PPRAKASH IR PRUJ022438083
				throws ServletException {

	Vector codeVector = new Vector();                   //list of codes that are viewable.

//if the current user is an admin user thaen populate the list with all the instrument types.
//otherwise we need to find out what the Corporate Org allows the user to see...
	if ( securityProfileType.equals(TradePortalConstants.ADMIN) )
	{
		codeVector = getAllInstrumentTypesForAdmin(ownerLevel);
		return codeVector;
	}

	try
	{
		CorporateOrganization corporateOrg;             //Setup this object to get data from Db
		corporateOrg = (CorporateOrganization)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(),
	  	                                                        "CorporateOrganization", organization_Oid );

		//Rather than hit the Object twice per check, this var will get that data into
		//resident memory once to save on overhead. And it's easier to read.
		String attributeValue = corporateOrg.getAttribute("allow_airway_bill");

		//Check the Corporate Org to see if Air Waybill is viewable, If it is,
		//add it to the outgoing List.  Always guard against the Null value first!
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.AIR_WAYBILL);
		}


		attributeValue = corporateOrg.getAttribute("allow_export_collection");

		//Check the Corporate Org for the Export Collection
		if( ( attributeValue != null )  &&
			( attributeValue.equals( TradePortalConstants.INDICATOR_YES )) )
		{
			codeVector.addElement(InstrumentType.EXPORT_COL);
		}
		//Vasavi CR 524 03/31/2010 Begin
		attributeValue = corporateOrg.getAttribute("allow_new_export_collection");

		//Check the Corporate Org for the Export Collection
		if( ( attributeValue != null )  &&
			( attributeValue.equals( TradePortalConstants.INDICATOR_YES )) )
		{
			codeVector.addElement(InstrumentType.NEW_EXPORT_COL);
		}
		//Vasavi CR 524 03/31/2010 End
		attributeValue = corporateOrg.getAttribute("allow_guarantee");

		//Check the Corporate Org for the Guarantee
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.GUARANTEE);
		}


		attributeValue = corporateOrg.getAttribute("allow_import_DLC");

		//Check the Corporate Org for the Import Letter of Credit
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.IMPORT_DLC);
			// Nar CR-818 Add DFP and DBA instrument type also if Import DLC is allow for customer
			codeVector.addElement(InstrumentType.DOCUMENTARY_BA);
			codeVector.addElement(InstrumentType.DEFERRED_PAY);
		}


		attributeValue = corporateOrg.getAttribute("allow_SLC");

		//Check the Corporate Org for the Stand by Letter of Credit
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.STANDBY_LC);
		}


		attributeValue = corporateOrg.getAttribute("allow_export_LC");

		//Check the Corporate Org for the Export Letter of Credit
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.EXPORT_DLC);
		}


		attributeValue = corporateOrg.getAttribute("allow_shipping_guar");

		//Check the Corporate Org for the Shipping Guarantee
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.SHIP_GUAR);
		}

		attributeValue = corporateOrg.getAttribute("allow_loan_request");

		//Check the Corporate Org for the Loan Request
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.LOAN_RQST);
		}

		attributeValue = corporateOrg.getAttribute("allow_funds_transfer");

		//Check the Corporate Org for the Funds Transfer
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.FUNDS_XFER);
		}

		attributeValue = corporateOrg.getAttribute("allow_domestic_payments");
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.DOM_PMT);
		}

		attributeValue = corporateOrg.getAttribute("allow_request_advise");

		//Check the Corporate Org for the Request Advise
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.REQUEST_ADVISE);
		}
                attributeValue = corporateOrg.getAttribute("allow_approval_to_pay");

		//Check the Corporate Org for the Approval to pay
		if( ( attributeValue != null )  &&
			( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		{
			codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
		}

		attributeValue = corporateOrg.getAttribute("allow_xfer_btwn_accts");
		if( ( attributeValue != null )  &&
				( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
	    {
		    codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
		}
		// IR T36000019769 Rel 8.3 START - Duplicate Instrument Types are getting added when panel auth is enabled
			attributeValue = corporateOrg.getAttribute("allow_xfer_btwn_accts_panel");
			if( ( attributeValue != null )  &&
					( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) &&
					!codeVector.contains(InstrumentType.XFER_BET_ACCTS))
		    {
			    codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
			}
			attributeValue = corporateOrg.getAttribute("allow_domestic_payments_panel");
			if( ( attributeValue != null )  &&
					( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) &&
					!codeVector.contains(InstrumentType.DOM_PMT))
		    {
			    codeVector.addElement(InstrumentType.DOM_PMT);
			}
			attributeValue = corporateOrg.getAttribute("allow_funds_transfer_panel");
			if( ( attributeValue != null )  &&
					( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) &&
					!codeVector.contains(InstrumentType.FUNDS_XFER))
		    {
			    codeVector.addElement(InstrumentType.FUNDS_XFER);
			}
			//IR T36000019769 Rel 8.3 START - Duplicate Instrument Types are getting added when panel auth is enabled
			//ctq add direct debits and receivables types
			
			//RPasupulati #IR T36000014543 Starts.
			//adding condition,If user have access to this instrument then only it will display
			attributeValue = corporateOrg.getAttribute("allow_direct_debit");
			if( ( attributeValue != null )  &&
					( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		    {            
			codeVector.addElement( InstrumentType.DIRECT_DEBIT_INSTRUCTION );
		    }
			attributeValue = corporateOrg.getAttribute("allow_arm_invoice");
			if( ( attributeValue != null )  &&
					( attributeValue.equals(TradePortalConstants.INDICATOR_YES)) )
		    {
            codeVector.addElement( InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT );
            codeVector.addElement( InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT );
		    }

			
			attributeValue = corporateOrg.getAttribute("allow_payables");
			if(TradePortalConstants.INDICATOR_YES.equals(attributeValue) &&
					!codeVector.contains(TradePortalConstants.PAYABLES_MGMT))
		    {
			    codeVector.addElement(TradePortalConstants.PAYABLES_MGMT);
			}
			
			//MEer 08/05/2015 Rel9.4 CR-932
			if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("enable_billing_instruments"))){
				if(StringFunction.isNotBlank(fieldName)) //Add Billing instrumentType only to instrument type dropdown for transactions
					codeVector.addElement(TradePortalConstants.BILLING);							
			}
			// Nar CR-818 Add Import collection instrument type if applicable
			attributeValue = corporateOrg.getAttribute("allow_imp_col");
			//Check the Corporate Org for the Import Collection
			if( TradePortalConstants.INDICATOR_YES.equals(attributeValue) )
			{
				codeVector.addElement(InstrumentType.IMPORT_COL);
			}




		corporateOrg.remove();

	}catch( java.rmi.RemoteException e ) {
  	    LOG.info("Error attempting to build list of viewable " +
	                       "(in Dropdown class) instrument types " + e.toString());
	    throw new ServletException(e.getMessage());

	}catch( com.amsinc.ecsg.frame.AmsException e ) {
	    LOG.info("Error attempting to build list of viewable " +
	                       "(in Dropdown class) instrument types " + e.toString());
	    throw new ServletException(e.getMessage());

	}catch( javax.ejb.RemoveException e ) {
  	    LOG.info("Error removing EJB in getViewableInstrumentTypes");
	    throw new ServletException(e.getMessage());

	}

		return codeVector;



 }


 public static String createSortedBankCountryCodeOptions(String selectedOption,
 			String localeName) throws ServletException
 {
 	 return createSortedBankCountryCodeOptions(selectedOption, localeName, null);
 }

  public static String createSortedBankCountryCodeOptions(String selectedOption,
 			String localeName, Vector codesToExclude) throws ServletException {

 	 DropdownOptions options = new DropdownOptions();
 	 options.setSelected(selectedOption);

 	 try {


 		 // Get the reference data and iterate through it to create the list
 		 // box html
 		 Hashtable refData;
 		 refData = ReferenceDataManager.getRefDataMgr().
 		 	getAllCodeAndDescr(TradePortalConstants.BANK_COUNTRY_CODE, localeName);

 		 Enumeration enumer = refData.keys();
 		 	while (enumer.hasMoreElements()) {
 		 		String code = (String) enumer.nextElement();

 		 		// Only add the code if it has not been included in the list of codes
 		 		// 	to be excluded
 		 		if( (codesToExclude == null) ||
 		 				!codesToExclude.contains(code))
 		 		{
 		 			// 	Use code for both value and displayed text
 		 			options.addOption(code, code);
 		 		}
 		 	}
 	 } catch (AmsException e) {
 		 throw new ServletException(e.getMessage());
 	 }

 	 return getSortedOptions(options, localeName);
 }
/*Added for CR-1051 by dpatra Dt 14-Jan-2016*/
  public static String createSortedRegAccTypeCodeOptions(String selectedOption,
			String localeName) throws ServletException
	{
		 return createSortedRegAccTypeCodeOptions(selectedOption, localeName, null);
	}
  /*Added for CR-1051 by dpatra Dt 14-Jan-2016*/
  public static String createSortedRegAccTypeCodeOptions(String selectedOption,
			String localeName, Vector codesToExclude) throws ServletException {

	 DropdownOptions options = new DropdownOptions();
	 options.setSelected(selectedOption);

	 try {


		 // Get the reference data and iterate through it to create the list
		 // box html
		 Hashtable refData;
		 refData = ReferenceDataManager.getRefDataMgr().
		 	getAllCodeAndDescr(TradePortalConstants.COPS_REGULATORY_ACCOUNT_TYPE, localeName);

		 Enumeration enumer = refData.keys();
		 	while (enumer.hasMoreElements()) {
		 		String code = (String) enumer.nextElement();

		 		// Only add the code if it has not been included in the list of codes
		 		// 	to be excluded
		 		if( (codesToExclude == null) ||
		 				!codesToExclude.contains(code))
		 		{
		 			// 	Use code for both value and displayed text
		 			options.addOption(code, code);
		 		}
		 	}
	 } catch (AmsException e) {
		 throw new ServletException(e.getMessage());
	 }

	 return getSortedOptions(options, localeName);
}
 public static Vector getCashManagementInstrumentTypes(FormManager formMgr,
			 String securityRights,
			   long organization_Oid,
			 String securityProfileType,
			 String ownerLevel,
			 long userOid)
throws ServletException {

Vector codeVector = new Vector();       //list of codes that are modifyable based on Bus. rules
//list of viewable types
Vector viewableVector = getViewableInstrumentTypes(formMgr, organization_Oid, securityProfileType, ownerLevel,userOid);//PPRAKASH IR PRUJ022438083

//If the user has the security right to modify an air waybill, and the corporation has
//defined that the airwaybill to be viewable...Then add the air waybill to the list of
//codes to be returned.
//Narayan IR-AOUL051863355 Rel 7.0.0.0 Begin
//Reomoved the restriction to visible cash Instrument type in dropdown even though user are able to delete or  authorized or route only.
//Funds Transfer
if( viewableVector.contains( InstrumentType.FUNDS_XFER ) )
codeVector.addElement(InstrumentType.FUNDS_XFER);


if( viewableVector.contains( InstrumentType.DOM_PMT ) )
codeVector.addElement(InstrumentType.DOM_PMT);

if( viewableVector.contains( InstrumentType.XFER_BET_ACCTS ) )
codeVector.addElement(InstrumentType.XFER_BET_ACCTS);
return codeVector;
}

public static Vector getTradeInstrumentTypes() throws ServletException {

	Vector codeVector = new Vector();         //list of all codes for the Admin user.

	codeVector.addElement(InstrumentType.AIR_WAYBILL);
	codeVector.addElement(InstrumentType.EXPORT_COL);
	codeVector.addElement(InstrumentType.NEW_EXPORT_COL); //Vasavi CR 524 03/31/2010 Add
	codeVector.addElement(InstrumentType.GUARANTEE);
	codeVector.addElement(InstrumentType.IMPORT_DLC);
	codeVector.addElement(InstrumentType.STANDBY_LC);
	codeVector.addElement(InstrumentType.EXPORT_DLC);
	codeVector.addElement(InstrumentType.INCOMING_SLC);
	codeVector.addElement(InstrumentType.CLEAN_BA);
	codeVector.addElement(InstrumentType.DOCUMENTARY_BA);
	codeVector.addElement(InstrumentType.REFINANCE_BA);
	codeVector.addElement(InstrumentType.INDEMNITY);
	codeVector.addElement(InstrumentType.DEFERRED_PAY);
	codeVector.addElement(InstrumentType.COLLECT_ACCEPT);
	codeVector.addElement(InstrumentType.INCOMING_GUA);
	codeVector.addElement(InstrumentType.IMPORT_COL);
	codeVector.addElement(InstrumentType.SHIP_GUAR);
	codeVector.addElement(InstrumentType.LOAN_RQST);
	codeVector.addElement(InstrumentType.REQUEST_ADVISE);
	codeVector.addElement(InstrumentType.APPROVAL_TO_PAY);
	codeVector.addElement(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT);
	return codeVector;

 }


	/**
	 * Creates a Suffix for the values in the dropdown
	 *
	 */
public static String getSuffixDropdownOptions(DocumentHandler inputDoc,
		String valueAttribute, String textAttribute, String suffixText,String refDataTableType,
		String selectedOption, SecretKey secretKey,String localeName,ResourceManager resMgr) throws ServletException {
	DropdownOptions options = new DropdownOptions();
	if (secretKey != null) {
		options.setSecretKeyForEncryption(secretKey);
	}
	options.setSelected(selectedOption);
	DocumentHandler xmlDoc = null;
	String value = null;
	Vector optionList = null;
	int numItems = 0;

	try {
		optionList = inputDoc.getFragments("/ResultSetRecord");
		numItems = optionList.size();

		for (int i = 0; i < numItems; i++) {
			xmlDoc = (DocumentHandler) optionList.elementAt(i);

			// Extract the value and text attributes from the document.
			try {
				value = xmlDoc.getAttribute("/" + valueAttribute);
			} catch (Exception e) {
				value = "0";
			}
			StringBuilder text = new StringBuilder();
			try {
				text.append(xmlDoc.getAttribute("/" + textAttribute));
			} catch (Exception e) {
				text.append("");
			}
			try {
				if(StringFunction.isNotBlank(refDataTableType)){
					String txt = "";
					if("P".equals(xmlDoc.getAttribute("/" + suffixText)))
					{
						txt = "InvoiceDefinition.PayableMgmt";	
					}
					else{
						txt = "InvoiceDefinition.ReceivableMgmt";
					}
					String suffix =	resMgr.getText(txt,TradePortalConstants.TEXT_BUNDLE);
				
				text.append(" - "+suffix);
				}
			} catch (Exception e) {
			
			}
			options.addOption(value, text.toString());
		}
	} catch (Exception e) {
		LOG.info("Error in Dropdown.getSuffixDropdownOptions "
				+ e.toString());
	}

	return getSortedOptions(options, localeName);
}


	/**
	 * Creates an xml string of <option> tags using reference data for a
	 * specific table and local.
	 *
	 * The purpose of this method is to provide the option tags to include
	 * within an HTML select.
	 *
	 * If the selectedOption is provided, it attempts to match a code with this
	 * value. If found, that option includes the selected="true" attribute.
	 *
	 * An example of how this might be used within a JSP <select> <option
	 * value="0"<-- default option -->/option>
	 * <%=Dropdown.createSortedRefDataIncludeOptions(formMgr, "usage", "", "")%>
	 * </select>
	 *
	 * @param selectedOption
	 *            String - Value to make the seleted item in the list box.
	 * @param localeName
	 *            String - Locale to retrieve ref data for, may be blank
	 * @param codesToInclude
	 *            Vector - a list of codes that should  be included in the
	 *            output
	 *
	 * @return string - HTML options tags that can be wrappered by a select
	 */
	public static String createSortedRefDataIncludeOptions(String tableType,
			String selectedOption, ResourceManager resMgr,String localeName, Vector codesToInclude,boolean fromTextRes)
	throws ServletException {

		DropdownOptions options = new DropdownOptions();
		options.setSelected(selectedOption);

		try {
			// Determine what reference data to use
			if (tableType == null)
				throw new AmsException("Need to send the tableType parameter");

			// Get the reference data and iterate through it to create the list
			// box html
			Hashtable refData;
			refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(
			tableType, localeName);

			Enumeration enumer = refData.keys();
			while (enumer.hasMoreElements()) {
				String code = (String) enumer.nextElement();
				String descr = (String) refData.get(code);
			
				if(fromTextRes)
				descr = resMgr.getText("TradingPartnerDetails."+code, TradePortalConstants.TEXT_BUNDLE);
					
				// Only include the code if it has not been placed in the list
				// of
				// codes to exclude
				if ((codesToInclude != null) && codesToInclude.contains(code)) {
					options.addOption(code, descr);
				}
			}
		} catch (AmsException e) {
			throw new ServletException(e.getMessage());
		}

		return getSortedOptions(options, localeName);
	}


	
	public static String createSortedRefDataOptionsNoDefault(String tableType, String localeName, Vector codesToExclude)
			throws ServletException {

		DropdownOptions options = getRefDataDropdownOptionsNoDefault(tableType, localeName, codesToExclude);

		return getSortedOptions(options, localeName);
	}
	
	private static DropdownOptions getRefDataDropdownOptionsNoDefault(String tableType, String localeName, Vector codesToExclude)
			throws ServletException {

		DropdownOptions options = new DropdownOptions();


		try {
			// Determine what reference data to use
			if (tableType == null)
				throw new AmsException("Need to send the tableType parameter");

			// Get the reference data and iterate through it to create the list
			// box html
			Hashtable refData;
			refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr(
					tableType, localeName);

			Enumeration enumer = refData.keys();
			while (enumer.hasMoreElements()) {
				String code = (String) enumer.nextElement();
				String descr = (String) refData.get(code);

				// Only include the code if it has not been placed in the list
				// of
				// codes to exclude
				if ((codesToExclude == null) || !codesToExclude.contains(code)) {
					options.addOption(code, descr);
				}
			}
		} catch (AmsException e) {
			throw new ServletException(e.getMessage());
		}
		return options;
	}

	/**
	 * Creates HTML string of <option> tags for GL Category dropdown.
	 *
	 *
	 * @param resMgr ResourceManager - the resource manager
	 * @param selectedOption String - Value to make the seleted item in the list box.
	 *
	 * @return StringBuffer - HTML options tags that can be wrappered by a select
	 */
	public static StringBuffer createGLCategoryOptions(ResourceManager resMgr,
		                                            String selectedOption)  throws ServletException {

		StringBuffer options = new StringBuffer();


		   options.append("<option value='");
		   options.append(TradePortalConstants.ERP_PAYMENT);
		   options.append("' ");
		   if (TradePortalConstants.ERP_PAYMENT.equals(selectedOption)) {
		      options.append(" selected ");
		   }
		   options.append(">");
		   options.append(resMgr.getText("ErpGlCodes.ERPPayment", TradePortalConstants.TEXT_BUNDLE));
		   options.append("</option>");

		   options.append("<option value='");
		   options.append(TradePortalConstants.ERP_OVERPAYMENT);
		   options.append("' ");
		   if (TradePortalConstants.ERP_OVERPAYMENT.equals(selectedOption)) {
		      options.append(" selected ");
		   }
		   options.append(">");
		   options.append(resMgr.getText("ErpGlCodes.ERPOverPayment", TradePortalConstants.TEXT_BUNDLE));
		   options.append("</option>");

		   options.append("<option value='");
		   options.append(TradePortalConstants.ERP_DISCOUNT);
		   options.append("' ");
		   if (TradePortalConstants.ERP_DISCOUNT.equals(selectedOption)) {
		      options.append(" selected ");
		   }
		   options.append(">");
		   options.append(resMgr.getText("ErpGlCodes.ERPDiscount", TradePortalConstants.TEXT_BUNDLE));
		   options.append("</option>");

		return options;
	}	
	
	/**
	  *
	  *AAlubala - Rel8.2 CR741 - 02/04/2012
	  *
	  * @param inputDoc DocumentHandler - The document to read values from
	  * @param valueAttribute String - The name of the attribute in the input document
	  *                                that represents the "value" for each option.
	  * @param textAttribute String  - The name of the multi attribute in the input document
	  *                                that represents the "text" for each option. Comma separated
	  *                                Here this will be a combination of code and description
	  *                               Code-Description concatenation. ERP GL Code (17 characters/alphanumeric) – 
	  *                               (hyphen) Description (30 characters / alphanumeric)
	  *                               Example:  GL Disc9 –Discount due to poor quality 
	  * @param selectedOption String - The value of the option to default as selected.
	  *                                If null, the first value defaults to selected.
	  * @param localeName - the locale of the user
	  *  @param filterOn - the property to exclude from the list. If it is null, then everything in the doc is inluded 
	  *  
	  *
	  * @return string - HTML options tags that can be wrappered by a select
	  */
	public static String createMultiTextOptions(DocumentHandler inputDoc,
            String valueAttribute,
	         String textAttribute,
	         String selectedOption,
            String localeName)
throws ServletException {
		return createMultiTextOptions(inputDoc,valueAttribute,textAttribute,selectedOption,null,null,localeName);
	}
	public static String createMultiTextOptions(DocumentHandler inputDoc,
		                                     String valueAttribute,
									         String textAttribute,
									         String selectedOption,
									         String filterValue,
									         String filterOn,
	                                         String localeName)
		   throws ServletException {
		
	    DropdownOptions options = new DropdownOptions();

	    options.setSelected(selectedOption);

		try {
		        Vector v = inputDoc.getFragments("/ResultSetRecord");
		        int numItems = v.size();
	            String value;
	            String text="";
	            String pre="";
	            String post="";
	            String filterVal="";

	            
	            String[] vals = textAttribute.split(",");

		        for (int i = 0; i < numItems; i++) {
			        DocumentHandler doc = (DocumentHandler) v.elementAt(i);
			        //
			        //if filterOn is not null, then only process
			        //elements matching filterOn string, otherwise, process all
			        //
			        // Extract the value and text attributes from the document.
			        try {
				        value = doc.getAttribute("/" + valueAttribute);
				        if(StringFunction.isNotBlank(filterValue) && StringFunction.isNotBlank(filterOn)){
				        	filterVal = doc.getAttribute("/" + filterValue);
				        	if(!filterVal.equalsIgnoreCase(filterOn)) continue;
				        }
			        } catch (Exception e) {
				        value = "0";
			        }
			        try {
/*			        	//The Text will be multi-valued, 
			        	//so, loop on list and get all text attrs
			        	Iterator<String> it = multiVals.iterator();
			        	while(it.hasNext()){
			        		String elem = it.next();
			        		text = text + doc.getAttribute("/" + elem);
			        	}*/
			        	//This is a simple case of just two values.
			        	//TODO: Refactor so it can use more than two values
			        	//TODO: separator to be sent along , if not default to dash.
			        	pre = doc.getAttribute("/" +vals[0]);
			        	post = doc.getAttribute("/" +vals[1]);
			        	
			        	if(StringFunction.isNotBlank(post) && StringFunction.isNotBlank(pre)){
			        		text = pre+"-"+post;
			        	}else
			        		text="";
				       
			        } catch (Exception e) {
				        text = "";
			        }

			        options.addOption(value, text);
		        }
	        } catch (Exception e) {
		        LOG.info("Error in Dropdown.getDropdownOptions " + e.toString());
	        }



		return getSortedOptions(options, localeName);

	}	
	
	public static Vector getCreateModifySettlementInstrumentTypes(FormManager formMgr, String securityRights, long organization_Oid,
			 String securityProfileType, String ownerLevel, boolean isSettlementInstr) throws AmsException {
		
		List<String> codeList = new ArrayList<String>();      
        List<String> viewableList = getViewableSettlementInstrumentTypes(formMgr, organization_Oid, securityProfileType, 
        		ownerLevel, isSettlementInstr);
        
        if( SecurityAccess.hasRights(securityRights, SecurityAccess.SIT_CREATE_MODIFY ) )
    	{
        	codeList = viewableList;
    	}
		
		return new Vector(codeList);
	}

	private static List<String> getViewableSettlementInstrumentTypes( FormManager formMgr, long organization_Oid, String securityProfileType, String ownerLevel,
			boolean isSettlementInstr) throws AmsException {

		List<String> codeList = new ArrayList<String>();
		if (TradePortalConstants.ADMIN.equals(securityProfileType)) {
			codeList = getAllSettlementInstrumentTypesForAdmin(ownerLevel, isSettlementInstr);
		} else {

			try {
				CorporateOrganization corporateOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(),
								"CorporateOrganization", organization_Oid);
				if (TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_loan_request"))) {
					codeList.add(InstrumentType.LOAN_RQST);
				}
				if ( isSettlementInstr ) {
					if (TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_import_DLC"))) {
						codeList.add(InstrumentType.DOCUMENTARY_BA);
						codeList.add(InstrumentType.DEFERRED_PAY);
					}
					if (TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_imp_col"))) {
						codeList.add(InstrumentType.IMPORT_COL);
					}
				}
				corporateOrg.remove();
			} catch (AmsException | RemoteException | RemoveException e) {
				LOG.info("Error attempting to build list of viewable " +
	                       "(in Dropdown class) instrument types " + e.toString());
	             throw new AmsException(e.getMessage());
			}
		}
		return codeList;
	}
	
	/**
	 * This method is used to list all the instrument list for ADMIN to settlment instruction message and response
	 * @param ownerLevel
	 * @param isSettlementInstr
	 * @return
	 */
	private static List<String> getAllSettlementInstrumentTypesForAdmin( String ownerLevel, boolean isSettlementInstr ) {
		
		List<String> codeList = new ArrayList<String>();		
		codeList.add(InstrumentType.LOAN_RQST);
		if ( isSettlementInstr ) {
			codeList.add(InstrumentType.IMPORT_COL);
			codeList.add(InstrumentType.DOCUMENTARY_BA);
			codeList.add(InstrumentType.DEFERRED_PAY);
		}		
		return codeList;
	}
	
}