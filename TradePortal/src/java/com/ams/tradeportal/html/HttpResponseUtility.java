package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.PropertyResourceBundle;

import com.ams.tradeportal.common.TradePortalConstants;

public class HttpResponseUtility {
private static final Logger LOG = LoggerFactory.getLogger(HttpResponseUtility.class);
    public final static String LOGON_SUCCESS = "logonSuccess";
	public final static String SESSION_TIMEOUT = "sessionTimeout";
	public final static String INCOMPLETE_USER = "incompleteUser";
	public final static String ACCESS_FAILURE = "accessFailure";
	public final static String LOCKED_USER = "lockedUser";
	public final static String LOGON_TOLERANCE_TIMEOUT = "logonToleranceTimeout";

	public static int getResponseCode(String orgId, String user, String responseType) {
		String client_bank="";
		if(!TradePortalConstants.GLOBAL_ORG_ID.equalsIgnoreCase(orgId )){
			client_bank=orgId;
		}
		try {
			// lookup property of form "httpResponseCode." + client bank
			// + "." + responseType
			PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
			// if found return that code
			// else return 200
			return Integer.parseInt(portalProperties.getString("httpResponseCode."+ client_bank + "." + responseType));
		} catch (Exception ex) {
			return 200;
		}
	}

}
