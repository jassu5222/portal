package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InputField {
private static final Logger LOG = LoggerFactory.getLogger(InputField.class);
/**
 * InputField constructor comment.
 */
public InputField() {
	super();
}

/**
 * This static method creates html for input checkbox field.  Various
 * parameters are incorporated into resulting html.  If a parm value is
 * blank, the attribute is not returned (eq, if fieldName is blank, the
 * the name= attribute is not created).
 *
 * Two styles are provided to allow both the checkbox and the
 * displayed text to have differing styles.
 *
 * If the isReadOnly flag is set, rather than returning an
 * input checkbox field, an "readonly" checkbox image is used.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param displayedValue java.lang.String - becomes the text displayed to user
 * @param checked boolean - indicates if the button is selected
 * @param textStyle java.lang.String - becomes the 'class' attribute of the displayed text
 * @param buttonStyle java.lang.String - becomes the 'class' attribute of the checkbox
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createCheckboxField(String fieldName,
	                                     String value,
	                                     String displayedValue,
	                                     boolean checked,
	                                     String textStyle,
	                                     String buttonStyle,
	                                     boolean isReadOnly) {

	return createCheckboxField(fieldName, value, displayedValue, checked,
		                       textStyle, buttonStyle, isReadOnly, "");

}

/**
 * This static method creates html for input checkbox field.  Various
 * parameters are incorporated into resulting html.  If a parm value is
 * blank, the attribute is not returned (eq, if fieldName is blank, the
 * the name= attribute is not created).
 *
 * Two styles are provided to allow both the checkbox and the
 * displayed text to have differing styles.
 *
 * If the isReadOnly flag is set, rather than returning an
 * input checkbox field, an "readonly" checkbox image is used.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param displayedValue java.lang.String - becomes the text displayed to user
 * @param checked boolean - indicates if the button is selected
 * @param textStyle java.lang.String - becomes the 'class' attribute of the displayed text
 * @param buttonStyle java.lang.String - becomes the 'class' attribute of the checkbox
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createCheckboxField(String fieldName,
	                                     String value,
	                                     String displayedValue,
	                                     boolean checked,
	                                     String textStyle,
	                                     String buttonStyle,
	                                     boolean isReadOnly,
	                                     String extraTags) {

	// We assume the caller is not working from an express template.
	return createCheckboxField(fieldName, value, displayedValue, checked,
		                       textStyle, buttonStyle, isReadOnly, false, extraTags);


}

/**
 * This static method creates html for input checkbox field.  Various
 * parameters are incorporated into resulting html.  If a parm value is
 * blank, the attribute is not returned (eq, if fieldName is blank, the
 * the name= attribute is not created).
 *
 * Two styles are provided to allow both the checkbox and the
 * displayed text to have differing styles.
 *
 * If the isReadOnly flag is set, rather than returning an
 * input checkbox field, an "readonly" checkbox image is used.
 *
 * Special caution: because checkboxes with a N value are not sent in the request,
 * we may have a situation of a readonly checkbox that is checked but the form is
 * otherwise editable (as in the case of instruments created from express templates).
 * In this case the checkbox value of Y must be sent otherwise the pre-edit
 * mediators will reset the value to N.  Therefore, an additional parameter,
 * isFromExpress allows the caller to indicate this condition.  In this case, if the
 * checkbox is readonly, AND the isFromExpress is true AND the checkbox is checked,
 * then in addition to the readonly checkbox image being returned, a hidden text
 * field with the value specified is returned.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param displayedValue java.lang.String - becomes the text displayed to user
 * @param checked boolean - indicates if the button is selected
 * @param textStyle java.lang.String - becomes the 'class' attribute of the displayed text
 * @param buttonStyle java.lang.String - becomes the 'class' attribute of the checkbox
 * @param isReadOnly boolean - if true, returns value as text
 * @param isFromExpress boolean - if true and is readonly and is checked, returns
 *                                a hidden text field.  This ensures that Y values
 *                                remain Y (rather than being reset to N in premediators)
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createCheckboxField(String fieldName,
	                                     String value,
	                                     String displayedValue,
	                                     boolean checked,
	                                     String textStyle,
	                                     String buttonStyle,
	                                     boolean isReadOnly,
	                                     boolean isFromExpress,
	                                     String extraTags) {


	StringBuffer inputField = new StringBuffer("");

	if (isReadOnly) {
		// Display a "readonly" image of a checkbox
		if (checked) {
			// Image of a checked checkbox
	        // W Zhu 7/23/07 ANUH062057065 use absolution path to the image.
			inputField.append("<img src=/portal/images/Checkbox_On_grey.gif ");
		} else {
			// Image of a non-checked checkbox
	        // W Zhu 7/23/07 ANUH062057065 use absolution path to the image.
			inputField.append("<img src=/portal/images/Checkbox_Off_grey.gif ");
		}
		inputField.append("width=10 height=11 border=0 ");
		inputField.append("name=");
		inputField.append(fieldName);
		inputField.append(">");

		// The field is readonly.  If the checkbox is from an express
		// template and is checked, also return a hidden text field
		// with a Y value.
		if (isFromExpress && checked) {
		// Otherwise created the input checkbox field.
		inputField.append("<input type=hidden ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}

		if (!value.equals("")) {
			inputField.append( " value=\"");
			inputField.append(value);
			inputField.append("\"");
		}
		inputField.append(">");

		}

	} else {
		// Otherwise created the input checkbox field.
		inputField.append("<input type=checkbox ");

		if (checked) {
			inputField.append(" checked");
		}

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
		if (!value.equals("")) {
			inputField.append( " value=\"");
			inputField.append(value);
			inputField.append("\"");
		}
		if (!buttonStyle.equals("")) {
			inputField.append(" class=");
			inputField.append(buttonStyle);
		}
		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");
	}

	// Now display the text for the checkbox or image.
	// Use a span tag to allow the style to be applied.
	inputField.append("<span ");
	if (!textStyle.equals("")) {
		inputField.append("class=");
		inputField.append(textStyle);
	}
	inputField.append(">");
	inputField.append(displayedValue);
	inputField.append("</span>");

	return inputField.toString();
}

/**
 * This static method creates html for input radio button field.  Various
 * parameters are incorporated into resulting html.  If a parm value is
 * blank, the attribute is not returned (eq, if fieldName is blank, the
 * the name= attribute is not created).
 *
 * Two styles are provided to allow both the radio button and the
 * displayed text to have differing styles.
 *
 * If the isReadOnly flag is set, rather than returning an
 * input radio field, an "readonly" radio button image is used.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param displayedValue java.lang.String - becomes the text displayed to user
 * @param checked boolean - indicates if the button is selected
 * @param textSstyle java.lang.String - becomes the 'class' attribute of the displayed text
 * @param buttonStyle java.lang.String - becomes the 'class' attribute of the button
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createRadioButtonField(String fieldName,
	                                        String value,
	                                        String displayedValue,
	                                        boolean checked,
	                                        String textStyle,
	                                        String buttonStyle,
	                                        boolean isReadOnly) {

	return createRadioButtonField(fieldName, value, displayedValue,
		                          checked, textStyle, buttonStyle,
		                          isReadOnly, "");
}

/**
 * This static method creates html for select field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If defaultValue is given (not blank), an option tag is
 * created using the defaultValue and defaultText.  This becomes
 * the first option tag.  The remaining options come from the
 * options parm (and must be valid HTML option tags)
 *
 * If the isReadOnly flag is set, rather than returning an
 * select field, simple text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param defaultValue java.lang.String - becomes the 'value' for
 *                  the default option
 * @param defaultText java.lang.String - becomes the displayed text
 *                  for the default option tag
 * @param options java.lang.String - preformatted option tags
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createSelectField(String fieldName,
	                                   String defaultValue,
	                                   String defaultText,
	                                   String options,
	                                   String style,
	                                   boolean isReadOnly) {

	return createSelectField(fieldName, defaultValue, defaultText,
		                     options, style, isReadOnly, "");
}

/**
 * This static method creates html for input text field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input text field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param maxLength java.lang.String - becomes the 'maxlength' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createTextField(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly) {

	return createTextField(fieldName, value, maxLength, size, style, isReadOnly, "");

}

/**
 * This static method creates html for input file field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input file field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createFileField(String fieldName,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly)
{
   return createFileField(fieldName, size, style, isReadOnly, "");
}

/**
 * Parses a string composed of valid HTML option tags and
 * searches for a "selected" value.  Once found the displayed
 * text value associated with that option is extracted.
 *
 * For example, in this option tag "the value" is returned.
 *   &lt;option selected value="x"&gt;the value&lt;/option&gt;
 *
 * @return java.lang.String - displayed text for selected option
 * @param options java.lang.String - valid HTML options
 */
private static String parseSelectedText(String options) {
	String textValue = "";

	int select = options.indexOf("selected");

	if (select > -1)
	 {
		int start = options.indexOf(">", select);
		int end = options.indexOf("</option>", select);

		try {
			textValue = options.substring(start+1, end);
		} catch (Exception e) {
			textValue = "";
		}
	 }

    if (textValue.equals(""))
        textValue = "&nbsp;";

	return textValue;
}

/**
 * This static method creates html for input radio button field.  Various
 * parameters are incorporated into resulting html.  If a parm value is
 * blank, the attribute is not returned (eq, if fieldName is blank, the
 * the name= attribute is not created).
 *
 * Two styles are provided to allow both the radio button and the
 * displayed text to have differing styles.
 *
 * If the isReadOnly flag is set, rather than returning an
 * input radio field, an "readonly" radio button image is used.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param displayedValue java.lang.String - becomes the text displayed to user
 * @param checked boolean - indicates if the button is selected
 * @param textSstyle java.lang.String - becomes the 'class' attribute of the displayed text
 * @param buttonStyle java.lang.String - becomes the 'class' attribute of the button
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createRadioButtonField(String fieldName,
	                                        String value,
	                                        String displayedValue,
	                                        boolean checked,
	                                        String textStyle,
	                                        String buttonStyle,
	                                        boolean isReadOnly,
	                                        String extraTags) {

	StringBuffer inputField = new StringBuffer("");

	if (isReadOnly) {
		// Display a "readonly" image of a radio button
		if (checked)
	        // W Zhu 7/23/07 ANUH062057065 use absolution path to the image.
			inputField.append("<img src=/portal/images/Radio_On_grey.gif ");
		else
	        // W Zhu 7/23/07 ANUH062057065 use absolution path to the image.
			inputField.append("<img src=/portal/images/Radio_Off_grey.gif ");
		inputField.append("width=10 height=11 border=0 ");
		inputField.append("name=");
		inputField.append(fieldName);
		inputField.append(">");
	} else {
		// Otherwise created the input radio button field.
		inputField.append("<input type=radio ");

		if (checked) {
			inputField.append(" checked");
		}

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
		if (!value.equals("")) {
			inputField.append( " value=\"");
			inputField.append(value);
			inputField.append("\"");
		}
		if (!buttonStyle.equals("")) {
			inputField.append(" class=");
			inputField.append(buttonStyle);
		}
		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");
	}

	// Now display the text for the button or image.  Use a span tag to allow
	// the style to be applied.
	inputField.append("<span ");
	if (!textStyle.equals("")) {
		inputField.append("class=");
		inputField.append(textStyle);
	}
	inputField.append(">");
	inputField.append(displayedValue);
	inputField.append("</span>");

	return inputField.toString();
}

/**
 * This static method creates html for select field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If defaultValue is given (not blank), an option tag is
 * created using the defaultValue and defaultText.  This becomes
 * the first option tag.  The remaining options come from the
 * options parm (and must be valid HTML option tags)
 *
 * If the isReadOnly flag is set, rather than returning an
 * select field, simple text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param defaultValue java.lang.String - becomes the 'value' for
 *                  the default option
 * @param defaultText java.lang.String - becomes the displayed text
 *                  for the default option tag
 * @param options java.lang.String - preformatted option tags
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createSelectField(String fieldName,
	                                   String defaultValue,
	                                   String defaultText,
	                                   String options,
	                                   String style,
	                                   boolean isReadOnly,
	                                   String extraTags) {
	StringBuffer inputField = new StringBuffer("");

	if (isReadOnly) {
		inputField.append("<span ");
		if (!style.equals("")) {
			inputField.append("class=");
			inputField.append(style);
		}
		inputField.append(">");
		inputField.append(parseSelectedText(options));
		inputField.append("</span>");
	} else {
		inputField.append("<select ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");
		if (!defaultText.equals("")) {
			inputField.append("<option value=\"");
			inputField.append(defaultValue);
			inputField.append("\">");
			inputField.append(defaultText);
			inputField.append("</option>");
		}
		inputField.append(options);

		inputField.append("</select>");
	}

	return inputField.toString();
}

//IAZ CR-586 IR-VRUK091652765 10/05/10 Begin
//This static method call createTextField() with all paarmeters
//However, if the result of called routine is a label (vs. text filed) -
//e.g., the data is protected, the hidden field is created with the same
//value to be send to the server on save()
public static String createTextFieldOrLabel(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags)
 {

	String outputData = createTextField(fieldName, value, maxLength, size, style, isReadOnly, extraTags);

	if (isReadOnly)
		outputData += " <input type=hidden name=\"" + fieldName + "\" value=\"" + value + "\">";

	return outputData;

 }
 //IAZ CR-586 IR-VRUK091652765 10/05/10 End

/**
 * This static method creates html for input text field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input text field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param maxLength java.lang.String - becomes the 'maxlength' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createTextField(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags) {
	StringBuffer inputField = new StringBuffer("");

	// [BEGIN] PPX-050 - jkok
	value = StringFunction.xssCharsToHtml(value);
	// [END] PPX-050 - jkok

	if (isReadOnly) {
		inputField.append("<span ");
		if (!style.equals("")) {
			inputField.append("class=");
			inputField.append(style);
		}
		inputField.append(">");
		// Now output the value.  If value is "" or null, output
		// a non-breaking space.
		if ((value == null) || value.trim().equals("")) {
			inputField.append("&nbsp;");
		} else {
			inputField.append(value);
		}
		inputField.append("</span>");
	} else {
		inputField.append("<input type=text ");

		if (!fieldName.equals("")) {
			inputField.append(" name=\"");
			inputField.append(fieldName);
			inputField.append("\"");
		}
		if (!size.equals("")) {
			inputField.append(" size=");
			inputField.append(size);
		}
		if (!maxLength.equals("")) {
			inputField.append(" maxlength=");
			inputField.append(maxLength);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		if (value!=null && !value.equals("")) {
			inputField.append( " value=\"");
			inputField.append(value);
			inputField.append("\"");
		}
		// IR-IRUK081063995 rkazi BEGIN
		if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
		}
		// IR-IRUK081063995 rkazi END

		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");
	}
	return inputField.toString();
}

/**
 * This static method creates html for input file field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input file field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createFileField(String fieldName,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags)
{
   StringBuffer inputField = new StringBuffer("");

   if (isReadOnly)
   {
      inputField.append("<span ");
      if (!style.equals(""))
      {
         inputField.append("class=");
         inputField.append(style);
      }
      inputField.append(">");
      inputField.append("&nbsp;");
      inputField.append("</span>");
   }
   else
   {
      inputField.append("<input type=file ");

      if (!fieldName.equals(""))
      {
         inputField.append(" name=");
         inputField.append(fieldName);
      }
      if (!size.equals(""))
      {
         inputField.append(" size=");
         inputField.append(size);
      }
      if (!style.equals(""))
      {
         inputField.append(" class=");
         inputField.append(style);
      }

      inputField.append(" ");
      inputField.append(extraTags);
      inputField.append(">");
   }

   return inputField.toString();
}

/**
 * This static method creates html for input password field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input password field, a string of *'s is returned (based on length)
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param maxLength java.lang.String - becomes the 'maxlength' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createPasswordField(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly) {

	return createPasswordField(fieldName, value, maxLength, size, style, isReadOnly, "");

}public static String createPasswordField(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags) {
	StringBuffer inputField = new StringBuffer("");

	// [BEGIN] PPX-050 - jkok
	value = StringFunction.xssCharsToHtml(value);
	// [END] PPX-050 - jkok

	if (isReadOnly) {
		inputField.append("<span ");
		if (!style.equals("")) {
			inputField.append("class=");
			inputField.append(style);
		}
		inputField.append(">");
		// Now output the value.  If value is "" or null, output
		// a non-breaking space.
		if ((value == null) || value.trim().equals("")) {
			inputField.append("&nbsp;");
		} else {
			inputField.append("********");
		}
		inputField.append("</span>");
	} else {
		inputField.append("<input type=password ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
		if (!size.equals("")) {
			inputField.append(" size=");
			inputField.append(size);
		}
		if (!maxLength.equals("")) {
			inputField.append(" maxlength=");
			inputField.append(maxLength);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		if (!value.equals("")) {
			inputField.append( " value=\"");
			inputField.append(value);
			inputField.append("\"");
		}
		// IR-IRUK081063995 rkazi BEGIN
		if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
		}
		// IR-IRUK081063995 rkazi END
		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");
	}
	return inputField.toString();
}

/**
 * This static method creates html for input text field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input text field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param maxLength java.lang.String - becomes the 'maxlength' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 */
public static String createTextArea(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly) {

	return createTextArea(fieldName, value, maxLength, size, style, isReadOnly, "", null);

}

/**
 * This static method creates html for input text field.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * input text field, simple paragraph text is returned.
 *
 * If the wrapText flag is set, newline characters are inserted if
 * a line is more than lineWidth characters long.
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the 'value' attribute
 * @param maxLength java.lang.String - becomes the 'maxlength' attribute
 * @param size java.lang.String - becomes the 'size' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param wrapText boolean - if true, inserts newlines based on line width
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag
 * @param wrapMode String - the setting for the wrap attribute
 */
public static String createTextArea(String fieldName,
	                                 String value,
	                                 String maxLength,
	                                 String size,
	                                 String style,
	                                 boolean isReadOnly,
	                                 boolean wrapText,
	                                 String extraTags,
                                         String wrapMode) {

	// Check to wrap long lines in readonly mode
	if (isReadOnly && wrapText)
	{
		value = formatStringWrapText(value, Integer.parseInt(maxLength));
		value = formatStringWithSpaces(value);
	}
	return createTextArea(fieldName, value, maxLength, size, style, isReadOnly, extraTags, wrapMode);

}


/**
 * This static method creates html for input textarea.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * textarea field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the text of the field
 * @param cols java.lang.String - becomes the 'cols' attribute
 * @param rows java.lang.String - becomes the 'rows' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag
 * @param wrapMode String - the setting for the wrap attribute
 */
public static String createTextArea(String fieldName,
	                                 String value,
	                                 String cols,
	                                 String rows,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags)
{
   return createTextArea(fieldName, value, cols, rows, style, isReadOnly, extraTags, null);
}



/**
 * This static method creates html for input textarea.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * textarea field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the text of the field
 * @param cols java.lang.String - becomes the 'cols' attribute
 * @param rows java.lang.String - becomes the 'rows' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 */
public static String createTextArea(String fieldName,
	                                 String value,
	                                 String cols,
	                                 String rows,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags,
                                         String wrapMode) {
	StringBuffer inputField = new StringBuffer("");

	// [BEGIN] PPX-050-02 - hml
	value = StringFunction.xssCharsToHtml(value);
	// [END] PPX-050-02 - hml

	if (isReadOnly) {
		// Replace new lines with <br> tags
		value = formatStringWithBR(value);

		inputField.append("<table width=");

		if( (cols != null) && (!cols.equals("")) )
        {
            Integer widthVal = new Integer(cols);
            if (fieldName.equals("GoodsDescText") || fieldName.equals("POLineItems"))
            {
               widthVal = new Integer(widthVal.intValue() * 8 + 12);
            }
            else
            {
               widthVal = new Integer( widthVal.intValue() * 5 );
            }

		    inputField.append( "\"" + widthVal.toString() + "\" border=1 ");
		}else{
		    inputField.append("100% border=1 ");
		}
		inputField.append("cellspacing=0 cellpadding=3>");
		inputField.append("<tr><td>");
		if (!style.equals("")) {
			inputField.append("<p class=");
			inputField.append(style);
			inputField.append(">");
		}
		// Now output the value.  If value is "" or null, output
		// a non-breaking space.
		if ((value == null) || value.trim().equals("")) {
			inputField.append("&nbsp;");
		} else {
			inputField.append(value);
		}
		// Close all the tags
		inputField.append("</p></td></tr></table>");
	} else {

		// [BEGIN] PPX-050-02 - hml
		// [BEGIN] PPX-050 - jkok
		//value = StringFunction.xssCharsToHtml(value);
		// [END] PPX-050 - jkok
		// [END] PPX-050-02 - hml
		inputField.append("<textarea ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
                if((wrapMode == null) || wrapMode.equals(""))
   		   inputField.append(" wrap=VIRTUAL");
                else
                   inputField.append(" wrap="+wrapMode);

		if (!cols.equals("")) {
			inputField.append(" cols=");
			inputField.append(cols);
		}
		if (!rows.equals("")) {
			inputField.append(" rows=");
			inputField.append(rows);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		// IR-IRUK081063995 rkazi BEGIN
		if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
		}
		// IR-IRUK081063995 rkazi END

		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(">");

		inputField.append(value);

		inputField.append("</textarea>");
	}
	return inputField.toString();
}

// Fix of IR#6760 CR-708 10/22/2012 Srinivasu_D Start 

/**
 * This static method creates html for input textarea.  Various
 * parameters are incorporated into resulting html.  If a value
 * is blank, the attribute is not returned (eq, if fieldName
 * is blank, the name= attribute is not created).
 *
 * If the isReadOnly flag is set, rather than returning an
 * textarea field, simple paragraph text is returned
 *
 * @return java.lang.String - the resulting html
 * @param fieldName java.lang.String - becomes the 'name' attribute
 * @param value java.lang.String - becomes the text of the field
 * @param cols java.lang.String - becomes the 'cols' attribute
 * @param rows java.lang.String - becomes the 'rows' attribute
 * @param style java.lang.String - becomes the 'class' attribute
 * @param isReadOnly boolean - if true, returns value as text
 * @param extraTags String - extra tags that are added as is
 *                            to the input tag (non-readonly mode only)
 * @param isScrollBar - determines if scrollbar needs in case of read-only mode
 */
public static String createTextArea(String fieldName,
	                                 String value,
	                                 String cols,
	                                 String rows,
	                                 String style,
	                                 boolean isReadOnly,
	                                 String extraTags,
                                     String wrapMode,
									 boolean isScrollBar) {
	StringBuffer inputField = new StringBuffer("");
	
	// [BEGIN] PPX-050-02 - hml
	value = StringFunction.xssCharsToHtml(value);
	// [END] PPX-050-02 - hml

	if (isReadOnly && isScrollBar) {
		
		inputField.append("<textarea ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
                if((wrapMode == null) || wrapMode.equals(""))
   		   inputField.append(" wrap=VIRTUAL");
                else
                   inputField.append(" wrap="+wrapMode);

		if (!cols.equals("")) {
			inputField.append(" cols=");
			inputField.append(cols);
		}
		if (!rows.equals("")) {
			inputField.append(" rows=");
			inputField.append(rows);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		// IR-IRUK081063995 rkazi BEGIN
		if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
		}
		// IR-IRUK081063995 rkazi END

		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(" readOnly =\"true\" ");
		inputField.append(">");

		inputField.append(value);

		inputField.append("</textarea>");
	} else if (isReadOnly && !isScrollBar) {
		// Replace new lines with <br> tags
		value = formatStringWithBR(value);

		inputField.append("<table width=");

		if( (cols != null) && (!cols.equals("")) )
        {
            Integer widthVal = new Integer(cols);
            if (fieldName.equals("GoodsDescText") || fieldName.equals("POLineItems"))
            {
               widthVal = new Integer(widthVal.intValue() * 8 + 12);
            }
            else
            {
               widthVal = new Integer( widthVal.intValue() * 5 );
            }

		    inputField.append( "\"" + widthVal.toString() + "\" border=1 ");
		}else{
		    inputField.append("100% border=1 ");
		}
		inputField.append("cellspacing=0 cellpadding=3>");
		inputField.append("<tr><td>");
		if (!style.equals("")) {
			inputField.append("<p class=");
			inputField.append(style);
			inputField.append(">");
		}
		// Now output the value.  If value is "" or null, output
		// a non-breaking space.
		if ((value == null) || value.trim().equals("")) {
			inputField.append("&nbsp;");
		} else {
			inputField.append(value);
		}
		// Close all the tags
		inputField.append("</p></td></tr></table>");
	} 
	else {

		// [BEGIN] PPX-050-02 - hml
		// [BEGIN] PPX-050 - jkok
		//value = StringFunction.xssCharsToHtml(value);
		// [END] PPX-050 - jkok
		// [END] PPX-050-02 - hml
		inputField.append("<textarea ");

		if (!fieldName.equals("")) {
			inputField.append(" name=");
			inputField.append(fieldName);
		}
                if((wrapMode == null) || wrapMode.equals(""))
   		   inputField.append(" wrap=VIRTUAL");
                else
                   inputField.append(" wrap="+wrapMode);

		if (!cols.equals("")) {
			inputField.append(" cols=");
			inputField.append(cols);
		}
		if (!rows.equals("")) {
			inputField.append(" rows=");
			inputField.append(rows);
		}
		if (!style.equals("")) {
			inputField.append(" class=");
			inputField.append(style);
		}
		// IR-IRUK081063995 rkazi BEGIN
		if (!extraTags.toLowerCase().contains("autocomplete")){
			extraTags = extraTags.concat(" AutoComplete =\"off\" ");
		}
		// IR-IRUK081063995 rkazi END

		inputField.append(" ");
		inputField.append(extraTags);
		inputField.append(" readOnly =\"true\" ");
		inputField.append(">");

		inputField.append(value);

		inputField.append("</textarea>");
	}
	
	return inputField.toString();
}
// Fix of IR#6760 CR-708 10/22/2012 Srinivasu_D End 

/**
 * Given a String value, this method returns the String that can be used
 * for SQL query by adding an additional single quote (') character for any
 * single quote character that is found in the given string.
 *
 *
 * @param String value - the String to convert from
 * @return java.lang.String   - the converted String to use for Sql search.
 */
public static String prepareStringForSql( String value) {
    int startIndex = 0;
    int aphrostapheIndex = -1;
    StringBuffer sqlString = new StringBuffer();

    if (value == null || value.equals("")) return value;

    aphrostapheIndex = value.indexOf("'", startIndex);

    while (aphrostapheIndex >= 0)
    {
	sqlString.append(value.substring(startIndex, aphrostapheIndex+1));
	sqlString.append("'");
	startIndex = ++aphrostapheIndex;
	aphrostapheIndex = value.indexOf("'", startIndex);
    }

    if (startIndex < value.length())
	sqlString.append(value.substring(startIndex, value.length()));

    return sqlString.toString();


}

/**
 * Given a String value, this method replaces all newline characters
 * with <br> tags.  The <br> tag will be used to display newlines
 * in html.
 *
 *
 * @param String value - the String to convert from
 * @return java.lang.String - the reformatted String
 */
public static String formatStringWithBR( String value) {
    int startIndex = 0;
    int newlineIndex = -1;
    StringBuffer newString = new StringBuffer();

    if (value == null || value.equals("")) return value;

    newlineIndex = value.indexOf("\n", startIndex);

    while (newlineIndex > 0)
    {
	newString.append(value.substring(startIndex, newlineIndex));
	newString.append("<br>");
	startIndex = ++newlineIndex;
	newlineIndex = value.indexOf("\n", startIndex);
    }

    if (startIndex < value.length())
	newString.append(value.substring(startIndex, value.length()));

    return newString.toString();
}

/**
 * Given a String value and a max line width, this method returns the
 * String formatted to display carriage returns and maximum line width
 * wrapping as newlines.  The <br> tag will be used to display newlines
 * in html.
 *
 *
 * @param String value - the String to convert from
 * @param String lineWidth - the max number of chars per line
 * @return java.lang.String - the reformatted String
 */
public static String formatStringWrapText( String value,
						int lineWidth) {
    int startIndex = 0;
    int newlineIndex = -1;
    String line;
    StringBuffer newString = new StringBuffer();

    if (value == null || value.equals("")) return value;

    // Look for a newline
    newlineIndex = value.indexOf("\n", startIndex);

    // Loop through the length of the string
    while (startIndex < value.length())
    {
	// If newline is beyond max line width, insert newline
	if (newlineIndex > startIndex + lineWidth + 1)
	{
		newString.append(value.substring(startIndex, startIndex + lineWidth));
		newString.append("\n");
		startIndex += lineWidth;
	}
	// If newline is within max limit, skip section
	else if (newlineIndex >= 0)
	{
		newString.append(value.substring(startIndex, newlineIndex + 1));
		startIndex = ++newlineIndex;
		newlineIndex = value.indexOf("\n", startIndex);
	}
	// If line is beyond max line width, insert newline
	else if (startIndex + lineWidth < value.length())
	{
		newString.append(value.substring(startIndex, startIndex + lineWidth));
		newString.append("\n");
		startIndex += lineWidth;
	}
	// Just append rest of line
	{
		newString.append(value.substring(startIndex, value.length()));
		startIndex = value.length();
	}
    }

    return newString.toString();
}

/**
 * Given a String value and a max line width, this method returns the
 * String formatted to display blank spaces.  Tab characters are not
 * recognized by html, therefore we simply replace all blank spaces
 * with a &nbsp;.  We replace all blank spaces with this character
 * because the tab characters may not always be the same length.
 *
 *
 * @param String value - the String to convert from
 * @return java.lang.String - the reformatted String
 */
public static String formatStringWithSpaces( String value) {

    int startIndex = 0;
    int blankSpaceIndex = -1;
    StringBuffer newString = new StringBuffer();

    if (value == null || value.equals("")) return value;

    // Look for a blank space
    blankSpaceIndex = value.indexOf(" ", startIndex);

    // Loop through the length of the string
    while (startIndex < value.length())
    {
        // If a blank space is found then replace it with the &nbsp; character
        // in the newString
        if (blankSpaceIndex >= 0)
        {
           newString.append(value.substring(startIndex, blankSpaceIndex));
           newString.append("&nbsp;");
           startIndex = ++blankSpaceIndex;
           blankSpaceIndex = value.indexOf(" ", startIndex);
        }
        else
        {
           newString.append(value.substring(startIndex, value.length()));
           startIndex = value.length();
        }
    }

    return newString.toString();
}

} // public class InputField