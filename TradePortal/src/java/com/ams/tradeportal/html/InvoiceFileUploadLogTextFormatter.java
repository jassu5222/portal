package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 * InvoiceFileUploadLogTextFormatter writes out 'View Log' or 'View Errors' to specify
 * at runtime the type of link to display based on the validation status of the upload
 */
public class InvoiceFileUploadLogTextFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadLogTextFormatter.class);

	/**
	 * Format data.
	 * @param obj - validation status
	 * @return '' if pending or in progress
	 * @return 'View Log' if successful
	 * @return 'View Errors' if failed
	 */
    public String format(Object obj) {
        String formattedData = ""; //default

        String value = (String) obj;
        if ((!value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING)) &&
           (!value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS)))
        {
	        //if (value.equals(TradePortalConstants.PAYBACK_STATUS_SUCCESS))//BSL IR NNUM040229447 04/02/2012 - changed constant
        	 // SureshL IR-T36000027751 06/05/2014 Start 
        	if (value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL))
		        formattedData = StringFunction.asciiToUnicode(getResourceManager().getText("PaymentFileUpload.UploadLogSuccess", TradePortalConstants.TEXT_BUNDLE)); //default
		    else
		    {
		    	formattedData = StringFunction.asciiToUnicode(getResourceManager().getText("PaymentFileUpload.UploadLogErrors", TradePortalConstants.TEXT_BUNDLE));
		    	 // SureshL IR-T36000027751 06/05/2014 Start 
				// DK IR T36000016424 Rel8.2 05/12/2013
		    	//formattedData = formattedData.concat("<span class=\"invDeleteSelectedItem\"></span>");
	        	//formattedData = formattedData.concat("&nbsp; &nbsp;<img src=/portal/themes/default/images/delete_small_default.png border=0>");
			}
		}
         LOG.info("text" +formattedData);
        return formattedData;
    }
}
