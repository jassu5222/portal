package com.ams.tradeportal.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.FavoriteTaskObject;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.frame.XmlConfigFileCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

public class MenuFactory {
	private static final Logger LOG = LoggerFactory.getLogger(MenuFactory.class);
	private static final String XPATH_MENU_BAR_VAL = "/navigationMenuBar";
	private static final String XPATH_MENU_VAL = "/menuBarItem";
	private static final String XPATH_ITEM_VAL = "/menuItem";
	private static final String XPATH_CAT_VAL = "/category";
	private static final String XPATH_DISPLAY_KEY_VAL = "/displayKey";
	private static final String XPATH_COND_DISPLAY_VAL = "/conditionalDisplay";
	private static final String XPATH_LINK_ACTION_NAME = "/linkAction";
	private static final String XPATH_LINK_PARAMETER = "/linkParameter";
	private static final String XPATH_PARAMETER_NAME = "/name";
	private static final String XPATH_PARAMETER_VALUE = "/value";
	// jgadela 10/01/2014 R9.1 T36000032989 -- Vara code task - Encrypt the parameters.
	private static final String XPATH_IS_PARAMETER_VALUE_ENCRYPTED = "/encrypted";
	private static final String XPATH_ON_CLICK_SCRIPT = "/onClick";
	private static final String XPATH_DIVIDER_CLASS = "/dividerClass";
	private static final String XPATH_CAT_INLINE = "/inline";
	private static final String XPATH_CAT_PREVELEM = "/prevElement";

	private static final String NL = System.getProperty("line.separator");

	// use table layout for now - want to use display-table and float/one true layout for ie6/7,
	// but quirks mode is causing issues with that in internet explorer
	// NOTE: when ie6, 7 are no longer supported this should be set to FALSE
	// so we use css rather than table html elements
	private static final boolean USE_TABLE_LAYOUT = true;

	private DocumentHandler navigationMenuBarDoc = null;
	private String currentMenu = null;
	private ResourceManager resMgr = null;
	private SessionWebBean userSession = null;
	private FormManager formMgr = null;
	private HttpServletResponse response = null;

	private ConditionalDisplay condDisplay = null;

	// collect all of the menu links and onClicks for use later this is a map of key to link
	private Map linkMap = new HashMap();
	private Map onClickMap = new HashMap();

	public MenuFactory(String xmlFileName, String currentMenu, ResourceManager resMgr, SessionWebBean userSession,
			FormManager formMgr, HttpServletResponse response, ConditionalDisplay condDisplay) {
		// read the xml file from the xml file cache which should help with performance
		// dev note: this requires restart of app to update menu. in prod this would be expected becuase the
		// xml file is part of the ear!
		navigationMenuBarDoc = XmlConfigFileCache.readConfigFile(xmlFileName);

		this.currentMenu = currentMenu;
		this.resMgr = resMgr;
		this.userSession = userSession;
		this.formMgr = formMgr;
		this.response = response;
		this.condDisplay = condDisplay;
	}

	/** disallow default constructor */
	private MenuFactory() {
	}

	/*
	 * Create menu bar item.
	 *
	 * @param: String menuBarItemDisplayKey - menu bar item to display
	 */
	public String createMenuBarItem(String menuBarItemDisplayKey) {

		StringBuffer output = new StringBuffer();

		DocumentHandler mainBar = navigationMenuBarDoc.getComponent(XPATH_MENU_BAR_VAL);
		List<DocumentHandler> menuList = mainBar.getFragmentsList(XPATH_MENU_VAL);

		// find the correct menu bar item
		// todo: this would be quicker if we read the doc in constructor and put menu items in a hashtable
		for (DocumentHandler menu : menuList) {

			String displayKeyM = menu.getAttribute(XPATH_DISPLAY_KEY_VAL);
			if (menuBarItemDisplayKey.equals(displayKeyM)) {
				String conditionalDisplay = menu.getAttribute(XPATH_COND_DISPLAY_VAL);
				if (shouldDisplay(conditionalDisplay)) {
					createMenuBarItem(menu, output);
				}
			}
		}

		return output.toString();
	}

	private void createMenuBarItem(DocumentHandler menu, StringBuffer output) {

		String displayKey = menu.getAttribute(XPATH_DISPLAY_KEY_VAL);

		// displayLable from property file
		String menuDisplayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

		// menu names
		List<DocumentHandler> items = menu.getFragmentsList(XPATH_ITEM_VAL);
		List<DocumentHandler> categories = menu.getFragmentsList(XPATH_CAT_VAL);

		// first deal with popup menu bars
		if ((items != null && items.size() > 0) || (categories != null && categories.size() > 0)) {
			output.append("  <div data-dojo-type=\"dijit.PopupMenuBarItem\" class=\"megaMenuBarItem");
			if (displayKey != null && displayKey.equals(currentMenu)) {
				output.append(" mmMenuBarItemSelected");
			}
			output.append("\" ");
			output.append(">");
			output.append(NL);
			output.append("   <span>");
			output.append(menuDisplayText);
			output.append("<span class=\"menuBarItemIcon\"></span>"); // the dropdown image
			output.append("</span>");
			output.append(NL);

			createMegaMenuDropdown(displayKey, items, categories, output);

			output.append("  </div>");
			output.append(NL);

		} else { // no popup -- menu bar item is a link on its own
			String linkAction = menu.getAttribute(XPATH_LINK_ACTION_NAME);
			List<DocumentHandler> linkParameters = menu.getFragmentsList(XPATH_LINK_PARAMETER);
			String linkHref = getHrefLink(linkAction, linkParameters);

			output.append("  <div data-dojo-type=\"dijit.MenuBarItem\" ");

			// only generate the link if it exists
			if (linkHref != null && linkHref.length() > 0) {
				// generate the link if this is not currently selected
				if (currentMenu == null || !currentMenu.equals(displayKey)) {
					// use onclick rather than anchor
					output.append("data-dojo-props=\"onClick:function(){");
					output.append("window.location='").append(linkHref).append("';}\" ");
				}

				// add link to link map
				linkMap.put(displayKey, linkHref);
			}

			output.append("class=\"megaMenuBarItem");
			if (displayKey != null && displayKey.equals(currentMenu)) {
				output.append(" mmMenuBarItemSelected");
			}
			output.append("\" >");

			output.append("   <span>").append(menuDisplayText).append("</span>");

			output.append("  </div>");
			output.append(NL);
		}
	}

	private void createMegaMenuDropdown(String menuBarItemDisplayKey, List<DocumentHandler> itemList, List<DocumentHandler> categoryList,
			StringBuffer output) {
		// include style display:none so the popup will initially not display
		// before dojo is ready. dojo handles ensuring they render when
		// menu bar items are selected.
		output.append("   <div data-dojo-type=\"dijit.layout.ContentPane\" class=\"mmDropdown\" style=\"display:none;\" >");
		output.append(NL);
		output.append("    <div class=\"mmContent\" >");
		output.append(NL);
		if (USE_TABLE_LAYOUT) {
			output.append("<table>");
		} else {
			output.append("<div class=\"mmTable\">");
		}
		output.append(NL);

		// This if block will execute if menu is having directly menu items.
		if (itemList != null && itemList.size() > 0) {
			if (categoryList != null && categoryList.size() > 0) {
				if (USE_TABLE_LAYOUT) {
					output.append("     <tr class=\"mmHeaderItems\" > <td>");
				} else {
					output.append("     <div class=\"mmHeaderItems mmRow\">");
				}
				output.append(NL);
				output.append("      <ul>");
				output.append(NL);

				// looping through the menu items
				for (DocumentHandler item : itemList) {
					createMenuItem(item, output);
				}

				output.append("      </ul>");
				output.append(NL);
				if (USE_TABLE_LAYOUT) {
					output.append("    </td> </tr>");
				} else {
					output.append("     </div>");
				}
				output.append(NL);

				// because categories follow, add a divider
				if (USE_TABLE_LAYOUT) {
					output.append("     <tr> <td>");
				} else {
					output.append("     <div class=\"mmRow\">");
				}
				output.append(NL);
				output.append("      <div class=\"mmHeaderDivider\" ></div>");
				output.append(NL);
				if (USE_TABLE_LAYOUT) {
					output.append("   </td>  </tr>");
				} else {
					output.append("     </div>");
				}
				output.append(NL);
			} else {

				if (USE_TABLE_LAYOUT) {
					output.append("     <tr class=\"mmMainSection\" > <td>");
				} else {
					output.append("     <div class=\"mmMainSection mmRow\">");
				}
				output.append(NL);
				output.append("      <div class=\"mmMainItems\" >");
				output.append(NL);
				output.append("       <ul>");
				output.append(NL);

				for (DocumentHandler item : itemList) {
					createMenuItem(item, output);
				}

				output.append("       </ul>");
				output.append(NL);
				output.append("      </div>");
				output.append(NL);
				if (USE_TABLE_LAYOUT) {
					output.append("   </td>  </tr>");
				} else {
					output.append("     </div>");
				}
				output.append(NL);
			}
		}

		// This if block will execute if menu is having menu items under different categories.
		if (categoryList != null && categoryList.size() > 0) {
			createCategories(menuBarItemDisplayKey, categoryList, output);
		}

		if (USE_TABLE_LAYOUT) {
			output.append("</table>");
		} else {
			output.append("</div>");
		}
		output.append(NL);

		output.append("    </div>");
		output.append(NL);
		output.append("   </div>");
		output.append(NL);
	}

	// cquinton 11/20/2012 newly refactored method that handles layout
	// depending on what categories actually are displayed
	private void createCategories(String menuBarItemDisplayKey, List<DocumentHandler> categoryList, StringBuffer output) {
		if (USE_TABLE_LAYOUT) {
			output.append("     <tr class=\"mmMainSection\" > ");
		} else {
			output.append("     <div class=\"mmMainSection mmRow\" >");
		}
		output.append(NL);

		// cquinton 11/20/2012 ir#7464 generate layout for each category
		// before wrapping and adding classes that specify dividers...
		List<String> categoryOutputList = new ArrayList<>();

		for (DocumentHandler category : categoryList) {

			// createCategory(menuBarItemDisplayKey, category, cat.hasNext(), catIdx!=0, output);
			String inline = category.getAttribute(XPATH_CAT_INLINE);
			if (TradePortalConstants.INDICATOR_YES.equals(inline)) {
				createInlineCategory(menuBarItemDisplayKey, category, output);
			} else {

				createCategory(menuBarItemDisplayKey, category, categoryOutputList);
			}
		}

		// now that we've built the column innards, wrap them
		Iterator catOut = categoryOutputList.iterator();
		for (int catOutIdx = 0; catOut.hasNext(); catOutIdx++) {
			String categoryOutput = (String) catOut.next();
			boolean catAfter = catOut.hasNext();
			boolean catBefore = catOutIdx != 0;

			// add classes to the column element to
			// create the vertical divider
			String columnClass = "mmMainItems";
			if (catAfter) {
				columnClass += " mmColumnAfter";
			}
			if (catBefore) {
				columnClass += " mmColumnBefore";
			}

			if (USE_TABLE_LAYOUT) {

				output.append("      <td class=\"").append(columnClass).append("\" >");
			} else {
				output.append("      <div class=\"mmCell ").append(columnClass).append("\" >");
			}
			output.append(NL);

			output.append(categoryOutput);

			if (USE_TABLE_LAYOUT) {

				output.append("      </td>");
			} else {
				output.append("      </div>");
			}
			output.append(NL);
		}

		if (USE_TABLE_LAYOUT) {
			output.append("     </tr>");
		} else {
			output.append("     </div>");
		}
		output.append(NL);

	}

	// cquinton 11/20/2012 createCategory now just creates the internal
	// category html and returns a list of those
	// this allows the caller to then determine how many columns
	// there really will be in the menu
	private void createCategory(String menuBarItemDisplayKey, DocumentHandler category, List<String> categoryOutputList) {
		String displayKey = category.getAttribute(XPATH_DISPLAY_KEY_VAL);
		String conditionalDisplay = category.getAttribute(XPATH_COND_DISPLAY_VAL);

		// check displayRights
		if (shouldDisplay(conditionalDisplay)) {

			// cquinton 11/20/2012 - move column class determination to the caller

			StringBuffer output = new StringBuffer();

			if (displayKey != null) {
				// displayLable from property file
				String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

				// category name
				output.append("       <div class=\"mmCategory\">").append(displayText).append("</div>");
				output.append(NL);
			}

			List<DocumentHandler> itemsCat = category.getFragmentsList(XPATH_CAT_VAL);
			for (DocumentHandler cat : itemsCat) {
				String inline = cat.getAttribute(XPATH_CAT_INLINE);
				String prevElement = cat.getAttribute(XPATH_CAT_PREVELEM);
				String displayName = cat.getAttribute(XPATH_DISPLAY_KEY_VAL);
				if (TradePortalConstants.INDICATOR_YES.equals(inline) && displayName.equals(prevElement)) {
					createInlineCategory(menuBarItemDisplayKey, cat, output);
				}
			}

			output.append("       <ul>");
			output.append(NL);

			List<DocumentHandler> itemsC = category.getFragmentsList(XPATH_ITEM_VAL);

			for (DocumentHandler item : itemsC) {
				createMenuItem(item, output);
				List<DocumentHandler> itemsCat1 = category.getFragmentsList(XPATH_CAT_VAL);

				for (DocumentHandler cat : itemsCat1) {

					String inline = cat.getAttribute(XPATH_CAT_INLINE);
					String prevElement = cat.getAttribute(XPATH_CAT_PREVELEM);
					String displayName = item.getAttribute(XPATH_DISPLAY_KEY_VAL);
					if (TradePortalConstants.INDICATOR_YES.equals(inline) && displayName.equals(prevElement)) {
						createInlineCategory(menuBarItemDisplayKey, cat, output);
					}
				}

			}

			output.append("       </ul>");
			output.append(NL);

			categoryOutputList.add(output.toString());

			// cquinton 11/20/2012 - move column class finalization to the caller
		}
	}

	// cquinton 11/20/2012 createCategory now just creates the internal
	// category html and returns a list of those this allows the caller to then determine how many columns
	// there really will be in the menu
	private void createInlineCategory(String menuBarItemDisplayKey, DocumentHandler category, StringBuffer input) {
		String displayKey = category.getAttribute(XPATH_DISPLAY_KEY_VAL);
		String conditionalDisplay = category.getAttribute(XPATH_COND_DISPLAY_VAL);
		// check displayRights
		if (shouldDisplay(conditionalDisplay)) {

			StringBuffer output = new StringBuffer();

			if (displayKey != null) {
				// displayLable from property file
				String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

				// category name
				output.append("       <div class=\"mmCategory\">").append(displayText).append("</div>");
				output.append(NL);
			}
			List<DocumentHandler> itemsCat = category.getFragmentsList(XPATH_CAT_VAL);

			for (DocumentHandler cat : itemsCat) {
				String inline = cat.getAttribute(XPATH_CAT_INLINE);
				String prevElement = cat.getAttribute(XPATH_CAT_PREVELEM);
				String displayName = cat.getAttribute(XPATH_DISPLAY_KEY_VAL);
				if (TradePortalConstants.INDICATOR_YES.equals(inline) && displayName.equals(prevElement)) {
					createInlineCategory(menuBarItemDisplayKey, cat, output);
				}
			}

			output.append("       <ul>");
			output.append(NL);

			List<DocumentHandler> itemsC = category.getFragmentsList(XPATH_ITEM_VAL);
			
				for (DocumentHandler item : itemsC) {
					createMenuItem(item, output);
					List<DocumentHandler> itemsCat1 = category.getFragmentsList(XPATH_CAT_VAL);
					for (DocumentHandler cat : itemsCat1) {
						String inline = cat.getAttribute(XPATH_CAT_INLINE);
						String prevElement = cat.getAttribute(XPATH_CAT_PREVELEM);
						String displayName = item.getAttribute(XPATH_DISPLAY_KEY_VAL);
						if (TradePortalConstants.INDICATOR_YES.equals(inline) && displayName.equals(prevElement)) {
							createInlineCategory(menuBarItemDisplayKey, cat, output);
						}
					}

				}
			
			output.append("       </ul>");
			output.append(NL);
			input.append(output);

		}
	}

	private void createMenuItem(DocumentHandler item, StringBuffer output) {

		String displayKey = item.getAttribute(XPATH_DISPLAY_KEY_VAL);
		String conditionalDisplay = item.getAttribute(XPATH_COND_DISPLAY_VAL);
		String linkAction = item.getAttribute(XPATH_LINK_ACTION_NAME);
		List<DocumentHandler> linkParameters = item.getFragmentsList(XPATH_LINK_PARAMETER);
		String onClick = item.getAttribute(XPATH_ON_CLICK_SCRIPT);
		String dividerClass = item.getAttribute(XPATH_DIVIDER_CLASS);

		// check security and displayRights
		if (shouldDisplay(conditionalDisplay)) {

			// displayText from property file
			String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

			String linkHref = getHrefLink(linkAction, linkParameters);

			output.append("<li");
			output.append(">");

			output.append("<a");
			// set id if an onclick for eventhandling selection
			if (onClick != null && onClick.length() > 0) {
				output.append(" id=\"").append(formatId(displayKey)).append("\"");

				// add onClick to onClick map
				onClickMap.put(displayKey, onClick);
			}
			// only generate the link if it exists
			if (linkHref != null && linkHref.length() > 0) {
				output.append(" href=\"").append(linkHref).append("\" ");

				// add link to link map
				linkMap.put(displayKey, linkHref);
			}
			output.append(">").append(displayText).append("</a>");

			output.append("</li>");
			output.append(NL);

			if (dividerClass != null && dividerClass.length() > 0) {
				output.append("<div class=\"").append(dividerClass).append("\" ></div>");
				output.append(NL);
			}

		}
	}

	/**
	 * Determines whether to display the item or not.
	 *
	 * @param conditionalDisplay
	 * @return
	 */
	private boolean shouldDisplay(String conditionalDisplay) {
		boolean display = true;
		// only go conditional if menu specified a value
		if (conditionalDisplay != null && conditionalDisplay.length() > 0) {
			// and we have a conditionalDisplay instance
			if (condDisplay != null) {
				display = condDisplay.shouldDisplay(conditionalDisplay);
			}
		}
		return display;
	}

	private String getHrefLink(String linkAction, List<DocumentHandler> linkParameters) {
		String linkHref = "";
		StringBuilder paramStr = new StringBuilder();

		if (linkAction != null && linkAction.length() > 0) {
			// add the link parameters
			if (linkParameters != null) {
				for (DocumentHandler parm : linkParameters) {
					String parmName = parm.getAttribute(XPATH_PARAMETER_NAME);
					String parmValue = parm.getAttribute(XPATH_PARAMETER_VALUE);
					String isValueEncrypted = parm.getAttribute(XPATH_IS_PARAMETER_VALUE_ENCRYPTED);
					// whether the parm should be encrypted or not
					if (StringFunction.isNotBlank(parmName)
							&& (StringFunction.isNotBlank(isValueEncrypted) && isValueEncrypted.equalsIgnoreCase("Y"))) {
						parmValue = EncryptDecrypt.encryptStringUsingTripleDes(parmValue, userSession.getSecretKey());
					}
					paramStr.append("&amp;").append(parmName).append("=").append(parmValue);
				}
			}

			linkHref = formMgr.getLinkAsUrl(linkAction, paramStr.toString(), response);
		}

		return linkHref;
	}

	private String formatId(String value) {
		String id = "";
		if (value != null) {
			id = value.replace('.', '_');
		}
		return id;
	}

	// todo: figure out how to use onClickMap to generate the event handlers without going through the menu again
	/*
	 * Create menu bar item.
	 *
	 * @param: String menuBarItemDisplayKey - menu bar item to display
	 */
	public String generateMenuBarItemEventHandlers(String menuBarItemDisplayKey) {

		StringBuffer output = new StringBuffer();

		DocumentHandler mainBar = navigationMenuBarDoc.getComponent(XPATH_MENU_BAR_VAL);
		List<DocumentHandler> menus = mainBar.getFragmentsList(XPATH_MENU_VAL);

		// find the correct menu bar item
		// todo: this would be quicker if we read the doc in constructor and put menu items in a hashtable
		for (DocumentHandler menu : menus) {
			String displayKeyM = menu.getAttribute(XPATH_DISPLAY_KEY_VAL);
			if (menuBarItemDisplayKey.equals(displayKeyM)) {
				String conditionalDisplay = menu.getAttribute(XPATH_COND_DISPLAY_VAL);
				if (shouldDisplay(conditionalDisplay)) {
					generateMenuBarItemEventHandlers(menu, output);
				}
			}
		}

		return output.toString();
	}

	private void generateMenuBarItemEventHandlers(DocumentHandler menu, StringBuffer output) {

		String displayKey = menu.getAttribute(XPATH_DISPLAY_KEY_VAL);

		// menu names
		List<DocumentHandler> items = menu.getFragmentsList(XPATH_ITEM_VAL);
		List<DocumentHandler> categories = menu.getFragmentsList(XPATH_CAT_VAL);

		// first deal with popup menu bars
		if ((items != null && items.size() > 0) || (categories != null && categories.size() > 0)) {
			generateMegaMenuEventHandlers(displayKey, items, categories, output);
		} else { // no popup -- menu bar item is a link on its own
			// todo: event handlers for menu bar item!!!
		}
	}

	private void generateMegaMenuEventHandlers(String menuBarItemDisplayKey, List<DocumentHandler> items,
			List<DocumentHandler> categories, StringBuffer output) {

		// This if block will execute if menu is having directly menu items.
		if (items != null && items.size() > 0) {
			if (categories != null && categories.size() > 0) {

				// looping through the menu items
				for (DocumentHandler item : items) {
					generateMenuItemEventHandler(item, output);
				}

			} else {

				for (DocumentHandler item : items) {
					generateMenuItemEventHandler(item, output);
				}
			}
		}

		// This if block will execute if menu is having menu items under different categories.
		if (categories != null && categories.size() > 0) {

			Iterator cat = categories.iterator();
			for (int catIdx = 0; cat.hasNext(); catIdx++) {
				DocumentHandler category = (DocumentHandler) cat.next();

				generateCategoryEventHandlers(menuBarItemDisplayKey, category, cat.hasNext(), catIdx != 0, output);
			}

		}
	}

	private void generateCategoryEventHandlers(String menuBarItemDisplayKey, DocumentHandler category, boolean catAfter,
			boolean catBefore, // information on category placement
			StringBuffer output) {

		String conditionalDisplay = category.getAttribute(XPATH_COND_DISPLAY_VAL);

		// check displayRights
		if (shouldDisplay(conditionalDisplay)) {

			List<DocumentHandler> itemsC = category.getFragmentsList(XPATH_ITEM_VAL);

			for (DocumentHandler item : itemsC) {
				generateMenuItemEventHandler(item, output);
			}

		}
	}

	private void generateMenuItemEventHandler(DocumentHandler item, StringBuffer output) {

		String displayKey = item.getAttribute(XPATH_DISPLAY_KEY_VAL);
		String conditionalDisplay = item.getAttribute(XPATH_COND_DISPLAY_VAL);
		String onClick = item.getAttribute(XPATH_ON_CLICK_SCRIPT);

		// check security and displayRights
		if (shouldDisplay(conditionalDisplay)) {

			output.append(" query('#").append(formatId(displayKey)).append("')");
			output.append(".on('click', function() {").append(NL);
			if (onClick != null && onClick.length() > 0) {
				output.append("  menu.").append(onClick).append(NL);
			}
			output.append(" });").append(NL);

			// cquinton 11/19/2012
			// add onClick to onClick map (if already there its ok)
			if (onClick != null && onClick.length() > 0) {
				onClickMap.put(displayKey, onClick);
			}
		}
	}

	/**
	 * Create html for MyLinks favorite task items. This manipulates the userSession object which retains this info across the
	 * session so it is only set once.
	 *
	 * @return html list items
	 */
	public String createMyLinksFavoriteTasks() {
		StringBuffer output = new StringBuffer();

		List<FavoriteTaskObject> favoriteTaskList = userSession.getFavoriteTasks();
		if (favoriteTaskList == null) {
			favoriteTaskList = initializeFavoriteTasks();
		}

		if (favoriteTaskList != null) {
			for (FavoriteTaskObject favTask : favoriteTaskList) {
				String favoriteId = favTask.getFavId();

				// cquinton 2/25/2013 check security first
				if (FavoriteUtility.shouldDisplayTask(favoriteId, condDisplay)) {

					// first explicitly check for non-menu items
					if (TradePortalConstants.FAV_TASK__CREATE_MAIL.equals(favoriteId)) {
						String messagesUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__MAIL + "&currentFolder="
								+ TradePortalConstants.INBOX_FOLDER + "&currentMailOption"
								+ EncryptDecrypt.encryptStringUsingTripleDes(
										resMgr.getText("Mail.MeAndUnassigned", TradePortalConstants.TEXT_BUNDLE),
										userSession.getSecretKey());
						String messagesLink = formMgr.getLinkAsUrl("goToMessagesHome", messagesUrlParm, response);
						createFavoriteTaskMenuItem(favoriteId, messagesLink, null, output);
					} else if (TradePortalConstants.FAV_TASK__VIEW_NOTIFS.equals(favoriteId)) {
						String notificationsUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__NOTIFICATIONS;
						String notificationsLink = formMgr.getLinkAsUrl("goToMessagesHome", notificationsUrlParm, response);
						createFavoriteTaskMenuItem(favoriteId, notificationsLink, null, output);
					} else {
						String menuDisplayKey = FavoriteUtility.getTaskMenuKey(favoriteId);
						if (menuDisplayKey != null) {
							String linkHref = (String) linkMap.get(menuDisplayKey);
							String onClick = (String) onClickMap.get(menuDisplayKey);
							if ((linkHref != null && linkHref.length() > 0) || (onClick != null && onClick.length() > 0)) {
								createFavoriteTaskMenuItem(favoriteId, linkHref, onClick, output);
							}
							// if no link, don't display
						}
						// if menu item not found, don't display the item
					}
				}
			}
		}
		return output.toString();
	}

	/**
	 * create a menu item - note format is a bit different if a link or javascript
	 *
	 * @param favId
	 * @param linkHref
	 * @param onClick
	 * @param output
	 */
	private void createFavoriteTaskMenuItem(String favId, String linkHref, String onClick, StringBuffer output) {
		output.append("<li>");
		output.append("<a");

		// set id if an onclick for eventhandling selection
		if (onClick != null && onClick.length() > 0) {
			output.append(" id=\"").append(formatId(favId)).append("_myLink").append("\"");
		}

		if (linkHref != null && linkHref.length() > 0) {
			output.append(" href=\"").append(linkHref).append("\" ");
		}
		String displayKey = FavoriteUtility.getTaskDisplayKey(favId);
		String displayText = resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE);

		output.append(">").append(displayText).append("</a>");

		output.append("</li>");
		output.append(NL);
	}

	private List<FavoriteTaskObject> initializeFavoriteTasks() {

		List<FavoriteTaskObject> fTasks = retrieveFavorites(TradePortalConstants.FAV_TYPE_TASK);
		userSession.setFavoriteTasks(fTasks);

		return fTasks;
	}

	public String generateMyLinksFavoriteTasksEventHandlers() {
		StringBuffer output = new StringBuffer();

		List<FavoriteTaskObject> favoriteTaskList = userSession.getFavoriteTasks();
		if (favoriteTaskList == null) {
			favoriteTaskList = initializeFavoriteTasks();
		}

		if (favoriteTaskList != null) {
			for (FavoriteTaskObject favTask : favoriteTaskList) {
				String favoriteId = favTask.getFavId();

				String menuDisplayKey = FavoriteUtility.getTaskMenuKey(favoriteId);
				if (menuDisplayKey != null) {
					String onClick = (String) onClickMap.get(menuDisplayKey);
					if ((onClick != null && onClick.length() > 0)) {
						generateFavoriteTaskEventHandler(favoriteId, onClick, output);
					}
					// if no link, don't display
				}
			}
		}
		String outputStr = output.toString();
		return outputStr;
	}

	private void generateFavoriteTaskEventHandler(String favId, String onClick, StringBuffer output) {

		output.append(" query('#").append(formatId(favId)).append("_myLink").append("')");
		output.append(".on('click', function() {").append(NL);
		if (onClick != null && onClick.length() > 0) {
			output.append("  menu.").append(onClick).append(NL);
		}
		output.append(" });").append(NL);
	}

	public String createMyLinksRecentInstruments() {
		// first get the existing recent instruments list
		StringBuilder output = new StringBuilder("");

		ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

		Queue<SessionWebBean.RecentInstrumentRef> recentsQ = userSession.getRecentInstrumentsQueue();
		for (SessionWebBean.RecentInstrumentRef recentInstr : recentsQ) {
			String params = "&oid=";
			params += EncryptDecrypt.encryptStringUsingTripleDes(recentInstr.getTransactionOid(), userSession.getSecretKey());
			String linkUrl = formMgr.getLinkAsUrl("updateTransaction", params, response);

			String tranType;
			try {
				tranType = refData.getDescr(TradePortalConstants.TRANSACTION_TYPE, recentInstr.getTransactionType(),
						userSession.getUserLocale());
			} catch (Exception ex) {
				// just display the code if there is a problem
				tranType = recentInstr.getTransactionType();
			}
			String tranStatus;
			try {
				tranStatus = refData.getDescr(TradePortalConstants.TRANSACTION_STATUS, recentInstr.getTransactionStatus(),
						userSession.getUserLocale());
			} catch (Exception ex) {
				// just display the code if there is a problem
				tranStatus = recentInstr.getTransactionStatus();
			}
			if (StringFunction.isNotBlank(recentInstr.getInstrumentId())) {
				// not ideal - we have to insert insert of append becuase we
				// want the latest as the first item in the html
				// todo: use a deque (double ended queue) when java 1.6 is used
				// so it is more efficient
				output.insert(0,
						"<li><a href=\"" + StringFunction.xssCharsToHtml(linkUrl) + "\">"
								+ StringFunction.xssCharsToHtml(recentInstr.getInstrumentId()) + " - "
								+ StringFunction.xssCharsToHtml(tranType) + " - " + "(" + tranStatus + ")" + "</a></li>");
			}
		}
		return output.toString();
	}

	/**
	 * Create html for MyLinks favorite task items. This manipulates the userSession object which retains this info across the
	 * session so it is only set once.
	 *
	 * @return html list items
	 */
	public String createMyLinksFavoriteReports() {
		StringBuffer output = new StringBuffer();

		List<FavoriteTaskObject> favoriteReportList = userSession.getFavoriteReports();
		if (favoriteReportList == null) {
			favoriteReportList = initializeFavoriteReports();
		}

		if (favoriteReportList != null) {
			for (FavoriteTaskObject favReport : favoriteReportList) {
				String docId = favReport.getFavId();
				String addlParams = favReport.getAddlParams();
				StringTokenizer strTok = new StringTokenizer(addlParams, "|");
				String reportName = "";
				if (strTok.hasMoreTokens()) {
					reportName = strTok.nextToken();
				}
				String reportType = "C";
				if (strTok.hasMoreTokens()) {
					reportType = strTok.nextToken();
				}

				String encDocId = StringFunction.xssCharsToHtml(docId);
				String encReportName = StringFunction.xssCharsToHtml(reportName);
				String encReportType = StringFunction.xssCharsToHtml(reportType);
				String params = "&oid=" + encDocId;
				params += "&Name=" + encReportName;
				// Intermediary page report type - not the same as numeric bo report type
				params += "&IntReportType=" + encReportType;
				String linkHref = formMgr.getLinkAsUrl("goToReportIntermediary", params, response);
				if (linkHref != null && linkHref.length() > 0) {
					createFavoriteReportMenuItem(docId, reportName, linkHref, output);
				}
			}
		}
		return output.toString();
	}

	/**
	 * create a menu item - note format is a bit different if a link or javascript
	 *
	 * @param favId
	 * @param linkHref
	 * @param output
	 */
	private void createFavoriteReportMenuItem(String docId, String reportName, String linkHref, StringBuffer output) {
		output.append("<li>");
		output.append("<a");

		if (linkHref != null && linkHref.length() > 0) {
			output.append(" href=\"").append(linkHref).append("\" ");
		}
		output.append(">").append(reportName).append("</a>");

		output.append("</li>");
		output.append(NL);
	}

	private List<FavoriteTaskObject> initializeFavoriteReports() {

		List<FavoriteTaskObject> fReports = retrieveFavorites(TradePortalConstants.FAV_TYPE_REPORT);
		userSession.setFavoriteReports(fReports);

		return fReports;
	}

	private List<FavoriteTaskObject> retrieveFavorites(String favType) {

		List<FavoriteTaskObject> favs = new ArrayList<>();
		String sql = "select DISPLAY_ORDER, FAVORITE_ID, OPT_LOCK, FAVORITE_TYPE, ADDL_PARAMS from FAVORITE_TASK_CUSTOMIZATION "
				+ " where A_USER_OID = ?  and FAVORITE_TYPE = ?  order by DISPLAY_ORDER";
		try {
			DocumentHandler favReportListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, userSession.getUserOid(), favType);
			if (favReportListDoc != null) {
				List<DocumentHandler> favReportList = favReportListDoc.getFragmentsList("/ResultSetRecord");
				for (DocumentHandler doc : favReportList) {

					FavoriteTaskObject favReportOb = new FavoriteTaskObject();
					String favId = doc.getAttribute("/FAVORITE_ID");
					favReportOb.setFavId(favId);

					String displayOrder = doc.getAttribute("/DISPLAY_ORDER");
					favReportOb.setDisplayOrder(Integer.parseInt(displayOrder));

					String addlParms = doc.getAttribute("/ADDL_PARAMS");
					if (StringFunction.isNotBlank(addlParms)) {
						favReportOb.setAddlParams(addlParms);
					} else {
						favReportOb.setAddlParams(favId);
					}
					favs.add(favReportOb);
				}
			}
			
		} catch (Exception ex) {
			LOG.error("Problem initializing favorites: " , ex);
		}

		return favs;
	}
}
