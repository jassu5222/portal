package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;

/**
 *  This class contains utility methods that are used when building the 
 *  HTML for the navigation bar.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NavigationBar
 {
private static final Logger LOG = LoggerFactory.getLogger(NavigationBar.class);
 
   /**
    * Adds the HTML for a navigation bar image to the string buffer passed in.
    *
    *
    */
   public static void buildNavbarImage(StringBuffer buff,
					  String imageTextResourceKey,
                                          String brandingDirectory,
					  String altTextResourceKey,
					  ResourceManager resMgr)
    {
		buildNavbarImageLink(buff, null, imageTextResourceKey, brandingDirectory, altTextResourceKey, resMgr, false, false, "");
    }


   /**
    * Adds the HTML for a navigation bar link (with image rollover) to the string buffer passed in.
    *
    *
    */
   public static void buildNavbarLink(StringBuffer buff,
				 String link,
				 String imageTextResourceKey,
                                 String brandingDirectory,                                 
				 String altTextResourceKey,
				 ResourceManager resMgr)
    {
		buildNavbarImageLink(buff, link, imageTextResourceKey, brandingDirectory, altTextResourceKey, resMgr, true, false, "");
    }

   /**
    * Adds the HTML for a navigation bar link (with image rollover) to the string buffer passed in.
    *
    *
    */
   public static void buildNavbarLink(StringBuffer buff,
				 String link,
				 String imageTextResourceKey,
                                 String brandingDirectory,                                 
				 String altTextResourceKey,
				 ResourceManager resMgr,
                                 boolean launchInNewWindow,
                                 String newWindowName)
    {
		buildNavbarImageLink(buff, link, imageTextResourceKey, brandingDirectory,
                                     altTextResourceKey, resMgr, true, launchInNewWindow, newWindowName);
    }


   /**
    * Adds the HTML for a navigation bar image (possibly with image rollover) to the string buffer passed in.
    *
    *
    */
   private static void buildNavbarImageLink(StringBuffer buff,
						 String link,
						 String imageTextResourceKey,
                                                 String brandingDirectory,                                                 
						 String altTextResourceKey,
						 ResourceManager resMgr,
		 				 boolean linkWithRollover,
                                                 boolean launchInNewWindow,
                                                 String newWindowName)
   {
      // Images on the navigation bar are branded.  When building the path to the image,
      // the Branding Images Root ("/portal/images") is added to the branding directory, which is then 
      // then added to the image path from the text resource bundle
      
      // For example, an image with defined in the text resource bundle as "en/button.gif" 
      // and the user is using a branding directory of "bank", then the full path of the image is:
      //         /portal/images/bank/en/button.gif
      
      
      if(linkWithRollover)
       {
              buff.append("<a href=\"");
	      buff.append(link);
	      buff.append("\" onMouseOut=\"changeImage('");
	      buff.append(imageTextResourceKey);
	      buff.append("', '");
	      buff.append(TradePortalConstants.IMAGES_PATH);
              buff.append(brandingDirectory);
 	      buff.append(resMgr.getText(imageTextResourceKey, TradePortalConstants.TEXT_BUNDLE));             
	      buff.append("')\" onMouseOver=\"changeImage('");
	      buff.append(imageTextResourceKey);
	      buff.append("', '");
	      buff.append(TradePortalConstants.IMAGES_PATH);
              buff.append(brandingDirectory);
 	      buff.append(resMgr.getText(imageTextResourceKey+"RO", TradePortalConstants.TEXT_BUNDLE));  
	      buff.append("')\"");
              if(launchInNewWindow)
               {
                  buff.append(" target='");
                  buff.append(newWindowName);
                  buff.append("' ");
               }    
	      buff.append("><img ");
       }
	  else
       {
	    buff.append("<img ");
       }
       
	  buff.append("name = \"");
          buff.append(imageTextResourceKey);
          buff.append("\"src=\"");
          buff.append(TradePortalConstants.IMAGES_PATH);
          buff.append(brandingDirectory);
          buff.append(resMgr.getText(imageTextResourceKey, TradePortalConstants.TEXT_BUNDLE));        
          buff.append("\" border=\"0\" alt=\"");
          buff.append(resMgr.getText(altTextResourceKey, TradePortalConstants.TEXT_BUNDLE));
          buff.append("\">");
      
	  if(linkWithRollover)
       {
	     buff.append("</a>");
       }
   }
}