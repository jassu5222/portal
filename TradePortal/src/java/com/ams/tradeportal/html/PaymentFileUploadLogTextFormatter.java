package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.StringFunction;

/**
 * PaymentFileUploadLogTextFormatter writes out 'View Log' or 'View Errors' to specify
 * at runtime the type of link to display based on the validation status of the upload
 */
public class PaymentFileUploadLogTextFormatter extends AbstractCustomListViewColumnFormatter {
private static final Logger LOG = LoggerFactory.getLogger(PaymentFileUploadLogTextFormatter.class);

	/**
	 * Format data.
	 * @param obj - validation status
	 * @return '' if pending or in progress
	 * @return 'View Log' if successful
	 * @return 'View Errors' if failed
	 */
    public String format(Object obj) {
        String formattedData = ""; //default

        String value = (String) obj;
        if ((!value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING)) &&
           (!value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_IN_PROGRESS)))
        {
	        if (value.equals(TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL))
	        	 // SureshL IR-T36000027751 06/05/2014 Start 
		        formattedData = StringFunction.asciiToUnicode(getResourceManager().getText("PaymentFileUpload.UploadLogSuccess", TradePortalConstants.TEXT_BUNDLE)); //default
		    else
		    {
	        	formattedData = StringFunction.asciiToUnicode(getResourceManager().getText("PaymentFileUpload.UploadLogErrors", TradePortalConstants.TEXT_BUNDLE));
	        	// SureshL IR-T36000027751 06/05/2014 End
	        	//Image should not be displayed in Portal Refresh. Hence removing - Pavani 05/12/12
	        	/*formattedData = formattedData.concat("&nbsp;<img src=/portal/images/Error.gif border=0>");*/
			}
		}

        return formattedData;
    }
}
