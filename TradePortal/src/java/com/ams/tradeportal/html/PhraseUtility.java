package com.ams.tradeportal.html;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.QueryListView;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

public class PhraseUtility {
private static final Logger LOG = LoggerFactory.getLogger(PhraseUtility.class);
/**
 * PhraseUtility constructor comment.
 */
public PhraseUtility() {
	super();
}

public static DocumentHandler createPhraseList(String category, SessionWebBean userSession, FormManager formMgr, ResourceManager resMgr) {
	return createPhraseList(category,  userSession,  formMgr,resMgr,null);
}
/**
 * Retrieves a list of phrases given a phrase category type and user session.
 * The list of phrases include all phrase for the given category plus MISC.
 * All phrases must belong to the user's org, bank group, client bank, or
 * the global organization (orgs are pulled from the user session.)
 * Returns a blank document if no data meets criteria.
 *
 * @return com.amsinc.ecsg.util.DocumentHandler
 * @param category java.lang.String
 * @param userSession com.ams.tradeportal.busobj.webbean.SessionWebBean
 * @param formMgr com.amsinc.ecsg.web.FormManager (helps create an EJB)
 * @param resMgr com.amsinc.ecsg.util.ResourceManager - manages localized sort order
 */
public static DocumentHandler createPhraseList(String category, SessionWebBean userSession, FormManager formMgr, ResourceManager resMgr,String currency) {
	StringBuilder sql = new StringBuilder();
	DocumentHandler phraseList = new DocumentHandler();

	QueryListView queryListView = null;
	 List<Object> sqlParams = new ArrayList<>();

	try {
	  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView"); 

	  // Note, although we search for all 4 levels of owner orgs, if a user
	  // is at a higher level, the lower owner orgs will be null.  Thus,
	  // the same sql works for a user at any level.
	  sql.append("select phrase_oid, name ");
	  sql.append(" from phrase");
	  sql.append(" where (phrase_category = ");
	  if(!TradePortalConstants.PHRASE_CAT_PMT_INST.equals(category)){
	  sql.append("? or phrase_category=");
	  sqlParams.add(TradePortalConstants.PHRASE_CAT_MISC);
	  }
	  sql.append("?)");
	  sqlParams.add(category);
	  
	  
	  if(StringFunction.isNotBlank(currency)){
		  sql.append(" and name like ? " );
		  sqlParams.add(currency+"%");
	  }
	  //get only Bank Group level payment Instructions
	  if(TradePortalConstants.PHRASE_CAT_PMT_INST.equals(category)){
	  sql.append(" and p_owner_org_oid = ? " ); 
	  sqlParams.add(userSession.getBogOid());
	  }
	  else{
	  sql.append(" and (p_owner_org_oid = ? " );
	  sqlParams.add( userSession.getOwnerOrgOid());
	  

          // Also include phrases from the user's actual organization if using subsidiary access
          if(userSession.showOrgDataUnderSubAccess())
          {
           sql.append(" or p_owner_org_oid = ? "); 
           sqlParams.add(userSession.getSavedUserSession().getOwnerOrgOid());
          }

	  sql.append(" or p_owner_org_oid =?  or p_owner_org_oid =?  or p_owner_org_oid =? )");   
	  sqlParams.add( userSession.getGlobalOrgOid());
	  sqlParams.add( userSession.getBogOid());
	  sqlParams.add( userSession.getClientBankOid());
	  }
	  sql.append(" order by ");
          sql.append(resMgr.localizeOrderBy("upper(name)"));
	  //LOG.info(sql.toString());
	  queryListView.setSQL(sql.toString(), sqlParams);

	  queryListView.getRecords();

	  phraseList = queryListView.getXmlResultSet();

 
	} catch (Exception e) {
		LOG.error("Exception in createPhraseList() ", e);

	} finally {
	  try {
		  if (queryListView != null) {
			  queryListView.remove();
		  }
	  } catch (Exception e) {
		  LOG.error("error removing querylistview in UserDetail.jsp");
	  }
	}  

	return phraseList;

}

/**
 * Insert the method's description here.
 * Creation date: (3/2/01 3:41:50 PM)
 * @return java.lang.String
 * @param textPath java.lang.String
 * @param fieldName java.lang.String
 */
public static String getPhraseChange(String textPath, String fieldName) {
	StringBuffer tag = new StringBuffer("'setPhraseLookupFields(");
	tag.append("this.options[this.selectedIndex].value,\"");
	tag.append(textPath);
	tag.append("\", \"");
	tag.append(fieldName);
	tag.append("\");'");
	return tag.toString();
}

/**
 * Return a string of the form:
 *   'setPhraseLookupFields(this.options[this.selectedIndex].value,
 *                        "textpath", "fieldName", "maxLength");'
 * that can be used in an onChange tag.  This calls a method when
 * a new phrase is selected which sets some hidden fields with the 
 * selected oid and other items to help append the text.
 * 
 * @return java.lang.String
 * @param textPath java.lang.String
 * @param fieldName java.lang.String
 */
public static String getPhraseChange(String textPath, String fieldName, String maxLength) {

	StringBuffer tag = new StringBuffer("'setPhraseLookupFields(");
	tag.append("this.options[this.selectedIndex].value,\"");
	tag.append(textPath);
	tag.append("\", \"");
	tag.append(fieldName);
	tag.append("\", \"");
	tag.append(maxLength);
	tag.append("\");'");
	return tag.toString();
}

public static String getPhraseChange(String textPath, String fieldName, String maxLength, String textAreaOb) {

	StringBuffer tag = new StringBuffer("\"setPhraseLookupFields(");
	tag.append("this,\'");
	tag.append(textPath);
	tag.append("', '");
	tag.append(fieldName);
	tag.append("', '");
	tag.append(maxLength);
	tag.append("',"+textAreaOb+");\"");
	return tag.toString();
}

public static String getPhraseChangeNew(String textPath, String fieldName, String maxLength, String textAreaOb) {

	StringBuffer tag = new StringBuffer("\"setPhraseLookupFieldsAppend(");
	tag.append("this,\'");
	tag.append(textPath);
	tag.append("', '");
	tag.append(fieldName);
	tag.append("', '");
	tag.append(maxLength);
	tag.append("',"+textAreaOb+");\"");
	return tag.toString();
}
/**
 * Return a string of the form:
 *   'setPhraseLookupFields("textpath", "fieldName", "maxLength");'
 * that can be used in an onClick tag.  This calls a method when
 * to set some hidden fields with items to for dealing with a text phrase.
 * This is similar to getPhraseChange method but does not include an oid.
 * This variation was added to support fill-in phrases.
 * 
 * @return java.lang.String
 * @param textPath java.lang.String
 * @param fieldName java.lang.String
 */
public static String setPhraseParameters(String textPath, String fieldName, String maxLength) {

	StringBuffer tag = new StringBuffer("setPhraseParmFields('");
	tag.append(textPath);
	tag.append("', '");
	tag.append(fieldName);
	tag.append("', '");
	tag.append(maxLength);
	tag.append("');");
	return tag.toString();
}

}
