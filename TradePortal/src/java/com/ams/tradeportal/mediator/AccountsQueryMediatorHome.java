package com.ams.tradeportal.mediator;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AccountsQueryMediatorHome extends javax.ejb.EJBHome
{

  public AccountsQueryMediator create()
		throws CreateException, RemoteException;}