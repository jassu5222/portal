package com.ams.tradeportal.mediator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.PurchaseOrder;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
/**
 * This class is used for adding one or more Structured PO to an Import LC
 * Issue or Amend transaction. If POs  are being assigned to a
 * transaction that previously had no POs , the class will ensure
 * that all of the selected POs  have the same PO upload definition
 * oid, seller name, and currency. If all selected POs  are
 * valid, the transaction's amount, last shipment date, expiry date are updated accordingly.
 *
 * 
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.rmi.RemoteException;

public class AddStructuredPOMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(AddStructuredPOMediatorBean.class);
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient boolean hasstrucPOs = false;
	private transient String uploadDefinitionOid = null;
	private transient String beneficiaryName = null;
	private transient String currency = null;
	private transient Vector strucPOOidsList = null;
	private transient String strucPOOidsSql = null;
	private transient Terms terms = null;
	private transient int numberOfstrucPOs = 0;
	private transient long shipmentTermsOid = 0;

	private transient String userLocale = null;
	private transient String userOID  =null ;

	/**
	 * This method performs addition of POs The format expected from the input
	 * doc is:
	 * 
	 * <POLineItemList> <POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 * <POLineItem ID="1"><poLineItemOid>1000002</POLineItem> <POLineItem
	 * ID="2"><poLineItemOid>1000003</POLineItem> <POLineItem
	 * ID="3"><poLineItemOid>1000004</POLineItem> <POLineItem
	 * ID="6"><poLineItemOid>1000001</POLineItem> </POLineItemList>
	 * 
	 * This is a non-transactional mediator that executes transactional methods
	 * in business objects
	 * 
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator
	 *            XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall
	 *            Mediator XML document)
	 * @param newMediatorServices
	 *            Associated class that contains lots of the auxillary
	 *            functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {
		// DocumentHandler strucPODoc = null;
		String substitutionParm = null;
		Vector structuredPOItemsList = null;
		long transactionOid = 0;

		mediatorServices = newMediatorServices;
		structuredPOItemsList = inputDoc
				.getFragments("/POLineItemList/POLineItem");
		transactionOid = inputDoc
				.getAttributeLong("/Transaction/transaction_oid");
		hasstrucPOs = Boolean.valueOf(
				inputDoc.getAttribute("../hasStructuredPO")).booleanValue();
		if(StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/ShipmentTermsList/shipment_oid")))
			shipmentTermsOid = Long.valueOf(inputDoc.getAttribute("/Terms/ShipmentTermsList/shipment_oid"));
		else
			shipmentTermsOid=0;
		numberOfstrucPOs = structuredPOItemsList.size();
		userLocale = mediatorServices.getCSDB().getLocaleName();
		userOID  = inputDoc.getAttribute("/User/userOid");

		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}
		String buttonPressed = "";
		buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");

		if (!(TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS
				.equals(buttonPressed) || "AddStructuredPOButton".equals(buttonPressed))) {
			return outputDoc;
		}
		// If nothing was selected by the user, issue an error indicating this
		// and return
		if (numberOfstrucPOs == 0) {
			substitutionParm = mediatorServices.getResourceManager().getText(
					"StructuredPOAction.Add", TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);

			addStructuredPO(transactionOid, outputDoc);
			return outputDoc;
		}

		// Get a list of all the PO oids that were selected
		String multiplestrucPOs = TradePortalConstants.INDICATOR_NO;
		strucPOOidsList = POLineItemUtility.getPOLineItemOids(
				structuredPOItemsList, multiplestrucPOs,
				TradePortalConstants.PO_STRUCT_ADD);
		// Build the SQL to use in numerous queries throughout the mediator
		 String requiredOid = "purchase_order_oid";//SHR CR708 Rel8.1 
		 strucPOOidsSql = POLineItemUtility.getPOLineItemOidsSql(
				strucPOOidsList, requiredOid);

		// If POs don't currently exist for the transaction, make sure that all
		// of
		// the POs selected by the user have the same PO upload definition,
		// beneficiary, and currency; if they don't, issue an error indicating
		// this. On the
		// other hand, if the transaction *does* currently have POs assigned to
		// it,
		// we don't need to validate anything since it has already been done in
		// the listview
		// (i.e., the listview ensures that these fields match the corresponding
		// fields for
		// the transaction).
			if(strucPOOidsList!=null && strucPOOidsList.size()>0){
				long strucPOOid = Long.valueOf((String) strucPOOidsList.elementAt(0))
					.longValue();
			 PurchaseOrder strucPO =(PurchaseOrder) mediatorServices.createServerEJB(
					"PurchaseOrder", strucPOOid);

			uploadDefinitionOid = strucPO.getAttribute("upload_definition_oid");

			beneficiaryName = strucPO.getAttribute("seller_name");
			currency = strucPO.getAttribute("currency");
			}
		if (!hasstrucPOs) {
			if (validatePOLineItems(multiplestrucPOs, transactionOid) == false) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PURCHASE_ORDER_MISMATCH);

				return outputDoc;
			}
			else{
				hasstrucPOs = true;
			}
			
		}

		try {
			transaction = (Transaction) mediatorServices.createServerEJB(
					"Transaction", transactionOid);

			// Needs to instantiate instrument object for updating amendment
			// transaction
			String transactionType = transaction
					.getAttribute("transaction_type_code");
				long instrumentOid = transaction
						.getAttributeLong("instrument_oid");
				instrument = (Instrument) mediatorServices.createServerEJB(
						"Instrument", instrumentOid);
			
			addStructuredPOs(strucPOOidsList, outputDoc);
		} catch (Exception e) {
			LOG.error("Exception occured in AddStructuredPOMediator: ", e);
		}

		// If the transaction previously did not have POs assigned to it, set
		// the PO upload definition oid, beneficiary, and currency in the xml
		// doc so that
		// the jsp loads up the correct data in the listview (otherwise, the
		// user could
		// potentially add PO that don't have matching PO upload definitions,
		// beneficiaries, and currencies).
		
		if (hasstrucPOs) {
			outputDoc.setAttribute("/Transaction/uploadDefinitionOid",
					uploadDefinitionOid);
			outputDoc.setAttribute("../beneficiaryName", beneficiaryName);
			outputDoc.setAttribute("../currency", currency);
		}
		
		
		//IR# SRUM051055272 Rel 8.0.0 - Begin -
		// set default text to goods description if it is blank
		if (StringFunction.isBlank(inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"))) {
			outputDoc.setAttribute("/ChangePOs/goods_description",TradePortalConstants.getPropertyValue("TextResources", "StructuredPurchaseOrders.defaultGoodsDescription", null));
		}else {
			outputDoc.setAttribute("/ChangePOs/goods_description",inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"));
		}
		//IR# SRUM051055272 Rel 8.0.0 - End -
		
		outputDoc.setAttribute("/ChangePOs/isStructuredPO",TradePortalConstants.INDICATOR_YES);
		return outputDoc;
	}

	/**
	 * This method determines whether or not the PO upload definition oid,
	 * beneficiary name, and currency are the same for all POs selected by the
	 * user.
	 * @param transactionOid 
	 * 
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return boolean - indicates whether or not validation was successful
	 *         (true - the PO upload definition oid, beneficiary name, and
	 *         currency are the same for all selected POs false - the PO upload
	 *         definition oid, seller name, and currency are not the same for
	 *         all selected POs )
	 */
	private boolean validatePOLineItems(String multiplestrucPOs, long transactionOid)
			throws AmsException, RemoteException {
		StringBuffer whereClause = null;
		
			boolean isValid = true;

		List<Object> sqlParams = new ArrayList<Object>();
		whereClause = new StringBuffer();
		whereClause.append("select count(distinct(currency)) currencyCount from purchase_order where ( ");
		whereClause.append(strucPOOidsSql);
		whereClause.append(" or a_transaction_oid = ? ) ");
		sqlParams.add(transactionOid);
		if (StringFunction.isBlank(uploadDefinitionOid)) {
			whereClause.append(" and a_upload_definition_oid is null");
		} else {
			whereClause.append(" and a_upload_definition_oid = ? ");
			sqlParams.add(uploadDefinitionOid);
		}


		DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(whereClause.toString(), false, sqlParams);
		int currCount = 0;
		if(resultDoc!=null){
		 currCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/CURRENCYCOUNT");
		}
		
		sqlParams = new ArrayList<Object>();
		whereClause = new StringBuffer();
		whereClause.append("select count(distinct(seller_name)) sellerCount from purchase_order where ( ");
		whereClause.append(strucPOOidsSql);
		whereClause.append(" or a_transaction_oid = ? ");
		whereClause.append(" ) ");
		sqlParams.add(transactionOid);
		if (StringFunction.isBlank(uploadDefinitionOid)) {
			whereClause.append(" and a_upload_definition_oid is null");
		} else {
			whereClause.append(" and a_upload_definition_oid  = ? ");
			sqlParams.add(uploadDefinitionOid);
		}


		resultDoc = DatabaseQueryBean.getXmlResultSet(whereClause.toString(), false, sqlParams);
		int sellerCount = 0;
		if(resultDoc!=null){
		 sellerCount = resultDoc.getAttributeInt("/ResultSetRecord(0)/SELLERCOUNT");
		}
		isValid = (currCount == 1) && (sellerCount == 1);

		return isValid;
	}

	/**
	 * This method keeps the previously added POs by the user to the transaction
	 * he/she is currently viewing when error comes up if user has not selected
	 * any PO and still hit the add selected items button.
	 * 
	 * @param long transactionOid
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addStructuredPO(long transactionOid, DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		transaction = (Transaction) mediatorServices.createServerEJB(
				"Transaction", transactionOid);

		// Get the terms associated with the current transaction and its
		// shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms
				.getComponentHandle("ShipmentTermsList");

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList
				.getComponentObject(shipmentTermsOid);

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the
		// updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/amount_currency_code",
				terms.getAttribute("amount_currency_code"));

		// Convert date formats so that the Out section of the XML is properly
		// updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (!StringFunction.isBlank(shipmentTerms
					.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms
						.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date",
						iso.format(date));
			}

			if (!StringFunction.isBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date",
						iso.format(date));
			}
		} catch (Exception e) {
			LOG.error("Exception while dealing with attributes latest_shipment_date/expiry_date. ",e);
		}
	}

	/**
	 * This method adds the POs selected by the user to the transaction that
	 * he/she is currently viewing. After all PO have been saved, the amount,
	 * date fields are all re-derived to reflect the new item values.
	 * 
	 * @param java
	 *            .util.Vector strucPOOidsList - the list of PO line item oids
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void addStructuredPOs(Vector strucPOOidsList,
			DocumentHandler outputDoc) throws AmsException, RemoteException {
		PurchaseOrder strucPO = null;
		String transactionType = null;
		String transactionOid = null;
		String instrumentOid = null;
		long strucPOOid = 0;
		Vector shipmentPOsList = new Vector(10);//TODO needed?
		Vector poNumList = new Vector(10);

		transactionType = transaction.getAttribute("transaction_type_code");
		transactionOid = transaction.getAttribute("transaction_oid");
		instrumentOid = transaction.getAttribute("instrument_oid");
		// Update the assigned to transaction indicators for each PO line item
		// being
		// added to the transaction
		for (int i = 0; i < numberOfstrucPOs; i++) {
			strucPOOid = Long.valueOf((String) strucPOOidsList.elementAt(i))
					.longValue();
			strucPO = (PurchaseOrder) mediatorServices.createServerEJB(
					"PurchaseOrder", strucPOOid);
			String transOID = strucPO.getAttribute("transaction_oid");
			String instruOID = strucPO.getAttribute("instrument_oid");
			String shipOID = strucPO.getAttribute("shipment_oid");
			//if in case PO is already associated then issue error.
			if(StringFunction.isNotBlank(shipOID) || StringFunction.isNotBlank(instruOID) || StringFunction.isNotBlank(transOID))
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PO_ALREADY_ASSOCIATED,strucPO.getAttribute("purchase_order_num"));
				continue;
			}
			strucPO.setAttribute("transaction_oid", transactionOid);
			strucPO.setAttribute("instrument_oid", instrumentOid);
			strucPO.setAttribute("shipment_oid",
					String.valueOf(shipmentTermsOid));
			strucPO.setAttribute("user_oid", userOID);
			strucPO.setAttribute("action", TradePortalConstants.PO_ACTION_LINK_TO_INSTR);
			strucPO.setAttribute("status", TradePortalConstants.PO_STATUS_ASSIGNED);
			poNumList.addElement(strucPO.getAttribute("purchase_order_num"));
			strucPO.save();
			shipmentPOsList.addElement(String.valueOf(strucPOOid));
			
			strucPO = null;
		}
		

		// Get the terms associated with the current transaction and its
		// shipment terms
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms
				.getComponentHandle("ShipmentTermsList");
		int numShipments = shipmentTermsList.getObjectCount();

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList
				.getComponentObject(shipmentTermsOid);


	 	      StringBuilder sb = new StringBuilder();
	
			  sb.append(" select purchase_order_num from purchase_order where a_transaction_oid = ? ");
		      
		      DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});
			  
			  Vector rows = resultSet.getFragments("/ResultSetRecord");
				 for (int j=0; j<rows.size(); j++) {
					 DocumentHandler row = (DocumentHandler) rows.elementAt(j);
					 String poNum = row.getAttribute("/PURCHASE_ORDER_NUM");
					 poNumList.addElement(poNum);
				 }
			boolean isStructuredPO = true;
			POLineItemUtility.deriveTranDataFromStrucPO(isStructuredPO,
					transaction, shipmentTerms, numShipments, shipmentPOsList,
					Long.parseLong(uploadDefinitionOid), getSessionContext(),instrument,
					userLocale,outputDoc,poNumList);
		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page of transaction, the
		// updated data will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/amount",
				terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code",
				terms.getAttribute("amount_currency_code"));
		outputDoc.setAttribute("/ChangePOs/name",beneficiaryName);
	
		// Convert date formats so that the Out section of the XML is properly
		// updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (!StringFunction.isBlank(shipmentTerms
					.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms
						.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date",
						iso.format(date));
			}

			if (!StringFunction.isBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date",
						iso.format(date));
			}
		} catch (Exception e) {
			LOG.error("Exception while dealing with attributes latest_shipment_date/expiry_date.", e);
		}

		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.ADD_PURCHASE_ORDER_SUCCESS,
				String.valueOf(numberOfstrucPOs));
		outputDoc.setAttribute("/ChangePOs/fromStrucPO","Y");
		String termsPartyOid = terms.getAttribute("c_FirstTermsParty");
		if(!StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty"))){
		 	TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
		 	String b = firstTermsParty.getAttribute("terms_party_oid");
        	firstTermsParty.setAttribute("name", beneficiaryName);
		}
        	transaction.save(false);
	}

}
