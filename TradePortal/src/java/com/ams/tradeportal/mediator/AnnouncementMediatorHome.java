package com.ams.tradeportal.mediator;

import com.amsinc.ecsg.frame.*;
import javax.ejb.*;
import java.rmi.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AnnouncementMediatorHome extends javax.ejb.EJBHome
{
   public AnnouncementMediator create() throws CreateException, RemoteException;
}
