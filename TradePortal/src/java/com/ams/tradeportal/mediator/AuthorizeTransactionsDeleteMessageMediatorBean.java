package com.ams.tradeportal.mediator;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.MailMessage;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AuthorizeTransactionsDeleteMessageMediatorBean extends AuthorizeTransactionsMediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(AuthorizeTransactionsDeleteMessageMediatorBean.class);

	@Override
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		/**
		 * This method performs  authorization of Transaction. The format expected from the input doc is:
		 *     
		 * <TransactionList>
		 *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
		 *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
		 *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
		 *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
		 *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
		 * </TransactionList>
		 * <User>
		 *    <userOid>1001</userOid>
		 *    <securityRights>302231454903657293676543</securityRights>
		 * </User>
		 * <Authorization>
		 *    <preAuthorize>Y|N</preAuthorize>
		 * </Authorization>
		 *
		 * The oid value consists of transaction oid, a '/', instrument oid, a '/'
		 * and the transaction type
		 *
		 * This is a non-transactional mediator that executes transactional
		 * methods in business objects.  It accepts preAuthorize as a parameter which
		 * determines if the transaction should be preAuthorized (ie. authorized at the
		 * background) or if it should be authorized immediately.  
		 * For preauthorization, it processes each transaction by
		 * locking the instrument (if necessary) and setting the transaction 
		 * status to Authorised Pending.
		 * If there is more than one transaction selected for an instrument, 
		 * this mediator locks the instrument for the first transaction and gives
		 * a lock error for any subsequent transactions.
		 * For authorization, it locks the instrument, performs the authorize process
		 * and sets the transaction status to Failed, Authorized or Partially Authorized.
		 * This method accepts multiple transactions in the format specified above.
		 * Note that this mediator has been tested for both preAuthorize and
		 * Authorize. However, for preAuthorize, the effect of instrument locking 
		 * on a general level has to be examined further.
		 *
		 * @param inputDoc The input XML document (&lt;In&gt; section of overall Mediator XML document)
		 * @param outputDoc The output XML document (&lt;Out&gt; section of overall Mediator XML document)
		 * @param mediatorServices Associated class that contains lots of the auxillary functionality for a mediator.
		 */
		// Call the AuthorizeTransactionsMediator's execute method to authorize the transaction
		super.execute(inputDoc, outputDoc, mediatorServices);

		String userOid = inputDoc.getAttribute("/User/userOid");
		String rights = inputDoc.getAttribute("/User/securityRights");
		// IR-PAUI060561797 Krishna
		// Associated Mail Notice deletion should happen only for the authorized transactions.
		int noOfauthorizedTransactions = authorizedTransactionsOidList.size();
		LOG.debug("AuthorizeTransactionsDeleteMessageMediator: number of transactions: {}", noOfauthorizedTransactions);
		for (String transactionOid : authorizedTransactionsOidList) {

			// Fetch the associated Mail Notice for this transaction and
			// and delete the same
			// Fetching the associated Mail Notice for this Transaction by using response_transaction_oid of Mail message
			// which is same as the current transactionOid
			MailMessage message = (MailMessage) mediatorServices.createServerEJB("MailMessage");
			message.find("response_transaction_oid", transactionOid);

			// In addition, there are some other fields that need to be set
			// All validations of fields will have already taken place in MailMessageMediator
			// Retrieve User Oid based on Listview or Detailed view
			if (inputDoc.getAttribute("/User/user_oid") != null) {
				// Mail Message Doc
				message.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/User/user_oid"));
			} else {
				// Delete or Route Messages Doc
				message.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/User/userOid"));
			}
			message.setAttribute("last_update_date", DateTimeUtility.getGMTDateTime());
			message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);

			// If message_source_type isn't set yet (like for new messages), set it to PORTAL
			if (StringFunction.isBlank(message.getAttribute("message_source_type")))
				message.setAttribute("message_source_type", TradePortalConstants.PORTAL);

			// Also default the discrepancy flag
			if (StringFunction.isBlank(message.getAttribute("discrepancy_flag")))
				message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);

			// rkrishna CR 375-D ATP 08/22/2007 Begin
			// Also default the Atp Notice flag
			if (StringFunction.isBlank(message.getAttribute("atp_notice_flag")))
				message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
			// rkrishna CR 375-D ATP 08/22/2007 End
			String messageTitle = message.getAttribute("message_subject");
			boolean success = false;
			try {
				success = message.deleteMessage(rights, userOid);
			} catch (RemoteException | AmsException ex) {
				LOG.error("Exception occured deleting Message in AuthorizeTransactionsDeleteMessageMediator: " , ex);
			}
			if (success) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ASCT_MESSAGE_PROCESSED, messageTitle,
						mediatorServices.getResourceManager().getText("MessageAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
			}

		}

		return outputDoc;

	}
}