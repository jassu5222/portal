package com.ams.tradeportal.mediator;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface AutoCreateInvoicesMediatorHome extends EJBHome
{

  public AutoCreateInvoicesMediator create()
		throws CreateException, RemoteException;}