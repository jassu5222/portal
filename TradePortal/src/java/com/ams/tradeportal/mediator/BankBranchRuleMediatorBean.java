package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;

/*
 * Manages the Change Password and Unlock User Transaction.
 *
 *     Copyright  2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class BankBranchRuleMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(BankBranchRuleMediatorBean.class);
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {


		String paymentMethod = "";
		String AccountOid = "";
		String accCountry = "";
		String bankType = "";
		String tableType = TradePortalConstants.COUNTRY_ISO3116_3;

		paymentMethod = inputDoc.getAttribute("/DomesticPayment/payment_method_type");
		if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethod))
			AccountOid  = inputDoc.getAttribute("/Terms/credit_account_oid");
		else
			AccountOid  = inputDoc.getAttribute("/Terms/debit_account_oid");

		bankType = inputDoc.getAttribute("/BankSearchInfo/Type");

		if (InstrumentServices.isBlank(paymentMethod) && !bankType.equals(TermsPartyType.ORDERING_PARTY_BANK)) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_PAYMENT_METHOD);
		}
		if (InstrumentServices.isBlank(AccountOid )) {
			if (paymentMethod.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.SELECT_DEBIT_ACCOUNT, mediatorServices.getResourceManager().getText("common.CreditAccount", TradePortalConstants.TEXT_BUNDLE));
			else
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.SELECT_DEBIT_ACCOUNT, mediatorServices.getResourceManager().getText("common.DebitAccount", TradePortalConstants.TEXT_BUNDLE));
		}

		if (InstrumentServices.isNotBlank(AccountOid )){
			Account account = (Account) mediatorServices.createServerEJB("Account");
			account.getData(Long.parseLong(AccountOid ));
			accCountry=account.getAttribute("bank_country_code");
			//jgadela R91 IR T36000026319 - SQL INJECTION FIX
			String selectCountrySQL = "SELECT ADDL_VALUE FROM REFDATA WHERE CODE = ? AND TABLE_TYPE = ?";
			DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(selectCountrySQL, false,accCountry, tableType);
			if (countryDoc != null)
				accCountry = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");

		}

        //IAZ/VShah IR SHUK011146898 and 01/20/10 - Begin
		StringBuilder whereClause = new StringBuilder(); 
		
		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
		List<Object> sqlParams = new ArrayList();
		if (bankType.equals(TermsPartyType.ORDERING_PARTY_BANK))
		{
			if (paymentMethod.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))
			{
				//whereClause.append(" ADDRESS_COUNTRY = '").append(accCountry).append("'");
				whereClause.append(" PAYMENT_METHOD = ? ");
				sqlParams.add(paymentMethod);
			}
			else
			{
				//whereClause.append(" ADDRESS_COUNTRY = '").append(accCountry).append("'");
				whereClause.append(" PAYMENT_METHOD NOT IN (?)");
				sqlParams.add(InstrumentType.DIRECT_DEBIT_INSTRUCTION);
			}
		}
		else
		{
			whereClause.append(" PAYMENT_METHOD = ? ");
			sqlParams.add(paymentMethod);
		}

        if (!TradePortalConstants.PAYMENT_METHOD_CBFT.equals(paymentMethod)){
			whereClause.append(" AND ADDRESS_COUNTRY = ? ");
			sqlParams.add(accCountry);
        }
        //IAZ/VShah IR SHUK011146898 and 01/20/10 - End
      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
		int count = DatabaseQueryBean.getCount("bank_branch_rule_oid", "bank_branch_rule", whereClause.toString(), false, sqlParams);

		if (count < 1) {
			if (bankType.equals(TermsPartyType.ORDERING_PARTY_BANK))
				mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.NO_MATCH_FOR_SELECTED_DEBITACCOUNT,
								paymentMethod.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION) ?
										mediatorServices.getResourceManager().getText("common.CreditAccount",TradePortalConstants.TEXT_BUNDLE)
										: mediatorServices.getResourceManager().getText("common.DebitAccount",TradePortalConstants.TEXT_BUNDLE));
			else
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_MATCH_FOR_PAYMENTMETHOD_DEBITACCOUNT,
								paymentMethod.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION) ?
										mediatorServices.getResourceManager().getText("common.CreditAccount",TradePortalConstants.TEXT_BUNDLE)
										: mediatorServices.getResourceManager().getText("common.DebitAccount",TradePortalConstants.TEXT_BUNDLE));

		}

		return outputDoc;
	}

}