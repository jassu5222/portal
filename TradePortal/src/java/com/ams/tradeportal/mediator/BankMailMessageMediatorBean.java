package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

import javax.crypto.spec.SecretKeySpec;

import java.rmi.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.mediator.util.BankUpdateCentreProcessor;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001
 *     CGI, Incorporated
 *     All rights reserved
 */
public class BankMailMessageMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(BankMailMessageMediatorBean.class);

  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException {

        mediatorServices.debug("------------------- Bank Mail Message Mediator called ------------------");     
        mediatorServices.debug("Mediator : InputDoc == " + inputDoc.toString() );

	// Declare variables
	BankMailMessage message                 = null;
	String isReply                          = inputDoc.getAttribute("/BankMailMessage/is_reply");
	String mode                             = inputDoc.getAttribute("/Update/MessageMode");
    String oid                                = null;
	long messageOid                         = 0;
	String jPylonDate                       = DateTimeUtility.getGMTDateTime();		//Find out what today's date is...
	String assignCorpOrgOid = null;
	
	if(StringFunction.isNotBlank(inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid"))){
		assignCorpOrgOid   					=  inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid");
	}

	try {
        String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	    mediatorServices.debug("The button pressed is " + buttonPressed);
		SecurityAccess.validateUserRightsForTrans( TradePortalConstants.EMAIL_TRIGGER_MAIL_MSG, "", inputDoc.getAttribute("/User/securityRights"), buttonPressed );

	    /* First we need to create the Mail Message object that will be updated.
	        If we're NOT in create mode meaning we need to create a new message,
	        then we need to create the object based on the passed in oid.  Assuming
	        that the oid was successfully passed in.  Otherwise we create a new
	        object from scratch.
	    */
		message = (BankMailMessage) mediatorServices.createServerEJB("BankMailMessage");
		if( !TradePortalConstants.MESSAGE_CREATE.equals(mode) ){
		    if(StringFunction.isNotBlank(inputDoc.getAttribute("/BankMailMessage/bank_mail_message_oid"))){
		    	messageOid = Long.parseLong(inputDoc.getAttribute("/BankMailMessage/bank_mail_message_oid"));
		    }
		    if (messageOid == 0){
			    mediatorServices.debug("Error: message_oid not present");
			    return null;


		    }
		    mediatorServices.debug("Mediator: Retrieving message " + messageOid);
		    message.getData(messageOid);
		    
        }else{
            message.newObject();
            messageOid = message.getAttributeLong("bank_mail_message_oid");
        }
		// Validate message
		if(validateBankMailMessage(inputDoc, mediatorServices)){
			return outputDoc;
		}
		
		 if(!buttonPressed.equals(TradePortalConstants.BUTTON_ATTACH_DOCUMENTS) ||TradePortalConstants.MESSAGE_CREATE.equals(mode)) {
		message.populateFromXmlDoc(inputDoc);   //Set objects attributes to what was initially passed in.
		 }
    	oid = Long.toString(messageOid);
    	message.setAttribute("bank_mail_message_oid", oid);   
    	
        if(StringFunction.isBlank(message.getAttribute("message_source_type"))){
             message.setAttribute("message_source_type", TradePortalConstants.BANK);
        }
        message.setAttribute("last_update_date", jPylonDate );
      //check for valid instrument id.
		 message.setRelatedInstrument(inputDoc.getAttribute("/Instrument/complete_instrument_id"), inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid"));
		
		 //check for special char's 
		 String message_subject = null;
		 message_subject = inputDoc.getAttribute("/BankMailMessage/message_subject");
		 if(null != message_subject){
			 message.setAttribute("message_subject", StringFunction.xssHtmlToChars(message_subject));
		 }
       
		 if( TradePortalConstants.BUTTON_SAVEASDRAFT.equals(buttonPressed) ||
        		 buttonPressed.equals(TradePortalConstants.BUTTON_ATTACH_DOCUMENTS) ) {
            
        	 message.setAttribute("message_status", TradePortalConstants.DRAFT);
        	 message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
        	 message.setAttribute("assigned_to_user_oid", inputDoc.getAttribute("/User/user_oid"));
        	 message.setAttribute("bank_instrument_id", inputDoc.getAttribute("/MailMessage/bank_instrument_id"));
             message.setAttribute("complete_instrument_id", inputDoc.getAttribute("/Instrument/complete_instrument_id"));
            
        	 if( StringFunction.isBlank( message.getAttribute("assigned_to_corp_org_oid") ) ){
                message.setAttribute("assigned_to_corp_org_oid", inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid"));
        	 }
         }
        //Send To Bank
        else if( TradePortalConstants.BUTTON_SENDTOCUST.equals(buttonPressed) ) {
            if( message.isDeleted() ){
                mediatorServices.getErrorManager ().issueError (
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
            }
            if( StringFunction.isBlank( message.getAttribute("assigned_to_user_oid") ) ){
                message.setAttribute("assigned_to_corp_org_oid", inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid"));
            }
            if( StringFunction.isBlank( message.getAttribute("assigned_to_corp_org_oid") ) ){
                message.setAttribute("assigned_to_user_oid", inputDoc.getAttribute("/User/user_oid"));
            }
            assignCorpOrgOid = message.getAttribute("assigned_to_corp_org_oid");
            message.setAttribute("bank_instrument_id", inputDoc.getAttribute("/MailMessage/bank_instrument_id"));
            message.setAttribute("complete_instrument_id", inputDoc.getAttribute("/Instrument/complete_instrument_id"));
            message.setAttribute("message_status", TradePortalConstants.SENT_TO_CUST);
            message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);            
        }
        //Delete Attached Documents
        else if(buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS)) {
        	outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
        }

        //Try to save the message data
	    mediatorServices.debug("Performing Message Save");

	    try {
	        //reset the output doc to null
            outputDoc = new DocumentHandler();
	        outputDoc.setAttribute("bank_mail_message_oid", message.getAttribute("bank_mail_message_oid"));
	        
	        
	        
	        
	        
		    int success = message.save(true);
		    if ( success > 0 && TradePortalConstants.BUTTON_SENDTOCUST.equals(buttonPressed) ) {
		    	// create mail message to customer.
		    	BankUpdateCentreProcessor.createMailMessageToCust( oid, assignCorpOrgOid, inputDoc, mediatorServices );
		    }
		    
		    mediatorServices.debug("Mediator: Completed the save of Message");
	    } catch (Exception e) {
		    e.printStackTrace();
		    throw new AmsException("General exception caught in BankMessageMediator while performing : " + buttonPressed);
	    } finally {
            //If the business object saved data, we need to send the user a confirmation message.
            //This is accomplished by issueing an error with a severity level of 1.
		    mediatorServices.debug("Mediator: Result of update is "+ this.isTransSuccess(mediatorServices.getErrorManager()));
		    if (this.isTransSuccess(mediatorServices.getErrorManager())){
		        String messageSubject = inputDoc.getAttribute("/BankMailMessage/message_subject");

	            if ( buttonPressed.equals(TradePortalConstants.BUTTON_SENDTOCUST) ) {
				    mediatorServices.getErrorManager().issueError(
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.SENT_TO_CUST_SUCCESSFUL,
					                    messageSubject);
	            }else if( buttonPressed.equals(TradePortalConstants.BUTTON_SAVEASDRAFT) ||
	                      buttonPressed.equals(TradePortalConstants.BUTTON_ROUTEANDSAVE)) {
				    mediatorServices.getErrorManager().issueError(
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.SAVE_SUCCESSFUL,
					                    messageSubject);
	            }
	        }
	    }
	} catch (InvalidObjectIdentifierException ex) {
	  // If the object does not exist, issue an error
	  mediatorServices.getErrorManager ().issueError (
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
	}
    return outputDoc;
  }

	private boolean validateBankMailMessage(DocumentHandler inputDoc,
		MediatorServices mediatorServices) throws AmsException, RemoteException {
		
		String messageText = inputDoc.getAttribute("/BankMailMessage/message_text");
		String bankMailMessageSubject = inputDoc.getAttribute("/BankMailMessage/message_subject");
		String bankMailSendToCustomer = inputDoc.getAttribute("/BankMailMessage/assigned_to_corp_org_oid");
		boolean flag = false;
		/*Validate length of messageText.
		 * Text length should not exceed 4000 bytes*/
		if(StringFunction.isNotBlank(messageText)){
			if(messageText.getBytes().length > 4000){
				mediatorServices.getErrorManager().issueError(
						getClass().getName(),
						TradePortalConstants.STRING_LENGTH_EXCEED,"Message");
				flag = true;
			}
		}//Send to Customer is mandatory
		if(StringFunction.isBlank(bankMailSendToCustomer)){
			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText(
							"BankMail.SendToCustomer",
							TradePortalConstants.TEXT_BUNDLE));
			flag = true;
		}
		if(StringFunction.isBlank(bankMailMessageSubject)){
			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText(
							"BankMailSend.Subject",
							TradePortalConstants.TEXT_BUNDLE));
			flag = true;
		}
		return flag;
	}

	/**
	 * Deletes attached documents marked by the user on a mail message (CR-186 - jkok)
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
        javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");

		DocumentHandler outputDoc = new DocumentHandler();

		List<String> attachedDocuments = inputDoc.getAttributes("/AttachedDocument");

		for(String attDoc : attachedDocuments) {
			if(StringFunction.isNotBlank(attDoc))
			{
				long docId = Long.parseLong(EncryptDecrypt.decryptStringUsingTripleDes(attDoc, secretKey));

				// Save the transaction oid in the mail message oid column.  Also
				// set transaction_oid to 0 to further flag that it was a deleted image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId);
				documentImageInstance.setAttribute("transaction_oid", documentImageInstance.getAttribute("mail_message_oid"));
				documentImageInstance.setAttribute("mail_message_oid", "0");
				documentImageInstance.save();
			}
		}
	
		return outputDoc;
	}
}