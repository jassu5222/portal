package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;

import javax.ejb.*;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.common.*;

/*
 * Manages the Change Password and Unlock User Transaction.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ChangePasswordMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ChangePasswordMediatorBean.class);
	
	   private static final String ATTR_SHOWTIPS = "showtips";
	   private static final String ATTR_DATEPATTERN = "datepattern";
   /*
	* Called by the processRequest() method of the Ancestor class to process the designated
	* transaction.  The input parameters for the transaction are received in an XML document
	* (inputDoc).  The execute() method is responsible for calling one or more business objects
	* to process the transaction and populating the outputDoc XML document with the results
	* of the transaction. 
	*/
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
								  throws RemoteException, AmsException
   {
        // Get necessary data from the input XML
	    String checkCurrentPasswordFlag = inputDoc.getAttribute("/checkCurrentPasswordFlag");
	    String currentPassword          = inputDoc.getAttribute("/currentPassword");
	    String newPassword              = inputDoc.getAttribute("/newPassword");
	    String retypePassword           = inputDoc.getAttribute("/retypePassword");
	    String resettingUserFlag        = inputDoc.getAttribute("/resettingUserFlag");
	    String changingPasswordFlag     = inputDoc.getAttribute("/changingPasswordFlag");
	    String forceFlag                = inputDoc.getAttribute("/force");
	    String fromUserPref                = inputDoc.getAttribute("/fromUserPref");
	    long   userOID                  = inputDoc.getAttributeLong("/userOID");
	    
	    // [START] CR-482
	    String encryptedCurrentPassword          = inputDoc.getAttribute("/encryptedCurrentPassword");
	    // [START] CR-543
	    String protectedCurrentPassword			 = inputDoc.getAttribute("/protectedCurrentPassword");
	 // T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
	    String encryptedRetypedNewPassword 		 = inputDoc.getAttribute("/encryptedRetypedNewPassword");
 	    String protectedNewPassword				 = inputDoc.getAttribute("/protectedNewPassword");
	    // [END] CR-543
	    String passwordValidationError			 = inputDoc.getAttribute("/passwordValidationError");
	    //removed DatePattern
	    
	    //SHR
	    String datepattern 			       = inputDoc.getAttribute("/datepattern");
	    String showToolTipInd 			   = inputDoc.getAttribute("/showtips");
	    //MEer Rel8.4 CR-934A
	    String mobileBankingInd			       = inputDoc.getAttribute("/mobile_banking_access_ind");
	   
	    
	    
	    User user = (User) mediatorServices.createServerEJB("User", userOID);
	   
	   
	   String prevDatePattern = user.getAttribute(ATTR_DATEPATTERN);
	   String prevShowTipsInd = user.getAttribute(ATTR_SHOWTIPS);
	   String prevMobileBankInd = user.getAttribute("mobile_banking_access_ind");
	 //  boolean userUIPrefModified = false;
	   if(InstrumentServices.isNotBlank(datepattern) && different(prevDatePattern, datepattern)) {
		user.setAttribute(ATTR_DATEPATTERN, datepattern);
	//	userUIPrefModified=true;
	   }
		
	   if(InstrumentServices.isNotBlank(showToolTipInd) && different(prevShowTipsInd, showToolTipInd)) {
		user.setAttribute(ATTR_SHOWTIPS, showToolTipInd);
	//	userUIPrefModified=true;
	   }
	   
	   if(StringFunction.isNotBlank(mobileBankingInd) && different(prevMobileBankInd, mobileBankingInd)) {
		   user.setAttribute("mobile_banking_access_ind", mobileBankingInd);
	   }
	   
	  /* if(userUIPrefModified == true) {
		user.save();
		if(user.getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY)
		{
			mediatorServices.getErrorManager().issueError(
		             TradePortalConstants.ERR_CAT_1, TradePortalConstants.UPDATE_SUCCESSFUL,
		             mediatorServices.getResourceManager().getText("UserDetail.UserPreferences", TradePortalConstants.TEXT_BUNDLE));
		}
	   }*/
	    
	    //SHR
	    
	    
	    
	    LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] encryptedCurrentPassword = "
	    		+ encryptedCurrentPassword);
	    // [START] CR-543
	    LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] protectedCurrentPassword = "
	    		+ protectedCurrentPassword);
	    LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] protectedNewPassword = "
	    		+ protectedNewPassword);
	    // [END] CR-543
	    LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] passwordValidationError = "
	    		+ passwordValidationError);
	    boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	    LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] instanceHSMEnabled = "
	    		+ instanceHSMEnabled);
	    // [END] CR-482
	    
	    String numberOfWrongPasswordsStr = inputDoc.getAttribute("/numberOfWrongPasswords");
	    int numberOfWrongPasswords = 0;
	    
	    if(numberOfWrongPasswordsStr != null)
	       numberOfWrongPasswords = Integer.parseInt(numberOfWrongPasswordsStr);
	    
	    boolean changingPassword = false;	    
	    boolean checkCurrentPassword = false;
	    boolean resettingUser = false;
	    boolean forceChangePassword = false;
//Naveen 22-Oct-12. IR-T36000006331. Commented the new conditions added by Suresh P, as it was always resulting in Password reset condition and reverted to the old conditions.
		//Suresh 	    
/*		if ("".equals(currentPassword) && "".equals(newPassword) && "".equals(retypePassword)) {
			checkCurrentPassword = false;
			changingPassword = false;
		}else if((currentPassword == null) && (newPassword != null) && (retypePassword != null)){ // Komal ANZ Issue 43
			checkCurrentPassword = false;
			changingPassword = true;
		}else{
			checkCurrentPassword = true;
			changingPassword = true;
		}*/
        // Determines if the user has requested that the password be changed	    
		if( (changingPasswordFlag != null) &&
			(changingPasswordFlag.equals("true") ) )
			   changingPassword = true;    
		        
	    // In some cases, validate against the current password
		if( (checkCurrentPasswordFlag != null) &&
			(checkCurrentPasswordFlag.equals("true") ) )
			   checkCurrentPassword = true;	    
		// Determines if the user is being unlocked and their invalid logon attempt reset	       
		if( (resettingUserFlag != null) &&
			(resettingUserFlag.equals("true") ) )
				resettingUser = true;
				
		// Determines if the user will be forced to change their password next time they log in		
	    if( (forceFlag != null) &&
	     	(forceFlag.equals("true") ) )
	            forceChangePassword = true;				
	    
	    // Get data for the user
    //    User user = (User) mediatorServices.createServerEJB("User", userOID);

        // [START] CR-482
        boolean isLegacyUser = Password.isUserPasswordLegacy(user);
		if (instanceHSMEnabled) {
			if(isLegacyUser) {
				// Legacy password - use the encrypted password
				try {
					currentPassword = HSMEncryptDecrypt.decryptForTransport(encryptedCurrentPassword);
					LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] "
							+ "User has a legacy password");
				} catch (Exception e) {
					System.err.println("Exception occurred in ChangePasswordMediator while trying to decrypt a legacy password : " + e.toString());
				}
			} else {
				// Hashed password - use the encrypted SHA-256 hashed password
				try {
					// [START] CR-543
					currentPassword = HSMEncryptDecrypt.decryptForTransport(protectedCurrentPassword);
					LOG.debug("[ChangePasswordMediatorBean.execute(DocumentHandler,DocumentHandler,MediatorServices)] " 
							+ "User has a protected password");
				} catch (Exception e) {
					System.err.println("Exception occurred in ChangePasswordMediator while trying to decrypt a protected password: " + e.toString());
					// [END] CR-543
				}
			}
		} //  if (instanceHSMEnabled)
        // [END] CR-482

		// [START] CR-543
		String[] knownChallengeResponses = null;
		String eMacKeyString = null;
		if(instanceHSMEnabled && !isLegacyUser)
		{
			knownChallengeResponses = new String[1];
			knownChallengeResponses[0] = user.getAttribute("password");
			eMacKeyString = currentPassword;
		}
		// [END] CR-543
		
		// [START] CR-482 Updated IF statement to include legacy user logic 
		//
        // If the user must enter their current password,
        // verify that they actually entered the correct password
		// [START] CR-543
		boolean validate = true;
		if("true".equals(fromUserPref) && (InstrumentServices.isBlank(currentPassword) && InstrumentServices.isBlank(newPassword) && InstrumentServices.isBlank(retypePassword)))
			validate = false;
		
 	    if( changingPassword && validate &&
 	        checkCurrentPassword &&
	        ((isLegacyUser 
	        		&& !Password.encryptPassword(currentPassword, user.getAttribute("encryption_salt")).equals(user.getAttribute("password")))
	         || (!isLegacyUser && instanceHSMEnabled
	        		 && (user.getAttribute("password").equals("") || !Password.validateProtectedPassword(knownChallengeResponses, eMacKeyString))))) 
 	    {                   
 	    	// [END] CR-543
 	    	// [END] CR-482
                   // Issue an error  
                   mediatorServices.getErrorManager().
                                issueError(TradePortalConstants.ERR_CAT_1,
                                           TradePortalConstants.CURRENT_PASSWORD_INVALID);     
                                           
                   // Increment the count of wrong passwords entered                     
                   outputDoc.setAttribute("/numberOfWrongPasswords", String.valueOf(++numberOfWrongPasswords) );                                          
                                           
		           // Increment the invalid login attempt count for the user
    	           int currentInvalidLoginAttemptCount = user.getAttributeInteger("invalid_login_attempt_count");
    	           user.setAttribute("invalid_login_attempt_count",String.valueOf(++currentInvalidLoginAttemptCount));
                
                   // Determine if the user should be locked out
                
                   // Get maximum failed attempts from a hashtable
                   int maxFailedAttemptsAllowed = Integer.parseInt((String)user.getSecurityLimits().get("maxFailedLogons"));           

                   if(user.getAttributeInteger("invalid_login_attempt_count") > maxFailedAttemptsAllowed)
                   {
                        // Lock out the user
                        user.setAttribute("locked_out_ind",TradePortalConstants.INDICATOR_YES);
                   }
                   
                   user.save(); 
    	           
    	           return outputDoc;
                }
                
 	    // [START] CR-482
 	    if(instanceHSMEnabled) {
 	    	try 
 	    	{
 	    		if(newPassword.equals(retypePassword)){ // DK IR T36000032172 Rel9.1
 	    		// [START] CR-543
 	 	    		newPassword = HSMEncryptDecrypt.decryptForTransport(protectedNewPassword);
 	 	    		
 	 	    	// T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
 	 	    		retypePassword = HSMEncryptDecrypt.decryptForTransport(encryptedRetypedNewPassword);
;
 	    		}
 	    		else {
 					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
 	                         TradePortalConstants.ENTRIES_DONT_MATCH,
 	                         mediatorServices.getResourceManager().getText("ChangePassword.NewPassword", TradePortalConstants.TEXT_BUNDLE),
 	                         mediatorServices.getResourceManager().getText("ChangePassword.RetypeNewPassword", TradePortalConstants.TEXT_BUNDLE));
 					return new DocumentHandler();    
 	    		}
			} catch (Exception e) {
				System.err.println("Exception occurred in ChangePasswordMediator while trying to decrypt protectedNewPassword: " + e.toString());
				// [END] CR-543
			}
 	    } // if(instanceHSMEnabled)
 	    // [END] CR-482
 	    
	    // Validate that the new password is not blank
	    if(validate && changingPassword && newPassword.equals("") )
	     {
	         // Issue an error  
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                           AmsConstants.REQUIRED_ATTRIBUTE,
                                                           mediatorServices.getResourceManager().getText("ChangePassword.NewPassword", TradePortalConstants.TEXT_BUNDLE));	        
                                                           
             return new DocumentHandler();                                                                
	     }

	    // Validate that the retype password is not blank
	    if(validate && changingPassword && retypePassword.equals("") )
	     {
	         // Issue an error  
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                           AmsConstants.REQUIRED_ATTRIBUTE,
                                                           mediatorServices.getResourceManager().getText("ChangePassword.RetypeNewPassword", TradePortalConstants.TEXT_BUNDLE));	        
                                                           
             return new DocumentHandler();                                                                
	     }		    
	    
	    
	    if(resettingUser)
	     {
	     	// Unlock the user
			user.setAttribute("locked_out_ind",TradePortalConstants.INDICATOR_NO);	
	     }
	    
	    if(validate && changingPassword)
	     {
		    // Set the password equal to the password entered by the user
		    // This is necessary so that the validations will work properly on save
		    // method will still work
	        user.setAttribute("newPassword",newPassword);
	        user.setAttribute("retypePassword",retypePassword);
	        //SPenke DateFormat
	        
	
	        if(resettingUser)
	          {
		       // Setting the password change date to null will make it so
		       // that the user must change their password next time they log in
		       user.setAttribute("password_change_date", "");
	          }
	        else
			  {
			    // Set the password change date
			    user.setAttribute("password_change_date", DateTimeUtility.getGMTDateTime());
			    
			    //cquinton 12/20/2012 reset the new user indicator
                user.setAttribute("new_user_ind",TradePortalConstants.INDICATOR_NO);
			  }
	     }
	     //CR 821 Rel 8.3 START
	    String emailAddress = inputDoc.getAttribute("/emailAddressUserPref");
	    if(StringFunction.isNotBlank(emailAddress)){
		    if(!EmailValidator.validateEmailList(emailAddress)){
		    	mediatorServices.getErrorManager().issueError(
			             TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_EMAIL,
			          "");
		    	return outputDoc;
		    }else{
		    	user.setAttribute("email_addr", emailAddress);	
		    }
	    }
	    //CR 821 - Rel 8.3 END
		user.setAttribute("invalid_login_attempt_count", "0");	     
	     
		// CR-482
		user.setAttribute("passwordValidationError", passwordValidationError);
		
		// Save the changes (this also encrypts the password)
        user.save();

           // Issue a confirmation message if the transaction was succesful
	    if(user.getErrorManager().getErrorCount() < 1)
            {
				if(!resettingUser)
				 {
					String msg = mediatorServices.getResourceManager().getText("ChangePassword.Password", TradePortalConstants.TEXT_BUNDLE);
					if("true".equals(fromUserPref)){
						msg= mediatorServices.getResourceManager().getText("UserDetail.UserPreferences", TradePortalConstants.TEXT_BUNDLE);
					}
		             mediatorServices.getErrorManager().issueError(
			             TradePortalConstants.ERR_CAT_1, TradePortalConstants.UPDATE_SUCCESSFUL,
			          msg);
				 }
				else
				 {
					mediatorServices.getErrorManager().issueError(
					     TradePortalConstants.ERR_CAT_1, TradePortalConstants.RESET_USER_SUCCESSFUL,
					     mediatorServices.getResourceManager().getText("ChangePassword.Password", TradePortalConstants.TEXT_BUNDLE));
				 	
				 }
            }	  
          return outputDoc;
   } 
   private static final boolean different(String a, String b) {
	   	if (a == null && b == null)
	   		return false;
	   	
	   	return  
	   			(a == null && b != null) || 
	   			(a != null && b == null) || 
	   			(! a.equals(b));
	   }
   
  
}