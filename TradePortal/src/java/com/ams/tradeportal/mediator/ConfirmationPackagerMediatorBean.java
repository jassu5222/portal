
package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Vector;
import java.util.Date;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ConfirmationPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(ConfirmationPackagerMediatorBean.class);

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                    throws RemoteException, AmsException{

        try {

            String unpackaging_error_text   = inputDoc.getAttribute("/unpackaging_error_text"); //non-empty in case of MSGFAIL message
            String msg_type                 = inputDoc.getAttribute("/msg_type");
            String message_id               = inputDoc.getAttribute("/message_id");
            String reply_to_message_id      = inputDoc.getAttribute("/reply_to_message_id");

            // Header fields
            String destinationID            = TradePortalConstants.DESTINATION_ID_PROPONIX;
            String senderID                 = TradePortalConstants.SENDER_ID;    // NSX CR-542 03/04/10
            String operationOrganizationID  = "";                                      //empty for confirmation messsages
            String messageType              = msg_type;
            String dateSent                 = null;
            String timeSent                 = null;
            String messageID                = message_id;

            // Body fields
            String replyToMessageID         = reply_to_message_id;
            String processingStatus         = "";
            String errorCode                = "";
            String errorText                = unpackaging_error_text;

            String processParameters = inputDoc.getAttribute("/process_parameters");
            String destinationIdParm = getParm("destination_id", processParameters);
            if  (destinationIdParm!=null && !destinationIdParm.equals("")){
            	destinationID = destinationIdParm; 
            }
            
            /* Note: EJB Server should always be up in order for DataTimeUtility to work properly
               example of timestamp expected by JPylon:
                    "03/17/2001 19:18:17"
               example of timestamp (i.e. DateSent and TimeSent) expected by MQ:
                    "03/17/2001 00:00:00" and "19:18:17"
            */
            try{
                Date gmtDate = GMTUtility.getGMTDateTime();
                if (TradePortalConstants.DESTINATION_ID_PROPONIX.equals(destinationID)) {
                	String timeStamp = DateTimeUtility.convertDateToDateTimeString(gmtDate);
                	dateSent    =  timeStamp;
                    timeSent    =  timeStamp.substring(11,19); 
                }
                else {
                	dateSent    =  DateTimeUtility.formatDate(gmtDate,"yyyyMMdd");
                    timeSent    =  DateTimeUtility.formatDate(gmtDate,"HHmm");
                    
                    if (InstrumentServices.isNotBlank(errorText)) {
                    	DocumentHandler errorDoc;
                    	DocumentHandler doc = new DocumentHandler(errorText,false);
                    	StringBuffer sb = new StringBuffer();
                    	Vector  errorList   = doc.getFragments("/Error/errorlist/error");
                        int errorListSize   = errorList.size();

                        for (int i = 0; i < errorListSize; i++){
                            errorDoc = (DocumentHandler) errorList.elementAt(i);
                            sb.append(errorDoc.getAttribute("/message"));
                            sb.append("  ");
                            }
                         errorText = sb.toString();
                        }
                    }
                
            }
            catch(Exception e){
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_PACKAGER_TIMESTAMP);
                e.printStackTrace();
                return outputDoc;
            }

            if (unpackaging_error_text == null || unpackaging_error_text.equals(""))
                processingStatus = TradePortalConstants.PROCESSING_STATUS_OKAY;
            else
                processingStatus = TradePortalConstants.PROCESSING_STATUS_FAIL;

            /* */
            mediatorServices.debug( " ConfirmationPackager: "    +
                                    " DestinationID="            + destinationID +
                                    " SenderID="                 + senderID +
                                    " OperationOrganizationID="  + operationOrganizationID +
                                    " MessageType="              + messageType +
                                    " DateSent="                 + dateSent +
                                    " TimeSent="                 + timeSent +
                                    " MessageID="                + messageID +

                                    " ReplytoMessageID="         + replyToMessageID +
                                    " ProcessingStatus="         + processingStatus +

                                    " ErrorCode="                + errorCode +
                                    " ErrorText="                + errorText
                                 );

           //IValavala May 01 09. BEGIN IR# IAUJ030435765. Use process parms instead 
            
           /*IV Jul 31 06. If UBC is appended to reply to message ID, set senderID = UBC */
            /*
            int len = reply_to_message_id.length();
            String sender="";
            if (len >= 3)
             {
				sender = reply_to_message_id.substring(len-3,len);
				if (sender!=null && sender.equals(TradePortalConstants.SENDER_ID_UBC))  {
				  replyToMessageID = reply_to_message_id.substring(0,len-3);
				  destinationID = TradePortalConstants.SENDER_ID_UBC;
				}
			 }
            */
            
           
            
            outputDoc.setAttribute("/Proponix/Header/DestinationID",            destinationID);
            outputDoc.setAttribute("/Proponix/Header/SenderID",                 senderID);
            outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID",  operationOrganizationID);
            outputDoc.setAttribute("/Proponix/Header/MessageType",              messageType);
            outputDoc.setAttribute("/Proponix/Header/DateSent",                 dateSent);
            outputDoc.setAttribute("/Proponix/Header/TimeSent",                 timeSent);
            outputDoc.setAttribute("/Proponix/Header/MessageID",                messageID);

            outputDoc.setAttribute("/Proponix/Body/ReplytoMessageID",           replyToMessageID);
            outputDoc.setAttribute("/Proponix/Body/ProcessingStatus",           processingStatus);

            outputDoc.setAttribute("/Proponix/Body/ErrorCode",                  errorCode);
            outputDoc.setAttribute("/Proponix/Body/ErrorText",                  errorText);

            return outputDoc;
        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_PACKAGER_GENERAL);
            e.printStackTrace();
            return outputDoc;
        }
  }// end of execute()
   
   public static String getParm(String parmName, String processingParms) {

       int locationOfParmName, startOfParmValue, endOfParmValue;
       String parmValue;

       locationOfParmName = processingParms.indexOf(parmName);

       // Search for the whole word only.
       // For example, getParm("portfolio_activity_uoid=0|activity=1", "activity" should return 1.
       // Keep searching if the character before or after the occurance is not a word delimitor

       while ((locationOfParmName > 0
               && processingParms.charAt(locationOfParmName - 1) != ' '
               && processingParms.charAt(locationOfParmName - 1) != '|')
               || (locationOfParmName >= 0
               && locationOfParmName < processingParms.length() - parmName.length()
               && processingParms.charAt(locationOfParmName + parmName.length()) != ' '
               && processingParms.charAt(locationOfParmName + parmName.length()) != '=')) {

           locationOfParmName = processingParms.indexOf(parmName, locationOfParmName + parmName.length());

       }

       if (locationOfParmName == -1) {
           return "";
       }

       startOfParmValue = processingParms.indexOf("=", locationOfParmName);

       if (startOfParmValue == -1 ) {
           return "";
       }

       startOfParmValue++;

       endOfParmValue = processingParms.indexOf("|", startOfParmValue);

       if (endOfParmValue == -1) {
           endOfParmValue = processingParms.length();
       }

       parmValue = processingParms.substring(startOfParmValue , endOfParmValue);

       return parmValue;
   }
}// end of ConfirmationPackagerMediatorBean class

