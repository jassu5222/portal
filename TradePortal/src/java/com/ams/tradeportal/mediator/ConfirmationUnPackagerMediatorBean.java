package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ConfirmationUnPackagerMediatorBean extends MediatorBean{
private static final Logger LOG = LoggerFactory.getLogger(ConfirmationUnPackagerMediatorBean.class);
  
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices) 
                                throws RemoteException, AmsException{
        
        OutgoingInterfaceQueue  outgoingInterfaceQueue = null; 
        
        /* 
        ReplytoMessageID and ProcessingStatus are the only attributes needed by ConfirmationUnPackager to do its job.
        Notice that reply_to_message_id attribute in Incoming_Queue table corresponds to message_id attribute on
        Outgoing_Queue table. Also msg_type attribute on Incoming_Queue table is no longer used to determine value 
        to set on confirm_status on outgoing_Queue table, but rather ProcessingStatus does. 
        */
        
        String reply_to_message_id  =   inputDoc.getAttribute("/Proponix/Body/ReplytoMessageID");
        String processingStatus     =   inputDoc.getAttribute("/Proponix/Body/ProcessingStatus");
          
        mediatorServices.debug("ConfirmationUnPackager: ReplytoMessageID="+reply_to_message_id+" ProcessingStatus="+processingStatus );
               
        try {
           outgoingInterfaceQueue  = (OutgoingInterfaceQueue)  mediatorServices.createServerEJB("OutgoingInterfaceQueue");
            
           /*
           com.amsinc.ecsg.frame.NameAndValueNotUniqueException is thrown if:
           multiple objects are found with same message_id;
           no objects are found with such message_id.
           Sometimes com.amsinc.ecsg.frame.ObjectNotFoundException is also thrown.
           */
           try{
                outgoingInterfaceQueue.find("message_id", reply_to_message_id);
           }
           catch (Exception e){
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_UNPACK_UNRECOGN_MSG, reply_to_message_id);
                e.printStackTrace();
                return outputDoc;
           }
           
           String queue_oid = outgoingInterfaceQueue.getAttribute("queue_oid");
           mediatorServices.debug("ConfirmationUnPackager: queue_oid: "  + queue_oid );
 
           String confirm_status = outgoingInterfaceQueue.getAttribute("confirm_status");
           mediatorServices.debug("ConfirmationUnPackager: confirm_status: "  + confirm_status );
                               
           if (confirm_status.equals(TradePortalConstants.CONFIRM_STATUS_COMPLETED) || confirm_status.equals(TradePortalConstants.CONFIRM_STATUS_ERROR)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_UNPACK_REDUNDANT_MSG);
                return outputDoc;
           }    
                  
           if ( confirm_status.equals(TradePortalConstants.CONFIRM_STATUS_PENDING)){
                             
                if (processingStatus.equals(TradePortalConstants.PROCESSING_STATUS_OKAY)){
                    mediatorServices.debug("ConfirmationUnPackager: ProcessingStatus is MSGOKAY");
                    outgoingInterfaceQueue.setAttribute("confirm_status", TradePortalConstants.CONFIRM_STATUS_COMPLETED);
                }
                
                if (processingStatus.equals(TradePortalConstants.PROCESSING_STATUS_FAIL)){
                    mediatorServices.debug("ConfirmationUnPackager: ProcessingStatus is MSGFAIL");
                    outgoingInterfaceQueue.setAttribute("confirm_status", TradePortalConstants.CONFIRM_STATUS_ERROR);  
                }
           }
           
           else {
                // If confirm_status in not STATUS_PENDING then no confirmation is expected
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_UNPACK_UNEXPECTED_MSG, reply_to_message_id);
                return outputDoc;
           }
                   
            outgoingInterfaceQueue.save(); 
            //outgoingInterfaceQueue.remove(); commented out purposely
            
            return outputDoc;
        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_UNPACK_GENERAL);
            e.printStackTrace();
            return outputDoc;
        }
     
  }// end of execute() 
}// end of ConfirmationUnPackagerMediatorBean class
