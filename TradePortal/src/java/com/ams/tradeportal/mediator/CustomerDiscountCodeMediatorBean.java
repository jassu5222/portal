package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Vector;

import javax.crypto.spec.SecretKeySpec;

import com.ams.tradeportal.busobj.CustomerDiscountCode;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CustomerDiscountCodeMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(CustomerDiscountCodeMediatorBean.class);

	public static final String INSERT_MODE = "C";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		String updateButtonValue = null;
		String deleteButtonValue = null;
		String currencyCode = null;
		String userLocale = null;
		String updateMode = this.UPDATE_MODE;
		long theOid = 0;
		CustomerDiscountCode customerDiscountCode = null;
		Vector discountFragments = new Vector();
		DocumentHandler doc;
		String codeMsg ="";
		String codeDesc = "";
		OutgoingInterfaceQueue oiQueue = null;
		String codeIndicaor = "";
		String deactivateIndicator = "";		
		String discountCode = "";
		String theOwnerOrgOid = "";
		Vector theCodes = null;
		Vector bankCodes = null;
		//Srinivasu_D CR-997 Rel9.3 03/13/2015 - Added below 3 variables
		String discountIndicator = null;
		int dbDefaultDiscountCount = 0;
		String dbDiscountCode = null;
		updateButtonValue = inputDoc.getAttribute("/Action/saveXPos");
		deleteButtonValue = inputDoc.getAttribute("/Action/deleteXPos");

		if (updateButtonValue != null) {
			updateMode = this.UPDATE_MODE;
		} else if (deleteButtonValue != null) {
			updateMode = this.DELETE_MODE;
		}

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);
		
		
		//RPasupulati -Rel 9.1 IR #T36000032488
		if (buttonPressed.equalsIgnoreCase(TradePortalConstants.BUTTON_SAVETRANS))
			updateMode = this.UPDATE_MODE;
		else if (buttonPressed.equalsIgnoreCase(TradePortalConstants.BUTTON_DELETE))
			updateMode = this.DELETE_MODE;

		if (updateMode == null) {
			throw new AmsException("Unable to determine update mode.");
		}
		String loginUserRights = inputDoc.getAttribute("/LoginUser/security_rights");
		if ( !SecurityAccess.hasRights(loginUserRights, SecurityAccess.MAINTAIN_DISCOUNT_CODES ) ) {
				throw new AmsException("User is trying to maintain: Discount Codes without proper access level");
		}
		
		try {
	        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
	        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
	        javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");	
	        theOwnerOrgOid = inputDoc.getAttribute("/CustomerDiscountCode/p_owner_oid");
	      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
			String sqlStmntStr = "select discount_code  from customer_discount_code  where p_owner_oid in (?) ";			
	        //First, obtain all current codes - This will be used for checking duplicates
	        DocumentHandler codesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new Object[]{theOwnerOrgOid});
	        
	        if (codesDoc != null){
	        	theCodes  = getValues(codesDoc.getFragments("/ResultSetRecord/"), "DISCOUNT_CODE");
	        }	        

	        //Obtain codes from BankRefData
	        sqlStmntStr = "select code from bankrefdata"; 
	        DocumentHandler bankCodesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr, false, new ArrayList<Object>());
	        
	        if (bankCodesDoc != null){
	        	bankCodes  = getValues(bankCodesDoc.getFragments("/ResultSetRecord/"), "CODE");
	        }			
			//
			// Get all fragments from the inputdoc corresponding to each row
			// add them to the bean and save row at a time
			//Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added for Checking Default Disc Code
	        String discRateCountSql = "select discount_code  from customer_discount_code  where p_owner_oid in (?)  and default_disc_rate_ind = ?";			
	        //First, obtain all current codes - This will be used for checking duplicates
	        DocumentHandler discRateCountDoc = DatabaseQueryBean.getXmlResultSet(discRateCountSql.toString(), false, new Object[]{theOwnerOrgOid, TradePortalConstants.INDICATOR_YES});
	        
	        if (discRateCountDoc != null){
	        	Vector rateList = discRateCountDoc.getFragments("/ResultSetRecord"); 	
  		 		 DocumentHandler listDoc = (DocumentHandler)rateList.elementAt(0);			
  		 		 dbDiscountCode  = listDoc.getAttribute("/DISCOUNT_CODE");	
				 if(StringFunction.isNotBlank(dbDiscountCode)){
					 dbDefaultDiscountCount++;
				 }
	        }
	        
	        //Srinivasu_D CR#997 Rel9.3 03/10/2015 - End
			discountFragments = inputDoc.getFragments("/CustomerDiscountCode/");
			int available = discountFragments.size();
			int blank = 0;
			int discountIndCount = 0;
			boolean duplicateDiscFound = false;
			if (discountFragments != null) {
				for (int i = 0; i < discountFragments.size(); i++) {
					doc = (DocumentHandler) discountFragments.elementAt(i);
					
					//SSikhakolli - Rel 9.0 IR# T36000029332 06/18/2014 - Begin
					if (StringFunction.isNotBlank(doc.getAttribute("/customer_erp_gl_code_oid"))){
						theOid = Long.parseLong(doc.getAttribute("/customer_erp_gl_code_oid"));
						//RPasupulati -Rel 9.1 IR #T36000032488 start
					}else{
						theOid = Long.parseLong(doc.getAttribute("/customer_discount_code_oid"));
					}
					//RPasupulati -Rel 9.1 IR #T36000032488 End
					//SSikhakolli - Rel 9.0 IR# T36000029332 06/18/2014 - End
					
					deactivateIndicator = doc.getAttribute("/deactivate_ind");
					discountCode = doc.getAttribute("/discount_code");
					theOwnerOrgOid = doc.getAttribute("/p_owner_oid");
					codeDesc = doc.getAttribute("/discount_description");
					//Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added default disc rate ind
					discountIndicator = doc.getAttribute("/default_disc_rate_ind");
					
					if(TradePortalConstants.INDICATOR_YES.equals(discountIndicator)){
						discountIndCount++;
					}
					if (theOid == 0) {
						updateMode = this.INSERT_MODE;
					}

					
					
					// Get a handle to an instance of the object
					customerDiscountCode = (CustomerDiscountCode) mediatorServices
					.createServerEJB("CustomerDiscountCode");

					// AiA - Start
					if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
						mediatorServices
						.debug("Mediator: Creating new Discount Code");

						theOid = customerDiscountCode.newObject();
						
						//outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(""+theOid, secretKey) );						
												
						
					} else {
						mediatorServices.debug("Mediator: Retrieving Discount Code "
								+ theOid);

						customerDiscountCode.getData(theOid);
					}

					if (updateMode.equalsIgnoreCase(this.DELETE_MODE)) {
						mediatorServices
						.debug("Mediator: Deleting discountCode object");

						// Delete the Discount Code
						customerDiscountCode.delete();
						codeMsg = discountCode;
						mediatorServices
						.debug("Mediator: Completed the delete of the Discount Code object");
					} else {
						//Validations:
						//If the whole row is empty, skip it. Only validate when at least one item is entered
						//if(StringFunction.isBlank(discountCode) && StringFunction.isBlank(codeDesc) && StringFunction.isBlank(deactivateIndicator)){
						if(StringFunction.isBlank(discountCode) && StringFunction.isBlank(codeDesc)){
							
							//skip empty row
							blank++;
							if (blank == available){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DISCOUNT_CODES_REQUIRED);	
								return outputDoc;	
							}
							continue;
						}
						else if(StringFunction.isBlank(discountCode) || StringFunction.isBlank(codeDesc)){						
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DISCOUNT_CODES_REQUIRED);	
								return outputDoc;						
						}	

						if(StringFunction.isNotBlank(discountCode)){
							if(discountCode.length()>2){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DISCOUNT_CODES_TWO_CHARS);	
								return outputDoc;
							}
						}
						if(codeDesc.length()>30){
							mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.DISCOUNT_CODES_THIRTY_CHARS);	
							return outputDoc;
						}	
						
						//Srinivasu_D CR#997 Rel9.3 03/10/2015 - Added validation for def disc rate ind
						if(discountIndCount>1){	
							duplicateDiscFound = true;							
						}
						if(StringFunction.isNotBlank(dbDiscountCode) && !dbDiscountCode.equals(discountCode)){
							if(discountIndCount>0){	
								duplicateDiscFound = true;							
							}							
						}						
						//Srinivasu_D CR#997 Rel9.3 03/10/2015 - End
						
						if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
							if(theCodes != null && theCodes.contains(discountCode.toUpperCase())){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.DISCOUNT_CODES_EXISTS);	
								return outputDoc;						
							}
							//Industry codes IR  T36000015501  
							if(bankCodes != null && bankCodes.contains(discountCode.toUpperCase())){
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.INDUSTRY_DISCOUNT_CODES_EXISTS);	
								return outputDoc;						
							}		
							//Srinivasu_D CR#997 Rel9.3 03/10/2015 - Issue error if dup found
							if(duplicateDiscFound){	
								if(StringFunction.isNotBlank(outputDoc.getAttribute("oid"))){
									outputDoc.setAttribute("oid","");
								}
								mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.DEFAULT_DISCOUNT_RATE_DUPLICATE);	
									return outputDoc;
							}
							//Srinivasu_D CR#997 Rel9.3 03/10/2015 - End
							customerDiscountCode.setAttribute("customer_disc_code_oid",""+theOid);							
							outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(""+theOid, secretKey) );

						} else {
							
							outputDoc.setAttribute( "oid", EncryptDecrypt.encryptStringUsingTripleDes(
                                    doc.getAttribute("/customer_discount_code_oid"), secretKey) );	
							if(!discountCode.equalsIgnoreCase(customerDiscountCode.getAttribute("discount_code"))){
								if(theCodes != null && theCodes.contains(discountCode.toUpperCase())){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.DISCOUNT_CODES_EXISTS);	
									return outputDoc;						
								}	
								//Industry codes
								if(bankCodes != null && bankCodes.contains(discountCode.toUpperCase())){
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.INDUSTRY_DISCOUNT_CODES_EXISTS);	
									return outputDoc;						
								}								
							}
							//Srinivasu_D CR#997 Rel9.3 03/10/2015 - Issue error if dup found
							if(StringFunction.isNotBlank(dbDiscountCode) && !dbDiscountCode.equals(discountCode)){
							if(discountIndCount>0){	
								
								mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.DEFAULT_DISCOUNT_RATE_DUPLICATE);	
									return outputDoc;					
							}							
						 }
						//Srinivasu_D CR#997 Rel9.3 03/10/2015 - End	
						}

						if(StringFunction.isBlank(deactivateIndicator)) deactivateIndicator = TradePortalConstants.INDICATOR_NO;
						if(StringFunction.isBlank(codeIndicaor)) codeIndicaor = TradePortalConstants.INDICATOR_NO;						
						if(StringFunction.isBlank(discountIndicator)) discountIndicator = TradePortalConstants.INDICATOR_NO;
						//CR-741 Rel8.2 IR T36000015498
						//Check for unicode chars
			            if (!StringFunction.canEncodeIn_WIN1252(discountCode) || !StringFunction.canEncodeIn_WIN1252(codeDesc) ) {
			            	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                        TradePortalConstants.UNICODE_NOT_ALLOWED,
			                        mediatorServices.getResourceManager().getText("ERPGLCodes.DiscountCodes",
			                                TradePortalConstants.TEXT_BUNDLE));
			                return outputDoc;
			            }						
						customerDiscountCode.setAttribute("discount_code",
								discountCode);
						customerDiscountCode.setAttribute("discount_description",
								codeDesc);
						customerDiscountCode.setAttribute("p_owner_oid",
								theOwnerOrgOid);
						customerDiscountCode.setAttribute("deactivate_ind",
								deactivateIndicator);
						customerDiscountCode.setAttribute("default_disc_rate_ind",
								discountIndicator);
						
						if(i==0){
							codeMsg = discountCode;
						}else{
							if(StringFunction.isNotBlank(discountCode)){								
								codeMsg = codeMsg + "," + discountCode;
							}
						}

						// AiA- end
						if(StringFunction.isNotBlank(discountCode)){
							customerDiscountCode.save();
						}			

					}
				}
				//Now set on Queue
				//


				oiQueue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
				oiQueue.newObject();
				
				
				oiQueue.setAttribute("date_created",
						DateTimeUtility.getGMTDateTime());
				oiQueue.setAttribute("status",
						TradePortalConstants.OUTGOING_STATUS_STARTED);
				oiQueue.setAttribute("message_id",
						InstrumentServices.getNewMessageID());
				StringBuilder processParameters = new StringBuilder("REFTABLETYPE=DISCCODES|REFTABLEOWNEROID=");
				
				oiQueue.setAttribute("process_parameters",
						processParameters.toString()+ theOwnerOrgOid);
				
				oiQueue.setAttribute("msg_type",
						TradePortalConstants.CUSREF);
				
				if (oiQueue.save() != 1) {
					throw new AmsException(
							"Error occurred while inserting Discount Code type in Outgoing Queue");
				}					
				//
				//end Queue setting	
			}
		} catch (Exception e) {
			String serverErrorNumber = "SE" + System.currentTimeMillis();
			LOG.info("General exception caught in CustomerDiscountCodeMediator. "
					+ "Server error#" + serverErrorNumber);
			e.printStackTrace();
			// we don't know exactly what this is, so just issue a generic error
			// another option is to just propogate the exception
			mediatorServices.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.SERVER_ERROR, serverErrorNumber);
		} finally {

			mediatorServices.debug("Mediator: Result of Discount update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {

				// Save the update record to the ref data audit log
				mediatorServices.debug("Mediator: Creating audit record");

				mediatorServices.debug("Mediator: Finished with audit record");

				// Finally issue a success message based on update type.
				if (updateMode.equalsIgnoreCase(this.DELETE_MODE)) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DELETE_SUCCESSFUL, codeMsg);
				} else if (updateMode.equalsIgnoreCase(this.INSERT_MODE)) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INSERT_SUCCESSFUL, codeMsg);
				} else {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.UPDATE_SUCCESSFUL, codeMsg);
				}
			}

		}

		return outputDoc;
	}
	//Returns string values in upper case
	private Vector getValues(Vector frags, String xpath){
		Vector<String> values = new Vector<String>();
        DocumentHandler resDoc = null;
        for(int j=0; j<frags.size(); j++){
        	resDoc = (DocumentHandler)frags.elementAt(j);
        	values.addElement(resDoc.getAttribute(xpath).toUpperCase());
        }		
		return values;
	}

}