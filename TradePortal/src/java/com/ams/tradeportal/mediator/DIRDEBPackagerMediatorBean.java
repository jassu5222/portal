package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.PaymentParty;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.*;



/**
 * Manages DIRDEB Message Packaging....
 *
 *     Copyright  ? 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class DIRDEBPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(DIRDEBPackagerMediatorBean.class);


	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		try{
			ClientBank clientBank = null;
			Instrument instrument = null;
			Transaction transaction = null;
			OperationalBankOrganization operationalBankOrganization = null;
			CorporateOrganization corpOrg = null;

			Date gmtDate = GMTUtility.getGMTDateTime();
			String dateSent = DateTimeUtility.formatDate(gmtDate,"yyyyMMdd");
			String timeSent = DateTimeUtility.formatDate(gmtDate,"HHmm");

			String InstrumentOid  = inputDoc.getAttribute("/instrument_oid");
			String TransactionOid = inputDoc.getAttribute("/transaction_oid");


			//Get The Required Objects
			mediatorServices.debug("Ready to Instantiate Instrument Object");
			instrument  = (Instrument)  mediatorServices.createServerEJB("Instrument",  Long.parseLong(InstrumentOid));
			mediatorServices.debug("Ready to instantiate transaction" );
			transaction = (Transaction)instrument.getComponentHandle("TransactionList", Long.parseLong(TransactionOid));
			mediatorServices.debug("Ready to instantiate CustomerTerms" );
			Terms customerEnteredTerms = (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));

			//Client Bank
			String clientBankOid = instrument.getAttribute("client_bank_oid");
			clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank", Long.parseLong(clientBankOid));
			//Corporate Org
			String corpOrgOid = instrument.getAttribute("corp_org_oid");
			corpOrg	= (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
			//Operational Org
			String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
			operationalBankOrganization	= (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization",Long.parseLong(opBankOrgOid));

			String creditAmt = customerEnteredTerms.getAttribute("amount");
			String creditAccOid = customerEnteredTerms.getAttribute("credit_account_oid");
			String creditAccount = "";
			if(StringFunction.isNotBlank(creditAccOid))
			{
				Account account = (Account)mediatorServices.createServerEJB("Account", Long.parseLong(creditAccOid));
				creditAccount = account.getAttribute("account_number");
			}

			// *******************************************************************************************************************//
			// ************************************************************ HEADER ***********************************************//
			// *******************************************************************************************************************//
			outputDoc.setAttribute("/Proponix/Header/DestinationID",            clientBank.getAttribute("OTL_id"));
			outputDoc.setAttribute("/Proponix/Header/SenderID",                 TradePortalConstants.SENDER_ID);    // NSX CR-542 03/04/10
			outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID",  operationalBankOrganization.getAttribute("Proponix_id"));
			outputDoc.setAttribute("/Proponix/Header/MessageType",              MessageType.DIRECTDEBIT);
			outputDoc.setAttribute("/Proponix/Header/DateSent",                 dateSent);
			outputDoc.setAttribute("/Proponix/Header/TimeSent",                 timeSent);
			outputDoc.setAttribute("/Proponix/Header/MessageID",                inputDoc.getAttribute("/message_id"));

			// *******************************************************************************************************************//
			// ******************************************************** SUB - HEADER *********************************************//
			// *******************************************************************************************************************//

			outputDoc.setAttribute("/Proponix/SubHeader/CustomerName",          corpOrg.getAttribute("Proponix_customer_id"));
			outputDoc.setAttribute("/Proponix/SubHeader/CreditAmt",             creditAmt);
			outputDoc.setAttribute("/Proponix/SubHeader/CreditAccount",         creditAccount);
			outputDoc.setAttribute("/Proponix/SubHeader/TransactionID",         instrument.getAttribute("complete_instrument_id"));

			// *******************************************************************************************************************//
			// ******************************************************** BODY *****************************************************//
			// *******************************************************************************************************************//

			/* STang - IR SEUK011354245 - Execution Date is not in the correct format . Should be in the format - yyyyMMdd Begin */
			Date paymentDate = transaction.getAttributeDate("payment_date");
			String executionDate = DateTimeUtility.formatDate(paymentDate,"yyyyMMdd");
			outputDoc.setAttribute("/Proponix/Body/CreditDetail/ExecutionDate",          executionDate);
			/* STang - IR SEUK011354245 - End */

			PaymentParty orderingParty = null;
			PaymentParty orderingPartyBank = null;
			if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("c_OrderingParty")))
			{
				orderingParty = (PaymentParty) customerEnteredTerms.getComponentHandle("OrderingParty");
				String [] paymentPartyAttributes = {"bank_name","address_line_1","address_line_2","address_line_3","address_line_4","country"};
				String [] paymentPartyXMLTag = {"OrderingPartyName","OrderingPartyAddressLn1","OrderingPartyAddressLn2","OrderingPartyAddressLn3","OrderingPartyAddressLn4","OrderingPartyCountry"};

				Hashtable paymentPartyValues     = orderingParty.getAttributes(paymentPartyAttributes);
				for (int i=0; i < paymentPartyAttributes.length; i++)
				{
					if (StringFunction.isNotBlank((String) paymentPartyValues.get(paymentPartyAttributes[i]) ))
						outputDoc.setAttribute("/Proponix/Body/CreditDetail/" +paymentPartyXMLTag[i] ,     (String) paymentPartyValues.get(paymentPartyAttributes[i] ));
				}
			}

			if (StringFunction.isNotBlank(customerEnteredTerms.getAttribute("c_OrderingPartyBank")))
			{
				orderingPartyBank = (PaymentParty) customerEnteredTerms.getComponentHandle("OrderingPartyBank");
				String [] paymentPartyBankAttributes = {"bank_name","bank_branch_code","address_line_1","address_line_2","address_line_3","country"};
				String [] paymentPartyBankXMLTag = {"OrderingPartyBankName","OrderingPartyBankBranchCode","OrderingPartyBankAddressLn1","OrderingPartyBankAddressLn2","OrderingPartyBankAddressLn3","OrderingPartyBankCountry"};

				Hashtable paymentPartyValues     = orderingPartyBank.getAttributes(paymentPartyBankAttributes);
				for (int i=0; i < paymentPartyBankAttributes.length; i++)
				{
					if (StringFunction.isNotBlank( (String)paymentPartyValues.get(paymentPartyBankAttributes[i])))
						outputDoc.setAttribute("/Proponix/Body/CreditDetail/" +paymentPartyBankXMLTag[i] ,     (String) paymentPartyValues.get(paymentPartyBankAttributes[i] ));
				}
			}


			ComponentList domesticPayResults = (ComponentList)transaction.getComponentHandle("DomesticPaymentList");

			for(int i = 0; i < domesticPayResults.getObjectCount(); i++){
				domesticPayResults.scrollToObjectByIndex(i);
				DomesticPayment domesticPayment = (DomesticPayment)domesticPayResults.getBusinessObject();
				DocumentHandler handler = new DocumentHandler();

				String [] payerPartyAttributes = {"payee_address_line_1","payee_address_line_2","payee_address_line_3","payee_address_line_4","country","payee_fax_number","payee_email","payee_bank_name"};
				String [] payerPartyXMLTag = {"PayerAddressLn1","PayerAddressLn2","PayerAddressLn3","PayerAddressLn4","PayerCountry","PayerFaxNumber","PayerEmailAddress","PayerBankName"};

				String [] payerPartyBankAttributes = {"payee_branch_name","address_line_1","address_line_2","address_line_3","payee_bank_country"};
				String [] payerPartyBankXMLTag = {"PayerBankBranchName","PayerBankBranchAddressLn1","PayerBankBranchAddressLn2","PayerBankBranchAddressLn3","PayerBankBranchCountry"};

				Hashtable payerPartyValues     = domesticPayment.getAttributes(payerPartyAttributes);
				Hashtable payerPartyBankValues     = domesticPayment.getAttributes(payerPartyBankAttributes);

				String boReference = domesticPayment.getAttribute("customer_reference");
				String detailsOfPayment = domesticPayment.getAttribute("payee_description");

				Date valueDate = domesticPayment.getAttributeDate("value_date");
				String paymentValueDate = DateTimeUtility.formatDate(valueDate,"yyyyMMdd");

				handler.setAttribute("/PaymentLineDetail/PaymentUniqueID", domesticPayment.getAttribute("domestic_payment_oid"));
				handler.setAttribute("/PaymentLineDetail/PaymentMethodType", InstrumentType.DIRECT_DEBIT_INSTRUCTION);
				handler.setAttribute("/PaymentLineDetail/PayerCurrency", customerEnteredTerms.getAttribute("amount_currency_code"));
				handler.setAttribute("/PaymentLineDetail/PayerAmount", domesticPayment.getAttribute("amount"));
				handler.setAttribute("/PaymentLineDetail/PaymentValueDate", paymentValueDate);

				if(StringFunction.isNotBlank(boReference))
					handler.setAttribute("/PaymentLineDetail/BOReference", boReference);

				handler.setAttribute("/PaymentLineDetail/BankChrgs", domesticPayment.getAttribute("bank_charges_type"));

				if(StringFunction.isNotBlank(detailsOfPayment))
					handler.setAttribute("/PaymentLineDetail/DetailsOfPayment", detailsOfPayment);

				handler.setAttribute("/PaymentLineDetail/PayerAccountNum", domesticPayment.getAttribute("payee_account_number"));
				handler.setAttribute("/PaymentLineDetail/PayerName", domesticPayment.getAttribute("payee_name"));

				for (int j=0; j < payerPartyAttributes.length; j++)
				{
					if (StringFunction.isNotBlank((String) payerPartyValues.get(payerPartyAttributes[j])))
						handler.setAttribute("/PaymentLineDetail/" +payerPartyXMLTag[j] ,     (String) payerPartyValues.get(payerPartyAttributes[j] ));
				}

				handler.setAttribute("/PaymentLineDetail/PayerBankBranchCode", domesticPayment.getAttribute("payee_bank_code"));

				for (int k=0; k < payerPartyBankAttributes.length; k++)
				{
					if (StringFunction.isNotBlank((String) payerPartyBankValues.get(payerPartyBankAttributes[k]))){
						String temp = (String) payerPartyBankValues.get(payerPartyBankAttributes[k] );
						//IR 17424 - if branchname >35, just package first 35chars
						if("payee_branch_name".equals(payerPartyBankAttributes[k]) && temp.length()>35){
							temp = temp.substring(0, 35);
						}
						handler.setAttribute("/PaymentLineDetail/" +payerPartyBankXMLTag[k] ,     temp);
					}
				}

				outputDoc.addComponent("/Proponix/Body/PaymentLineDetail("+i+")", handler.getFragment("/PaymentLineDetail"));
			}

		}
		catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "DIRDEB Message Packaging");
			if(outputDoc != null){
				mediatorServices.debug("The outputDoc generated so far is:" + "\n" + outputDoc.toString());
			}
			e.printStackTrace();

		}

		return outputDoc;
	}

}
