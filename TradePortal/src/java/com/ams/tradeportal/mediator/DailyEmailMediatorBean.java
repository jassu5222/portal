package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.PeriodicTaskHistory;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;


/*
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DailyEmailMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(DailyEmailMediatorBean.class);
    private static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");


  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException
  {
        try
         {
             // Create a record in the database that this task is being run.
             // The record of when the task has run is important in the agent's determining when this task should be run
             PeriodicTaskHistory taskHistory = (PeriodicTaskHistory)mediatorServices.createServerEJB("PeriodicTaskHistory");
             taskHistory.newObject();
             taskHistory.setAttribute("task_type", TradePortalConstants.DAILY_EMAIL_TASK_TYPE);
             taskHistory.setAttribute("run_timestamp", formatter.format(GMTUtility.getGMTDateTime(true, false)));
             taskHistory.save();
         }
        catch(Exception e)
         {
             e.printStackTrace();
             return new DocumentHandler();
         }

        LOG.info("Starting run of daily email process...");

        DocumentHandler resultSet = null;
        Vector corpOrgList = null;

        try{
	   // Create an email for each corp org that sends email daily for messages and notifications
           // or sends both a daily email and an email for each individual message/notification and
           // has notifications or messages assigned to it
        	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
        /**
         * Rel9.5 - CR 927B - Modifying the below query to retrieve the organization_oid from notification_rule table, instead of retrieving a_notify_rule_oid from corporate_org table. 
         * This is done as the column 'a_notify_rule_oid' will be removed.
         */
           String sql = "select organization_oid from corporate_org where activation_status = ?"
        		   		+" and organization_oid in (select p_owner_org_oid from notification_rule"
        		   		+" where email_frequency = ? or email_frequency = ?)";

           resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, TradePortalConstants.ACTIVE, TradePortalConstants.NOTIF_RULE_DAILY, TradePortalConstants.NOTIF_RULE_BOTH);

           if(resultSet == null)
            {
              LOG.info("Finised run of daily email process. Sent 0 e-mails. ");
              return new DocumentHandler();
            }

           corpOrgList = resultSet.getFragments("/ResultSetRecord");

        } catch (Exception e) {
	   e.printStackTrace();
           return new DocumentHandler();
        }

           String corpOrgOid = null;
           int emailsSent = 0;

           LOG.info("DateTimeUtility.getCurrentDate returns this  :"+DateTimeUtility.getCurrentDate());

           String currentDate = String.valueOf(DateTimeUtility.getCurrentDate());
           CorporateOrganization corpOrg = null;


           for (int i=0; i< corpOrgList.size(); i++)
           {
              try
               {
				 corpOrgOid = resultSet.getAttribute("/ResultSetRecord(" + i + ")/ORGANIZATION_OID");
       			 corpOrg	= (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));

				 //First check if the daily email was sent for the current date
					if(!currentDate.equals(corpOrg.getAttribute("last_email_date")))
					  {
						  // Initialize variables which will help determine if an email should be triggered
						  boolean triggerEmail = false;
						  String instrumentOid = null;
						  String transactionOid = null;
						  int messageCount = 0;
						  // Check if any mail messages have been received for this corporate customer
						//jgadela R91 IR T36000026319 - SQL INJECTION FIX
						  String whereClause = " a_assigned_to_corp_org_oid = ? and message_status <> ?"
							  					+" and message_source_type = ?";

						  messageCount = DatabaseQueryBean.getCount("message_oid", "mail_message", whereClause, false, corpOrgOid, TradePortalConstants.REC_DELETED, TradePortalConstants.BANK);

						  // Trigger an email if mail messages exist for this corporate customer
						  if (messageCount > 0 )
						  {
							 triggerEmail = true;
						  }
						  else
						  {
							 // If no mail messages exist for this customer then check if any notifications have
							 // been received for this corporate customer
							 int transactionCount = 0;
							//jgadela R91 IR T36000026319 - SQL INJECTION FIX
							 whereClause =" show_on_notifications_tab = ?  and p_instrument_oid in (select instrument_oid from instrument "
								 			+" where instrument_oid = p_instrument_oid "
								 			+" and a_corp_org_oid = ?)";

							 transactionCount = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, false, TradePortalConstants.INDICATOR_YES, corpOrgOid);
							 if (transactionCount > 0)
							 {
							triggerEmail = true;
							 }
						  }

						  if (triggerEmail)
						  {
							 /*CorporateOrganization corpOrg = (CorporateOrganization)
																  mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
							 */
							 corpOrg.createEmailMessage(instrumentOid, transactionOid, " ", " ", TradePortalConstants.EMAIL_TRIGGER_DAILY);
								 emailsSent++;
						   LOG.info("Trying to save the date :"+currentDate);
							// corpOrg.setAttribute("last_email_date",currentDate);
                             //currentDate = String.valueOf(DateTimeUtility.convertTimestampToDateString(currentDate,"yyyyMMdd"));
                    	  // LOG.info("Trying to save the converted date :"+currentDate);
							 corpOrg.setAttribute("last_email_date",currentDate);
							 corpOrg.save();
							  }
					}
					//End of compare currentDate and CorpOrg.last_email_date
               }
              catch(Exception e)
               {
                  LOG.info("Could not send daily email for corporate org:"+ corpOrgOid);
                  e.printStackTrace();
               }
           }

        LOG.info("Finised run of daily email process. Sent "+emailsSent+" e-mails. ");

        return new DocumentHandler();
  }

}
