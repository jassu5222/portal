package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.ams.tradeportal.busobj.DashboardSection;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DashboardPreferencesMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(DashboardPreferencesMediatorBean.class);
   // Business methods
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
	   List<String> deleteList = new ArrayList<String>();
	   Vector dashboardSectionList = null;
	   String userOid = null;
	   DocumentHandler dashboardSectionDoc = null;
	   String dashboardSectionOid = "";
	   DashboardSection dashboardSection = null;

	   //for now we just update dashboard sections
	   //other things could be added later.
      
      try
      {
    	  userOid = inputDoc.getAttribute("/User/userOid");

    	  //first retrieve the existing entries to see if there are any to delete...
    	  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    	  String deleteSql = "select dashboard_section_oid from dashboard_section where a_user_oid = ?";

    	  DocumentHandler deleteDoc = DatabaseQueryBean.getXmlResultSet(deleteSql, false, new Object[]{userOid});
    	  if (deleteDoc != null) {
    		  Vector dashList = deleteDoc.getFragments("/ResultSetRecord");
    		  for( int i=0; i<dashList.size(); i++ ) {
    			  DocumentHandler dashDoc = (DocumentHandler) dashList.get(i);
    			  String sectionOidAttr = dashDoc.getAttribute("/DASHBOARD_SECTION_OID");
    			  deleteList.add(sectionOidAttr);
    		  }
    	  }

    	  //insert/update - flag the existing ones updated
    	  dashboardSectionList = inputDoc.getFragments("/DashboardSection");

    	  for (int i = 0; i < dashboardSectionList.size(); i++)
    	  {
    		  // Retrieve the next FX rate item
    		  dashboardSectionDoc = (DocumentHandler) dashboardSectionList.elementAt(i);

    		  //todo: verify display order is unique.

    		  // Get a handle to an instance of the FX rate object
    		  dashboardSection = (DashboardSection) mediatorServices.createServerEJB("DashboardSection");

    		  // Retrieve the unique oid
    		  dashboardSectionOid = dashboardSectionDoc.getAttribute("/dashboard_section_oid");
    		  String   dashboardNoSections = dashboardSectionDoc.getAttribute("/display_no_sections");

    		  if ( StringFunction.isNotBlank(dashboardSectionOid)  &&
    				  !"0".equals(dashboardSectionOid) ) { //update    			 
    			  dashboardSection.getData(Long.parseLong(dashboardSectionOid));
    			  // Set the optimistic lock attribute to make sure hasn't been changed by someone else
    			  // since it was first retrieved
    			  dashboardSection.setAttribute("opt_lock", dashboardSectionDoc.getAttribute("/opt_lock"));

    			  int deleteIdx = deleteList.indexOf(dashboardSectionOid);
    			  if ( deleteIdx>=0 && !TradePortalConstants.INDICATOR_YES.equals(dashboardNoSections)) {
    				  deleteList.remove(deleteIdx);
    			  }
    		  }else { //insert
    			  dashboardSection.newObject();
    		  }
    		  //MEer IR-34829  If DisplayNoSections check box is selected then save section_id  as  NONE in DashBoard Section Object.
    		  if(StringFunction.isNotBlank(dashboardNoSections)){
    			  if(TradePortalConstants.INDICATOR_YES.equals(dashboardNoSections)){    				 
    				  dashboardSection.setAttribute("section_id", TradePortalConstants.DASHBOARD_NO_SECTIONS_DISPLAY);
    				  dashboardSection.setAttribute("user_oid", userOid);
    				  dashboardSection.setAttribute("display_order", "0");
    			  }
    		  }else{
    			  // Set object attributes equal to input arguments and save
    			  dashboardSection.setAttribute("user_oid", userOid);
    			  dashboardSection.setAttribute("display_order", dashboardSectionDoc.getAttribute("/display_order"));
    			  dashboardSection.setAttribute("section_id", dashboardSectionDoc.getAttribute("/section_id"));
    		  }

    		  // Save the FX rate
    		  dashboardSection.save();

    	  }
    	  //delete those still in delete list
    	  for (int i = 0; i<deleteList.size(); i++) {
    		  dashboardSectionOid = deleteList.get(i);
    		  // Get a handle to an instance of the FX rate object
    		  dashboardSection = (DashboardSection) mediatorServices.createServerEJB("DashboardSection");
    		  dashboardSection.getData(Long.parseLong(dashboardSectionOid));
    		  dashboardSection.delete();
    	  }
      }
      catch (AmsException e)
      {
    	  LOG.info("AmsException occurred in DashboardPreferencesMediator: " + e.toString());
      }
      catch (RemoteException e)
      {
    	  LOG.info("RemoteException occurred in DashboardPreferencesMediator: " + e.toString());
      }
      finally
      {
    	  // If all were updated successfully without errors, issue info message indicating this
    	  if (this.isTransSuccess(mediatorServices.getErrorManager()))
    	  {
    		  mediatorServices.getErrorManager().issueError(
    				  TradePortalConstants.ERR_CAT_1,
    				  TradePortalConstants.UPDATE_SUCCESSFUL,
    				  mediatorServices.getResourceManager().getText(
    						  "DashboardPreferences.Title", TradePortalConstants.TEXT_BUNDLE) );
    	  }
      }

      return outputDoc;
   }
}
