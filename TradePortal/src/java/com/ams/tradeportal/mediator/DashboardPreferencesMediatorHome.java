package com.ams.tradeportal.mediator;

import com.amsinc.ecsg.frame.*;
import javax.ejb.*;
import java.rmi.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface DashboardPreferencesMediatorHome extends javax.ejb.EJBHome
{
   public DashboardPreferencesMediator create() throws CreateException, RemoteException;
}
