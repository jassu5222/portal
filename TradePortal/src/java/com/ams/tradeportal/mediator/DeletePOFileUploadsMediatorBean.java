package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;

import java.rmi.*;

import javax.naming.*;
import javax.ejb.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class DeletePOFileUploadsMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(DeletePOFileUploadsMediatorBean.class);
	/**
	 * This method performs deletions of PO Upload Files The format
	 * expected from the input doc is:
	 * 
	 * <TransactionList> <POFileUpload
	 * ID="0"><POFileUploadData>1/VALIDATION_SUCCESSFUL
	 * </POFileUploadData></POFileUpload> <POFileUpload
	 * ID="1"><POFileUploadData
	 * >2/VALIDATION_IN_PROGRESS</POFileUploadData></POFileUpload>
	 * <POFileUpload
	 * ID="2"><POFileUploadData>3/VALIDATED_WITH_ERRORS</
	 * POFileUploadData></POFileUpload> </TransactionList> <User>
	 * <userOid>1001</userOid>
	 * <securityRights>302231454903657293676543</securityRights> </User>
	 * 
	 * 
	 * This is a non-transactional mediator that executes transactional methods
	 * in business objects
	 * 
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator
	 *            XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall
	 *            Mediator XML document)
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary
	 *            functionality for a mediator.
	 */

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		// get each PO File Upload Oid and process each po upload
		// file; each PO upload file is unique within itself
		DocumentHandler docUpload = null;
		PurchaseOrderFileUpload uploadFile = null;
		Vector uploadList = null;
		String uploadData = null;
		String uploadOid = null;
		String validationStatus = null;
		String securityRights = null;
		String userLocale = null;
		String userOid = null;
		int count = 0;
		int separator1 = 0;

		// populate variables with inputDocument information
		uploadList = inputDoc.getFragments("/POFileUploadList/POFileUpload");
		userOid = inputDoc.getAttribute("/User/userOid");
		securityRights = inputDoc.getAttribute("../securityRights");
		if (uploadList != null)
			count = uploadList.size();

		// get the locale from client server data bridge, used for getting
		// locale specific information when passing error parameters
		userLocale = mediatorServices.getCSDB().getLocaleName();

		mediatorServices
				.debug("DeletePOFileUploadsMediator: number of po upload files to delete: "
						+ count);

		if (count <= 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"TransactionAction.Deleted",
							TradePortalConstants.TEXT_BUNDLE));

			return outputDoc;
		}

		for (int i = 0; i < count; i++) {
			docUpload = (DocumentHandler) uploadList.get(i);
			uploadData = docUpload.getAttribute("/poFileUploadData");

			// Split up the po_file_upload_oid, transaction_status
			separator1 = uploadData.indexOf('/');
			uploadOid = uploadData.substring(0, separator1);
			validationStatus = uploadData.substring(separator1 + 1);

			mediatorServices
					.debug("DeletePOFileUploadsMediator: uploadData contents: "
							+ uploadData + "Upload oid: " + uploadOid
							+ " Validation status: " + validationStatus);

			try {
				if ((TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED
						.equals(validationStatus))) {
					uploadFile = (PurchaseOrderFileUpload) mediatorServices
							.createServerEJB("PurchaseOrderFileUpload");
					uploadFile.deletePOFileUpload(uploadOid);

				} else
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_UPLOAD_DELETE_FILE);
			} catch (Exception e) {
				System.out
						.println("Exception occured in DeletePOFileUploadsMediator: "
								+ e.toString());
				e.printStackTrace();
			}
		}

		mediatorServices.debug("DeletePOFileUploadsMediator: " + inputDoc);

		return outputDoc;
	}

}
