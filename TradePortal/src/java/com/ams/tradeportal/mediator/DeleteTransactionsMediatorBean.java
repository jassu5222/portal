package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;
import javax.ejb.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;
/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class DeleteTransactionsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(DeleteTransactionsMediatorBean.class);
  /**
   * This method performs deletions of Transactions
   * The format expected from the input doc is:
   *     
   * <TransactionList>
   *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
   *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
   *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
   *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
   *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
   * </TransactionList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   *
   * The transactionData value consists of an transaction oid, a '/', instrument oid, a '/',
   * and the transaction type.
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      // get each Transaction Oid and process each transaction; each transaction is unique within itself
      DocumentHandler   transaction          = null;
      Instrument        instrument           = null;
      String[]          substitutionParms    = null;
      Vector            transactionsList     = null;
      String            transactionTypeDescr = null;
      String            transactionData      = null;
      String            transactionType      = null;
      String            transactionOid       = null;
      String            securityRights       = null;
      String            instrumentOid        = null;
      String            userLocale           = null;
      String            userOid              = null;
      int               separator1           = 0;
      int               separator2           = 0;
      int               count                = 0;

      // populate variables with inputDocument information such as transaction list, user oid and 
      // security rights, etc
      transactionsList = inputDoc.getFragments("/TransactionList/Transaction");
	  userOid          = inputDoc.getAttribute("/User/userOid");
	  securityRights   = inputDoc.getAttribute("../securityRights");
	  count            = transactionsList.size();

	  // get the locale from client server data bridge, used for getting locale specific 
	  // information when passing error parameters
	  userLocale = mediatorServices.getCSDB().getLocaleName();
	
	  mediatorServices.debug("DeleteTransactionMediator: number of transactions: " + count);

	  if (count <= 0)
	  {
	     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
	                                                   TradePortalConstants.NO_ITEM_SELECTED, 
			mediatorServices.getResourceManager().getText("TransactionAction.Delete",
							   TradePortalConstants.TEXT_BUNDLE));

	     return outputDoc;
	  }

      // a transaction should be locked and processed at the instrument level 
      // since the transaction is a component of instrument
      // for each transaction, lock the instrument and call deleteTransaction
      // on the instrument.  deleteTransaction is a transactional method.
      for (int i = 0; i < count; i++)
      {
         transaction     = (DocumentHandler) transactionsList.get(i);
         transactionData = transaction.getAttribute("/transactionData");

         // Split up the instrument oid, transaction oid and transaction type
         // It is possible to use StringTokenizer to break the data up, but since
         // there are only two separators, this is fine for now.
         separator1      = transactionData.indexOf('/');
         separator2      = transactionData.indexOf('/', separator1 + 1);

         transactionOid  = transactionData.substring(0, separator1);
         instrumentOid   = transactionData.substring(separator1 + 1, separator2);
         transactionType = transactionData.substring(separator2 + 1);

         mediatorServices.debug("DeleteTransactionMediator: transactionData contents: " + transactionData + 
                                "Instrument oid: " + instrumentOid + " Transaction oid: " + transactionOid + 
                                " Transaction Type: " + transactionType);

         // Before creating the instrument, attempt to lock the instrument
         try 
         {
            LockingManager.lockBusinessObject(Long.parseLong(instrumentOid), Long.parseLong(userOid), true);

            mediatorServices.debug("DeleteTransactionMediator: Locking instrument " + instrumentOid + " successful");
         }
         catch (InstrumentLockException e)
         {
            // issue instrument in use error indicating instrument ID, 
            // transaction type, first name and last name of user that has instrument
            // reserved
            transactionTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE", transactionType, 
                                                                                 userLocale);

            substitutionParms = new String[] {e.getInstrumentNumber(), transactionTypeDescr, e.getFirstName(), e.getLastName(), 
		    			       mediatorServices.getResourceManager().getText("TransactionAction.Deleted",
	    	    			       TradePortalConstants.TEXT_BUNDLE)};


            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                          TradePortalConstants.TRANSACTION_IN_USE, substitutionParms);

            continue;
         }

         try
         {
            // create and load the instrument and call deleteTransaction
            instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));

            instrument.deleteTransaction(transactionOid, userOid, securityRights);
         }
	 catch(Exception e)
	 {
	    LOG.info("Exception occured in DeleteTransactionsMediator: " + e.toString());
	    e.printStackTrace();
	 }
         finally
         {
            try
            {
               LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
            }
            catch (InstrumentLockException e)
            {
               // this should never happen
               e.printStackTrace();
            }
         }
      }

      mediatorServices.debug("DeleteTransactionsMediator: " + inputDoc);

      return outputDoc;
   }
}
