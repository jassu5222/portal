package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.agents.MQWrapper;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.rmi.*;
import java.util.Vector;
import javax.ejb.*;
import org.w3c.dom.*;
import java.math.BigDecimal;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class EODTransUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(EODTransUnPackagerMediatorBean.class);

	/*
	 * Manages EOD Transaction UnPackaging..
	 */

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		SessionContext mContext = mediatorServices.mContext;
		boolean unpackage = true;
		int count = 0;
		String accountOid = null;
		try
		{
			EODDailyData eodDailyData = (EODDailyData)  mediatorServices.createServerEJB("EODDailyData");
			DocumentHandler resultSet=null;
			String destinationID =  unpackagerDoc.getAttribute("/Proponix/Header/DestinationID");
			String senderID   =  unpackagerDoc.getAttribute("/Proponix/Header/SenderID");
			String dateSent   =  unpackagerDoc.getAttribute("/Proponix/Header/DateSent");
			String timeSent   =  unpackagerDoc.getAttribute("/Proponix/Header/TimeSent");
			String messageID  =  unpackagerDoc.getAttribute("/Proponix/Header/MessageID");
			String curCode 	  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/CurCode");
			String acctId 	  =  unpackagerDoc.getAttribute("/Proponix/SubHeader/AcctId");
			String branchId   =  unpackagerDoc.getAttribute("/Proponix/SubHeader/BranchId");
			if(destinationID == null || destinationID.equals(""))
			{
				mediatorServices.debug("No Destination ID");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Destination ID", "/Proponix/Header/DestinationID");
				mediatorServices.debug("Debug 1");
				unpackage=false;
			}
			else if(senderID == null || senderID.equals(""))
			{
				mediatorServices.debug("No Sender ID");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Sender ID", "/Proponix/Header/SenderID");
				mediatorServices.debug("Debug 2");
				unpackage=false;
			}
			else if(dateSent == null || dateSent.equals(""))
			{
				mediatorServices.debug("No Date Send");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Date Send", "/Proponix/Header/DateSent");
				mediatorServices.debug("Debug 3");
				unpackage=false;
			}
			else if(timeSent == null || timeSent.equals(""))
			{
				mediatorServices.debug("No Time Send");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Time Send", "/Proponix/Header/TimeSent");
				mediatorServices.debug("Debug 4");
				unpackage=false;
			}
			else if(messageID == null || messageID.equals(""))
			{
				mediatorServices.debug("No Message ID");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Time Send", "/Proponix/Header/MessageID");
				mediatorServices.debug("Debug 5");
				unpackage=false;
			}
			else if(curCode == null || curCode.equals(""))
			{
				mediatorServices.debug("No Currency Code");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Currency Code", "/Proponix/SubHeader/CurCode");
				mediatorServices.debug("Debug 6");
				unpackage=false;
			}
			else if(acctId == null || acctId.equals(""))
			{
				mediatorServices.debug("No Account Id");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Account Id", "/Proponix/Header/AcctId");
				mediatorServices.debug("Debug 7");
				unpackage=false;
			}
			else if(branchId == null || branchId.equals(""))
			{
				mediatorServices.debug("No Branch Identifier");
				eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"Branch Identifier", "/Proponix/Header/BranchId");
				mediatorServices.debug("Debug 8");
				unpackage=false;
			}
			else {
                // G Roy 06/28/2010 IR-LIUK020456367 - Added Deactivated = 'N' to the select statement
				String sql = "select account_oid from account where ";
				sql += "ACCOUNT_NUMBER = ? AND CURRENCY = ? AND SOURCE_SYSTEM_BRANCH = ? AND DEACTIVATE_INDICATOR = 'N'";

				resultSet = DatabaseQueryBean.getXmlResultSet(sql,false, acctId, curCode, branchId);
				mediatorServices.debug("resultSet="+resultSet);
				if (resultSet == null) {
					unpackage=false;
					handleAccountNotFound(unpackagerDoc, outputDoc, mediatorServices,branchId,curCode,acctId);  	// NSX - CR-542 03/16/10
					//eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.EOD_ACCOUNT_NOTFOUND,acctId,curCode,branchId);    	// NSX - CR-542 03/16/10

				} else {
					//CR-610 - Adarsha K S -Start. Changed condition to < 1 and added for loop updating 
					//all the matching account.
					Vector resultList = resultSet.getFragments("/ResultSetRecord");
					//if (resultList.size() !=1) {            // CR-610 - Adarsha K S Delete
					if (resultList.size() < 1) {                 // CR-610 - Adarsha K S Add
						unpackage=false;
						handleAccountNotFound(unpackagerDoc, outputDoc, mediatorServices,branchId,curCode,acctId);    	// NSX - CR-542 03/16/10
						//eodDailyData.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.EOD_ACCOUNT_NOTFOUND,acctId,curCode,branchId);   	// NSX - CR-542 03/16/10
					} 
				}
			}
			String eodDailyDataOid ;
			if(unpackage == true)
			{
				for(int i=0;i<resultSet.getFragments("/ResultSetRecord").size();i++){ // CR-610 - Adarsha K S Add
					eodDailyDataOid=null;	// CR-610 - Adarsha K S Add
					EODDailyData eodDailyData1 = (EODDailyData)  mediatorServices.createServerEJB("EODDailyData"); // CR-610 - Adarsha K S Add
					//accountOid = resultSet.getAttribute("//ResultSetRecord(0)/ACCOUNT_OID"); // CR-610 - Adarsha K S Delete
					accountOid = resultSet.getAttribute("//ResultSetRecord("+i+")/ACCOUNT_OID"); // CR-610 - Adarsha K S Add
					mediatorServices.debug("accountOid="+accountOid);
					mediatorServices.debug("This is unpackagerDoc :" + unpackagerDoc.toString());
					eodDailyDataOid = unpackageEODTransAccountDataAttributes(eodDailyData1,accountOid,unpackagerDoc,mediatorServices);
					if (eodDailyDataOid != null) {
						unpackageEODTransTransactionDataAttributes(unpackagerDoc,mediatorServices,eodDailyDataOid);
					}
				}
				//CR-610 - Adarsha K S -End.
			}
			outputDoc.setAttribute("/Param/DestinationID",destinationID);
			outputDoc.setAttribute("/Param/SenderID",senderID);
			outputDoc.setAttribute("/Param/DateSent",dateSent);
			outputDoc.setAttribute("/Param/TimeSent",timeSent);
			outputDoc.setAttribute("/Param/CurCode",curCode);
			outputDoc.setAttribute("/Param/AcctId",acctId);
			outputDoc.setAttribute("/Param/BranchId",branchId);
		} //end of try

		catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();

		}
		catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();
		}
		catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();
		}

		return outputDoc;
	}

	// NSX - CR-542 03/16/10 - Begin
	/*
	 *  This method routes the message if route queue is defined. Otherwise, it will log error.
	 */
	private void handleAccountNotFound(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices, String branchId, String curCode, String acctId) throws AmsException {
		String routeQueue = getRouteToQueueName();

		if (InstrumentServices.isBlank(routeQueue)) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.EOD_ACCOUNT_NOTFOUND,acctId,curCode,branchId);
			LOG.debug("EOD message routing turned off... ");
		}
		else {
			// put message to routeQueue
			MQWrapper mqWrapper = new MQWrapper();
			int rCode = mqWrapper.sendMessageToQueue(unpackagerDoc.getFragment("/Proponix").toString(), routeQueue);
			if (rCode == 1) {
				outputDoc.setAttribute("/Param/messageRouted","true");
			}
			else {
				//IR 16131 start- if there is an error then set the output params
				outputDoc.setAttribute("/Param/messageRouted","false");
				//outputDoc.setAttribute("/Param/messageRoutedErr","Error:  Could not route EODMessage to queue " + routeQueue);
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.EOD_FAIL_ROUTE);
				//IR 16131 end
				LOG.info("Error:  Could not route EODMessage to queue " + routeQueue);
			}
		}

	}

	/*
	 * Returns MQ queue name to route message
	 */
	private String getRouteToQueueName() {

		try {
			PropertyResourceBundle properties = (PropertyResourceBundle)ResourceBundle.getBundle("AgentConfiguration");
			return properties.getString("EODMessageRouteToQueue");
		}
		catch (MissingResourceException e){
			return null;
		}
	}

	// NSX - CR-542 03/16/10 - End

	/**
	 * This method takes the MailMessage object, instrument_oid, corp_org_oid
	 * and populates the database with the value from the unpackagerDoc
	 * @param mailMessage, MailMessage object whose values are being packaged
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param String instrument_oid
	 * @param String corp_org_oid
	 * @param MediatorServices mediatorServices
	 * author iv
	 */
	private String unpackageEODTransAccountDataAttributes(EODDailyData eodDailyData, String accountOid, DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException
	{
		eodDailyData.newObject();
		String eodDailyDataOid = eodDailyData.getAttribute("eod_daily_data_oid");
		String [] eodTransAttributes = {"balance_date","source","bank_routing_id","statement_number",
				"opening_balance","closing_balance", "total_debit_movement",
				"number_of_debits","total_credit_movement","number_of_credits",
				"debit_interest_rate","credit_interest_rate","overdraft_limit",
				"debit_interest_accrued","credit_interest_accrued","fid_accrued",
				"badt_accrued","next_posting_date"};
		String [] documentValues   = {"BalanceDate","Source","BankRoutingId","StatementNumber","OpeningBalance",
				"ClosingBalance","TotalDebitMovement","NumberOfDebits","TotalCreditMovement","NumberOfCredits",
				"DebitInterestRate","CreditInterestRate","OverdraftLimit","DebitInterestAccrued","CreditInterestAccrued",
				"FidAccrued","BadtAccrued","NextPostingDate"};
		String [] eodTransValues = {"","","","","","","","","","","","","","","","","",""} ;

		int eodTransAttrCounter = 0;
		for( eodTransAttrCounter=0; eodTransAttrCounter<eodTransAttributes.length; eodTransAttrCounter++)
		{
			eodTransValues[eodTransAttrCounter] = unpackagerDoc.getAttribute("/Proponix/Body/EODTransaction/AccountData/"+documentValues[eodTransAttrCounter]);
			if (eodTransAttributes[eodTransAttrCounter].equals("balance_date") ||
				eodTransAttributes[eodTransAttrCounter].equals("next_posting_date")) {
				eodTransValues[eodTransAttrCounter] = convertDateToDateString(convertStringDateToDate(eodTransValues[eodTransAttrCounter]));
			}

		}

		mediatorServices.debug("Step before setAttributes");
		mediatorServices.debug("Attribute " + eodTransAttributes);
		mediatorServices.debug("Values " +eodTransValues);
		eodDailyData.setAttributes(eodTransAttributes, eodTransValues);
		eodDailyData.setAttribute("account_oid",accountOid);
		eodDailyData.setAttribute("date_time_received", convertDateToDateString(convertStringDateToDate(unpackagerDoc.getAttribute("/Proponix/Timestamp/Date"))));
		mediatorServices.debug("Done setting Attributes");
		mediatorServices.debug("Before saving eodDailyData");

         /* Save the object to the database */
		eodDailyData.save();
		return eodDailyDataOid;
	}


	private void unpackageEODTransTransactionDataAttributes(DocumentHandler unpackagerDoc, MediatorServices mediatorServices, String eodDailyDataOid)
		throws RemoteException, AmsException
	{ 	//Vector transData = unpackagerDoc.getAttributes("/Proponix/Body/EODTransaction/TransactionData");
		HashMap eodTransAttributes = new HashMap();
		eodTransAttributes.put("TransactionDate","transaction_date");
		eodTransAttributes.put("TransactionType","transaction_type");
		eodTransAttributes.put("TransactionReference","transaction_reference");
		eodTransAttributes.put("Amount","transaction_amount");
		eodTransAttributes.put("TransactionNarrative","transaction_narrative");
		eodTransAttributes.put("EffectiveDate","effective_date");
		eodTransAttributes.put("TraceId","trace_id");
		eodTransAttributes.put("TransactionCode","transaction_code");
		eodTransAttributes.put("AuxiliaryDomestic","auxiliary_dom");
		eodTransAttributes.put("ExAuxiliaryDomestic","ex_auxiliary_dom");
		eodTransAttributes.put("BaiCode","bai_code");
		eodTransAttributes.put("RemitterName","remitter_name");
		eodTransAttributes.put("LodgmentReference","lodgment_reference");
		eodTransAttributes.put("ShortDescription","short_description");
		eodTransAttributes.put("SequenceNum","sequence_number");
		eodTransAttributes.put("NumberCollectionItems","number_collection_items");

		FullDocumentHandler fullDocHandler = new FullDocumentHandler(unpackagerDoc.toString(), false);
		Node eodTransData =fullDocHandler.getDocumentNode("/Proponix/Body/EODTransaction");
		NodeList eodTransChildData =eodTransData.getChildNodes() ;
		int eodTransChildLength = eodTransChildData.getLength();

		for (int i=0; i<eodTransChildLength; i++) {
			Node eodTransChildNode = eodTransChildData.item(i);
			if (eodTransChildNode.getNodeName().equals("TransactionData")) {
				EODDailyTransaction eodDailyTrans = (EODDailyTransaction)  mediatorServices.createServerEJB("EODDailyTransaction");
				eodDailyTrans.newObject();

				NodeList eodTransChildNodeList = eodTransChildNode.getChildNodes() ;
				for (int j =0; j<eodTransChildNodeList.getLength(); j++) {
					Node childNode = eodTransChildNodeList.item(j);
					if (eodTransAttributes.containsKey(childNode.getNodeName())) {
						if (childNode.getFirstChild()!= null ){
							String nodeValue = (String)childNode.getFirstChild().getNodeValue();
							if (nodeValue != null && (childNode.getNodeName().equals("TransactionDate") ||
								childNode.getNodeName().equals("EffectiveDate"))) {
								nodeValue = convertDateToDateString(convertStringDateToDate(nodeValue));
							}
							eodDailyTrans.setAttribute((String)eodTransAttributes.get(childNode.getNodeName()),nodeValue);
						}
					}
				}
				eodDailyTrans.setAttribute("date_time_received",convertDateToDateString(convertStringDateToDate(unpackagerDoc.getAttribute("/Proponix/Timestamp/Date"))));
				eodDailyTrans.setAttribute("eod_daily_data_oid",eodDailyDataOid);
				eodDailyTrans.save();
			}
		}
	}

    private static Date convertStringDateToDate(String stringDate) throws AmsException{
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            return formatter.parse(stringDate);
        }catch(Exception e){
            throw new AmsException("Error converting date: " + stringDate + " to java.util.Date object. Nested exception: " + e.getMessage());
        }
    }

    private static String convertDateToDateString(java.util.Date dateObject) throws AmsException{
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		return formatter.format(dateObject);
    }
}
