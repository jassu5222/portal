package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.busobj.FXRate;
import com.ams.tradeportal.busobj.util.TradePortalAuditor;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class EditAllFxRatesMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(EditAllFxRatesMediatorBean.class);
   // Business methods
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      DocumentHandler   updateTextDoc   = null;
      DocumentHandler   fxRateDoc       = null;
      FXRate            fxRate          = null;
      Vector            updateTextList  = new Vector();
      Vector            fxRateOidList   = new Vector();
      Vector            fxRateList      = null;
      Vector            updateList      = null;
      String            ownerOrgOid = null; //AAlubala Rel7000 CR610 Renamed to owner
      String            updateText      = null;
      String            userOid         = null;
      long              fxRateOid       = 0;
      int               numberOfFxRates = 0;
      int               numberOfUpdates = 0;

      // Retrieve the list and number of FX rates from the input doc
      updateList = inputDoc.getFragments("/UpdateText");
      fxRateList = inputDoc.getFragments("/FXRate");

      numberOfFxRates = fxRateList.size();

      try
      {
         // First retrieve the oid of the corporate organization to which the FX rates belong; this
         // should be the same for all FX rates here, so we only need to get it once
         ownerOrgOid = inputDoc.getAttribute("/FXRate(0)/owner_org_oid"); //AAlubala Rel7000 CR610 renamed to owner
         userOid         = inputDoc.getAttribute("/User/userOid");

         // For each FX rate being updated, instantiate the appropriate business object and
         // save all attributes
         for (int i = 0; i < numberOfFxRates; i++)
         {
            // Retrieve the next FX rate item
            fxRateDoc     = (DocumentHandler) fxRateList.elementAt(i);
            updateTextDoc = (DocumentHandler) updateList.elementAt(i);

            // Retrieve the unique oid of the FX rate item
            fxRateOid = fxRateDoc.getAttributeLong("/fx_rate_oid");

            fxRateOidList.addElement(String.valueOf(fxRateOid));

            // Get a handle to an instance of the FX rate object
            fxRate = (FXRate) mediatorServices.createServerEJB("FXRate");

            mediatorServices.debug("Mediator: Retrieving FX rate " + fxRateOid);

            fxRate.getData(fxRateOid);

            // Set the optimistic lock attribute to make sure the FX rate hasn't been changed by someone else
            // since it was first retrieved
            fxRate.setAttribute("opt_lock", fxRateDoc.getAttribute("/opt_lock"));

            // Set object attributes equal to input arguments and save
            fxRate.setAttribute("multiply_indicator", fxRateDoc.getAttribute("/multiply_indicator"));

            // Set the new FX rate
            fxRate.setAttribute("rate", fxRateDoc.getAttribute("/rate"));

            //CM IAZ 10/14/08 Begin

            // Set the new FX Buy rate
            fxRate.setAttribute("buy_rate", fxRateDoc.getAttribute("/buy_rate"));

            // Set the new FX Sell rate
            fxRate.setAttribute("sell_rate", fxRateDoc.getAttribute("/sell_rate"));

            //CM IAZ 10/14/08 END
            
			//AAlubala Rel 7.0.0.0 CR 610 03/22/2011
			//fxRate.setAttribute("fx_rate_group", fxRateDoc.getAttribute("/fx_rate_group"));            

            if (fxRate.hasChangesPending())
            {
               // Save the FX rate
               fxRate.save();

               // Get the update text for the audit log and error messages
               updateTextList.addElement(updateTextDoc.getAttribute("/currency_descr"));
            }

            mediatorServices.debug("Mediator: Completed the save of the FX rate object");
         }
      }
      catch (AmsException e)
      {
         LOG.info("AmsException occurred in EditAllFxRatesMediator: " + e.toString());
      }
      catch (RemoteException e)
      {
         LOG.info("RemoteException occurred in EditAllFxRatesMediator: " + e.toString());
      }
      finally
      {
         mediatorServices.debug("Mediator: Result of FX rate update is "
                                + this.isTransSuccess(mediatorServices.getErrorManager()));

         // If all FX rates were updated successfully without errors, issue info messages indicating this
         // and record an audit log message for each
         if (this.isTransSuccess(mediatorServices.getErrorManager()))
         {
            // Only issue errors and update the audit log if an FX rate was modified/updated.
            numberOfUpdates = updateTextList.size();

            for (int j = 0; j < numberOfUpdates; j++)
            {
               updateText = (String) updateTextList.elementAt(j);

               // Save the update record to the ref data audit log
               mediatorServices.debug("Mediator: Creating audit record");

               TradePortalAuditor.createRefDataAudit(mediatorServices,
                                                     fxRate,
                                                     (String) fxRateOidList.elementAt(j),
                                                     updateText,
                                                     ownerOrgOid, //AAlubala Rel7000 CR610 name change to owner
                                                     "U",
                                                     userOid);

               mediatorServices.debug("Mediator: Finished with audit record");

               mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                             TradePortalConstants.UPDATE_SUCCESSFUL,
                                                             updateText);
            }
         }
      }

      return outputDoc;
   }
}
