package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface FavoriteTaskCustomizationMediatorHome extends javax.ejb.EJBHome
{

  public FavoriteTaskCustomizationMediator create()
		throws CreateException, RemoteException;}