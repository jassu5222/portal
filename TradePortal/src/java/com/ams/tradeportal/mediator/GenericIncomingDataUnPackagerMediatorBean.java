package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.ams.tradeportal.busobj.GenericIncomingData;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;

public class GenericIncomingDataUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(GenericIncomingDataUnPackagerMediatorBean.class);
private static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	public DocumentHandler execute(DocumentHandler unpackagerDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		boolean unpackage = true;
		String accountOid = null;
		String sql = null;
		
		


		try {
			DocumentHandler resultSet = null;

	        
			String date_sent = unpackagerDoc
					.getAttribute("/Proponix/Header/DateSent");
			String time_sent = unpackagerDoc
					.getAttribute("/Proponix/Header/TimeSent");
			String message_ID = unpackagerDoc
					.getAttribute("/Proponix/Header/MessageID");
			String currency = unpackagerDoc
					.getAttribute("/Proponix/SubHeader/CurrencyCode");
			boolean isValidCurrency = ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", currency);
			if(isValidCurrency==false){
				mediatorServices.debug("Missing values in  field: <MessageCategory/>");
			}
			String acct_number = unpackagerDoc
					.getAttribute("/Proponix/SubHeader/AcctId");
			String source_system_branch = unpackagerDoc
					.getAttribute("/Proponix/SubHeader/BranchId");
			String message_category = unpackagerDoc
			.getAttribute("/Proponix/SubHeader/MessageCategory");

			SimpleDateFormat otherFormatter = new SimpleDateFormat(
					"MM/dd/yyyy HH:mm:ss");

			Date dateTimeSent = TPDateTimeUtility.convertDateStringToDate(
					date_sent + time_sent, "yyyyMMddHHmm");
			String date_time_sent = null;
			if (null != dateTimeSent) {
				date_time_sent = otherFormatter.format(dateTimeSent);
			}

			Date userDefinedDate = TPDateTimeUtility.convertDateStringToDate(
					unpackagerDoc
							.getAttribute("/Proponix/Body/UserDefinedDate"),
					"yyyyMMdd");
			String user_defined_date = null;
			if (null != userDefinedDate) {
				user_defined_date = otherFormatter.format(userDefinedDate);
			}

			String receipt_date = unpackagerDoc
					.getAttribute("/Proponix/Timestamp/Date");
			String receipt_time = unpackagerDoc
					.getAttribute("/Proponix/Timestamp/Time");

			Date receiptDateTime = TPDateTimeUtility.convertDateStringToDate(
					receipt_date + receipt_time, "yyyyMMddHHmmss");
			String receipt_date_time = null;
			if (null != receiptDateTime) {
				receipt_date_time = otherFormatter.format(receiptDateTime);
			}

			// genericIncDataMessage.setAttribute("receipt_date",
			// unpackagerDoc.getAttribute("/Proponix/Timestamp/Date"));
			// genericIncDataMessage.setAttribute("receipt_time",
			// unpackagerDoc.getAttribute("/Proponix/Timestamp/Time"));

			if (!InstrumentServices.isBlank(unpackagerDoc
					.getAttribute("/Proponix/SubHeader/CustomerAlias"))) {

				if (InstrumentServices.isBlank(message_category)) {
					//mediatorServices.debug("No Message Category");<MessageCategory/>
					mediatorServices.debug("Missing values in  field: <MessageCategory/>");
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.VALUE_MISSING, "Message Category",
							"/Proponix/SubHeader/MessageCategory");
					unpackage = false;
				}

				sql = "select P_OWNER_OID from CUSTOMER_ALIAS where ";
				sql += "ALIAS = ? ";

				resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{unpackagerDoc
						.getAttribute("/Proponix/SubHeader/CustomerAlias")});
				mediatorServices.debug("resultSet=" + resultSet);
				if (resultSet == null) {
					unpackage = false;
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.EOD_ACCOUNT_NOTFOUND,
							acct_number, currency, source_system_branch);
					

				} else {
					// condition to < 1 and added for loop updating
					// all the matching account.
					Vector resultList = resultSet
							.getFragments("/ResultSetRecord");
					if (resultList.size() < 1) {
						unpackage = false;
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_MIDDLEWARE,
								TradePortalConstants.EOD_ACCOUNT_NOTFOUND,
								acct_number, currency, source_system_branch);
					}
				}

			} else {
				if (InstrumentServices.isBlank(message_category)) {
					//mediatorServices.debug("No Message Category");<MessageCategory/>
					mediatorServices.debug("Missing values in  field: <MessageCategory/>");
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.VALUE_MISSING, "Message Category",
							"/Proponix/SubHeader/MessageCategory");
					unpackage = false;
				} else if (InstrumentServices.isBlank(currency)) {
					//mediatorServices.debug("No Currency Code");
					mediatorServices.debug("Missing values in  field: <CurrencyCode/>");
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.VALUE_MISSING,
							"Currency Code", "/Proponix/SubHeader/CurCode");
					unpackage = false;
				} else if (InstrumentServices.isBlank(acct_number)) {
					//mediatorServices.debug("No Account Id");
					mediatorServices.debug("Missing values in  field: <AcctId/>");
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.VALUE_MISSING, "Account Id",
							"/Proponix/Header/AcctId");
					unpackage = false;
				} else if (source_system_branch == null
						|| source_system_branch.equals("")) {
					//mediatorServices.debug("No Branch Identifier");
					mediatorServices.debug("Missing values in  field: <BranchId/>");
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.VALUE_MISSING,
							"Branch Identifier", "/Proponix/Header/BranchId");
					unpackage = false;
				} else {
					sql = "select P_OWNER_OID from account where ";
					sql += "ACCOUNT_NUMBER = ? AND CURRENCY = ? AND SOURCE_SYSTEM_BRANCH = ? ";

					resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, acct_number, currency, source_system_branch);
					mediatorServices.debug("resultSet=" + resultSet);
					if (resultSet == null) {
						unpackage = false;
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_MIDDLEWARE,
								TradePortalConstants.EOD_ACCOUNT_NOTFOUND,
								acct_number, currency, source_system_branch);

					} else {
						// condition to < 1 and added for loop updating
						// all the matching account.
						Vector resultList = resultSet
								.getFragments("/ResultSetRecord");
						if (resultList.size() < 1) {
							unpackage = false;
							mediatorServices
									.getErrorManager()
									.issueError(
											TradePortalConstants.ERR_CAT_MIDDLEWARE,
											TradePortalConstants.EOD_ACCOUNT_NOTFOUND,
											acct_number, currency,
											source_system_branch);
						}
					}
				}
			}
			
			if(ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", currency)==false){
				unpackage=false;
				mediatorServices.debug("Bad value found in CurrencyCode: "+currency);
				
			}
			if(!InstrumentServices.isBlank(unpackagerDoc
					.getAttribute("/Proponix/Body/UserDefinedCurr1"))&& ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", unpackagerDoc
					.getAttribute("/Proponix/Body/UserDefinedCurr1"))==false){
				unpackage=false;
				mediatorServices.debug("Bad value found in UserDefinedCurr1: "+unpackagerDoc
						.getAttribute("/Proponix/Body/UserDefinedCurr1"));
				
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.VALUE_MISSING,
						"userDefinedCurr2", "/Proponix/Body/UserDefinedCurr1");
			}
			if(!InstrumentServices.isBlank(unpackagerDoc
					.getAttribute("/Proponix/Body/UserDefinedCurr2"))&& ReferenceDataManager.getRefDataMgr().checkCode("CURRENCY_CODE", unpackagerDoc
					.getAttribute("/Proponix/Body/UserDefinedCurr2"))==false){
				unpackage=false;
				mediatorServices.debug("Bad value found in UserDefinedCurr2: "+unpackagerDoc
						.getAttribute("/Proponix/Body/UserDefinedCurr2"));
				
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.VALUE_MISSING,
						"userDefinedCurr2", "/Proponix/Body/UserDefinedCurr2");
			}
			

			if (unpackage == true) {
				for (int i = 0; i < resultSet.getFragments("/ResultSetRecord")
						.size(); i++) {
					accountOid = resultSet.getAttribute("//ResultSetRecord("
							+ i + ")/P_OWNER_OID");
					mediatorServices.debug("accountOid=" + accountOid);
					mediatorServices.debug("This is unpackagerDoc :"
							+ unpackagerDoc.toString());

					// updating the table row
					
					GenericIncomingData genericIncDataMessage = (GenericIncomingData) mediatorServices
					.createServerEJB("GenericIncomingData");
					genericIncDataMessage.newObject();

					genericIncDataMessage.setAttribute("corporate_org_oid",accountOid);
					// genericIncDataMessage.setAttribute("date_sent",
					// adate.toString());
					// genericIncDataMessage.setAttribute("time_sent",
					// adate.toString());
					genericIncDataMessage.setAttribute("date_time_sent",date_time_sent);
					genericIncDataMessage.setAttribute("message_category",message_category);
					genericIncDataMessage.setAttribute("currency", currency);
					genericIncDataMessage.setAttribute("acct_number",acct_number);
					genericIncDataMessage.setAttribute("source_system_branch",source_system_branch);
					genericIncDataMessage.setAttribute("customer_alias",unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerAlias"));
					genericIncDataMessage.setAttribute("user_defined_1",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined1"));
					genericIncDataMessage.setAttribute("user_defined_2",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined2"));
					genericIncDataMessage.setAttribute("user_defined_3",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined3"));
					genericIncDataMessage.setAttribute("user_defined_4",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined4"));
					genericIncDataMessage.setAttribute("user_defined_5",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined5"));
					genericIncDataMessage.setAttribute("user_defined_6",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined6"));
					genericIncDataMessage.setAttribute("user_defined_7",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined7"));
					genericIncDataMessage.setAttribute("user_defined_8",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined8"));
					genericIncDataMessage.setAttribute("user_defined_9",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined9"));
					genericIncDataMessage.setAttribute("user_defined_10",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined10"));
					genericIncDataMessage.setAttribute("user_defined_11",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined11"));
					genericIncDataMessage.setAttribute("user_defined_12",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined12"));
					genericIncDataMessage.setAttribute("user_defined_13",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined13"));
					genericIncDataMessage.setAttribute("user_defined_14",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined14"));
					genericIncDataMessage.setAttribute("user_defined_15",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined15"));
					genericIncDataMessage.setAttribute("user_defined_16",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined16"));
					genericIncDataMessage.setAttribute("user_defined_17",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined17"));
					genericIncDataMessage.setAttribute("user_defined_18",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined18"));
					genericIncDataMessage.setAttribute("user_defined_19",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined19"));
					genericIncDataMessage.setAttribute("user_defined_20",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined20"));
					genericIncDataMessage.setAttribute("user_defined_21",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined21"));
					genericIncDataMessage.setAttribute("user_defined_22",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined22"));
					genericIncDataMessage.setAttribute("user_defined_23",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined23"));
					genericIncDataMessage.setAttribute("user_defined_24",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined24"));
					genericIncDataMessage.setAttribute("user_defined_25",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined25"));
					genericIncDataMessage.setAttribute("user_defined_26",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined26"));
					genericIncDataMessage.setAttribute("user_defined_27",unpackagerDoc.getAttribute("/Proponix/Body/UserDefined27"));

					genericIncDataMessage.setAttribute("user_defined_curr1",unpackagerDoc
											.getAttribute("/Proponix/Body/UserDefinedCurr1"));
					genericIncDataMessage.setAttribute("user_defined_amount1",unpackagerDoc
											.getAttribute("/Proponix/Body/UserDefinedAmount1"));
					genericIncDataMessage.setAttribute("user_defined_curr2",unpackagerDoc.getAttribute("/Proponix/Body/UserDefinedCurr2"));
					genericIncDataMessage.setAttribute("user_defined_amount2",unpackagerDoc.getAttribute("/Proponix/Body/UserDefinedAmount2"));
					genericIncDataMessage.setAttribute("user_defined_date",user_defined_date);

					// genericIncDataMessage.setAttribute("receipt_date",
					// unpackagerDoc.getAttribute("/Proponix/Timestamp/Date"));
					// genericIncDataMessage.setAttribute("receipt_time",
					// unpackagerDoc.getAttribute("/Proponix/Timestamp/Time"));

					genericIncDataMessage.setAttribute("receipt_date_time",receipt_date_time);

					genericIncDataMessage.save(true);
					
					genericIncDataMessage.remove();
				}
			}

		} catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Message Unpackaging");

			e.printStackTrace();

		} catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Message Unpackaging");
			e.printStackTrace();

		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Message Unpackaging");
			e.printStackTrace();

		}
		
//		try
//        {
//            // Create a record in the database that this task is being run.
//            // The record of when the task has run is important in the agent's determining when this task should be run
//            PeriodicTaskHistory taskHistory = (PeriodicTaskHistory)mediatorServices.createServerEJB("PeriodicTaskHistory");
//            taskHistory.newObject();
//            taskHistory.setAttribute("task_type", TradePortalConstants.ARC_GEN_INC_DATA_MESSAGE_TASK_TYPE);
//            taskHistory.setAttribute("run_timestamp", formatter.format(GMTUtility.getGMTDateTime(true, false)));
//            taskHistory.save(true);
//        }
//       catch(Exception e)
//        {
//            e.printStackTrace();
//            //return new DocumentHandler();
//        }
//       
//       try
//       {
//           // Create a record in the database that this task is being run.
//           // The record of when the task has run is important in the agent's determining when this task should be run
//           PeriodicTaskHistory taskHistory = (PeriodicTaskHistory)mediatorServices.createServerEJB("PeriodicTaskHistory");
//           taskHistory.newObject();
//           taskHistory.setAttribute("task_type", TradePortalConstants.ARC_EOD_TRANSACTION_MESSAGE_TASK_TYPE);
//           taskHistory.setAttribute("run_timestamp", formatter.format(GMTUtility.getGMTDateTime(true, false)));
//           taskHistory.save(true);
//       }
//      catch(Exception e)
//       {
//           e.printStackTrace();
//           //return new DocumentHandler();
//       }
		
		return outputDoc;

	}

}
