package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;

/*
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class GlobalTradePackagerMediatorBean extends MediatorBean{
private static final Logger LOG = LoggerFactory.getLogger(GlobalTradePackagerMediatorBean.class);

   /*
    * This mediator is now used to package message for TradeBeam instead of
    * GlobalTrade.
    *
    * Take the  transaction OID as input, go to that transaction and extract the
    * TradeBeam message. After populating a few values, return that XML to be
    * placed into a queue to be sent to TradeBeam.
    */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                throws RemoteException, AmsException
    {
       String transactionOid = inputDoc.getAttribute("/transaction_oid");
       String messageID      = inputDoc.getAttribute("/message_id");

       Transaction transaction  = (Transaction)
                      mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));

       String xmlString = transaction.getAttribute("globaltrade_msg");
       outputDoc = new DocumentHandler(xmlString, true, true);

       // Check that the TradeBeam ID is not blank.  If it is, issue an error and don't send the message to TradeBeam
       String gtId = outputDoc.getAttribute("/GTProponix/GTSubHeader/CorpCustomerID");
       if((gtId == null) || gtId.equals(""))
        {
          mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING, "Customer ID", "/GTProponix/GTSubHeader/CorpCustomerID");
        }

       // Get the date and time values to place into the XML
       String timeStamp   = DateTimeUtility.getGMTDateTime(false);
       String dateSent    =  timeStamp;
       String timeSent    =  timeStamp.substring(11,19);

       // Override values in XML and then return it to be placed on the queue
       outputDoc.setAttribute("/GTProponix/GTHeader/MessageType",MessageType.TRADEBEAM);
       outputDoc.setAttribute("/GTProponix/GTHeader/DestinationID",TradePortalConstants.DESTINATION_ID_TRADEBEAM);
       outputDoc.setAttribute("/GTProponix/GTHeader/SenderID",TradePortalConstants.SENDER_ID);    // NSX CR-542 03/04/10
       outputDoc.setAttribute("/GTProponix/GTHeader/DateSent", dateSent);
       outputDoc.setAttribute("/GTProponix/GTHeader/TimeSent",timeSent);
       outputDoc.setAttribute("/GTProponix/GTHeader/MessageID",messageID);

       return outputDoc;
    }
}
