package com.ams.tradeportal.mediator;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

/*
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface GlobalTradePackagerMediatorHome extends EJBHome
{
  public GlobalTradePackagerMediator create()throws RemoteException, AmsException, CreateException;
}

