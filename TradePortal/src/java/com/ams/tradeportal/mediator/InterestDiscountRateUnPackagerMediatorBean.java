package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import com.ams.tradeportal.busobj.InvoiceFinanceAmountQueue;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;

/*
 *     InterestDiscountRateUnPackagerMediatorBean reads input DocumentHandler object and segregates the data from it and
 *     persists the data in INTEREST__DISCOUNT_RATE and INTEREST_DISCOUNT_RATE_GROUP tables respectively.
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InterestDiscountRateUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(InterestDiscountRateUnPackagerMediatorBean.class);

	private static final long serialVersionUID = 1L;

	/**
	 * execute() method performs the reading of IntDiscGroupID from input
	 * document handler and determines whether to continue reading the input
	 * data and do persist or throw error
	 */
	public DocumentHandler execute(DocumentHandler unpackagerDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		LOG.debug("InterestDiscountRateUnPackagerMediatorBean.execute()..mediatorServices..(->)"
				+ unpackagerDoc);

		try {
			// SubHeader
			String intDiscGroupID = unpackagerDoc
					.getAttribute("/Proponix/SubHeader/IntDiscGroupID");

			if (intDiscGroupID != null && intDiscGroupID.length() > 0) {
				try {
					insertDiscountGroupRate(unpackagerDoc, mediatorServices);
				} catch (SQLException sqe) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.TASK_NOT_SUCCESSFUL,
							"Error occured while unpackaging, error: "
									+ sqe.getMessage());

				}
			} else {
				// throw new
				// AmsException("Interest Discount RateGroupId id unavailable");
				mediatorServices
						.getErrorManager()
						.issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
								TradePortalConstants.TASK_NOT_SUCCESSFUL,
								"Interest DiscountRateGroupId is unavailable in inputdoc ");
			}
		} catch (AmsException sqe) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL,
					"Error occured while unpackaging, error: "
							+ sqe.getMessage());
			// throw new AmsException(sqe.getMessage());
		}

		return outputDoc;
	}

	private void updateInvoiceFinanceQueue(DocumentHandler unpackagerDoc,
											long dbIntDiscRateGroupOid, MediatorServices mediatorServices)
													throws RemoteException, AmsException {
		

		// Instantiating the object for generating unique oid for invoice_fin_amount_queue_oid
		ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
		long invFinAmtQueueOid = oid.generateObjectID();   //Generating unique no for queueOID
		//creating a new entry in invoice_fin_amount_queue table for the InvoiceFinanceAmountUpdateAgent to process 
		InvoiceFinanceAmountQueue invoiceFinanceAmountQueue  = 
				(InvoiceFinanceAmountQueue) mediatorServices.createServerEJB("InvoiceFinanceAmountQueue");
        invoiceFinanceAmountQueue.newObject();
		invoiceFinanceAmountQueue.setAttribute("invoice_fin_amount_queue_oid",String.valueOf(invFinAmtQueueOid) );
		invoiceFinanceAmountQueue.setAttribute("creation_timestamp", DateTimeUtility.getCurrentDateTime());
		invoiceFinanceAmountQueue.setAttribute("status", TradePortalConstants.INV_FIN_AMNT_STAT_RECALC_PENDING);
		//invoiceFinanceAmountQueue.setAttribute("agent_id", agentId);
		invoiceFinanceAmountQueue.setAttribute("interest_disc_rate_grp_oid", String.valueOf(dbIntDiscRateGroupOid));
		if (invoiceFinanceAmountQueue.save() != 1){
            //TODO log error.
            LOG.debug("InterestDiscountRateUnPackagerMediatorBean.updateInvoiceFinanceQueue()..mediatorServices..(->)"
                    + unpackagerDoc);
        }
		
		
	}

	/**
	 * This method reads the data from DocumentHandler and inserts body in
	 * INTEREST_DISCOUNT_RATE & INTEREST_DISCOUNT_RATE_GROUP tables
	 * 
	 * @param unpackagerDoc
	 * @throws SQLException
	 * @throws AmsException
	 */
	protected void insertDiscountGroupRate(DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
			throws SQLException, AmsException, RemoteException {

		LOG.debug("insertDiscountGroupRate() -><- ");
		String ownershipLevel = TradePortalConstants.OWNER_BANK;
		String ownershipType = TradePortalConstants.OWNER_TYPE_ADMIN;
		int ownerOrgOid = 0;
		DocumentHandler intRateDoc = null;
		Connection conn = null;
		PreparedStatement groupStmt = null;
		PreparedStatement rateStmt = null;
		PreparedStatement qryStmt = null;
		PreparedStatement rateOidStmt = null;
		PreparedStatement detailStmt = null;
		ResultSet rs = null;
		long intDiscRateGroupOid = 0;
		long dbIntDiscRateGroupOid = 0;
		int optLock = 1;
		long intDiscRateOid = 0;
		int recStatus = 0;
		long owner_OrgOid=0;
		long dbDiscRateGroupOid=0;
		try {

			// Header
			String clientBank = unpackagerDoc.getAttribute("/Proponix/Header/ClientBank");

			// SubHeader
			String intDiscGroupID = unpackagerDoc.getAttribute("/Proponix/SubHeader/IntDiscGroupID");
			String description = unpackagerDoc.getAttribute("/Proponix/SubHeader/Description");
			String shortDescription = unpackagerDoc.getAttribute("/Proponix/SubHeader/ShortDescription");
			Vector intRateVector = unpackagerDoc.getFragments("/Proponix/Body/IntRate");
			
			// Instantiating the object for generating unique oid for interest_disc_rate_group_oid
			ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
			
			conn = DatabaseQueryBean.connect(true);
			//Step 1-> Checking the group id for the given groupname and Client Bank
			rateOidStmt = getQryStmtForInterestDiscRateOid(conn);  
			rateOidStmt.setString(1, intDiscGroupID);
			rateOidStmt.setString(2, clientBank);
			rs = rateOidStmt.executeQuery();
			if (rs != null && rs.next()) { 
				dbIntDiscRateGroupOid = rs.getLong("interest_disc_rate_group_oid");
				owner_OrgOid=rs.getLong("p_owner_org_oid");
				LOG.debug("dbIntDiscRateGroupOid->" + dbIntDiscRateGroupOid);
			}
			// Step 2-> If Group_Id found updating the Group info below
			if (dbIntDiscRateGroupOid != 0) { 
				
				groupStmt=updateStmtForInterestDiscGroup(conn);
				groupStmt.setString(1, description);
				groupStmt.setString(2, shortDescription);
				groupStmt.setString(3, intDiscGroupID);
				groupStmt.setLong(4, owner_OrgOid);
				recStatus=groupStmt.executeUpdate();
				if(recStatus>0){
					LOG.debug("Successfully updated in INTEREST_DISCOUNT_RATE_GROUP for groupid:"+ intDiscGroupID);
				}else {
					LOG.debug("Failed to updated in INTEREST_DISCOUNT_RATE_GROUP for groupid:"+ intDiscGroupID);
				}
				
			} //Ste 3->If no Group found then inserting Fresh Group in INTEREST_DISCOUNT_RATE_GROUP table
			else{
				qryStmt = getQryStmtForCorporateOrgId(conn);
				qryStmt.setString(1, clientBank);
				rs = qryStmt.executeQuery();
				if (rs != null && rs.next()) {
					ownerOrgOid = rs.getInt("organization_oid");
				}
				LOG.debug("ownerOrgOid->" + ownerOrgOid);
				
				groupStmt = getInsStmtForInterestGroup(conn);	
				
				intDiscRateGroupOid = oid.generateObjectID();   //Generating unique no for groupid
				
				groupStmt.setLong(1, intDiscRateGroupOid);
				groupStmt.setString(2, intDiscGroupID);
				groupStmt.setString(3, description);
				groupStmt.setString(4, shortDescription);
				groupStmt.setInt(5, optLock);
				groupStmt.setString(6, ownershipLevel);
				groupStmt.setString(7, ownershipType);
				groupStmt.setLong(8, ownerOrgOid);
				recStatus = groupStmt.executeUpdate();
				if (recStatus > 0) {
					LOG.debug("Succesfully inserted into INTEREST_DISCOUNT_RATE_GROUP table with interest_disc_rate_group_oid as ->"+intDiscRateGroupOid);
				} else {
					LOG.debug("Failed to insert into INTEREST_DISCOUNT_RATE_GROUP table");
				}
			}
			//determining the group oid by below condition
			 dbIntDiscRateGroupOid=(dbIntDiscRateGroupOid>0)?dbIntDiscRateGroupOid:intDiscRateGroupOid;
			// Iterating the Interest Rate for the given Group
			LOG.debug(" <-----Persisting process about to start--->"+dbIntDiscRateGroupOid);
			Iterator itr1 = intRateVector.iterator();
			while (itr1.hasNext()) {
				intRateDoc = (DocumentHandler) itr1.next();
				// Body
				String rateCurrency = intRateDoc.getAttribute("/RateCurrency");
				String rateType = intRateDoc.getAttribute("/RateType");
				String callRate = intRateDoc.getAttribute("/CallRate");
				String rate30Day = intRateDoc.getAttribute("/Rate30Day");
				String rate60Day = intRateDoc.getAttribute("/Rate60Day");
				String rate90Day = intRateDoc.getAttribute("/Rate90Day");
				String rate120Day = intRateDoc.getAttribute("/Rate120Day");
				String rate180Day = intRateDoc.getAttribute("/Rate180Day");
				String rate360Day = intRateDoc.getAttribute("/Rate360Day");
				String numInterestDays = intRateDoc.getAttribute("/NumInterestDays");
				String dateEffective = intRateDoc.getAttribute("/DateEffective");
				Date date = TPDateTimeUtility.convertDateStringToDate(dateEffective, "yyyyMMddHHmm");
				Timestamp time = new Timestamp(date.getTime());				
				//Step 4-> Find the details for the given group in INTEREST_DISCOUNT_RATE table
				LOG.debug(" dbIntDiscRateGroupOid ->"+dbIntDiscRateGroupOid+"rateCurrency:"+rateCurrency+"\trateType"+rateType+"\t:+time:"+time);
				detailStmt=getQryStmtForInterestDiscRateDetails(conn);
				detailStmt.setLong(1, dbIntDiscRateGroupOid);
				detailStmt.setString(2,rateCurrency);
				detailStmt.setString(3, rateType);
				rs=detailStmt.executeQuery();
				if(rs!=null && rs.next()){
					dbDiscRateGroupOid=rs.getLong("p_interest_disc_rate_group_oid");				   
				}
				LOG.debug("Checking child record exist or not for the given group (dbDiscRateGroupOid) ->"+dbDiscRateGroupOid+"\t:+time:"+time);
				//Step 5-> If details found in INTEREST_DISCOUNT_RATE updating from msg
				if(dbDiscRateGroupOid!=0){
						rateStmt=updateStmtForInterestDiscRate(conn);
						rateStmt.setDouble(1, Double.parseDouble(callRate));
						rateStmt.setDouble(2, Double.parseDouble(rate30Day));
						rateStmt.setDouble(3, Double.parseDouble(rate60Day));
						rateStmt.setDouble(4, Double.parseDouble(rate90Day));
						rateStmt.setDouble(5, Double.parseDouble(rate120Day));
						rateStmt.setDouble(6, Double.parseDouble(rate180Day));
						rateStmt.setDouble(7, Double.parseDouble(rate360Day));
						rateStmt.setInt(8, Integer.parseInt(numInterestDays));
						rateStmt.setTimestamp(9, time);
						rateStmt.setLong(10, dbDiscRateGroupOid);
						rateStmt.setString(11,rateCurrency);
						rateStmt.setString(12, rateType);
						int updateStatus=rateStmt.executeUpdate();
						LOG.debug("Updated InsertDiscountRate Details for group:"+dbDiscRateGroupOid+" is updateStatus:"+updateStatus);
				}//Step 6-> If not found then inserting fresh details for the given GROUP In INTEREST_DISCOUNT_RATE table
				else{
					LOG.debug("Not found the group in db so insert Operation will be soon.");
					intDiscRateOid = oid.generateObjectID();
					rateStmt = getInstStmtForInterestRate(conn);
					rateStmt.setLong(1, intDiscRateOid);
					rateStmt.setString(2, rateCurrency);
					rateStmt.setString(3, rateType);
					rateStmt.setDouble(4, Double.parseDouble(callRate));
					rateStmt.setDouble(5, Double.parseDouble(rate30Day));
					rateStmt.setDouble(6, Double.parseDouble(rate60Day));
					rateStmt.setDouble(7, Double.parseDouble(rate90Day));
					rateStmt.setDouble(8, Double.parseDouble(rate120Day));
					rateStmt.setDouble(9, Double.parseDouble(rate180Day));
					rateStmt.setDouble(10, Double.parseDouble(rate360Day));
					rateStmt.setInt(11, Integer.parseInt(numInterestDays));
					rateStmt.setTimestamp(12, time);
					rateStmt.setInt(13, optLock);
					rateStmt.setLong(14, dbIntDiscRateGroupOid);
					recStatus=0;
					recStatus = rateStmt.executeUpdate();
					if (recStatus > 0) {
						LOG.debug("Succesfully inserted into INTEREST_DISCOUNT_RATE table with group id:"+dbIntDiscRateGroupOid);
					} else {
						LOG.debug("Failed to  insert into INTEREST_DISCOUNT_RATE table");
					}
				}
				dbDiscRateGroupOid=0;
				try {
					if (rs != null && !rs.isClosed()) {
						rs.close();
					}if (rateStmt != null) {
						rateStmt.close();
					}if(detailStmt!=null){
						detailStmt.close();
					}
				} catch (SQLException e) {
					throw new AmsException(e.getMessage());
				}
			}

		} catch (SQLException sqe) {
			throw new AmsException(sqe.getMessage());
		}

		finally {
			try {
				if (rs != null) {
					rs.close();
				}if (groupStmt != null) {
					groupStmt.close();
				}if (rateStmt != null) {
					rateStmt.close();
				}if (qryStmt != null) {
					qryStmt.close();
				}if(rateOidStmt!=null){
					rateOidStmt.close();
				}if(detailStmt!=null){
					detailStmt.close();
				}if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				throw new AmsException(e.getMessage());
			}
		}

		//calling method to update invoice method to recalculate interest/discount for invoices
		updateInvoiceFinanceQueue(unpackagerDoc, dbIntDiscRateGroupOid, mediatorServices);
	}

	//Insert statement for INSERT_DISCOUNT_RATE_GROUP table
	protected PreparedStatement getInsStmtForInterestGroup(Connection con)
			throws SQLException, AmsException {

		return con
				.prepareStatement("insert into interest_discount_rate_group (INTEREST_DISC_RATE_GROUP_OID,GROUP_ID,DESCRIPTION,SHORT_DESCRIPTION,OPT_LOCK,OWNERSHIP_LEVEL,"
						+ "OWNERSHIP_TYPE,P_OWNER_ORG_OID) values(?,?,?,?,?,?,?,?)");
	}
	//Insert statement for INSERT_DISCOUNT_RATE table
	protected PreparedStatement getInstStmtForInterestRate(Connection con)
			throws SQLException, AmsException {
		return con
				.prepareStatement("insert into INTEREST_DISCOUNT_RATE (INTEREST_DISCOUNT_RATE_OID,CURRENCY_CODE,RATE_TYPE,CALL_RATE,RATE_30_DAYS,RATE_60_DAYS,RATE_90_DAYS,RATE_120_DAYS,"
						+ "RATE_180_DAYS,RATE_360_DAYS,NUM_INTEREST_DAYS,EFFECTIVE_DATE,OPT_LOCK,P_INTEREST_DISC_RATE_GROUP_OID)"
						+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	}
	//SQL statement for getting organization_oid table
	protected PreparedStatement getQryStmtForCorporateOrgId(Connection con)
			throws SQLException, AmsException {
		return con
				.prepareStatement("select organization_oid from client_bank where otl_id=?");
	}
	//Select statement for INSERT_DISCOUNT_RATE table
	protected PreparedStatement getQryStmtForInterestDiscRateOid(Connection con)
			throws SQLException, AmsException {
		return con.prepareStatement("select interest_disc_rate_group_oid,p_owner_org_oid from interest_discount_rate_group where group_id=? and p_owner_org_oid = (select organization_oid from client_bank where otl_id=?)");
		//select interest_disc_rate_group_oid from interest_discount_rate_group where GROUP_ID=?
		
	}
	//Update statement for INSERT_DISCOUNT_RATE_GROUP table
	protected PreparedStatement updateStmtForInterestDiscGroup(Connection con)
			throws SQLException, AmsException {

		return con.prepareStatement(" update interest_discount_rate_group set description=?,short_description=? " +
						"where group_id=? and p_owner_org_oid=? ");
	}
	//Select statement for INSERT_DISCOUNT_RATE_GROUP table to get group for the given conditions
	protected PreparedStatement getQryStmtForInterestDiscRateDetails(Connection con)
			throws SQLException, AmsException {
		return con.prepareStatement(" select p_interest_disc_rate_group_oid,interest_discount_rate_oid,currency_code,rate_type from interest_discount_rate " +
				"where p_interest_disc_rate_group_oid=? and currency_code=? and rate_type=?");
		
	}
	//Update statement for Insert statement for INSERT_DISCOUNT_RATE table
	protected PreparedStatement updateStmtForInterestDiscRate(Connection con)
			throws SQLException, AmsException {

		return con.prepareStatement(" update interest_discount_rate set CALL_RATE=?,RATE_30_DAYS=?,RATE_60_DAYS=?,RATE_90_DAYS=?,RATE_120_DAYS=?,RATE_180_DAYS=?,RATE_360_DAYS=?,NUM_INTEREST_DAYS=?,EFFECTIVE_DATE=? " +
						"where p_interest_disc_rate_group_oid=? and currency_code=? and rate_type=? ");
	}
	
}
