/**
 * 
 */
package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.InvoiceCRNoteEmailNotification;
import com.ams.tradeportal.busobj.PeriodicTaskHistory;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/**

 *
 */
public class InvoiceCRNoteApprovalEmailMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceCRNoteApprovalEmailMediatorBean.class);
	 private static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");


	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

		CorporateOrganization corpOrg = null;
		InvoiceCRNoteEmailNotification invCRNEmailNotification = null;
		String invCRNEmailOid = null;
		int emailsSent = 0;
		String corpOrgOid = null;
		String invCrnCount = null;
		String errorMessage = null;
		int invCrnAwaitingCnt = 0;
		String taskType = null;
		StringBuilder sql = null;
		String emailSentInd = "";
		String nextEmailDue = "";
		String emailFreq =null;
		String notifRuleOid = null;
		String notifTransType = null;

		try
		{
			if(inputDoc != null){ 
				corpOrgOid =    inputDoc.getAttribute("corpOrgOid");		
				invCRNEmailOid =    inputDoc.getAttribute("invCRNEmailOid");
				emailSentInd =    inputDoc.getAttribute("emailSentInd");
				invCrnCount =  inputDoc.getAttribute("invCRNCount");
				taskType = inputDoc.getAttribute("taskType");
				nextEmailDue = inputDoc.getAttribute("nextEmailDue");
			}
			LOG.info("Starting run of Invoice CRNote Email process...");

			String  emailDueinGMT =  null;
			String instrumentOid = null;
			String transactionOid = null;
			List<Object>sqlParams = new ArrayList<Object>();
			String tableName = "";
			String instrType = TradePortalConstants.PAYABLES_MGMT;
			LOG.info("Starting run of Invoice CRNote email process for approval  OrgId: "+corpOrgOid+" with invCRNEmailOid: "+invCRNEmailOid+"emailSentInd["+emailSentInd+"]taskType["+taskType+"]nextEmailDue["+nextEmailDue);
			corpOrg	= (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
			notifRuleOid = corpOrg.fetchNotificationRule();
            		
			if(TradePortalConstants.PCNA_EMAIL_TASK_TYPE.equals(taskType)){
				tableName = "credit_notes";
				notifTransType = TradePortalConstants.PCN_TRANS_TYPE;
			}else if (TradePortalConstants.PINA_EMAIL_TASK_TYPE.equals(taskType)){
				tableName =  "invoices_summary_data";
				notifTransType = TradePortalConstants.PIN_TRANS_TYPE;
			}else if (TradePortalConstants.RINA_EMAIL_TASK_TYPE.equals(taskType)){
				tableName =  "invoices_summary_data";
				instrType =TradePortalConstants.REC;
				notifTransType = TradePortalConstants.RIN_TRANS_TYPE;
			}
			//Rel9.5 CR-927B Fetch email frequency from NotificationRule
			emailFreq = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid, TradePortalConstants.HTWOH_INV_INST_TYPE, notifTransType, TradePortalConstants.NOTIF_RULE_EMAIL_FREQ);
			
			String updateSQL = "UPDATE  "+ tableName+" SET " +emailSentInd +" = ? where  A_CORP_ORG_OID = ?  and linked_to_instrument_type = ? and PORTAL_APPROVAL_REQ_IND = ? and "+emailSentInd +" <> ?";

			LOG.info("updateSQL["+ updateSQL+"]");

			invCrnAwaitingCnt  = DatabaseQueryBean.executeUpdate(updateSQL, false, new Object[]{TradePortalConstants.INDICATOR_YES,
					corpOrgOid,instrType,TradePortalConstants.INDICATOR_YES,TradePortalConstants.INDICATOR_YES});
			LOG.info("Updated email_sent_indicator to YES for ["+invCrnAwaitingCnt+"] records. ");

			corpOrg.createEmailMessage(instrumentOid, transactionOid,  String.valueOf(invCrnAwaitingCnt), taskType, TradePortalConstants.EMAIL_TRIGGER_INVOICE_CRN );
			emailsSent++;

			invCRNEmailNotification = (InvoiceCRNoteEmailNotification) mediatorServices.createServerEJB("InvoiceCRNoteEmailNotification",Long.parseLong(invCRNEmailOid));

			if(StringFunction.isNotBlank(emailFreq)){
				emailDueinGMT = DateTimeUtility.addMinutesToGMTTime(Integer.parseInt(emailFreq));
				//LOG.info("User given interval:["+emailFreq+"] minutes and nextEmailDue is ["+emailDueinGMT+"]");
			}
			
			invCRNEmailNotification.setAttribute(nextEmailDue, emailDueinGMT);
			invCRNEmailNotification.save();
			outputDoc.setAttribute("result", "SUCCESS");
			

		}catch(Exception e){
			String serverErrorNumber = "SE" + System.currentTimeMillis();
			errorMessage  = "Error occurred while processing email: errorCode: " + serverErrorNumber+ " errorDescription: "+e.getMessage();
			System.err.println(errorMessage + 
					" Server error#" + serverErrorNumber + " " +
					" with error: " + e.toString());
			e.printStackTrace();
			outputDoc.setAttribute("result", "ERROR");
			outputDoc.setAttribute("errorMessage", errorMessage);
		}

		LOG.info("Finised run of InvoiceCRNote Approval email process. Sent "+emailsSent+" e-mails. ");
		return outputDoc;


	}

}
