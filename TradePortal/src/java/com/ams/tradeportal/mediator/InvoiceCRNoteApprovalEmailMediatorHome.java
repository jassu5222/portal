/**
 * 
 */
package com.ams.tradeportal.mediator;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

import com.amsinc.ecsg.frame.AmsException;

/**

 *
 */
public interface InvoiceCRNoteApprovalEmailMediatorHome extends javax.ejb.EJBHome{
	
	public InvoiceCRNoteApprovalEmailMediator create()
			throws CreateException, RemoteException, AmsException;

}
