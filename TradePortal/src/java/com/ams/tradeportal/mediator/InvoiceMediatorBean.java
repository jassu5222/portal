package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.rmi.*;
import java.util.*;
import java.text.*;
import java.math.*;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(InvoiceMediatorBean.class);
  private BigDecimal disputeUndisputeThresholdAmount = BigDecimal.ZERO;
  private BigDecimal closeInvoiceThresholdAmount 	= BigDecimal.ZERO;
  private BigDecimal financeInvoiceThresholdAmount 	= BigDecimal.ZERO;
  private String userOid =  null;
  private String userOrgOid = null;
  private String baseCurrency = null;
  private String clientBankOid = null;
 
  private boolean unlimitedDisputeThreshold = false;
  private boolean unlimitedCloseThreshold = false; 
  private boolean unlimitedFinanceThreshold = false; 
  
  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException {
	// Based on the button clicked, process an appropriate action.
	String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	mediatorServices.debug("The button pressed is " + buttonPressed);

	//get the threshold amounts

	  unlimitedDisputeThreshold = false;
	 unlimitedCloseThreshold   = false;
	 unlimitedFinanceThreshold = false;

	 userOid = inputDoc.getAttribute("/User/userOid");
	  String USER_SQL = "select a_threshold_group_oid, p_owner_org_oid from users where user_oid = ?";

	  DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(USER_SQL, false, new Object[]{userOid});
	  String thresholdGroupOid = resultSet.getAttribute("/ResultSetRecord/A_THRESHOLD_GROUP_OID");


	  //if the user does not have a ThresholdGroup defined, we let the threshold amounts default to zero
    if (StringFunction.isNotBlank(thresholdGroupOid)) {
		ThresholdGroup thresholdGroup = (ThresholdGroup) mediatorServices.createServerEJB("ThresholdGroup", Long.parseLong(thresholdGroupOid));
		// PPRAKSH IR MNUJ030642618 Change Begin
	         String value = thresholdGroup.getThresholdAmount(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, TransactionType.RECEIVABLES_DISPUTE_INVOICE);
	         if (value.equals(""))
	        	 unlimitedDisputeThreshold = true;
	         else
	        	 disputeUndisputeThresholdAmount 	= new BigDecimal(value);

	         value = thresholdGroup.getThresholdAmount(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, TransactionType.RECEIVABLES_CLOSE_INVOICE);
	         if (value.equals(""))
	        	 unlimitedCloseThreshold = true;
	         else
	        	 closeInvoiceThresholdAmount 	= new BigDecimal(value);


	         value = thresholdGroup.getThresholdAmount(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, TransactionType.RECEIVABLES_FINANCE_INVOICE);
	         if (value.equals(""))
	        	 unlimitedFinanceThreshold = true;
	         else
	        	 financeInvoiceThresholdAmount 	= new BigDecimal(value);

	     }
	     else{
	    	 unlimitedDisputeThreshold= true;
	    	 unlimitedCloseThreshold =true;
	    	 unlimitedFinanceThreshold = true;
	     }


    userOrgOid = inputDoc.getAttribute("/User/orgOid");
     CorporateOrganization userCorpOrg = (CorporateOrganization) mediatorServices.createServerEJB(
             "CorporateOrganization", Long.parseLong(userOrgOid));
     baseCurrency = userCorpOrg.getAttribute("base_currency_code");

     clientBankOid = userCorpOrg.getAttribute("client_bank_oid");


	if (buttonPressed.equals(TradePortalConstants.BUTTON_DISPUTEUNDISPUTE_SELECTEDITEMS)) {
		outputDoc = disputeUndisputeSelectedItems(inputDoc, mediatorServices);
	}
    else if (buttonPressed.equals(TradePortalConstants.BUTTON_CLOSE_SELECTED_ITEMS)) {
   	    outputDoc = closeSelectedItems(inputDoc, mediatorServices);
    }
	else if (buttonPressed.equals(TradePortalConstants.BUTTON_FINANCE_SELECTED_ITEMS)) {
		outputDoc = financeSelectedItems(inputDoc, mediatorServices);
    }
	//CR 913 start
	else if (TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE.equals(buttonPressed) ) {
		outputDoc = applyPaymentDate(inputDoc, mediatorServices);
    }
	else if (TradePortalConstants.BUTTON_INV_CLEAR_PAYMENT_DATE.equals(buttonPressed) ) {
		outputDoc = clearPaymentDate(inputDoc, mediatorServices);
    }
	else if ( TradePortalConstants.PAY_INV_BUTTON_MODIFY_SUPP_DATE.equals(buttonPressed)) {
		outputDoc = modifySupplierDate(inputDoc, mediatorServices);
    }
	else if ( TradePortalConstants.PAY_MGMT_BUTTON_RESET_SUPP_DATE.equals(buttonPressed)) {
		outputDoc = resetSupplierDate(inputDoc, mediatorServices);
    }
	else if (TradePortalConstants.PAY_INV_BUTTON_APPLY_PAY_AMT.equals(buttonPressed)) {
		outputDoc = applyPaymentAmount(inputDoc, mediatorServices);
    }
	else if (TradePortalConstants.PAY_MGMT_BUTTON_RESET_PAY_AMOUNT.equals(buttonPressed)) {
		outputDoc = resetPaymentAmount(inputDoc, mediatorServices);
    }
	else if (TradePortalConstants.BUTTON_AUTHORIZE.equals(buttonPressed)) {
		outputDoc = authorizeInvoice(inputDoc, mediatorServices);
    }
	else if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(buttonPressed)){
			outputDoc = performNotifyPanelAuthUser(inputDoc, mediatorServices);
		}
	//CR 913 end
	else { LOG.info("Error in InvoiceMediator - unknown button: " + buttonPressed);
	}

	return outputDoc;
  }

/**
   * This method performs deletions of invoice orders
   * The format expected from the input doc is:
   *
   * <InvoiceList>
   *<PurchaseOrder ID="0"><POLineItemOid>1000001/1200001/AMD</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="1"><POLineItemOid>1000002/1200002/ISS</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="2"><POLineItemOid>1000003/1200003/TRN</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="3"><POLineItemOid>1000004/1200004/ASG</POLineItemOid></PurchaseOrder>
   *<PurchaseOrder ID="6"><POLineItemOid>1000001/1200007/ISS</POLineItemOid></PurchaseOrder>
   * </PurchaseOrderList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   *                 Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   *                 auxillary functionality for a mediator.
   * @return outputDoc A blank document
   */

public DocumentHandler disputeUndisputeSelectedItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {


	// Populate variables with inputDocument information: invoice list,
	// user oid and security rights.
	List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceList/Invoice");
	int count = invoiceList.size();
	//disputed_indicator
	mediatorServices.debug(
		"InvoiceMediator: number of invoices selected: " + count);
	if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.NO_ITEM_SELECTED,
			mediatorServices.getResourceManager().getText(
				"InvoiceAction.DisputeUndisputeSelectedItems",
						TradePortalConstants.TEXT_BUNDLE));
   	}

    // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
    String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
	LOG.debug("InvoiceMediator:: Recert Reqt: " + reCertification);
    if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
            //ir ctuk113042306 11/29/2010 - if an authentication error occurred on a list
            // issue an individual error for each invoice
            issueAuthenticationErrors(invoiceList, mediatorServices);
            return new DocumentHandler();
    }

	// Loop through the list of invoice oids, and processing each one in the list
	Invoice invoice = null;
	String invoiceOid = null;
	String invoiceReferenceNum = null;
	String invoiceStatus = null;
	boolean isDisputed = false;
	for (DocumentHandler invoiceItem : invoiceList) {
		invoiceOid = invoiceItem.getAttribute("/InvoiceOid");

		mediatorServices.debug(
			"InvoiceMediator: invoice item oid to be disputed: " + invoiceOid);

			// create and load the invoice and do the processing
			invoice =
				(Invoice) mediatorServices.createServerEJB(
					"Invoice",
					Long.parseLong(invoiceOid));

		invoiceReferenceNum = invoice.getAttribute("invoice_reference_id");

			BigDecimal invoiceAmount = BigDecimal.ZERO;
			if ((invoice.getAttribute("invoice_total_amount") != null) && (!invoice.getAttribute("invoice_total_amount").equals(""))) {
                String invoiceAmountString = invoice.getAttribute("invoice_total_amount");
                String currency = invoice.getAttribute("currency_code");
                invoiceAmount = FXRateBean.getAmountInBaseCurrency(currency,
                        invoiceAmountString, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());
                if (invoiceAmount.compareTo(new BigDecimal(-1.0f)) == 0) continue;
			}

			invoiceStatus = invoice.getAttribute("invoice_status");
            isDisputed = invoice.getAttribute("disputed_indicator").equals(TradePortalConstants.INDICATOR_YES);
		String financeStatus = invoice.getAttribute("finance_status");

            //disputeUndisputeThresholdAmount = 200;
		boolean transactionSuccess = false;
			if(!isDisputed && (invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED)||
							invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED)||
							financeStatus.equals(TradePortalConstants.INVOICE_REPAID_LIQUIDATE)))
            {
            	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CANT_DISPUTE,invoiceReferenceNum);
            }
			else if (invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED)||
			    		invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVOICE_ALREADY_CLOSED,invoiceReferenceNum);
				}
            else if(financeStatus.equals(TradePortalConstants.INVOICE_STATUS_FINANCE_REQUESTED))
            {
            	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CANT_DISPUTE_INV_WITH_FINREQUESTED,invoiceReferenceNum);
            }

            else if (invoiceAmount.compareTo(disputeUndisputeThresholdAmount) > 0 && !unlimitedDisputeThreshold){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_AMT_GT_DISPUTE_THRESHOLD_AMT,invoiceReferenceNum);
			}
			// if the invoice has a match in progress (i.e., a UOID to the match results table),
			else if (matchProgressCount(invoiceOid) > 0){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DISPUTED_MATCHING_IS_IN_PROGRESS,invoiceReferenceNum);
			} else {
				String corpOrgOid = invoice.getAttribute("corp_org_oid");  // corporate org that the object belongs to.
				String invoiceID = invoice.getAttribute("invoice_reference_id");  // A reference ID for the object.
				StringBuilder processParm =new StringBuilder("status=");
				if (isDisputed){
					invoice.setAttribute("disputed_indicator", TradePortalConstants.INDICATOR_NO);
					// Save the update record to the ref data audit log
					processParm.append(TradePortalConstants.INVOICE_STATUS_UNDISPUTED);
					mediatorServices.debug("Mediator: Creating audit record");
					TradePortalAuditor.createRefDataAudit(mediatorServices,
							invoice,
							invoiceOid,
							invoiceID,
							corpOrgOid,
							TradePortalConstants.CHANGE_TYPE_UNDISPUTE,
							userOid);
					mediatorServices.debug("Mediator: Finished with audit record");
				}
				else if (!isDisputed){
					invoice.setAttribute("disputed_indicator", TradePortalConstants.INDICATOR_YES);
					// Save the update record to the ref data audit log
					processParm.append(TradePortalConstants.INVOICE_STATUS_DISPUTED);
					mediatorServices.debug("Mediator: Creating audit record");
					TradePortalAuditor.createRefDataAudit(mediatorServices,
							invoice,
							invoiceOid,
							invoiceID,
							corpOrgOid,
							TradePortalConstants.CHANGE_TYPE_DISPUTE,
							userOid);
					mediatorServices.debug("Mediator: Finished with audit record");
				}

                invoice.save();

                createOutgoingQueue(invoiceOid,processParm.toString(), mediatorServices,MessageType.INVSTI);
                transactionSuccess = true;


			}

            // W Zhu 1/20/09 PNUI121964617 check transactionSuccess for current invoice instead of checking mediatorServices.getErrorManager()
			if (transactionSuccess) {
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SAVE_SUCCESSFUL,
					invoiceReferenceNum );
			}


	}

	return (new DocumentHandler());
}

public DocumentHandler closeSelectedItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	// Populate variables with inputDocument information: invoice list,
	// user oid and security rights.
	List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceList/Invoice");
	int count = invoiceList.size();

	mediatorServices.debug(
		"InvoiceMediator: number of invoices selected: " + count);
	if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.NO_ITEM_SELECTED,
			mediatorServices.getResourceManager().getText(
				"InvoiceAction.CloseSelectedItems",
				TradePortalConstants.TEXT_BUNDLE));
	}

    // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
    String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
	LOG.debug("InvoiceMediator:: Recert Reqt: " + reCertification);
    if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
            //ir ctuk113042306 11/29/2010 - if an authentication error occurred on a list
            // issue an individual error for each invoice
            issueAuthenticationErrors(invoiceList, mediatorServices);
            return new DocumentHandler();
    }

	// Loop through the list of invoice oids, and processing each one in the list
	Invoice invoice = null;
	String invoiceOid = null;
	String invoiceReferenceNum = null;
	String invoiceStatus = null;
	String financeStatus = null;
	for (DocumentHandler invoiceItem : invoiceList) {

		invoiceOid = invoiceItem.getAttribute("/InvoiceOid");

		mediatorServices.debug(
			"InvoiceMediator: invoice item oid to be closed: " + invoiceOid);

			// create and load the invoice and do the processing
			invoice =
				(Invoice) mediatorServices.createServerEJB(
					"Invoice",
					Long.parseLong(invoiceOid));


		int	matchInProgressCount =  matchProgressCount(invoiceOid);

			invoiceReferenceNum = invoice.getAttribute("invoice_reference_id");
			BigDecimal unpaidAmount = BigDecimal.ZERO;
		
			boolean isDisputed = invoice.getAttribute("disputed_indicator").equals(TradePortalConstants.INDICATOR_YES);
			
			if ((invoice.getAttribute("invoice_outstanding_amount") != null) && (!invoice.getAttribute("invoice_outstanding_amount").equals(""))) {
              
                String unpaidAmountString = invoice.getAttribute("invoice_outstanding_amount");
                String currency = invoice.getAttribute("currency_code");
 
                if(isDisputed){
         
                	unpaidAmount = FXRateBean.getAmountInBaseCurrency(currency,
                            unpaidAmountString, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());
       
                }else{
                	unpaidAmount = (new BigDecimal(unpaidAmountString)).abs();
                }
             
                if (unpaidAmount.compareTo(new BigDecimal(-1.0f)) == 0) continue;
               
			}
			BigDecimal pendingPaymentAmount = BigDecimal.ZERO;
			if ((invoice.getAttribute("pending_payment_amount") != null) && (!invoice.getAttribute("pending_payment_amount").equals(""))) {
                
                String pendingPaymentAmountString = invoice.getAttribute("pending_payment_amount");
                String currency = invoice.getAttribute("currency_code");
             
				if(isDisputed){
					
					pendingPaymentAmount = FXRateBean.getAmountInBaseCurrency(currency,
	                        pendingPaymentAmountString, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());
					
                }else{
                	pendingPaymentAmount = (new BigDecimal(pendingPaymentAmountString)).abs();
                }
				
                if (pendingPaymentAmount.compareTo(new BigDecimal(-1.0f)) == 0) continue;
                
			}

			financeStatus = invoice.getAttribute("finance_status");
            invoiceStatus = invoice.getAttribute("invoice_status");

			String corpOrgOid = invoice.getAttribute("corp_org_oid");  // corporate org that the object belongs to.
			String invoiceID = invoice.getAttribute("invoice_reference_id");  // A reference ID for the object.
			// if the invoice has a UOID to the match results table, then return an error that the invoice cannot be closed because matching is in progress
		boolean transactionSuccess = false;
		if (matchInProgressCount > 0) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.CLOSE_MATCHING_IS_IN_PROGRESS,invoiceReferenceNum);
			}
		    else if (pendingPaymentAmount.compareTo(BigDecimal.ZERO) > 0 ){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_HAS_PENDING_PAYMENT,invoiceReferenceNum);
			}
			else if (financeStatus.equals(TradePortalConstants.INVOICE_STATUS_FINANCE_REQUESTED)||
								financeStatus.equals(TradePortalConstants.INVOICE_APPROVED_FOR_FINANCE)){//PPRAKASH IR MHUJ030364113
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.CANT_CLOSE_FINANCE,invoiceReferenceNum);
			}
		    else if (unpaidAmount.compareTo(closeInvoiceThresholdAmount) > 0  && !unlimitedCloseThreshold){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_AMT_GT_CLOSE_THRESHOLD_AMT,invoiceReferenceNum);
			}
			else if (invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_ALREADY_CLOSED,invoiceReferenceNum);
			}
			else if (invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_ALREADY_CLOSED,invoiceReferenceNum);
			}
            else {
				if (financeStatus.equals(TradePortalConstants.INVOICE_FINANCED)){
					invoiceStatus = TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED;
				}
				else{
					invoiceStatus = TradePortalConstants.INVOICE_STATUS_CLOSED;
				}
				invoice.setAttribute("invoice_status", invoiceStatus);
                invoice.save();
                //CR 913 start- entry log
                InvoiceUtility.createInvoiceHistory(invoiceOid, invoiceStatus, TradePortalConstants.UPLOAD_INV_ACTION_CLOSE, userOid,userOrgOid, mediatorServices);
                
                String processParm ="status="+TradePortalConstants.INVOICE_STATUS_CLOSED;
                createOutgoingQueue(invoiceOid, processParm,mediatorServices,MessageType.INVSTI);

                // Save the update record to the ref data audit log
                mediatorServices.debug("Mediator: Creating audit record");
                TradePortalAuditor.createRefDataAudit(mediatorServices,
	                  invoice,
	                  invoiceOid,
	                  invoiceID,
	                  corpOrgOid,
	                  TradePortalConstants.CHANGE_TYPE_FINANCE,
	                  userOid);
                mediatorServices.debug("Mediator: Finished with audit record");

                transactionSuccess = true;

			}

            // W Zhu 1/20/09 PNUI121964617 check transactionSuccess for current invoice instead of checking mediatorServices.getErrorManager()
			if (transactionSuccess) {
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SAVE_SUCCESSFUL,
					invoiceReferenceNum );
			}




	}

	return (new DocumentHandler());

}

public DocumentHandler financeSelectedItems(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	/*pgedupudi - Rel9.3 CR1006 05/06/2015*/

	// Populate variables with inputDocument information: invoice list,
	// user oid and security rights.
	List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceList/Invoice");
	int count = invoiceList.size();

	mediatorServices.debug(
		"InvoiceMediator: number of invoices selected: " + count);

	if (count <= 0) {
		mediatorServices.getErrorManager().issueError(
			TradePortalConstants.ERR_CAT_1,
			TradePortalConstants.NO_ITEM_SELECTED,
			mediatorServices.getResourceManager().getText(
				"InvoiceAction.FinanceSelectedItems",
				TradePortalConstants.TEXT_BUNDLE));
	}

    // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
    String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
	LOG.debug("InvoiceMediator:: Recert Reqt: " + reCertification);
    if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
            //ir ctuk113042306 11/29/2010 - if an authentication error occurred on a list
            // issue an individual error for each invoice
            issueAuthenticationErrors(invoiceList, mediatorServices);
            return new DocumentHandler();
    }

	// Loop through the list of invoice oids, and processing each one in the list
	Invoice invoice = null;
	String invoiceOid = null;
	String invoiceReferenceNum = null;
	String invoiceStatus = null;
	String invoicePaymentStatus = null;
	String financeStatus = null;
	for (DocumentHandler invoiceItem :invoiceList) {
		invoiceOid = invoiceItem.getAttribute("/InvoiceOid");

		mediatorServices.debug(
			"InvoiceMediator: invoice item oid to be financed: " + invoiceOid);

			// create and load the invoice and do the processing
			invoice =
				(Invoice) mediatorServices.createServerEJB(
					"Invoice",
					Long.parseLong(invoiceOid));

		//IR PNUJ011659126 Commenting the code

			String USER_SQL = " select d.financing_allowed, d.finance_percentage "
								+ "  from invoice a, instrument b, transaction c, terms d"
								+ " where a.invoice_oid = ? "
								+ "   and a.instrument_id = b.complete_instrument_id"
								+ "   and b.active_transaction_oid = c.transaction_oid"
								+ "   and c.c_bank_release_terms_oid = d.terms_oid";
			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(USER_SQL, false, new Object[]{invoiceOid});
			String financingAllowed=null;
			String financePercentage=null;
			if (resultSet!= null){

				financingAllowed = resultSet.getAttribute("//ResultSetRecord(0)/FINANCING_ALLOWED");
				financePercentage = resultSet.getAttribute("//ResultSetRecord(0)/FINANCE_PERCENTAGE");
			}
			BigDecimal unpaidAmount = BigDecimal.ZERO;
        	if ((invoice.getAttribute("invoice_outstanding_amount") != null) && (!invoice.getAttribute("invoice_outstanding_amount").equals(""))) {
               
        		String unpaidAmountString = invoice.getAttribute("invoice_outstanding_amount");
                String currency = invoice.getAttribute("currency_code");
           
                unpaidAmount = FXRateBean.getAmountInBaseCurrency(currency,
                        unpaidAmountString, baseCurrency, userOrgOid, clientBankOid, mediatorServices.getErrorManager());
               
                if (unpaidAmount.compareTo(new BigDecimal(-1.0f)) == 0) continue;
               
			}

        	invoiceReferenceNum = invoice.getAttribute("invoice_reference_id");
            financeStatus = invoice.getAttribute("finance_status");
            invoiceStatus = invoice.getAttribute("invoice_status");
            /*pgedupudi - Rel9.3 CR1006 05/06/2015*/
            invoicePaymentStatus = invoice.getAttribute("invoice_payment_status");

            //validate that the invoice can be financed based on the instrument setting
            //financeInvoiceThresholdAmount = 200;
		boolean transactionSuccess = false;
		    if (financingAllowed == null || financingAllowed.equals("") || financingAllowed.equals("N")){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_CANNOT_BE_FINANCED,invoiceReferenceNum);
			}
		    else if (unpaidAmount.compareTo(financeInvoiceThresholdAmount) > 0 && !unlimitedFinanceThreshold){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_AMT_GT_FINANCE_THRESHOLD_AMT,invoiceReferenceNum);
			}else if(invoice.getAttribute("disputed_indicator").equals(TradePortalConstants.INDICATOR_YES))
			{
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CANT_FINANCE_DISPUTED_INVOICE,invoiceReferenceNum);
			}

		    else if (!financeStatus.equals(TradePortalConstants.INVOICE_NOT_FINANCED)){
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_ALREADY_FINANCED,invoiceReferenceNum);
			}
		    else if (invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED)||
		    		invoiceStatus.equals(TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVOICE_ALREADY_CLOSED,invoiceReferenceNum);
			}
		    /*pgedupudi - Rel9.3 CR1006 05/06/2015 start*/
		    else if(StringFunction.isNotBlank(invoicePaymentStatus) && invoicePaymentStatus.equals(TradePortalConstants.INVOICE_PAYMENT_STATUS_UAP)){
		    	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVOICE_PAYMENT_IS_UNAPPROVED);
		    }
		    /*pgedupudi - Rel9.3 CR1006 05/06/2015 end*/
            else{
				invoice.setAttribute("finance_status", TradePortalConstants.INVOICE_STATUS_FINANCE_REQUESTED);
				// need to set the finance % = default value in RPM Instrument
				if (StringFunction.isBlank(financePercentage) ) {
					invoice.setAttribute("finance_percentage", "0");
				} else {
					invoice.setAttribute("finance_percentage", financePercentage);
				}


				invoice.save();
				String processParm ="status="+TradePortalConstants.INVOICE_STATUS_FINANCE_REQUESTED;
				createOutgoingQueue(invoiceOid,processParm, mediatorServices,MessageType.INVSTI);

				String corpOrgOid = invoice.getAttribute("corp_org_oid");  // corporate org that the object belongs to.
				String invoiceID = invoice.getAttribute("invoice_reference_id");  // A reference ID for the object.
				// Save the update record to the ref data audit log
	 			mediatorServices.debug("Mediator: Creating audit record");
	 			TradePortalAuditor.createRefDataAudit(mediatorServices,
	                  invoice,
	                  invoiceOid,
	                  invoiceID,
	                  corpOrgOid,
	                  TradePortalConstants.CHANGE_TYPE_FINANCE,
	                  userOid);
	 			mediatorServices.debug("Mediator: Finished with audit record");

                transactionSuccess = true;


			}

			if (transactionSuccess) {
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SAVE_SUCCESSFUL,
					invoiceReferenceNum );
			}


	}

	return (new DocumentHandler());

}

private int matchProgressCount(String invoiceOid) throws AmsException{

	String selectClause = "PMR.a_invoice_oid";
	String fromClause   = "PAY_REMIT PR, PAY_MATCH_RESULT PMR";
	String whereClause  = " pmr.p_pay_remit_oid = pr.pay_remit_oid and TRANSACTION_STATUS != ? and  PMR.A_INVOICE_OID= ?";

	int matchInProgressCount = DatabaseQueryBean.getCount(selectClause, fromClause, whereClause, false, new Object[]{TransactionStatus.AUTHORIZED, invoiceOid});
	return matchInProgressCount;
}

public void createOutgoingQueue(String invoiceOid,String process, MediatorServices mediatorServices,String mgsType) throws RemoteException, AmsException{

	OutgoingInterfaceQueue outgoingMessage = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
    outgoingMessage.newObject();
    outgoingMessage.setAttribute("date_created", DateTimeUtility.getGMTDateTime());
    outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
    outgoingMessage.setAttribute("msg_type", mgsType);
	outgoingMessage.setAttribute("message_id", InstrumentServices.getNewMessageID());

   	String processParameters = "invoice_oid=" + invoiceOid + '|' + process;
    outgoingMessage.setAttribute("process_parameters", processParameters);
    outgoingMessage.save();
}

    /**
     * Generate an authentication error for each individual invoice.
     */
    public void issueAuthenticationErrors(
    		List<DocumentHandler> invoiceList, 
            MediatorServices mediatorServices)
            throws AmsException, RemoteException {        
        //generate an error for each transaction in the list
        if (invoiceList != null ) {
            for (Object anInvoiceList : invoiceList) {
                DocumentHandler invoiceDoc = (DocumentHandler) anInvoiceList;
                String invoiceOid = invoiceDoc.getAttribute("/InvoiceOid");
        
                // get the invoice ID
                Invoice invoice =
                    (Invoice) mediatorServices.createServerEJB(
                        "Invoice",
                        Long.parseLong(invoiceOid));
                String invoiceId = invoice.getAttribute("invoice_reference_id"); 

                mediatorServices.getErrorManager().issueError(getClass().getName(),
                    TradePortalConstants.INVOICE_AUTHENTICATION_FAILED,
                    invoiceId );
            }
        }
    }

    
	public DocumentHandler applyPaymentDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		String paymentDate = inputDoc.getAttribute("/InvoiceList/payment_date");
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_PAYMENT_DATE)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.ApplyPaymentDate",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		try{
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String datePattern = user.getAttribute("datepattern");
		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(paymentDate);
		    paymentDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();		
		}
		
		updatePayablesInvoices(inputDoc, mediatorServices,
				"invoice_payment_date", paymentDate,TradePortalConstants.PAY_INV_INFO_APPLY_FIELD_VALUE,
				"AddInvoiceList.payment_date",TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE);
		return (new DocumentHandler());
	}

	public DocumentHandler clearPaymentDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_CLEAR_PAYMENT_DATE)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.ClearPaymentDate",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		updatePayablesInvoices(inputDoc, mediatorServices,
				"invoice_payment_date", "",TradePortalConstants.PAY_INV_INFO_CLEAR_FIELD_VALUE,
				"AddInvoiceList.payment_date",TradePortalConstants.UPLOAD_INV_ACTION_CLEAR_PAYMENT_DATE);
		return (new DocumentHandler());
	}
	
	public DocumentHandler modifySupplierDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String supplierDate = inputDoc.getAttribute("/InvoiceList/supplier_date");
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_SUPPLIER_DATE)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ApplySendtoSuppDate",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		try{
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String datePattern = user.getAttribute("datepattern");
		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(supplierDate);
		    supplierDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();		
		}
		updatePayablesInvoices(inputDoc, mediatorServices,
				"send_to_supplier_date", supplierDate,TradePortalConstants.PAY_INV_INFO_APPLY_FIELD_VALUE,
				"InvoiceUploadRequest.send_to_supplier_date",TradePortalConstants.UPLOAD_INV_ACTION_MODIFY_SUPPLIER_DATE);
		return (new DocumentHandler());
	}
	
	public DocumentHandler resetSupplierDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_RESET_SUPPLIER_DATE)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ResetSendtoSuppDate",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		updatePayablesInvoices(inputDoc, mediatorServices,
				"send_to_supplier_date", "",TradePortalConstants.PAY_INV_INFO_CLEAR_FIELD_VALUE,
				"InvoiceUploadRequest.send_to_supplier_date",TradePortalConstants.UPLOAD_INV_ACTION_RESET_SUPPLIER_DATE);
		return (new DocumentHandler());
	}
	
	public DocumentHandler applyPaymentAmount(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String payAmt = inputDoc.getAttribute("/InvoiceList/payment_amount");
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_APPLY_EARLY_PAY_AMOUNT)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.ApplyEarlyPayAmt",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		updatePayablesInvoices(inputDoc, mediatorServices,
				"invoice_payment_amount", payAmt,TradePortalConstants.PAY_INV_INFO_APPLY_FIELD_VALUE,
				"UploadInvoices.PaymentAmount",TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_AMT);
		return (new DocumentHandler());
	}
	
	public DocumentHandler resetPaymentAmount(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		String payAmt = inputDoc.getAttribute("/InvoiceList/payment_amount");
		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_RESET_EARLY_PAY_AMOUNT)) {
	            // issue error that user does not have 'authorize'
	            // authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ResetEarlyPayAmt",
								TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return new DocumentHandler();

	        }
		updatePayablesInvoices(inputDoc, mediatorServices,
				"invoice_payment_amount", payAmt,TradePortalConstants.PAY_INV_INFO_CLEAR_FIELD_VALUE,
				"UploadInvoices.PaymentAmount",TradePortalConstants.UPLOAD_INV_ACTION_RESET_PAYMENT_AMT);
		return (new DocumentHandler());
	}

    public DocumentHandler updatePayablesInvoices(DocumentHandler inputDoc, MediatorServices mediatorServices,
    		String fieldName,String fieldValue,String errMsg,String label,String invAction)
    	throws RemoteException, AmsException {

		// Populate variables with inputDocument information: invoice list,
    	// user oid and security rights.
    	List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceList/Invoice");
    	
    	// if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
        String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
    	LOG.debug("InvoiceMediator:: Recert Reqt: " + reCertification);
        if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
                && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
                //if an authentication error occurred on a list
                // issue an individual error for each invoice
                issueAuthenticationErrors(invoiceList, mediatorServices);
                return new DocumentHandler();
        }
        // Loop through the list of invoice oids, and processing each one in the list
		Invoice invoice = null;
		String invoiceOid = "";
		String invoiceReferenceNum = "";
		String optLock = "";
		for (DocumentHandler invoiceItem : invoiceList) {
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceOid").split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];
			// create and load the invoice and do the processing
			invoice = (Invoice) mediatorServices.createServerEJB("Invoice",Long.parseLong(invoiceOid));
			invoiceReferenceNum = invoice.getAttribute("invoice_reference_id");
			if (!isPayableInvoiceUpdateable( invoice, mediatorServices)) {
				continue;
			}
			if("send_to_supplier_date".equals(fieldName) && StringFunction.isNotBlank(fieldValue)){
				String payDate = StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_date"))?invoice.getAttribute("invoice_payment_date"):
					invoice.getAttribute("invoice_due_date");
				if(StringFunction.isNotBlank(payDate)){
				//if 'Send to Supplier Date' is greater  than or equal to the earliest of payment date/due date, OR 
				// if it is less than current system date then error
				Date paymentDate = TPDateTimeUtility.convertDateStringToDate(payDate,"MM/dd/yyyy");
				Date supplierDate = TPDateTimeUtility.convertDateStringToDate(fieldValue,"MM/dd/yyyy");
				if(!supplierDate.before(paymentDate)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVALID_SUPPLIER_DATE,
							"", null);
					continue;
				}
				if(supplierDate.before(GMTUtility.getGMTDateTime())){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INVALID_SUPPLIER_DATE_WITH_SYS_DATE,
							"", null);
					continue;
				}
			  }
			}
			//Validate -ve payment amount
			if("invoice_payment_amount".equals(fieldName) && StringFunction.isNotBlank(fieldValue) && fieldValue.startsWith("-")){
			
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_PAYMENT_AMOUNT);
				continue;
					}
			if("invoice_payment_amount".equals(fieldName) && invoice.isCreditNoteApplied()){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.CANNOT_APPLY_PAY_AMT);
				continue;
					
			
			
			}
					invoice.setAttribute(fieldName, fieldValue);
					invoice.setAttribute("opt_lock", optLock);
					
			String status =invoice.getAttribute("invoice_status");
			if (invoice.save() == 1) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,errMsg,
						mediatorServices.getResourceManager().getText(label, TradePortalConstants.TEXT_BUNDLE),invoiceReferenceNum);
				InvoiceUtility.createInvoiceHistory(invoiceOid,status,invAction,userOid,userOrgOid,mediatorServices);
				}
		}

    	return (new DocumentHandler());
    }
   

	public DocumentHandler authorizeInvoice(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		String securityRights = inputDoc.getAttribute("/InvoiceList/securityRights");
		 if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_AUTH) || SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_MGM_OFFLINE_AUTH))) {
	            // issue error that user does not have 'authorize' authority.
			 mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.Authorize",
								TradePortalConstants.TEXT_BUNDLE));

	            return new DocumentHandler();

	        }
		List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceList/Invoice");
			int count = invoiceList.size();
		mediatorServices.debug("InvoiceMediator: number of invoices selected: "
				+ count);
		if (count <= 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.Authorize",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		String subsAuth =inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		User user    = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
  	  
		CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB(
					"CorporateOrganization", Long.parseLong(userOrgOid));
		
		// Loop through the list of invoice oids, and processing each one in the
		// invoice list
		String invoiceOid = null;
		String optLock = null;
		for (DocumentHandler invoiceItem : invoiceList) {
			invoiceOid = invoiceItem.getAttribute("/InvoiceOid");
			String[] invoiceData = invoiceItem.getAttribute("/InvoiceOid").split("/");
			invoiceOid = invoiceData[0];
			optLock = invoiceData[1];
			performPayAuth(corpOrg, invoiceOid,user, optLock,subsAuth, mediatorServices);

		}

		return (new DocumentHandler());
	}
	
    private void performPayAuth(CorporateOrganization corpOrg,String invoiceOid,User user,String optLock,String subsAuth,MediatorServices mediatorServices){
    	try
		{ 
 			Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice", Long.parseLong(invoiceOid));
			String invoiceId      = invoice.getAttribute("invoice_reference_id");
			
			if (!isPayableInvoiceAuthorizable(invoice,mediatorServices)) {
				
				return ; // Move on to next Invoice
			}  
			//if none of payment date/supplier date/payment amt is changed then cant authorise
	    	if(!isInvoiceUpdated(invoice)){
	    		mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
						TradePortalConstants.PAY_MGMT_INVALID_AUTHORISATION,invoice.getAttribute("invoice_reference_id"));	
	    		return ; 
	    	}
			String authorizeRights    =  corpOrg.getAttribute("allow_payables");
			String authorizeType      = corpOrg.getAttribute("dual_auth_payables_inv");
			String userWorkGroupOid         = user.getAttribute("work_group_oid");
			String firstAuthorizer          = invoice.getAttribute("first_authorizing_user_oid");
			String firstAuthorizerWorkGroup = invoice.getAttribute("first_authorizing_work_group_oid");	  	        

			if (!TradePortalConstants.INDICATOR_YES.equals(authorizeRights)) 
			{
				mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
					TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,invoiceId ,
					mediatorServices.getResourceManager().getText("TransactionAction.Authorize", TradePortalConstants.TEXT_BUNDLE));
				return ; // Move on to next Invoice

			} 
			
			// Check threshold amount start
			
			String baseCurrency = corpOrg.getAttribute("base_currency_code");
			String invCurrency = invoice.getAttribute("currency_code");
			String invoiceAmt  = (StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_amount"))
			&& invoice.getAttributeDecimal("invoice_payment_amount").compareTo(BigDecimal.ZERO) != 0) ?
					invoice.getAttribute("invoice_payment_amount"):invoice.getAttribute("invoice_total_amount");
			
			String selectClause = "select distinct invoice_oid, currency_code, case when (invoice_payment_amount is null or invoice_payment_amount =0) " +
					"then invoice_total_amount else invoice_payment_amount end as payment_amount from invoice ";		
			
			boolean isValid = InvoiceUtility.checkThresholds(invoiceAmt,invCurrency , baseCurrency,
					"pyb_mgm_inv_thold", "pyb_mgm_inv_dlimit", userOrgOid,clientBankOid, user ,selectClause, false,subsAuth, mediatorServices);
			if(!isValid){
				return ;
			}
			//threshold check end
			
			// if two different workgroups required authority is selected, then user must belong to a group
			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					&& (StringFunction.isBlank(userWorkGroupOid))) 
			{
				mediatorServices.getErrorManager().issueError(
						PayRemitMediator.class.getName(),
						TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);

				invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
				if (invoice.save() == 1) {
					InvoiceUtility.createInvoiceHistory(invoiceOid,TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED,
							TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices);
				}
			}
			
			else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
					||(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(authorizeType))
			{
				authoriseWithDiffWorkGroup(invoice, authorizeType, optLock, firstAuthorizer, userOid, userWorkGroupOid,
						firstAuthorizerWorkGroup, mediatorServices);
			
			}
			//Panel Authorisation
			else if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType)){
				panelAuth(invoice, authorizeType, optLock, firstAuthorizer, userOid, 
						userOrgOid, userWorkGroupOid, firstAuthorizerWorkGroup, corpOrg, user, mediatorServices);
			}
			else 
			{
				invoice.setAttribute("first_authorizing_user_oid", userOid);
				invoice.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
				invoice.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				invoice.setAttribute("invoice_status",  TradePortalConstants.INVOICE_STATUS_AUTHORISED);
				invoice.setAttribute("send_to_supplier_date_tps", invoice.getAttribute("send_to_supplier_date"));
	 			invoice.setAttribute("invoice_payment_date_tps", invoice.getAttribute("invoice_payment_date"));
	 			invoice.setAttribute("invoice_payment_amount_tps", invoice.getAttribute("invoice_payment_amount"));
				// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
				invoice.setAttribute("opt_lock", optLock);
				int success = invoice.save();
				if(success == 1)
				{   
					InvoiceUtility.createInvoiceHistory(invoiceOid, TradePortalConstants.UPLOAD_INV_STATUS_AUTH, TradePortalConstants.UPLOAD_INV_ACTION_AUTH, userOid,userOrgOid,mediatorServices);
						mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),TradePortalConstants.INV_AUTH_SUCCESSFUL,
						mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
								invoiceId);	
					String processParameters = "TriggerType="+TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE;
					createOutgoingQueue(invoiceOid,processParameters,mediatorServices,MessageType.TRIG); 	}	   
			}
		}       
		catch(Exception e)
		{
			LOG.info("General exception caught in UploadInvoiceMediator:authorizeInvoice");
			e.printStackTrace();
		}
    }
    /*Check if Payable Invoice status is authorizable*/
    private boolean isPayableInvoiceAuthorizable(Invoice invoice,MediatorServices mediatorServices) throws RemoteException, AmsException {
    	boolean isValid = true;
    	String invoiceStatus = invoice.getAttribute("invoice_status");
    	String finStatus = invoice.getAttribute("finance_status");
    	// only 'Assigned','Auth' , 'Auth Failed','non financed' invoices can be authorized
    	//IR 27784 start
    	if(TradePortalConstants.INVOICE_STATUS_CLOSED.equals(invoiceStatus) ||
				TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED.equals(invoiceStatus) ||
				TradePortalConstants.INVOICE_FINANCED.equals(finStatus) ||
				TradePortalConstants.INVOICE_REPAID_LIQUIDATE.equals(finStatus)){
    		mediatorServices.getErrorManager().issueError(
					 TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_FIN_STATUS,
					 ReferenceDataManager.getRefDataMgr().getDescr("INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));
   		    isValid = false;
    	}
    	//IR 27784 end
    	
    	
		return isValid;
	}
    //IR 28017 start
	
    /*Check if Payable Invoice status is authorizable*/
    private boolean isPayableInvoiceUpdateable(Invoice invoice, MediatorServices mediatorServices) throws RemoteException, AmsException {
    	boolean isValid = true;
    	String invoiceStatus = invoice.getAttribute("invoice_status");
    	String finStatus = invoice.getAttribute("finance_status");
    	// only 'Assigned','Auth' , 'Auth Failed','non financed' invoices can be authorized
    	if(TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus) ||
				TradePortalConstants.INVOICE_STATUS_CLOSED.equals(invoiceStatus) ||
				TradePortalConstants.INVOICE_STATUS_CLOSED_AND_FINANCED.equals(invoiceStatus) ||
				TradePortalConstants.INVOICE_FINANCED.equals(finStatus) ||
				TradePortalConstants.INVOICE_REPAID_LIQUIDATE.equals(finStatus)){
    		mediatorServices.getErrorManager().issueError(
					 TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_FIN_STATUS,
					 ReferenceDataManager.getRefDataMgr().getDescr("INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));
    		isValid = false;
		}
    	    	
		return isValid;
	}
    
    private boolean isInvoiceUpdated(Invoice invoice) throws RemoteException, AmsException{
    	String tpsSuppDate = StringFunction.isNotBlank(invoice.getAttribute("send_to_supplier_date_tps"))?invoice.getAttribute("send_to_supplier_date_tps"):"";
    	String tpsPayDate = StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_date_tps"))?invoice.getAttribute("invoice_payment_date_tps"):"";
    	String tpsPayAmt = StringFunction.isNotBlank(invoice.getAttribute("invoice_payment_amount_tps"))?invoice.getAttribute("invoice_payment_amount_tps"):"";
    	
    	boolean suppDateModified =!tpsSuppDate.equals(invoice.getAttribute("send_to_supplier_date"));
    	
    	boolean payAmtModified =!tpsPayAmt.equals(invoice.getAttribute("invoice_payment_amount"));
    	boolean payDateModified=!tpsPayDate.equals(invoice.getAttribute("invoice_payment_date"));
    	if(suppDateModified || payAmtModified || payDateModified) 
    		invoice.setAttribute("send_to_supplier_date_ind", TradePortalConstants.INDICATOR_YES);
    	return suppDateModified || payAmtModified || payDateModified;
    	
    }
    
   /* Panel Auth process for Payables Invoices*/
    private void panelAuth(Invoice invoice,String authorizeType,String optLock, String firstAuthorizer,String userOid,String userOrgOid,
    		String userWorkGroupOid ,String firstAuthorizerWorkGroup,CorporateOrganization corpOrg,User user,
    		MediatorServices mediatorServices)throws RemoteException, AmsException {
		String invoiceId = invoice.getAttribute("invoice_reference_id");
		String invoiceOid = invoice.getAttribute("invoice_oid");
		String panelGroupOid = null;
		String panelRangeOid = null;
		CorporateOrganization transactionOwnerOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
						Long.parseLong(userOrgOid));

		if (StringFunction.isBlank(invoice.getAttribute("panel_auth_group_oid"))) {

			panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(
					TradePortalConstants.PAYABLES_MANAGEMENT_INVOICE, corpOrg,mediatorServices.getErrorManager());
			PanelAuthorizationGroup panelAuthGroup = null;
			if (StringFunction.isNotBlank(panelGroupOid)) {
				panelAuthGroup = (PanelAuthorizationGroup) mediatorServices
						.createServerEJB("PanelAuthorizationGroup",Long.parseLong(panelGroupOid));
			} else {
				// issue error as panel group is required if panel auth defined for instrument type
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						AmsConstants.REQUIRED_ATTRIBUTE,mediatorServices.getResourceManager().getText(
								"CorpCust.PanelGroupId",TradePortalConstants.TEXT_BUNDLE));
				// if panel id is blank, return the flow back.
				return;
			}
			invoice.setAttribute("panel_auth_group_oid",panelAuthGroup.getAttribute("panel_auth_group_oid"));
			invoice.setAttribute("panel_oplock_val",panelAuthGroup.getAttribute("opt_lock"));

			String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(
					transactionOwnerOrg, panelAuthGroup, null,TradePortalConstants.INVOICE);

			BigDecimal amountInBaseToPanelGroup = PanelAuthUtility
					.getAmountInBaseCurrency(invoice.getAttribute("currency_code"),
							invoice.getAttribute("invoice_total_amount"),baseCurrencyToPanelGroup, userOrgOid,
							TradePortalConstants.INVOICE, null, false,mediatorServices.getErrorManager());

			panelRangeOid = PanelAuthUtility.getPanelRange(
					panelAuthGroup.getAttribute("panel_auth_group_oid"),amountInBaseToPanelGroup);
			
			if (StringFunction.isBlank(panelRangeOid)) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
								amountInBaseToPanelGroup.toString(),TradePortalConstants.INVOICE);
				return;
			}
			invoice.setAttribute("panel_auth_range_oid", panelRangeOid);
		}

		   panelGroupOid  = invoice.getAttribute("panel_auth_group_oid");
		String panelOplockVal = invoice.getAttribute("panel_oplock_val");
		   panelRangeOid =  invoice.getAttribute("panel_auth_range_oid");   	
		    
	       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
	       panelAuthProcessor.init(panelGroupOid, new String[] {panelRangeOid}, panelOplockVal);

	       String panelAuthTransactionStatus = panelAuthProcessor.process(invoice, panelRangeOid, false);
	       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
	    	  		invoice.setAttribute("first_authorizing_user_oid", userOid);
					invoice.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
					invoice.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
					invoice.setAttribute("invoice_status", TradePortalConstants.INVOICE_STATUS_AUTHORISED);
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoice.setAttribute("opt_lock", optLock);
					invoice.setAttribute("send_to_supplier_date_tps", invoice.getAttribute("send_to_supplier_date"));
    	 			invoice.setAttribute("invoice_payment_date_tps", invoice.getAttribute("invoice_payment_date"));
    	 			invoice.setAttribute("invoice_payment_amount_tps", invoice.getAttribute("invoice_payment_amount"));
					int success = invoice.save();
					if(success == 1)
					{   
						InvoiceUtility.createInvoiceHistory(invoiceOid, TradePortalConstants.UPLOAD_INV_STATUS_AUTH, TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices,true);
					mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
						TradePortalConstants.INV_AUTH_SUCCESSFUL,mediatorServices.getResourceManager().
						getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invoiceId);	

					String processParameters = "TriggerType="+TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE;
					createOutgoingQueue(invoiceOid,processParameters,mediatorServices,MessageType.TRIG); 
						
					}
	       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
	    	 		invoice.setAttribute("first_authorizing_user_oid", userOid);
					invoice.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
					invoice.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
					
					invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);	             
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoice.setAttribute("opt_lock", optLock);//BSL CR710 02/03/2012 Rel8.0 ADD
						int success = invoice.save();	                   
					if (success == 1)
					{                   
						InvoiceUtility.createInvoiceHistory(invoiceOid, TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH,TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices,true); 
						mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
							TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,mediatorServices.getResourceManager().
							getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invoiceId);
					}
	    	   
          }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
       	   		invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);	
       	      	if (invoice.save() == 1) {
       	      	InvoiceUtility.createInvoiceHistory(invoiceOid, TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED,TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices,true); 
				}
	       } 
    }
    
    /*
	// If dual auth is required and f its not been previously authorized then set the status to partially authorized.
	// set the status to authorized if there has been a previous authorizer.
	// The second authorizer must not be the same as the first authorizer
	// not the same work groups, depending on the corporate org setting.
	// If dual authorization is not required, set the invoice state to authorized.
	*/   
    private void authoriseWithDiffWorkGroup(Invoice invoice,String authorizeType,String optLock, String firstAuthorizer,String userOid , String userWorkGroupOid ,
    		String firstAuthorizerWorkGroup,MediatorServices mediatorServices) throws RemoteException, AmsException
    	{
    		String invoiceId = invoice.getAttribute("invoice_reference_id");
    		String invoiceOid = invoice.getAttribute("invoice_oid");
    		if (StringFunction.isBlank(firstAuthorizer)) 
			{
				invoice.setAttribute("first_authorizing_user_oid", userOid);
				invoice.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
				invoice.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);	             
				// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
				invoice.setAttribute("opt_lock", optLock);
				int success = invoice.save();	                   
				if (success == 1)
				{                   
					InvoiceUtility.createInvoiceHistory(invoiceOid ,TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH ,TradePortalConstants.UPLOAD_INV_ACTION_AUTH, userOid,userOrgOid,mediatorServices); 
						mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
						TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
						mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invoiceId);
				}
			} 
			// if firstAuthorizer is not empty
			else 
			{
				// Two work groups required and this user belongs to the
				// same work group as the first authorizer: Error.
				if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
						&& userWorkGroupOid.equals(firstAuthorizerWorkGroup)) 
				{	
					mediatorServices.getErrorManager().issueError(
						InvoiceMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
					    invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
			
					if (invoice.save() == 1) {
						InvoiceUtility.createInvoiceHistory(invoiceOid,TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED, TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices);
					}
				}
				// Two authorizers required and this user is the same as the
				// first authorizer. Give error.
				// Note requiring two work groups also implies requiring two
				// users.
				else if (firstAuthorizer.equals(userOid)) 
				{
					mediatorServices.getErrorManager().issueError(
					 InvoiceMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
					 invoice.setAttribute("invoice_status", TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
					if (invoice.save() == 1) {
						InvoiceUtility.createInvoiceHistory(invoiceOid,TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED, TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid ,mediatorServices);
					}  
				}
				// Authorize
				else 
				{
					invoice.setAttribute("second_authorizing_user_oid", userOid);
					invoice.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
					invoice.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
					invoice.setAttribute("invoice_status", TradePortalConstants.INVOICE_STATUS_AUTHORISED);
					invoice.setAttribute("send_to_supplier_date_tps", invoice.getAttribute("send_to_supplier_date"));
    	 			invoice.setAttribute("invoice_payment_date_tps", invoice.getAttribute("invoice_payment_date"));
    	 			invoice.setAttribute("invoice_payment_amount_tps", invoice.getAttribute("invoice_payment_amount"));
					// Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
					invoice.setAttribute("opt_lock", optLock);
					int success = invoice.save();
					if(success == 1)
					{
						InvoiceUtility.createInvoiceHistory(invoiceOid,TradePortalConstants.UPLOAD_INV_STATUS_AUTH, TradePortalConstants.UPLOAD_INV_ACTION_AUTH,userOid,userOrgOid, mediatorServices);  
						mediatorServices.getErrorManager().issueError(InvoiceMediator.class.getName(),
							TradePortalConstants.INV_AUTH_SUCCESSFUL,mediatorServices.getResourceManager().
							getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),invoiceId);
						
						String processParameters = "TriggerType="+TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE;
						createOutgoingQueue(invoiceOid,processParameters,mediatorServices,MessageType.TRIG); 
						}
				}
			}
    	}
  
    /**
     * this method is used to send email messages notifying pending panel authorisation
     * @return DocumentHandler
     * @param inputDoc xml doc has transaction data
     * @param MediatorService
     * @throws RemoteException, AmsException
     */
	private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("InvoiceMediatorBean:::performNotifyPanelAuthUser");
		String invoiceOid = inputDoc.getAttribute("/InvoiceSummary/invoiceSummaryOid");
		 
    	 // create email for all selected users to notification
	    List<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragmentsList("/EmailList/user");
	    StringBuilder emailReceivers = new StringBuilder();
	    String userOid = null;
	    String ownerOrgOid = null;
	    DocumentHandler outputDoc = new DocumentHandler();
	    for(DocumentHandler doc : beneficiaryDetailsList){
		    userOid = doc.getAttribute("/userOid");
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String userMailID = user.getAttribute("email_addr");
			ownerOrgOid = user.getAttribute("owner_org_oid");
			if(StringFunction.isNotBlank(userMailID)){
				emailReceivers.append(userMailID).append(',');
			}
	    }
	    if(StringFunction.isNotBlank(emailReceivers.toString())){
	 			// Get the data from transaction that will be used to build the email message
				String amount = "";
				String currency = "";
				if (StringFunction.isNotBlank(invoiceOid)) {
					Invoice invoice =(Invoice) mediatorServices.createServerEJB("Invoice", Long.parseLong(invoiceOid));
					currency = invoice.getAttribute("currency_code");
			        amount = invoice.getAttribute("invoice_payment_amount");
			        amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
				}

				// Build the subject line and content of the email based on the type of email (transaction, mail message)
				StringBuilder emailContent = new StringBuilder();
				 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
				 emailContent.append(TradePortalConstants.NEW_LINE);
				// Include Invoice Amount and currency
				if (StringFunction.isNotBlank(amount)) {
					emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount", TradePortalConstants.TEXT_BUNDLE)).append(' ');
					emailContent.append(currency).append(' ').append(amount);
					emailContent.append(TradePortalConstants.NEW_LINE);
				}

				emailContent.append(TradePortalConstants.NEW_LINE);
				// call generic method to trigger email
			InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
	    }
        
		return outputDoc;

	}
}

