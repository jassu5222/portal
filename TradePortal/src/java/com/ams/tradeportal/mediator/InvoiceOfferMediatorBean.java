package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.SQLException;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;

import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

import java.math.BigDecimal;


/*
 *     Process the requests from the Invoice Offer pages.
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class InvoiceOfferMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceOfferMediatorBean.class);

    /**
     * Called by the processRequest() method of the Ancestor class to process
     * the designated transaction. The input parameters for the transaction are
     * received in an XML document (inputDoc). The execute() method is
     * responsible for calling one or more business objects to process the
     * transaction and populating the outputDoc XML document with the results of
     * the transaction.
     */
    @Override
    public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
            throws RemoteException, AmsException {
        // Based on the button clicked, process an appropriate action.
        String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
        mediatorServices.debug("InvoiceOfferMediatorBean: The button pressed is " + buttonPressed);
        try {
            if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_GROUP_INVOICES)) {
                outputDoc = groupInvoices(inputDoc, mediatorServices);
                return outputDoc;
            } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_ACCEPT_INVOICE_GROUPING)) {
                return acceptInvoiceGrouping(inputDoc, mediatorServices);
            } else if (buttonPressed.equals("AdHocTest")) {
                return adHocTest(inputDoc, mediatorServices);
            } // PMitnala CR 821 Rel 8.3 
            else if (buttonPressed.equals(TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER)) {
                return performNotifyPanelAuthUser(inputDoc, mediatorServices);
            }
        } catch (SQLException e) {
            AmsException newException = new AmsException(e);
            newException.setStackTrace(e.getStackTrace());
            throw newException;
        }
        AbstractInvoiceOfferProcessor processor = getInvoiceOfferProcessor(buttonPressed);
        if (processor == null) {
            throw new AmsException("InvoiceOfferMediatorBean: Invalid button pressed.  ButtonPressed = " + buttonPressed);
        }
        String securityRights = inputDoc.getAttribute("/securityRights");

        if (!processor.checkSecurityRights(securityRights, mediatorServices)) {
            return new DocumentHandler();
        }
        List<DocumentHandler> invoiceOfferGroupList = inputDoc.getFragmentsList("/InvoiceOfferGroupList/InvoiceOfferGroup");
        List<DocumentHandler> invoiceOfferList = inputDoc.getFragmentsList("/InvoiceOfferList/InvoiceOffer");
        try {
            if (invoiceOfferGroupList.size() > 0) {
                processInvoiceOfferGroups(processor, inputDoc, mediatorServices);
            } else if (invoiceOfferList.size() > 0) {
                processInvoiceOffers(processor, inputDoc, mediatorServices);
            } else {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.NO_ITEM_SELECTED,
                        mediatorServices.getResourceManager().getText(
                                processor.getActionLabelForDisplay(),
                                TradePortalConstants.TEXT_BUNDLE));
                return new DocumentHandler();
            }
        } catch (SQLException e) {
            AmsException newException = new AmsException(e);
            newException.setStackTrace(e.getStackTrace());
            throw newException;
        }

        return outputDoc;
    }

    private DocumentHandler adHocTest(DocumentHandler inputDoc, MediatorServices mediatorServices)
            throws AmsException, RemoteException {
        Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice", 101);
        invoice.calculateInvoiceOfferNetAmountOffered();
        invoice.updateAssociatedInvoiceOfferGroup();
        invoice.save();
        return new DocumentHandler();
    }

    private AbstractInvoiceOfferProcessor getInvoiceOfferProcessor(String buttonPressed) {
        if (buttonPressed == null) {
            return null;
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_ASSIGN_FVD)) {
            return new AssignFVDProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_REMOVE_FVD)) {
            return new RemoveFVDProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_ACCEPT_OFFER)) {
            return new AcceptOfferProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_DECLINE_OFFER)) {
            return new DeclineOfferProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_RESET_OFFER)) {
            return new ResetOfferProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_AUTHORIZE_OFFER)) {
            return new AuthorizeOfferProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_ATTACH_DOCUMENT)) {
            return new AttachDocumentProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_DELETE_DOCUMENT)) {
            return new DeleteDocumentProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_REMOVE_INVOICES)) {
            return new RemoveInvoicesProcessor();
        } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_RESET_INVOICES_OFFER)) {
            return new ResetInvoicesOfferProcessor();
        }
        return null;
    }

    /**
     * This method performs the Authorization or Partial Authorization of all
     * selected Invoices.
     */
    private DocumentHandler processInvoiceOfferGroups(AbstractInvoiceOfferProcessor processor, DocumentHandler inputDoc,
            MediatorServices mediatorServices)
            throws RemoteException, AmsException, SQLException {
        mediatorServices.debug("InvoiceOfferMediatorBean.processInvoiceOfferGroups: Start: ");
        String userOid = inputDoc.getAttribute("/userOid");
        String corpOrgOid = inputDoc.getAttribute("/ownerOrg");

        User user = (User) mediatorServices.createServerEJB("User",
                Long.parseLong(userOid));
        CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                Long.parseLong(corpOrgOid));

        List<DocumentHandler> invoiceOfferGroupList = inputDoc.getFragmentsList("/InvoiceOfferGroupList/InvoiceOfferGroup");
        mediatorServices.debug("InvoiceOfferMediatorBean: number of invoice offer groups selected: "
                + invoiceOfferGroupList.size());

        String panelGroupOid = "";
        String panelOptLockVal = "";
        boolean isPanelEnabled = false;
        if (TradePortalConstants.BUTTON_SP_AUTHORIZE_OFFER.equals(inputDoc.getAttribute("/Update/ButtonPressed"))
                && corpOrg.isPanelAuthEnabled(TradePortalConstants.SUPPLIER_PORTAL)) {
            isPanelEnabled = true;
            if (StringFunction.isBlank(panelGroupOid)) {
                panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.SUPPLIER_PORTAL, corpOrg, mediatorServices.getErrorManager());
                if (panelGroupOid != null) {
                    PanelAuthorizationGroup panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
                            Long.parseLong(panelGroupOid));

                    panelOptLockVal = panelAuthGroup.getAttribute("opt_lock");
                } else {
                    mediatorServices.getErrorManager().issueError(
                            getClass().getName(),
                            TradePortalConstants.PANEL_GROUP_NOT_PRESENT);
                    return new DocumentHandler();
                }
            }
        }

        boolean errorFound = false;
        for (DocumentHandler invoiceGroupItem : invoiceOfferGroupList) {
            String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
            String invoiceOfferGroupOid = invoiceData[0];
            String optLock = invoiceData[1];

            InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) mediatorServices.createServerEJB("InvoiceOfferGroup",
                    Long.parseLong(invoiceOfferGroupOid));

            //Set panelGroupOid and panelOprLockKey if panel group enabled and authorizing first user
            if (isPanelEnabled && TradePortalConstants.BUTTON_SP_AUTHORIZE_OFFER.equals(inputDoc.getAttribute("/Update/ButtonPressed"))
                    && TradePortalConstants.SP_STATUS_OFFER_ACCEPTED.equals(invoiceOfferGroup.getAttribute("supplier_portal_invoice_status"))) {
                invoiceOfferGroup.setAttribute("panel_auth_group_oid", panelGroupOid);
                invoiceOfferGroup.setAttribute("panel_oplock_val", panelOptLockVal);

                PanelAuthorizationGroup panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
                        Long.parseLong(panelGroupOid));
                String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.SUPPLIER_PORTAL);
                String currency = invoiceOfferGroup.getAttribute("currency");
                String amount = invoiceOfferGroup.getAttribute("total_amount");

                BigDecimal amountInBaseToPanelGroup = PanelAuthUtility.getAmountInBaseCurrency(currency, amount, baseCurrencyToPanelGroup, corpOrg.getAttribute("organization_oid"),
                        TradePortalConstants.SUPPLIER_PORTAL, null, false, mediatorServices.getErrorManager());

                String rangeOid = PanelAuthUtility.getPanelRange(panelGroupOid, amountInBaseToPanelGroup);
                if (StringFunction.isBlank(rangeOid)) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, amount, TradePortalConstants.SUPPLIER_PORTAL);
                    errorFound = true;
                    continue;
                }
                invoiceOfferGroup.setAttribute("panel_auth_range_oid", rangeOid);
            }

            if (!processor.validateInvoiceOfferGroup(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
                errorFound = true;
                continue;
            }
            if (!processor.validateInvoiceOffers(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
                errorFound = true;
                continue;
            }

            invoiceOfferGroup.setAttribute("opt_lock", optLock);

            if (!processor.processAllInvoiceOffers(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
                errorFound = true;
                continue;
            }
            if (!processor.processInvoiceOfferGroup(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
                errorFound = true;
                continue;
            }
            if (!processor.createLogHistory(invoiceOfferGroup, user, mediatorServices)) {
                errorFound = true;
                continue;
            }
            if (!processor.regroupInvoiceOffers(invoiceOfferGroup, user, inputDoc, mediatorServices)) {
                errorFound = true;
                continue;
            }

            if (invoiceOfferGroup.getAttributeInteger("total_number") <= 0) {
                invoiceOfferGroup.delete();
            } else {
                invoiceOfferGroup.save();
            }
        }

        if (!errorFound) {
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.SAVE_SUCCESSFUL,
                    mediatorServices.getResourceManager().getText("TransactionsMenu.InvoiceOffers", TradePortalConstants.TEXT_BUNDLE)
            );
        }
        return new DocumentHandler();
    }

    private DocumentHandler processInvoiceOffers(AbstractInvoiceOfferProcessor processor, DocumentHandler inputDoc,
            MediatorServices mediatorServices)
            throws RemoteException, AmsException, SQLException {
        mediatorServices.debug("InvoiceOfferMediatorBean.processInvoiceOffers: Start: ");
        String userOid = inputDoc.getAttribute("/userOid");
        String corpOrgOid = inputDoc.getAttribute("/ownerOrg");

        User user = (User) mediatorServices.createServerEJB("User",
                Long.parseLong(userOid));
        CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                Long.parseLong(corpOrgOid));

        String invoiceOfferGroupOid = inputDoc.getAttribute("/invoice_offer_group_oid");
        InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) mediatorServices.createServerEJB("InvoiceOfferGroup",
                Long.parseLong(invoiceOfferGroupOid));
                // CR 821 if panelAuthEnabled and InvoiceOfferGroup Status is either offer accepted or partial auth then no any action allow other
        // than attach and delete document button on invoice list inside invoice group.
        if (corpOrg.isPanelAuthEnabled(TradePortalConstants.SUPPLIER_PORTAL)) {
            String gruopStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (TradePortalConstants.SP_STATUS_OFFER_ACCEPTED.equals(gruopStatus) || TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED.equals(gruopStatus)) {
                String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
                if (!(TradePortalConstants.BUTTON_SP_ATTACH_DOCUMENT.equals(buttonPressed) || TradePortalConstants.BUTTON_SP_DELETE_DOCUMENT.equals(buttonPressed))) {
                    mediatorServices.getErrorManager().issueError(
                            TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.SP_INCORRECT_STATUS);
                    return new DocumentHandler();
                }
            }

        }
        processor.buildInvoiceOfferOids(inputDoc, user, corpOrg, mediatorServices);
        if (!processor.validateInvoiceOfferGroup(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
            return new DocumentHandler();
        }

        if (!processor.validateInvoiceOffers(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
            return new DocumentHandler();
        }

        if (!processor.processAllInvoiceOffers(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
            return new DocumentHandler();
        }
        if (!processor.processInvoiceOfferGroup(invoiceOfferGroup, inputDoc, user, corpOrg, mediatorServices)) {
            return new DocumentHandler();
        }
        if (!processor.createLogHistory(invoiceOfferGroup, user, mediatorServices)) {
            return new DocumentHandler();
        }
        if (!processor.regroupInvoiceOffers(invoiceOfferGroup, user, inputDoc, mediatorServices)) {
            return new DocumentHandler();
        }
        if (invoiceOfferGroup.getAttributeInteger("total_number") <= 0) {
            invoiceOfferGroup.delete();
        } else {
            invoiceOfferGroup.save();
        }

        mediatorServices.getErrorManager().issueError(
                TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.SAVE_SUCCESSFUL,
                mediatorServices.getResourceManager().getText("TransactionsMenu.InvoiceOffers", TradePortalConstants.TEXT_BUNDLE)
        );

        return new DocumentHandler();
    }

    public abstract class AbstractInvoiceOfferProcessor {

        protected String newSPInvoiceStatus;
        protected StringBuilder allSelectedInvoicesOid = new StringBuilder();
        protected StringBuilder allSelectedInvoicesOidOptLock = new StringBuilder();
        protected ArrayList<String> allSelectedInvoicesOids = new ArrayList<>();
        protected ArrayList<String> allSelectedInvoicesOptLocks = new ArrayList<>();
        protected int numberOfSelectedInvoices = 0;
        protected int numberOfUpdatedInvoices = 0;
        protected String action = null;

        public boolean checkSecurityRights(String securityRigh, MediatorServices mediatorServices) throws AmsException {
            return true;
        }

        public abstract String getActionLabelForDisplay();

        public abstract boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException;

        public abstract boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException;

        public abstract boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException;

        public abstract boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException;

        public boolean buildInvoiceOfferOids(DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            List<DocumentHandler> invoiceOfferList = inputDoc.getFragmentsList("/InvoiceOfferList/InvoiceOffer");
            mediatorServices.debug("InvoiceOfferMediatorBean: number of invoice offers selected: "
                    + invoiceOfferList.size());
            for (DocumentHandler invoiceOfferItem : invoiceOfferList) {
                String[] invoiceData = invoiceOfferItem.getAttribute("InvoiceData").split("/");
                String invoiceOfferOid = invoiceData[0];
                String optLock = invoiceData[1];
                if (allSelectedInvoicesOid.length() > 0) {
                    allSelectedInvoicesOid.append(',');
                    allSelectedInvoicesOidOptLock.append(',');
                } else {
//                    allSelectedInvoicesOid.append('(');
                    allSelectedInvoicesOidOptLock.append('(');
                }
                allSelectedInvoicesOid.append(invoiceOfferOid);
                allSelectedInvoicesOidOptLock.append('(').append(invoiceOfferOid).append(',').append(optLock).append(')');
                allSelectedInvoicesOids.add(invoiceOfferOid);
                allSelectedInvoicesOptLocks.add(optLock);
                numberOfSelectedInvoices++;
            }
            if (allSelectedInvoicesOid.length() > 0) {
//                allSelectedInvoicesOid.append(')');
                allSelectedInvoicesOidOptLock.append(')');
            }
            return true;
        }

        protected boolean regroupInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, User user, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
                // Note: implementation class needs to set newFutureValueDate and newSPInvoiceStatus before this is called.
            // Note: this is overriden for AttachDocument and RemoveDocument since they don't need to regroup.

            String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
            String newFutureValueDate = null;
            if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_ASSIGN_FVD)) {
                newFutureValueDate = inputDoc.getAttribute("/future_value_date");
                String datePattern = user.getAttribute("datepattern");
                SimpleDateFormat fmt = new SimpleDateFormat(datePattern);
                SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    Date futureValueDate = fmt.parse(newFutureValueDate);
                    newFutureValueDate = reqformatter.format(futureValueDate);
                } catch (java.text.ParseException ignored) {

                }
            } else if (buttonPressed.equals(TradePortalConstants.BUTTON_SP_REMOVE_FVD)) {
                newFutureValueDate = null;
            } else {
                newFutureValueDate = invoiceOfferGroup.getAttribute("future_value_date");
            }

            if (newSPInvoiceStatus == null || newSPInvoiceStatus.isEmpty()) {
                newSPInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            }

            // Do not regroup named groups, i.e. manually created groups.
            String groupName = invoiceOfferGroup.getAttribute("name");
            if (groupName != null && !groupName.isEmpty()) {
                invoiceOfferGroup.setAttribute("future_value_date", newFutureValueDate);
                invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", newSPInvoiceStatus);
                return true;
            }

                // Nar CR 821 don't regroup if status is authorized or partial authorized. while authorizing user detail is saved in panel
            // auth user object with group id. panel authorization is depend on invoice group amount. so don't regroup once it is either authorized
            // or partial authorized
            if ((invoiceOfferGroup.getAttribute("panel_auth_group_oid") != null) && (TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED.equals(newSPInvoiceStatus)
                    || TradePortalConstants.SP_STATUS_AUTHORIZED.equals(newSPInvoiceStatus)
                    || TradePortalConstants.SP_STATUS_FVD_AUTHORIZED.equals(newSPInvoiceStatus))) {
                invoiceOfferGroup.setAttribute("future_value_date", newFutureValueDate);
                invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", newSPInvoiceStatus);
                return true;
            }

            String invoiceOfferGroupOid = invoiceOfferGroup.getAttribute("invoice_offer_group_oid");
            StringBuilder sql = new StringBuilder();
            List<Object> sqlParams = new ArrayList<Object>();
            sql.append("select invoice_offer_group_oid from invoice_offer_group ")
                    .append(" where currency = ? ")
                    .append(" and due_payment_date = ")
                    .append("to_date(?, 'mm/dd/yyyy')")
                    .append(" and a_corp_org_oid = ? ")
                    .append(" and invoice_offer_group_oid != ? ");
            sqlParams.add(invoiceOfferGroup.getAttribute("currency"));
            sqlParams.add(invoiceOfferGroup.getAttribute("due_payment_date"));
            sqlParams.add(invoiceOfferGroup.getAttribute("corp_org_oid"));
            sqlParams.add(invoiceOfferGroupOid);
            if (newFutureValueDate == null || newFutureValueDate.isEmpty()) {
                sql.append(" and future_value_date is null");
            } else {
                sql.append(" and future_value_date = to_date(?, 'mm/dd/yyyy')");
                sqlParams.add(newFutureValueDate);
            }
            sql.append(" and supplier_portal_invoice_status = ? ");
            sqlParams.add(newSPInvoiceStatus);
            DocumentHandler counterInvoiceOfferGroupDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
            // If we are processing the whole group and it cannot be merged into another group, we can just update the current group.
            if ((allSelectedInvoicesOid.length() == 0 || allSelectedInvoicesOids.size() == invoiceOfferGroup.getAttributeInteger("total_number"))
                    && (counterInvoiceOfferGroupDoc == null || counterInvoiceOfferGroupDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OFFER_GROUP_OID") == null)) {
                invoiceOfferGroup.setAttribute("future_value_date", newFutureValueDate);
                invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", newSPInvoiceStatus);
                return true;
            }

            InvoiceOfferGroup counterInvoiceOfferGroup = (InvoiceOfferGroup) mediatorServices.createServerEJB("InvoiceOfferGroup");
            Long counterInvoiceOfferGroupOid = null;
            if (counterInvoiceOfferGroupDoc != null && counterInvoiceOfferGroupDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OFFER_GROUP_OID") != null) {
                // If there is already an invoice offer group for the new attributes, we will add the invoices to that one.
                counterInvoiceOfferGroupOid = Long.parseLong(counterInvoiceOfferGroupDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OFFER_GROUP_OID"));
                counterInvoiceOfferGroup.getData(counterInvoiceOfferGroupOid);
            } else {
                // If we are processing selected invoice, we will add the updated ones to teh other invoice offer group, and keep the current group for the remaining ones.
                counterInvoiceOfferGroupOid = counterInvoiceOfferGroup.newObject();
                counterInvoiceOfferGroup.setAttribute("future_value_date", newFutureValueDate);
                counterInvoiceOfferGroup.setAttribute("supplier_portal_invoice_status", newSPInvoiceStatus);
                counterInvoiceOfferGroup.setAttribute("due_payment_date", invoiceOfferGroup.getAttribute("due_payment_date"));
                counterInvoiceOfferGroup.setAttribute("corp_org_oid", invoiceOfferGroup.getAttribute("corp_org_oid"));
                counterInvoiceOfferGroup.setAttribute("currency", invoiceOfferGroup.getAttribute("currency"));
                counterInvoiceOfferGroup.setAttribute("attachment_ind", invoiceOfferGroup.getAttribute("attachment_ind"));
                counterInvoiceOfferGroup.setAttribute("total_number", "0");
                counterInvoiceOfferGroup.setAttribute("total_amount", "0");
                counterInvoiceOfferGroup.setAttribute("total_net_amount_offered", "0");
            }

            // Find out how much we need to transfer to the counter Invoice Offer Group.
            int totalNumber;
            BigDecimal totalAmount;
            BigDecimal totalNetAmountOffer;
            if (allSelectedInvoicesOid.length() > 0) {

                sqlParams = new ArrayList<Object>();
                sql = new StringBuilder()
                        .append("select count(*) total_number, sum(invoice_total_amount) total_amount, sum(net_amount_offered) total_net_amount_offered from invoice")
                        .append(" where invoice_oid in (");

                String allSelectedInvoicesOidStr = allSelectedInvoicesOid.toString();
//                    allSelectedInvoicesOidStr.replaceAll("(", "");
//                    allSelectedInvoicesOidStr.replaceAll(")", "");
                sql.append(SQLParamFilter.preparePlaceHolderStrForInClause(allSelectedInvoicesOidStr, sqlParams));
                sql.append(')');

                DocumentHandler allSelectedInvoicesDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
                totalNumber = allSelectedInvoicesDoc.getAttributeInt("/ResultSetRecord(0)/TOTAL_NUMBER");
                totalAmount = allSelectedInvoicesDoc.getAttributeDecimal("/ResultSetRecord(0)/TOTAL_AMOUNT");
                totalNetAmountOffer = allSelectedInvoicesDoc.getAttributeDecimal("/ResultSetRecord(0)/TOTAL_NET_AMOUNT_OFFERED");
            } else {
                totalNumber = invoiceOfferGroup.getAttributeInteger("total_number");
                totalAmount = invoiceOfferGroup.getAttributeDecimal("total_amount");
                totalNetAmountOffer = invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered");
            }
            counterInvoiceOfferGroup.setAttribute("total_number", String.valueOf(counterInvoiceOfferGroup.getAttributeInteger("total_number") + totalNumber));
            counterInvoiceOfferGroup.setAttribute("total_amount", counterInvoiceOfferGroup.getAttributeDecimal("total_amount").add(totalAmount).toPlainString());
            counterInvoiceOfferGroup.setAttribute("total_net_amount_offered", counterInvoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").add(totalNetAmountOffer).toPlainString());
            if (allSelectedInvoicesOid.length() > 0) {
                invoiceOfferGroup.setAttribute("total_number", String.valueOf(invoiceOfferGroup.getAttributeInteger("total_number") - totalNumber));
                invoiceOfferGroup.setAttribute("total_amount", invoiceOfferGroup.getAttributeDecimal("total_amount").subtract(totalAmount).toPlainString());
                invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(totalNetAmountOffer).toPlainString());
            } else {
                invoiceOfferGroup.setAttribute("total_number", "0");
            }

            // Update the invoices to associate with the counter invoice offer group
            sql = new StringBuilder().append("update invoice set a_invoice_offer_group_oid = ? where a_invoice_offer_group_oid = ? ");
            sqlParams = new ArrayList<Object>();
            sqlParams.add(counterInvoiceOfferGroupOid);
            sqlParams.add(invoiceOfferGroupOid);
            if (allSelectedInvoicesOid.length() > 0) {
                sql.append(" and invoice_oid in ( ");

                String allSelectedInvoicesOidStr = allSelectedInvoicesOid.toString();
//                    allSelectedInvoicesOidStr.replaceAll("(", "");
//                    allSelectedInvoicesOidStr.replaceAll(")", "");
                sql.append(SQLParamFilter.preparePlaceHolderStrForInClause(allSelectedInvoicesOidStr, sqlParams));
                sql.append(')');
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sql.toString(), false, sqlParams);
            counterInvoiceOfferGroup.save();

            return true;
        }

        protected boolean createLogHistory(InvoiceOfferGroup invoiceOfferGroup, User user, MediatorServices mediatorServices) throws AmsException, RemoteException {
            if (this.action == null) {
                return true;
            }

            List<Object> sqlParams = new ArrayList<Object>();

            StringBuilder sqlStatement = new StringBuilder()
                    .append("select invoice_oid from invoice where a_invoice_offer_group_oid = ? ");
            sqlParams.add(invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (allSelectedInvoicesOid.length() > 0) {
                sqlStatement.append(" and invoice_oid in (");

                String allSelectedInvoicesOidStr = allSelectedInvoicesOid.toString();
//                    allSelectedInvoicesOidStr.replaceAll("(", "");
//                    allSelectedInvoicesOidStr.replaceAll(")", "");
                sqlStatement.append(SQLParamFilter.preparePlaceHolderStrForInClause(allSelectedInvoicesOidStr, sqlParams));
                sqlStatement.append(')');
            }
            DocumentHandler invoicesDoc = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), false, sqlParams);
            List<DocumentHandler> records = invoicesDoc.getFragmentsList("/ResultSetRecord");
            //String dateCreated = DateTimeUtility.getGMTDateTime(true, false);
            String status = newSPInvoiceStatus;
            if (status == null || status.isEmpty()) {
                status = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            }

            for (DocumentHandler doc : records) {

                if ("SP_AUTHORIZE".equals(this.action) && StringFunction.isNotBlank(invoiceOfferGroup.getAttribute("panel_auth_group_oid"))) {
                    InvoiceUtility.createInvoiceHistory(doc.getAttribute("INVOICE_OID"), status,
                            this.action, user.getAttribute("user_oid"), "", mediatorServices, true);
                } else {
                    InvoiceUtility.createInvoiceHistory(doc.getAttribute("INVOICE_OID"), status,
                            this.action, user.getAttribute("user_oid"), "", mediatorServices);
                }

            }
            return true;
        }

        protected void createOutgoingInterfaceQueue(InvoiceOfferGroup invoiceOfferGroup, MediatorServices mediatorServices) throws AmsException, RemoteException {
            List<Object> sqlParams = new ArrayList<Object>();
            StringBuilder sqlStatement = new StringBuilder()
                    .append("select invoice_oid from invoice where a_invoice_offer_group_oid = ? ");
            sqlParams.add(invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (allSelectedInvoicesOid.length() > 0) {
                sqlStatement.append(" and invoice_oid in( ");
                String allSelectedInvoicesOidStr = allSelectedInvoicesOid.toString();
//                    allSelectedInvoicesOidStr.replaceAll("(", "");
//                    allSelectedInvoicesOidStr.replaceAll(")", "");
                sqlStatement.append(SQLParamFilter.preparePlaceHolderStrForInClause(allSelectedInvoicesOidStr, sqlParams));
                sqlStatement.append(')');
            }
            DocumentHandler invoicesDoc = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), false, sqlParams);
            List<DocumentHandler> records = invoicesDoc.getFragmentsList("/ResultSetRecord");
            String dateCreated;
            String futureValueDate = invoiceOfferGroup.getAttribute("future_value_date");
            if (futureValueDate != null && futureValueDate.length() > 0) {
                dateCreated = futureValueDate + " 00:00:00";
            } else {
                dateCreated = DateTimeUtility.getGMTDateTime();
            }
            for (DocumentHandler doc : records) {
                OutgoingInterfaceQueue outgoingMessage = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
                outgoingMessage.newObject();
                String messageId = InstrumentServices.getNewMessageID();
                outgoingMessage.setAttribute("message_id", messageId);
                outgoingMessage.setAttribute("date_created", dateCreated);
                outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
                outgoingMessage.setAttribute("transaction_oid", doc.getAttribute("/INVOICE_OID"));
                String processParameters = "invoice_oid=" + doc.getAttribute("/INVOICE_OID") + "|TriggerType=FINR";
                outgoingMessage.setAttribute("msg_type", MessageType.TRIG);
                outgoingMessage.setAttribute("process_parameters", processParameters);

                outgoingMessage.save();

            }
        }

    }

    public class AcceptOfferProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.AcceptOffer";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            action = "SP_ACCEPT";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }           
           
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set supplier_portal_invoice_status = ?, opt_lock = opt_lock + 1 where a_invoice_offer_group_oid = ? ");
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, TradePortalConstants.SP_STATUS_OFFER_ACCEPTED, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Let regroup take care of the update if needed
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_OFFER_ACCEPTED;
            return true;
        }

    }

    public class DeclineOfferProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.DeclineOffer";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_DECLINE";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set supplier_portal_invoice_status = ?, opt_lock = opt_lock + 1 where a_invoice_offer_group_oid =  ? ");
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, TradePortalConstants.SP_STATUS_OFFER_DECLINED, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            createOutgoingInterfaceQueue(invoiceOfferGroup, mediatorServices);
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Let regroup take care of the update if needed
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_OFFER_DECLINED;
            return true;
        }

    }

    private class AuthorizeOfferProcessor extends AbstractInvoiceOfferProcessor {

        boolean isFirstAuthorizor = true;
        boolean isPanelEnabled = false;
        private PanelAuthProcessor panelAuthProcessor = null;

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.AuthoriseOffer";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_AUTHORIZE";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            String futureValueDate = invoiceOfferGroup.getAttribute("future_value_date");

            String userWorkGroupOid = user.getAttribute("work_group_oid");
            String authorizeType = corpOrg.getAttribute("dual_auth_supplier_portal");

            // work group is required if the authorize type needs it
            if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                    && StringFunction.isBlank(userWorkGroupOid)) {
                mediatorServices.getErrorManager().issueError(
                        getClass().getName(),
                        TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
            }

            if (spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)
                    || TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(authorizeType)) {
                isFirstAuthorizor = true;
            } else {
                isFirstAuthorizor = false;
            }

            if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                    || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) {
                if (spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)) {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED;
                } else if (futureValueDate == null || futureValueDate.length() == 0) {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_AUTHORIZED;
                } else {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_FVD_AUTHORIZED;
                }
            } //CR 821 Check for panel authorization and set status Begin
            else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType)) {
                isPanelEnabled = true;
                if (panelAuthProcessor == null) {
                    panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.SUPPLIER_PORTAL, mediatorServices.getErrorManager());
                }
                String panelGroupOid = invoiceOfferGroup.getAttribute("panel_auth_group_oid");
                String panelOplockVal = invoiceOfferGroup.getAttribute("panel_oplock_val");
                String[] panelRangeOid = new String[]{invoiceOfferGroup.getAttribute("panel_auth_range_oid")};
                panelAuthProcessor.init(panelGroupOid, panelRangeOid, panelOplockVal);
                String panelAuthInvOfferStatus = panelAuthProcessor.process(invoiceOfferGroup, invoiceOfferGroup.getAttribute("panel_auth_range_oid"), false);

                if (TradePortalConstants.AUTHFALSE.equals(panelAuthInvOfferStatus)) {
                    return false;
                } else if (TradePortalConstants.AUTHPARTL.equals(panelAuthInvOfferStatus)) {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED;
                } else {
                    if (futureValueDate == null || futureValueDate.length() == 0) {
                        newSPInvoiceStatus = TradePortalConstants.SP_STATUS_AUTHORIZED;
                    } else {
                        newSPInvoiceStatus = TradePortalConstants.SP_STATUS_FVD_AUTHORIZED;
                    }
                }// CR 821 End
            } else {
                if (futureValueDate == null || futureValueDate.length() == 0) {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_AUTHORIZED;
                } else {
                    newSPInvoiceStatus = TradePortalConstants.SP_STATUS_FVD_AUTHORIZED;
                }
            }

            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            if (isFirstAuthorizor || isPanelEnabled) {
                return true;
            }

            // If two authorizor is required, check it is not the same user or user group.
            List<Object> sqlParams = new ArrayList<>();
            String userOid = user.getAttribute("user_oid");
            String userWorkGroupOid = user.getAttribute("work_group_oid");
            String authorizeType = corpOrg.getAttribute("dual_auth_supplier_portal");
            StringBuilder sql = new StringBuilder();
            sql.append("select count(*) theCount from invoice where A_INVOICE_OFFER_GROUP_OID = ? ");
            sqlParams.add(invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (allSelectedInvoicesOid.length() > 0) {
                sql.append(" and invoice_oid in ")
                        .append(allSelectedInvoicesOid);
            }
            if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) {
                sql.append(" and a_first_authorizing_user_oid = ? ");
                sqlParams.add(userOid);
            } else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)) {
                sql.append(" and a_first_auth_work_group_oid = ? ");
                sqlParams.add(userWorkGroupOid);
            }
            DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);

            if (invoiceListDoc != null && invoiceListDoc.getAttributeInt("/ResultSetRecord(0)/THECOUNT") > 0) {
                if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) {
                    mediatorServices.getErrorManager().issueError(
                            getClass().getName(),
                            TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
                } else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)) {
                    mediatorServices.getErrorManager().issueError(
                            getClass().getName(),
                            TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
                }
                return false;
            }

            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            String userOid = user.getAttribute("user_oid");
            String userWorkGroupOid = user.getAttribute("work_group_oid");			
			
            if (userWorkGroupOid == null || userWorkGroupOid.length() == 0) {
                userWorkGroupOid = null;
            }
            List<Object> sqlParams = new ArrayList<Object>();
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set supplier_portal_invoice_status = ? ");
            sqlParams.add(newSPInvoiceStatus);
			//Srinivasu_D IR#40292 Rel9.3 07/09 start
			Long longUserUid = StringFunction.isNotBlank(userOid) ? Long.parseLong(userOid):null;
            Long longUserWorkGroupOid = StringFunction.isNotBlank(userWorkGroupOid) ? Long.parseLong(userWorkGroupOid):null;
            String invGroupOid = invoiceOfferGroup.getAttribute("invoice_offer_group_oid");
            Long longInvGroupOid = StringFunction.isNotBlank(invGroupOid) ? Long.parseLong(invGroupOid):null;
			//Srinivasu_D IR#40292 Rel9.3 07/09 - end
            if (!isPanelEnabled) {
                if (isFirstAuthorizor) {
                    sqlStatement.append(", a_first_authorizing_user_oid =? ")
                            .append(", a_first_auth_work_group_oid =? ")
                            .append(", first_authorize_status_date = sysdate ");
                   // sqlParams.add(userOid);
                   // sqlParams.add(userWorkGroupOid);
					sqlParams.add(longUserUid);
                    sqlParams.add(longUserWorkGroupOid);

                } else {
                    sqlStatement.append(", a_second_authorizing_user_oid = ? ")
                            .append(", a_second_auth_work_group_oid = ? ")
                            .append(", second_authorize_status_date = sysdate ");
                    //sqlParams.add(userOid);
                    //sqlParams.add(userWorkGroupOid);
					sqlParams.add(longUserUid);
                    sqlParams.add(longUserWorkGroupOid);

                }
            }			
            sqlStatement.append(", opt_lock = opt_lock + 1 where a_invoice_offer_group_oid = ? ");
			sqlParams.add(longInvGroupOid);
            //sqlParams.add(invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }
			
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, sqlParams);
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            if (newSPInvoiceStatus.equals(TradePortalConstants.SP_STATUS_AUTHORIZED)
                    || newSPInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_AUTHORIZED)) {
                createOutgoingInterfaceQueue(invoiceOfferGroup, mediatorServices);
            }

            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

    }

    private class AssignFVDProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.AssignFutureValueDate";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_ASSIGN_FVD";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || (!spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED))) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            String datePattern = user.getAttribute("datepattern");
            String futureValueDateStr = inputDoc.getAttribute("/future_value_date");
            SimpleDateFormat fmt = new SimpleDateFormat(datePattern);
            SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
            Date futureValueDate = null;
            try {
                futureValueDate = fmt.parse(futureValueDateStr);
                futureValueDateStr = reqformatter.format(futureValueDate);
            } catch (java.text.ParseException ignored) {

            }
            if (futureValueDateStr == null || futureValueDateStr.length() == 0 || futureValueDate == null) {
                mediatorServices.getErrorManager().issueError(
                        getClass().getName(),
                        AmsConstants.REQUIRED_ATTRIBUTE,
                        mediatorServices.getResourceManager().getText("InvoicesOffered.FutureValueDate", TradePortalConstants.TEXT_BUNDLE));
                return false;
            }

            CurrencyCalendarRule calendarRule = (CurrencyCalendarRule) mediatorServices.createServerEJB("CurrencyCalendarRule");
            String currency = invoiceOfferGroup.getAttribute("currency");
            try {
                calendarRule.find("currency", currency);
            } catch (ObjectNotFoundException e) {
                mediatorServices.getErrorManager().issueError(getClass().getName(),
                        TradePortalConstants.CURRENCY_CALENDAR_NOT_DEFINED,
                        currency);
                return false;
            }

            String calendarOid = calendarRule.getAttribute("tp_calendar_oid");
            TPCalendar tpCalendar = (TPCalendar) mediatorServices.createServerEJB("TPCalendar", Long.parseLong(calendarOid));
            Calendar fvdCal = Calendar.getInstance();
            fvdCal.setTime(futureValueDate);
            if (tpCalendar.isWeekend(fvdCal)) {
                mediatorServices.getErrorManager().issueError(getClass().getName(),
                        TradePortalConstants.IS_WEEKEND,
                        mediatorServices.getResourceManager().getText(
                                "InvoicesOffered.FutureValueDate",
                                TradePortalConstants.TEXT_BUNDLE));
                return false;
            }
            TPCalendarYear tpCalendarYear = tpCalendar.getTPCalendarYear(fvdCal.get(Calendar.YEAR));
            if (tpCalendarYear == null) {
                mediatorServices.getErrorManager().issueError(getClass().getName(),
                        TradePortalConstants.CALENDAR_NOT_DEFINED_FOR_YEAR,
                        currency,
                        String.valueOf(fvdCal.get(Calendar.YEAR)));
                return false;
            }
            if (tpCalendarYear.isHoliday(fvdCal)) {
                mediatorServices.getErrorManager().issueError(getClass().getName(),
                        TradePortalConstants.IS_HOLIDAY,
                        mediatorServices.getResourceManager().getText(
                                "InvoicesOffered.FutureValueDate",
                                TradePortalConstants.TEXT_BUNDLE));
                return false;
            }
			
			 //Srinivasu_D IR#T36000037155 Rel9.3 07/14/2015 - Future date validation

				if(!TPDateTimeUtility.isFutureDate(futureValueDateStr,user.getAttribute("timezone"))){
		        
			    	mediatorServices.getErrorManager().issueError(
				            TradePortalConstants.ERR_CAT_1,
					        TradePortalConstants.SP_INVOICE_OFFER_FUTURE_DATE);
	                return false;
				}
			
             //Srinivasu_D IR#T36000037155 Rel9.3 07/14/2015 - End		
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            String newFutureValueDate = inputDoc.getAttribute("/future_value_date");
            String datePattern = user.getAttribute("datepattern");
            SimpleDateFormat fmt = new SimpleDateFormat(datePattern);
            SimpleDateFormat reqformatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date futureValueDate = fmt.parse(newFutureValueDate);
                newFutureValueDate = reqformatter.format(futureValueDate);
            } catch (java.text.ParseException ignored) {

            }

            StringBuilder sql = new StringBuilder()
                    .append("select count(*) theCount from invoice ")
                    .append(" where a_invoice_offer_group_oid = ? and (offer_expiry_date < to_date(?, 'yyyy-MM-dd')")
                    .append(" or decode(invoice_payment_date, null, invoice_due_date, invoice_payment_date) < to_date(?, 'yyyy-MM-dd'))");
            if (allSelectedInvoicesOid.length() > 0) {
                sql.append(" and invoice_oid in ")
                        .append(allSelectedInvoicesOid);
            }
            DocumentHandler invoiceCountDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"), newFutureValueDate, newFutureValueDate);
            if (invoiceCountDoc != null && invoiceCountDoc.getAttributeInt("/ResultSetRecord(0)/THECOUNT") > 0) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SP_INCORRECT_FUTURE_VALUE_DATE);
                return false;
            }
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            StringBuilder sqlStatement = new StringBuilder();
            String datePattern = user.getAttribute("datepattern");
            String FVDDate = "";
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
                SimpleDateFormat reqformatter = new SimpleDateFormat("yyyy-MM-dd");
                Date formatFVDDate = formatter.parse(inputDoc.getAttribute("/future_value_date"));
                FVDDate = reqformatter.format(formatFVDDate);
            } catch (ParseException e1) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.WEBI_INVALID_DATE_ERROR);
                return false;
            }

            sqlStatement.append("update invoice set supplier_portal_invoice_status = ?, future_value_date = to_date(?, 'yyyy-MM-dd'), opt_lock = opt_lock + 1")
                    .append(" where a_invoice_offer_group_oid = ? ");
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, TradePortalConstants.SP_STATUS_FVD_ASSIGNED, FVDDate, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            String debugSettingForCalculation = null;
            try {
                PropertyResourceBundle properties = (PropertyResourceBundle) ResourceBundle.getBundle("TradePortal");
                debugSettingForCalculation = properties.getString("DebugInvoiceOfferCalculation");
            } catch (MissingResourceException ignored) {

            }
            if ("Y".equals(debugSettingForCalculation) && this.allSelectedInvoicesOids.size() > 0) {
                Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice", Long.parseLong(allSelectedInvoicesOids.get(0)));
                invoice.calculateInvoiceOfferNetAmountOffered();
            }

            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Let regroup take care of the update if needed
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_FVD_ASSIGNED;
            return true;
        }

    }

    private class RemoveFVDProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.RemoveFutureValueDate";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_REMOVE_FVD";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || (!spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED))) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            List<Object> sqlParams = new ArrayList<>();
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set supplier_portal_invoice_status = '")
                    .append(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    .append("', future_value_date = null")
                    .append(", opt_lock = opt_lock + 1")
                    .append(" where a_invoice_offer_group_oid = ? ");
            sqlParams.add(invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));

            String allSelectedInvoicesOidOptLockStr = allSelectedInvoicesOidOptLock.toString();
            allSelectedInvoicesOidOptLockStr = allSelectedInvoicesOidOptLockStr.replaceAll("\\(", "");
            allSelectedInvoicesOidOptLockStr = allSelectedInvoicesOidOptLockStr.replaceAll("\\)", "");

            StringBuilder invoicePlaceHolders = new StringBuilder("(");
            String[] ids = allSelectedInvoicesOidOptLockStr.split(",");
            for (int i = 0; i < ids.length; i++) {
                invoicePlaceHolders.append('?');
                if (ids.length - 1 > i) {
                    invoicePlaceHolders.append(',');
                }
                sqlParams.add(ids[i]);
            }
            invoicePlaceHolders.append(')');

            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(invoicePlaceHolders);
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, sqlParams);

            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Let regroup take care of the update if needed
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_BUYER_APPROVED;
            return true;
        }

    }

    private class ResetOfferProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.ResetToOffer";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_RESET_OFFER";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || (!spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_AUTHORIZED))) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            // Note Reset To Offered can only be done by groups.
            DatabaseQueryBean.executeUpdate("update invoice set supplier_portal_invoice_status = ?, opt_lock = opt_lock + 1 where a_invoice_offer_group_oid =? ", false, TradePortalConstants.SP_STATUS_FVD_ASSIGNED, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            DatabaseQueryBean.executeUpdate("delete from outgoing_queue where a_transaction_oid in (select invoice_oid from invoice  where a_invoice_offer_group_oid = ? )  and msg_type = ? ", false, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"), MessageType.TRIG);
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_FVD_ASSIGNED;
            return true;
        }

    }

    private class AttachDocumentProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.AttachDocument";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }

                   // This processor is called twice - when the user navigates to the PDF uploading page and after the PDF is uploaded.
            // When the user is just navigating to the PDF uploading page, there is no image yet.  We want to go through the mediator
            // so that we can do some of the validation.
            if (StringFunction.isBlank(inputDoc.getAttribute("/DocImage/image_id"))) {
                return true;
            }

            DocumentImage documentImage = (DocumentImage) mediatorServices.createServerEJB("DocumentImage");
            documentImage.newObject();

            String docName = inputDoc.getAttribute("/DocImage/documentName");
            String imageId = inputDoc.getAttribute("/DocImage/image_id");
            String imageFormat = inputDoc.getAttribute("/DocImage/image_format");
            String numPages = inputDoc.getAttribute("/DocImage/num_pages");
            String imageBytes = inputDoc.getAttribute("/DocImage/image_bytes");

            if (((docName != null) && (imageId != null)
                    && (imageFormat != null) && (numPages != null) && (imageBytes != null))) {
                documentImage.setAttribute("doc_name", docName);
                documentImage.setAttribute("image_id", imageId);
                documentImage.setAttribute("image_format", imageFormat);
                documentImage.setAttribute("num_pages", numPages);
                documentImage.setAttribute("image_bytes", imageBytes);
                documentImage.setAttribute("form_type", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

                String imageHash = inputDoc.getAttribute("/DocImage/hash");
                if (imageHash != null) {
                    documentImage.setAttribute("hash", imageHash);
                }

                String docImageOid = documentImage.getAttribute("doc_image_oid");
                documentImage.save();
                inputDoc.setAttribute("/DocImage/doc_image_oid", docImageOid);
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
                   // This processor is called twice - when the user navigates to the PDF uploading page and after the PDF is uploaded.
            // When the user is just navigating to the PDF uploading page, there is no image yet.  We want to go through the mediator
            // so that we can do some of the validation.
            if (StringFunction.isBlank(inputDoc.getAttribute("/DocImage/image_id"))) {
                return true;
            }
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set a_document_image_oid = ? ")
                    .append(", opt_lock = opt_lock + 1")
                    .append(" where a_invoice_offer_group_oid = ? ");
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }

            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, inputDoc.getAttribute("/DocImage/doc_image_oid"), invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
                   // This processor is called twice - when the user navigates to the PDF uploading page and after the PDF is uploaded.
            // When the user is just navigating to the PDF uploading page, there is no image yet.  We want to go through the mediator
            // so that we can do some of the validation.
            if (StringFunction.isBlank(inputDoc.getAttribute("/DocImage/image_id"))) {
                return true;
            }
            invoiceOfferGroup.setAttribute("attachment_ind", "Y");
            return true;
        }

        @Override
        protected boolean regroupInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, User user, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Do not regroup invoice offers for pdf attachment, which does not change invoice offer status
            return true;
        }

    }

    private class DeleteDocumentProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.DeleteDocument";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            String attachmentInd = invoiceOfferGroup.getAttribute("attachment_ind");
            if (spInvoiceStatus == null
                    || (!spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED))
                    || !"Y".equals(attachmentInd)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            if (allSelectedInvoicesOid.length() > 0) {
                StringBuilder sql = new StringBuilder()
                        .append("select count(*) theCount from invoice ")
                        .append(" where a_invoice_offer_group_oid = ? ")
                        .append(" and a_document_image_oid is not null")
                        .append(" and invoice_oid in ")
                        .append(allSelectedInvoicesOid);
                DocumentHandler invoiceCountDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, new Object[]{invoiceOfferGroup.getAttribute("invoice_offer_group_oid")});
                if (invoiceCountDoc != null && invoiceCountDoc.getAttributeInt("/ResultSetRecord(0)/THECOUNT") == 0) {
                    mediatorServices.getErrorManager().issueError(
                            TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.SP_INCORRECT_STATUS);
                    return false;
                }
            }
            return true;

        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            StringBuilder sqlStatement = new StringBuilder();
            sqlStatement.append("update invoice set a_document_image_oid = null")
                    .append(", opt_lock = opt_lock + 1")
                    .append(" where a_invoice_offer_group_oid = ? ");
            if (allSelectedInvoicesOidOptLock.length() > 0) {
                sqlStatement.append(" and (invoice_oid, opt_lock) in ")
                        .append(allSelectedInvoicesOidOptLock);
            }
            int numberUpdated = DatabaseQueryBean.executeUpdate(sqlStatement.toString(), false, new Object[]{invoiceOfferGroup.getAttribute("invoice_offer_group_oid")});
            if (numberOfSelectedInvoices != 0 && numberUpdated != numberOfSelectedInvoices) {
                mediatorServices.getErrorManager().issueError(getClass().getName(), "ATT07");
                throw new OptimisticLockException(this.getClass().getName());
            }
            numberOfUpdatedInvoices += numberUpdated;
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Check whether to set the invoice offer group attachment_ind.
            DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet("select count(*) theCount from invoice where A_INVOICE_OFFER_GROUP_OID = ? and a_document_image_oid is not null", true, new Object[]{invoiceOfferGroup.getAttribute("invoice_offer_group_oid")});
            if (invoiceListDoc != null && invoiceListDoc.getAttributeInt("/ResultSetRecord(0)/THECOUNT") == 0) {
                invoiceOfferGroup.setAttribute("attachment_ind", "N");
            }
            return true;
        }

        @Override
        protected boolean regroupInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, User user, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Do not regroup invoice offers for pdf attachment, which does not change invoice offer status
            return true;
        }

    }

    private DocumentHandler groupInvoices(DocumentHandler inputDoc, MediatorServices mediatorServices)
            throws AmsException, RemoteException, SQLException {
        String groupName = inputDoc.getAttribute("/group_name");

        // error for empty group name
        if (groupName == null || groupName.length() == 0) {
            mediatorServices.getErrorManager().issueError(
                    getClass().getName(),
                    AmsConstants.REQUIRED_ATTRIBUTE,
                    mediatorServices.getResourceManager().getText(
                            "InvoicesOffered.InvoiceGroupName",
                            TradePortalConstants.TEXT_BUNDLE));
            return new DocumentHandler();
        }
        String corpOrgOid = inputDoc.getAttribute("/ownerOrg");

        // Check for duplicate invoice offer group
        String sqlQry = "select count(*) thecount from invoice_offer_group where a_corp_org_oid = ? and name = ? "
                + " and supplier_portal_invoice_status in ('BUYER_APPROVED','OFFER_ACCEPTED','PARTIALLY_AUTHORIZED','FVD_ASSIGNED',"
                + "'REJECTED_BY_BANK' ) and total_number > 0";
        DocumentHandler existingGroupDoc = DatabaseQueryBean.getXmlResultSet(sqlQry, true, corpOrgOid, groupName);
        int noOfExistingGroup = existingGroupDoc.getAttributeInt("/ResultSetRecord(0)/THECOUNT");
        if (noOfExistingGroup > 0) {
            mediatorServices.getErrorManager().issueError(
                    getClass().getName(),
                    TradePortalConstants.ALREADY_EXISTS,
                    groupName,
                    mediatorServices.getResourceManager().getText(
                            "InvoicesOffered.InvoiceGroupName",
                            TradePortalConstants.TEXT_BUNDLE));

        }

        String currency = inputDoc.getAttribute("/group_currency");

        String groupAmount = inputDoc.getAttribute("/group_amount");
        try {
            groupAmount = NumberValidator.getNonInternationalizedValue(groupAmount, mediatorServices.getResourceManager().getResourceLocale(), true);
        } catch (InvalidAttributeValueException iae) {
            //IR T36000018837 - display error message if its invalid amount
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.INVALID_SUPP_AMT);
            return new DocumentHandler();
        }

        BigDecimal amount = new BigDecimal(groupAmount);
        DocumentHandler outputDoc = new DocumentHandler();
        outputDoc.setAttribute("/group_name", groupName);
        outputDoc.setAttribute("/group_currency", currency);
        outputDoc.setAttribute("/group_amount", inputDoc.getAttribute("/group_amount"));

            // 1) Create a list of invoices where eligibility indicator = Y and eligibility expiry date > future date (if entered)
        // 2) Sort the invoices by due date and then by amount in descending order
        // 3) Start selecting the invoices from the top of the list until the total amount of selected invoices is
        // equal to or slightly less than the requested amount, crossing due dates as necessary.  If the total net
        // offered amount of the invoices equals the required amount, then stop.  If the next invoice in the
        // descending list exceeds the required amount, skip it and continue until you find another invoice that will
        // bring the total invoice amount closer to the required amount.
        List<Object> sqlParams = new ArrayList<Object>();
        StringBuilder sql = new StringBuilder()
                .append("select invoice_oid, net_amount_offered, invoice_total_amount from invoice ")
                .append(" where currency_code = ? and a_corp_org_oid = ? ")
                .append(" and supplier_portal_invoice_status in (?, ?, ?, ?)")
                .append(" and not exists (select * from invoice_offer_group ")
                .append(" where invoice_offer_group_oid = a_invoice_offer_group_oid")
                .append(" and invoice_offer_group.name is not null)")
                .append(" and net_amount_offered <= ?")
                .append(" order by decode(invoice_payment_date, null, invoice_due_date, invoice_payment_date) desc")
                .append(", net_amount_offered desc");
        sqlParams.add(currency);
        sqlParams.add(corpOrgOid);
        sqlParams.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
        sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
        sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
        sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
        sqlParams.add(amount);

        DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
        StringBuilder invoiceOids = new StringBuilder();
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalNetAmountOffer = BigDecimal.ZERO;
        int totalNumber = 0;
        if (invoiceListDoc != null) {
            List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler doc : records) {
                BigDecimal invoiceNetAmountOffered = doc.getAttributeDecimal("/NET_AMOUNT_OFFERED");
                BigDecimal invoiceAmount = doc.getAttributeDecimal("/INVOICE_TOTAL_AMOUNT");
                if (totalNetAmountOffer.add(invoiceNetAmountOffered).compareTo(amount) <= 0) {
                    String invoiceOid = doc.getAttribute("/INVOICE_OID");
                    outputDoc.setAttribute("/InvoiceOfferList/InvoiceOffer(" + totalNumber + ")/InvoiceData", invoiceOid);
                    if (invoiceOids.length() > 0) {
                        invoiceOids.append(',');
                    }
                    invoiceOids.append(invoiceOid);
                    totalNumber++;
                    totalAmount = totalAmount.add(invoiceAmount);
                    totalNetAmountOffer = totalNetAmountOffer.add(invoiceNetAmountOffered);
                }
            }
        }

                // If the bottom of the list is reached and the total cannot be reached, then re-sort the remaining
        // list by amount and due date in ascending order and find the lowest value invoice that, added to
        // the current running total will just exceed the required amount
        if (totalNetAmountOffer.compareTo(amount) < 0) {
            sqlParams = new ArrayList<Object>();
            sql = new StringBuilder()
                    .append("select invoice_oid, net_amount_offered, invoice_total_amount from invoice ")
                    .append(" where currency_code = ? and a_corp_org_oid = ? and supplier_portal_invoice_status in (?,?,?,?)")
                    .append(" and not exists (select * from invoice_offer_group ")
                    .append(" where invoice_offer_group_oid = a_invoice_offer_group_oid")
                    .append(" and invoice_offer_group.name is not null)");
            sqlParams.add(currency);
            sqlParams.add(corpOrgOid);
            sqlParams.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
            sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
            sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
            sqlParams.add(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);

            if (invoiceOids.length() > 0) {
                sql.append(" and invoice_oid not in (")
                        .append(invoiceOids)
                        .append(')');
            }
            sql.append(" order by net_amount_offered, decode(invoice_payment_date, null, invoice_due_date, invoice_payment_date) desc");
            invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
            if (invoiceListDoc != null) {
                BigDecimal invoiceNetAmountOffered = invoiceListDoc.getAttributeDecimal("/ResultSetRecord(0)/NET_AMOUNT_OFFERED");
                BigDecimal invoiceAmount = invoiceListDoc.getAttributeDecimal("/ResultSetRecord(0)/INVOICE_TOTAL_AMOUNT");
                String invoiceOid = invoiceListDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OID");
                outputDoc.setAttribute("/InvoiceOfferList/InvoiceOffer(" + totalNumber + ")/InvoiceData", invoiceOid);
                totalNumber++;
                totalAmount = totalAmount.add(invoiceAmount);
                totalNetAmountOffer = totalNetAmountOffer.add(invoiceNetAmountOffered);
            }

        }
        outputDoc.setAttribute("/target_amount", TPCurrencyUtility.getDisplayAmount(amount.toString(), currency, mediatorServices.getResourceManager().getResourceLocale()));
        outputDoc.setAttribute("/actual_amount", TPCurrencyUtility.getDisplayAmount(totalNetAmountOffer.toString(), currency, mediatorServices.getResourceManager().getResourceLocale()));

        LOG.debug("InvoiceOfferMedaitorBean.GroupInvoices output:" + outputDoc);
        return outputDoc;
    }

    private DocumentHandler acceptInvoiceGrouping(DocumentHandler inputDoc, MediatorServices mediatorServices)
            throws AmsException, RemoteException, SQLException {
        List<String> invoiceOidList = getInvoicesToUserDefinedGruop(inputDoc, mediatorServices);
        if (invoiceOidList.isEmpty()) {
            return new DocumentHandler();
        }

        // Create new Invoice Offer Group
        InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) mediatorServices.createServerEJB("InvoiceOfferGroup");
        long newInvoiceOfferGroupOid = invoiceOfferGroup.newObject();
        String currency = inputDoc.getAttribute("/group_currency");
        String name = inputDoc.getAttribute("/group_name");
        String corpOrgOid = inputDoc.getAttribute("/ownerOrg");
        invoiceOfferGroup.setAttribute("currency", currency);
        invoiceOfferGroup.setAttribute("name", name);
        invoiceOfferGroup.setAttribute("corp_org_oid", corpOrgOid);
        invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_BUYER_APPROVED);

        String attachmentInd = "N";
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalNetAmountOffered = BigDecimal.ZERO;
        int totalNumber = 0;
        for (String invoiceOid : invoiceOidList) {

            // Remove the invoice from the original invoice offer group
            String invoiceOfferGroupUpdateSQL = "update invoice_offer_group "
                    + " set total_number = total_number - 1, "
                    + " total_amount = total_amount - (select invoice_total_amount from invoice where invoice_oid = ? ), "
                    + " total_net_amount_offered = total_net_amount_offered - (select net_amount_offered from invoice where invoice_oid = ? ) "
                    + " where invoice_offer_group_oid = (select a_invoice_offer_group_oid from invoice where invoice_oid = ? )";
            // If the original invoice offer group is empty now, remove the invoice group itself
            DatabaseQueryBean.executeUpdate(invoiceOfferGroupUpdateSQL, false, invoiceOid, invoiceOid, invoiceOid);
            String invoiceOfferGroupDeleteSQL = "delete invoice_offer_group where "
                    + " total_number = 0  and invoice_offer_group_oid = (select a_invoice_offer_group_oid from invoice where invoice_oid = ? )";
            DatabaseQueryBean.executeUpdate(invoiceOfferGroupDeleteSQL, false, new Object[]{invoiceOid});

            // Associate the invoice with the new invoice Offer group
            String invoiceUpdateSQL = "update invoice  set a_invoice_offer_group_oid =?, supplier_portal_invoice_status = ?,"
                    + " future_value_date = null where invoice_oid = ? ";
            DatabaseQueryBean.executeUpdate(invoiceUpdateSQL, false, newInvoiceOfferGroupOid, TradePortalConstants.SP_STATUS_BUYER_APPROVED, invoiceOid);

            // Add up the total_amount, total_net_amount_offered
            String invoiceQuerySQL = "select a_document_image_oid, invoice_total_amount, net_amount_offered from invoice where"
                    + " invoice_oid = ? ";
            DocumentHandler result = DatabaseQueryBean.getXmlResultSet(invoiceQuerySQL, false, invoiceOid);
            if (result.getAttribute("/ResultSetRecord(0)/A_DOCUMENT_IMAGE_OID") != null
                    && result.getAttribute("/ResultSetRecord(0)/A_DOCUMENT_IMAGE_OID").length() > 0) {
                attachmentInd = "Y";
            }
            BigDecimal invoiceNetAmountOffered = result.getAttributeDecimal("/ResultSetRecord(0)/NET_AMOUNT_OFFERED");
            BigDecimal invoiceAmount = result.getAttributeDecimal("/ResultSetRecord(0)/INVOICE_TOTAL_AMOUNT");
            totalAmount = totalAmount.add(invoiceAmount);
            totalNetAmountOffered = totalNetAmountOffered.add(invoiceNetAmountOffered);
            totalNumber++;

        }

        invoiceOfferGroup.setAttribute("total_number", String.valueOf(totalNumber));
        invoiceOfferGroup.setAttribute("total_amount", totalAmount.toString());
        invoiceOfferGroup.setAttribute("total_net_amount_offered", totalNetAmountOffered.toString());
        invoiceOfferGroup.setAttribute("attachment_ind", attachmentInd);
        invoiceOfferGroup.save();
        return new DocumentHandler();

    }

    /**
     * Remove offers from group.
     */
    public class RemoveInvoicesProcessor extends AbstractInvoiceOfferProcessor {

        BigDecimal totalAmountToRemove;
        BigDecimal totalNetAmountOfferedToRemove;

        public RemoveInvoicesProcessor() {
            super();
            totalAmountToRemove = BigDecimal.ZERO;
            totalNetAmountOfferedToRemove = BigDecimal.ZERO;
        }

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.RemoveInvoices";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_BUYER_APPROVED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_FVD_ASSIGNED)
                    && !spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED)) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            for (int i = 0; i < allSelectedInvoicesOids.size(); i++) {
                String invoiceOid = allSelectedInvoicesOids.get(i);
                Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice",
                        Long.parseLong(invoiceOid));
                totalAmountToRemove = totalAmountToRemove.add(invoice.getAttributeDecimal("invoice_total_amount"));
                totalNetAmountOfferedToRemove = totalNetAmountOfferedToRemove.add(invoice.getAttributeDecimal("net_amount_offered"));
                invoice.setAttribute("opt_lock", allSelectedInvoicesOptLocks.get(i));
                invoice.setAttribute("invoice_offer_group_oid", null);
                if (invoice.getAttribute("future_value_date").length() > 0) {
                    invoice.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
                } else {
                    invoice.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_BUYER_APPROVED);
                }
                invoice.updateAssociatedInvoiceOfferGroup();
                invoice.save();
            }

            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            // Let regroup take care of the update if needed
            int totalNumber = invoiceOfferGroup.getAttributeInteger("total_number");
            int totalNumberToRemove = allSelectedInvoicesOids.size();
            invoiceOfferGroup.setAttribute("total_number", String.valueOf(totalNumber - totalNumberToRemove));
            invoiceOfferGroup.setAttribute("total_amount", invoiceOfferGroup.getAttributeDecimal("total_amount").subtract(totalAmountToRemove).toString());
            invoiceOfferGroup.setAttribute("total_net_amount_offered", invoiceOfferGroup.getAttributeDecimal("total_net_amount_offered").subtract(totalNetAmountOfferedToRemove).toString());
            invoiceOfferGroup.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_BUYER_APPROVED);
            return true;
        }

        @Override
        protected boolean regroupInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, User user, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            // No more processing to do.  All invoices to be removed are already regrouped.
            return true;
        }

    }

    private class ResetInvoicesOfferProcessor extends AbstractInvoiceOfferProcessor {

        @Override
        public String getActionLabelForDisplay() {
            return "InvoicesOffered.ResetToOffer";
        }

        @Override
        public boolean validateInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            this.action = "SP_RESET_OFFER";
            String spInvoiceStatus = invoiceOfferGroup.getAttribute("supplier_portal_invoice_status");
            if (spInvoiceStatus == null
                    || (!(spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED)
                    || spInvoiceStatus.equals(TradePortalConstants.SP_STATUS_PARTIALLY_AUTHORIZED)))) {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1,
                        TradePortalConstants.SP_INCORRECT_STATUS);
                return false;
            }
            return true;
        }

        @Override
        public boolean validateInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            return true;
        }

        @Override
        public boolean processAllInvoiceOffers(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException, SQLException {
            // Note Reset To Offered can only be done by groups.
            DatabaseQueryBean.executeUpdate("update invoice set supplier_portal_invoice_status = ?, opt_lock = opt_lock + 1 where a_invoice_offer_group_oid = ? ", false, TradePortalConstants.SP_STATUS_BUYER_APPROVED, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"));
            DatabaseQueryBean.executeUpdate("delete from panel_authorizer where p_owner_oid = ? and owner_object_type = ? ", false, invoiceOfferGroup.getAttribute("invoice_offer_group_oid"), TradePortalConstants.INVOICE_OFFER_GROUP);
            return true;
        }

        @Override
        public boolean processInvoiceOfferGroup(InvoiceOfferGroup invoiceOfferGroup, DocumentHandler inputDoc, User user, CorporateOrganization corpOrg, MediatorServices mediatorServices) throws AmsException, RemoteException {
            newSPInvoiceStatus = TradePortalConstants.SP_STATUS_BUYER_APPROVED;
            invoiceOfferGroup.setAttribute("panel_auth_group_oid", null);
            invoiceOfferGroup.setAttribute("panel_oplock_val", null);
            invoiceOfferGroup.setAttribute("panel_auth_range_oid", null);
            return true;
        }

    }

    private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
            throws RemoteException, AmsException {

        LOG.debug("InvoiceOfferMediatorBean:::performNotifyPanelAuthUser");
        String invoiceOfferGroupOid = inputDoc.getAttribute("/InvoiceOffer/invoiceOfferGroupOid");

        // create email for all selected users to notification
        List<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragmentsList("/EmailList/user");
        StringBuilder emailReceivers = new StringBuilder();
        String userOid = null;
        String ownerOrgOid = null;
        DocumentHandler outputDoc = new DocumentHandler();
        for (DocumentHandler doc : beneficiaryDetailsList) {
            userOid = doc.getAttribute("/userOid");
            User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
            String userMailID = user.getAttribute("email_addr");
            ownerOrgOid = user.getAttribute("owner_org_oid");
            if (StringFunction.isNotBlank(userMailID)) {
                emailReceivers.append(userMailID).append(',');
            }
        }

        if (StringFunction.isNotBlank(emailReceivers.toString())) {
            // Get the data from transaction that will be used to build the email message
            String amount = "";
            String currency = "";
            if (StringFunction.isNotBlank(invoiceOfferGroupOid)) {
                InvoiceOfferGroup invoiceOfferGroup = (InvoiceOfferGroup) mediatorServices.createServerEJB("InvoiceOfferGroup", Long.parseLong(invoiceOfferGroupOid));
                currency = invoiceOfferGroup.getAttribute("currency");
                amount = invoiceOfferGroup.getAttribute("total_amount");
                amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
            }

            StringBuilder emailContent = new StringBuilder();

            emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
            emailContent.append(TradePortalConstants.NEW_LINE);
            // Include PayRemit Amount and currency
            if (StringFunction.isNotBlank(amount)) {
                emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount", TradePortalConstants.TEXT_BUNDLE)).append(' ');
                emailContent.append(currency).append(' ').append(amount);
                emailContent.append(TradePortalConstants.NEW_LINE);
            }

            emailContent.append(TradePortalConstants.NEW_LINE);
            // call generic method to trigger email
            InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
        }

        return outputDoc;

    }

    //Nar Release8.3.0.0 09-09-2013 IR-T36000020527 ADD--BEGIN

    /**
     * This Method returns invoiceOid list to group for user defined criteria
     * (currency and amount).
     */
    private List<String> getInvoicesToUserDefinedGruop(DocumentHandler inputDoc, MediatorServices mediatorServices) throws
            AmsException, RemoteException {

        List<String> invoiceOidList = new ArrayList<>();
        String currency = inputDoc.getAttribute("/group_currency");
        String groupAmount = inputDoc.getAttribute("/group_amount");
        String corpOrgOid = inputDoc.getAttribute("/ownerOrg");
        groupAmount = NumberValidator.getNonInternationalizedValue(groupAmount, mediatorServices.getResourceManager().getResourceLocale(), true);
        BigDecimal amount = new BigDecimal(groupAmount);

            // 1) Create a list of invoices where eligibility indicator = Y and eligibility expiry date > future date (if entered)
        // 2) Sort the invoices by due date and then by amount in descending order
        // 3) Start selecting the invoices from the top of the list until the total amount of selected invoices is
        // equal to or slightly less than the requested amount, crossing due dates as necessary.  If the total net
        // offered amount of the invoices equals the required amount, then stop.  If the next invoice in the
        // descending list exceeds the required amount, skip it and continue until you find another invoice that will
        // bring the total invoice amount closer to the required amount.
        StringBuilder sql = new StringBuilder()
                .append("select invoice_oid, net_amount_offered from invoice ")
                .append(" where currency_code =? and a_corp_org_oid = ? ")
                .append(" and supplier_portal_invoice_status in (?,?,?,?)")
                .append(" and not exists (select * from invoice_offer_group ")
                .append(" where invoice_offer_group_oid = a_invoice_offer_group_oid")
                .append(" and invoice_offer_group.name is not null)")
                .append(" and net_amount_offered <=? ")
                .append(" order by decode(invoice_payment_date, null, invoice_due_date, invoice_payment_date) desc")
                .append(", net_amount_offered desc");
        List<Object> sqlParams = new ArrayList<Object>();
        sqlParams.add(currency);
        sqlParams.add(corpOrgOid);
        sqlParams.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
        sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
        sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
        sqlParams.add(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);
        sqlParams.add(amount);

        DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
        StringBuilder invoiceOids = new StringBuilder();
        BigDecimal totalNetAmountOffer = BigDecimal.ZERO;
        if (invoiceListDoc != null) {
            List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler doc : records) {
                BigDecimal invoiceNetAmountOffered = doc.getAttributeDecimal("/NET_AMOUNT_OFFERED");
                if (totalNetAmountOffer.add(invoiceNetAmountOffered).compareTo(amount) <= 0) {
                    String invoiceOid = doc.getAttribute("/INVOICE_OID");
                    invoiceOidList.add(invoiceOid);
                    if (invoiceOids.length() > 0) {
                        invoiceOids.append(',');
                    }
                    invoiceOids.append(invoiceOid);
                    totalNetAmountOffer = totalNetAmountOffer.add(invoiceNetAmountOffered);
                }
            }
        }

                // If the bottom of the list is reached and the total cannot be reached, then re-sort the remaining
        // list by amount and due date in ascending order and find the lowest value invoice that, added to
        // the current running total will just exceed the required amount
        if (totalNetAmountOffer.compareTo(amount) < 0) {
            sqlParams = new ArrayList<Object>();
            sql = new StringBuilder()
                    .append("select invoice_oid from invoice ")
                    .append(" where currency_code = ?")
                    .append(" and a_corp_org_oid =? ")
                    .append(" and supplier_portal_invoice_status in (?,?,?,?)")
                    .append(" and not exists (select * from invoice_offer_group ")
                    .append(" where invoice_offer_group_oid = a_invoice_offer_group_oid")
                    .append(" and invoice_offer_group.name is not null)");
            sqlParams.add(currency);
            sqlParams.add(corpOrgOid);
            sqlParams.add(TradePortalConstants.SP_STATUS_REJECTED_BY_BANK);
            sqlParams.add(TradePortalConstants.SP_STATUS_BUYER_APPROVED);
            sqlParams.add(TradePortalConstants.SP_STATUS_FVD_ASSIGNED);
            sqlParams.add(TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);

            if (invoiceOids.length() > 0) {
                sql.append(" and invoice_oid not in (")
                        .append(invoiceOids)
                        .append(')');
            }
            sql.append(" order by net_amount_offered, decode(invoice_payment_date, null, invoice_due_date, invoice_payment_date) desc");
            invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParams);
            if (invoiceListDoc != null) {
                String invoiceOid = invoiceListDoc.getAttribute("/ResultSetRecord(0)/INVOICE_OID");
                invoiceOidList.add(invoiceOid);
            }
        }

        return invoiceOidList;
    }
		//Nar Release8.3.0.0 09-09-2013 IR-T36000020527 ADD-END
}
