package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
 
import java.rmi.RemoteException;
import java.util.Vector;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.ActiveUserSession;
import com.ams.tradeportal.busobj.RecentInstrument;
import com.ams.tradeportal.busobj.UserSessionHistory;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
/*
 * Manages the Logout Transaction.
 */
public class LogoutMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(LogoutMediatorBean.class);

/*
 * Performs a logout.  Removed the user's record from the active user session table and
 * cleans up any locks he may have.
 */
public DocumentHandler execute(DocumentHandler inputDoc, 
                               DocumentHandler outputDoc,
                               MediatorServices mediatorServices) 
        throws RemoteException, AmsException {

    //cquinton 2/16/2012 Rel 8.0 ppx255 start
    // get active session specific to the active session oid
    String activeUserSessionOidStr = inputDoc.getAttribute("/activeUserSessionOid");
    if ( StringFunction.isNotBlank(activeUserSessionOidStr)) {
        //move active user session to history

        //String userOid;
        ActiveUserSession activeSession;
        UserSessionHistory sessionHistory;
    
        try {
            activeSession = (ActiveUserSession) mediatorServices.createServerEJB("ActiveUserSession");
    
            // Create a new row in the user session history for the session being logged out
            sessionHistory = (UserSessionHistory) mediatorServices.createServerEJB("UserSessionHistory");
            sessionHistory.newObject();
    
            Long activeUserSessionOid = null;
            try {
                activeUserSessionOid = Long.parseLong(activeUserSessionOidStr);
            }
            catch (Exception e) {
                //LOG.info("Logon Mediator error: " + e.toString());
                e.printStackTrace();
                mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
                    String.valueOf(activeUserSessionOidStr), "active_user_session_oid");
            }
            
            try {
                activeSession.getData(activeUserSessionOid);
            }
            catch (Exception e) {
                //LOG.info("Logon Mediator error: " + e.toString());
                e.printStackTrace();
                mediatorServices.getErrorManager().issueError(getClass().getName(), AmsConstants.INVALID_ATTRIBUTE,
                    String.valueOf(activeUserSessionOid), "active_user_session_oid");
            }
    
            //ctq comment out old mechanism of finding active session
            // Get the user oid from the input XML document 
            //userOid = inputDoc.getAttribute("/User/user_oid");

            // Populate the active session object with the one that corresponds to the user
            //activeSession.find("user_oid", userOid);
    
            // Populate the session history
            sessionHistory.populateFromActiveSession(activeSession);
    
            // Keep track of the logout timestamp for this user
            sessionHistory.setAttribute("logout_timestamp", DateTimeUtility.getGMTDateTime(true, false));
    
            // Save a history record of the user's session
            sessionHistory.save();

            // Since the user is logging out, delete that record
            activeSession.delete();
    
        } catch (InvalidObjectIdentifierException ex) {
            // If the object does not exist, issue an error, we hope this doesn't
            // happen
            mediatorServices.getErrorManager ().issueError (
                getClass ().getName (), "INF01", ex.getObjectId());
        } catch (Exception e) {
            LOG.info("Exception caught in LogoutMediator.execute(): " 
                + e.toString());
        } finally {
            mediatorServices.debug("Mediator: Result of logout update is "
                + this.isTransSuccess(mediatorServices.getErrorManager()));
        }
    }
    //cquinton 2/16/2012 Rel 8.0 pxx255 end
    
    //cquinton 12/13/2012 persist the user's current preference data    
    String userOid = inputDoc.getAttribute("/User/user_oid");
    if ( !InstrumentServices.isBlank(userOid) ) { //we have a user!
        saveUserPreferenceData(userOid, inputDoc, mediatorServices);
    }
            
    return outputDoc;
}

    public void saveUserPreferenceData(String userOid, 
            DocumentHandler inputDoc, MediatorServices mediatorServices) {
    
        //recent instruments        
        try
        {
            //first delete any existing
        	//jgadela R91 IR T36000026319 - SQL INJECTION FIX
            String deleteSql = "select recent_instrument_oid from recent_instrument where a_user_oid = ?";
			
            DocumentHandler deleteDoc = DatabaseQueryBean.getXmlResultSet(deleteSql, false, new Object[]{userOid});
            if (deleteDoc != null) {
                Vector deleteDocList = deleteDoc.getFragments("/ResultSetRecord");
                for( int i=0; i<deleteDocList.size(); i++ ) {
                    DocumentHandler dashDoc = (DocumentHandler) deleteDocList.get(i);
                    String deleteOid = dashDoc.getAttribute("/RECENT_INSTRUMENT_OID");
                    RecentInstrument r = (RecentInstrument) 
                        mediatorServices.createServerEJB("RecentInstrument");
                    r.getData(Long.parseLong(deleteOid));
                    r.delete();
                }
            }
                                
            int i=0;
            String transactionOid = inputDoc.getAttribute("/UserPrefs/RecentInstrument("+i+")/transactionOid");
            while ( transactionOid!=null && i<10 ) { //provide a failsafe 
                String instrumentId = inputDoc.getAttribute("/UserPrefs/RecentInstrument("+i+")/instrumentId"); 
                String transactionType = inputDoc.getAttribute("/UserPrefs/RecentInstrument("+i+")/transactionType"); 
                String transactionStatus = inputDoc.getAttribute("/UserPrefs/RecentInstrument("+i+")/tranactionStatus");
           
                RecentInstrument r = (RecentInstrument)
                    mediatorServices.createServerEJB("RecentInstrument");
                r.newObject();
                r.setAttribute("user_oid", userOid);
                r.setAttribute("display_order", ""+i);
                r.setAttribute("transaction_oid", transactionOid);
                r.setAttribute("complete_instrument_id", instrumentId);
                r.setAttribute("transaction_type_code", transactionType);
                r.setAttribute("transaction_status", transactionStatus);
                r.save();
           
                i++;
                transactionOid = inputDoc.getAttribute("/UserPrefs/RecentInstrument("+i+")/transactionOid");
            }
        }
        catch(Exception ex) {
            //if it doesn't work we don't really care. but lets at least write to log
            System.err.println("An error occurred removing recent transaction data for " +
                "userOid " + userOid + ". Exception: " + ex.toString());
        }
    }
}
