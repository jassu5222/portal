package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.MailMessage;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class MailMessageMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(MailMessageMediatorBean.class);

  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException {

        mediatorServices.debug("------------------- Mail Message Mediator called ------------------");
        mediatorServices.debug("Mediator : InputDoc == " + inputDoc.toString() );

	// Declare variables
	MailMessage message                     = null;
	OutgoingInterfaceQueue outgoingMessage  = null;
	String isReply                          = inputDoc.getAttribute("/MailMessage/is_reply");
	String mode                             = inputDoc.getAttribute("/Update/MessageMode");
    Long oid                                = null;
	long messageOid                         = 0;
	String jPylonDate                       = DateTimeUtility.getGMTDateTime();		//Find out what today's date is...

	try {

        String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	    mediatorServices.debug("The button pressed is " + buttonPressed);
	    
	      // Nar Rel 9.2.0.0 04-Sep-2014 ADD - Begin
		  SecurityAccess.validateUserRightsForTrans( TradePortalConstants.EMAIL_TRIGGER_MAIL_MSG, "", inputDoc.getAttribute("/User/securityRights"), buttonPressed );
		 // Nar Rel 9.2.0.0 04-Sep-2014 ADD - End

	    /* First we need to create the Mail Message object that will be updated.
	        If we're NOT in create mode meaning we need to create a new message,
	        then we need to create the object based on the passed in oid.  Assuming
	        that the oid was successfully passed in.  Otherwise we create a new
	        object from scratch.
	    */

		message = (MailMessage) mediatorServices.createServerEJB("MailMessage");

		if(!mode.equals(TradePortalConstants.MESSAGE_CREATE))
		{
		    messageOid = inputDoc.getAttributeLong("/MailMessage/message_oid");
		    if (messageOid == 0)
		    {
			    mediatorServices.debug("Error: message_oid not present");
			    return null;
		    }
		    mediatorServices.debug("Mediator: Retrieving message " + messageOid);

		    message.getData(messageOid);

        }
		else
		{
            message.newObject();
            messageOid = message.getAttributeLong("message_oid");
        }

    	message.populateFromXmlDoc(inputDoc);   //Set objects attributes to what was initially passed in.
    	oid = new Long(messageOid);
    	message.setAttribute("message_oid", oid.toString());    //In case the inputDoc mucked up the
    	                                                        //Oid from the PopulateFromXmlDoc call.

        message.setRelatedInstrument(inputDoc.getAttribute("/Instrument/complete_instrument_id"), inputDoc.getAttribute("/User/owner_org_oid"));

	// Based on the button clicked, process an appropriate action.


            // If message_source_type isn't set yet, set it to PORTAL
            if(InstrumentServices.isBlank(message.getAttribute("message_source_type")))
                 message.setAttribute("message_source_type", TradePortalConstants.PORTAL);

	    // These attributes are always set regardless of the action being performed
            message.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/User/user_oid"));
          //update date only for non pre-debit funding message
    		if(StringFunction.isBlank(message.getAttribute("funding_amount")) &&
    				StringFunction.isBlank(message.getAttribute("funding_date")) &&
    						StringFunction.isBlank(message.getAttribute("funding_currency"))){
            message.setAttribute("last_update_date", jPylonDate );
    		}

//Route & Save
	    if (buttonPressed.equals(TradePortalConstants.BUTTON_ROUTEANDSAVE))
            {
                // If we're doing a route and save, the message should not be saved here.
                // Instead, it must be saved after the user has selected to whom they want
                // to route.

                // Set these so that required field validations won't fail
                if(InstrumentServices.isBlank(message.getAttribute("discrepancy_flag")))
                    message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);
                //rkrishna CR 375-D ATP 08/22/2007 Begin
                if(InstrumentServices.isBlank(message.getAttribute("atp_notice_flag")))
                    message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
                //rkrishna CR 375-D ATP 08/22/2007 End
                if(InstrumentServices.isBlank(message.getAttribute("message_status")))
                   message.setAttribute("message_status", TradePortalConstants.DRAFT);

                message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
				// validateMessageForISO8859Chars(message);
                // Validations should take place here so that the user will see any errors that occur
                // before they are sent to the route page
                message.validate();
                return new DocumentHandler();
             }
//Save As Draft
//Attach Documents
         else if( buttonPressed.equals(TradePortalConstants.BUTTON_SAVEASDRAFT) ||
        		 buttonPressed.equals(TradePortalConstants.BUTTON_ATTACH_DOCUMENTS) ) {

            message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);
            //rkrishna CR 375-D ATP 08/22/2007 Begin
            message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
            //rkrishna CR 375-D ATP 08/22/2007 End
            message.setAttribute("message_status", TradePortalConstants.DRAFT);
            message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
            message.setAttribute("assigned_to_user_oid", inputDoc.getAttribute("/User/user_oid"));
	    //Vshah - 05/08/09 - IR#LNUI051352836 - ADD BEGIN
            if( InstrumentServices.isBlank( message.getAttribute("assigned_to_corp_org_oid") ) )
                message.setAttribute("assigned_to_corp_org_oid", inputDoc.getAttribute("/User/owner_org_oid"));
            //Vshah - 05/08/09 - IR#LNUI051352836 - ADD END
				/*
				 * if(!validateMessageForISO8859Chars(message)){ return new DocumentHandler(); }
				 */
//Send To Bank
        }else if( buttonPressed.equals(TradePortalConstants.BUTTON_SENDTOBANK) ) {

            if( message.isDeleted() )
            {
                mediatorServices.getErrorManager ().issueError (
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
            }
           
            if( InstrumentServices.isBlank( message.getAttribute("assigned_to_user_oid") ) )
                message.setAttribute("assigned_to_corp_org_oid", inputDoc.getAttribute("/User/owner_org_oid"));

            if( InstrumentServices.isBlank( message.getAttribute("assigned_to_corp_org_oid") ) )
                message.setAttribute("assigned_to_user_oid", inputDoc.getAttribute("/User/user_oid"));

				/*
				 * if(!validateMessageForISO8859Chars(message)){ return new DocumentHandler(); }
				 */
            message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);
            //rkrishna CR 375-D ATP 08/22/2007 Begin
            message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
            //rkrishna CR 375-D ATP 08/22/2007 End
            message.setAttribute("message_status", TradePortalConstants.SENT_TO_BANK);
            message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);

            //When 'Sending to Bank' we need to build the Outgoing Interface Queue
            //And build it's required data...no special validation.
            outgoingMessage = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
            outgoingMessage.newObject();

            outgoingMessage.setAttribute("date_created", jPylonDate);
            outgoingMessage.setAttribute("status", TradePortalConstants.OUTGOING_STATUS_STARTED);
            outgoingMessage.setAttribute("msg_type", TradePortalConstants.QUEUE_MESSAGE_TYPE_MAIL);
            outgoingMessage.setAttribute("mail_message_oid", message.getAttribute("message_oid"));
            
            // Nar CR-1029 06/11/2015 Rel 9.4 Begin
            String ownerOrgOid = inputDoc.getAttribute("/User/owner_org_oid");
            if ( StringFunction.isNotBlank(ownerOrgOid) ) {
            	CorporateOrganization corpOrg = (CorporateOrganization)  mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(ownerOrgOid));
               if ( corpOrg.isCustNotIntegratedWihTPS() ) {
                  outgoingMessage.setAttribute("process_parameters", "DoNotSendMQ=Y|");
               }
            }
            // Nar CR-1029 06/11/2015 Rel 9.4 End
			// NSX 04/20/2010 - RDUK041649583 Begin
//        	/* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
//        	/* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
//            //outgoingMessage.setAttribute("message_id", Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false)));
//        	String messageId = Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false));
//
//        	PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("TradePortal");
//        	String serverName = portalProperties.getString("serverName");
//
//        	messageId += serverName.charAt(serverName.length() - 1);
        	String messageId = InstrumentServices.getNewMessageID();
			// NSX 04/20/2010 - RDUK041649583 End

        	outgoingMessage.setAttribute("message_id", messageId);
        	/* IValera - IR# JOUI111280267 - 02/18/2009 - Change End */

            outgoingMessage.save();
        }
//Delete Attached Documents
        else if(buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS)) {
        	outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
        }

        //Try to save the message data
	    mediatorServices.debug("Performing Message Save");

	    try {
	        //reset the output doc to null
            outputDoc = new DocumentHandler();
	        outputDoc.setAttribute("message_oid", message.getAttribute("message_oid"));

		    message.save(true);

		    mediatorServices.debug("Mediator: Completed the save of Message");

	    } catch (Exception e) {
		    LOG.info("General exception caught in MessageMediator:performSave");
		    e.printStackTrace();
	    } finally {
            //If the business object saved data, we need to send the user a confirmation message.
            //This is accomplished by issueing an error with a severity level of 1.

		    mediatorServices.debug("Mediator: Result of update is "
			                + this.isTransSuccess(mediatorServices.getErrorManager()));

		    if (this.isTransSuccess(mediatorServices.getErrorManager()))
		    {
		        String messageSubject = inputDoc.getAttribute("/MailMessage/message_subject");

	            if ( buttonPressed.equals(TradePortalConstants.BUTTON_SENDTOBANK) ) {

				    mediatorServices.getErrorManager().issueError(
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.SENT_TO_BANK_SUCCESSFUL,
					                    messageSubject);

	            }else if( buttonPressed.equals(TradePortalConstants.BUTTON_SAVEASDRAFT) ||
	                      buttonPressed.equals(TradePortalConstants.BUTTON_ROUTEANDSAVE)) {

				    mediatorServices.getErrorManager().issueError(
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.SAVE_SUCCESSFUL,
					                    messageSubject);
	            }

	        }

	    }

	} catch (InvalidObjectIdentifierException ex) {

	  // If the object does not exist, issue an error
	  mediatorServices.getErrorManager ().issueError (
					                    TradePortalConstants.ERR_CAT_1,
					                    TradePortalConstants.DELETED_BY_ANOTHER_USER);

	}

    return outputDoc;
  }

	/**
	 * Deletes attached documents marked by the user on a mail message (CR-186 - jkok)
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
        javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");

		DocumentHandler outputDoc = new DocumentHandler();

		List<String> attachedDocuments = inputDoc.getAttributes("/AttachedDocument");

		for(String attDoc : attachedDocuments) {
			if(StringFunction.isNotBlank(attDoc))
			{
				long docId = Long.parseLong(EncryptDecrypt.decryptStringUsingTripleDes(attDoc, secretKey));

				// Save the transaction oid in the mail message oid column.  Also
				// set transaction_oid to 0 to further flag that it was a deleted image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId);
				documentImageInstance.setAttribute("transaction_oid", documentImageInstance.getAttribute("mail_message_oid"));
				documentImageInstance.setAttribute("mail_message_oid", "0");
				documentImageInstance.save();
			}
		}
		return outputDoc;
	}
	


}