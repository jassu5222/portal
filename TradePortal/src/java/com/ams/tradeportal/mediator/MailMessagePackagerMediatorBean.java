package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.*;
import com.ams.tradeportal.mediator.util.BankUpdateCentreProcessor;
import com.ams.tradeportal.mediator.util.InvoicePackagerUtility;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import javax.ejb.*;

import java.rmi.*;
import java.util.Hashtable;
import java.util.Vector;

/*
 * Manages MailMessage Packaging..
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public class MailMessagePackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(MailMessagePackagerMediatorBean.class);


  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                                                                    throws RemoteException, AmsException
  {
   try {

        //String eventTransactionOid = inputDoc.getAttribute("/transaction_oid");
        String eventInstrumentOid  = null;
        String mailMessageOid      = inputDoc.getAttribute("/mail_message_oid");
        String messageID   = null;
        String messageType = TradePortalConstants.QUEUE_MESSAGE_TYPE_MAIL; //Is "MAIL" right now
        ClientBank clientBank         = (ClientBank) mediatorServices.createServerEJB("ClientBank");
        OperationalBankOrganization  operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization");
        CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
        Instrument instrument         = (Instrument)  mediatorServices.createServerEJB("Instrument");
        User user         = (User) mediatorServices.createServerEJB("User");
        
        messageID    = inputDoc.getAttribute("/message_id");
        prePopulateMessage(outputDoc);
        mediatorServices.debug("This is the outputDoc " + outputDoc.toString());

        //Get The Required Objects
        MailMessage mailMessage = (MailMessage)  mediatorServices.createServerEJB("MailMessage",  Long.parseLong(mailMessageOid));
        eventInstrumentOid = mailMessage.getAttribute("related_instrument_oid");
        mediatorServices.debug("Ready to Instantiate Instrument Object");
	    if(!(eventInstrumentOid == null || eventInstrumentOid.equals("")))
	     instrument.getData(Long.parseLong(eventInstrumentOid));

        mediatorServices.debug("Ready to instantiate transaction  " );
     //   String a_client_bank_oid = instrument.getAttribute("client_bank_oid");
        boolean packageStatus = true;
	   // mediatorServices.debug("Ready to instantiate clientBank  with id :"+a_client_bank_oid);
	/*    if(!(a_client_bank_oid == null || a_client_bank_oid.equals("") ))
	    {
	    clientBank.getData(Long.parseLong(a_client_bank_oid));
	    }

	    String a_op_bank_org_oid = instrument.getAttribute("op_bank_org_oid");
		mediatorServices.debug("Ready to instantiate operBankOrg with Id = : " + a_op_bank_org_oid);
	   if(!(a_op_bank_org_oid==null || a_op_bank_org_oid.equals("")))
        {
		operationalBankOrganization.getData(Long.parseLong(a_op_bank_org_oid));
		}
   */
        String corp_org_oid = instrument.getAttribute("corp_org_oid");

        mediatorServices.debug("This is instrument corp_org :"  + corp_org_oid);
        String m_corp_org_oid = mailMessage.getAttribute("assigned_to_corp_org_oid");

        mediatorServices.debug("This is mailmessage corp_org :"  + m_corp_org_oid);

        mediatorServices.debug("Ready to instantiate corpOrg with id = : " + corp_org_oid );
        if(!(m_corp_org_oid== null || m_corp_org_oid.equals("")))
        	corpOrg.getData(Long.parseLong(m_corp_org_oid));
        else
        {
        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_ASSOCIATED_VALUE,"CorporateOrg","MailMessage");
        	packageStatus = false;
	    }
	    mediatorServices.debug("Ready to get different Objects Attributes : " );
        mediatorServices.debug("Ready to get Instrument Attributes : ");

        String a_client_bank_oid = corpOrg.getAttribute("client_bank_oid");
        if(!(a_client_bank_oid == null || a_client_bank_oid.equals("") ))
 	    {
        	clientBank.getData(Long.parseLong(a_client_bank_oid));
	    }

        String first_op_bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
        if(!(first_op_bank_org_oid==null || first_op_bank_org_oid.equals("")))
        {
        	operationalBankOrganization.getData(Long.parseLong(first_op_bank_org_oid));
	    }
        //IR T36000013842 start
        String m_last_user_oid = mailMessage.getAttribute("last_entry_user_oid");
        if(StringFunction.isNotBlank(m_last_user_oid))
        {
        	user.getData(Long.parseLong(m_last_user_oid));
	    }
        //IR T36000013842 end
        if(packageStatus == true)
        {

        	packageheader(messageType,messageID,operationalBankOrganization,outputDoc);

        	mediatorServices.debug("Ready to get subHeader Attributes : ");
        	
        	packagesubHeader(corpOrg,clientBank,instrument,outputDoc);
        	
        	mediatorServices.debug("Ready to get mailMessage Attributes : ");

        	packagemailMessage(user,mailMessage,outputDoc,mediatorServices, instrument, corpOrg); //IR T36000013842 -- Pass user object
        
        	// [BEGIN] CR-186 - jkok
        	mediatorServices.debug("Ready to get AttachedDocuments Attributes : ");
        	packageAttachedDocuments(mailMessageOid, outputDoc);
        	// [END] CR-186 - jkok
        	
        	// Nar CR-1029 06/11/2015 Rel 9.4 Begin
        	if ( TradePortalConstants.INDICATOR_YES.equals(InvoicePackagerUtility.getParm("DoNotSendMQ", inputDoc.getAttribute("/process_parameters")))) {
        	  BankUpdateCentreProcessor.createMailMessageToBank( outputDoc, mediatorServices );
        	}
        	// Nar CR-1029 06/11/2015 Rel 9.4 End
       }

       else
       {
    	   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"MailMessage Packaging");
    	   return outputDoc;
       }

        mediatorServices.debug("Final outputDoc is here :" + "\n" + outputDoc.toString());

  }
    catch (RemoteException e) {
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"MailMessage Packaging");
      e.printStackTrace();
    }
    catch (AmsException e) {
       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"MailMessage Packaging");
        e.printStackTrace();

    }
    catch (Exception e) {
       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"MailMessage Packaging");
       e.printStackTrace();

    }
    return outputDoc;
  }



    /**
	  * This method takes the CorporateOrganization, ClientBank, Instrument objects
	  * and outputDoc and populates the outputDoc with the subHeader values required.
	  *
	  * @param corpOrg,     CorporateOrganization object whose values are being packaged
	  * @param clientBank,  ClientBank object whose values are being packaged
	  * @param instrument,  Instrument object whose values are being packaged
	  * @param transaction, Transaction object whose values are being packaged
	  * @param outputDoc, DocumentHandler object which is the final output with
	  *  packaged values.
	  * author iv
      */

  public void packagesubHeader(CorporateOrganization corpOrg, ClientBank clientBank, Instrument instrument, DocumentHandler outputDoc)
                                                                   throws RemoteException, AmsException
  {

	  outputDoc.setAttribute("/Proponix/SubHeader/CustomerID", corpOrg.getAttribute("Proponix_customer_id"));
	  outputDoc.setAttribute("/Proponix/SubHeader/ClientBank/OTLID", clientBank.getAttribute("OTL_id"));
	  outputDoc.setAttribute("/Proponix/SubHeader/InstrumentID", instrument.getAttribute("complete_instrument_id"));
	  outputDoc.setAttribute("/Proponix/SubHeader/InstrumentTypeCode", instrument.getAttribute("instrument_type_code"));

   }

     /**
	  * This method takes the messageType, messageID, OperationalBankOrganization and outputDoc
	  * to populate the outputDoc with the Header values required.
	  *
	  * @param String messageType
	  * @param String messageID
	  * @param OperationalBankOrganization
	  * @param DocumentHandler outputDoc which is the final output with
	  *  packaged values.
	  * author iv
      */

  public void packageheader(String messageType, String messageID,OperationalBankOrganization operationalBankOrganization, DocumentHandler outputDoc)
                                                                 throws RemoteException, AmsException
    {
	  /* Build the Header here */

         String headerPath = UniversalMessage.headerPath;

	     String destinationID    = TradePortalConstants.DESTINATION_ID_PROPONIX;
	     String senderID         = TradePortalConstants.SENDER_ID;    // NSX CR-542 03/04/10
	     String dateSent         = null;
	     String timeSent         = null;
	     String operationOrganizationID = null;

    	 operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
         String timeStamp   = DateTimeUtility.getGMTDateTime(false);
         dateSent    =  timeStamp;
         timeSent    =  timeStamp.substring(11,19);


         outputDoc.setAttribute(headerPath+"/DestinationID",destinationID);
         outputDoc.setAttribute(headerPath+"/SenderID",senderID);
         outputDoc.setAttribute(headerPath+"/OperationOrganizationID", operationOrganizationID);
         outputDoc.setAttribute(headerPath+"/MessageType",messageType);
         outputDoc.setAttribute(headerPath+"/DateSent", dateSent);
         outputDoc.setAttribute(headerPath+"/TimeSent",timeSent);
         outputDoc.setAttribute(headerPath+"/MessageID",messageID);

  }
      /**
	    * This method takes the MailMessage object and outputDoc and populates
	    * the outputDoc with the  values required.
	    * 
	    * @param corpOrg 
	    * @param instrument, instrument object to fetch contact details 
	    * @param mailMessage, MailMessage object whose values are being packaged
	    * @param outputDoc, DocumentHandler object which is the final output with
	    *  packaged values.
	    * author iv
        */
    public void packagemailMessage(User user,MailMessage mailMessage, DocumentHandler outputDoc,MediatorServices mediatorServices, Instrument instrument, CorporateOrganization corpOrg)
                                                                   throws RemoteException, AmsException
      {
        outputDoc.setAttribute("/Proponix/Body/MessageSubject", mailMessage.getAttribute("message_subject"));
        //IR T36000013842 start-  TPS bank user can see who the TP User sent the message.
        StringBuffer msg = new StringBuffer();
        msg.append(mailMessage.getAttribute("message_text"));
        if(user!=null){
        msg.append("---");
        msg.append(mediatorServices.getResourceManager().getText("mail.sentBy", TradePortalConstants.TEXT_BUNDLE));
        msg.append(" ");
        msg.append(user.getAttribute("first_name"));
        msg.append(" ");
        msg.append(user.getAttribute("last_name"));
        }
        //IR T36000013842 end
        outputDoc.setAttribute("/Proponix/Body/MessageText",    msg.toString());
        //RKAZI - 16-Mar-2016 -CR 1146 - START
        packageContactDetails(user, corpOrg, outputDoc, mediatorServices);
        //RKAZI - 16-Mar-2016 -CR 1146 - END
     }

    /**
     * Package Contact details to be sent to TPS as part of thsi message.
     * 
     * @param user
     * @param corpOrg
     * @param outputDoc
     * @param mediatorServices
     * @throws AmsException 
     * @throws RemoteException 
     */
    private void packageContactDetails(User user, CorporateOrganization corpOrg,
			DocumentHandler outputDoc, MediatorServices mediatorServices) throws RemoteException, AmsException {
    	
    	outputDoc.setAttribute("/Proponix/Body/Contact/CustomerName",    corpOrg.getAttribute("name"));
    	
    	StringBuilder userName = new StringBuilder("");
    	userName.append(user.getAttribute("first_name"));
    	userName.append(" ");
    	userName.append(user.getAttribute("last_name"));
        
    	outputDoc.setAttribute("/Proponix/Body/Contact/UserName",    userName.toString());
    	outputDoc.setAttribute("/Proponix/Body/Contact/UserPhoneNumber",    user.getAttribute("telephone_num"));
    	outputDoc.setAttribute("/Proponix/Body/Contact/UserEmailAddress",    user.getAttribute("email_addr"));
    	
        
	}



	public void prePopulateMessage(DocumentHandler outputDoc)
                                             throws RemoteException, AmsException
      {
		outputDoc.setAttribute("/Proponix/Header/DestinationID", "");
		outputDoc.setAttribute("/Proponix/Header/SenderID", "");
		outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID", "");
		outputDoc.setAttribute("/Proponix/Header/MessageType", "");
		outputDoc.setAttribute("/Proponix/Header/DateSent", "");
		outputDoc.setAttribute("/Proponix/Header/TimeSent", "");
		outputDoc.setAttribute("/Proponix/Header/MessageID", "");

		outputDoc.setAttribute("/Proponix/SubHeader/CustomerID", "");
	    outputDoc.setAttribute("/Proponix/SubHeader/ClientBank/OTLID", "");
		outputDoc.setAttribute("/Proponix/SubHeader/InstrumentID", "");

        outputDoc.setAttribute("/Proponix/Body/MessageSubject", "");
        outputDoc.setAttribute("/Proponix/Body/MessageText",    "");
     }

    /**
     * 
     * @param oid
     * @param outputDoc
     * @throws RemoteException
     * @throws AmsException
     */
    private void packageAttachedDocuments(String oid,
  			DocumentHandler outputDoc) throws RemoteException, AmsException {
  	  if((oid != null) && (oid.trim().length() > 0)) {

  		  String sql = "select "
  		      //cquinton 11/10/2011 Rel 7.1 cnul111048855 add hash
  			  +"IMAGE_ID, NUM_PAGES, IMAGE_FORMAT, IMAGE_BYTES, DOC_NAME, HASH "
  			  +"from DOCUMENT_IMAGE "
  			  +"where "
  			  // [BEGIN] IR-YYUH032255414 - jkok
  			  +"FORM_TYPE = ? "
  			  // [END] IR-YYUH032255414 - jkok
  			  +"and MAIL_MESSAGE_OID  = ?";
  		  LOG.debug("[MailMessagePackagerMediatorBean.packageAttachedDocuments] sql = "+sql);
  		  
  		  DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED, oid});
  		  if((resultXML != null) && (resultXML.getFragments("/ResultSetRecord/").size() > 0)) {
  			Vector resultVector = resultXML.getFragments("/ResultSetRecord/");
  			
  			// '/Proponix/Body/DocumentImage'
  			String documentImagePath = "/Proponix/Body/DocumentImage";

			outputDoc.setAttribute(documentImagePath+"/NumberOfEntries", String.valueOf(resultVector.size()));
			LOG.debug("[MailMessagePackagerMediatorBean.packageAttachedDocuments] After adding to '"+documentImagePath+"/NumberOfEntries"+"' outputDoc = "+outputDoc.toString());
  			
  			for(int i = 0; i < resultVector.size(); i++) {
  				DocumentHandler myDoc = (DocumentHandler) resultVector.elementAt(i);
  				LOG.debug("[MailMessagePackagerMediatorBean.packageAttachedDocuments] myDoc = "+myDoc.toString());
  				
  				// [BEGIN] IR-YYUH032255414 - jkok
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/FormType",
  						TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED.substring(0,1).toUpperCase());
  				// [END] IR-YYUH032255414 - jkok
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageID", myDoc.getAttribute("/IMAGE_ID"));
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/DocName", myDoc.getAttribute("/DOC_NAME"));
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageFormat", myDoc.getAttribute("/IMAGE_FORMAT"));
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageBytes", myDoc.getAttribute("/IMAGE_BYTES"));
  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/NumPages", myDoc.getAttribute("/NUM_PAGES"));
                //cquinton 11/10/2011 Rel 7.1 cnul111048855 add hash
  				String hash = myDoc.getAttribute("/HASH");
  				if ( hash != null && hash.length()>0 ) {
                    outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/Hash", myDoc.getAttribute("/HASH"));
  				}
  			} // for(int i = 0; i < resultVector.size(); i++) {
  			
  			LOG.debug("[MailMessagePackagerMediatorBean.packageAttachedDocuments] After adding attached documents outputDoc = "+outputDoc.toString());
  			
  		  } // if((resultXML != null) && (resultXML.getFragments("/ResultSetRecord/").size() > 0)) {
  	  } // if((oid != null) && (oid.trim().length() > 0)) {
  	} // private void packageAttachedDocuments(String oid, DocumentHandler outputDoc) throws RemoteException, AmsException {
}

