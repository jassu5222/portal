package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Vector;

import com.ams.tradeportal.busobj.CustomerDiscountCodeBean;
import com.ams.tradeportal.busobj.MatchPayDedGlDtls;
import com.ams.tradeportal.busobj.PayMatchResult;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class MatchPayDedGlDtlsMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(MatchPayDedGlDtlsMediatorBean.class);

	public static final String INSERT_MODE = "C";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		String updateButtonValue = null;
		String deleteButtonValue = null;
		String currencyCode = null;
		String userLocale = null;
		String updateMode = this.UPDATE_MODE;
		long theOid = 0;
		MatchPayDedGlDtls matchPayDedGlDtls = null;
		Vector matchPayFragments = new Vector();
		DocumentHandler doc;
		String invoiceRef ="";
		String transType = "";
		String discDescription = "";
		String appliedAmount = "";
		String discComments = "";
		String discCode = "";
		String glCode = "";
		String otlinvoiceoid = "";
		//Totals
		String invOSAmount ="";
		String payTotal = "";
		String overPayTotal = "";
		String discountTotal = "";
		String theDate = DateTimeUtility.getCurrentDate();
		String currentDtTime = DateTimeUtility.getCurrentDateTime();
		String invPayDate = "";
		String remitOid = "";
		StringBuilder sqlStmntStr = new StringBuilder();
		String matchedAmount = "";
		//IR#T36000018260 
		String payMatchResultOid = "";
		//Srinivasu_D CR#997 Rel9.3 04/03/2015 - Added below property to indicate if any server validation errors occured
		//and to notify to UI for show pop up again.
		String errorInd = "";
		updateButtonValue = inputDoc.getAttribute("/Action/saveXPos");
		deleteButtonValue = inputDoc.getAttribute("/Action/deleteXPos");

		if (updateButtonValue != null) {
			updateMode = this.UPDATE_MODE;
		} else if (deleteButtonValue != null) {
			updateMode = this.DELETE_MODE;
		}

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);

		if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVE))
			updateMode = this.UPDATE_MODE;
		else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE))
			updateMode = this.DELETE_MODE;

		if (updateMode == null) {
			throw new AmsException("Unable to determine update mode.");
		}
		try {
			invOSAmount = inputDoc.getAttribute("/Amounts/invTotalAmount");
			discountTotal = inputDoc.getAttribute("/Amounts/discountTotal");
			overPayTotal = inputDoc.getAttribute("/Amounts/overPayTotal");
			payTotal = inputDoc.getAttribute("/Amounts/payTotal");
			errorInd = inputDoc.getAttribute("/Amounts/payMatchErrorInd");
			//Invoice Date
			invPayDate = inputDoc.getAttribute("/Dates/invPayDate");
			payMatchResultOid = inputDoc.getAttribute("/Misc/remitOid");//This corresponds to the pay match result oid
			currencyCode = inputDoc.getAttribute("/Amounts/invPayCurrency");
			//invoice ref id to use for obtaining matched amount
		
			String invoiceRefId = inputDoc.getAttribute("/MatchPayDedGlDtls[1]/invoice_reference_id[1]");
			
			sqlStmntStr.append("select P_PAY_REMIT_OID, matched_amount,pending_matched_amount");
			sqlStmntStr.append(" from pay_match_result"); 
			sqlStmntStr.append(" where PAY_MATCH_RESULT_OID = ?");
			//AiA 8/20 -IR#T36000019620 End
			//
			System.out.print("Test Match Amount step 1= "+sqlStmntStr.toString());
			DocumentHandler matchedDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr.toString(), false, new Object[]{payMatchResultOid});

			if (matchedDoc != null){
				//matchedAmount = matchedDoc.getAttribute("/ResultSetRecord/MATCHED_AMOUNT/"); 
				matchedAmount = matchedDoc.getAttribute("/ResultSetRecord/PENDING_MATCHED_AMOUNT/"); 
				System.out.print("Test Match Amount step 2= "+matchedAmount);
				//IR#T36000018260  - OBTAIN THE PAY MATCH RESULT OID FOR THE CURRENT INVOICE
				//payMatchResultOid = matchedDoc.getAttribute("/ResultSetRecord/PAY_MATCH_RESULT_OID/");
				remitOid = matchedDoc.getAttribute("/ResultSetRecord/P_PAY_REMIT_OID/");
				
			}			
	
			java.text.NumberFormat format = java.text.NumberFormat.getNumberInstance();//.getInstance(mediatorServices.resMgr.getResourceLocaleValue());

			if(StringFunction.isNotBlank(invOSAmount) && StringFunction.isNotBlank(discountTotal) && StringFunction.isNotBlank(overPayTotal) && StringFunction.isNotBlank(payTotal)){						
				double invOutstandingAmount = format.parse(invOSAmount).doubleValue();
				double discount = format.parse(discountTotal).doubleValue();
				double overPay = format.parse(overPayTotal).doubleValue();
				double pay = format.parse(payTotal).doubleValue();
				//create a temp outstanding amount
				if(StringFunction.isNotBlank(matchedAmount)){
					double matchedAmnt = format.parse(matchedAmount).doubleValue();
					invOutstandingAmount = invOutstandingAmount+matchedAmnt;
				}
				boolean overPayIsZero = false;
				double totalPayAndDiscount = pay + discount + overPay;
				//AiA IR#T36000018263 06/14/2013
				double onlyPayAndDiscount = pay + discount;
				
				//IR 22112  start  - Use BigDecimal.ZERO.compareTo
				/*
				if(totalPayAndDiscount == onlyPayAndDiscount){
					//Implies overPay is zero
					overPayIsZero = true;
				}
				
				//It is tricky when dealing with doubles and want to compare to zero!
				//if(overPay == 0){	
				//BigDecimal zero = new BigDecimal("0.0");
				//if(BigDecimal.ZERO.doubleValue() == zero.doubleValue()){
				//if(overPayIsZero){	
				 
				 */
				System.out.print("Test Match Amount step 3= "+onlyPayAndDiscount +"outstn="+invOutstandingAmount+"overpay="+overPay);
				if(BigDecimal.ZERO.compareTo(BigDecimal.valueOf(overPay)) == 0){
					//IR 22112- use only pay+discount  no overpay
					if(onlyPayAndDiscount > invOutstandingAmount){
					//if(totalPayAndDiscount > invOutstandingAmount){
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.TOTAL_AMT_EXCEEDS_OUTSTANDING_AMT);	
						//Srinivasu_D CR#997 Rel9.3 04/03/2015 - Setting Y to say validation error occured
						outputDoc.setAttribute("errorInd","Y");
						return outputDoc;	
					}
				}else if(overPay > 0){
					//IR 22112 - use only pay+discount  no overpay
					if(onlyPayAndDiscount < invOutstandingAmount){
						//if(totalPayAndDiscount < invOutstandingAmount){
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.EXCESS_PAY_NOT_EXCEED_OUTSTANDING_AMT);
						//Srinivasu_D CR#997 Rel9.3 04/03/2015 - Setting Y to say validation error occured
						outputDoc.setAttribute("errorInd","Y");
						return outputDoc;	
					}					
				}
			}			
			
			
	
			
			//
			// Get all fragments from the inputdoc corresponding to each row
			// add them to the bean and save row at a time
			//
			matchPayFragments = inputDoc.getFragments("/MatchPayDedGlDtls/");
			if (matchPayFragments != null) {
				for (int i = 0; i < matchPayFragments.size(); i++) {
					doc = (DocumentHandler) matchPayFragments.elementAt(i);
					theOid = doc.getAttributeLong("/match_pay_ded_gl_dtls_oid");
					invoiceRef = doc.getAttribute("/invoice_reference_id");
					glCode = doc.getAttribute("/general_ledger_code");
					discCode = doc.getAttribute("/discount_code");
					discComments = doc.getAttribute("/discount_comments");
					appliedAmount = doc.getAttribute("/applied_amount");
					discDescription = doc.getAttribute("/discount_description");
				
					transType = doc.getAttribute("/transaction_type");
					
					if (theOid == 0) {
						updateMode = this.INSERT_MODE;
					}

					// Get a handle to an instance of the object
					matchPayDedGlDtls = (MatchPayDedGlDtls) mediatorServices
					.createServerEJB("MatchPayDedGlDtls");

					if (updateMode.equals(this.INSERT_MODE)) {
						mediatorServices
						.debug("Mediator: Creating new MatchPay");

						matchPayDedGlDtls.newObject();
					} else {
						mediatorServices.debug("Mediator: Retrieving MatchPay "
								+ theOid);

						matchPayDedGlDtls.getData(theOid);
					}

					if (updateMode.equals(this.DELETE_MODE)) {
						mediatorServices
						.debug("Mediator: Deleting MatchPay object");
						// Delete the MatchPay
						matchPayDedGlDtls.delete();
						mediatorServices.debug("Mediator: Completed the delete of the MatchPay object");
					} else {
						//Validations:
						//If the whole row is empty, skip it. Only validate when at least one item is entered
						//
						//if(StringFunction.isBlank(transType) && StringFunction.isBlank(discDescription) && StringFunction.isBlank(appliedAmount) && StringFunction.isBlank(discComments)&& StringFunction.isBlank(discCode) && StringFunction.isBlank(glCode)){
						if(StringFunction.isBlank(transType) &&  StringFunction.isBlank(appliedAmount)){	
							//skip empty row
							if(theOid > 0){
								//don't skip even though trans type and applied amount is empty
							}else
							continue;
						}else{
							//Transaction Type
							if(StringFunction.isBlank(transType)){						
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.TRANSACTION_TYPE_REQUIRED);	
									outputDoc.setAttribute("errorInd","Y");
									return outputDoc;						
							}
							//Applied Amount
							BigDecimal enteredAmount = BigDecimal.ZERO;
							if(StringFunction.isBlank(appliedAmount)){						
								mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.APPLIED_AMOUNT_REQUIRED);	
								outputDoc.setAttribute("errorInd","Y");
								return outputDoc;						
							}
							//UB Issue start- validate amount using currency code
							
							else if(mediatorServices.getErrorManager().getErrorCount()==0){	//side effect
								 TPCurrencyUtility.validateAmount(currencyCode, appliedAmount, mediatorServices.getResourceManager().getText(
											"PaymentDiscountDetail.AppliedAmount",
											TradePortalConstants.TEXT_BUNDLE), mediatorServices.getErrorManager() );
								 try{
									 	appliedAmount = NumberValidator.getNonInternationalizedValue(appliedAmount, 
												mediatorServices.getCSDB().getLocaleName(), false);
										 enteredAmount = new BigDecimal(appliedAmount);
							     		 //enteredAmount.doubleValue(); 
									}
								 catch (Exception e) {
									 String[] substitutionValues = {"", ""};
									 substitutionValues[0] = mediatorServices.getResourceManager().getText("PaymentDiscountDetail.AppliedAmount", TradePortalConstants.TEXT_BUNDLE);
										substitutionValues[1] = appliedAmount;
									 mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.INVALID_VALUE,substitutionValues);
									 outputDoc.setAttribute("errorInd","Y");
									 return outputDoc;
								 }
							//if(!InstrumentServices.containsOnlyNumbers(appliedAmount)){						
							/*	mediatorServices.getErrorManager().issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.APPLIED_AMT_BE_GREATER_THAN_ZERO);*/
								 if(mediatorServices.getErrorManager().getErrorCount()>0)
								return outputDoc;		
								//UB Issue end
							}	
							//If Discount Trans Type is selected, then Discount should be selected
							if(TradePortalConstants.ERP_DISCOUNT.equalsIgnoreCase(transType)){
								if(StringFunction.isBlank(discCode)){						
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.DISCOUNT_CODE_REQUIRED_FOR_DISC_TRANSACTION);	
									outputDoc.setAttribute("errorInd","Y");
									return outputDoc;
								}								
							}
							//If Pay or Over pay Trans Type is selected, and Discount Code is selected
							if((TradePortalConstants.ERP_OVERPAYMENT.equalsIgnoreCase(transType) || TradePortalConstants.ERP_PAYMENT.equalsIgnoreCase(transType)) && StringFunction.isNotBlank(discCode)){
								//if(StringFunction.isBlank(discCode)){						
									mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.DISCOUNT_CODE_NOT_ALLOWED_FOR_PAY);
									outputDoc.setAttribute("errorInd","Y");
									return outputDoc;
								//}								
							}	
							
							
						}
						
						// IR 31289 SureshL 6/10/2015 REL9.3 
						sqlStmntStr =new StringBuilder();
						sqlStmntStr.append("SELECT * from pay_match_result where "); 
					    sqlStmntStr.append(" invoice_reference_id = ?");
						
					    DocumentHandler matchedDoc1 = DatabaseQueryBean.getXmlResultSet(sqlStmntStr.toString(), false, new Object[]{invoiceRefId});
						
						otlinvoiceoid = matchedDoc1.getAttribute("/ResultSetRecord/OTL_INVOICE_UOID"); 	
						
						matchPayDedGlDtls.setAttribute("invoice_reference_id",invoiceRef);
						matchPayDedGlDtls.setAttribute("otl_invoice_oid",otlinvoiceoid);
						matchPayDedGlDtls.setAttribute("transaction_type",transType);						
						
						
						matchPayDedGlDtls.setAttribute("applied_amount",appliedAmount);
						//
						//Save only Discount items if it is a discount
						if(StringFunction.isNotBlank(transType) && transType.equalsIgnoreCase(TradePortalConstants.ERP_DISCOUNT)){
							//lookup the description if not available
							if(StringFunction.isBlank(discDescription))
								discDescription = getDiscountDescription(discCode);
							matchPayDedGlDtls.setAttribute("discount_comments",discComments);
							matchPayDedGlDtls.setAttribute("discount_code",discCode);
							matchPayDedGlDtls.setAttribute("discount_description",discDescription);
						}
						matchPayDedGlDtls.setAttribute("general_ledger_code",glCode);
						matchPayDedGlDtls.setAttribute("system_date_time",currentDtTime);							
						//
						matchPayDedGlDtls.setAttribute("a_initiating_obj",remitOid);
					
						matchPayDedGlDtls.setAttribute("initiating_obj_cls","PayRemit"); //Currently this is set to PayRemit i.e in Portal
						matchPayDedGlDtls.setAttribute("transaction_date",theDate);	

						if(StringFunction.isNotBlank(transType) || theOid > 0){
							//delete any blank entries - transaction type blank records to be deleted
							if(StringFunction.isBlank(transType) && theOid > 0){
								matchPayDedGlDtls.delete();
							}else
								matchPayDedGlDtls.save();
						}

					}
				}
				//IR T36000018280 start
				sqlStmntStr =new StringBuilder();
				//IR 22112 start - consider PAY + OVER_PAY for match amount
				//sqlStmntStr.append("SELECT sum(applied_amount) as AMT from match_pay_ded_gl_dtls where transaction_type='");
				//sqlStmntStr.append(TradePortalConstants.ERP_PAYMENT); 
				sqlStmntStr.append("SELECT sum(applied_amount) as AMT from match_pay_ded_gl_dtls where transaction_type in (?,?) and "); 
				//IR 22112
				sqlStmntStr.append(" invoice_reference_id = ?");
				DocumentHandler matchedDoc1 = DatabaseQueryBean.getXmlResultSet(sqlStmntStr.toString(), false, new Object[]{TradePortalConstants.ERP_PAYMENT,
																				TradePortalConstants.ERP_OVERPAYMENT, invoiceRefId});
				String newMatchedAmount ="";
				if (matchedDoc1 != null){
					newMatchedAmount = matchedDoc1.getAttribute("/ResultSetRecord/AMT"); 
				}
				//IR T36000018280 end
				
				//AiA - 06/15/2013
				//IR#T36000018260 
				//Save the Total Discount in Pay Match Result for the current invoice (invoice reference id)
				//
				if(StringFunction.isNotBlank(payMatchResultOid)){
					PayMatchResult payMatchResultBean = (PayMatchResult) mediatorServices.createServerEJB("PayMatchResult",Long.parseLong(payMatchResultOid));
					//payMatchResultBean.getData(Long.valueOf(payMatchResultOid).longValue());
					//set the discount applied attribute
					//UB issue - Make sure discountTotal is in correct decimal format(excluding commas)
					discountTotal = NumberValidator.getNonInternationalizedValue(discountTotal, 
							mediatorServices.getCSDB().getLocaleName(), false);
					payMatchResultBean.setAttribute("discount_applied", discountTotal);
					//IR T36000018280 start Total of the applied amounts where transaction type = Payment is copied over to the match amount field
					if(StringFunction.isNotBlank(newMatchedAmount))
					payMatchResultBean.setAttribute("matched_amount", newMatchedAmount);
					//IR T36000018280 end
					//save the bean
					payMatchResultBean.save(true);
				}
				//
				//END
			}
		} catch (Exception e) {
			String serverErrorNumber = "SE" + System.currentTimeMillis();
			LOG.info("General exception caught in RefDataMediator. "
					+ "Server error#" + serverErrorNumber);
			e.printStackTrace();
			// we don't know exactly what this is, so just issue a generic error
			// another option is to just propogate the exception
			outputDoc.setAttribute("errorInd","Y");
			mediatorServices.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.SERVER_ERROR, serverErrorNumber);
		} finally {

			mediatorServices.debug("Mediator: Result of FX rate update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {

				// Save the update record to the ref data audit log
				mediatorServices.debug("Mediator: Creating audit record");

				mediatorServices.debug("Mediator: Finished with audit record");
				//LOG.info("errorInd:"+errorInd);
				// Finally issue a success message based on update type.
				if (updateMode.equals(this.DELETE_MODE)) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.DELETE_SUCCESSFUL, invoiceRef);
				} else if (updateMode.equals(this.INSERT_MODE)) {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.INSERT_SUCCESSFUL, invoiceRef);
				} else {
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.UPDATE_SUCCESSFUL, invoiceRef);
				}
			}

		}

		return outputDoc;
	}

	private String getDiscountDescription(String code){
		String desc = "";
		String discSQL = "SELECT DISCOUNT_DESCRIPTION as DISCOUNT_DESCRIPTION FROM CUSTOMER_DISCOUNT_CODE WHERE DISCOUNT_CODE = ? union select descr as DISCOUNT_DESCRIPTION from bankrefdata where code = ?";

		//String discSQL = "SELECT DISCOUNT_DESCRIPTION FROM CUSTOMER_DISCOUNT_CODE WHERE DISCOUNT_CODE = '" + code+"'";
			 
		DocumentHandler discCodeDoc;
		try {
			discCodeDoc = DatabaseQueryBean.getXmlResultSet(discSQL, false, new Object[]{code, code});			
			if (discCodeDoc != null){
				desc = discCodeDoc.getAttribute("/ResultSetRecord(0)/DISCOUNT_DESCRIPTION");
			}		
		} catch (AmsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return desc;
	}

}