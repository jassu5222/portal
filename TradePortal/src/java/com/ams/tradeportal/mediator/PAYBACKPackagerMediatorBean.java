
package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.Date;
import java.util.TreeMap;

import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.busobj.PaymentUploadErrorLog;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.*;
/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PAYBACKPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PAYBACKPackagerMediatorBean.class);

   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                    throws RemoteException, AmsException{

        try {

            String dateSent                 = null;
            String timeSent                 = null;
            String processingStatus         = "";
            String errorText                = "";
            Transaction transaction			= null;
            String partialPayAuthInd		= null;
            //String messageDetail 			= "";

            String unpackaging_error_text   = inputDoc.getAttribute("/unpackaging_error_text"); //non-empty in case of error
            String processParameters = inputDoc.getAttribute("/process_parameters");
            DocumentHandler processParamDoc = new DocumentHandler(processParameters,false);
            
          //PMitnala Rel 9.1 CR 970 - START
            String transactionOid = inputDoc.getAttribute("/transaction_oid");
            if(StringFunction.isNotBlank(transactionOid)) {
	            transaction = (Transaction)  mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
	            processingStatus = transaction.getAttribute("transaction_status");
	            
	        	if(TransactionStatus.VERIFIED_PENDING_FX.equals(transaction.getAttribute("transaction_status")) &&
	        			   TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")))
	        			{
	        				MediatorServices medService = new MediatorServices();
	        				IssuedError issErr = null;
	        				Date date     = null;
	        			 	SimpleDateFormat sdf      = null;
	        			  	SimpleDateFormat iso      = null;
	        			  	String result   = transaction.getAttribute("payment_date");
	        			   
	        			   	sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
	        			   	iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");   
	        			   		try{
	        			   	  		date = iso.parse(result);   
	        			   	  		result = sdf.format(date);
	        			      	}
	        			      	catch (Exception e)
	        			      	{   
	        			   		}
	        					   
	        				if(TPDateTimeUtility.isFutureDate(result)){
	        					issErr = medService.getErrorManager().getIssuedError(null,TradePortalConstants.INFO_DOM_FUTURE_DEB_CRED_DIFF, null, null);
	        			  	}else{
	        					issErr = medService.getErrorManager().getIssuedError(null,transaction.getAttribute("authorization_errors"), null, null);
	        				}
	        				if(issErr!=null) 
	        					errorText = issErr.getErrorText();
	        					errorText = errorText.replaceAll("-", "");
	        			} 
            }
          //PMitnala Rel 9.1 CR 970 - END
            
            try{
            	Date gmtDate = GMTUtility.getGMTDateTime();
            	dateSent    =  DateTimeUtility.formatDate(gmtDate,"MM/dd/yyyy");
            	timeSent    =  DateTimeUtility.formatDate(gmtDate,"HH:mm:ss");
            }
            catch(Exception e){
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_PACKAGER_TIMESTAMP);
                e.printStackTrace();
                return outputDoc;
            }

            
        	if (InstrumentServices.isNotBlank(unpackaging_error_text)) {
        		DocumentHandler errorDoc;
        		DocumentHandler doc = new DocumentHandler(unpackaging_error_text, false);
        		StringBuffer sb = new StringBuffer();
        		Vector  errorList   = doc.getFragments("/Error/errorlist/error");
        		int errorListSize   = errorList.size();

        		for (int i = 0; i < errorListSize; i++){
        			errorDoc = (DocumentHandler) errorList.elementAt(i);
        			sb.append(errorDoc.getAttribute("/message"));
        			sb.append("  ");
        		}
        		errorText = sb.toString();
        		errorText = errorText.replaceAll("\'", "");//IR T36000032974 
        		//messageDetail = doc.getAttribute("/In/Proponix/Body/MessageDetail");
        		processingStatus = TradePortalConstants.PAYBACK_STATUS_FILE_REJECTED; //Rel 9.1 CR 970 - Changed status 
        	}
        	else 
        	{
        		 if(StringFunction.isBlank(processingStatus)) { //That means no transaction has been created
        			 processingStatus = TradePortalConstants.PAYBACK_STATUS_SUCCESS;
        		 }
            }
        	
        	//Rel9.2 CR 988 BEGIN - Modify processing status if partial payment authorisation is allowed
        	String sql = "Select payment_file_upload_oid,allow_partial_pay_auth from payment_file_upload where message_id=?" ; 
        	DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{inputDoc.getAttribute("/reply_to_message_id")});
        	String paymentFileUploadOid = "";
        	int paymentUploadErrorLogTotal = 0;
        	int paymentUploadRepErrorLogTotal = 0;
        	if ( resultSet != null) {
    			partialPayAuthInd = resultSet.getAttribute("/ResultSetRecord/ALLOW_PARTIAL_PAY_AUTH");
    			paymentFileUploadOid = resultSet.getAttribute("/ResultSetRecord/PAYMENT_FILE_UPLOAD_OID");
    			
    			sql = "select payment_upload_error_log_oid, beneficiary_info, error_msg, line_number from PAYMENT_UPLOAD_ERROR_LOG where P_PAYMENT_FILE_UPLOAD_OID = ? and REPORTING_CODE_ERROR_IND = ? order by line_number asc";
				resultSet = DatabaseQueryBean.getXmlResultSet(sql,   false,  new Object[]{paymentFileUploadOid, TradePortalConstants.INDICATOR_NO});
				if (resultSet != null){
	    			List resultSetList = resultSet.getFragmentsList("/ResultSetRecord");
	            	paymentUploadErrorLogTotal = resultSetList.size();
				}
				
				sql = "select payment_upload_error_log_oid, beneficiary_info, error_msg, line_number from PAYMENT_UPLOAD_ERROR_LOG where P_PAYMENT_FILE_UPLOAD_OID = ? and REPORTING_CODE_ERROR_IND = ? order by line_number asc";
				resultSet = DatabaseQueryBean.getXmlResultSet(sql,   false,  new Object[]{paymentFileUploadOid, TradePortalConstants.INDICATOR_YES});
				if (resultSet != null){
	    			List resultSetList = resultSet.getFragmentsList("/ResultSetRecord");
	            	paymentUploadRepErrorLogTotal = resultSetList.size();
				}

        	}

        	if(transaction != null){
	        	String transStatus = transaction.getAttribute("transaction_status");
	        	 if( (TransactionStatus.AUTHORIZED.equals(transStatus) || 
	        	      TransactionStatus.FVD_AUTHORIZED.equals(transStatus)  ||
	        	      TransactionStatus.VERIFIED_PENDING_FX.equals(transStatus) ||
	        	             TransactionStatus.READY_TO_CHECK.equals(transStatus)) && 
	        	     (StringFunction.isNotBlank(partialPayAuthInd) && TradePortalConstants.INDICATOR_YES.equals(partialPayAuthInd)) && paymentUploadErrorLogTotal>0) 
	        	{
	        	processingStatus = TransactionStatus.PARTIAL_PAYMENT.concat(transStatus);
	        	}
	        	 else if(TransactionStatus.REPORTING_CODES_REQUIRED.equals(transStatus) && 
        	     (StringFunction.isNotBlank(partialPayAuthInd) && TradePortalConstants.INDICATOR_YES.equals(partialPayAuthInd))) {
	        		 //CR-1001 Scenario:1-If both beneficiary & reporting code errors are present, if partialPayAuth=Y, then beneficiary errors are automatically confirmed.
	        		 //Set the status to PARTIAL_REPORTING_CODES_REQUIRED
	        		//CR-1001 Scenario:2-If only reporting code errors are present, if partialPayAuth=Y, then Set the status to REPORTING_CODES_REQUIRED
	        		 if(paymentUploadErrorLogTotal>0 && paymentUploadRepErrorLogTotal>0){
	        			 processingStatus = TransactionStatus.PARTIAL_PAYMENT.concat(transStatus);
	        		 }else if(paymentUploadErrorLogTotal == 0 && paymentUploadRepErrorLogTotal>0){
	        			 processingStatus = transStatus;
	        		 }
	        	 }
	        	 
	        	if(TransactionStatus.REPORTING_CODES_REQUIRED.equals(transStatus)){
	         		errorText =mediatorServices.getResourceManager().getText("PaymentUploadLogDetail.H2HReportingErrMessage", TradePortalConstants.TEXT_BUNDLE);
	         	}
	        	//CR-1001 Scenario:3-If both bene & reporting code errors are present, if partialPayAuth=N, then DO NOT package Reporting code errors in PAYBACK msg
	        	 //This indicator is used within AgentsUtility.addPaybackErrors
	        	 if((StringFunction.isNotBlank(partialPayAuthInd) && TradePortalConstants.INDICATOR_NO.equals(partialPayAuthInd)) &&
	        			 paymentUploadErrorLogTotal>0 && paymentUploadRepErrorLogTotal>0 ){
	        		 outputDoc.setAttribute("/Param/PackageReportingCodeErrors",  TradePortalConstants.INDICATOR_NO);
	        	 }
        	}
        	
        	
        	//Rel9.2 CR 988 END
        	 
            outputDoc.setAttribute("/Proponix/Header/DestinationID",            processParamDoc.getAttribute("/SenderID")); // Nar CR694A Rel9.0
            outputDoc.setAttribute("/Proponix/Header/SenderID",                 TradePortalConstants.SENDER_ID);
            outputDoc.setAttribute("/Proponix/Header/ClientBank",               TradePortalConstants.SENDER_ID_ANZ);
            outputDoc.setAttribute("/Proponix/Header/MessageType",              inputDoc.getAttribute("/msg_type"));
            outputDoc.setAttribute("/Proponix/Header/DateSent",                 dateSent);
            outputDoc.setAttribute("/Proponix/Header/TimeSent",                 timeSent);
            outputDoc.setAttribute("/Proponix/Header/MessageID",                inputDoc.getAttribute("/message_id"));

            outputDoc.setAttribute("/Proponix/SubHeader/ReplytoMessageID",      inputDoc.getAttribute("/reply_to_message_id"));
            outputDoc.setAttribute("/Proponix/SubHeader/ProcessingStatus",      processingStatus);
            outputDoc.setAttribute("/Proponix/SubHeader/CustomerID",            processParamDoc.getAttribute("/CustomerID"));
            outputDoc.setAttribute("/Proponix/SubHeader/GXSRef",                processParamDoc.getAttribute("/GXSRef"));
            outputDoc.setAttribute("/Proponix/SubHeader/CustomerFileRef",       processParamDoc.getAttribute("/CustomerFileRef"));
            outputDoc.setAttribute("/Proponix/SubHeader/FileName",              processParamDoc.getAttribute("/FileName"));

            if (TradePortalConstants.PAYBACK_STATUS_REJECTED.equals(processingStatus) || StringFunction.isNotBlank(errorText)) {
                outputDoc.setAttribute("/Proponix/SubHeader/ErrorText",         errorText);
                //outputDoc.setAttribute("/Proponix/Body/MessageDetail",          messageDetail);
            }
            //RKAZI REL 9.1  T36000032474 10/31/2014 - Moved - Beneficiary Errors handling is now done from OutboundAgent to avoid StackOverflow

        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.CONFIRM_PACKAGER_GENERAL);
            e.printStackTrace();

        }
        
        return outputDoc;
  }

   
   /**
    * Added for Rel 9.1 CR 970 - Sorting the payment upload error logs based on line number
    * @param paymentUploadErrorLogComponent
    * @return
    * @throws AmsException
    * @throws RemoteException
    */
   public TreeMap<Integer, String> sortUploadErrors(ComponentList paymentUploadErrorLogComponent) throws AmsException, RemoteException
	{
		TreeMap<Integer,String> errorMap = new TreeMap<Integer, String>();
		int errorCount = paymentUploadErrorLogComponent.getObjectCount();
		for (int i =0 ; i < errorCount ; i++)
		{
			paymentUploadErrorLogComponent.scrollToObjectByIndex(i);
			errorMap.put(Integer.parseInt(paymentUploadErrorLogComponent.getListAttribute("line_number")), paymentUploadErrorLogComponent.getListAttribute("payment_upload_error_log_oid"));
		}
		return errorMap;
	}
   
}

