package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;






import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.UniversalMessage;

import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * Unpackager that handles PAYINVOT message.
 *
 * Copyright  � 2008
 * American Management Systems, Incorporated
 * All rights reserved
 */
public class PAYINVOTUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PAYINVOTUnPackagerMediatorBean.class);

    static private Hashtable payRemitAttributeMap;
    static private Hashtable sellerPartyAttributeMap;
    static private Hashtable buyerPartyAttributeMap;

    static final private String ACTION_TYPE_P = "P"; //Portal processing needed
    static final private String ACTION_TYPE_R = "R"; //Recall
    static final private String ACTION_TYPE_C = "C"; //Portal recall completed
    static final private String ACTION_TYPE_W = "W"; //Wait for Portal (portal recall not completed)

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
																			throws RemoteException, AmsException
	 {

        String actionType = unpackagerDoc.getAttribute("/Proponix/SubHeader/ActionType");
        if (actionType.equals(ACTION_TYPE_P)) {
             unpackagePayRemit(unpackagerDoc, outputDoc, mediatorServices);
        }
        else if (actionType.equals(ACTION_TYPE_R)) {
             recallPayRemit(unpackagerDoc, mediatorServices);
        }
        else {
            String[] parms = {actionType, "ActionType"};
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,AmsConstants.INVALID_ATTRIBUTE, parms);
        }
        return outputDoc;

     }

    /**
     * Unpackage XML to create Pay Remit
     *
     * @param unpackagerDoc
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */

    private void unpackagePayRemit(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
    throws RemoteException, AmsException

    {

        /* **************************************************************************
         * Check PayRemit is unique
         * **************************************************************************/
        PayRemit payRemit  = (PayRemit) mediatorServices.createServerEJB("PayRemit");
        String payRemitUoid = unpackagerDoc.getAttribute("/Proponix/SubHeader/OTLPayRemitUoid");
        CorporateOrganization corpOrg = null;
        String instrumentOid = null;
		String transactionOid = null;

        // Check the Pay Remit does not already exist.
        boolean existingFound = true;
        try {
            payRemit.find("otl_pay_remit_uoid", payRemitUoid);
        }
        catch (ObjectNotFoundException e) {
            existingFound = false;
        }
        catch (NameAndValueNotUniqueException e) {
            //existingFound = true;
        }
        // If no exception is thrown by find(), existingFound = true
        long payRemitOid;
        if (existingFound) {
            // W Zhu 3/5/2009 MUUJ022035910 
            // The same pay remit could be received again in case the previous manual match sent by TP 
            // is rejected by OTL.  For example, the invoice being matched could have been matched automatically
            // by another payment and therefore outstanding amount = 0.  
            payRemit.deleteComponent("PayMatchResultList");
            payRemit.deleteComponent("PayRemitInvoiceList");
            payRemit.setAttribute("first_authorizing_user_oid", null);
            payRemit.setAttribute("second_authorizing_user_oid", null);            
            payRemitOid = payRemit.getAttributeLong("pay_remit_oid");
            //mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.COMPONENT_ALREADY_EXISTS, "PayRemit with Uoid = " + payRemitUoid);
            //return;
            //Pavani Rel8.3.0.0 CR 821 deleted all entry from panel_authorizer table Begin
			 String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'PAY_REMIT' and p_owner_oid=?";
			 try {
				DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, false, new Object[]{payRemitOid});
			} catch (SQLException e) {
				mediatorServices.debug("PAYINVOTUnPackager: Error in deleting entry from PANEL_AUTHORIZER for p_owner_oid= " + payRemitOid );
				e.printStackTrace();
			}
			//Pavani Rel8.3.0.0 CR 821 deleted all entry from panel_authorizer table End
        }
        else {
            payRemitOid = payRemit.newObject();            
        }
        
        // Set transaction_oid on the incoming_queue for easy query.
        outputDoc.setAttribute("/Param/TransactionOid", String.valueOf(payRemitOid));


        /* ********************************************************
         *  Recursively populate the PayRemit and its component
         * ******************************************************/
        try {
            payRemit.populateFromXmlDoc(unpackagerDoc, "/Proponix/Body/PayRemit/", new MyXMLTagTranslator(payRemitAttributeMap));
        }
        catch(Exception e){
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINVOT Unpackaging");
            e.printStackTrace();
            return;
        }

        /* ********************************************************
         *  Populate the other attributes that are out of place in the XML
         * ******************************************************/
        String corpOrgProponixCustID = null;
        String corpOrganizationOid = null;
        String corpOrganizationName = null;
        try {
            corpOrgProponixCustID = unpackagerDoc.getAttribute("/Proponix/SubHeader/SellrCustID");
            String sqlStatement = " SELECT ORGANIZATION_OID,NAME  FROM CORPORATE_ORG WHERE PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = ?";
            DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, corpOrgProponixCustID, TradePortalConstants.ACTIVE);
            corpOrganizationOid = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");
            corpOrganizationName = resultXML.getAttribute("/ResultSetRecord(0)/NAME");
            payRemit.setAttribute("seller_corp_org_oid", corpOrganizationOid);
        }
        catch (Exception e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization " + corpOrgProponixCustID);
            e.printStackTrace();
            return;
        }
        payRemit.setAttribute("currency_code", unpackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
        payRemit.setAttribute("otl_pay_remit_uoid", unpackagerDoc.getAttribute("/Proponix/SubHeader/OTLPayRemitUoid"));
        
        // W Zhu 2/13/09 MHUJ021103886 add seller_id_for_buyer & seller_name_for_buyer
        payRemit.setAttribute("seller_id_for_buyer", unpackagerDoc.getAttribute("/Proponix/SubHeader/SellerIdForByr"));
        payRemit.setAttribute("seller_name_for_buyer", unpackagerDoc.getAttribute("/Proponix/SubHeader/SellerNmForByr"));
        
        // Note:  seller_id_for_buyer & seller_name_for_buyer are only for PAYINVIN from TP to OTL.
        payRemit.setAttribute("transaction_status", TradePortalConstants.TRANS_STATUS_MATCH_REQUIRED);
        String currentDateTime = DateTimeUtility.getGMTDateTime();
        payRemit.setAttribute("transaction_status_date", currentDateTime);
        payRemit.setAttribute("receipt_datetime", currentDateTime); // MTUJ022059343 set receipt_datetime
        // PKUI122956323 fix attribte name and check for empty value.
        String paymentInvoiceID = unpackagerDoc.getAttribute("/Proponix/SubHeader/PayInvRefID");
        if (paymentInvoiceID == null || paymentInvoiceID.equals("")) {
            paymentInvoiceID = "NONE";
        }
        payRemit.setAttribute("payment_invoice_reference_id", paymentInvoiceID);
        
        
        /* ********************************************************
         * Populate the party attributes.  They are hierarchical in XML and OTL but flattened in TP object model.
         * ******************************************************/
        Vector partyDocs = unpackagerDoc.getFragments("/Proponix/Body/PayRemit/Party/");
        for (int i = 0; i < partyDocs.size(); i++) {
            DocumentHandler partyDoc = (DocumentHandler)partyDocs.elementAt(i);
            String partyType = partyDoc.getAttribute("/PtyType");
            if (partyType.equals("SAR")) {
                payRemit.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(sellerPartyAttributeMap));
            }
            else if (partyType.equals("BAR")) {
                payRemit.populateFromXmlDoc(partyDoc, "/", new MyXMLTagTranslator(buyerPartyAttributeMap));
            }
        }

        //IValavala CR 742 Rel 8.2.0.0 Extract linked images
        int diXMLCount = 0;
		long dioid = 0;
		DocumentImage documentImage = null;
		String form_type = null;
		String image_id = null;
		Vector documentImageDocs = unpackagerDoc.getFragments("/Proponix/Body/PayRemit/Image/");
	    for (int j = 0; j < documentImageDocs.size(); j++) {
				DocumentHandler documentImageDoc = (DocumentHandler)documentImageDocs.elementAt(j);
				mediatorServices.debug("This is the id of the DocImage " + dioid);
				dioid = payRemit.newComponent("DocumentImageList");
				documentImage = (DocumentImage)payRemit.getComponentHandle("DocumentImageList",dioid);
				unpackageDocumentImageAttributes(documentImage, documentImageDoc, mediatorServices);
				mediatorServices.debug("PAYINVOTUnPackager: DocumentImage is updated with Document_Image_Oid## " + dioid );
				
			} //End of for
//IVAlavala End CR 742

        int returnValue = payRemit.save();

        if (returnValue < 0) {
            mediatorServices.getErrorManager().addErrors(payRemit.getErrorManager().getIssuedErrorsList());
        }else{
        	//CR-927B Send email notification
        	corpOrg	= (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrganizationOid));
            corpOrg.createEmailMessage(instrumentOid, transactionOid,  String.valueOf("0"), " ", TradePortalConstants.EMAIL_TRIGGER_AR_MATCH);
        }

        /* ********************************************************
         *  Set a_invoice_oid and a_pay_remit_invoice_oid on pay_match_result.
         *  When there are large number of pay_remit_result,
         *  this is much faster than querying DB and set association for every pay_match_result
         * ******************************************************/
        try {
            // Set A_INVOICE_OID to the invoice by matching otl_invoice_uoid
        	//IR 22112- copy matched amount sent from TPS to PENDING_MATCHED_AMOUNT
            String sqlStatement = "UPDATE PAY_MATCH_RESULT P SET PENDING_MATCHED_AMOUNT = MATCHED_AMOUNT , A_INVOICE_OID = "
                 + "(SELECT INVOICE_OID FROM INVOICE I WHERE I.OTL_INVOICE_UOID = P.OTL_INVOICE_UOID) WHERE P_PAY_REMIT_OID = ? "
                 + " AND A_INVOICE_OID is NULL";
            DatabaseQueryBean.executeUpdate(sqlStatement, false, new Object[]{payRemitOid});
            // Set A_PAY_REMIT_INVOICE to the pay_remit_invoice by matching invoice_reference_id.
          //IR 22112- copy matched amount sent from TPS to PENDING_MATCHED_AMOUNT
            sqlStatement = "UPDATE PAY_MATCH_RESULT P SET PENDING_MATCHED_AMOUNT = MATCHED_AMOUNT , A_PAY_REMIT_INVOICE_OID = "
                 + "(SELECT PAY_REMIT_INVOICE_OID FROM PAY_REMIT_INVOICE PI WHERE PI.OTL_PAY_REMIT_INVOICE_UOID = P.OTL_PAY_REMIT_INVOICE_UOID AND PI.P_PAY_REMIT_OID = ? "
                 + ") WHERE P_PAY_REMIT_OID = ? "
                 + " AND A_PAY_REMIT_INVOICE_OID IS NULL";
            DatabaseQueryBean.executeUpdate(sqlStatement,  false, new Object[]{payRemitOid, payRemitOid});

            // W Zhu 3/5/09 UKUJ022766616, UHUJ022775664
            // Set discount indicator if there is any invoice_discount_amount.
            sqlStatement = "update pay_remit set discount_indicator = ?, transaction_status = ? "
                         + " where pay_remit_oid = ? "
                         + " and (select sum(invoice_discount_amount) from pay_remit_invoice where p_pay_remit_oid = ?) > 0";
            DatabaseQueryBean.executeUpdate(sqlStatement, false, new Object[]{TradePortalConstants.INDICATOR_YES, TradePortalConstants.TRANS_STATUS_DISC_APPROVAL, payRemitOid, payRemitOid});
        }
        catch (java.sql.SQLException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINVOT Unpackaging");
            e.printStackTrace();
            return;
        }
        
        //Srinivasu_D CR#997 Rel9.3 04/03/2015 - Automatically generating the payment details for matched invoice(s)
        mediatorServices.debug("before starting into CR#997!!..........unpackagerDoc:"+unpackagerDoc);
        
        if(unpackagerDoc.getFragments("/Proponix/Body/PayRemit/MatchingInfo/").size()>0) {          	
        	
        	Vector matchInfo = unpackagerDoc.getFragments("/Proponix/Body/PayRemit/MatchingInfo");
        	String tranType   = "";
        	String appliedAmt = "";
        	String paymentGlCode = "";
        	String invoiceReferenceId = "";
        	int status = 0;
			String currentDt = "";
        	String currentDtTime = "";
			String invUoid = null;
        	MatchPayDedGlDtls matPayDedDtls = null;
			
			for (int i = 0; i < matchInfo.size(); i++) {
			     DocumentHandler matchingDoc = (DocumentHandler) matchInfo.elementAt(i);
			         String matchStatus = matchingDoc.getAttribute("/MatchStatus");
			            if (TradePortalConstants.INDICATOR_A.equals(matchStatus)) {
			            	mediatorServices.debug("matchstatus:"+matchStatus);
			            	 tranType = TradePortalConstants.ERP_PAYMENT;
			            	 appliedAmt = matchingDoc.getAttribute("/AmtMatched");
			            	 invoiceReferenceId = matchingDoc.getAttribute("/InvoiceIDMatch");
							 invUoid = matchingDoc.getAttribute("/OTLInvoiceUoid");
							 deletePaymentDetails(invUoid);
			            	 paymentGlCode = getGLCode(corpOrganizationOid);
			            	 mediatorServices.debug("invUoid:"+invUoid+"\tpayRemitOid:"+payRemitOid+"\tpaymentGlCode:"+paymentGlCode+"\t invoiceReferenceId:"+invoiceReferenceId+"\t appliedAmt:"+appliedAmt+"\ttranType:"+tranType+"\tcurrentDateTime:"+currentDateTime);
			            	 matPayDedDtls = (MatchPayDedGlDtls)mediatorServices.createServerEJB("MatchPayDedGlDtls");
							 matPayDedDtls.newObject();	       		
			            	 currentDt = DateTimeUtility.getCurrentDate();
							 currentDtTime = DateTimeUtility.getCurrentDateTime();
							
			            	 matPayDedDtls.setAttribute("invoice_reference_id",invoiceReferenceId);
			            	 matPayDedDtls.setAttribute("transaction_type",tranType);
			            	 matPayDedDtls.setAttribute("applied_amount",appliedAmt);
			            	 matPayDedDtls.setAttribute("general_ledger_code",paymentGlCode);
			            	 matPayDedDtls.setAttribute("transaction_date",currentDt);
			            	 matPayDedDtls.setAttribute("system_date_time",currentDtTime);
			            	 matPayDedDtls.setAttribute("discount_code","");
			            	 matPayDedDtls.setAttribute("discount_description","");
			            	 matPayDedDtls.setAttribute("discount_comments","");
			            	 matPayDedDtls.setAttribute("a_initiating_obj",String.valueOf(payRemitOid));
			            	 matPayDedDtls.setAttribute("initiating_obj_cls","PayRemit");
							 matPayDedDtls.setAttribute("otl_invoice_oid",invUoid);
			            
			            	 status = matPayDedDtls.save();
			            	 mediatorServices.debug("status>"+status);
			            	  
			            }
			}
			
        }
        //Srinivasu_D CR#997 Rel9.3 04/03/2015 -End
        
		return;
	 }


    /**
     * Recall Pay Remit, i.e. delete it, if the Pay Remit has not been processed.
     *
     * @param unpackagerDoc DocumentHandler
     * @param outputDoc DocumentHandler
     * @param mediatorServices MediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private void recallPayRemit(DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
    throws RemoteException, AmsException

    {
        String payRemitUoid = unpackagerDoc.getAttribute("/Proponix/SubHeader/OTLPayRemitUoid");
        PayRemit payRemit  = (PayRemit) mediatorServices.createServerEJB("PayRemit");
        try {
            payRemit.find("otl_pay_remit_uoid", payRemitUoid);
        }
        catch (ObjectNotFoundException e) {
            // nothing to process.  Return success.
            replyRecallMessage(unpackagerDoc, true, mediatorServices);
            return ;
        }
        catch (NameAndValueNotUniqueException e) {
            // ok
        }
        long userOid = 0; // dummy user_oid for agent
        long payRemitOid = payRemit.getAttributeLong("pay_remit_oid");


        // Lock the Pay Remit object to avoid conflict with the on-line processing.
        try {
            LockingManager.lockBusinessObject(payRemitOid, userOid, true);
            mediatorServices.debug("PayRemitMediator: Locking PayRemit " + payRemitOid + "successful");
        } catch (InstrumentLockException e) {
            // if the object is currently locked by this user, reply to OTL that it cannot be recalled.  The
            // current message is considered successfully processed.
            if (e.getUserOid() != userOid) {
                replyRecallMessage(unpackagerDoc, false, mediatorServices);
                return ;
            }
        }

        try {
            // By the time we are able to lock the object, the data on the object we
            // instantiated before the locking might have changed.  Need to re-instantiate it.
            payRemit  = (PayRemit) mediatorServices.createServerEJB("PayRemit");
            payRemit.getData(payRemitOid);
            if (payRemit.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_MATCH_REQUIRED)) {
                // W Zhu 4/13/09 MNUJ040972417
                // Revert the invoice amounts for the auto matched ones.  OTL will not send INVSTO message to update them.
                String sqlStatement = "update invoice set pending_payment_amount = pending_payment_amount -  "
                             + " (select sum(matched_amount) from pay_match_result where a_invoice_oid = invoice.invoice_oid and p_pay_remit_oid = ? and invoice_matching_status = ?), "
                     + " invoice_outstanding_amount = invoice_outstanding_amount + "
                             + " (select sum(matched_amount) from pay_match_result where a_invoice_oid = invoice.invoice_oid and p_pay_remit_oid = ? and invoice_matching_status = ?)"
                     + " where invoice_oid in"
                     + " (select a_invoice_oid from pay_match_result where p_pay_remit_oid = ? and invoice_matching_status = ?)";
                DatabaseQueryBean.executeUpdate(sqlStatement, false, new Object[]{payRemitOid, TransactionType.AMEND, payRemitOid, TransactionType.AMEND, payRemitOid, TransactionType.AMEND});
                
                // Recall by deleting
                payRemit.delete();
                payRemit.save();
                replyRecallMessage(unpackagerDoc, true, mediatorServices);
            }
            else {
                // Cannot recall
                replyRecallMessage(unpackagerDoc, false, mediatorServices);
            }
        }
        catch (SQLException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINVOT Unpackaging");
            e.printStackTrace();            
        }
        catch (AmsException e){
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINVOT Unpackaging");
            e.printStackTrace();
        }
        finally {
            try {
                LockingManager.unlockBusinessObjectByInstrument(payRemitOid, true);
            } catch (InstrumentLockException e) {
                // this should never happen
                e.printStackTrace();
            }
        }

        return ;

    }

    /**
     * Create OutgoingInterfaceQueue msg_type = PAYINVIN.  This will be sent to OTL to
     * confirm whether the Pay Remit is recalled.
     *
     * If the Pay Remit is recalled, then it would have been deleted from the DB.  So we
     * need to pass all the information that the packager needs in the process_parameters.
     *
     * @param unpackagerDoc
     * @param recallSuccess
     * @param mediatorServices
     * @throws RemoteException
     * @throws AmsException
     */
    private void replyRecallMessage(DocumentHandler unpackagerDoc, boolean recallSuccess, MediatorServices mediatorServices)
       throws RemoteException, AmsException  {
        OutgoingInterfaceQueue oiQueue = (OutgoingInterfaceQueue) mediatorServices
        .createServerEJB("OutgoingInterfaceQueue");
        oiQueue.newObject();
        String attributeNames[] = new String[] { "date_created", "status",
                "msg_type", "message_id", "reply_to_message_id", "process_parameters" };

        // the message_id (oid) should be unique for the middleware -
        // retrieve this information by getting an instance of ObjectIDCache
        // for MESSAGE (middleware) to generate the object id.

        String messageId = Long.toString(ObjectIDCache.getInstance(
                AmsConstants.MESSAGE).generateObjectID(false));
		/* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
		/* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
		PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("AgentConfiguration");
		String serverName = portalProperties.getString("serverName");
		
		// NSX 04/20/2010 - RDUK041649583 Begin
		//messageId += serverName.charAt(serverName.length() - 1);
		messageId += serverName.substring(serverName.length() - 2);
		// NSX 04/20/2010 - RDUK041649583 End
		
		/* IValera - IR# JOUI111280267 - 02/18/2009 - Change End */

        String replyToMsgId = unpackagerDoc.getAttribute("/Proponix/Header/MessageID");

        StringBuffer parms = new StringBuffer();
        parms.append("ActionType=");
        if (recallSuccess) {
            parms.append(ACTION_TYPE_C);
        }
        else {
            parms.append(ACTION_TYPE_W);
        }
        parms.append("|SellrCustID=");
        parms.append(unpackagerDoc.getAttribute("/Proponix/SubHeader/SellrCustID"));
        parms.append("|Currency=");
        parms.append(unpackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
        parms.append("|PayInvRefID=");
        parms.append(unpackagerDoc.getAttribute("/Proponix/SubHeader/PayInvRefID"));
        parms.append("|OTLPayRemitUoid=");
        parms.append(unpackagerDoc.getAttribute("/Proponix/SubHeader/OTLPayRemitUoid"));

        String attributeValues[] = new String[] {
                DateTimeUtility.getGMTDateTime(),
                TradePortalConstants.OUTGOING_STATUS_STARTED,
                "PAYINVIN",
                messageId,
                replyToMsgId,
                parms.toString()
                };

        oiQueue.setAttributes(attributeNames, attributeValues);
        if (oiQueue.save() < 0) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINVOT Unpackaging");
        }

        return;
    }
    
    /**
	 * This method takes the DocumentImage object
	 * and populates the database with values from DocumentImage section
	 * @param documentImage, DocumentImage object
	 * @param imageDoc, DocumentHandler object with the XML section for this DocumentImage
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageDocumentImageAttributes(DocumentImage documentImage,DocumentHandler imageDoc,  MediatorServices mediatorServices )
	throws RemoteException, AmsException
	{
		
		documentImage.setAttribute("form_type",          "PAYREM");
		documentImage.setAttribute("image_id",           imageDoc.getAttribute("/ImageID"));
		documentImage.setAttribute("source_type", 		 imageDoc.getAttribute("/ImageSource"));
		documentImage.setAttribute("image_format", 		 imageDoc.getAttribute("/ImageFormat"));
		documentImage.setAttribute("doc_name",		 imageDoc.getAttribute("/ImageDesc"));
		documentImage.setAttribute("parent_class",       "PayRemit");
		mediatorServices.debug("Done updating the DocumentImage object number : " + documentImage.getAttribute("doc_image_oid"));
	}




    /**
     *
     * PAYINVOTTranslator: The adaptor class used to translate the tag name and values in
     * PAYINVOT XML to object attribute name and values.  This is used for populate the object
     * with the XML file.
     *
     */

   public class MyXMLTagTranslator extends com.amsinc.ecsg.util.XMLTagTranslator {

        private Map nameMap;

        public MyXMLTagTranslator (Map nameMap) {
            this.nameMap = nameMap;
        }

        /**
         * translate tagName according to nameMap.
         * translate attrbite values as necessary.
         */
        public void translateTag(String tagName, String tagValue) {

            attributeName = (String)nameMap.get(tagName);

            if (tagName.equals("ValueDate")
                    || tagName.equals("PODtOfIsse")
                    || tagName.equals("InvoiceDate")) {
                attributeValue = tagValue.substring(4,6) + "/"
                                + tagValue.substring(6,8) + "/"
                                + tagValue.substring(0,4);
            }
            else if (tagName.equals("RemitDate")) {
                attributeValue = tagValue.substring(4,6) + "/"
                + tagValue.substring(6,8) + "/"
                + tagValue.substring(0,4)+ " 00:00:00";
            }
            else if (tagName.equals("MatchStatus")){
                // W Zhu 2/13/09 MJUJ012652239 add X
                // W Zhu 3/9/09 MNUJ030467825 udpate
                // X is "Proposed Match"
                // P is "Partial Match", where payment amount < invoice amount and otherwise treated like Auto Match.
                attributeValue = tagValue.equals("A")?"AMD"
                                :tagValue.equals("M")?"MMD"
                                :tagValue.equals("P")?"AMD"
                                :tagValue.equals("U")?"NMD"
                                :tagValue.equals("X")?"PPD"
                                :"";
            }
            else {
                attributeValue = tagValue;
            }

        }
    }

    /*
     ****************************************************
     static initialization of the attributemap:
     **************************************************
     */
    {
        payRemitAttributeMap = new Hashtable();
        payRemitAttributeMap.put("RecdDataStat", "received_data_status");
        payRemitAttributeMap.put("ValueDate", "payment_value_date");
        payRemitAttributeMap.put("PaymentAmount", "payment_amount");
        payRemitAttributeMap.put("PaySource", "payment_source_type");
        payRemitAttributeMap.put("RemitSource", "remit_source_type");
        payRemitAttributeMap.put("RemitDate", "remittance_datetime");
        payRemitAttributeMap.put("TracerLine", "tracer_line");
        payRemitAttributeMap.put("ChequeRef", "cheque_reference");
        payRemitAttributeMap.put("BuyrInvReferenceId", "buyer_invoice_reference_id");
        payRemitAttributeMap.put("Narrative", "narrative");
        payRemitAttributeMap.put("TotalInvAmt", "total_invoice_amount");
        payRemitAttributeMap.put("TotalInvNo", "total_number_of_invoices");


        // PayRemit/RemitInv translates to PayRemit.PayRemitInvoiceList
        payRemitAttributeMap.put("RemitInv", "PayRemitInvoiceList"); // component
        payRemitAttributeMap.put("OTLRemitUoid", "otl_pay_remit_invoice_uoid");
        payRemitAttributeMap.put("InvoiceID", "invoice_reference_id");
        payRemitAttributeMap.put("InvoiceAmt", "invoice_payment_amount");
        payRemitAttributeMap.put("InvoiceBalAmt", "invoice_balance_amount");
        payRemitAttributeMap.put("InvoiceOrigAmt", "invoice_original_amount");
        payRemitAttributeMap.put("InvoiceDate", "invoice_date"); //MUUJ010737162
        payRemitAttributeMap.put("InvoiceDiscAmt", "invoice_discount_amount");
        payRemitAttributeMap.put("CreditNoteInd", "credit_note_indicator");
        payRemitAttributeMap.put("InvoiceDiscCode", "buyer_disc_code");
        payRemitAttributeMap.put("InvoiceDiscComments", "buyer_disc_comments");
        
        // PayRemit/RemitInv/PurchsOrdrRef translates to PayRemit.PayRemitInvoiceList.PayRemitInvoicePoList
        payRemitAttributeMap.put("PurchsOrdrRef", "PayRemitInvoicePoList");
        payRemitAttributeMap.put("POId", "po_reference_id");
        payRemitAttributeMap.put("PODtOfIsse", "po_issue_date");
        payRemitAttributeMap.put("POType", "po_type");
        payRemitAttributeMap.put("PODescription", "po_description");
        payRemitAttributeMap.put("SlrLabel1", "seller_information_label1");
        payRemitAttributeMap.put("SlrLabel2", "seller_information_label2");
        payRemitAttributeMap.put("SlrLabel3", "seller_information_label3");
        payRemitAttributeMap.put("SlrLabel4", "seller_information_label4");
        payRemitAttributeMap.put("SlrLabel5", "seller_information_label5");
        payRemitAttributeMap.put("SlrLabel6", "seller_information_label6");
        payRemitAttributeMap.put("SlrLabel7", "seller_information_label7");
        payRemitAttributeMap.put("SlrLabel8", "seller_information_label8");
        payRemitAttributeMap.put("SlrLabel9", "seller_information_label9");
        payRemitAttributeMap.put("SlrLabel10", "seller_information_label10");
        payRemitAttributeMap.put("SlrInfo1", "seller_information1");
        payRemitAttributeMap.put("SlrInfo2", "seller_information2");
        payRemitAttributeMap.put("SlrInfo3", "seller_information3");
        payRemitAttributeMap.put("SlrInfo4", "seller_information4");
        payRemitAttributeMap.put("SlrInfo5", "seller_information5");
        payRemitAttributeMap.put("SlrInfo6", "seller_information6");
        payRemitAttributeMap.put("SlrInfo7", "seller_information7");
        payRemitAttributeMap.put("SlrInfo8", "seller_information8");
        payRemitAttributeMap.put("SlrInfo9", "seller_information9");
        payRemitAttributeMap.put("SlrInfo10", "seller_information10");

        // PayMatchResultList   Component
        payRemitAttributeMap.put("MatchingInfo", "PayMatchResultList");
        payRemitAttributeMap.put("PayRemitInvUoid", "otl_pay_remit_invoice_uoid");
        payRemitAttributeMap.put("InvoiceIDMatch", "invoice_reference_id");
        payRemitAttributeMap.put("OTLInvoiceUoid", "otl_invoice_uoid");
        payRemitAttributeMap.put("MatchStatus", "invoice_matching_status");
        payRemitAttributeMap.put("AmtMatched", "matched_amount");
        payRemitAttributeMap.put("Explanation", "match_explanation");


        // Flatten the Party node to PayRemit itself

        buyerPartyAttributeMap = new Hashtable();
        buyerPartyAttributeMap.put("PartyName", "buyer_name");
        buyerPartyAttributeMap.put("PartyContact", "buyer_contact_name");
        buyerPartyAttributeMap.put("PartyAddress", "buyer_address");
        buyerPartyAttributeMap.put("PtyReference", "buyer_reference");
        buyerPartyAttributeMap.put("PtyBankDetails", "buyer_bank_details");

        sellerPartyAttributeMap = new Hashtable();
        sellerPartyAttributeMap.put("PartyName", "seller_name");
        sellerPartyAttributeMap.put("PartyContact", "seller_contact_name");
        sellerPartyAttributeMap.put("PartyAddress", "seller_address");
        sellerPartyAttributeMap.put("PtyReference", "seller_reference");
        sellerPartyAttributeMap.put("PtyBankDetails", "seller_bank_details");


    }
    
    //Srinivasu_D CR#997 Rel9.3 04/03/2015 - Added below method to figure out Payment GL Code
    /**
     * This method finds Payment GL Code from AR_MATCHING_TABLE if available
     * else check in CUSTOMER_ERP_GL_CODE for the same otherwise blank would return.
     * @param corporateOrgOid
     * @return String
     * @throws AmsException
     */
    private String getGLCode (String corporateOrgOid) throws AmsException {
    	
    	String paymentGLCode = null;
    	List<Object> sqlParams = new ArrayList();
    	 String arMatchingSql = "select payment_gl_code,over_payment_gl_code,discount_gl_code from ar_matching_rule where p_corp_org_oid = ?";
    	 sqlParams.add(corporateOrgOid);
    	 DocumentHandler result = DatabaseQueryBean.getXmlResultSet(arMatchingSql,false,sqlParams);	
    	 //LOG.info("payment_gl_code:"+result);
    	 if(result !=null && result.getFragments("/ResultSetRecord").size()>0) {
    		 Vector glCodeVector = result.getFragments("/ResultSetRecord");
    		 for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
				  DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
				  paymentGLCode = glCodeDoc.getAttribute("/PAYMENT_GL_CODE");
    	 }	
    	}
    	 if(StringFunction.isBlank(paymentGLCode)) {
    		 
    		 String glCodeSql = "select erp_gl_code from CUSTOMER_ERP_GL_CODE where p_owner_oid = ? and deactivate_ind = ? and erp_gl_category = ? and default_gl_code_ind = ?";
    		 sqlParams = new ArrayList();
    		 sqlParams.add(corporateOrgOid);
    		 sqlParams.add(TradePortalConstants.INDICATOR_NO);
    		 sqlParams.add(TradePortalConstants.ERP_PAYMENT);
			 sqlParams.add(TradePortalConstants.INDICATOR_YES);
    		 result = DatabaseQueryBean.getXmlResultSet(glCodeSql,false,sqlParams);
			 //LOG.info("erp gl code:"+result);
        	 if(result !=null && result.getFragments("/ResultSetRecord").size()>0) {
        		 Vector glCodeVector = result.getFragments("/ResultSetRecord");
        		 for (int mLoop = 0; mLoop < glCodeVector.size(); mLoop++) {
					  DocumentHandler glCodeDoc = (DocumentHandler) glCodeVector.elementAt(mLoop);
					  paymentGLCode = glCodeDoc.getAttribute("/ERP_GL_CODE");
        	 }	
        	}
    	 }
		return paymentGLCode;
    }
	
	//Srinivasu_D IR#T36000040971 Rel9.3 06/30/2015- Added below method to delete exisitng match details before creating a new ones
	private  int deletePaymentDetails(String otlInvoiceUoid) throws RemoteException, AmsException {		
		
		//mediatorServices.debug("deletePaymentDetails()...:"+otlInvoiceUoid);
    	int status = 0;	
		
    	if(StringFunction.isNotBlank(otlInvoiceUoid)) {
			
			String deleteSql = "delete from match_pay_ded_gl_dtls where otl_invoice_oid = ? ";
			//LOG.info("otlInvoiceUoid:"+otlInvoiceUoid);
			List<Object> sqlParams = new ArrayList();
			sqlParams.add(otlInvoiceUoid);
			try{
			status = DatabaseQueryBean.executeUpdate(deleteSql, false,
					sqlParams);
			}catch(SQLException sql){
				throw new AmsException(sql);
			}
			
    	}
    	//mediatorServices.debug("delete status:"+status);
		
		return status;
	}
	//Srinivasu_D IR#T36000040971 Rel9.3 06/30/2015- End

    //Srinivasu_D CR#997 Rel9.3 04/03/2015 - End
}
