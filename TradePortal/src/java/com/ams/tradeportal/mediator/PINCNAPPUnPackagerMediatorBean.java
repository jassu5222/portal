package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.InvoiceLineItemDetail;
import com.ams.tradeportal.busobj.InvoiceSummaryGoods;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.util.InvoiceCRNoteAttibuteUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/**
 * Unpackages that handles PINCNAPP message.
 * 
 * 
 */
public class PINCNAPPUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PINCNAPPUnPackagerMediatorBean.class);
	/*
	 * static private Map<String, String> invAttributeMap; static private
	 * Map<String, String> sellerPartyAttributeMap; static private Map<String,
	 * String> creditNoteAttributeMap; static private Map<String, String>
	 * paymentDetailsAttributeMap;
	 */

	public DocumentHandler execute(DocumentHandler unpackagerDoc,DocumentHandler outputDoc, 
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		String customerID = unpackagerDoc.getAttribute("/Proponix/SubHeader/CustomerID");
		String corpOrg = null;
		if (StringFunction.isNotBlank(customerID)) {
			try {

				String sqlStatement = " SELECT organization_oid FROM corporate_org WHERE proponix_customer_id = ? AND activation_status = ? ";
				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true,
						new Object[] { customerID, "ACTIVE" });
				corpOrg = resultXML.getAttribute("/ResultSetRecord(0)/ORGANIZATION_OID");

			} catch (Exception e) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Corporate Organization " + corpOrg);
				e.printStackTrace();
				return outputDoc;
			}
		}else{
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.NO_MATCHING_VALUE_FOUND,"customerID is null ");
			return outputDoc;
		}

		String LinkedtoInstrType = unpackagerDoc.getAttribute("/Proponix/SubHeader/LinkedtoInstrType");
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(LinkedtoInstrType)) {
			unpackagePayableInvoiceCRNote(corpOrg, unpackagerDoc, mediatorServices);
		}
		else if (TradePortalConstants.INV_LINKED_INSTR_REC.equals(LinkedtoInstrType)) {
			unpackageReceivableInvoice(corpOrg, unpackagerDoc, mediatorServices);
		}
		else{
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.NO_MATCHING_VALUE_FOUND,"LinkedtoInstrType is invalid "+LinkedtoInstrType);
			
		}
		return outputDoc;
		

	}

	/**
	 * Unpackage XML to create Invoice
	 * 
	 * @param unpackagerDoc
	 * @param outputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */

	private void unpackagePayableInvoiceCRNote(String corpOrg,DocumentHandler unpackagerDoc,
			MediatorServices mediatorServices)throws RemoteException, AmsException

	{
		String linkedToInstrType = unpackagerDoc.getAttribute("/Proponix/SubHeader/LinkedtoInstrType");
		List<DocumentHandler> invoiceDocs = unpackagerDoc.getFragmentsList("/Proponix/Body/InvoiceCreditNote/");
		AbstractCRNoteProcessor cr =getCRNoteProcessor(linkedToInstrType);
		AbstractInvoiceProcessor processor = getInvProcessor(linkedToInstrType);
		for (DocumentHandler invoiceDoc : invoiceDocs) {
			
			if (TradePortalConstants.CREDIT_NOTE.equals(invoiceDoc.getAttribute("/InvoiceCreditNoteInd"))) {
				if(cr==null) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Insrument Type " + linkedToInstrType);
					return ;
				}
				if (TradePortalConstants.INITIAL_INVOICE.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					cr.processCreditNotes(corpOrg, unpackagerDoc, invoiceDoc, mediatorServices,linkedToInstrType);
				}else if (TransactionType.AMEND.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					cr.processReplacementCreditNotes(corpOrg, unpackagerDoc,invoiceDoc, mediatorServices);
				} else {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Invoice Type " + invoiceDoc.getAttribute("/InvoiceType"));
				}
				
			}

			if (TradePortalConstants.INVOICE_IND.equals(invoiceDoc.getAttribute("/InvoiceCreditNoteInd"))) {
				if(processor==null) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Insrument Type " + linkedToInstrType);
					return ;
				}
				
				if (TradePortalConstants.INITIAL_INVOICE.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					if (processor.isDuplicateEntry(corpOrg, unpackagerDoc, invoiceDoc, mediatorServices, linkedToInstrType)) {
						return;
					}
					processor.processInvoice(corpOrg, unpackagerDoc, invoiceDoc, mediatorServices, linkedToInstrType);
				} else if (TransactionType.AMEND.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					processor.processReplacementInvoice(corpOrg, unpackagerDoc,invoiceDoc, mediatorServices, linkedToInstrType);
				} else {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Invoice Type " + invoiceDoc.getAttribute("/InvoiceType"));
				}

			}

		}
	}

	private void unpackageReceivableInvoice(String corpOrg,
			DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException

	{
		
		String linkedToInstrType = unpackagerDoc.getAttribute("/Proponix/SubHeader/LinkedtoInstrType");
		AbstractInvoiceProcessor processor = getInvProcessor(linkedToInstrType);
		List<DocumentHandler> invoiceDocs = unpackagerDoc.getFragmentsList("/Proponix/Body/InvoiceCreditNote/");
		for (DocumentHandler invoiceDoc : invoiceDocs) {
			if (TradePortalConstants.INVOICE_IND.equals(invoiceDoc.getAttribute("/InvoiceCreditNoteInd"))) {

				if(processor==null) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Insrument Type " + linkedToInstrType);
					return ;
				}
				if (TradePortalConstants.INITIAL_INVOICE.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					if (processor.isDuplicateEntry(corpOrg, unpackagerDoc, invoiceDoc, mediatorServices, linkedToInstrType)) {
						return;
					}
					processor.processInvoice(corpOrg, unpackagerDoc, invoiceDoc, mediatorServices, linkedToInstrType);
				} else if (TransactionType.AMEND.equals(invoiceDoc.getAttribute("/InvoiceType"))) {
					processor.processReplacementInvoice(corpOrg, unpackagerDoc,invoiceDoc, mediatorServices, linkedToInstrType);
				} else {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
							TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Invoice Type " + invoiceDoc.getAttribute("/InvoiceType"));
				}

			}
		}

	}
	/**
	 * 
	 * Invoice Translator: The "call-back" class used to translate the tag name
	 * and values in RM Invoice XML to object attribute name and values. This is
	 * used for populate the object with the XML file.
	 * 
	 */

	public class MyXMLTagTranslator extends com.amsinc.ecsg.util.XMLTagTranslator {

		private Map<String, String> nameMap;

		public MyXMLTagTranslator(Map<String, String> nameMap) {
			this.nameMap = nameMap;
		}

		// 20010102 -> 01/02/2001
		private String translateDateTime(String tagName, String tagValue,
				boolean addTime) {
			if (!StringFunction.isBlank(tagValue)) {
				String date = tagValue.substring(4, 6) + "/"
						+ tagValue.substring(6, 8) + "/"
						+ tagValue.substring(0, 4);
				return addTime ? date + " 00:00:00" : date;

			}
			return "";
		}

		@Override
		public void translateTag(String tagName, String tagValue) {
			 attributeName = (String)nameMap.get(tagName);
			 if (tagName.equals("ActualShipmentDate")){
	            	attributeValue =translateDateTime(tagName,tagValue,false);
	           }
	             else {
	                attributeValue = tagValue;
	            }
	    }

	}


	public abstract class AbstractInvoiceProcessor {

		public boolean isDuplicateEntry(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler creditNoteDoc,
				MediatorServices mediatorServices, String linkedToType)
				throws RemoteException, AmsException {

			String sql = " SELECT count(*) the_count FROM invoices_summary_data "
					+ " WHERE invoice_id = ? AND  a_corp_org_oid = ?  and linked_to_instrument_type=?";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(
					sql,
					true,
					new Object[] {
							creditNoteDoc.getAttribute("/InvoiceCreditNoteID"),
							corpOrg, linkedToType });
			int theCount = resultXML != null ? resultXML
					.getAttributeInt("/ResultSetRecord(0)/THE_COUNT") : 0;
			if (theCount > 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.DUPLICATE_INVOICE,
						"Invoice with invoice id : "
								+ creditNoteDoc
										.getAttribute("/InvoiceCreditNoteID"));
				return true;
			}

			return false;
		}

		/**
		 * This method is used to process new payable invoices uploaded in TPS H2H.
		 * 
		 * @param corpOrg
		 * @param customerID
		 * @param activityType
		 * @param creditNoteDoc
		 * @param mediatorServices
		 * @throws RemoteException
		 * @throws AmsException
		 */

		public InvoicesSummaryData processInvoice(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler invoiceDoc,
				MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {
			InvoicesSummaryData inv = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData");
			inv.newObject();
			MyXMLTagTranslator myXMLTagTranslator = new MyXMLTagTranslator(InvoiceCRNoteAttibuteUtility.invAttributeMap);
			inv.populateFromXmlDoc(invoiceDoc, "/", myXMLTagTranslator);
			
			inv.setAttribute("corp_org_oid", corpOrg);
			inv.setAttribute("currency", unPackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
			processDates(inv,invoiceDoc,myXMLTagTranslator);
			inv.setAttribute("invoice_status",TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
			inv.setAttribute("linked_to_instrument_type",linkedToInstrType);
			inv.setAttribute("action",TradePortalConstants.UPLOAD_INV_ACTION_FILE_UPL);
			inv.setAttribute("portal_approval_requested_ind", TradePortalConstants.INDICATOR_YES);
			
			List<DocumentHandler> goodsDocs = invoiceDoc.getFragmentsList("/Goods/"); 
			if (goodsDocs.size() ==0){
				
				long goodsOid = inv
                        .newComponent("InvoiceSummaryGoodsList");

				InvoiceSummaryGoods  invGoods = (InvoiceSummaryGoods) inv .getComponentHandle(
                                "InvoiceSummaryGoodsList", goodsOid);
				invGoods.setAttribute("buyer_users_def1_label",
	    				"Buyer User Definition Label");
				invGoods.setAttribute("buyer_users_def1_value",
	    				"Dummy Line Item");
				
				 long lineItemOid = invGoods
                 .newComponent("InvoiceLineItemDetailList");

				 InvoiceLineItemDetail invLineItem = (InvoiceLineItemDetail) invGoods
                 .getComponentHandle(
                         "InvoiceLineItemDetailList",
                         lineItemOid);
				 invLineItem.setAttribute("line_item_id", "1");
				 invLineItem.setAttribute("unit_price",invoiceDoc.getAttribute("/Amount"));
				 invLineItem.setAttribute("inv_quantity", "1");
				 //invLineItem.setAttribute("derived_line_item_ind", TradePortalConstants.INDICATOR_YES);
			}
			
			
			return inv;
		}

		/*This method is used to process RPL payable invoices uploaded in TPS H2H.
		 * 
		 */
		public InvoicesSummaryData processReplacementInvoice(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler invoiceDoc,
				MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {

			String sql = " SELECT count(*) the_count, max(upload_invoice_oid) the_oid FROM invoices_summary_data "
					+ " WHERE invoice_id = ? AND  a_corp_org_oid = ?  and linked_to_instrument_type=?";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(
					sql,true,new Object[] {
							invoiceDoc.getAttribute("/InvoiceCreditNoteID"),corpOrg, linkedToInstrType });
			int theCount = resultXML != null ? resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT") : 0;
			if (theCount != 1) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NO_MATCHING_VALUE_FOUND,
						"Invoice with invoice id : "+ invoiceDoc.getAttribute("/InvoiceCreditNoteID"));
				return null;
			}
			Long oid = resultXML.getAttributeLong("/ResultSetRecord(0)/THE_OID");
			InvoicesSummaryData inv = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData");
			inv.getData(oid);
			MyXMLTagTranslator myXMLTagTranslator = new MyXMLTagTranslator(InvoiceCRNoteAttibuteUtility.invAttributeMap);
		
			List<DocumentHandler> goodsDocs = invoiceDoc.getFragmentsList("/Goods/");
		    if (goodsDocs.size() > 0 ) {
					ComponentList invoiceGoodsList = (ComponentList)inv.getComponentHandle("InvoiceSummaryGoodsList");
					if (invoiceGoodsList.getObjectCount() > 0) {
						inv.deleteComponent("InvoiceSummaryGoodsList");
					}
		    }
		    
			inv.populateFromXmlDoc(invoiceDoc, "/", myXMLTagTranslator);
			inv.setAttribute("currency", unPackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
			inv.setAttribute("portal_approval_requested_ind", TradePortalConstants.INDICATOR_YES);
			inv.setAttribute("corp_org_oid", corpOrg);
			inv.setAttribute("rin_notification_email_sent_ind", TradePortalConstants.INDICATOR_NO);
			inv.setAttribute("pin_notification_email_sent_ind", TradePortalConstants.INDICATOR_NO);
			
			processDates(inv,invoiceDoc,myXMLTagTranslator);
			inv.setAttribute("invoice_status",TradePortalConstants.UPLOAD_INV_STATUS_RAA);
			inv.setAttribute("action",TradePortalConstants.REPLACEMENT);
			inv.save();
			try {
				inv.remove();
			} catch (RemoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
			

		}
		public void processDates(InvoicesSummaryData inv,
				DocumentHandler invoiceDoc,MyXMLTagTranslator myXMLTagTranslator) throws RemoteException,
				AmsException {
			if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/DateOfIssue"))) {
				inv.setAttribute("issue_date", myXMLTagTranslator
						.translateDateTime("DateOfIssue",invoiceDoc.getAttribute("/DateOfIssue"),
								false));
			}if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/InvoiceDueDate"))) {
				inv.setAttribute("due_date", myXMLTagTranslator
						.translateDateTime("InvoiceDueDate",invoiceDoc.getAttribute("/InvoiceDueDate"),
								false));
			}if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/InvoicePaymentDate"))) {
				inv.setAttribute("payment_date", myXMLTagTranslator
						.translateDateTime("InvoicePaymentDate",invoiceDoc.getAttribute("/InvoicePaymentDate"),
								false));
			}if (StringFunction.isNotBlank(invoiceDoc.getAttribute("/SendToSupplierDate"))) {
				inv.setAttribute("send_to_supplier_date", myXMLTagTranslator
						.translateDateTime("SendToSupplierDate",invoiceDoc.getAttribute("/SendToSupplierDate"),
								false));
			}
			
		}


	}

	public class PayableNewInvoiceProcessor extends AbstractInvoiceProcessor {
		
		public InvoicesSummaryData processInvoice(String corpOrg, DocumentHandler unPackagerDoc,
				DocumentHandler invoiceDoc,MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {
			InvoicesSummaryData inv =	super.processInvoice(corpOrg,
					unPackagerDoc, invoiceDoc, mediatorServices,linkedToInstrType);
			inv.setAttribute("invoice_classification", TradePortalConstants.INVOICE_TYPE_PAY_MGMT);
			
			inv.save();
			try {
				inv.remove();
			} catch (RemoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
	}

	public class ReceivableNewInvoiceProcessor extends AbstractInvoiceProcessor {

		public InvoicesSummaryData processInvoice(String corpOrg, DocumentHandler unPackagerDoc,
				DocumentHandler invoiceDoc,MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {
			InvoicesSummaryData inv =	super.processInvoice(corpOrg,
					unPackagerDoc, invoiceDoc, mediatorServices,linkedToInstrType);
			inv.setAttribute("invoice_classification", TradePortalConstants.INVOICE_TYPE_REC_MGMT);
			
			inv.save();
			try {
				inv.remove();
			} catch (RemoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
	}

	public abstract class AbstractCRNoteProcessor {

		public boolean isDuplicateEntry(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler creditNoteDoc,
				MediatorServices mediatorServices) throws RemoteException,
				AmsException {

			String sql = " SELECT count(*) credit_count FROM credit_notes WHERE invoice_id = ? "
					+ " AND a_corp_org_oid = ?  AND linked_to_instrument_type = ? ";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(
					sql,true,new Object[] {creditNoteDoc.getAttribute("/InvoiceCreditNoteID"),
							corpOrg, TradePortalConstants.PAYABLES_MGMT });

			int count = resultXML != null ? resultXML.getAttributeInt("/ResultSetRecord(0)/CREDIT_COUNT") : 0;

			if (count > 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.DUPLICATE_CREDIT_NOTE,
						"with credit note ID: "+ creditNoteDoc.getAttribute("/InvoiceID"));
				return true;
			}
			return false;
		}
		
		/*This method is used to process new payable CRNote uploaded in TPS H2H.
		 * 
		 */
		public CreditNotes processCreditNotes(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler creditNoteDoc,
				MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {
			CreditNotes creditNote = (CreditNotes) mediatorServices
					.createServerEJB("CreditNotes");
			creditNote.newObject();
			MyXMLTagTranslator myXMLTagTranslator = new MyXMLTagTranslator(
					InvoiceCRNoteAttibuteUtility.creditNoteAttributeMap);
			creditNote.populateFromXmlDoc(creditNoteDoc, "/",
					myXMLTagTranslator);
			creditNote.setAttribute("portal_approval_requested_ind", TradePortalConstants.INDICATOR_YES);
			creditNote.setAttribute("corp_org_oid", corpOrg);
			creditNote.setAttribute("currency",unPackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
			processDates(creditNote,creditNoteDoc,myXMLTagTranslator);
			String credAppliedAmt = creditNoteDoc.getAttribute("/TotalCreditNoteAppliedAmount");
			if (StringFunction.isNotBlank(credAppliedAmt)) {
				BigDecimal amt = new BigDecimal(credAppliedAmt);
				if (amt.compareTo(BigDecimal.ZERO) != 0 && amt.signum() == 1) {
					amt = amt.negate();
				}
				creditNote.setAttribute("credit_note_applied_amount",amt.toPlainString());
			}
			creditNote.setAttribute("linked_to_instrument_type",linkedToInstrType);
			creditNote.setAttribute("credit_note_status",TradePortalConstants.CREDIT_NOTE_STATUS_AVA);
			creditNote.setAttribute("credit_note_applied_status", TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP);
			creditNote.setAttribute("credit_note_utilised_status", TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_UNU);
			creditNote.setAttribute("action",TradePortalConstants.UPLOAD_INV_ACTION_FILE_UPL);
				creditNote.setAttribute("invoice_classification", "P");
			
			creditNote.save();
			try {
				creditNote.remove();
			} catch (RemoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		/*This method is used to process RPL payable CRNote uploaded in TPS H2H.
		 * 
		 */
		public CreditNotes processReplacementCreditNotes(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler creditNoteDoc,
				MediatorServices mediatorServices) throws RemoteException,
				AmsException {

			String sqlStatement = " SELECT count(*) the_count, max(upload_credit_note_oid) oid FROM credit_notes WHERE invoice_id = ? "
					+ " AND a_corp_org_oid = ?  AND linked_to_instrument_type = ? ";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(
					sqlStatement,true,new Object[] {creditNoteDoc.getAttribute("/InvoiceCreditNoteID"),
							corpOrg, TradePortalConstants.PAYABLES_MGMT });
			int theCount = resultXML != null ? resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT") : 0;
			if (theCount != 1) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NO_MATCHING_VALUE_FOUND,
						"count of CRNotes:"+ theCount + " CRNote with creditnote id "+ creditNoteDoc.getAttribute("/InvoiceCreditNoteID"));
				return null;
			}
			// if original invoice is already present in portal and TPS send
			// updated version then perform below process
			String crOid = resultXML.getAttribute("/ResultSetRecord(0)/OID");
			CreditNotes creditNote = (CreditNotes) mediatorServices
					.createServerEJB("CreditNotes");
			creditNote.getData(Long.parseLong(crOid));
			MyXMLTagTranslator myXMLTagTranslator = new MyXMLTagTranslator(
					InvoiceCRNoteAttibuteUtility.creditNoteAttributeMap);
			List<DocumentHandler> goodsDocs = creditNoteDoc.getFragmentsList("/Goods/");
		    if (goodsDocs.size() > 0 ) {
					ComponentList invoiceGoodsList = (ComponentList)creditNote.getComponentHandle("CreditNotesGoodsList");
					if (invoiceGoodsList.getObjectCount() > 0) {
						creditNote.deleteComponent("CreditNotesGoodsList");
					}
		    }
			creditNote.populateFromXmlDoc(creditNoteDoc, "/",
					myXMLTagTranslator);
			creditNote.setAttribute("currency", unPackagerDoc.getAttribute("/Proponix/SubHeader/Currency"));
			creditNote.setAttribute("corp_org_oid", corpOrg);
			creditNote.setAttribute("portal_approval_requested_ind", TradePortalConstants.INDICATOR_YES);
			processDates(creditNote,creditNoteDoc,myXMLTagTranslator);
			creditNote.setAttribute("action",TradePortalConstants.REPLACEMENT);
			creditNote.setAttribute("credit_note_status",TradePortalConstants.UPLOAD_INV_STATUS_RAA);
			creditNote.setAttribute("credit_note_applied_status", TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP);
			creditNote.setAttribute("credit_note_utilised_status", TradePortalConstants.CREDIT_NOTE_UTILIZED_STATUS_UNU);
			creditNote.setAttribute("invoice_classification", "P");
			creditNote.setAttribute("pcn_notification_email_sent_ind", TradePortalConstants.INDICATOR_NO);
			creditNote.save();
			try {
				creditNote.remove();
			} catch (RemoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;

		}
		public void processDates(CreditNotes creditNote,
				DocumentHandler creditNoteDoc,MyXMLTagTranslator myXMLTagTranslator) throws RemoteException,
				AmsException {
			if (StringFunction.isNotBlank(creditNoteDoc.getAttribute("/DateOfIssue"))) {
				creditNote.setAttribute("issue_date", myXMLTagTranslator
						.translateDateTime("DateOfIssue",creditNoteDoc.getAttribute("/DateOfIssue"),
								false));
			}if (StringFunction.isNotBlank(creditNoteDoc.getAttribute("/InvoiceDueDate"))) {
				creditNote.setAttribute("due_date", myXMLTagTranslator
						.translateDateTime("InvoiceDueDate",creditNoteDoc.getAttribute("/InvoiceDueDate"),
								false));
			}if (StringFunction.isNotBlank(creditNoteDoc.getAttribute("/InvoicePaymentDate"))) {
				creditNote.setAttribute("payment_date", myXMLTagTranslator
						.translateDateTime("InvoicePaymentDate",creditNoteDoc.getAttribute("/InvoicePaymentDate"),
								false));
			}if (StringFunction.isNotBlank(creditNoteDoc.getAttribute("/SendToSupplierDate"))) {
				creditNote.setAttribute("send_to_supplier_date", myXMLTagTranslator
						.translateDateTime("SendToSupplierDate",creditNoteDoc.getAttribute("/SendToSupplierDate"),
								false));
			}
			
		}

		

	}

	public class PayableNewCRNoteProcessor extends AbstractCRNoteProcessor{
		
		/*public CreditNotes processCreditNotes(String corpOrg,
				DocumentHandler unPackagerDoc, DocumentHandler creditNoteDoc,
				MediatorServices mediatorServices, String linkedToInstrType)
				throws RemoteException, AmsException {
			super.processCreditNotes(corpOrg, unPackagerDoc,
					creditNoteDoc, mediatorServices, linkedToInstrType);
			crn.setAttribute("seller_tps_customer_id", creditNoteDoc.getAttribute("/SellerTPSCustomerID"));
			
			return null;
		}*/

		
	}
	private AbstractCRNoteProcessor getCRNoteProcessor(String linkedInstrType) {
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(linkedInstrType)) {
			return new PayableNewCRNoteProcessor();
		}
		
		return null;

	}

	private AbstractInvoiceProcessor getInvProcessor(String linkedInstrType) {
		if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(linkedInstrType)) {
			return new PayableNewInvoiceProcessor();
		}
		if (TradePortalConstants.INV_LINKED_INSTR_REC.equals(linkedInstrType)) {
			return new ReceivableNewInvoiceProcessor();
		}
		return null;

	}

}
