package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.MatchPayDedGlDtls;
import com.ams.tradeportal.busobj.PayMatchResult;
import com.ams.tradeportal.busobj.PayRemit;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.StringFunction;


/**
 * Manages PAYINVIN Message Packaging...
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class PayInvPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PayInvPackagerMediatorBean.class);

	/** Constant value representing a Recall action type. **/
	public static final String PAYINVIN_ACTION_TYPE_RECALL = "C";

	/** Constant value representing a Wait action type. **/
	public static final String PAYINVIN_ACTION_TYPE_WAIT = "W";

	/** Constant value representing a Matched action type. **/
	public static final String PAYINVIN_ACTION_TYPE_MATCHED = "M";

	/** Static Map that maps the trade portal matching status to the expected
	 *  values in the packaged document. **/
	private static Map matchStatusMap = new HashMap();

	// Static initialization mapping the reference codes for Matching status to the expected
	// value for the packaged document.
	static{
		matchStatusMap.put("PPD", "P");
		matchStatusMap.put("AMD", "A");
		matchStatusMap.put("MMD", "M");
		matchStatusMap.put("NMD", "U");
        matchStatusMap.put("ANM", "U");
        matchStatusMap.put("PMD", "M");
	}

  /**
   * Entrance point into the PayInv Packager.
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc,
			MediatorServices mediatorServices) throws RemoteException, AmsException {
		try {
			mediatorServices.debug("inputDoc:"+inputDoc);
			String messageType = MessageType.PAYMENT_MATCH;
			String messageID = inputDoc.getAttribute("/message_id");
			String payRemitOid = inputDoc.getAttribute("/transaction_oid");
 			// Need to get the processing parameters			
			Map parms = parseParameters(inputDoc.getAttribute("/process_parameters"));

			mediatorServices.debug("Beginning packaging Pay Remit Information.");
			mediatorServices.debug("The pay_remit_oid id is " + payRemitOid);
			
			// Create the PayRemit object.
			PayRemit payRemit = null;
			if(payRemitOid != null && !"".equals(payRemitOid)){
				payRemit = (PayRemit) mediatorServices.createServerEJB("PayRemit", Long.parseLong(payRemitOid));
			}

			// Generate the Output Document			
			outputDoc = packageComponents(payRemit, parms, messageType, messageID, mediatorServices);
			if(payRemit != null){
				payRemit.save();
			}		
			
		} catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
					TradePortalConstants.TASK_NOT_SUCCESSFUL, "PAYINV Message Packaging");
			if(outputDoc != null){
				mediatorServices.debug("The outputDoc generated so far is:" + "\n" + outputDoc.toString());
			}
			e.printStackTrace();

		}
		return outputDoc;
	}

	/**
	 * This method is responsible for packaging the request into the outputDoc.
	 *
	 * @param payRemit The current PayRemit object.
	 * @parms parms The process parameters for the message.
	 * @param messageType The message type being processed.
	 * @param messageID The message ID of the message being processed.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents the Body component of the document.
	 */	
	private DocumentHandler packageComponents(PayRemit payRemit, Map parms, String messageType, String messageID, MediatorServices mediatorServices) throws AmsException, RemoteException{
		DocumentHandler doc = new DocumentHandler();
		mediatorServices.debug("Packaging Header Attributes : ");
		DocumentHandler header = packageHeader(messageType, messageID, mediatorServices);
		doc.addComponent("/Proponix/Header",header);

		mediatorServices.debug("Packaging subHeader Attributes : ");
		DocumentHandler subHeader = packageSubHeader(payRemit, parms, mediatorServices);
		doc.addComponent("/Proponix/SubHeader",subHeader);

		DocumentHandler body = null;
		
		// Now construct the body if necessary.
		String actionType = (String)parms.get("ActionType");
		if(actionType == null || PAYINVIN_ACTION_TYPE_MATCHED.equals(actionType)){
			mediatorServices.debug("Packaging Body Attributes : ");
			body = packageBody(payRemit, mediatorServices);
			doc.addComponent("/Proponix/Body", body);
		} 	

		mediatorServices.debug("Final outputDoc is here :" + "\n" + doc.toString());

		return doc;
	}
	
	
	/**
	 * This method takes the PayRemit object and populates the Body document component along with
	 * the child document components.
	 *
	 * @param payRemit The current PayRemit object.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents the Body component of the document.
	 */
	private DocumentHandler packageBody(PayRemit payRemit, MediatorServices mediatorServices)throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();

		DocumentHandler payRemitDoc = packagePayRemit(payRemit, mediatorServices);
		doc.addComponent("/PayRemit",payRemitDoc);

		return doc;
	}

	/**
	 * This method takes the PayRemit object and populates the PayRemit document component along with
	 * the child document components.
	 *
	 * @param payRemit The current PayRemit object.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents the PayRemit component of the document.
	 */
	private DocumentHandler packagePayRemit(PayRemit payRemit, MediatorServices mediatorServices)throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();

		// Only the ValueDate and Unapplied Amt are passed to OTL.
		doc.setAttribute("/ValueDate", payRemit.getAttribute("payment_value_date"));
		doc.setAttribute("/UnappliedAmt", payRemit.getAttribute("unapplied_payment_amount"));
        doc.setAttribute("/ApproveDiscount", payRemit.getAttribute("discount_indicator")); // PTUJ031270584

		ComponentList payMatchResults = (ComponentList)payRemit.getComponentHandle("PayMatchResultList");
		for(int i = 0; i < payMatchResults.getObjectCount(); i++){
			payMatchResults.scrollToObjectByIndex(i);
			PayMatchResult payMatchResult = (PayMatchResult)payMatchResults.getBusinessObject();
            
            // W Zhu 2/3/09 MMUJ010840001 begin
            // W Zhu 3/11/09 MNUJ030467825 add AMD
            // No need to send PPD (Proposed) or AMD (auto matched) back to OTL.
            // They have not been changed by TP.
            String matchStatus = payMatchResult.getAttribute("invoice_matching_status");
			//Srinivasu_D IR#40932 Rel9.3 07/15/2015 - Removed AMD as to be included as part of msg
           // if ("AMD".equals(matchStatus) || "PPD".equals(matchStatus)) {
            if ("PPD".equals(matchStatus)) {
                continue;
            }
			//Srinivasu_D IR#40932 Rel9.3 07/15/2015 - End
            // W Zhu 2/3/09 MMUJ010840001 end

			// Must use MergeDocument instead of add component because the MatchingInfo component must
			// be after the ValueDate and Unapplied amount.
			DocumentHandler matchingInfoDoc = new DocumentHandler(new DocumentNode("MatchingInfo"));
			DocumentHandler contents = packageMatchingInfo(payMatchResult, mediatorServices);
			matchingInfoDoc.addComponent("/", contents);

			doc.mergeDocument("/",matchingInfoDoc);
		}

		// RemitInvoice information is omitted when going to OTL
		// Party information is omitted when going to OTL

		return doc;
	}

	/**
	 * This method takes the PayMatchResult object and populates a MatchingInfo document component along with
	 * the child document components.
	 *
	 * @param payMatchResult The current PayMatchResult object.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents a MatchingInfo component of the document.
	 */
	private DocumentHandler packageMatchingInfo(PayMatchResult payMatchResult, MediatorServices mediatorServices)throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();


        // W Zhu 12/22/08 WAUI121944792 add PayRemitInvUoid
        doc.setAttribute("/PayRemitInvUoid", payMatchResult.getAttribute("otl_pay_remit_invoice_uoid"));
		doc.setAttribute("/InvoiceIDMatch", payMatchResult.getAttribute("invoice_reference_id"));
		doc.setAttribute("/OTLInvoiceUoid", payMatchResult.getAttribute("otl_invoice_uoid"));
		doc.setAttribute("/MatchStatus", (String)matchStatusMap.get(payMatchResult.getAttribute("invoice_matching_status")));
		doc.setAttribute("/AmtMatched", payMatchResult.getAttribute("matched_amount"));
		//AiA IR#T36000018260 - Send the Discount Applied Amount to TPS - 06/15/2013 - START
		//DiscountApplied
		doc.setAttribute("/DiscountApplied", payMatchResult.getAttribute("discount_applied"));
		//
		//IR#T36000018260 - END
		doc.setAttribute("/Explanation", payMatchResult.getAttribute("match_explanation"));

		//Rel8.2 IR T36000014376 - 04/04/13 - Check ERP setting - Start
		//Scope of User object expanded
		User user = null;
		CorporateOrganization corpOrg = null;
		String erpSetting = "";
        // Get the associated User to get the login id
        // W Zhu 12/26/08 WAUI121944792 check for empty user_oid
        String userOid = payMatchResult.getAttribute("user_oid");
        if (userOid != null && !userOid.equals("")) {
            user = (User)mediatorServices.createServerEJB("User", Long.parseLong(userOid));
            doc.setAttribute("/PortalUser", user.getAttribute("login_id"));
        }
        
        doc.setAttribute("/OutstandingAmt", payMatchResult.getAttribute("invoice_outstanding_amount"));
        
        //AAlubala - CR741 Rel 8.2 IR T36000014376 - 04/04/13 - Check ERP setting
        //IR T36000016039 04/22/2013 Add the Seller ERP Detail info
        //
        String ownerOrg = "";
        if(user != null){
        	ownerOrg = user.getAttribute("owner_org_oid");
        }
        //Obtain ERP setting value for this corporate org
        if(StringFunction.isNotBlank(ownerOrg)){
        	try{
        		corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(ownerOrg));
        	}catch (AmsException e){
        		String parentIDAttribute = payMatchResult.getParentIDAttributeName(false);
        		String parentID = payMatchResult.getAttribute(parentIDAttribute);
        		PayRemit payRemit = (PayRemit) mediatorServices.createServerEJB("PayRemit", Long.parseLong(parentID));
        		if (payRemit != null){
        			String slOrgOid = payRemit.getAttribute("seller_corp_org_oid");
        			corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(slOrgOid));
        		}
        	}
	        erpSetting = corpOrg.getAttribute("erp_transaction_rpt_reqd");
	    }
        //if(TradePortalConstants.INDICATOR_YES.equals(erpSetting))
        	doc = packagePayDeductionInfo(payMatchResult,mediatorServices,doc);
        //
        //IR T36000014376, T36000016039 - End
        //End CR741
        
		return doc;
	}


	/**
	 * This method takes the PayRemit object and the process parameters and
	 * populates the SubHeader document component
	 *
	 * @param payRemit The current PayRemit object.
	 * @param parms The process parameters.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents the SubHeader component of the document.
	 */
	private DocumentHandler packageSubHeader(PayRemit payRemit, Map parms, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();
		
		String actionType = (String)parms.get("ActionType");
		actionType = actionType == null ? PAYINVIN_ACTION_TYPE_MATCHED : actionType;	
	
		// Use the payRemit object if it exists, otherwise rely on the parameters.  The payRemit object should only be 
		// null if the action type is not "M"
		if(payRemit != null){
			doc.setAttribute("/SellrCustID", payRemit.getAttribute("seller_reference"));
			doc.setAttribute("/Currency", payRemit.getAttribute("currency_code"));
			doc.setAttribute("/PayInvRefID", payRemit.getAttribute("payment_invoice_reference_id"));
			doc.setAttribute("/OTLPayRemitUoid", payRemit.getAttribute("otl_pay_remit_uoid"));			
			doc.setAttribute("/ActionType", actionType); //IR - MUUJ022361235
			// Only include the SellerIdForByr and SellerNmForBuyer if the indicator is set to 'Y'
			if(TradePortalConstants.INDICATOR_YES.equals(payRemit.getAttribute("buyer_added_in_portal_indicator"))){
				doc.setAttribute("/SellerIdForByr", payRemit.getAttribute("seller_id_for_buyer"));
				doc.setAttribute("/SellerNmForByr", payRemit.getAttribute("seller_name_for_buyer"));
				payRemit.setAttribute("buyer_added_in_portal_indicator", "");
			}			
		}else{
			doc.setAttribute("/SellrCustID", (String)parms.get("SellrCustID"));
			doc.setAttribute("/Currency", (String)parms.get("Currency"));
			doc.setAttribute("/PayInvRefID", (String)parms.get("PayInvRefID"));
			doc.setAttribute("/OTLPayRemitUoid", (String)parms.get("OTLPayRemitUoid"));
			doc.setAttribute("/ActionType", actionType);//IR - MUUJ022361235
		}
		//doc.setAttribute("/ActionType", actionType);//IR - MUUJ022361235
			
		return doc;
	}

	/**
	 * This method takes the messageType and the messageId
	 * populates the Header document component
	 *
	 * @param messageType The messageType being requested.
	 * @param messageID The message ID.
	 * @param mediatorServices The MediatorServices helper object.
	 *
	 * @param doc A documentHandler that represents the SubHeader component of the document.
	 */
	private DocumentHandler packageHeader(String messageType, String messageID, MediatorServices mediatorServices)
			throws RemoteException, AmsException {
		DocumentHandler doc = new DocumentHandler();

		String dateSent = DateTimeUtility.getGMTDateTime(false);
		String timeSent = dateSent.substring(11,19);

		doc.setAttribute("/DestinationID", TradePortalConstants.DESTINATION_ID_PROPONIX);
		doc.setAttribute("/SenderID", TradePortalConstants.SENDER_ID);     // NSX CR-542 03/04/10
		doc.setAttribute("/OperationOrganizationID", "");
		doc.setAttribute("/MessageType", messageType);
		doc.setAttribute("/DateSent", dateSent);
		doc.setAttribute("/TimeSent", timeSent);
		doc.setAttribute("/MessageID", messageID);
		return doc;
	}

	/**
	 * This method takes "process_parameters" string value from the input document and
	 * converts it to a Map.
	 * The processParms string will be in the format of name=value with multiple parameters
	 * delimited by | (pipe).
	 * eg.
	 * 	"parm1=parmValue1|parm2=parmValue2|parm3=parmValue3"
	 *
	 * @param processParms The parameters string
	 *
	 * @param parms A map containing the parameter values, keyed by the parameter name.
	 */
	public static Map parseParameters(String processParms){
		Map parms = new HashMap();

		if(processParms == null || processParms.equals("")){
			return parms;
		}

		StringTokenizer st = new StringTokenizer(processParms, "|");
		while(st.hasMoreTokens()){
			String pair = st.nextToken();
			int equalIndex = pair.indexOf("=");
			if(equalIndex > 0){
				parms.put(pair.substring(0,equalIndex), pair.substring(equalIndex+1));
			}
		}
		return parms;
	}
	
	/**
	 * AAlubala - 11/28/2012
	 * CR741 Rel 8.2
	 * This method takes the PayMatchResult object and populates 
	 * SellerERPDetail 
	 * 
	 * @param payMatchResult The current PayMatchResult object.
	 * @param mediatorServices The MediatorServices helper object.
	 * @param doc 
	 *
	 * @param doc A documentHandler that represents a MatchingInfo component of the document.
	 */
	//private void packagePayDeductionInfo(PayMatchResult payMatchResult, MediatorServices mediatorServices, DocumentHandler doc)throws RemoteException, AmsException {
	private DocumentHandler packagePayDeductionInfo(PayMatchResult payMatchResult, MediatorServices mediatorServices, DocumentHandler doc)throws RemoteException, AmsException {
		//DocumentHandler doc = new DocumentHandler();
		
		//Get the Invoice Id
		String matchedInvoiceId = payMatchResult.getAttribute("otl_invoice_uoid");
		String sysDateTime = DateTimeUtility.getGMTDateTime(false);
		
		//IValavala CR 741 get the oid from matchPayDedGlDtls first.
		String matchPayDedGlDtlsOid="";
		//jgadela R92 - SQL INJECTION FIX     
		String sql = "SELECT MATCH_PAY_DED_GL_DTLS_OID FROM MATCH_PAY_DED_GL_DTLS WHERE OTL_INVOICE_OID = ? ";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql,true, new Object[]{matchedInvoiceId});
		if (resultXML != null && (resultXML.getFragments("/ResultSetRecord/").size() > 0)) {
			
			for (int i = 0; i < resultXML.getFragments("/ResultSetRecord/").size(); i++){
				matchPayDedGlDtlsOid = resultXML.getAttribute("/ResultSetRecord(" + i + ")/MATCH_PAY_DED_GL_DTLS_OID");
				DocumentHandler sellerERPDetailDoc = new DocumentHandler(new DocumentNode("SellerERPDetail"));
				DocumentHandler contents = new DocumentHandler();
				if (StringFunction.isNotBlank(matchPayDedGlDtlsOid))
				{
					MatchPayDedGlDtls matchPayDedGlDtls = (MatchPayDedGlDtls) mediatorServices.createServerEJB("MatchPayDedGlDtls", Long.parseLong(matchPayDedGlDtlsOid));
					if(matchPayDedGlDtls != null){						
						contents.setAttribute("/InvoiceIDMatch", matchPayDedGlDtls.getAttribute("invoice_reference_id"));
						contents.setAttribute("/TransactionDate", sysDateTime);
						contents.setAttribute("/TransactionType", matchPayDedGlDtls.getAttribute("transaction_type"));
						contents.setAttribute("/AppliedAmt", matchPayDedGlDtls.getAttribute("applied_amount"));
						contents.setAttribute("/TransactionGLCode", matchPayDedGlDtls.getAttribute("general_ledger_code"));
						contents.setAttribute("/DiscountCode", matchPayDedGlDtls.getAttribute("discount_code"));		
						contents.setAttribute("/DiscountDescription", matchPayDedGlDtls.getAttribute("discount_description"));
						contents.setAttribute("/DiscountComments", matchPayDedGlDtls.getAttribute("discount_comments"));
						contents.setAttribute("/SystemDateTime", matchPayDedGlDtls.getAttribute("system_date_time"));
						contents.setAttribute("/OtlInvoiceOid", matchPayDedGlDtls.getAttribute("otl_invoice_oid"));
					}
					sellerERPDetailDoc.addComponent("/", contents);
					doc.mergeDocument("/", sellerERPDetailDoc);
					
				}
			}
			
		}
		return doc;
	}

 
}
