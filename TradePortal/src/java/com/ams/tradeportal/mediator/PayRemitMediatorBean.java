package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Vector;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.EmailTrigger;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.PayRemit;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.ThresholdGroup;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.CertificateUtility;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.PanelAuthProcessor;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.RouteServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *     Handles the requests from Receivables Match Notices page.
 *     (ReceivablesManagementHome.jsp & ReceivablesManagement_Notices.frag)
 *
 *     Route, Authorize, Approve Discount & Authorize Pay Remit.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PayRemitMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PayRemitMediatorBean.class);

    // Business methods
    public DocumentHandler execute(DocumentHandler inputDoc,
            DocumentHandler outputDoc, MediatorServices mediatorServices)
            throws RemoteException, AmsException {

        DocumentHandler payRemitDoc = null;
        String[] substitutionParms = null;
        Vector payRemitList = null;
        String payRemitData = null;
        String payRemitOid = null;
        String payRemitID = null;
        String userOid = null;
        boolean previouslyLocked = false;
        boolean processSuccess = false;
        int count = 0;

        mediatorServices.debug("PayRemitMediator input doc: "
                + inputDoc);
        outputDoc = new DocumentHandler();

        // PMitnala CR 821 Rel 8.3 START
 	   	if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(inputDoc.getAttribute("/Update/ButtonPressed"))){
 	   			processSuccess = performNotifyPanelAuthUser(inputDoc, mediatorServices);
 	   			return outputDoc;
 	   	}
 	   		//PMitnala CR 821 Rel 8.3 END
        // populate variables with inputDocument information such as transaction
        // list, user oid,
        // and security rights, etc.
        payRemitList = inputDoc.getFragments("/PayRemitList/PayRemits");
        userOid = inputDoc.getAttribute("/User/userOid");
        String securityRights = inputDoc.getAttribute("/User/securityRights");
        count = payRemitList.size();

        mediatorServices.debug("PayRemitMediator: number of Pay Remit: " + count);

        if (count <= 0) {
            mediatorServices.getErrorManager().issueError(
                    TradePortalConstants.ERR_CAT_1,
                    TradePortalConstants.NO_ITEM_SELECTED,
                    mediatorServices.getResourceManager().getText(
                            "TransactionAction.Route",
                            TradePortalConstants.TEXT_BUNDLE));

            return outputDoc;
        }
       
        // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
        String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
        LOG.debug("PayRemitMediator:: Recert Reqt: " + reCertification);
        if (TradePortalConstants.INDICATOR_MULTIPLE.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
            //ir ctuk113042306 11/29/2010 - if an authentication error occurred on a list
            // issue an individual error for each pay remit
            issueAuthenticationErrors(payRemitList, mediatorServices);
            return new DocumentHandler();
        }

        
        // a transaction should be locked and processed at the instrument level
        // since the transaction is a component of instrument
        // for each transaction, lock the instrument and call routeTransaction
        // on the instrument. routeTransaction is a transactional method.
        for (int i = 0; i < count; i++) {
            payRemitDoc = (DocumentHandler) payRemitList.get(i);
            payRemitData = payRemitDoc.getAttribute("/payRemitData");
            int separator1 = payRemitData.indexOf('/');

            payRemitOid = payRemitData.substring(0, separator1);
            payRemitID = payRemitData.substring(separator1 + 1);

            // Before creating the instrument, attempt to lock the instrument
            try {
                LockingManager.lockBusinessObject(Long.parseLong(payRemitOid),
                        Long.parseLong(userOid), true);

                mediatorServices.debug("PayRemitMediator: Locking PayRemit "
                        + payRemitOid + "successful");
            } catch (InstrumentLockException e) {
                // if the instrument is currently locked by this user, continue
                // processing
                if (e.getUserOid() == Long.parseLong(userOid)) {
                    previouslyLocked = true;
                } else {
                    // issue instrument in use error indicating instrument ID,
                    // transaction
                    // type, first name and last name of user that has
                    // instrument reserved

                    substitutionParms = new String[] {
                            mediatorServices.getResourceManager().getText(
                                    "ARMatchNotices.ReceivablesMatchNotices",
                                    TradePortalConstants.TEXT_BUNDLE),
                            payRemitID,
                            e.getFirstName(),
                            e.getLastName(),
                            mediatorServices.getResourceManager().getText(
                                    "TransactionAction.Routed",
                                    TradePortalConstants.TEXT_BUNDLE) };

                    mediatorServices.getErrorManager().issueError(
                            TradePortalConstants.ERR_CAT_1,
                            TradePortalConstants.TRANSACTION_IN_USE,
                            substitutionParms);

                    continue;
                }
            }

            try {
                // Based on the button clicked, process an appropriate action.
                String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
                mediatorServices.debug("The button pressed is " + buttonPressed);

                if (buttonPressed.equals(TradePortalConstants.BUTTON_AUTHORIZE)) {
                    processSuccess = performAuthorize(userOid, securityRights, payRemitOid, false, mediatorServices);
                } else if (buttonPressed.equals(TradePortalConstants.BUTTON_APPROVE_DISCOUNT_AUTHORIZE)) {
                    processSuccess = performAuthorize(userOid, securityRights, payRemitOid, true, mediatorServices);
                } else if (buttonPressed.equals(TradePortalConstants.BUTTON_ROUTE)) {
                    processSuccess = performRoute(inputDoc, outputDoc, payRemitOid, mediatorServices,securityRights);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // if the route fails and the Pay Remit was previously locked,
                // don't unlock the Pay Remit.
                if (!(!processSuccess && previouslyLocked)) {
                    try {
                        LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(payRemitOid), true);
                    } catch (InstrumentLockException e) {
                        // this should never happen
                        e.printStackTrace();
                    }
                }
            }

        }

        mediatorServices.debug("PayRemitMediator outputDoc: " + outputDoc);
        return outputDoc;
    }


	/**
	 * Perform the Routing on the Pay Remit.
     *
	 * @return boolean.  true - success.  false - failure.
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
     * @param outputDoc com.amsinc.ecsg.util.DocumentHandler
     * @param payRemitOid String
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
     *
	 */

	static protected boolean performRoute(DocumentHandler inputDoc, DocumentHandler outputDoc, String payRemitOid, MediatorServices mediatorServices,String securityRights)
	throws RemoteException, AmsException {
	      PayRemit                payRemit             = null;
	      StringBuffer	      routeToUserOid 	   = new StringBuffer();
	      String		      routeValidation      = null;
	      
	      // create and load the PayRemit.
	      payRemit = (PayRemit) mediatorServices.createServerEJB("PayRemit", Long.parseLong(payRemitOid));

	      payRemit.setAttribute("assigned_to_user_oid", routeToUserOid.toString());

	      //String corpOrgOid = payRemit.getAttribute("seller_corp_org_oid");
          String payRemitID = payRemit.getAttribute("payment_invoice_reference_id");

	      if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_ROUTE)) {
	            // issue error that user does not have 'authorize'
	            // authority.
	    	  mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
	                            "MessageAction.Route",
	                            TradePortalConstants.TEXT_BUNDLE));

	            // return false to set the transaction status to failed
	            return false;

	        }
	      routeValidation = RouteServices.validateRouteData(inputDoc, mediatorServices, routeToUserOid);

	      // if there are errors validating the route user/organization,
	      // set the outputDoc to return routeSelectionError is True
	      // note: using getmaxerrorseverity from mediator services won't work here
	      if (routeValidation.equals(TradePortalConstants.ROUTE_SELECTION_ERROR))
	      {
	          outputDoc.setAttribute("/Route/RouteSelectionErrors",
		      TradePortalConstants.INDICATOR_YES);
	          return false;
	      }
	      else if (routeValidation.equals(TradePortalConstants.ROUTE_GENERAL_ERROR))
	      {
	          return false;
	      }


	     

          payRemit.save();

          // Create audit entry.
          //TradePortalAuditor.createRefDataAudit(mediatorServices,
          //        payRemit,
          //        payRemitOid,
          //        payRemitID,
          //        corpOrgOid,
          //        TradePortalConstants.CHANGE_TYPE_DISPUTE,
          //        inputDoc.getAttribute("/User/userOid"));

          // issue message that the transaction has been successfully routed
          mediatorServices.getErrorManager().issueError(PayRemitMediator.class.getName (),
               TradePortalConstants.TRANSACTION_PROCESSED,
               mediatorServices.getResourceManager().getText(
                       "ARMatchNotices.ReceivablesMatchNotices",
                       TradePortalConstants.TEXT_BUNDLE),
               payRemitID ,
               mediatorServices.getResourceManager().getText(
                       "TransactionAction.Routed",
                       TradePortalConstants.TEXT_BUNDLE));

          return true;

	}


	/**
	 * Perform the Authorization on Pay Remit.
	 *
	 * @return boolean.  true - success.  false - failure.
	 * @param userOid String
     * @param securityRights String
     * @param payRemitOid String
     * @param approveDiscount boolean - Whether to approve discount.
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	static protected boolean performAuthorize(String userOid, String securityRights, String payRemitOid, boolean approveDiscount,
            MediatorServices mediatorServices) throws RemoteException,
            AmsException {

        // get each PayRemit Oid and process each transaction; each transaction
        // is unique within itself.
        boolean trxAuthorized = authorizePayRemit(payRemitOid, userOid,
                securityRights, approveDiscount, mediatorServices);

        if (!trxAuthorized) {
            PayRemit payRemit = (PayRemit) mediatorServices.createServerEJB("PayRemit",
                    Long.parseLong(payRemitOid));
            payRemit.setAttribute("transaction_status",
                    TransactionStatus.AUTHORIZE_FAILED);
            payRemit.setAttribute("transaction_status_date", DateTimeUtility
                    .getGMTDateTime());
            try
            {
                payRemit.save();
            }
            catch(Exception e)
            {
                LOG.info(e.toString());
                e.printStackTrace();
            }
            return false;
        }

        return true;

    }

	 /**
	* This method updates the status of the Pay Remit to Authorized
	* if it passes validations.  It returns false if validations fail. The
	* Failed status cannot be set here because this is a transactional method
	* that will rollback when any error messages are issued.
	* If the transaction is Authorized, a new outgoing interface is created.
	*
	* This is a transactional method.  An assumption made is that the user has
	* the instrument locked currently.
	*
	* @param payRemitOid java.lang.String - the Pay Remit oid to authorize
	* @param userOid  java.lang.String - the user oid used to delete transaction
	* @param securityRights java.lang.String - the user's security rights
    * @param approveDiscount boolean - Whether to approve discount.
	* @return boolean - false if authorizing a transaction is unsuccessful and the
	* 			transaction status has to be set to FAILED,
	*                   true if the transaction successfully saves OR if the
	*                   the authorization fails BUT transaction should not be
	*                   set to FAILED.
   */
   static private boolean authorizePayRemit(String payRemitOid, String userOid,
            String securityRights, boolean approveDiscount, MediatorServices mediatorServices)
            throws RemoteException, AmsException {

        boolean errorsFound = false;
        boolean createOutgoingQueueAndIssueSuccess = false;
        boolean suppressTransactionProcessedMessage = false;
        String userOrgOid = null;
        String panelAuthTransactionStatus = TradePortalConstants.AUTHFALSE;
        OutgoingInterfaceQueue oiQueue = null;
        CorporateOrganization corpOrg = null;

        User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
        userOrgOid = user.getAttribute("owner_org_oid");

        PayRemit payRemit = (PayRemit) mediatorServices.createServerEJB("PayRemit",
                Long.parseLong(payRemitOid));

        // ***********************************************************************
        // check transaction_status - if transaction_status is not authorizable,
        // return error without updating pay_remit information
        // ***********************************************************************

        String transactionStatus = payRemit.getAttribute("transaction_status");
        if (!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_MATCH_REQUIRED)
                &&!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_IN_PROGRESS)
                &&!transactionStatus.equals(TransactionStatus.READY_TO_AUTHORIZE)
                &&!transactionStatus.equals(TransactionStatus.PARTIALLY_AUTHORIZED)
                &&!transactionStatus.equals(TransactionStatus.AUTHORIZE_FAILED)
                &&!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_DISC_APPROVAL)
                &&!transactionStatus.equals(TradePortalConstants.TRANS_STATUS_DISC_APPR_FAILED))
        {
            // issue error that transaction is not in an authorizable state
            mediatorServices.getErrorManager().issueError(
                    PayRemitMediator.class.getName(),
                    TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
                    mediatorServices.getResourceManager().getText(
                            "ARMatchNotices.ReceivablesMatchNotices",
                            TradePortalConstants.TEXT_BUNDLE),
                    payRemit.getAttribute("payment_invoice_reference_id") ,
                    ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_STATUS",
                            payRemit.getAttribute("transaction_status"),
                            mediatorServices.getCSDB().getLocaleName()),
                    mediatorServices.getResourceManager().getText(
                            "TransactionAction.Authorized",
                            TradePortalConstants.TEXT_BUNDLE));
            // return true to NOT set the transaction status to failed
             return true;
        }

        // ***********************************************************************
        // check to make sure that the user is allowed to authorize the
        // pay remit - the user has to be a corporate user (not an admin
        // user) and have the rights to authorize a Receivables
        // ***********************************************************************

        if (!(user.getAttribute("ownership_level"))
                .equals(TradePortalConstants.OWNER_CORPORATE)) {
            mediatorServices.getErrorManager().issueError(PayRemitMediator.class.getName(),
                    TradePortalConstants.ADMIN_USER_CANNOT_AUTHORIZE,
                    user.getAttribute("first_name"),
                    user.getAttribute("last_name"));

            // return false to set the transaction status to failed
            return false;
        } else if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_AUTHORIZE) && !approveDiscount) {
            // issue error that user does not have 'authorize'
            // authority.
            mediatorServices.getErrorManager().issueError(
                    PayRemitMediator.class.getName(),
                    TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
                    payRemit.getAttribute("payment_invoice_reference_id") ,
                    mediatorServices.getResourceManager().getText(
                            "TransactionAction.Authorize",
                            TradePortalConstants.TEXT_BUNDLE));

            // return false to set the transaction status to failed
            return false;

        }else if (!SecurityAccess.hasRights(securityRights, SecurityAccess.RECEIVABLES_APPROVE_DISCOUNT_AUTHORIZE) && approveDiscount) {
            // issue error that user does not have 'Approve discount & authorize'
            // authority.
            mediatorServices.getErrorManager().issueError(
                    PayRemitMediator.class.getName(),
                    TradePortalConstants.NO_ACTION_TRANSACTION_AUTH,
                    payRemit.getAttribute("payment_invoice_reference_id") ,
                    mediatorServices.getResourceManager().getText(
                            "SecurityProfileDetail.ReceivablesApproveDiscountAuthorize",
                            TradePortalConstants.TEXT_BUNDLE));

            // return false to set the transaction status to failed
            return false;

        }

        corpOrg = (CorporateOrganization) mediatorServices.createServerEJB(
                "CorporateOrganization", Long.parseLong(userOrgOid));

        String baseCurrency = corpOrg.getAttribute("base_currency_code");

        // ***********************************************************************
        // If the user has threshold group limits, check the two limits,
        // transaction limits and daily limits. Transaction amount has to be
        // converted from transaction currency to the instrument's corporate
        // org's
        // base currency, since all limits are in base currency.
        // Note that both limits should be checked even if the other fails. This
        // is to ensure that all limits errors are provided at once.
        // If we are approving the discount, check the thrshold for approve discount
        // as well.
        // ***********************************************************************

        String currency = payRemit.getAttribute("currency_code");
        String amount = payRemit.getAttribute("payment_amount");

        if (approveDiscount) {
            if ( !checkThresholds(amount, currency, baseCurrency,
                    "ar_approve_discount_thold", "ar_approve_discount_dlimit", userOid, userOrgOid,
                    user, true, mediatorServices)){
                errorsFound = true;
                return false;
            }

        }
        else {
            if (TradePortalConstants.INDICATOR_YES.equals(payRemit.getAttribute("discount_indicator"))) {
				            	 payRemit.setAttribute("discount_indicator",
				                         TradePortalConstants.INDICATOR_NO);
            }
        }

        if (!checkThresholds(amount, currency, baseCurrency,
                "ar_match_response_thold", "ar_match_response_dlimit", userOid, userOrgOid,
                user, false, mediatorServices)) {
            errorsFound = true;
            return false;
        }

        // ***********************************************************************
        // The payment amount should be equal to the matched amount
        // ***********************************************************************

        String sqlQuery = "select sum(matched_amount) as sum_matched_amount from pay_match_result where p_pay_remit_oid = ? and invoice_matching_status in (?,?,?)";

          DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{payRemitOid,TransactionType.AMEND,TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH,TradePortalConstants.MATCHING_STATUS_PARTIALLY_MATCHED});

          // Retrieve the total for the SQL query that was just constructed
          BigDecimal matchedAmount = BigDecimal.ZERO ;
          try {
             matchedAmount = resultSet.getAttributeDecimal("/ResultSetRecord/SUM_MATCHED_AMOUNT");
          }
          catch(Exception e) {
             /* Ignore null amount */
          }

          BigDecimal paymentAmount = payRemit.getAttributeDecimal("payment_amount");
          BigDecimal unappliedAmount = payRemit.getAttributeDecimal("unapplied_payment_amount");

          // W Zhu VIUJ011957614 error if matched amount > payment amount
          if (matchedAmount.compareTo(paymentAmount) > 0) {
              mediatorServices.getErrorManager().issueError(
                      PayRemitMediator.class.getName(),
                      TradePortalConstants.MATCH_AMT_GREATER_THAN_PAYMENT);
              errorsFound = true;
              return false;
          }

          // Error if match amount + unapplied amount < payment amount
          if (matchedAmount.add(unappliedAmount).compareTo(paymentAmount) != 0) {
              mediatorServices.getErrorManager().issueError(
                      PayRemitMediator.class.getName(),
                      TradePortalConstants.PAYMENT_AMT_NOT_EQUAL_MATCH_AMT);
              errorsFound = true;
              return false;
          }



          // ***********************************************************************
          // There can be only one matching for each invoice.
          // ***********************************************************************

          sqlQuery = "select count(*) as Number_Of_Multiple_Matches from (select a_invoice_oid, count(*) as count from pay_match_result"
          + "  where p_pay_remit_oid = ? and invoice_matching_status in (?,?,?) group by a_invoice_oid) where count > 1";

            resultSet = DatabaseQueryBean.getXmlResultSet(sqlQuery, true, new Object[]{payRemitOid,TransactionType.AMEND,TradePortalConstants.MATCHING_STATUS_MANUAL_MATCH,TradePortalConstants.MATCHING_STATUS_PARTIALLY_MATCHED});
            resultSet = resultSet.getFragment("/ResultSetRecord");

            // Retrieve the total for the SQL query that was just constructed
            int numberOfMultipleMatches = 0;
            try {
                numberOfMultipleMatches = resultSet.getAttributeInt("/NUMBER_OF_MULTIPLE_MATCHES");
            }
            catch(Exception e) {
               /* Ignore null amount */
            }

            if (numberOfMultipleMatches > 0) {
                mediatorServices.getErrorManager().issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.MORE_THAN_ONE_MATCH_FOR_INVOICE);
                errorsFound = true;
                return false;
            }



        // ***********************************************************************
        // if a different user should authorize this transaction
        // and verification fails, issue error and stop further validations
        // ***********************************************************************


        if (!errorsFound
                && corpOrg.getAttribute("auth_user_different_ind").equals(
                        TradePortalConstants.INDICATOR_YES)) {
            if (payRemit.getAttribute("last_entry_user_oid").equals(userOid)) {
                mediatorServices.getErrorManager().issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.DIFFERENT_AUTHORIZER_REQUIRED);
                // transaction status should be set to fail
                errorsFound = true;
                return false;
            }
        }
        
        
        // ***********************************************************************
        // If dual authorization is required, set the transaction state to
        // partially authorized if it has not been previously authorized and set
        // the transaction to authorized if there has been a previous
        // authorizer.
        // The second authorizer must not be the same as the first authorizer
        // not the same work groups, depending on the corporate org setting.
        // If dual authorization is not required, set the transaction state to
        // authorized.
        // ***********************************************************************
        
        if (!errorsFound) {
            String dualAuthIndicator = corpOrg.getAttribute("dual_auth_arm");
            String userWorkGroupOid = user.getAttribute("work_group_oid");
            String firstAuthorizer = payRemit.getAttribute("first_authorizing_user_oid");
            String firstAuthorizerWorkGroup = payRemit.getAttribute("first_authorizing_work_group_oid");
            
            //Pavani Rel 8.3 CR 821 - START
           // check if panel Authorization is applicable for "ARM"
    		if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH)
    				&& corpOrg.isPanelAuthEnabled(TradePortalConstants.NEW_RECEIVABLES)) {
    			if(StringFunction.isBlank(payRemit.getAttribute("panel_auth_group_oid"))) {
    				String panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.NEW_RECEIVABLES, corpOrg, mediatorServices.getErrorManager());
    				PanelAuthorizationGroup panelAuthGroup = null;    				
    				if(StringFunction.isNotBlank(panelGroupOid)){
    					panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
    							Long.parseLong(panelGroupOid));   					
    				}else{
    					// if panel id is blank, return the flow back.
    					// issue error as panel group is required if panel auth defined for instrument type
    					mediatorServices.getErrorManager().issueError(
    								TradePortalConstants.ERR_CAT_1,
    								AmsConstants.REQUIRED_ATTRIBUTE, 
    								mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
    				   return false;
    				}
    				payRemit.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
    				payRemit.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
    				
    	    		BigDecimal amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency(currency, amount, baseCurrency, userOrgOid, 
    	    				   TradePortalConstants.NEW_RECEIVABLES, null, false, mediatorServices.getErrorManager()); 
    	    		
    	    		String panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
    	    		if(StringFunction.isBlank(panelRangeOid)){
    	    			//SSikhakolli - Rel-8.3 Sys Test IR# T36000021058 on 09/24/2013 - Begin
        	    		String formattedAmountInBaseToPanelGroup = TPCurrencyUtility.getDisplayAmount(amount,baseCurrency,mediatorServices.getCSDB().getLocaleName());
        	    		//SSikhakolli - Rel-8.3 Sys Test IR# T36000021058 on 09/24/2013 - End
        	    		
    	    			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE, formattedAmountInBaseToPanelGroup, TradePortalConstants.NEW_RECEIVABLES );
    	    			return false;
    	    		}
    	    		payRemit.setAttribute("panel_auth_range_oid", panelRangeOid);
    			}
    			String panelGroupOid  = payRemit.getAttribute("panel_auth_group_oid");
    		    String panelOplockVal = payRemit.getAttribute("panel_oplock_val");
    		    String []panelRangeOid = new String[] {payRemit.getAttribute("panel_auth_range_oid")};

    	       
    	       PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.NEW_RECEIVABLES, mediatorServices.getErrorManager());
    	       panelAuthProcessor.init(panelGroupOid, panelRangeOid, panelOplockVal);

    	       panelAuthTransactionStatus = panelAuthProcessor.process(payRemit, payRemit.getAttribute("panel_auth_range_oid"), false);
    	       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
    	    	   payRemit.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);
                   payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                   createOutgoingQueueAndIssueSuccess = true;
    	       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	    	   payRemit.setAttribute("transaction_status", TransactionStatus.PARTIALLY_AUTHORIZED);
                   payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                   suppressTransactionProcessedMessage = true;
                   mediatorServices.getErrorManager().issueError(
                                   PayRemitMediator.class.getName(),
                                   TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
                                   mediatorServices.getResourceManager().getText(
                                           "ARMatchNotices.ReceivablesMatchNotices",
                                           TradePortalConstants.TEXT_BUNDLE),
                                   payRemit.getAttribute("payment_invoice_reference_id"));
               }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
            	   payRemit.setAttribute("transaction_status",TransactionStatus.AUTHORIZE_FAILED);
                   payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                  //MEerupula - 04 Sept 2013 - Rel8.3.0 IR-T36000020332 ADD BEGIN
               	  // Set the errorsFound flag to true so that the success message is not shown when panelAuthStatus returns false
                   errorsFound = true;  
                  //MEerupula - 04 Sept 2013 - Rel8.3.0 IR-T36000020332 ADD END 
    	       } 
    	    //Pavani Rel 8.3 CR 821 - END
    	       
            // Two work groups required and the user does not have a work group
            // assigned. Give error.
    		}else if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
                    && (userWorkGroupOid == null || userWorkGroupOid.equals(""))) {
                errorsFound = true;
                mediatorServices.getErrorManager().issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
            } else if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS)
                    || dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
                    || !(firstAuthorizer == null || firstAuthorizer.equals(""))) // pcutrone
            {
                if (firstAuthorizer == null || firstAuthorizer.equals("")) {
                    // Two authorizers required and first authorizer is EMPTY:
                    // Set to partially authorized.
                    payRemit.setAttribute("first_authorizing_user_oid", userOid);
                    payRemit.setAttribute("transaction_status",
                                    TransactionStatus.PARTIALLY_AUTHORIZED);
                    payRemit.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
                    payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                    suppressTransactionProcessedMessage = true;
                    mediatorServices.getErrorManager().issueError(
                                    PayRemitMediator.class.getName(),
                                    TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
                                    mediatorServices.getResourceManager().getText(
                                            "ARMatchNotices.ReceivablesMatchNotices",
                                            TradePortalConstants.TEXT_BUNDLE),
                                    payRemit.getAttribute("payment_invoice_reference_id"));
                } else // firstAuthorizer not empty
                {
                    // Two work groups required and this user belongs to the
                    // same work group as the first authorizer: Error.
                    if (dualAuthIndicator.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)
                            && userWorkGroupOid.equals(firstAuthorizerWorkGroup)) {
                        errorsFound = true;
                        mediatorServices.getErrorManager().issueError(
                                        PayRemitMediator.class.getName(),
                                        TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
                    }
                    // Two authorizers required and this user is the same as the
                    // first authorizer. Give error.
                    // Note requiring two work groups also implies requiring two
                    // users.
                    else if (firstAuthorizer.equals(userOid)) {
                        mediatorServices.getErrorManager().issueError(
                                        PayRemitMediator.class.getName(),
                                        TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
                        // transaction status should be set to fail
                        errorsFound = true;
                    }
                    // Authorize
                    else {
                        payRemit.setAttribute("second_authorizing_user_oid", userOid);
                        payRemit.setAttribute("second_authorizing_work_group_oid", userWorkGroupOid);
                        payRemit.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
                        payRemit.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);
                        payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                        createOutgoingQueueAndIssueSuccess = true;
                    }
                }
            } else {
                // dual auth ind is NO, first auth is EMPTY
                payRemit.setAttribute("first_authorizing_user_oid", userOid);
                payRemit.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
                payRemit.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
                payRemit.setAttribute("transaction_status", TransactionStatus.AUTHORIZED);
                payRemit.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
                createOutgoingQueueAndIssueSuccess = true;

            }

        }

        // W Zhu 3/18/09 MNUJ030467825 update invoice amounts
        if (createOutgoingQueueAndIssueSuccess) {
            // Update the invoice amounts for the auto matched ones that are unmatched (that is, matching status changes from AMD to ANM)in TP.
            // If an invoice is auto-matched in OTL and sent to TP, the invoice amounts are updated by a separate INVSTO message.
            // If the invoice is unmatched in TP, the invoice amounts need to be reversed.  The OTL will have this invoice match
            // removed so it will be able to send an INVSTO for this invoice.  So the TP has to update it by itself.
            String sqlStatement = "update invoice set pending_payment_amount = decode(pending_payment_amount, null, 0, pending_payment_amount) -  "
                         + " (select sum(matched_amount) from pay_match_result where a_invoice_oid = invoice.invoice_oid and p_pay_remit_oid = ? and invoice_matching_status = 'ANM'), "
                 + " invoice_outstanding_amount = invoice_outstanding_amount + "
                         + " (select sum(matched_amount) from pay_match_result where a_invoice_oid = invoice.invoice_oid and p_pay_remit_oid = ? and invoice_matching_status = 'ANM')"
                 + " where invoice_oid in"
                 + " (select a_invoice_oid from pay_match_result where p_pay_remit_oid = ? and invoice_matching_status = 'ANM')";
            try {
                DatabaseQueryBean.executeUpdate(sqlStatement, true, new Object[]{payRemitOid, payRemitOid, payRemitOid});
            }
            catch (java.sql.SQLException e) {
                mediatorServices.getErrorManager().issueError(PayRemitMediator.class.getName(), AmsConstants.SQL_ERROR, String.valueOf(e.getErrorCode()), e.getMessage());
                e.printStackTrace();
                errorsFound = true;
            }
       
        }

        // ***********************************************************************
        // create the interface event record if pay remit has been set to
        // authorized. It uses the Business Object to create it instead of
        // EJBObjectFactory to allow this object to participate in the current
        // transaction
        // ***********************************************************************

        if (createOutgoingQueueAndIssueSuccess) {
            oiQueue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
            oiQueue.newObject();
            String attributeNames[] = new String[] { "date_created", "status",
                    "msg_type", "message_id", "transaction_oid" };
            
			String messageId = InstrumentServices.getNewMessageID();
			
			
            String attributeValues[] = new String[] {
                    DateTimeUtility.getGMTDateTime(),
                    TradePortalConstants.OUTGOING_STATUS_STARTED,
                    "PAYINVIN",
                    messageId,
                    payRemit.getAttribute("pay_remit_oid")};

            // Set the return message id of the transaction to the message id
            // generated for the outgoing queue
            // Do we need this?
            // payRemit.setAttribute("return_message_id", messageId);

            oiQueue.setAttributes(attributeNames, attributeValues);
            if (oiQueue.save() < 0) {
                errorsFound = true;
            }
        }

        if (!errorsFound) {

            String payRemitID = payRemit.getAttribute("payment_invoice_reference_id");
            int success = payRemit.save();

            // return true to NOT set the transaction status to failed
            if (mediatorServices.getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY || 
            		                  (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus) && success > 0) ){
            	if(!suppressTransactionProcessedMessage){
	                mediatorServices.getErrorManager().issueError(PayRemitMediator.class.getName(),
	                        TradePortalConstants.TRANSACTION_PROCESSED,
	                        mediatorServices.getResourceManager().getText(
	                                "ARMatchNotices.ReceivablesMatchNotices",
	                                TradePortalConstants.TEXT_BUNDLE),
	                        payRemitID ,
	                        mediatorServices.getResourceManager().getText(
	                                "TransactionAction.Authorized",
	                                TradePortalConstants.TEXT_BUNDLE));
            	}
               return true;
            }
            else
                return false;
        } else
            // return false to set the transaction status to failed
            return false;

    }


   /**
    * Check the necessary thresholds.
    *
    * @param amount
    * @param currency
    * @param baseCurrency
    * @param thresholdAttributeName
    * @param dailyLimitAttributeName
    * @param userOid
    * @param userOrgOid
    * @param user
    * @param discountIndicator
    * @param mediatorServices
    * @return
    * @throws RemoteException
    * @throws AmsException
    */

    static private boolean checkThresholds(String amount, String currency, String baseCurrency,
            String thresholdAttributeName, String dailyLimitAttributeName,
            String userOid, String userOrgOid, User user,
            boolean discountIndicator,
            MediatorServices mediatorServices)
    throws RemoteException, AmsException {
        boolean errorsFound = false;
        boolean checkThresholdLimit = false;
        boolean checkDailyLimit = false;
        String dailyLimit = null;
        String thresholdAmount = null;
        BigDecimal amountInBase = null;
        ThresholdGroup thresholdGroup = null;

        String thresholdGroupOid = user.getAttribute("threshold_group_oid");

        // get the threshold and daily limit amounts
        if (!(thresholdGroupOid == null || thresholdGroupOid.equals(""))) {

            thresholdGroup = (ThresholdGroup) mediatorServices.createServerEJB(
                    "ThresholdGroup", Long.parseLong(thresholdGroupOid));

            dailyLimit = thresholdGroup
                    .getAttribute(dailyLimitAttributeName);
            thresholdAmount = thresholdGroup
                    .getAttribute(thresholdAttributeName);

            if (!(dailyLimit == null || dailyLimit.equals("")))
                checkDailyLimit = true;

            if (!(thresholdAmount == null || thresholdAmount.equals("")))
                checkThresholdLimit = true;

        }

        // if any limits need to be checked, convert transaction amount to base
        // currency
        if (checkDailyLimit || checkThresholdLimit) {
            amountInBase = getAmountInBaseCurrency(currency,
                    amount, baseCurrency, userOrgOid, mediatorServices);

            if (amountInBase.compareTo(BigDecimal.ZERO) < 0) {
                errorsFound = true;
                checkDailyLimit = false;
                checkThresholdLimit = false;
            }

        }

        // if threshold amount check fails, issue an error, set
        // the transaction status to failed and continue to validate for
        // daily limit

        if (checkThresholdLimit) {
            BigDecimal threshold = new BigDecimal(thresholdAmount);
            if (amountInBase.compareTo(threshold) > 0) {
                mediatorServices.getErrorManager().issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.TRANSACTION_LIMIT_EXCEEDED,
                        user.getAttribute("first_name"),
                        user.getAttribute("last_name"));

                // transaction status should be set to FAILED
                errorsFound = true;

            }
        }

        // if daily limit check fails, issue an error and
        // stop further validations
        if (checkDailyLimit) {
            BigDecimal limit = new BigDecimal(dailyLimit);
            // get the total pay remit amount authorized by the user today
            BigDecimal amountAuthorized = getAmountAuthorized(userOid, user
                    .getAttribute("timezone"), baseCurrency, userOrgOid, discountIndicator, mediatorServices);
            if (amountAuthorized.compareTo(BigDecimal.ZERO) < 0) {
                errorsFound = true;
            } else {
                BigDecimal sumAuthorized = amountInBase.add(amountAuthorized);
                // debug("Daily Limit Total Authorized: " +sumAuthorized);
                if (sumAuthorized.compareTo(limit) > 0) {
                    mediatorServices.getErrorManager().issueError(
                            PayRemitMediator.class.getName(),
                            TradePortalConstants.DAILY_LIMIT_EXCEEDED,
                            user.getAttribute("first_name"),
                            user.getAttribute("last_name"));
                    // transaction status should be set to fail
                    errorsFound = true;
                }
            }

        }
        return ! errorsFound;
    }


	 /**
         * This method returns the total pay remit amount (in base currency)
         * approved by the user today.
         * "Today" is determined by the user's time zone.
         *
         * @param userOid java.lang.String - the user's oid
         * @param timeZone java.lang.String - the user's time zone
         * @param baseCurrency String - The base currency
         * @param uerOrgOid String - the user's corporate org oid
         * @param discountIndicator String - whether we are getting discounted Pay Remit only
         * @param mediatorServices MediatorServices - for issuing error message
         */
   static protected BigDecimal getAmountAuthorized(String userOid, String timeZone,
			String baseCurrency, String userOrgOid, boolean discountIndicator, MediatorServices mediatorServices)
			throws AmsException, RemoteException
		   {


			// find out user's date (without the time)
			// and convert the date to GMT date and time to determine the start
            // of
			// the user's day in GMT time.

			DocumentHandler resultSet = null;
			DocumentHandler transactionData = null;
			Vector transactionList = null;
			BigDecimal amountAuthorized = null;
			BigDecimal amountToAdd = null;
			int count = 0;
			String transactionAmount = null;
			String transactionCurrency = null;


			Date gmtDateTime = TPDateTimeUtility.getStartOfLocalDayInGMT(timeZone);
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

			String dateToCompare = formatter.format(gmtDateTime);

			// note: the first authorizing user and second authorizing user
            // cannot be the
			// same, hence there is no danger of selecting a row twice.

			List<Object> sqlParamsList = new ArrayList();
			String whereClause =
			    "where ((a_first_authorizing_user_oid = ? " +
			    " and first_authorize_status_date " +
			    ">= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')) or " +
			    "(a_second_authorizing_user_oid = ? " +
					" and second_authorize_status_date " +
			    ">= TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')))";

			sqlParamsList.add(Long.parseLong(userOid));
			sqlParamsList.add(dateToCompare);
			sqlParamsList.add(Long.parseLong(userOid));
			sqlParamsList.add(dateToCompare);
            if (discountIndicator == true) {
                whereClause = " and discount_indicator = ?";
                sqlParamsList.add(TradePortalConstants.INDICATOR_YES);
            }
			String sql = "select distinct pay_remit_oid, " +
			    " currency_code, payment_amount " +
				" from " +
				" pay_remit " +
			    whereClause;

			amountAuthorized = BigDecimal.ZERO;


			resultSet = DatabaseQueryBean.getXmlResultSet(sql, true, sqlParamsList);

			if (resultSet == null)
			{
			    count = 0;
			}
			else
			{

			    transactionList = resultSet.getFragments("/ResultSetRecord");

			    count = transactionList.size();
			}



			// get the amounts in base currency of the user
			for (int i = 0; i < count; i++)
			{
			    transactionData = (DocumentHandler) transactionList.get(i);

				transactionCurrency = transactionData.getAttribute("/CURRENCY_CODE");
				transactionAmount = transactionData.getAttribute("/PAYMENT_AMOUNT");

				// Handle case for blank Loan Request Amount (Get loan proceeds payment amount and currency instead.

				// Handle any amendments with blank amounts (Treat them as 0)
			    if (!transactionAmount.equals("")) {
					amountToAdd = getAmountInBaseCurrency(transactionCurrency,
					transactionAmount, baseCurrency, userOrgOid, mediatorServices);
				} else {
					amountToAdd = BigDecimal.ZERO;
				}

			    if (amountToAdd.compareTo(BigDecimal.ZERO) < 0)
			    {
			    	amountAuthorized = new BigDecimal(-1.0f);
				break;
			    }
			    else
			    {
			        amountAuthorized = amountAuthorized.add(amountToAdd);
			    }
			}


			return amountAuthorized;

		   }


   /**
    * This method returns an amount from a foreign currency to the
    * instrument's base currency. If the foreign exchange rate is not found
    * for the instrument's org, it will return a -1.0 which indicates an
    * error. Note: only the absolute value is returned. If the amount is
    * provided is negative, the amount returned is positive.
    *
    * @param currency
    *            java.lang.String - the foreign currency
    * @param amount
    *            java.lang.String - the amount to convert to base currency
    * @return double - the amount converted to base currency
    */
   static protected BigDecimal getAmountInBaseCurrency(String currency,
            String amount, String baseCurrency, String userOrgOid,
            MediatorServices mediatorServices) throws AmsException,
            RemoteException {


        BigDecimal amountInForeign = (new BigDecimal(amount)).abs();
        BigDecimal amountInBase = new BigDecimal(-1.0f);
        DocumentHandler resultSet = null;
        DocumentHandler fxRate = null;
        String multiplyIndicator = null;
        BigDecimal rate = null;


        // Get the foreign exchange rate for the given userOrgOid
        // and foreign currency code

        if (baseCurrency.equals(currency)) {
            amountInBase = amountInForeign;
        } else {

            // get the foreign exchange rate. If it doesn't exist,
            // issue error. Otherwise, get the multiply indicator
            // and rate and convert the amount
      	   Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
      	   resultSet = (DocumentHandler)fxCache.get(userOrgOid + "|" + currency);

            if (resultSet == null) {
                mediatorServices.getErrorManager().issueError(
                        PayRemitMediator.class.getName(),
                        TradePortalConstants.NO_FOREX_DEFINED);
            } else {
                fxRate = resultSet.getFragment("/ResultSetRecord");
                if (fxRate == null) {
                    mediatorServices.getErrorManager().issueError(
                            PayRemitMediator.class.getName(),
                            TradePortalConstants.NO_FOREX_DEFINED);
                } else {
                    multiplyIndicator = fxRate
                            .getAttribute("/MULTIPLY_INDICATOR");
                    rate = fxRate.getAttributeDecimal("/RATE");
                }
            }

            if (multiplyIndicator != null && rate != null) {
                if (multiplyIndicator.equals(TradePortalConstants.DIVIDE))
                    amountInBase = amountInForeign.divide(rate,
                            BigDecimal.ROUND_HALF_UP);
                else
                    amountInBase = amountInForeign.multiply(rate);
            }
        }

        mediatorServices.debug("Original amount: " + amount
                + " amount in base: " + amountInBase.toString());
        return amountInBase;

    }

    /**
     * Generate an authentication error for each individual pay remit.
     */
    public void issueAuthenticationErrors(
            Vector payRemitList, 
            MediatorServices mediatorServices)
            throws AmsException, RemoteException {        
        //generate an error for each transaction in the list
        if (payRemitList != null ) {
            for (int i = 0; i < payRemitList.size(); i++)
            {
                DocumentHandler payRemitDoc = (DocumentHandler) payRemitList.get(i);
                String payRemitData = payRemitDoc.getAttribute("/payRemitData");
    
                // get the data out of the checkbox...
                int separator1 = payRemitData.indexOf('/');
                //String payRemitOid = payRemitData.substring(0, separator1);
                String payRemitID = payRemitData.substring(separator1 + 1);    

                //the payRemitID is the payment reference
                mediatorServices.getErrorManager().issueError(getClass().getName(),
                    TradePortalConstants.PAY_MATCH_AUTHENTICATION_FAILED,
                    payRemitID );
            }
        }
    }

    private boolean performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("PayRemitMediatorBean:::performNotifyPanelAuthUser");
		String payRemitOid = inputDoc.getAttribute("/PayRemit/payRemitOid");
		 
    	 // create email for all selected users to notification
	    Vector<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragments("/EmailList/user");
	    StringBuilder emailReceivers = new StringBuilder();
	    String userOid = null;
	    String ownerOrgOid = null;
	    
	    for(DocumentHandler doc : beneficiaryDetailsList){
		    userOid = doc.getAttribute("/userOid");
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String userMailID = user.getAttribute("email_addr");
			ownerOrgOid = user.getAttribute("owner_org_oid");
			if(StringFunction.isNotBlank(userMailID)){
				emailReceivers.append(userMailID + ",");
			}
	    }

	    if(StringFunction.isNotBlank(emailReceivers.toString())){
	      		// Get the data from transaction that will be used to build the email message
				String amount = "";
				String currency = "";
				if (StringFunction.isNotBlank(payRemitOid)) {
					PayRemit payRemit =(PayRemit) mediatorServices.createServerEJB("PayRemit", Long.parseLong(payRemitOid));
					currency = payRemit.getAttribute("currency_code");
			        amount = payRemit.getAttribute("payment_amount");
			        amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
				}

				StringBuilder emailContent = new StringBuilder();
				 
				 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
				 emailContent.append(TradePortalConstants.NEW_LINE);
				// Include PayRemit Amount and currency
				if (StringFunction.isNotBlank(amount)) {
					emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
					emailContent.append(currency+" "+amount);
					emailContent.append(TradePortalConstants.NEW_LINE);
				}

				emailContent.append(TradePortalConstants.NEW_LINE);
				// call generic method to trigger email
				InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
	    }
        
		return true;

	}
}
