package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.ejb.RemoveException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ArMatchingRule;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.CredAppliedInvDetail;
import com.ams.tradeportal.busobj.CreditNotes;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceFileUpload;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.InvoicesCreditNotesRelation;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.PanelAuthProcessor;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;


/*
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PayableInvoiceGroupMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PayableInvoiceGroupMediatorBean.class);
	
	private transient Transaction transaction = null;
	private transient Terms terms = null;
	private transient Instrument newInstr = null;
	private String SELECT_SQL = "";
	private Set<String> invOIds = new TreeSet<>();
	private boolean loanReqStatus = true;
	private Set processedByRule = null; 

	  // This additional logic is needed to make sure the rules are sorted in following order always at the top
	  // 1. RULE with max number of INV DATA ITEM Columns defined
	  // 2. if more than 1 rule has max number of colmns defined then the Rule with widest min max days range is always at the top
	  // This ensure that invoice are matched against rule which are very specific in nature as this rule is the best match for the selected
	  // invoices.
	  // In short we are trying to find a rule which best matches the invoice for grouping.

	  private static final String EXTRA_SELECT_SQL =" ,  due_or_pay_max_days- due_or_pay_min_days AS daysDiff ,  " +
								       "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END AS colCount,  " +
									   "CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END AS tempCount ,  " +
									   "(  CASE    WHEN inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item1 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item2 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item3 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item4 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item5 IS NULL    THEN 0    ELSE 1  END +  " +
									   "CASE    WHEN inv_data_item6 IS NULL    THEN 0    ELSE 1  END) - (  " +
									   "CASE     WHEN inv_only_create_rule_max_amt IS NULL    THEN 1    ELSE 0    END +  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    THEN 1    ELSE 0  END) as colSort,  " +
									   "CASE    WHEN due_or_pay_max_days IS NULL    AND due_or_pay_min_days  IS NULL    AND inv_only_create_rule_max_amt IS NULL    THEN 0    ELSE 1  END as tCol ";
	  private static final String ORDERBY_CLAUSE = " ORDER BY  tCol DESC nulls last, colSort DESC nulls last,  inv_only_create_rule_max_amt DESC nulls last ,  " +
	   										 " daysDiff DESC nulls last,  creation_date_time DESC nulls last ";

	private static final String INVOICE_ORDERBY = "order by seller_name, currency, invdate, a_upload_definition_oid, amount desc";

        private static final String INV_ITEM_SEPERATOR = ";";
	private static final String INV_DATA_SEPERATOR = ",";

	/**
	 * Called by the processRequest() method of the Ancestor class to process the designated
	 * transaction.  The input parameters for the transaction are received in an XML document
	 * (inputDoc).  The execute() method is responsible for calling one or more business objects
	 * to process the transaction and populating the outputDoc XML document with the results
	 * of the transaction.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		
		try {
			 if (TradePortalConstants.BUTTON_INV_DETAIL_CREATE_APPROVAL_PAY.equals(buttonPressed) || "PAYCreateApprovalToPay".equals(buttonPressed)) {
				outputDoc = createDetailApprovalToPay(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_DETAIL_APPLY_PAYMENT_DATE.equals(buttonPressed) ) {
				outputDoc = applyDetailPaymentDate(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_APPLY_PAYMENT_DATE.equals(buttonPressed)) {
				outputDoc = applyPaymentDate(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_ATTACH_DOCUMENT.equals(buttonPressed)) {
				outputDoc = attachDocument(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_INV_CREATE_APPROVAL_PAY.equals(buttonPressed)) {
				outputDoc = createApprovalToPay(inputDoc, mediatorServices);
			}
			
			else if ((TradePortalConstants.BUTTON_INV_ASSIGN_LOAN_TYPE).equals(buttonPressed)) {
		   	    outputDoc = assignLoanType(inputDoc, mediatorServices);
		    }
			else if ("ListAssignInstrType".equals(buttonPressed)) {
				outputDoc = assignInstrType(inputDoc, mediatorServices);
			}
			else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ).equals(buttonPressed)) {
		   	       outputDoc = createLoanRequest(inputDoc, mediatorServices);
		    }
			else if ((TradePortalConstants.BUTTON_INV_CREATE_LOAN_REQ_BY_RULES).equals(buttonPressed)) {
		   	       outputDoc = createLoanRequestByRules(inputDoc, mediatorServices);
		    }
	
			else if ((TradePortalConstants.PAY_GROUP_INV_CREATE_ATP_BYRULES).equals(buttonPressed)) {
		   	       outputDoc = createATPByRules(inputDoc, mediatorServices);
		    }
			else if ((TradePortalConstants.PAY_GROUP_DETAIL_INV_ATP_BYRULES).equals(buttonPressed)) {
		   	       outputDoc = createDetailATPByRules(inputDoc, mediatorServices);
		    }
				
			else if (TradePortalConstants.BUTTON_MODIFY_SUPPLIER_DATE.equals(buttonPressed) || 
					TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)){
 	   			outputDoc = performModifyInvoiceData(inputDoc, mediatorServices, buttonPressed);
 	   		}
			else if (TradePortalConstants.BUTTON_RESET_SUPPLIER_DATE.equals(buttonPressed) ){
 	   			outputDoc = performResetInvoiceData(inputDoc, mediatorServices, buttonPressed);
 	   		}
			else if (TradePortalConstants.BUTTON_INV_AUTHORIZE_INVOICES.equals(buttonPressed)) {
				outputDoc = authorizeInvoice(inputDoc, mediatorServices);
		    }
			else if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(buttonPressed)){
					outputDoc = performNotifyPanelAuthUser(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.PAY_PGM_BUTTON_RESET_TO_AUTHORIZE.equals(buttonPressed)){
				outputDoc = resetToAuthorize(inputDoc, mediatorServices);
			}
			
			else if ("ApplyCreditNote".equals(buttonPressed)){
				outputDoc = applyCreditNoteToInvoices(inputDoc, mediatorServices);
			}
			else if ("UnApplyCreditNote".equals(buttonPressed)){
				outputDoc =unApplyCreditNoteToInvoices(inputDoc, mediatorServices);
			}
			
			else if (TradePortalConstants.BUTTON_DELETE_PAY_CREDIT_NOTE.equals(buttonPressed)){
				outputDoc = deletePayCreditNotes(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_CLOSE_PAY_CREDIT_NOTE.equals(buttonPressed)){
				outputDoc = closePayCreditNotes(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_RESET_TO_AUTHORIZE_CREDIT_NOTE.equals(buttonPressed)){
				outputDoc = resetCreditNoteToAuthorize(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_ATTACH_DOCUMENT_CREDIT_NOTE.equals(buttonPressed)) {
				outputDoc = attachDocumentToCreditNote(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_DELETE_DOCUMENT_CREDIT_NOTE.equals(buttonPressed)) {
				outputDoc = deleteAttachedDocumentsFromCreditNote(inputDoc, mediatorServices);
			}
			else if (TradePortalConstants.BUTTON_AUTHORIZE_CREDIT_NOTE.equals(buttonPressed)) {
				outputDoc = authorizeCreditNote(inputDoc, mediatorServices);
		    }
			else if (TradePortalConstants.BUTTON_DECLINE_INVOICE.equals(buttonPressed)) {
				outputDoc = declineInvoice(inputDoc, mediatorServices);
		    }
			else if (TradePortalConstants.BUTTON_CR_NOTE.equals(buttonPressed)) {
				outputDoc = declineCreditNote(inputDoc, mediatorServices);
		    }
			
			else {
				LOG.info("Error in PayableInvoiceGroupMediator - unknown button: " + buttonPressed);
			}
		}
		catch (AmsException | RemoteException e) {
			e.printStackTrace();
			throw e;
		} 

		return outputDoc;
	}

	
	/**
	 * This method applies payment date to Payable invoices. Validates the user entered
	 * payment against rules defined at corporate customer level.
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler applyPaymentDate(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String paymentDate = inputDoc.getAttribute("/InvoiceGroupList/payment_date");
		String invoiceType = null;
		String ownerOrg = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		String datePattern = user.getAttribute("datepattern");
		String invoiceDueDate=null;
		int invoiceSuccess = 0;
		if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE))) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.ApproveForPayment",
									TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList.isEmpty()) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.ApplyPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		else if (invoiceGroupList.size() > 1) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_ONLY_ONE_ITEM,
					"InvoiceGroups.InvoiceGroup",
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.ApplyPaymentDate",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		if(StringFunction.isBlank(paymentDate) || "null".equals(paymentDate)){

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INVOICE_PAYMENT_DATE_REQ);
			return new DocumentHandler();
		}
		try{

		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(paymentDate);
		    paymentDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			LOG.error("Invalid Payment Date {}", paymentDate);
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();
		}

		//There is only one Invoice Group to process
		DocumentHandler invoiceGroupItem = invoiceGroupList.get(0);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		String invoiceGroupOid = invoiceData[0];
		String optLock = invoiceData[1];
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
				Long.parseLong(invoiceGroupOid));

		String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
		String groupDescription = invoiceGroup.getDescription();
		String instrumentType = null;
		String loanType = null;
		String invSql = "select ISSUE_DATE,INVOICE_CLASSIFICATION,LINKED_TO_INSTRUMENT_TYPE,LOAN_TYPE from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
		DocumentHandler	invoicesListDoc = DatabaseQueryBean.getXmlResultSet(invSql, false, new Object[]{invoiceGroupOid});
		LOG.debug(" applyPaymentDate invoicesListDoc:", invoicesListDoc);
		if(invoicesListDoc!=null){
			List<DocumentHandler> records = invoicesListDoc.getFragmentsList("/ResultSetRecord");
			if(records!=null){
			invoiceType =  records.get(0).getAttribute("/INVOICE_CLASSIFICATION");
			instrumentType = records.get(0).getAttribute("/LINKED_TO_INSTRUMENT_TYPE");
			loanType = records.get(0).getAttribute("/LOAN_TYPE");
			}
		}
		LOG.debug("applyPaymentDate instrumentType{} / loanType{} :", instrumentType, loanType);

		// Payment Date can be only assigned to invoices with status 'Available for financing'
		if (invoiceType!=null && TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceType) && !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus))) {
			mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
					TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
					mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
					groupDescription,
					ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));

			return new DocumentHandler();
		}


		if (invoicesListDoc != null) {
			List<DocumentHandler> invRecords = invoicesListDoc.getFragmentsList("/ResultSetRecord");
			for (DocumentHandler doc : invRecords) {
				String date = (doc!=null)?doc.getAttribute("ISSUE_DATE"):null;
				if(date!=null){
				String issueDate = DateTimeUtility.convertTimestampToDateString(date);
				if ((DateTimeUtility.convertStringDateToDate(paymentDate)).before(DateTimeUtility.convertStringDateToDate(issueDate)))
			      {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
						return new DocumentHandler();
				  }
				}
			}
		}

		String oldGroupDate = invoiceGroup.getAttribute("payment_date");
        invoiceGroup.setAttribute("payment_date", paymentDate);
		invoiceGroup.setAttribute("opt_lock", optLock);

		String newGroupOid = null;
		int success=0;
		boolean issuedSuccessMessage = false;

			ATPInvoiceRules atpRules = getATPInvoiceRules(ownerOrg,mediatorServices);
			if(!InstrumentType.LOAN_RQST.equals(instrumentType)){
			// Ensure the ATP Invoice rule allows Payment Date
			if(atpRules == null){

				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_REQUIRED);
				return new DocumentHandler();
			}
			else
			{
				if (TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(atpRules.getPaymentAllow())) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_REQUIRED);
					return new DocumentHandler();
				}
			}
			} else if( InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
					CorporateOrganization corpObj = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(ownerOrg));

						if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corpObj.getAttribute("use_payment_date_allowed_ind"))){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.TRADE_LOAN_RULES_RESTRICT_PAYMENT_DATE);

							return new DocumentHandler();
						}

			}
			ArMatchingRule rule = null;
			String sql = "select UPLOAD_INVOICE_OID, INVOICE_ID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
			DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
			if (invoiceListDoc != null) {
				String tpName ="";
				List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

					for (int j=0;j < records.size();j++) {
					DocumentHandler doc = records.get(j);

					String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
					String invoiceId = doc.getAttribute("/INVOICE_ID");

					if (StringFunction.isNotBlank(invoiceOid)) {
						InvoicesSummaryData invoiceSummaryData =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",	Long.parseLong(invoiceOid));
						invoiceDueDate = invoiceSummaryData.getAttribute("due_date");
						String oldPaymentDate = invoiceSummaryData.getAttribute("payment_date");

						if(j==0){
								if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
									rule = invoiceSummaryData.getMatchingRule();
									 if(rule!=null)
										tpName =rule.getAttribute("buyer_name");

									 else
										 tpName= StringFunction.isNotBlank(invoiceSummaryData.getAttribute("buyer_name"))?invoiceSummaryData.getAttribute("buyer_name"):invoiceSummaryData.getAttribute("buyer_id");
								 }
								 else if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))) {
									tpName = StringFunction.isNotBlank(invoiceSummaryData.getAttribute("seller_name"))?invoiceSummaryData.getAttribute("seller_name"):invoiceSummaryData.getAttribute("seller_id");
								 }
						}
						boolean isCompliant = true;
						try {
							invoiceSummaryData.setAttribute("payment_date",	paymentDate);
						LOG.debug("paymentDate applied to UPLOAD_INVOICE_OID {}  is {}", invoiceOid, paymentDate);
						if (InstrumentType.APPROVAL_TO_PAY.equals(instrumentType)) {
								validatePaymentDate(paymentDate,invoiceDueDate,atpRules,mediatorServices);
							}
							if (StringFunction.isNotBlank(paymentDate) &&
									!paymentDate.equals(oldGroupDate)) {
								// check that another matching group does not already exist.
								newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);

								if (StringFunction.isNotBlank(newGroupOid)
										&& !newGroupOid.equals(invoiceGroupOid)) {
									// there is another match, so assign the other group's OID
									// to these invoices and delete this group
									success = invoiceGroup.delete();

								}
								else {
									success = invoiceGroup.save();

								}
							}
							else {
								success = invoiceGroup.save();

							}

							if (StringFunction.isNotBlank(newGroupOid)) {
								invoiceSummaryData.setAttribute("invoice_group_oid",newGroupOid);
							}

							LOG.debug("IsComlaint:"+isCompliant +"\tinvoiceSuccess:"+invoiceSuccess+"\t newGroupOid:"+newGroupOid);
						}
						catch (AmsException e) {
							isCompliant = false;

							IssuedError is = e.getIssuedError();
							if (is != null) {
								String errorCode = is.getErrorCode();

								if (TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL.equals(errorCode) ||
										TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL.equals(errorCode)||
										TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
										TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
								{
									mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
											errorCode, invoiceId);

									// Reset payment date to its former value
									invoiceSummaryData.setAttribute("payment_date",
											oldPaymentDate);
								}

								else {
									mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
									throw e;
								}
							}
							else {
								mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
								throw e;
							}
						}

						invoiceSummaryData.setAttribute("tp_rule_name", tpName);
						invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE);
                        invoiceSummaryData.setAttribute("user_oid", userOid);
						invoiceSuccess = invoiceSummaryData.save();
						newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
						LOG.debug("IsComlaint:"+isCompliant +"\tinvoiceSuccess:"+invoiceSuccess+"\t newGroupOid:"+newGroupOid);
						if (isCompliant && invoiceSuccess == 1) {

								if(StringFunction.isNotBlank(newGroupOid))
									updateAttachmentDetails(newGroupOid,mediatorServices);
								if (!issuedSuccessMessage) {
								// display message
								mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
										TradePortalConstants.INV_ASSIGN_PAYMENT_DATE,
										mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE)+" ",groupDescription);

								issuedSuccessMessage = true;
							}
						}
					}
				}
			}

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyPaymentDate: End: ");
		return new DocumentHandler();
	}

	/**
	 * This method validates the user entered Payment date against the rules
	 * defined at corporate customer level. Failing the validation throws exception
	 * @param paymentDate
	 * @param dueDate
	 * @param rules
	 * @param mediatorServices
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private void validatePaymentDate(String paymentDate,String dueDate,ATPInvoiceRules rules,MediatorServices mediatorServices) throws AmsException {

		int daysBefore = 0;
		int daysAfter = 0;
		String usePayamentDate = null;
		if(rules!=null){
			daysAfter = rules.getDaysAfter();
			daysBefore = rules.getDaysBefore();
			usePayamentDate=rules.getPaymentAllow();
			if (TradePortalConstants.INDICATOR_YES.equals(usePayamentDate)) {
				int dueDatePaymentDateDaysDiff = calculateDaysDiff(dueDate, paymentDate);

				int daysAllowed;
				if (dueDatePaymentDateDaysDiff < 0) {
					//Payment Date is after Due Date
					dueDatePaymentDateDaysDiff *= -1;
					daysAllowed = daysAfter;
				}
				else {
					//Payment Date is before Due Date
					daysAllowed = daysBefore;
				}

				if (dueDatePaymentDateDaysDiff > daysAllowed) {
					IssuedError is = new IssuedError();
					is.setErrorCode(TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL);
					throw new AmsException(is);
				}
			}

		}

	}

	private int calculateDaysDiff(String dateStr1, String dateStr2) throws AmsException {
		Date date1 = DateTimeUtility.convertStringDateToDate(dateStr1);
		Date date2 = DateTimeUtility.convertStringDateToDate(dateStr2);

		return calculateDaysDiff(date1, date2);
	}


	private int calculateDaysDiff(Date date1, Date date2) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(date1);
		long dateInMillis1 = cal.getTimeInMillis();

		cal.setTime(date2);
		long dateInMillis2 = cal.getTimeInMillis();

		double daysDiff = (double)(dateInMillis1 - dateInMillis2) / (24 * 60 * 60 * 1000);
		// use ceil to make  full day if it is coming 20.24, then it should be 21 days.
		return ((int)Math.ceil(daysDiff));
	}

	/**
	 * This method returns the ATP Invoices rules object to determine
	 * the whether user have already been allowed to perform 'Apply payment date'
	 * @param corpOrgOid
	 * @param mediatorServices
	 * @return ATPInvoiceRules
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private ATPInvoiceRules getATPInvoiceRules(String corpOrgOid , MediatorServices mediatorServices) throws AmsException,RemoteException {

		ATPInvoiceRules rulesObj = null;
		String paymentAllow = "";
		String daysBefore = null;
		String daysAfter= null;

		if(corpOrgOid!=null){
			CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
			rulesObj = new ATPInvoiceRules();
			paymentAllow = corpOrg.getAttribute("payment_day_allow");
			daysBefore =  corpOrg.getAttribute("days_before");
			daysAfter = corpOrg.getAttribute("days_after");


			if(StringFunction.isBlank(daysAfter))
				daysAfter="0";
			if(StringFunction.isBlank(daysBefore))
				daysBefore="0";
			rulesObj.setPaymentAllow(paymentAllow);

			rulesObj.setDaysAfter(Integer.parseInt(daysAfter));
			rulesObj.setDaysBefore(Integer.parseInt(daysBefore));
		}
		return rulesObj;
	}
	

	/**
	 * Attaches document to the invoice group
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler attachDocument(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		mediatorServices.debug("attachDocument():"+inputDoc);

		// ***********************************************************************
		// check to make sure that the user is allowed to attach documents to
		// the invoice - the user has to be a corporate user (not an admin
		// user) and have the rights to approve financing or authorize
		// ***********************************************************************

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceAttachDocumentText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice group OIDs
		for (DocumentHandler invoiceItem : invoiceGroupList) {
			String[] invoiceData = invoiceItem.getAttribute("InvoiceData").split("/");
			String invoiceGroupOid = invoiceData[0];
			String optLock = invoiceData[1];
			InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(invoiceGroupOid));

			String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
			String groupDescription = invoiceGroup.getDescription();
		
				if ((TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) 
						&& !(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_AUTH_FAILED.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_FIN_PAR_AUTH.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_NO_INSTR_TYPE.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN.equals(invoiceStatus)
						|| TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_CREDIT_NOTE.equals(invoiceStatus)
					    || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus)
						|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
					    || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus)))
					    || (TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoiceGroup.getAttribute("invoice_classification")) 
								&& !(TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(invoiceStatus)
								|| TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH.equals(invoiceStatus)
								|| TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_UNASSIGNED.equals(invoiceStatus))))
		    {
		   	    mediatorServices.getErrorManager().issueError(
		                PayableInvoiceGroupMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC,
		                mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
		                groupDescription,
		                ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()));
		    }


			//Only one doc attachment allowed per invoice group
			//If invoice group already has a doc attached, throw error
			boolean groupAlreadyHasDocAttached = groupAlreadyHasDocAttached(invoiceGroupOid);
			if (groupAlreadyHasDocAttached){
				mediatorServices.getErrorManager().issueError(
						PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED_TO_GRP,
						groupDescription);
			}

			invoiceGroup.setAttribute("opt_lock", optLock);
			invoiceGroup.save();
		}

		return new DocumentHandler();
	}

   /**
	*
	*This method returns @return true if an invoice in the group has a doc attached
	*
	**/
	private boolean groupAlreadyHasDocAttached(String invoiceGroupOid)
	throws AmsException {
		boolean docAttached = false;
		String sql = "select doc_image_oid from document_image where p_transaction_oid in ("
			+"select upload_invoice_oid from invoices_summary_data where a_invoice_group_oid = ?)";
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invoiceGroupOid});
		if (imagesListDoc != null){
			docAttached = true;
		}

		return docAttached;
	}



	/**
	 * This method creates an ATP Instrument for Receivable invoices
	 * @param inputDoc
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler createApprovalToPay(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.createApprovalToPay: Start: "+inputDoc);
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
		String opOrgOid = inputDoc.getAttribute("/InvoiceGroupList/bankBranch");
		 Vector invoiceOidsVector = new Vector();
		 DocumentHandler instrCreateDoc = null;
		 DocumentHandler  uploadInvoiceDoc = null;
		 String uploadInvOid= null;
		 //int invoiceSuccess  = 0;
		 String instrumentOid = null;
		 String transactionOid = null;
		 String corpOrgOid = null;
		 String firstInvoiceGroupOid = null;
		 String [] groupIds=null;
		 String newGroupOid = null;
		if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_CREATE_ATP))) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INV_ATP_NO_ACCESS);
			return new DocumentHandler();
		}

		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.CreateApprovalToPay",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		if(StringFunction.isBlank(opOrgOid)){

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INVOICE_BANK_BRANCH_REQ);
			return new DocumentHandler();
		}

		groupIds = new String[ invoiceGroupList.size()];
		for (int i = 0; i < invoiceGroupList.size(); i++) {
			DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		firstInvoiceGroupOid = invoiceData[0];
		groupIds[i]=invoiceData[0];
		}

		if(firstInvoiceGroupOid!=null) {
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
					Long.parseLong(firstInvoiceGroupOid));
		corpOrgOid = invoiceGroup.getAttribute("corp_org_oid");
		// Retrieve the set of invoices for the passing group id
		List<DocumentHandler> invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
		int count = 0;
		if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){
				 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
				 uploadInvoiceDoc =  invoicesGroupDoc.get(pos);

			String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
			String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
			//IR T36000016870 Start -If Linked to Instrument Type is null while create ATP then throw error.
			if(StringFunction.isBlank(uploadInvoiceDoc.getAttribute("/LINKED_TO_INSTRUMENT_TYPE"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_INSTR_TYPE_ATP,uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
				count++;
			}

			if(TradePortalConstants.REPLACEMENT.equals(uploadInvoiceDoc.getAttribute("/INVOICE_TYPE"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.RPL_ATP_ISSUE);
				return new DocumentHandler();
			}

			//if already associated then do not create an instrument
			if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
				Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
				mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.INSTRUMENT_EXISTS,
						mediatorServices.getResourceManager().getText("PayableInvoiceGroups.ATPMessage", TradePortalConstants.TEXT_BUNDLE) +" " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
				invoicesGroupDoc.remove(pos);
				pos--;
			}
				 }
		}
		if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0 || count == invoicesGroupDoc.size())
		return new DocumentHandler();


		 InstrumentProperties instProp = new InstrumentProperties();
		 instProp.setCorporateOrgOid(corpOrgOid);
		 instProp.setSecurityRights(securityRights);
		 instProp.setUserOid(userOid);
		 instProp.setClientBankOid(clientBankOid);
		 instProp.setOpOrgOid(opOrgOid);
		 instProp.setInstrumentType(InstrumentType.APPROVAL_TO_PAY);
		 instrCreateDoc = processInvoiceGroups(instProp,invoicesGroupDoc,mediatorServices);

		 String instType = instrCreateDoc!= null? instrCreateDoc.getAttribute("/instrumentType"):null;
		 mediatorServices.debug("instType:"+instType);

		 if(instType != null) {
			 if (invoicesGroupDoc != null) {
			 for (DocumentHandler row : invoicesGroupDoc) {
				 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
				 invoiceOidsVector.addElement(invOid);
			 }
		}
		  // Create instrument using the document
		DocumentHandler doc = createATPInstrument(instrCreateDoc, mediatorServices,invoiceOidsVector);
		transactionOid = doc.getAttribute("/transactionOid");
	    	if(StringFunction.isNotBlank(transactionOid)){
	    			transaction = (Transaction) mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
			        instrumentOid = transaction.getAttribute("instrument_oid");
			        mediatorServices.debug("Instrument OID:"+instrumentOid);
			 }

	    	String completeInstrId = "";

	    	if(StringFunction.isNotBlank(instrumentOid)){
		        newInstr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrumentOid));
		        completeInstrId = newInstr.getAttribute("complete_instrument_id");
		        mediatorServices.debug("Instrument Num:"+completeInstrId);
	    	}

	    	Vector invoiceIDVector = new Vector();
	    	boolean success = true;

	    	terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			ArMatchingRule rule = null;
			String tempDate = "";
			InvoicesSummaryData invoice =
				(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
						Long.parseLong((String)invoiceOidsVector.get(0)));
			 String tpName="";
			 rule = invoice.getMatchingRule();
			if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoice.getAttribute("invoice_classification"))){
				 if(rule!=null)
					tpName =rule.getAttribute("buyer_name");

				 else
					 tpName= StringFunction.isNotBlank(invoice.getAttribute("buyer_name"))?invoice.getAttribute("buyer_name"):invoice.getAttribute("buyer_id");
			 }
			 if(TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invoice.getAttribute("invoice_classification"))) {
				tpName = StringFunction.isNotBlank(invoice.getAttribute("seller_name"))?invoice.getAttribute("seller_name"):invoice.getAttribute("seller_id");
			 }
			tempDate = invoice.getActualDate();

			try(Connection con = DatabaseQueryBean.connect(false);
					PreparedStatement pStmt1 = con
							.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, A_INSTRUMENT_OID=?, A_TRANSACTION_OID=?, "
									+ "A_TERMS_OID=?, a_invoice_group_oid=? where upload_invoice_oid =?");
					PreparedStatement pStmt2 = con
							.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
							+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)");) {
				
				con.setAutoCommit(false);
				
				for (int i = 0; i < invoiceOidsVector.size(); i++) {
				uploadInvOid =  (String) invoiceOidsVector.get(i);

				InvoicesSummaryData inv =
						(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(uploadInvOid));

				invoiceIDVector.addElement(inv.getAttribute("invoice_id"));
				inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
					inv.setAttribute("tp_rule_name",tpName);
				inv.verifyInvoiceGroup();
				newGroupOid = inv.getAttribute("invoice_group_oid");
				inv=null;
				pStmt1.setString(1, TradePortalConstants.PO_STATUS_ASSIGNED);
				pStmt1.setString(2,instrumentOid);
				pStmt1.setString(3,transactionOid);
				pStmt1.setString(4,String.valueOf(terms.getAttribute("terms_oid")));
				pStmt1.setString(5,newGroupOid);
				pStmt1.setString(6,uploadInvOid);
				pStmt1.addBatch();
				pStmt1.clearParameters();
				InvoiceUtility.createPurchaseOrderHisotryLog(
						pStmt2,
						TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST,
						TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED,
						(String) invoiceOidsVector.elementAt(i), userOid);
			}
				pStmt1.executeBatch();
				pStmt2.executeBatch();
				con.commit();
				
		} catch (AmsException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			 success = false;
			e.printStackTrace();
		}

	   			// Success message to User
			if(success){
				if(newGroupOid!=null && !firstInvoiceGroupOid.equals(newGroupOid)){

  					updateAttachmentDetails(newGroupOid,mediatorServices);
			}
	   			mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.INSERT_SUCCESSFUL,
						mediatorServices.getResourceManager().getText("PayableInvoiceGroups.ATPMessage", TradePortalConstants.TEXT_BUNDLE) +" " +completeInstrId); //SHR IR T36000005407- display completeInstrumentID
	    	   }else
	    	   {
	    		   mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.PAYABLE_INV_ATP_FAILED);
	    	   }

	   		long oid = terms.newComponent("FirstTermsParty");
	  		TermsParty termsParty = (TermsParty) terms
	  				.getComponentHandle("FirstTermsParty");
	  		LOG.debug("termsParty handle acquired -> "
	  				+ termsParty.getAttribute("terms_party_oid"));
    		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
	  				transaction, terms, invoiceOidsVector, newInstr,invoiceIDVector,"");
	  		terms.setAttribute("invoice_only_ind",
	  				TradePortalConstants.INDICATOR_YES);
	  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");


	  		try {
	  			if (!StringFunction.isBlank(tempDate)) {
	  				Date date = jPylon.parse(tempDate);
	  				terms.setAttribute("invoice_due_date",
	  						DateTimeUtility.convertDateToDateTimeString(date));
					terms.setAttribute("expiry_date",
							DateTimeUtility.convertDateToDateTimeString(date));
	  			}
	  		} catch (Exception e) {
	  			e.printStackTrace();
	  		}

			TermsParty firstTermsParty = (TermsParty) terms
			.getComponentHandle("FirstTermsParty");
			if(!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
			firstTermsParty.setAttribute("name", tpName);
			if(rule!=null){
			firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
			firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
			firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
			firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
			firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
			firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
			firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			}

	  		// Set the counterparty for the instrument and some "copy of" fields
	  		newInstr.setAttribute("a_counter_party_oid",
	  				terms.getAttribute("c_FirstTermsParty"));
	  		transaction
	  				.setAttribute("copy_of_amount", terms.getAttribute("amount"));
	  		transaction.setAttribute("instrument_amount",
	  				terms.getAttribute("amount"));
	  		newInstr.setAttribute("copy_of_instrument_amount",
	  				transaction.getAttribute("instrument_amount"));

	  		if (terms.isAttributeRegistered("expiry_date")) {
	  			newInstr.setAttribute("copy_of_expiry_date",
	  					terms.getAttribute("expiry_date"));
	  		}

	  		if (terms.isAttributeRegistered("reference_number")) {
	  			newInstr.setAttribute("copy_of_ref_num",
	  					terms.getAttribute("reference_number"));
	  		}
	  		transaction.save(false);
	  		LOG.debug("Saving instrument with oid '"
	  				+ newInstr.getAttribute("instrument_oid") + "'...");
	  		newInstr.save(false);// save without validate
	  		LOG.debug("Instrument saved");
		  }
		}
		mediatorServices.debug("PayableInvoiceGroupMediatorBean.createApprovalToPay: End: ");
		return new DocumentHandler();

	}

	/**
	 * This method returns Invoices for a selected Invoice Group
	 * @param groupIds
	 * @param corpOrgOid
	 * @return Vector
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public List<DocumentHandler> getInvoices(String[] groupIds,String corpOrgOid) throws AmsException, RemoteException {

		List<DocumentHandler> rows = new ArrayList<>();
		List<Object> sqlParamsLst = new ArrayList<>();
		StringBuilder invGroupSql = new StringBuilder();
		StringBuilder whereClause = new StringBuilder(200);
		if(groupIds!=null && groupIds.length>0){
			for (String groupId : groupIds) {
				whereClause.append("?, ");
				sqlParamsLst.add(groupId);
			}
		}
		String finalWhereClause = "";
		if(whereClause.toString().length()>0){
			finalWhereClause = whereClause.toString().substring(0,whereClause.toString().length()-2);
		}

			invGroupSql.append(" select UPLOAD_INVOICE_OID,SELLER_NAME,CURRENCY,AMOUNT,DUE_DATE,PAYMENT_DATE,INVOICE_ID,INVOICE_TYPE,A_INSTRUMENT_OID,A_TRANSACTION_OID," +
				" INVOICE_STATUS,A_CORP_ORG_OID,LOAN_TYPE,LINKED_TO_INSTRUMENT_TYPE from invoices_summary_data where A_INVOICE_GROUP_OID in ( "+finalWhereClause+" ) and a_corp_org_oid = ? ");
			sqlParamsLst.add(corpOrgOid);
		DocumentHandler	invoicesGroupDoc = DatabaseQueryBean.getXmlResultSet(invGroupSql.toString(), false, sqlParamsLst);
		if(invoicesGroupDoc!=null)
		  rows = invoicesGroupDoc.getFragmentsList("/ResultSetRecord");
		return rows;
	}

	/**
	 * This method loops through the invoices of a selected invoice group
	 * and validate based on Seller name/Currency name/Amount
	 * @param instrumentProp
	 * @param invGroupList
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler processInvoiceGroups(InstrumentProperties instrumentProp, List<DocumentHandler> invGroupList, MediatorServices mediatorServices)
		      throws  AmsException {

		   mediatorServices.debug("processInvoiceGroups() "+invGroupList);
		   String sellerName = null;
		   String currency =null;
		   String paymentDate = null;
		   String dueDate = null;
		   String invalidSellerName = null;
		   String invalidCurrency = null;
		   String invalidPaymentDate = null;
		   String invalidDueDate = null;
		   DocumentHandler invGroupDoc = null;
		   DocumentHandler nxtInvGroupDoc = null;
		   DocumentHandler instrCreateDoc = null;
		   BigDecimal amount =  BigDecimal.ZERO;
		   BigDecimal nextAmount = BigDecimal.ZERO;
		   String amt = null;
		   BigDecimal totalAmount = BigDecimal.ZERO;
		   String date = null;
		   String invalidDate = null;
		   if(invGroupList!=null && invGroupList.size()>0){
		   int pos = 0;

				  invGroupDoc = invGroupList.get(pos);

				  sellerName = invGroupDoc.getAttribute("/SELLER_NAME");
				  currency =  invGroupDoc.getAttribute("/CURRENCY");
				  paymentDate = invGroupDoc.getAttribute("/PAYMENT_DATE");
				  dueDate = invGroupDoc.getAttribute("/DUE_DATE");
				  if(StringFunction.isBlank(paymentDate))
					  date = dueDate;
				  else
					  date = paymentDate;
				  mediatorServices.debug("date:"+date);
				  amt = invGroupDoc.getAttribute("/AMOUNT");
				  if(amt!=null)
					  amount = BigDecimal.valueOf(Double.parseDouble(amt));
				  totalAmount = amount;
				  int subIdx = pos+1;

			        while (subIdx < invGroupList.size()) {

			        		nxtInvGroupDoc = invGroupList.get(subIdx);
			            	invalidSellerName = nxtInvGroupDoc.getAttribute("/SELLER_NAME");
			            	invalidCurrency =  nxtInvGroupDoc.getAttribute("/CURRENCY");
			            	invalidPaymentDate = nxtInvGroupDoc.getAttribute("/PAYMENT_DATE");
			            	invalidDueDate = nxtInvGroupDoc.getAttribute("/DUE_DATE");
							String am =  nxtInvGroupDoc.getAttribute("/AMOUNT");
							if(StringFunction.isBlank(invalidPaymentDate))
								invalidDate = invalidDueDate;
							else
								invalidDate =invalidPaymentDate;
							mediatorServices.debug("invalid Date:"+invalidDate);
							 if(am!=null)
								  nextAmount = BigDecimal.valueOf(Double.parseDouble(am));

							if(!(sellerName.equals(invalidSellerName) && currency.equals(invalidCurrency) && date.equals(invalidDate))){
								  mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.INVALID_PAYABLE_INVOICE_GROUP);
									return new DocumentHandler();
							}else{
									totalAmount = totalAmount.add(nextAmount);
									subIdx++;
							}
			            }

				  if(totalAmount.compareTo(BigDecimal.valueOf(0))==-1){
					    mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.PAYABLE_INV_NO_CREDIT_PROCESSING);
									return new DocumentHandler();
				  }
				  instrumentProp.setAmount(totalAmount);
				  instrumentProp.setCurrency(currency);
				  instrumentProp.setShipDate(date);
				  instrumentProp.setBenName(sellerName);

				  // Build the input document used to create a new instrument
			      instrCreateDoc = buildInstrumentCreateDoc(instrumentProp);

		   }

		return instrCreateDoc;
	  }

	/**
	 * This method prepares instrument document which will be input to
	 * Instrument EJB for creation a new ATP instrument
	 * @param instrumentProp
	 * @return DocumentHandler
	 * @throws AmsException
	 */
	private DocumentHandler buildInstrumentCreateDoc(InstrumentProperties instrumentProp) {


		 DocumentHandler instrCreateDoc = new DocumentHandler();
		    instrCreateDoc.setAttribute("/instrumentOid", null);
			instrCreateDoc.setAttribute("/instrumentType",instrumentProp.getInstrumentType());
			instrCreateDoc.setAttribute("/clientBankOid", instrumentProp.getClientBankOid());
			instrCreateDoc.setAttribute("/bankBranch", instrumentProp.getOpOrgOid());
		    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);
		    instrCreateDoc.setAttribute("/userOid", instrumentProp.getUserOid());
		    instrCreateDoc.setAttribute("/ownerOrg", instrumentProp.getCorporateOrgOid());
		    instrCreateDoc.setAttribute("/securityRights", instrumentProp.getSecurityRights());
		    instrCreateDoc.setAttribute("/benName", instrumentProp.getBenName());
		    instrCreateDoc.setAttribute("/currency", instrumentProp.getCurrency());
		    instrCreateDoc.setAttribute("/shipDate", instrumentProp.getShipDate());
		    instrCreateDoc.setAttributeDecimal("/amount", instrumentProp.getAmount());


		return instrCreateDoc;

	 }

	/**
	  * This method invokes the Instrument EJB.
	  * successful creation of a new Instrument returns a transction_oid
	  * @param inputDoc
	  * @param mediatorServices
	  * @return String
	  * @throws AmsException
	  * @throws RemoteException
	  */
	 private DocumentHandler createATPInstrument(DocumentHandler inputDoc, MediatorServices mediatorServices,Vector invOids)
		      throws AmsException, RemoteException {
		 	 mediatorServices.debug("createATPInstrument()->"+inputDoc);
		      newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
		     DocumentHandler outputDoc = new DocumentHandler();


		        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the invoice line items
		        String currency             = inputDoc.getAttribute("/currency"); //the currency of the invoice line items
		        String newTransactionOid    = null; //the oid of the transaction created for these invoice line items

		        //Create a new instrument
		        newTransactionOid = newInstr.createNew(inputDoc);
		        LOG.debug("New instrument created with an original transacion oid of -> " + newTransactionOid);

		        if (newTransactionOid == null)
		        {
		        	LOG.debug("newTransactionOid is null.  Error in this.createNew()");
		            return outputDoc; //outputDoc is null
		        }
		        else
		            outputDoc.setAttribute("/transactionOid", newTransactionOid);

		        LOG.debug("ben_name     = " + benName);
		        LOG.debug("currency     = " + currency);

		        /************************************************************************
		         * Instantiate the transaction, terms, and termsParty and set the fields
		         * derived from the po line item data
		         ************************************************************************/

		        //get a handle to the terms object through the transaction then set the values on terms
		        //we'll also have to create a termsParty object and associate it to the terms object
		        Transaction transaction = (Transaction) newInstr.getComponentHandle("TransactionList",
                        Long.valueOf(newTransactionOid));
		        LOG.debug("Transaction handle acquired -> " + transaction.getAttribute("transaction_oid"));


		        try
		        {
		            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		            LOG.debug("terms handle acquired -> " + terms.getAttribute("terms_oid"));
		            outputDoc.setAttribute("/termsOid", terms.getAttribute("terms_oid"));
		        }
		        catch (Exception e)
		        {
		            LOG.debug("Caught error getting handle to terms");
		            e.printStackTrace();
		        }

		        long oid = terms.newComponent("FirstTermsParty");
		        TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
		        LOG.debug("termsParty handle acquired -> " + termsParty.getAttribute("terms_party_oid"));

		        return outputDoc;

	 }


	private class InstrumentProperties {
		/**
		 * This class holds the properties to be set for Instrument EJB
		 */
		 private String corporateOrgOid = null;
		 private String securityRights = null;
		 private String userOid = null;
		 private String benName = null;
		 private String currency =null;
		 private BigDecimal amount = null;
		 private String shipDate = null;
		 private String opOrgOid = null;
		 private String clientBankOid = null;
		 private String instrumentType = null;
		 private MediatorServices mediatorServices;
			private String timeZone = null;
			private transient String userLocale = null;
			private transient String usePaymentDate = null;
		
		 public String getInstrumentType() {
			return instrumentType;
		}
		public void setInstrumentType(String instrumentType) {
			this.instrumentType = instrumentType;
		}
		public String getClientBankOid() {
			return clientBankOid;
		}
		public void setClientBankOid(String clientBankOid) {
			this.clientBankOid = clientBankOid;
		}
		public String getOpOrgOid() {
			return opOrgOid;
		}
		public void setOpOrgOid(String opOrgOid) {
			this.opOrgOid = opOrgOid;
		}
		public String getCorporateOrgOid() {
			return corporateOrgOid;
		}
		public void setCorporateOrgOid(String corporateOrgOid) {
			this.corporateOrgOid = corporateOrgOid;
		}
		public String getSecurityRights() {
			return securityRights;
		}
		public void setSecurityRights(String securityRights) {
			this.securityRights = securityRights;
		}
		public String getUserOid() {
			return userOid;
		}
		public void setUserOid(String userOid) {
			this.userOid = userOid;
		}
		public String getBenName() {
			return benName;
		}
		public void setBenName(String benName) {
			this.benName = benName;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public BigDecimal getAmount() {
			return amount;
		}
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
		public String getShipDate() {
			return shipDate;
		}
		public void setShipDate(String shipDate) {
			this.shipDate = shipDate;
		}
		
			private String instTypeCode = null;
				public String getInstTypeCode() {
				return instTypeCode;
			}
			public void setInstTypeCode(String instTypeCode) {
				this.instTypeCode = instTypeCode;
			}
			public String getUsePaymentDate() {
				return usePaymentDate;
			}
			public void setUsePaymentDate(String usePaymentDate) {
				this.usePaymentDate = usePaymentDate;
			}
			
			public String getUserLocale() {
				return userLocale;
			}
			public void setUserLocale(String userLocale) {
				this.userLocale = userLocale;
			}
			public String getTimeZone() {
				return timeZone;
			}
			public void setTimeZone(String timeZone) {
				this.timeZone = timeZone;
			}
			public MediatorServices getMediatorServices() {
				return mediatorServices;
			}
			public void setMediatorServices(MediatorServices mediatorServices) {
				this.mediatorServices = mediatorServices;
			}
	 }

	private class ATPInvoiceRules{
		int daysBefore = 0;
		int daysAfter = 0;
		String paymentAllow = null;

		public int getDaysBefore() {
			return daysBefore;
		}
		public void setDaysBefore(int daysBefore) {
			this.daysBefore = daysBefore;
		}
		public int getDaysAfter() {
			return daysAfter;
		}
		public void setDaysAfter(int daysAfter) {
			this.daysAfter = daysAfter;
		}
		public String getPaymentAllow() {
			return paymentAllow;
		}
		public void setPaymentAllow(String paymentAllow) {
			this.paymentAllow = paymentAllow;
		}

	}

	/**
	 * This method creates an ATP Instrument for Receivable invoices
	 * @param inputDoc
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler createDetailApprovalToPay(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("PayableInvoiceGroupMediatorBean.createApprovalToPay: Start: "+inputDoc);
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		String clientBankOid  = inputDoc.getAttribute("/UploadInvoiceList/clientBankOid");
		String opOrgOid 	  = inputDoc.getAttribute("/UploadInvoiceList/bankBranch");
		List<DocumentHandler> invoicesOidDoc = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String corpOrgOid 	  = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");

		 Vector invoiceOidsVector = new Vector();
		 Vector invoiceIDVector = new Vector();
		 DocumentHandler instrCreateDoc = null;
		 String uploadInvOid= null;
		 String instrumentOid = null;
		 String transactionOid = null;
		 String firstInvoiceGroupOid = null;
		 List<String> invoicesList=new ArrayList<>();
		 List<DocumentHandler> invoicesDoc = new ArrayList<>();
		 String invoiceIds[]=null;
		 String newGroupOid = null;
		if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_CREATE_ATP))) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INV_ATP_NO_ACCESS);
			return new DocumentHandler();
		}

		if (invoicesOidDoc == null || invoicesOidDoc.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.CreateApprovalToPay",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		if(StringFunction.isBlank(opOrgOid)){

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INVOICE_BANK_BRANCH_REQ);
			return new DocumentHandler();
		}

		invoiceIds = new String[invoicesOidDoc.size()];
		for (int i = 0; i < invoicesOidDoc.size(); i++) {
			DocumentHandler invoiceGroupItem = invoicesOidDoc.get(i);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		invoicesList.add(invoiceData[0]);
		invoiceIds[i]=invoiceData[0];
		}
		int count = 0;
		if(invoicesList!=null && invoicesList.size()>0){

		invoicesDoc =  getInvoicesDetail(invoiceIds,corpOrgOid);

				 for(int pos = 0; pos<invoicesList.size();pos++){
				String  invOid = invoicesList.get(pos);
				 InvoicesSummaryData invoiceSummaryData =
						(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(invOid));
			String instrOid = invoiceSummaryData.getAttribute("instrument_oid");
			String tranOid = invoiceSummaryData.getAttribute("transaction_oid");
			String invoiceId = invoiceSummaryData.getAttribute("invoice_id");
			firstInvoiceGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
			//IR T36000016870 Start -If Linked to Instrument Type is null while create ATP then throw error.
			if(StringFunction.isBlank((invoiceSummaryData.getAttribute("linked_to_instrument_type")))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_INSTR_TYPE_ATP,invoiceId);
				count++;
			}
			if(!InstrumentType.APPROVAL_TO_PAY.equals(invoiceSummaryData.getAttribute("linked_to_instrument_type"))){
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_ACTION,
							mediatorServices.getResourceManager().getText("UploadInvoices.CreateApprovalToPay", TradePortalConstants.TEXT_BUNDLE),
							invoiceId,invoiceSummaryData.getAttribute("linked_to_instrument_type"));
					return new DocumentHandler();
			}

			if(TradePortalConstants.REPLACEMENT.equals(invoiceSummaryData.getAttribute("invoice_type"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.RPL_ATP_ISSUE);
				return new DocumentHandler();
			}

			//if already associated then do not create an instrument
			if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
				Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
				mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.INSTRUMENT_EXISTS,
						mediatorServices.getResourceManager().getText("PayableInvoiceGroups.ATPMessage", TradePortalConstants.TEXT_BUNDLE) +" " +instr.getAttribute("complete_instrument_id"),invoiceId);
				invoicesList.remove(pos);
				pos--;
			}
				 }
		}
		if(invoicesList==null || invoicesList.size()==0 || count == invoicesList.size())
		return new DocumentHandler();


		 InstrumentProperties instProp = new InstrumentProperties();
		 instProp.setCorporateOrgOid(corpOrgOid);
		 instProp.setSecurityRights(securityRights);
		 instProp.setUserOid(userOid);
		 instProp.setClientBankOid(clientBankOid);
		 instProp.setOpOrgOid(opOrgOid);
 		 instProp.setInstrumentType(InstrumentType.APPROVAL_TO_PAY);
		 instrCreateDoc = processInvoiceGroups(instProp,invoicesDoc,mediatorServices);

		 String instType = instrCreateDoc!= null? instrCreateDoc.getAttribute("/instrumentType"):null;
		 LOG.debug("instType-:"+instType);

		 InvoicesSummaryData inv = null;
		 if(instType != null) {
			 if (invoicesList != null) {
			 for (String invOid : invoicesList) {
				 inv = (InvoicesSummaryData) mediatorServices.createServerEJB(
						 "InvoicesSummaryData", Long.valueOf(invOid));
				 invoiceOidsVector.addElement(invOid);
				 invoiceIDVector.addElement(inv.getAttribute("invoice_id"));
				 inv = null;
			 }

		}

		  // Create instrument using the document
		DocumentHandler doc = createDetailATPInstrument(instrCreateDoc, mediatorServices);
		transactionOid = doc.getAttribute("/transactionOid");
	    	if(transactionOid!=null){
	    		transaction = (Transaction) mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
			        instrumentOid = transaction.getAttribute("instrument_oid");
			        mediatorServices.debug("Instrument OID:"+instrumentOid);
			 }

	    	String completeInstrId = "";

	    	if(instrumentOid!=null){
		        Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrumentOid));
		        completeInstrId = instr.getAttribute("complete_instrument_id");
		        mediatorServices.debug("Instrument Num:"+completeInstrId);
	    	}
	    	boolean success =true;
			ArMatchingRule rule = null;
			String tempDate = "";
			  terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			InvoicesSummaryData invoice =
				(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
						Long.parseLong((String)invoiceOidsVector.get(0)));
	  				 String tpName="";
	  					 rule = invoice.getMatchingRule();
	  					tpName = invoice.getTpRuleName(rule);
	  				tempDate = invoice.getActualDate();

			try(Connection con = DatabaseQueryBean.connect(false);
					PreparedStatement pStmt1 = con
							.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, A_INSTRUMENT_OID=?, A_TRANSACTION_OID=?, "
									+ "A_TERMS_OID=?, a_invoice_group_oid=? where upload_invoice_oid =?");
					PreparedStatement pStmt2 = con
							.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
					+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)");

			) {
				
				con.setAutoCommit(false);
				
				
				for (int i = 0; i < invoiceOidsVector.size(); i++) {
				uploadInvOid =  (String) invoiceOidsVector.get(i);

  				 InvoicesSummaryData invoiceSummaryData =
						(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
								Long.parseLong(uploadInvOid));
  				 invoiceIDVector.addElement(invoiceSummaryData.getAttribute("invoice_id"));

  				invoiceSummaryData.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
  				invoiceSummaryData.setAttribute("tp_rule_name",tpName);
  				invoiceSummaryData.verifyInvoiceGroup();
  				newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
  				invoiceSummaryData=null;
  				pStmt1.setString(1, TradePortalConstants.PO_STATUS_ASSIGNED);
  				pStmt1.setString(2,instrumentOid);
  				pStmt1.setString(3,transactionOid);
  				pStmt1.setString(4,terms.getAttribute("terms_oid"));
  				pStmt1.setString(5,newGroupOid);
  				pStmt1.setString(6,uploadInvOid);
  				pStmt1.addBatch();
  				pStmt1.clearParameters();
				InvoiceUtility.createPurchaseOrderHisotryLog(
						pStmt2,
						TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST,
						TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED,
						(String) invoiceOidsVector.elementAt(i), userOid);
			}
				pStmt1.executeBatch();
				pStmt2.executeBatch();
				con.commit();
				
		} catch (AmsException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			 success =false;
			e.printStackTrace();
		}
	    if(success){

	    	if(newGroupOid!=null && !firstInvoiceGroupOid.equals(newGroupOid)){
	    		updateAttachmentDetails(newGroupOid, firstInvoiceGroupOid,mediatorServices);
	    	}
	   			mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.INSERT_SUCCESSFUL,
						mediatorServices.getResourceManager().getText("PayableInvoiceGroups.ATPMessage", TradePortalConstants.TEXT_BUNDLE) +" " +completeInstrId); //SHR IR T36000005407- display completeInstrumentID
	    	   }else
	    	   {
	    		   mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.PAYABLE_INV_ATP_FAILED);
	    	   }
	    	  long oid = terms.newComponent("FirstTermsParty");
	  		TermsParty termsParty = (TermsParty) terms
	  				.getComponentHandle("FirstTermsParty");
	  		LOG.debug("termsParty handle acquired -> "
	  				+ termsParty.getAttribute("terms_party_oid"));
    		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
	  				transaction, terms, invoiceOidsVector, newInstr,invoiceIDVector,"");
	  		terms.setAttribute("invoice_only_ind",
	  				TradePortalConstants.INDICATOR_YES);
	  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");

	  		try {
	  			if (!StringFunction.isBlank(tempDate)) {
	  				Date date = jPylon.parse(tempDate);
	  				terms.setAttribute("invoice_due_date",
	  						DateTimeUtility.convertDateToDateTimeString(date));
	  				terms.setAttribute("expiry_date",
							DateTimeUtility.convertDateToDateTimeString(date));
	  			}
	  		} catch (Exception e) {
	  			e.printStackTrace();
	  		}

			TermsParty firstTermsParty = (TermsParty) terms
			.getComponentHandle("FirstTermsParty");
			if(!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
			firstTermsParty.setAttribute("name", tpName);
			if(rule!=null){
			firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
			firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
			firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
			firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
			firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
			firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));
			firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			}

	  		// Set the counterparty for the instrument and some "copy of" fields
	  		newInstr.setAttribute("a_counter_party_oid",
	  				terms.getAttribute("c_FirstTermsParty"));
	  		transaction
	  				.setAttribute("copy_of_amount", terms.getAttribute("amount"));
	  		transaction.setAttribute("instrument_amount",
	  				terms.getAttribute("amount"));
	  		newInstr.setAttribute("copy_of_instrument_amount",
	  				transaction.getAttribute("instrument_amount"));

	  		if (terms.isAttributeRegistered("expiry_date")) {
	  			newInstr.setAttribute("copy_of_expiry_date",
	  					terms.getAttribute("expiry_date"));
	  		}

	  		if (terms.isAttributeRegistered("reference_number")) {
	  			newInstr.setAttribute("copy_of_ref_num",
	  					terms.getAttribute("reference_number"));
	  		}
	  		transaction.save(false);

	  		LOG.debug("Saving instrument with oid '"
	  				+ newInstr.getAttribute("instrument_oid") + "'...");
	  		newInstr.save(false);// save without validate
	  		LOG.debug("Instrument saved");

		  }

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.createApprovalToPay: End: ");
		return new DocumentHandler();

	}

	/**
	 * This method invokes the Instrument EJB. successful creation of a new
	 * Instrument returns a transction_oid
	 *
	 * @param inputDoc
	 * @param mediatorServices
	 * @return String
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private DocumentHandler createDetailATPInstrument(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
			throws AmsException, RemoteException {

		mediatorServices.debug("createDetailATPInstrument()->" + inputDoc);
		 newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
		DocumentHandler outputDoc = new DocumentHandler();

		String benName = inputDoc.getAttribute("/benName"); // the ben_name of
															// the po line items
		String currency = inputDoc.getAttribute("/currency"); // the currency of
																// the po line
																// items
		String newTransactionOid = null; // the oid of the transaction created
											// for these po line items
		// Create a new instrument
		newTransactionOid = newInstr.createNew(inputDoc);
		LOG.debug("New instrument created with an original transacion oid of -> "
				+ newTransactionOid);

		if (StringFunction.isBlank(newTransactionOid)) {
			LOG.debug("newTransactionOid is null.  Error in this.createNew()");
			return outputDoc; // outputDoc is null
		} else
			outputDoc.setAttribute("/transactionOid", newTransactionOid);

		LOG.debug("ben_name     = " + benName);
		LOG.debug("currency     = " + currency);

		/************************************************************************
		 * Instantiate the transaction, terms, and termsParty and set the fields
		 * derived from the po line item data
		 ************************************************************************/

		// get a handle to the terms object through the transaction then set the
		// values on terms
		// we'll also have to create a termsParty object and associate it to the
		// terms object
		 transaction = (Transaction) newInstr.getComponentHandle(
				"TransactionList", Long.valueOf(newTransactionOid));
		LOG.debug("Transaction handle acquired -> "
				+ transaction.getAttribute("transaction_oid"));

		 terms = null;
		try {
			terms = (Terms) transaction
					.getComponentHandle("CustomerEnteredTerms");
			LOG.debug("terms handle acquired -> "
					+ terms.getAttribute("terms_oid"));
			outputDoc
					.setAttribute("/termsOid", terms.getAttribute("terms_oid"));
		} catch (Exception e) {
			LOG.debug("Caught error getting handle to terms");
			e.printStackTrace();
		}

		return outputDoc;

	}



	/**
	 * This method applies Payment date for individual invoices
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler applyDetailPaymentDate(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		String status = "";
		int saveSuccess = 0;
		
		String invoiceOid = null;
		String optLock = null; //optimistic locking
		DocumentHandler invoiceItem = null;
		ATPInvoiceRules atpRules = null;
		String newGroupOid = null;
		int count = 0;
		List<DocumentHandler> invoiceList  = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
		String userOid        = inputDoc.getAttribute("/UploadInvoiceList/userOid");
		String paymentDate   =  inputDoc.getAttribute("/UploadInvoiceList/payment_date");
		String ownerOrg = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
		
		String securityRights = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
		if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE))) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"SecurityProfileDetail.ApproveForPayment",
									TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		String datePattern = user.getAttribute("datepattern");
		count = invoiceList.size();
		mediatorServices.debug("InvoiceMediator: number of invoices selected: " + count);
		if (count <= 0)
		{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		try{

		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatPaymentDate   =  formatter.parse(paymentDate);
		    paymentDate = reqformatter.format(formatPaymentDate);
		}catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();
		}

		if(StringFunction.isBlank(paymentDate) || "null".equals(paymentDate)){

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PAYABLE_INVOICE_PAYMENT_DATE_REQ);
			return new DocumentHandler();
		}

		// Loop through the list of invoice oids, and processing each one in the invoice list
	    for (int i = 0; i < count; i++)
	    {
		  invoiceItem = invoiceList.get(i);
		  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
		  invoiceOid = invoiceData[0];
		  optLock = invoiceData[1];
		   try
		   {
		      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
		      String invoiceStatus = invoiceSummaryData.getAttribute("invoice_status");
		      String invoiceId     = invoiceSummaryData.getAttribute("invoice_id");
		      String invoiceGroupId     = invoiceSummaryData.getAttribute("invoice_group_oid");
		      String instrumentType = invoiceSummaryData.getAttribute("linked_to_instrument_type");
		      String loanType		= invoiceSummaryData.getAttribute("loan_type");
				 String tpName=invoiceSummaryData.getTpRuleName();

		      if(TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invoiceSummaryData.getAttribute("invoice_classification"))){
		      if (!(TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_FIN.equals(invoiceStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_PENDING_ACTION.equals(invoiceStatus))) {
		    	  mediatorServices.getErrorManager().issueError(
		    			  PayableInvoiceGroupMediator.class.getName(),
		    			  TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
		    			  mediatorServices.getResourceManager().getText("UploadInvoices.InvoiceID",TradePortalConstants.TEXT_BUNDLE),
		    			  invoiceId ,
		    			  ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
		    			  mediatorServices.getResourceManager().getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));
		    	  return new DocumentHandler();
			  }
		     }
		     // payment data should not be less than issue date
		      if ((DateTimeUtility.convertStringDateToDate(paymentDate)).before(DateTimeUtility.convertStringDateToDate(invoiceSummaryData.getAttribute("issue_date"))))
		      {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
					continue; // Move on to next Invoice
			  }
		      if(!InstrumentType.LOAN_RQST.equals(instrumentType)){
		      atpRules = getATPInvoiceRules(ownerOrg,mediatorServices);

				// Ensure the ATP Invoice rule allows Payment Date
				if(atpRules == null){

					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_REQUIRED);
					return new DocumentHandler();
				}
				else
				{
					if (TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(atpRules.getPaymentAllow())) {
						mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_REQUIRED);
						return new DocumentHandler();
					}
				}
		      }
		      if(InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
					CorporateOrganization corpObj = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(ownerOrg));

						if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corpObj.getAttribute("use_payment_date_allowed_ind"))){
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.TRADE_LOAN_RULES_RESTRICT_PAYMENT_DATE);

							return new DocumentHandler();
						}
				}

		      String oldPaymentDate = invoiceSummaryData.getAttribute("payment_date");
		      String invoiceDueDate = invoiceSummaryData.getAttribute("due_date");

			  boolean isCompliant = true;
			  try {
				  invoiceSummaryData.setAttribute("payment_date",	paymentDate);
				  LOG.debug("instrumentType:"+instrumentType+"\t loanType"+loanType);
				  if(!InstrumentType.LOAN_RQST.equals(instrumentType)){
					validatePaymentDate(paymentDate,invoiceDueDate,atpRules,mediatorServices);
				  }
				  if(InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){

//					 invoiceSummaryData.calculateInterestDiscount(opOrgOid);
				  }

					 // Set the optimistic lock attribute to make sure the invoice hasn't been changed by someone else since it was first retrieved
			        invoiceSummaryData.setAttribute("opt_lock", optLock);
			        status = invoiceSummaryData.getAttribute("invoice_status");
					LOG.debug("new group id:"+newGroupOid);
			  }
				catch (AmsException e) {
					isCompliant = false;
					IssuedError is = e.getIssuedError();
					if (is != null) {
						String errorCode = is.getErrorCode();

						if (TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL.equals(errorCode) ||
								TradePortalConstants.ATP_INVOICE_PAYMENT_DATE_RULE_FAIL.equals(errorCode)||
								TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
								TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
						{
							mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
									errorCode, invoiceId);

							// Reset payment date to its former value
							invoiceSummaryData.setAttribute("payment_date",
									oldPaymentDate);
						}

						else {
							mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyPaymentDate: Unexpected error: " + e.toString());
							throw e;
						}
					}
					else {
						mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyDetailPaymentDate: Unexpected error: " + e.toString());
						throw e;
					}
				}

				invoiceSummaryData.setAttribute("tp_rule_name",tpName);
				invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE);
                invoiceSummaryData.setAttribute("user_oid", userOid);
				saveSuccess = invoiceSummaryData.save();
				newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
				LOG.debug("new group id->"+newGroupOid);

				if(saveSuccess == 1 && isCompliant)
				  {
					if(StringFunction.isNotBlank(newGroupOid) && !invoiceGroupId.equals(newGroupOid)){
						InvoiceUtility.updateAttachmentDetails(newGroupOid,invoiceGroupId,invoiceOid,mediatorServices);
					}

						// display success message...
						mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INV_ASSIGN_PAYMENT_DATE,
									mediatorServices.getResourceManager().getText("UploadInvoices.Invoice", TradePortalConstants.TEXT_BUNDLE),
									invoiceId);
				   }
		   }
		   catch(Exception e)
		   {
			   LOG.info("General exception caught in PayableInvoiceGroupMediator:applyDetailPaymentDate");
			   e.printStackTrace();
		   }

	    }

	   return (new DocumentHandler());
	}

	/**
	 * This method retrives details for selected invoices
	 * @param invoiceOids
	 * @param corpOrgOid
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public List<DocumentHandler> getInvoicesDetail(String[] invoiceOids,String corpOrgOid) throws AmsException, RemoteException {

		List<Object> sqlParamsLst = new ArrayList<>();
		StringBuilder invGroupSql = new StringBuilder();
		StringBuilder whereClause = new StringBuilder(200);
		if(invoiceOids!=null && invoiceOids.length>0){
			for (String invoiceOid : invoiceOids) {
				whereClause.append("?, ");
				sqlParamsLst.add(invoiceOid);
			}
		}
		String finalWhereClause = "";
		if(whereClause.toString().length()>0){
			finalWhereClause = whereClause.toString().substring(0,whereClause.toString().length()-2);
		}

		invGroupSql.append(" select UPLOAD_INVOICE_OID,SELLER_NAME,CURRENCY,AMOUNT,DUE_DATE,PAYMENT_DATE,INVOICE_ID,A_INSTRUMENT_OID,A_TRANSACTION_OID," +
				" INVOICE_STATUS,A_CORP_ORG_OID from invoices_summary_data where UPLOAD_INVOICE_OID in ( "+finalWhereClause+" ) and a_corp_org_oid = ? ");
		sqlParamsLst.add(corpOrgOid);
		DocumentHandler	invoicesGroupDoc = DatabaseQueryBean.getXmlResultSet(invGroupSql.toString(), false, sqlParamsLst);
		List<DocumentHandler> rows = invoicesGroupDoc.getFragmentsList("/ResultSetRecord");
		return rows;
	}


	/**
	 * This method finds the attachments available or not for the new group
	 * and updates the Attachment_Ind for that group respectively.
	 * Use this method for group level
	 * @param newGroupOid
	 * @param mediatorServices
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private int updateAttachmentDetails(String newGroupOid, MediatorServices mediatorServices)
			throws AmsException, RemoteException {


		LOG.debug("updateAttachmentDetails() : newGroupOid:"+newGroupOid);
		int status = 0;
		String attachmentAvailable = TradePortalConstants.INDICATOR_NO;
		String sql = "select i.UPLOAD_INVOICE_OID, d.DOC_IMAGE_OID from INVOICES_SUMMARY_DATA i "
					+" left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID "
					+" where i.A_INVOICE_GROUP_OID = ? ";
		DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{newGroupOid});
		
		if (invoiceListDoc != null) {
			List<DocumentHandler> invoiceList = invoiceListDoc.getFragmentsList("/ResultSetRecord");
			for (DocumentHandler invoiceDoc : invoiceList) {
				String invoiceOid = invoiceDoc.getAttribute("/UPLOAD_INVOICE_OID");
				String docImageOid = invoiceDoc.getAttribute("/DOC_IMAGE_OID");
				LOG.debug("invoiceOid:"+invoiceOid+"\tdocImageOid:"+docImageOid);
				if (StringFunction.isNotBlank(invoiceOid) &&
						StringFunction.isNotBlank(docImageOid)) {
					attachmentAvailable =  TradePortalConstants.INDICATOR_YES;
					break;
				}
			}
			LOG.debug("attachmentAvailable:"+attachmentAvailable);
			   InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong(newGroupOid));
				invoiceGroup.setAttribute("attachment_ind", attachmentAvailable);
				status = invoiceGroup.save();
				LOG.debug("Updated status:"+status);



		}
		return status;
	}

	private int updateAttachmentDetails(String newInvoiceGroupOid, String oldInvoiceOid, MediatorServices medServices) throws AmsException,RemoteException{

		String attachmentAvailable = TradePortalConstants.INDICATOR_NO;
		int status =0;
		int	imagesCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID", "DOCUMENT_IMAGE", "P_TRANSACTION_OID in " +
				" (select upload_invoice_oid from invoices_summary_data where  a_invoice_group_oid = ?) ", false, new Object[]{newInvoiceGroupOid});


		if(imagesCount>0)
			attachmentAvailable = TradePortalConstants.INDICATOR_YES;
		else
			attachmentAvailable = TradePortalConstants.INDICATOR_NO;

		 InvoiceGroup invoiceGroup = (InvoiceGroup) medServices.createServerEJB("InvoiceGroup",
						Long.parseLong(newInvoiceGroupOid));
			invoiceGroup.setAttribute("attachment_ind", attachmentAvailable);
			status = invoiceGroup.save();
			LOG.debug("Updated status:"+status);
		  int oldGroupImgCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID", "DOCUMENT_IMAGE", "P_TRANSACTION_OID in " +
					" (select upload_invoice_oid from invoices_summary_data where  a_invoice_group_oid = ?)", false, new Object[]{oldInvoiceOid});

			LOG.debug("oldGroupImgCount:"+oldGroupImgCount);
			if(oldGroupImgCount>0)
				attachmentAvailable = TradePortalConstants.INDICATOR_YES;
			else
				attachmentAvailable = TradePortalConstants.INDICATOR_NO;

			  invoiceGroup = (InvoiceGroup) medServices.createServerEJB("InvoiceGroup",
							Long.parseLong(oldInvoiceOid));
				invoiceGroup.setAttribute("attachment_ind", attachmentAvailable);
				status = invoiceGroup.save();
				LOG.debug("Updated status:"+status);

		return status;

	}

	  private DocumentHandler assignLoanType(DocumentHandler inputDoc, MediatorServices mediatorServices)
				throws RemoteException, AmsException {
		  mediatorServices.debug("PayableGroupMediatorBean.assignLoanType: Start: ");
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String loanType = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
			String corpOrgOid  = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_LOAN_TYPE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayAssignLoanType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");

			if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.LoanType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
					+ invoiceGroupList.size());

			// Process each Invoice Group in the list
			for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
				String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
				String invoiceGroupOid = invoiceData[0];
				String optLock = invoiceData[1];
				InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(invoiceGroupOid));
				String oldStatus = invoiceGroup.getAttribute("invoice_status");
				String instrType = invoiceGroup.getAttribute("linked_to_instrument_type");
				String tradingPartnerName = invoiceGroup.getAttribute("trading_partner_name");
				String groupDescription = invoiceGroup.getDescription();
				boolean groupHasBeenSaved = false;

				// only Pending Action - No Instr Type invoices can be assigned an
				// instrument type
				if(TradePortalConstants.INV_LINKED_INSTR_REC.equals(instrType)){

					   mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.REC_INVOICE_ASSIGN_LOAN_TYPE_NOT_ALLOW,tradingPartnerName);
			    	  return new DocumentHandler();
				  }

				  if(TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED.equals(oldStatus) || TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_DELETED.equals(oldStatus)){
					 mediatorServices.getErrorManager().issueError(
		     	               UploadInvoiceMediator.class.getName(),
		     	               TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
		     	               mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
		     	               groupDescription ,
		     	               ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", oldStatus, mediatorServices.getCSDB().getLocaleName()),
		     	               mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignLoanType", TradePortalConstants.TEXT_BUNDLE));
					 return new DocumentHandler();
				}
                 // check if payment is null or not while assigning TRADE_LOAN
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));

				 LOG.info("isPayDateNull:"+isPayDateNull);
				 if(TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
							TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){

					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE,mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE) +" - "+tradingPartnerName);
			    	  return new DocumentHandler();
				}

					invoiceGroup.setAttribute("loan_type", loanType);
					invoiceGroup.setAttribute("opt_lock", optLock);
					String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
					DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
					if (invoiceListDoc != null) {
						int success = 0;
						String newGroupOid = null;
						List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

						for (DocumentHandler doc : records) {
							String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

							if (StringFunction.isNotBlank(invoiceOid)) {
								InvoicesSummaryData invoiceSummaryData =
									(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
											Long.parseLong(invoiceOid));
								String amount = invoiceSummaryData.getAttribute("amount");
								String newStatus = null;
								LOG.debug("loanType:"+loanType +"\t amount:"+amount+"\t corpOrgOid:"+corpOrgOid);
							try{
							 if(StringFunction.isNotBlank(loanType) && TradePortalConstants.EXPORT_FIN.equals(loanType)){
									 LOG.debug("amount:"+amount);
									 invoiceSummaryData.setAttribute("net_invoice_finance_amount", amount);
									 invoiceSummaryData.calculateInterestDiscount(corpOrgOid);
								}
								else if(StringFunction.isNotBlank(loanType) && TradePortalConstants.TRADE_LOAN.equals(loanType)){
									LOG.debug("corpOrgOid:"+corpOrgOid);
								}
								else { //if(StringFunction.isBlank(loanType)){
									invoiceSummaryData.setAttribute("net_invoice_finance_amount", null);
								}
							}
							catch (AmsException e) {
								LOG.info("Inside exceptoin");
								IssuedError is = e.getIssuedError();
								if (is != null) {
									String errorCode = is.getErrorCode();
									LOG.info("errorCode:"+errorCode);
									//BSL IR NNUM040526786 04/05/2012 BEGIN - add Pct Val To Fn Zero and combine three if branches w/identical logic
									if (TradePortalConstants.ERR_UPLOAD_INV_TENOR_DAYS_LESS.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_TRADE_INV_DUE_PMT_DAYS_DIFF.equals(errorCode) ||
											TradePortalConstants.ERR_UPLOAD_INV_PCT_VAL_TO_FIN_ZERO.equals(errorCode))
									{
										newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PEND_ACT_UNABLE_TO_FIN;
									}
									//BSL IR NNUM040526786 04/05/2012 END
									else {	LOG.info("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
										mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
										throw e;
									}
								}
								else {	LOG.info("2 InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
									mediatorServices.debug("InvoiceGroupMediatorBean.assignLoanType: Unexpected error: " + e.toString());
									throw e;
								}
							}
									if (!groupHasBeenSaved) {
										invoiceGroup.setAttribute("loan_type", loanType);
											// check that another matching group does not already exist.
											newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);
											LOG.info("newgroup:"+newGroupOid);
											if (StringFunction.isNotBlank(newGroupOid)
													&& !newGroupOid.equals(invoiceGroupOid)) {
												// there is another match, so assign the other group's OID
												// to these invoices and delete this group
												success = invoiceGroup.delete();
											}
											else {
												success = invoiceGroup.save();
											}

										if (success == 1) {
											groupHasBeenSaved = true;

											mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
													TradePortalConstants.INV_ASSIGN_LOAN_TYPE,
													groupDescription);
										}
									}
									invoiceSummaryData.setAttribute("loan_type", loanType);
									//invoiceSummaryData.setAttribute("invoice_status", newStatus);
									if (StringFunction.isNotBlank(newGroupOid)) {
										invoiceSummaryData.setAttribute("invoice_group_oid",
												newGroupOid);
									}
									 String tpName=invoiceSummaryData.getTpRuleName();

									LOG.info("buyerNam:"+tpName);
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_LOAN_TYPE);
					                invoiceSummaryData.setAttribute("user_oid", userOid);
									if (invoiceSummaryData.save() == 1) {
										newGroupOid = invoiceSummaryData.getAttribute("invoice_group_oid");
										if(StringFunction.isNotBlank(newGroupOid)){
											updateAttachmentDetails(newGroupOid,mediatorServices);
									}
									}
								}
							}
						}
					}
					mediatorServices.debug("PayableGroupMediatorBean.assignLoanType: End: ");
					return new DocumentHandler();
				}


	 private DocumentHandler assignInstrType(DocumentHandler inputDoc,
				MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			mediatorServices.debug("PayableInvoiceGroupMediatorBean.assignInstrType: Start: ");
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String loanType       = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
			String instrumentType = inputDoc.getAttribute("/InvoiceGroupList/linked_to_instrument_type");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_ASSIGN_INSTR_TYPE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayAssignInstrType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			String transactionStatus = null;
			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");

			if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.AssignInstrType",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			mediatorServices.debug("PayableInvoiceGroupMediatorBean: number of invoice groups selected: "
					+ invoiceGroupList.size());
			//validate loan type against REFDATA-LOANTYPE
			Vector loanList = InvoiceUtility.getLoanTypesFromRefData();
			LOG.info("loanList:"+loanList);
			if(StringFunction.isNotBlank(loanType)){
				 if(!loanList.contains(loanType)){
					 loanType="";
				 }
			}
			LOG.info("finally loanType:"+loanType);
			// Process each Invoice Group in the list
			for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
				String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
				String invoiceGroupOid = invoiceData[0];
				String optLock = invoiceData[1];
				InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(invoiceGroupOid));
				String oldStatus = invoiceGroup.getAttribute("invoice_status");
				String oldInstrType = invoiceGroup.getAttribute("linked_to_instrument_type");
				String groupDescription = invoiceGroup.getDescription();
				boolean groupHasBeenSaved = false;

				//PMitnala IR#T36000019380 Rel 8.3 - System should not allow to Assign Instrument type to Deleted Invoice. START
				if (TradePortalConstants.UPLOAD_INV_STATUS_DELETED.equals(oldStatus)) {
					mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
							mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
							groupDescription,
							ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", oldStatus, mediatorServices.getCSDB().getLocaleName()),
							mediatorServices.getResourceManager().getText("UploadInvoiceAction.AssignInstrType", TradePortalConstants.TEXT_BUNDLE));


					continue; // Move on to next Invoice Group

				}
				//PMitnala IR#T36000019380 Rel 8.3 - System should not allow to Assign Instrument type to Deleted Invoice. END
				// check if payment is null or not while assigning TRADE_LOAN
				 boolean isPayDateNull = InvoiceUtility.isPaymentDateNull(invoiceGroupOid);
				 CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
							Long.parseLong(corpOrgOid));
				 LOG.info("isPayDateNull:"+isPayDateNull);

				if(InstrumentType.LOAN_RQST.equals(instrumentType) && TradePortalConstants.TRADE_LOAN.equals(loanType) && !isPayDateNull &&
						TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(corporateOrganization.getAttribute("use_payment_date_allowed_ind"))){

					mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.TRADE_LOAN_NO_PAYMENT_DATE,mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE) +" - "+groupDescription);
			    	  return new DocumentHandler();

				}
				invoiceGroup.setAttribute("linked_to_instrument_type", instrumentType);
				invoiceGroup.setAttribute("opt_lock", optLock);
				String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
				DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
				if (invoiceListDoc != null) {
					int success = 0;
					String newGroupOid = null;
					List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");

					for (DocumentHandler doc : records) {
						String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");

						if (StringFunction.isNotBlank(invoiceOid)) {
							InvoicesSummaryData invoiceSummaryData =
								(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
										Long.parseLong(invoiceOid));
							String invoiceId		   = invoiceSummaryData.getAttribute("invoice_id");
							String transactionOid    = invoiceSummaryData.getAttribute("transaction_oid");
							if(StringFunction.isNotBlank(transactionOid)){
								  Transaction tran = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
									 transactionStatus = tran.getAttribute("transaction_status");
									 LOG.debug("transactionStatus:"+transactionStatus);
							  }
							if(!TransactionStatus.AUTHORIZED.equals(transactionStatus)){
								invoiceGroup.setAttribute("linked_to_instrument_type", instrumentType);

								if(InstrumentType.LOAN_RQST.equals(instrumentType)){
									invoiceGroup.setAttribute("loan_type", loanType);
								}
							}else {
								 mediatorServices.getErrorManager().issueError(
				     	                UploadInvoiceMediator.class.getName(),
				     	                TradePortalConstants.REC_INVOICE_ASSIGN_INSTRUMENT_TYPE_NOT_ALLOW, invoiceId);
				     	   	 continue; // Move on to next Invoice
							}
							invoiceSummaryData.setAttribute("linked_to_instrument_type",instrumentType);
							if(TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrumentType)){
								invoiceSummaryData.setAttribute("invoice_status",TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH);
							}

							invoiceSummaryData.setAttribute("loan_type", loanType);
							ArMatchingRule matchingRule = null;
							 String tpName=invoiceSummaryData.getTpRuleName();

							if (!groupHasBeenSaved) {
								if (StringFunction.isNotBlank(oldInstrType)
												&& !oldInstrType.equals(instrumentType))
								{
									// check that another matching group does not already exist.
									newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);

									if (StringFunction.isNotBlank(newGroupOid)
											&& !newGroupOid.equals(invoiceGroupOid)) {
										// there is another match, so assign the other group's OID
										// to these invoices and delete this group
										success = invoiceGroup.delete();
									}
									else {
										success = invoiceGroup.save();
									}
								}
								else {
									success = invoiceGroup.save();
								}

								if (success == 1) {
									groupHasBeenSaved = true;

									mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
											TradePortalConstants.INV_INSTR_ASSIGNED_SUCCESSFUL,
											ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE", instrumentType, mediatorServices.getCSDB().getLocaleName()),tpName);
								}
							}


							invoiceSummaryData.setAttribute("tp_rule_name",tpName);
							LOG.debug("AssignInst Group called mathcingrule:"+matchingRule);
							if (StringFunction.isNotBlank(newGroupOid)) {
								invoiceSummaryData.setAttribute("invoice_group_oid",
										newGroupOid);
							}
							String status =invoiceSummaryData.getAttribute("invoice_status");
							invoiceSummaryData.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_ASSIGN_INSTR_TYPE);
			                invoiceSummaryData.setAttribute("user_oid", userOid);
							
							if (invoiceSummaryData.save() == 1) {
								if(StringFunction.isNotBlank(newGroupOid)){
									updateAttachmentDetails(newGroupOid,mediatorServices);
							}
							}
						}
					}
				}
			}
			mediatorServices.debug("InvoiceGroupMediatorBean.assignInstrType: End: ");
			return new DocumentHandler();
		}

		
	    /**
		 * This method creates an ATP Instrument for Receivable invoices
		 * @param inputDoc
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler createLoanRequest(DocumentHandler inputDoc,
				MediatorServices mediatorServices)
		throws RemoteException, AmsException {

			LOG.debug("PayableInvoiceGroupMediatorBean.createLoanRequest: Start: "+inputDoc);
			String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String clientBankOid = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
			String opOrgOid = inputDoc.getAttribute("/InvoiceGroupList/bankBranch");
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayCreateLoanReq",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			 Vector invoiceOidsVector = new Vector();
			 DocumentHandler instrCreateDoc = null;
			 DocumentHandler  uploadInvoiceDoc = null;
			 String uploadInvOid= null;
			 String instrumentOid = null;
			 String transactionOid = null;
			 String corpOrgOid = null;
			 String firstInvoiceGroupOid = null;
			 String [] groupIds=null;
			 String newGroupOid = null;
			 Vector tempOids = new Vector();
			if (!(SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ))) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.REC_INV_LRQ_NO_ACCESS);
				return new DocumentHandler();
			}

			List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.InvLRCreateLRQ",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}

			if(StringFunction.isBlank(opOrgOid)){

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYABLE_INVOICE_BANK_BRANCH_REQ);
				return new DocumentHandler();
			}

			groupIds = new String[ invoiceGroupList.size()];
			for (int i = 0; i < invoiceGroupList.size(); i++) {
				DocumentHandler invoiceGroupItem = invoiceGroupList.get(i);
			String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
			firstInvoiceGroupOid = invoiceData[0];
			groupIds[i]=invoiceData[0];
			tempOids.add(invoiceData[0]);
			}

			 //LoanType check for the selected invoices
			  InvoiceGroup invoiceGroup = null;
			  String loanType = "";
			 if(tempOids.size()>0){
				 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong((String)tempOids.get(0)));
				String tpName = invoiceGroup.getDescription();
				loanType = invoiceGroup.getAttribute("loan_type");
				LOG.debug("tpName:"+tpName+"\t loanType:"+loanType);
				if(StringFunction.isBlank(loanType)){
					 mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);
						return new DocumentHandler();
				 }
				String dupName = null;
				String dupLoanType = null;
				 for(int i=1;i<tempOids.size();i++){
					 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
								Long.parseLong((String)tempOids.get(i)));
					 dupName = 	invoiceGroup.getDescription();
					 dupLoanType = invoiceGroup.getAttribute("loan_type");
					 LOG.debug("dupName:"+dupName+"\t dupLoanType:"+dupLoanType);
					 if(StringFunction.isBlank(dupLoanType)){
						 mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);
							return new DocumentHandler();
					 }
					// if((!tpName.equals(dupName) && !TradePortalConstants.TRADE_LOAN.equals(loanType)) || !loanType.equals(dupLoanType)){
					if(!loanType.equals(dupLoanType)){
						 mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.LOAN_REQ_MULTIPLE_LOAN_TYPES_SELECTED);
							return new DocumentHandler();
					 }
				 }
			 }

			if(firstInvoiceGroupOid!=null) {
			 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
						Long.parseLong(firstInvoiceGroupOid));
			corpOrgOid = invoiceGroup.getAttribute("corp_org_oid");
			// Retrieve the set of invoices for the passing group id
			List<DocumentHandler> invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
			if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){
					 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
					 uploadInvoiceDoc = invoicesGroupDoc.get(pos);

				String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
				String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
				//if already associated then do not create an instrument
				if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
					Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
					mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.LRQ_INSTRUMENT_EXISTS,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
					invoicesGroupDoc.remove(pos);
					pos--;
				}
					 }
			}
			if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0)
			return new DocumentHandler();


			 InstrumentProperties instProp = new InstrumentProperties();
			 instProp.setCorporateOrgOid(corpOrgOid);
			 instProp.setSecurityRights(securityRights);
			 instProp.setUserOid(userOid);
			 instProp.setClientBankOid(clientBankOid);
			 instProp.setOpOrgOid(opOrgOid);
			 instrCreateDoc = processLoanInvoiceGroups(instProp,invoicesGroupDoc,mediatorServices);

			 String instType = instrCreateDoc!= null? instrCreateDoc.getAttribute("/instrumentType"):null;
			 mediatorServices.debug("instType:"+instType);

			 if(instType != null) {
				if (invoicesGroupDoc != null) {
				 for (DocumentHandler row : invoicesGroupDoc) {
					 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
					 invoiceOidsVector.addElement(invOid);
				 }
			}

			        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, TradePortalConstants.INVOICE_TYPE_PAY_MGMT );
			        String importIndicator = (String)impFinBuyerBackMap.get("importIndicator");
			        String financeType = (String)impFinBuyerBackMap.get("financeType");
			        String buyerBacked = (String)impFinBuyerBackMap.get("buyerBacked");

			        if ( TradePortalConstants.TRADE_LOAN.equals(loanType) && TradePortalConstants.TRADE_LOAN_PAY.equals(financeType) ){
			        	String country = InvoiceUtility.fetchOpOrgCountryForOpOrg(opOrgOid);

			        	boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethodForInvoices(invoiceOidsVector, country, instProp.getCurrency(), mediatorServices.getErrorManager(),false);
			        	boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformationForInvoices(invoiceOidsVector, country, instProp.getCurrency(), mediatorServices.getErrorManager(),false);
			        	if (!isPaymentMethodValid || !isBankBranchCodeValid){
			        		return new DocumentHandler();
			        	}

			        }

			  // Create instrument using the document
			DocumentHandler doc = createLRQInstrument(instrCreateDoc, mediatorServices,invoiceOidsVector);
			transactionOid = doc.getAttribute("/transactionOid");
		    	if(transactionOid!=null){
		    			transaction = (Transaction) mediatorServices.createServerEJB("Transaction",Long.parseLong(transactionOid));
				        instrumentOid = transaction.getAttribute("instrument_oid");
				        LOG.debug("Instrument OID:"+instrumentOid);
				 }


		    	String completeInstrId = "";

		    	if(instrumentOid!=null){
			        newInstr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrumentOid));
			        completeInstrId = newInstr.getAttribute("complete_instrument_id");
			        if (StringFunction.isNotBlank(importIndicator)){
			        	newInstr.setAttribute("import_indicator", importIndicator);
			        }
			        LOG.debug("Instrument Num:"+completeInstrId);
		    	}


		    	Vector invoiceIDVector = new Vector();
		    	boolean success = true;

		    	terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		        if (StringFunction.isNotBlank(financeType)){
		        	terms.setAttribute("finance_type", financeType);
		        }
		        if (StringFunction.isNotBlank(buyerBacked)){
		        	terms.setAttribute("financing_backed_by_buyer_ind", buyerBacked);
		        }
				ArMatchingRule rule = null;
				String tempDate = "";
				 String tpName="";
				InvoicesSummaryData invoice =
					(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
							Long.parseLong((String)invoiceOidsVector.get(0)));

				 tempDate = StringFunction.isNotBlank(invoice
						.getAttribute("payment_date")) ? invoice
						.getAttribute("payment_date") : invoice.getAttribute("due_date");
				try(Connection con = DatabaseQueryBean.connect(false);
						PreparedStatement pStmt1 = con
								.prepareStatement("UPDATE INVOICES_SUMMARY_DATA  SET INVOICE_STATUS=?, A_INSTRUMENT_OID=?, A_TRANSACTION_OID=?, "
										+ "A_TERMS_OID=?, a_invoice_group_oid=?, status =?  where upload_invoice_oid =?");
						PreparedStatement pStmt2 = con
								.prepareStatement("INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, "
						+ "INVOICE_STATUS, P_UPLOAD_INVOICE_OID, A_USER_OID) VALUES (?,?,?,?,?,?)")
						) {
					
					con.setAutoCommit(false);
					
					
					for (int i = 0; i < invoiceOidsVector.size(); i++) {
					uploadInvOid =  (String) invoiceOidsVector.get(i);

					InvoicesSummaryData inv =
							(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
									Long.parseLong(uploadInvOid));
					invoiceIDVector.addElement(inv.getAttribute("invoice_id"));
					inv.setAttribute("invoice_status", TradePortalConstants.PO_STATUS_ASSIGNED);
					inv.setAttribute("status", TradePortalConstants.PO_STATUS_ASSIGNED);

						 rule = inv.getMatchingRule();
					tpName =inv.getTpRuleName(rule);
					inv.setAttribute("tp_rule_name",tpName);
					inv.verifyInvoiceGroup();
					newGroupOid = inv.getAttribute("invoice_group_oid");
					inv=null;
					pStmt1.setString(1, TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(2,instrumentOid);
					pStmt1.setString(3,transactionOid);
					pStmt1.setString(4,String.valueOf(terms.getAttribute("terms_oid")));
					pStmt1.setString(5,newGroupOid);
					pStmt1.setString(6,TradePortalConstants.PO_STATUS_ASSIGNED);
					pStmt1.setString(7,uploadInvOid);
					pStmt1.addBatch();
					pStmt1.clearParameters();
					InvoiceUtility.createPurchaseOrderHisotryLog(
							pStmt2,
							TradePortalConstants.PAYABLE_UPLOAD_INV_ACTION_LINK_INST,
							TradePortalConstants.PAYABLE_UPLOAD_INV_STATUS_ASSIGNED,
							(String) invoiceOidsVector.elementAt(i), userOid);
				}
					pStmt1.executeBatch();
					pStmt2.executeBatch();
					con.commit();
					

			} catch (AmsException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				 success = false;
				e.printStackTrace();
			}

		   			// Success message to User
				if (success) {
					if (newGroupOid != null
							&& !firstInvoiceGroupOid.equals(newGroupOid)) {

						updateAttachmentDetails(newGroupOid, mediatorServices);
					}
					mediatorServices.getErrorManager().issueError(
							PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSERT_SUCCESSFUL,
							mediatorServices.getResourceManager().getText(
									"UploadInvoices.LRQMessage",
									TradePortalConstants.TEXT_BUNDLE)
									+ " -  " + completeInstrId);
				} else {
					mediatorServices.getErrorManager().issueError(
							PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.PAYABLE_INV_ATP_FAILED);
				}
	    		InvoiceUtility.deriveTransactionCurrencyAndAmountsFromInvoice(
		  				transaction, terms, invoiceOidsVector, newInstr,invoiceIDVector,"");
		  		terms.setAttribute("invoice_only_ind",
		  				TradePortalConstants.INDICATOR_YES);

				if (!(TradePortalConstants.INDICATOR_T.equals(newInstr.getAttribute("import_indicator")) &&
						TradePortalConstants.TRADE_LOAN_PAY.equals(terms.getAttribute("finance_type")))){

			   		long oid = terms.newComponent("FirstTermsParty");
			  		TermsParty termsParty = (TermsParty) terms
			  				.getComponentHandle("FirstTermsParty");
			  		LOG.debug("termsParty handle acquired -> "
			  				+ termsParty.getAttribute("terms_party_oid"));
			  		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy");


			  		try {
			  			if (!StringFunction.isBlank(tempDate)) {
			  				Date date = jPylon.parse(tempDate);
			  				terms.setAttribute("invoice_due_date",
			  						DateTimeUtility.convertDateToDateTimeString(date));
			  			}
			  		} catch (Exception e) {
			  			e.printStackTrace();
			  		}


					String charges = StringFunction.isBlank(invoice.getAttribute("charges"))? TradePortalConstants.CHARGE_UPLOAD_FW_SHARE : invoice.getAttribute("charges") ;
					terms.setAttribute("bank_charges_type",charges);

					TermsParty firstTermsParty = (TermsParty) terms
					.getComponentHandle("FirstTermsParty");
					if(!(TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")) || TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType))){
					firstTermsParty.setAttribute("name", tpName);
					if(rule!=null){
					firstTermsParty.setAttribute("address_line_1",rule.getAttribute("address_line_1"));
					firstTermsParty.setAttribute("address_line_2",rule.getAttribute("address_line_2"));
					firstTermsParty.setAttribute("address_city",rule.getAttribute("address_city"));
					firstTermsParty.setAttribute("address_state_province",rule.getAttribute("address_state"));
					firstTermsParty.setAttribute("address_country",rule.getAttribute("address_country"));
					firstTermsParty.setAttribute("address_postal_code",rule.getAttribute("postalcode"));

			       
					 }
					}
					if(rule!=null && TradePortalConstants.SELLER_REQUESTED_FINANCING.equals(financeType)){
						firstTermsParty.setAttribute("OTL_customer_id",rule.getAttribute("tps_customer_id"));
			        }
				}

		  		// Set the counterparty for the instrument and some "copy of" fields
		  		transaction
		  				.setAttribute("copy_of_amount", terms.getAttribute("amount"));
		  		transaction.setAttribute("instrument_amount",
		  				terms.getAttribute("amount"));
		  		newInstr.setAttribute("copy_of_instrument_amount",
		  				transaction.getAttribute("instrument_amount"));

		  		if (terms.isAttributeRegistered("expiry_date")) {
		  			newInstr.setAttribute("copy_of_expiry_date",
		  					terms.getAttribute("expiry_date"));
		  		}

		  		if (terms.isAttributeRegistered("reference_number")) {
		  			newInstr.setAttribute("copy_of_ref_num",
		  					terms.getAttribute("reference_number"));
		  		}
				String requiredOid = "upload_invoice_oid";
				 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
						 invoiceOidsVector, requiredOid);
				if (TradePortalConstants.INDICATOR_T.equals(newInstr.getAttribute("import_indicator")) &&
						TradePortalConstants.TRADE_LOAN_PAY.equals(terms.getAttribute("finance_type"))){
					StringBuilder payMethodSQL = new StringBuilder("select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where ");
					payMethodSQL.append(invoiceOidsSql);
					DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL.toString(), false, new ArrayList<Object>());
					if (resultDoc != null){
						String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
						if (StringFunction.isNotBlank(payMethod)){
							terms.setAttribute("payment_method",payMethod);
							int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
							if (singleBeneCount == 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_BEN_ACCT);
								InvoiceUtility.populateTerms(terms,(String)invoiceOidsVector.get(0),invoiceOidsVector.size(), mediatorServices);
							}else if (singleBeneCount > 1){
								terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
								//IR 29777 start
								if(StringFunction.isBlank(terms.getAttribute("c_FirstTermsParty")))
					            {
					                 terms.newComponent("FirstTermsParty");
					            }
								TermsParty firstTermsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
								firstTermsParty.setAttribute("terms_party_type", TermsPartyType.BENEFICIARY);

								if(!TradePortalConstants.EXPORT_FIN.equals(invoice.getAttribute("loan_type")))
								firstTermsParty.setAttribute("name", mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
								//IR 29777 end
								
								int paymentChargeCount=InvoiceUtility.isSinglePaymentCharge(invoiceOidsSql);
								if (paymentChargeCount == 1){
									
									terms.setAttribute("bank_charges_type",invoice.getAttribute("charges"));
								}
								else if (paymentChargeCount > 1){
									terms.setAttribute("bank_charges_type",TradePortalConstants.CHARGE_UPLOAD_FW_SHARE);
									InvoiceUtility.setBankChargesForInvoices(invoiceOidsSql);
									mediatorServices.getErrorManager().issueError(
									          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INFO_SHARED_BANK_CHARGES);
									 
								}
							}

						}

					}
				}
				if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) ||
						TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){

					 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
					String invoiceDueDate = null;
					if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) &&
							InvoiceUtility.validateSameDate(invoiceOidsSql) &&
							InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
						StringBuilder dueDateSQL = new StringBuilder("select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
								" from invoices_summary_data where ");
						dueDateSQL.append(invoiceOidsSql);
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
						invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
					}


					if (StringFunction.isNotBlank(invoiceDueDate)){

						try {
							SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
							SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

								Date date = jPylon.parse(invoiceDueDate);

								terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
						} catch (Exception e) {
							e.printStackTrace();
						}


					}
				}


		  		newInstr.setAttribute("a_counter_party_oid",
		  				terms.getAttribute("c_FirstTermsParty"));

		  		transaction.save(false);
		  		LOG.debug("Saving instrument with oid '"
		  				+ newInstr.getAttribute("instrument_oid") + "'...");
		  		newInstr.save(false);// save without validate
		  		LOG.debug("Instrument saved");
			  }
			}

			LOG.debug("PayableInvoiceGroupMediatorBean.createLoanRequest: End: ");
			return new DocumentHandler();

		}


		 /**
		  * This method invokes the Instrument EJB.
		  * successful creation of a new Instrument returns a transction_oid
		  * @param inputDoc
		  * @param mediatorServices
		  * @return String
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler createLRQInstrument(DocumentHandler inputDoc, MediatorServices mediatorServices,Vector invOids)
			      throws AmsException, RemoteException {
			 	 LOG.debug("createLRQInstrument()->"+inputDoc+"\t:"+invOids);
			     newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
			     DocumentHandler outputDoc = new DocumentHandler();

			        String benName              = inputDoc.getAttribute("/benName"); //the ben_name of the invoice line items
			        String currency             = inputDoc.getAttribute("/currency"); //the currency of the invoice line items
			        String newTransactionOid    = null; //the oid of the transaction created for these invoice line items

			        //Create a new instrument
			        newTransactionOid = newInstr.createNew(inputDoc);
			        LOG.debug("New instrument created with an original transacion oid of -> " + newTransactionOid);

			        if (newTransactionOid == null)
			        {
			        	LOG.debug("newTransactionOid is null.  Error in this.createNew()");
			            return outputDoc; //outputDoc is null
			        }
			        else
			            outputDoc.setAttribute("/transactionOid", newTransactionOid);

			        LOG.debug("ben_name     = " + benName);
			        LOG.debug("currency     = " + currency);

			        /************************************************************************
			         * Instantiate the transaction, terms, and termsParty and set the fields
			         * derived from the po line item data
			         ************************************************************************/

			        //get a handle to the terms object through the transaction then set the values on terms
			        //we'll also have to create a termsParty object and associate it to the terms object
			        Transaction transaction = (Transaction) newInstr.getComponentHandle("TransactionList", Long.valueOf(newTransactionOid));
			        LOG.debug("Transaction handle acquired -> " + transaction.getAttribute("transaction_oid"));


			        try
			        {
			            terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
			            LOG.debug("terms handle acquired -> " + terms.getAttribute("terms_oid"));
			            outputDoc.setAttribute("/termsOid", terms.getAttribute("terms_oid"));
			        }
			        catch (Exception e)
			        {
			            LOG.debug("Caught error getting handle to terms");
			            e.printStackTrace();
			        }

			        long oid = terms.newComponent("FirstTermsParty");
			        TermsParty termsParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
			        LOG.debug("termsParty handle acquired -> " + termsParty.getAttribute("terms_party_oid"));

			        return outputDoc;

		 }

		/**
		 * This method loops through the invoices of a selected invoice group
		 * and validate based on Seller name/Currency name/Amount
		 * @param instrumentProp
		 * @param invGroupList
		 * @param mediatorServices
		 * @return DocumentHandler
		 * @throws RemoteException
		 * @throws AmsException
		 */
		private DocumentHandler processLoanInvoiceGroups(InstrumentProperties instrumentProp, List<DocumentHandler> invGroupList, MediatorServices mediatorServices)
			      throws AmsException {

			   LOG.debug("processLoanInvoiceGroups()-> "+invGroupList);
			   String buyerName = null;
			   String currency =null;
			   String paymentDate = null;
			   String dueDate = null;
			   String loanType = null;
			   String invalidBuyerName = null;
			   String invalidCurrency = null;
			   String invalidPaymentDate = null;
			   String invalidDueDate = null;
			   DocumentHandler invGroupDoc = null;
			   DocumentHandler nxtInvGroupDoc = null;
			   DocumentHandler instrCreateDoc = null;
			   BigDecimal amount = BigDecimal.ZERO;
			   BigDecimal nextAmount = BigDecimal.ZERO;
			   String amt = null;
			   BigDecimal totalAmount = BigDecimal.ZERO;
			   String date = null;
			   String invalidDate = null;
			   if(invGroupList!=null && invGroupList.size()>0){
			   int pos = 0;

					  invGroupDoc = invGroupList.get(pos);

					  buyerName = invGroupDoc.getAttribute("/SELLER_NAME");
					  currency =  invGroupDoc.getAttribute("/CURRENCY");
					  paymentDate = invGroupDoc.getAttribute("/PAYMENT_DATE");
					  dueDate = invGroupDoc.getAttribute("/DUE_DATE");
					  loanType = invGroupDoc.getAttribute("/LOAN_TYPE");
					  if(StringFunction.isBlank(paymentDate))
						  date = dueDate;
					  else
						  date = paymentDate;
					  LOG.debug("date:"+date);
					  amt = invGroupDoc.getAttribute("/AMOUNT");
					  if(amt!=null)
						  amount = BigDecimal.valueOf(Double.parseDouble(amt));
					  totalAmount = amount;
					  int subIdx = pos+1;

				        while (subIdx < invGroupList.size()) {

				        		nxtInvGroupDoc = invGroupList.get(subIdx);
				        		invalidBuyerName = nxtInvGroupDoc.getAttribute("/SELLER_NAME");
				            	invalidCurrency =  nxtInvGroupDoc.getAttribute("/CURRENCY");
				            	invalidPaymentDate = nxtInvGroupDoc.getAttribute("/PAYMENT_DATE");
				            	invalidDueDate = nxtInvGroupDoc.getAttribute("/DUE_DATE");
								String am =  nxtInvGroupDoc.getAttribute("/AMOUNT");
								if(StringFunction.isBlank(invalidPaymentDate))
									invalidDate = invalidDueDate;
								else
									invalidDate =invalidPaymentDate;
								LOG.debug("invalid Date:"+invalidDate);
								 if(am!=null)
									  nextAmount = BigDecimal.valueOf(Double.parseDouble(am));
								 LOG.debug("loanType:"+loanType);
								 if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
									 if(!currency.equals(invalidCurrency)){
										 mediatorServices.getErrorManager().issueError(
													TradePortalConstants.ERR_CAT_1,
													TradePortalConstants.INVALID_LOAN_REQ_TRADE_LOAN_GROUP);
											return new DocumentHandler();
									 }
									 else{
											totalAmount = totalAmount.add(nextAmount);

									}
								 }else if(TradePortalConstants.INV_FIN_BR.equals(loanType) || TradePortalConstants.INV_FIN_BB.equals(loanType)){
									if(!(buyerName.equals(invalidBuyerName) && currency.equals(invalidCurrency) && date.equals(invalidDate))){
									  mediatorServices.getErrorManager().issueError(
												TradePortalConstants.ERR_CAT_1,
												TradePortalConstants.INVALID_LOAN_REQ_PAYABLE_GROUP);
										return new DocumentHandler();
								}else{
										totalAmount = totalAmount.add(nextAmount);

								}
							  }
								 LOG.info("subIdx ++ "+subIdx);
								 subIdx++;
				            }

					  instrumentProp.setAmount(totalAmount);
					  instrumentProp.setCurrency(currency);
					  instrumentProp.setShipDate(date);
					  instrumentProp.setBenName(buyerName);
					  instrumentProp.setInstrumentType(InstrumentType.LOAN_RQST);
					  // Build the input document used to create a new instrument
				      instrCreateDoc = buildInstrumentCreateDoc(instrumentProp);

			   }

			return instrCreateDoc;
		  }


		/**
		  * This method handles the creation of Loan Request Instrument based on rules.
		  *  These rules are based on Loan Creation Rules.
		  * @param inputDoc
		  * @param mediatorServices
		  * @return
		  * @throws RemoteException
		  * @throws AmsException
		  */
		 private DocumentHandler createLoanRequestByRules(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

			LOG.debug("PayableInvoiceGroupMediatorBean.createLoanRequestByRules()...."+inputDoc);

			Vector invoiceOidList = new Vector();
			Vector invoiceOidsVector = new Vector();
			String groupIds[] = null;
			String invoiceOid = null;
			DocumentHandler invoiceItem = null;
			DocumentHandler outputDoc =  new DocumentHandler();
			DocumentHandler  uploadInvoiceDoc = null;
			Vector tempOids = new Vector();
			int count = 0;
			boolean success = false;
			List<DocumentHandler> invoiceList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
			String userOid			  = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String loanType			  = inputDoc.getAttribute("/InvoiceGroupList/loan_type");
			String securityRights	  = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String clientBankOid	  = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
			String corpOrgOid 		  = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			String timeZone 		  = inputDoc.getAttribute("/InvoiceGroupList/timeZone");
			String currencyCode 	  = inputDoc.getAttribute("/InvoiceGroupList/baseCurrencyCode");
			
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREATE_LOAN_REQ)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.PayCreateLoanReq",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			
			//IR 22652 start- get correct allow payment date ind for LRQ
			CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
		        .createServerEJB("CorporateOrganization");
			corporateOrg.getData(Integer.parseInt(corpOrgOid));
			InstrumentProperties instProp = new InstrumentProperties();
			instProp.setUsePaymentDate(corporateOrg.getAttribute("use_payment_date_allowed_ind"));
			instProp.setCurrency(currencyCode);
			instProp.setUserOid(userOid);
			instProp.setClientBankOid(clientBankOid);
			instProp.setCorporateOrgOid(corpOrgOid);
			instProp.setMediatorServices(mediatorServices);
			instProp.setTimeZone(timeZone);
			instProp.setSecurityRights(securityRights);
			ClientServerDataBridge csdb = mediatorServices.getCSDB();
	        instProp.setUserLocale(csdb.getLocaleName());
	        instProp.setInstTypeCode(InstrumentType.LOAN_RQST);
			count = invoiceList.size();
			LOG.debug("InvoiceGroupMediatorBean: number of invoices selected : " + count);

			if (count <= 0)
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			groupIds = new String[count];
			// Loop through the list of invoice oids, and processing each one in the invoice list
		    for (int i = 0; i < count; i++)
		    {
			  invoiceItem = invoiceList.get(i);
			  LOG.debug(" value is :"+invoiceItem.getAttribute("/InvoiceData"));
			  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			  }
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  invoiceOidList.add(invoiceOid);
			  groupIds[i]=invoiceData[0];
			  tempOids.add(invoiceData[0]);
		    }
		    LOG.info("invoiceOidList->"+invoiceOidList);

		    //LoanType check for the selected invoices
			  InvoiceGroup invoiceGroup = null;
			 if(tempOids.size()>0){
				 for(int i=0;i<tempOids.size();i++){
				 invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
							Long.parseLong((String)tempOids.get(i)));
				String tpName = invoiceGroup.getDescription();
				 loanType = invoiceGroup.getAttribute("loan_type");
				LOG.debug("tpName:"+tpName+"\t loanType:"+loanType);
				if(StringFunction.isBlank(loanType)){
					 mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INST_LOAN_TYPE_MISSED_GROUP);
						return new DocumentHandler();
				 }
				}
			 }

		    List<DocumentHandler> invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
		    List<DocumentHandler> tempGroupDoc = invoicesGroupDoc;
		    LOG.info("invoicesGroupDoc->"+tempGroupDoc +"\t:"+invoicesGroupDoc);

		    if(tempGroupDoc!=null && tempGroupDoc.size()>0){
				 for(int pos = 0; pos<tempGroupDoc.size();pos++){
				 uploadInvoiceDoc = tempGroupDoc.get(pos);

			String linkedToInstType = uploadInvoiceDoc.getAttribute("/LINKED_TO_INSTRUMENT_TYPE");
			//if Instrument Type is blank or not LRQ type then do not create an instrument
			 if(StringFunction.isBlank(linkedToInstType) || !InstrumentType.LOAN_RQST.equals(linkedToInstType)){

		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.REC_INVOICE_LOAN_REQ_RULES_NOT_ALLOW);

		    	  tempGroupDoc.remove(pos);
					pos--;
			   }
	    	 }
		  }
		   if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){

			 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
					 uploadInvoiceDoc = invoicesGroupDoc.get(pos);

				String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
				String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
				//if already associated then do not create an instrument
				if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
					Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
					mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
							TradePortalConstants.INSTRUMENT_EXISTS,
							mediatorServices.getResourceManager().getText("UploadInvoices.LRQMessage", TradePortalConstants.TEXT_BUNDLE) +" - " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
					invoicesGroupDoc.remove(pos);
					pos--;
				}
			 }
		}
		    LOG.info("invoicesGroupDoc:"+invoicesGroupDoc);
		    if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0)
				return new DocumentHandler();
		   	 for ( DocumentHandler row : invoicesGroupDoc) {
					 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
					 invoiceOidsVector.addElement(invOid);
				 }
		
		    LOG.info("oid list:"+invoiceOidsVector);

			     try{
					   String creationDateTime = DateTimeUtility.getGMTDateTime(true, false);
					     groupInvoices(inputDoc,invoiceOidsVector,instProp);

						 LOG.debug("size:"+invOIds.size()+"\t:"+invOIds.toString());

						 if (invOIds.size() > 0){

							   String uploadInvOid = null;
							   Vector invoiceIDVector = null;
							   invoiceIDVector = new Vector(invOIds);
								LOG.debug("invoiceIDVector:"+invoiceIDVector);
								InvoicesSummaryData invoiceSummaryData = null;
								//ArMatchingRule matchingRule = null;
								for (int i = 0; i < invoiceIDVector.size(); i++) {
									uploadInvOid =  (String) invoiceIDVector.get(i);

									 invoiceSummaryData =
											(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
													Long.parseLong(uploadInvOid));
									 String tpName=invoiceSummaryData.getTpRuleName();
									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									int saveYN = invoiceSummaryData.save();
									LOG.debug("SaveYN :"+saveYN);
								}

							StringBuilder sql = new StringBuilder("select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
					                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
					                "from invoices_summary_data isd inner join instrument ins on isd.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
					                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
					                "and ");
						 	List<Object> sqlParamsLst = new ArrayList<>();
					        sql.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(new Vector(invOIds),"isd.upload_invoice_oid", sqlParamsLst));
					        
					        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParamsLst);
					        outputDoc.addComponent("/Out",resultSetDoc);

					        updateFileUpload(inputDoc,outputDoc, creationDateTime,"InvoiceGroupList",instProp);

							Vector newGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(new Vector(invOIds));

							updateAttachmentDetails(tempOids,newGroupOids, mediatorServices);

						success =true;
					  }else if(loanReqStatus && invOIds.size() == 0){
						 mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.AUTO_LOAN_REQ_BY_RULES_NO_ITEMS_FOR_LRC_RULE);
					  }

					} catch (AmsException e) {

						e.printStackTrace();
					}
				   invOIds = new TreeSet<String>();
					   if(success)
						   mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.AUTO_LOAN_REQ_RULE_CREATION_SUCCESS);
		   return outputDoc;

	      }


		 private void updateFileUpload(DocumentHandler inputDoc,
					DocumentHandler outputDoc, String creationDateTime, String xmlElement,InstrumentProperties instProp) throws RemoteException, AmsException {
				boolean success = true;

				InvoiceFileUpload invoiceUpload = (InvoiceFileUpload) instProp.getMediatorServices()
						.createServerEJB("InvoiceFileUpload");
				invoiceUpload.newObject();
				invoiceUpload.setAttribute("invoice_file_name", "Automated");
				invoiceUpload.setAttribute("creation_timestamp", creationDateTime);


				invoiceUpload.setAttribute("owner_org_oid",
						inputDoc.getAttribute("/"+xmlElement+"/ownerOrg"));
				invoiceUpload.setAttribute("user_oid",
						inputDoc.getAttribute("/"+xmlElement+"/userOid"));

				DocumentHandler resultsParamDoc = new DocumentHandler();
				DocumentHandler errorDoc = outputDoc.getFragment("/Error");
				if (errorDoc != null) {
					String maxErrorSeverity = errorDoc.getAttribute("maxerrorseverity");
					if ("5".equals(maxErrorSeverity)) {
						List<DocumentHandler> errVector = errorDoc.getFragmentsList("/errorlist/error");
						for (DocumentHandler errDoc : errVector) {
							
							String errMessage = errDoc.getAttribute("message");
							InvoiceUtility.insertToInvErrorLog("groupingErr- ",
									invoiceUpload
											.getAttribute("invoice_file_upload_oid"),
									errMessage, null);
							InvoiceUtility.insertToInvErrorLog("groupingErr- ",
									invoiceUpload
											.getAttribute("invoice_file_upload_oid"),
									"Error returned: " + errorDoc.toString(), "N");
						}
						success = false;
					}
				}

				if (success) {
					resultsParamDoc.addComponent("/", outputDoc.getFragment("/Out"));
					List<DocumentHandler> v = resultsParamDoc.getFragmentsList("/ResultSetRecord");
					String errorCode = "";
					if (errorDoc != null) {
						String maxSeverity = errorDoc.getAttribute("/maxerrorseverity");
						if ("1".equals(maxSeverity)) {
							List<DocumentHandler> errVector = errorDoc
									.getFragmentsList("/errorlist/error");
							for (DocumentHandler errDoc : errVector) {
								
								if ("I182".equals(errDoc.getAttribute("/code"))) {
									errorCode = errDoc.getAttribute("/code");
								} else if ("I477".equals(errDoc.getAttribute("/code"))) {
									errorCode = errDoc.getAttribute("/code");
								}
							}
						}
					}
					if (v.size() > 0) {
						String resultParams = resultsParamDoc.toString();
						invoiceUpload.setAttribute("inv_result_parameters",
								resultParams);
					} else if (StringFunction.isBlank(errorCode)) {
						IssuedError ie = ErrorManager.findErrorCode(
								TradePortalConstants.ERROR_GROUPING_INVS, instProp.getUserLocale());
						String errMsg = ie.getErrorText();
						InvoiceUtility.insertToInvErrorLog("groupingErr- ",
								invoiceUpload.getAttribute("invoice_file_upload_oid"),
								errMsg, null);
						InvoiceUtility.insertToInvErrorLog("groupingErr- ",
								invoiceUpload.getAttribute("invoice_file_upload_oid"),
								"No Output returned: " + errorDoc!=null? errorDoc.toString():"", "N");
					} else {
						IssuedError ie = ErrorManager.findErrorCode(errorCode,
								instProp.getUserLocale());
						if (ie != null) {
							String errMsg = ie.getErrorText();
							InvoiceUtility.insertToInvErrorLog("groupingErr- ",
									invoiceUpload
											.getAttribute("invoice_file_upload_oid"),
									errMsg, null);
							InvoiceUtility.insertToInvErrorLog("groupingErr- ",
									invoiceUpload
											.getAttribute("invoice_file_upload_oid"),
									"No Output returned: " +errorDoc!=null? errorDoc.toString():"", "N");
						}
					}
					invoiceUpload.setAttribute("validation_status",
							TradePortalConstants.PYMT_UPLOAD_VALIDATION_SUCCESSFUL);

				}else{
					invoiceUpload.setAttribute("validation_status",
							TradePortalConstants.PYMT_UPLOAD_VALIDATION_FAILED);

				}
				invoiceUpload.setAttribute("completion_timestamp", DateTimeUtility.getGMTDateTime(true, false));

			    //save the active_po_upload record
				invoiceUpload.save(false); //false means don't validate just save

			}

		 /**
		  * This method groups the invoices based on loan request rules.
		  * @param inputDoc
		  * @param invoiceOidList
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler groupInvoices(DocumentHandler inputDoc, Vector invoiceOidList,InstrumentProperties instProp) throws AmsException, RemoteException {
			 LOG.debug("groupInvoices()....");

			 Vector loanReqRulesList = null;
			 DocumentHandler loanRequestDoc = null;
			 Vector invoiceItemList = null;
			 int count =0;
			 StringBuilder invoiceOidBuf = new StringBuilder();
			 for (Object anInvoiceOidList : invoiceOidList) {
				 invoiceOidBuf.append(anInvoiceOidList + ",");
			 }
			 invoiceOidBuf.delete(invoiceOidBuf.toString().length()-1,invoiceOidBuf.toString().length());
			 SELECT_SQL = "select name,inv_only_create_rule_desc,inv_only_create_rule_template,inv_only_create_rule_max_amt,due_or_pay_max_days,due_or_pay_min_days,a_op_bank_org_oid, " +
		 		"pregenerated_sql " +
		 		EXTRA_SELECT_SQL +
		 		"  from inv_only_create_rule where a_invoice_def in (select a_upload_definition_oid from invoices_summary_data  " +
		 		" where upload_invoice_oid in ("+invoiceOidBuf.toString()+")) and a_owner_org_oid in ( ";

			 LOG.debug(" SELECT SQL After OIDS:"+SELECT_SQL);

			 loanReqRulesList = getLoanRequestRules(instProp.getCorporateOrgOid(),instProp);
			 LOG.debug("loanReqRulesList:"+loanReqRulesList);
		     //Get the distinct loan types for the list of invoicesOids and apply rules on those list
		     String loanTypeSQL = " select distinct loan_type loantype from invoices_summary_data where upload_invoice_oid in (";
		     Vector loanList = InvoiceUtility.getDistinctLoanTypes(loanTypeSQL,invoiceOidBuf.toString(),instProp.getCorporateOrgOid());
		     String loanType = "";
		     LOG.debug("loanList:"+loanList);

		     // create loan requests
		     if((loanList!=null && loanList.size()>0) && InstrumentType.LOAN_RQST.equals(instProp.getInstTypeCode())){

		    	 for(int cnt =0;cnt<loanList.size();cnt++){

			    	 loanType = (String)loanList.get(cnt);

			    	 for (int i = 0; i < loanReqRulesList.size(); i++) {
					      // Get the Loan Request rule and pull some values off it.
				    	 loanRequestDoc = (DocumentHandler) loanReqRulesList.elementAt(i);
						 LOG.debug("loanRequestDoc:"+loanRequestDoc);

						 // Get the getInvoiceItems for this loan request rules
				    	 invoiceItemList = getInvoiceItems(invoiceOidBuf.toString(),loanRequestDoc,loanType,instProp);
						  LOG.info("count()"+count +"\t invoiceItemList:"+invoiceItemList);
					      if (invoiceItemList.size() < 1) {
					    	  LOG.debug("No rules InvoiceItems available...");
					      } else {
 			 			    	  chunkInvoices(invoiceItemList, loanRequestDoc,instProp);
 			 			    	  count++;
					      }
			    	 }
		    	 }
		     }else if(InstrumentType.APPROVAL_TO_PAY.equals(instProp.getInstTypeCode())){

		    	 LOG.debug("APProval to Pay avaialble");
				 for (int i = 0; i < loanReqRulesList.size(); i++) {
				      // Get the ATP Request rule and pull some values off it.
			    	 loanRequestDoc = (DocumentHandler) loanReqRulesList.elementAt(i);
				      // Get the getInvoiceItems for this loan request rules
			    	 invoiceItemList = getInvoiceItems(invoiceOidBuf.toString(),loanRequestDoc,loanType,instProp);
				      if (invoiceItemList.size() < 1) {
				    	  LOG.debug("No rules InvoiceItems available...");
				      } else {

				    	  // Now start chunking to Invoice line items, and mark invoices that will be processed by this rule

				    	  for (int j=0;j < invoiceItemList.size();j++) {

				    		  DocumentHandler doc = (DocumentHandler)invoiceItemList.get(j);
				    		  String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
				    		  processedByRule.add(invoiceOid);
				    	  }

				    	  chunkInvoices(invoiceItemList, loanRequestDoc,instProp);
 			    	  	  count++;
				      }
			    }
			 }
		     if(count !=0){
		     logUnassignedInvoiceCount(invoiceOidBuf.toString(),instProp);
		     }

		     LOG.info("count:"+count +"\t size:"+invoiceItemList);
				return loanRequestDoc;


		 }
	private void logUnassignedInvoiceCount(String invoiceOids,InstrumentProperties instProp)
			throws AmsException {
		int count = 0;
		List<Object> sqlParamsLst = new ArrayList<>();
		String whereClause = " a_transaction_oid is NULL and a_corp_org_oid = ? ";
		sqlParamsLst.add(instProp.getCorporateOrgOid());

		// If we're processing for a specific upload sequence, print a message
		// for how many items are unassigned from that sequence.
		if (StringFunction.isNotBlank(invoiceOids)) {
			StringBuilder whereBySequenceSql = new StringBuilder(whereClause);
			String[] ids = invoiceOids.split(",");
            String placeHolders ="";
            for (int i = 0; i < ids.length; i++) {
            	placeHolders = placeHolders+"?";
            	sqlParamsLst.add(ids[i]);
                   if (i != (ids.length - 1)) {
                	   placeHolders = placeHolders+", ";
                   }
            }
			
			whereBySequenceSql.append(" and upload_invoice_oid in ("+ placeHolders + ")");

			count = DatabaseQueryBean.getCount("upload_invoice_oid","INVOICES_SUMMARY_DATA", whereBySequenceSql.toString(), false, sqlParamsLst);
			if (count > 0) {
				instProp.getMediatorServices().getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_MATCH_RULE_INV,
						String.valueOf(count));
			}
		}
	}
		 /**
		  * This method processes the invoices which are already
		  * passed through the rules and groups invoices for creating
		  * loan request instrument
		  * @param invoiceItemList
		  * @param creationRuleDoc
		  * @throws RemoteException
		  * @throws AmsException
		  */
		 private void chunkInvoices(Vector invoiceItemList, DocumentHandler creationRuleDoc, InstrumentProperties instProp)
			      throws RemoteException, AmsException {

			    LOG.debug("inside chunkInvoices()....invoiceItemList "+invoiceItemList);
			    BigDecimal invoiceAmount;
			    BigDecimal maxAmount = null;
			    BigDecimal convertedMaxAmount = null;
			    BigDecimal totalAmount = BigDecimal.ZERO;

			    int itemCount = 0;
			    String lastBenName = "";
			    String lastCurrency = "";
			    String lastPayMethod = "";
			    String latestDuepaymentDt = null;
			    String lastUploadInvoiceOid = "";
			    String benName = "";
			    String currency = "";
			    String payMethod ="";
			    String duePaymentDt;
			    String loanType = "";
			    String uploadInvoiceOid = "";
			    String loanReqRuleName = creationRuleDoc.getAttribute("/NAME");
				LOG.debug("loanReqRuleName:"+loanReqRuleName);
			    Vector invoiceOids = null;
			    DocumentHandler invoiceItemDoc = null;

			    CorporateOrganization corporateOrg = (CorporateOrganization) instProp.getMediatorServices()
			        .createServerEJB("CorporateOrganization");
			    try {
			    	corporateOrg.getData(Integer.parseInt(instProp.getCorporateOrgOid()));
			    } catch (InvalidObjectIdentifierException e) {
			    	instProp.getMediatorServices().getErrorManager().issueError(
			          TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORG_NOT_EXIST);
			    }
			    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
			    ClientBank clientBank = (ClientBank) instProp.getMediatorServices().createServerEJB("ClientBank");
			    try {
			      clientBank.getData(clientBankOid);
			    } catch (InvalidObjectIdentifierException e) {
			    	 instProp.getMediatorServices().getErrorManager().issueError(
			          TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CLIENT_BANK_ORG);
			    }

			    // Determine the number of Invoices allowed per instrument
			    //int maximumInvoicesInInstr = MAX_ITEMS_ON_INVOICE;
			    String value = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_MAX_AMT");

			    if (StringFunction.isNotBlank(value)) {
			      maxAmount = new BigDecimal(value);
			    } else {
			      maxAmount = null;

			    }
			    LOG.info("invoiceItemList->"+invoiceItemList);
				Vector invOids = new Vector();

			    for (int x = 0; x < invoiceItemList.size(); x++) {
					invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
					invOids.add(invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID"));

			    }
				LOG.info("invOids:"+invOids);
				List<Object> sqlParamsLst = new ArrayList<>();
			    //This is to check if amount to validated against rules if invoice rules found>1
			    StringBuilder amtSQL = new StringBuilder("select count(distinct (a_upload_definition_oid)) a_upload_definition_oid from " +
			    		"invoices_summarY_data where ");
			    amtSQL.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids,"upload_invoice_oid", sqlParamsLst));
		        
		        DocumentHandler amoutDoc = DatabaseQueryBean.getXmlResultSet(amtSQL.toString(), false, sqlParamsLst);
		        List<DocumentHandler> resultsVector = amoutDoc.getFragmentsList("/ResultSetRecord/");
		        if( resultsVector.get(0) != null )
		        {
		          DocumentHandler loanDoc = resultsVector.get(0);

		          String amountCnt = loanDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
		          LOG.debug("amountCnt:"+amountCnt);
		          if(Integer.parseInt(amountCnt)>1){
		        	  maxAmount = null;
		          }
		        }
			    Vector processedInvoiceOids = new Vector();
			    for (int x = 0; x < invoiceItemList.size(); x++) {
			      // We will use this variable to count the number of Invoice line items fully processed, i.e. not skipped.

			      invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(x);
				  LOG.debug("invoiceItemDoc:"+invoiceItemDoc);
			      // Pull off values from the document that drive the processing.
				  invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			      benName = invoiceItemDoc.getAttribute("/SELLER_NAME");
			      currency = invoiceItemDoc.getAttribute("/CURRENCY");
			      uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");
			      duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE");
			      payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD");
			      loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE");
				  LOG.debug("loanType:"+loanType+"\t invoiceAmount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadINv oid:"+uploadInvoiceOid+"\t duePaymentDt:"+duePaymentDt);
			      invoiceOids = new Vector();

			        lastBenName = benName;
			        lastCurrency = currency;
			        lastPayMethod = payMethod;
			        latestDuepaymentDt = duePaymentDt;
			        lastUploadInvoiceOid = uploadInvoiceOid;
			        // Add the oid to the Invoice oid vector.
			        invoiceOids.add(uploadInvoiceOid);
					LOG.debug("first adding into invoiceOids:"+invoiceOids+"\t lastUploadInvoiceOid:"+lastUploadInvoiceOid);
			        convertedMaxAmount = getConvertedMaxAmount(maxAmount,instProp.getCurrency(), currency, creationRuleDoc,instProp);

			        itemCount = 1;
			        totalAmount = invoiceAmount;
					LOG.debug("totalAmount:"+totalAmount+"\t convertedMaxAmount:"+convertedMaxAmount);
			        if ((convertedMaxAmount == null ||
			        		totalAmount.compareTo(convertedMaxAmount) <= 0)) {
			            // Find Invoices that can be added to this group
			            int subIdx = x + 1;

			            while (subIdx < invoiceItemList.size()) {
			            	invoiceItemDoc = (DocumentHandler) invoiceItemList.elementAt(subIdx);

			                // Pull off values from the document that drive the processing.
			                invoiceAmount = invoiceItemDoc.getAttributeDecimal("/AMOUNT");
			                benName = invoiceItemDoc.getAttribute("/SELLER_NAME");
			                currency = invoiceItemDoc.getAttribute("/CURRENCY");
			                uploadInvoiceOid = invoiceItemDoc.getAttribute("/UPLOAD_INVOICE_OID");
			                duePaymentDt = invoiceItemDoc.getAttribute("/INVDATE");
			                payMethod = invoiceItemDoc.getAttribute("/PAY_METHOD") == null ? "" : invoiceItemDoc.getAttribute("/PAY_METHOD");
			                loanType = invoiceItemDoc.getAttribute("/LOAN_TYPE");
							LOG.debug(" loanType:"+loanType+"\t -> poamount:"+invoiceAmount+"\t benName:"+benName+"\t currency:"+currency+"\t uploadInvoiceOid:"+uploadInvoiceOid+"\t duePaymentDt :"+duePaymentDt);

						    if(TradePortalConstants.TRADE_LOAN.equals(loanType)){
						    	if (StringFunction.isNotBlank(payMethod)){
					                if (!currency.equals(lastCurrency) || !payMethod.equals(lastPayMethod))
					                {
					                	// None of the remaining INVs can be added to this group
					                    break;
					                }

						    	}else{
					                if (!currency.equals(lastCurrency))
					                {
					                	// None of the remaining INVs can be added to this group
					                    break;
					                }
						    	}
							 } else {
				                if (!benName.equals(lastBenName) ||
				                		!currency.equals(lastCurrency) ||
				                		!duePaymentDt.equals(latestDuepaymentDt))
				                {
				                	// None of the remaining Invoices can be added to this group
				                    break;
				                }
			            	}
						    LOG.debug("invoiceAmount:"+invoiceAmount);
			                BigDecimal newTotal = totalAmount.add(invoiceAmount);
							LOG.debug("newTotal:"+newTotal +"\t convertedMaxAmount:"+convertedMaxAmount);
			                if (convertedMaxAmount == null ||
			                		newTotal.compareTo(convertedMaxAmount) <= 0) {
			                	// Add this Invoice to the group
			                	invoiceOids.add(uploadInvoiceOid);
								LOG.debug("adding into poOids:"+invoiceOids+"\t uploadInvoiceOid:"+uploadInvoiceOid);
								invoiceItemList.remove(subIdx);
			                    itemCount++;
			                    totalAmount = newTotal;

			                	if (duePaymentDt != null) {
			                		if (latestDuepaymentDt == null) {
			                			latestDuepaymentDt = duePaymentDt;
			                		}
			                		else if (duePaymentDt.compareTo(latestDuepaymentDt) > 0) {
			                			latestDuepaymentDt = duePaymentDt;
			                		}
			                	}
			                }
			                else {
			                	// This Invoice would bring the total above the max
			                	subIdx++;
			                }
			            }
			        }
					LOG.debug(" before instCreteDoc:"+loanReqStatus);
			         if(loanReqStatus){
				        Map impFinBuyerBackMap = InvoiceUtility.getFinanceType(loanType, TradePortalConstants.INVOICE_TYPE_PAY_MGMT );

				        if (StringFunction.isNotBlank(lastPayMethod) &&
				        		TradePortalConstants.TRADE_LOAN.equals(loanType) &&
				        		TradePortalConstants.TRADE_LOAN_PAY.equals((String)impFinBuyerBackMap.get("financeType"))){
				        	if (!processedInvoiceOids.contains(invoiceOids.get(0))){


					        	String country = InvoiceUtility.fetchOpOrgCountryForOpOrg(creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));

					        	boolean isPaymentMethodValid = InvoiceUtility.validatePaymentMethodForInvoices(invoiceOids, country, lastCurrency, instProp.getMediatorServices().getErrorManager(),false);
					        	boolean isBankBranchCodeValid = InvoiceUtility.validateBankInformationForInvoices(invoiceOids, country, lastCurrency, instProp.getMediatorServices().getErrorManager(),false);
					        	if (!isPaymentMethodValid || !isBankBranchCodeValid){
					        		processedInvoiceOids.addAll(invoiceOids);
					        		continue;
					        	}
				        	}
				        }

			        // Build the input document used to create a new instrument
			        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
			        		lastBenName, lastCurrency, totalAmount, latestDuepaymentDt, invoiceOids,instProp);
				    instrCreateDoc.setAttribute("/importIndicator", (String)impFinBuyerBackMap.get("importIndicator"));
				    instrCreateDoc.setAttribute("/financeType", (String)impFinBuyerBackMap.get("financeType"));
				    instrCreateDoc.setAttribute("/buyerBacked", (String)impFinBuyerBackMap.get("buyerBacked"));
				   //IR 22917 - no bene name for EXP_FIN and SEL_REQ
				    if(TradePortalConstants.EXPORT_FIN.equals(loanType)
							|| TradePortalConstants.SELLER_REQUESTED_FINANCING.equals((String)impFinBuyerBackMap.get("financeType")))
				    	instrCreateDoc.setAttribute("/benName", "");
			        // Create instrument using the document, assign the invoice items, and create log records.
			        try {
			        	createLRQInstrumentFromInvoiceData(instrCreateDoc, invoiceOids, loanReqRuleName,instProp);
			        	invOIds.addAll(invoiceOids);

			        }
			        catch (AmsException e) {
			        	// This is probably a sql error but could be anything.
			        	// Post this exception to the log.
			        	LOG.info(instProp.getCorporateOrgOid()+"\t"+ e.getMessage());
			        	e.printStackTrace();
			        }
			    }
			}
		 }

		 /**
		  * This method prepares the doc object required for creating an Instrument
		  * with appropriate attributes for Loan Req Instrument.
		  * @param creationRuleDoc
		  * @param benName
		  * @param currency
		  * @param amount
		  * @param latestShipDt
		  * @param invoiceOids
		  * @return
		  */
		 private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
			      String benName, String currency, BigDecimal amount,
			      String latestShipDt, Vector invoiceOids, InstrumentProperties instProp) {

			 LOG.debug("buildInstrumentCreateDoc () creationRuleDoc->"+creationRuleDoc);
			 LOG.debug("benName:"+benName+"\t currency:"+currency+"\t amount:"+amount+"\t latestShipDt:"+latestShipDt+"\t invoiceOids:"+invoiceOids);
			   DocumentHandler instrCreateDoc = new DocumentHandler();
				String secretKeyString = instProp.getMediatorServices().getCSDB().getCSDBValue("SecretKey");
				byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
				javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
				String templateOid = creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE");
				String encryptedString = null;
				if (StringFunction.isNotBlank(templateOid)){
					encryptedString = EncryptDecrypt.encryptStringUsingTripleDes(templateOid, secretKey);
				}
			    instrCreateDoc.setAttribute("/instrumentOid", encryptedString);
			    instrCreateDoc.setAttribute("/clientBankOid", instProp.getClientBankOid());
			    instrCreateDoc.setAttribute("/instrumentType",instProp.getInstTypeCode());
			    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc.getAttribute("/A_OP_BANK_ORG_OID"));
				//Srinivasu_D IR#T36000018442 Rel8.2 06/29/2013 - TEMPL if template being used else blank
			    if(StringFunction.isBlank(templateOid)){
			    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_BLANK);
				}else {
					instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
				}
			    instrCreateDoc.setAttribute("/userOid", instProp.getUserOid());
			    instrCreateDoc.setAttribute("/ownerOrg", instProp.getCorporateOrgOid());
			    instrCreateDoc.setAttribute("/securityRights", instProp.getSecurityRights());
			    instrCreateDoc.setAttribute("/benName", benName);
			    instrCreateDoc.setAttribute("/currency", currency);
			    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
			    instrCreateDoc.setAttributeDecimal("/amount", amount);
			    instrCreateDoc.setAttribute("/uploadIndicator", TradePortalConstants.INDICATOR_YES);
			    if (StringFunction.isBlank(instProp.getUserLocale())) {
			    	instProp.setUserLocale("en_US");
			    }
			    instrCreateDoc.setAttribute("/locale", instProp.getUserLocale());

			    for (int i = 0; i < invoiceOids.size(); i++) {
			      instrCreateDoc.setAttribute("/InvoiceLines(" + i + ")/uploadInvoiceOid",
			          (String) invoiceOids.elementAt(i));
			    }

			    instProp.getMediatorServices().debug("Creation rules document is" + instrCreateDoc.toString());
			    return instrCreateDoc;
			  }

		 /**
		  * This method invokes the Instrument EJB and writes the invoice
		  * details in INVOICE_HISTORY table.
		  * @param inputDoc
		  * @param invoiceOids
		  * @param lcRuleName
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private DocumentHandler createLRQInstrumentFromInvoiceData(DocumentHandler inputDoc,
			      Vector invoiceOids, String lcRuleName,InstrumentProperties instProp)
			      throws AmsException, RemoteException {

			    LOG.debug("Entered createLRQInstrumentFromInvoiceData(). inputDoc follows ->\n"  + inputDoc.toString());
			    MediatorServices mediatorServices = instProp.getMediatorServices();
			    DocumentHandler outputDoc = null;
			    String instrumentType = instProp.getInstTypeCode();
			    LOG.debug("instrumentType:"+instrumentType);
			    //Create a new instrument
			    Instrument newInstr = (Instrument) mediatorServices.createServerEJB("Instrument");
			    outputDoc = newInstr.createLoanReqForInvoiceData(inputDoc, invoiceOids, lcRuleName);
			    LOG.debug("Returned from instrument.createInstrumentFromPOData()\n"+ "Returned document follows -> " + outputDoc.toString());
			    try{
			    // Now that the transaction has been created, assign this transaction
			    // oid to all the line items on the transaction.
				LOG.debug("invoiceOids:"+invoiceOids+"\t outputDoc:"+outputDoc);
				Transaction transaction = (Transaction) mediatorServices.createServerEJB("Transaction");
					String transOid = outputDoc.getAttribute("/transactionOid");
					if (transOid != null){
				long transactionOid = outputDoc.getAttributeLong("/transactionOid");
				transaction.getData(transactionOid);
				Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
				String termsOid = terms.getAttribute("terms_oid");


			        POLineItemUtility.updateInvoiceTransAssignment(invoiceOids, outputDoc.getAttribute("/transactionOid"), newInstr.getAttribute("instrument_oid"),
			             TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED, termsOid);

			        POLineItemUtility.createInvoiceHistoryLog (invoiceOids,TradePortalConstants.INVOICE_ACTION_AUTOCREATE,
			        		TradePortalConstants.RECEIVABLE_UPLOAD_INV_STATUS_ASSIGNED,instProp.getUserOid());
					String requiredOid = "upload_invoice_oid";
					 String invoiceOidsSql = POLineItemUtility.getPOLineItemOidsSql(
							 invoiceOids, requiredOid);

					if (TradePortalConstants.INDICATOR_T.equals(newInstr.getAttribute("import_indicator")) &&
							TradePortalConstants.TRADE_LOAN_PAY.equals(terms.getAttribute("finance_type"))){
						StringBuilder payMethodSQL = new StringBuilder("select distinct(PAY_METHOD) PAYMETHOD from invoices_summary_data where ");
						payMethodSQL.append(invoiceOidsSql);
						DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(payMethodSQL.toString(), false, new ArrayList<Object>());
						if (resultDoc != null){
							String payMethod = resultDoc.getAttribute("/ResultSetRecord(0)/PAYMETHOD");
							if (StringFunction.isNotBlank(payMethod)){
								terms.setAttribute("payment_method",payMethod);
								int singleBeneCount = InvoiceUtility.isSingleBeneficairyInvoice(invoiceOidsSql);
								if (singleBeneCount == 1){
									terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_BEN_ACCT);
									InvoiceUtility.populateTerms(terms,(String)invoiceOids.get(0),invoiceOids.size(), mediatorServices);
								}else if (singleBeneCount > 1){
									terms.setAttribute("loan_proceeds_credit_type",TradePortalConstants.CREDIT_MULTI_BEN_ACCT);
								}
								transaction.save(false);
							}

						}
					}
					if (TradePortalConstants.INDICATOR_NO.equals(newInstr.getAttribute("import_indicator")) ||
							TradePortalConstants.INDICATOR_X.equals(newInstr.getAttribute("import_indicator"))){

						 boolean useSeller = TradePortalConstants.BUYER_REQUESTED_FINANCING.equals(terms.getAttribute("finance_type")) ;
						String invoiceDueDate = null;
						if (InvoiceUtility.validateSameCurrency(invoiceOidsSql) &&
								InvoiceUtility.validateSameDate(invoiceOidsSql) &&
								InvoiceUtility.validateSameTradingPartner(invoiceOidsSql, useSeller)){
							StringBuilder dueDateSQL = new StringBuilder("select distinct(case when payment_date is null then due_date else payment_date end ) DUEDATE " +
									" from invoices_summary_data where ");
							dueDateSQL.append(invoiceOidsSql);
							DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(dueDateSQL.toString(), false, new ArrayList<Object>());
							invoiceDueDate = resultDoc.getAttribute("/ResultSetRecord(0)/DUEDATE");
						}


						if (StringFunction.isNotBlank(invoiceDueDate)){

							try {
								SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
								SimpleDateFormat jPylon = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

									Date date = jPylon.parse(invoiceDueDate);

									terms.setAttribute("loan_terms_fixed_maturity_dt",TPDateTimeUtility.convertISODateToJPylonDate(iso.format(date)));
									transaction.save(false);
							} catch (Exception e) {
								e.printStackTrace();
							}


						}
					}

					}else{
				       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
				                 TradePortalConstants.ERROR_CREATING_ISTRUMENT);

					}
			    } catch (AmsException e){
			        String [] substitutionValues = {"",""};
			        substitutionValues[0] = e.getMessage();
			        substitutionValues[1] = "0";
			         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			                 AmsConstants.SQL_ERROR, substitutionValues);
			         e.printStackTrace();
			    }


			    return outputDoc;
			  }

		 /**
		  * This method returns the set of invoices which pass through the
		  * rules defined for those invoice/invoices
		  * @param invoiceOids
		  * @param creationRuleDoc
		  * @return
		  * @throws AmsException
		  * @throws RemoteException
		  */
		 private Vector getInvoiceItems(String invoiceOids,DocumentHandler creationRuleDoc,String loanType, InstrumentProperties instProp) throws AmsException {
			 	LOG.debug("getInvoiceItems()....invoiceOids ->"+invoiceOids +"\t:"+loanType);
			 	String date = "";
			 	List<Object> sqlParmsLst = new ArrayList();

			 	if(TradePortalConstants.INDICATOR_YES.equals(instProp.getUsePaymentDate()))
			 		date = " cast (case when payment_date is null then due_date else payment_date end as date ) invdate ";
			 	else
			 		date = " due_date invdate ";

			 	String INVOICE_SELECT_SQL = "select upload_invoice_oid,invoice_id,seller_name,loan_type," +
				"currency,amount,p_invoice_file_upload_oid,issue_date,"+date+" , pay_method from invoices_view " +
				"where nvl(deleted_ind,'N')!='Y' and a_transaction_oid is NULL and upload_invoice_oid in ("+invoiceOids+") and a_corp_org_oid = ";

			   StringBuilder invoiceSql = new StringBuilder(INVOICE_SELECT_SQL);
			   
			   invoiceSql.append("?");
			   sqlParmsLst.add(instProp.getCorporateOrgOid());
			   if(StringFunction.isNotBlank(loanType)){
				   invoiceSql.append(" and loan_type =? ");
				   sqlParmsLst.add(loanType);
			   }
			   // Add condition for last_ship_dt if both min and max are set;
			   String minDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MIN_DAYS");
			   String maxDays = creationRuleDoc.getAttribute("/DUE_OR_PAY_MAX_DAYS");
			   LOG.debug("mindays:"+minDays+"\t max days:"+maxDays);
			   if (StringFunction.isNotBlank(minDays)  && StringFunction.isNotBlank(maxDays)) {

			     // First get the current local date
			     String currentGmtDate = DateTimeUtility.getGMTDateTime();
			     Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(currentGmtDate, instProp.getTimeZone());
			     SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
			     String today = formatter.format(currentLocalDate);
			     LOG.debug("today:"+today);
			     if(TradePortalConstants.INDICATOR_YES.equals(instProp.getUsePaymentDate()))
			    	 invoiceSql.append(" and case when payment_date is null then due_date else payment_date end  between TO_DATE(");
			     else
			    	 invoiceSql.append(" and due_date between TO_DATE(");

			     invoiceSql.append("?)? and  TO_DATE(?)?");
			     sqlParmsLst.add(today);
			     sqlParmsLst.add(minDays);
			     sqlParmsLst.add(today);
			     sqlParmsLst.add(maxDays);
			   }

			   // Add condition for the remaining criteria from the lc creation rule
			   String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");

			   if (StringFunction.isNotBlank(criteria)) {
				   invoiceSql.append(" and ");
				   invoiceSql.append(criteria);
			   }

			   // Finally add the orderby clause.
			   invoiceSql.append(" ");
				  if (TradePortalConstants.TRADE_LOAN.equals(loanType)){
					  StringBuilder tempOrderBy = new StringBuilder(" order by currency, pay_method, a_upload_definition_oid, amount desc ");
					  invoiceSql.append(tempOrderBy);
				  }else{
					  invoiceSql.append(INVOICE_ORDERBY);
				  }

			   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(invoiceSql.toString(), true, sqlParmsLst);

			   if (resultSet == null)
				   resultSet = new DocumentHandler();

			   instProp.getMediatorServices().debug("invoice list is " + resultSet.toString());
  			   return resultSet.getFragments("/ResultSetRecord");

	  }

		 /**
		  * This method returns the Loan Request rules
		  * @param corporateOrgOid
		  * @return
		  * @throws AmsException
		  */
		private Vector getLoanRequestRules(String corporateOrgOid,InstrumentProperties instProp) throws AmsException {

			 LOG.debug("getCreationRules() corporateOrgOid:"+corporateOrgOid);
			    StringBuilder loanReqSQL = new StringBuilder(SELECT_SQL);
			    loanReqSQL.append("? ");
			    List<Object> sqlParamsLst = new ArrayList();
			    sqlParamsLst.add(corporateOrgOid);
			    
			    // Loop back through all parent orgs for oids to include in search for LoanRequest Creation rules
			    do {
			      String parentCorpSql = "select p_parent_corp_org_oid from corporate_org where organization_oid = ?"
			    	  					+" AND activation_status = ?";
			      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql, false, corporateOrgOid, TradePortalConstants.ACTIVE);
			      if( parentCorpOrg != null )
			      {
			        List<DocumentHandler> resultsVector = parentCorpOrg.getFragmentsList("/ResultSetRecord/");
			        if( resultsVector.get(0) != null )
			        {
			          DocumentHandler parentOid = resultsVector.get(0);
			          String parentCorpOid = "";
			          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
			          if (StringFunction.isNotBlank(parentCorpOid)) {
			        	  loanReqSQL.append(",?");
			        	  sqlParamsLst.add(parentCorpOid);
			              corporateOrgOid = parentCorpOid;
			          } else {  // No more parents, so break out of loop
			            break;
			          }
			        } else {  // No more parents, so break out of loop
			          break;
			        }
			      } else { // no more parents, so break out of loop
			        break;
			      }
			    } while (true);

			    loanReqSQL.append(") ");
			    if(InstrumentType.APPROVAL_TO_PAY.equals(instProp.getInstTypeCode())){
			    	loanReqSQL.append(" and instrument_type_code = ? ");
			    	sqlParamsLst.add(InstrumentType.APPROVAL_TO_PAY);
			    }
			    else if(InstrumentType.LOAN_RQST.equals(instProp.getInstTypeCode())){
			    	loanReqSQL.append(" and instrument_type_code = ? ");
			    	sqlParamsLst.add(InstrumentType.LOAN_RQST);
			    }

			    loanReqSQL.append(ORDERBY_CLAUSE);

			    LOG.debug("LoanRequest SQL is" + loanReqSQL.toString());
			    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(loanReqSQL.toString(), false, sqlParamsLst);

			    if (resultSet == null)
			      resultSet = new DocumentHandler();

			    LOG.debug("LoanRequest rule list doc is " + resultSet.toString());

			    return resultSet.getFragments("/ResultSetRecord");

			  }

		 /**
		  * This method converts the given amount based on currency
		  * @param fromAmount
		  * @param fromCurrency
		  * @param toCurrency
		  * @param creationRuleDoc
		  * @return
		  */
		 private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
			      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc,InstrumentProperties instProp) {
			    LOG.debug("getConvertedMaxAmount()..loanReqStatus ->"+loanReqStatus);
			    BigDecimal convertedAmount = null;
				String param = (InstrumentType.LOAN_RQST.equals(instProp.getInstTypeCode())) ? TradePortalConstants.FX_LOAN_REQUEST : TradePortalConstants.FX_APPROVAL_TO_PAY;
			    try {

			      if ((fromAmount == null) ||
			    		  fromAmount.compareTo(BigDecimal.ZERO) == 0) {
			        return null;
			      }

			      // No need to convert if the from and to currencies are the same
			      if (fromCurrency!= null && fromCurrency.equals(toCurrency)) {
			        return fromAmount;
			      }

			      String templateCurrency = null;
			      
			      String multiplyIndicator = null;
			      BigDecimal rate = null;
			      DocumentHandler fxRateDoc = null;

			      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

			      // Test whether the po line item currency code is a valid Trade
			      // Portal currency.
			      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
			    		  instProp.getUserLocale())) {
			        // Get the fx rate info for the po line item currency
			        fxRateDoc = getFxRateDoc(instProp.getCorporateOrgOid(), toCurrency);
			        if (fxRateDoc == null) {
			          // No fx rate found, isse warning and return a blank amount.
			          // This indicates that maxAmount is not used for grouping.
			          // Give error with the po line item currency.
			        	LOG.debug(" 1/1");
			        	instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
			             // getLogSequence(),
			              TradePortalConstants.LOANREQ_LOG_NO_FX_RATE, toCurrency,param);
			        	  loanReqStatus = false;
						  //break;
			          return null;
			        } else {
			          // Found an fx rate for the template currency.  Use it.
			        }
			      } else {
			        // Currency code for the po line item is not valid.  Use
			        // the currency code from the instrument creation rule's template.
			    	
			        String sql = "select copy_of_currency_code from template, transaction " 
			                   +  "where p_instrument_oid = c_template_instr_oid and template_oid = ? ";
			        DocumentHandler templateDoc = null;
			        try {
			          templateDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{creationRuleDoc.getAttribute("/INV_ONLY_CREATE_RULE_TEMPLATE")});
			        } catch (AmsException e) {
			          templateDoc = null;
			        }

			        if (templateDoc == null) {
			          // Can't get the currency code off the template.  Issue a warning and return
			          // a null amount.  This indicates NOT to use maxAmount for grouping.
			        	LOG.debug(" 1/2");
			        	instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
			              TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
			              creationRuleDoc.getAttribute("/NAME"));
			        	  loanReqStatus = false;
			          return null;
			        } else {
			          // We got the template currency but it may be blank.  We're only
			          // concerned with the first record (there should not be more).
			          templateCurrency = templateDoc
			              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
			          if (StringFunction.isBlank(templateCurrency)) {
			            // Can't get the currency code off the template.  Issue a warning and return
			            // a null amount.  This indicates NOT to use maxAmount for grouping.
							instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
			                TradePortalConstants.LOANREQ_LOG_INVALID_CURRENCY,param,
			                creationRuleDoc.getAttribute("/NAME"));
			        	    loanReqStatus = false;
			            return null;
			          } else {
			            // Currency is good so attempt to get the fx rate doc.
			            fxRateDoc = getFxRateDoc(instProp.getCorporateOrgOid(),
			                templateCurrency);

			            // Don't look for an FX rate if the template's currency is the base currency
			            // No FX rate is needed in that case
			            if (!templateCurrency.equals(instProp.getCurrency())) {
			              if (fxRateDoc == null) {
			                // No fx rate found, isse warning and return a blank amount.
			                // This indicates that maxAmount is not used for grouping.
			                // Give error using the template currency.
								LOG.debug(" 1/4");
								instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
			                    TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,
			                    templateCurrency);
			            	    loanReqStatus = false;
			                return null;
			              } else {
			                // Found an fx rate for the template currency.  Use it.
			              }
			            } else {
			              // Base Currency and Template currency are identical
			              // No need for conversion.  Return the from amount
			              return fromAmount;
			            }
			          } // end templateCurrency == null
			        } // end templateDoc == null
			      } // end checkCode

			      // We now have an fx rate doc (although the values on the doc may be
			      // blank.)  Attempt to convert the amount.
			      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
			      rate = fxRateDoc.getAttributeDecimal("/RATE");

			      if (multiplyIndicator != null && rate != null) {
			        // We're converting FROM the base currency TO the po line item
			        // currency so we need to do the opposite of the multiplyIndicator
			        // (the indicator is set for converting FROM some currency TO the
			        // base currency)
			        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
			          convertedAmount = fromAmount.divide(rate,
			              BigDecimal.ROUND_HALF_UP);
			        } else {
			          convertedAmount = fromAmount.multiply(rate);
			        }
			      } else {
			        // We have bad data in the fx rate record.  Issue warning and return
			        // null amount (indicating that maxAmount is not used for grouping)
					LOG.debug(" 1/5");
					instProp.getMediatorServices().getErrorManager().issueError(instProp.getCorporateOrgOid(),
			            TradePortalConstants.LOANREQ_LOG_NO_FX_RATE,
			            fxRateDoc.getAttribute("/CURRENCY_CODE"));
			            loanReqStatus = false;
			        return null;
			      }
			    } catch (AmsException e) {
			      LOG.debug("Exception found getting converted max amount:");
			      LOG.info(e.toString());
			      e.printStackTrace();
			    }
				LOG.debug("loanReqStatus:"+loanReqStatus);
			    return convertedAmount;
			  }


		/**
		 * This method returns fx rates for the given currency.
		 * @param corporateOrg
		 * @param currency
		 * @return
		 */
		private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {

			Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
			DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);

			if (resultSet != null) {
				List<DocumentHandler> fxRateDocs = resultSet.getFragmentsList("/ResultSetRecord");
				return fxRateDocs.get(0);
			}

			return null;

		}


		 private DocumentHandler createATPByRules(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			LOG.debug("PayableInvoiceGroupMediatorBean.createATPByRules()...."+inputDoc);
			Vector invoiceList = null;
			Vector invoiceOidList = new Vector();
			Vector invoiceOidsVector = new Vector();
			Vector toBeProcessed = new Vector();
			HashMap invoiceMap = new HashMap();
			String groupIds[] = null;
			String invoiceOid = null;
			DocumentHandler invoiceItem = null;
			DocumentHandler outputDoc =  new DocumentHandler();
			DocumentHandler  uploadInvoiceDoc = null;
			boolean success =false;
			int count = 0;
			invoiceList				  = inputDoc.getFragments("/InvoiceGroupList/InvoiceGroup");
			String userOid			  = inputDoc.getAttribute("/InvoiceGroupList/userOid");
			String securityRights	  = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
			String clientBankOid	  = inputDoc.getAttribute("/InvoiceGroupList/clientBankOid");
			String corpOrgOid 		  = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
			String timeZone 		  = inputDoc.getAttribute("/InvoiceGroupList/timeZone");
			String usePaymentDate 	  = inputDoc.getAttribute("/InvoiceGroupList/usePaymentDate");
			String currencyCode 	  = inputDoc.getAttribute("/InvoiceGroupList/baseCurrencyCode");
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_CREATE_ATP)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.CreateApprovalToPay",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			
			
			InstrumentProperties instProp = new InstrumentProperties();
			instProp.setCurrency(currencyCode);
			instProp.setUserOid(userOid);
			instProp.setClientBankOid(clientBankOid);
			instProp.setCorporateOrgOid(corpOrgOid);
			instProp.setMediatorServices(mediatorServices);
			instProp.setTimeZone(timeZone);
			instProp.setUsePaymentDate(usePaymentDate);
			instProp.setSecurityRights(securityRights);
			ClientServerDataBridge csdb = mediatorServices.getCSDB();
	        instProp.setUserLocale(csdb.getLocaleName());
	        instProp.setInstTypeCode(InstrumentType.APPROVAL_TO_PAY);
			count = invoiceList.size();
			LOG.debug("PayableInvoiceGroupMediatorBean: number of invoices selected : " + count);
			processedByRule = new TreeSet<String>();

			if (count <= 0)
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateATPByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			groupIds = new String[count];
			// Loop through the list of invoice oids, and processing each one in the invoice list
		    for (int i = 0; i < count; i++)
		    {
			  invoiceItem = (DocumentHandler) invoiceList.get(i);
			  LOG.debug(" value is :"+invoiceItem.getAttribute("/InvoiceData"));
			  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateATPByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			  }
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");

			  invoiceOid = invoiceData[0];
			  invoiceOidList.add(invoiceOid);
			  groupIds[i]=invoiceData[0];
		    }
		    LOG.info("invoiceOidList->"+invoiceOidList);

		    List<DocumentHandler> invoicesGroupDoc =  getInvoices(groupIds,corpOrgOid);
		    List<DocumentHandler> tempGroupDoc = invoicesGroupDoc;
		    LOG.info("invoicesGroupDoc->"+tempGroupDoc +"\t:"+invoicesGroupDoc);

		    if(tempGroupDoc!=null && tempGroupDoc.size()>0){
				 for(int pos = 0; pos<tempGroupDoc.size();pos++){
				 uploadInvoiceDoc =  tempGroupDoc.get(pos);

			String linkedToInstType = uploadInvoiceDoc.getAttribute("/LINKED_TO_INSTRUMENT_TYPE");
			//if Instrument Type is blank or ATP type then do not create an instrument
			 if(StringFunction.isBlank(linkedToInstType) || !InstrumentType.APPROVAL_TO_PAY.equals(linkedToInstType)){

		    	  mediatorServices.getErrorManager().issueError(
		    			  UploadInvoiceMediator.class.getName(),
		    			  TradePortalConstants.PAY_INVOICE_ATP_RULES_NOT_ALLOW);

		    	  tempGroupDoc.remove(pos);
					pos--;
			  }

		  }
		}
		    if(invoicesGroupDoc!=null && invoicesGroupDoc.size()>0){

				 for(int pos = 0; pos<invoicesGroupDoc.size();pos++){
						 uploadInvoiceDoc = invoicesGroupDoc.get(pos);

					String instrOid = uploadInvoiceDoc.getAttribute("/A_INSTRUMENT_OID");
					String tranOid = uploadInvoiceDoc.getAttribute("/A_TRANSACTION_OID");
					//if already associated then do not create an instrument
					if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
						Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));
						mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
								TradePortalConstants.INSTRUMENT_EXISTS,
								mediatorServices.getResourceManager().getText("UploadInvoices.CreateATPByRules", TradePortalConstants.TEXT_BUNDLE) +" " +instr.getAttribute("complete_instrument_id"),uploadInvoiceDoc.getAttribute("/INVOICE_ID"));
						invoicesGroupDoc.remove(pos);
						pos--;
					}
				 }
			}
		    LOG.info("invoicesGroupDoc:"+invoicesGroupDoc);
		    if(invoicesGroupDoc==null || invoicesGroupDoc.size()==0)
				return new DocumentHandler();
		    	 for ( DocumentHandler row : invoicesGroupDoc) {
					 String invOid = row.getAttribute("/UPLOAD_INVOICE_OID");
					 String invId = row.getAttribute("/INVOICE_ID");
					 invoiceOidsVector.addElement(invOid);
					 toBeProcessed.addElement(invOid);
					 invoiceMap.put(invOid, invId);
				 }
		
		    LOG.info("oid list:"+invoiceOidsVector);
		 		 try {

					   String creationDateTime = DateTimeUtility.getGMTDateTime(true, false);

					   // this will not only group by rule, but also create associated instrument
					    groupInvoices(inputDoc,invoiceOidsVector,instProp);

					   // check if all of the invoices have matched to atleast one of the rule
 					   toBeProcessed.removeAll(processedByRule); //

 					   	LOG.debug("toBeProcessed:"+toBeProcessed+"\t invoiceMap:"+invoiceMap);
 					     if (toBeProcessed.size()>0 && invoiceMap.size()>0){
							 for (String eInvoiceId : (Iterable<String>) toBeProcessed) {
								 Object invId = invoiceMap.get(eInvoiceId);
								 String[] errorParameters = {String.valueOf(invId)};
								 boolean[] wrapParameters = {true};
								 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_MATCHING_RULE_ERROR,
										 errorParameters, wrapParameters);
							 }
 					     }

						 LOG.debug("size:"+invOIds.size()+"\t:"+invOIds.toString());
						 Vector invoiceIDVector = null;
						 String uploadInvOid = null;

						 if (invOIds.size() > 0){

							   invoiceIDVector = new Vector(invOIds);
								LOG.debug("invoiceIDVector:"+invoiceIDVector);
								InvoicesSummaryData invoiceSummaryData = null;
								for (int i = 0; i < invoiceIDVector.size(); i++) {
									uploadInvOid =  (String) invoiceIDVector.get(i);

									 invoiceSummaryData =
											(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
													Long.parseLong(uploadInvOid));
									 String tpName=invoiceSummaryData.getTpRuleName();

									invoiceSummaryData.setAttribute("tp_rule_name", tpName);
									int saveYN = invoiceSummaryData.save();
									LOG.debug("SaveYN :"+saveYN);
								}
							List<Object> sqlParamsLst = new ArrayList();
					 	    StringBuilder sql = new StringBuilder("select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
					                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
					                "from invoices_summary_data isd inner join instrument ins on isd.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
					                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
					                "and ");
					        sql.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(new Vector(invOIds),"isd.upload_invoice_oid", sqlParamsLst));
					        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParamsLst);
					        outputDoc.addComponent("/Out",resultSetDoc);

					        updateFileUpload(inputDoc,outputDoc, creationDateTime,"InvoiceGroupList",instProp);

							Vector newGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(new Vector(invOIds));

							updateAttachmentDetails(invoiceOidList,newGroupOids, mediatorServices);

						success =true;
					  }else if(loanReqStatus && invOIds.size() == 0){
					 }

					} catch (AmsException e) {

						e.printStackTrace();
					}
				   invOIds = new TreeSet<String>();
					if(success)
						   mediatorServices.getErrorManager().issueError(
									TradePortalConstants.ERR_CAT_1,
									TradePortalConstants.AUTO_ATP_REQ_RULE_CREATION_SUCCESS);
		   return outputDoc;

	      }

		   private DocumentHandler createDetailATPByRules(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {
			LOG.debug("PayableInvoiceGroupMediatorBean.createDetailATPByRules()...."+inputDoc);
			List<DocumentHandler> invoiceList = null;
			Vector invoiceOidList = new Vector();
			String invoiceOid = null;
			DocumentHandler invoiceItem = null;
			String invoiceId = null;
			String instrumentType = null;
			String instrOid = null;
			String tranOid = null;
			DocumentHandler outputDoc =  new DocumentHandler();
			int count = 0;
			int errorCount = 0;
			boolean success = false;
			HashMap invoiceMap = new HashMap();
			Vector toBeProcessed = new Vector();
			processedByRule = new TreeSet<String>();

			invoiceList				  = inputDoc.getFragmentsList("/UploadInvoiceList/InvoicesSummaryData");
			String userOid			  = inputDoc.getAttribute("/UploadInvoiceList/userOid");
			String securityRights	  = inputDoc.getAttribute("/UploadInvoiceList/securityRights");
			String clientBankOid	  = inputDoc.getAttribute("/UploadInvoiceList/clientBankOid");
			String corpOrgOid 		  = inputDoc.getAttribute("/UploadInvoiceList/ownerOrg");
			String timeZone 		  = inputDoc.getAttribute("/UploadInvoiceList/timeZone");
			String usePaymentDate 	  = inputDoc.getAttribute("/UploadInvoiceList/usePaymentDate");
			String currencyCode 	  = inputDoc.getAttribute("/UploadInvoiceList/baseCurrencyCode");
			
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_CREATE_ATP)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoices.CreateApprovalToPay",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			InstrumentProperties instProp = new InstrumentProperties();
			instProp.setCurrency(currencyCode);
			instProp.setUserOid(userOid);
			instProp.setClientBankOid(clientBankOid);
			instProp.setCorporateOrgOid(corpOrgOid);
			instProp.setMediatorServices(mediatorServices);
			instProp.setTimeZone(timeZone);
			instProp.setUsePaymentDate(usePaymentDate);
			instProp.setSecurityRights(securityRights);
			ClientServerDataBridge csdb = mediatorServices.getCSDB();
	        instProp.setUserLocale(csdb.getLocaleName());
	        instProp.setInstTypeCode(InstrumentType.APPROVAL_TO_PAY);
			count = invoiceList.size();
			LOG.debug("UploadInvoiceMediatorBean: number of invoices selected : " + count);

			if (count <= 0)
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}

			// Loop through the list of invoice oids, and processing each one in the invoice list
		    for (int i = 0; i < count; i++)
		    {
			  invoiceItem = invoiceList.get(i);
			  LOG.debug(" value is :"+invoiceItem.getAttribute("/InvoiceData"));
			  if(StringFunction.isBlank(invoiceItem.getAttribute("/InvoiceData"))){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ITEM_SELECTED,
						mediatorServices.getResourceManager().getText("UploadInvoices.CreateLoanReqByRules", TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			  }
			  String[] invoiceData = invoiceItem.getAttribute("/InvoiceData").split("/");
			  invoiceOid = invoiceData[0];
			  invoiceOidList.add(invoiceOid);
			  toBeProcessed.addElement(invoiceOid);
			   try
			   {
			      InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
			      invoiceId = invoiceSummaryData.getAttribute("invoice_id");
				  instrumentType = invoiceSummaryData.getAttribute("linked_to_instrument_type");
				  instrOid = invoiceSummaryData.getAttribute("instrument_oid");
				  tranOid = invoiceSummaryData.getAttribute("transaction_oid");
				  invoiceMap.put(invoiceOid, invoiceId);
			      if(StringFunction.isBlank(instrumentType) || !InstrumentType.APPROVAL_TO_PAY.equals(instrumentType)){

			    	  mediatorServices.getErrorManager().issueError(
			    			  UploadInvoiceMediator.class.getName(),
			    			  TradePortalConstants.PAY_INVOICE_ATP_RULES_NOT_ALLOW);
			    	  return new DocumentHandler();
				  }
			      if(StringFunction.isNotBlank(tranOid) && StringFunction.isNotBlank(instrOid)){
						Instrument	instr = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(instrOid));

						mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
							TradePortalConstants.LRQ_INSTRUMENT_EXISTS,
							mediatorServices.getResourceManager().getText("PayableInvoiceGroups.ATPMessage", TradePortalConstants.TEXT_BUNDLE) +" " +instr.getAttribute("complete_instrument_id"),invoiceId);
						errorCount++;
					}
			   }
			   catch(Exception e)
			   {
				   LOG.info("General exception caught in PayableInvoiceGroupMediatorBean:createDetailATPByRules");
				   e.printStackTrace();
			   }

		    }
		    if(errorCount>0){
				   return new DocumentHandler();
			 }

		   try{
			   String creationDateTime = DateTimeUtility.getGMTDateTime(true, false);
		    	 Vector oldGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(invoiceOidList);
				 LOG.debug("oldGroupOids:"+oldGroupOids);
				 groupInvoices(inputDoc,invoiceOidList,instProp);

				   // check if all of the invoices have matched to atleast one of the rule
				  toBeProcessed.removeAll(processedByRule); //
				  LOG.info("toBeProcessed:"+toBeProcessed+"\t invoiceMap:"+invoiceMap);
				     if (toBeProcessed.size()>0 && invoiceMap.size()>0){

						 for (String eInvoiceId : (Iterable<String>) toBeProcessed) {
							 Object invId = invoiceMap.get(eInvoiceId);
							 String[] errorParameters = {String.valueOf(invId)};
							 boolean[] wrapParameters = {true};
							 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_MATCHING_RULE_ERROR,
									 errorParameters, wrapParameters);
						 }
				     }

				 LOG.debug("size:"+invOIds.size()+"\t:"+invOIds.toString()+"\t loanReqStatus:"+loanReqStatus);
				 if (invOIds.size() > 0){


					   String uploadInvOid = null;
					   Vector invoiceIDVector = null;
					   invoiceIDVector = new Vector(invOIds);
						LOG.debug("invoiceIDVector:"+invoiceIDVector);
						InvoicesSummaryData invoiceSummaryData = null;
						for (int i = 0; i < invoiceIDVector.size(); i++) {
							uploadInvOid =  (String) invoiceIDVector.get(i);

							 invoiceSummaryData =
									(InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
											Long.parseLong(uploadInvOid));
							 String tpName=invoiceSummaryData.getTpRuleName();
							invoiceSummaryData.setAttribute("tp_rule_name",tpName);
							int saveYN = invoiceSummaryData.save();
							LOG.debug("SaveYN :"+saveYN);
						}
						List<Object> sqlParamsLst = new ArrayList();
						StringBuilder sql = new StringBuilder("select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
				                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
				                "from invoices_summary_data isd inner join instrument ins on isd.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
				                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
				                "and ");
				        sql.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(new Vector(invOIds)," isd.upload_invoice_oid", sqlParamsLst));
				        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParamsLst);
				        outputDoc.addComponent("/Out",resultSetDoc);

				        updateFileUpload(inputDoc,outputDoc, creationDateTime,"UploadInvoiceList",instProp);

						Vector newGroupOids = POLineItemUtility.getInvoiceGroupOidsForInvoiceOids(new Vector(invOIds));

						updateAttachmentDetails(oldGroupOids,newGroupOids, mediatorServices);

						success =true;
				  }else if(loanReqStatus && invOIds.size() == 0){
				 }

				} catch (AmsException e) {

					e.printStackTrace();
				}
			   invOIds = new TreeSet<String>();
				if(success)
					   mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.AUTO_ATP_REQ_RULE_CREATION_SUCCESS);
		   return outputDoc;
	      }

	public void updateAttachmentDetails(Vector groupOids,Vector oidsList, MediatorServices mediatorSer) throws AmsException{

				LOG.debug("updateAttachmentDetails() : groupOid:"+groupOids +"\t oidsList:"+oidsList);

				Vector removeList = new Vector();
				Vector addList = new Vector();
				if(oidsList!=null && oidsList.size()>0){
					for (Object anOidsList : oidsList) {
						groupOids.add(anOidsList);
					}
				}
				LOG.debug("groupOids:"+groupOids);


				StringBuilder groupSearchSQL = new StringBuilder("select d.DOC_IMAGE_OID from INVOICES_SUMMARY_DATA i");
				groupSearchSQL.append(" inner join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID");
				groupSearchSQL.append(" where i.A_INVOICE_GROUP_OID =  ? ");
		for (Object groupOid1 : groupOids) {
			String groupOid = (String) groupOid1;

			DocumentHandler oldGroupDoc = DatabaseQueryBean.getXmlResultSet(groupSearchSQL.toString(), false, new Object[]{groupOid});

			Vector oldGroupList = null;
			//Searching for image_id in document_image table if any attachments available for exisitng group oid
			if (oldGroupDoc != null) {
				oldGroupList = oldGroupDoc.getFragments("/ResultSetRecord");
				LOG.debug("GroupList:" + oldGroupList);
				if (oldGroupList != null && oldGroupList.size() >= 1) {
					addList.add(groupOid);
				}
			} else {
				removeList.add(groupOid);
			}
		}

				LOG.debug("before entering in to old sql :"+removeList);
				//Updating the group when attachment founds with NO
				String removeSQL ="UPDATE invoice_group SET attachment_ind = ?' WHERE ";
				if(removeList.size()>0){
					List<Object> sqlprmLst = new ArrayList<Object>();
					sqlprmLst.add(TradePortalConstants.INDICATOR_NO);
			        removeSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(removeList, "invoice_group_oid",sqlprmLst);

				try {

					int resultCount = DatabaseQueryBean.executeUpdate(removeSQL.toString(),false,new ArrayList<Object>());


				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}
				}
				LOG.debug("before entering in to new sql :->"+addList);
				//Updating the group when attachment founds with YES
				String addSQL = "UPDATE invoice_group SET attachment_ind = ? WHERE ";
				if(addList.size()>0){
					List<Object> sqlParamsLst = new ArrayList<Object>();
					sqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
			        addSQL+= POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(addList, "invoice_group_oid",sqlParamsLst);
				try {

					int resultCount = DatabaseQueryBean.executeUpdate(addSQL.toString(), false, sqlParamsLst);

				} catch (SQLException e) {
					LOG.debug("exception while updating attach ind"+e.getMessage());
					e.printStackTrace();
					}
				}


			}
	
	// Nar CR 913 Begin
	private DocumentHandler performModifyInvoiceData(DocumentHandler inputDoc, MediatorServices mediatorServices, String buttonPressed) 
	throws AmsException, NumberFormatException, RemoteException {

		String modifyAttr = null;
		String modifyVal = null;
		String buttonTextVal = null;
		String blankMsgTextVal = null;
		String successMsgVal = null;
		String invAction = "";
		String securityRights	  = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		//Set attributes based on buttonPressed
		if(TradePortalConstants.BUTTON_MODIFY_SUPPLIER_DATE.equals(buttonPressed)) {
			
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ApplySendtoSuppDate",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			modifyAttr = "send_to_supplier_date";
			modifyVal = inputDoc.getAttribute("/InvoiceGroupList/supplier_date");
			blankMsgTextVal = mediatorServices.getResourceManager().getText("InvoiceUploadRequest.supplier_date",TradePortalConstants.TEXT_BUNDLE);
			buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayModifySuppDate",TradePortalConstants.TEXT_BUNDLE);
			successMsgVal = TradePortalConstants.PAY_PRGM_MODIFY_SUPP_DATE;
			invAction = TradePortalConstants.UPLOAD_INV_ACTION_MODIFY_SUPPLIER_DATE;
		}else if(TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)) {
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAYABLES_APPLY_PAYDATE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"UploadInvoiceAction.ApplyPaymentDate",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			modifyAttr = "payment_date";
			modifyVal = inputDoc.getAttribute("/InvoiceGroupList/payment_date");
			blankMsgTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.payment_date",TradePortalConstants.TEXT_BUNDLE);
			buttonTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayApplyPaymentDate",TradePortalConstants.TEXT_BUNDLE);
			successMsgVal = TradePortalConstants.INV_ASSIGN_PAYMENT_DATE;
			invAction = TradePortalConstants.UPLOAD_INV_ACTION_APPLY_PAYMENT_DATE;
		}
		
		// Get the InvoiceGroupOids from the inputDoc and use them to get
		// invoices belong to the group
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ITEM_SELECTED,
					buttonTextVal);
			return new DocumentHandler();
		}
		else if (invoiceGroupList.size() > 1) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_ONLY_ONE_ITEM,
					mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
					mediatorServices.getResourceManager().getText("UploadInvoices.InvPayModifySuppDate",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		if(StringFunction.isBlank(modifyVal)){

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.REQUIRED_ATTRIBUTE, blankMsgTextVal);
			return new DocumentHandler();
		}
		//Validate  Date
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(inputDoc.getAttribute("/InvoiceGroupList/userOid")));
		String datePattern = user.getAttribute("datepattern");
		 try{

		    SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			SimpleDateFormat reqformatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date formatSupplierDate   =  formatter.parse(modifyVal);
		    modifyVal = reqformatter.format(formatSupplierDate);
		 }catch(Exception e){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.WEBI_INVALID_DATE_ERROR);
			return new DocumentHandler();
		 } 
		
		
		//There is only one Invoice Group to process
		DocumentHandler invoiceGroupItem = invoiceGroupList.get(0);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		String invoiceGroupOid = invoiceData[0];
		String optLock = invoiceData[1];
		//CR914A  start-The same  Due/Payment date and Send To Supplier Date must be present on all invoices in the same E2E group. 
		//If a user selects one or more E2E invoices, but not all of the invoices in the End to End group, the user will get an error message 
		if(TradePortalConstants.BUTTON_MODIFY_SUPPLIER_DATE.equals(buttonPressed) || TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed)){
		String sql = "select DISTINCT a_invoice_group_oid from invoices_summary_data where end_to_end_id in" +
				" ( select end_to_end_id from invoices_summary_data where a_invoice_group_oid =?) ";
		DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
		if(invoiceListDoc!=null && invoiceListDoc.getFragmentsList("/ResultSetRecord").size()>1){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_DATE_FOR_END_TO_END_GROUP);
			return new DocumentHandler();
		}
		}
		//CR914A end
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", Long.parseLong(invoiceGroupOid));
		String groupDescription = invoiceGroup.getDescription();
		if(!InvoiceUtility.isPayableInvoiceUpdatable(invoiceGroup.getAttribute("invoice_status"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,groupDescription);
			return new DocumentHandler();
		}
		if(TradePortalConstants.BUTTON_MODIFY_SUPPLIER_DATE.equals(buttonPressed) && StringFunction.isNotBlank(invoiceGroup.getAttribute("payment_date"))){
			//if 'Send to Supplier Date' is greater  than or equal to the earliest of payment date/due date, OR 
			// if it is less than current system date then error
			Date paymentDate = TPDateTimeUtility.convertDateStringToDate(invoiceGroup.getAttribute("payment_date"),"MM/dd/yyyy");
			Date supplierDate = TPDateTimeUtility.convertDateStringToDate(modifyVal,"MM/dd/yyyy");
			if(!supplierDate.before(paymentDate)){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_SUPPLIER_DATE,
						"", null);
				return new DocumentHandler();
			}
			if(supplierDate.before(GMTUtility.getGMTDateTime())){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVALID_SUPPLIER_DATE_WITH_SYS_DATE,
						"", null);
				return new DocumentHandler();
			}
		  }
		
		String oldGroupDate = invoiceGroup.getAttribute(modifyAttr);
		invoiceGroup.setAttribute(modifyAttr, modifyVal);
		
		boolean issuedSuccessMessage = false;
		String newGroupOid="";
		String sql = "SELECT upload_invoice_oid,ISSUE_DATE,portal_approval_req_ind, end_to_end_id FROM invoices_summary_data where a_invoice_group_oid = ?";
		DocumentHandler invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
		boolean isGroupSaved = false;
		// loop thru the invoice oids and get all the invoices belong to the
		// group and then set the supplier date in each invoice
		if (invoiceListDoc != null) {
			List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
                    for (DocumentHandler doc : records) {
                        String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
                        String date = doc.getAttribute("ISSUE_DATE");
                        //Rel9.3 CR 1006 START
                        String end2EndId = doc.getAttribute("END_TO_END_ID");
                        String appReqInd = doc.getAttribute("PORTAL_APPROVAL_REQ_IND");
                        if(TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed) && 
                        		TradePortalConstants.INDICATOR_YES.equals(appReqInd) &&  StringFunction.isNotBlank(end2EndId)){
                        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PAYMENT_DATE_CANNOT_BE_MODIFIED);
                            return new DocumentHandler();
                        }
                        //Rel9.3 CR 1006 END
                        if(TradePortalConstants.PAY_PGM_BUTTON_APPLY_PAYMENT_DATE.equals(buttonPressed) && StringFunction.isNotBlank(date)){
                            String issueDate = DateTimeUtility.convertTimestampToDateString(date);
                            if ((DateTimeUtility.convertStringDateToDate(modifyVal)).before(DateTimeUtility.convertStringDateToDate(issueDate)))
                            {
                                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_INV_PAYMENT_DATE);
                                return new DocumentHandler();
                            }
                        }
                        if (StringFunction.isNotBlank(invoiceOid)) {
                            InvoicesSummaryData invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
                                    Long.parseLong(invoiceOid));
                            
                            try{
                                invoiceSummaryData.setAttribute(modifyAttr, modifyVal);
                                if (StringFunction.isNotBlank(modifyVal) &&
                                        !modifyVal.equals(oldGroupDate) && !isGroupSaved) {
                                    // check that another matching group does not already exist.
                                    newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup,true);
                                    
                                    if (StringFunction.isNotBlank(newGroupOid)
                                            && !newGroupOid.equals(invoiceGroupOid)) {
                                        // there is another match, so assign the other group's OID
                                        // to these invoices and delete this group
                                        invoiceGroup.delete();
                                    }
                                    else {
                                        invoiceGroup.setAttribute("opt_lock", optLock);
                                        if(invoiceGroup.save()==1)
                                            isGroupSaved =true;
                                    }
                                }
                                
                            }
                            catch (AmsException e) {
                                mediatorServices.debug("PayableInvoiceGroupMediatorBean.resetSuppDate: Unexpected error: " + e.toString());
                                e.printStackTrace();
                                return new DocumentHandler();
                            }
                            if (StringFunction.isNotBlank(newGroupOid)) {
                                invoiceSummaryData.setAttribute("invoice_group_oid",newGroupOid);
                            }
                            String tpName=invoiceSummaryData.getTpRuleName();
                            invoiceSummaryData.setAttribute("tp_rule_name", tpName);
                            invoiceSummaryData.setAttribute("action", invAction);
    		                invoiceSummaryData.setAttribute("user_oid", inputDoc.getAttribute("/InvoiceGroupList/userOid"));
                            int success = invoiceSummaryData.save();
                            if (success > 0 ) {
                               if(!issuedSuccessMessage){
                                    // display message
                                    mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
                                            successMsgVal,
                                            mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE)+" ",groupDescription);
                                    issuedSuccessMessage = true;
                                }
                            }
                        }
                    }
		}
		return new DocumentHandler();
	}
	public class MyXMLTagTranslator extends com.amsinc.ecsg.util.XMLTagTranslator {

	       private Map<String, String> nameMap;

	       public MyXMLTagTranslator(Map<String, String> nameMap) {
	           this.nameMap = nameMap;
	       }

	       /**
	        * translate tagName according to nameMap.
	        * translate attribute values as necessary.
	        */
	       public void translateTag(String tagName, String tagValue) {

	           attributeName = (String)nameMap.get(tagName);

	           
	               attributeValue = tagValue;
	           

	       }
	       }
	private DocumentHandler performResetInvoiceData(DocumentHandler inputDoc, MediatorServices mediatorServices, String buttonPressed) 
	throws RemoteException, AmsException {
		String resetAttr = null;
		String successMsgVal = null;
		String blankMsgTextVal = null;
		String invAction ="";
		String securityRights	  = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		mediatorServices.debug("InvoiceGroupMediatorBean.payPgmResetSupplierDate: Start: ");
		//Set attributes based on buttonPressed
		if(TradePortalConstants.BUTTON_RESET_SUPPLIER_DATE.equals(buttonPressed)) {
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ResetSendtoSuppDate",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			resetAttr = "send_to_supplier_date";
			successMsgVal = TradePortalConstants.PAY_PRGM_RESET_SUPP_DATE;
			blankMsgTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.InvPayResetSuppDate", TradePortalConstants.TEXT_BUNDLE);
			invAction =TradePortalConstants.UPLOAD_INV_ACTION_RESET_SUPPLIER_DATE;
		}else if(TradePortalConstants.PAY_MGMT_BUTTON_RESET_PAY_AMOUNT.equals(buttonPressed)) {
			if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT)) {
				// user does not have appropriate authority.
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
						mediatorServices.getResourceManager().getText(
								"SecurityProfileDetail.ResetEarlyPayAmt",
								TradePortalConstants.TEXT_BUNDLE));
				return new DocumentHandler();
			}
			resetAttr = "payment_amount";
			successMsgVal = TradePortalConstants.PAY_PRGM_RESET_PAYMENT_AMT;
			blankMsgTextVal = mediatorServices.getResourceManager().getText("UploadInvoices.ResetEarlyPaymentAmt", TradePortalConstants.TEXT_BUNDLE);
			invAction =TradePortalConstants.UPLOAD_INV_ACTION_RESET_PAYMENT_AMT;
		}
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NO_ITEM_SELECTED,
					blankMsgTextVal);
			return new DocumentHandler();
		}
		else if (invoiceGroupList.size() > 1) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_ONLY_ONE_ITEM,
					mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),
					mediatorServices.getResourceManager().getText("UploadInvoices.InvPayModifySuppDate",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		//There is only one Invoice Group to process
		DocumentHandler invoiceGroupItem = invoiceGroupList.get(0);
		String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
		String invoiceGroupOid = invoiceData[0];
		String optLock = invoiceData[1];
		InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", Long.parseLong(invoiceGroupOid));
        String newGroupOid ="";
		//String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
		String groupDescription = invoiceGroup.getDescription();
		if(!InvoiceUtility.isPayableInvoiceUpdatable(invoiceGroup.getAttribute("invoice_status"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,TradePortalConstants.PAYABLE_MGMT_INVALID_STATUS,groupDescription);
			return new DocumentHandler();
		}
		invoiceGroup.setAttribute(resetAttr,null);
		
		boolean issuedSuccessMessage = false;
		boolean isGroupSaved = false;
		String sql = "SELECT upload_invoice_oid FROM invoices_summary_data WHERE a_invoice_group_oid = ? ";
		DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
		if (invoiceListDoc != null) {
			List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
			InvoicesSummaryData invoiceSummaryData = null;
			for (DocumentHandler doc : records) {
				String invoiceOid = doc.getAttribute("/UPLOAD_INVOICE_OID");
				if (StringFunction.isNotBlank(invoiceOid)) {
					invoiceSummaryData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));				 					
				    try{
					invoiceSummaryData.setAttribute(resetAttr,	null);
						// check that another matching group does not already exist.
					if(!isGroupSaved){
						newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup,true);

						if (StringFunction.isNotBlank(newGroupOid)
								&& !newGroupOid.equals(invoiceGroupOid)) {
							// there is another match, so assign the other group's OID
							// to these invoices and delete this group
							invoiceGroup.delete();
						}
						else {
							invoiceGroup.setAttribute("opt_lock", optLock);
							if(invoiceGroup.save()==1)
							isGroupSaved = true;
							}
						}
				    }
					catch (AmsException e) {
						mediatorServices.debug("PayableInvoiceGroupMediatorBean.resetSuppDate: Unexpected error: " + e.toString());
						e.printStackTrace();	
						return new DocumentHandler();
					}
					if (StringFunction.isNotBlank(newGroupOid)) {
						invoiceSummaryData.setAttribute("invoice_group_oid",newGroupOid);
					}
				    String tpName=invoiceSummaryData.getTpRuleName();
			        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
			        invoiceSummaryData.setAttribute("action", invAction);
	                invoiceSummaryData.setAttribute("user_oid", userOid);
				    int success = invoiceSummaryData.save();
					if (success > 0 ) {
						// display message
						 if(!issuedSuccessMessage){
						mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
								successMsgVal,
								mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE)+" ",groupDescription);
						issuedSuccessMessage = true;
						 }
					}
				}
			}
		}
	   return (new DocumentHandler());
     
	}
	//Nar CR 913 End
	
	private class InvoiceProperties{
		private String userOid = "";
		private String corpOrgOid = "";
		private String groupDescription = "";
		private String newGroupOid = "";
		private String userWorkGroupOid = "";
		private MediatorServices mediatorServices = null;
		private boolean groupHasBeenSaved = false;
		private String subsAuth = "";
		private String panelGroupOid = "";
		private String panelOplockVal = "";
		private String panelRangeOid  = ""; 
        private String panelLevelCode = "";
		
		public String getPanelLevelCode() {
			return panelLevelCode;
		}
		public void setPanelLevelCode(String panelLevelCode) {
			this.panelLevelCode = panelLevelCode;
		}		
		public String getPanelGroupOid() {
			return panelGroupOid;
		}
		public void setPanelGroupOid(String panelGroupOid) {
			this.panelGroupOid = panelGroupOid;
		}
		public String getPanelOplockVal() {
			return panelOplockVal;
		}
		public void setPanelOplockVal(String panelOplockVal) {
			this.panelOplockVal = panelOplockVal;
		}
		public String getPanelRangeOid() {
			return panelRangeOid;
		}
		public void setPanelRangeOid(String panelRangeOid) {
			this.panelRangeOid = panelRangeOid;
		}
		public String getUserOid() {
			return userOid;
		}
		public void setUserOid(String userOid) {
			this.userOid = userOid;
		}
		public String getCorpOrgOid() {
			return corpOrgOid;
		}
		public void setCorpOrgOid(String corpOrgOid) {
			this.corpOrgOid = corpOrgOid;
		}
		public String getGroupDescription() {
			return groupDescription;
		}
		public void setGroupDescription(String goodsDescription) {
			this.groupDescription = goodsDescription;
		}
		public MediatorServices getMediatorServices() {
			return mediatorServices;
		}
		public void setMediatorServices(MediatorServices mediatorServices) {
			this.mediatorServices = mediatorServices;
		}
		public String getNewGroupOid() {
			return newGroupOid;
		}
		public void setNewGroupOid(String newGroupOid) {
			this.newGroupOid = newGroupOid;
		}
		public String getUserWorkGroupOid() {
			return userWorkGroupOid;
		}
		public void setUserWorkGroupOid(String userWorkGroupOid) {
			this.userWorkGroupOid = userWorkGroupOid;
		}
		public boolean isGroupHasBeenSaved() {
			return groupHasBeenSaved;
		}
		public void setGroupHasBeenSaved(boolean groupHasBeenSaved) {
			this.groupHasBeenSaved = groupHasBeenSaved;
		}
		public String getSubsAuth() {
			return subsAuth;
		}
		public void setSubsAuth(String subsAuth) {
			this.subsAuth = subsAuth;
		}
		
	}
	private DocumentHandler authorizeInvoice(DocumentHandler inputDoc,MediatorServices mediatorServices)
		throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.authorizeInvoice: Start: ");
		String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
		String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
		String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
		String subsAuth =inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String buyerName = null;
		String userOrgOid = null;
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.UPLOAD_PAY_MGM_AUTH)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.Authorize",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		ResourceManager resMgr = mediatorServices.getResourceManager();
		ErrorManager errMgr = mediatorServices.getErrorManager();
		User user = (User) mediatorServices.createServerEJB("User",
				Long.parseLong(userOid));
		userOrgOid = user.getAttribute("owner_org_oid");
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_ITEM_SELECTED,
					resMgr.getText("UploadInvoiceAction.Authorize",TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());

		String userWorkGroupOid = user.getAttribute("work_group_oid");
		CorporateOrganization corpOrg = null;
		InvoiceProperties invProp = new InvoiceProperties();
		invProp.setCorpOrgOid(corpOrgOid);
		invProp.setUserOid(userOid);
		invProp.setUserWorkGroupOid(userWorkGroupOid);
		invProp.setMediatorServices(mediatorServices);
		invProp.setSubsAuth(subsAuth);
		// create Map to store END_TO_END_ID info
		Map<String, String> endToEndIdGroupProp = new HashMap<String, String>();
		endToEndIdGroupProp.put("corpOrgOid", corpOrgOid);
		endToEndIdGroupProp.put("userOid", userOid);
		endToEndIdGroupProp.put("userWorkGroupOid", userWorkGroupOid);
		endToEndIdGroupProp.put("subsAuth", subsAuth);
		Set<String> endToEndIDSet = new HashSet<String>();
		// Process each Invoice Group in the list
		GROUPS_LIST: 
		for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
               String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
               String invoiceGroupOid = invoiceData[0];
               String optLock = invoiceData[1];
               invProp.setGroupHasBeenSaved(false);//IR 27899
               InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
                         Long.parseLong(invoiceGroupOid));
               String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
               String groupDescription = invoiceGroup.getAttribute("trading_partner_name");
               invProp.setGroupDescription(groupDescription);
                    
               // only certain invoice statuses can be authorized.
               
                if(StringFunction.isNotBlank(invoiceGroup.getAttribute("end_to_end_id")))
                {
                	if( endToEndIDSet.add(invoiceGroup.getAttribute("end_to_end_id")) )
    				{
                		
                		if (!InvoiceUtility.isPayableInvoiceAuthorizable(resMgr.getText("InvoiceGroups.InvoiceGroup",
                                TradePortalConstants.TEXT_BUNDLE),groupDescription,invoiceStatus,mediatorServices)) {
                            continue;
                       }
                	authorizeEndToEndIDGroup( invoiceGroup, endToEndIdGroupProp, mediatorServices);
                	 String panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
                	 String panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
                	 String panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid"); 
                	 invoiceStatus = invoiceGroup.getAttribute("invoice_status");
                	 String updateSQL = "UPDATE invoice_group SET invoice_status = ?,a_panel_auth_group_oid=?,a_panel_auth_range_oid=?,"
                			+ "panel_oplock_val=? where end_to_end_id = ?";
        			try {
        				DatabaseQueryBean.executeUpdate(updateSQL, false, invoiceStatus, panelGroupOid,panelRangeOid,panelOplockVal,invoiceGroup.getAttribute("end_to_end_id"));
        			} catch (SQLException e) {
        				e.printStackTrace();
        				//throw new AmsException(e.getMessage());
        			}
    				}
                } 
                else
                {   if (!InvoiceUtility.isPayableInvoiceAuthorizable(resMgr.getText("InvoiceGroups.InvoiceGroup",
                        TradePortalConstants.TEXT_BUNDLE),groupDescription,invoiceStatus,mediatorServices)) {
                    continue;
                	}
                    invoiceGroup.setAttribute("opt_lock", optLock);
                    String invoiceGroupAmount = null;
                    String invoiceGroupCurrency = null;
                    String invoiceGroupPayAmt = "";
                    String panelAuthTransactionStatus = null;
                    String panelGroupOid = null;
                    String panelOplockVal = null;
                    String panelRangeOid = null;
                    
                    BigDecimal amountInBaseToPanelGroup = null ;
                    if (corpOrg == null) {
                        corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                                Long.parseLong(corpOrgOid));
                    }
                    String authorizeType = corpOrg.getAttribute("dual_auth_upload_pay_inv");
                    
                    
                    String clientBankOid = corpOrg.getAttribute("client_bank_oid");
                    String groupAmountsql = "select AMOUNT, PAYMENT_AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ? ";
                    DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
                    if(invoiceAmountDoc !=null){
                        invoiceGroupAmount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
                        invoiceGroupCurrency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
                        invoiceGroupPayAmt =  (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("PAYMENT_AMOUNT");
                    }
                    // Check threshold amount start
                    
                    String baseCurrency = corpOrg.getAttribute("base_currency_code");
                    invoiceGroupPayAmt  = StringFunction.isNotBlank(invoiceGroupPayAmt)?invoiceGroupPayAmt:invoiceGroupAmount;
                    
                    String selectClause = "select distinct upload_invoice_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) " +
                            "then amount else payment_amount end as payment_amount from invoices_summary_data ";
                    
                    boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt,invoiceGroupCurrency , baseCurrency,
                            "upload_pay_inv_thold", "upload_pay_inv_dlimit", userOrgOid,clientBankOid, user ,selectClause, false,subsAuth, mediatorServices);
                    if(!isValid){
                        return new DocumentHandler();
                    }
                    
                    //threshold check end
                    
                    //for Panel Auth
                    if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
                            corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) {
                        PanelAuthorizationGroup panelAuthGroup = null;
                        if(StringFunction.isBlank(invoiceGroup.getAttribute("panel_auth_group_oid"))) {
                            
                            panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_PROGRAM_INVOICE, corpOrg, errMgr);
                            if(StringFunction.isNotBlank(panelGroupOid)){
                                panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
                                        Long.parseLong(panelGroupOid));
                            }else{
                                // issue error as panel group is required if panel auth defined for instrument type
                                errMgr.issueError(TradePortalConstants.ERR_CAT_1,AmsConstants.REQUIRED_ATTRIBUTE,
                                        resMgr.getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
                                // if panel id is blank, return the flow back.
                                IssuedError is = new IssuedError();
                                is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
                                throw new AmsException(is);
                            }
                            
                            invoiceGroup.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
                            invoiceGroup.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
                            
                            String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
                            //IR 27059- Use Payment_Amount if present
                            amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
                                           (invoiceGroupCurrency, invoiceGroupPayAmt, baseCurrencyToPanelGroup, corpOrgOid,
                                                   TradePortalConstants.INVOICE, null, false, errMgr);
                            
                            panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
                            if(StringFunction.isBlank(panelRangeOid)){
                                errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
                                        amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE );
                                IssuedError is = new IssuedError();
                                is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
                                throw new AmsException(is);
                            }
                            invoiceGroup.setAttribute("panel_auth_range_oid", panelRangeOid);
                        }
                        
                        panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
                        panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
                        panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid");
                        
                        
                        
                        PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, errMgr);
                        panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);
                        
                        panelAuthTransactionStatus = panelAuthProcessor.process(invoiceGroup, panelRangeOid, false);
                    }
                    String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
                    DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
                    if (invoiceListDoc != null) {
                        List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
                        for (DocumentHandler doc : records) {
                            String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
                            
                            if (StringFunction.isNotBlank(invoiceOid)) {
                                InvoicesSummaryData invoiceSummaryData =
                                        (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
                                
                                String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
                                String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");
                                String newStatus = invoiceSummaryData.getAttribute("invoice_status");
                                boolean isSendToTPS= false;
                                boolean isUpdateAttachment = false;
                                
                                // ***********************************************************************
                                // If dual authorization is required, set the transaction state to
                                // partially authorized if it has not been previously authorized and set
                                // the transaction to authorized if there has been a previous
                                // authorizer.
                                // The second authorizer must not be the same as the first authorizer
                                // not the same work groups, depending on the corporate org setting.
                                // If dual authorization is not required, set the invoice state to
                                // authorized.
                                // ***********************************************************************
                                
                                String tpName=invoiceSummaryData.getTpRuleName();
                                invoiceSummaryData.setAttribute("tp_rule_name", buyerName);
                                
                                if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                                        && StringFunction.isBlank(userWorkGroupOid))
                                {
                                    newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
                                    
                                    if (!invProp.isGroupHasBeenSaved()) {
                                        saveInvoiceGroup(newStatus, invoiceGroup,invProp);
                                        errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
                                    }
                                    if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
                                        invoiceSummaryData.setAttribute("invoice_group_oid",
                                                invProp.getNewGroupOid());
                                    }
                                    saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
                                }
                                else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                                        || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
                                {
                                    authoriseWithDiffWorkGroup(invoiceSummaryData, invoiceGroup,  firstAuthorizer,
                                            firstAuthorizerWorkGroup,  authorizeType, errMgr, resMgr,invProp);
                                }else if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
                                        corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) {
                                    try{
                                        panelAuth(invoiceSummaryData, invoiceGroup,
                                                user,  errMgr, resMgr,panelAuthTransactionStatus,invProp);
                                    }
                                    catch(AmsException e){
                                        IssuedError is = e.getIssuedError();
                                        if (is != null) {
                                            String errorCode = is.getErrorCode();
                                            if (TradePortalConstants.REQUIRED_ATTRIBUTE.equals(errorCode) ||
                                                    TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE.equals(errorCode)){
                                                return new DocumentHandler();
                                            }
                                            else throw(e);
                                        }
                                    }
                                    
                                }
                                else {
                                    // dual auth ind is NO, first auth is EMPTY
                                    newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
                                    if (!invProp.isGroupHasBeenSaved()) {
                                        saveInvoiceGroup(newStatus, invoiceGroup, invProp);
                                        errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
                                                resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
                                                groupDescription,resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
                                        
                                    }
                                    if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
                                        invoiceSummaryData.setAttribute("invoice_group_oid",
                                                invProp.getNewGroupOid());
                                    }
                                    invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
                                    invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
                                    invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
                                    isSendToTPS = true;
                                    saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
                                }
                                
                            }
                        }
                    }
                }
		}
		return new DocumentHandler();
	}

	/* Panel Auth process for Payables Invoices*/
    private void panelAuth(InvoicesSummaryData invoiceSummaryData,InvoiceGroup invoiceGroup, User user,
    		ErrorManager errMgr,ResourceManager resMgr,String panelAuthTransactionStatus,
    		InvoiceProperties invProp)throws RemoteException, AmsException {
		String invoiceOid = invoiceSummaryData.getAttribute("upload_invoice_oid");
		String newStatus ="";
		boolean isSendToTPS=false;
		boolean isUpdateAttachment = false;

  		
		if(StringFunction.isBlank(invoiceSummaryData.getAttribute("panel_auth_group_oid"))){
			invoiceSummaryData.setAttribute("panel_auth_group_oid", invoiceGroup.getAttribute("panel_auth_group_oid"));
			invoiceSummaryData.setAttribute("panel_oplock_val", invoiceGroup.getAttribute("panel_oplock_val"));
		}
       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
    	   newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

			if (!invProp.isGroupHasBeenSaved()) {
				saveInvoiceGroup(newStatus, invoiceGroup,invProp);
				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
							resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription(),
							resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
				
			}

			invoiceSummaryData.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid",invProp.getUserWorkGroupOid());
			invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			
			isSendToTPS= true;
			isUpdateAttachment = true;
			
       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	   newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;

    	   if (!invProp.isGroupHasBeenSaved()) {
    		  saveInvoiceGroup(newStatus, invoiceGroup,invProp);
				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
						resMgr.getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription());
			}

			invoiceSummaryData.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
			invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
			invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
			invoiceSummaryData.setAttribute("invoice_status", newStatus);
			
       }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
    	   newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;

			if (!invProp.isGroupHasBeenSaved()) {
				saveInvoiceGroup(newStatus, invoiceGroup,invProp);
			}
       } 
       if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
			invoiceSummaryData.setAttribute("invoice_group_oid",
					invProp.getNewGroupOid());
		}
       saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp, true);
       
	
    }
    
    /*
	// If dual auth is required and f its not been previously authorized then set the status to partially authorized.
	// set the status to authorized if there has been a previous authorizer.
	// The second authorizer must not be the same as the first authorizer
	// not the same work groups, depending on the corporate org setting.
	// If dual authorization is not required, set the invoice state to authorized.
	*/   
    private void authoriseWithDiffWorkGroup(InvoicesSummaryData invoiceSummaryData,InvoiceGroup invoiceGroup, String firstAuthorizer
    		,String firstAuthorizerWorkGroup,String authorizeType,
    		ErrorManager errMgr,ResourceManager resMgr,InvoiceProperties invProp) throws RemoteException, AmsException
    	{
    		String invoiceOid = invoiceSummaryData.getAttribute("upload_invoice_oid");
    		String newStatus="";
    		
    		boolean isSendToTPS=false;
    		boolean isUpdateAttachment = false;
    		if (StringFunction.isBlank(firstAuthorizer)) {
				// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
				  newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;

				if (!invProp.isGroupHasBeenSaved()) {
					saveInvoiceGroup(newStatus, invoiceGroup,invProp);
					errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
								resMgr.getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription());
					
				}

				invoiceSummaryData.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
				invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
			}
			else {// firstAuthorizer not empty
				if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
						&& invProp.getUserWorkGroupOid().equals(firstAuthorizerWorkGroup)) {
					// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
					if (!invProp.isGroupHasBeenSaved()) {
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
						
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					
				}
				else if (firstAuthorizer.equals(invProp.getUserOid())) {
					// Two authorizers required and this user is the same as the
					// first authorizer. Give error.
					// Note requiring two work groups also implies requiring two users.
					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
					if (!invProp.isGroupHasBeenSaved()) {
						
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
						
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					
				}
				else {// Authorize
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;

					if (!invProp.isGroupHasBeenSaved()) {
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
								resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription(),
								resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
					}

					invoiceSummaryData.setAttribute("second_authorizing_user_oid", invProp.getUserOid());
					invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
					invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					isSendToTPS= true;
					isUpdateAttachment = true;
				}
				
			}
    		if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
				invoiceSummaryData.setAttribute("invoice_group_oid",
						invProp.getNewGroupOid());
			}
    		 saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
    	     
    	}
  
	private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("InvoiceGroupMediatorBean:::performNotifyPanelAuthUser");
		String invoiceGroupOid = inputDoc.getAttribute("/InvoiceGroup/invoiceGroupOid");
		 
    	 // create email for all selected users to notification
	    List<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragmentsList("/EmailList/user");
	    StringBuilder emailReceivers = new StringBuilder();
	    String userOid = null;
	    String ownerOrgOid = null;
	    DocumentHandler outputDoc = new DocumentHandler();
	    for(DocumentHandler doc : beneficiaryDetailsList){
		    userOid = doc.getAttribute("/userOid");
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String userMailID = user.getAttribute("email_addr");
			ownerOrgOid = user.getAttribute("owner_org_oid");
			if(StringFunction.isNotBlank(userMailID)){
				emailReceivers.append(userMailID).append(",");
			}
	    }

	    if(StringFunction.isNotBlank(emailReceivers.toString())){
	      		// Get the data from transaction that will be used to build the email message
				String amount = "";
				String currency = "";
				if (StringFunction.isNotBlank(invoiceGroupOid)) {
					
					String groupAmountsql = "select AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ? ";
					DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
					if(invoiceAmountDoc !=null){
						currency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
						amount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
						amount = TPCurrencyUtility.getDisplayAmount(amount, currency, mediatorServices.getResourceManager().getResourceLocale());
					}
				}

				StringBuilder emailContent = new StringBuilder();
				 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBodyInvoice", TradePortalConstants.TEXT_BUNDLE));
				 emailContent.append(TradePortalConstants.NEW_LINE);
					
				// Include PayRemit Amount and currency
				if (StringFunction.isNotBlank(amount)) {
					emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
					emailContent.append(currency+" "+amount);
					emailContent.append(TradePortalConstants.NEW_LINE);
				}
				
				emailContent.append(TradePortalConstants.NEW_LINE);
				// call generic method to trigger email
				InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
		  }
        
		return outputDoc;
	}
	
	private void saveInvoiceGroup(String newStatus, InvoiceGroup invoiceGroup, InvoiceProperties invProp) throws RemoteException, AmsException{
			
			int success=0;
			if (StringFunction.isNotBlank(newStatus) && !newStatus.equals(invoiceGroup.getAttribute("invoice_status")))
			{
				invoiceGroup.setAttribute("invoice_status", newStatus);
				String newGroupOid = InvoiceUtility.findMatchingInvoiceGroup(invoiceGroup);
				invProp.setNewGroupOid(newGroupOid);
				// check that another matching group does not already exist.
				if (StringFunction.isNotBlank(newGroupOid)
						&& !newGroupOid.equals(invoiceGroup.getAttribute("invoice_group_oid"))) {
					// there is another match, so assign the other group's OID
					// to these invoices and delete this group
					success = invoiceGroup.delete();
				}
				else {
					success = invoiceGroup.save();
				}
			}
			if (success == 1) {
				invProp.setGroupHasBeenSaved(true);
			}
	}
	
	private void saveInvoiceSummaryData(String newStatus, InvoicesSummaryData invoiceSummaryData,String invoiceOid,
			boolean isSendToTPS,boolean isUpdateAttachment,InvoiceProperties invProp) throws RemoteException, AmsException{
		saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, 
				isSendToTPS, isUpdateAttachment, invProp, false);
	}
	
	private void saveInvoiceSummaryData(String newStatus, InvoicesSummaryData invoiceSummaryData,String invoiceOid,
			boolean isSendToTPS,boolean isUpdateAttachment,InvoiceProperties invProp, boolean isPanelAuth) throws RemoteException, AmsException
			{
		String newGroupOid =invoiceSummaryData.getAttribute("invoice_group_oid");
	    invoiceSummaryData.setAttribute("invoice_status", newStatus);
	    String tpName=invoiceSummaryData.getTpRuleName();
        invoiceSummaryData.setAttribute("tp_rule_name", tpName);
        invoiceSummaryData.setAttribute("action",  TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
        invoiceSummaryData.setAttribute("user_oid", invProp.getUserOid());
        invoiceSummaryData.setAttribute("reqPanleAuth", isPanelAuth? TradePortalConstants.INDICATOR_YES:TradePortalConstants.INDICATOR_NO);
		if(invoiceSummaryData.save() == 1) {
			if(isSendToTPS)
			InvoiceUtility.createOutgoingQueueAndIssueSuccess(newStatus,
					invoiceOid, false, invProp.getMediatorServices());
			if(isUpdateAttachment)
				updateAttachmentDetails(newGroupOid,invProp.getMediatorServices());
		}
	}
	
	/**CR 913 - Rel 9.0
	 * This method changes the status of Invoice Group to 'Available for Authorisation'
	 *
	 * @param inputDoc
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler resetToAuthorize(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.resetToAuthorize: Start: ");
		
		List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
		if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceResetToAuthorizeText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of invoice groups selected: "
				+ invoiceGroupList.size());

            // Process each Invoice Group in the list
            for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
                String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
                String invoiceGroupOid = invoiceData[0];
                String optLock = invoiceData[1];
                InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
                        Long.parseLong(invoiceGroupOid));
                String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
                String groupDescription = invoiceGroup.getDescription();
                
                // only invoice status 'Available for Authorisation' and 'Partially Authorised' invoices can be
                // reset to authorise
                if (!TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(invoiceStatus)) {
                    mediatorServices.getErrorManager().issueError(
                            PayableInvoiceGroupMediator.class.getName(),
                            TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
                            mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
                            groupDescription,
                            ReferenceDataManager.getRefDataMgr().getDescr("UPLOAD_INVOICE_STATUS", invoiceStatus, mediatorServices.getCSDB().getLocaleName()),
                            mediatorServices.getResourceManager().getText("InvoiceSummary.ResetAuthorised", TradePortalConstants.TEXT_BUNDLE));
                    
                    continue; // Move on to next Invoice Group
                }
                
                String newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AVL_FOR_AUTH;
                String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
                DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
                if (invoiceListDoc != null) {
                    List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
                    for (DocumentHandler doc : records) {
                        String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
                        
                        if (StringFunction.isNotBlank(invoiceOid)) {
                            InvoicesSummaryData invoiceSummaryData =
                                    (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
                                            Long.parseLong(invoiceOid));
                            
                            invoiceSummaryData.setAttribute("invoice_status",newStatus);
                            invoiceSummaryData.setAttribute("tp_rule_name", invoiceSummaryData.getTpRuleName());
                            invoiceSummaryData.setAttribute("panel_auth_group_oid", null);
                            invoiceSummaryData.setAttribute("panel_oplock_val", null);
                            invoiceGroup.setAttribute("panel_auth_range_oid", null);
                            invoiceSummaryData.save();
                            try {
                                invoiceSummaryData.remove();
                            } catch (RemoveException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                invoiceGroup.setAttribute("invoice_status", newStatus);
                invoiceGroup.setAttribute("opt_lock", optLock);
                invoiceGroup.setAttribute("trading_partner_name", groupDescription);
                invoiceGroup.setAttribute("panel_auth_group_oid", null);
                invoiceGroup.setAttribute("panel_oplock_val", null);
                invoiceGroup.setAttribute("panel_auth_range_oid", null);
                int success = invoiceGroup.save();
                
                if(success ==1){
                    String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'INVOICE_GROUP' and p_owner_oid = ?";
                    try {
                        DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, false, new Object[]{invoiceGroupOid});
                    } catch (SQLException e) {
                        LOG.debug("PayableInvoiceGroupMediatorBean.resetToAUthorize: Error in deleting from panel_authorizer");
                        e.printStackTrace();
                    }
                    // display message
                    mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
                            TradePortalConstants.INV_RESET_TO_AUTHORIZE,
                            mediatorServices.getResourceManager().getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
                            groupDescription);
                }
            }
		return new DocumentHandler();
	}
	
	
	/**
	 * This method performs to set invoice status as 'Deleted'.
	 * this method doesn't delete invoice from database, this only set status as deleted invoice.
	 */
	private DocumentHandler applyCreditNoteToInvoices(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyCreditNoteToInvoices: Start: ");
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		String action = null;
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_MANUAL_APPLY)) {//TODO
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoices.ApplyCreditNote",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		//get all the invoice oids selected for which creditNote is to be applied.
		Vector invOids           = inputDoc.getFragments("/UploadCreditNote/InvoicesSummaryData");

		if (creditNoteList.size() > 1) {//throw error if more than 1 creditnote is seleced
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_SINGLE_CREDIT_NOTE);
			return new DocumentHandler();
		}
		
		invOids= getAllInvOisdInVector(invOids,mediatorServices);
		String corpOrgOid = inputDoc.getAttribute("/UploadCreditNote/ownerOrg");
		CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
				Long.parseLong(corpOrgOid));
		String  creditNoteApplyMethod = corporateOrganization.getAttribute("apply_pay_credit_note");
		DocumentHandler invoiceGroupItem = creditNoteList.get(0);
		String[] creditData = invoiceGroupItem.getAttribute("CreditData").split("/");
		String cnOid = creditData[0];
		CreditNotes cn = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(cnOid)); 
		if (!isValidateCRNote(cn, creditNoteApplyMethod, mediatorServices,true)){
			return new DocumentHandler();
		}
		if ( !isValidInvoiceStatusToApplyCred(invOids, mediatorServices) )
		{
		   return new DocumentHandler();
		}

		String  sequentialApplyMethod = corporateOrganization.getAttribute("pay_credit_sequential_type");

		int success =0;
		StringBuilder appliedInvDetailToCred = new StringBuilder("");
		String CreditNoteUpdateInd = TradePortalConstants.CREDIT_NOTE_NEW;
		if( TradePortalConstants.CREDIT_NOTE_STATUS_STB.equals(cn.getAttribute("credit_note_status")) || 
				TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL.equals(cn.getAttribute("credit_note_status")) ){
			CreditNoteUpdateInd = TradePortalConstants.CREDIT_NOTE_UPDATE;
		}
		//for proportionate application
		if(TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE.equals(creditNoteApplyMethod)){
		//using invoiceOids get the total amount of the selected invoices(use sql).
			success = applyCreditNoteProportionately(userOid, invOids, getInvoiceTotalAmt(invOids), cn, appliedInvDetailToCred, mediatorServices);//TODO
		}
		//for Sequential application

		else if(TradePortalConstants.CREDIT_NOTE_APPLICATION_SEQUENTIAL.equals(creditNoteApplyMethod)){
		
		success =applySequentialCreditNote(userOid, invOids, cn, sequentialApplyMethod, appliedInvDetailToCred, mediatorServices);
		}
		LOG.debug("Credit Note Applied Deatil to Invoice:" + appliedInvDetailToCred.toString());
		if(success==1){
			
		 String messageID = InstrumentServices.getNewMessageID();
		 String preProcessParameter = constCreditNoteProcessParam(CreditNoteUpdateInd, cn.getAttribute("credit_note_applied_status"), appliedInvDetailToCred, messageID, mediatorServices);
 
		
		 InvoiceUtility.createOutgoingQueueAndIssueSuccess( cn.getAttribute("credit_note_status"),
				 cnOid, false, mediatorServices, preProcessParameter, messageID);	
		 mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CRNOTE_APPLIED_SUCCESSFULLY);
		}
		return new DocumentHandler();
	}

	private int applySequentialCreditNote(String userOid, Vector invOids, CreditNotes cn,String sequentialApplyMethod, StringBuilder appliedInvDetailToCred, 
			MediatorServices mediatorServices) throws RemoteException, AmsException{
		Vector invList = null;
		if(TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_INVOICE_ID.equals(sequentialApplyMethod)){
		    invList = getInvs(invOids, "order by invoice_id asc",mediatorServices);
		  }
		else if(TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_DATES.equals(sequentialApplyMethod)){
		invList = getInvs(invOids, "order by (case when payment_date is null then due_date else payment_date end) asc",mediatorServices);
		  }
		else if(TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_HIGHTOLOW.equals(sequentialApplyMethod)){
		invList = getInvs(invOids, "order by (case when payment_amount is null then amount else payment_amount end) desc",mediatorServices);

		  }
		else if(TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_LOWTOHIGH.equals(sequentialApplyMethod)){
		invList = getInvs(invOids, "order by (case when payment_amount is null then amount else payment_amount end) asc",mediatorServices);

		  }
	BigDecimal creditNoteAmt=  cn.getAttributeDecimal("amount");
	BigDecimal totalCreditAmtApplied =cn.getAttributeDecimal("credit_note_applied_amount");//existing applied amount
	BigDecimal remainingCreditNoteAmt=creditNoteAmt.subtract(cn.getAttributeDecimal("credit_note_applied_amount"));
	InvoicesCreditNotesRelation invoicesCreditNotesRel =null;
	InvoicesSummaryData uploadInvoice = null;
	if(invList!=null){
           for (Object invList1 : invList) {
                String uploadInvoiceOid = (String) invList1;
                uploadInvoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
                        Long.parseLong(uploadInvoiceOid));
                BigDecimal existigAppliedAmt = uploadInvoice.getAttributeDecimal("total_credit_note_amount");
                BigDecimal payAmt = (StringFunction.isNotBlank(uploadInvoice.getAttribute("payment_amount")))?
                        uploadInvoice.getAttributeDecimal("payment_amount"): uploadInvoice.getAttributeDecimal("amount");
                BigDecimal	creditCanBeAppliedOnInv =	payAmt.subtract(existigAppliedAmt.negate());
                BigDecimal creditAmtApplied =null;
                if( remainingCreditNoteAmt.negate().compareTo(creditCanBeAppliedOnInv)<=0){
                    creditAmtApplied = remainingCreditNoteAmt;
                }
                else{
                    creditAmtApplied = creditCanBeAppliedOnInv.negate();//payAmt.subtract(existigAppliedAmt);
                }       
				uploadInvoice.setAttribute("total_credit_note_amount", existigAppliedAmt.add(creditAmtApplied).toPlainString());
                totalCreditAmtApplied = totalCreditAmtApplied.add(creditAmtApplied);
                //create new InvoicesCreditNotesRelBean and set the below values and save
                invoicesCreditNotesRel = (InvoicesCreditNotesRelation) mediatorServices.createServerEJB("InvoicesCreditNotesRelation");
                invoicesCreditNotesRel.newObject();
                invoicesCreditNotesRel.setAttribute("a_upload_credit_note_oid", cn.getAttribute("upload_credit_note_oid"));
                invoicesCreditNotesRel.setAttribute("a_upload_invoice_oid", uploadInvoice.getAttribute("upload_invoice_oid"));
                invoicesCreditNotesRel.setAttribute("credit_note_applied_amount", creditAmtApplied.toPlainString());
                uploadInvoice.setAttribute("tp_rule_name", uploadInvoice.getTpRuleName());
                uploadInvoice.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_APPLY);
                uploadInvoice.setAttribute("user_oid", userOid);
                //CR 914A
                int invSuccess = uploadInvoice.save();
                if(invSuccess == 1){
                    // this method is used to add invoice detail in porcess_parameter then process_parameetr is use to generate INV message
                    // with detail of applied invoices.
                    addInvoiceDetailInAppliedCred(uploadInvoice.getAttribute("invoice_id"), creditAmtApplied.negate().toPlainString(), appliedInvDetailToCred);
                     }       
				invoicesCreditNotesRel.save();
                //if no balance in credit note then come out of for loop
	
	remainingCreditNoteAmt= creditNoteAmt.subtract(totalCreditAmtApplied);
                if(remainingCreditNoteAmt.negate().compareTo(BigDecimal.ZERO)<=0){
		break;
		}
            }
	}
	if(creditNoteAmt.negate().compareTo(totalCreditAmtApplied.negate())==0)
		cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_FAP);
	else if(creditNoteAmt.negate().compareTo(totalCreditAmtApplied.negate())>0)
		cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_PAP);
	//capture total creditNote Applied against all invoices and save credit note bean
	cn.setAttribute("credit_note_applied_amount",totalCreditAmtApplied.toPlainString());
	cn.setAttribute("credit_note_status",TradePortalConstants.CREDIT_NOTE_STATUS_STB);
	return cn.save();

	}
	
	private Vector getInvs(Vector invOids, String ORDERBY,MediatorServices mediatorServices) throws AmsException{
		List<Object> sqlprmLst = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder("SELECT UPLOAD_INVOICE_OID from invoices_summary_data where ");
		sb.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids, "UPLOAD_INVOICE_OID",sqlprmLst));
		sb.append(" ").append(ORDERBY);

		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sb.toString(), false, sqlprmLst);
		if (resultSet == null ){
			return new Vector();
		}

		List<DocumentHandler> resultSetVec = resultSet.getFragmentsList("/ResultSetRecord");
		Vector tempOids = new Vector();

		for (DocumentHandler resultDoc : resultSetVec) {
			tempOids.add(resultDoc.getAttribute("UPLOAD_INVOICE_OID"));
		}

		return tempOids;
	}
	
	private BigDecimal getInvoiceTotalAmt(Vector invOids) throws AmsException{
		List<Object> sqlprmLst = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder("select sum( case when (payment_amount is null or payment_amount=0) then ( NVL(amount,0) +  NVL(total_credit_note_amount,0)) else (NVL(payment_amount,0) + NVL(total_credit_note_amount,0)) end) as AMOUNT from invoices_summary_data  where ");
		sb.append(POLineItemUtility.getInvoiceItemOidsSqlWithPlaceHolder(invOids, "UPLOAD_INVOICE_OID",sqlprmLst));
		sb.append(" order by invoice_id asc ");
		BigDecimal  invoiceTotalAmt = null;
		DocumentHandler resultXml = DatabaseQueryBean.getXmlResultSet(sb.toString(),false,sqlprmLst);
		if (resultXml != null) {
			DocumentHandler record = resultXml.getFragment("/ResultSetRecord");
			if(record!=null){
				invoiceTotalAmt = record.getAttributeDecimal("/AMOUNT");
			}
		}

		return invoiceTotalAmt;
	}
		
	private int applyCreditNoteProportionately(String userOid, Vector invList, BigDecimal totalInvoiceAmount ,
			CreditNotes cn , StringBuilder appliedInvDetailToCred, MediatorServices mediatorServices) 
					throws NumberFormatException, RemoteException, AmsException{
		BigDecimal creditNoteAmt=  cn.getAttributeDecimal("amount");
		BigDecimal totalCreditAmtApplied =cn.getAttributeDecimal("credit_note_applied_amount");//existing applied amount
		InvoicesSummaryData invoice = null;
		InvoicesCreditNotesRelation invoicesCreditNotesRel =null;
		for (int i = 0; i < invList.size(); i++) {
			
			String uploadInvoiceOid = (String) invList.get(i);

		invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
					Long.parseLong(uploadInvoiceOid)); 

		BigDecimal payAmt = StringFunction.isNotBlank(invoice.getAttribute("payment_amount"))?
				invoice.getAttributeDecimal("payment_amount"): invoice.getAttributeDecimal("amount");
		BigDecimal existigAppliedAmt = invoice.getAttributeDecimal("total_credit_note_amount");

		BigDecimal	creditCanBeAppliedOnInv =	payAmt.subtract(existigAppliedAmt.negate());

		BigDecimal  creditAmtApplied =  (creditCanBeAppliedOnInv.multiply(creditNoteAmt)).divide(totalInvoiceAmount,2,BigDecimal.ROUND_HALF_UP);
		if(creditAmtApplied.negate().compareTo(creditCanBeAppliedOnInv) > 0 ){
			creditAmtApplied = creditCanBeAppliedOnInv.negate();
		}
		invoice.setAttribute("total_credit_note_amount", existigAppliedAmt.add(creditAmtApplied).toString());

		totalCreditAmtApplied = totalCreditAmtApplied.add( creditAmtApplied);

		//create new InvoicesCreditNotesRelBean and set the below values and save
		invoicesCreditNotesRel = (InvoicesCreditNotesRelation) mediatorServices.createServerEJB("InvoicesCreditNotesRelation");
		invoicesCreditNotesRel.newObject();
		invoicesCreditNotesRel.setAttribute("a_upload_credit_note_oid", cn.getAttribute("upload_credit_note_oid"));
		invoicesCreditNotesRel.setAttribute("a_upload_invoice_oid", invoice.getAttribute("upload_invoice_oid"));

		invoicesCreditNotesRel.setAttribute("credit_note_applied_amount", creditAmtApplied.toPlainString());
		invoice.setAttribute("tp_rule_name", invoice.getTpRuleName());
		invoice.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_APPLY);
		invoice.setAttribute("user_oid", userOid);
		//CR 914A		
		int invSuccess = invoice.save();
		if(invSuccess == 1){
			addInvoiceDetailInAppliedCred(invoice.getAttribute("invoice_id"), creditAmtApplied.negate().toPlainString(), appliedInvDetailToCred);
			
		}
		
		invoicesCreditNotesRel.save();
			if(i==invList.size()-2){
				break;
			}

		}
		if(invList.size()>1){
		// for the last invoice processing..
		String uploadInvoiceOid = (String) invList.get(invList.size()-1);
		invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
				Long.parseLong(uploadInvoiceOid)); 
		BigDecimal payAmt = StringFunction.isNotBlank(invoice.getAttribute("payment_amount"))?
				invoice.getAttributeDecimal("payment_amount"): invoice.getAttributeDecimal("amount");
		BigDecimal existigAppliedAmt = invoice.getAttributeDecimal("total_credit_note_amount");

		BigDecimal	creditCanBeAppliedOnInv =	payAmt.subtract(existigAppliedAmt);
		BigDecimal creditAmtApplied = creditNoteAmt.subtract(totalCreditAmtApplied);
		if(creditAmtApplied.negate().compareTo(creditCanBeAppliedOnInv) > 0 ){
			creditAmtApplied = creditCanBeAppliedOnInv.negate();
		}
		totalCreditAmtApplied = totalCreditAmtApplied.add( creditAmtApplied);
		invoice.setAttribute("total_credit_note_amount", existigAppliedAmt.add(creditAmtApplied).toString());
		
		//create new InvoicesCreditNotesRelBean and set the below values and save
		invoicesCreditNotesRel = (InvoicesCreditNotesRelation) mediatorServices.createServerEJB("InvoicesCreditNotesRelation");
		invoicesCreditNotesRel.newObject();
		invoicesCreditNotesRel.setAttribute("a_upload_credit_note_oid", cn.getAttribute("upload_credit_note_oid"));
		invoicesCreditNotesRel.setAttribute("a_upload_invoice_oid", invoice.getAttribute("upload_invoice_oid"));

		invoicesCreditNotesRel.setAttribute("credit_note_applied_amount", creditAmtApplied.toPlainString());
		invoice.setAttribute("tp_rule_name", invoice.getTpRuleName());
		invoice.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_APPLY);
		invoice.setAttribute("user_oid", userOid);
		int invSuccess = invoice.save();
		if(invSuccess == 1){
			addInvoiceDetailInAppliedCred(invoice.getAttribute("invoice_id"), creditAmtApplied.negate().toPlainString(), appliedInvDetailToCred);
			}
		
		invoicesCreditNotesRel.save();
		}
		if(creditNoteAmt.negate().compareTo(totalCreditAmtApplied.negate())==0)
			cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_FAP);
		else if(creditNoteAmt.negate().compareTo(totalCreditAmtApplied.negate())>0)
			cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_PAP);
		//capture total creditNote Applied against all invoices and save credit note bean
		cn.setAttribute("credit_note_applied_amount",totalCreditAmtApplied.toPlainString());
		cn.setAttribute("credit_note_status",TradePortalConstants.CREDIT_NOTE_STATUS_STB);
		return cn.save();
		
	}
	
	private DocumentHandler unApplyCreditNoteToInvoices(DocumentHandler inputDoc,
			MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.applyCreditNoteToInvoices: Start: ");
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_MANUAL_UNAPPLY)) {//TODO
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoices.UnApplyCreditNote",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		Vector<DocumentHandler> creditNoteList = inputDoc.getFragments("/UploadCreditNote/CreditNote");
		//get all the invoice oids selected for which creditNote is to be applied.
		Vector invOids           = inputDoc.getFragments("/UploadCreditNote/InvoicesSummaryData");

		if (creditNoteList.size() > 1) {//throw error if more than 1 creditnote is seleced
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SELECT_SINGLE_CREDIT_NOTE);
			return new DocumentHandler();
		}
		

		String corpOrgOid = inputDoc.getAttribute("/UploadCreditNote/ownerOrg");
		CorporateOrganization corporateOrganization = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
				Long.parseLong(corpOrgOid));
		String  creditNoteApplyMethod = corporateOrganization.getAttribute("apply_pay_credit_note");
		DocumentHandler invoiceGroupItem = creditNoteList.get(0);
		String[] creditData = invoiceGroupItem.getAttribute("CreditData").split("/");
		String cnOid = creditData[0];
		CreditNotes cn = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(cnOid)); 
		cn.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_UNAPPLY);
		cn.setAttribute("user_oid", userOid);
		if(!isValidateCRNote(cn, creditNoteApplyMethod, mediatorServices, false)){
			return new DocumentHandler();
		}
		
		String  sequentialApplyMethod = corporateOrganization.getAttribute("pay_credit_sequential_type");
		StringBuilder appliedInvDetailToCred = new StringBuilder("");
		String CreditNoteUpdateInd = TradePortalConstants.CREDIT_NOTE_NEW;
		if( TradePortalConstants.CREDIT_NOTE_STATUS_STB.equals(cn.getAttribute("credit_note_status")) || 
				TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL.equals(cn.getAttribute("credit_note_status")) ){
			CreditNoteUpdateInd = TradePortalConstants.CREDIT_NOTE_UPDATE;
		}
        unApplyAmounts(userOid, invOids, cn, appliedInvDetailToCred, mediatorServices);
        invOids = getRemainingInvoiceOids(userOid, cn, mediatorServices);
		int success =0;
		if(invOids!=null && invOids.size()>0){
		//for proportionate application
		if(TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE.equals(creditNoteApplyMethod)){
		//using invoiceOids get the total amount of the selected invoices(use sql).
			success = applyCreditNoteProportionately(userOid, invOids, getInvoiceTotalAmt(invOids), cn, appliedInvDetailToCred, mediatorServices);//TODO
		}
		//for Sequential application

		else if(TradePortalConstants.CREDIT_NOTE_APPLICATION_SEQUENTIAL.equals(creditNoteApplyMethod)){
		
		success = applySequentialCreditNote(userOid, invOids, cn, sequentialApplyMethod, appliedInvDetailToCred, mediatorServices);
		}
		}
		else{
			success =cn.save();
			
		}
		LOG.debug("Credit note Invoice Detail for Un-Apply Action"+ appliedInvDetailToCred);
		if(success==1){
			 String messageID = InstrumentServices.getNewMessageID();
			 String preProcessParameter = constCreditNoteProcessParam(CreditNoteUpdateInd, cn.getAttribute("credit_note_applied_status"), appliedInvDetailToCred, messageID, mediatorServices);		 
			 InvoiceUtility.createOutgoingQueueAndIssueSuccess( cn.getAttribute("credit_note_status"),
					 cnOid, false, mediatorServices, preProcessParameter, messageID);
		     mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CRNOTE_UNAPPLIED_SUCCESSFULLY);
		}
		return new DocumentHandler();
	}
	
	private void unApplyAmounts(String userOid, Vector invOids, CreditNotes cn, StringBuilder appliedInvDetailToCred,
			MediatorServices mediatorServices) throws NumberFormatException, RemoteException, AmsException{
		InvoicesSummaryData invoice = null;
		InvoicesCreditNotesRelation invoicesCreditNotesRel =null;
		BigDecimal creditNoteAppliedAmount = cn.getAttributeDecimal("credit_note_applied_amount");
		for (int i = 0; i < invOids.size(); i++) {
			DocumentHandler invoiceItem = (DocumentHandler) invOids.get(i);
			String[] invoiceData = invoiceItem.getAttribute("InvoiceData").split("/");
			String uploadInvoiceOid = invoiceData[0];
			//String optLock = invoiceData[1];
		invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
					Long.parseLong(uploadInvoiceOid)); 
		BigDecimal payAmt = StringFunction.isNotBlank(invoice.getAttribute("payment_amount"))?
				invoice.getAttributeDecimal("payment_amount"): invoice.getAttributeDecimal("amount");
		BigDecimal existigAppliedAmt = invoice.getAttributeDecimal("total_credit_note_amount");

		StringBuilder sqlStatement = new StringBuilder()
        .append(" SELECT invoice_credit_note_rel_oid, credit_note_applied_amount FROM INVOICES_CREDIT_NOTES_RELATION WHERE a_upload_invoice_oid = ? AND a_upload_credit_note_oid = ?");
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), true, new Object[]{uploadInvoiceOid, cn.getAttribute("upload_credit_note_oid")});
		String relOID = resultXML.getAttribute("/ResultSetRecord(0)/INVOICE_CREDIT_NOTE_REL_OID");
		BigDecimal creditAmtApplied = resultXML.getAttributeDecimal("/ResultSetRecord(0)/CREDIT_NOTE_APPLIED_AMOUNT");
		invoice.setAttribute("total_credit_note_amount", existigAppliedAmt.subtract(creditAmtApplied).toString());
		creditNoteAppliedAmount = creditNoteAppliedAmount.subtract(creditAmtApplied);
		//delete the existing InvoicesCreditNotesRelBean 
		invoicesCreditNotesRel = (InvoicesCreditNotesRelation) mediatorServices.createServerEJB("InvoicesCreditNotesRelation",
				Long.parseLong(relOID)); 
		
		invoice.setAttribute("tp_rule_name", invoice.getTpRuleName());
		invoice.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_UNAPPLY);
		invoice.setAttribute("user_oid", userOid);
		//CR 914A
		int invSuccess = invoice.save();
		if(invSuccess == 1){
			addInvoiceDetailInAppliedCred(invoice.getAttribute("invoice_id"), creditAmtApplied.toPlainString(), appliedInvDetailToCred);
			}
		
		invoicesCreditNotesRel.delete();
		BigDecimal creditNoteAmt = cn.getAttributeDecimal("amount");
		if (creditNoteAppliedAmount.compareTo(BigDecimal.ZERO) == 0) {
			cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_UAP);
		} else if(creditNoteAmt.negate().compareTo(creditNoteAppliedAmount.negate())==0) {
			cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_FAP);
		} else if(creditNoteAmt.negate().compareTo(creditNoteAppliedAmount.negate())>0) {
			cn.setAttribute("credit_note_applied_status",TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_PAP);
		}
		cn.setAttribute("credit_note_applied_amount", creditNoteAppliedAmount.toPlainString());
	}
	}
	//get remaining invoice which are still assinged to CRnote and prepare it for reapply of credit note
	private Vector getRemainingInvoiceOids( String userOid, CreditNotes cn,MediatorServices mediatorServices) throws AmsException, RemoteException {
		StringBuilder sqlStatement = new StringBuilder()
        .append(" SELECT a_upload_invoice_oid , invoice_credit_note_rel_oid, credit_note_applied_amount " +
        		" FROM invoices_credit_notes_relation a, invoices_summary_data b  " +
        		" WHERE a.a_upload_invoice_oid =  b.upload_invoice_oid AND a.a_upload_credit_note_oid = ? " +
        		" AND b.invoice_status not in (?, ?, ?) ");
		String uploadCredNoteOid = SQLParamFilter.filter(cn.getAttribute("upload_credit_note_oid"));
        DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), true, new Object[] {uploadCredNoteOid, "PROCESSED_BY_BANK", "AUTHORIZED", "ASN"});
		Vector oidsList = new Vector();
		if(resultXML!=null)
		{
		  InvoicesCreditNotesRelation invoicesCreditNotesRel=null;
		  InvoicesSummaryData invoice = null;
		  BigDecimal creditNoteAppliedAmount = cn.getAttributeDecimal("credit_note_applied_amount");
		  List<DocumentHandler> rows = resultXML.getFragmentsList("/ResultSetRecord");
		  for (DocumentHandler row : rows) {
			String invOid = row.getAttribute("/A_UPLOAD_INVOICE_OID");
			oidsList.addElement(invOid);
			
			invoice = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",
					Long.parseLong(invOid)); 
			BigDecimal payAmt = StringFunction.isNotBlank(invoice.getAttribute("payment_amount"))?
				invoice.getAttributeDecimal("payment_amount"): invoice.getAttributeDecimal("amount");
			BigDecimal existigAppliedAmt = invoice.getAttributeDecimal("total_credit_note_amount");
			BigDecimal creditAmtApplied = row.getAttributeDecimal("/CREDIT_NOTE_APPLIED_AMOUNT");
			creditNoteAppliedAmount = creditNoteAppliedAmount.subtract(creditAmtApplied);
			invoice.setAttribute("total_credit_note_amount", existigAppliedAmt.subtract(creditAmtApplied).toString());
			
			invoice.setAttribute("tp_rule_name", invoice.getTpRuleName());
			invoice.setAttribute("action", TradePortalConstants.UPLOAD_CR_ACTION_UNAPPLY);
			invoice.setAttribute("user_oid", userOid);
			int success = invoice.save();
			
			invoicesCreditNotesRel = (InvoicesCreditNotesRelation) mediatorServices.createServerEJB("InvoicesCreditNotesRelation",
					Long.parseLong(row.getAttribute("/INVOICE_CREDIT_NOTE_REL_OID"))); 
			
			invoicesCreditNotesRel.delete();
			}
		  cn.setAttribute("credit_note_applied_amount", creditNoteAppliedAmount.toPlainString());
		}
		return oidsList; 		
	}
	
	private Vector getAllInvOisdInVector(Vector invOids,MediatorServices mediatorServices )  {
		Vector oidsList = new Vector();
		for (int i = 0; i < invOids.size(); i++) {
			DocumentHandler invoiceItem = (DocumentHandler) invOids.get(i);
			String[] invoiceData = invoiceItem.getAttribute("InvoiceData").split("/");
			String uploadInvoiceOid = invoiceData[0];
			oidsList.addElement(uploadInvoiceOid);
		}
		return oidsList;
		
	}
	private boolean isValidateCRNote( CreditNotes cn, String creditNoteApplyMethod, MediatorServices mediatorServices,boolean isApply) 
			throws AmsException, RemoteException {
		
		if(TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE.equals(creditNoteApplyMethod)){
		  if (!isValidateCRNoteGroup(cn,mediatorServices,isApply)) {//throw error if already group member is authorised
			  return false;
		  }
		}
		if (StringFunction.isNotBlank(cn.getAttribute("end_to_end_id"))) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.END_TO_END_CR_NOTE);
			return false;
		}
		String crStatus = cn.getAttribute("credit_note_status");
		if(TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(crStatus)){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PARTIALLY_AUTH_CRNOTE_ERROR);
			return false;
		
		}
		if(!(TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(crStatus) || TradePortalConstants.CREDIT_NOTE_STATUS_PBB_BAL.equals(crStatus)
				|| TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(crStatus) || TradePortalConstants.CREDIT_NOTE_STATUS_STB.equals(crStatus))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.CRNOTE_INVALID_STATUS_ERROR);
			return false;
			
		}
		
		if(isApply && TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS_FAP.equals(cn.getAttribute("credit_note_applied_status"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.FULLY_APPLIED_CR_NOTE);
			return false;
		
		}
		
		//Condition added for CR 1006
		if(TradePortalConstants.INDICATOR_YES.equals(cn.getAttribute("portal_approval_requested_ind"))){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.H2H_CREDIT_NOTES_CANNOT_BE_APPLIED);
			return false;
		}
		
		Date todayDate = GMTUtility.getGMTDateTime();
		if(StringFunction.isNotBlank(cn.getAttribute("expiry_date")) ){
			if(TPDateTimeUtility.convertDateStringToDate(cn.getAttribute("expiry_date"),"MM/dd/yyyy").before(todayDate)){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.EXPIRED_CR_NOTE);
				return false;
			}
		}
	    
		return true;

	}
	private boolean isValidateCRNoteGroup( CreditNotes cn,MediatorServices mediatorServices, boolean isApply) throws AmsException, RemoteException {
		String sqlQuery ="select i.upload_invoice_oid as oid from invoices_summary_data i,INVOICES_CREDIT_NOTES_RELATION c " +
		"where  c.a_upload_invoice_oid = i.upload_invoice_oid and i.invoice_status in (?,?)"+
		"and c.a_upload_credit_note_oid =?";
		
		String oid="";
		DocumentHandler result = null;
		try {
			result = DatabaseQueryBean.getXmlResultSet(
			sqlQuery,true,TradePortalConstants.UPLOAD_INV_STATUS_AUTH,
			TransactionStatus.PROCESSED_BY_BANK,cn.getAttribute("upload_credit_note_oid"));
		} catch (AmsException e) {
			e.printStackTrace();
		}
		if (result!=null){
			oid = result.getAttribute("/ResultSetRecord(0)/OID");
		}
		
		if( StringFunction.isNotBlank(oid)){
			String err = isApply?TradePortalConstants.CANNOT_APPLY_CR_NOTE: TradePortalConstants.CANNOT_UNAPPLY_CR_NOTE;
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,err);
			return false;
		}
		return true;
	}
		

 // Nar CR 914A Release9.2 11/20/2014 Begin
	/**
	 * This method is used to Delete credit notes. Only credit notes with a credit note status 
	 * of "Available for Authorisation" with a credit note applied status of "Not Applied" can be deleted by a user 
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler deletePayCreditNotes( DocumentHandler inputDoc, MediatorServices mediatorServices )
	throws RemoteException, AmsException {

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.deletePayCreditNotes: Start: ");
		DocumentHandler outputDoc = new DocumentHandler();
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_DELETE)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText( "UploadInvoiceAction.Delete", TradePortalConstants.TEXT_BUNDLE));
			return outputDoc;
		}

		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList == null || creditNoteList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText( "CreditNotesAction.DeleteCreditNote", TradePortalConstants.TEXT_BUNDLE ) );
			return outputDoc;
		}
		mediatorServices.debug("PayableInvoiceGroupMediatorBean: number of credit notes selected: " + creditNoteList.size());

		// Process each Credit Note in the list
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[] creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote              = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteId                 = creditNote.getAttribute("invoice_id");
			String creditNoteStatus             = creditNote.getAttribute("credit_note_status");
			String creditNoteAppliedStatus      = creditNote.getAttribute("credit_note_applied_status");
			if ( !InvoiceUtility.isPayableCreditNoteDeletable( creditNoteStatus, creditNoteAppliedStatus ) )
			{
				mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),
						creditNoteId,
						ReferenceDataManager.getRefDataMgr().getDescr("CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
				continue; // Move on to next Credit Note

			}
			else if ( isCreditNoteExpired(creditNote) ) 
			{
				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.CREDIT_NOTE_EXPIRY_DATE_EXPIRED, creditNoteId );
				continue; // Move on to next Credit Note
			}
			creditNote.setAttribute("credit_note_status", TradePortalConstants.CREDIT_NOTE_STATUS_DEL);
			creditNote.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_DELETE);
			creditNote.setAttribute("user_oid", userOid);
			int deleteSuccess = creditNote.save();
			if (deleteSuccess == 1) {
				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(), 
						TradePortalConstants.DELETE_SUCCESSFUL,
						mediatorServices.getResourceManager().getText( "PayableCreditNote.PayCreditNote", TradePortalConstants.TEXT_BUNDLE)
						+ " " + creditNoteId);
			}

		}
		mediatorServices.debug("PayableInvoiceGroupMediatorBean.deletePayCreditNotes: End ");
		return outputDoc;
	}
	
	/**
	 * This method is used to Delete credit notes. Only credit notes with a credit note status 
	 * of "Available for Authorisation" with a credit note applied status of "Not Applied" can be deleted by a user 
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler closePayCreditNotes( DocumentHandler inputDoc, MediatorServices mediatorServices )
	throws RemoteException, AmsException {

		mediatorServices.debug("PayableInvoiceGroupMediatorBean.closePayCreditNotes: Start: ");
		DocumentHandler outputDoc = new DocumentHandler();
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		if ( !SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_CLOSE) ) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText( "UploadInvoiceAction.Close", TradePortalConstants.TEXT_BUNDLE));
			return outputDoc;
		}

		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList == null || creditNoteList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText( "CreditNotesAction.DeleteCreditNote", TradePortalConstants.TEXT_BUNDLE ) );
			return outputDoc;
		}
		mediatorServices.debug("PayableInvoiceGroupMediatorBean: number of credit notes selected: " + creditNoteList.size());

		// Process each Credit Note in the list
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[] creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote              = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteId                 = creditNote.getAttribute("invoice_id");
			String creditNoteStatus             = creditNote.getAttribute("credit_note_status");
			String creditNoteAppliedStatus      = creditNote.getAttribute("credit_note_applied_status");
			if ( !InvoiceUtility.isPayableCreditNoteClosable( creditNoteStatus, creditNoteAppliedStatus ) )
			{
				mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),
						creditNoteId,
						ReferenceDataManager.getRefDataMgr().getDescr("CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("UploadInvoiceAction.Deleted", TradePortalConstants.TEXT_BUNDLE));
				continue; // Move on to next Credit Note

			}
			else if ( !isCreditNoteExpired(creditNote) ) 
			{
				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.CREDIT_NOTE_EXPIRY_DATE_NOT_EXPIRED, creditNoteId );
				continue; // Move on to next Credit Note
			}
			creditNote.setAttribute("credit_note_status", TradePortalConstants.CREDIT_NOTE_STATUS_CLO);
			creditNote.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_CLOSE);
			creditNote.setAttribute("user_oid", userOid);
			int closeSuccess = creditNote.save();
			if (closeSuccess == 1) {
				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(), 
						TradePortalConstants.CLOSE_SUCCESSFUL, creditNoteId );
			}

		}
		mediatorServices.debug("PayableInvoiceGroupMediatorBean.closePayCreditNotes: End: ");
		return outputDoc;
	}
	
	
	
	/**This method changes the status of Credit Note to 'Available for Authorisation'
	 *
	 * @param inputDoc
	 * @param mediatorServices
	 * @return DocumentHandler
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler resetCreditNoteToAuthorize( DocumentHandler inputDoc, MediatorServices mediatorServices )
	throws RemoteException, AmsException {
		
		mediatorServices.debug("InvoiceGroupMediatorBean.resetCreditNoteToAuthorize: Start: ");	
		DocumentHandler outputDoc = new DocumentHandler();
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList == null || creditNoteList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceResetToAuthorizeText",
							TradePortalConstants.TEXT_BUNDLE));
			return outputDoc;
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of Credit Note selected: " + creditNoteList.size());

		// Process each Invoice Group in the list
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[] creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteId     = creditNote.getAttribute("invoice_id");
			String creditNoteStatus = creditNote.getAttribute("credit_note_status");

			// only credit note status 'Authorize Failed' and 'Partially Authorised' invoices can be
			// reset to authorise
			if ( !InvoiceUtility.isPayableCreditNoteToReset( creditNoteStatus ) ) {
				mediatorServices.getErrorManager().issueError(
						PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
						mediatorServices.getResourceManager().getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),
						creditNoteId,
						ReferenceDataManager.getRefDataMgr().getDescr("CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
						mediatorServices.getResourceManager().getText("InvoiceSummary.ResetAuthorised", TradePortalConstants.TEXT_BUNDLE));

				continue; // Move on to next Credit Note
			}

			
				creditNote.setAttribute("credit_note_status", TradePortalConstants.CREDIT_NOTE_STATUS_AVA);
				creditNote.setAttribute("panel_auth_group_oid", null);
				creditNote.setAttribute("panel_auth_range_oid", null);
				creditNote.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_RESET_AUTH);
				creditNote.setAttribute("user_oid", userOid);
				int resetSuccess = creditNote.save();

				if(resetSuccess ==1){
				  String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'CREDIT_NOTES' and p_owner_oid= ?";
				  try {
					DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new String[]{creditNotesOid});
				  } catch (SQLException e) {
					LOG.debug("PayableInvoiceGroupMediatorBean.resetCreditNoteToAuthorize: Error in deleting from panel_authorizer");
					e.printStackTrace();
				 }
				mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.INV_RESET_TO_AUTHORIZE,
						mediatorServices.getResourceManager().getText("PayableInvoice.Invoice", TradePortalConstants.TEXT_BUNDLE),
						creditNoteId);
			 }
			}
		return new DocumentHandler();
	}
	
	/**
	 * Deletes attached documents marked by the user on a Credit Note
	 *
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 */
	private DocumentHandler deleteAttachedDocumentsFromCreditNote(DocumentHandler inputDoc, MediatorServices mediatorServices) 
	throws RemoteException, AmsException {
		
		mediatorServices.debug("InvoiceGroupMediatorBean.deleteAttachedDocumentsFromCreditNote: Start: ");	
		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceDeleteDocumentText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice group OIDs
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[]  creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteStatus = creditNote.getAttribute("credit_note_status");
			String creditNoteId     = creditNote.getAttribute("invoice_id");
			if (!InvoiceUtility.isPayableCreditNoteToDocumentAction(creditNoteStatus) )

		    {
				  mediatorServices.getErrorManager().issueError(
		                PayableInvoiceGroupMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_DELETE_DOC,
		                mediatorServices.getResourceManager().getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),
		                creditNoteId,
		                ReferenceDataManager.getRefDataMgr().getDescr("CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()));
		   	    return new DocumentHandler();
		    }

			String imageOid = checkDocumentAlreadyAttachedToCreditNote(creditNotesOid, mediatorServices, creditNoteId);
			if (StringFunction.isNotBlank(imageOid)) {

				StringBuilder doWhereClause = new StringBuilder();
				doWhereClause.append("p_transaction_oid = ? ");

				StringBuilder updateStatement = new StringBuilder();
				updateStatement.append("update document_image");
				updateStatement.append(" set p_transaction_oid = ").append(0);
				updateStatement.append(" where ").append(doWhereClause);

				Vector rowDocImageOIDs = DatabaseQueryBean.selectRowsForUpdate("document_image",
						"doc_image_oid", doWhereClause.toString(), updateStatement.toString(), false, new Object[]{creditNotesOid}, new Object[]{});

				if(rowDocImageOIDs != null){				
					mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.DOC_DELETED_SUCCESSFULLY,
								creditNoteId, "");
				}

			}
		}
		mediatorServices.debug("InvoiceGroupMediatorBean.deleteAttachedDocumentsFromCreditNote: End: ");
		return new DocumentHandler();
	}
	
	/**
	 * Attaches document to the invoice group
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler attachDocumentToCreditNote(DocumentHandler inputDoc, MediatorServices mediatorServices) 
			throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.attachDocumentToCreditNote: Start: ");
		
		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList.size() == 0) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.UploadInvoiceAttachDocumentText",
							TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}

		// Loop through the list of invoice group OIDs
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[]  creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];

			CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteStatus = creditNote.getAttribute("credit_note_status");
			String creditNoteId     = creditNote.getAttribute("invoice_id");
			if ( !InvoiceUtility.isPayableCreditNoteToDocumentAction(creditNoteStatus) ) {
		   	    mediatorServices.getErrorManager().issueError(
		                PayableInvoiceGroupMediator.class.getName(),
		                TradePortalConstants.ERR_UPLOAD_INV_CANNOT_ATTACH_DOC,
		                mediatorServices.getResourceManager().getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),
		                creditNoteId,
		                ReferenceDataManager.getRefDataMgr().getDescr("CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()));
		    }

			//Only one doc attachment allowed credit note
			//If credit note already has a doc attached, throw error
			boolean creditNoteAlreadyHasDocAttached = creditNoteAlreadyHasDocAttached(creditNotesOid);
			if (creditNoteAlreadyHasDocAttached){
				mediatorServices.getErrorManager().issueError(
						PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.ERR_UPLOAD_INV_DOC_ALREADY_ATTACHED_TO_GRP,
						creditNoteId);
			}
        }
		return new DocumentHandler();
	}
	
	/**
	 * This method is used to perform authorize action for payable credit note.
	 * 
	 * @param inputDoc
	 * @param mediatorServices
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private DocumentHandler authorizeCreditNote(DocumentHandler inputDoc,MediatorServices mediatorServices)
    throws RemoteException, AmsException {
		mediatorServices.debug("InvoiceGroupMediatorBean.authorizeCreditNote: Start: ");
		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String corpOrgOid = inputDoc.getAttribute("/UploadCreditNote/ownerOrg");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String buyerName = null;
		String userOrgOid = null;
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_AUTH)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"UploadInvoiceAction.AuthorizeCreditNote", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		ResourceManager resMgr = mediatorServices.getResourceManager();
		ErrorManager errMgr = mediatorServices.getErrorManager();
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		userOrgOid = user.getAttribute("owner_org_oid");
		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList == null || creditNoteList.size() == 0) {
			errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, resMgr.getText(
							"UploadInvoiceAction.AuthorizeCreditNote", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		mediatorServices.debug("InvoiceGroupMediatorBean: number of Credit Note selected: "+ creditNoteList.size());

		String userWorkGroupOid = user.getAttribute("work_group_oid");
		CorporateOrganization corpOrg = null;
		InvoiceProperties invProp = new InvoiceProperties();
		invProp.setCorpOrgOid(corpOrgOid);
		invProp.setUserOid(userOid);
		invProp.setUserWorkGroupOid(userWorkGroupOid);
		invProp.setMediatorServices(mediatorServices);
		Set<String> endToEndIDSet = new HashSet<String>();
		
		// Process each credit Note in the list
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[] creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteStatus = creditNote.getAttribute("credit_note_status");
			String creditNoteId = creditNote.getAttribute("invoice_id");

			String endToEndID = creditNote.getAttribute("end_to_end_id");
            if ( StringFunction.isNotBlank( endToEndID ) ) 
            {
            	if ( endToEndIDSet.add( endToEndID ) ){     
            		// only certain invoice statuses can be authorized.
        			if ( !InvoiceUtility.isPayableCreditNoteAuthorizable(creditNoteStatus) ) {
        				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(),
        						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,resMgr.getText("UploadInvoices.CreditNoteId", TradePortalConstants.TEXT_BUNDLE),
        						creditNoteId,
        						ReferenceDataManager.getRefDataMgr().getDescr(
        						"CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
        						resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
        				continue;
        			}
            		Map<String, String> endToEndIdGroupProp = new HashMap<String, String>();
					endToEndIdGroupProp.put("corpOrgOid", corpOrgOid);
					endToEndIdGroupProp.put("userOid", userOid);
					endToEndIdGroupProp.put("userWorkGroupOid", user.getAttribute("work_group_oid"));
					endToEndIdGroupProp.put("subsAuth", subsAuth);
					endToEndIdGroupProp.put("creditNotesOid", creditNotesOid);
            		String invoiceGroupOid = null;
            		String sql = " SELECT invoice_group_oid FROM invoice_group WHERE end_to_end_id = ? AND a_corp_org_oid = ? AND rownum = 1 ";
            		DocumentHandler	resultDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] {endToEndID, corpOrgOid});  
            		if ( resultDoc!= null ) {
            		   invoiceGroupOid = resultDoc.getAttribute("/ResultSetRecord(0)/INVOICE_GROUP_OID");
            		}
            		if( StringFunction.isNotBlank(invoiceGroupOid) )
            		{            			
						InvoiceGroup endToEndIDinvoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup", 
								Long.parseLong(invoiceGroupOid));
						authorizeEndToEndIDGroup(endToEndIDinvoiceGroup, endToEndIdGroupProp, mediatorServices);
            		}
            		else
            		{
            			authorizeCreditNoteEndToEndIDGroup(creditNote, endToEndIdGroupProp, mediatorServices);
            		}
            	}
            }
            else 
            {
            	// only certain invoice statuses can be authorized.
    			if ( !InvoiceUtility.isPayableCreditNoteAuthorizable(creditNoteStatus) ) {
    				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(),
    						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,resMgr.getText("UploadInvoices.CreditNoteId", TradePortalConstants.TEXT_BUNDLE),
    						creditNoteId,
    						ReferenceDataManager.getRefDataMgr().getDescr(
    						"CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
    						resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
    				continue;
    			}
			String creditNoteAmount = null;
			String creditNoteCurrency = null;
			String creditNotePayAmt = "";
			String panelAuthTransactionStatus = null;
			String panelGroupOid = null;
			String panelOplockVal = null;
			String panelRangeOid = null;

			BigDecimal amountInBaseToPanelGroup = null;
			if (corpOrg == null) {
				corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
			}
			String authorizeType = corpOrg.getAttribute("dual_auth_pay_credit_note");
			String clientBankOid = corpOrg.getAttribute("client_bank_oid");
			String creditNoteAmountsql = "SELECT amount, payment_amount, currency FROM credit_notes where upload_credit_note_oid = ?";
			DocumentHandler creditNoteAmountDoc = DatabaseQueryBean.getXmlResultSet(creditNoteAmountsql, true, new String[]{creditNotesOid});
			if (creditNoteAmountDoc != null) {
				creditNoteAmount = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
				creditNoteCurrency = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
				creditNotePayAmt = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("PAYMENT_AMOUNT");
			}

			// Check threshold amount start
			String baseCurrency = corpOrg.getAttribute("base_currency_code");
			creditNotePayAmt = StringFunction.isNotBlank(creditNotePayAmt) ? creditNotePayAmt : creditNoteAmount;

			String selectClause = "select distinct upload_credit_note_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) "
					+ "then amount else payment_amount end as payment_amount from credit_notes ";

			boolean isValid = InvoiceUtility.checkThresholds(creditNotePayAmt,
					creditNoteCurrency, baseCurrency, "pay_credit_note_thold",
					"pay_credit_note_dlimit", userOrgOid, clientBankOid, user,
					selectClause, false, subsAuth, mediatorServices);
			if (!isValid) {
				return new DocumentHandler();
			}

			// threshold check end

			// for Panel Auth
			if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType)
					&& corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_CREDIT_NOTE)) {
				PanelAuthorizationGroup panelAuthGroup = null;
				if (StringFunction.isBlank(creditNote.getAttribute("panel_auth_group_oid"))) {
					panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_CREDIT_NOTE, corpOrg, errMgr);
					if (StringFunction.isNotBlank(panelGroupOid)) {
						panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
										Long.parseLong(panelGroupOid));
					} else {
						// issue error as panel group is required if panel auth
						// defined for instrument type
						errMgr.issueError(TradePortalConstants.ERR_CAT_1,
								AmsConstants.REQUIRED_ATTRIBUTE,
								resMgr.getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
						// if panel id is blank, return the flow back.
						IssuedError is = new IssuedError();
						is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
						throw new AmsException(is);
					}

					creditNote.setAttribute("panel_auth_group_oid",
									panelAuthGroup.getAttribute("panel_auth_group_oid"));
					creditNote.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));

					String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null,
									TradePortalConstants.PAYABLES_CREDIT_NOTE);
					amountInBaseToPanelGroup = PanelAuthUtility.getAmountInBaseCurrency(creditNoteCurrency,
									creditNotePayAmt, baseCurrencyToPanelGroup,
									corpOrgOid,
									TradePortalConstants.PAYABLES_CREDIT_NOTE,
									null, false, errMgr);

					panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"),amountInBaseToPanelGroup);
					if (StringFunction.isBlank(panelRangeOid)) {
						errMgr.issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
								amountInBaseToPanelGroup.toString(),
								TradePortalConstants.PAYABLES_CREDIT_NOTE);
						IssuedError is = new IssuedError();
						is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
						throw new AmsException(is);
					}
					creditNote.setAttribute("panel_auth_range_oid", panelRangeOid);
				}

				panelGroupOid = creditNote.getAttribute("panel_auth_group_oid");
				panelOplockVal = creditNote.getAttribute("panel_oplock_val");
				panelRangeOid = creditNote.getAttribute("panel_auth_range_oid");
				PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.PAYABLES_CREDIT_NOTE, errMgr);
				panelAuthProcessor.init(panelGroupOid, new String[] { panelRangeOid }, panelOplockVal);
				panelAuthTransactionStatus = panelAuthProcessor.process(creditNote, panelRangeOid, false);
			}

			String firstAuthorizer = creditNote.getAttribute("first_authorizing_user_oid");
			String firstAuthorizerWorkGroup = creditNote.getAttribute("first_authorizing_work_group_oid");
			boolean isSendToTPS = false;
			
			// ***********************************************************************
			// If dual authorization is required, set the transaction state to
			// partially authorized if it has not been previously authorized and
			// set
			// the transaction to authorized if there has been a previous
			// authorizer.
			// The second authorizer must not be the same as the first
			// authorizer
			// not the same work groups, depending on the corporate org setting.
			// If dual authorization is not required, set the invoice state to
			// authorized.
			// ***********************************************************************

			creditNote.setAttribute("tp_rule_name", buyerName);

			if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
					&& StringFunction.isBlank(userWorkGroupOid)) {
				saveCreditNote(TradePortalConstants.CREDIT_NOTE_STATUS_FAUT,creditNote, creditNotesOid, isSendToTPS, invProp, false);
				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
			} else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
					|| TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) {
				authoriseCreditNoteWithDiffWorkGroup(creditNote,
						firstAuthorizer, firstAuthorizerWorkGroup,
						authorizeType, errMgr, resMgr, invProp);
			} else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType)
					&& corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_CREDIT_NOTE)) {
				try {
					panelAuthForPayCreditNote(creditNote, user, errMgr, resMgr, panelAuthTransactionStatus, invProp);
				} catch (AmsException e) {
					IssuedError is = e.getIssuedError();
					if (is != null) {
						String errorCode = is.getErrorCode();
						if (TradePortalConstants.REQUIRED_ATTRIBUTE
								.equals(errorCode)
								|| TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE
										.equals(errorCode)) {
							return new DocumentHandler();
						} else
							throw (e);
					}
				}

			} else {
				// dual auth ind is NO, first auth is EMPTY
				creditNote.setAttribute("first_authorizing_user_oid", userOid);
				creditNote.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
				creditNote.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				isSendToTPS = true;
				saveCreditNote(TradePortalConstants.CREDIT_NOTE_STATUS_AUT,
						creditNote, creditNotesOid, isSendToTPS, invProp, false);

				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),
						TradePortalConstants.TRANSACTION_PROCESSED, resMgr
								.getText("PayableInvoice.PayCreditNote",
										TradePortalConstants.TEXT_BUNDLE), creditNoteId,
						resMgr.getText("TransactionAction.Authorized",
								TradePortalConstants.TEXT_BUNDLE));
			}
          }
		}
		return new DocumentHandler();
	}
	
	private String checkDocumentAlreadyAttachedToCreditNote(String creditNotesOid, MediatorServices mediatorServices, String creditNoteId)
	throws AmsException {

		String sql = "select doc_image_oid from document_image where p_transaction_oid =?";
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new String[]{creditNotesOid});
		if (imagesListDoc != null){
			return imagesListDoc.getAttribute("/ResultSetRecord(0)/DOC_IMAGE_OID");
		}else{
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DOC_NOT_PRESENT_TO_DEL,
					creditNoteId);

		 return null;
		}
	}
	
	private boolean creditNoteAlreadyHasDocAttached(String creditNotesOid)
	throws AmsException {
		boolean docAttached = false;
		String sql = "select doc_image_oid from document_image where p_transaction_oid =?";		
		DocumentHandler	imagesListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new String[]{creditNotesOid});
		if (imagesListDoc != null){
			docAttached = true;
		}

		return docAttached;
	}
	
	/**
	 * This method checks whether credit notes is expired or not.
	 * @param creditNote
	 * @return
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private boolean isCreditNoteExpired(CreditNotes creditNote) throws RemoteException, AmsException{
		boolean isExpired = false;
		Date creditNoteExpirtDate = creditNote.getAttributeDate("expiry_date");
		Date currentBusinessDate  = GMTUtility.getGMTDateTime();
		if (StringFunction.isNotBlank(creditNoteExpirtDate.toString()) && 
				currentBusinessDate.after(creditNoteExpirtDate)) {
			isExpired = true;
		}		
		return isExpired;		
	}
	
	
	private void saveCreditNote(String newStatus, CreditNotes creditNote,String creditNoteOid,
			boolean isSendToTPS, InvoiceProperties invProp, boolean isPanelAuth) throws RemoteException, AmsException
			{
	    creditNote.setAttribute( "credit_note_status", newStatus );
	    String tpName=creditNote.getTpRuleName();
        creditNote.setAttribute( "tp_rule_name", tpName );
        creditNote.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
		creditNote.setAttribute("user_oid", invProp.getUserOid());
		creditNote.setAttribute("reqPanleAuth",isPanelAuth? TradePortalConstants.INDICATOR_YES:TradePortalConstants.INDICATOR_NO);
		if(creditNote.save() == 1) {
			if(isSendToTPS){
				String preProcessParameter = "creditNoteInd=Y|creditNoteUpdateInd=N";// Authorize credit note is always new to TPS.
			    InvoiceUtility.createOutgoingQueueAndIssueSuccess( newStatus,
					creditNoteOid, false, invProp.getMediatorServices(), preProcessParameter );	
			}
		}
	}
			
	 /* Panel Auth process for Payables Invoices*/
    private void panelAuthForPayCreditNote( CreditNotes creditNote, User user,
    		ErrorManager errMgr,ResourceManager resMgr,String panelAuthTransactionStatus,
    		InvoiceProperties invProp)throws RemoteException, AmsException {
		String creditNoteId = creditNote.getAttribute("invoice_id");
		String creditNoteOid = creditNote.getAttribute("upload_credit_note_oid");
		String newStatus ="";
		boolean isSendToTPS=false;
		
       if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {

    	    newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
			creditNote.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
			creditNote.setAttribute("first_authorizing_work_group_oid",invProp.getUserWorkGroupOid());
			creditNote.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());		
			isSendToTPS= true;
			//isUpdateAttachment = true;
			
			errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
					resMgr.getText("PayableInvoice.Invoice", TradePortalConstants.TEXT_BUNDLE), creditNoteId,
					resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
			
       }else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus)) {
    	   
    	    newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_PAUT;
			creditNote.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
			creditNote.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
			creditNote.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
			
			errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
					resMgr.getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription());
			
       }else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus)) {
    	   newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
       } 

       saveCreditNote(newStatus, creditNote, creditNoteOid, isSendToTPS, invProp, true);
       
	
    }
    
    
    
    /*
   	// If dual auth is required and f its not been previously authorized then set the status to partially authorized.
   	// set the status to authorized if there has been a previous authorizer.
   	// The second authorizer must not be the same as the first authorizer
   	// not the same work groups, depending on the corporate org setting.
   	// If dual authorization is not required, set the invoice state to authorized.
   	*/   
       private void authoriseCreditNoteWithDiffWorkGroup(CreditNotes creditNote,String firstAuthorizer
       		,String firstAuthorizerWorkGroup,String authorizeType,
       		ErrorManager errMgr,ResourceManager resMgr,InvoiceProperties invProp) throws RemoteException, AmsException
       	{
       		String creditNoteId = creditNote.getAttribute("invoice_id");
       		String creditNoteOid = creditNote.getAttribute("upload_credit_note_oid");
       		String newStatus="";      		
       		boolean isSendToTPS=false;
       		//boolean isUpdateAttachment = false;
       		if (StringFunction.isBlank(firstAuthorizer)) {
   				// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
   				newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_PAUT;
   				creditNote.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
   				creditNote.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
   				creditNote.setAttribute("invoice_status", newStatus);
   				
   				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
						resMgr.getText("PayableInvoice.PayCreditNote",TradePortalConstants.TEXT_BUNDLE), creditNoteId);
   			}
   			else {// firstAuthorizer not empty
   				if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
   						&& invProp.getUserWorkGroupOid().equals(firstAuthorizerWorkGroup)) {
   					// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
   					 newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;   					
   					 creditNote.setAttribute("credit_note_status", newStatus);
   					 errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);   					
   				}
   				else if (firstAuthorizer.equals(invProp.getUserOid())) {
   					// Two authorizers required and this user is the same as the
   					// first authorizer. Give error.
   					// Note requiring two work groups also implies requiring two users.
   					 newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
   					 creditNote.setAttribute("credit_note_status", newStatus);
   					 errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);   					
   				}
   				else {// Authorize
   					newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
   					creditNote.setAttribute("second_authorizing_user_oid", invProp.getUserOid());
   					creditNote.setAttribute("second_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
   					creditNote.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
   					creditNote.setAttribute("credit_note_status", newStatus);
   					isSendToTPS= true;
   					//isUpdateAttachment = true;   					
   					errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
   							resMgr.getText("PayableInvoice.PayCreditNote", TradePortalConstants.TEXT_BUNDLE),creditNoteId,
   							resMgr.getText("TransactionAction.Authorized", TradePortalConstants.TEXT_BUNDLE));
   				} 				
   			}
       		
       		saveCreditNote(newStatus, creditNote, creditNoteOid, isSendToTPS, invProp, false);       		       	     
       	}
       
       /**
        * This method is used to add invoice detail on which credit not has been applied or un-applied.
        * @param invoiceID
        * @param appliedAmt
        * @param appliedInvDetailToCred
        */
       private void addInvoiceDetailInAppliedCred(String invoiceID, String appliedAmt, StringBuilder appliedInvDetailToCred){
    	   appliedInvDetailToCred.append(invoiceID).append(INV_DATA_SEPERATOR);
   		   appliedInvDetailToCred.append(appliedAmt).append(INV_ITEM_SEPERATOR);
       }
       
       /**
        * This Method is used to construct process_parameters info for Payable credit Note. this parameter is used during package of INV message to TPS.
        * 
        * @param CreditNoteUpdateInd
        * @param creditNoteApplyStatus
        * @param appliedInvDetailToCred
        * @return
        * @throws AmsException 
        * @throws RemoteException 
        */
       private String constCreditNoteProcessParam (String creditNoteUpdateInd, String creditNoteApplyStatus, StringBuilder appliedInvDetailToCred, String messageID, MediatorServices mediatorServices) throws RemoteException, AmsException{
    	   
    	 // insert applied invoice detaill entry
    	 String credAppliedInvDetailOid = createCredAppliedInvDetailEntry(appliedInvDetailToCred.toString(), messageID, mediatorServices);
    	 StringBuilder preProcessParameter = new StringBuilder("");
    	 preProcessParameter.append("creditNoteInd=Y");
  		 preProcessParameter.append("|creditNotePreApplicationStatus=").append(creditNoteApplyStatus);
  		 preProcessParameter.append("|creditNoteUpdateInd=").append(creditNoteUpdateInd);
  		 preProcessParameter.append("|credAppliedInvDetailOid=").append(credAppliedInvDetailOid);	
  		 
  		 return preProcessParameter.toString();
       }
       
   	  /**
   	   * This method is used to check whether any invoices status is Partial authorized or Not.
   	   * credit note can not be applied to those invoices which has status as Partial Authorized.
   	   * @param invOids2 list of invoice to apply credit note
   	   * @param mediatorServices
   	   * @return 
   	   * @throws AmsException
   	   */
   	  private boolean isValidInvoiceStatusToApplyCred( List invOids, MediatorServices mediatorServices) throws AmsException {
   		boolean isValid = true;
   		List Param = new ArrayList();
   		StringBuilder whereClause = new StringBuilder("invoice_status = ? AND upload_invoice_oid in (");
   		for( int i=0; i < invOids.size(); i++  ){
   			if(i == 0) {
   				whereClause.append("?");
   			}else {
   				whereClause.append(",?");
   			}
   		}
   		whereClause.append(")");
   		Param.add( TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH );
   		Param.addAll(invOids);
   		int count = DatabaseQueryBean.getCount("upload_invoice_oid", "invoices_summary_data", whereClause.toString(), 
   				true, Param);
   		if( count > 0 ){
   			isValid = false;
   			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PARTIALLY_AUTH_INV_ERROR);
   		}
   		
   		return isValid;
   	 }

   	public DocumentHandler authorizeEndToEndIDGroup(InvoiceGroup invoiceGroup, Map<String, String> endToEndIdGroupProp, MediatorServices mediatorServices) 
   			throws RemoteException, AmsException {
   	  	
     	 String endToEndID = invoiceGroup.getAttribute("end_to_end_id");
     	 String corpOrgOid = endToEndIdGroupProp.get("corpOrgOid");
     	 CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                 Long.parseLong(corpOrgOid));
     	 User user = (User) mediatorServices.createServerEJB("User",
   			Long.parseLong(endToEndIdGroupProp.get("userOid")));
		endToEndIdGroupProp.put("panelLevelCode", user.getAttribute("panel_authority_code"));
     	 String authorizeType = InvoiceUtility.getAuthTypeOfEndToEndID(corpOrg, endToEndID);
     	 String clientBankOid = corpOrg.getAttribute("client_bank_oid");
     	 String authorizeLabel = TradePortalConstants.USER_AUTH_LEVEL_FIRST;
     	boolean isInvUpdateReq = true; // this invoice group method si it willalways be true.
     	boolean isCredUpdateReq = false;
     	if ( totalEndToEndIdCredNoteAvl(corpOrgOid, endToEndID) > 0 ){
     		isCredUpdateReq = true;
     	}
        // Check threshold amount start    
        String baseCurrency = corpOrg.getAttribute("base_currency_code");
        String invoiceGroupPayAmt  = InvoiceUtility.getGroupAmountOFEndToEndID(corpOrgOid, endToEndID).toPlainString();
        String invoiceGroupCurrency = invoiceGroup.getAttribute( "currency" );
        String selectClause = "select distinct upload_invoice_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) " +
                "then amount else payment_amount end as payment_amount from invoices_summary_data ";
        
        boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt, invoiceGroupCurrency , baseCurrency,
                "upload_pay_inv_thold", "upload_pay_inv_dlimit", corpOrgOid, clientBankOid, user ,selectClause, false, 
                endToEndIdGroupProp.get("subsAuth"), mediatorServices);
        if(!isValid){
            return new DocumentHandler();
        }     
        //threshold check end
            
        //for Panel Auth Start
        String panelGroupOid = null;
        String panelOplockVal = null;
        String panelRangeOid = null;
        BigDecimal amountInBaseToPanelGroup = null ;
        String panelAuthTransactionStatus = null;
        String newStatus = invoiceGroup.getAttribute("invoice_status");
        boolean isSendToTPS = false;
        int success = 0;
        if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
                corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_PROGRAM_INVOICE)) {
            PanelAuthorizationGroup panelAuthGroup = null;
            if(StringFunction.isBlank(invoiceGroup.getAttribute("panel_auth_group_oid"))) {
                
                panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_PROGRAM_INVOICE, corpOrg, mediatorServices.getErrorManager());
                if(StringFunction.isNotBlank(panelGroupOid)){
                    panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
                            Long.parseLong(panelGroupOid));
                }else{
                    // issue error as panel group is required if panel auth defined for instrument type
               	 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,AmsConstants.REQUIRED_ATTRIBUTE,
               			 mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
                    // if panel id is blank, return the flow back.
                    IssuedError is = new IssuedError();
                    is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
                    throw new AmsException(is);
                }
                
                invoiceGroup.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
                invoiceGroup.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
                
                String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
                //IR 27059- Use Payment_Amount if present
                amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
                               (invoiceGroupCurrency, invoiceGroupPayAmt, baseCurrencyToPanelGroup, corpOrgOid,
                                       TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
                
                panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
                if(StringFunction.isBlank(panelRangeOid)){
               	 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
                            amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE );
                    IssuedError is = new IssuedError();
                    is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
                    throw new AmsException(is);
                }
                invoiceGroup.setAttribute("panel_auth_range_oid", panelRangeOid);
            }
            
            panelGroupOid  = invoiceGroup.getAttribute("panel_auth_group_oid");
            panelOplockVal = invoiceGroup.getAttribute("panel_oplock_val");
            panelRangeOid =  invoiceGroup.getAttribute("panel_auth_range_oid");                                   
            PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
            panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);
            
            panelAuthTransactionStatus = panelAuthProcessor.process(invoiceGroup, panelRangeOid, false);
            
			endToEndIdGroupProp.put("panelGroupOid", panelGroupOid);
			endToEndIdGroupProp.put("panelRangeOid", panelRangeOid);
			endToEndIdGroupProp.put("panelOplockVal", panelOplockVal);
            
            if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
     	          newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
     	          isSendToTPS = true;
            }
            else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus))
            {
           	 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH; 
            }
            else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus))
            {
           	 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
            }
            invoiceGroup.setAttribute("invoice_status", newStatus);
            
              success = saveEndToEndInvoiceGroup(newStatus, endToEndID, endToEndIdGroupProp, 
            		  authorizeType, authorizeLabel, isInvUpdateReq, isCredUpdateReq, isSendToTPS, mediatorServices);
        }
        //for Panel Auth End
        else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                && StringFunction.isBlank(endToEndIdGroupProp.get("userWorkGroupOid")))
        {
            newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
                     
        }
        else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
        {
           String sql = "SELECT a_first_authorizing_user_oid first_Authorizer, a_first_auth_work_group_oid first_Authorizer_WorkGroup " +
                                   "FROM invoices_summary_data " +
                                   "WHERE a_invoice_group_oid = ? and rownum =1 ";
           DocumentHandler	resultDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[] {invoiceGroup.getAttribute("invoice_group_oid")});           
           String firstAuthorizer = resultDoc.getAttribute("/ResultSetRecord(0)/FIRST_AUTHORIZER");
           String firstAuthorizerWorkGroup = resultDoc.getAttribute("/ResultSetRecord(0)/FIRST_AUTHORIZER_WORKGROUP");
           
           if (StringFunction.isBlank(firstAuthorizer)) {
    			newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH;			

    		}
    		// if firstAuthorizer is not empty
    		else {
    			// Two work groups required and this user belongs to the
    			// same work group as the first authorizer: Error.
    			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
    					&& endToEndIdGroupProp.get("userWorkGroupOid").equals(firstAuthorizerWorkGroup))
    			{
    				
    				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
    				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
    						TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);				
    			}
    			// Two authorizers required and this user is the same as the
    			// first authorizer. Give error.
    			// Note requiring two work groups also implies requiring two
    			// users.
    			else if (firstAuthorizer.equals(endToEndIdGroupProp.get("userOid"))) {
    				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED;
    				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
    						TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);				
    			}
    			// Authorize
    			else {
    				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
    				authorizeLabel = TradePortalConstants.USER_AUTH_LEVEL_SECOND;
    				isSendToTPS = true;
    			}
    		}
           
           invoiceGroup.setAttribute("invoice_status", newStatus);
           success = saveEndToEndInvoiceGroup(newStatus, endToEndID, endToEndIdGroupProp, authorizeType, authorizeLabel, isInvUpdateReq, isCredUpdateReq, 
        		   isSendToTPS, mediatorServices);
        }       
        else 
        {
            // dual auth ind is NO, first auth is EMPTY
            newStatus = TradePortalConstants.UPLOAD_INV_STATUS_AUTH;
            isSendToTPS = true;
            invoiceGroup.setAttribute("invoice_status", newStatus);
            success = saveEndToEndInvoiceGroup(newStatus, endToEndID, endToEndIdGroupProp, authorizeType, authorizeLabel, isInvUpdateReq, isCredUpdateReq, isSendToTPS, mediatorServices);                           
        }
        if( success > 0) {
       	 int isInvGroupSave = invoiceGroup.save();
       	 if ( isInvGroupSave > 0 ) {
			   if(TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(newStatus)){				  
			      mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.AUTH_END_TO_END_ID_GROUP,
			    		endToEndID);
			   }
			   else if ( TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(newStatus) )
			   {
				  mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.PARTIALLY_AUTH_END_TO_END_ID_GROUP,
	 			    		endToEndID);  
			   }
			}
        }
        return new DocumentHandler();
     }
     	
     private int totalEndToEndIdCredNoteAvl(String corpOrgOid, String endToEndID) throws AmsException {
    	 String whereClause = " a_corp_org_oid = ? AND end_to_end_id = ? and credit_note_status !=? ";
    	 int  count = DatabaseQueryBean.getCount("UPLOAD_CREDIT_NOTE_OID", "CREDIT_NOTES", whereClause, 
    			 true, new Object[]{corpOrgOid, endToEndID,TradePortalConstants.UPLOAD_INV_STATUS_DBC});
		return count;
	}

	private int saveEndToEndInvoiceGroup( String newStatus, String endToEndID, Map<String, String> endToEndIdGroupProp, String authorizeType, 
   		  String authorizeLabel, boolean isInvUpdateReq, boolean isCredUpdateReq, boolean isSendToTPS, MediatorServices mediatorServices ) throws AmsException {
   	  
   	    String sql = null;
   	    Object userWorkGroupOid = null;
   	    if(StringFunction.isNotBlank(endToEndIdGroupProp.get("userWorkGroupOid"))){
   	    	userWorkGroupOid = Long.parseLong(endToEndIdGroupProp.get("userWorkGroupOid"));
   	    }
   	    int success = 1;
   	    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        java.sql.Timestamp timeStamp = java.sql.Timestamp.valueOf(sd.format(DateTimeUtility.convertStringDateTimeToDate(DateTimeUtility.getGMTDateTime())));	    
   	    String historySQl = "INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, A_USER_OID,P_UPLOAD_INVOICE_OID,INVOICE_STATUS) " +
                   " VALUES (?,?,?,?,?,?)";
   	    String outgoingQueueSql = "INSERT INTO OUTGOING_QUEUE (QUEUE_OID, DATE_CREATED, STATUS, MSG_TYPE, MESSAGE_ID, PROCESS_PARAMETERS) VALUES (?,?,?,?,?,?)";
   	    String invoiceAction = TradePortalConstants.UPLOAD_INV_ACTION_AUTH;
   	    Connection con =null;
        PreparedStatement invDatapstmt = null;
        PreparedStatement invHistorypstmt = null;
        PreparedStatement credDatapstmt = null;
        PreparedStatement credHistorypstmt = null;
        PreparedStatement outgoingQueuestmt = null;
   	    try {
   	      con = DatabaseQueryBean.connect(true);
          con.setAutoCommit(false);  	   
   	      if (isInvUpdateReq) 
   	      {
   	    	sql = "SELECT upload_invoice_oid FROM invoices_summary_data WHERE end_to_end_id = ? AND a_corp_org_oid = ? ";
   	    	DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{endToEndID, 
   	    			endToEndIdGroupProp.get("corpOrgOid")});
   	    	List<DocumentHandler> invoiceList = invoiceListDoc.getFragmentsList("/ResultSetRecord");
   	    	
   	    	if ( TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(authorizeType) || 
   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType) ||
   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType) )
   	    	{
   	    	 if(TradePortalConstants.USER_AUTH_LEVEL_FIRST.equals(authorizeLabel))
   	    	 {
   	    	     sql = "UPDATE invoices_summary_data " +
   	    	 	       "SET invoice_status = ?,  a_first_authorizing_user_oid = ?, a_first_auth_work_group_oid = ?, first_authorize_status_date = ? " +
   	    	 	       "WHERE upload_invoice_oid = ? ";
   	    	 }
   	    	 else
   	    	 {
   	    		 sql = "UPDATE invoices_summary_data " +
     	    	 	       "SET invoice_status = ?,  a_second_authorizing_user_oid = ?, a_second_auth_work_group_oid = ?, second_authorize_status_date = ? " +
     	    	 	       "WHERE upload_invoice_oid = ? ";
   	    	 }
   	    	        invHistorypstmt = con.prepareStatement(historySQl);
   	    	        if ( !TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(newStatus) ) {
   		                invDatapstmt = con.prepareStatement(sql);  
   	    	        }
   		            if(isSendToTPS){
 		    	        outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
 		            }
   		           		            
   		            for (DocumentHandler doc : invoiceList){
   		            	
                        // Invoice History Update   			            
			            insertInvoiceHistory(invHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
			            			doc.getAttribute("/UPLOAD_INVOICE_OID"), newStatus);
   		            	
   		            	// Invoices update of End To End ID group		            	   		            	
   		            	if( !TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(newStatus) ){
	   			             updateUserAuthotizeActionData(invDatapstmt, timeStamp, endToEndIdGroupProp.get("userOid"), userWorkGroupOid, 
	   			                		doc.getAttribute("/UPLOAD_INVOICE_OID"), newStatus);
						}

						// Insert Outgoingqueue entry
						if (isSendToTPS) {
							StringBuilder processParameter = new StringBuilder();
							processParameter.append("|status=").append(newStatus);
							processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_INVOICE_OID"));
							createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
						}
   		            	
   		            }
					int[] succesCount = invHistorypstmt.executeBatch();
					if (succesCount.length != invoiceList.size()) {
						success = 0;
					}
					if (invDatapstmt != null) {
						succesCount = invDatapstmt.executeBatch();
						if (succesCount.length != invoiceList.size()) {
							success = 0;
						}
					}
					if (outgoingQueuestmt != null) {
						succesCount = outgoingQueuestmt.executeBatch();
						if (succesCount.length != invoiceList.size()) {
							success = 0;
						}
					}
   		               		               	    	 
   	    	}
   	    	else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType))
   	    	{
   	    	 sql = "UPDATE invoices_summary_data " +
   	    	 	   "SET invoice_status = ?,  a_panel_auth_group_oid = ?, a_panel_auth_range_oid = ?, panel_oplock_val = ? " +
   	    	 	   "WHERE upload_invoice_oid = ? ";
   	    	 	     	
   	    	        invHistorypstmt = con.prepareStatement(historySQl);
   	    	        if( !TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(newStatus) ){
   		              invDatapstmt = con.prepareStatement(sql);
   	    	        }
   	    	        if(isSendToTPS){
		    	        outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
		            }
   		           		            
   		            for (DocumentHandler doc : invoiceList){
   		            	
                        // Invoice History Update
 			            insertInvoiceHistory(invHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
		            			doc.getAttribute("/UPLOAD_INVOICE_OID"), newStatus);
 			           
   		            	// Invoices update of End To End ID group		            	  		            	
   		            	if( !TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(newStatus) ){		            	  						            	
				            updateUserPanelAuthorizeActionData(invDatapstmt, endToEndIdGroupProp.get("panelGroupOid"), endToEndIdGroupProp.get("panelRangeOid"), 
				            	endToEndIdGroupProp.get("panelOplockVal"), doc.getAttribute("/UPLOAD_INVOICE_OID"), newStatus);	            	
				        }
   		            	
   		                // Insert Outgoingqueue entry
						if (isSendToTPS) {
							StringBuilder processParameter = new StringBuilder();
							processParameter.append("|status=").append(newStatus);
							processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_INVOICE_OID"));
							createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
						}
   		            	  			           
					}
   		            int[] succesCount = invHistorypstmt.executeBatch();
					if (succesCount.length != invoiceList.size()) {
						success = 0;
					}
					if (invDatapstmt != null) {
						succesCount = invDatapstmt.executeBatch();
						if (succesCount.length != invoiceList.size()) {
							success = 0;
						}
					}
					if (outgoingQueuestmt != null) {
						succesCount = outgoingQueuestmt.executeBatch();
						if (succesCount.length != invoiceList.size()) {
							success = 0;
						}
				  }
   	    	}
   	     }
   	    
   	     if (isCredUpdateReq) 
   	     {
   	    	    String preProcessParameter = "creditNoteInd=Y|creditNoteUpdateInd=N";
   	    	    String creditStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AVA;
    	        if(TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equals(newStatus)){
    	    	  creditStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
    	        }
    	        else if (TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH.equals(newStatus)){
    	    	  creditStatus = TradePortalConstants.CREDIT_NOTE_STATUS_PAUT;
    	       }
    	       else if (TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED.equals(newStatus)) {
    	    	  creditStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
    	        }
   		    	sql = "SELECT upload_credit_note_oid FROM credit_notes WHERE end_to_end_id = ? AND a_corp_org_oid = ? ";
   		    	DocumentHandler	creditListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{endToEndID, endToEndIdGroupProp.get("corpOrgOid")});
   		    	List<DocumentHandler> creditNoteList = creditListDoc.getFragmentsList("/ResultSetRecord");
   		    	
   		    	if ( TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(authorizeType)  || 
   	   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType) ||
   	   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
   		    	{
   		    	if(TradePortalConstants.USER_AUTH_LEVEL_FIRST.equals(authorizeLabel))
   		    	{
   		    	 sql = "UPDATE credit_notes " +
   		    	 	   "SET credit_note_status = ?, a_first_authorizing_user_oid = ?, a_first_auth_work_group_oid= ?, first_authorize_status_date = ? " +
   		    	 	   "WHERE upload_credit_note_oid = ? ";
   		    	 
   		    	}
   		    	else
   		    	{
   		    	 sql = "UPDATE credit_notes " +
     		    	 	   "SET credit_note_status = ?, a_second_authorizing_user_oid = ?, a_second_authorizing_work_group_oid = ?, second_authorize_status_date = ? " +
     		    	 	   "WHERE upload_credit_note_oid = ? ";
   		    	}
					credHistorypstmt = con.prepareStatement(historySQl);
					if (!TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditStatus)) {
						credDatapstmt = con.prepareStatement(sql);
					}
					if (isSendToTPS) {
						outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
					}

					for (DocumentHandler doc : creditNoteList) {
   			            	
   	                        // Invoice History Update
   			            	insertInvoiceHistory(credHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
				            			doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), creditStatus);
   			            	
   			            	// Invoices update of invoice group	
   			            	if( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditStatus) ){
   	   			                updateUserAuthotizeActionData(credDatapstmt, timeStamp, endToEndIdGroupProp.get("userOid"), userWorkGroupOid, 
   	   			                		doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), creditStatus);
   	   			            } 
   			            	
   			             // Insert Outgoingqueue entry
	   			                if(isSendToTPS) {
	   			                  StringBuilder processParameter = new StringBuilder(preProcessParameter);
	   			                  processParameter.append("|status=").append(newStatus);
	   			                  processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"));
	   			                  createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
	   			                }
   			            }

   			            int[] succesCount = credHistorypstmt.executeBatch();
			            if (succesCount.length != creditNoteList.size()){
			            	success = 0;
			              }
			            if(credDatapstmt != null){
			              succesCount = credDatapstmt.executeBatch();
			              if (succesCount.length != creditNoteList.size()){
			            	success = 0;
			              }
			            }
			            if(outgoingQueuestmt != null){
			             succesCount = outgoingQueuestmt.executeBatch();
			             if (succesCount.length != creditNoteList.size()){
			            	success = 0;
			             }
			            }
   			            
   		    	 
   		    	}
   		    	else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType))
		    	{
		    		     sql = "UPDATE credit_notes " +
				    	 	   "SET credit_note_status = ?, a_panel_auth_group_oid = ?, a_panel_auth_range_oid = ?, panel_oplock_val = ? " +
				    	 	   "WHERE upload_credit_note_oid = ? ";
				    	 
					credHistorypstmt = con.prepareStatement(historySQl);
					if (!TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditStatus)) {
						credDatapstmt = con.prepareStatement(sql);
					}
					if (isSendToTPS) {
						outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
					}

					for (DocumentHandler doc : creditNoteList) {
					            	
			                        // Invoice History Update
					            	insertInvoiceHistory(credHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
		 				            			doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), creditStatus);
					            	
					            	// Invoices update of invoice group
					            	if( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(creditStatus) ){
  						            	 // Invoices update of invoice group		            	  						            	
  						            	 updateUserPanelAuthorizeActionData(credDatapstmt, endToEndIdGroupProp.get("panelGroupOid"), endToEndIdGroupProp.get("panelRangeOid"), 
  						            			endToEndIdGroupProp.get("panelOplockVal"), doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), creditStatus);
  						            }	
					            	
					            	// Insert Outgoingqueue entry
		   			                if(isSendToTPS) {
		   			                  StringBuilder processParameter = new StringBuilder(preProcessParameter);
		   			                  processParameter.append("|status=").append(newStatus);
		   			                  processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"));
		   			                  createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
		   			                }
					            }
					            int[] succesCount = credHistorypstmt.executeBatch();
					            if (succesCount.length != creditNoteList.size()){
					            	success = 0;
					              }
					            if(credDatapstmt != null){
					              succesCount = credDatapstmt.executeBatch();
					              if (succesCount.length != creditNoteList.size()){
					            	success = 0;
					              }
					            }
					            if(outgoingQueuestmt != null){
					             succesCount = outgoingQueuestmt.executeBatch();
					             if (succesCount.length != creditNoteList.size()){
					            	success = 0;
					             }
					            }
		    	}
   		     
   	     }
   	     
   	     con.commit();
   	                
        } catch ( AmsException | SQLException e ) {
        	 e.printStackTrace();
             throw new AmsException(e);
        } 
        finally{
        	try{
        	 if (invDatapstmt!= null) invDatapstmt.close();
       	     if (invHistorypstmt!= null) invHistorypstmt.close();
       	     if (credDatapstmt!= null) credDatapstmt.close();
       	     if (credHistorypstmt!= null) credHistorypstmt.close();
       	     if (outgoingQueuestmt!= null) outgoingQueuestmt.close();
       	     if (con!= null) con.close();
           } catch(SQLException  e){
        		e.printStackTrace();
        	}
        }

   	  return success;
   	}
     
     public DocumentHandler authorizeCreditNoteEndToEndIDGroup(CreditNotes creditNotes, Map<String, String> endToEndIdGroupProp, MediatorServices mediatorServices) 
    			throws RemoteException, AmsException {
    	  	
      	 String endToEndID = creditNotes.getAttribute("end_to_end_id");
      	 String corpOrgOid = endToEndIdGroupProp.get("corpOrgOid");
      	 CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                  Long.parseLong(corpOrgOid));
      	 User user = (User) mediatorServices.createServerEJB("User",
    			Long.parseLong(endToEndIdGroupProp.get("userOid")));
 		endToEndIdGroupProp.put("panelLevelCode", user.getAttribute("panel_authority_code"));
      	 String authorizeType = InvoiceUtility.getAuthTypeOfEndToEndID(corpOrg, endToEndID);
      	 String clientBankOid = corpOrg.getAttribute("client_bank_oid");
      	 String authorizeLabel = TradePortalConstants.USER_AUTH_LEVEL_FIRST;
      	 boolean isSendToTPS = false;
         // Check threshold amount start    
         String baseCurrency = corpOrg.getAttribute("base_currency_code");
         String invoiceGroupPayAmt  = InvoiceUtility.getGroupAmountOFEndToEndID(corpOrgOid, endToEndID).toPlainString();
         String invoiceGroupCurrency = creditNotes.getAttribute( "currency" );
         String selectClause = "select distinct upload_credit_note_oid, currency as currency_code , abs(amount) as payment_amount from credit_notes ";
         
         boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt, invoiceGroupCurrency , baseCurrency,
                 "pay_credit_note_thold", "pay_credit_note_dlimit", corpOrgOid, clientBankOid, user ,selectClause, false, 
                 endToEndIdGroupProp.get("subsAuth"), mediatorServices);
         if(!isValid){
             return new DocumentHandler();
         }     
         //threshold check end
             
         //for Panel Auth Start
         String panelGroupOid = null;
         String panelOplockVal = null;
         String panelRangeOid = null;
         BigDecimal amountInBaseToPanelGroup = null ;
         String panelAuthTransactionStatus = null;
         int success = 0;
         boolean isPanelAuth = false;
         String newStatus = creditNotes.getAttribute("credit_note_status");
         if(TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType) &&
                 corpOrg.isPanelAuthEnabled(TradePortalConstants.PAYABLES_CREDIT_NOTE)) {
             PanelAuthorizationGroup panelAuthGroup = null;
             isPanelAuth = true;
             if( StringFunction.isBlank(creditNotes.getAttribute("panel_auth_group_oid")) ) {
                 
                 panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(TradePortalConstants.PAYABLES_CREDIT_NOTE, corpOrg, mediatorServices.getErrorManager());
                 if(StringFunction.isNotBlank(panelGroupOid)){
                     panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
                             Long.parseLong(panelGroupOid));
                 }else{
                     // issue error as panel group is required if panel auth defined for instrument type
                	 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,AmsConstants.REQUIRED_ATTRIBUTE,
                			 mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
                     // if panel id is blank, return the flow back.
                     IssuedError is = new IssuedError();
                     is.setErrorCode(TradePortalConstants.REQUIRED_ATTRIBUTE);
                     throw new AmsException(is);
                 }
                 
                 creditNotes.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
                 creditNotes.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));
                 
                 String baseCurrencyToPanelGroup = PanelAuthUtility.getBaseCurrency(corpOrg, panelAuthGroup, null, TradePortalConstants.INVOICE);
                 //IR 27059- Use Payment_Amount if present
                 amountInBaseToPanelGroup =  PanelAuthUtility.getAmountInBaseCurrency
                                (invoiceGroupCurrency, invoiceGroupPayAmt, baseCurrencyToPanelGroup, corpOrgOid,
                                        TradePortalConstants.INVOICE, null, false, mediatorServices.getErrorManager());
                 
                 panelRangeOid = PanelAuthUtility.getPanelRange(panelAuthGroup.getAttribute("panel_auth_group_oid"), amountInBaseToPanelGroup);
                 if(StringFunction.isBlank(panelRangeOid)){
                	 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
                             amountInBaseToPanelGroup.toString(), TradePortalConstants.INVOICE );
                     IssuedError is = new IssuedError();
                     is.setErrorCode(TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE);
                     throw new AmsException(is);
                 }
                 creditNotes.setAttribute("panel_auth_range_oid", panelRangeOid);
             }
             
             panelGroupOid  = creditNotes.getAttribute("panel_auth_group_oid");
             panelOplockVal = creditNotes.getAttribute("panel_oplock_val");
             panelRangeOid =  creditNotes.getAttribute("panel_auth_range_oid");
                                       
             PanelAuthProcessor panelAuthProcessor = new PanelAuthProcessor(user, TradePortalConstants.INVOICE, mediatorServices.getErrorManager());
             panelAuthProcessor.init(panelGroupOid, new String[]{panelRangeOid}, panelOplockVal);
             
             panelAuthTransactionStatus = panelAuthProcessor.process(creditNotes, panelRangeOid, false);
             
 			endToEndIdGroupProp.put("panelGroupOid", panelGroupOid);
 			endToEndIdGroupProp.put("panelRangeOid", panelRangeOid);
 			endToEndIdGroupProp.put("panelOplockVal", panelOplockVal);
             
             if (TradePortalConstants.AUTHTRUE.equals(panelAuthTransactionStatus)) {
      	          newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
      	         isSendToTPS = true;
             }
             else if (TradePortalConstants.AUTHPARTL.equals(panelAuthTransactionStatus))
             {
            	 newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_PAUT; 
             }
             else if (TradePortalConstants.AUTHFALSE.equals(panelAuthTransactionStatus))
             {
            	 newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
             }
             creditNotes.setAttribute("credit_note_status", newStatus);
             success = saveEndToEndCreditNoteGroup(newStatus, endToEndID, endToEndIdGroupProp, 
             		  authorizeType, authorizeLabel, isSendToTPS, mediatorServices);             
         }
         //for Panel Auth End
         else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                 && StringFunction.isBlank(endToEndIdGroupProp.get("userWorkGroupOid")))
         {
             newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
                      
         }
         else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                 || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
         {
            String firstAuthorizer = creditNotes.getAttribute("first_authorizing_user_oid");
            String firstAuthorizerWorkGroup = creditNotes.getAttribute("first_authorizing_work_group_oid");
            
            if (StringFunction.isBlank(firstAuthorizer)) {
     			newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_PAUT;			

     		}
     		// if firstAuthorizer is not empty
     		else {
     			// Two work groups required and this user belongs to the
     			// same work group as the first authorizer: Error.
     			if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(authorizeType)
     					&& endToEndIdGroupProp.get("userWorkGroupOid").equals(firstAuthorizerWorkGroup))
     			{
     				
     				newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
     				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
     						TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);				
     			}
     			// Two authorizers required and this user is the same as the
     			// first authorizer. Give error.
     			// Note requiring two work groups also implies requiring two
     			// users.
     			else if (firstAuthorizer.equals(endToEndIdGroupProp.get("userOid"))) {
     				newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_FAUT;
     				mediatorServices.getErrorManager().issueError(UploadInvoiceMediator.class.getName(),
     						TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);				
     			}
     			// AuthorizeTradePortalConstants
     			else {
     				newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
     				isSendToTPS = true;
     				authorizeLabel = TradePortalConstants.USER_AUTH_LEVEL_SECOND;
     			}
     		}
            
            creditNotes.setAttribute("credit_note_status", newStatus);
            success = saveEndToEndCreditNoteGroup(newStatus, endToEndID, endToEndIdGroupProp, authorizeType, authorizeLabel, isSendToTPS, mediatorServices);
         }       
         else 
         {
             // dual auth ind is NO, first auth is EMPTY
             newStatus = TradePortalConstants.CREDIT_NOTE_STATUS_AUT;
             isSendToTPS = true;
             creditNotes.setAttribute("credit_note_status", newStatus);
             success = saveEndToEndCreditNoteGroup(newStatus, endToEndID, endToEndIdGroupProp, authorizeType, authorizeLabel, isSendToTPS, mediatorServices);                         
         }
         if( success > 0) {
        	 creditNotes.setAttribute("action", TradePortalConstants.UPLOAD_INV_ACTION_AUTH);
        	 creditNotes.setAttribute("user_oid", endToEndIdGroupProp.get("userOid"));
        	 creditNotes.setAttribute("reqPanleAuth", isPanelAuth? TradePortalConstants.INDICATOR_YES:TradePortalConstants.INDICATOR_NO);
        	 int isCredSave = creditNotes.save();
        	 if ( isCredSave > 0 ) {
        	    if(isSendToTPS){
 				  String preProcessParameter = "creditNoteInd=Y|creditNoteUpdateInd=N";// Authorize credit note is always new to TPS.
 			      InvoiceUtility.createOutgoingQueueAndIssueSuccess( newStatus,
 			    		creditNotes.getAttribute("upload_credit_note_oid"), false, mediatorServices, preProcessParameter );	
 			      mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.AUTH_END_TO_END_ID_GROUP,
 			    		endToEndID);
 			   }
 			   else if ( TradePortalConstants.CREDIT_NOTE_STATUS_PAUT.equals(newStatus) )
 			   {
 				  mediatorServices.getErrorManager().issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.PARTIALLY_AUTH_END_TO_END_ID_GROUP,
 	 			    		endToEndID);  
 			   }
 			}
         }
         return new DocumentHandler();
      }
      
   	
   	 private int saveEndToEndCreditNoteGroup( String newStatus, String endToEndID, Map<String, String> endToEndIdGroupProp, String authorizeType, 
   	   		  String authorizeLabel, boolean isSendToTPS, MediatorServices mediatorServices ) throws AmsException {
   	   	  
   	   	    String sql = null;
   	   	    Object userWorkGroupOid = null;
   	   	    int success = 1;
   	   	    if(StringFunction.isNotBlank(endToEndIdGroupProp.get("userWorkGroupOid"))){
   	   	    	userWorkGroupOid = Long.parseLong(endToEndIdGroupProp.get("userWorkGroupOid"));
   	   	    }
   	   	    sql = "SELECT upload_credit_note_oid FROM credit_notes WHERE end_to_end_id = ? AND a_corp_org_oid = ? AND upload_credit_note_oid != ?";
	   		DocumentHandler	creditListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{endToEndID, endToEndIdGroupProp.get("corpOrgOid"), 
	   		    			endToEndIdGroupProp.get("creditNotesOid")});
	   		if ( creditListDoc != null ) 
	   		{
   	   	     SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
   	         java.sql.Timestamp timeStamp = java.sql.Timestamp.valueOf(sd.format(DateTimeUtility.convertStringDateTimeToDate(DateTimeUtility.getGMTDateTime())));	    
   	   	     String historySQl = "INSERT into INVOICE_HISTORY (INVOICE_HISTORY_OID, ACTION_DATETIME, ACTION, A_USER_OID,P_UPLOAD_INVOICE_OID,INVOICE_STATUS) " +
   	                   " VALUES (?,?,?,?,?,?)";
   	   	     String outgoingQueueSql = "INSERT INTO OUTGOING_QUEUE (QUEUE_OID, DATE_CREATED, STATUS, MSG_TYPE, MESSAGE_ID, PROCESS_PARAMETERS) VALUES (?,?,?,?,?,?)";
   	   	     String invoiceAction = TradePortalConstants.UPLOAD_INV_ACTION_AUTH;
   	   	     String preProcessParameter = "creditNoteInd=Y|creditNoteUpdateInd=N";
   	   	     
   	         PreparedStatement credDatapstmt = null;
   	         PreparedStatement credHistorypstmt = null;
   	         PreparedStatement panelAuthorizerstmt = null;
   	         PreparedStatement outgoingQueuestmt = null;
   	         Connection con =null;
   	   	     try  {
   	   	    	con = DatabaseQueryBean.connect(true);
   	            con.setAutoCommit(false);
   	   		    List<DocumentHandler> creditNoteList = creditListDoc.getFragmentsList("/ResultSetRecord");
   	   		    	
   	   		    if ( TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(authorizeType)  || 
   	   	   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType) ||
   	   	   	    			TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
   	   		    {
   	   		    	if(TradePortalConstants.USER_AUTH_LEVEL_FIRST.equals(authorizeLabel))
   	   		    	{
   	   		    	 sql = "UPDATE credit_notes " +
   	   		    	 	   "SET credit_note_status = ?, a_first_authorizing_user_oid = ?, a_first_auth_work_group_oid= ?, first_authorize_status_date = ? " +
   	   		    	 	   "WHERE upload_credit_note_oid = ? ";
   	   		    	 
   	   		    	}
   	   		    	else
   	   		    	{
   	   		    	 sql = "UPDATE credit_notes " +
   	     		    	   "SET credit_note_status = ?, a_second_authorizing_user_oid = ?, a_second_authorizing_work_group_oid = ?, second_authorize_status_date = ? " +
   	     		    	   "WHERE upload_credit_note_oid = ? ";
   	   		    	}
   	   		                credHistorypstmt = con.prepareStatement(historySQl);
   	   		                if ( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(newStatus) ) {
   	   		    	        credDatapstmt = con.prepareStatement(sql);
   	   		                }
   	   		                if(isSendToTPS){
   	   		    	          outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
   	   		                }
   	   			           		            
   	   			            for (DocumentHandler doc : creditNoteList){
   	   			            	   	   			            	
   	   	                        // Invoice History Update 	   			            	
   	   			                insertInvoiceHistory(credHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
 				            			doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), newStatus);
   	   			                
   	   			            	// Invoices update of invoice group
   	   			            	if( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(newStatus) ){
   	   			                updateUserAuthotizeActionData(credDatapstmt, timeStamp, endToEndIdGroupProp.get("userOid"), userWorkGroupOid, 
   	   			                		doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), newStatus);
   	   			            	}
   	   			                
   	   			                // Insert Outgoingqueue entry
   	   			                if(isSendToTPS) {
   	   			     	   	      StringBuilder processParameter = new StringBuilder(preProcessParameter);
   	   			                  processParameter.append("|status=").append(newStatus);
   	   			                  processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"));
   	   			                  createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
   	   			                }
   	   			            }
   	   			            
   	   			            int[] succesCount = credHistorypstmt.executeBatch();
	   			            if (succesCount.length != creditNoteList.size()){
	   			            	success = 0;
	   			            }
	   			            if ( credDatapstmt != null){
   	   			              succesCount = credDatapstmt.executeBatch();
   	   			              if (succesCount.length != creditNoteList.size()){
   	   			                success = 0;
   	   			              }
	   			            }
	   			            if (outgoingQueuestmt != null) {
   	   			              succesCount = outgoingQueuestmt.executeBatch();
	   			              if (succesCount.length != creditNoteList.size()){
	   			            	success = 0;
	   			              } 
	   			            }
   	   		    	}
   	   		    	else if (TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(authorizeType))
   			    	{
   			    		     
   	   		    	      String panelAuthorizerSQl = "INSERT into panel_authorizer (panel_authorizer_oid, panel_code, a_user_oid, auth_date_time, owner_object_type, p_owner_oid) " +
   	  	                                      " VALUES (?,?,?,?,?,?)";
   	   		    		     sql = "UPDATE credit_notes " +
   					    	 	   "SET credit_note_status = ?, a_panel_auth_group_oid = ?, a_panel_auth_range_oid = ?, panel_oplock_val = ? " +
   					    	 	   "WHERE upload_credit_note_oid = ? ";
   					    	    	
   	   		    		            credHistorypstmt = con.prepareStatement(historySQl);
   	   		    		            if( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(newStatus) ){
   					    	          credDatapstmt = con.prepareStatement(sql);   					    	        
   					    	          panelAuthorizerstmt =  con.prepareStatement(panelAuthorizerSQl);
   	   		    		            }
   	   		    		            if(isSendToTPS) {
   					    	          outgoingQueuestmt = con.prepareStatement(outgoingQueueSql);
   	   		    		            }
   						            for (DocumentHandler doc : creditNoteList){
   						            	 						            	
   				                        // Invoice History Update
   						            	insertInvoiceHistory(credHistorypstmt, timeStamp, invoiceAction, endToEndIdGroupProp.get("userOid"), 
   						            			doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), newStatus);
   						            	
   						            	if( !TradePortalConstants.CREDIT_NOTE_STATUS_FAUT.equals(newStatus) ){
   						            	 // Invoices update of invoice group		            	  						            	
   						            	 updateUserPanelAuthorizeActionData(credDatapstmt, endToEndIdGroupProp.get("panelGroupOid"), endToEndIdGroupProp.get("panelRangeOid"), 
   						            			endToEndIdGroupProp.get("panelOplockVal"), doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), newStatus);
   						            	
   						            	 // Insert Panel AuthData
   						            	 insertPanelAuth(panelAuthorizerstmt, timeStamp, endToEndIdGroupProp.get("panelLevelCode"), endToEndIdGroupProp.get("userOid"), 
   						            			doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"), "CREDIT_NOTES");
   						            	}
   						            	
   						               // Insert Outgoingqueue entry
   		  	   			                if(isSendToTPS) {
   		  	   			                  StringBuilder processParameter = new StringBuilder(preProcessParameter);
   		  	   			                  processParameter.append("|status=").append(newStatus);
   		  	   			                  processParameter.append("|invoice_oid=").append(doc.getAttribute("/UPLOAD_CREDIT_NOTE_OID"));
   		  	   			                  createOutgoingQueueEntry(outgoingQueuestmt, timeStamp, processParameter.toString());
   		  	   			                }
   						            	
   						            }

   						            int[] succesCount = credHistorypstmt.executeBatch();
						            if (succesCount.length != creditNoteList.size()){
						            	success = 0;
						              }
   						            if(credDatapstmt != null){
   						              succesCount = credDatapstmt.executeBatch();
   						              if (succesCount.length != creditNoteList.size()){
   						            	success = 0;
   						              }
   						            }
   						            if(panelAuthorizerstmt != null){
   						             succesCount = panelAuthorizerstmt.executeBatch();
						             if (succesCount.length != creditNoteList.size()){
						            	 success = 0;
						             }
   						            }
						            if(outgoingQueuestmt != null){
						             succesCount = outgoingQueuestmt.executeBatch();
   						             if (succesCount.length != creditNoteList.size()){
   						            	success = 0;
   						             }
						            }
   			    	}
   	   		 	   	     
   	   	        con.commit();
   	   	        
   	        } catch (AmsException |  SQLException e) {
   	        	 e.printStackTrace();
   	             throw new AmsException(e);
   	        }
   	        finally{
   	        	try{
   	       	     if (credDatapstmt!= null) credDatapstmt.close();
   	       	     if (credHistorypstmt!= null) credHistorypstmt.close();
   	       	     if (panelAuthorizerstmt!= null) panelAuthorizerstmt.close();
   	       	     if (outgoingQueuestmt!= null) outgoingQueuestmt.close();
   	       	     if (con!= null) con.close();
   	           } catch(SQLException  e){
   	        	   e.printStackTrace();
   	        	}
   	        }
	   	  }
   	   	  return success;
   	   	}
        
   	 /**
   	  * This method is used to insert user action data in Invoice_history table.
   	  * 
   	  * @param pstmt
   	  * @param timeStamp
   	  * @param invoiceAction
   	  * @param userOid
   	  * @param invoiceOid
   	  * @param newStatus
   	 * @throws AmsException 
   	 * @throws SQLException 
   	  */
   	   private void insertInvoiceHistory( PreparedStatement pstmt, java.sql.Timestamp timeStamp, String invoiceAction, String userOid, String invoiceOid, String newStatus) throws SQLException, AmsException{
   		
   		pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());
   		pstmt.setTimestamp(2, timeStamp);
   		pstmt.setString(3, invoiceAction);
   		pstmt.setLong(4, Long.parseLong(userOid));
   		pstmt.setLong(5, Long.parseLong(invoiceOid));
   		pstmt.setString(6, newStatus);
   		pstmt.addBatch();
   		
   	   }
   	   
   	   /**
   	    * This method is used to set authroize User action data for invoice.
   	    * @param pstmt
   	    * @param timeStamp
   	    * @param userOid
   	    * @param userWorkGroupOid
   	    * @param invoiceOid
   	    * @param newStatus
   	 * @throws SQLException 
   	    */
   	   private void updateUserAuthotizeActionData( PreparedStatement pstmt, java.sql.Timestamp timeStamp, String userOid, 
   			   Object userWorkGroupOid, String invoiceOid, String newStatus ) throws SQLException{
   		   pstmt.setString(1, newStatus);
   		   pstmt.setLong(2, Long.parseLong(userOid));
   		   pstmt.setObject(3, userWorkGroupOid);
   		   pstmt.setTimestamp(4, timeStamp);
   		   pstmt.setLong(5, Long.parseLong(invoiceOid));
   		   pstmt.addBatch();
   	   }
   	   
   	   /**
   	    * This method is used to update panel authorize related data.
   	    * @param pstmt
   	    * @param panelGroupOid
   	    * @param panelRangeOid
   	    * @param panelOplockVal
   	    * @param invoiceOid
   	    * @param newStatus
   	 * @throws SQLException 
   	    */
   	   
   	   private void updateUserPanelAuthorizeActionData(PreparedStatement pstmt, String panelGroupOid, String panelRangeOid, 
   			   String panelOplockVal, String invoiceOid, String newStatus) throws SQLException{
   		  pstmt.setString(1, newStatus);
   		  pstmt.setLong(2, Long.parseLong(panelGroupOid));
   		  pstmt.setLong(3, Long.parseLong(panelRangeOid));
   		  pstmt.setLong(4, Long.parseLong(panelOplockVal));
   		  pstmt.setLong(5, Long.parseLong(invoiceOid));
   		  pstmt.addBatch();
   	   }
   	   
   	   /**
   	    * This method is used to insert panel auth data related to invoice.
   	    * @param pstmt
   	    * @param timeStamp
   	    * @param panelCode
   	    * @param userOid
   	    * @param objectOwnerOid
   	    * @param objectOwnerType
   	    * @param PanelCode
   	 * @throws AmsException 
   	 * @throws SQLException 
   	    */
   	   private void insertPanelAuth(PreparedStatement pstmt, java.sql.Timestamp timeStamp, String panelCode, String userOid, 
   			   String objectOwnerOid, String objectOwnerType ) throws SQLException, AmsException{
   		  pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());
  		  pstmt.setString(2, panelCode);
  		  pstmt.setLong(3, Long.parseLong(userOid));
  		  pstmt.setTimestamp(4, timeStamp);
  		  pstmt.setString(5, objectOwnerType);
  		  pstmt.setLong(6, Long.parseLong(objectOwnerOid));
  		  pstmt.addBatch();
   	   }
   	   
   	   /**
   	    * This method is used to create entry in outgoing queue table.
   	    * @param pstmt
   	    * @param timeStamp
   	    * @param process_parameters
   	 * @throws AmsException 
   	 * @throws SQLException 
   	    */
   	   private void createOutgoingQueueEntry( PreparedStatement pstmt, java.sql.Timestamp timeStamp, String process_parameters ) throws SQLException, AmsException{
   		  pstmt.setLong(1, ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID());
 		  pstmt.setTimestamp(2, timeStamp);
 		  pstmt.setString(3, TradePortalConstants.OUTGOING_STATUS_STARTED);
 		  pstmt.setString(4, MessageType.BULKINVUPL);
 		  pstmt.setString(5, InstrumentServices.getNewMessageID());
 		  pstmt.setString(6, process_parameters);
 		  pstmt.addBatch(); 
   	   }
   	   
   	   /**
   	    * This method is used to insert detail of applied invoice detail on which credit note has been applied
   	    * @param appliedInvDetail
   	    * @param mediatorServices
   	    * @return
   	    * @throws AmsException
   	    * @throws RemoteException
   	    */
   	   private String createCredAppliedInvDetailEntry( String appliedInvDetail, String messageID, MediatorServices mediatorServices) throws AmsException, RemoteException {
   		 CredAppliedInvDetail credAppliedInvDetail= (CredAppliedInvDetail) mediatorServices.createServerEJB("CredAppliedInvDetail");
		 String invDetailOid = String.valueOf(credAppliedInvDetail.newObject()); 
		 credAppliedInvDetail.setAttribute("invoice_detail", appliedInvDetail);	
		 credAppliedInvDetail.setAttribute("message_id", messageID);
		 int success = credAppliedInvDetail.save();
		 if( success != 1){
			 throw new AmsException("Error occurred while inserting Applied invoice Detail");
		 }
		 return invDetailOid;
   	   }
        
	// Nar CR 914A Release9.2 11/20/2014 End
   	   
   	   //CR1006 start
   	   
   	private DocumentHandler declineInvoice(DocumentHandler inputDoc,MediatorServices mediatorServices)
	throws RemoteException, AmsException {
	String userOid = inputDoc.getAttribute("/InvoiceGroupList/userOid");
	String corpOrgOid = inputDoc.getAttribute("/InvoiceGroupList/ownerOrg");
	String securityRights = inputDoc.getAttribute("/InvoiceGroupList/securityRights");
	String subsAuth =inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
	String buyerName = null;
	String userOrgOid = null;
	if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_INVOICE_DECLINE_AUTH)) {
		// user does not have appropriate authority.
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
				mediatorServices.getResourceManager().getText(
						"CorpCust.PayablesMgmtInvoiceDecline",
						TradePortalConstants.TEXT_BUNDLE));
		return new DocumentHandler();
	}
	ResourceManager resMgr = mediatorServices.getResourceManager();
	ErrorManager errMgr = mediatorServices.getErrorManager();
	User user = (User) mediatorServices.createServerEJB("User",
			Long.parseLong(userOid));
	userOrgOid = user.getAttribute("owner_org_oid");
	List<DocumentHandler> invoiceGroupList = inputDoc.getFragmentsList("/InvoiceGroupList/InvoiceGroup");
	if (invoiceGroupList == null || invoiceGroupList.size() == 0) {
		errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.NO_ITEM_SELECTED,
				resMgr.getText("CorpCust.PayablesMgmtInvoiceDecline",TradePortalConstants.TEXT_BUNDLE));
		return new DocumentHandler();
	}
	
	String userWorkGroupOid = user.getAttribute("work_group_oid");
	CorporateOrganization corpOrg = null;
	InvoiceProperties invProp = new InvoiceProperties();
	invProp.setCorpOrgOid(corpOrgOid);
	invProp.setUserOid(userOid);
	invProp.setUserWorkGroupOid(userWorkGroupOid);
	invProp.setMediatorServices(mediatorServices);
	invProp.setSubsAuth(subsAuth);
	// create Map to store END_TO_END_ID info
	Map<String, String> endToEndIdGroupProp = new HashMap<String, String>();
	endToEndIdGroupProp.put("corpOrgOid", corpOrgOid);
	endToEndIdGroupProp.put("userOid", userOid);
	endToEndIdGroupProp.put("userWorkGroupOid", userWorkGroupOid);
	endToEndIdGroupProp.put("subsAuth", subsAuth);
	// Process each Invoice Group in the list
	GROUPS_LIST: 
	for (DocumentHandler invoiceGroupItem : invoiceGroupList) {
           String[] invoiceData = invoiceGroupItem.getAttribute("InvoiceData").split("/");
           String invoiceGroupOid = invoiceData[0];
           String optLock = invoiceData[1];
           invProp.setGroupHasBeenSaved(false);
           InvoiceGroup invoiceGroup = (InvoiceGroup) mediatorServices.createServerEJB("InvoiceGroup",
                     Long.parseLong(invoiceGroupOid));
           String invoiceStatus = invoiceGroup.getAttribute("invoice_status");
           String groupDescription = invoiceGroup.getAttribute("trading_partner_name");
           invProp.setGroupDescription(groupDescription);
                
           // only certain invoice statuses can be authorized.
           if (!InvoiceUtility.isInvoiceCRDeclinable(resMgr.getText("InvoiceGroups.InvoiceGroup",
                     TradePortalConstants.TEXT_BUNDLE),groupDescription,invoiceStatus,mediatorServices)) {
                 continue;
            }
              
                invoiceGroup.setAttribute("opt_lock", optLock);
                String invoiceGroupAmount = null;
                String invoiceGroupCurrency = null;
                String invoiceGroupPayAmt = "";
                String panelAuthTransactionStatus = null;
                String panelGroupOid = null;
                String panelOplockVal = null;
                String panelRangeOid = null;
                
                BigDecimal amountInBaseToPanelGroup = null ;
                if (corpOrg == null) {
                    corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
                            Long.parseLong(corpOrgOid));
                }
                String authorizeType = corpOrg.getAttribute("dual_auth_pay_dec_inv");
                
                
                String clientBankOid = corpOrg.getAttribute("client_bank_oid");
                String groupAmountsql = "select AMOUNT, PAYMENT_AMOUNT, CURRENCY from INVOICE_GROUP_VIEW where INVOICE_GROUP_OID = ? ";
                DocumentHandler invoiceAmountDoc = DatabaseQueryBean.getXmlResultSet(groupAmountsql, true, new Object[]{invoiceGroupOid});
                if(invoiceAmountDoc !=null){
                    invoiceGroupAmount = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
                    invoiceGroupCurrency = (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
                    invoiceGroupPayAmt =  (invoiceAmountDoc.getFragment("/ResultSetRecord")).getAttribute("PAYMENT_AMOUNT");
                }
                // Check threshold amount start
                
                String baseCurrency = corpOrg.getAttribute("base_currency_code");
                invoiceGroupPayAmt  = StringFunction.isNotBlank(invoiceGroupPayAmt)?invoiceGroupPayAmt:invoiceGroupAmount;
                
                String selectClause = "select distinct upload_invoice_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) " +
                        "then amount else payment_amount end as payment_amount from invoices_summary_data ";
                
                boolean isValid = InvoiceUtility.checkThresholds(invoiceGroupPayAmt,invoiceGroupCurrency , baseCurrency,
                        "upload_pay_inv_thold", "upload_pay_inv_dlimit", userOrgOid,clientBankOid, user ,selectClause, false,subsAuth, mediatorServices);
                if(!isValid){
                    return new DocumentHandler();
                }
                
                //threshold check end
                
                String sql = "select UPLOAD_INVOICE_OID from INVOICES_SUMMARY_DATA where A_INVOICE_GROUP_OID = ? ";
                DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceGroupOid});
                if (invoiceListDoc != null) {
                    List<DocumentHandler> records = invoiceListDoc.getFragmentsList("/ResultSetRecord");
                    for (DocumentHandler doc : records) {
                        String invoiceOid = doc.getAttribute("UPLOAD_INVOICE_OID");
                        
                        if (StringFunction.isNotBlank(invoiceOid)) {
                            InvoicesSummaryData invoiceSummaryData =
                                    (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData",Long.parseLong(invoiceOid));
                            if(!TradePortalConstants.INDICATOR_YES.equals(invoiceSummaryData.getAttribute("portal_approval_requested_ind"))){
                            	//This case will never happen in prod as client will not decline if invoices r not H2H uploaded
                            	errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.CANNOT_DECLINE);
                            		return new DocumentHandler();
                            }
                            
                            String firstAuthorizer          = invoiceSummaryData.getAttribute("first_authorizing_user_oid");
                            String firstAuthorizerWorkGroup = invoiceSummaryData.getAttribute("first_authorizing_work_group_oid");
                            String newStatus = invoiceSummaryData.getAttribute("invoice_status");
                            boolean isSendToTPS= false;
                            boolean isUpdateAttachment = false;
                            
                            String tpName=invoiceSummaryData.getTpRuleName();
                            invoiceSummaryData.setAttribute("tp_rule_name", buyerName);
                            
                            if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                                    && StringFunction.isBlank(userWorkGroupOid))
                            {
                                newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
                                
                                if (!invProp.isGroupHasBeenSaved()) {
                                    saveInvoiceGroup(newStatus, invoiceGroup,invProp);
                                    errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DEC_WORK_GROUP_REQUIRED,
                                    		resMgr.getText("CorpCust.RecDeclineInvoice", TradePortalConstants.TEXT_BUNDLE));
                                }
                                if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
                                    invoiceSummaryData.setAttribute("invoice_group_oid",
                                            invProp.getNewGroupOid());
                                }
                                saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
                                    || TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType))
                            {
                            	declineWithDiffWorkGroup(invoiceSummaryData, invoiceGroup,  firstAuthorizer,
                                        firstAuthorizerWorkGroup,  authorizeType, errMgr, resMgr,invProp);
                            }
                            else {
                                // dual auth ind is NO, first auth is EMPTY
                                newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;
                                if (!invProp.isGroupHasBeenSaved()) {
                                    saveInvoiceGroup(newStatus, invoiceGroup, invProp);
                                    errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
                                            resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),
                                            groupDescription,resMgr.getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
                                    
                                }
                                if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
                                    invoiceSummaryData.setAttribute("invoice_group_oid",
                                            invProp.getNewGroupOid());
                                }
                                invoiceSummaryData.setAttribute("first_authorizing_user_oid", userOid);
                                invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
                                invoiceSummaryData.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
                                isSendToTPS = true;
                                saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
                            }
                            
                        }
                    }
                }
            
	}
	return new DocumentHandler();
}
   	private void declineWithDiffWorkGroup(InvoicesSummaryData invoiceSummaryData,InvoiceGroup invoiceGroup, String firstAuthorizer
    		,String firstAuthorizerWorkGroup,String authorizeType,
    		ErrorManager errMgr,ResourceManager resMgr,InvoiceProperties invProp) throws RemoteException, AmsException
    	{
    		String invoiceOid = invoiceSummaryData.getAttribute("upload_invoice_oid");
    		String newStatus="";
    		
    		boolean isSendToTPS=false;
    		boolean isUpdateAttachment = false;
    		if (StringFunction.isBlank(firstAuthorizer)) {
				// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
				  newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PDC;

				if (!invProp.isGroupHasBeenSaved()) {
					saveInvoiceGroup(newStatus, invoiceGroup,invProp);
					errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
								resMgr.getText("InvoiceGroups.InvoiceGroup",TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription());
					
				}

				invoiceSummaryData.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
				invoiceSummaryData.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
				invoiceSummaryData.setAttribute("invoice_status", newStatus);
			}
			else {// firstAuthorizer not empty
				if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
						&& invProp.getUserWorkGroupOid().equals(firstAuthorizerWorkGroup)) {
					// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
					if (!invProp.isGroupHasBeenSaved()) {
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);
						
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					
				}
				else if (firstAuthorizer.equals(invProp.getUserOid())) {
					// Two authorizers required and this user is the same as the
					// first authorizer. Give error.
					// Note requiring two work groups also implies requiring two users.
					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
					if (!invProp.isGroupHasBeenSaved()) {
						
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);
						
					}
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					
				}
				else {// Authorize
					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;

					if (!invProp.isGroupHasBeenSaved()) {
						saveInvoiceGroup(newStatus, invoiceGroup,invProp);
						errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANSACTION_PROCESSED,
								resMgr.getText("InvoiceGroups.InvoiceGroup", TradePortalConstants.TEXT_BUNDLE),invProp.getGroupDescription(),
								resMgr.getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
					}

					invoiceSummaryData.setAttribute("second_authorizing_user_oid", invProp.getUserOid());
					invoiceSummaryData.setAttribute("second_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
					invoiceSummaryData.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
					invoiceSummaryData.setAttribute("invoice_status", newStatus);
					isSendToTPS= true;
					isUpdateAttachment = true;
				}
				
			}
    		if (StringFunction.isNotBlank(invProp.getNewGroupOid())) {
				invoiceSummaryData.setAttribute("invoice_group_oid",
						invProp.getNewGroupOid());
			}
    		 saveInvoiceSummaryData(newStatus, invoiceSummaryData, invoiceOid, isSendToTPS, isUpdateAttachment, invProp);
    	     
    	}
   	
	private DocumentHandler declineCreditNote(DocumentHandler inputDoc,MediatorServices mediatorServices)
    throws RemoteException, AmsException {

		String userOid = inputDoc.getAttribute("/UploadCreditNote/userOid");
		String corpOrgOid = inputDoc.getAttribute("/UploadCreditNote/ownerOrg");
		String securityRights = inputDoc.getAttribute("/UploadCreditNote/securityRights");
		String subsAuth = inputDoc.getAttribute("/Authorization/authorizeAtParentViewOrSubs");
		String buyerName = null;
		String userOrgOid = null;
		if (!SecurityAccess.hasRights(securityRights, SecurityAccess.PAY_CREDIT_NOTE_AUTH)) {
			// user does not have appropriate authority.
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ACTION_MATCH_NOTICE_AUTH,
					mediatorServices.getResourceManager().getText(
							"CorpCust.CreditNoteDecline", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		ResourceManager resMgr = mediatorServices.getResourceManager();
		ErrorManager errMgr = mediatorServices.getErrorManager();
		User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
		userOrgOid = user.getAttribute("owner_org_oid");
		List<DocumentHandler> creditNoteList = inputDoc.getFragmentsList("/UploadCreditNote/CreditNote");
		if (creditNoteList == null || creditNoteList.size() == 0) {
			errMgr.issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, resMgr.getText(
							"CorpCust.CreditNoteDecline", TradePortalConstants.TEXT_BUNDLE));
			return new DocumentHandler();
		}
		LOG.debug("InvoiceGroupMediatorBean: number of Credit Note selected: {} ", creditNoteList.size());

		String userWorkGroupOid = user.getAttribute("work_group_oid");
		CorporateOrganization corpOrg = null;
		InvoiceProperties invProp = new InvoiceProperties();
		invProp.setCorpOrgOid(corpOrgOid);
		invProp.setUserOid(userOid);
		invProp.setUserWorkGroupOid(userWorkGroupOid);
		invProp.setMediatorServices(mediatorServices);
		
		// Process each credit Note in the list
		for (DocumentHandler creditNoteItem : creditNoteList) {
			String[] creditNoteData = creditNoteItem.getAttribute("CreditData").split("/");
			String creditNotesOid = creditNoteData[0];
			CreditNotes creditNote = (CreditNotes) mediatorServices.createServerEJB("CreditNotes", Long.parseLong(creditNotesOid));
			String creditNoteStatus = creditNote.getAttribute("credit_note_status");
			String creditNoteId = creditNote.getAttribute("invoice_id");
           	// only certain invoice statuses can be declined.
    			if ( !(TradePortalConstants.CREDIT_NOTE_STATUS_AVA.equals(creditNoteStatus) ||
    					 TradePortalConstants.UPLOAD_INV_STATUS_RAA.equals(creditNoteStatus) )) {
    				mediatorServices.getErrorManager().issueError( PayableInvoiceGroupMediator.class.getName(),
    						TradePortalConstants.TRANSACTION_CANNOT_PROCESS,resMgr.getText("UploadInvoices.CreditNoteId", TradePortalConstants.TEXT_BUNDLE),
    						creditNoteId,
    						ReferenceDataManager.getRefDataMgr().getDescr(
    						"CREDIT_NOTE_STATUS", creditNoteStatus, mediatorServices.getCSDB().getLocaleName()),
    						resMgr.getText("TransactionAction.Declined", TradePortalConstants.TEXT_BUNDLE));
    				continue;
    			}
    			if(!TradePortalConstants.INDICATOR_YES.equals(creditNote.getAttribute("portal_approval_requested_ind"))){
                	//This case will never happen in prod as client will not decline if invoices r not H2H uploaded
                	errMgr.issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.CANNOT_DECLINE);
                		return new DocumentHandler();
            }
			String creditNoteAmount = null;
			String creditNoteCurrency = null;
			String creditNotePayAmt = "";

			if (corpOrg == null) {
				corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
			}
			String authorizeType = corpOrg.getAttribute("dual_auth_pay_dec_crn");
			String clientBankOid = corpOrg.getAttribute("client_bank_oid");
			String creditNoteAmountsql = "SELECT amount, payment_amount, currency FROM credit_notes where upload_credit_note_oid = ?";
			DocumentHandler creditNoteAmountDoc = DatabaseQueryBean.getXmlResultSet(creditNoteAmountsql, true, new String[]{creditNotesOid});
			if (creditNoteAmountDoc != null) {
				creditNoteAmount = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("AMOUNT");
				creditNoteCurrency = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("CURRENCY");
				creditNotePayAmt = (creditNoteAmountDoc.getFragment("/ResultSetRecord")).getAttribute("PAYMENT_AMOUNT");
			}

			// Check threshold amount start
			String baseCurrency = corpOrg.getAttribute("base_currency_code");
			creditNotePayAmt = StringFunction.isNotBlank(creditNotePayAmt) ? creditNotePayAmt : creditNoteAmount;

			String selectClause = "select distinct upload_credit_note_oid, currency as currency_code , case when (payment_amount is null or payment_amount=0) "
					+ "then amount else payment_amount end as payment_amount from credit_notes ";

			boolean isValid = InvoiceUtility.checkThresholds(creditNotePayAmt,
					creditNoteCurrency, baseCurrency, "pay_credit_note_thold",
					"pay_credit_note_dlimit", userOrgOid, clientBankOid, user,
					selectClause, false, subsAuth, mediatorServices);
			if (!isValid) {
				return new DocumentHandler();
			}

			// threshold check end

			String firstAuthorizer = creditNote.getAttribute("first_authorizing_user_oid");
			String firstAuthorizerWorkGroup = creditNote.getAttribute("first_authorizing_work_group_oid");
			boolean isSendToTPS = false;
			
			// ***********************************************************************
			// If dual authorization is required, set the transaction state to
			// partially authorized if it has not been previously authorized and
			// set
			// the transaction to authorized if there has been a previous
			// authorizer.
			// The second authorizer must not be the same as the first
			// authorizer
			// not the same work groups, depending on the corporate org setting.
			// If dual authorization is not required, set the invoice state to
			// authorized.
			// ***********************************************************************

			//String tpName = creditNote.getTpRuleName();
			creditNote.setAttribute("tp_rule_name", buyerName);

			if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
					&& StringFunction.isBlank(userWorkGroupOid)) {
				saveCreditNote(TradePortalConstants.UPLOAD_INV_STATUS_DCF,creditNote, creditNotesOid, isSendToTPS, invProp, false);
				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(), TradePortalConstants.TRANS_WORK_GROUP_REQUIRED);
			} else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
					|| TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(authorizeType)) {
				declineCreditNoteWithDiffWorkGroup(creditNote,
						firstAuthorizer, firstAuthorizerWorkGroup,
						authorizeType, errMgr, resMgr, invProp);
			} else {
				// dual auth ind is NO, first auth is EMPTY
				creditNote.setAttribute("first_authorizing_user_oid", userOid);
				creditNote.setAttribute("first_authorizing_work_group_oid", userWorkGroupOid);
				creditNote.setAttribute("first_authorize_status_date", DateTimeUtility.getGMTDateTime());
				isSendToTPS = true;
				saveCreditNote(TradePortalConstants.UPLOAD_INV_STATUS_DBC,
						creditNote, creditNotesOid, isSendToTPS, invProp, false);

				errMgr.issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.DECLINED_CRNOTE,
							creditNoteId);
			}
          }
		
		return new DocumentHandler();
	}
	 private void declineCreditNoteWithDiffWorkGroup(CreditNotes creditNote,String firstAuthorizer
	       		,String firstAuthorizerWorkGroup,String authorizeType,
	       		ErrorManager errMgr,ResourceManager resMgr,InvoiceProperties invProp) throws RemoteException, AmsException
	       	{
	       		String creditNoteId = creditNote.getAttribute("invoice_id");
	       		String creditNoteOid = creditNote.getAttribute("upload_credit_note_oid");
	       		String newStatus="";      		
	       		boolean isSendToTPS=false;
	       		if (StringFunction.isBlank(firstAuthorizer)) {
	   				// Two authorizers required and first authorizer is EMPTY: Set to partially authorized.
	   				newStatus = TradePortalConstants.UPLOAD_INV_STATUS_PDC;
	   				creditNote.setAttribute("first_authorizing_user_oid", invProp.getUserOid());
	   				creditNote.setAttribute("first_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
	   				creditNote.setAttribute("invoice_status", newStatus);
	   				
	   				errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_PARTIALLY_AUTHORIZED,
							resMgr.getText("PayableInvoice.PayCreditNote",TradePortalConstants.TEXT_BUNDLE), creditNoteId);
	   			}
	   			else {// firstAuthorizer not empty
	   				if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(authorizeType)
	   						&& invProp.getUserWorkGroupOid().equals(firstAuthorizerWorkGroup)) {
	   					// Two work groups required and this user belongs to the same work group as the first authorizer: Error.
	   					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;   					
	   					 creditNote.setAttribute("credit_note_status", newStatus);
	   					 errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.TRANS_DIFF_WORK_GROUP_REQUIRED);   					
	   				}
	   				else if (firstAuthorizer.equals(invProp.getUserOid())) {
	   					// Two authorizers required and this user is the same as the
	   					// first authorizer. Give error.
	   					// Note requiring two work groups also implies requiring two users.
	   					 newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DCF;
	   					 creditNote.setAttribute("credit_note_status", newStatus);
	   					 errMgr.issueError(PayableInvoiceGroupMediator.class.getName(),TradePortalConstants.DIFFERENT_SECOND_AUTH_REQRUIED);   					
	   				}
	   				else {// Authorize
	   					newStatus = TradePortalConstants.UPLOAD_INV_STATUS_DBC;
	   					creditNote.setAttribute("second_authorizing_user_oid", invProp.getUserOid());
	   					creditNote.setAttribute("second_authorizing_work_group_oid", invProp.getUserWorkGroupOid());
	   					creditNote.setAttribute("second_authorize_status_date", DateTimeUtility.getGMTDateTime());
	   					creditNote.setAttribute("credit_note_status", newStatus);
	   					isSendToTPS= true;
	   					
	   					errMgr.issueError(UploadInvoiceMediator.class.getName(),TradePortalConstants.DECLINED_CRNOTE,
	   							creditNoteId);
	   				} 				
	   			}
	       		
	       		saveCreditNote(newStatus, creditNote, creditNoteOid, isSendToTPS, invProp, false);       		       	     
	       	}
	       
   	//CR1006 end
   	 
}
