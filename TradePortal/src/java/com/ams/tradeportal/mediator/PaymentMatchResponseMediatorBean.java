package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.PayMatchResult;
import com.ams.tradeportal.busobj.PayRemit;
import com.ams.tradeportal.busobj.PayRemitInvoice;
import com.ams.tradeportal.busobj.util.AutoPayMatchUtility;
import com.ams.tradeportal.busobj.util.CertificateUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2008
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class PaymentMatchResponseMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentMatchResponseMediatorBean.class);
    private String autoButton = "";
  // Business methods
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

		String info = inputDoc.toString();
		mediatorServices.debug("The input document is " + info);

		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		mediatorServices.debug("The button pressed is " + buttonPressed);
			
        // if authentication with certificate is required for authorize and reCertification failed, issue an error and stop.
        String reCertification = inputDoc.getAttribute("/Authorization/reCertification");
        mediatorServices.debug("PayRemitMediator:: Recert Reqt: " + reCertification);
        if (TradePortalConstants.INDICATOR_YES.equals(reCertification)
            && !CertificateUtility.validateCertificate(inputDoc, mediatorServices)) {
            return new DocumentHandler();
        }

		try{
			boolean processSuccess;
			boolean validateAuthorizationData = false;
			String userOid = inputDoc.getAttribute("/User/userOid");
			String securityRights = inputDoc.getAttribute("/User/securityRights");
			String payRemitOid = inputDoc.getAttribute("/PayRemit/payRemitOid");
			Vector <DocumentHandler> autoPaymentVec = inputDoc.getFragments("/AutoPayment");
			mediatorServices.debug("autoPaymentVec:"+autoPaymentVec);
			if(autoPaymentVec != null && autoPaymentVec.size()>0){
				String inputButton = inputDoc.getAttribute("/AutoPayment(0)/autoMatchButton");
				if(TradePortalConstants.AUTO_PAY_MATCH.equals(inputButton)){
					autoButton = inputButton;
				}else{
					autoButton = "";
				}
			}
			mediatorServices.debug("autoButton:"+autoButton);
			//
			//AAlubala CR711 Rel8.0 - Check for offline Authorize also - START
			if(buttonPressed.equals(TradePortalConstants.BUTTON_AUTHORIZE) || 
					buttonPressed.equals(TradePortalConstants.BUTTON_APPROVEDISCOUNTAUTHORIZE)
					|| TradePortalConstants.BUTTON_PROXY_AUTHORIZE
					.equalsIgnoreCase(buttonPressed)
			|| TradePortalConstants.BUTTON_PROXY_APPROVEDISCOUNTAUTHORIZE
					.equalsIgnoreCase(buttonPressed)){
				validateAuthorizationData = true;
			}
			
			processSuccess = performSave(inputDoc, validateAuthorizationData, mediatorServices );
			if(processSuccess == true){
				if (buttonPressed.equals(TradePortalConstants.BUTTON_AUTHORIZE) || buttonPressed.equals(TradePortalConstants.BUTTON_PROXY_AUTHORIZE)){
					processSuccess = PayRemitMediatorBean.performAuthorize(userOid, securityRights, payRemitOid, false, mediatorServices);							
				}else if (buttonPressed.equals(TradePortalConstants.BUTTON_APPROVEDISCOUNTAUTHORIZE) || buttonPressed.equals(TradePortalConstants.BUTTON_PROXY_APPROVEDISCOUNTAUTHORIZE)){
					processSuccess = PayRemitMediatorBean.performAuthorize(userOid, securityRights, payRemitOid, true, mediatorServices);		
				}
			}
			//
			//CR711 END		
			//Srinivasu_D CR#997 Rel9.3 04/23/2015 - Added this to invoke utility class for payment creation
			String autoButtonName = null;
			String erpSetting = null;			
			
			if(autoPaymentVec != null && autoPaymentVec.size()>0){
			autoButtonName = inputDoc.getAttribute("/AutoPayment(0)/autoMatchButton");	
			erpSetting = inputDoc.getAttribute("/AutoPayment(0)/erpSettings");
			//autoButton =  inputDoc.getAttribute("/AutoPayment(0)/autoMatchButton");	
			mediatorServices.debug("erpSetting:"+erpSetting+"\t autoButton:"+autoButton);
			mediatorServices.debug("last buttonName:"+autoButtonName);
			if(TradePortalConstants.INDICATOR_YES.equals(erpSetting)){
			if(StringFunction.isNotBlank(autoButtonName)){		
				
				AutoPayMatchUtility autoPayMatchUtilObj = new AutoPayMatchUtility();		
			
			if(TradePortalConstants.AUTO_PAY_UNMATCH.equals(autoButtonName)){
				
				long oid = autoPayMatchUtilObj.deletePayMatchResult(inputDoc,mediatorServices);
				outputDoc.setAttribute("/payInvOid", "");
				
			}

			else if(TradePortalConstants.AUTO_PAY_MATCH.equals(autoButtonName)){
			
				long oid = autoPayMatchUtilObj.createPayMtchResult(inputDoc,mediatorServices);
				mediatorServices.debug("oid:"+oid);
				if(oid !=0){
					mediatorServices.debug("payoid:"+inputDoc.getAttribute("/AutoPayment(0)/payInvOid"));
				//outputDoc.setAttribute("/autoOid", String.valueOf(oid));
				outputDoc.setAttribute("/payInvOid", inputDoc.getAttribute("/AutoPayment(0)/payInvOid"));
				outputDoc.setAttribute("/payInvoiceID", inputDoc.getAttribute("/AutoPayment(0)/invoiceID"));
				}
			}else if(TradePortalConstants.AUTO_PAY_SEARCH.equals(autoButtonName)){
				    long oid = autoPayMatchUtilObj.createPayMtchResult(inputDoc, mediatorServices);
					mediatorServices.debug("oid:"+oid);
					outputDoc.setAttribute("/payInvOid", "");
			 }
			} 
			}
			//Srinivasu_D CR#997 Rel9.3 04/23/2015 - End
		 }	
		} catch(Exception e){
			e.printStackTrace();
		}

		return outputDoc;
	}

	private boolean validateAuthorizationData(BigDecimal amountUnmatched, String unapplyRemainingBalance){
	    // W Zhu 2/9/09 VIUJ011957614 Use > 0 instead of != 0.
        // When unmatched amount < 0 (amount matched > payment amount), that error is caught in authorization.
        //if(amountUnmatched.compareTo(BigDecimal.ZERO) != 0){
		if(amountUnmatched.compareTo(BigDecimal.ZERO) > 0){
			if(unapplyRemainingBalance.equals(TradePortalConstants.INDICATOR_YES)){
				return true;
			}
			return false;
		}
		return true;
	}

  private boolean performSave(DocumentHandler inputDoc, boolean validateForAuthorization, MediatorServices mediatorServices)throws AmsException, RemoteException{

	  Long payRemitOid = Long.valueOf(inputDoc.getAttribute("/PayRemit/payRemitOid"));
	  String buyerId = inputDoc.getAttribute("/PayRemit/buyer_reference");
	  String buyerName = inputDoc.getAttribute("/PayRemit/buyer_name");
	  BigDecimal amountUnmatched = inputDoc.getAttributeDecimal("/Match/AmountUnmatched");
	  String unapplyRemainingBalance = inputDoc.getAttribute("/Match/UnapplyRemainingBalance");

	  String updatedStatus = TradePortalConstants.TRANS_STATUS_IN_PROGRESS;
	  String buttonText =inputDoc.getAttribute("/Update/ButtonPressed");
	  boolean isValid = true;
	  if(validateForAuthorization == true){
		  isValid = validateAuthorizationData(amountUnmatched, unapplyRemainingBalance);
		  if(!isValid){
			  updatedStatus = TransactionStatus.AUTHORIZE_FAILED;
              mediatorServices.getErrorManager().issueError(PaymentMatchResponseMediator.class.getName(),
                 TradePortalConstants.AUTHORIZE_INVALID_UNMATCHED_AMOUNT);
		  }
	  }

	  if (!updatePayRemit(payRemitOid, amountUnmatched, unapplyRemainingBalance, buyerId, buyerName, updatedStatus, mediatorServices)) {
         return false;
      }

	  String userOid = inputDoc.getAttribute("/User/userOid");
	  Vector <DocumentHandler> matchItems = inputDoc.getFragments("/MatchItem");
	  Vector <DocumentHandler> newMatchItems = inputDoc.getFragments("/NewMatchItem");
	  Vector <DocumentHandler> unMatchItems = inputDoc.getFragments("/UnMatchItem");
	// W Zhu 9/11/2012 Rel 8.1 T36000004579 add key parameter.
        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
        javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");

        
        for (DocumentHandler currDoc: unMatchItems){
  		  Long matchItemOid = Long.valueOf(EncryptDecrypt.decryptStringUsingTripleDes(currDoc.getAttribute("/unmatchItem"), secretKey));
  		  String matchItemStatus = currDoc.getAttribute("/unmatchItemStatus");
  		  String amount = currDoc.getAttribute("/unmatchItemMatchAmount");
  		  BigDecimal matchItemMatchAmount = new BigDecimal(amount.replace(",",""));
            String outAmount = currDoc.getAttribute("/unmatchItemInvoiceOutstandingAmount");
  		  BigDecimal matchItemInvoiceOutstandingAmount = new BigDecimal(outAmount.replace(",",""));
  		  // Now update these items.
  		  updatePayMatchResult(matchItemOid, matchItemStatus, matchItemMatchAmount, matchItemInvoiceOutstandingAmount, mediatorServices);

  	  }   
        
        
	 for (DocumentHandler currDoc: matchItems){
		  Long matchItemOid = Long.valueOf(EncryptDecrypt.decryptStringUsingTripleDes(currDoc.getAttribute("/matchItem"), secretKey));
		  String matchItemStatus = currDoc.getAttribute("/matchItemStatus");
		  String amount = currDoc.getAttribute("/matchItemMatchAmount");
		  BigDecimal matchItemMatchAmount = new BigDecimal(amount.replace(",",""));
          String outAmount = currDoc.getAttribute("/matchItemInvoiceOutstandingAmount");
		  BigDecimal matchItemInvoiceOutstandingAmount = new BigDecimal(outAmount.replace(",",""));
		  // Now update these items.
		  updatePayMatchResult(matchItemOid, matchItemStatus, matchItemMatchAmount, matchItemInvoiceOutstandingAmount, mediatorServices);

	  }

	  for (DocumentHandler currDoc: newMatchItems) {
		  String matchItemOid = EncryptDecrypt.decryptStringUsingTripleDes(currDoc.getAttribute("/newMatchItem"), secretKey);
		  String payRemitInvoiceOid = EncryptDecrypt.decryptStringUsingTripleDes(currDoc.getAttribute("/payRemitInvoiceOid"), secretKey);
		  String matchItemStatus = currDoc.getAttribute("/newMatchItemStatus");
		  String amount =  currDoc.getAttribute("/newMatchItemMatchAmount");
		  BigDecimal matchItemMatchAmount = new BigDecimal(amount.replace(",",""));
		  String outAmount = currDoc.getAttribute("/newMatchItemInvoiceOutstandingAmount");
          BigDecimal matchItemInvoiceOutstandingAmount = new BigDecimal(outAmount.replace(",",""));
		  // Now insert these items.
		  insertPayMatchResult(payRemitOid, matchItemOid, payRemitInvoiceOid, matchItemStatus, matchItemMatchAmount, matchItemInvoiceOutstandingAmount, userOid, mediatorServices,buttonText );

	  }

	  return isValid;
  }

  protected boolean updatePayRemit(Long payRemitOid, BigDecimal amountUnmatched, String unapplyRemainingBalance, String buyerId, String buyerName, String transStatus, MediatorServices mediatorServices)throws AmsException, RemoteException{
	  PayRemit bean = (PayRemit) mediatorServices.createServerEJB("PayRemit");
	  bean.getData(payRemitOid.longValue());

      // Cannot update if the Pay Remit is authorized.  This could happen if another user
      // has authorized it while it is open.
      String transactionStatus = bean.getAttribute("transaction_status");
      if (transactionStatus.equals(TransactionStatus.AUTHORIZED)) {
          mediatorServices.getErrorManager().issueError(
                  PayRemitMediator.class.getName(),
                  TradePortalConstants.TRANSACTION_CANNOT_PROCESS,
                  mediatorServices.getResourceManager().getText(
                          "ARMatchNotices.ReceivablesMatchNotices",
                          TradePortalConstants.TEXT_BUNDLE),
                  bean.getAttribute("payment_invoice_reference_id") ,
                  ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_STATUS",
                          bean.getAttribute("transaction_status"),
                          mediatorServices.getCSDB().getLocaleName()),
                  mediatorServices.getResourceManager().getText(
                          "TransactionAction.Saved",
                          TradePortalConstants.TEXT_BUNDLE));
          return false;
      }


	  String currentBuyerId = bean.getAttribute("seller_id_for_buyer");
	  if(currentBuyerId == null || "".equals(currentBuyerId)){
		  if(buyerId != null && !"".equals(buyerId)){
			  bean.setAttribute("seller_id_for_buyer", buyerId);
			  bean.setAttribute("seller_name_for_buyer", buyerName);
			  bean.setAttribute("buyer_added_in_portal_indicator", TradePortalConstants.INDICATOR_YES);
		  }
	  }

	  if(TradePortalConstants.INDICATOR_YES.equals(unapplyRemainingBalance)){
		  bean.setAttribute("unapplied_payment_amount",amountUnmatched.toString());
	  }else {
		  bean.setAttribute("unapplied_payment_amount", "0");
	  }

	  bean.setAttribute("transaction_status", transStatus);
	  bean.save(true);
      return true;
  }

  protected void updatePayMatchResult(Long payMatchResultOid, String status, BigDecimal amount, BigDecimal invoiceOutstandingAmount, MediatorServices mediatorServices) throws AmsException, RemoteException{
	
	  PayMatchResult bean = (PayMatchResult) mediatorServices.createServerEJB("PayMatchResult");
	  bean.getData(payMatchResultOid.longValue());
	  bean.setAttribute("matched_amount", amount.toString());
	  bean.setAttribute("invoice_matching_status", status);
      bean.setAttribute("invoice_outstanding_amount", invoiceOutstandingAmount.toString());
	  bean.save(true);
	
  }

  protected void insertPayMatchResult(Long payRemitOid, String matchItemOid, String payRemitInvoiceOid, String status, BigDecimal amount, BigDecimal invoiceOutstandingAmount, String user_oid, MediatorServices mediatorServices,String buttonText) throws AmsException, RemoteException{
	  
	  if("NMD".equals(status)){
		  return;
	  }
	  mediatorServices.debug("payRemitOid:"+payRemitOid+"\tmatchItemOid:"+matchItemOid+"\tpayRemitInvoiceOid:"+payRemitInvoiceOid+"\tstatus:"+status+"\tuser_oid:"+user_oid);
	  PayMatchResult bean = (PayMatchResult) mediatorServices.createServerEJB("PayMatchResult");
	  //Rpasupulati IR T36000031139 Start: Commenting below code as it is throwing null pointer exception as invoiceOid is not match Item Oid.
	  //creating bean using PayMatchResult bean and getting invoiceOid and payRemitInvoiceOid
	  /*Invoice invoice = (Invoice)mediatorServices.createServerEJB("Invoice");
	  invoice.getData(Long.valueOf(invoiceOid));*/
	  String invoiceOid = null;
	  Invoice invoice = null;

	  if(StringFunction.isNotBlank(autoButton) && TradePortalConstants.AUTO_PAY_MATCH.equals(autoButton)){
		  buttonText = autoButton;
	  }
	   if(StringFunction.isNotBlank(buttonText)){		 
		  
		  bean.getData(Long.parseLong(matchItemOid));
		  invoiceOid = bean.getAttribute("invoice_oid");
		  payRemitInvoiceOid=bean.getAttribute("pay_remit_invoice_oid");
		 
	  }else{		
		  invoice = (Invoice)mediatorServices.createServerEJB("Invoice");
		  invoice.getData(Long.valueOf(matchItemOid));
		  invoiceOid = matchItemOid;
		  bean.newObject();
		 
		  
	  }
	
	  
	    //PayMatchResult bean = (PayMatchResult) mediatorServices.createServerEJB("PayMatchResult");
	  //bean.newObject();
     
	  Long oid = Long.valueOf(bean.getAttributeLong("pay_match_result_oid"));

	  bean.setAttribute("invoice_matching_status", status);
	  bean.setAttribute("matched_amount", amount.toString());
	  bean.setAttribute("pay_remit_oid",payRemitOid.toString());
	  bean.setAttribute("pay_remit_invoice_oid", payRemitInvoiceOid);
	  bean.setAttribute("invoice_oid", invoiceOid);
	  bean.setAttribute("user_oid", user_oid);
	  bean.setAttribute("manual_indicator", TradePortalConstants.INDICATOR_YES);
      bean.setAttribute("invoice_outstanding_amount", invoiceOutstandingAmount.toString());

	  // Populate some fields from the associated Invoice object.
      //Rpasupulati replacing invoice with bean.
      if(StringFunction.isNotBlank(buttonText)){
    	  bean.setAttribute("otl_invoice_uoid", bean.getAttribute("otl_invoice_uoid"));
    	  bean.setAttribute("invoice_reference_id", bean.getAttribute("invoice_reference_id"));
      }else{
    	  bean.setAttribute("otl_invoice_uoid", invoice.getAttribute("otl_invoice_uoid"));
    	  bean.setAttribute("invoice_reference_id", invoice.getAttribute("invoice_reference_id"));  
      }
	  //Rpasupulati IR T36000031139 End
	  
	  if(payRemitInvoiceOid != null && !payRemitInvoiceOid.equals("")){
	      // W Zhu 12/26/08 WAUI121944792 set otl_pay_remit_invoice_uoid
	      PayRemitInvoice payRemitInvoice = (PayRemitInvoice)mediatorServices.createServerEJB("PayRemitInvoice");
	      payRemitInvoice.getData(Long.parseLong(payRemitInvoiceOid));
	      bean.setAttribute("otl_pay_remit_invoice_uoid", payRemitInvoice.getAttribute("otl_pay_remit_invoice_uoid"));
	  }
	  
	  bean.save(true);
	  }
  

  }

