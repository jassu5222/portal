package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;
import javax.ejb.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * This mediator performs one of several methods that deals with phrases.
 * It can perform a lookup of a phrase and append that to an existing 
 * text value.  It can clear a text value.  It can perform fill-in-the-blank
 * logic.
 *
 * For specific details of each function, look at the other methods.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */ 
public class PhraseLookupMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PhraseLookupMediatorBean.class);
	
	
  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	 throws RemoteException, AmsException {

	// Based on the button clicked, process an appropriate action.

	String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
	mediatorServices.debug("The button pressed is " + buttonPressed);	

	if (buttonPressed.equals(TradePortalConstants.BUTTON_PHRASELOOKUP)) {
		outputDoc = performPhraseLookup(inputDoc, mediatorServices);
	} else if (buttonPressed.equals(TradePortalConstants.BUTTON_PHRASECLEAR)) {
		outputDoc = performPhraseClear(inputDoc, mediatorServices);
	} else if (buttonPressed.equals(TradePortalConstants.BUTTON_PHRASEFILLIN)) {
		outputDoc = performPhraseFillIn(inputDoc, mediatorServices);
	} else {
		LOG.info("Error in PhraseLookupMediator - unknown button: " + buttonPressed);
	}
	
	return outputDoc;
  }                
	
	
	
/**
 * This method is similar to performPhraseLookup except no lookup is
 * done and the current text is blanked out.
 *
 * The input XML looks like the following and should contain the
 * oid to look up, the max allowed appended text length, the path
 * in the input doc where to find to text to append to (text_path),
 * and the name of the field the resulting text is for.
 * <In>
 *   <PhraseLookupInfo>
 *     <phrase_oid>76079</phrase_oid>
 *     <text_maxlength>5000</text_maxlength>
 *     <text_path>/Terms/guar_bank_standard_text</text_path>
 *     <text_field_name>guar_bank_standard_text</text_field_name>
 *   </PhraseLookupInfo>
 * </In>
 *
 * The output doc contains the blank text, a result flag (Y or N
 * depending on if we could look up and append text, and the phrase
 * type.
 * <Out>
 *   <PhraseLookupInfo>
 *     <PhraseType>PART_MODIF</PhraseType>
 *     <Result>Y</Result>
 *     <NewText></NewText>
 *   </PhraseLookupInfo>
 * </Out>
 * 
 * @return com.amsinc.ecsg.util.DocumentHandler
 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
 */ 
public DocumentHandler performPhraseClear(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	DocumentHandler outputDoc = new DocumentHandler();

	try {
		// Reset the new text to a blank string.
		mediatorServices.debug("Mediator: Clearing phrase text"); 

		outputDoc.setAttribute("/PhraseLookupInfo/NewText", "");
		outputDoc.setAttribute("/PhraseLookupInfo/Result", 
								TradePortalConstants.INDICATOR_YES); 
	} catch (Exception e) {
		LOG.info("Exception caught in PhraseLookupMediator:performPhraseClear");
		LOG.info(e.toString());
	}

	return outputDoc;
}

/**
 * This method takes a phrase and fills in enterable fields with the
 * values given by the user.  It uses the /PhraseLookupInfo and
 * /PhraseFields sections from the input document.  This method is
 * performed after the user has filled fields and presset the Submit
 * button.
 *
 * Here is a sample of the input xml
 * <In>
 *   <PhraseFields ID="0">
 *     <FieldLabel>Amount</FieldLabel>
 *     <FieldValue>123</FieldValue>
 *   </PhraseFields>
 *   <PhraseFields ID="1">
 *     <FieldLabel>Amount Wording</FieldLabel>
 *     <FieldValue>one hundred</FieldValue>
 *   </PhraseFields>
 *   <PhraseLookupInfo>
 *     <phrase_oid></phrase_oid>
 *     <text_maxlength>5000</text_maxlength>
 *     <text_path>/Terms/guar_bank_standard_text</text_path>
 *     <text_field_name>guar_bank_standard_text</text_field_name>
 *  </PhraseLookupInfo>
 * </In>
 *
 * Validation is done to make sure all the enterable fields have values.
 * If not, an error is given.
 *
 * If all goes well, the resulting text is returned in the out section
 * along with a revised phrase type of 'FILLED' (indicating a partly
 * modifable phrase that has been filled in).  Otherwise the original
 * text is returned with the phrase type of 'MODIF' and a result indicator
 * of N.
 *
 * Here is sample output XML (carriage return within NewText is for
 * readability).
 *  
 * <Out>
 *   <PhraseLookupInfo>
 *     <PhraseType>FILLED</PhraseType>
 *     <Result>Y</Result>
 *     <NewText>This is a phrase with an amount of 123 and amount wording 
 *                   of one hundred.</NewText>
 *   </PhraseLookupInfo>
 * </Out>
 *
 *
 * @return com.amsinc.ecsg.util.DocumentHandler
 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
 */ 
public DocumentHandler performPhraseFillIn(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	DocumentHandler outputDoc = new DocumentHandler();

	// Declare business object variables
	Phrase phrase = null;
	String phraseType = TradePortalConstants.PHRASE_PARTLY_MODIFIABLE;
	String currentText = "";
	String phraseText = "";
	String value = "";
	String fieldLabel;
	Vector fieldList;
	Vector fieldLabels = new Vector();
	Vector fieldValues = new Vector();
	DocumentHandler fieldDoc;
	
	fieldList = inputDoc.getFragments("/PhraseFields");

	for (int i=0; i<fieldList.size(); i++) {
		fieldDoc = (DocumentHandler) fieldList.elementAt(i);

		fieldLabel = fieldDoc.getAttribute("/FieldLabel");
		value = fieldDoc.getAttribute("/FieldValue");
		if (InstrumentServices.isBlank(value)) {
			mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1, 
							AmsConstants.REQUIRED_ATTRIBUTE, 
							fieldLabel); 
		}
		fieldLabels.addElement(fieldLabel);
		fieldValues.addElement(value);
	}

	try {
		
		String textPath = inputDoc.getAttribute("/PhraseLookupInfo/text_path");
		currentText = inputDoc.getAttribute(textPath);

		//
		mediatorServices.debug("Mediator: Replacing user provided values in phrase.");
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {
			// No errors issued yet so data values must be good.  Go ahead and
			// perform the substitution.
			currentText = substituteParmsInText(mediatorServices, currentText, 
											fieldLabels, fieldValues);
		} else {
			LOG.info("found error so throwing exception");
			throw new AmsException();
		}
		
		// Get the max length attribute from the input doc.  If not a number
		// default to -1 (indicating no max length).
		int maxLength = -1;
		value = inputDoc.getAttribute("/PhraseLookupInfo/text_maxlength");
		try {
			maxLength = Integer.parseInt(value);
		} catch (Exception e) {
			maxLength = -1;
		}

		if (maxLength > 0) {
			if (currentText.length() > maxLength) {
				currentText = currentText.substring(0, maxLength);
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.TEXT_TOO_LONG, 
					String.valueOf(maxLength)); 
			}
		}

		outputDoc.setAttribute("/PhraseLookupInfo/NewText", currentText);
		outputDoc.setAttribute("/PhraseLookupInfo/Result", TradePortalConstants.INDICATOR_YES); 
		outputDoc.setAttribute("/PhraseLookupInfo/PhraseType", 
								TradePortalConstants.PHRASE_PARTLY_MODIF_FILLED);
	} catch (AmsException e) {
		outputDoc.setAttribute("/PhraseLookupInfo/NewText", currentText);
		outputDoc.setAttribute("/PhraseLookupInfo/Result", TradePortalConstants.INDICATOR_NO); 
		outputDoc.setAttribute("/PhraseLookupInfo/PhraseType",
								TradePortalConstants.PHRASE_PARTLY_MODIFIABLE);
	} catch (Exception e) {
		LOG.info("Exception caught in PhraseLookupMediator");
		LOG.info(e.toString());
	}

	return outputDoc;
}

/**
 * This method uses information from the inputDoc to lookup a phrase.
 * The phrase is appended to whatever text was passed in the inputDoc
 * and the resulting appended text is returned in the outputDoc.
 *
 * The input XML looks like the following and should contain the
 * oid to look up, the max allowed appended text length, the path
 * in the input doc where to find to text to append to (text_path),
 * and the name of the field the resulting text is for.
 * <In>
 *   <PhraseLookupInfo>
 *     <phrase_oid>76079</phrase_oid>
 *     <text_maxlength>5000</text_maxlength>
 *     <text_path>/Terms/guar_bank_standard_text</text_path>
 *     <text_field_name>guar_bank_standard_text</text_field_name>
 *   </PhraseLookupInfo>
 * </In>
 *
 * The output doc contains the appended text, a result flag (Y or N
 * depending on if we could look up and append text, and the phrase
 * type.
 * <Out>
 *   <PhraseLookupInfo>
 *     <PhraseType>PART_MODIF</PhraseType>
 *     <Result>Y</Result>
 *     <NewText> This is a first phrase.  This is the second phrase.</NewText>
 *   </PhraseLookupInfo>
 * </Out>
 * 
 * @return com.amsinc.ecsg.util.DocumentHandler
 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
 */ 
public DocumentHandler performPhraseLookup(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	DocumentHandler outputDoc = new DocumentHandler();

	Phrase phrase = null;
	String phraseType = "";

	long lOid = 0;

	try {
		// Get the oid from the doc.  '0' indicates a new item so
		// change the mode.
		lOid = inputDoc.getAttributeLong("/PhraseLookupInfo/phrase_oid");
		if (lOid == 0) {
			mediatorServices.debug("Error: phrase_oid not present");
			return null;
		}

		// Get a handle to an instance of the object
		phrase = (Phrase) mediatorServices.createServerEJB("Phrase");

		mediatorServices.debug("Mediator: Retrieving phrase " + lOid);
		phrase.getData(lOid);

		String phraseText = phrase.getAttribute("phrase_text");
		phraseType = phrase.getAttribute("phrase_type");

		String textPath = inputDoc.getAttribute("/PhraseLookupInfo/text_path");

		// Get the max length attribute from the input doc.  If not a number
		// default to -1 (indicating no max length).
		int maxLength = -1;
		String value = inputDoc.getAttribute("/PhraseLookupInfo/text_maxlength");
		try {
			maxLength = Integer.parseInt(value);
		} catch (Exception e) {
			maxLength = -1;
		}

		String currentText = inputDoc.getAttribute(textPath);

		mediatorServices.debug(
			"Mediator: Appending '"
				+ phraseText
				+ "' to existing text: '"
				+ currentText
				+ "'"); 

		// If the current text isn't blank, first add a space.
		// In either case, append the new phrase text to the current text.
		if (InstrumentServices.isNotBlank(currentText)) {
			currentText += " ";
		}
		currentText += phraseText;

		if (maxLength > 0) {
			if (currentText.length() > maxLength) {
				currentText = currentText.substring(0, maxLength);
				mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1, 
					TradePortalConstants.TEXT_TOO_LONG, 
					String.valueOf(maxLength)); 
			}
		}

		outputDoc.setAttribute("/PhraseLookupInfo/NewText", currentText);
		outputDoc.setAttribute(
			"/PhraseLookupInfo/Result", 
			TradePortalConstants.INDICATOR_YES); 
		outputDoc.setAttribute("/PhraseLookupInfo/PhraseType", phraseType);
	} catch (InvalidObjectIdentifierException ex) {

		// If the object does not exist, issue an error
		mediatorServices.getErrorManager().issueError(
			getClass().getName(), 
			"INF01", 
			ex.getObjectId()); 

	} catch (AmsException e) {
		// This is probably because the phrase oid passed is not a number.
		// (caused when selecting the 'Select a Phrase' option).  In this
		// case, we still want to return a PhraseLookupInfo section
		// but not with any text.
		outputDoc.setAttribute("/PhraseLookupInfo/NewText", "");
		outputDoc.setAttribute(
			"/PhraseLookupInfo/Result", 
			TradePortalConstants.INDICATOR_NO); 
		outputDoc.setAttribute("/PhraseLookupInfo/PhraseType", phraseType);
	} catch (Exception e) {
		LOG.info("Exception caught in PhraseLookupMediator");
		LOG.info(e.toString());
	}

	return outputDoc;
}

/**
 * This method performs the fill-in subsitution.  It takes the text, parses
 * through it looking for fields (defined by []).  Each field is replaced
 * with a value from the fieldValues vector.  It is assumed that the vector
 * contains enough entries for all the fields.  (We previously tested for
 * this in performPhraseFillIn.  
 *
 * @return java.lang.String
 * @param mediatorServices MediatorServices
 * @param text java.lang.String
 * @param fieldLabels java.util.Vector
 * @param fieldValues java.util.Vector
 */
private String substituteParmsInText(MediatorServices mediatorServices, String text, Vector fieldLabels, Vector fieldValues) {

	StringTokenizer st = new StringTokenizer(text, "[]", true);
	String token;
	String fieldLength;
	String fieldLabel = "";
	boolean fieldFound = false;
	boolean addToken = true;
	int semicolon;
	int fieldCount = -1;
	StringBuffer result = new StringBuffer("");

	// (Note, this loop is similar to logic in the PhraseParser.  However
	// we want both the fields and the non-fields so we need to use
	// similar but different logic here.

	// Search the text.  For straight text, append it to the result.
	// For fields, append the nth value from the vector of values where
	// n corresponds to the nth field found.
	while (st.hasMoreTokens()) {
		// Get the token
		token = st.nextToken();

		// Determine if it is the start of a field, end of a field, or
		// just text
		if (token.equals("[")) {
			fieldFound = true;
			fieldCount++;
			addToken = false;
		} else if (token.equals("]")) {
				fieldFound = false;
				addToken = false;
		} else {
			addToken = true;
			if (fieldFound) {
				// If we've found a field token, attempt to parse out
				// the label and length
				semicolon = token.indexOf(";");
				if (semicolon > -1) {
					fieldLabel = token.substring(0, semicolon);
					fieldLength = token.substring(semicolon + 1);
				} else {
					fieldLabel = token;
					fieldLength = PhraseField.MAX_LENGTH;
				}
			}
		}

		// Tokens are appended to the result string.  (The delimiters
		// are tokens but we do not append them.)  If the token is
		// a field, we take the nth value from the vector of values
		// corresponding to the nth field found.
		if (addToken) {
			if (fieldFound) {
				token = (String) fieldValues.elementAt(fieldCount);
			}
			result.append(token);
		}
	} // end while

	return result.toString();
}

}