package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InstrumentPurgeHistory;
import com.ams.tradeportal.busobj.MailMessage;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;

public class PurgeUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(PurgeUnPackagerMediatorBean.class);

/*
 * Manages PurgeMessage UnPackaging..
 * PurgeMessage XML looks like below
 *- <Proponix>
 * - <Header>
 *   <DestinationID>TPL</DestinationID>
 *   <SenderID>PRO</SenderID>
 *   <OperationOrganizationID>ANZ-MEL</OperationOrganizationID>
 *   <MessageType>PURGE</MessageType>
 *   <DateSent>09/18/2001 00:00:00</DateSent>
 *   <TimeSent>19:33:00</TimeSent>
 *   <MessageID>9831234hggd</MessageID>
 *  </Header>
 *- <Body>
 *   <InstrumentID>MEL30136EXP</InstrumentID>
 *  </Body>
 *</Proponix>
 *
 * There can be more than one Instrument existing in the TradePortal database for one InstrumentID,
 * however this InstrumentId is unique in the OTL database. This situation occurs when both Applicant
 * and the Beneficiary belong to the TradePortal, in which case two instruments with same InstrumentID
 * but different CustomerID exist in the TradePortal database and both need to be purged.
 */

       public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                                                                        throws RemoteException, AmsException
 {

      boolean unpackage = true;
      int success;
      String purgeInstrumentOid = null ;
      String assignedToCorpOrgOid = null ;
      int count = 0;
      String whereClause;
      Instrument              purgeInstrument ;
      InstrumentPurgeHistory instrumentPurgeHistory ;


     try
      {

		  String opBankOrgProponixId   =  unpackagerDoc.getAttribute("/Proponix/Header/OperationOrganizationID");
		  if(opBankOrgProponixId == null || opBankOrgProponixId.equals(""))
		  	{
		  	  mediatorServices.debug("No opBankOrg to update");
		  	  mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OperationOrganizationID", "/Proponix/Header/OperationOrganizationID");
		      mediatorServices.debug("Debug 1");
		      unpackage=false;
		    }
		  else
		  //IValavala Jan 29 07. Make sure the org is ACTIVE
		  	{
		  	
		      String sqlStatement = " SELECT ORGANIZATION_OID FROM OPERATIONAL_BANK_ORG WHERE " +
			  						"  PROPONIX_ID = ? AND ACTIVATION_STATUS = ? ";
			  DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{opBankOrgProponixId, "ACTIVE"});
			  if(resultXML == null)
				{
			  		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
					unpackage = false;
				}
			  else
				{
				//make sure that we don't have more than one result
				Vector v = resultXML.getFragments("/ResultSetRecord(0)");
				if (v.size() > 1) {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "OperationalBankOrg");
					unpackage = false;
					}
				int rid = 0;//We only need the first record in the result set so use id = '1'
					
				String opOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");
				if(opOrgOid==null || opOrgOid.equals(""))
				 {
					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
					unpackage = false;
			 	  }
				else
				unpackage = true;
				}
			//IValavala End
		    }


           String completeInstrumentId   = unpackagerDoc.getAttribute("/Proponix/Body/InstrumentID");

           if(completeInstrumentId == null || completeInstrumentId.equals(""))
             {
 			 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"InstrumentID","/Proponix/SubHeader/InstrumentID");
			 unpackage = false;
 			 mediatorServices.debug("Debug 8");
 			 }
           else
              {
               
                int instrumentCount = DatabaseQueryBean.getCount("INSTRUMENT_OID","INSTRUMENT","COMPLETE_INSTRUMENT_ID = ?", false, new Object[]{completeInstrumentId});
                mediatorServices.debug("Debug 81: Instrument count =" + instrumentCount);
                String sqlStatement = " SELECT INSTRUMENT_OID FROM INSTRUMENT WHERE" +
				                      "  COMPLETE_INSTRUMENT_ID = ? " ;

                DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{completeInstrumentId});

                Hashtable clientBanks = new Hashtable(1);

                for (int purgeCount=0;purgeCount<instrumentCount;purgeCount++)
                {
		  purgeInstrument       = (Instrument)  mediatorServices.createServerEJB("Instrument");
                  purgeInstrumentOid    = resultXML.getAttribute("/ResultSetRecord(" + purgeCount + ")/INSTRUMENT_OID");
		  mediatorServices.debug("Debug 83  Instrument_oid :" + purgeInstrumentOid);
		  purgeInstrument.getData(Long.parseLong(purgeInstrumentOid));
		  mediatorServices.debug("Debug 84  Instrument_oid :" + purgeInstrumentOid + "Got the Data");

                  // Determine what should happen on purge based on client bank of instrument
                  String clientBankOid = purgeInstrument.getAttribute("client_bank_oid");
                  ClientBank clientBank = (ClientBank) clientBanks.get(clientBankOid);
                  if(clientBank == null)
                   {
                        // If we haven't looked up this client bank yet, get it from database and add to cache
                        clientBank = (ClientBank)  mediatorServices.createServerEJB("ClientBank", Long.parseLong(clientBankOid));
                        clientBanks.put(clientBankOid, clientBank);
                   }

                  String clientBankPurgeSetting = clientBank.getAttribute("instrument_purge_type");

                  if((clientBankPurgeSetting != null) &&
                     clientBankPurgeSetting.equals(TradePortalConstants.PURGE_DELETE_INSTRUMENT))
                   {
		      mediatorServices.debug("Debug 185: purging instrument"+purgeInstrumentOid);
                      // Delete the instrument from the database and create a purge history record
                      purgeRelatedInstruments(purgeInstrumentOid,mediatorServices);
                      purgeRelatedMailMessages(purgeInstrumentOid,mediatorServices);
                      instrumentPurgeHistory = (InstrumentPurgeHistory) mediatorServices.createServerEJB("InstrumentPurgeHistory");
    		      instrumentPurgeHistory.newObject();
		      populateInstrumentPurgeHistory(purgeInstrument,instrumentPurgeHistory,mediatorServices);
		      instrumentPurgeHistory.save();
		      mediatorServices.debug("Debug 85  Instrument_oid :" + purgeInstrumentOid + "Ready to Delete");

		      success = purgeInstrument.delete();
		      if(success < 0)                         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
                                                                    TradePortalConstants.TASK_NOT_SUCCESSFUL,"PurgeInstrument delete");
                   }
                  else
                   {
                      // Don't delete the instrument from the database
                      // Only update its purge date
		      mediatorServices.debug("Debug 186: not purging instrument"+purgeInstrumentOid+"... only updating purge date");
		      purgeInstrument.setAttribute("purge_date", DateTimeUtility.getGMTDateTime());
		      mediatorServices.debug("Debug 187  Instrument_oid :" + purgeInstrumentOid + "Ready to Update Purge Date");
                      success = purgeInstrument.save();
		      if(success < 0)                         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
                                                                    TradePortalConstants.TASK_NOT_SUCCESSFUL,"PurgeInstrument update purge date");
                   }
	         } //End of for
               } //end of Else
	    } //end of try

       catch (RemoteException e) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");

           e.printStackTrace();

       }
       catch (AmsException e) {
          mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
           e.printStackTrace();


       }
       catch (Exception e) {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
            e.printStackTrace();

       }

    return outputDoc;
 }

      /**
       * This method takes the Instrument object, InstrumentPurgeHistory
       * and populates the InstrumentPurgeHistory with required values from
       * from the Instrument Object
       * @param purgeInstrument, Instrument object
       * @param instrumentPurgeHistory, InstrumentPurgeHistory object
       * @param mediatorServices, MediatorServices
       * author iv
       */

  public void populateInstrumentPurgeHistory(Instrument purgeInstrument,InstrumentPurgeHistory instrumentPurgeHistory,MediatorServices mediatorServices)
                                                                                 throws RemoteException, AmsException
  {
	String [] instrumentAttributes = {"complete_instrument_id","instrument_type_code","copy_of_instrument_amount","corp_org_oid","instrument_oid"};
	Hashtable instrumentValues     = purgeInstrument.getAttributes(instrumentAttributes);
    String instrumentOid           = (String)instrumentValues.get("instrument_oid");
    String sqlStatementCurrency = " SELECT BASE_CURRENCY_CODE FROM INSTRUMENT , TRANSACTION  WHERE " +
                                  " ACTIVE_TRANSACTION_OID = TRANSACTION_OID AND INSTRUMENT_OID = ? ";
    String counterpartyName = null;
    String currency          = null;
    mediatorServices.debug("I am inside populateInstrumentPurgeHistory :");
    DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementCurrency, false, new Object[]{instrumentOid} );
    if(resultXML != null)
       currency = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/BASE_CURRENCY_CODE");

    String sqlStatementCounterParty = " SELECT NAME FROM INSTRUMENT I, TERMS_PARTY TP WHERE  " +
                                      " I.A_COUNTER_PARTY_OID = TP.TERMS_PARTY_OID AND INSTRUMENT_OID = ? " ;

    resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatementCurrency, false, new Object[]{instrumentOid});
    if(resultXML != null)
       counterpartyName = resultXML.getAttribute("/ResultSetRecord(" + 0 + ")/NAME");
    String purgeTimeStamp   = DateTimeUtility.getGMTDateTime(false);
    String [] instrumentPurgeHisAttr = {"instrument_id","instrument_type_code","amount","corp_org_oid",
                                        "currency","counterparty_name","purge_timestamp"};
    String [] instrumentPurgeHisValues = {"","","","","","",""};
    mediatorServices.debug("I am inside populateInstrumentPurgeHistory : Just before setting all the attributes");

    for(int i = 0; i<4 ; i++)
    {
	 instrumentPurgeHisValues[i] = (String)instrumentValues.get(instrumentAttributes[i]);
	 mediatorServices.debug("The value here is                :" +  instrumentPurgeHisValues[i]);
	}
	instrumentPurgeHisValues[4] = currency;
	instrumentPurgeHisValues[5] = counterpartyName;
	instrumentPurgeHisValues[6] = purgeTimeStamp;
    mediatorServices.debug("The value of currency is           :" +  currency + "\n" +
                            "The value of counterpartyName is :" + counterpartyName + "\n" +
                            "The value of purgeTimeStamp is   :" + purgeTimeStamp);
    instrumentPurgeHistory.setAttributes(instrumentPurgeHisAttr,instrumentPurgeHisValues);


	mediatorServices.debug("I am inside populateInstrumentPurgeHistory : finished setting all the attributes");
    mediatorServices.debug("I am inside populateInstrumentPurgeHistory : finished saving instruPrgHis");

  }
      /**
       * This method takes the Oid of the Instrument being purged,
       * and purges any related Instruments from the database
       * @param parentInstrumentOid, Oid of the parent Instrument
       * @param mediatorServices, MediatorServices
       * author iv
       */
  public void purgeRelatedInstruments(String parentInstrumentOid, MediatorServices mediatorServices)
                                   throws RemoteException, AmsException
  {
     Instrument              relatedInstrument;
     InstrumentPurgeHistory relInsPurgeHistory;
     DocumentHandler resultXML ;
     String relatedInstrumentOid = null;
     mediatorServices.debug("Debug 61");
     String sqlRelatedInstruments = "SELECT INSTRUMENT_OID FROM INSTRUMENT WHERE " +
	                               " A_RELATED_INSTRUMENT_OID = ? " ;
	 mediatorServices.debug("Debug 62");
	 
	 int instrumentCount = DatabaseQueryBean.getCount("INSTRUMENT_OID","INSTRUMENT","A_RELATED_INSTRUMENT_OID = ?", false, new Object[]{parentInstrumentOid});
	 mediatorServices.debug("Debug 63");
	 resultXML = DatabaseQueryBean.getXmlResultSet(sqlRelatedInstruments, false, new Object[]{parentInstrumentOid});
	 mediatorServices.debug("Debug 64");
	// if(resultXML != null)
	// {
	  for(int count = 0 ; count<instrumentCount; count++)
	  {
		relatedInstrumentOid = resultXML.getAttribute("/ResultSetRecord(" + count + ")/INSTRUMENT_OID");
		mediatorServices.debug("Debug 65");
		relatedInstrument       = (Instrument)  mediatorServices.createServerEJB("Instrument");
		relatedInstrument.getData(Long.parseLong(relatedInstrumentOid));
	    relInsPurgeHistory = (InstrumentPurgeHistory) mediatorServices.createServerEJB("InstrumentPurgeHistory");
		mediatorServices.debug("Debug 66");
		relInsPurgeHistory.newObject();
		mediatorServices.debug("Debug 67");
		populateInstrumentPurgeHistory(relatedInstrument,relInsPurgeHistory,mediatorServices);
		purgeRelatedMailMessages(relatedInstrumentOid,mediatorServices);
		purgeRelatedInstruments(relatedInstrumentOid,mediatorServices);
		relInsPurgeHistory.save();
		mediatorServices.debug("Debug 68");
		relatedInstrument.delete();
		mediatorServices.debug("Debug 69");
	 // }
     }

  }

      /**
       * This method takes the Oid of the Instrument being purged,
       * and purges any related MailMessages from the database
       * @param parentInstrumentOid, Oid of the parent Instrument
       * @param mediatorServices, MediatorServices
       * author iv
       */
 public void purgeRelatedMailMessages(String parentInstrumentOid, MediatorServices mediatorServices)
                                    throws RemoteException, AmsException
   {
 	// Instrument              relatedInstrument       = (Instrument)  mediatorServices.createServerEJB("Instrument");
     // InstrumentPurgeHistory relInsPurgeHistory = (InstrumentPurgeHistory) mediatorServices.createServerEJB("InstrumentPurgeHistory");
      MailMessage              mailMessage;
     // InstrumentPurgeHistory relInsPurgeHistory;
      DocumentHandler resultXML ;
      String messageOid = null;
      mediatorServices.debug("Debug m61");
      String sqlRelatedMailMsg = "SELECT MESSAGE_OID FROM MAIL_MESSAGE WHERE " +
 	                               " A_RELATED_INSTRUMENT_OID = ? " ;
 	 mediatorServices.debug("Debug M62");
 	 int mailMsgCount = DatabaseQueryBean.getCount("MESSAGE_OID","MAIL_MESSAGE","A_RELATED_INSTRUMENT_OID = ?",false, new Object[]{parentInstrumentOid});
 	 mediatorServices.debug("Debug M63");
 	 resultXML = DatabaseQueryBean.getXmlResultSet(sqlRelatedMailMsg, false,  new Object[]{parentInstrumentOid});
 	 mediatorServices.debug("Debug M64");

 	  for(int count = 0 ; count<mailMsgCount; count++)
 	  {
 		messageOid = resultXML.getAttribute("/ResultSetRecord(" + count + ")/MESSAGE_OID");
 		mediatorServices.debug("Debug M65");
 		mailMessage       = (MailMessage)  mediatorServices.createServerEJB("MailMessage");
 		mailMessage.getData(Long.parseLong(messageOid));
 	    mediatorServices.debug("Debug M67");
 		purgeMailRelatedDocumentImages(messageOid,mediatorServices);
 		mediatorServices.debug("Debug M68");
 		mailMessage.delete();
 		mediatorServices.debug("Debug M69");

      }

   }
      /**
       * This method takes the Oid of the MailMessage being purged,
       * and purges any related DocumentImages from the database
       * @param mailMessageOid, Oid of the parent Instrument
       * @param mediatorServices, MediatorServices
       * author iv
       */
 public void purgeMailRelatedDocumentImages(String mailMessageOid, MediatorServices mediatorServices)
                                     throws RemoteException, AmsException
   {
  	   DocumentImage             documentImage;
      // InstrumentPurgeHistory relInsPurgeHistory;
       DocumentHandler resultXML ;
       String docImageOid = null;
       mediatorServices.debug("Debug 61");
       String sqlRelatedDocImage = "SELECT DOC_IMAGE_OID FROM DOCUMENT_IMAGE WHERE " +
  	                               "MAIL_MESSAGE_OID = ? " ;
  	 mediatorServices.debug("Debug 62");
  	 String whereClause  = "MAIL_MESSAGE_OID = ? ";
  	 int docImgCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID","DOCUMENT_IMAGE",whereClause, false, new Object[]{mailMessageOid});
  	 mediatorServices.debug("Debug 63");
  	 resultXML = DatabaseQueryBean.getXmlResultSet(sqlRelatedDocImage, false, new Object[]{mailMessageOid});
  	 mediatorServices.debug("Debug 64");

  	  for(int count = 0 ; count<docImgCount; count++)
  	  {
  		docImageOid = resultXML.getAttribute("/ResultSetRecord(" + count + ")/DOC_IMAGE_OID");
  		mediatorServices.debug("Debug 65");
  		documentImage       = (DocumentImage)  mediatorServices.createServerEJB("DocumentImage");
  		documentImage.getData(Long.parseLong(docImageOid));
  	    mediatorServices.debug("Debug 67");
  		mediatorServices.debug("Debug 68");
  		documentImage.delete();
  		mediatorServices.debug("Debug 69");
  	 // }
       }

   }

}

