package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.MissingResourceException;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import java.rmi.*;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.*;
import com.thales_esecurity.ssas.BasicResponse;
import com.thales_esecurity.ssas.DigiPassResponse;
import com.thales_esecurity.ssas.SSASError;
import com.thales_esecurity.ssas.SSASSoap;
import com.thales_esecurity.ssas.SSASSoapServiceLocator;
import com.ams.tradeportal.BTMUeBizWebService.*;
import java.io.StringWriter;
import com.thales_esecurity.ssas.CSCResponse;
import com.thales_esecurity.ssas.ContentData;
import com.thales_esecurity.ssas.ExtraData;

import java.net.InetAddress;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.PropertyResourceBundle;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.ws.BindingProvider;

/*
 * ReauthenticationMediatorBean manages reauthentication.
 */
public class ReauthenticateMediatorBean extends LogonMediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(ReauthenticateMediatorBean.class);
    /*
    * Manages reauthentication.  First calls LogonMediator to
    * do the actual authentication, then does additional
    * functional specific actions.
    */
    public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
        throws RemoteException, AmsException
    {
    	//
    	//CR711. Rel8.0. 04/19/2012 - need to only go to LogonMediatorBean if not using VASCO type
    	//START
    	//
        String userOid = ""; 
        String loginId             = inputDoc.getAttribute("/User/loginId"); //user_identifier
        String authMethod			= inputDoc.getAttribute("/auth");
        
        //-AAlubala IR#MNUM062844286 - VASCO SSO changes. Use user_identifier instead of login_id
        String sqlStmt = "select user_oid from USERS where user_identifier = ?";
        DocumentHandler userResultXML = DatabaseQueryBean.getXmlResultSet(sqlStmt, false, new Object[]{loginId});
        if (userResultXML != null){
        	userOid = userResultXML.getAttribute("/ResultSetRecord/USER_OID");
        }
        DocumentHandler returnDoc = null;        
        //       
        User user = (User) mediatorServices.createServerEJB("User", Long.valueOf(userOid));        
        if(TradePortalConstants.VASCO_TOKEN.equals(user.getAttribute("token_type")) && TradePortalConstants.AUTH_2FA.equals(authMethod)){
        	returnDoc = reauthenticateTransactionUsingVasco(inputDoc, outputDoc, user, mediatorServices);
        }else if ("G".equals(user.getAttribute("certificate_type")) && TradePortalConstants.AUTH_CERTIFICATE.equals(authMethod)) {
            returnDoc = reauthenticateTransactionUsingGemalto(inputDoc, outputDoc, user, mediatorServices);
        }
        else {
        	returnDoc = super.execute(inputDoc, outputDoc, mediatorServices);
        	userOid = StringFunction.isNotBlank(returnDoc.getAttribute("/userOid"))? returnDoc.getAttribute("/userOid"):userOid;
        }
        //
        //CR711 - END
        //
        if (mediatorServices.getErrorManager().getMaxErrorSeverity()
                >= ErrorManager.ERROR_SEVERITY) { 

            String recertificationInd = inputDoc.getAttribute("/RecertificationIndicator");                    
            String reAuthObjectType = inputDoc.getAttribute("/reAuthObjectType");
            String reAuthObjectOids = inputDoc.getAttribute("/reAuthObjectOids");
            //
            //AAlubala - Fix the Nullpointer Error happening on the StringTokenizer
            //when no reAuthObjectOids aren't found in inputDoc
            List reAuthObjectOidList = new ArrayList();
            if(StringFunction.isNotBlank(reAuthObjectOids)){
	            StringTokenizer strTok = new StringTokenizer(reAuthObjectOids,"!");
	            while (strTok.hasMoreTokens()) {
	                String oid = strTok.nextToken();
	                reAuthObjectOidList.add(oid);
	            }
            }
            processAuthenticationErrors(recertificationInd, reAuthObjectType, reAuthObjectOidList, 
                    userOid, returnDoc, mediatorServices);
        }
               
        return returnDoc;
    }
    /**

     * @date 04/20/2012
     * This method performs the Reauthentication steps when a user is configured to use a VASCO token type and set to use 2FA
     * @param inputDoc - coming in with the request
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    
    private DocumentHandler reauthenticateTransactionUsingVasco(DocumentHandler inputDoc, DocumentHandler outputDoc, User user, MediatorServices mediatorServices) throws RemoteException, AmsException {
        String userClientBankOid = user.getAttribute("client_bank_oid");
        String BTMUeBizClientBankOid = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "VASCO_BTMUeBiz_CB", "");
        String[] BTMUeBizClientBankOids = BTMUeBizClientBankOid.split(";");
        for (int i = 0; i < BTMUeBizClientBankOids.length; i++){
            if (userClientBankOid.equals(BTMUeBizClientBankOids[i])) {
                return reauthenticateTransactionUsingVascoBTMUeBiz(inputDoc,  outputDoc,  user,  mediatorServices);
            }
        }
        return reauthenticateTransactionUsingVascoANZSSAS(inputDoc,  outputDoc,  user,  mediatorServices);
    }
    
    
        
    	
    /**

     * @date 04/20/2012
     * This method performs the Reauthentication steps when a user is configured to use a VASCO token type and set to use 2FA
     * @param inputDoc - coming in with the request
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    
    private DocumentHandler reauthenticateTransactionUsingVascoANZSSAS(DocumentHandler inputDoc, DocumentHandler outputDoc, User user, MediatorServices mediatorServices) throws RemoteException, AmsException{
    	DocumentHandler returnDoc = inputDoc;
        LOG.info("ReauthenticateMediatorBean.reauthenticateTransactionUsingVasco: inputDoc="+inputDoc.toString());
    	
        if(TradePortalConstants.VASCO_TOKEN.equals(user.getAttribute("token_type"))){
        	//Obtain the values from the sign Transaction Page.       	
        	
            String onlineOTPApp = "";
            String offlineSignApp = "";
            String onlineSignApp = "";
            
            try{
            	onlineOTPApp = portalProperties.getString("online.otp.app");
            }catch (MissingResourceException e){
           	//ignore
            } 
            
            try{
            	offlineSignApp = portalProperties.getString("offline.signature.app");
            }catch (MissingResourceException e){
           	//ignore
            } 
            
            try{
            	onlineSignApp = portalProperties.getString("online.signature.app");
            }catch (MissingResourceException e){
           	//ignore
            } 
        	
        	
        	String isCMTransaction = inputDoc.getAttribute("/isCMTransaction");
        	boolean isCashManagementTrans = false;
	 		String[] signatureFields = new String[1];   //04/28/2012 - changed the size of the signature fields array   	
        	if(StringFunction.isNotBlank(isCMTransaction)){
        		isCashManagementTrans=Boolean.valueOf(isCMTransaction);
        	}
        	//
        	//Now there is just one keyInfo regardless of the Transaction Type
        	//because the keyInfo is computed within the VASCO popup page
        	signatureFields[0] = inputDoc.getAttribute("/VascoKeyInfo");
        	
        	//Obtain the One Time Password (OTP) or SIG entered in the Token
            String password2FA         = inputDoc.getAttribute(("/User/password2FA"));
            String encryptedPassword2FA = inputDoc.getAttribute("/User/encryptedPassword2FA");
            boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
			if(instanceHSMEnabled && (encryptedPassword2FA != null)&& (encryptedPassword2FA.trim().length() > 0)) {
				 try {
					 password2FA = HSMEncryptDecrypt.decryptForTransport(encryptedPassword2FA);
				 } catch (Exception e) {
					 System.err.print("Exception caught while trying to decrypt encryptedPassword2FA: ReauthenticateMediatorBean: "+e.getMessage());
				 }
			}  
			//This is actually the offline authorizer
			String authorizer=inputDoc.getAttribute("/authorizer");
			String encryptedAuthorizer = inputDoc.getAttribute("/User/encryptedAuthorizer");
			if(instanceHSMEnabled && (encryptedAuthorizer != null)&& (encryptedAuthorizer.trim().length() > 0)) {
				 try {
					 authorizer = HSMEncryptDecrypt.decryptForTransport(encryptedAuthorizer);
				 } catch (Exception e) {
					 System.err.print("Exception caught while trying to decrypt encryptedAuthorizer: ReauthenticateMediatorBean: "+e.getMessage());
				 }
			}
			//The tokenApplicationID is the concatenation of the secure device id that 
			//is defined on the Corporate Customer User Profile page and the Application ID from CAAS. 
			//This can be configured to come from reference data or a property file. Using property file for now.            	
            String tokenApplicationID = user.getAttribute("token_id_2fa");	
	 		//The signature is the value generated by the VASCO device as a result of
	 		//entering the signature fields into the device.  
	 		String signature = password2FA;  	 		
            //
            //Obtain the Application ID corresponding to signature signing application
	 		//If it is proxy (or offline) authentication, the application will be "offline.signature.app"
	 		//otherwise it is assumed to be regular online authentication which will be "online.signature.app"
	 		//
	 		String signatureApplication = "";
	 		String authorizeButtonPressed = inputDoc.getAttribute("/submitedButtonName");
	          if(StringFunction.isNotBlank(authorizeButtonPressed))
        		  if(authorizeButtonPressed.equalsIgnoreCase("ProxyAuthorize") || isItAProxyButton(authorizeButtonPressed)){        			  
        			  if(StringFunction.isNotBlank(offlineSignApp)){
        				  signatureApplication=portalProperties.getString("offline.signature.app");
        			  }else{
        				  signatureApplication="SIGOOBH";
        			  }
        		  }else{
        			//04/02/2012 - Determine if user is set to use OTP or SIG
        			  //online Auth can either be done via OTP or via SIG
        			  if(TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equalsIgnoreCase(user.getAttribute("auth2fa_subtype"))){
        	                 if(StringFunction.isNotBlank(onlineOTPApp)){
        	                	 signatureApplication=portalProperties.getString("online.otp.app");
        	                 }else{
        	                	 signatureApplication = "OTPPINH";
        	                 }        				  
        			  }else{
		        			  if(StringFunction.isNotBlank(onlineSignApp)){
		        				  signatureApplication=portalProperties.getString("online.signature.app");
		        			  }else{
		        				  signatureApplication="SIGONLH";
		        			  } 
        			  }
        		  }	 		
                          
            //transID is a unique number for every transaction. using a random number
            String transID = ""; 
            SecureRandom sr = new SecureRandom();
            long transactionID = sr.nextLong();
            //
            //Only use positive values and restrict to 6 in length
            transactionID = Math.abs(transactionID);
            transID = String.valueOf(transactionID);
            if(transID.length()>6)transID=transID.substring(0, 6);
            String userRights = "";
            String securityProfileOid="";
            String panelAuthorityCode = "";
            String proxyUserOid="";
            User proxyUser;
            String proxyUserTokenType="";
            String auth2faSubtype="";
            String loggedInUser2fa = user.getAttribute("auth2fa_subtype");
            String proxyUserAuthMethod="";
            String corpOrgOid="";

            if(StringFunction.isNotBlank(authorizeButtonPressed))
            	if(authorizeButtonPressed.equalsIgnoreCase("ProxyAuthorize") || isItAProxyButton(authorizeButtonPressed)){
            		//First, let's ensure that we have an offline authorizer
            		//An offline authorizer may be missing because either the entry wasn't filled in
            		//or it is an OTP 2fa type auth and they pressed offline authorize button
            		if(StringFunction.isNotBlank(loggedInUser2fa)){
            			if(TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(loggedInUser2fa)){
            				//Ok, the logged in user uses OTP so, the authorizer will be null
            				//if he gets this far i.e. if he presses offline authorize button

                        	//OTP should only be used for online authorization

                      			mediatorServices.getErrorManager().issueError(
                    					TradePortalConstants.ERR_CAT_1,
                    					TradePortalConstants.ERROR_VASCO_OTP_FOR_OFFLINE_AUTH,
                    					"");                     		
                                 				
            				return returnDoc;
            			}
            		}
            		//
            		//If Offline user is not entered, immediately return with an error
            		//
            		if((InstrumentServices.isBlankOrWhiteSpace(authorizer)) || (InstrumentServices.isBlankOrWhiteSpace(signature))){
            			mediatorServices.getErrorManager().issueError(
            					TradePortalConstants.ERR_CAT_1,
            					TradePortalConstants.ERROR_EMPTY_OFFLINE_AUTHORIZER,
            					authorizer); 
            			return returnDoc;            			
            		}             		
            		//Let's get some information about the offline user
            		//user_identifier
                    //-AAlubala IR#MNUM062844286 - VASCO SSO changes. Use user_identifier instead of login_id
            		String sqlStatement = "select user_oid, p_owner_org_oid, a_security_profile_oid, panel_authority_code, token_id_2fa, token_type, auth2fa_subtype, authentication_method from USERS where user_identifier = ?";
                    DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{authorizer});
                    if (resultXML != null){
                    	securityProfileOid = resultXML.getAttribute("/ResultSetRecord/A_SECURITY_PROFILE_OID");
                    	panelAuthorityCode = resultXML.getAttribute("/ResultSetRecord/PANEL_AUTHORITY_CODE");
                    	proxyUserOid = resultXML.getAttribute("/ResultSetRecord/USER_OID");
                    	tokenApplicationID = resultXML.getAttribute("/ResultSetRecord/TOKEN_ID_2FA");
                    	proxyUserTokenType = resultXML.getAttribute("/ResultSetRecord/TOKEN_TYPE");
                    	auth2faSubtype = resultXML.getAttribute("/ResultSetRecord/AUTH2FA_SUBTYPE");
                    	proxyUserAuthMethod = resultXML.getAttribute("/ResultSetRecord/AUTHENTICATION_METHOD");
                    	//If AUTHENTICATION_METHOD is blank on USER level, then fetch the same from CORPORATE ORG level.
                    	if (StringFunction.isBlank(proxyUserAuthMethod))
                    	{
                    		corpOrgOid = resultXML.getAttribute("/ResultSetRecord/P_OWNER_ORG_OID");
                    		if (StringFunction.isNotBlank(corpOrgOid)) {
                    			CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.valueOf(corpOrgOid));
                    			proxyUserAuthMethod = corporateOrg.getAttribute("authentication_method");
                    		}
                    	}
                    }

                    //IR#MJUM061351814 - AAlubala - If offline user is not set to use 2FA or SSO (SSO is indirectly authenticated by 2FA),
                    // display an error message
                    // W Zhu 6/28/12 Rel 8.0 DEUM060733158 Add SSO since it is indirectly authenticated by 2FA. 
                    if(!TradePortalConstants.AUTH_2FA.equalsIgnoreCase(proxyUserAuthMethod)
                    		&& !TradePortalConstants.AUTH_SSO.equalsIgnoreCase(proxyUserAuthMethod)){
              			mediatorServices.getErrorManager().issueError(
            					TradePortalConstants.ERR_CAT_1,
            					TradePortalConstants.ERROR_CERTUSER_FOR_OFFLINE_AUTH,
            					""); 
              			return returnDoc;
                    }                    
                    //
                    //CR711. REL8.0. 04/05/2012
                    //Check that the offline user is configured to use a VASCO token type
                    //if not, return immediately
                    //
                    if(!TradePortalConstants.VASCO_TOKEN.equalsIgnoreCase(proxyUserTokenType)){
              			mediatorServices.getErrorManager().issueError(
            					TradePortalConstants.ERR_CAT_1,
            					TradePortalConstants.ERROR_NONVASCO_FOR_OFFLINE_AUTH,
            					""); 
              			return returnDoc;
                    }

            		//
            		//Ok, now let's get security rights for the offline user
                    if(StringFunction.isNotBlank(securityProfileOid))
                    {                   	
                    	sqlStatement = "select security_rights from security_profile where security_profile_oid = ?";
                    	resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, new Object[]{securityProfileOid});
                    	userRights = resultXML.getAttribute("/ResultSetRecord/SECURITY_RIGHTS");
                    }            		
                    String transactionType = inputDoc.getAttribute("/transactionType");
                    String instrumentType = inputDoc.getAttribute("/instrumentType");   
                    //Ok, can this offline user authorize this instrument?
            		//
            		boolean canAuthorize = SecurityAccess.canAuthorizeInstrument(userRights, instrumentType, transactionType);
            		if(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT.equalsIgnoreCase(instrumentType) || InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT.equalsIgnoreCase(instrumentType)){
            			canAuthorize = SecurityAccess.hasRights(userRights, SecurityAccess.RECEIVABLES_AUTHORIZE);
            		}else if(TradePortalConstants.INV_LINKED_INSTR_REC.equalsIgnoreCase(instrumentType)){
            			canAuthorize = SecurityAccess.hasRights(userRights, SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);
            		}
            		//
            		if(!canAuthorize){
            			mediatorServices.getErrorManager().issueError(
            					TradePortalConstants.ERR_CAT_1,
            					TradePortalConstants.ERROR_PROXY_AUTHORIZING,
            					authorizer); 
            			return returnDoc;
            		}
            		//ok if can authorize, let's set the offline user's panel code
            		//to be used during authorization
            		if(StringFunction.isNotBlank(panelAuthorityCode)){
            			//proxyUser.setAttribute("panel_authority_code", panelAuthorityCode);
            			returnDoc.setAttribute("/panel_authority_code", panelAuthorityCode);
            			mediatorServices.getCSDB().setCSDBValue("/panel_authority_code", panelAuthorityCode);
            		}
            		//Actually let's set the offline user's user oid. We will need this info later on
            		//in the Authorization process
            		if(StringFunction.isNotBlank(proxyUserOid)){
            			//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - Start
				//W Zhu - 19 Feb 2013 Rel 8.1 T36000011771 Back out Ravindra's change for DEUM070438986
				returnDoc.setAttribute("/proxyUserOid", proxyUserOid);
				mediatorServices.getCSDB().setCSDBValue("proxyUserOid", proxyUserOid);
            			//Ravindra - 19th Nov 2012- Rel8100 IR-DEUM070438986 - Start
            			//create proxy/offline user object
            			proxyUser = (User) mediatorServices.createServerEJB("User", Long.valueOf(proxyUserOid));
            		}else{
            			proxyUser = user; //If offline user not available, then using logged in user
            		}

                    //
                    //now let's put together the tokenApplication ID and send it off to verification
                  	 tokenApplicationID = tokenApplicationID+signatureApplication;
                  	 //The user to be logged for auditing will be the offline user (proxy user)
                  	 //since this is an offline aithorise transaction
                  	 //Rel 8.1 IR: DEUM070438986 07/2012 - AAlubala

               	 if(verifyAndSignTransaction(transID, tokenApplicationID, signature, signatureFields,
               			 user,inputDoc, outputDoc, mediatorServices, proxyUser )){
               		 		returnDoc.setAttribute("/logonSuccesful","true");
                         return returnDoc;
                     }


            	}else{
            		//Got here because online authorize was clicked
        	 		//Check for the signature, if not entered,
        	 		//immediately return and display error message to user, no need to proceed further
        	 		//For online authorization - the Error message is different from the offline user's message
            		if(InstrumentServices.isBlankOrWhiteSpace(signature)){
            			mediatorServices.getErrorManager().issueError(
            					TradePortalConstants.ERR_CAT_1,
            					TradePortalConstants.ERROR_EMPTY_VASCO_SIGNATURE,
            					signature);
            			return returnDoc;
            		}

                    //
                    //now let's put together the tokenApplication ID and send it off to verification
                  	 tokenApplicationID = tokenApplicationID+signatureApplication;
                  	 //The user to be used for auditing will be the logged on user
                	 //since this is an online transaction
                  	//Rel 8.1 IR: DEUM070438986 07/2012 - AAlubala
               	 if(verifyAndSignTransaction(transID, tokenApplicationID, signature, signatureFields,
               			 user,inputDoc, outputDoc, mediatorServices, user )){
               		 		returnDoc.setAttribute("/logonSuccesful","true");
                         return returnDoc;
                     }

            	}

       	 sr=null;
        } else{
        	//Ok, you got here but aren't configured to use VASCO card
        	//check to see if you performed offline Authorizing, if so,
        	//present error and return
        	//Offline Auth can only be performed if one is configured to use a VASCO card
        	//CR711 Rel8.0 -AAlubala - 04/04/2012
        	//
	 		String authorizeButtonPressed = inputDoc.getAttribute("/submitedButtonName");
	          if(StringFunction.isNotBlank(authorizeButtonPressed))
      		  if(authorizeButtonPressed.equalsIgnoreCase("ProxyAuthorize") || isItAProxyButton(authorizeButtonPressed)){
      			mediatorServices.getErrorManager().issueError(
    					TradePortalConstants.ERR_CAT_1,
    					TradePortalConstants.ERROR_NONVASCO_FOR_OFFLINE_AUTH,
    					"");
    			return returnDoc;
      		  }
        }

        return returnDoc;
    }


	/**

     * @date 10/27/2011
     * @param tokenApplicationID - a concatenation of token id that defined on corporate customer profile page
     * and application id from CAAS (defined in Tradeportal.properties file)
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @param userToLog4Audit TODO
     * @param transId - A unique identifier for each transaction/request
     * @param oneTimePassword - entry on the token typed in by the user
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean verifyAndSignTransaction(String transID, String tokenApplicationID, String signature, String[] signatureFields, User user,
 		   DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices, User userToLog4Audit ) //Rel 8.1 IR: DEUM070438986 07/2012 - AAlubala - added userToLog4Audit
    throws RemoteException, AmsException {

 	  String serviceURL = "";
 	  String auth2FALogOid = "";

 	   String ignoreValidation2FA = null;
 	   // 2FAignoreValidation is set to Y if we ignore the 2FA validation (in Dev region where there is no SSASoap Endpoint server)
 	   try {
 		   ignoreValidation2FA = portalProperties.getString("2FAignoreValidation");
 	   }
 	   catch (MissingResourceException e){
 		   // Ignore
 	   }
 	   try {
 		   serviceURL = portalProperties.getString("VascoSSASSoapEndpointAddress");
 	   }
 	   catch (MissingResourceException e){
 		   //LOG.debug(" THE VASCO SSAS soap endpoint is missing: "+e);
 		   //SOP used for VASCO debugging in order to capture output always in the out log
 		   LOG.info("VASCO Debug: THE VASCO SSAS soap endpoint is missing: "+e);
 		   //The default URL specified in the WSDL file will be used TODO: add code to retrieve the default one if necessary
 	   }	    
 	   if ("Y".equals(ignoreValidation2FA)) {
 		   LOG.info("[ReauthenticateMediatorBean.validateAndSignTransaction Using VASCO] loginId=" + user.getAttribute("user_identifier") + ";2FAignoreValidation=Y");
 		   return true;
 	   }
 	   
 	   try {
		   
 		   // Create an instance of the SOAP service.
 		   SSASSoapServiceLocator soapService = new SSASSoapServiceLocator();
 		   // Set the URL explicitly based on configuration
 		   soapService.setSSASSoapEndpointAddress(serviceURL); 

 		   BasicResponse basicResponse = null;
 		   DigiPassResponse response = null;
 		   SSASSoap soapCtx = soapService.getSSASSoap();
 		   //
 		   //Check the type of SIG being performed, if otp call the doDigiPassVerifyPassword method
 		   //otherwise call doDigiPassVerifySignature 
 		   //
 		   if(TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(user.getAttribute("auth2fa_subtype"))){
 	 		   //SOP used for VASCO debugging in order to capture output always in the out log
 	 		   LOG.info("VASCO Debug: OTP Signing transID: "+transID+" AppID: "+tokenApplicationID);
 			   response = soapCtx.doDigiPassVerifyPassword(transID, null, tokenApplicationID, signature);
 		   }else{
 	 		   //SOP used for VASCO debugging in order to capture output always in the out log
 	 		   LOG.info("VASCO Debug: ONLINE/OFFLINE Signing transID: "+transID+" AppID: "+tokenApplicationID); 			   
 			   response = soapCtx.doDigiPassVerifySignature(transID, null, tokenApplicationID, signatureFields, signature);
 		   }
 		   //
 		   
 		   // Check the response.
 		   basicResponse = response.getBasicResponse();
 		   // Check the status.
 		   String status = basicResponse.getStatus();
 		   if (status.equalsIgnoreCase("ERROR")) {
 			   SSASError errors[] = basicResponse.getErrors();
 	 		   //SOP used for VASCO debugging in order to capture output always in the out log
 	 		   LOG.info("VASCO Debug: Error returned from VASCO server: "); 			   
 			   for (int i=0; i<errors.length; i++) {
 	 	 		   //SOP used for VASCO debugging in order to capture output always in the out log	   
 				   LOG.info("VASCO Debug: \nVASCO: AdditionalInfo: " + errors[i].getAdditionalInfo()+" \nVASCO: Description: "+errors[i].getDescription()+" \nVASCO: Name: "+ errors[i].getName());
 				   //Display to the user Transaction signing specific error message
 				   //IR#DRUM060436621,IR#DAUM060543868, MRUM061250737 - AAlubala - The error message is changed to a generic message
 				   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.RECERTIFICATION_FAILED);
 			   }
 			   return false;
 		   }
		   //log the entry in the audit table.
		   //V indicates that it is a VASCO token
		   auth2FALogOid = logAuth2FARequest(
				   // Rel 8.1 IR: DEUM070438986 07/2012 - AAlubala - userToLog4Audit could be offline user or logged in user
				   //depending on whether it is an offline authorise transaction or online transaction
				   userToLog4Audit.getAttribute("user_identifier"), serviceURL,//-AAlubala IR#MNUM062844286 - VASCO SSO changes. Use user_identifier instead of login_id
		    		tokenApplicationID, null, "V" );

	    } catch(UnknownHostException uhe){
			   removeAuth2FALog(auth2FALogOid);
			   //Display the server unknown or unreachable error to the user
 	 		   //SOP used for VASCO debugging in order to capture output always in the out log
 	 		   LOG.info("VASCO Debug: ReauthenticateMediatorBean: verifyAndSignTransaction "+uhe);			   
	    }catch(Exception e){
		   removeAuth2FALog(auth2FALogOid);
			   //IR#DRUM060436621,IR#DAUM060543868, MRUM061250737 - AAlubala - The error message is changed to a generic message
			   mediatorServices.getErrorManager().issueError(getClass().getName(), TradePortalConstants.RECERTIFICATION_FAILED);		   
	 		   //SOP used for VASCO debugging in order to capture output always in the out log
	 		   LOG.info("VASCO Debug: ReauthenticateMediatorBean: verifyAndSignTransaction "+e);
		  return false;
	   }

 	   return true;
    }    
    
    /**
     * 
     * This method performs the Reauthentication steps when a user is configured to use a VASCO token type and set to use 2FA
     * @param inputDoc - coming in with the request
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    
    private DocumentHandler reauthenticateTransactionUsingVascoBTMUeBiz(DocumentHandler inputDoc, DocumentHandler outputDoc, User user, MediatorServices mediatorServices) 
            throws RemoteException, AmsException{
    	DocumentHandler returnDoc = inputDoc;
    	
        	
 		String signatureDataField = inputDoc.getAttribute("/VascoKeyInfo");
        	
        	//Obtain the One Time Password (OTP) or SIG entered in the Token
                String signature         = inputDoc.getAttribute(("/User/password2FA"));
                String encryptedSignature = inputDoc.getAttribute("/User/encryptedPassword2FA");
                boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
                if(instanceHSMEnabled && (encryptedSignature != null)&& (encryptedSignature.trim().length() > 0)) {
                         signature = HSMEncryptDecrypt.decryptForTransport(encryptedSignature);
                }  

                //Check for the signature, if not entered,
                //immediately return and display error message to user, no need to proceed further
                if(InstrumentServices.isBlankOrWhiteSpace(signature)){
                        mediatorServices.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.ERROR_EMPTY_VASCO_SIGNATURE,
                                        signature);
                        return returnDoc;
                }

                if(signature.length() != 6){
                        mediatorServices.getErrorManager().issueError(
                                        TradePortalConstants.ERR_CAT_1,
                                        TradePortalConstants.ERROR_VASCO_SIGNATURE_NOT_6_DIGIT,
                                        signature);
                        return returnDoc;
                }
                          
                ObjectFactory objFactory = new ObjectFactory();
                ExecuteInput executeInput = objFactory.createExecuteInput(); 

                // /executeInput/soakyoutsuuheader
                Soakyoutsuuheader soukyousuuheader = objFactory.createSoakyoutsuuheader();
                executeInput.setSoakyoutsuuheader(soukyousuuheader);
                String serviceId = createServiceid();
                soukyousuuheader.setOriginalserviceid(serviceId);
                soukyousuuheader.setSourceserviceid(serviceId);
                soukyousuuheader.setServiceid(serviceId); 
                soukyousuuheader.setRequestTimestamp(timestamp2.get());
                soukyousuuheader.setSoushinRetryFlag(false);
                soukyousuuheader.setRequestSystemId("TPL");
                soukyousuuheader.setSoakyoutsuuheaderVersion("1.0.0");
                soukyousuuheader.setServiceVersion("1.0.0");

                OtpVerifySignatureRequestmessage otpVerifySignatureRequestmessage = objFactory.createOtpVerifySignatureRequestmessage();
                executeInput.setOtpVerifySignatureRequestmessage(otpVerifySignatureRequestmessage);

                // /executeInput/otpVerifySignatureRequestmessage/commonInformation/
                CommonInformation commonInformation = objFactory.createCommonInformation();        
                otpVerifySignatureRequestmessage.setCommonInformation(commonInformation);
                String userID = user.getAttribute("sso_id");
                String eBizCustomerID = "";
                if (userID.length() >= 8) eBizCustomerID = userID.substring(0, 8);
                String eBizUserID = "";
                if (userID.length() > 8) eBizUserID = userID.substring(8); 
                commonInformation.setCustomerId(eBizCustomerID); 
                commonInformation.setUserId(eBizUserID); 
                commonInformation.setSystemId("007");
                String serviceURL = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "Vasco_BTMUeBiz_EndpointAddress", "");
                String reAuthObjectType = inputDoc.getAttribute("/reAuthObjectType");
                String reAuthObjectOids = inputDoc.getAttribute("/reAuthObjectOids");
		String   auth2FALogOid = logAuth2FARequest(user.getAttribute("user_identifier"), serviceURL,
                                    reAuthObjectOids, reAuthObjectType, "V" );
                commonInformation.setEntryNo(auth2FALogOid); // Use the auth_2fa_log_oid as a unique Identifier for entryNo 
                String structureType;
                if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE)) {
                    structureType = "0"; // corporate user
                }
                else {
                    structureType = "1"; // admin user
                }
                commonInformation.setStructureType(structureType); 

                // /executeInput/otpVerifySignatureRequestmessage/deviceInformation
                DeviceInformation deviceInformation  = objFactory.createDeviceInformation();
                otpVerifySignatureRequestmessage.setDeviceInformation(deviceInformation);
                deviceInformation.getApplicationId().add("2110");
                deviceInformation.getApplicationId().add("1010");

                // /executeInput/otpVerifySignatureRequestmessage/authenticationInformation
                AuthenticationInformation authenticationInformation = objFactory.createAuthenticationInformation();
                otpVerifySignatureRequestmessage.setAuthenticationInformation(authenticationInformation);
                authenticationInformation.setSignature(signature);
                authenticationInformation.getSignedDataFields().add(signatureDataField);

                // Print out the input SOAP message 
                try {
                    LOG.info("ReauthenticateMediatorBean.reauthenticateTransactionUsingVascoBTMUeBiz executeInput");
                    JAXBElement<ExecuteInput> jxo = objFactory.createExecuteInput(executeInput);
                    JAXBContext jc = JAXBContext.newInstance(ExecuteInput.class);
                    Marshaller m = jc.createMarshaller();
                    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    StringWriter writer = new StringWriter();
                    m.marshal(jxo, writer);
                    LOG.info(writer.toString());
                   
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                // Ignore SSL certificate error if necessary.  Used during development.  Should not be needed. 
                String ignoreCert = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "BTMUeBiz_Ignore_SSL_Cert", "N");
                if ("Y".equals(ignoreCert)) {
                    TrustManager[] trustAllCerts = new TrustManager[] { 
                      new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() { 
                          return new X509Certificate[0]; 
                        }
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                    }};

                    // Ignore differences between given hostname and certificate hostname
                    HostnameVerifier hv = new HostnameVerifier() {
                      public boolean verify(String hostname, SSLSession session) { return true; }
                    };

                    // Install the all-trusting trust manager
                    try {
                      SSLContext sc = SSLContext.getInstance("SSL");
                      sc.init(null, trustAllCerts, new SecureRandom());
                      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                      HttpsURLConnection.setDefaultHostnameVerifier(hv);
                    } catch (Exception e) {
                        LOG.info ("ReauthenticateMediatorBean.reauthenticateTransactionUsingVascoBTMUeBiz: " + e.getMessage());
                    }
                }

                EbzOTPVerifySignatureService service = new EbzOTPVerifySignatureService();
                EbzOTPVerifySignature port = service.getEbzOTPVerifySignaturePort();


                BindingProvider bp = (BindingProvider)port;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceURL);

                ExecuteOutput executeOutput = null;
                try {
                    executeOutput = port.execute(executeInput);
                }
                catch (Exception e) {
                    String exceptionMessage = e.getMessage();
                    if (exceptionMessage.length() > 2000) exceptionMessage = exceptionMessage.substring(0, 2000);
                    logAuth2FAError(auth2FALogOid, "Exception", exceptionMessage);
                    mediatorServices.getErrorManager().issueError(
                                    TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.SERVER_ERROR);
                    e.printStackTrace();
                    return returnDoc;
                    
                }
                // print out the response SOAP message
                try {
                    LOG.info("ReauthenticateMediatorBean.reauthenticateTransactionUsingVascoBTMUeBiz executeOutput");
                    JAXBElement<ExecuteOutput> jxo = objFactory.createExecuteOutput(executeOutput);
                    JAXBContext jc = JAXBContext.newInstance(ExecuteOutput.class);
                    Marshaller m = jc.createMarshaller();
                    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    StringWriter writer = new StringWriter();
                    m.marshal(jxo, writer);
                    LOG.info(writer.toString());
                }
                catch (Exception e) {
                    LOG.info(e.getMessage());
                }

                // Check result
                String errorCode = null;
                String errorMsg = null;
                if (executeOutput.getSoakyoutsuuerrorjouhou() != null 
                        && executeOutput.getSoakyoutsuuerrorjouhou().size() > 0 
                        && executeOutput.getSoakyoutsuuerrorjouhou().get(0) != null
                        && executeOutput.getSoakyoutsuuerrorjouhou().get(0).getErrorJouhou() != null
                        && executeOutput.getSoakyoutsuuerrorjouhou().get(0).getErrorJouhou().size() > 0
                        && executeOutput.getSoakyoutsuuerrorjouhou().get(0).getErrorJouhou().get(0) != null) {
                    ErrorJouhou errorJouhou = executeOutput.getSoakyoutsuuerrorjouhou().get(0).getErrorJouhou().get(0);
                    errorCode = errorJouhou.getErrorCode();
                    errorMsg = errorJouhou.getErrorMongon();
                }
                if (errorCode != null && !errorCode.trim().equals("")) {
                    logAuth2FAError(auth2FALogOid, errorCode, errorMsg);
                    String[] errorParms = {errorCode, 
                        mediatorServices.getResourceManager().getText("BTMUeBizErrorMessage."+errorCode, TradePortalConstants.TEXT_BUNDLE)};
                    mediatorServices.getErrorManager().issueError(
                                    TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.AUTHENTICATION_FAILED,
                                    errorParms);
                    return returnDoc;
                }
                
                returnDoc.setAttribute("/logonSuccesful","true");
                logAuth2FASuccess(auth2FALogOid, errorCode, errorMsg);
                return returnDoc;
    }

    private static ThreadLocal<String> timestamp = new ThreadLocal<String>() {
            protected String initialValue() {
                    return new String();
            }
    };
    private static ThreadLocal<String> timestamp2 = new ThreadLocal<String>() {
            protected String initialValue() {
                    return new String();
            }
    };
    private static ThreadLocal<Integer> counter = new ThreadLocal<Integer>() {
            protected Integer initialValue() {
                    return new Integer(0);
            }
    };

    private static String createServiceid() throws AmsException {

            final int HOST_NAME_LENGTH = 8;
            final int APPL_SERVER_NAME_LENGTH = 8;
            final int THREADID_LENGTH = 16;
            final int SERVICE_NAME_LENGTH = 50;
            final String TIMESTAMP_FORMAT = "yyyyMMddHHmmssSSS";;
            final String TIMESTAMP_FORMAT2 = "yyyy-MM-dd HH:mm:ss.SSS";;
            final char CHAR_UNDER_SCORE = '_';
            final char CHAR_ZERO = '0';
            InetAddress localHost = null;
            try {
                localHost = InetAddress.getLocalHost();
            }
            catch (java.net.UnknownHostException e) {
                throw new AmsException(e); // just cast the type and rethrow
            }
            String localHostName = localHost.getHostName();

            if (localHostName.length() >= HOST_NAME_LENGTH) {
                    localHostName = localHostName.substring(0, HOST_NAME_LENGTH);
            } else {
                    localHostName = rightPad(localHostName, HOST_NAME_LENGTH, CHAR_UNDER_SCORE);
            }

            // app Server Name
            PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("TradePortal");
            String appServerName  = portalProperties.getString("serverName");

            if (appServerName.length() >= APPL_SERVER_NAME_LENGTH) {
                    appServerName = appServerName.substring(0,APPL_SERVER_NAME_LENGTH);
            } else if (appServerName.length() < APPL_SERVER_NAME_LENGTH) {
                    appServerName = rightPad(appServerName, 8, CHAR_UNDER_SCORE);
            }


            // ThreadID
            Thread current = Thread.currentThread();
            String threadId = Long.toHexString(current.getId());

            if (threadId.length() < THREADID_LENGTH) {
                    threadId = leftPad(threadId, THREADID_LENGTH, CHAR_ZERO);
            }

            // servicename
            String serviceName = "EbzOTPVerifySignaturePort";
            if (serviceName.length() >= SERVICE_NAME_LENGTH) {
                    serviceName = serviceName.substring(0, SERVICE_NAME_LENGTH);
            } else {
                    serviceName = rightPad(serviceName, SERVICE_NAME_LENGTH, CHAR_UNDER_SCORE);
            }

            // DateTime and Counter
            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
            String currentTimestamp = sdf.format(currentDate);

            int currentCounter;
            if (currentTimestamp.equals(timestamp.get())) {
                    currentCounter = counter.get().intValue() + 1;
                    if (currentCounter == 10) {
                            while (currentTimestamp.equals(timestamp.get())) {
                                    currentCounter = 0;
                                    currentDate = new Date();
                                    currentTimestamp = sdf.format(currentDate);
                            }
                    }
            } else {
                    currentCounter = 0;
            }

            timestamp.set(currentTimestamp);
            counter.set(new Integer(currentCounter));
            
            // tempstamp2 has the value to be used in requestTimestamp field
            SimpleDateFormat sdf2 = new SimpleDateFormat(TIMESTAMP_FORMAT2);
            String currentTimestamp2 = sdf2.format(currentDate);
            timestamp2.set(currentTimestamp2);

            StringBuilder serviceID = new StringBuilder();
            serviceID.append(localHostName).append(appServerName).append(threadId)
                     .append(serviceName).append(currentTimestamp).append(currentCounter);

            return serviceID.toString();

	}
	private static String leftPad(String str, int size, char padchar) {

		if (str == null || size <= str.length()) {
			return str;
		}

		int padsize = size - str.length();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < padsize; i++) {
			sb.append(padchar);
		}

		sb.append(str);
		return sb.toString();
	}

	/**
	 * </DD>
	 * </DL>
	 * 
	 * @param String
	 * @param int size 
	 * @param char padchar 
	 * 
	 * @return String 
	 */
	private static String rightPad(String str, int size, char padchar) {

		if (str == null || size <= str.length()) {
			return str;
		}

		int padsize = size - str.length();
		StringBuffer sb = new StringBuffer();
		sb.append(str);

		for (int i = 0; i < padsize; i++) {
			sb.append(padchar);
		}

		return sb.toString();
	}
    
 /**

     * @date 04/20/2012
     * This method performs the Reauthentication steps when a user is configured to use a VASCO token type and set to use 2FA
     * @param inputDoc - coming in with the request
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    
    private DocumentHandler reauthenticateTransactionUsingGemalto(DocumentHandler inputDoc, DocumentHandler outputDoc, User user, MediatorServices mediatorServices) throws RemoteException, AmsException{
    	DocumentHandler returnDoc = inputDoc;
        LOG.info("ReauthenticateMediatorBean.reauthenticateTransactionUsingGemalto: inputDoc="+inputDoc.toString());
    	
        	
        	
        //String isCMTransaction = inputDoc.getAttribute("/isCMTransaction");
        //boolean isCashManagementTrans = false;
        //        String[] signatureFields = new String[1];   //04/28/2012 - changed the size of the signature fields array   	
        //if(StringFunction.isNotBlank(isCMTransaction)){
        //        isCashManagementTrans=Boolean.valueOf(isCMTransaction);
        //}

        //Obtain the One Time Password (OTP) or SIG entered in the Token
        String signature         = inputDoc.getAttribute(("/Signature_Data"));
        String userKey = user.getAttribute("sso_id");	
        String contentData = inputDoc.getAttribute("/VascoKeyInfo");
        
        //transID is a unique number for every transaction. using a random number
        //Only use positive values and restrict to 6 in length
        String transID = ""; 
        SecureRandom sr = new SecureRandom();
        long transactionID = sr.nextLong();
        transactionID = Math.abs(transactionID);
        transID = String.valueOf(transactionID);
        if(transID.length()>6)transID=transID.substring(0, 6);


         if(verifyGemaltoSignature(transID, userKey, signature, contentData,
                         user,inputDoc, outputDoc, mediatorServices )){
                 returnDoc.setAttribute("/logonSuccesful","true");
                 return returnDoc;
         }

       	 sr=null;
       

        return returnDoc;
    }
    
	/**

     * @date 10/27/2011
     * @param tokenApplicationID - a concatenation of token id that defined on corporate customer profile page
     * and application id from CAAS (defined in Tradeportal.properties file)
     * @param user - current logged in user
     * @param outputDoc
     * @param mediatorServices
     * @param userToLog4Audit TODO
     * @param transId - A unique identifier for each transaction/request
     * @param oneTimePassword - entry on the token typed in by the user
     * @return
     * @throws RemoteException
     * @throws AmsException
     */
    private boolean verifyGemaltoSignature(String transID, String userKey, String signature, String contentDataString, User user,
 		   DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices) 
    throws RemoteException, AmsException {

 	  String serviceURL = "";
 	   try {
 		   serviceURL = portalProperties.getString("VascoSSASSoapEndpointAddress");
 	   }
 	   catch (MissingResourceException e){
 		   //LOG.debug(" THE VASCO SSAS soap endpoint is missing: "+e);
 		   //SOP used for VASCO debugging in order to capture output always in the out log
 		   LOG.info("ReauthenticateMediatorBean.verifyGemaltoSignature: VascoSSASSoapEndpointAddress missing: "+e);
 		   //The default URL specified in the WSDL file will be used TODO: add code to retrieve the default one if necessary
 	   }	    

           String auth2FALogOid = "";
           String reAuthObjectType = inputDoc.getAttribute("/reAuthObjectType");
           //log the entry in the audit table.
           //G indicates that it is a Gemalto request
           auth2FALogOid = logAuth2FARequest(
                           user.getAttribute("user_identifier"), serviceURL,
                        transID, reAuthObjectType, "G" );
 	   String ignoreValidation2FA = null;
 	   // 2FAignoreValidation is set to Y if we ignore the 2FA validation (in Dev region where there is no SSASoap Endpoint server)
 	   try {
 		   ignoreValidation2FA = portalProperties.getString("2FAignoreValidation");
 	   }
 	   catch (MissingResourceException e){
 		   // Ignore
 	   }
 	   if ("Y".equals(ignoreValidation2FA)) {
               this.logAuth2FASuccess(auth2FALogOid, "", "2FAignoreValidation=Y");
 		   LOG.info("[ReauthenticateMediatorBean.verifyGemaltoSignature] ignore_validation_2fa loginId=" + user.getAttribute("user_identifier") + ";2FAignoreValidation=Y");
 		   return true;
 	   }

 	   
 	   try {
		   
 		   // Create an instance of the SOAP service.
 		   SSASSoapServiceLocator soapService = new SSASSoapServiceLocator();
 		   // Set the URL explicitly based on configuration
 		   soapService.setSSASSoapEndpointAddress(serviceURL); 

 		   BasicResponse basicResponse = null;
 		   SSASSoap soapCtx = soapService.getSSASSoap();
                   
                   String [] extraDataKeys = {"UserKey"};
                   String [] extraDataValues = {userKey};
                  ExtraData extraData = new ExtraData(extraDataKeys, extraDataValues);
                  ContentData contentData = new ContentData(contentDataString.getBytes(), false);

                  
                  CSCResponse response = soapCtx.doCSCPKCS7(transID, extraData, signature.getBytes(), contentData);

 		   //
 		   
 		   // Check the response.
 		   basicResponse = response.getBasicResponse();
 		   // Check the status.
 		   String status = basicResponse.getStatus();
                   boolean isSuccessful = true;
                   String errorDescription = "";
 		   if (status.equalsIgnoreCase("ERROR")) {
 			   SSASError errors[] = basicResponse.getErrors();
                           if (errors.length > 0) {
                               for (int i = 0; i < errors.length; i++) {
                                   errorDescription += errors[i].getDescription();
                                   String errorCode = getGemaltoErrorCode(errors[i].getDescription());
 				   LOG.info("ReauthenticateMediatorBean.verifyGemaltoSignature: AdditionalInfo: " + errors[i].getAdditionalInfo()+"; Description: "+errors[i].getDescription()+"; Name: "+ errors[i].getName());
                                   mediatorServices.getErrorManager().issueError(getClass().getName(), 
                                       errorCode);                      
                               }
                               isSuccessful = false;
                           }
                           else {
                               errorDescription = "errros.length = " + errors.length;
                               mediatorServices.getErrorManager().issueError(getClass().getName(), 
                                       "GEMALTO_ERROR.MESG_STATUS_UNKNOWN ");                               
                               isSuccessful = false;                               
                           }
 		   }
                   else if (status.toUpperCase().contains("REVOKED")) {
                       mediatorServices.getErrorManager().issueError(getClass().getName(), 
                                       "GEMALTO_ERROR.MESG_STATUS_REVOKED");                               
                       isSuccessful = false;
                   }
                   else if (status.toUpperCase().contains("UNKNOWN")) {
                       mediatorServices.getErrorManager().issueError(getClass().getName(), 
                                       "GEMALTO_ERROR.MESG_STATUS_UNKNOWN ");                               
                       isSuccessful = false;
                   }
                   
                   if (isSuccessful) {
                       logAuth2FASuccess(auth2FALogOid, status, null);
                   }
                   else {
                       errorDescription = "transID="+transID + ";UserKey="+userKey+";contentData="+contentDataString+";original error description="+errorDescription+";Signature_Data="+signature;
 	 	       LOG.info("ReauthenticateMediatorBean.verifyGemaltoSignature: " + errorDescription); 			   
                       if (errorDescription.length() > 2000) errorDescription = errorDescription.substring(0, 2000);
                       logAuth2FAError(auth2FALogOid, status, errorDescription);
                   }
                   return isSuccessful;


	    } 
           catch (Exception e) {
                    String exceptionMessage = e.getMessage();
                    exceptionMessage = "transID="+transID + ";UserKey="+userKey+";contentData="+contentDataString+";exception message="+exceptionMessage+";Signature_Data="+signature;
                    LOG.info("ReauthenticateMediatorBean.verifyGemaltoSignature: " + exceptionMessage);
                    if (exceptionMessage.length() > 2000) exceptionMessage = exceptionMessage.substring(0, 2000);
                    logAuth2FAError(auth2FALogOid, "Exception", exceptionMessage);
                    mediatorServices.getErrorManager().issueError(
                                    TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.SERVER_ERROR );
                    e.printStackTrace();
                    return false;
                    
           }

    }    
        
  static final private String SSAS_DESC_SIG_MISSING    = "Missing signature to check";
  static final private String SSAS_DESC_REVCHK_ERROR   = "Revocation check error";
  static final private String SSAS_DESC_CERT_EXPIRED   = "Certificate has expired";
  static final private String SSAS_DESC_CERT_NOPATH    = "No path found for the certificate within the current PKI";
  static final private String SSAS_DESC_FAILED_VERIFY  = "Signature verification failed";
  static final private String SSAS_DESC_INVALID_FORMAT = "Invalid format of the signature message";
  static final private String SSAS_DESC_NO_DIR_SERVER  = "Directory server not found";
  static final private String SSAS_DESC_DIR_BIND_FAIL  = "Unable to bind to the directory";
  static final private String SSAS_DESC_DIR_NO_USER    = "User not found in directory";
  static final private String SSAS_DESC_DIR_NO_CERT    = "Signing certificate not found in directory";
  static final private String SSAS_DESC_CERT_MISMATCH  = "Signing certificate not matched directory certificate";
private String getGemaltoErrorCode (String ssasMessage) {
    String msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";

    if (ssasMessage != null)
    {
      if (ssasMessage.contains(SSAS_DESC_SIG_MISSING))
        msg = "GEMALTO_ERROR.MESG_DESC_SIG_MISSING";
      else if (ssasMessage.contains(SSAS_DESC_REVCHK_ERROR))
        msg = "GEMALTO_ERROR.MESG_DESC_REVCHK_ERROR";
      else if (ssasMessage.contains(SSAS_DESC_CERT_EXPIRED))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_EXPIRED";
      else if (ssasMessage.contains(SSAS_DESC_CERT_NOPATH))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_NOPATH";
      else if (ssasMessage.contains(SSAS_DESC_FAILED_VERIFY))
        msg = "GEMALTO_ERROR.MESG_DESC_FAILED_VERIFY";  
      else if (ssasMessage.contains(SSAS_DESC_INVALID_FORMAT))
        msg = "GEMALTO_ERROR.MESG_DESC_INVALID_FORMAT";
      else if (ssasMessage.contains(SSAS_DESC_NO_DIR_SERVER))
        msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";  
      else if (ssasMessage.contains(SSAS_DESC_DIR_BIND_FAIL))
        msg = "GEMALTO_ERROR.MESG_STATUS_DEFAULT";
      else if (ssasMessage.contains(SSAS_DESC_DIR_NO_USER))
        msg = "GEMALTO_ERROR.MESG_DESC_DIR_NO_USER";  
      else if (ssasMessage.contains(SSAS_DESC_DIR_NO_CERT))
        msg = "GEMALTO_ERROR.MESG_DESC_DIR_NO_CERT";
      else if (ssasMessage.contains(SSAS_DESC_CERT_MISMATCH))
        msg = "GEMALTO_ERROR.MESG_DESC_CERT_MISMATCH";  
    }
    
    return msg;
  }  


    /**
     * Process authentication errors.
     * 
     * @param recertificationInd
     * @param reAuthObjectType
     * @param reAuthObjectOidList
     * @param userOid
     * @param mediatorServices
     * @throws AmsException
     * @throws RemoteException
     */
    private void processAuthenticationErrors(String recertificationInd, 
            String reAuthObjectType, List reAuthObjectOidList, String userOid, 
            DocumentHandler outputDoc, MediatorServices mediatorServices) 
            throws AmsException, RemoteException {
        for(int i=0; i<reAuthObjectOidList.size(); i++) {
            String oid = (String)reAuthObjectOidList.get(i);
            
            if ( "Transaction".equals(reAuthObjectType) ) {
                processTransactionAuthenticationError( recertificationInd, oid, userOid, outputDoc, mediatorServices );
            } else if ( "PayRemit".equals(reAuthObjectType) ) {
                processPayRemitAuthenticationError( oid, mediatorServices );
            } else if ( "Invoice".equals(reAuthObjectType) ) {
                processInvoiceAuthenticationError( oid, mediatorServices );
            }else if ( "InvoiceMgnt".equals(reAuthObjectType) ) {
            	processInvoiceManagementAuthenticationError( oid, mediatorServices ); //IR#DEUM060655441
            }
        }
    }

  
    /**
     * If an authentication error, do some follow up --
     * If on a list page, issue additional individual errors.
     * Also change transaction status if necessary.
     */
    private void processTransactionAuthenticationError(
                String recertificationInd,
                String reAuthObjectOid, String userOid, 
                DocumentHandler outputDoc, MediatorServices mediatorServices)
            throws AmsException, RemoteException {

        //get potentially necessary info about the transaction
        Transaction transObj = (Transaction) 
            mediatorServices.createServerEJB("Transaction",
                Long.parseLong(reAuthObjectOid));
        String transactionType = transObj.getAttribute("transaction_type_code");
        String instrumentOid = transObj.getAttribute("instrument_oid");
        Instrument instrument = (Instrument) 
            mediatorServices.createServerEJB("Instrument",
                Long.parseLong(instrumentOid));
        String instrumentType = instrument.getAttribute("instrument_type_code");
        long clientBankOid = instrument.getAttributeLong("client_bank_oid");

        boolean requireAuthentication = 
            requireTransactionAuthentication(instrumentOid, instrumentType,
                reAuthObjectOid, transactionType, clientBankOid, mediatorServices);
        
        //if on a list page and the transaction requires authentication issue an additional error 
        if ( TradePortalConstants.INDICATOR_MULTIPLE.equals(recertificationInd) &&
                requireAuthentication ) {
            mediatorServices.getErrorManager().issueError(getClass().getName(),
                TradePortalConstants.INSTRUMENT_AUTHENTICATION_FAILED,
                instrument.getAttribute("complete_instrument_id") );
                    
        }

        //if ready to authorize, change the status to authorize failed
        if ( requireAuthentication &&
                 TransactionStatus.READY_TO_AUTHORIZE.equals(
                     transObj.getAttribute("transaction_status") ) ) {

            //lock the object
            boolean previouslyLocked = false;
            try
            {
                LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
                    Long.parseLong(userOid), true);
                mediatorServices.debug("LogonMediator: Locking instrument "
                    + instrumentOid + "successful");
            }
            catch (InstrumentLockException e)
            {
                if (e.getUserOid()==Long.parseLong(userOid)) {
                    previouslyLocked = true;
                }
                else
                {                    
                    // issue instrument in use error indicating instrument ID,
                    // transaction type, first name and last name of user that has instrument
                    // reserved, then skip processing this instrument
                    String userLocale = mediatorServices.getCSDB().getLocaleName();
                    mediatorServices.getErrorManager().issueError (getClass ().getName (),
                        TradePortalConstants.TRANSACTION_IN_USE, new String[] {
                            e.getInstrumentNumber(),
                            ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",
                                transactionType, userLocale), e.getFirstName(), e.getLastName(),
                            mediatorServices.getResourceManager().getText("TransactionAction.Authorized",
                                TradePortalConstants.TEXT_BUNDLE)});
                    return;    
                }
            }
            
            try { //wrap all work in order to unlock
                // set transaction status to failed
                transObj.setAttribute("transaction_status",
                    TransactionStatus.AUTHORIZE_FAILED);
                transObj.setAttribute("transaction_status_date",
                    DateTimeUtility.getGMTDateTime());
                try
                {
                    transObj.save();
                    outputDoc.setAttribute("/Refresh/Transaction", "Y");
                }
                catch(Exception e)
                {
                    //not significant enough to stop processing, just leave status as is
                    LOG.info(e.toString());
                    e.printStackTrace();
                }
            } finally {
                //unlock instrument
                if (!previouslyLocked) { //but not if previously locked
                    try {
                        LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
                    } catch (InstrumentLockException e) {
                        // this should never happen
                        e.printStackTrace();
                    }
                }
            }
        }
    }

   /**
    * Does the transaction require authentication?
    */
   private boolean requireTransactionAuthentication(String instrumentOid, String instrumentType, 
         String transactionOid, String transactionType, long clientBankOid, MediatorServices mediatorServices) 
         throws RemoteException, AmsException {
      ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
      clientBank.getDataFromReferenceCache(clientBankOid);
      String requireTranAuth = clientBank.getAttribute("require_tran_auth");
      if (InstrumentAuthentication.requireTransactionAuthentication(
             requireTranAuth,
             instrumentType + "_" + transactionType)) {
         return true;
      }
      return false;
   }


    /**
     * Generate an authentication error for each individual pay remit.
     */
    public void processPayRemitAuthenticationError(
            String reAuthObjectOid, 
            MediatorServices mediatorServices)
            throws AmsException, RemoteException {        
        String payRemitOid = reAuthObjectOid;
        
        //the payRemitID is the payment reference
        PayRemit payRemit =
            (PayRemit) mediatorServices.createServerEJB(
                "PayRemit",
                Long.parseLong(payRemitOid));
        String paymentReferenceNum = payRemit.getAttribute("payment_invoice_reference_id"); 
        
        mediatorServices.getErrorManager().issueError(getClass().getName(),
             TradePortalConstants.PAY_MATCH_AUTHENTICATION_FAILED,
             paymentReferenceNum );
    }

    /**
     * Generate an authentication error for each individual invoice.
     */
    public void processInvoiceAuthenticationError(
            String reAuthObjectOid, 
            MediatorServices mediatorServices)
            throws AmsException, RemoteException {        
        String invoiceOid = reAuthObjectOid;
        
        // get the invoice ID
        Invoice invoice =
            (Invoice) mediatorServices.createServerEJB(
                "Invoice",
                Long.parseLong(invoiceOid));
        String invoiceId = invoice.getAttribute("invoice_reference_id"); 

        mediatorServices.getErrorManager().issueError(getClass().getName(),
            TradePortalConstants.INVOICE_AUTHENTICATION_FAILED,
            invoiceId );
    }
    
    /**
     * IR#DEUM060655441
     * Generate an authentication error for each individual invoice (Groups - InvoiceManagement).
     */
    public void processInvoiceManagementAuthenticationError(
            String reAuthObjectOid, 
            MediatorServices mediatorServices)
            throws AmsException, RemoteException {        
        String invoiceOid = reAuthObjectOid;
        
        // get the oid
        InvoiceGroup invoiceGroup =
            (InvoiceGroup) mediatorServices.createServerEJB(
                "InvoiceGroup",
                Long.parseLong(invoiceOid));
        try {
			//String invoiceId = invoiceGroup.getAttribute("invoice_group_oid"); 
			String invoiceStatus = invoiceGroup.getAttribute("invoice_status"); 
			
			//if status is authorized, set it to partially authorized
			//otherwise set it to failed
			if(TradePortalConstants.UPLOAD_INV_STATUS_AUTH.equalsIgnoreCase(invoiceStatus)){
				invoiceGroup.setAttribute("invoice_status",	TradePortalConstants.UPLOAD_INV_STATUS_PAR_AUTH);	
			}else
				invoiceGroup.setAttribute("invoice_status",	TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
		} catch (Exception e) {
			LOG.debug("Exception processing invoice ReAuth errors: "+e);
		}
        
    }    

    
	private boolean isItAProxyButton(String buttonPressed) {
		if (TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_INVOICES
				.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_INV_PROXYAUTHORIZE_CREDIT_NOTE
						.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_PROXY_AUTHORIZE
						.equalsIgnoreCase(buttonPressed)
				|| TradePortalConstants.BUTTON_PROXY_APPROVEDISCOUNTAUTHORIZE
						.equalsIgnoreCase(buttonPressed)) {
			return true;
		}
		return false;
	}
    
}
