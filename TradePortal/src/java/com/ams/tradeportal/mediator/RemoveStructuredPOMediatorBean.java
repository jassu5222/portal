package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.PurchaseOrder;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
/**
 * This class is used for removing one or more PO  from an Import LC
 * Issue or Amend , ATP transaction. Once the PO  have been removed from a
 * transaction, the method will update the transaction's amount, last shipment
 * date, expiry date accordingly.
 *
 */
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class RemoveStructuredPOMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(RemoveStructuredPOMediatorBean.class);
	private transient String userLocale = null;
	// The following variables have been declared transient because the mediator
	// is stateless and they are initialized in only one place.
	private transient MediatorServices mediatorServices = null;
	private transient Transaction transaction = null;
	private transient Instrument instrument = null;
	private transient Vector strucPOOidsList = null;
	private transient String uploadDefinitionOid = null;
	private transient Terms terms = null;
	private transient int numberOfPOs = 0;
	private transient long shipmentTermsOid = 0;
	private transient String userOID  = null;

	/**
	 * This method performs deletion of PO The format expected from the input
	 * doc is:
	 * 
	 * <POLineItemList> <POLineItem ID="0"><poLineItemOid>1000001</POLineItem>
	 * <POLineItem ID="1"><poLineItemOid>1000002</POLineItem> <POLineItem
	 * ID="2"><poLineItemOid>1000003</POLineItem> <POLineItem
	 * ID="3"><poLineItemOid>1000004</POLineItem> <POLineItem
	 * ID="6"><poLineItemOid>1000001</POLineItem> </POLineItemList> This is a
	 * non-transactional mediator that executes transactional methods in
	 * business objects
	 * 
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator
	 *            XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall
	 *            Mediator XML document)
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary
	 *            functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices newMediatorServices)
			throws RemoteException, AmsException {
		DocumentHandler poLineItemDoc = null;
		String substitutionParm = null;
		Vector strucPOList = null;
		long transactionOid = 0;

		mediatorServices = newMediatorServices;
		strucPOList = inputDoc.getFragments("/POLineItemList/POLineItem");
		transactionOid = inputDoc
				.getAttributeLong("/Transaction/transaction_oid");
		shipmentTermsOid = inputDoc
				.getAttributeLong("/Terms/ShipmentTermsList/shipment_oid");
		// Based on the button clicked, process an appropriate action.
		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		if (!(TradePortalConstants.REMOVE_PO.equals(buttonPressed) || "RemoveStructuredPOButton".equals(buttonPressed))) {
			return outputDoc;
		}
		
		userOID  = inputDoc.getAttribute("/userOID");
		mediatorServices.debug("The button pressed is " + buttonPressed);

		numberOfPOs = strucPOList.size();

		userLocale = mediatorServices.getCSDB().getLocaleName();

		// if the user locale not specified default to en_US
		if ((userLocale == null) || (userLocale.equals(""))) {
			userLocale = "en_US";
		}

		// If nothing was selected by the user, issue an error indicating this
		// and return
		if (numberOfPOs <= 0) {
			substitutionParm = mediatorServices.getResourceManager().getText(
					"StructuredPOAction.Remove",
					TradePortalConstants.TEXT_BUNDLE);

			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_ITEM_SELECTED, substitutionParm);

			return outputDoc;
		}

		// Get a list of all the PO oids that were selected
		String multipleStrucPOs = TradePortalConstants.INDICATOR_NO;
		strucPOOidsList = POLineItemUtility.getPOLineItemOids(strucPOList,
				multipleStrucPOs, TradePortalConstants.PO_REMOVE);
		if (multipleStrucPOs.equals(TradePortalConstants.INDICATOR_YES))
			numberOfPOs = strucPOOidsList.size();
		try {
			transaction = (Transaction) mediatorServices.createServerEJB(
					"Transaction", transactionOid);

			// Needs to instantiate instrument object for updating amendment
			// transaction.
			String transactionType = transaction
					.getAttribute("transaction_type_code");
			if (transactionType.equals(TransactionType.AMEND)) {
				long instrumentOid = transaction
						.getAttributeLong("instrument_oid");
				instrument = (Instrument) mediatorServices.createServerEJB(
						"Instrument", instrumentOid);
			}

			removePOLineItems(outputDoc);
			transaction.save(false);
		} catch (Exception e) {
			System.out
					.println("Exception occured in RemoveStructuredPOMediator: "
							+ e.getMessage());
			e.printStackTrace();
		}
		
		if (InstrumentServices.isBlank(inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"))) {
			outputDoc.setAttribute("/ChangePOs/goods_description",TradePortalConstants.getPropertyValue("TextResources", "StructuredPurchaseOrders.defaultGoodsDescription", null));
		}else {
			outputDoc.setAttribute("/ChangePOs/goods_description",inputDoc.getAttribute("/Terms/ShipmentTermsList/goods_description"));
		}		

		outputDoc.setAttribute("/ChangePOs/isStructuredPO",TradePortalConstants.INDICATOR_YES);
		return outputDoc;
	}

	/**
	 * This method removes the PO selected by the user from the transaction that
	 * he/she is currently viewing. If the PO were uploaded, they are
	 * disassociated from the transaction. If the PO line items were manually
	 * entered, they are deleted. After all PO have been saved, the amount,
	 * date, and PO description fields are all re-derived to reflect the removed
	 * item values.
	 * 
	 * @param DocumentHandler
	 *            - the mediator's output doc
	 * @exception com.amsinc.ecsg.frame.AmsException
	 * @exception java.rmi.RemoteException
	 * @return void
	 */
	private void removePOLineItems(DocumentHandler outputDoc)
			throws AmsException, RemoteException {
		PurchaseOrder poLineItem = null;

		long poLineItemOid = 0;
		int numberOfPOsRemaining = 0;
		Vector associatedPoList = new Vector();
		Vector associatedPoNumList = new Vector();

		// Get a list of the POs already associated with this instrument
		String alreadyAssociatedSql = "select purchase_order_oid,purchase_order_num from purchase_order where a_shipment_oid =?  ";
		DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(alreadyAssociatedSql, false, new Object[]{shipmentTermsOid});

		if (resultSet == null)
			resultSet = new DocumentHandler();

		Vector alreadyAssociatedList = resultSet
				.getFragments("/ResultSetRecord");
		for (int i = 0; i < alreadyAssociatedList.size(); i++) {
			String poOid = ((DocumentHandler) alreadyAssociatedList
					.elementAt(i)).getAttribute("/PURCHASE_ORDER_OID");
			associatedPoList.addElement(poOid);
			associatedPoNumList.add(((DocumentHandler) alreadyAssociatedList
					.elementAt(i)).getAttribute("/PURCHASE_ORDER_NUM"));
		}

		// Update the assigned to transaction indicators for each PO line item
		// being
		// added to the transaction
		for (int i = 0; i < numberOfPOs; i++) {
			poLineItemOid = Long.valueOf((String) strucPOOidsList.elementAt(i))
					.longValue();
			poLineItem = (PurchaseOrder) mediatorServices.createServerEJB(
					"PurchaseOrder", poLineItemOid);

			uploadDefinitionOid = poLineItem
					.getAttribute("upload_definition_oid");

			// Manually entered POs should be deleted and uploaded POs are just
			// un-associated with the transaction
			poLineItem.setAttribute("transaction_oid", null);
			poLineItem.setAttribute("instrument_oid", null);
			poLineItem.setAttribute("shipment_oid", null);
			poLineItem.setAttribute("user_oid", userOID);
			poLineItem.setAttribute("action", TradePortalConstants.PO_ACTION_UNLINK);
			poLineItem.setAttribute("status", TradePortalConstants.PO_STATUS_UNASSIGNED);
			associatedPoNumList.remove(poLineItem.getAttribute("purchase_order_num"));
			poLineItem.save();
			associatedPoList.remove(strucPOOidsList.elementAt(i));
			


			poLineItem = null;
		}
		

		// Get the terms associated with the current transaction
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		ComponentList shipmentTermsList = (ComponentList) terms
				.getComponentHandle("ShipmentTermsList");
		int numShipments = shipmentTermsList.getObjectCount();

		ShipmentTerms shipmentTerms = (ShipmentTerms) shipmentTermsList
				.getComponentObject(shipmentTermsOid);


			POLineItemUtility.deriveTranDataFromStrucPO(true, transaction,
					shipmentTerms, numShipments, associatedPoList,
					Long.parseLong(uploadDefinitionOid), getSessionContext(),instrument,
					userLocale,outputDoc,associatedPoNumList); 

			String termsPartyOid = terms.getAttribute("c_FirstTermsParty");
	        if (termsPartyOid != null && !termsPartyOid.equals(""))
	        {
	        	TermsParty firstTermsParty = (TermsParty) terms
				.getComponentHandle("FirstTermsParty");
	        	
	        	outputDoc.setAttribute("/ChangePOs/name", firstTermsParty.getAttribute("name"));//SHR IR 6752
	        }

		// Get data from Terms and set it in the Out section of the XML
		// That way. when the user is returned to the page, the updated data
		// will
		// appear along with other data that they entered
		outputDoc.setAttribute("/ChangePOs/amount",
				terms.getAttribute("amount"));
		outputDoc.setAttribute("/ChangePOs/amount_currency_code",
				terms.getAttribute("amount_currency_code"));

		// Convert date formats so that the Out section of the XML is properly
		// updated
		SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat jPylon = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			if (!StringFunction.isBlank(shipmentTerms
					.getAttribute("latest_shipment_date"))) {
				Date date = jPylon.parse(shipmentTerms
						.getAttribute("latest_shipment_date"));
				outputDoc.setAttribute("/ChangePOs/latest_shipment_date",
						iso.format(date));
			}

			if (!InstrumentServices.isBlank(terms.getAttribute("expiry_date"))) {
				Date date = jPylon.parse(terms.getAttribute("expiry_date"));
				outputDoc.setAttribute("/ChangePOs/expiry_date",
						iso.format(date));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Issue an informational message indicating to the user that the
		// selected PO
		// have been removed successfully and save the transaction
		outputDoc.setAttribute("/ChangePOs/fromStrucPO","Y");
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.REMOVE_PURCHASE_ORDER_SUCCESS,
				String.valueOf(numberOfPOs));

	}

}
