package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;
import javax.ejb.*;
import java.util.*;

import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class RouteDeleteMessagesMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(RouteDeleteMessagesMediatorBean.class);
  /**
   * This method performs routing of Transactions based on either a given corp
   * org's default user or a given user.  If an Org oid is passed, this oid will
   * be used to determine the default oid.  Otherwise, the user's route 
   * user's oid will be used to reassign the transaction.
   * 
   * The format expected from the input doc is:
   *     
   * <MessageList>
   *	<Message ID="0"><messageOid>1000001</messageOid></Message>
   *	<Message ID="1"><messageOid>1000002</messageOid></Message>
   *	<Message ID="2"><messageOid>1000003</messageOid></Message>
   *	<Message ID="3"><messageOid>1000004</messageOid></Message>
   *	<Message ID="6"><messageOid>1000001</messageOid></Message>
   * </MessageList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   * <Route>
   *    <userOid>1001</userOid>
   *    <corporateOrgOid>1000001</corporateOrgOid>
   *    <routeToUser>Y/N</routeToUser>
   *    <multipleOrgs>Y/N</multipleOrgs>
   * </Route>
   *
   * The messageData value consists of message oid, a '/' and discrepancy flag,
   * which is "Y" or "N".  /User/userOid and /User/securityRights contain
   * the userOid and securityRights of the user performing the routing
   * and the user's security rights respectively.
   * The /Route/routeToUser is optional and used for routing.  
   * It indicates if the user has selected the checkbox for
   * routing to the User (value of Y) or to the organization (value of N).
   * multipleOrgs indicate if there are multiple organizations related to the user
   * or if there is only one organizaion, in which case the user does not have
   * the option of selecting the organization.
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects.
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      // get each Transaction Oid and process each transaction; each transaction is unique within itself
		DocumentHandler messageDoc = null;
		MailMessage message = null;
		Vector messageList = null;
		String securityRights = null;
		String messageOid = null;
		StringBuffer routeToUserOid = new StringBuffer();
		String routeUserOid = null;
		String userLocale = null;
		String userOid = null;
		String messageTitle = null;
		boolean routeAction = false;
		boolean success = false;
		String routeValidation = null;
		int count = 0;
		String buttonPressed = "";
		String type = "";

      mediatorServices.debug("Route Delete Messages Mediator input doc: " + inputDoc);

      // populate variables with inputDocument information such as transaction list, user oid, 
      // and security rights, etc.
      messageList     	   = inputDoc.getFragments("/MessageList/Message");
      userOid              = inputDoc.getAttribute("/User/userOid");
      securityRights       = inputDoc.getAttribute("../securityRights");
      count                = messageList.size();

      mediatorServices.debug("RouteDeleteMessagesMediator: number of transactions: " + count);

      // check to see if the messages are to be routed or deleted
      if (inputDoc.getFragment("/routeAction") == null)
      {
	    routeAction = false;
	    buttonPressed = TradePortalConstants.BUTTON_DELETE;
      }
      else
      {
	   routeAction = true;
	   buttonPressed = TradePortalConstants.BUTTON_ROUTE;
      }
     
      // if there are no messages selected, return no item selected error.
      if (count <= 0)
      {
          if (routeAction)
	  {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                   TradePortalConstants.NO_ITEM_SELECTED, 
	         mediatorServices.getResourceManager().getText("TransactionAction.Route",
	    	    			           TradePortalConstants.TEXT_BUNDLE));
	  }
	  else
	  {
             mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                   TradePortalConstants.NO_ITEM_SELECTED, 
	         mediatorServices.getResourceManager().getText("TransactionAction.Delete",
	    	    			           TradePortalConstants.TEXT_BUNDLE));
	  }
         return outputDoc;
      }

      // if the message(s) should be routed, verify the route input data is correctly
      // set - otherwise return with the routing error message
      if (routeAction)
      {      
      	routeValidation = RouteServices.validateRouteData(inputDoc, mediatorServices, routeToUserOid);
	routeUserOid = routeToUserOid.toString();

      	// if there are errors validating the route user/organization, 
      	// set the outputDoc to return routeSelectionError is True
      	// note: using getmaxerrorseverity from mediator services won't work here
      	if (routeValidation.equals(TradePortalConstants.ROUTE_SELECTION_ERROR))
      	{
	    outputDoc.setAttribute("/Route/RouteSelectionErrors", 
	    TradePortalConstants.INDICATOR_YES);
	    return outputDoc;
      	}
      	else if (routeValidation.equals(TradePortalConstants.ROUTE_GENERAL_ERROR))
        {
	    return outputDoc;
        }

      	// get the locale from client server data bridge, used for getting locale
      	// specific information when passing error parameters
      	userLocale = mediatorServices.getCSDB().getLocaleName();
      }

      // For each message, open the message and delete or route the message.
      // The routeMessage and deleteMessage called on MailMessage are transactional
      // whereas this execute method is non-transactional so that each message
      // is processed and saved independently of the other.

      for (int i = 0; i < count; i++)
      {
         messageDoc  = (DocumentHandler) messageList.get(i);
         messageOid = messageDoc.getAttribute("/messageOid");
         mediatorServices.debug("RouteDeleteMessagesMediator: Message oid: " + messageOid );          
	 success= false;         
         
         if( StringFunction.isBlank( messageOid ) ) {
            // This is a new message that has not yet been saved
            message = (MailMessage) mediatorServices.createServerEJB("MailMessage");
            message.newObject();
         }
         else 
         {
             // create the message and call routeMessage or deleteMessage.
             // this may cause the AmsException or the RemoteException which will
             // be thrown by this method.  
             try
             {
                 message = (MailMessage) mediatorServices.createServerEJB("MailMessage", Long.parseLong(messageOid));
             }
             catch (Exception e)
             {
                // message must have been deleted - issue error
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                     TradePortalConstants.ITEM_DELETED);
                continue;
             }
         }       

         // Nar Rel 9.2.0.0 04-Sep-2014 ADD - Begin
			if (TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/fromListView"))
					&& TradePortalConstants.BUTTON_DELETE.equals(buttonPressed)) {
				if (TradePortalConstants.INDICATOR_YES.equals(message.getAttribute("discrepancy_flag"))) {
					type = TransactionType.DISCREPANCY;
				} else if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(inputDoc.getAttribute("/Instrument/instrument_type_code"))
						|| StringFunction.isNotBlank(message.getAttribute("funding_amount"))
						|| StringFunction.isNotBlank(message.getAttribute("funding_date"))
						|| StringFunction.isNotBlank(message.getAttribute("funding_currency"))) {
					type = TradePortalConstants.EMAIL_PRE_DEBIT_FUNDING;
				} else {
					type = TradePortalConstants.EMAIL_TRIGGER_MAIL_MSG;
				}
			}
   	     SecurityAccess.validateUserRightsForTrans( type, "", securityRights, buttonPressed );
   	     // Nar Rel 9.2.0.0 04-Sep-2014 ADD - End
         
		// If routing a message in the message detail page, extract the message detail info
		if (routeAction)
		{
			if( (inputDoc.getAttribute("/Route/fromListView") == null) ||
				(!inputDoc.getAttribute("/Route/fromListView").equals(TradePortalConstants.INDICATOR_YES)))
			 {
				// Populate the message with data from the XML if coming from the mail message detail page
				message.populateFromXmlDoc(inputDoc);
				//Do not setRelatedInstrument if coming from Bank. It would be updated by unpackager.
				if (!message.getAttribute("message_source_type").equals(TradePortalConstants.BANK)){
					message.setRelatedInstrument(inputDoc.getAttribute("/Instrument/complete_instrument_id"), inputDoc.getAttribute("/User/owner_org_oid")); 
				}
			 }
		}

		// In addition, there are some other fields that need to be set
		// All validations of fields will have already taken place in MailMessageMediator
		// Retrieve User Oid based on Listview or Detailed view
		if( inputDoc.getAttribute("/User/user_oid") != null)
		{
			// Mail Message Doc
			message.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/User/user_oid")); 
		}
		else
		{
			// Delete or Route Messages Doc
			message.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/User/userOid"));
		}
		//update date only for non pre-debit funding message
		if(StringFunction.isBlank(message.getAttribute("funding_amount")) &&
				StringFunction.isBlank(message.getAttribute("funding_date")) &&
						StringFunction.isBlank(message.getAttribute("funding_currency"))){
		message.setAttribute("last_update_date", DateTimeUtility.getGMTDateTime() );
		}
		message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);

               // If message_source_type isn't set yet (like for new messages), set it to PORTAL
               if(StringFunction.isBlank(message.getAttribute("message_source_type")))
                  message.setAttribute("message_source_type", TradePortalConstants.PORTAL); 
      
               // Also default the discrepancy flag
               if(StringFunction.isBlank(message.getAttribute("discrepancy_flag")))              
                    message.setAttribute("discrepancy_flag", TradePortalConstants.INDICATOR_NO);
               
               //rkrishna CR 375-D ATP 08/22/2007 Begin
               // Also default the Atp Notice flag
               if(StringFunction.isBlank(message.getAttribute("atp_notice_flag")))              
                    message.setAttribute("atp_notice_flag", TradePortalConstants.INDICATOR_NO);
               //rkrishna CR 375-D ATP 08/22/2007 End

	 messageTitle = message.getAttribute("message_subject");
	 if (routeAction)
	 {
	    try
	    {
	    	success = message.routeMessage(securityRights, userOid, routeUserOid);
	    }
	    catch (Exception e)
	    {
		LOG.info("Exception occured in RouteDeleteMessagesMediator: " + e.toString());
		e.printStackTrace();
	    }
	    if (success)
	    {
	        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
	   	    TradePortalConstants.MESSAGE_PROCESSED, messageTitle,
		    mediatorServices.getResourceManager().getText("MessageAction.Routed",
	    	    TradePortalConstants.TEXT_BUNDLE));
	    }
         }
	 else
	 {
	     try
	     {
	     	success = message.deleteMessage(securityRights, userOid);
	     }
	     catch (Exception e)
	     {
		LOG.info("Exception occured in RouteDeleteMessagesMediator: " + e.toString());
		e.printStackTrace();
	     }
	     if (success)
	     {
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
		    TradePortalConstants.MESSAGE_PROCESSED, messageTitle,
		    mediatorServices.getResourceManager().getText("MessageAction.Deleted",
	    	    TradePortalConstants.TEXT_BUNDLE));
	    }
	 }
      }

      mediatorServices.debug("DeleteRouteTransactionsMediator: " + inputDoc);

      return outputDoc;
   }
}
