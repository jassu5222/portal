package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.util.RouteServices;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class RouteTransactionsMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(RouteTransactionsMediatorBean.class);
  /**
   * This method performs routing of Transactions based on either a given corp
   * org's default user or a given user.  If an Org oid is passed, this oid will
   * be used to determine the default oid.  Otherwise, the user's route 
   * user's oid will be used to reassign the transaction.
   * 
   * The format expected from the input doc is:
   *     
   * <TransactionList>
   *	<Transaction ID="0"><transactionData>1000001/1200001/AMD</transactionData></Transaction>
   *	<Transaction ID="1"><transactionData>1000002/1200002/ISS</transactionData></Transaction>
   *	<Transaction ID="2"><transactionData>1000003/1200003/TRN</transactionData></Transaction>
   *	<Transaction ID="3"><transactionData>1000004/1200004/ASG</transactionData></Transaction>
   *	<Transaction ID="6"><transactionData>1000001/1200007/ISS</transactionData></Transaction>
   * </TransactionList>
   * <User>
   *    <userOid>1001</userOid>
   *    <securityRights>302231454903657293676543</securityRights>
   * </User>
   * <Route>
   *    <userOid>1001</userOid>
   *    <corporateOrgOid>1000001</corporateOrgOid>
   *    <routeToUser>Y/N</routeToUser>
   *    <multipleOrgs>Y/N</multipleOrgs>
   * </Route>
   *
   * The transactionData value consists of transaction oid, a '/', instrument oid, a '/',
   * and the transaction type.  /User/userOid and /User/securityRights contain
   * the userOid and securityRights of the user performing the routing
   * and the user's security rights respectively.
   * The /Route/routeToUser indicates if the user has selected the checkbox for
   * routing to the User (value of Y) or to the organization (value of N).
   * multipleOrgs indicate if there are multiple organizations related to the user
   * or if there is only one organizaion, in which case the user does not have
   * the option of selecting the organization.
   *
   * This is a non-transactional mediator that executes transactional
   * methods in business objects.
   *
   * @param inputDoc The input XML document (&lt;In&gt; section of overall
   * Mediator XML document)
   * @param outputDoc The output XML document (&lt;Out&gt; section of overall
   * Mediator XML document)
   * @param mediatorServices Associated class that contains lots of the
   * auxillary functionality for a mediator.
   */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
                                  throws RemoteException, AmsException
   {
      // get each Transaction Oid and process each transaction; each transaction is unique within itself
      DocumentHandler         transaction          = null;
      Instrument              instrument           = null;
      String[]                substitutionParms    = null;
      Vector                  transactionsList     = null;
      String                  transactionTypeDescr = null;
      String                  transactionData      = null;
      String                  transactionType      = null;
      String                  transactionOid       = null;
      String                  securityRights       = null;
      String                  instrumentOid        = null;
      String                  routeUserOid         = null;
      String                  userLocale           = null;
      String                  userOid              = null;
      StringBuffer	      routeToUserOid 	   = new StringBuffer();
      boolean		      previouslyLocked     = false;
      boolean		      routeSuccess         = false;
      String		      routeValidation      = null;
      int                     separator1           = 0;
      int                     separator2           = 0;
      int                     count                = 0;
 
      mediatorServices.debug("Route Transactions Mediator input doc: " + inputDoc);

      // populate variables with inputDocument information such as transaction list, user oid, 
      // and security rights, etc.
      transactionsList     = inputDoc.getFragments("/TransactionList/Transaction");
      userOid              = inputDoc.getAttribute("/User/userOid");
      securityRights       = inputDoc.getAttribute("/User/securityRights");
      count                = transactionsList.size();

      mediatorServices.debug("RouteTransactionMediator: number of transactions: " + count);

      if (count <= 0)
      {
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                       TradePortalConstants.NO_ITEM_SELECTED, 
	     mediatorServices.getResourceManager().getText("TransactionAction.Route",
	    	    			           TradePortalConstants.TEXT_BUNDLE));

         return outputDoc;
      }

      routeValidation = RouteServices.validateRouteData(inputDoc, mediatorServices, routeToUserOid);
      routeUserOid = routeToUserOid.toString();

      // if there are errors validating the route user/organization, 
      // set the outputDoc to return routeSelectionError is True
      // note: using getmaxerrorseverity from mediator services won't work here
      if (routeValidation.equals(TradePortalConstants.ROUTE_SELECTION_ERROR))
      {
	outputDoc.setAttribute("/Route/RouteSelectionErrors", 
	    TradePortalConstants.INDICATOR_YES);
	return outputDoc;
      }
      else if (routeValidation.equals(TradePortalConstants.ROUTE_GENERAL_ERROR))
      {
	return outputDoc;
      }

      // get the locale from client server data bridge, used for getting locale
      // specific information when passing error parameters
      userLocale = mediatorServices.getCSDB().getLocaleName();

      // a transaction should be locked and processed at the instrument level 
      // since the transaction is a component of instrument
      // for each transaction, lock the instrument and call routeTransaction
      // on the instrument.  routeTransaction is a transactional method.
      for (int i = 0; i < count; i++)
      {
         transaction     = (DocumentHandler) transactionsList.get(i);
         transactionData = transaction.getAttribute("/transactionData");
			transactionData = StringFunction.asciiToUnicode(transactionData);

	 previouslyLocked = false;
	 routeSuccess = false;

         // Split up the instrument oid, transaction oid and transaction type
         // It is possible to use StringTokenizer to break the data up, but since
         // there are only two separators, this is fine for now.
         separator1      = transactionData.indexOf('/');
         separator2      = transactionData.indexOf('/', separator1 + 1);

         transactionOid  = transactionData.substring(0, separator1);
         instrumentOid   = transactionData.substring(separator1 + 1, separator2);
         transactionType = transactionData.substring(separator2 + 1);

         mediatorServices.debug("RouteTransactionMediator: transactionData contents: " + transactionData + 
                                "Instrument oid: " + instrumentOid + " Transaction oid" + transactionOid + 
                                " Transaction Type " + transactionType);

         // Before creating the instrument, attempt to lock the instrument
         try 
         {
            LockingManager.lockBusinessObject(Long.parseLong(instrumentOid), Long.parseLong(userOid), true);

            mediatorServices.debug("RouteTransactionMediator: Locking instrument " + instrumentOid + "successful");
         }
         catch (InstrumentLockException e)
         {
	    // if the instrument is currently locked by this user, continue processing
	    if (e.getUserOid()==Long.parseLong(userOid))
	    {
		previouslyLocked = true;
	    }
	    else
	    {
            	// issue instrument in use error indicating instrument ID, transaction 
            	// type, first name and last name of user that has instrument reserved
            	transactionTypeDescr = ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE", transactionType, 
                                                                                 userLocale);

            	substitutionParms = new String[] {"Instrument ID", transactionTypeDescr, "First Name", "Last Name", 
                                               mediatorServices.getResourceManager().getText("TransactionAction.Routed",
	    	    			       TradePortalConstants.TEXT_BUNDLE)};

            	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                          TradePortalConstants.TRANSACTION_IN_USE, substitutionParms);

            	continue;
	    }
         }

         try
         {
            // create and load the instrument and call routeTransaction
            // this may cause the AmsException or the RemoteException which will
            // be thrown by this method.  The finally clause will unlock the
            // business object
            instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
            
            // Security check for Logged in user
            String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed") ;
            if(StringFunction.isBlank(buttonPressed)){
            	buttonPressed = TradePortalConstants.BUTTON_ROUTE;
            }
            
    		SecurityAccess.validateUserRightsForTrans(
    				instrument.getAttribute("instrument_type_code"),
    				transactionType,
    				securityRights, buttonPressed);

            routeSuccess = instrument.routeTransaction(transactionOid, userOid, securityRights, routeUserOid);
         }
	 catch( Exception e)
	 {
	        LOG.info("Exception occured in RouteTransactionsMediator: " + e.toString());
		e.printStackTrace();
	 }
         finally
         {
 	    // if the route fails and the instrument was previously locked,
	    // don't unlock the instrument because it will return to the transaction
	    // detail page - otherwise unlock
	    if (!(!routeSuccess && previouslyLocked))
	    {
	        try
	        {
	    	    LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
	    	}
	    	catch (InstrumentLockException e)
	    	{
		    // this should never happen
		    e.printStackTrace();
	    	}
	     }
	 }

      }

      mediatorServices.debug("RouteTransactionsMediator: " + inputDoc);

      return outputDoc;
   }
}
