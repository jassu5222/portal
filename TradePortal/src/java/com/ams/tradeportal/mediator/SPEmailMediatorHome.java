package com.ams.tradeportal.mediator;

import java.rmi.RemoteException;

/*
*
*     Copyright  � 2001                         
*     American Management Systems, Incorporated 
*     All rights reserved
*/

import javax.ejb.CreateException;

public interface SPEmailMediatorHome extends javax.ejb.EJBHome{
	
	public SPEmailMediator create()
	throws CreateException, RemoteException;

}
