package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.PurchaseOrderDefinition;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.StructuredPOLogger;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.cache.Cache;
import com.ams.tradeportal.common.cache.TPCacheManager;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;


/*
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class StructuredPOCreateMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(StructuredPOCreateMediatorBean.class);
  // This SQL selects information about all the creation rules
  // for an org.

  private String SELECT_SQL = "";


  private static String ORDERBY_CLAUSE = "order by creation_date_time DESC";

  // This SQL selects unassigned po line items for an org.  The
  // remaining where clause criteria is added dynamically.  They are
  // ordered by ben_name and currency for processing logic.  They are
  // also ordered by upload date/time to provide some consistency
  // for transaction assignment.
  //
  private static String PO_SELECT_SQL = "select purchase_order_oid, seller_name, currency, amount, "
      + "a_upload_definition_oid, purchase_order_num, latest_shipment_date " //BSL IR SRUM042154839 04/24/2012 Rel 8.0 - add PO Def
      + "from purchase_order "
      + "where  deleted_ind !='Y' and a_transaction_oid is NULL "
      + "and a_owner_org_oid = ";
  String poUploadInstrType = null;


  //BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN
  private static String PO_ORDERBY = "order by seller_name, currency, a_upload_definition_oid, amount desc";
  //BSL IR SRUM042154839 04/24/2012 Rel 8.0 END


  // This indicates the maximum number of po line items that
  // can be assigned to a transaction.
  private static int MAX_ITEMS_ON_PO = 94;


  // These are instance variables that are set each and every time the
  // mediator is created.  They are transient because mediators are
  // intended to be stateless.

  private transient MediatorServices mediatorServices;

  private transient StructuredPOLogger logger = null;

  private transient String baseCurrency = null;

  private transient String clientBankOid = null;

  private transient String corporateOrgOid = null;

  private transient String logSequence = null;

  private transient String securityRights = null;

  private transient String timeZone = null;

  private transient String uploadSequenceNumber = null;

  private transient String userLocale = null;

  private transient String userOid = null;

  private transient long uploadDefinitionOid = 0;

  private transient String uploadFileName = null;

	private transient String subsidiaryAccessOrgOid = null;

  private Set pOOIds = new TreeSet<String>();
  // Business methods
  public DocumentHandler execute(DocumentHandler inputDoc,
      DocumentHandler outputDoc, MediatorServices mediatorServices)
      throws RemoteException, AmsException {

    String poGroupingOption = null;
    String poDataOption = null;
    String value = null;

    setMediatorServices(mediatorServices);

    mediatorServices
        .debug("\n\n\nStructuredPOCreateMediator: in Instrument Creation process with document:");
    mediatorServices.debug(inputDoc.toString());

    // Pull values out of the input document and call various setter methods
    value = inputDoc.getAttribute("/User/userOid");
    setUserOid(value);

    value = inputDoc.getAttribute("../timeZone");
    setTimeZone(value);

    value = inputDoc.getAttribute("../locale");
    setUserLocale(value);

    value = inputDoc.getAttribute("../securityRights");
    setSecurityRights(value);

    value = inputDoc.getAttribute("../baseCurrencyCode");
    setBaseCurrency(value);

    value = inputDoc.getAttribute("../corporateOrgOid");
    setCorporateOrgOid(value);

    value = inputDoc.getAttribute("../clientBankOid");
    setClientBankOid(value);

    value = inputDoc.getAttribute("../poDataFilePath");
    setUploadFileName(value);

    value = inputDoc.getAttribute("../uploadSequenceNumber");
    setUploadSequenceNumber(value);

    value = inputDoc.getAttribute("../subsidiaryAccessOrgOid");
    if (value != null)
      setSubsidiaryAccessOrgOid(value);


      // If no upload sequence was given, it means we're grouping everything
    // (all upload sequences).  Therefore, the log sequence needs to be
    // generated.  Otherwise, we use the upload sequence of the log sequence
    if (uploadSequenceNumber == null) {
      long newSeq = ObjectIDCache.getInstance(
          TradePortalConstants.AUTOLC_SEQUENCE).generateObjectID();
      setLogSequence(String.valueOf(newSeq));
    } else {
      setLogSequence(uploadSequenceNumber);
    }

    // If we're coming from the PO Listview page, the button that was pressed will still
    // be set in the xml doc; otherwise (if it doesn't exist), we know that a PO upload
    // just occurred, in which case we should retrieve the PO data option and grouping
    // option (if one exists).
    value = inputDoc.getAttribute("/Update/ButtonPressed");
    if (value == null) {
      poDataOption = inputDoc
          .getAttribute("/PurchaseOrderUploadInfo/POUploadDataOption");
      if (poDataOption!=null&&poDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS)) {
        poGroupingOption = inputDoc.getAttribute("../POUploadGroupingOption");
        if (poGroupingOption!=null&&poGroupingOption
            .equals(TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS)) {
          setUploadSequenceNumber("");
        }
      }

      setUploadDefinitionOid(inputDoc
          .getAttributeLong("../uploadDefinitionOid"));

      poUploadInstrType = inputDoc
          .getAttribute("../POUploadInstrOption");

      if (poUploadInstrType == null) {
		  // This happens when user chooses to upload but not creating any automatic instrument.
		  // The user just want to upload PO so that it will be available to adding manually from the instrument
		  poUploadInstrType = "";
	  }

      if (poUploadInstrType.equals(TradePortalConstants.AUTO_LC_PO_UPLOAD)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_LC_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid "
                            + (( getUploadDefinitionOid() == 0) ? " is not null " : " = " + getUploadDefinitionOid())
                            + " and a_owner_org_oid in (";
      }

      if (poUploadInstrType.equals(TradePortalConstants.AUTO_ATP_PO_UPLOAD)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid "
                            + (( getUploadDefinitionOid() == 0) ? " is not null " : " = " + getUploadDefinitionOid())
                            + " and a_owner_org_oid in (";
      }
    } else {
         // If it gets in here, it means that we are processing active po from the active_po_upload queue
         // where the user placed request(s) by clicking the 'Create ATPs Using Creation Rules' or 'Create LCs Using Creation Rules'
         // button on the PO list view tabpage. The value of the button is kept the xml file stored in active_po_upload.po_upload_parameters.
         if (value.equals(TradePortalConstants.BUTTON_CREATE_LC_FROM_STRUCT_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_LC_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid is not null and a_owner_org_oid in (";
            poUploadInstrType = InstrumentType.IMPORT_DLC;
         }
         else if (value.equals(TradePortalConstants.BUTTON_CREATE_ATP_FROM_STRUCT_PO)) {
            SELECT_SQL = "select name, description, maximum_amount, pregenerated_sql, "
                            + " min_last_shipment_date, max_last_shipment_date, "
                            + " a_template_oid, a_op_bank_org_oid "
                            + "from lc_creation_rule "
                            + "where instrument_type_code = '" + TradePortalConstants.AUTO_ATP_PO_UPLOAD + "'"
                            + " and a_struct_po_upload_def_oid is not null and a_owner_org_oid in (";
            poUploadInstrType = InstrumentType.APPROVAL_TO_PAY;
         }
    }

    setLogger(StructuredPOLogger.getInstance(getCorporateOrgOid()));
    try{
        groupPOs();
    }catch (Exception e){
        getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(), TradePortalConstants.ERROR_GROUPING_POS, e.getMessage());
        String[]   substitutionValues = {e.getMessage()};
        boolean[] emptyArray = null;
        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ERROR_GROUPING_POS, substitutionValues, emptyArray);
        e.printStackTrace();

    }
    if (pOOIds.size() > 0){
        StringBuilder sql = new StringBuilder("select distinct ins.COMPLETE_INSTRUMENT_ID INS_NUM, " +
                "ins.instrument_type_code INS_TYP, tran.transaction_type_code TRAN_TYP " +
                "from purchase_order po inner join instrument ins on po.A_INSTRUMENT_OID = ins.INSTRUMENT_OID  " +
                "inner join transaction tran on ins.original_transaction_oid = tran.transaction_oid " +
                "and po.purchase_order_oid in (");
        StringBuilder sb = new StringBuilder(); 
        sb = POLineItemUtility.getAllPOLineItemOidsSql(new Vector(pOOIds),pOOIds.size(),sb,0);
        sql.append(sb);
        DocumentHandler resultSetDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, new ArrayList<Object>());
        outputDoc.addComponent("/",resultSetDoc);

    }

      pOOIds = new TreeSet<String>();
    mediatorServices.debug("Exiting StructuredPOMediator \n\n\n");

    return outputDoc;
  }

  /**
   * Builds an Input document handler containing the information required to create
   * a new instrument.
   *
   * @return com.amsinc.ecsg.util.DocumentHandler
   * @param creationRuleDoc com.amsinc.ecsg.util.DocumentHandler
   * @param benName java.lang.String
   * @param currency java.lang.String
   * @param amount java.math.BigDecimal
   * @param latestShipDt java.util.Date
   */
  private DocumentHandler buildInstrumentCreateDoc(DocumentHandler creationRuleDoc,
      String benName, String currency, BigDecimal amount,
      String latestShipDt, Vector poOids) {
    DocumentHandler instrCreateDoc = new DocumentHandler();
    String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
	javax.crypto.SecretKey secretKey = null;
	if (secretKeyString != null){
		byte[] keyBytes = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(secretKeyString);
		secretKey = new javax.crypto.spec.SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
		
		
	}
    instrCreateDoc.setAttribute("/instrumentOid", EncryptDecrypt.encryptStringUsingTripleDes(creationRuleDoc
	        .getAttribute("/A_TEMPLATE_OID"), secretKey));
    instrCreateDoc.setAttribute("/clientBankOid", getClientBankOid());

    if (poUploadInstrType.equals(TradePortalConstants.AUTO_ATP_PO_UPLOAD)) {
		instrCreateDoc.setAttribute("/instrumentType",
			InstrumentType.APPROVAL_TO_PAY);
	} else {
		instrCreateDoc.setAttribute("/instrumentType",
			InstrumentType.IMPORT_DLC);

	}

    instrCreateDoc.setAttribute("/bankBranch", creationRuleDoc
        .getAttribute("/A_OP_BANK_ORG_OID"));
    instrCreateDoc.setAttribute("/copyType", TradePortalConstants.FROM_TEMPL);
    instrCreateDoc.setAttribute("/userOid", getUserOid());
    instrCreateDoc.setAttribute("/ownerOrg", getCorporateOrgOid());
    instrCreateDoc.setAttribute("/securityRights", getSecurityRights());

    instrCreateDoc.setAttribute("/benName", benName);
    instrCreateDoc.setAttribute("/currency", currency);
    instrCreateDoc.setAttribute("/shipDate", latestShipDt);
    instrCreateDoc.setAttributeDecimal("/amount", amount);

    if(StringFunction.isBlank(userLocale)){
      userLocale = "en_US";
    }
    instrCreateDoc.setAttribute("/locale", userLocale);

    for (int i = 0; i < poOids.size(); i++) {
      instrCreateDoc.setAttribute("/POLines(" + i + ")/poLineItemOid",
          (String) poOids.elementAt(i));
    }

    getMediatorServices().debug(
        "Creation rules document is " + instrCreateDoc.toString());
    return instrCreateDoc;
  }

  /**
   * Process through the vector of po line items, creating a set of the
   * po line items oids.  While doing this increment a line count and
   * accumulate the amount from each po line item.  When the line count
   * exceeds a certain number of lines or the total amount exceeds the passed in max
   * amount, pass the set of po line item oids to the create instrument process.
   * Furthermore, the po line items are ordered by ben_name and currency.
   * Whenever the ben_name or currency is different from the preceeding
   * line, pass the set of po line items oids to the create instrument process.
   * After each call to the instrument create process, reset the po line item
   * oid set to nothing.  Continue processing through the po
   * line items until done.
   *
   * @param poItemList java.util.Vector - the po item records (document)
   * @param creationRuleDoc java.math.BigDecimal - the max amount allowed for the chunk
   * @exception com.amsinc.ecsg.frame.AmsException
   */
  private void chunkPOs(Vector poItemList, DocumentHandler creationRuleDoc)
      throws RemoteException, AmsException {

    BigDecimal poAmount;
    BigDecimal maxAmount = null;
    BigDecimal convertedMaxAmount = null;
    BigDecimal totalAmount = BigDecimal.ZERO;

    int itemCount = 0;

    String lastBenName = "";
    String lastCurrency = "";
    String lastUploadDefinitionOid = ""; //BSL IR SRUM042154839 04/24/2012 Rel 8.0 ADD
    String latestShipDt = null;

    String benName = "";
    String currency = "";
    String uploadDefinitionOid = ""; //BSL IR SRUM042154839 04/24/2012 Rel 8.0 ADD
    String oid;
    String shipDt;
    String poNum;
    String itemNum;

    String instrumentId = "";
    String lcRuleName = creationRuleDoc.getAttribute("/NAME");

    boolean startNewChunk = true;
    boolean goesInThisChunk = true;

    Vector poOids = new Vector();
    Vector poNums = new Vector();

    DocumentHandler poItemDoc = null;

    String listOfInvalidCharacters = null;
    String poText = null;
    int poListCount = 0;

    CorporateOrganization corporateOrg = (CorporateOrganization) mediatorServices
        .createServerEJB("CorporateOrganization");
    try {
      corporateOrg.getData(Integer
          .parseInt(getCorporateOrgOid()));
    } catch (InvalidObjectIdentifierException e) {
      mediatorServices.getErrorManager().issueError(
          TradePortalConstants.ERR_CAT_1,
          TradePortalConstants.ORG_NOT_EXIST);
    }
    long clientBankOid = corporateOrg.getAttributeLong("client_bank_oid");
    ClientBank clientBank = (ClientBank) mediatorServices
        .createServerEJB("ClientBank");
    try {
      clientBank.getData(clientBankOid);
    } catch (InvalidObjectIdentifierException e) {
      mediatorServices.getErrorManager().issueError(
          TradePortalConstants.ERR_CAT_1,
          TradePortalConstants.INVALID_CLIENT_BANK_ORG);
    }
    String swiftOverrideInd = clientBank
        .getAttribute("override_swift_length_ind");

    // Determine the number of POs allowed per instrument
    int maximumPOsInInstr = MAX_ITEMS_ON_PO;

    long uploadDefnOid = 0;
    uploadDefnOid = getUploadDefinitionOid();

    if (uploadDefnOid != 0) {
        // When the user clicks on the 'Create ATPs using Creation Rules' or 'Create LCs using Creation Rules',
        // we don't have information of the PO definition. So skip it here for that case

      PurchaseOrderDefinition poDef = (PurchaseOrderDefinition) mediatorServices
        .createServerEJB("PurchaseOrderDefinition", uploadDefnOid);
      String includePoText = "";

      if (includePoText.equals(TradePortalConstants.INDICATOR_YES)
          && (!swiftOverrideInd
            .equals(TradePortalConstants.INDICATOR_YES))) {
        // Get the maximum characters for the po text field
        int poTextSize = poDef.getAttributeInteger("po_text_size");
        // Determine the number of lines that po_text could take up (65 chars per line)
        int poTextLines = (int) Math.ceil(poTextSize / 65d);

        // Each PO will use up one line for its data in addition to the po_text lines, so add 1
        int linesPerPo = poTextLines + 1;

        // Figure out how many POs will fit into 94 lines
        maximumPOsInInstr = (int) Math.floor(94 / (poTextLines + 1));
      }
    }

    String value = creationRuleDoc.getAttribute("/MAXIMUM_AMOUNT");
    if (StringFunction.isNotBlank(value)) {
      maxAmount = new BigDecimal(value);
    } else {
      //BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN
      maxAmount = null;
      //BSL IR SRUM042154839 04/24/2012 Rel 8.0 END
    }

    for (int x = 0; x < poItemList.size(); x++) {
      // We will use this variable to count the number of PO line items fully processed, i.e. not skipped.
      poListCount++;

      poItemDoc = (DocumentHandler) poItemList.elementAt(x);

      // Pull off values from the document that drive the processing.
      poAmount = poItemDoc.getAttributeDecimal("/AMOUNT");
      benName = poItemDoc.getAttribute("/SELLER_NAME");
      currency = poItemDoc.getAttribute("/CURRENCY");
      uploadDefinitionOid = poItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID"); //BSL IR SRUM042154839 04/24/2012 Rel 8.0 ADD
      oid = poItemDoc.getAttribute("/PURCHASE_ORDER_OID");
      poNum = poItemDoc.getAttribute("/PURCHASE_ORDER_NUM");
      itemNum = "";//poItemDoc.getAttribute("/ITEM_NUM");
      shipDt = poItemDoc.getAttribute("/LATEST_SHIPMENT_DATE");

      // Get the po_text from the XML doc and check it for invalid Swift characters
      poText = "";

      if (StringFunction.isNotBlank(poText))
      {
        listOfInvalidCharacters = InstrumentServices.checkAutoLCForInvalidSwiftCharacters(poText);

        if (StringFunction.isNotBlank(listOfInvalidCharacters))
        {
          // Log the error message and decrement the PO list counter (not the loop counter x)
          // Since we're skipping this invalid PO line item, we do not count it towards the items fully processed.
          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_INVALID_SWIFT_CHARS_2PARM, getDisplayedPoNum(poNum, itemNum), listOfInvalidCharacters);
          poListCount--;
          continue;
        }
      }

  
        poOids = new Vector();
        poNums = new Vector();

        lastBenName = benName;
        lastCurrency = currency;
        lastUploadDefinitionOid = uploadDefinitionOid;
        latestShipDt = shipDt;

        // Add the oid to the po oid vector.
        poOids.add(oid);
        poNums.add(getDisplayedPoNum(poNum, itemNum));

        convertedMaxAmount = getConvertedMaxAmount(maxAmount,
        		getBaseCurrency(), currency, creationRuleDoc);

        itemCount = 1;
        totalAmount = poAmount;

        if (convertedMaxAmount == null ||
        		totalAmount.compareTo(convertedMaxAmount) < 0) {
            // Find POs that can be added to this group
            int subIdx = x + 1;

            while (subIdx < poItemList.size()) {
                poItemDoc = (DocumentHandler) poItemList.elementAt(subIdx);

                // Pull off values from the document that drive the processing.
                poAmount = poItemDoc.getAttributeDecimal("/AMOUNT");
                benName = poItemDoc.getAttribute("/SELLER_NAME");
                currency = poItemDoc.getAttribute("/CURRENCY");
                uploadDefinitionOid = poItemDoc.getAttribute("/A_UPLOAD_DEFINITION_OID");
                oid = poItemDoc.getAttribute("/PURCHASE_ORDER_OID");
                poNum = poItemDoc.getAttribute("/PURCHASE_ORDER_NUM");
                shipDt = poItemDoc.getAttribute("/LATEST_SHIPMENT_DATE");
                itemNum = "";

                if (!benName.equals(lastBenName) ||
                		!currency.equals(lastCurrency) ||
                		!uploadDefinitionOid.equals(lastUploadDefinitionOid) ||
                		(!TradePortalConstants.INDICATOR_YES.equals(swiftOverrideInd) &&
                				itemCount + 1 >= maximumPOsInInstr))
                {
                	// None of the remaining POs can be added to this group
                    break;
                }

                BigDecimal newTotal = totalAmount.add(poAmount);
                if (convertedMaxAmount == null ||
                		newTotal.compareTo(convertedMaxAmount) < 0) {
                	// Add this PO to the group
                    poOids.add(oid);
                    poNums.add(getDisplayedPoNum(poNum, itemNum));
                    poItemList.remove(subIdx);
                    poListCount++;
                    itemCount++;
                    totalAmount = newTotal;

                	if (shipDt != null) {
                		if (latestShipDt == null) {
                			latestShipDt = shipDt;
                		}
                		else if (shipDt.compareTo(latestShipDt) > 0) {
                			latestShipDt = shipDt;
                		}
                	}
                }
                else {
                	// This PO would bring the total above the max
                	subIdx++;
                }
            }
        }

        // Build the input document used to create a new instrument
        DocumentHandler instrCreateDoc = buildInstrumentCreateDoc(creationRuleDoc,
        		lastBenName, lastCurrency, totalAmount, latestShipDt, poOids);

        // Create instrument using the document, assign the po line
        // items, and create log records.
        try {
        	createInstrumentFromPOData(instrCreateDoc, poOids, poNums, lcRuleName);
        	pOOIds.addAll(poOids);

        	// Add a blank line spacer after log messages for this instrument
        	getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());
        }
        catch (AmsException e) {
        	// This is probably a sql error but could be anything.
        	// Post this exception to the log.
        	getLogger().addLogMessage(getCorporateOrgOid(),
        			getLogSequence(), e.getMessage());
        	e.printStackTrace();
        }
    }
    //BSL IR SRUM042154839 04/24/2012 Rel 8.0 END
  }


  /**
   * Getter for the baseCurrency attribute
   *
   * @return java.lang.String
   */
  private String getBaseCurrency() {
    return baseCurrency;
  }

  /**
   * Getter for the attribute clientBankOid
   * @return java.lang.String
   */
  private String getClientBankOid() {
    return clientBankOid;
  }

  /**
   * This method attempts to convert the given fromAmount (which is in teh given
   * currency which also happens to be the base currency) into the currency of the
   * po line item.  If the fromCurrency and toCurrency are the same, there is no
   * conversion to do.  Otherwise, we look at the currency of the po line item.
   * If it is a valid TradePortal currency (i.e., in refdata manager), we try to
   * get an fx rate using the toCurrency (the po line item currency).  If found,
   * use that information.  Otherwise get the base currency of the template
   * associated with the instrument creation rule.  If we can't find an fx rate for that
   * currency we can't convert and return a null amount.  Since the conversion we
   * are doing is FROM a base currency TO another currency (the opposite of the
   * way the fx rate information is defined, our calculation is opposite of the
   * fx rate info (e.g., we divide if the multiply_indicator is multiply).
   * <p>
   * @return java.math.BigDecimal - the resulting converted amount, null if can't
   *                                convert.
   * @param fromAmount java.math.BigDecimal - The amount to convert from
   * @param fromCurrency java.lang.String - The currency the fromAmount is in
   * @param toCurrency java.lang.String - The currency we want to convert to
   * @param creationRuleDoc DocumentHandler - The instrument creation rule record (as a doc)
   */
  private BigDecimal getConvertedMaxAmount(BigDecimal fromAmount,
      String fromCurrency, String toCurrency, DocumentHandler creationRuleDoc) {

    BigDecimal convertedAmount = null;

    try {
      //BSL IR SRUM042154839 04/24/2012 Rel 8.0 BEGIN - add null check and move before currency check
      // fromAmount may have been defaulted to 0 (to prevent a null
      // pointer).  However, we don't want to set the maxAmount value
      // to 0 since nothing would get grouped.  If the fromAmount is
      // 0, return a null (indicating maxAmount not relevant for
      // grouping).
      if ((fromAmount == null) ||
    		  fromAmount.compareTo(BigDecimal.ZERO) == 0) {
        return null;
      }

      // No need to convert if the from and to currencies are the same
      if (fromCurrency.equals(toCurrency)) {
        return fromAmount;
      }

      String templateCurrency = null;
      StringBuilder sql;
      String multiplyIndicator = null;
      BigDecimal rate = null;
      DocumentHandler fxRateDoc = null;

      ReferenceDataManager rdm = ReferenceDataManager.getRefDataMgr();

      // Test whether the po line item currency code is a valid Trade
      // Portal currency.
      if (rdm.checkCode(TradePortalConstants.CURRENCY_CODE, toCurrency,
          getUserLocale())) {
        // Get the fx rate info for the po line item currency
        fxRateDoc = getFxRateDoc(getCorporateOrgOid(), toCurrency);
        if (fxRateDoc == null) {
          // No fx rate found, isse warning and return a blank amount.
          // This indicates that maxAmount is not used for grouping.
          // Give error with the po line item currency.

          getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_NO_FX_RATE, toCurrency);
          return null;
        } else {
          // Found an fx rate for the template currency.  Use it.
        }
      } else {
        // Currency code for the po line item is not valid.  Use
        // the currency code from the instrument creation rule's template.
        sql = new StringBuilder(
            "select copy_of_currency_code from template, transaction ");
        sql
            .append("where p_instrument_oid = c_template_instr_oid and template_oid = ?");
        DocumentHandler templateDoc = null;
        try {
          templateDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, new Object[]{creationRuleDoc.getAttribute("/A_TEMPLATE_OID")});
        } catch (AmsException e) {
          templateDoc = null;
        }

        if (templateDoc == null) {
          // Can't get the currency code off the template.  Issue a warning and return
          // a null amount.  This indicates NOT to use maxAmount for grouping.

             getLogger().addLogMessage(getCorporateOrgOid(),
              getLogSequence(),
              TradePortalConstants.LCLOG_INVALID_CURRENCY,
              creationRuleDoc.getAttribute("/NAME"));
          return null;
        } else {
          // We got the template currency but it may be blank.  We're only
          // concerned with the first record (there should not be more).
          templateCurrency = templateDoc
              .getAttribute("/ResultSetRecord(0)/COPY_OF_CURRENCY_CODE");
          if (StringFunction.isBlank(templateCurrency)) {
            // Can't get the currency code off the template.  Issue a warning and return
            // a null amount.  This indicates NOT to use maxAmount for grouping.

            getLogger().addLogMessage(getCorporateOrgOid(),
                getLogSequence(),
                TradePortalConstants.LCLOG_INVALID_CURRENCY,
                creationRuleDoc.getAttribute("/NAME"));
            return null;
          } else {
            // Currency is good so attempt to get the fx rate doc.
            fxRateDoc = getFxRateDoc(getCorporateOrgOid(),
                templateCurrency);

            // Don't look for an FX rate if the template's currency is the base currency
            // No FX rate is needed in that case
            if (!templateCurrency.equals(getBaseCurrency())) {
              if (fxRateDoc == null) {
                // No fx rate found, isse warning and return a blank amount.
                // This indicates that maxAmount is not used for grouping.
                // Give error using the template currency.

                getLogger().addLogMessage(getCorporateOrgOid(),
                    getLogSequence(),
                    TradePortalConstants.LCLOG_NO_FX_RATE,
                    templateCurrency);
                return null;
              } else {
                // Found an fx rate for the template currency.  Use it.
              }
            } else {
              // Base Currency and Template currency are identical
              // No need for conversion.  Return the from amount
              return fromAmount;
            }
          } // end templateCurrency == null
        } // end templateDoc == null
      } // end checkCode

      // We now have an fx rate doc (although the values on the doc may be
      // blank.)  Attempt to convert the amount.
      multiplyIndicator = fxRateDoc.getAttribute("/MULTIPLY_INDICATOR");
      rate = fxRateDoc.getAttributeDecimal("/RATE");

      if (multiplyIndicator != null && rate != null) {
        // We're converting FROM the base currency TO the po line item
        // currency so we need to do the opposite of the multiplyIndicator
        // (the indicator is set for converting FROM some currency TO the
        // base currency)
        if (multiplyIndicator.equals(TradePortalConstants.MULTIPLY)) {
          convertedAmount = fromAmount.divide(rate,
              BigDecimal.ROUND_HALF_UP);
        } else {
          convertedAmount = fromAmount.multiply(rate);
        }
      } else {
        // We have bad data in the fx rate record.  Issue warning and return
        // null amount (indicating that maxAmount is not used for grouping)

        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_NO_FX_RATE,
            fxRateDoc.getAttribute("/CURRENCY_CODE"));
        return null;
      }
    } catch (AmsException e) {
      LOG.info("Exception found getting converted max amount:");
      LOG.info(e.toString());
      e.printStackTrace();
    }

    return convertedAmount;
  }

  /**
   * Getter for the attribute corporateOrgOid
   * @return java.lang.String
   */
  private String getCorporateOrgOid() {
    return corporateOrgOid;
  }

  /**
   * Returns the displayable form of the ponum and line number.)
   * @return java.lang.String
   * @param poNum java.lang.String
   * @param itemNum java.lang.String
   */
  private String getDisplayedPoNum(String poNum, String itemNum) {
    return poNum + "  " + itemNum;
  }

  /**
   * Uses the given corporate org oid and currency to get an fx rate record
   * (as a document handler).  The currency, rate, and multiple_indicator are
   * returned in the document.
   *
   * @return DocumentHandler - result of the query, may be null
   * @param corporateOrg java.lang.String - corporate org oid to use
   * @param currency java.lang.String - currency to use
   */
  private DocumentHandler getFxRateDoc(String corporateOrg, String currency) {

		Cache fxCache = TPCacheManager.getInstance().getCache(TradePortalConstants.FX_RATE_CACHE);
		DocumentHandler resultSet = (DocumentHandler) fxCache.get(corporateOrg + "|" + currency);

		if (resultSet != null) {
			Vector fxRateDocs = resultSet.getFragments("/ResultSetRecord");
			return (DocumentHandler) fxRateDocs.elementAt(0);
		}

		return null;

	}

  /**
	 * Uses the DatabaseQueryBean to execute sql to retrieve data from the
	 * po_line_item table which are assigned to the given transaction. the result
	 * is a vector of documents, one for each po line item row.
	 *
	 * @return java.util.Vector
	 * @param corporateOrgOid
	 *           java.lang.String
	 */
  private Vector getCreationRules(String corporateOrgOid) throws AmsException {

  	List<Object> sqlParamsLst = new ArrayList();
    StringBuffer lcSql = new StringBuffer(SELECT_SQL);
    lcSql.append(" ? ");
    sqlParamsLst.add(corporateOrgOid);

    // Loop back through all parent orgs for oids to include in search for LC Creation rules
    do {

      StringBuilder parentCorpSql = new StringBuilder();
      parentCorpSql.append("select p_parent_corp_org_oid from corporate_org ");
      parentCorpSql.append("where organization_oid = ?");
      parentCorpSql.append(" AND activation_status='" + TradePortalConstants.ACTIVE + "'");
      DocumentHandler parentCorpOrg = DatabaseQueryBean.getXmlResultSet(parentCorpSql.toString(), false,  new Object[]{corporateOrgOid});
      if( parentCorpOrg != null )
      {
        Vector resultsVector = parentCorpOrg.getFragments("/ResultSetRecord/");
        if( ((DocumentHandler)resultsVector.elementAt(0)) != null )
        {
          DocumentHandler parentOid = ((DocumentHandler)resultsVector.elementAt(0));
          String parentCorpOid = "";
          parentCorpOid = parentOid.getAttribute("/P_PARENT_CORP_ORG_OID");
          if (StringFunction.isNotBlank(parentCorpOid)) {
            lcSql.append(",").append(" ? ");
            corporateOrgOid = parentCorpOid;
            sqlParamsLst.add(corporateOrgOid);
          } else {  // No more parents, so break out of loop
            break;
          }
        } else {  // No more parents, so break out of loop
          break;
        }
      } else { // no more parents, so break out of loop
        break;
      }
    } while (true);
    // Commented the previous code out for subsidiary as we are getting all orgs in the family tree
    // If user that uploaded the POs was using subsidiary access and is allowed
    // to use their own data as well as the subsidiary's, include the parent org oid in the query
    // Loop back through all parent orgs for oids to include in search for LC Creation rules

    lcSql.append(") ");
    lcSql.append(ORDERBY_CLAUSE);

    LOG.info("LC SQL is" + lcSql.toString());

    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(lcSql.toString(), true, sqlParamsLst);

    if (resultSet == null)
      resultSet = new DocumentHandler();

    getMediatorServices().debug(
        "lc rule list doc is " + resultSet.toString());
    return resultSet.getFragments("/ResultSetRecord");

  }

    /**
     * Getter for the auto lc logger attribute
     * @return com.ams.tradeportal.busobj.util.StructuredPOLogger
     */
    private StructuredPOLogger getLogger() {
        return logger;
    }

  /**
   * Getter for the attribute logSequence
   * @return java.lang.String
   */
  private String getLogSequence() {
    return logSequence;
  }

  /**
   * Getter for the mediator services attribute.
   * @return com.amsinc.ecsg.frame.MediatorServices
   */
  private MediatorServices getMediatorServices() {
    return mediatorServices;
  }

  /**
   * Uses the DatabaseQueryBean to execute sql to retrieve data from the
   * po_line_item table which are
   *     1) unassiged,
   *     2) for the given org,
   *     3) for the given upload sequence (if non-blank)
   *     4) with a last_ship_dt between today + mindays and today + maxdays
   *        (where mindays and maxdays are from the lc rule and both are non-blank),
   *     5) meet the remaining lc creation rule criteria,
   *     6) and, were uploaded (as opposed to being manually entered)
   *
   * The result is a vector of documents, one for each po line item row.
   *
   * @return java.util.Vector
   * @param creationRuleDoc DocumentHadler - the current lc rule being considered
   */
  private Vector getPOItems(DocumentHandler creationRuleDoc) throws AmsException,
      RemoteException {

    // The Where clause for the po select will include:
    //     a_assigned_to_trans_oid is null
    //     a_owner_org_oid = x
    //     sequence = y (if y is non-blank)
    //     last_ship_dt between today+mindays and today+maxdays (if both non-blank)
    //     the pregenerated sql from the lc creation rule (criteria)
    // It is ordered by ben_name and currency (to support chunking)

    StringBuilder poSql = new StringBuilder(PO_SELECT_SQL);
    List<Object> sqlParamList = new ArrayList();

    poSql.append(" ? ");
    sqlParamList.add(getCorporateOrgOid());

    // Add condition for sequence if it is non-blank
    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
      poSql.append(" and a_po_file_upload_oid = ? ");
      sqlParamList.add(getUploadSequenceNumber());
    }

    // Add condition for last_ship_dt if both min and max are set;
    String minDays = creationRuleDoc.getAttribute("/MIN_LAST_SHIPMENT_DATE");
    String maxDays = creationRuleDoc.getAttribute("/MAX_LAST_SHIPMENT_DATE");

    if (StringFunction.isNotBlank(minDays)
        && StringFunction.isNotBlank(maxDays)) {

      // The syntax for the last_ship_dt condition will be something like:
      //  "and last_ship_dt between TO_DATE('02-JUL-01')+3 and
      // TO_DATE('02-JUL-01')+6" where today's current date (localized
      // for the user's timezone) is Jul 01, 2001
      // and min_last_shipment_date is 3 and max_last_shipment_date is 6

      // First get the current local date
      String currentGmtDate = DateTimeUtility.getGMTDateTime();
      Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(
          currentGmtDate, getTimeZone());

      SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");

      String today = formatter.format(currentLocalDate);

      poSql.append(" and latest_shipment_date between TO_DATE(?)+? and  TO_DATE('?)+? ");
      
      sqlParamList.add(today);
      sqlParamList.add(minDays);
      sqlParamList.add(today);
      sqlParamList.add(maxDays);
    }

    // Add condition for the remaining criteria from the lc creation rule
    String criteria = creationRuleDoc.getAttribute("/PREGENERATED_SQL");

    if (StringFunction.isNotBlank(criteria)) {
      poSql.append(" and ");
      poSql.append(criteria);
    }

    // Finally add the orderby clause.
    poSql.append(" ");
    poSql.append(PO_ORDERBY);

    getMediatorServices().debug("po sql is " + poSql.toString());
    DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(poSql.toString(), true, sqlParamList);

    if (resultSet == null)
      resultSet = new DocumentHandler();

    getMediatorServices().debug("po list is " + resultSet.toString());
    return resultSet.getFragments("/ResultSetRecord");

  }

  /**
   * Getter for the attribute security rights
   * @return java.lang.String
   */
  private String getSecurityRights() {
    return securityRights;
  }

  /**
   * Getter for the timeZone attribute
   * @return java.lang.String
   */
  private String getTimeZone() {
    return timeZone;
  }

  /**
   * Getter for the uploadFileName attribute
   * @return java.lang.String
   */
  private String getUploadFileName() {
    return uploadFileName;
  }

  /**
   * Getter for the attribute uploadSequenceNumber
   * @return java.lang.String
   */
  private String getUploadSequenceNumber() {
    return uploadSequenceNumber;
  }

  /**
   * Getter for the user locale attribute
   * @return java.lang.String
   */
  private String getUserLocale() {
    return userLocale;
  }

  /**
   * Getter for the attribute userOid
   * @return java.lang.String
   */
  private String getUserOid() {
    return userOid;
  }

  private long getUploadDefinitionOid() {
    return uploadDefinitionOid;
  }

  /**
   * This is the main method for handling the Group PO's logic.
   * The basic steps for this are as follows:
   * 1. For each LC Creation Rule for the given org (processed in
   *    reverse creation date order)
   * 1.1 Select all the po_line_item records that belong to that org
   *     and are unassigned.  If we're processing for only a single
   *     upload file, then add the criteria that the line items are
   *     for the given upload sequence.
   * 1.2 Begin a tally of the line item count and the total dollar
   *     amount.
   * 1.3 Get a chunk of PO Line items
   * 1.3.1  If the line item count or dollar amount is exceeded for
   *        the current lc creation rule, we're finished with this chunk
   * 1.3.2  If the ben_name is different, finish the chunk.
   * 1.3.3  If the currency is differenet, finish the chunk.
   * 1.4 For a PO Line item chunk, create an LC, passing in the set
   *     of oids from the chunk.  This causes an LC to be created,
   *     the line items assigned to this LC, several LC fields to be
   *     derived, and the goods description to be built,
   *
   */
  private void groupPOs() throws AmsException, RemoteException {

    DocumentHandler creationRuleDoc;
    BigDecimal maxAmount = null;
    String name;

    Vector lcRulesList = getCreationRules(getCorporateOrgOid());
    Vector poItemList;
    int count=0; // DK IR - RAUM070159323 rel8.1.1 09/14/2012

    Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(
        DateTimeUtility.getGMTDateTime(), getTimeZone());

    String localDate = TPDateTimeUtility.formatDateTime(currentLocalDate,
        getUserLocale());

    if (lcRulesList.size() < 1) {
      // There are no LC creation rules to process.  Post a message
      getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
          TradePortalConstants.LCLOG_NO_LC_RULES);

      return;
    }

    for (int i = 0; i < lcRulesList.size(); i++) {
      // Get the lc creation rule and pull some values off it.
      creationRuleDoc = (DocumentHandler) lcRulesList.elementAt(i);

      String parms2[] = { creationRuleDoc.getAttribute("/NAME") };

      getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

      getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
                TradePortalConstants.LCLOG_PROCESSING_LC_RULE, parms2);

      // Get the po items for this lc creation rule
      poItemList = getPOItems(creationRuleDoc);

      // (Note, because the error manager wraps message paramters
      // within single quotes, we can't reuse parms2[] above here
      // because its entries are now wrapped.  If we reused it,
      // the parms would get wrapped again.
      String parms3[] = { creationRuleDoc.getAttribute("/NAME") };

      if (poItemList.size() < 1) {

        getLogger()
            .addLogMessage(
                getCorporateOrgOid(),
                getLogSequence(),
                TradePortalConstants.LCLOG_NO_ITEMS_FOR_LC_RULE,
                parms3);

      } else {
        // Now start chunking to po line items.
        chunkPOs(poItemList, creationRuleDoc);
        count++; // DK IR - RAUM070159323 rel8.1.1 09/14/2012
      }
    }
 // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Begins
    if(count==0){        
        mediatorServices.getErrorManager().issueError(
  	          TradePortalConstants.ERR_CAT_1,
  	          TradePortalConstants.LCLOG_NO_ITEMS_FOR_LC_RULE);
    }
 // DK IR - RAUM070159323 rel8.1.1 09/14/2012 Ends

    // All finished processing through the po line items (for either a
    // single upload sequence or all of them.  Now print an informational
    // message indicating how many are still unassigned.

    logUnassignedCount(TradePortalConstants.GROUP_PO_LINE_ITEMS);

    currentLocalDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility
        .getGMTDateTime(), getTimeZone());
    localDate = TPDateTimeUtility.formatDateTime(currentLocalDate,
        getUserLocale());
    String[] parms2 = { localDate };

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
              TradePortalConstants.LCLOG_CREATE_LC_STOP, parms2);

  }

  /**
   * This methods posts one or two messages to the log.  The first optional
   * message indicates how many po line items for a specific upload sequence were
   * not assigned.  This message is applicable only when the user is processing
   * an upload file.  The second message always prints and indicates how many
   * po line items (in total) are still unassigned.
   */
  private void logUnassignedCount(String poDataOption) throws AmsException,
      RemoteException {
    int count = 0;
    String whereClause = " a_transaction_oid is NULL and a_owner_org_oid = ? ";

    // If we're processing for a specific upload sequence, print a message
    // for how many items are unassigned from that sequence.
    if (StringFunction.isNotBlank(getUploadSequenceNumber())) {
      StringBuilder whereBySequenceSql = new StringBuilder(whereClause);
      whereBySequenceSql.append(" and a_po_file_upload_oid = ?");

      count = DatabaseQueryBean.getCount("purchase_order_oid", "PURCHASE_ORDER", whereBySequenceSql.toString(), true, new Object[]{getCorporateOrgOid(),getUploadSequenceNumber()});



      if (TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS
          .equals(poDataOption)) {

        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_UNASSIGNED_ITEMS_UPLOADED,
            String.valueOf(count));

      } else {

        getLogger().addLogMessage(getCorporateOrgOid(),
            getLogSequence(),
            TradePortalConstants.LCLOG_UPLOAD_ITEMS_UNASSIGNED,
            String.valueOf(count), getUploadFileName());
      }
    }

    // In either event, we always get the total count of all unassigned
    // po line items.
    count = DatabaseQueryBean.getCount("PURCHASE_ORDER_OID", "PURCHASE_ORDER", whereClause, true, new Object[]{getCorporateOrgOid()});

    getLogger().addBlankLine(getCorporateOrgOid(), getLogSequence());

    getLogger().addLogMessage(getCorporateOrgOid(), getLogSequence(),
        TradePortalConstants.LCLOG_TOTAL_ITEMS_UNASSIGNED,
        String.valueOf(count));

  }

  /**
   * Setter for the base currency attribute
   *
   * @param newBaseCurrency java.lang.String
   */
  private void setBaseCurrency(String newBaseCurrency) {
    baseCurrency = newBaseCurrency;
  }

  /**
   * Setter for the clientBankOid attribute
   * @param newClientBankOid java.lang.String
   */
  private void setClientBankOid(String newClientBankOid) {
    clientBankOid = newClientBankOid;
  }

  /**
   * Setter for the corporateOrgOid attribute
   * @param newCorporateOrgOid java.lang.String
   */
  private void setCorporateOrgOid(String newCorporateOrgOid) {
    corporateOrgOid = newCorporateOrgOid;
  }

  /**
   * Setter for the auto upload log attribute
   * @param newLogger com.ams.tradeportal.busobj.util.StructuredPOLogger
   */
  private void setLogger(StructuredPOLogger newLogger) {
      logger = newLogger;

  }


  /**
   * Setter for the logSequence attribute
   * @param newLogSequence java.lang.String
   */
  private void setLogSequence(String newLogSequence) {
    logSequence = newLogSequence;
  }

  /**
   * Setter for the mediator services attribute
   * @param newMediatorServices com.amsinc.ecsg.frame.MediatorServices
   */
  private void setMediatorServices(MediatorServices newMediatorServices) {
    mediatorServices = newMediatorServices;
  }

  /**
   * Setter for the security rights attribute
   * @param newSecurityRights java.lang.String
   */
  private void setSecurityRights(String newSecurityRights) {
    securityRights = newSecurityRights;
  }

  /**
   * Setter for the timeZone attribute
   * @param newTimeZone java.lang.String
   */
  private void setTimeZone(String newTimeZone) {
    timeZone = newTimeZone;
  }

  /**
   * Setter for the upload definition oid attribute
   * @param newUploadDefinitionOid long
   */
  private void setUploadDefinitionOid(long newUploadDefinitionOid) {
    uploadDefinitionOid = newUploadDefinitionOid;
  }

  /**
   * Setter for the upload file name attribute
   * @param newUploadFileName java.lang.String
   */
  private void setUploadFileName(String newUploadFileName) {
    uploadFileName = newUploadFileName;
  }

  /**
   * Setter for the uploadSequenceNumber attribute
   * @param newUploadSequenceNumber java.lang.String
   */
  private void setUploadSequenceNumber(
      String newUploadSequenceNumber) {
    uploadSequenceNumber = newUploadSequenceNumber;
  }

  /**
   * Setter for the user locale
   * @param newUserLocale java.lang.String
   */
  private void setUserLocale(String newUserLocale) {
    userLocale = newUserLocale;
  }

  /**
   * Setter for the userOid attribute
   * @param newUserOid java.lang.String
   */
  private void setUserOid(String newUserOid) {
    userOid = newUserOid;
  }

  /**
   * Setter for the subsidiaryAccessOrgOid attribute
   * @param subsidiaryAccessOrgOid java.lang.String
   */
  private void setSubsidiaryAccessOrgOid(String subsidiaryAccessOrgOid) {
    this.subsidiaryAccessOrgOid = subsidiaryAccessOrgOid;
  }


  /**
   * createInstrumentFromPOData()
   *
   * This method will call the createNew() method on instrument to create a new ImportLC/ATP/etc.
   * It will then set certain fields on the LC based on what data is in the grouped PO's.
   *
   * INPUTDOC - The input doc must contain the following
   *    /instrumentOid ---------- The existing instruments oid or "0" for a new instr.
   *    /transactionType -------- The type of the transaction being created
   *    /bankBranch ------------- The oid of the Operational Bank Org
   *    /clientBankOid ---------- The oid of the client bank that owns the corporation
   *    /userOid ---------------- The oid of the user creating the doc
   *    /securityRights --------- The security rights of the user that is creating the transaction
   *    /copyType --------------- What we copy a new instrument from (TP CONSTANTS: FROM_INSTR, FROM_TEMPL, & FROM_BLANK)
   *    /ownerOrg --------------- The oid of the user's corporate organization
   *    /benName ---------------- The name of the beneficiary of the PO Line Items
   *    /amount ----------------- The sum of the amounts of the PO Line Items
   *    /currency --------------- The currency of the amounts of the PO Line Items
   *    /shipDate --------------- The last shipment date of the amounts of the PO Line Items
   *
   * OUTPUTDOC - The output doc will contain the following
   *    /transactionOid --------- The oid of the issue tranasction for the new ImportLC/ATP/etc.
   *
   * @param inputDoc - described above
   * @param poOids Vector - list of po line item oids going in this instrument
   * @param poNums Vector - list of po num/line items going in this instrument
   * @param lcRuleName String - name of the lc creation rule causing this instrument creation
   * @return DocumentHandler outputDoc - described above
   */

  private DocumentHandler createInstrumentFromPOData(DocumentHandler inputDoc,
      Vector poOids, Vector poNums, String lcRuleName)
      throws AmsException, RemoteException {
    MediatorServices mediatorServices = getMediatorServices();
    mediatorServices
        .debug("Entered createInstrumentFromPOData(). inputDoc follows ->\n"
            + inputDoc.toString());

    DocumentHandler outputDoc = null;

    //Create a new instrument
    Instrument newInstr = (Instrument) mediatorServices
        .createServerEJB("Instrument");
    outputDoc = newInstr.createLCFromStructuredPOData(inputDoc, poOids, poNums,
        lcRuleName);
    mediatorServices
        .debug("Returned to StructuredPOCreateMediator.createInstrumentFromPOData() from instrument.createInstrumentFromPOData()\n"
            + "Returned document follows -> "
            + outputDoc.toString());
    try{
    // Now that the transaction has been created, assign this transaction
    // oid to all the line items on the transaction.
        POLineItemUtility.updateStructuredPOTransAssignment(poOids, outputDoc
            .getAttribute("/transactionOid"), newInstr
            .getAttribute("instrument_oid"), outputDoc
            .getAttribute("/shipmentOid"), TradePortalConstants.PO_STATUS_ASSIGNED);
        POLineItemUtility.createHistoryLog (poOids,TradePortalConstants.PO_ACTION_AUTOCREATE,TradePortalConstants.PO_STATUS_ASSIGNED,getUserOid());
    } catch (AmsException e){
        String [] substitutionValues = {"",""};
        substitutionValues[0] = e.getMessage();
        substitutionValues[1] = "0";
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                 AmsConstants.SQL_ERROR, substitutionValues);
         e.printStackTrace();
    }
    // Finally, create log records for each po assigned to the transaction
    String instrumentId = newInstr.getAttribute("complete_instrument_id");
    POLineItemUtility.logStructuredPOAssignment(instrumentId, lcRuleName, poNums,
        getCorporateOrgOid(), getLogSequence());

    return outputDoc;
  }



}