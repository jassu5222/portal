package com.ams.tradeportal.mediator;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface StructuredPOCreateMediatorHome extends EJBHome
{

  public StructuredPOCreateMediator create()
		throws CreateException, RemoteException;}