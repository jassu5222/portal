package com.ams.tradeportal.mediator;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.ejb.RemoveException;
import javax.ejb.SessionContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.agents.TradePortalAgent;
import com.ams.tradeportal.busobj.ActiveInstrumentParty;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.Fee;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PaymentBenEmailQueue;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.ConfigSettingManager;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.common.UniversalMessage;
import com.ams.tradeportal.mediator.util.UnPackagerServices;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ComponentList;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.InstrumentLockException;
import com.amsinc.ecsg.frame.LockingManager;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright   2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TPLUnPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(TPLUnPackagerMediatorBean.class);

	/*
	 * Manages TPL Message UnPackaging..
	 */
	//UpdateParent is a boolean added to the mediator to determine
	//if we need to perform an update to the position of the parent/related instrument
	transient boolean updateParent;
	//Hashtable to preserve transactional integrity
	//All values in parentTransaction will be used to set the related instrument's position
	transient Hashtable parentTransaction ;
	// Stores all of the transaction oids which require an email to be sent
	Vector transactionEmailList = null;

	// Vector of transaction oids.  If successful saves for these transactions and if other
	// conditions are met, a message is placed in the outgoing queue table
	Vector outgoingGlobalTradeMessagesList = null;

	boolean isLatestTransaction = true;
	boolean isEarliestTransaction = false;
	boolean hasTransactionExisted = false;
	boolean accountBalanceUpdated = false;
	boolean resubmitMsg = false;

	public DocumentHandler execute(DocumentHandler unpackagerDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		boolean hasInstrumentExisted            = false;
		boolean unpackage = true;
		boolean revalue   = false;
		updateParent = false;
		parentTransaction = new Hashtable();
		transactionEmailList = new Vector();
		outgoingGlobalTradeMessagesList = new Vector(1);
		int instrumentSave = 0;
		isLatestTransaction = true;
		isEarliestTransaction = false;
		accountBalanceUpdated = false;


		SessionContext mContext = mediatorServices.mContext;

		/*  Core Business Objects instantiation: */
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_BANK);   	// NSX CR-574 09/21/10
		Instrument instrument = (Instrument)mediatorServices.createServerEJB("Instrument");
		Transaction transaction = null;

		long toid = 0;
		String completeInstrumentID = null;
		String corpOrgProponixCustID = null;
		String transactionType = null;


		try{
			if(unpackagerDoc!= null){
				LOG.info("Inside execute() unpackagerDoc="+unpackagerDoc.toString());
			}
			String agentId=unpackagerDoc.getAttribute("/AgentID");
			String messageType = unpackagerDoc.getAttribute(UniversalMessage.headerPath+"/MessageType");
			//Leelavathi PPX-208 29/4/2011 Begin
			if(messageType.equals(MessageType.TPL)||messageType.equals(MessageType.TPLH))
			{
				//Leelavathi PPX-208 29/4/2011 End
				/*
				 First resolve relationships among business objects then update/create them.
				 */
				transactionType          = unpackagerDoc.getAttribute(UniversalMessage.transactionPath+"/TransactionTypeCode");
				completeInstrumentID   = unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentID");
				boolean newCancelledTransaction = false;
				boolean rejectedTransaction = false;
				if(completeInstrumentID == null ||completeInstrumentID.equals("") )
				{
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"InstrumentID", UniversalMessage.instrumentPath+"/InstrumentID");
				}
				corpOrgProponixCustID = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
				if(corpOrgProponixCustID==null || corpOrgProponixCustID.equals(""))
				{
					mediatorServices.debug("No corpOrg to update");
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"CustomerID", UniversalMessage.subHeaderPath+"/CustomerID");
				}
				
			
				String instrumentOid = getRelatedInstrument(completeInstrumentID,corpOrgProponixCustID,mediatorServices);
				String transactionOid = null;
				int transactionIndex = 0;
				//PPX-208 - Ravindra B - 03/18/2011 - Start.
				if(instrumentOid != null)
				{
					try{

						String messageId = unpackagerDoc.getAttribute(UniversalMessage.headerPath+"/MessageID");
						if(agentId==null){
							agentId= getAgentID(messageId);
						}
						LockingManager.lockBusinessObject(Long.parseLong(instrumentOid),
								agentId , true);
						mediatorServices.debug("TPLUnPackagerMediator: Locking instrument "
						    + instrumentOid + "successful");
				    }
				    catch (InstrumentLockException e){
				    	//Instrument is Previously locked.
				    	outputDoc.setAttribute("/Param/ResubmitMsg","Y");
						return outputDoc;
				    }
				}

			    try{
			    //PPX-208 - Ravindra B - 03/18/2011 - End.
				revalue = transactionType.equals(TransactionType.REVALUE) || transactionType.equals(TransactionType.DEAL_CHANGE);
				if(instrumentOid!= null && revalue)
				{
					instrument.getData(Long.parseLong(instrumentOid));
					String activeTransactionOid = instrument.getAttribute("active_transaction_oid");
					if(activeTransactionOid==null) activeTransactionOid = "";
					if(activeTransactionOid.equals(""))
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Active Transaction to perform REVALUE or DEAL_CHANGE");
					}
					else
					{	//PPX-208 - Adarsha - 03/10/2011 - Covered unpackagePosition in If block.
						if(!isOutOfOrderMessage(unpackagerDoc, instrumentOid)){
						Transaction activeTransaction = (Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOid));
						unpackagePosition(activeTransaction,instrument,unpackagerDoc,0);
						mediatorServices.debug("Debug 2");
						instrumentSave = instrument.save();
						}
					}
					return outputDoc;
				}



				if(instrumentOid!= null && !revalue)
				{
					hasInstrumentExisted =  true;

					instrument.getData(Long.parseLong(instrumentOid));
					transactionOid = getRelatedTransaction(unpackagerDoc,instrument,mContext,transactionIndex,mediatorServices);
					String transactionStatus = unpackagerDoc.getAttribute(UniversalMessage.transactionPath+ "/TransactionStatus");
					if(transactionOid!= null)
					{
						hasTransactionExisted = true;
						transaction=(Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(transactionOid));
						unpackage = true;

						// If the transaction is rejected by bank, only update the transaction status
						// and rejection reason text.  The message does not have position data etc.  Do not
						// update those.
						String rejectReason = unpackagerDoc.getAttribute(UniversalMessage.transactionPath+ "/RejectReason");
						String instrumentType = instrument.getAttribute("instrument_type_code");
						if((rejectReason != null)
								&& (rejectReason.trim().length() > 0))
						{
							String origTransactionStatus =  transaction.getAttribute("transaction_status");
							transaction.setAttribute("transaction_status", TransactionStatus.REJECTED_BY_BANK);
							transaction.setAttribute("rejection_reason_text", rejectReason);
							
							
							String indexPrefix[] = {"first","second"};
							for (int authIndex = 0; authIndex < 2; authIndex++){
								transaction.setAttribute( indexPrefix[authIndex] +"_authorizing_user_oid", "");
								transaction.setAttribute( indexPrefix[authIndex] + "_authorize_status_date", "");
							}

							String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'TRANSACTION' and p_owner_oid=?";
							DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new Object[]{transactionOid});
							// Nar release 8.3.0.0 CR 857  27 June 2013 End
							if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))){
									sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'DMST_PMT_PANEL_AUTH_RANGE' and p_owner_oid " +
											"in ( select dmst_pmt_panel_auth_range_oid from dmst_pmt_panel_auth_range where p_transaction_oid = ?)";
									
									Object ob = new Object();
                                    ob = (transactionOid !=null)?Long.parseLong(transactionOid):transactionOid;
									DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new Object[]{ob});
							}
							
							Terms terms = (Terms)transaction.getComponentHandle("CustomerEnteredTerms");
							String workItemNumber = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms/TermsDetails/WorkItemNumber");
							terms.setAttribute("work_item_number", workItemNumber);
							//IValavala IR#AYUG120848966. set term's import indicator
							terms.setImportIndicator(instrument.getAttribute("import_indicator"));
							//instrumentSave = instrument.save(); -- rbhaduri - 16th Aug 06 - IR KNUG061942857
							//Placed it below after email notification
							unpackage = false;
							rejectedTransaction = true;

							// rbhaduri - 16th Aug 06 - IR KNUG061942857 - Add Begin - Send email notification for portal rejection
							// Once we are done unpackaging and the instrument has saved, create an email notification
							// The call to this method is placed here because of Portal Rejection CR. The same code is placed below
							// but since we are returning from here for the portal rejection, this needs to be called here as well.
							// Call method which determines (1) if this transaction should be displayed on the notificiations tab
							// and (2) if an email should be sent for this notification
							boolean displayNotification = triggerEmailForNotification(instrument, transaction, unpackagerDoc, mediatorServices);
							if (displayNotification)
							{
								transaction.setAttribute("show_on_notifications_tab", TradePortalConstants.SHOW_ON_NOTIFICATIONS_TAB_Y);
								transaction.setAttribute("notification_date", DateTimeUtility.getGMTDateTime());
								transaction.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);
								// Also send messages to @GlobalTrade if appropriate
								createGlobalTradeMessagesForTransaction(instrument, mediatorServices);
								// Send an email notification
								/* Rel 9.5 - CR 1061 - IR T36000047276
								 * call to this email method is not taking the updated transaction status from unpackager doc.
								 * Need to place this method after instrument save.  So moving the method below after save.
								 *  */
								//createEmailForTransaction(instrument, mediatorServices);
							}
							else
							{
								transaction.setAttribute("show_on_notifications_tab", "N");
							}

							//IValavala CR457. Update AccountBalances
							//IValavala 02/20/09 UpdateBalance for CM Instruments only.

							if(instrumentType.equals(InstrumentType.DOM_PMT) ||
									instrumentType.equals(InstrumentType.FUNDS_XFER) ||
									instrumentType.equals(InstrumentType.XFER_BET_ACCTS)){
								boolean resubmit = UnPackagerServices.
								    updateLimits(instrument, origTransactionStatus,
								            transaction, true,
								            transaction.getAttributeDateTime("transaction_status_date"),
								            mediatorServices);
								if (resubmit) {
								    resubmitMsg = true;
								}
								//Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
								//IAZ 06/15/09 End
								accountBalanceUpdated = true;
							}
							
							// Nar IR-T36000044485 Rel9350 12/17/2015 Add - Begin
							// Add below code to unpackage image doc. it is only applicable for BTMU when customer is not integrated with TPS.
							// putting this restriction because it should not impact other bank.
							if (StringFunction.isNotBlank(corpOrgProponixCustID) ) {						
								  String sqlStatement = " SELECT cust_is_not_intg_tps FROM corporate_org WHERE proponix_customer_id = ? AND activation_status = ? ";
								  DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{corpOrgProponixCustID, TradePortalConstants.ACTIVE});
								  if (resultXML != null) {
								    String custNotIntegratedWithTPS = resultXML.getAttribute("/ResultSetRecord(0)/CUST_IS_NOT_INTG_TPS");
								    if ( TradePortalConstants.INDICATOR_YES.equals(custNotIntegratedWithTPS) ) {
								       unpackageDocumentImage(transactionIndex, transaction, instrument, unpackagerDoc, mediatorServices);
								    }
								}
							}
							//SAVE The Instrument
							instrumentSave = instrument.save();
							// rbhaduri - 16th Aug 06 - IR KNUG061942857 - Add End
							
							//Rel 9.5 - CR 1061 - IR T36000047276
							//Call email trigger after instrument save.
							//This will help to get the updated transaction status.
							if (displayNotification){
								createEmailForTransaction(instrument, mediatorServices);
							}							
						} 
						else
						{
							if(transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK)){
								if(instrumentType.equals(InstrumentType.DOM_PMT) ||
										instrumentType.equals(InstrumentType.FUNDS_XFER) ||
										instrumentType.equals(InstrumentType.XFER_BET_ACCTS)){
									String origTransactionStatus = transaction.getAttribute("transaction_status");
									boolean resubmit = UnPackagerServices.
									    updateLimits(instrument, origTransactionStatus,
						                    transaction, true,
						                    transaction.getAttributeDateTime("transaction_status_date"),
						                    mediatorServices);
                                    if (resubmit) {
                                        resubmitMsg = true;
                                    }
                                   accountBalanceUpdated = true;
								}
							}
						}
					}
					else
					{

						if(transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
						{
							mediatorServices.debug("No need to update this transaction as it Cancellend By Bank and doesn't exist on TradePortal");
							unpackage = false;
							newCancelledTransaction = true;
							//IValavala CR457. Update AccountBalances
//							IR - PUJ031770622 - 03/19/2009 - ADD BEGIN
							//(After confirming with BAs) We should not update Pending Balances as well as Daily Limits if the instrument is initiated
							//in OTL and doesn't exist in TradePortal. Hence commenting out below two lines and moving them up.
//							IR - PUJ031770622 - 03/19/2009 - ADD END
						}//End of if (If transactionStatus is CANCELLED_BY_BANK and transaction doesn't already exist on the Portal)
						else
						{
							toid = instrument.newComponent("TransactionList");
							transaction = (Transaction)instrument.getComponentHandle("TransactionList",toid);
							unpackage = true;
						}
					}
				}
				if(hasInstrumentExisted==false)
				{
					unpackage = true;
					instrument.newObject();
					toid = instrument.newComponent("TransactionList");
					transaction = (Transaction)instrument.getComponentHandle("TransactionList",toid);
				}
				boolean success = false;
				if(unpackage==false)
				{
					if(!(newCancelledTransaction || rejectedTransaction)) {
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.UNPACKAGING_MISMATCH_DATA);
					}					
					return outputDoc;
				}

				// WWUH100253221 BEGIN
				// Check whether this transaction is the latest transaction or ealiest transaciton.
				// The transaction could come in wrong order.  We use the TransactionStatusDate, which is pretty much the release datetime,
				// to determine the order of the transaction.
				if (instrumentOid != null) {
					String checkTransactionOrder = null;
					// Add a configuration parameter TPLUnpackagerChecksTransactionOrder in TradePortal.properties
					// to determine whether to check the transaction order.  This allows to turn off the feature easily
					// in production in case it causes problems.

					try {
						PropertyResourceBundle properties =
							(PropertyResourceBundle)ResourceBundle.getBundle("TradePortal");
						checkTransactionOrder = properties.getString("TPLUnpackagerChecksTransactionOrder").trim();
					}
					catch (java.util.MissingResourceException e){
					}

					if (checkTransactionOrder == null || !checkTransactionOrder.equals("N")) {
						String transactionPath = UniversalMessage.instrumentTransactionPath + "(0)/Transaction";
						String sequenceDate = unpackagerDoc.getAttribute(transactionPath+"/SequenceDate");
						if( sequenceDate != null && sequenceDate.trim().length() > 0 ) {
							isLatestTransaction = !isOutOfOrderMessage(unpackagerDoc, instrumentOid);

							String whereClause = "p_instrument_oid = ?"
							//W Zhu 11/29/2011 Rel8.0 SRUL101357224 Check "<=" instead of "<" below for SEQUENCE_DATE.  CHG activity
							// could send both STAT & TPL messages. STAT would update the sequence_date of thge original
							// transaction.  So TPL message needs to consider that as a valid original transaction even though
							// it has the same sequence_date.
							+ " AND (SEQUENCE_DATE <= to_date(?, 'mm/dd/yyyy HH24:MI:SS') OR SEQUENCE_DATE is null)"
							+ " AND TRANSACTION_STATUS = 'PROCESSED_BY_BANK'";
							Object intOb = new Object();
							intOb = (instrumentOid !=null)?Long.parseLong(instrumentOid):instrumentOid;
							int numberOfEarlierTransaction = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, true, new Object[]{intOb, sequenceDate});
							isEarliestTransaction = (numberOfEarlierTransaction == 0);
						}
					}
				}
				// WWUH100253221 END

				if(unpackage==true)
				{
					String totalNumber = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/TotalNumberOfEntries");
					if(totalNumber == null) totalNumber = "";
					if(totalNumber.equals("")) totalNumber = "0";
					int totalNumberOfTransactions =Integer.parseInt(totalNumber);
					if(totalNumberOfTransactions == 0 )
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NO_TRANSACTION_TO_UPDATE);
						return outputDoc;
					}
					if(totalNumberOfTransactions >1)
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NUMBER_OF_ENTRIES_MORE,"TPL Message","1");
						return outputDoc;
					}

					unpackageInstrumentAttributes(instrument,unpackagerDoc,hasInstrumentExisted,mediatorServices,outputDoc);

					success = unpackageInstrumentTransaction(instrument,transaction,unpackagerDoc,transactionIndex,hasInstrumentExisted,mediatorServices,outputDoc);
					//IValavala CR 457. Check resubmitMsg
					if (resubmitMsg){
						outputDoc.setAttribute("/Param/ResubmitMsg","Y");
						return outputDoc;
					}
					//Make sure to reset resubmitInd
					else {
						String resubmitInd = outputDoc.getAttribute("/Out/Param/ResubmitMsg");
						if (resubmitInd!= null && resubmitInd.equals("Y")) outputDoc.setAttribute("/Param/ResubmitMsg","N");
					}
					if(success==true)
					{
						mediatorServices.debug("Now trying to save the instrument");
						//We check the boolean updateParent to see if we need to change the position of the parent/related instrument
						if(updateParent)
						{
							//We call the method updateParent in the instrument so that both the update of the instrument
							//and the update of its parent will be in one transaction.
							//we pass it a hashtable of attribute name, attribute value pairs to update its active
							//transaction with
							//PPX-208 - Adarsha - 03/10/2011 - Covered insturment.updateParetn in If block.
							if(isLatestTransaction){
								instrument.updateParent(parentTransaction);
							}
						}
						else
							instrumentSave = instrument.save();
						mediatorServices.debug("This is the transactionStatusDate after the save :" + transaction.getAttribute("transaction_status_date"));

						//Krishna Code compare with 3.6.0.2-applying to 4.0  12/11/2007 Add Begin
						/**Extract FinanceLinks */

						if(unpackagerDoc.getFragment(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/LinkedFinances") != null)
						{
							String corporateOrgId = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
							createFinanceLinks(unpackagerDoc, mediatorServices, instrument.getAttribute("instrument_oid"),corporateOrgId,transactionIndex );

						}
						//Krishna Code compare with 3.6.0.2 - applying to 4.0 12/11/2007 Add End
						//VS CR 634 Begin Rel 6.1.0.0
						/**Extract PrePaymentLinks */

						if(unpackagerDoc.getFragment(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/LinkedPrePayment") != null)
						{
							String corpOrgId = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
							createPrePaymentLinks(unpackagerDoc, mediatorServices, instrument.getAttribute("instrument_oid"),corpOrgId,transactionIndex );

						}
						//VS CR 634 End Rel 6.1.0.0

					}

					if(success==false)
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"TPL Message Unpackaging");
						return outputDoc;
					}
				}

				//Krishna Code compare with 3.6.0.2 - applying to 4.0  12/11/2007 Add Begin
				// W Zhu 1/15/07 KLUH011440691
				// If the children related instruments were uploaded earlier than this
				// instrument is uploaded, they have not been able
				// to get related_instrument_oid set yet at that time.  Need to update them
				// with the correct related_instrument_oid.
				if (! hasInstrumentExisted) {			
					updateRelatedChildrenInstruments(instrument.getAttribute("instrument_oid"), instrument.getAttribute("complete_instrument_id"), corpOrgProponixCustID);
					// W Zhu 1/9/2009 SLUJ010768298 update mail messages that may have been uploaded before this instrument.
					updateMailMessages(instrument.getAttribute("instrument_oid"), instrument.getAttribute("complete_instrument_id"), corpOrgProponixCustID);
				}

				// CJR 04/13/2011 CR-593 Begin
				if(transaction.isH2HSentPayment()){
					String instrumentstatus = instrument.getAttribute("instrument_status");
                	if ( TradePortalConstants.INSTR_STATUS_CLOSED.equals( instrumentstatus ) ||
                			TradePortalConstants.INSTR_STATUS_REJECTED_PAYMENT.equals( instrumentstatus ) ||
                			TradePortalConstants.INSTR_STATUS_PARTIALLY_REJECTED_PAYMENT.equals( instrumentstatus ) ) {
                		String date_created = DateTimeUtility.getGMTDateTime(false);
                		String messageId = InstrumentServices.getNewMessageID();
                		OutgoingInterfaceQueue oiQueue = (OutgoingInterfaceQueue) EJBObjectFactory.createServerEJB(getSessionContext(),"OutgoingInterfaceQueue");
                		oiQueue.newObject();
                		oiQueue.setAttribute("date_created", date_created);
                		oiQueue.setAttribute("status", TradePortalAgent.STATUS_START);
                		oiQueue.setAttribute("msg_type", "PAYCOMP");
                		oiQueue.setAttribute("instrument_oid", instrumentOid);
                		oiQueue.setAttribute("transaction_oid", transactionOid);
                		oiQueue.setAttribute("message_id", messageId);
                		oiQueue.save();
                	}
                }
				
			    }//try end
			    finally{
			    	if(instrumentOid != null){
				    	try {
			                LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(instrumentOid), true);
			            } catch (InstrumentLockException e) {
			                // this should never happen
			                e.printStackTrace();
			            }
			    	}
			    }
			  //PPX208- 03/14/2011 -Adarsha - End
			}
			if(messageType.equals(MessageType.HISTORY))
			{
				completeInstrumentID   = unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentID");
				corpOrgProponixCustID = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");

				String historyinstrumentOid = getRelatedInstrument(completeInstrumentID,corpOrgProponixCustID,mediatorServices);
				int totalNumberOfTransactions = 0;
				String stotalNumberOfTransactions = null;

				if(historyinstrumentOid == null || historyinstrumentOid.equals(""))
				{
					hasInstrumentExisted = false;
					instrument.newObject();
					totalNumberOfTransactions = 0;
					stotalNumberOfTransactions = (unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/TotalNumberOfEntries"));
					if(stotalNumberOfTransactions==null)
					{
						totalNumberOfTransactions = 0;
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NO_TRANSACTION_TO_UPDATE);
					}
					else
						totalNumberOfTransactions = Integer.parseInt(stotalNumberOfTransactions);


					boolean success = true;
					mediatorServices.debug ("Total Number of Transactions for this instrument " + totalNumberOfTransactions);
					unpackageInstrumentAttributes(instrument,unpackagerDoc,hasInstrumentExisted,mediatorServices,outputDoc);
					for(int transactionIndex = 0;transactionIndex < totalNumberOfTransactions ;transactionIndex++)
					{
						transactionType = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionTypeCode");
						revalue = transactionType.equals(TransactionType.REVALUE) || transactionType.equals(TransactionType.DEAL_CHANGE);

						if(revalue)
						{
							String activeTransactionOid = instrument.getAttribute("active_transaction_oid");
							if(activeTransactionOid==null) activeTransactionOid = "";
							if(activeTransactionOid.equals(""))
							{
								instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Active Transaction to perform REVALUE or DEAL_CHANGE");
							}
							else
							{
								//PPX-208 - Adarsha - 03/10/2011 - Covered unpackagePosition in If block.
								if(isLatestTransaction){
									Transaction activeTransaction = (Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOid));
									unpackagePosition(activeTransaction,instrument,unpackagerDoc,transactionIndex);
								}
							}
						}
						else
						{
							String transactionStatusXML = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionStatus");

							if(transactionStatusXML.equals(TransactionStatus.CANCELLED_BY_BANK))
							{
								mediatorServices.debug("No need to update this transaction as it Cancellend By Bank and doesn't exist on TradePortal");
							}//End of if (If transactionStatus is CANCELLED_BY_BANK and transaction doesn't already exist on the Portal)
							else
							{
								toid = instrument.newComponent("TransactionList");
								transaction = (Transaction)instrument.getComponentHandle("TransactionList",toid);
								success =  unpackageInstrumentTransaction(instrument,transaction,unpackagerDoc,transactionIndex,hasInstrumentExisted,mediatorServices,outputDoc);

								if(success==false)
									break;
								//else continue
							}//End of ELSE (If transactionStatus is not CANCELLED_BY_BANK)
						}
					}
					if(success==true)
						instrumentSave = instrument.save();
					if(success==false)
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"History Message Unpackaging");
						return outputDoc;
					}
				}//End of IF Instrument not found
				else
				{
					//PPX-208 - Ravindra B - 03/18/2011 - Start.
					try{
						String messageId = unpackagerDoc.getAttribute(UniversalMessage.headerPath+"/MessageID");
						if(agentId==null){
							agentId= getAgentID(messageId);
						}

						LockingManager.lockBusinessObject(Long.parseLong(historyinstrumentOid),
								agentId , true);
						mediatorServices.debug("TPLUnPackagerMediator: Locking instrument "
						    + historyinstrumentOid + "successful");
				    }
				    catch (InstrumentLockException e){
				    	//Instrument is Previously locked.
				    	outputDoc.setAttribute("/Param/ResubmitMsg","Y");
						return outputDoc;
				    }
				    //PPX-208 - Ravindra B - 03/18/2011 - End.
				    try{
					instrument.getData(Long.parseLong(historyinstrumentOid));
					hasInstrumentExisted = true;
					String historytransactionOid = null;
					stotalNumberOfTransactions = (unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/TotalNumberOfEntries"));
					if(stotalNumberOfTransactions==null)
					{
						totalNumberOfTransactions = 0;
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NO_TRANSACTION_TO_UPDATE);
					}
					else
						totalNumberOfTransactions = Integer.parseInt(stotalNumberOfTransactions);

					boolean success = true;
					mediatorServices.debug ("Total Number of Transactions for this instrument " + totalNumberOfTransactions);
					unpackageInstrumentAttributes(instrument,unpackagerDoc,hasInstrumentExisted,mediatorServices,outputDoc);
					for(int transactionIndex = 0;transactionIndex < totalNumberOfTransactions ;transactionIndex++)
					{
						transactionType = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionTypeCode");
						revalue = transactionType.equals(TransactionType.REVALUE) || transactionType.equals(TransactionType.DEAL_CHANGE);

						if(revalue)
						{
							String activeTransactionOid = instrument.getAttribute("active_transaction_oid");
							if(activeTransactionOid==null) activeTransactionOid = "";
							if(activeTransactionOid.equals(""))
							{
								instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND,"Active Transaction to perform REVALUE or DEAL_CHANGE");
							}
							else
							{
								//PPX-208 - Adarsha - 03/10/2011 - Covered unpackagePosition in If block.
								if(isLatestTransaction){
								Transaction activeTransaction = (Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(activeTransactionOid));
								unpackagePosition(activeTransaction,instrument,unpackagerDoc,transactionIndex);
								}
							}
						} //End of IF (Transaction is a REVALUE)


						else
						{
							historytransactionOid = getRelatedTransaction(unpackagerDoc,instrument,mContext,transactionIndex,mediatorServices);
							if(historytransactionOid == null || historytransactionOid.equals(""))
							{
								String transactionStatusXML = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionStatus");
								if(transactionStatusXML.equals(TransactionStatus.CANCELLED_BY_BANK))
								{
									mediatorServices.debug("No need to update this transaction as it Cancellend By Bank and doesn't exist on TradePortal");
								}//End of if (If transactionStatus is CANCELLED_BY_BANK and transaction doesn't already exist on the Portal)
								else
								{
									toid = instrument.newComponent("TransactionList");
									transaction = (Transaction)instrument.getComponentHandle("TransactionList",toid);
									success =  unpackageInstrumentTransaction(instrument,transaction,unpackagerDoc,transactionIndex,hasInstrumentExisted,mediatorServices,outputDoc);
									if(success==false)
										break;
								}//End of else (If transactionStatus is not CANCELLED_BY_BANK)
							} //End of if (Transaction Not found)
							else
							{
								transaction =(Transaction)instrument.getComponentHandle("TransactionList",Long.parseLong(historytransactionOid));
								String transactionStatus = transaction.getAttribute("transaction_status");
								if(transactionStatus.equals(TransactionStatus.PROCESSED_BY_BANK) ||
										transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
									mediatorServices.debug("Transaction already existing and is Processed By Bank or Cancelled By Bank, no need to download");
								else
								{
									success =  unpackageInstrumentTransaction(instrument,transaction,unpackagerDoc,transactionIndex,hasInstrumentExisted,mediatorServices,outputDoc);
									if(success==false)
										break;
								}//End of else (transactionStatus not Processed by Bank or Cancelled by Bank)
							}//End of else (transaction found)
						}//End of else (Transaction is not a REVALUE)

					} //End of for
					if(success==true)
						instrumentSave = instrument.save();
					if(success==false)
					{
						instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"History Message Unpackaging");
						return outputDoc;
					}
				    }
				    //PPX208- 03/14/2011 -Adarsha - Start
				    finally{
				    	try {
			                LockingManager.unlockBusinessObjectByInstrument(Long.parseLong(historyinstrumentOid), true);
			            } catch (InstrumentLockException e) {
			                // this should never happen
			                e.printStackTrace();
			            }
				    }
				}//End of Else (If Instrument found)

			}//End of HISTORY

		}
		catch (RemoteException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();

		}
		catch (AmsException e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();


		}
		catch (Exception e) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
			e.printStackTrace();

		}

		// Once we are done unpackaging and the instrument has saved, create any emails that need to be sent
		// The call to this method is placed here because we don't want to create an email unless the instrument
		// has succesfully saved
		// Also send messages to @GlobalTrade if appropriate
		if (instrumentSave != -1)
		{
			createGlobalTradeMessagesForTransaction(instrument, mediatorServices);
			createEmailForTransaction(instrument, mediatorServices);
			//CR 913 start- trigger payable invoice supplier email once unpackaging is successful
			if (TradePortalConstants.INV_LINKED_INSTR_PYB.equals(instrument.getAttribute("instrument_type_code")) &&
					TradePortalConstants.PAYABLES_TRANSACTION_TYPE.equals(transaction.getAttribute("transaction_type_code")))
			processPayInvoiceSupplierEmails(mediatorServices, transaction);
			//CR 913 end
			// CR 818 Begin
			// once Instrument saved successfully, then delete all pending settlement Instruction request.
			String workItemNumber = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(0)/Terms/TermsDetails/WorkItemNumber");
            deletePendingSettlementInstructionReqAndRes( workItemNumber, instrument.getAttribute("instrument_oid"), transaction.getAttribute("transaction_oid"), 
            		        transaction.getAttribute("transaction_type_code"), transaction.getAttribute("transaction_status"), instrument.getAttribute("corp_org_oid"), 
            		        mediatorServices );	
            // CR 818 End
		}

		return outputDoc;
	}


	/**
	 * This method takes the instrument object and the transaction object
	 * and populates the database with values from InstrumentTransaction section
	 * of the unpackagerDoc
	 * @param instrument, Instrument object whose values are being packaged
	 * @param transaction, Transaction object
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 * @throws RemoveException
	 */
	public boolean unpackageInstrumentTransaction(Instrument instrument, Transaction transaction, DocumentHandler unpackagerDoc, int transactionIndex, boolean hasInstrumentExisted, MediatorServices mediatorServices, DocumentHandler outputDoc)
	throws RemoteException, AmsException, java.sql.SQLException, RemoveException
	{
		String instrumentTransactionPath = UniversalMessage.instrumentTransactionPath;
		boolean success = false;
		boolean unpackageTerms = true;
		String bankRelTerms_oid;
		mediatorServices.debug("This is the value at instrumentTransactionPath : "+unpackagerDoc.getAttribute(instrumentTransactionPath+"(" + transactionIndex + ")"));

		/*Unpackaging the Transaction*/
		String transactionTypeCode = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionTypeCode");
		String transactionStatus =  unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionStatus");

		mediatorServices.debug(transactionTypeCode);
		mediatorServices.debug("If the above value is null, program should quit now!!");
		if(transactionTypeCode == null || transactionTypeCode.equals(""))
		{
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING, "TransactionTypeCode",instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction/TransactionTypeCode");
			mediatorServices.debug("Inside Null Transaction Handling");
			return false;
		}
		unpackageTransactionAttributes(instrument, transaction,unpackagerDoc,transactionIndex,mediatorServices,outputDoc);

		//CR 913 start
		if(TradePortalConstants.INV_LINKED_INSTR_PYB
				.equals(unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode")) && TradePortalConstants.PAYABLES_TRANSACTION_TYPE.equals(transactionTypeCode)){

		unpackagePayablesMgmtTermsAttributes(instrument,transaction,unpackagerDoc,transactionIndex,mediatorServices);
	    }
		//CR 913 end

		//Setting the value of active transaction_oid
		//ivalavala IR#IHUD111539587 11/24/03. Update active transaction only if BankRelTerms do not exist.
		//Setting the value of Original_transaction_oid for new instruments.
		// WWUH100253221 check isEarliestTransaction
		if((hasInstrumentExisted == false && transactionIndex == 0) || isEarliestTransaction )
			instrument.setAttribute("original_transaction_oid", transaction.getAttribute("transaction_oid"));


		mediatorServices.debug("This is the transactionTypeCode to be used by Terms Manager :"+ transaction.getAttribute("transaction_type_code"));

		/*Unpackage BankReleaseTerms, Fee and Charges only if BankReleaseterms are
		 not already existing IR#SUUC082657240*/
		bankRelTerms_oid = transaction.getAttribute("c_BankReleasedTerms");
		mediatorServices.debug("BankRelTemsFor This Transaction = " + bankRelTerms_oid);
		
		//Rel 9.5.1 CR 1160- Start
		String InstrumentTypeCode = instrument.getAttribute("instrument_type_code");
		boolean isSecondUpdate = StringFunction.isNotBlank(bankRelTerms_oid) &&
				( InstrumentTypeCode.equals(InstrumentType.EXPORT_DLC) || InstrumentTypeCode.equals(InstrumentType.IMPORT_DLC) || 
						InstrumentTypeCode.equals(InstrumentType.STANDBY_LC) || InstrumentTypeCode.equals(InstrumentType.GUARANTEE)	||
						InstrumentTypeCode.equals(InstrumentType.INCOMING_SLC) || InstrumentTypeCode.equals(InstrumentType.INCOMING_GUA) ) 
					&& ( TransactionType.ISSUE.equals(transactionTypeCode) || TransactionType.AMEND.equals(transactionTypeCode) ||
							TransactionType.ADVISE.equals(transactionTypeCode) || TransactionType.PAYMENT.equals(transactionTypeCode) );

		//Rel 9.5.1 CR 1160- End
		
		//ivalavala Nov 14 03 IR#IKUD102435453. Do not create BankRelease terms for a DCR - added TRC 9/21/04 IR#EAUE030460225.
		String OrigTransactionTypeCode = transaction.getAttribute("transaction_type_code");
		if((bankRelTerms_oid == null || bankRelTerms_oid.equals("")|| isSecondUpdate)
				&& !(OrigTransactionTypeCode.equals(TransactionType.DISCREPANCY) || OrigTransactionTypeCode.equals(TransactionType.TRACER)))
		{
			/*Unpackaging the BankReleasedTerms*/

			//ivalavala IR#IHUD111539587 11/24/03. Update active transaction only if BankRelTerms do not exist.

			if(!transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
			{
				// WWUH100253221: check isLatestTransaction
				if (isLatestTransaction) {
					//<IR#BKUF080335338 & BIUF080336121 25thAug 2005>
					//Following if condition is added to fix the issue temporarily as per disscussion with Ravinder
					//so that for Adjustment and Accounts Receivable(A/R Update & A/R Payment) activities the
					//"active_transaction_oid" is not modified because in Trade Portal,it shows the active
					//transaction's currency as instrument and available amount currency in the Current Terms
					//Summary and Show Details page.--Papia Dastidar
					if(!(OrigTransactionTypeCode.equals(TransactionType.ADJUSTMENT)
							|| OrigTransactionTypeCode.equals(TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE)
							|| OrigTransactionTypeCode.equals(TradePortalConstants.ACCOUNTS_RECEIVABLE_PAYMENT))){
						instrument.setAttribute("active_transaction_oid", transaction.getAttribute("transaction_oid"));
					}
				}
			}


			/*Begin Modify -- ivalavala
			 IR # GNUC012845934 Creating the terms Component irrespective of whether the terms are populated,
			 to avoid TradePortal Errors*/

			//Rel 9.5.1 CR-1160 - Start
			long tpoid= 0;
			if(!isSecondUpdate){
				tpoid= transaction.newComponent("BankReleasedTerms");
				mediatorServices.debug("After creating the new transaction I am creating new BnkRelTerms with id = " + tpoid);
			}else{
				transaction.getClientServerDataBridge().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_UPDATE_RECEIVED);
			}
			//Rel 9.5.1 CR-1160 - End

			Terms terms = (Terms)transaction.getComponentHandle("BankReleasedTerms");

			//Vasavi Feb 26 10, CR-489 Start
			if  (TransactionType.DOC_EXAM.equals(OrigTransactionTypeCode) || TransactionType.PAYMENT.equals(OrigTransactionTypeCode))
			{
				String refNumber = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/TermsParty/FullTermsParty(" + transactionIndex + ")/ReferenceNumber");
				terms.setAttribute("reference_number", refNumber);
				mediatorServices.debug("Setting Reference Number "+ refNumber);

			}
			//Vasavi Feb 26 10, CR-489 End


			/*End Modify -- ivalavala */
			unpackageTerms = !(transactionTypeCode.equals(TransactionType.PCE)||transactionTypeCode.equals(TransactionType.PCS)||transactionTypeCode.equals(TransactionType.NAC));
			if(unpackageTerms)
			{
				if(unpackagerDoc.getFragment(instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms") == null)
				{
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE , TradePortalConstants.VALUE_MISSING,"Terms",instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms");
					return false;
				}
				DocumentHandler termsDoc = new DocumentHandler();
				termsDoc.setComponent("/Terms",unpackagerDoc.getFragment(instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms"));
				mediatorServices.debug("Just before creating new Terms and termsDoc = "+termsDoc.toString());			
				success = unpackageTermsAttributes(terms,termsDoc,mediatorServices);
				String expiryDate = unpackagerDoc.getAttribute(instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms/TermsDetails/ExpiryDate");
				if(expiryDate == null) expiryDate = "";
				// WWUH100253221: check isLatestTransaction
				if (isLatestTransaction) {

                     // Nar-IR T36000026627 Rel 8.4.0.0 27-Mar-2014
					 // modify code to add condition to make copy of expiry date field empty for SLC and GUA instrument only in case of it is coming empty from TPS.
					if(((!"".equals(expiryDate)) || InstrumentType.GUARANTEE.equals(unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode")) ||
							InstrumentType.STANDBY_LC.equals(unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode"))) && !transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
					{
						instrument.setAttribute("copy_of_expiry_date",expiryDate);
					}

					if(unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode").equals(InstrumentType.EXPORT_DLC)
							|| unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode").equals(InstrumentType.INCOMING_SLC)
							|| unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode").equals(InstrumentType.INCOMING_GUA))
					{
						String confirmationType = unpackagerDoc.getAttribute(instrumentTransactionPath+"(" + transactionIndex + ")"+"/Terms/OtherConditions/ConfirmationType");
						if((confirmationType != null)
								&& (confirmationType.equals(TradePortalConstants.ADDING_CONFIRMATION)
										|| confirmationType.equals(TradePortalConstants.SILENT_CONFIRMATION)))
						{
							instrument.setAttribute("copy_of_confirmation_ind", TradePortalConstants.INDICATOR_YES);
						}
						else if((confirmationType != null)
								&& (confirmationType.equals(TradePortalConstants.NOT_ADDING_CONFIRMATION)
										|| confirmationType.equals(TradePortalConstants.BANK_MAY_ADD)))
						{
							instrument.setAttribute("copy_of_confirmation_ind", TradePortalConstants.INDICATOR_NO);
						}
					}
				}

				// Special handling for Request to Advise
				// Commenting out for now because exceptions are thrown from terms while setting attributes
				
				/*  Unpackaging the TermsParty
				 Get handles to one-to-many TermsParty component registered with Terms BO.*/

				long longTermsPartyId = 0;
				long benTermsPartyId = 0;

				int tpXMLCount = 0 ;
				TermsParty termsParty = null;
				Integer ItpXMLCount =new Integer(unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/TermsParty/TotalNumberOfEntries"));
				tpXMLCount = ItpXMLCount.intValue();

				// Give error if number of parties exceeds the maximum.
				// But in case of assignment transaction, when the number of assignees exceeds the maximum, simply
				// unpackage what we can.
				if (transaction.getAttribute("transaction_type_code").equals(TransactionType.ASSIGNMENT))
				{
					if(tpXMLCount > TradePortalConstants.MAX_NUM_TERMS_PARTIES - 2)
					{
						tpXMLCount = TradePortalConstants.MAX_NUM_TERMS_PARTIES - 2;
					}
				}
				else if (tpXMLCount > TradePortalConstants.MAX_NUM_TERMS_PARTIES)
				{
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.NUMBER_OF_ENTRIES_MORE,"TermsParty", Integer.toString(TradePortalConstants.MAX_NUM_TERMS_PARTIES));
				}

				int tpcount = 2; // The first and second terms party are reserved for applicant and beneficiary.  The other party starts at index 2.
				String termsPartyType = null;
				String otlCustomerID = null;
				boolean counterParty = false;
				// Flags to check if applicant or benificiary is sender of the message
				boolean isAppSenderId = false;
				boolean isBenSenderId = false;
				String corpOrg = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");

				// Store the party types that are already in the Active Instrument Party List
				ComponentList activeInstrumentPartyList = (ComponentList)instrument.getComponentHandle("ActiveInstrumentPartyList");
				int activeInstrumentPartyCount = activeInstrumentPartyList.getObjectCount();
				Hashtable existingActivePartyTypes = new Hashtable();
				for (int activePartyIndex = 0; activePartyIndex < activeInstrumentPartyCount; activePartyIndex++)
				{
					activeInstrumentPartyList.scrollToObjectByIndexReadOnly(activePartyIndex);
					String activePartyOid = activeInstrumentPartyList.getAttributeReadOnly("active_party_oid");
					String activePartyType = activeInstrumentPartyList.getAttributeReadOnly("terms_party_type");
					existingActivePartyTypes.put(activePartyType, activePartyOid);
				}


				for (int tpXMLIndex = 0; tpXMLIndex < tpXMLCount; tpXMLIndex++)
				{
					termsPartyType = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+ "(" + transactionIndex + ")" + "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/TermsPartyType");
					otlCustomerID = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+ "(" + transactionIndex + ")" + "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/OTLCustomerID");
					mediatorServices.debug("TermsParty of type :" + termsPartyType);
					mediatorServices.debug("OTL Customer ID is :" + otlCustomerID);
					//rkrishna CR 375-D ATP 08/22/2007 inserted
					if(termsPartyType.equals(TermsPartyType.APPLICANT)||termsPartyType.equals(TermsPartyType.ATP_BUYER))
					{
						longTermsPartyId = terms.newComponent("FirstTermsParty");
						termsParty = (TermsParty)terms.getComponentHandle("FirstTermsParty");
						if (otlCustomerID.equals(corpOrg))
						{
							isAppSenderId = true;
							mediatorServices.debug("We have a isAppSenderId is true");
						}
					}
					//rkrishna CR 375-D ATP 08/22/2007 inserted
					else if(termsPartyType.equals(TermsPartyType.BENEFICIARY)||termsPartyType.equals(TermsPartyType.ATP_SELLER))
					{
						longTermsPartyId = terms.newComponent("SecondTermsParty");
						benTermsPartyId = longTermsPartyId;
						termsParty = (TermsParty)terms.getComponentHandle("SecondTermsParty");
						if (otlCustomerID.equals(corpOrg))
						{
							isBenSenderId = true;
							mediatorServices.debug("We have a isBenSenderId is true");

						}
					}
					else
					{
						longTermsPartyId = terms.newComponent(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[tpcount]);
						termsParty = (TermsParty)terms.getComponentHandle(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[tpcount]);
						tpcount++;
					}
					mediatorServices.debug("I have just created the : with id = " + longTermsPartyId);
					unpackageTermsPartyAttributes(termsParty, tpXMLIndex, unpackagerDoc,transactionIndex,mediatorServices);

					//checkifCounterParty
					counterParty = isTermsPartyCounterParty(instrument,transactionTypeCode,termsParty,mediatorServices);
					if(counterParty == true)
					{
						String termsPartyOid = null;
						String vendorId = null;
						termsPartyOid = termsParty.getAttribute("terms_party_oid");
						vendorId = termsParty.getAttribute("vendor_id");

						// WWUH100253221 BEGIN
						// In a rare occasion when the initial transaction of an instrument comes later than an amendment
						// of the instrument, the Amendment did not know what should be the counter party type and so did not
						// set the counter_party_oid on instrument.  Now the initial transaction comes in, we figure out the
						// counter party type, we need to get the latest terms party to set the counter_party_oid.
						if (!isLatestTransaction
								&& isTransactionTypeInitial(transactionTypeCode, instrument.getAttribute("instrument_type_code")))
						{
							String instrumentOid = instrument.getAttribute("instrument_oid");
							String sqlStatement = "SELECT TERMS_PARTY_OID, VENDOR_ID FROM TERMS_PARTY, TRANSACTION, TERMS"
								+ " WHERE TERMS_PARTY_TYPE = ?"
								+ " AND (TERMS_PARTY_OID = C_FIRST_TERMS_PARTY_OID OR TERMS_PARTY_OID = C_SECOND_TERMS_PARTY_OID)"
								+ " AND TERMS_OID = C_BANK_RELEASE_TERMS_OID"
								+ " AND SEQUENCE_DATE = "
								+      " (SELECT MAX(SEQUENCE_DATE) FROM TRANSACTION WHERE P_INSTRUMENT_OID = ?"
								+         " AND TRANSACTION_STATUS = 'PROCESSED_BY_BANK'  )"
								+ " AND  P_INSTRUMENT_OID = ?"
								+ " AND TRANSACTION_STATUS = 'PROCESSED_BY_BANK'";
							Object intOb = new Object();
							intOb = (instrumentOid !=null)?Long.parseLong(instrumentOid):instrumentOid;
							
							DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{termsPartyType, intOb, intOb});
							if(resultXML != null)
							{
								termsPartyOid = resultXML.getAttribute("/ResultSetRecord(0)/TERMS_PARTY_OID");
								vendorId = resultXML.getAttribute("/ResultSetRecord(0)/VENDOR_ID");
							}
						}
						// WWUH100253221 END
						instrument.setAttribute("a_counter_party_oid",termsPartyOid);
						instrument.setAttribute("vendor_id", vendorId);
					}
					mediatorServices.debug("This is TermsParty Updated " + termsParty.getAttribute("terms_party_oid"));
					mediatorServices.debug("This is TermsParty Updated " + termsParty.getAttribute("terms_party_oid"));

					// Update Active Instrument Party if there is already an Active Instrument Party for this party type
					// or create a new Active Instrument Party
					// But do not put assignee in the active party table.  Assignee party should be accessed via transactions.
					if (!termsPartyType.equals(TermsPartyType.ASSIGNEE_01))
					{
						String existingActivePartyOid = (String)existingActivePartyTypes.get(termsPartyType);
						long activePartyOidLong = 0;
						if (existingActivePartyOid!=null)
						{
							activePartyOidLong = Long.parseLong(existingActivePartyOid);
						}
						else
						{
							activePartyOidLong = instrument.newComponent("ActiveInstrumentPartyList");
						}
						ActiveInstrumentParty activeInstrumentParty;
						activeInstrumentParty = (ActiveInstrumentParty)activeInstrumentPartyList.getComponentObject(activePartyOidLong);
						activeInstrumentParty.setAttribute("a_terms_party_oid", String.valueOf(longTermsPartyId));
						activeInstrumentParty.setAttribute("terms_party_type", termsPartyType);
					}

				} // end loop for each terms party


				// check if beneficiary exist and beneficiary and applicant are not the sender of the message then set counter party as beneficiary
				// WWUH100253221: check isLatestTransaction
				if ((benTermsPartyId > 0) && (isBenSenderId == false) && (isAppSenderId == false) && isLatestTransaction)
				{
					String benTermsOid = Long.toString(benTermsPartyId);
					instrument.setAttribute("a_counter_party_oid",benTermsOid);
					mediatorServices.debug("Anoop's conditions are met here");
				}
				else
					mediatorServices.debug("Anoop's conditions are NOT met here");
				mediatorServices.debug("TPLUnPackager: TermsParty is updated.");


			}
			else
				success = true;

			/*Unpackaging the Position */
			if(!transactionStatus.equals(TransactionStatus.CANCELLED_BY_BANK))
				//PPX-208 - Adarsha - 03/10/2011 - Covered unpackagePosition in If block.
				if(isLatestTransaction){
					unpackagePosition(transaction,instrument,unpackagerDoc,transactionIndex);
				}
			//the boolean update parent will be set to true if we have a related instrument to update, false if we
			//do not
			//We call the updateRelatedInstrument method in order to populate the hashtable parentTransaction
			//with values that we will use to update the related instruement's position
			if (transactionTypeCode.equals(TransactionType.CREATE_USANCE))
				updateParent = updateRelatedInstrument(instrument, unpackagerDoc, mediatorServices, parentTransaction );
		
			
			/*Unpackaging the Fee
			 Get handles to one-to-many Fee component registered with Transaction BO.*/
			int feeXMLCount = 0;
			Integer IfeeXMLCount = new Integer(unpackagerDoc.getAttribute(instrumentTransactionPath+"(" + transactionIndex + ")"+"/Fee/TotalNumberOfEntries"));
			mediatorServices.debug("This is the number of Fee Components :"  + IfeeXMLCount );
			feeXMLCount = IfeeXMLCount.intValue();
			mediatorServices.debug( " number of fee items existing" + feeXMLCount);
			mediatorServices.debug(String.valueOf(feeXMLCount));
			long feeoid = 0;
			Fee fee = null;
			if (feeXMLCount > 0)
			{
				for (int feeXMLIndex = 0; feeXMLIndex < feeXMLCount; feeXMLIndex++)
				{
					feeoid = transaction.newComponent("FeeList");
					mediatorServices.debug("This is the id of the Fee " + feeoid);
					fee = (Fee)transaction.getComponentHandle("FeeList",feeoid);
					unpackageFeeAttributes(fee,feeXMLIndex,unpackagerDoc, transactionIndex,mediatorServices);
					mediatorServices.debug("TPLUnPackager: Fee is updated with Fee_Oid = ## " + feeoid );
					mediatorServices.debug("TPLUnPackager: Fee is updated with Fee_Oid = ## " + feeoid );

				}
				mediatorServices.debug("TPLUnPackager: Fee is updated.");
			}
			else mediatorServices.debug("No fee items to update");

		}
		else
		{
			mediatorServices.debug("TPLUnPackager: No BankRelTerms,Fees updated.");
			success = true;
		}
		/*ivalavala Nov 4 2003. Update the documentImages even if Bank release terms are
		 existing. Check if the ImageId is existing prior to creating it. IR#LEUD072948087*/
		/* Unpackaging the DocumentImages
		 Get handles to one-to-many DocumentImage component registered with Transaction BO.
		 */
		
		unpackageDocumentImage(transactionIndex, transaction, instrument, unpackagerDoc, mediatorServices);

		// If this is a Change Activity, determine if it should be displayed to the Corporate Customers. If
		// the transaction will not be displayed then do not send an email notification or display it on the notifications
		// screen.
		boolean displayTransaction = true;
		if (transaction.getAttribute("transaction_type_code").equals(TransactionType.CHANGE))
		{
			String displayChange = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/DisplayChangeActivity");
			if (StringFunction.isBlank(displayChange))
				displayChange = TradePortalConstants.INDICATOR_NO;
			transaction.setAttribute("display_change_transaction", displayChange);
			if (displayChange.equals(TradePortalConstants.INDICATOR_NO))
				displayTransaction = false;
		}

		if((unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/HistoryIndicator") == null ||
				!unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/HistoryIndicator").equals("Y")) && (displayTransaction))
		{
			// Call method which determines (1) if this transaction should be displayed on the notificiations tab
			// and (2) if an email should be sent for this notification
			boolean displayAsNotification = triggerEmailForNotification(instrument, transaction, unpackagerDoc, mediatorServices);
			LOG.info("Inside unpackageInstrumentTransaction()..displayAsNotification="+ displayAsNotification);
			if (displayAsNotification)
			{
				transaction.setAttribute("show_on_notifications_tab", TradePortalConstants.SHOW_ON_NOTIFICATIONS_TAB_Y);
				//added by jm
				transaction.setAttribute("unread_flag",TradePortalConstants.INDICATOR_YES);
				transaction.setAttribute("notification_date", DateTimeUtility.getGMTDateTime());
				transaction.setAttribute("unread_flag", TradePortalConstants.INDICATOR_YES);
			}
			else
			{
				transaction.setAttribute("show_on_notifications_tab", "N");
			}

			/**** Get and store the message that goes to @GlobalTrade ****/
			// This should only be done if not in historic download
			DocumentHandler globalTradeMsg = unpackagerDoc.getFragment(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/GTMessage/GTProponix");

			if(globalTradeMsg != null)
			{
				transaction.setAttribute("globaltrade_msg", globalTradeMsg.toString());
				outgoingGlobalTradeMessagesList.addElement(transaction.getAttribute("transaction_oid"));
			}
		}
                //rkazi IR HAUL010633771 01/05/2011 Begin
                String transStatus = transaction.getAttribute("transaction_status");
                String transOid = transaction.getAttribute("transaction_oid");
                Object intOb = new Object();
				intOb = (transOid !=null)?Long.parseLong(transOid):transOid;
                int noOfPaymentBeneficiaries = DatabaseQueryBean.getCount("P_TRANSACTION_OID","DOMESTIC_PAYMENT", "P_TRANSACTION_OID = ?" , true, new Object[]{intOb});
                if ( InstrumentType.DOMESTIC_PMT.equals(instrument.getAttribute("instrument_type_code"))
                        && noOfPaymentBeneficiaries > 0 && TransactionStatus.PROCESSED_BY_BANK.equals(transStatus)){
                            String updtStatement = " PAYMENT_STATUS = '" + TransactionStatus.PROCESSED_BY_BANK + "' ";
                            String sqlStatement = DatabaseQueryBean.buildUpdateStatement("DOMESTIC_PAYMENT", "P_TRANSACTION_OID", updtStatement, transOid);
                            int updateCount = DatabaseQueryBean.executeUpdate(sqlStatement,false,new ArrayList<Object>());
                            if(updateCount == 0)
                            {
                                 mediatorServices.debug("No Domestic Payments to update");
                                 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING, "TransactionOid", transOid);
                            }
                            // CR-597 crhodes 3/1/20011 Begin
                            processPaymentBeneficiaryEmails(mediatorServices, transOid);
                            // CR-597 crhodes 3/1/20011 End
                        }


                //rkazi IR HAUL010633771 01/05/2011 End
                //rkazi IR RIUL012081432 02/04/2011 START
                if ( InstrumentType.DOMESTIC_PMT.equals(instrument.getAttribute("instrument_type_code"))
                        && noOfPaymentBeneficiaries > 0 && TransactionStatus.CANCELLED_BY_BANK.equals(transStatus))
                {
                    String updtStatement = " PAYMENT_STATUS = '" + TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_REJECTED + "' ";
                    String sqlStatement = DatabaseQueryBean.buildUpdateStatement("DOMESTIC_PAYMENT", "P_TRANSACTION_OID", updtStatement, transOid);
                    int updateCount = DatabaseQueryBean.executeUpdate(sqlStatement,false,new ArrayList<Object>());
                    if(updateCount == 0)
                    {
                         mediatorServices.debug("No Domestic Payments to update");
                         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING, "TransactionOid", transOid);
                    }

                }
              //rkazi IR RIUL012081432 02/04/2011 END
		return success;

	}


	/**
	 * This method takes the instrument object
	 * and populates the database with values from Instrument section
	 * of the unpackagerDoc
	 * @param instrument, Instrument object whose values are being packaged
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageInstrumentAttributes( Instrument instrument, DocumentHandler unpackagerDoc,boolean hasInstrumentExisted, MediatorServices mediatorServices,DocumentHandler outputDoc)
	throws RemoteException, AmsException, java.sql.SQLException
	{ /*
	Updating data for BO:    Instrument
	*/

		instrument.setAttribute("instrument_num",           unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentIDNumber"));
		instrument.setAttribute("instrument_prefix",        unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentIDPrefix"));
		instrument.setAttribute("instrument_suffix",        unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentIDSuffix"));
		//CR- PPX-068 Krishna SCF Tradeportal Reporting through Logical mapping to OTL DB
		instrument.setAttribute("otl_instrument_uoid",      unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/OTLInstrumentUoid"));
		if(unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentID") == null ||
				unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentID").equals(""))
		{
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.VALUE_MISSING, "InstrumentID",UniversalMessage.instrumentPath+"/InstrumentID");
			return;
		}

		// PIUG111379826
		// Update present_docs_within_days
		// WWUH100253221: check isLatestTransaction
		String instrumentType  = unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode");
		if (isLatestTransaction) {
			instrument.setAttribute("present_docs_within_days", unpackagerDoc.getAttribute(UniversalMessage.termsPath+"/PresentationDays"));
			// Special Handling for Request to Advise
			String bankRefNum = unpackagerDoc.getAttribute(UniversalMessage.termsPath+"/TermsDetails/OpeningBankReferenceNumber");
			if((bankRefNum != null) && (!bankRefNum.equals(""))) {
				if (instrumentType.equals(InstrumentType.REQUEST_ADVISE))
					instrument.setAttribute("copy_of_ref_num", bankRefNum);
				else
					instrument.setAttribute("opening_bank_ref_num", bankRefNum);
			}
			String refNum = unpackagerDoc.getAttribute(UniversalMessage.termsPath+"/TermsDetails/ReferenceNumber");

			// Don't overwrite it if doing so will make it blank
			if((refNum != null) && (!refNum.equals("")))
				if (!instrumentType.equals(InstrumentType.REQUEST_ADVISE))
					instrument.setAttribute("copy_of_ref_num", refNum);

			if(!unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentStatus").equals(TradePortalConstants.INSTR_STATUS_PENDING))
				instrument.setAttribute("issue_date",               unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/IssueDate"));
			instrument.setAttribute("instrument_status",        unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentStatus"));
		}

		instrument.setAttribute("complete_instrument_id",   unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentID"));
		instrument.setAttribute("instrument_type_code", unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode"));
		//Default value of instrument object
		// pcutrone - IR#AOUG032155080 - comment out hardcoding of express template flag ...
		instrument.setAttribute("template_flag",   "N");
		outputDoc.setAttribute("/Param/InstrumentOid",instrument.getAttribute("instrument_oid"));

		String transactionType =  unpackagerDoc.getAttribute(UniversalMessage.transactionPath+"/TransactionTypeCode");
		if(isTransactionTypeInitial(transactionType,instrumentType))
		{
			String instrumentLanguage = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/Language");
			if (StringFunction.isBlank(instrumentLanguage))
				instrumentLanguage = TradePortalConstants.INSTRUMENT_LANGUAGE_ENGLISH;
			instrument.setAttribute("language", instrumentLanguage);
		}
		// 4.1 UNV change do not issue error since the messages could come in out of order.
		else
		{
			// WWUH100253221-2 1/12/09 begin
			// Note if an non-originating transaction comes in before the originating transaction,
			// we give an error.  If we uncomment the following code, we would allow the non-originating to create
			// the instrument.  Due to BMO's request, we would not do that.  We give an error instead.  OPs will
			// handle this error manually.			
			// WWUH100253221-2 1/12/09 end
			if ( ! hasInstrumentExisted ) {
				instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NOT_ORIGINATING_TRANSACTION,transactionType,instrumentType);
			}
		}

		// GGAYLE - 6/3-03 - IR RRUD052938928 - Start - Everything between my two ID lines were removed from inside the IF statement above [if(hasInstrumentExisted==false)].
		
		// Lines within the IF statement above were originally here

		String relatedInstrumentID =  unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/RelatedInstrumentID");

		if(relatedInstrumentID!=null)
		{
			if(!relatedInstrumentID.equals(""))
			{
				String corpOrg = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
				String instrumentOid = getRelatedInstrument(relatedInstrumentID,corpOrg,mediatorServices);
				if(instrumentOid!=null)
					if (isLatestTransaction) instrument.setAttribute("related_instrument_oid", instrumentOid);
				//IVALAVALA 11/3/03 Do not issue error if no related Instrument found IR#IDUD062364014				
				//IR Krishna RSUH111235799(Spawning Instrument case on OTL End) 11/18/2007 Begin
					else
					{
						unpackagerDoc.setAttribute(UniversalMessage.termsPath+"/TermsDetails/RelatedInstrumentID", relatedInstrumentID);
					}
				//IR Krishna RSUH111235799 11/18/2007 End
			}
		}
		// GGAYLE - 6/3-03 - IR RRUD052938928 - End

		//Saving other values to the instrument Object
		String opBankOrgProponixID   =  unpackagerDoc.getAttribute(UniversalMessage.headerPath+"/OperationOrganizationID");
		if(opBankOrgProponixID == null || opBankOrgProponixID.equals(""))
		{
			mediatorServices.debug("No opBankOrg to update");
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"OperationOrganizationID", UniversalMessage.headerPath+"/OperationOrganizationID");
		}
		else
		{
			//IValavala Jan 29 07. Make sure the org is ACTIVE
			String sqlStatement = " SELECT ORGANIZATION_OID FROM OPERATIONAL_BANK_ORG WHERE PROPONIX_ID = ? AND ACTIVATION_STATUS = 'ACTIVE'";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{opBankOrgProponixID});
			if(resultXML == null)
			{
				instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
			}
			else
			{
				//make sure that we don't have more than one result
				Vector v = resultXML.getFragments("/ResultSetRecord(0)");
				if (v.size() > 1) {
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "OperationalBankOrg");
				}
				int rid = 0;//We only need the first record in the result set so use id = '1'
				String opOrgOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");

				if(opOrgOid==null || opOrgOid.equals(""))
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "OperationalBankOrg");
				else
					if (isLatestTransaction) instrument.setAttribute("op_bank_org_oid", opOrgOid);
			}

		}


		String clientBankOTLID = unpackagerDoc.getAttribute(UniversalMessage.clientBankPath+"/OTLID");

		if(clientBankOTLID==null || clientBankOTLID.equals(""))
		{
			mediatorServices.debug("No clientBank to update");
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"ClientBank OTLID", UniversalMessage.clientBankPath+"/OTLID");

		}
		else
		{
			//Here, we need to get the organization_oid from the client bank specified in the
			//incoming xml message.  However, there may be multiple client banks with the same OTL ID so we
			//need to bring back the single client bank with the given OTL_ID and an active_status of ACTIVE.
			//The business objects framework will not allow us to find a BO based on multiple fields so we
			//have to do a direct SQL call.//AE 5/14/2002
			String sqlStatement = " SELECT ORGANIZATION_OID FROM CLIENT_BANK WHERE OTL_ID = ? AND ACTIVATION_STATUS = 'ACTIVE'";

			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{clientBankOTLID});
			if(resultXML == null)
			{
				instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "ClientBank");
			}
			else
			{
				//make sure that we don't have more than one result
				Vector v = resultXML.getFragments("/ResultSetRecord(0)");
				if (v.size() > 1) {
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "ClientBank");
				}
				int rid = 0;//We only need the first record in the result set so use id = '1'
				String organizationOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");

				if(organizationOid==null || organizationOid.equals(""))
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "ClientBank");
				else
					instrument.setAttribute("client_bank_oid", organizationOid);
			}
		}

		String corpOrgProponixCustID = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
		if(corpOrgProponixCustID==null || corpOrgProponixCustID.equals(""))
		{
			mediatorServices.debug("No corpOrg to update");
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"CustomerID", UniversalMessage.subHeaderPath+"/CustomerID");
		}
		else
		{
			//Here, we need to get the corporate organization oid from the customer id specified in the
			//incoming xml message.  However, there may be multiple corporate organizations with the same OTL ID so we
			//need to bring back the single corporate organization with the given OTL_ID and an active_status of ACTIVE.
			//The business objects framework will not allow us to find a BO based on multiple fields so we
			//have to do a direct SQL call.//AE 5/28/2002
			//String whereClause  = "PROPONIX_CUSTOMER_ID = '"+ corpOrgProponixCustID + "'";
			String sqlStatement = " SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = 'ACTIVE'";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{corpOrgProponixCustID});
			if(resultXML == null)
			{
				instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization");
			}
			else
			{
				//make sure that we don't have more than one result
				Vector v = resultXML.getFragments("/ResultSetRecord(0)");
				if (v.size() > 1) {
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.MORE_THAN_ONE_VALUE_FOUND, "CorporateOrg");
				}
				int rid = 0;//We only need the first record in the result set so use id = '1'
				String corpOrganizationOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/ORGANIZATION_OID");

				if(corpOrganizationOid==null || corpOrganizationOid.equals(""))
					instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.NO_MATCHING_VALUE_FOUND, "Corporate Organization");
				else
					instrument.setAttribute("corp_org_oid", corpOrganizationOid);
			}

		}

		// W Zhu 3/11/2008 RRUI022958001 begin
		// W Zhu 3/31/2008 WIUI031742166 No need to set otl_po_invoice_portfolio_oid.
		//Get the otl_po_invoice_portfolio oid from the otl_uoid specified in the
		//incoming xml message.  If the SCFTP message is not in, we will not be able to find it
		// then set otl_po_invoice_portfolio_uoid on instrument and later SCFTP will set the OID
		String otl_poInvoicePortfolioUoid = unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/POInvoicePortfolioUoid");
		if(otl_poInvoicePortfolioUoid!=null && !otl_poInvoicePortfolioUoid.equals(""))
		{
			instrument.setAttribute("otl_po_invoice_portfolio_uoid", otl_poInvoicePortfolioUoid);
		}
		// W Zhu 3/11/2008 RRUI022958001 end
	
	}

	//CR 913 start - Unpackage  the PayMgtTerms node
	private void unpackagePayablesMgmtTermsAttributes(Instrument instrument,Transaction transaction, DocumentHandler unpackagerDoc, int transactionIndex, MediatorServices mediatorServices)
		throws RemoteException, AmsException{
		DocumentHandler payMgtTerms = unpackagerDoc.getFragment(UniversalMessage.instrumentTransactionPath +"(" + transactionIndex + ")"+"/Terms/PayMgtTerms");
		List<String> paySupplierTextList =payMgtTerms!=null?payMgtTerms.getAttributes("/PayablesSupplierText"):null;
		if(paySupplierTextList!=null && !paySupplierTextList.isEmpty()){
		mediatorServices.debug("unpackagePayablesMgmtTermsAttributes: total domestic payments = "+paySupplierTextList.size());

		DomesticPayment domesticPayment = null;
		String[] attributeNames = {"sequence_number",
				"amount_currency_code", "amount", "payee_account_number",
				"payment_method_type","value_date","payee_name","payee_email","payee_bank_code","payee_bank_name","buyer_name","invoice_details"};
		String corpOrgOid = instrument.getAttribute("corp_org_oid");
		String corpOrgName = "";
		if(StringFunction.isNotBlank(corpOrgOid))
		{
			CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
			corpOrgName = corpOrg.getAttribute("name");
		}
		for (String payablesSupplierTextTemp: paySupplierTextList)
		{
			
			payablesSupplierTextTemp = payablesSupplierTextTemp.substring(3, payablesSupplierTextTemp.lastIndexOf("|||"));
			String[] values =payablesSupplierTextTemp.split("\\|",-1);

			long domPayOid = transaction.newComponent("DomesticPaymentList");
			domesticPayment = (DomesticPayment)transaction.getComponentHandle("DomesticPaymentList",domPayOid);
			String sentEmail = domesticPayment.setAllAttributes(attributeNames, values, corpOrgName);
			domesticPayment.setAttribute("payment_status",TransactionStatus.PROCESSED_BY_BANK);
			domesticPayment.setAttribute("payment_ben_email_sent_flag",sentEmail);

		}
		transaction.setAttribute("transaction_status",      TransactionStatus.PROCESSED_BY_BANK);
		}
	}

	//CR 913 end

	/**
	 * This method takes the transaction object
	 * and populates the database with values from Transaction section
	 * of the unpackagerDoc
	 * @param transaction, Transaction object
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageTransactionAttributes( Instrument instrument, Transaction transaction, DocumentHandler unpackagerDoc, int transactionIndex, MediatorServices mediatorServices, DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		String transactionPath = UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Transaction";
		mediatorServices.debug("This is the transactionPath :"+ transactionPath);
		String origTransactionStatus = transaction.getAttribute("transaction_status");


		if(unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode") == null ||
				unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode").equals(""))
		{
			transaction.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"TransactionTypeCode",transactionPath+"/TransactionTypeCode");
			return;
		}
		//ivalavala Nov 14 03. Do not update transaction_type_code if the original code is DCR. Added TRC Sep 21 04 IR#EAUE030460225.
		//KSreedhar May 12 05. Do not update transaction_type_code if the original code is AMD and the code in incoming message is CHG.
		String origTransactionType = transaction.getAttribute("transaction_type_code");


		if(!(origTransactionType.equals(TransactionType.DISCREPANCY) || origTransactionType.equals(TransactionType.TRACER)) &&
				!(origTransactionType.equals(TransactionType.AMEND) && (unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode")).equals(TransactionType.CHANGE)))
			transaction.setAttribute("transaction_type_code",   unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode"));

		transaction.setAttribute("sequence_num",            unpackagerDoc.getAttribute(transactionPath+"/SequenceNumber"));

		//<PDastidar CR-238 22-Nov-2005> Start

		String conversionIndicator = unpackagerDoc.getAttribute(transactionPath+"/ConversionIndicator");
		if ((conversionIndicator == null) || (conversionIndicator.equals("")))
			transaction.setAttribute("conversion_indicator", TradePortalConstants.INDICATOR_NO);
		else if ((conversionIndicator.equals(TradePortalConstants.INDICATOR_YES)) || (conversionIndicator.equals(TradePortalConstants.INDICATOR_NO)))
			transaction.setAttribute("conversion_indicator", conversionIndicator);
		else
		{
			instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.VALUE_MISSING, "ConversionIndicator",UniversalMessage.transactionPath+"/ConversionIndicator");
			return;
		}

		if((conversionIndicator != null) &&(conversionIndicator.equals(TradePortalConstants.INDICATOR_YES)))
			transaction.setAttribute("transaction_status",      TransactionStatus.PROCESSED_BY_BANK);
		else
			transaction.setAttribute("transaction_status",      unpackagerDoc.getAttribute(transactionPath+"/TransactionStatus"));

		//<PDastidar CR-238 22-Nov-2005> End
		//<PDastidar CR-238 22-Nov-2005> End
		String origTransactionStatusDate = "";

		//ivalavala 16 Feb 09. IR#IVUJ021045536. Get the value of original Transaction Status Date first
		if (hasTransactionExisted){
			origTransactionStatusDate = transaction.getAttribute("transaction_status_date");
		}

		if((unpackagerDoc.getAttribute(transactionPath+"/TransactionStatusDate") != null)
				&& (unpackagerDoc.getAttribute(transactionPath+"/TransactionStatusDate").trim().length() > 0))
		{
			transaction.setAttribute("transaction_status_date", unpackagerDoc.getAttribute(transactionPath+"/TransactionStatusDate"));
		}
		mediatorServices.debug("After updating transaction status date is :" + transaction.getAttribute("transaction_status_date"));

		// W Zhu 9/9/08 WWUH100253221 add sequence_date
		if((unpackagerDoc.getAttribute(transactionPath+"/SequenceDate") != null)
				&& (unpackagerDoc.getAttribute(transactionPath+"/SequenceDate").trim().length() > 0))
		{
			transaction.setAttribute("sequence_date", unpackagerDoc.getAttribute(transactionPath+"/SequenceDate"));
		}
		transaction.setAttribute("copy_of_amount",          unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Terms/TermsDetails/Amount"));
		transaction.setAttribute("copy_of_currency_code",   unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Terms/TermsDetails/AmountCurrencyCode"));
		
		transaction.setAttribute("copy_of_instr_type_code", unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/InstrumentTypeCode"));
		//CR- PPX-068 Krishna SCF Tradeportal Reporting through Logical mapping to OTL DB
		transaction.setAttribute("otl_transaction_uoid", unpackagerDoc.getAttribute(transactionPath+"/OTLActivityUoid"));
		outputDoc.setAttribute("/Param/TransactionOid",transaction.getAttribute("transaction_oid"));
	
		// W Zhu 3/11/2008 RRUI022958001 begin
		// W Zhu 3/31/2008 WIUI031742166 no need to set port_act_oid.
		//Get the associated otl_portfolio_activity oid from the otl_uoid specified in the
		//incoming xml message.  If the SCFTP message is not in, we will not be able to find it
		// then set association otl_portfolio_activity Uoid on transaction and later SCFTP will set the OID
		String otl_resultingPortActUoid = unpackagerDoc.getAttribute(UniversalMessage.transactionPath+"/ResultingPortActivityUoid");
		if(otl_resultingPortActUoid!=null && !otl_resultingPortActUoid.equals(""))
		{
			transaction.setAttribute("resulting_port_act_uoid", otl_resultingPortActUoid);
		}

		String otl_priorActivePortActUoid = unpackagerDoc.getAttribute(UniversalMessage.transactionPath+"/PriorActivePortActivityUoid");
		if(otl_priorActivePortActUoid!=null && !otl_priorActivePortActUoid.equals(""))
		{
			transaction.setAttribute("prior_active_port_act_uoid", otl_priorActivePortActUoid);
		}
		// W Zhu 3/11/2008 RRUI022958001 end

		//IValavala CR 451
		//IValavala 11 Feb 09. Do the below only if the transaction already exists in portal
		mediatorServices.debug("hasTransactionExisted value is" + hasTransactionExisted );

		//No need to call updateAccountBalances for CancelledTransaction from here. It would be done earlier
		if (hasTransactionExisted && doesCustEnteredTermsExist(transaction) && !accountBalanceUpdated ){
			String instrumentType = instrument.getAttribute("instrument_type_code");
			if(    	 instrumentType.equals(InstrumentType.DOM_PMT) ||
					instrumentType.equals(InstrumentType.FUNDS_XFER) ||
					instrumentType.equals(InstrumentType.XFER_BET_ACCTS)){				
				//rkazi IR RVUL011972931 02/10/2011 START
					if(instrumentType.equals(InstrumentType.DOM_PMT) )
					{
						String transactionStatus = transaction.getAttribute("transaction_status");
						//cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931
						//always update unless awaiting bank processing
						if (!TransactionStatus.TRANSACTION_PROCESSING.equals(transactionStatus))
						{
						    //refactor updateAccountBalances to utility class
							//Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                            boolean resubmit = UnPackagerServices.
							    updateLimits(instrument, origTransactionStatus,
							        transaction, false,
							        transaction.getAttributeDateTime("transaction_status_date"),
							        mediatorServices);
                            if ( resubmit ) {
                                resubmitMsg = true;
                            }
							//Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - End
						}
					}
					else
					{
						//IAZ 06/15/09 Begin						
                        //cquinton 2/23/2011 Rel 6.1.0 ir#rvul011972931
                        //refactor updateAccountBalances to utility class
						//Leelavathi - CR-610 - Rel 7.0.0 - 15-Mar-2011 - Start
                        boolean resubmit = UnPackagerServices.
    						updateLimits(instrument, origTransactionStatus,
    						    transaction, false,
    						    transaction.getAttributeDateTime("transaction_status_date"),
    						    mediatorServices);
                        if ( resubmit ) {
                            resubmitMsg = true;
                        }
					}
					//rkazi IR RVUL011972931 02/10/2011 END

			}
		}


	}

	/**
	 * This method takes the terms object
	 * and populates the database with values from Terms section
	 * of the unpackagerDoc
	 * @param terms, Terms object
	 * @param termsDoc, DocumentHandler object with the XML specific to Terms
	 * author iv
	 */
	public boolean unpackageTermsAttributes( Terms terms, DocumentHandler termsDoc, MediatorServices mediatorServices )
	throws RemoteException, AmsException
	{
		/*
		 Updating data for BO:    Terms & Also TermsParty
		 */
		boolean success = false;

		mediatorServices.debug("I will now try to save TermAttributes of term with id =" + terms.getAttribute("terms_oid")
				+ "\n" + "with the values from document which looks like " + termsDoc.toString());

		LOG.info("I will now try to save TermAttributes of term with id =" + terms.getAttribute("terms_oid"));
		LOG.info( "with the values from document= " + termsDoc.toString());
		success = (terms.setTermsAttributesFromDoc(termsDoc));
		
		return success;

	}


	/**
	 * This method takes the fee object
	 * and populates the database with values from Fee section
	 * of the unpackagerDoc
	 * @param fee, Fee object
	 * @param id, int value representing the fee
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageFeeAttributes(Fee fee, int id, DocumentHandler unpackagerDoc, int transactionIndex,MediatorServices  mediatorServices)
	throws RemoteException, AmsException
	{
		String feePath = UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/Fee";
		mediatorServices.debug("Now updating the Fee object number : " + id);
		if( unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/SettlementCurrencyCode") == null ||
				unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/SettlementCurrencyCode").equals(""))
		{
			fee.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING, "SettlementCurrCode",  feePath+"/FullFee(" + id + ")/SettlementCurrCode");
			return;
		}
		fee.setAttribute("settlement_curr_code",    unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/SettlementCurrencyCode"));
		fee.setAttribute("settlement_curr_amount",  unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/SettlementCurrencyAmount"));
		fee.setAttribute("charge_type",             unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/ChargeType"));
		fee.setAttribute("base_curr_amount",        unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/BaseCurrencyAmount"));
		fee.setAttribute("acct_number",             unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/AccountNumber"));
		fee.setAttribute("settlement_how_type",     unpackagerDoc.getAttribute(feePath+"/FullFee(" + id + ")/SettlementHowType"));
		mediatorServices.debug("Done updating the Fee object number : " + id);
	}

	/**
	 * This method takes the DocumentImage object
	 * and populates the database with values from DocumentImage section
	 * of the unpackagerDoc
	 * @param documentImage, DocumentImage object
	 * @param id, int value representing the documentImage
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageDocumentImageAttributes(DocumentImage documentImage, int id,DocumentHandler unpackagerDoc, int transactionIndex, MediatorServices mediatorServices )
	throws RemoteException, AmsException
	{
		String documentImagePath = UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/DocumentImage" ;

		if(unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/FormType") == null ||
				unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/FormType").equals(""))
		{
			documentImage.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"FormType", documentImagePath+"/FullDocumentImage(" + id + ")/FormType");
			return;
		}

		// [BEGIN] CR-186 - jkok - OTL will send a 'U' but the TradePortal is expecting 'USER' so we do the mapping here
		if (unpackagerDoc.getAttribute(documentImagePath + "/FullDocumentImage(" + id + ")/FormType")
				.equals(TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED.substring(0, 1).toUpperCase())) {
			documentImage.setAttribute("form_type",
					TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED);
		}
		else {
			documentImage.setAttribute("form_type",          unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/FormType"));
		}
		documentImage.setAttribute("image_id",           unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/ImageID"));

		// [BEGIN] CR-186 - jkok - new values for attachments
		documentImage.setAttribute("num_pages", unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/NumPages"));
		documentImage.setAttribute("image_format", unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/ImageFormat"));
		documentImage.setAttribute("image_bytes", unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/ImageBytes"));
		documentImage.setAttribute("doc_name", unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/DocName"));
		//cquinton 7/22/2011 Rel 7.1.0 ppx240 - add image hash
		String imageHash = unpackagerDoc.getAttribute(documentImagePath+"/FullDocumentImage(" + id + ")/Hash");
		if ( imageHash != null && imageHash.length()>0 ) {
		    documentImage.setAttribute( "hash", imageHash );
		}
		// [END] CR-186 - jkok - new values for attachments
		mediatorServices.debug("Done updating the DocumentImage object number : " + id);
	}


	/**
	 * This method takes the TermsParty object
	 * and populates the database with values from TermsParty section
	 * of the unpackagerDoc
	 * @param termsParty, TermsParty object
	 * @param id, int value representing the termsParty
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public void unpackageTermsPartyAttributes(TermsParty termsParty, int id, DocumentHandler unpackagerDoc, int transactionIndex, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		String termsPartyPath = UniversalMessage.instrumentTransactionPath+ "(" + transactionIndex + ")" + "/TermsParty";
		if(unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/TermsPartyType") == null)
		{
			termsParty.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.VALUE_MISSING,"TermsPartyType", termsPartyPath+"/FullTermsParty(" + id + ")/TermsPartyType");
			return;
		}
		termsParty.setAttribute("terms_party_type",         unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/TermsPartyType"));
		termsParty.setAttribute("name",                     unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/Name"));
		termsParty.setAttribute("address_line_1",           unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressLine1"));
		termsParty.setAttribute("address_line_2",           unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressLine2"));
		termsParty.setAttribute("address_line_3",           unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressLine3"));
		termsParty.setAttribute("address_city",             unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressCity"));
		termsParty.setAttribute("address_state_province",   unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressStateProvince"));
		termsParty.setAttribute("address_country",          unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressCountry"));
		termsParty.setAttribute("address_postal_code",      unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/AddressPostalCode"));
		termsParty.setAttribute("phone_number",             unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/PhoneNumber"));
		termsParty.setAttribute("OTL_customer_id",          unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/OTLCustomerID"));
		termsParty.setAttribute("address_seq_num",          unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/OTLAddressSequenceNo"));
		termsParty.setAttribute("vendor_id",                unpackagerDoc.getAttribute(termsPartyPath+"/FullTermsParty(" + id + ")/VendorID"));


	}


	/**
	 * This method takes the transaction object
	 * and populates the database with values from Position section
	 * of the unpackagerDoc to the Instrument Object
	 * @param instrument, Instrument object
	 * @param transactionIndex, int value representing the transaction.
	 * @param unpackagerDoc, DocumentHandler object with the XML
	 * author iv
	 */
	public void unpackagePosition(Transaction transaction, Instrument instrument, DocumentHandler unpackagerDoc, int transactionIndex)
	throws RemoteException, AmsException
	{
		/* Unpackaging the Position details */
		String positionPath = UniversalMessage.instrumentTransactionPath+ "(" + transactionIndex + ")" + "/Position";
		transaction.setAttribute("liability_amt_in_limit_curr",   unpackagerDoc.getAttribute(positionPath+"/LiabilityAmountInLimitCurrency"));
		transaction.setAttribute("liability_amt_in_base_curr",    unpackagerDoc.getAttribute(positionPath+"/LiabilityAmountInBaseCurrency"));
		transaction.setAttribute("base_currency_code",            unpackagerDoc.getAttribute(positionPath+"/InstrumentBaseCurrencyCode"));
		transaction.setAttribute("limit_currency_code",           unpackagerDoc.getAttribute(positionPath+"/InstrumentLimitCurrencyCode"));

		//rbhaduri - 11th Nov 06 - IR CUUG110169269 - Add Begin - When transaction type and realted activity type both
		//are payment then postion need to be set from related position. Commented out above the setting of the following four
		//variables

		//PPX 208 - 01/10/2011 - Adarsha - Add
		transaction.setAttribute("revalue_date",unpackagerDoc.getAttribute(positionPath+"/RevalueDate"));
		String relatedActivityType, transactionType;
		relatedActivityType = unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/RelatedActivityType");
		transactionType = transaction.getAttribute("transaction_type_code");

		if ((transactionType!= null && transactionType.equals(TransactionType.PAYMENT)) && (relatedActivityType!= null && relatedActivityType.equals(TransactionType.PAYMENT)))
		{
			int transIndex = 0;
			String relatedPositionPath = UniversalMessage.instrumentTransactionPath+ "(" + transIndex + ")" + "/RelatedInstrumentPosition";
			transaction.setAttribute("available_amount", unpackagerDoc.getAttribute(relatedPositionPath+"/AvailableAmount"));
			transaction.setAttribute("equivalent_amount",unpackagerDoc.getAttribute(relatedPositionPath+"/EquivalentAmount"));
			transaction.setAttribute("instrument_amount",unpackagerDoc.getAttribute(relatedPositionPath+"/CurrentInstrumentAmount"));
			// WWUH100253221: check isLatestTransaction
			if (isLatestTransaction) instrument.setAttribute("copy_of_instrument_amount", unpackagerDoc.getAttribute(relatedPositionPath+"/CurrentInstrumentAmount"));
		}
		else
		{
			transaction.setAttribute("available_amount", unpackagerDoc.getAttribute(positionPath+"/AvailableAmount"));
			transaction.setAttribute("equivalent_amount",unpackagerDoc.getAttribute(positionPath+"/EquivalentAmount"));
			transaction.setAttribute("instrument_amount",unpackagerDoc.getAttribute(positionPath+"/CurrentInstrumentAmount"));
			// WWUH100253221: check isLatestTransaction
			if (isLatestTransaction) instrument.setAttribute("copy_of_instrument_amount",unpackagerDoc.getAttribute(positionPath+"/CurrentInstrumentAmount"));
		}
		
	}

	/**
	 * This method takes the transaction object
	 * and checks if the corresponding BankReleaseTerms Component Exists
	 * @param transaction, Transaction object
	 * @param mContext, SessionContext representing the present context.
	 * author iv
	 */
	public boolean doesBankReleaseTermsExist( Transaction transaction)
	throws RemoteException, AmsException
	{
		String cBankReleasedTerms = transaction.getAttribute("c_BankReleasedTerms");
		if (cBankReleasedTerms == null || cBankReleasedTerms.equals(""))
		{
			return false;
		}
		return true;
	}


	/**
	 * This method takes the transaction object
	 * and checks if the corresponding CustEnteredTerms Component Exists
	 * @param transaction, Transaction object
	 * author iv
	 */
	public boolean doesCustEnteredTermsExist( Transaction transaction)
	throws RemoteException, AmsException
	{
		String cCustEnteredTerms = transaction.getAttribute("c_CustomerEnteredTerms");
		if (cCustEnteredTerms == null || cCustEnteredTerms.equals(""))
		{
			return false;
		}
		return true;
	}


	/**
	 * This method takes the transaction object
	 * and checks if any corresponding Fee Components Exist
	 * @param transaction, Transaction object
	 * @param mContext, SessionContext representing the present context.
	 * author iv
	 */
	public boolean doesFeeExist( Transaction transaction, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		ComponentList componentList   =   (ComponentList)        mediatorServices.createServerEJB("ComponentList");
		componentList = (ComponentList)transaction.getComponentHandle("FeeList");
		int feeCount = componentList.getObjectCount();
		if (feeCount > 0)
		{
			return true;
		}
		return false;
	}



	/**
	 * This method takes the transaction object
	 * and checks if any corresponding DocumentImage Components Exist
	 * @param transaction, Transaction object
	 * @param mContext, SessionContext representing the present context.
	 * author iv
	 */
	public boolean doesDocumentImageExist(Transaction transaction, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		ComponentList componentList   =   (ComponentList)        mediatorServices.createServerEJB("ComponentList");
		componentList = (ComponentList)transaction.getComponentHandle("DocumentImageList");
		int documentImageCount = componentList.getObjectCount();
		if (documentImageCount > 0)
		{
			return true;
		}
		return false;
	}


	/**
	 * This method takes the unpackagerDoc and checks for
	 * the instrument with matching InstrumentID and CustomerID.
	 * If a matching instrument is found, returns its ObjectID,
	 * if not returns null.
	 * @param unpackagerDoc, DocumentHandler object
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public String getRelatedInstrument(String completeInstrumentID, String corpOrgProponixCustID, MediatorServices mediatorServices)
	throws RemoteException, AmsException, java.sql.SQLException
	{
		
		if(completeInstrumentID == null ||completeInstrumentID.equals("") )
		{
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"InstrumentID", UniversalMessage.instrumentPath);
			return null;
		}
		String whereClause  = "COMPLETE_INSTRUMENT_ID = ?";
		int instrumentCount = DatabaseQueryBean.getCount("INSTRUMENT_OID","INSTRUMENT",whereClause, true, new Object[]{completeInstrumentID});
		
		
		if(instrumentCount > 0)
		{
			if(corpOrgProponixCustID==null || corpOrgProponixCustID.equals(""))
			{
				mediatorServices.debug("No corpOrg to update");
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"CustomerID", UniversalMessage.subHeaderPath+"/CustomerID");
			}
			else
			{
				//MEer Rel 9.2 IR-T36000037252- Add filter to get only active corporate customers				
				
				String sqlStatement = " SELECT INSTRUMENT_OID FROM INSTRUMENT  WHERE " +
						"  A_CORP_ORG_OID = (SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE ACTIVATION_STATUS = 'ACTIVE' AND PROPONIX_CUSTOMER_ID = ?)" +
						"  AND COMPLETE_INSTRUMENT_ID = ?";
				

				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{corpOrgProponixCustID, completeInstrumentID});
				if(resultXML == null)
				{
					return null;
				}
				else
				{					
					int rid = 0;
					String instrumentOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/INSTRUMENT_OID");

					if(instrumentOid==null || instrumentOid.equals(""))
						return null;
					else
						return instrumentOid;
				}
			}
		}

		return null;

	}


	/**
	 * This method takes the unpackagerDoc, instrument and checks
	 * the instrument for a matching Transaction component.
	 * If a matching transaction is found, returns its ObjectID,
	 * if not returns null.
	 * @param unpackagerDoc, DocumentHandler object
	 * @param instrument, Instrument object
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */
	public String  getRelatedTransaction(DocumentHandler unpackagerDoc, Instrument instrument, SessionContext mContext, int transactionIndex, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		int transactionCount = 0;
		ComponentList transactionList   =   null;

		transactionList = (ComponentList)instrument.getComponentHandle("TransactionList");
		transactionCount = transactionList.getObjectCount();
		String transactionPath = UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")" + "/Transaction";

		mediatorServices.debug("Number of transactions found for this instrument : " + transactionCount);
		String returnMessageIDXML             = unpackagerDoc.getAttribute(transactionPath+"/ReturnMessageID");

		//If there is no transaction retrieved, then return null
		if (transactionCount == 0)
		{
			return null;
		}

		//If there is one or more transactions retrieved, then search for the right one
		if (transactionCount >= 1)
		{
			String transactionTypeCodeXML    = unpackagerDoc.getAttribute(transactionPath+"/TransactionTypeCode");
			String sequenceNumXML             = unpackagerDoc.getAttribute(transactionPath+"/SequenceNumber");
			if(sequenceNumXML == null) sequenceNumXML= "";
			String ReturnMessageIdBO         = null;
			String transactionTypeCodeBO     = null;
			String sequenceNumBO              = null;
			String transactionOid			 = null;
			String transactionStatusDateXML = unpackagerDoc.getAttribute(transactionPath+"/TransactionStatusDate");
			String transactionStatusDateBO  = null;
			if(transactionTypeCodeXML ==null)
			{
				instrument.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE, TradePortalConstants.VALUE_MISSING,"TransactionTypeCode",transactionPath+"/TransactionTypeCode");
				transactionTypeCodeXML = "";
			}


			mediatorServices.debug("Need to find a transaction with TypeCode and message_id = " + transactionTypeCodeXML +  returnMessageIDXML);

			for (int transactionId = 0; transactionId < transactionCount; transactionId++)
			{
				transactionList.scrollToObjectByIndexReadOnly(transactionId);
				ReturnMessageIdBO             = transactionList.getAttributeReadOnly("return_message_id");
				mediatorServices.debug("Present message_id :" + ReturnMessageIdBO );
				transactionTypeCodeBO    = transactionList.getAttributeReadOnly("transaction_type_code");
				mediatorServices.debug("Present typeCode is :" + transactionTypeCodeBO  );
				sequenceNumBO            = transactionList.getAttributeReadOnly("sequence_num");
				transactionOid			 = transactionList.getAttributeReadOnly("transaction_oid");
				transactionStatusDateBO  = transactionList.getAttributeReadOnly("transaction_status_date");

				mediatorServices.debug("Present sequence_num :" + sequenceNumBO);
				mediatorServices.debug("Present oid :" + transactionOid  );
				mediatorServices.debug("Here before checking the typeCode and message_id");
				if(returnMessageIDXML == null) returnMessageIDXML = "";
				if(returnMessageIDXML == null || returnMessageIDXML.equals(""))
				{
					mediatorServices.debug("XML type code = " + transactionTypeCodeXML );
					mediatorServices.debug("XML sequence num = " + sequenceNumXML);
					//The following lines have been removed from the code because
					//Payments and Expiry should be uniquely identified by their
					//type and sequence number, rather than their type and date
					//This is because it is possible to have multiple payments, expirations
					//on the same date ---AE  4/22/2002
					//Check for TransactionStatusDate if SequenceNumber is empty.--IValavala 9/19/02
					if(transactionTypeCodeXML .equals(TransactionType.DEACTIVATE) ||
							transactionTypeCodeXML .equals(TransactionType.EXPIRE)     ||
							transactionTypeCodeXML .equals(TransactionType.REACTIVATE))
					{
						mediatorServices.debug("Date from XML :" + transactionStatusDateXML);
						mediatorServices.debug("Date from BO  :" + transactionStatusDateBO);
						if ((StringFunction.isBlank(sequenceNumBO)) &&
								(StringFunction.isBlank(sequenceNumXML)))
						{
							if (transactionTypeCodeXML .equals(transactionTypeCodeBO)
									&& transactionStatusDateBO.equals(transactionStatusDateXML))
							{
								mediatorServices.debug("Found a matching transaction with id : " + transactionOid );
								return transactionOid;
							}
						}
						else
						{
							if (transactionTypeCodeXML .equals(transactionTypeCodeBO) && sequenceNumBO!=null
									&& sequenceNumBO.equals(sequenceNumXML))
							{
								mediatorServices.debug("Found a matching transaction with id : " + transactionOid );
								return transactionOid;
							}
						}
					}
					else
					{
						if (transactionTypeCodeXML .equals(transactionTypeCodeBO) && sequenceNumBO!=null
								&& sequenceNumBO.equals(sequenceNumXML))
						{
							mediatorServices.debug("Found a matching transaction with id : " + transactionOid );
							return transactionOid;
						}
					}
				}
				else
				{

					if (returnMessageIDXML.equals(ReturnMessageIdBO))
					{
						mediatorServices.debug("Found a matching transaction with id : " + transactionOid );
						return transactionOid;
					}
				}
			}
			return null;
		}
		return null;


	}

	/**
	 * This method takes the instrumentType and transactionType
	 * to check if the transaction is the Initialtransaction
	 * for this particular instrument.
	 * If the rules are satisfied returns true else returns false
	 * @param instrumentTypeCode, String
	 * @param transactionTypeCode, String
	 * author iv
	 */
	public boolean isTransactionTypeInitial(String transactionTypeCode, String instrumentTypeCode)
	{
		if(transactionTypeCode.equals(TransactionType.CREATE_USANCE))
		{
			if(instrumentTypeCode.equals(InstrumentType.CLEAN_BA)       ||
					instrumentTypeCode.equals(InstrumentType.DOCUMENTARY_BA) ||
					instrumentTypeCode.equals(InstrumentType.DEFERRED_PAY)   ||
					instrumentTypeCode.equals(InstrumentType.REFINANCE_BA)   ||
					instrumentTypeCode.equals(InstrumentType.COLLECT_ACCEPT))
				return true;
			else return false;
		}

		if(transactionTypeCode.equals(TransactionType.COLLECTION))
		{
			if(instrumentTypeCode.equals(InstrumentType.IMPORT_COL))
				return true;
			else return false;
		}

		if(transactionTypeCode.equals("CRE"))
		{
			if(instrumentTypeCode.equals(InstrumentType.INDEMNITY))
				return true;
			else return false;
		}

		if(transactionTypeCode.equals(TransactionType.ISSUE))
		{
			if(instrumentTypeCode.equals(InstrumentType.EXPORT_COL) ||
					instrumentTypeCode.equals(InstrumentType.NEW_EXPORT_COL)|| //Vasavi CR 524 03/31/2010 Add
					instrumentTypeCode.equals(InstrumentType.IMPORT_DLC) ||
					instrumentTypeCode.equals(InstrumentType.GUARANTEE)  ||
					instrumentTypeCode.equals(InstrumentType.STANDBY_LC) ||
					instrumentTypeCode.equals(InstrumentType.SHIP_GUAR)  ||
					instrumentTypeCode.equals(InstrumentType.LOAN_RQST)  ||
					instrumentTypeCode.equals(InstrumentType.REQUEST_ADVISE)  ||
					instrumentTypeCode.equals(InstrumentType.FUNDS_XFER)  ||
					instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY) || //rkrishna CR 375-D ATP 08/22/2007 Inserted)
					instrumentTypeCode.equals(InstrumentType.XFER_BET_ACCTS) || //ivalavala CR 451. Adding FTBA, FTDP
					instrumentTypeCode.equals(InstrumentType.DOMESTIC_PMT) ||
					instrumentTypeCode.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT))// W Zhu CR-434 MAUI122349268 Add RFN
				return true;
			else return false;
		}

		if(transactionTypeCode.equals(TransactionType.PRE_ADVISE))
		{
			if(instrumentTypeCode.equals(InstrumentType.EXPORT_DLC))
				return true;
			else return false;
		}
		if(transactionTypeCode.equals(TransactionType.ADVISE))
		{
			if(instrumentTypeCode.equals(InstrumentType.INCOMING_GUA) ||
					instrumentTypeCode.equals(InstrumentType.INCOMING_SLC) ||
					instrumentTypeCode.equals(InstrumentType.EXPORT_DLC))
				return true;
			else return false;
		}

		if(transactionTypeCode.equals(TransactionType.RELEASE))
		{
			if(instrumentTypeCode.equals(InstrumentType.AIR_WAYBILL))
				return true;
			else return false;
		}

		if(transactionTypeCode.equals(TransactionType.TRANSFER))
		{
			if(instrumentTypeCode.equals(InstrumentType.EXPORT_DLC) ||
					instrumentTypeCode.equals(InstrumentType.GUARANTEE) ||
					instrumentTypeCode.equals(InstrumentType.IMPORT_DLC))
				return true;
			else return false;
		}
		//Pramey - 11/20/2008  ARM CR-434 Add Begin
		if(transactionTypeCode.equals(TradePortalConstants.NEW_RECEIVABLES))
		{
			if(instrumentTypeCode.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT))
				return true;
			else
				return false;
		}
		//Pramey - 11/20/2008  ARM CR-434 Add End

		//CR 913 start - 'APM' original transaction for PYB instrument
		if(transactionTypeCode.equals(TradePortalConstants.PAYABLES_CREATE))
		{
			if(instrumentTypeCode.equals(TradePortalConstants.INV_LINKED_INSTR_PYB))
				return true;
			else
				return false;
		}
		//CR 913 end
		
		// 08/11/2015 Rel 9.4 CR-932 STARTS HERE
		if(TradePortalConstants.NEWBILL.equals(transactionTypeCode))
		{
			if(TradePortalConstants.BILLING.equals(instrumentTypeCode))
				return true;
			else return false;
		}
		// 08/11/2015 Rel 9.4 CR-932 ENDS HERE
		

		return false;
	}
	/**
	 * This method extracts the related instrument position data out
	 * of the XML document(i.e. DocumentHandler) and puts
	 * it into the hashtable parentTransaction, which will
	 * later be used to update the position of the
	 * related instrument
	 * @param instrument, Instrument
	 * @param uppackagerDoc, DocumentHandler
	 * @param mediatorServices, MediatorServices
	 * @param parentTransaction, Hashtable
	 * author AE
	 */
	public boolean updateRelatedInstrument(Instrument instrument, DocumentHandler unpackagerDoc,MediatorServices mediatorServices, Hashtable parentTransaction )
	throws RemoteException, AmsException, java.sql.SQLException
	{
		
		int transactionIndex = 0;
		String relatedInstrumentID =  unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/RelatedInstrumentID");
		String relatedPositionPath = UniversalMessage.instrumentTransactionPath+ "(" + transactionIndex + ")" + "/RelatedInstrumentPosition";
		if (relatedInstrumentID == null || relatedInstrumentID.equals(""))
			return false;

		//ivalavala Oct 26 2004
		String corporateOrgId = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
		String relatedInstrumentOid = getRelatedInstrument(relatedInstrumentID,corporateOrgId,mediatorServices);
		
		if (relatedInstrumentOid==null|| relatedInstrumentID.equals("") )
			return false;
		else
		{		
			String[] relatedPosition = new String[7];

			////here we will get all of the attributes for the related position

			relatedPosition[0] = unpackagerDoc.getAttribute(relatedPositionPath+"/AvailableAmount");
			relatedPosition[1] = unpackagerDoc.getAttribute(relatedPositionPath+"/EquivalentAmount");
			relatedPosition[2] = unpackagerDoc.getAttribute(relatedPositionPath+"/LiabilityAmountInBaseCurrency");
			relatedPosition[3] = unpackagerDoc.getAttribute(relatedPositionPath+"/LiabilityAmountInLimitCurrency");
			relatedPosition[4] = unpackagerDoc.getAttribute(relatedPositionPath+"/InstrumentBaseCurrencyCode");
			relatedPosition[5] = unpackagerDoc.getAttribute(relatedPositionPath+"/InstrumentLimitCurrencyCode");
			relatedPosition[6] = unpackagerDoc.getAttribute(relatedPositionPath+"/CurrentInstrumentAmount");
			mediatorServices.debug("BEFORE going into the loop that sets the hashtable");
			//the above will likely be removed...now they are here for testing
			if (relatedPosition[0] != null && !relatedPosition[0].equals("")) {
				mediatorServices.debug("Should we be going into the loop that sets the hashtable");
				mediatorServices.debug("Because Available Amount is: " + (String) relatedPosition[0]);
				//Ravindra IR 26903 04th Nov 2014 added bank specific condition for BMO  - Start
				if(!"Y".equals(ConfigSettingManager.getConfigSetting("PORTAL_AGT","AGENT","ExcludeLiabilityUpdateOnParentInstrument","N"))){
					parentTransaction.put("liability_amt_in_limit_curr", relatedPosition[3]);
				}
				//Ravindra IR 26903 04th Nov 2014 added bank specific condition for BMO  - End
				parentTransaction.put("available_amount", relatedPosition[0]);
				parentTransaction.put("equivalent_amount", relatedPosition[1]);
				parentTransaction.put("liability_amt_in_base_curr", relatedPosition[2]);
				parentTransaction.put("base_currency_code", relatedPosition[4]);
				parentTransaction.put("limit_currency_code", relatedPosition[5]);
				parentTransaction.put("instrument_amount", relatedPosition[6]);
				return true;
			}
		}

		return false;//There was no related instrument's position to update
	}

	//Krishna Code compare with 3.6.0.2 - applying to 4.0  12/11/2007 Add Begin
	// W Zhu  1/26/07 KLUH011440691 add new method updateRelatedChildrenInstruments
	/**
	 * Update the related children instruments' a_related_instrument_oid with
	 * the current instrument's instrument_oid.  If the children instruments are
	 * uploaded first, they cannot set a_related_instrument_oid at that time.
	 * Instead they would have terms.related_instrument_id set.  We need to update
	 * instrument.a_related_instrument_oid when the parent instrument is uploaded.
	 * @param String parentInstrumentOid - parent instrument oid
	 * @param String parentInstrumentID - parent instrument ID
	 * @param String corpOrgProponixCustId - parent instrument' corporate org proponix id
	 *
	 */
	private boolean updateRelatedChildrenInstruments(String parentInstrumentOid,
			String parentInstrumentID, String corpOrgProponixCustID)
	throws RemoteException, AmsException{
		try {
            // WZUK082537468 add activation_status = 'ACTIVE'
			Object intOb = new Object();
			intOb = (parentInstrumentOid !=null)?Long.parseLong(parentInstrumentOid):parentInstrumentOid;
			int numberOfChanges = DatabaseQueryBean.executeUpdate(
					"update INSTRUMENT set A_RELATED_INSTRUMENT_OID = ? "
					+ " where instrument_oid in (select instrument_oid from instrument i, transaction t, terms "
					+ " where t.p_instrument_oid = i.instrument_oid"
					+ " and t.c_bank_release_terms_oid = terms.terms_oid"
					+ " and i.a_related_instrument_oid is null"
					+ " and terms.RELATED_INSTRUMENT_ID = ? "
					+ " AND i.A_CORP_ORG_OID = (SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE ACTIVATION_STATUS = 'ACTIVE' AND PROPONIX_CUSTOMER_ID = ?)) ",
					true, new Object[]{intOb, parentInstrumentID, corpOrgProponixCustID});
		}
		catch (java.sql.SQLException e){
			return true;
		}
		return true;

	}
	//Krishna Code compare with 3.6.0.2 -applying to 4.0  12/11/2007 Add End


	// W Zhu 1/9/2009 SLUJ010768298 update mail messages that may have been uploaded before this instrument.
	/**
	 * Update the related children instruments' a_related_instrument_oid with
	 * the current instrument's instrument_oid.  If the children instruments are
	 * uploaded first, they cannot set a_related_instrument_oid at that time.
	 * Instead they would have terms.related_instrument_id set.  We need to update
	 * instrument.a_related_instrument_oid when the parent instrument is uploaded.
	 * @param String relatedInstrumentOid - related instrument oid
	 * @param String relatedInstrumentID - related instrument ID
	 * @param String corpOrgProponixCustId - parent instrument' corporate org proponix id
	 *
	 */
	private boolean updateMailMessages(String relatedInstrumentOid,
			String relatedInstrumentID, String corpOrgProponixCustID)
	throws RemoteException, AmsException{
		try {
	
			//MEer Rel 9.2 IR-T36000037252- Add filter to get only active corporate customers
			Object intOb = new Object();
			intOb = (relatedInstrumentOid !=null)?Long.parseLong(relatedInstrumentOid):relatedInstrumentOid;
			DatabaseQueryBean.executeUpdate(
					"update MAIL_MESSAGE set A_RELATED_INSTRUMENT_OID = ?, COMPLETE_INSTRUMENT_ID = null"
					+ " where COMPLETE_INSTRUMENT_ID = ? "
					+ " AND A_ASSIGNED_TO_CORP_ORG_OID = (SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE  ACTIVATION_STATUS = 'ACTIVE'  AND PROPONIX_CUSTOMER_ID = ? )"
					+ " AND A_RELATED_INSTRUMENT_OID is null", true, new Object[]{intOb, relatedInstrumentID, corpOrgProponixCustID});
				
			
		}
		catch (java.sql.SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;

	}
	// W Zhu 1/9/2009 SLUJ010768298 end

	/**
	 * This method takes the instrument, transactionType and termsParty
	 * to check if the termsParty is the counterParty for this particular
	 * instrument and transactionType.
	 * If the rules are satisfied returns true else returns false
	 * @param instrument, Instrument object
	 * @param transactionType, String
	 * @param termsParty, TermsParty object
	 * @param mediatorServices, MediatorServices
	 * author iv
	 */

	public boolean isTermsPartyCounterParty(Instrument instrument, String transactionType, TermsParty termsParty, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		String termsPartyType = termsParty.getAttribute("terms_party_type");
		//Case 1
		if(transactionType.equals(TransactionType.ADVISE) ||
				transactionType.equals(TransactionType.PRE_ADVISE))
		{
			if(termsPartyType.equals(TermsPartyType.APPLICANT))
				return true;
			else return false;
		}
		//Case 2
		if(transactionType.equals(TransactionType.AMEND) ||
				transactionType.equals(TransactionType.AMEND_TRANSFER))
		{
			String instrumentOid = instrument.getAttribute("instrument_oid");

			String sqlStatement = " SELECT TERMS_PARTY_TYPE FROM INSTRUMENT, TERMS_PARTY WHERE " +
			"  A_COUNTER_PARTY_OID = TERMS_PARTY_OID AND INSTRUMENT_OID = ?";
			Object intOb = new Object();
			intOb = (instrumentOid !=null)?Long.parseLong(instrumentOid):instrumentOid;

			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{intOb});
			if(resultXML == null)
				return false;
			else
			{
				int rid = 0;
				String presentCounterPartyType = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/TERMS_PARTY_TYPE");
				if(presentCounterPartyType.equals(termsPartyType))
					return true;
				else
					return false;
			}

		}


		if(
				transactionType.equals(TransactionType.ISSUE))
		{
			String instrumentType = instrument.getAttribute("instrument_type_code");
			mediatorServices.debug("Instrument Type is " + instrumentType);


			//Case 3
			if(instrumentType.equals(InstrumentType.EXPORT_COL))
			{
				if(termsPartyType.equals(TermsPartyType.DRAWEE_BUYER)) return true;
				else return false;
			}
			//Vasavi CR 524 03/31/2010 Add
			if(instrumentType.equals(InstrumentType.NEW_EXPORT_COL))
			{
				if(termsPartyType.equals(TermsPartyType.DRAWEE_BUYER)) return true;
				else return false;
			}
			//Case 4
			else
			{
				//Case 7
				if(instrumentType.equals(InstrumentType.SHIP_GUAR))
				{
					if(termsPartyType.equals(TermsPartyType.FREIGHT_FORWARD)) return true;
					else return false;
				}
				//IR-PNUI061344367 of CR-419 Krishna Begin
				else if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))
				{
					if(termsPartyType.equals(TermsPartyType.ATP_SELLER)) return true;
					else return false;
				}
				// Narayan IR-LLUK061846087 Begin
				else if (instrumentType
						.equals(InstrumentType.FUNDS_XFER)
						|| instrumentType
								.equals(InstrumentType.DOMESTIC_PMT)
						|| instrumentType
								.equals(InstrumentType.XFER_BET_ACCTS)) {
					// Narayan IR-LLUK061846087 End
					if(termsPartyType.equals(TermsPartyType.PAYEE)) return true;
					else return false;
				}
				//IR-PNUI061344367 of CR-419 Krishna End
				else
				{
					if(termsPartyType.equals(TermsPartyType.BENEFICIARY)) return true;
					else return false;
				}
			}
		}
		//Case 5
		/* For Create Usnace find the related instrument id, if available then find the terms party of the related instrument
		 if terms party is avaiable and equals to the terms party of the new instrument set counter party to true
		 if it is not equal check whether the originator of the message which is CustomerID from XML is a Proponix Customer
		 if yes then don't set the couter party  else set counter party
		 if related instrument id is not available then again check whether the originator of the message which is CustomerID
		 from XML is a Proponix Customer if yes then don't set the couter party  else set counter party
		 */
		if(transactionType.equals(TransactionType.CREATE_USANCE))
		{


			String relatedInstrumentOid = instrument.getAttribute("related_instrument_oid");
			if(StringFunction.isNotBlank(relatedInstrumentOid))
			{
				String sqlStatement = " SELECT TERMS_PARTY_OID,INSTRUMENT.VENDOR_ID FROM INSTRUMENT, TERMS_PARTY WHERE " +
				"  A_COUNTER_PARTY_OID = TERMS_PARTY_OID AND INSTRUMENT_OID = ? ";
				
				Object intOb = new Object();
				intOb = (relatedInstrumentOid !=null)?Long.parseLong(relatedInstrumentOid):relatedInstrumentOid;

				DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{intOb});
				mediatorServices.debug("Related Instrument's counter party is here" + resultXML);

				if(resultXML == null)
				{
					mediatorServices.debug("Related Instrument has no counter party" + termsPartyType);
					String corpOrgOid = instrument.getAttribute("corp_org_oid");
					if(!corpOrgOid.equals(""))
					{
						mediatorServices.debug("This is the corporate_org" + corpOrgOid);

						CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
						if(corpOrg.getAttribute("Proponix_customer_id").equals(termsParty.getAttribute("OTL_customer_id")))
							return false;
						else return true;
					}

					else return false;
				}

				else
				{
//					if related instrument id exist then set the parents instrument counter party as for the child one
					// WWUH100253221: note do not check isLatestTransaction.  We always set if it if the transaciton is create usance.
					int rid = 0;
					String ParentCounterPartyOid = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/TERMS_PARTY_OID");
					instrument.setAttribute("a_counter_party_oid",ParentCounterPartyOid);

					String parentCounterPartyVendorId = resultXML.getAttribute("/ResultSetRecord(" + rid + ")/VENDOR_ID");
					instrument.setAttribute("vendor_id", parentCounterPartyVendorId);

					return false;
				}
			}
			else
			{
				String corpOrgOid = instrument.getAttribute("corp_org_oid");
				if(!corpOrgOid.equals(""))
				{
					CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
					if(corpOrg.getAttribute("Proponix_customer_id").equals(termsParty.getAttribute("OTL_customer_id")))
						return false;
					else return true;
				}

				else return false;
			}

		}

		//Case 6
		if(transactionType.equals(TransactionType.TRANSFER))
		{
			if(termsPartyType.equals(TermsPartyType.TRANSFEREE))
				return true;
			else
				return false;
		}

		if(transactionType.equals(TransactionType.COLLECTION))
		{
			if(termsPartyType.equals(TermsPartyType.DRAWER_SELLER)) return true;
			else return false;
		}

		//Case 8
		if(transactionType.equals(TransactionType.RELEASE))
		{
			String instrumentType = instrument.getAttribute("instrument_type_code");
			mediatorServices.debug("Instrument Type is " + instrumentType);
			if(instrumentType.equals(InstrumentType.AIR_WAYBILL))
			{
				if(termsPartyType.equals(TermsPartyType.FREIGHT_FORWARD)) return true;
				else return false;
			}
		}
		//For all others
		return false;
	}

	/**
	 * This method takes the instrument and transaction data and determines
	 * if an email notification should be triggered for this instrument's
	 * corporate customer
	 *
	 * @param instrument, Instrument object
	 * @param transaction, Transaction object
	 * @param unpackagerDoc, DocumentHanlder
	 * @param mediatorServices, MediatorServices
	 */
	public boolean triggerEmailForNotification(Instrument instrument, Transaction transaction, DocumentHandler unpackagerDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		boolean showOnNotificationsTab = false;
		boolean triggerEmail = false;

		//Vshah - 29/12/2010 - IR#JJUF051069751 - Rel7.0 - [BEGIN]
		String corpOrgProponixCustID = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
		if(StringFunction.isNotBlank(corpOrgProponixCustID))  {
			String sqlStatement = " SELECT ORGANIZATION_OID FROM CORPORATE_ORG WHERE " +
			"  PROPONIX_CUSTOMER_ID = ? AND ACTIVATION_STATUS = 'ACTIVE'";
			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{corpOrgProponixCustID});
			if(resultXML != null)
			{
		//Vshah - 29/12/2010 - IR#JJUF051069751 - Rel7.0 - [END]


				String corpOrgOid = instrument.getAttribute("corp_org_oid");

				// If a corp org is associated with this transaction's instrument, check if the corp org has specified
				// a notification rule
				if (!StringFunction.isBlank(corpOrgOid))
				{
					CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
					String notifRuleOid = corpOrg.fetchNotificationRule();//Rel9.5 Cr-927B Fetch notification rule for the corporate customer by looking into the 'Notification_Rule' table
					if (StringFunction.isBlank(notifRuleOid))
					{
						showOnNotificationsTab = true;
					}
					else
					{					
						String transactionType = transaction.getAttribute("transaction_type_code");
						LOG.info("notifRuleOid=["+notifRuleOid+"]transactionType="+transactionType);

						// Certain transaction types should use another transaction type's
						// notification email settings. The following transaction types
						// should be handled in the following manner:
						//   1. Amendment transfers = amendment
						//   2. Release = issue
						//   3. AR Payment (ARP) = AR Updates (ARU) - IR#NNUF021139139

						if (transactionType.equals(TransactionType.AMEND_TRANSFER))
							transactionType = TransactionType.AMEND;
						//else if (transactionType.equals(TradePortalConstants.RELEASE))
						//	transactionType = TradePortalConstants.ISSUE;
						else if (transactionType.equals(TradePortalConstants.ACCOUNTS_RECEIVABLE_PAYMENT))
							transactionType = TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE;

						String origInstrumentType = transaction.getAttribute("copy_of_instr_type_code");
						String instrumentType = null;
						String instrumentCategory = null;
						
						// The following instrument are created from another instrument, therefore we need to get the
						// instrument type of the related instrument in order to retrieve the appropriate setting value
						// from the notification rule
						if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA) 	||
								origInstrumentType.equals(InstrumentType.DEFERRED_PAY) 	||
								origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT) )
						{
							String relatedInstrumentID =  unpackagerDoc.getAttribute(UniversalMessage.instrumentPath+"/RelatedInstrumentID");
							if(StringFunction.isNotBlank(relatedInstrumentID))
							{
								try{

									String corpOrgId = unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/CustomerID");
									String relatedInstrumentOid = getRelatedInstrument(relatedInstrumentID,corpOrgId,mediatorServices);
									if (relatedInstrumentOid!=null)
									{
										Instrument relatedInstrument = (Instrument)mediatorServices.createServerEJB("Instrument",Long.parseLong(relatedInstrumentOid));
										instrumentType = relatedInstrument.getAttribute("instrument_type_code");
										// Changes done for Rel9.5 CR-927B - Set the instrument category based on the instrument type and determine if its Import or Export
										if (instrumentType.equals(InstrumentType.IMPORT_COL) 		||
												instrumentType.equals(InstrumentType.IMPORT_DLC)		||
												instrumentType.equals(InstrumentType.GUARANTEE)		||
												instrumentType.equals(InstrumentType.REQUEST_ADVISE)		||
												instrumentType.equals(InstrumentType.STANDBY_LC )		||
												instrumentType.equals(InstrumentType.APPROVAL_TO_PAY ) || 
												instrumentType.equals(InstrumentType.AIR_WAYBILL ) || 
												instrumentType.equals(InstrumentType.SHIP_GUAR )) 
										{
											instrumentCategory = TradePortalConstants.IMPORT;
											if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA)) 
													instrumentType = TradePortalConstants.IMPORT_BANKER_ACCEPTANCE;
											else if (origInstrumentType.equals(InstrumentType.DEFERRED_PAY)) 
												instrumentType = TradePortalConstants.IMPORT_DEFERRED_PAYMENT;
											else if (origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT)) 
												instrumentType = TradePortalConstants.IMPORT_TRADE_ACCEPTANCE;
										}
										else if (instrumentType.equals(InstrumentType.EXPORT_COL) 	        ||
												instrumentType.equals(InstrumentType.NEW_EXPORT_COL) 		|| 
												instrumentType.equals(InstrumentType.EXPORT_DLC) 		||
												instrumentType.equals(InstrumentType.INCOMING_GUA)	        ||
												instrumentType.equals(InstrumentType.INCOMING_SLC))
										{
											instrumentCategory = TradePortalConstants.EXPORT;
											if (origInstrumentType.equals(InstrumentType.DOCUMENTARY_BA)) 
												instrumentType = TradePortalConstants.EXPORT_BANKER_ACCEPTANCE;
											else if (origInstrumentType.equals(InstrumentType.DEFERRED_PAY)) 
												instrumentType = TradePortalConstants.EXPORT_DEFERRED_PAYMENT;
											else if (origInstrumentType.equals(InstrumentType.COLLECT_ACCEPT)) 
												instrumentType = TradePortalConstants.EXPORT_TRADE_ACCEPTANCE;
										}
									}
								}
								catch (Exception e) {
									mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Message Unpackaging");
									e.printStackTrace();
								}
							}
						}

						if (StringFunction.isBlank(instrumentType))
						{
							instrumentType = origInstrumentType;
						}

						
						// Determine if documents and fees are associated with this transaction
						boolean documentsFeesExist = false;
						if (doesDocumentImageExist(transaction,mediatorServices) || doesFeeExist(transaction,mediatorServices))
						{
							documentsFeesExist = true;
						}
						if (instrumentType.equals(InstrumentType.LOAN_RQST)
								&& TradePortalConstants.INDICATOR_YES.equals(unpackagerDoc.getAttribute(UniversalMessage.subHeaderPath+"/SupplierPortalInd"))){ // Added for Rel 9.0 CR 913
							instrumentType = TradePortalConstants.SUPPLIER_PORTAL;
						}
						
						//Rel9.5 CR-927B Pass the instrumentType itself instead of instrumentCategory
						// Retrieve the email setting from the notification rule using the instrument's category and the transaction type
						String notificationSetting = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid, instrumentType, transactionType, TradePortalConstants.NOTIF_RULE_NOTIFICATION);

						// Display transaction on the notifications tab if the notification setting is set to always or if its
						// set to charges/docs only and charges and/or docs exist
						if (!StringFunction.isBlank(notificationSetting))
						{
							LOG.info("Inside triggerEmailForNotification()... notificationSetting="+notificationSetting );
							if (notificationSetting.equals(TradePortalConstants.NOTIF_RULE_ALWAYS))
							{
								showOnNotificationsTab = true;
							}
							else if (notificationSetting.equals(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY))
							{
								if (documentsFeesExist)
								{
									showOnNotificationsTab = true;
								}
							}
						}

						// We only want to trigger an email if the corporate org does not send emails daily
						String emailFrequency = corpOrg.getNotificationRuleAttribute("email_frequency");
						if (!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY)
								&&!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
						{
							// Check if the notification rule specifies that an email should only be triggered
							// when charges or documents exist
							String emailSetting = corpOrg.getNotificationRuleSettingOrFreq(notifRuleOid, instrumentType, transactionType, TradePortalConstants.NOTIF_RULE_EMAIL);
							
							if (StringFunction.isNotBlank(emailSetting))
							{
								LOG.info("Inside triggerEmailForNotification()... emailSetting="+emailSetting);
								if (emailSetting.equals(TradePortalConstants.NOTIF_RULE_ALWAYS))
								{
									triggerEmail = true;
								}
								else if (emailSetting.equals(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY))
								{
									if (documentsFeesExist)
									{
										triggerEmail = true;
									}
								}
							}
						}
					}
				}
			}
		}

		if (triggerEmail)
		{
			transactionEmailList.addElement(transaction.getAttribute("transaction_oid"));
		}
		
		
		return showOnNotificationsTab;
	}

	/**
	 * This method triggers an outgoing message to GlobalTrade for the transaction once the instrument and
	 * transaction has been saved
	 *
	 * @param instrument, Instrument object
	 * @param mediatorServices, MediatorServices
	 */
	public void createGlobalTradeMessagesForTransaction( Instrument instrument, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		// If there are no messages to be sent to @GlobalTrade, there's nothing to do here
		if(outgoingGlobalTradeMessagesList.size() == 0)
			return;

		// Get the instrument oid
		String instrumentOid = instrument.getAttribute("instrument_oid");
		if (StringFunction.isNotBlank(instrumentOid))
		{
			String corpOrgOid = instrument.getAttribute("corp_org_oid");
			if (StringFunction.isNotBlank(corpOrgOid))
			{
				// If the corporate org is not set up for doc prep, there's nothing to do, so return
				CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));                String globalTradeSetting = corpOrg.getAttribute("doc_prep_ind");
				if((globalTradeSetting == null) || globalTradeSetting.equals(TradePortalConstants.INDICATOR_NO))
					return;

				for (int i= 0; i < outgoingGlobalTradeMessagesList.size(); i++)
				{
					OutgoingInterfaceQueue oiq = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue");
					oiq.newObject();
					String attributeNames[] = new String[] {"date_created", "status",
							"msg_type", "instrument_oid", "transaction_oid", "message_id"};

					// the message_id (oid) should be unique for the middleware -
					// retrieve this information by getting an instance of ObjectIDCache
					// for MESSAGE (middleware) to generate the object id.
					String messageId = Long.toString(ObjectIDCache.getInstance(AmsConstants.MESSAGE).generateObjectID(false));
					/* IValera - IR# JOUI111280267 - 02/18/2009 - Change Begin */
					/* To avoid duplication of message id across servers, appends last character of 'serverName' to new message id */
					PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("AgentConfiguration");
					String serverName = portalProperties.getString("serverName");

					// NSX 04/20/2010 - RDUK041649583 Begin
					messageId += serverName.substring(serverName.length() - 2);
					// NSX 04/20/2010 - RDUK041649583 End

					/* IValera - IR# JOUI111280267 - 02/18/2009 - Change End */

					String attributeValues[] = new String[] {DateTimeUtility.getGMTDateTime(),
							TradePortalConstants.OUTGOING_STATUS_STARTED,
							MessageType.GT,
							instrument.getAttribute("instrument_oid"),
							(String)outgoingGlobalTradeMessagesList.elementAt(i),
							messageId };

					oiq.setAttributes(attributeNames, attributeValues);
					oiq.save();
				}
			}
		}

	}



	/**
	 * This method creates an email for the transaction once the instrument and
	 * transaction has been saved
	 *
	 * @param instrument, Instrument object
	 * @param mediatorServices, MediatorServices
	 */
	public void createEmailForTransaction( Instrument instrument, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{

		// Get the instrument oid
		String instrumentOid = instrument.getAttribute("instrument_oid");
		if (StringFunction.isNotBlank(instrumentOid))
		{
			String corpOrgOid = instrument.getAttribute("corp_org_oid");
			if (StringFunction.isNotBlank(corpOrgOid))
			{
				for (int i= 0; i < transactionEmailList.size(); i++)
				{
					// Call method to create the corporate org's email
					CorporateOrganization corpOrg = (CorporateOrganization)mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
					String transactionOid = (String)transactionEmailList.elementAt(i);
					LOG.debug("Create Email Trigger Message for transaction oid {} with CorpOrg oid {}", transactionOid,corpOrgOid);
					corpOrg.createEmailMessage(instrumentOid, transactionOid, " ", " ", TradePortalConstants.EMAIL_TRIGGER_TRANSACTION);


				}
			}
		}

	}
	//Krishna Code compare with 3.6.0.2 - applying to 4.0  12/11/2007 Add Begin
	/**
	 * This method links the finance instruments related to this instrument
	 * by updating value of related_instrument_oid.
	 * @param financeLinksDoc, DocumentHandler object
	 * @param mediatorServices, MediatorServices
	 * @param instrumentOid, String
	 * @param corpOrgId, String
	 * @param transactionIndex, int
	 * author - iv
	 */
	public void createFinanceLinks(DocumentHandler financeLinksDoc,MediatorServices mediatorServices, String instrumentOid, String corporateOrgId, int transactionIndex)
	throws RemoteException, AmsException, java.sql.SQLException
	{
		int	financeCount = 0;
		String sfinanceCount = financeLinksDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/LinkedFinances/NumberOfEntries");
		Integer IfinanceCount =new Integer(sfinanceCount);
		financeCount = IfinanceCount.intValue();
		String financeInstrumentId, financeInstrumentOid;
		Instrument financeInstrument;
		for(int finIndex = 0; finIndex < financeCount; finIndex++)
		{
			financeInstrumentId= financeLinksDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/LinkedFinances/Finance("+ finIndex +")/InstrumentID");
			financeInstrumentOid = getRelatedInstrument(financeInstrumentId,corporateOrgId,mediatorServices);
			if (StringFunction.isNotBlank(financeInstrumentOid))
			{
				financeInstrument = (Instrument) mediatorServices.createServerEJB("Instrument",Long.parseLong(financeInstrumentOid));
				financeInstrument.setAttribute("related_instrument_oid",instrumentOid);
				financeInstrument.save();
			}
		}


	}
	//Krishna Code compare with 3.6.0.2 - applying to 4.0 12/11/2007 Add End

	/**
	 * This method links the prepayment instruments related to this instrument
	 * by updating value of related_instrument_oid.
	 * @param prePaymentLinksDoc, DocumentHandler object
	 * @param mediatorServices, MediatorServices
	 * @param instrumentOid, String
	 * @param corpOrgId, String
	 * @param transactionIndex, int
	 * author - VS CR 634 Rel 6.1.0.0
	 */
	public void createPrePaymentLinks(DocumentHandler prePaymentLinksDoc,MediatorServices mediatorServices, String instrumentOid, String corpOrgId, int transactionIndex)
	throws RemoteException, AmsException, java.sql.SQLException
	{
		int	prePaymentCount = 0;
		//Removing all links
		//cquinton 1/26/2011 Rel 6.1.0 ir#laul012548753 - fix incorrect column name
        String sqlStatement = "UPDATE INSTRUMENT SET A_RELATED_INSTRUMENT_OID = NULL" +
            " WHERE A_RELATED_INSTRUMENT_OID = ? ";
        try {
            DatabaseQueryBean.executeUpdate(sqlStatement, true, new Object[]{Long.parseLong(instrumentOid)});
        } catch (SQLException sqlex) {
            //issue an error and move on
            mediatorServices.getErrorManager().issueError(
                TradePortalConstants.ERR_CAT_MIDDLEWARE, AmsConstants.SQL_ERROR,
                "Removing related instrument pre payment links",
                String.valueOf(sqlex.getErrorCode()) + " " + sqlex.getMessage() );
        }

		String sprePaymentCount = prePaymentLinksDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/LinkedPrePayment/NumberOfEntries");
		Integer IprePaymentCount =new Integer(sprePaymentCount);
		prePaymentCount = IprePaymentCount.intValue();
		String prePaymentInstrumentId, prePaymentInstrumentOid;
		for(int finIndex = 0; finIndex < prePaymentCount; finIndex++)
		{
			prePaymentInstrumentId= prePaymentLinksDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/LinkedPrePayment/PrePayment("+ finIndex +")/InstrumentID");
			prePaymentInstrumentOid = getRelatedInstrument(prePaymentInstrumentId,corpOrgId,mediatorServices);
			if (StringFunction.isNotBlank(prePaymentInstrumentOid))
			{
                //cquinton 1/26/2011 Rel 6.1.0 ir#laul012548753 - use sql rather than ejb to update for performance				
				String sqlSetStatement = "UPDATE INSTRUMENT SET " +
				    "A_RELATED_INSTRUMENT_OID = ? " +
				    "WHERE INSTRUMENT_OID = ?";
                try {
                	Object longOb = new Object();
                	Object longOb1 = new Object();
                	longOb = (instrumentOid !=null)?Long.parseLong(instrumentOid):instrumentOid;
                	longOb1 = (prePaymentInstrumentOid !=null)?Long.parseLong(prePaymentInstrumentOid):prePaymentInstrumentOid;
                    int updateCount = DatabaseQueryBean.executeUpdate(sqlSetStatement, true, new Object[]{longOb,longOb1 });
                    if (updateCount == 0 ) {
                        mediatorServices.debug( "Instrument oid" + prePaymentInstrumentOid + " not found for related instrument update" );
                    }
                } catch (SQLException sqlex) {
                    //issue an error and move on
                    mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_MIDDLEWARE, AmsConstants.SQL_ERROR,
                        "Updating related instrument pre payment links",
                        String.valueOf(sqlex.getErrorCode()) + " " + sqlex.getMessage() );
                }
			}
		}
	}


	/**
	 * This method takes the transaction object
	 * and checks if a particular DocumentImage Components Exist
	 * @param transaction, Transaction object
	 * @param mContext, SessionContext representing the present context.
	 * @param docImageID, String representin the ID on image server. Added with 3.2
	 * author iv
	 */
	public boolean doesDocumentImageExist(Transaction transaction, MediatorServices mediatorServices,String docImageID)
	throws RemoteException, AmsException
	{
		ComponentList componentList   =   (ComponentList)        mediatorServices.createServerEJB("ComponentList");
		componentList = (ComponentList)transaction.getComponentHandle("DocumentImageList");
		int documentImageCount = componentList.getObjectCount();
		String docImageIDExisting = null;
		DocumentImage documentImage = null;
		if (documentImageCount > 0)
		{
			//return true;
			for(int docIndex = 0; docIndex < documentImageCount; docIndex++)
			{
				//<AShahi IR#MNUF080264505 08/05/2005>
				//Changed the function call from scrollToObjectByIndexReadOnly to scrollToObjectByIndex
				componentList.scrollToObjectByIndex(docIndex);
				documentImage = (DocumentImage)componentList.getBusinessObject();
				//Changed the function call from getAttributeReadOnly to getAttribute
				docImageIDExisting = documentImage.getAttribute("image_id");
				//</AShahi IR#MNUF080264505 08/05/2005>
				mediatorServices.debug("This is the ID of DocImg in the message : " + docImageID);
				mediatorServices.debug("This is the ID of transactions DocImg at Index: " + docIndex + ": " + docImageIDExisting);

				if (docImageID.equals(docImageIDExisting))
				{
					return true;
				}
			}


		}
		return false;
	}

	
	//PPX-208 - Adarsha K S - 03/09/2011 - Start
	/**
	 *
	 * @param unpackagerDoc
	 * @param instrumentOid
	 * @return
	 * @throws AmsException
	 */

	protected boolean isOutOfOrderMessage(DocumentHandler unpackagerDoc,String instrumentOid) throws AmsException{
		String transactionPath = UniversalMessage.instrumentTransactionPath + "(0)/Transaction";
		String sequenceDate = unpackagerDoc.getAttribute(transactionPath+"/SequenceDate");
		if( sequenceDate != null && sequenceDate.trim().length() > 0 ) {

			String whereClause =
				"p_instrument_oid = ?"
				+ " AND SEQUENCE_DATE > to_date(?, 'mm/dd/yyyy HH24:MI:SS')"
				+ " AND TRANSACTION_STATUS = 'PROCESSED_BY_BANK'";
			Object longOb1 = new Object();
			longOb1 = (instrumentOid !=null)?Long.parseLong(instrumentOid):instrumentOid;
			int numberOfLaterTransaction = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, true, new Object[]{longOb1,sequenceDate});
			if (numberOfLaterTransaction == 0){
				String revalueDate = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+"(0)/Position/RevalueDate");
				if( revalueDate != null && revalueDate.trim().length() > 0 ) {
					whereClause =
						"p_instrument_oid = ?"
						+ " AND SEQUENCE_DATE = to_date(?, 'mm/dd/yyyy HH24:MI:SS')"
						+ " AND TRANSACTION_STATUS = 'PROCESSED_BY_BANK' AND REVALUE_DATE > to_date(?, 'mm/dd/yyyy HH24:MI:SS')";
					numberOfLaterTransaction = DatabaseQueryBean.getCount("transaction_oid", "transaction", whereClause, true, new Object[]{longOb1, sequenceDate, revalueDate});
					return numberOfLaterTransaction != 0;
				}
				else{
					return false;
				}
			}

		}else{
			return false;
		}


		return true;
	}

	/**
	 * This method retrieves AgentId from incoming_queue table for given messageID
	 * @param messageId
	 * @return agentId
	 * @throws AmsException
	 */

	protected String getAgentID(String messageId) throws AmsException{
		String agentId=null;

		String sqlStatement = "SELECT AGENT_ID FROM INCOMING_QUEUE WHERE MESSAGE_ID = ?";
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, true, new Object[]{messageId});
		if(resultXML != null){
			agentId = resultXML.getAttribute("/ResultSetRecord(0)/AGENT_ID");
		}

		return agentId;
	}
	//PPX-208 - Adarsha K S - 03/09/2011 - End
	/**
	 * Creates a record in the PaymentBenEmailQueue table for the specified transaction.  The transaction
	 * must be in a status of 'PROCESSED_BY_BANK' which is assumed when this method is called.  An agent
	 * will then process the queue recordswhich will then determine which domestic payments (if any)
     * should have emails generated for them.
	 *
	 * @param mediatorServices The MediatorServices object to use.
	 * @param transactionOID The transaction oid to send the emails for.
	 *
	 * @throws AmsException
	 * @throws RemoteException
	 * @throws SQLException
	 */
    protected void processPaymentBeneficiaryEmails(MediatorServices mediatorServices, String transactionOID)
    	throws AmsException, RemoteException, SQLException, RemoveException {

    	PaymentBenEmailQueue emailQueue = (PaymentBenEmailQueue) mediatorServices.createServerEJB("PaymentBenEmailQueue");
		emailQueue.createNewPaymentBeneficiaryEmailQueueRecord(transactionOID, TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_PROCESSED_BY_BANK);
    	emailQueue.save();
    	emailQueue.remove();

    }
    //CR 913 start - Process supplier emails
    private void processPayInvoiceSupplierEmails(MediatorServices mediatorServices, Transaction transaction) throws RemoteException, AmsException{
    	String transOid = transaction.getAttribute("transaction_oid");
    	String whrClause = "P_TRANSACTION_OID = ?";
    	Object longOb1 = new Object();
		longOb1 = (transOid !=null)?Long.parseLong(transOid):transOid;
    	int noOfPaymentBeneficiaries = DatabaseQueryBean.getCount("P_TRANSACTION_OID","DOMESTIC_PAYMENT",whrClause, true, new Object[]{longOb1});
    	if (noOfPaymentBeneficiaries > 0 && TransactionStatus.PROCESSED_BY_BANK.equals(transaction.getAttribute("transaction_status"))){
                try {
					processPaymentBeneficiaryEmails(mediatorServices, transOid);
				} catch (SQLException e) {
					LOG.error("SQLException while processing TPLUnPackagerMediator.processPayInvoiceSupplierEmails()");
					e.printStackTrace();
				} catch (RemoveException e) {
					LOG.error("RemoveException while processing TPLUnPackagerMediator.processPayInvoiceSupplierEmails()");
					e.printStackTrace();
				}
       }
    }
    //CR 913 end

	// Nar Cr-881 Rel9400 09/16/2015 Begin
    /**
     * This method is used to delete any un-authorized Settlement Instruction Request and response for customer when PAY or LIQ transaction
     * has been Rejected or Processed by Bank. Instrument Id and Work item number is required to delete respective settlement request and Response.
     * 
     * @param workItemNumber
     * @param instrumentOid
     * @param transactionOid
     * @param transactionType
     * @param transStatus
     * @param corpOrgOid
     * @throws SQLException
     * @throws AmsException
     */
    private void deletePendingSettlementInstructionReqAndRes( String workItemNumber, String instrumentOid, String transactionOid, String transactionType, 
    		String transStatus, String corpOrgOid, MediatorServices mediatorServices ) throws AmsException {
    	
    	if ( TransactionStatus.PROCESSED_BY_BANK.equals(transStatus) || TransactionStatus.REJECTED_BY_BANK.equals(transStatus) || 
    			TransactionStatus.CANCELLED_BY_BANK.equals(transStatus) ) {
    		if( (TransactionType.LIQUIDATE.equals(transactionType) || TransactionType.PAYMENT.equals(transactionType)) && 
    				StringFunction.isNotBlank(workItemNumber) ) {
    			try {
    			  String settlementRequestSql = " UPDATE mail_message SET message_status = ? WHERE work_item_number = ? AND a_related_instrument_oid = ? " +
                        " AND a_assigned_to_corp_org_oid = ? AND settlement_instr_flag != ?  AND (  response_transaction_oid is null OR " +
                        " Exists (SELECT 1 FROM transaction WHERE transaction_oid = response_transaction_oid AND transaction_status not in (?,?,?,?) )  ) ";
    			  DatabaseQueryBean.executeUpdate(settlementRequestSql, true, new Object[] { TradePortalConstants.REC_DELETED, workItemNumber, instrumentOid,
    					corpOrgOid, "N", TransactionStatus.AUTHORIZED, TransactionStatus.PROCESSED_BY_BANK,
      					TransactionStatus.REJECTED_BY_BANK, TransactionStatus.CANCELLED_BY_BANK });
    			  
    			  String settlementResponseSql = " UPDATE transaction T1 SET T1.transaction_status = ? WHERE T1.transaction_type_code = ? AND T1.p_instrument_oid = ? " + 
    			                  " AND T1.transaction_status not in (?,?,?,?) And Exists (SELECT 1 FROM terms T2 WHERE T1.c_cust_enter_terms_oid = T2.terms_oid " +
                                  " AND T2.work_item_number = ?) ";
      			  DatabaseQueryBean.executeUpdate(settlementResponseSql, true, new Object[] { TransactionStatus.DELETED, 
      					  TransactionType.SIR, instrumentOid, TransactionStatus.AUTHORIZED, TransactionStatus.PROCESSED_BY_BANK,
      					TransactionStatus.REJECTED_BY_BANK, TransactionStatus.CANCELLED_BY_BANK, workItemNumber });   
    			} catch ( AmsException | SQLException e ) {
    				 mediatorServices.debug("Error occured while deleting Pending Settlement Instrction Request and response for Instruemnt OID- " + instrumentOid);
    				throw new AmsException(e);
    			}
    		}
    	}
    	
    }
 
    /**
     * This method is used to unpackage Image document
     * @param transactionIndex
     * @param transaction
     * @param instrument
     * @param unpackagerDoc
     * @param mediatorServices
     * @throws RemoteException
     * @throws AmsException
     */
    private void unpackageDocumentImage( int transactionIndex, Transaction transaction, Instrument instrument, 
    		DocumentHandler unpackagerDoc, MediatorServices mediatorServices ) throws RemoteException, AmsException {
    	
    	int diXMLCount = 0;
    	long dioid = 0;
		DocumentImage documentImage = null;
		Integer IdiXMLCount = new Integer(unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath+"(" + transactionIndex + ")"+"/DocumentImage/TotalNumberOfEntries"));
		diXMLCount = IdiXMLCount.intValue();
		String form_type = null;
		String image_id = null;
		if(diXMLCount > 0)
		{
			for (int diXMLIndex = 0; diXMLIndex < diXMLCount; diXMLIndex++)
			{
				//we do not want to create a new document image component until we
				//have determined that it is not a swift7XX message
				mediatorServices.debug("This is the id of the DocImage " + dioid);
				form_type = unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/DocumentImage/FullDocumentImage(" + diXMLIndex + ")/FormType");
				if((form_type.equals(TradePortalConstants.DOC_IMG_FORM_TYPE_SWIFT700) ||
						form_type.equals(TradePortalConstants.DOC_IMG_FORM_TYPE_SWIFT710) ||
						form_type.equals(TradePortalConstants.DOC_IMG_FORM_TYPE_SWIFT720)) &&
						(instrument.getAttribute("instrument_type_code").equals(InstrumentType.EXPORT_DLC)))
				{
					transaction.setAttribute("transaction_as_text", unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/DocumentImage/FullDocumentImage(" + diXMLIndex + ")/DocumentText"));
				}
				else
				{
					/*Check if a document image with ImageID is already existing - ivalavala Nov 4 03*/
					image_id =  unpackagerDoc.getAttribute(UniversalMessage.instrumentTransactionPath + "(" + transactionIndex + ")" + "/DocumentImage/FullDocumentImage(" + diXMLIndex + ")/ImageID");
					if(!doesDocumentImageExist(transaction,mediatorServices,image_id))
					{
						dioid = transaction.newComponent("DocumentImageList");
						documentImage = (DocumentImage)transaction.getComponentHandle("DocumentImageList",dioid);
						unpackageDocumentImageAttributes(documentImage, diXMLIndex, unpackagerDoc, transactionIndex,mediatorServices);
						mediatorServices.debug("TPLUnPackager: DocumentImage is updated with Document_Image_Oid## " + dioid );
						mediatorServices.debug("TPLUnPackager: DocumentImage is updated with Document_Image_Oid## " + dioid );
					}
				}//End of If
			} //End of for

			mediatorServices.debug("TPLUnPackager: DocumentImage is updated.");
		} else {
			mediatorServices.debug("No Document items to update");
		}
    	
    }
}

