package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.Invoice;
import com.ams.tradeportal.busobj.InvoiceHistory;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;


public class TRIGPackagerMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(TRIGPackagerMediatorBean.class);

	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
	try{
		String processParameters = inputDoc.getAttribute("/process_parameters");
		String triggerType = getParm("TriggerType", processParameters);
                if (triggerType == null || triggerType.length() == 0) {
                    triggerType   = TradePortalConstants.INV_CRED_TRIGGER_TYPE;
                }
		
		String corp_org_oid = null;
		
		CorporateOrganization corpOrg = null;

		String invoiceOid = getParm("invoice_oid", processParameters);

		String messageID = inputDoc.getAttribute("/message_id");

		String amount="";
		String currency="" ;
		String buyerId="";
		String invoiceRefId="" ;
		String suppPortalInvoiceStatus =null; 
		String    credDicountCode = "";
		String  credGLCode  ="";
		String    credComments     ="";
		String otlInvoiceUoid = null;
        String docImageOid = null;
        String earlyPayAmount ="";
        String earlyPayDate ="";
        String sendToSupplierDateInd ="";
        String sendToSupplierDate ="";

		if(!TradePortalConstants.INV_FINR_TRIGGER_TYPE.equals(triggerType)  && !TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE.equals(triggerType)) {
                    mediatorServices.debug("Ready to Instantiate InvoicesSummaryData Object with oid " + invoiceOid);
                    InvoicesSummaryData invoiceSummData = (InvoicesSummaryData) mediatorServices.createServerEJB("InvoicesSummaryData", Long.parseLong(invoiceOid));
                    amount = invoiceSummData.getAttribute("amount");
                    currency = invoiceSummData.getAttribute("currency");
                    buyerId = invoiceSummData.getAttribute("buyer_id");

                    corp_org_oid = invoiceSummData.getAttribute("corp_org_oid");
                    suppPortalInvoiceStatus = null;
                    invoiceRefId = invoiceSummData.getAttribute("invoice_id");
                    //AAlubala - 11/26/2012 - CR741 Rel8.2 - START
                    //Obtain the values from INVOICE
                    credDicountCode = invoiceSummData.getAttribute("cred_discount_code");
                    credGLCode =  invoiceSummData.getAttribute("cred_gl_code");
                    credComments = invoiceSummData.getAttribute("cred_comments");		
                    //CR741 - END
         }
         else {
        	 		Invoice invoice = (Invoice) mediatorServices.createServerEJB("Invoice", Long.parseLong(invoiceOid));
        	 		corp_org_oid = invoice.getAttribute("corp_org_oid");
                    invoiceRefId = invoice.getAttribute("invoice_reference_id");
                    otlInvoiceUoid = invoice.getAttribute("otl_invoice_uoid");
                    currency = invoice.getAttribute("currency_code"); //
                    amount =   invoice.getAttribute("invoice_total_amount");
        	 		//CR 913 start
        	 		if(TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE.equals(triggerType)) {
        	 			earlyPayAmount =   invoice.getAttribute("invoice_payment_amount");
        	 			sendToSupplierDateInd =   invoice.getAttribute("send_to_supplier_date_ind");
        	 			sendToSupplierDate =   invoice.getAttribute("send_to_supplier_date");
        	 			earlyPayDate =   invoice.getAttribute("invoice_payment_date");
        	 			invoice.setAttribute("send_to_supplier_date_ind", TradePortalConstants.INDICATOR_NO);
        	 			invoice.save(false);
        	 		}
        	 		//CR 913 end
        	 		else{
        	 			suppPortalInvoiceStatus = invoice.getAttribute("supplier_portal_invoice_status");
        	 			buyerId =  invoice.getAttribute("buyer_party_identifier");
        	 			docImageOid = invoice.getAttribute("document_image_oid");
        	 		}
 
                        if (TradePortalConstants.SP_STATUS_FVD_AUTHORIZED.equals(suppPortalInvoiceStatus)) {

                                if(this.authorizingUsersStillValid(mediatorServices, invoice) == true) {
                                    invoice.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_AUTHORIZED);
                                    invoice.updateAssociatedInvoiceOfferGroup();
                                    this.createInvoiceHistory(invoice, "SP_TRANSMIT_FVD", mediatorServices);
                                    invoice.save(false);
                                } else {
                                    invoice.setAttribute("supplier_portal_invoice_status", TradePortalConstants.SP_STATUS_OFFER_ACCEPTED);
                                    this.createInvoiceHistory(invoice, "SP_INVALID_AUTHORIZER", mediatorServices);
                                    invoice.updateAssociatedInvoiceOfferGroup();
                                    invoice.save(false);
                                    //no further processing
                                    outputDoc = new DocumentHandler();
                                    outputDoc.setAttribute("/Proponix", "NoOutgoingMessage");
                                    return outputDoc;
                                }
                        }
		}


		// Find out the corporate org and instantiate corporate org
	
	    corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corp_org_oid));
	       
		String invCustomerID = corpOrg.getAttribute("Proponix_customer_id");
		


		outputDoc = new DocumentHandler();

		createHeader(corpOrg, messageID, outputDoc, mediatorServices);

        outputDoc.setAttribute("/Proponix/SubHeader/SenderCustomerID", invCustomerID);
        outputDoc.setAttribute("/Proponix/SubHeader/TriggerType", triggerType);

        outputDoc.setAttribute("/Proponix/Body/OTLInvoiceUoid", otlInvoiceUoid);
        outputDoc.setAttribute("/Proponix/Body/InvoiceId", invoiceRefId);
        if(TradePortalConstants.INV_FINR_TRIGGER_TYPE.equals(triggerType)) {
            outputDoc.setAttribute("/Proponix/Body/FinancePercentage", "100");
        }

        if(amount.charAt(0) == '-'){
        	amount = amount.substring(1);
        }
        outputDoc.setAttribute("/Proponix/Body/TriggerAmount", amount);
        outputDoc.setAttribute("/Proponix/Body/Currency", currency);
        outputDoc.setAttribute("/Proponix/Body/BuyerID", buyerId);

        if(TradePortalConstants.INV_FINR_TRIGGER_TYPE.equals(triggerType)) {
            if (TradePortalConstants.SP_STATUS_AUTHORIZED.equals(suppPortalInvoiceStatus) ||
                    TradePortalConstants.SP_STATUS_FVD_AUTHORIZED.equals(suppPortalInvoiceStatus)	) {
                    outputDoc.setAttribute("/Proponix/Body/InvoiceStatus",  "APF");
            } else if (TradePortalConstants.SP_STATUS_OFFER_DECLINED.equals(suppPortalInvoiceStatus)) {
                    outputDoc.setAttribute("/Proponix/Body/InvoiceStatus",  "DEU");
            }

            // W Zhu 17 Jun 2013 Rel 8.2 CR-708C T36000018289 Unpackage PDF documents
            if (docImageOid != null && docImageOid.length() > 0) {
                DocumentImage docImage = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", Long.parseLong(docImageOid));
                outputDoc.setAttribute("/Proponix/Body/PDFDocuments/TotalNumberOfEntries", "1");
                String documentImagePath = "/Proponix/Body/PDFDocuments/PDFDocument";

                outputDoc.setAttribute(documentImagePath + "(1)/ImageID", docImage.getAttribute("image_id"));
                outputDoc.setAttribute(documentImagePath + "(1)/DocumentText", docImage.getAttribute("doc_name"));
                outputDoc.setAttribute(documentImagePath + "(1)/NumPages", docImage.getAttribute("num_pages"));
                outputDoc.setAttribute(documentImagePath + "(1)/ImageFormat", docImage.getAttribute("image_format"));
                outputDoc.setAttribute(documentImagePath + "(1)/ImageBytes", docImage.getAttribute("image_bytes"));
                outputDoc.setAttribute(documentImagePath + "(1)/DocName", docImage.getAttribute("doc_name"));
                String imageHash = docImage.getAttribute("hash");
                if ( imageHash != null && imageHash.length()>0 ) {
                    outputDoc.setAttribute(documentImagePath +   "(1)/Hash",  imageHash );
                }
                outputDoc.setAttribute(documentImagePath + "(1)/SourceType", "W");
            }
        }
        //CR913 start
        else if(TradePortalConstants.PAYABLES_MGMT_INV_TRIGGER_MSG_TYPE.equals(triggerType)) {
        	if(StringFunction.isNotBlank(earlyPayDate)){
        		earlyPayDate = TPDateTimeUtility.getXMLFormattedDate(earlyPayDate, TPDateTimeUtility.LONG);
        		outputDoc.setAttribute("/Proponix/Body/PaymentDate",  earlyPayDate);
        	}
        	outputDoc.setAttribute("/Proponix/Body/PaymentAmount",  earlyPayAmount);
        	if(StringFunction.isNotBlank(sendToSupplierDate)){
        		sendToSupplierDate = TPDateTimeUtility.getXMLFormattedDate(sendToSupplierDate, TPDateTimeUtility.LONG);
        		outputDoc.setAttribute("/Proponix/Body/SendToSupplierDate", sendToSupplierDate);
        	}
        	outputDoc.setAttribute("/Proponix/Body/SendToSupplierDateInd",  sendToSupplierDateInd);
       						
       	}
        //CR913 end
       	else {
      		//AAlubala - 11/26/2012 - CR741 Rel8.2 - START
       		//Obtain the values from INVOICE
       		outputDoc.setAttribute("/Proponix/Body/DiscountCode", credDicountCode);
       		outputDoc.setAttribute("/Proponix/Body/DiscountGLCode",  credGLCode);
       		outputDoc.setAttribute("/Proponix/Body/DiscountComments",  credComments);
		
       		//CR741 - END
       	}
        
        
	} catch (RemoteException e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"Uploaded Invoice Packaging");
		e.printStackTrace();
	} catch (AmsException e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"Uploaded Invoice Packaging");
		e.printStackTrace();

	} catch (Exception e) {
		mediatorServices.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.TASK_NOT_SUCCESSFUL,
				"Uploaded Invoice Packaging");
		e.printStackTrace();

	}
		return outputDoc;
	}
	/**
	 * Interrogate the item�s INVOICE_STATUS. If an item picked up for processing has a INVOICE_STATUS of
	 * FVD_Authorised (instead of Authorised), the agent will now need to do additional processing for these
	 * items only.  It will:
	 *	(a)	Perform an additional new validation step to ensure that all of the users who authorised
	 *      the invoice are still valid users in the system for the corporation and they have not been deleted.
	 *      If a user is deleted, the group of FVD invoices will fail (because the user id is the same for all invoices) and must require re-authorisation.
	 *  (b)	Change the invoice status to Authorised.  To the user, the invoice will no longer
	 *      appear on the Future Value Dated page in an invoices group or in the FVD invoices list view.
	 *      It will now only appear on the �History� page.

	 * @throws Exception
	 */

	private boolean authorizingUsersStillValid(
			MediatorServices mediatorServices,
			Invoice invoice) throws RemoteException, AmsException {



		String userFirstOID  = invoice.getAttribute("first_authorizing_user_oid");
		String userSecondOID = invoice.getAttribute("second_authorizing_user_oid");

		//invoiceSummData.checkAssociation(userFirstOID); - this is checking only for existence but not for INACTIVE
		boolean firstUserInvalid = checkForValidUser(mediatorServices, userFirstOID);

		boolean secondUserInvalid = checkForValidUser(mediatorServices, userSecondOID);

		return firstUserInvalid && secondUserInvalid;
	}
        
	private boolean checkForValidUser(MediatorServices mediatorServices,
			String userOID) throws AmsException,
			RemoteException {
		boolean userValid = true;
		if (StringFunction.isNotBlank(userOID)) {
			User user = (User) mediatorServices.createServerEJB("User",Long.parseLong(userOID));
                        if (user == null) {
                            return  false;
                        }
			String status = user.getAttribute("activation_status");
			if (TradePortalConstants.INACTIVE.equals(status)) {
				userValid = false;
			}
		}
		return userValid;
	}
	private String getParm(String parmName, String processingParms) {

		int locationOfParmName, startOfParmValue, endOfParmValue;
		String parmValue;

		locationOfParmName = processingParms.indexOf(parmName);

		// Search for the whole word only.
		// For example, getParm("invoice_oid=0|insvoice_staus=Authorised",
		// "insvoice_staus" should return 1.
		// Keep searching if the character before or after the occurrence is not
		// a word delimiter

		while ((locationOfParmName > 0
				&& processingParms.charAt(locationOfParmName - 1) != ' ' && processingParms
				.charAt(locationOfParmName - 1) != '|')
				|| (locationOfParmName >= 0
						&& locationOfParmName < processingParms.length()
								- parmName.length()
						&& processingParms.charAt(locationOfParmName
								+ parmName.length()) != ' ' && processingParms
						.charAt(locationOfParmName + parmName.length()) != '=')) {

			locationOfParmName = processingParms.indexOf(parmName,
					locationOfParmName + parmName.length());

		}

		if (locationOfParmName == -1) {
			return "";
		}

		startOfParmValue = processingParms.indexOf("=", locationOfParmName);

		if (startOfParmValue == -1) {
			return "";
		}

		startOfParmValue++;

		endOfParmValue = processingParms.indexOf("|", startOfParmValue);

		if (endOfParmValue == -1) {
			endOfParmValue = processingParms.length();
		}

		parmValue = processingParms.substring(startOfParmValue, endOfParmValue);

		return parmValue;
	}
        
    private void createInvoiceHistory(Invoice invoice, String action, MediatorServices mediatorServices) throws NumberFormatException, AmsException, RemoteException {
        String corpOrgOid = invoice.getAttribute("corp_org_oid");
        CorporateOrganization corpOrgEJB = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
        String systemUserOid = corpOrgEJB.getAttribute("upload_system_user_oid");
        if (systemUserOid == null) {
            systemUserOid = "";
        }
        InvoiceHistory invHistory = (InvoiceHistory) mediatorServices.createServerEJB("InvoiceHistory");
        invHistory.newObject();                
        invHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime(true, false));// do not use override date, for easier tracking in dev region.
        invHistory.setAttribute("action", action);
        invHistory.setAttribute("invoice_status", invoice.getAttribute("supplier_portal_invoice_status"));
        invHistory.setAttribute("upload_invoice_oid", invoice.getAttribute("invoice_oid"));
        invHistory.setAttribute("user_oid", systemUserOid);
        invHistory.save();
    }
    
    private void createHeader(CorporateOrganization corpOrg,String messageID,DocumentHandler outputDoc,MediatorServices mediatorServices) throws RemoteException, AmsException{
    	

		// Find the operational org from the corporate org just instantiated
		String bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
		mediatorServices.debug("Ready to instantiate operational bank org ");
		OperationalBankOrganization operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization",Long.parseLong(bank_org_oid));
String operationOrganizationID ="";
		if (operationalBankOrganization != null)
		{
			operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
		}

		long clientBankOid = corpOrg.getAttributeLong("client_bank_oid");
		ClientBank clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank", clientBankOid);
		String clientBankID = clientBank != null ? clientBank.getAttribute("OTL_id") : "";
    	outputDoc.setAttribute("/Proponix/Header/DestinationID", TradePortalConstants.DESTINATION_ID_PROPONIX);
		outputDoc.setAttribute("/Proponix/Header/SenderID", TradePortalConstants.SENDER_ID);
		outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID", operationOrganizationID);
		outputDoc.setAttribute("/Proponix/Header/MessageType", MessageType.TRIG);
		outputDoc.setAttribute("/Proponix/Header/DateSent", DateTimeUtility.getGMTDateTime(false));
		outputDoc.setAttribute("/Proponix/Header/TimeSent", DateTimeUtility.getGMTDateTime(false).substring(11, 19));
		outputDoc.setAttribute("/Proponix/Header/MessageID", messageID);
		outputDoc.setAttribute("/Proponix/Header/ClientBankID",clientBankID);
    }
}