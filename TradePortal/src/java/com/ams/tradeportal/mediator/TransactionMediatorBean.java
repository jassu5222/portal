package com.ams.tradeportal.mediator;
import java.math.BigDecimal;
import java.math.MathContext;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.crypto.spec.SecretKeySpec;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.Account;
import com.ams.tradeportal.busobj.BankOrganizationGroup;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.DmstPmtPanelAuthRange;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.DomesticPayment;
import com.ams.tradeportal.busobj.EmailTrigger;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.InvoiceDetails;
import com.ams.tradeportal.busobj.InvoicePaymentInstructions;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.OutgoingInterfaceQueue;
import com.ams.tradeportal.busobj.PanelAuthorizationGroup;
import com.ams.tradeportal.busobj.PaymentFileUpload;
import com.ams.tradeportal.busobj.PaymentParty;
import com.ams.tradeportal.busobj.Template;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.TransactionHistory;
import com.ams.tradeportal.busobj.User;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.util.InvoiceUtility;
import com.ams.tradeportal.busobj.util.POLineItemUtility;
import com.ams.tradeportal.busobj.util.Panel;
import com.ams.tradeportal.busobj.util.PanelAuthUtility;
import com.ams.tradeportal.busobj.util.PanelRange;
import com.ams.tradeportal.busobj.util.SecurityAccess;
import com.ams.tradeportal.busobj.util.SettlementInstrUtility;
import com.ams.tradeportal.busobj.util.TradePortalAuditor;
import com.ams.tradeportal.common.InstrumentType;
import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.frame.GenericInitialContextFactory;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.frame.InvalidObjectIdentifierException;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.StringFunction;

/*
 *
 *     Copyright  2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class TransactionMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(TransactionMediatorBean.class);

	private java.lang.String completeInstrumentId = "";
	private java.lang.String templateName = "";

	//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	private static final int verifyCommitLimit = 500;
	
	private static final List<String> domesticPaymentField = new ArrayList<String>();
	static {
		domesticPaymentField.add("payment_method_type");
		domesticPaymentField.add("payee_instruction_number");
		domesticPaymentField.add("payee_fax_number");
		domesticPaymentField.add("country");
		domesticPaymentField.add("print_location");
		domesticPaymentField.add("payee_description");
		domesticPaymentField.add("payee_name");		
		domesticPaymentField.add("payee_account_number");
		domesticPaymentField.add("payee_bank_name");
		domesticPaymentField.add("payee_branch_name");
		domesticPaymentField.add("payee_bank_code");
		domesticPaymentField.add("payee_bank_country");
		domesticPaymentField.add("payee_email");
		domesticPaymentField.add("delivery_method");
		domesticPaymentField.add("mailing_address_line_1");
		domesticPaymentField.add("mailing_address_line_2");
		domesticPaymentField.add("mailing_address_line_3");
		domesticPaymentField.add("mailing_address_line_4");		
		domesticPaymentField.add("payee_address_line_1");
		domesticPaymentField.add("payee_address_line_2");
		domesticPaymentField.add("payee_address_line_3");
		domesticPaymentField.add("payee_address_line_4");
		domesticPaymentField.add("central_bank_reporting_1");
		domesticPaymentField.add("central_bank_reporting_2");
		domesticPaymentField.add("central_bank_reporting_3");
		domesticPaymentField.add("payable_location");
		domesticPaymentField.add("customer_reference");
		domesticPaymentField.add("payment_status");
		domesticPaymentField.add("amount");
		domesticPaymentField.add("payee_invoice_details");
	}

	// Business methods
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {


		// Based on the button clicked, process an appropriate action.

		// Note that Authorize is not handled by this mediator.  There is
		// another mediator that processes authorizations.

		String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
		LOG.debug("The button pressed is " + buttonPressed);		
		// Nar Rel 9.2.0.0 04-Sep-2014 ADD - Begin
		// checking the login user security rights for performing action.
		if (inputDoc.getDocumentNode("/Template") != null) {
			if( !SecurityAccess.hasRights(inputDoc.getAttribute("/LoginUser/security_rights"), SecurityAccess.MAINTAIN_TEMPLATE ) ){
				 throw new AmsException("User is trying to maintain Template without proper access level");
			}
		}else{
			String uploaded_ind = inputDoc.getAttribute("/Transaction/uploaded_ind");
			if(TradePortalConstants.INDICATOR_NO.equals(uploaded_ind) || StringFunction.isBlank(uploaded_ind)){ 
					SecurityAccess.validateUserRightsForTrans(
							inputDoc.getAttribute("/Instrument/instrument_type_code"),
							inputDoc.getAttribute("/Transaction/transaction_type_code"),
							inputDoc.getAttribute("/LoginUser/security_rights"),
							inputDoc.getAttribute("/Update/ButtonPressed"));
			}
			/* KMehta IR-T36000034712 Rel 9500 on 10-Dec-2015 Add  - Begin*/
			if ( (buttonPressed.equals(TradePortalConstants.BUTTON_SAVECLOSETRANS) || 
					buttonPressed.equals(TradePortalConstants.BUTTON_SAVETRANS) || buttonPressed.equals(TradePortalConstants.BUTTON_VERIFY)) && 
					InstrumentType.LOAN_RQST.equals(inputDoc.getAttribute("/Instrument/instrument_type_code"))) {
						String financeType = inputDoc.getAttribute("/Terms/finance_type");
						if(TradePortalConstants.TRADE_LOAN_PAY.equalsIgnoreCase(financeType)){
							validatePayablesLoanProceedsTo(inputDoc, mediatorServices);
						}
				         
			}
			/* KMehta IR-T36000034712 Rel 9500 on 10-Dec-2015 Add  - End*/

			// Nar IR-T36000037563 Rel 9.2.0.0 05/20/2015 Begin
			if ( (buttonPressed.equals(TradePortalConstants.BUTTON_SAVECLOSETRANS) || 
					buttonPressed.equals(TradePortalConstants.BUTTON_SAVETRANS) || buttonPressed.equals(TradePortalConstants.BUTTON_VERIFY)) && 
					InstrumentType.DOM_PMT.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")) && 
					isFixedPaymentInstrument(inputDoc.getAttribute("/Instrument/instrument_oid")) ) {
				
				         validateInstrumentDatawithFixedTempData(inputDoc, mediatorServices);
			}
			// Nar IR-T36000037563 Rel 9.2.0.0 05/20/2015 End
		}
		// Nar Rel 9.2.0.0 04-Sep-2014 ADD - End
		if (buttonPressed.equals(TradePortalConstants.BUTTON_ROUTE)) {
			outputDoc = performRoute(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_VERIFY)) {
			outputDoc = performVerify(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_VERIFY_FX)) {
			outputDoc = performVerifyFX(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_EDIT)) {
			outputDoc = performEdit(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVE)) {
			outputDoc = performSaveTemplate(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_PAYEE)) {
			outputDoc = performSaveTemplate(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_PAYEE_TRAN)) {
			outputDoc = performSave(inputDoc, mediatorServices);
		} else if ((buttonPressed.equals(TradePortalConstants.BUTTON_MODIFY_PAYEE))
				|| (buttonPressed.equals("UploadPaymentFile"))){
			return inputDoc;
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_PAYEE)) {
			outputDoc = deleteDomesticPayment(inputDoc, mediatorServices, true, true);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_SELECT_TEMPLATE)) {
			outputDoc = performCopyFromTemplate(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE)) {
			outputDoc = performDeleteTemplate(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_TIMEOUT)) {
			mediatorServices.getCSDB().setCSDBValue("timeout", TradePortalConstants.INDICATOR_YES);
			outputDoc = performSave(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_TEMPLATETIMEOUT)) {
			mediatorServices.getCSDB().setCSDBValue("timeout", TradePortalConstants.INDICATOR_YES);
			outputDoc = performSaveTemplate(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_SHIPMENT)) {
			outputDoc = deleteShipment(inputDoc, mediatorServices);
			performSave(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS)) {
			outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
			performSave(inputDoc, mediatorServices);
		} else  if (buttonPressed.equals(TradePortalConstants.BUTTON_SAVECLOSETRANS) || buttonPressed.equals(TradePortalConstants.BUTTON_SAVETRANS) || buttonPressed.equals(TradePortalConstants.BUTTON_ADDSAVETRANS)) {
			mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_SAVE);
			outputDoc = performSave(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_REJECT)) {
			outputDoc = performReject(inputDoc, mediatorServices);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_CONFIRM)) {
			outputDoc = performConfirm(inputDoc, mediatorServices);
		}  else if (buttonPressed.equals(TradePortalConstants.BUTTON_VIEW_STRUCTURED_PO) &&
				    TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/Page/isReadOnly"))) {
			outputDoc.setAttribute("/Refresh/Transaction", TradePortalConstants.INDICATOR_YES);
   	    }else if ((buttonPressed.equals(TradePortalConstants.BUTTON_REMOVE_STRUCTURED_PO) || buttonPressed.equals(TradePortalConstants.BUTTON_ADD_STRUCTURED_PO))) {
			outputDoc.setAttribute("/Refresh/Transaction", TradePortalConstants.INDICATOR_YES);
		} else if (buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_INV_PAYEE)) {
			outputDoc = deleteInvoicePaymentInstruction(inputDoc, mediatorServices, true, true);
		}else if (TradePortalConstants.BUTTON_CALC_DIST_AND_INTRST.equals(buttonPressed)) {
			inputDoc = calculateInterestDiscount(inputDoc, mediatorServices, InstrumentType.LOAN_RQST);
			outputDoc = performSave(inputDoc, mediatorServices);
	   }else if (TradePortalConstants.BUTTON_ADD_TRANS_INVOICES.equals(buttonPressed)) {


		   String importIndicator = inputDoc.getAttribute("/Instrument/import_indicator");
		   String financeType = inputDoc.getAttribute("/Terms/finance_type");

		   if (StringFunction.isBlank(importIndicator) ||
				   (StringFunction.isBlank(financeType) && !TradePortalConstants.INDICATOR_NO.equals(importIndicator))) {
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.MISSING_LOAN_TYPE_FOR_ADD_INVOICES);

			}
		   if (!StringFunction.isBlank(financeType) && TradePortalConstants.OTHER.equals(financeType)
					   ||TradePortalConstants.PRESHIPMENT_FINANCING.equals(financeType)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.NO_ADD_INV_TO_EXP_LOAN);
		   }else{
			   outputDoc = performSave(inputDoc, mediatorServices);
		   }

	   }else if (TradePortalConstants.BUTTON_SEND_FOR_AUTH.equals(buttonPressed)){
		   outputDoc = performSendForAuth(inputDoc, mediatorServices);
	   }else if (TradePortalConstants.BUTTON_SEND_FOR_REPAIR.equals(buttonPressed)){
		   outputDoc = performSendForRepair(inputDoc, mediatorServices);
	   }else if (TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER.equals(buttonPressed)){
		   outputDoc = performNotifyPanelAuthUser(inputDoc, mediatorServices);
	   } else if (TradePortalConstants.BUTTON_VIEW_TRANS_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_VIEW_INV.equals(buttonPressed)) {
		  //do nothing to avoid saving of blank values.
	   } else if (TradePortalConstants.BUTTON_APPLY_UPDATES_REPORTING_CODES.equals(buttonPressed)) {
		   outputDoc = performReportingCodesUpdate(inputDoc, mediatorServices);
	   }else {

			performSave(inputDoc, mediatorServices);
	   }

		return outputDoc;
	}


	/**
	 * Route can be called in two instances.  One, when the transaction is
	 * still in STARTED status and can thus still be edited.  Two, after the
	 * transaction is out of STARTED status and is basically in readonly mode.
	 *
	 * In the latter case we do NOT want to save the data.  (This is
	 * because the preedit logic has reset and unsent checkbox values to N.
	 * In readonly mode, no data is sent up and therefore all checkbox
	 * value would be reset.  This would be bad.)
	 *
	 * In the former case (when status is STARTED), we save the data.
	 *
	 * In either case, no route logic is handled here.  We forward the user
	 * to the route transaction page.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performRoute(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Transaction Route");

		DocumentHandler outputDoc = new DocumentHandler();

		try {
			String status = inputDoc.getAttribute("/Transaction/transaction_status");
			if (!status.equals(TransactionStatus.STARTED)) {
				// Status is not started so we don't want to save, just continue.
				return outputDoc;
			}

			// set the last entry user to the user who just Saved
			// the transaction
			inputDoc.setAttribute("/Transaction/last_entry_user_oid",
					inputDoc.getAttribute("/LoginUser/user_oid"));

			outputDoc = saveTransaction(false, inputDoc, mediatorServices);
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performRoute");
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}




	/**
	 * This method handles an instrument/transaction save without validate
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performSave(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		DocumentHandler outputDoc = new DocumentHandler();
		//Rel9.0 IR#T36000030015 - If status is Authorized, should not be able to save [start]
		try {
			String transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");
			Transaction transaction =(Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
			String tranStatus = transaction.getAttribute("transaction_status");
			if(TransactionStatus.AUTHORIZED.equals(tranStatus)  || TransactionStatus.FVD_AUTHORIZED.equals(tranStatus)){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AUTH_TRANS_NOT_UPDATEABLE);
				return outputDoc;
			}
		} catch (NumberFormatException e1) {
			LOG.debug("Error in TransactionMediatorBean obtaining transoid and trans status");
			e1.printStackTrace();
		}
		//[End]
		// If we are saving a *template* in multi-part mode, we need to call the performSaveTemplate method
		// in order to execute the correct save logic
		if (inputDoc.getDocumentNode("/Template") != null)
		{
			performSaveTemplate(inputDoc, mediatorServices);

			return outputDoc;
		}

		LOG.debug("Performing Transaction Save");
		
		try {

			// set the last entry user to the user who just Saved
			// the transaction
			inputDoc.setAttribute("/Transaction/last_entry_user_oid",
					inputDoc.getAttribute("/LoginUser/user_oid"));

			outputDoc = saveTransaction(false, inputDoc, mediatorServices);
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performSave");
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}





	/**
	 * This method handles an instrument/transaction save with validate (i.e.
	 * a verify).
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performVerify(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Transaction Verify");
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_VERIFY);     	// NSX CR-574 09/21/10

		DocumentHandler outputDoc = new DocumentHandler();

		try {
			// We will attempt to save the instrument/transaction with
			// a READY TO AUTHORIZE status.  If there are any errors, the
			// status will rollback and not be saved.  The assigned to
			// user should be set to the current user.  We will set this
			// in the input document.

			// if the user is an ADMIN user acting on behalf on the org (through customer access)
			// don't reassign the user to the ADMIN user
			User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));

			if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE))
			{
				inputDoc.setAttribute("/Transaction/assigned_to_user_oid",
						inputDoc.getAttribute("/LoginUser/user_oid"));
			}


			// set the last entry user to the user who just Saved
			// the transaction

			inputDoc.setAttribute("/Transaction/last_entry_user_oid",
					inputDoc.getAttribute("/LoginUser/user_oid"));
					
			outputDoc = saveTransaction(TransactionStatus.READY_TO_AUTHORIZE,
					true, inputDoc, mediatorServices);
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performVerify");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}

	/**
	 * This method handles transaction save and Verify FX details - Only for Payment Upload Process...
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performVerifyFX(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Transaction Verify FX");
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_VERIFY);

		DocumentHandler outputDoc = new DocumentHandler();

		try {
			// if the user is an ADMIN user acting on behalf on the org (through customer access)
			// don't reassign the user to the ADMIN user
			User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));
			if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE))
				inputDoc.setAttribute("/Transaction/assigned_to_user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));

			// set the last entry user to the user who just Saved
			// the transaction
			inputDoc.setAttribute("/Transaction/last_entry_user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));

			outputDoc = saveTransaction(TransactionStatus.READY_TO_AUTHORIZE,true, inputDoc, mediatorServices);

		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performVerifyFX");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is " + this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}


	/**
	 * This method updates the status of the transaction back to STARTED.
	 * In theory this is the only field that is updated since the screen should
	 * have been in read-only mode (and therefore nothing was sent, except for
	 * the secure parms).
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performEdit(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Transaction Edit");
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_EDIT);     	// NSX IR# HNUL021063380

		DocumentHandler outputDoc = new DocumentHandler();
		String transactionOid = "0"; //VShah - CR564
		Transaction     transaction;//VShah - CR564
		transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");

		try {
			// set the last entry user to the user who just Saved
			// the transaction
			inputDoc.setAttribute("/Transaction/last_entry_user_oid",
					inputDoc.getAttribute("/LoginUser/user_oid"));
			// Since the transaction may have been partially authorized when the 'Edit Data'
			// button was pressed, the attributes that keep track of authorization information
			// must be cleared
			inputDoc.setAttribute("/Transaction/first_authorizing_user_oid", "");
			inputDoc.setAttribute("/Transaction/second_authorizing_user_oid", "");
			 String sqlStatementToPanelAuth = "delete from panel_authorizer where owner_object_type = 'TRANSACTION' and p_owner_oid=?";
			 DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new Object[]{transactionOid});

			// if user perform edit action, panel group oid and opt lock value needs to be clean as these value set
			// at verify time.
			 inputDoc.setAttribute("/Transaction/panel_auth_group_oid", "");
			 inputDoc.setAttribute("/Transaction/panel_oplock_val", "");
			 inputDoc.setAttribute("/Transaction/panel_auth_range_oid", "");
			 inputDoc.setAttribute("/Transaction/panel_auth_next_approvals", "");
			// delete data related beneficiary panel auth during edit mode. this data is set at verify.
			 inputDoc.setAttribute("/Transaction/bene_panel_auth_ind", TradePortalConstants.INDICATOR_NO);
			 sqlStatementToPanelAuth = "DELETE FROM dmst_pmt_panel_auth_range WHERE p_transaction_oid = ?";
			 DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new Object[]{transactionOid});
			 sqlStatementToPanelAuth = "UPDATE domestic_payment SET a_dmst_pmt_panel_auth_range = null WHERE p_transaction_oid = ?";
			 DatabaseQueryBean.executeUpdate(sqlStatementToPanelAuth, true, new Object[]{transactionOid});
			// We will attempt to save the instrument/transaction with
			// a STARTED status.  If there are any errors, the
			// status will rollback and not be saved.
			String origStatus = inputDoc.getAttribute("/Transaction/transaction_status");

			//Get the transaction oid from the doc
			transaction = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
			if ( (InstrumentServices.isValidTransStatusForVerifiedStatus(origStatus)) &&
				InstrumentType.DOM_PMT.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")) &&
				TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")) )
			{
				outputDoc = saveTransaction(TransactionStatus.VERIFIED,false, inputDoc, mediatorServices);
			}
			else
			{
			outputDoc = saveTransaction(TransactionStatus.STARTED,
					false, inputDoc, mediatorServices);
			}

			if (origStatus.equalsIgnoreCase(TransactionStatus.FVD_AUTHORIZED))
			{
			     try
			     {
		 			long queue_oidLong = 0;
		 			String sqlStatement = "select QUEUE_OID from OUTGOING_QUEUE " +
 												"where A_INSTRUMENT_OID = ?  and A_TRANSACTION_OID=?";
		 			String instrumentOid = ""+inputDoc.getAttributeLong("/Instrument/instrument_oid");
		 			String transactionId = ""+inputDoc.getAttributeLong("/Transaction/transaction_oid");
		 			DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement, false, instrumentOid, transactionId);

		 			if (resultXML != null)
		 				queue_oidLong = resultXML.getAttributeLong("/ResultSetRecord(0)/QUEUE_OID");

					LOG.debug("TRANMED::Edit:: Queue OID to delete: " + queue_oidLong);

		            if(queue_oidLong != 0)
		            {
						LOG.debug("TRANMED::Edit:: Future Value Payment Transaction being deleted with queue oid: " + queue_oidLong);
						OutgoingInterfaceQueue outgoing_queue = (OutgoingInterfaceQueue) mediatorServices.createServerEJB("OutgoingInterfaceQueue", queue_oidLong);
	                	outgoing_queue.delete();
	            	}
		         } catch (RemoteException e) {
			         throw new AmsException("Remote exception found while deleting Future Value Payment transaction.Error: "  + e.toString());
		         }
			 }

			//cquinton 2/10/2011 Rel 6.1.0 ir#rrul021059464 start
			//clear out payment status and beneficiary sequence id
            if ( InstrumentType.DOM_PMT.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")) ) {
                //use sql instead of ejbs for performance
               try(Connection con = DatabaseQueryBean.connect(true);
                PreparedStatement pStmt = con.prepareStatement(
                        "update DOMESTIC_PAYMENT set PAYMENT_STATUS = null " +
                        "where p_transaction_oid = ?")){
                pStmt.setString(1, transactionOid);
                pStmt.executeUpdate();
               }catch (SQLException e) {
       				e.printStackTrace();
    		}
               
            }
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performEdit");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}





	/**
	 * This method uses the input document to get instances of instrument
	 * and transaction, populate them (including subcomponents of transction)
	 * and save them.  The doValidate parameter indicates if the save should
	 * be done with or without validation logic by the beans.
	 *
	 * This method catches an invalid object identifier.  All other exceptions
	 * should be caught by the caller.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler saveTransaction(boolean doValidate, DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		return saveTransaction("", doValidate, inputDoc, mediatorServices);
	}

	/**
	 * Getter for the complete instrument id attribute.
	 *
	 * @return java.lang.String
	 */
	private java.lang.String getCompleteInstrumentId() {
		return completeInstrumentId;
	}

	/**
	 * Getter for template name attribute
	 *
	 * @return java.lang.String
	 */
	private java.lang.String getTemplateName() {
		return templateName;
	}

	/**
	 * Performs a delete on the template record and the corresponding instrument
	 * record (which delete transaction, terms, termsparty components).  An
	 * audit record is created.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performDeleteTemplate(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Template Delete");

		DocumentHandler outputDoc = new DocumentHandler();

		Template template = null;
		long templateOid = 0;
		String ownerOrgOid = "";

		try {
			// Get the template oid from the doc.
			templateOid = inputDoc.getAttributeLong("/Template/template_oid");
			if (templateOid == 0) {
				LOG.debug("Error: template_oid not present");
				return null;
			}

			//IAZ CM-451 10/29/08: If this is Domestic Payment, need to delete
			//      all Domestic Payments entered with the template.
			//      Delete them first as to ensure related data is not left in
			//      the database aftre template is deleted.
			String templateTypeCode = inputDoc.getAttribute("/Instrument/instrument_type_code");


			if ((InstrumentType.DOM_PMT.equals(templateTypeCode)) ||
					(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(templateTypeCode)))	//IAZ CM CR-509 12/29/09
			{
				outputDoc = processMultipleDomesticPayments(inputDoc, mediatorServices,TradePortalConstants.BUTTON_DELETE, null);
			}


			// Get a handle to an instance of the template.
			template = (Template) mediatorServices.createServerEJB("Template");

			LOG.debug("Mediator: Retrieving template " + templateOid);
			template.getData(templateOid);

			setTemplateName(template.getAttribute("name"));
			ownerOrgOid = template.getAttribute("owner_org_oid");

			LOG.debug("before template delete");
			template.delete();
			LOG.debug("after template delete");

			LOG.debug("Mediator: Completed the delete of template");

		} catch (InvalidObjectIdentifierException ex) {
			// If the object does not exist, issue an error
			mediatorServices.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performDeleteTemplate");
			LOG.info("  " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				// Now try to create an audit record.
				LOG.debug("Mediator: Creating audit record");
				TradePortalAuditor.createRefDataAudit(mediatorServices,
						template,
						String.valueOf(templateOid),
						getTemplateName(),
						ownerOrgOid,
						"D",
						inputDoc.getAttribute("/LoginUser/user_oid"));
				LOG.debug("Mediator: Finished with audit record");

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DELETE_SUCCESSFUL,
						getTemplateName());
			}

		}
		return outputDoc;

	}

	/**
	 * Performs a save on the template record.  It calls the saveTransaction
	 * method to save the instrument, transaction, terms, and terms parties.
	 * The template 'save' does validation, the instrument, etc. 'save' do not
	 * validate.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performSaveTemplate(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Template Save");

		DocumentHandler outputDoc = new DocumentHandler();

		Template template = null;
		long templateOid = 0;
		String ownerOrgOid = "";

		try {
			// Get the instrument oid from the doc.
			templateOid = inputDoc.getAttributeLong("/Template/template_oid");
			if (templateOid == 0) {
				LOG.debug("Error: template_oid not present");
				return null;
			}

			// Get a handle to an instance of the template.
			template = (Template) mediatorServices.createServerEJB("Template");

			LOG.debug("Mediator: Retrieving template " + templateOid);
			template.getData(templateOid);

			template.populateFromXmlDoc(inputDoc);
			setTemplateName(template.getAttribute("name"));

			// Force the template object to save by changing
			// one of its attributes
			// The template object must be saved to the database
			// in order for optimistic locking to be enforced
			// If nothing is changed on the template object (even
			// though plenty could have been changed on the
			// related instrument and transaction), then it is
			// not saved and opt. locking is not enforced.
			template.setAttribute("name", "!!!");
			template.setAttribute("name", getTemplateName());

			ownerOrgOid = template.getAttribute("owner_org_oid");


			// We've finished saving the template.  Now save the instrument
			// and transaction data for this template.
			outputDoc = saveTransaction(false, inputDoc, mediatorServices);


			// Save  the template after the Validations of
			// Instrument and Transactions  happens successfully,
			// if it fails we don't need to save template and increase the
			// optimistic lock
			template.save(true);

			LOG.debug("Mediator: Completed the save of template");

		} catch (InvalidObjectIdentifierException ex) {
			// If the object does not exist, issue an error
			mediatorServices.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performSaveTemplate");
			LOG.info("  " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				// Now try to create an audit record.
				LOG.debug("Mediator: Creating audit record");
				TradePortalAuditor.createRefDataAudit(mediatorServices,
						template,
						String.valueOf(templateOid),
						getTemplateName(),
						ownerOrgOid,
						"U",
						inputDoc.getAttribute("/LoginUser/user_oid"));
				LOG.debug("Mediator: Finished with audit record");

				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getTemplateName());
			}

		}
		return outputDoc;

	}

	/**
	 * This method uses the input document to get instances of instrument
	 * and transaction, populate them (including subcomponents of transction)
	 * and save them.  The doValidate parameter indicates if the save should
	 * be done with or without validation logic by the beans.
	 *
	 * If a transaction status is passed in, it is used to update the status
	 * of the transaction.  This allows a transaction to go from STARTED to
	 * READY_TO_AUTHORIZE after verification.
	 *
	 * This method catches an invalid object identifier.  All other exceptions
	 * should be caught by the caller.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler saveTransaction(String transStatus, boolean doValidate, DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		DocumentHandler outputDoc = new DocumentHandler();		
		DocumentHandler preProcessPartyDoc = inputDoc;
		LOG.debug("Processing save");

		// Declare variables
		Transaction transaction = null;
		Terms terms = null;
		Instrument instrument = null;
		Hashtable emptyTermsPartyInfo = null;
		CorporateOrganization transactionCorpOrg = null;

		long instrumentOid = 0;
		long transactionOid = 0;

		String counterParty;		
		
		// Perform update.
		try {
			// Get the instrument oid from the doc.
			instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
			if (instrumentOid == 0)
			{
				LOG.debug("Error: instrument_oid not present");
				return null;
			}

			// Get the transaction oid from the doc.
			transactionOid = inputDoc.getAttributeLong("/Transaction/transaction_oid");
			if (transactionOid == 0)
			{
				LOG.debug("Error: transaction_oid not present");
				return null;
			}

			// Get a handle to an instance of the instrument, the transaction,
			// and the terms objects.
			instrument = (Instrument) mediatorServices.createServerEJB("Instrument");

			LOG.debug("Mediator: Retrieving instrument " + instrumentOid);
			instrument.getData(instrumentOid);
			setCompleteInstrumentId	(instrument.getAttribute("complete_instrument_id"));

			LOG.debug("Mediator: Getting handle to transaction " + transactionOid);
			transaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);


			// The user is not allowed to enter a negative number
			// This cannot be validated through amount's datatype because
			// negative numbers must be stored for amendments.  However,
			// for amendments, the user still should not enter the negative sign.
			// Instead, they select "Decrease" from the increase / decrease flag
			String amount = inputDoc.getAttribute("/Terms/amount");
			String fecAmount = inputDoc.getAttribute("/Terms/fec_amount");
			if (StringFunction.isNotBlank(fecAmount) && fecAmount.trim().startsWith("-")) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FEC_NEGATIVE_AMOUNT);
			}

			// Check to see if negative sign exists... If it does, issue an error
			if( (amount != null) && amount.trim().startsWith("-") )
			{
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NEGATIVE_AMOUNT);
			}
			else
			{
				// If the increase / decrease flag is set to decrease,
				// change the number into a negative one to be stored
				String increaseDecreaseFlag = inputDoc.getAttribute("/Terms/IncreaseDecreaseFlag");

				if (!StringFunction.isBlank(increaseDecreaseFlag))
				{
					if (increaseDecreaseFlag.equals(TradePortalConstants.DECREASE))
					{
						resetAmountToNegative(inputDoc);
					}
				}
			}

			// First, remove any empty terms party frgaments from the input xml doc
			// and and delete any necessary terms parties that were cleared in the
			// transaction page.
			emptyTermsPartyInfo = getEmptyTermsPartyInfo(inputDoc);

			inputDoc = removeEmptyTermsParties(inputDoc, emptyTermsPartyInfo);

			// Set object attributes equal to input arguments.  Because of the
			// way the inputDocument is set up, we need to do this separately
			// for instrument, transaction, and terms.  The terms party objects
			// populate themselves from terms.
			Hashtable nonEmptytermsPartyInfo = getEmptyTermsPartyInfo(inputDoc);

			inputDoc = clearTermsPartiesInfo(inputDoc, nonEmptytermsPartyInfo, mediatorServices);

			instrument.populateFromXmlDoc(inputDoc);

			transaction.populateFromXmlDoc(inputDoc);

			if (StringFunction.isNotBlank(transStatus))
			{
				String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
				if ((TradePortalConstants.BUTTON_VERIFY.equals(buttonPressed) || TradePortalConstants.BUTTON_VERIFY_FX.equals(buttonPressed) ||
						 (TradePortalConstants.BUTTON_CONFIRM.equals(buttonPressed) && TransactionStatus.READY_TO_AUTHORIZE.equals(transStatus))) 
						 && isCheckerWorkflowEnabled(instrument, transaction, mediatorServices)) {
		           // set transaction status to Ready To check
			       transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_CHECK);
		        }else {
		            transaction.setAttribute("transaction_status", transStatus);
		        }

				transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());

			}
			/*Rel 9.5 CR 1132 - if buttonpressed = 'verify' then, 
			 * get utilize_additional_bank_fields indicator value from client bank and 
			 * set it to client_bank_ind of transaction*/
			if(StringFunction.isNotBlank(instrument.getAttribute("client_bank_oid"))){ // IR T36000048054 Rel9.5 04/18/15
				long tempClientBankOid = 0;
				ClientBank client_Bank = null;
				String utilizeAdditionalBankFields = null;
				tempClientBankOid = instrument.getAttributeLong("client_bank_oid");
				client_Bank = (ClientBank) mediatorServices.createServerEJB("ClientBank");
				
				client_Bank.getData(tempClientBankOid);
				
				utilizeAdditionalBankFields = client_Bank.getAttribute("utilize_additional_bank_fields");
				if( TradePortalConstants.BUTTON_VERIFY.equals(inputDoc.getAttribute("/Update/ButtonPressed")) ){
					
					if(StringFunction.isBlank(utilizeAdditionalBankFields)){
						utilizeAdditionalBankFields = TradePortalConstants.INDICATOR_NO;
					}
					
					LOG.debug("Mediator: Retrieving Client Bank {} ", tempClientBankOid);
					LOG.debug("utilize_additional_bank_fields = {}",utilizeAdditionalBankFields);
					LOG.debug("Button Pressed = {}",TradePortalConstants.BUTTON_VERIFY);
					//set utilize_additional_bank_fields value to client_bank_ind
					transaction.setAttribute("client_bank_ind", utilizeAdditionalBankFields);
					
				}else{
					if(StringFunction.isBlank(utilizeAdditionalBankFields)){
						utilizeAdditionalBankFields = TradePortalConstants.INDICATOR_NO;
					}
					
					LOG.debug("Mediator: Retrieving Client Bank {} ", tempClientBankOid);
					LOG.debug("utilize_additional_bank_fields = {}",utilizeAdditionalBankFields);
					LOG.debug("Button Pressed = {}",inputDoc.getAttribute("/Update/ButtonPressed"));
					//set utilize_additional_bank_fields value to client_bank_ind
					transaction.setAttribute("client_bank_ind", utilizeAdditionalBankFields);
				}
			}
			
						
			// NAR Rel 8.3.0.0 CR-821 Begin
			// if user perform verify action and statuc is going to be set as ready to authorizr, set
			// group oid and opt lock value from PanelAuthorizationGroup object
			if (TransactionStatus.READY_TO_AUTHORIZE.equals(transStatus) &&
					TradePortalConstants.BUTTON_VERIFY.equals(inputDoc.getAttribute("/Update/ButtonPressed")) &&
					!TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/ProcessBenePanelProcess"))) {
				String transactionCorpOrgOid = instrument.getAttribute("corp_org_oid");
				String instrType = instrument.getAttribute("instrument_type_code");
				String transactionType = transaction.getAttribute("transaction_type_code");
				transactionCorpOrg = getCorporateOrg(transactionCorpOrgOid, mediatorServices);
				// If it is a H2H and is STA, skip panel check
				if(!(transaction.isH2HSentPayment() && transactionCorpOrg.isStraightThroughAuthorize())){
                 // check if panel Authorization is applicable for instrument type
				 if (transactionCorpOrg.isPanelAuthEnabled(instrType, transactionType)) {      
					performPanelProcessing(transactionCorpOrg, transaction, instrument, inputDoc, mediatorServices);				  
				  }									
				 //NAR Rel 8.3.0.0 CR-821 End
				}
			}

			// IAZ CM-451 10/29/08 Begin.
			// Domestic Payments Data: Save/ Verify/ CopyFromTemplate
			String orderingPartyToRemoveOid = null;												//IAZ CM CR-507 12/19/09 Add
			String orderingPartyBankToRemoveOid = null;											//IAZ CM CR-507 12/19/09 Add
			String templateTypeCode = inputDoc.getAttribute("/Instrument/instrument_type_code");
			
			//RKAZI CR-709 02/02/2013 update InvoicePaymentINstructions - Start
			String loanProceedsCreditType = inputDoc.getAttribute("/Terms/loan_proceeds_credit_type");
			if (StringFunction.isNotBlank(templateTypeCode) &&
					(templateTypeCode.equals(InstrumentType.LOAN_RQST) ) && TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType))
			{

				boolean updateInvPayInstErrorFree = true;
				String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
				if (StringFunction.isBlank(buttonPressed)||
						(!buttonPressed.equals(TradePortalConstants.BUTTON_SELECT_TEMPLATE)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_EDIT)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_PAYEE)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_DELETE)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_ADD_TRANS_INVOICES)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_VIEW_TRANS_INVOICES)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_REMOVE_TRANS_INVOICES)))
				{
					//IR 29777 - if invoices are attached, just set terms party related fields, else continue with existing logic
					if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/areInvoicesAttached"))){
						Object[] sqlParams = new Object[1];
						  sqlParams[0] =  transaction.getAttribute("transaction_oid");
						  int numberOfDomPmts = DatabaseQueryBean.getCount("a_transaction_oid", "invoice_beneficiary_view", "a_transaction_oid = ?", false, sqlParams);
						  if(numberOfDomPmts>1){
						inputDoc.setAttribute("/Terms/FirstTermsParty/name",
								mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));

						inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
								mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
						inputDoc.setAttribute("/Terms/FirstTermsParty/terms_party_type", TermsPartyType.BENEFICIARY);
					
						inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_1", "");
						inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_2", "");
						inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_3", "");
						inputDoc.setAttribute("/Terms/reference_number", mediatorServices.getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE));
						  }
					}
					else {
					
						updateInvPayInstErrorFree = updateInvPaymentInstructions(inputDoc, outputDoc, mediatorServices);
					
					if (!updateInvPayInstErrorFree)
					{
						transaction.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.DOM_PMT_TRAN_NOT_UPDATED,
								mediatorServices.getResourceManager().getText(templateTypeCode + ".FirstParty",
										TradePortalConstants.TEXT_BUNDLE));
					}else{
						    String sb = "SELECT inv_pay_inst_oid from invoice_payment_instructions ipi where ipi.p_transaction_oid = ?";
							DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sb, false, new Object[]{transaction.getAttribute("transaction_oid")});
							if (resultsDoc != null){
								List<DocumentHandler> resultsVec = resultsDoc.getFragmentsList("/ResultSetRecord");
								Vector newInvPayInsOidList = new Vector();
								for (DocumentHandler doc : resultsVec) {
									String oid = doc.getAttribute("/INV_PAY_INST_OID");
									newInvPayInsOidList.add(oid);

								}
								//IR29777 start
								updateTermsParty(inputDoc,mediatorServices,newInvPayInsOidList.size());
								//
								
								terms = transaction.populateCustomerTerms(inputDoc);
								deriveTransactionCurrencyAndAmountsFromInvoicePaymentInstructions(transaction, terms, newInvPayInsOidList, instrument, null, inputDoc);

							}
						}
					}
				}
			}
			
			
			//RKAZI CR-709 02/02/2013 update InvoicePaymentINstructions -End
			if (StringFunction.isNotBlank(templateTypeCode) &&
					((templateTypeCode.equals(InstrumentType.DOM_PMT)) ||
							(templateTypeCode.equals(InstrumentType.DIRECT_DEBIT_INSTRUCTION))))		//NShrestha CR-509
			{
				//Update/Save Domestic Payment as requested on the page.
				//IAZ CM-451 12/08/08 Begin
				//Update payee list unless just copied those from another template,
				//or goes back from readOnly to Edit mode or this is a Delete
				boolean updateDPErrorFree = true;
				String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
				if (StringFunction.isBlank(buttonPressed)||
						(!buttonPressed.equals(TradePortalConstants.BUTTON_SELECT_TEMPLATE)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_EDIT)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_DELETE_PAYEE)) &&
						(!buttonPressed.equals(TradePortalConstants.BUTTON_DELETE))
				)
				{
					//skip updateDomesticPayment if it is Uploaded Payment
					if (!TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind"))) {    //NSX - CR-564
						updateDPErrorFree = updateDomesticPayment (inputDoc, outputDoc, mediatorServices);
					if (!updateDPErrorFree)
					{
						transaction.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.DOM_PMT_TRAN_NOT_UPDATED,
								mediatorServices.getResourceManager().getText(templateTypeCode + ".FirstParty",		// IAZ 01/14/10 IR-PUK011354564 Add
										TradePortalConstants.TEXT_BUNDLE));		// IAZ 01/14/10 IR-PUK011354564 Add
					}
					//IAZ CM CR-507 12/19/09 and 01/12/10 Begin
					else
					{
						orderingPartyToRemoveOid = updatePaymentOrderingParty (inputDoc, "OrderingParty",
								mediatorServices, transaction);
						orderingPartyBankToRemoveOid = updatePaymentOrderingParty (inputDoc, "OrderingPartyBank",
								mediatorServices, transaction);
					}
					//IAZ CM CR-507 12/19/09 and 01/12/10 End
					}
				}
				//IAZ CM-451 12/08/08 End

				//If this is "Verify" -- validate payment date and all Payees' data
				//unless an issue is already found with payee just entered on the page
				if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
				{                                                                                                      // NSX IR# POUL022267716 Rel. 6.1.0
                       Panel panel = null;
                       String baseCurrency = null;
                       String transactionOrg =  instrument.getAttribute("corp_org_oid");
					   if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind")) &&
							   InstrumentType.DOM_PMT.equals(templateTypeCode) && 
							   !TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/ProcessBenePanelProcess"))){
					     panel =  new Panel(transaction.getAttribute("panel_auth_group_oid"), transaction.getAttribute("panel_oplock_val"), 
					    		 instrument.getAttribute("instrument_type_code"), mediatorServices.errorMgr);
					     baseCurrency = getBaseCurrency(transaction, InstrumentType.DOM_PMT, transactionOrg,
					    		 inputDoc, mediatorServices);
                       }
					   
					   /* KMehta IR-T36000017130 Rel 9400 on 03-Aug-2015 Changed & Moved - Begin */
					   User user = (User) mediatorServices.createServerEJB("User",
								inputDoc.getAttributeLong("/LoginUser/user_oid"));
						validatePaymentDate(transaction, user.getAttribute("timezone"), mediatorServices,buttonPressed );    //NSX IR# HRUL020839028   Rel. 6.1.0
						
						 if (transaction.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY || mediatorServices.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
							{
								inputDoc = preProcessPartyDoc;
							}
						 /* KMehta IR-T36000017130 Rel 9300 on 08-May-2015  Changed & Moved - End */
						 
					   if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind"))) {
					   	 	// POUL012577173 Add reVerifyUploadedDomesticPayments to improve performance
					    	// Verify the uploaded domestic payment again on-line.  The user cannot update the uploaded
					    	// domestica payment on-line.  So we do not need to verify them one by one.  We just need to
					    	// calculate one value date and update all domestic payment.
					    	outputDoc = reVerifyUploadedDomesticPayments(inputDoc, mediatorServices, transaction, panel, baseCurrency, transactionOrg);
					    }
					    else {
					    	outputDoc = processMultipleDomesticPayments(inputDoc, mediatorServices, TradePortalConstants.BUTTON_VERIFY, transaction, panel, baseCurrency, transactionOrg);
					    }


				}
			}
			// IAZ CM-451 10/29/08 End
			String financeType = instrument.getAttribute("import_indicator");
			terms = transaction.populateCustomerTerms(inputDoc);
			//IR 21232 - added condition - for import loan don't check for shipment goods details
			if (InstrumentType.LOAN_RQST.equals(templateTypeCode) && 
					!TransactionType.SIM.equals(transaction.getAttribute("transaction_type_code")) && 
					!TransactionType.SIR.equals(transaction.getAttribute("transaction_type_code")) && 
					!TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("template_flag")) &&
						!TradePortalConstants.INDICATOR_YES.equals(financeType)){

				String invoicesAttached = inputDoc.getAttribute("/areInvoicesAttached");
				String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
				String termsFinanceType = terms.getAttribute("finance_type");

				if (TradePortalConstants.INDICATOR_T.equals(financeType) && StringFunction.isBlank(termsFinanceType)){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.REC_OR_PAY_TYPE_MISSING);
				}

				if(!(TradePortalConstants.BUTTON_ADD_TRANS_INVOICES.equals(buttonPressed) || TradePortalConstants.BUTTON_CALC_DIST_AND_INTRST.equals(buttonPressed))){
				// for export loan/invoice financing - if No invoices linked  and  No invoice text  then error

				if ((TradePortalConstants.INDICATOR_X.equals(financeType) || TradePortalConstants.INDICATOR_NO.equals(financeType))
						&& (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(invoicesAttached))
						&& StringFunction.isBlank(terms.getAttribute("financed_invoices_details_text"))
						&& !TradePortalConstants.PRESHIPMENT_FINANCING.equalsIgnoreCase(termsFinanceType)
						&& !TradePortalConstants.OTHER.equalsIgnoreCase(termsFinanceType)){
					mediatorServices.getErrorManager().issueError(
	 							TradePortalConstants.ERR_CAT_1,
	 							TradePortalConstants.MISSING_ATTACHED_INVS);
				}
			}



			}
			//IR - PAUJ031670543 - 03/17/2009 - ADD BEGIN...
			if ((InstrumentType.FUNDS_XFER).equals(templateTypeCode))
			{
				if ((TransactionStatus.READY_TO_AUTHORIZE).equals(transStatus))
				{
					String fromAcctOID = terms.getAttribute("debit_account_oid");
					String baseCurrency ="";
					String fromCurrency="";
					if (StringFunction.isNotBlank(fromAcctOID)){
						Account fromAccount = (Account) mediatorServices.createServerEJB("Account");
						fromAccount.getData(Long.parseLong(fromAcctOID));
						fromCurrency=fromAccount.getAttribute("currency");
					}
					if (!TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("template_flag"))) {
						CorporateOrganization corp_org = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
								Long.parseLong(instrument.getAttribute("corp_org_oid")));
						baseCurrency = corp_org.getAttribute("base_currency_code");
					}

					User user = (User) mediatorServices.createServerEJB("User",
							inputDoc.getAttributeLong("/LoginUser/user_oid"));
					verifyFTRQData(fromCurrency,baseCurrency,transaction,terms, user, mediatorServices);
					calculateDPEquaivalentPaymentAmount(inputDoc, instrument, transaction, terms, terms.getAttribute("amount"), transStatus, mediatorServices); //MDB CR-640 Rel7.1 9/21/11

				}

			}


			if (templateTypeCode.equals(InstrumentType.XFER_BET_ACCTS))
			{
				String fromAcctOID = terms.getAttribute("debit_account_oid");
				String toAcctOID = terms.getAttribute("credit_account_oid");
				String baseCurrency ="";
				String fromCurrency="";
				String toCurrency="";
				boolean isMissingAccounts = false;
				if (!TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("template_flag"))) {
					if (StringFunction.isBlank(fromAcctOID)) {
						mediatorServices.getErrorManager().issueError(getClass().getName(),
								AmsConstants.REQUIRED_ATTRIBUTE,
								mediatorServices.getResourceManager().getText("TransferBetweenAccounts.FromAccount",
										TradePortalConstants.TEXT_BUNDLE));
						isMissingAccounts = true;
					}
					if (StringFunction.isBlank(toAcctOID)) {
						mediatorServices.getErrorManager().issueError(getClass().getName(),
								AmsConstants.REQUIRED_ATTRIBUTE,
								mediatorServices.getResourceManager().getText("TransferBetweenAccounts.ToAccount",
										TradePortalConstants.TEXT_BUNDLE));
						isMissingAccounts = true;
					}
				}
				if (isMissingAccounts) {
					return outputDoc;
				}

				Account fromAccount = (Account) mediatorServices.createServerEJB("Account");
				if(StringFunction.isNotBlank(fromAcctOID))fromAccount.getData(Long.parseLong(fromAcctOID));
				Account toAccount = (Account) mediatorServices.createServerEJB("Account");
				if(StringFunction.isNotBlank(toAcctOID))toAccount.getData(Long.parseLong(toAcctOID));
				fromCurrency=fromAccount.getAttribute("currency");
				toCurrency=toAccount.getAttribute("currency");

				String fromBankOrgOID = fromAccount.getAttribute("op_bank_org_oid");
				String   toBankOrgOID =   toAccount.getAttribute("op_bank_org_oid");

				if (!TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("template_flag"))) {
					if (StringFunction.isBlank(toBankOrgOID) || !toBankOrgOID.equals(fromBankOrgOID)) {
						mediatorServices.getErrorManager().issueError(getClass().getName(),
								TradePortalConstants.ACCOUNTS_NOT_SAME_BOPORG);
					}
				
					String corpOrgOid = fromAccount.getOwnerOfAccount();
					baseCurrency = fromAccount.getBaseCurrencyOfAccount();
			    	calculateEquivalentAmount(fromCurrency, toCurrency, baseCurrency,instrument, terms, mediatorServices,fromAccount,corpOrgOid, transaction, inputDoc);
				}
				if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
				{
					User user = (User) mediatorServices.createServerEJB("User",
							inputDoc.getAttributeLong("/LoginUser/user_oid"));
					verifyFTBAData(fromCurrency, toCurrency, baseCurrency, transaction, terms, user, mediatorServices) ;

					String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
					if ((TradePortalConstants.BUTTON_VERIFY.equals(buttonPressed) || TradePortalConstants.BUTTON_VERIFY_FX.equals(buttonPressed) ||
							 TradePortalConstants.BUTTON_CONFIRM.equals(buttonPressed)) && isCheckerWorkflowEnabled(instrument, transaction, mediatorServices) &&
							 TransactionStatus.READY_TO_AUTHORIZE.equals(transaction.getAttribute("transaction_status"))) {
			           // set transaction status to Ready To check
				       transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_CHECK);
			        }
				}
			}

			if (templateTypeCode.equals(InstrumentType.DOM_PMT))
			{
				LOG.debug("TransactionMediatorBean::Save::entering calculateDPEq");
				calculateDPEquaivalentPaymentAmount(inputDoc, instrument, transaction, terms, terms.getAttribute("amount"), transStatus, mediatorServices);
 				//MDB Rel6.1 IR#PUL011064558 1/12/11 - added check for confirm button
				if ((transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE)) &&
					(StringFunction.isNotBlank(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
				    (!inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_CONFIRM))) {
			        transaction.setAttribute("transaction_status", getTransactionStatus(transaction,inputDoc,mediatorServices));
			        String buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
			        // if panel enabled and butotn presed is Confirm, Verify, Vefiry Fx, then if transaction_status attribute has
			        // status as ready to authorize then make it as ready to check
			        if ((TradePortalConstants.BUTTON_VERIFY.equals(buttonPressed) || TradePortalConstants.BUTTON_VERIFY_FX.equals(buttonPressed) ||
							 TradePortalConstants.BUTTON_CONFIRM.equals(buttonPressed)) && isCheckerWorkflowEnabled(instrument, transaction, mediatorServices) &&
							 TransactionStatus.READY_TO_AUTHORIZE.equals(transaction.getAttribute("transaction_status"))) {
			           // set transaction status to Ready To check
				       transaction.setAttribute("transaction_status", TransactionStatus.READY_TO_CHECK);
			        }
			       }

				if (TransactionStatus.READY_TO_AUTHORIZE.equals(transStatus))
				{
					if (StringFunction.isNotBlank(transaction.getAttribute("authorization_errors")))
						outputDoc.setAttribute("/Transaction/authorization_errors", transaction.getAttribute("authorization_errors"));

					if (TradePortalConstants.BUTTON_VERIFY_FX.equals(inputDoc.getAttribute("/Update/ButtonPressed")))
			   			transaction.setAttribute("authorization_errors", null);
				}

			}
			//AAlubala Rel 7.0.0.0 CR610 - Additional Verifications steps are needed -03/2011
			//
			//Only do this for payment instruments
			//and only once upon VERIFICATION
			//
			String instrumentType = instrument.getAttribute("instrument_type_code");
			if(StringFunction.isNotBlank(instrumentType))
			   if (instrumentType.equals(InstrumentType.DOM_PMT) ||
				       	 instrumentType.equals(InstrumentType.FUNDS_XFER) ||
				       	 instrumentType.equals(InstrumentType.XFER_BET_ACCTS)){
							if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
							additionalPaymentVerication(inputDoc,mediatorServices, instrument);

			   }

			// Add a new method validateConditionalRequiredExpiryDate() called from the saveTransaction()
			// method on verify to conditionally validate presence of the expiry or end date for SLC and GUA issues.

			String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
			String transactionTypeCode = transaction.getAttribute("transaction_type_code");

            if ( ( InstrumentType.GUARANTEE.equals(instrumentType) ||
                    InstrumentType.STANDBY_LC.equals(instrumentType) )&&
                  (!( TransactionType.AMEND.equals(transactionTypeCode) ||
                            TransactionType.DISCREPANCY.equals(transactionTypeCode))) &&
                    TransactionStatus.READY_TO_AUTHORIZE.equals( transStatus )){
            			String expiryEndDate = null;
		        	    expiryEndDate = inputDoc.getAttribute("/Terms/expiry_date");
		            	validateConditionalRequiredExpiryDate(opBankOrgOid, expiryEndDate,terms);
            }


            //validate for swift characters for specific instrument/transaction types.
            // have to do this logic here rather than in terms mgr becuase we need to verify
            // instrument is portal originated, which is determined from original
            // instrument transaction. issue is done in terms mgr.
            String transactionType = transaction.getAttribute("transaction_type_code");

            if ( ( InstrumentType.GUARANTEE.equals(instrumentType) ||
                   InstrumentType.STANDBY_LC.equals(instrumentType) ) &&
                 ( TransactionType.AMEND.equals(transactionType) ||
                   TransactionType.DISCREPANCY.equals(transactionType) ) &&
                 TransactionStatus.READY_TO_AUTHORIZE.equals( transStatus ) ) {

                validateForSwiftCharacters( instrumentOid, instrument, terms );


            }

			if( InstrumentType.GUARANTEE.equals(instrumentType) && (TransactionType.ISSUE.equals(transactionType) ||
				TransactionType.AMEND.equals(transactionType) || TransactionType.PAYMENT.equals(transactionType))
				&& TransactionStatus.READY_TO_AUTHORIZE.equals( transStatus )){
				  LOG.debug("charge  validation().."+inputDoc);

            	int chrgIncRowCount=inputDoc.getFragmentsList("/Transaction/FeeList").size();
            	LOG.debug("chrgIncRowCount:"+chrgIncRowCount);

            	for(int iLoop=0; iLoop<(chrgIncRowCount); iLoop++){

            	if(StringFunction.isBlank(inputDoc.getAttribute("/Transaction/FeeList("+iLoop+")/settlement_curr_code"))){
            		mediatorServices.getErrorManager ().issueError (
            				getClass().getName(), TradePortalConstants.CHARGE_CURRENCY_CODE_REQ,""+(iLoop+1));
            	}
            	if(StringFunction.isBlank(inputDoc.getAttribute("/Transaction/FeeList("+iLoop+")/settlement_curr_amount"))){
            		mediatorServices.getErrorManager ().issueError (
            				getClass().getName(), TradePortalConstants.CHARGE_AMOUNT_REQ,""+(iLoop+1));
            	}
            	if(StringFunction.isBlank(inputDoc.getAttribute("/Transaction/FeeList("+iLoop+")/charge_type"))){

            		mediatorServices.getErrorManager ().issueError (
            				getClass().getName(), TradePortalConstants.CHARGE_TYPE_REQ,""+(iLoop+1));
            	}

            	}
			}
			// Populate some "copy of" fields
			setCopyOfFields(instrument, transaction, terms);

			// Remove any terms parties that were cleared out in the transaction page
			// and reset the component oid field in terms accordingly. Do this only if
			// we were previously in edit mode.
			// transStatus is blank if the user pressed Save and STARTED if they user
			// just press 'Edit Data' from read-only mode
			if (!transStatus.equals(TransactionStatus.STARTED))
			{
				 /* KMehta IR-T36000017130 Rel 9400 on 03-Aug-2015 Added If stmt - Begin */
				if (!(TradePortalConstants.BUTTON_VERIFY.equals(inputDoc.getAttribute("/Update/ButtonPressed")) && InstrumentType.DOM_PMT.equals(inputDoc.getAttribute("/Instrument/instrument_type_code"))))
				terms = deleteEmptyTermsParties(emptyTermsPartyInfo, terms, mediatorServices);
			}

			// Set certain values when editing the original transaction only
			// The middleware will take care of setting these fields at all other times
			String transOid = inputDoc.getAttribute("/Transaction/transaction_oid");
			if (transOid.equals(instrument.getAttribute("original_transaction_oid")))
			{
				// Now that terms has been populated, we know for sure that the
				// first terms party oid has been created.  Set this value as
				// the counter party oid.
				counterParty = terms.getAttribute("c_FirstTermsParty");
				instrument.setAttribute("a_counter_party_oid", counterParty);

				// CR383 - Update instrument.vendor_id each time instrument.a_counter_party_oid is updated
				instrument.setAttribute("vendor_id", inputDoc.getAttribute("/Terms/FirstTermsParty/vendor_id"));

				try {
					NumberValidator.getNonInternationalizedValue(transaction.getAttribute("copy_of_amount"),
							mediatorServices.getResourceManager().getResourceLocale() );
					// Set the instrument amount to the amount of the transaction
					transaction.setAttribute("instrument_amount", transaction.getAttribute("copy_of_amount"));
					instrument.setAttribute("copy_of_instrument_amount", transaction.getAttribute("copy_of_amount"));
				}
				catch (InvalidAttributeValueException e)
				{
					// Don't set the copy_of value if the number is invalid
				}

				// Only set the expiry date if it has been registered on terms
				if(terms.isAttributeRegistered("expiry_date"))
				{
					// Set the expiry date to the expiry date of the terms
					instrument.setAttribute("copy_of_expiry_date", terms.getAttribute("expiry_date"));
				}

				try {
					if (instrument.getAttribute("instrument_type_code").equals(InstrumentType.REQUEST_ADVISE)) {
						if(!StringFunction.isBlank(terms.getAttribute("issuer_ref_num"))) {
							// Don't overwrite the ref num if it would be set to a blank value
							instrument.setAttribute("copy_of_ref_num", terms.getAttribute("issuer_ref_num") );
						}
					} else {
						instrument.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number"));
					}
				}
				catch(AmsException e)
				{
					// If no reference number exists, which does happens in some cases.
				}
			}

			// Set the Applicant's Reference number on the instrument only if it is not blank
			try
			{
				if (instrument.getAttribute("instrument_type_code").equals(InstrumentType.REQUEST_ADVISE)) {
					if(!StringFunction.isBlank(terms.getAttribute("issuer_ref_num"))) {
						// Don't overwrite the ref num if it would be set to a blank value
						instrument.setAttribute("copy_of_ref_num", terms.getAttribute("issuer_ref_num") );
					}
				} else {
					if(!StringFunction.isBlank(terms.getAttribute("reference_number")))
					{
						// Don't overwrite the ref num if it would be set to a blank value
						instrument.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number") );
					}
				}
			}
			catch(AmsException e)
			{
				// If any of the attributes are not registered, do nothing
			}

			// Set this local attribute - not saved to the database
			// Sometimes, amendments need the currency of the original transaction
			// For amendments, the original transaction's currency is always sent over from the page
			// By setting this they can get to it from the TermsMgr
			terms.setAttribute("original_trans_curr_code", terms.getAttribute("amount_currency_code") );

			// TLE - 11/09/06 - IR#ANUG110757179 - Add Begin
			terms.setImportIndicator(instrument.getAttribute("import_indicator"));
			// TLE - 11/09/06 - IR#ANUG110757179 - Add End

			// If the transaction is an amendment, perform some special processing
			if(transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND))
			{
				// If the amount has been set to blank or 0 by the user, null
				// out the amount field and also null out the currency field
				if( (terms.getAttribute("amount").equals("")) ||
						(terms.getAttributeDecimal("amount").doubleValue() == 0) )
				{
					terms.setAttribute("amount","");
					terms.setAttribute("amount_currency_code", "");
					transaction.setAttribute("copy_of_amount", "");
					transaction.setAttribute("copy_of_currency_code", "");
				}
			}

			if (instrument.getAttribute("instrument_type_code").equals(InstrumentType.EXPORT_COL) &&
			    TradePortalConstants.INDICATOR_NO.equals(instrument.getAttribute("template_flag")) ) { //IR-WHUK101560580-Vshah-10-21-2010
				long clientBankOid = 0;
				String suppressDocInd = null;
				ClientBank clientBank = null;

				clientBankOid = instrument.getAttributeLong("client_bank_oid");
				clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");

				LOG.debug("Mediator: Retrieving Client Bank " + clientBankOid);
				clientBank.getData(clientBankOid);

				suppressDocInd = clientBank.getAttribute("suppress_doc_link_ind");
				if (StringFunction.isBlank(suppressDocInd))
					suppressDocInd = TradePortalConstants.INDICATOR_NO;

					instrument.setAttribute("suppress_doc_link_ind", suppressDocInd);
				}
			/* KMehta IR-T36000034670 Rel 9300 on 08-May-2015 Add - Begin */
			String transactionCorpOrgOid = instrument.getAttribute("corp_org_oid");	
			//Srinivasu_D IR#T36000040073 Rel9.3 06/09/2015 - Checking corp OrgOid is blank or not
			if(StringFunction.isNotBlank(transactionCorpOrgOid)){
			transactionCorpOrg = getCorporateOrg(transactionCorpOrgOid, mediatorServices);
			}
			//Srinivasu_D IR#T36000040073 Rel9.3 06/09/2015 - End
			/* KMehta IR-T36000034670 Rel 9300 on 08-May-2015 Add - End */
			// If it is a H2H and is STA, skip panel check
			if(!(transaction.isH2HSentPayment() && (transactionCorpOrg != null && transactionCorpOrg.isStraightThroughAuthorize()))){
			 if( TradePortalConstants.INDICATOR_NO.equals(transaction.getAttribute("bene_panel_auth_ind")) && 
					TransactionStatus.READY_TO_AUTHORIZE.equals(transStatus) &&
					TradePortalConstants.BUTTON_VERIFY.equals(inputDoc.getAttribute("/Update/ButtonPressed")) &&
					(transactionCorpOrg != null && transactionCorpOrg.isPanelAuthEnabled(instrument.getAttribute("instrument_type_code"), transaction.getAttribute("transaction_type_code")))){
				setPanelRangeDataInTransaction(transaction, instrument, terms, inputDoc, mediatorServices);
			  }
			}

			outputDoc.setAttribute("/Transaction/transaction_status", transaction.getAttribute("transaction_status"));

			//MEer Rel 9.1 CR-934b IR-30623 Save payment_method of the first beneficiary to the Transaction object
			if(templateTypeCode.equals(InstrumentType.DOM_PMT)){
				if	(StringFunction.isNotBlank(inputDoc.getAttribute("/Update/ButtonPressed")) && ( inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY) || (inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_SAVETRANS)) || (inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_SAVECLOSETRANS)) ) ){
					int numberOfDomPmts=0;
					List<Object> sqlParams = new ArrayList();
					StringBuffer whereClause = new StringBuffer();

					whereClause.append(" p_transaction_oid =? " );
					sqlParams.add(transactionOid);
					numberOfDomPmts = DatabaseQueryBean.getCount("domestic_payment_oid",	"domestic_payment", whereClause.toString(), false, sqlParams);

					if(numberOfDomPmts>0){
						String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?  order by DP.DOMESTIC_PAYMENT_OID";

						DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false,  new Object[]{transactionOid});

						String firstBenePaymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(0)/PAYMENT_METHOD_TYPE");
						transaction.setAttribute("payment_method_type", firstBenePaymentMethodType);

					}
				}
			}

			//RKAZI 07-07-2015 T36000038018 - START
			if (doValidate){
				String templateOid = instrument.getAttribute("template_oid");
				long clientBankOid = 0;
				ClientBank clientBank = null;

				clientBankOid = instrument.getAttributeLong("client_bank_oid");
				clientBank = (ClientBank) mediatorServices.createServerEJB("ClientBank");

				LOG.debug("Mediator: Retrieving Client Bank " + clientBankOid);
				clientBank.getData(clientBankOid);
				String defaultTemplateOid = clientBank.getDefaultTemplateOid(instrumentType);
				if (StringFunction.isBlank(templateOid)){

					templateOid = defaultTemplateOid;

				}
				if (transaction.getAttribute("transaction_type_code").equals(
						TransactionType.ISSUE)){
//						copyBankDefinedFields(instrumentType, templateOid, terms,  mediatorServices, defaultTemplateOid);
				}
				
				// Nar CR-818 08/10/2015 Rel 9400 Add- Begin
				String dcrInstrcution = inputDoc.getAttribute("/Terms/import_discrepancy_instr");
				if ( SettlementInstrUtility.isSettlemntInstrVerificationReq( instrument.getAttribute("corp_org_oid"), transactionTypeCode, dcrInstrcution) ) {
					String workItemType = transaction.getAttribute("settle_instr_work_item_type");
					performVerificationOfSettlemntInstrDetail( terms, transaction, instrument, inputDoc, mediatorServices);
				}
				// Nar CR-818 08/10/2015 Rel 9400 Add- End

			}
			//RKAZI 07-07-2015 T36000038018 - END

			instrument.save(doValidate);

			LOG.debug("Mediator: Completed the save of instrument and transaction");

			// if no error, remove payment party
			if(this.isTransSuccess(mediatorServices.getErrorManager())) {    //NNUK021160918
			          removePaymentOrderingPartyOrBank(orderingPartyToRemoveOid, mediatorServices);		//IAZ CM CR-507 12/19/09 Add
			          removePaymentOrderingPartyOrBank(orderingPartyBankToRemoveOid, mediatorServices);	//IAZ CM CR-507 12/19/09 Add
            }   //NNUK021160918
			LOG.debug("Completed Transaction Processing");

		} catch (InvalidObjectIdentifierException ex) {

			// If the object does not exist, issue an error
			mediatorServices.getErrorManager ().issueError (
					getClass ().getName (), "INF01", ex.getObjectId());

		}// catch (DataObjectExceptione
		return outputDoc;
	}

	private DocumentHandler clearTermsPartiesInfo(DocumentHandler inputDoc,
			Hashtable nonEmptytermsPartyInfo, MediatorServices mediatorServices) {
		
		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES; i++)
		{

			if (isTermsPartyInfoUpdated(inputDoc, TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i], mediatorServices))
			{
				DocumentHandler   termsPartyDoc           = null;
				StringBuffer      fragmentPath            = new StringBuffer("/Terms/");
				String            termsPartyStateProvince = null;
				String            termsPartyAddressLine1  = null;
				String            termsPartyAddressLine2  = null;
				String            termsPartyAddressLine3  = null;
				String            termsPartyPhoneNumber   = null;
				String            termsPartyPostalCode    = null;
				String            termsPartyCountry       = null;
				String            termsPartyCity          = null;
				String            termsPartyName          = null;
				String            termsPartyContactName   = null;
				String			  termsPartyAcctNum		  = null;
				String			  termsPartyAcctCcy		  = null;
				String			  branchCode			  = null;
				fragmentPath.append(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]);

				termsPartyDoc = inputDoc.getFragment(fragmentPath.toString());
				if (termsPartyDoc != null)
				{
					termsPartyName          = termsPartyDoc.getAttribute("/name");
					termsPartyAddressLine1  = termsPartyDoc.getAttribute("/address_line_1");
					termsPartyAddressLine2  = termsPartyDoc.getAttribute("/address_line_2");
					termsPartyAddressLine3  = termsPartyDoc.getAttribute("/address_line_3");
					termsPartyCity          = termsPartyDoc.getAttribute("/address_city");
					termsPartyStateProvince = termsPartyDoc.getAttribute("/address_state_province");
					termsPartyCountry       = termsPartyDoc.getAttribute("/address_country");
					termsPartyPostalCode    = termsPartyDoc.getAttribute("/address_postal_code");
					termsPartyPhoneNumber   = termsPartyDoc.getAttribute("/phone_number");
					termsPartyContactName   = termsPartyDoc.getAttribute("/contact_name");
					termsPartyAcctNum	    = termsPartyDoc.getAttribute("/acct_num");
					termsPartyAcctCcy	    = termsPartyDoc.getAttribute("/acct_currency");
					branchCode				= termsPartyDoc.getAttribute("/branch_code");
					inputDoc.setAttribute(fragmentPath.toString() + "/name", termsPartyName);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_line_1", termsPartyAddressLine1);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_line_2", termsPartyAddressLine2);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_line_3", termsPartyAddressLine3);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_city", termsPartyCity);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_state_province", termsPartyStateProvince);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_country", termsPartyCountry);
					inputDoc.setAttribute(fragmentPath.toString() + "/address_postal_code", termsPartyPostalCode);
					inputDoc.setAttribute(fragmentPath.toString() + "/phone_number", termsPartyPhoneNumber);
					inputDoc.setAttribute(fragmentPath.toString() + "/contact_name", termsPartyContactName);
					inputDoc.setAttribute(fragmentPath.toString() + "/acct_num", termsPartyAcctNum);
					inputDoc.setAttribute(fragmentPath.toString() + "/acct_currency", termsPartyAcctCcy);
					inputDoc.setAttribute(fragmentPath.toString() + "/branch_code", branchCode);
				}
			}
		}

		return inputDoc;
	}


	private boolean isTermsPartyInfoUpdated(DocumentHandler inputDoc,
			String termsPartyComponentName, MediatorServices mediatorServices) {
		DocumentHandler   termsPartyDoc           = null;
		StringBuffer      fragmentPath            = new StringBuffer("/Terms/");
		boolean           isUpdated                 = false;
		String            termsPartyStateProvince = null;
		String            termsPartyAddressLine1  = null;
		String            termsPartyAddressLine2  = null;
		String            termsPartyAddressLine3  = null;
		String            termsPartyPhoneNumber   = null;
		String            termsPartyPostalCode    = null;
		String            termsPartyCountry       = null;
		String            termsPartyCity          = null;
		String            termsPartyName          = null;
		String            termsPartyContactName   = null;
		String			  termsPartyAcctNum		  = null;
		String			  termsPartyAcctCcy		  = null;
		String			  branchCode			  = null;

		String            otermsPartyStateProvince = null;
		String            otermsPartyAddressLine1  = null;
		String            otermsPartyAddressLine2  = null;
		String            otermsPartyAddressLine3  = null;
		String            otermsPartyPhoneNumber   = null;
		String            otermsPartyPostalCode    = null;
		String            otermsPartyCountry       = null;
		String            otermsPartyCity          = null;
		String            otermsPartyName          = null;
		String            otermsPartyContactName   = null;
		String			  otermsPartyAcctNum		  = null;
		String			  otermsPartyAcctCcy		  = null;
		String			  obranchCode			  = null;

		fragmentPath.append(termsPartyComponentName);
		String currentTermsPartyOid = inputDoc.getAttribute(fragmentPath + "/terms_party_oid");
		if (StringFunction.isBlank(currentTermsPartyOid)){
			return false;
		}
		try {
			TermsParty tempParty = (TermsParty) mediatorServices.createServerEJB("TermsParty", Long.parseLong(currentTermsPartyOid));
			otermsPartyName          = tempParty.getAttribute("name");
			otermsPartyAddressLine1  = tempParty.getAttribute("address_line_1");
			otermsPartyAddressLine2  = tempParty.getAttribute("address_line_2");
			otermsPartyAddressLine3  = tempParty.getAttribute("address_line_3");
			otermsPartyCity          = tempParty.getAttribute("address_city");
			otermsPartyStateProvince = tempParty.getAttribute("address_state_province");
			otermsPartyCountry       = tempParty.getAttribute("address_country");
			otermsPartyPostalCode    = tempParty.getAttribute("address_postal_code");
			otermsPartyPhoneNumber   = tempParty.getAttribute("phone_number");
			otermsPartyContactName   = tempParty.getAttribute("contact_name");
			otermsPartyAcctNum	    = tempParty.getAttribute("acct_num");
			otermsPartyAcctCcy	    = tempParty.getAttribute("acct_currency");
			obranchCode				= tempParty.getAttribute("branch_code");

		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (AmsException e) {
			e.printStackTrace();
			return false;
		}

		termsPartyDoc = inputDoc.getFragment(fragmentPath.toString());
		if (termsPartyDoc != null)
		{
			termsPartyName          = termsPartyDoc.getAttribute("/name");
			termsPartyAddressLine1  = termsPartyDoc.getAttribute("/address_line_1");
			termsPartyAddressLine2  = termsPartyDoc.getAttribute("/address_line_2");
			termsPartyAddressLine3  = termsPartyDoc.getAttribute("/address_line_3");
			termsPartyCity          = termsPartyDoc.getAttribute("/address_city");
			termsPartyStateProvince = termsPartyDoc.getAttribute("/address_state_province");
			termsPartyCountry       = termsPartyDoc.getAttribute("/address_country");
			termsPartyPostalCode    = termsPartyDoc.getAttribute("/address_postal_code");
			termsPartyPhoneNumber   = termsPartyDoc.getAttribute("/phone_number");
			termsPartyContactName   = termsPartyDoc.getAttribute("/contact_name");
			termsPartyAcctNum	    = termsPartyDoc.getAttribute("/acct_num");
			termsPartyAcctCcy	    = termsPartyDoc.getAttribute("/acct_currency");
			branchCode				= termsPartyDoc.getAttribute("/branch_code");

			if ((StringFunction.isNotBlank(termsPartyStateProvince) && !termsPartyStateProvince.equals(otermsPartyStateProvince)) ||
					(StringFunction.isNotBlank(termsPartyAddressLine1) && !termsPartyAddressLine1.equals(otermsPartyAddressLine1)) ||
					(StringFunction.isNotBlank(termsPartyAddressLine2) && !termsPartyAddressLine2.equals(otermsPartyAddressLine2)) ||
					(StringFunction.isNotBlank(termsPartyAddressLine3) && !termsPartyAddressLine3.equals(otermsPartyAddressLine3)) ||
					(StringFunction.isNotBlank(termsPartyPhoneNumber) && !termsPartyPhoneNumber.equals(otermsPartyPhoneNumber)) ||
					(StringFunction.isNotBlank(termsPartyPostalCode) && !termsPartyPostalCode.equals(otermsPartyPostalCode)) ||
					(StringFunction.isNotBlank(termsPartyCountry) && !termsPartyCountry.equals(otermsPartyCountry)) ||
					(StringFunction.isNotBlank(termsPartyCity) && !termsPartyCity.equals(otermsPartyCity)) ||
					(StringFunction.isNotBlank(termsPartyContactName) && !termsPartyContactName.equals(otermsPartyContactName)) ||
					(StringFunction.isNotBlank(termsPartyName) && !termsPartyName.equals(otermsPartyName)) ||
					(StringFunction.isNotBlank(termsPartyAcctNum) && !termsPartyAcctNum.equals(otermsPartyAcctNum)) ||
					(StringFunction.isNotBlank(termsPartyAcctCcy) && !termsPartyAcctCcy.equals(otermsPartyAcctCcy)) ||
					(StringFunction.isNotBlank(branchCode) && !branchCode.equals(obranchCode)))
			{
				isUpdated = true;
			}
		}

		return isUpdated;
	}


	//
	//This method performs additional verification steps
	//
	/*
	 * Derive the transaction amount and currency based on Invoices attached to the transaction
	 */
	private void deriveTransactionCurrencyAndAmountsFromInvoicePaymentInstructions(
			Transaction transaction, Terms terms, Vector newInvOidList,
			Instrument instrument,Vector newInvIDList, DocumentHandler inputDoc) throws AmsException, RemoteException {

		String newListOfInvOidsAddedSql = null;
		String newListOfInvNumsAddedSql = null;
		String trnxCurrencyCode = null;
		// Create SQL for uploaded POs that are now a part of this shipment
		if ((newInvOidList != null) && (newInvOidList.size() > 0)) {
			String requiredOid = "INV_PAY_INST_OID";
			newListOfInvOidsAddedSql = POLineItemUtility.getPOLineItemOidsSql(
					newInvOidList, requiredOid);
		}
		double totalAmountOfCurrTranFromPO = 0.00;
		String poCurrency = null;

		// Total up the POs from the instantiaed POLineItemList components.
		// These PO Line Items may not have been saved to database.
		BigDecimal oldTotalPOAmt = BigDecimal.ZERO;
		BigDecimal offSetAmt = BigDecimal.ZERO;
		BigDecimal newTotalPOAmt = BigDecimal.ZERO;
		if (newListOfInvOidsAddedSql != null ) {
			StringBuffer sqlStatement = new StringBuffer();

				sqlStatement
						.append("select sum(amount) as amount, max(amount_currency_code) as currency, count(*) as pocount from invoice_payment_instructions");
				sqlStatement.append(" where (p_transaction_oid = ? ");
				sqlStatement.append(")");
				if (newListOfInvOidsAddedSql != null) {
					sqlStatement.append(" or (");
					sqlStatement.append(newListOfInvOidsAddedSql);
					sqlStatement.append(")");
				}
			DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), true, new Object[]{transaction.getAttribute(transaction.getIDAttributeName())});
			resultSet = resultSet.getFragment("/ResultSetRecord");

			// Retrieve the total for the SQL query that was just constructed
			try {
				totalAmountOfCurrTranFromPO += resultSet.getAttributeDecimal(
						"/AMOUNT").doubleValue();
			} catch (Exception e) {
				/* Ignore null amount */
			}

			oldTotalPOAmt = BigDecimal.ZERO;
			offSetAmt = BigDecimal.ZERO;
			newTotalPOAmt = new BigDecimal(totalAmountOfCurrTranFromPO);
			trnxCurrencyCode = transaction
			.getAttribute("copy_of_currency_code");

			if (StringFunction.isBlank(trnxCurrencyCode)){
				trnxCurrencyCode = resultSet.getAttribute("/CURRENCY");
			}

			int numberOfCurrencyPlaces = 2;

			if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
				numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
						.getRefDataMgr().getAdditionalValue(
								TradePortalConstants.CURRENCY_CODE,
								trnxCurrencyCode));
			}
			LOG.debug("New INV Amount in the transaction" +newTotalPOAmt);
			if (transaction.getAttribute("transaction_type_code").equals(TransactionType.AMEND)) {
				oldTotalPOAmt = getMatchAmount(transaction, newListOfInvOidsAddedSql,
						instrument,newListOfInvNumsAddedSql,null);
				LOG.debug("Old INV Matching Amount" +oldTotalPOAmt);
				boolean haveOldAmt = false;
				if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == 1) {
					offSetAmt = oldTotalPOAmt.subtract(newTotalPOAmt);
					offSetAmt = offSetAmt.negate();
					haveOldAmt = true;
				} else if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == -1) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				}else if (oldTotalPOAmt.intValue() != 0
						&& oldTotalPOAmt.compareTo(newTotalPOAmt) == 0) {
					offSetAmt = newTotalPOAmt.subtract(oldTotalPOAmt);
					haveOldAmt = true;
				}
				newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces,
						BigDecimal.ROUND_HALF_UP);
				if(offSetAmt.intValue() != 0){
				offSetAmt = offSetAmt.setScale(numberOfCurrencyPlaces,
						BigDecimal.ROUND_HALF_UP);
				}
				else if(!haveOldAmt){
					offSetAmt =	newTotalPOAmt;
				}
				LOG.debug("OffSet INV Amount in the transaction" +offSetAmt);
				transaction.setAttribute("copy_of_amount",
						String.valueOf(offSetAmt));



				transaction.setAttribute("instrument_amount",
						String.valueOf(oldTotalPOAmt));

				terms.setAttribute("amount", String.valueOf(offSetAmt));
				inputDoc.setAttribute("/Terms/amount", String.valueOf(offSetAmt));
			}
			else{
				if (totalAmountOfCurrTranFromPO != 0 ) {
					newTotalPOAmt = newTotalPOAmt.setScale(numberOfCurrencyPlaces,
							BigDecimal.ROUND_HALF_UP);
					transaction.setAttribute("copy_of_amount",
							String.valueOf(newTotalPOAmt));
					transaction.setAttribute("instrument_amount",
							String.valueOf(newTotalPOAmt));
					terms.setAttribute("amount", String.valueOf(newTotalPOAmt));
					inputDoc.setAttribute("/Terms/amount", String.valueOf(newTotalPOAmt));
					if (instrument != null){
						instrument.setAttribute("copy_of_instrument_amount",String.valueOf(newTotalPOAmt));
						//instrument.save(false);
					}
				}
			}
			if (poCurrency == null) {
				poCurrency = resultSet.getAttribute("/CURRENCY");
			}


		// Update currency of the issuance if the PO currency is valid
		// Amendment does not derive the currency from POs.
		if (poCurrency != null
				&& !poCurrency.equals("")
				&& transaction.getAttribute("transaction_type_code").equals(
						TransactionType.ISSUE)
				&& ReferenceDataManager.getRefDataMgr().checkCode(
						"CURRENCY_CODE", poCurrency)) {
			terms.setAttribute("amount_currency_code", poCurrency);
			inputDoc.setAttribute("/Terms/amount_currency_code", poCurrency);
			transaction.setAttribute("copy_of_currency_code", poCurrency);
			}
		}
		else{
			transaction.setAttribute("copy_of_amount","");
			terms.setAttribute("amount", "");
		}

	}

	private static BigDecimal getMatchAmount(Transaction transaction,String uploadedPOsInShipmentSql,
			Instrument instrument, String newListOfPoNumsAddedSql,String invType)  throws AmsException, RemoteException{

		      double oldTotalPOAmt =0.00;

			   if (uploadedPOsInShipmentSql != null ) {
				   String strQry =" select sum(amount) as AMOUNT from invoice_payment_instructions where "+newListOfPoNumsAddedSql
				     +" and a_transaction_oid =? ";

				   DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(strQry, true, new Object[]{instrument.getAttribute("active_transaction_oid")});
				   if(resultSet!=null){
					  resultSet = resultSet.getFragment("/ResultSetRecord");
				   oldTotalPOAmt = resultSet!=null && StringFunction.isNotBlank(resultSet.getAttribute("/AMOUNT"))?resultSet.getAttributeDecimal("/AMOUNT").doubleValue():0;
				//		 total = (matchAmount.subtract(newPOAmount)).doubleValue();
			   }
			   }

			return  new BigDecimal(oldTotalPOAmt);
		}

	/**
	 * CR 610
	 *
	 * The portal verification process will be changed to validate if the Debit Accounts associated TPS Customer Id is the same as the
	 * TPS Customer Id of the Corporate Customer associated with the User logged into the Portal.  If it is the same, no additional
	 * processing will be needed.  If it differs, the system will perform the following:
			Add a new Terms Party to the instrument called the POW(Payment Owner) party and set this party type to be equal to the Customer
		of the User who is logged in.  This is the corporate customer to whom the transaction will be sent upon release in the Trade Processing System.
			Update the operational bank organisation of the Payment Instrument (Payment instruments only, not Trade instruments)
		with the Operational Bank Organisation of the Debit Account for the instrument.  This will ensure that the correct
		operational bank organisation will be sent to the TPS to be used for TPS processing and therefore that the two systems are
		in synch in terms of operational bank organisation.
	 * @param instrument TODO

	 *
	 */
	private void additionalPaymentVerication(DocumentHandler inputDoc, MediatorServices mediatorServices, Instrument instrument)throws AmsException, RemoteException{
		String AccntAOPBankOrgOID = "";
		//create User object
		User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));
		String owner = user.getAttribute("owner_org_oid");
		/*if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE)){
			if(StringFunction.isNotBlank(owner)){
			}
		}*/
		//create account object
		//if debit account oid is not provided, Verify will be failed elsewhere, unless this IP which may use
		//"Other" option in which case account is no provided altogether.
		if (StringFunction.isBlank(inputDoc.getAttribute("/Terms/debit_account_oid")))
			return;
		Account account = (Account) mediatorServices.createServerEJB("Account", inputDoc.getAttributeLong("/Terms/debit_account_oid"));
		AccntAOPBankOrgOID = account.getAttribute("op_bank_org_oid");
		String InstrAOPBankOrgOid = instrument.getAttribute("op_bank_org_oid");

		if(StringFunction.isNotBlank(InstrAOPBankOrgOid) && (! InstrAOPBankOrgOid.equals(AccntAOPBankOrgOID))){
			//
			//if it is not same, we change it to match that of the Account
			//
			instrument.setAttribute("op_bank_org_oid", AccntAOPBankOrgOID);
			//
			//Also, set a new Terms Party called "POW" (Payment Owner) equal to the customer of the user who is logged in
			//
		}
	}

	/**
	 * Add a new method validateConditionalRequiredExpiryDate() called from the saveTransaction()
	 * method on verify to conditionally validate presence of the expiry or end date for SLC and GUA issues.
	 * This is not done in the specific terms manager because it is dependent on the bank group
	 * slc_gua_expiry_date_required indicator, which is not passed to the terms manager validate() method.
	 */
	public static void validateConditionalRequiredExpiryDate(String opBankOrgOid,  String expiryEndDate, Terms terms){
		DocumentHandler resultsDoc;
		String slc_gua_indicator = null;
        try {
            resultsDoc = com.amsinc.ecsg.frame.DatabaseQueryBean.getXmlResultSet(
                ("select slc_gua_expiry_date_required " +
                "from bank_organization_group bog, operational_bank_org obg " +
                "where bog.organization_oid =  obg.a_bank_org_group_oid " +                
                "and obg.organization_oid = ?"),
                true,new Object[]{opBankOrgOid});
            
            slc_gua_indicator = resultsDoc.getAttribute("/ResultSetRecord(0)/SLC_GUA_EXPIRY_DATE_REQUIRED");
    	    LOG.debug("slc_gua_indicator: " +slc_gua_indicator);
    	    if (slc_gua_indicator.equals(TradePortalConstants.INDICATOR_YES) && StringFunction.isBlank(expiryEndDate))
			{
            		terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.EXPIRY_END_DATE_REQUIRED);
            		LOG.debug("expiryEndDate: " +expiryEndDate);
			}

        } catch ( Exception ex ) {
            System.err.println("Problem determining portal originated");
            ex.printStackTrace();
        }

	}

    /**
     * For certain transactions, special swift character validation must occur.
     * Note this assumes that the originating transaction for a given instrument
     * is already persisted.
     */
    public static void validateForSwiftCharacters(long instrumentOid, Instrument instrument, Terms terms)
        throws AmsException, RemoteException {

        final String[] attributesToExclude = {"special_bank_instructions"};

        //we use presence of customer entered terms on the original transaction to
        // determine if portal originated or not
        boolean portalOriginated = false;
        String origCustEnteredTermsOid = "";
        String overrideSwiftValidationInd = "";
        DocumentHandler resultsDoc;
        try {
            resultsDoc = DatabaseQueryBean.getXmlResultSet(
                "select c_cust_enter_terms_oid, override_swift_validation_ind " +
                "from instrument i, transaction t, client_bank cb " +
                "where i.original_transaction_oid = t.transaction_oid " +
                "and i.a_client_bank_oid = cb.organization_oid " +
                "and i.instrument_oid = ?",true,new Object[]{instrumentOid});
            
            int curIndex = 0;
            origCustEnteredTermsOid =
                resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/C_CUST_ENTER_TERMS_OID");

            if ( origCustEnteredTermsOid != null && origCustEnteredTermsOid.length() > 0 ) {
                portalOriginated = true;
                overrideSwiftValidationInd =
                    resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/OVERRIDE_SWIFT_VALIDATION_IND");
            }
        } catch ( Exception ex ) {
            System.err.println("Problem determining portal originated");
            ex.printStackTrace();
        }


        if ( portalOriginated &&
             TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(
                 overrideSwiftValidationInd ) ) {
            //get the original deliver by method
            String originalDeliverBy = "";
            try {
                resultsDoc = DatabaseQueryBean.getXmlResultSet(
                    "select guar_deliver_by from terms " +
                    "where terms_oid = ?", true, new Object[]{origCustEnteredTermsOid});

                int curIndex = 0;
                originalDeliverBy =
                    resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/GUAR_DELIVER_BY");
            } catch ( Exception ex ) {
                System.err.println("Problem determining original deliver by method");
                ex.printStackTrace();
            }

            if ( TradePortalConstants.DELIVER_BY_TELEX.equalsIgnoreCase( originalDeliverBy ) ) {
                InstrumentServices.checkForInvalidSwiftCharacters(
                    terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);
            }
            //else don't check for swift characters.

        } else { //not portalOriginated

            InstrumentServices.checkForInvalidSwiftCharacters(
                terms.getAttributeHash(), terms.getErrorManager(), attributesToExclude);
        }
    }

	private String getTransactionStatus(Transaction transaction, DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {
		String status = transaction.getAttribute("transaction_status");
		DocumentHandler pDoc = inputDoc.getFragment("/PaymentFileUpload");
		if (pDoc == null) return status;

		int failedCount = pDoc.getAttributeInt("/number_of_bene_failed");
		int totCount = pDoc.getAttributeInt("/number_of_bene_uploaded");
		int successCount = totCount - failedCount;


		if (mediatorServices.getErrorManager().getMaxErrorSeverity()  >= ErrorManager.ERROR_SEVERITY || transaction.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
			status = TransactionStatus.FILE_UPLOAD_REJECT;
		} else {
			if (successCount < 1) {
				status = TransactionStatus.FILE_UPLOAD_REJECT;
			}
			else if (successCount != totCount) {
				status = TransactionStatus.VERIFIED_AWAITING_APPROVAL;

			}

		}

		return status;

	}

	/**
	 * Setter for complete instrument id attribute
	 *
	 * @param newCompleteInstrumentId java.lang.String
	 */
	private void setCompleteInstrumentId(java.lang.String newCompleteInstrumentId) {
		completeInstrumentId = newCompleteInstrumentId;
	}

	/**
	 * setter for template name attribute
	 *
	 * @param newTemplateName java.lang.String
	 */
	private void setTemplateName(java.lang.String newTemplateName) {
		templateName = newTemplateName;
	}
	/**
	 * Get values from the input data and create the "copy of" fields in
	 * the data.  These "copy of" fields are copies of trans/terms
	 * data that is replicated in instrument/trans for easier db access.
	 *
	 * @param instrument - instrument data
	 * @param transaction - transaction data
	 * @param terms - terms data
	 */
	public void setCopyOfFields(Instrument instrument, Transaction transaction, Terms terms)
	throws RemoteException
	{
		try
		{
			transaction.setAttribute("copy_of_amount", terms.getAttribute("amount"));
		}
		catch(AmsException e)
		{
			// If any of the attributes are not registered, do nothing
		}

		try
		{
			transaction.setAttribute("copy_of_currency_code", terms.getAttribute("amount_currency_code"));
		}
		catch(AmsException e)
		{
			// If any of the attributes are not registered, do nothing
		}

		try
		{
			transaction.setAttribute("copy_of_instr_type_code", instrument.getAttribute("instrument_type_code") );
		}
		catch(AmsException e)
		{
			// If any of the attributes are not registered, do nothing
		}

		try {
			if (instrument.getAttribute("instrument_type_code").equals(InstrumentType.REQUEST_ADVISE)) {
				if(!StringFunction.isBlank(terms.getAttribute("issuer_ref_num"))) {
					// Don't overwrite the ref num if it would be set to a blank value
					instrument.setAttribute("copy_of_ref_num", terms.getAttribute("issuer_ref_num") );
				}
			} else {
				if(!StringFunction.isBlank(terms.getAttribute("reference_number"))) {
					// Don't overwrite the ref num if it would be set to a blank value
					instrument.setAttribute("copy_of_ref_num", terms.getAttribute("reference_number") );
				}
			}
		}

		catch(AmsException e)
		{
			// If any of the attributes are not registered, do nothing
		}
	}


	/**
	 * This method is used to retrieve terms party information for terms parties
	 * that never had data in them or terms parties that have been cleared out
	 * (and therefore deleted) in a transaction page. It looks in the input xml
	 * doc for terms party fragments and determines whether relevant data is
	 * inside each; if there is none, it'll save the name of the terms party and
	 * the oid if one exists to a hashtable.
	 *
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler - the input xml doc
	 * @return emptyTermsPartyInfo java.util.Hashtable - hashtable containing the
	 *                             names of all empty terms parties (keys) and
	 *                             their oids (values) if they exist
	 */
	private Hashtable getEmptyTermsPartyInfo(DocumentHandler inputDoc)
	{
		Hashtable   emptyTermsPartyInfo  = new Hashtable();
		String      currentTermsPartyOid = null;

		for (int i = 0; i<TradePortalConstants.MAX_NUM_TERMS_PARTIES; i++)
		{

			if (isTermsPartyEmpty(inputDoc, TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i]))
			{
				currentTermsPartyOid = inputDoc.getAttribute("/Terms/" + TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i] + "/terms_party_oid");

				if(currentTermsPartyOid == null)
					currentTermsPartyOid = "";

				emptyTermsPartyInfo.put(TradePortalConstants.TERMS_PARTY_COMPONENT_NAMES[i], currentTermsPartyOid);
			}
		}

		return emptyTermsPartyInfo;
	}

	/**
	 * This method is used to determine whether or not a terms party xml fragment has
	 * any relevant data in it.  It'll retrieve all relevant terms party attributes
	 * and determine if any data has been entered for each in a transaction page. If
	 * no data was entered, the method returns true; otherwise, it'll return false.
	 *
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler - the input xml doc
	 * @param termsPartyComponentName java.lang.String - the name of the terms party to check for
	 * @return isEmpty boolean - boolean indicating whether relevant terms party data
	 *                           exists for the terms party being looked at; returns true
	 *                           if empty (i.e., no relevant data) and false otherwise
	 */
	private boolean isTermsPartyEmpty(DocumentHandler inputDoc, String termsPartyComponentName)
	{
		DocumentHandler   termsPartyDoc           = null;
		StringBuffer      fragmentPath            = new StringBuffer("/Terms/");
		boolean           isEmpty                 = false;
		String            termsPartyStateProvince = null;
		String            termsPartyAddressLine1  = null;
		String            termsPartyAddressLine2  = null;
		String            termsPartyAddressLine3  = null;
		String            termsPartyPhoneNumber   = null;
		String            termsPartyPostalCode    = null;
		String            termsPartyCountry       = null;
		String            termsPartyCity          = null;
		String            termsPartyName          = null;
		String            termsPartyContactName   = null;
		String			  termsPartyAcctNum		  = null;
		String			  termsPartyAcctCcy		  = null;
		String			  branchCode			  = null;

		fragmentPath.append(termsPartyComponentName);

		termsPartyDoc = inputDoc.getFragment(fragmentPath.toString());
		if (termsPartyDoc != null)
		{
			termsPartyName          = termsPartyDoc.getAttribute("/name");
			termsPartyAddressLine1  = termsPartyDoc.getAttribute("/address_line_1");
			termsPartyAddressLine2  = termsPartyDoc.getAttribute("/address_line_2");
			termsPartyAddressLine3  = termsPartyDoc.getAttribute("/address_line_3");
			termsPartyCity          = termsPartyDoc.getAttribute("/address_city");
			termsPartyStateProvince = termsPartyDoc.getAttribute("/address_state_province");
			termsPartyCountry       = termsPartyDoc.getAttribute("/address_country");
			termsPartyPostalCode    = termsPartyDoc.getAttribute("/address_postal_code");
			termsPartyPhoneNumber   = termsPartyDoc.getAttribute("/phone_number");
			termsPartyContactName   = termsPartyDoc.getAttribute("/contact_name");
			termsPartyAcctNum	    = termsPartyDoc.getAttribute("/acct_num");
			termsPartyAcctCcy	    = termsPartyDoc.getAttribute("/acct_currency");
			branchCode				= termsPartyDoc.getAttribute("/branch_code");

			if ((StringFunction.isBlank(termsPartyStateProvince)) &&
					(StringFunction.isBlank(termsPartyAddressLine1))  &&
					(StringFunction.isBlank(termsPartyAddressLine2))  &&
					(StringFunction.isBlank(termsPartyAddressLine3))  &&
					(StringFunction.isBlank(termsPartyPhoneNumber))   &&
					(StringFunction.isBlank(termsPartyPostalCode))    &&
					(StringFunction.isBlank(termsPartyCountry))       &&
					(StringFunction.isBlank(termsPartyCity))          &&
					(StringFunction.isBlank(termsPartyContactName))   &&
					(StringFunction.isBlank(termsPartyName))		 &&
					(StringFunction.isBlank(termsPartyAcctNum))       &&
					(StringFunction.isBlank(termsPartyAcctCcy)) 	&&
					(StringFunction.isBlank(branchCode)))
			{
				isEmpty = true;
			}
		}

		return isEmpty;
	}


	/**
	 * This method is used to remove empty terms party fragments from the input xml
	 * document.  This is done so that a populateFromXmlDoc call won't create empty
	 * terms parties in the database.
	 *
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler - the input xml doc
	 * @param emptyTermsPartyInfo java.util.Hashtable - hashtable containing the names of
	 *                                                  the terms parties to remove
	 * @return inputDoc com.amsinc.ecsg.util.DocumentHandler - the updated input xml doc
	 *                                                  with the empty terms parties removed
	 */
	private DocumentHandler removeEmptyTermsParties(DocumentHandler inputDoc, Hashtable emptyTermsPartyInfo)
	{
		StringBuffer   componentPath  = null;
		Enumeration    keys           = null;
		String         termsPartyName = null;

		keys = emptyTermsPartyInfo.keys();

		while (keys.hasMoreElements())
		{
			termsPartyName = (String) keys.nextElement();

			componentPath = new StringBuffer("/Terms/");
			componentPath.append(termsPartyName);
			inputDoc.removeComponent(componentPath.toString());
		}

		return inputDoc;
	}

	/**
	 * This method is used to delete empty terms parties from the database if they have been
	 * cleared out by a user in a transaction jsp.
	 *
	 * @param emptyTermsPartyInfo java.util.Hashtable - hashtable containing the names and oids
	 *                                                  of the terms parties to delete
	 * @param terms com.ams.tradeportal.Terms - the current transaction's terms object
	 * @return mediatorServices com.amsinc.ecsg.frame.MediatorServices - the current mediator
	 *                                                  services object
	 */
	private Terms deleteEmptyTermsParties(Hashtable emptyTermsPartyInfo, Terms terms, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		StringBuffer   termsPartyOidPath = null;
		Enumeration    keys              = null;
		TermsParty     termsParty        = null;
		String         termsPartyName    = null;
		String         termsPartyOid     = null;

		keys = emptyTermsPartyInfo.keys();

		while (keys.hasMoreElements())
		{
			termsPartyName = (String) keys.nextElement();
			termsPartyOid  = (String) emptyTermsPartyInfo.get(termsPartyName);

			if (!(StringFunction.isBlank(termsPartyOid)))
			{
				termsParty = (TermsParty) mediatorServices.createServerEJB("TermsParty");
				termsParty.getData(Long.parseLong(termsPartyOid));
				termsParty.delete();

				termsPartyOidPath = new StringBuffer("c_");
				termsPartyOidPath.append(termsPartyName);

				terms.setAttribute(termsPartyOidPath.toString(), "");
			}
		}

		return terms;
	}

	/**
	 * Converts the /In/Terms/amount value to negative
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void resetAmountToNegative(DocumentHandler doc) {

		// Get the value and append a '-' to the beginning.
		String value = doc.getAttribute("/Terms/amount");
		if(StringFunction.isNotBlank(value)){
			value = "-" + value;
		}

		// Reset the value
		doc.setAttribute("/Terms/amount", value);
	}

	/**
	 * Deletes a particular shipment.  An audit record is created.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteShipment(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		DocumentHandler outputDoc           = new DocumentHandler();
		Terms	    terms;
		Transaction     transaction;
		String          instrumentOid       = "0";
		String          transactionOid      = "0";
		String          shipmentOid         = "0";

		try {

			// Get the instrument oid from the doc
			instrumentOid = inputDoc.getAttribute("/Instrument/instrument_oid");

			// Get the transaction oid from the doc
			transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");

			// Get the shipment oid from the doc.
			shipmentOid = inputDoc.getAttribute("/Terms/ShipmentTermsList/shipment_oid");
			String userOID = inputDoc.getAttribute("/LoginUser/user_oid");

			// Remove the shipment terms from the document so the shipment data is not saved
			inputDoc.removeComponent("/Terms/ShipmentTermsList");

			// Delete the shipment from the database if this is not a new shipment
			if (!StringFunction.isBlank(shipmentOid))
			{
				// Create the terms and call deleteShipmentTerms
				transaction = (Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
				terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
				terms.deleteShipmentTerms(instrumentOid, transactionOid, shipmentOid, userOID);
			}

		} catch (InvalidObjectIdentifierException ex) {
			// If the object does not exist, issue an error
			mediatorServices.getErrorManager().issueError (
					getClass().getName(), "INF01", ex.getObjectId());
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:deleteShipment");
			LOG.info("  " + e.toString());
			e.printStackTrace();
		}

		return outputDoc;
	}

	/**
	 * Deletes attached documents marked by the user on a transaction (CR-186 - jkok)
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

        String secretKeyString = mediatorServices.getCSDB().getCSDBValue("SecretKey");
        byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
        javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede/CBC/PKCS5Padding");
		DocumentHandler outputDoc = new DocumentHandler();

		List<String> attachedDocuments = inputDoc.getAttributes("/AttachedDocument");

		for(String attDoc : attachedDocuments) {
			if(StringFunction.isNotBlank(attDoc))
			{
				long docId = Long.parseLong(EncryptDecrypt.decryptStringUsingTripleDes(attDoc, secretKey));

			// Save the transaction oid in the mail message oid column.  Also
			// set transaction_oid to 0 to further flag that it was a deleted image for a transaction.
			DocumentImage documentImageInstance = (DocumentImage) mediatorServices.createServerEJB("DocumentImage", docId);
			documentImageInstance.setAttribute("mail_message_oid", documentImageInstance.getAttribute("transaction_oid"));
			documentImageInstance.setAttribute("transaction_oid", "0");
			documentImageInstance.save();
			}
		}
		return outputDoc;
	}

	private void verifyFTRQData(String fromCurrency,String baseCurrency,Transaction transaction, Terms terms, User user, MediatorServices mediatorServices) throws AmsException, RemoteException	//IAZ CM-451 04/27/09 Add

	{

		if(StringFunction.isBlank(terms.getAttribute("release_by_bank_on"))){

			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText("FundsTransferRequest.ForReleaseByBankOn", TradePortalConstants.TEXT_BUNDLE));

		}


		else {

			java.util.Date curDate = terms.getAttributeDate("release_by_bank_on");
			java.util.Date todaysDate = GMTUtility.getGMTDateTime();

			String timeZone = user.getAttribute("timezone");

			if (StringFunction.isNotBlank(timeZone))
			{
				todaysDate
				= TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(),
						timeZone);
			}

			//compare dates and set status/warning/error accordingly
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd");

			String curDateStr = formatter.format(curDate);
			String todaysDateStr = formatter.format(todaysDate);
			LOG.debug("International Terms Manager:::: " + curDateStr + " vs. " + todaysDateStr);

			if (curDateStr.compareTo(todaysDateStr) < 0)
			{
				terms.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.DOM_PMT_DATE_CANT_BE_BACKDATED);
			}
		}


		String amountCurrency = terms.getAttribute("amount_currency_code");

		//check if two foreign currency is involved
		if(isTwoForeignCurrencyInvolved(baseCurrency, null, fromCurrency, amountCurrency)){
			//IAZ CM-451 04/30/09 Begin
			//Also verify if Market Rate is used instead of FEC
			if 	(StringFunction.isBlank(terms.getAttribute("use_mkt_rate")))
				terms.setAttribute("use_mkt_rate", TradePortalConstants.INDICATOR_NO);

		}

	}

	private void verifyFTBAData(String fromCurrency, String toCurrency, String baseCurrency,Transaction transaction,Terms terms,User user, MediatorServices mediatorServices) throws AmsException, RemoteException {

		if (StringFunction.isBlank(transaction.getAttribute("payment_date"))) {
			mediatorServices.getErrorManager ().issueError (
					getClass().getName (), AmsConstants.REQUIRED_ATTRIBUTE,
					transaction.getResourceMgr().getText("TransferBetweenAccounts.DateOfTransfer",
							TradePortalConstants.TEXT_BUNDLE));
			transaction.setAttribute("transaction_status", TransactionStatus.STARTED);
		}else {  //validate date
			java.util.Date transferDate = transaction.getAttributeDate("payment_date");
			String sTodayDate = DateTimeUtility.getGMTDateTime();
			if (StringFunction.isNotBlank(user.getAttribute("timezone"))) {
				sTodayDate= DateTimeUtility.convertDateToDateString(TPDateTimeUtility.getLocalDateTime(sTodayDate,user.getAttribute("timezone")));
			}
			else {
				sTodayDate = sTodayDate.substring(0,10);
			}
			sTodayDate += " 00:00:00";
			java.util.Date todayDate = DateTimeUtility.convertStringDateTimeToDate(sTodayDate);

			if (todayDate.after(transferDate)) 	{
				mediatorServices.getErrorManager ().issueError (
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.TRANSFER_DATE_CANNOT_BE_BACKDATED);
				transaction.setAttribute("transaction_status", TransactionStatus.STARTED);
			}
		}

	}


	protected boolean isTwoForeignCurrencyInvolved(String baseCurrency, String transferCurrency, String fromCurrency, String toCurrency) {
		return (!fromCurrency.equals(baseCurrency) && !toCurrency.equals(baseCurrency) && !fromCurrency.equals(toCurrency));
	}

	protected void calculateEquivalentAmount(String fromCurrency, String toCurrency, String baseCurrency, Instrument instrument, Terms terms,
											 MediatorServices mediatorServices, Account fromAccount, String accOwnerOid, Transaction transaction,
											 DocumentHandler inputDoc) throws RemoteException, AmsException //MDB CR-640 Rel7.1 9/21/11
	{
		String transferCurrency = terms.getAttribute("amount_currency_code");
		String amount = terms.getAttribute("amount");
		String rate=null, ex_currency=null, s_ex_amount=null;
		String ex_ind = null;																		//IAZ ER 5.2.1.3 06/29/10 Add
		BigDecimal ex_amount=null;
		boolean flag = false; //MDB CR-640 Rel7.1 9/21/11

		if (StringFunction.isNotBlank(fromCurrency) && StringFunction.isNotBlank(toCurrency) && StringFunction.isNotBlank(transferCurrency) && StringFunction.isNotBlank(amount)){
			if (!(transferCurrency.equals(fromCurrency) && transferCurrency.equals(toCurrency))) {
				if (transferCurrency.equals(fromCurrency)) {
					ex_currency = toCurrency;
				}
				else if (transferCurrency.equals(toCurrency)) {
					ex_currency = fromCurrency;
				}

                //Read Rate from Terms. If present, use with all cases.
				Vector outMDInd = new Vector(1,1);														//IAZ ER 5.2.1.3 06/29/10 Add
				BigDecimal bigDecRate = null;															//IAZ ER 5.2.1.3 06/29/10 Add
				rate = terms.getAttribute("fec_rate");													//IAZ ER 5.2.1.3 06/29/10 Add

				//check if two foreign currency is involved
				//IAZ: if so AND rate is present, use default MULTIPLY calc method
				if (isTwoForeignCurrencyInvolved(baseCurrency,transferCurrency,fromCurrency,toCurrency)) {
					if (StringFunction.isNotBlank(rate)) {
						if ((StringFunction.isNotBlank(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
							((inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY)) ||
							(inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY_FX)))  )
						{
							int dec = 0;
							if (fromCurrency.equalsIgnoreCase(transferCurrency))
							{
								ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, toCurrency, rate);
							    dec = TPCurrencyUtility.getDecimalPrecision(toCurrency);
							}
							else
							{
								ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, fromCurrency, rate);
							    dec = TPCurrencyUtility.getDecimalPrecision(fromCurrency);
							}

							if (TradePortalConstants.MULTIPLY.equals(ex_ind))
								ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
							else if (TradePortalConstants.DIVIDE.equals(ex_ind))
								ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
						}
					}
					else
					{
						rate=InstrumentServices.getFXRate(transferCurrency, fromAccount, TradePortalConstants.USE_BUY_RATE);
						fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction);
						if (instrument.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
						{
							instrument.fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, toCurrency, amount, terms);
							flag = true;
						}
					}
				}
				else {

					String rateType=null;
					if (fromCurrency.equals(baseCurrency)) {
						rateType=TradePortalConstants.USE_SELL_RATE;
					}
					else {
						rateType=TradePortalConstants.USE_BUY_RATE;
					}

					//Only use rate from table if it is not provided by user
					//Obtain Calc Mathod as well
					if (ex_currency.equals(baseCurrency)) {
						if (StringFunction.isBlank(rate))
						{
							rate = InstrumentServices.getFXRate(transferCurrency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction); //MDB CR-640 Rel7.1 9/21/11
						}
						if (StringFunction.isNotBlank(rate))
							bigDecRate = new BigDecimal(rate);

				    	ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount,baseCurrency , accOwnerOid, rateType,
								bigDecRate, outMDInd);

						if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))
							ex_ind = (String)outMDInd.get(0);

					}
					else {
						if (StringFunction.isBlank(rate))
						{
							rate = InstrumentServices.getFXRate(ex_currency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, ex_currency, terms, mediatorServices, instrument, transaction);  //MDB CR-640 Rel7.1 9/21/11
						}
						if (StringFunction.isNotBlank(rate))
							bigDecRate = new BigDecimal(rate);
						ex_amount = instrument.getAmounInFXCurrency(ex_currency, baseCurrency, amount, accOwnerOid,rateType,
							bigDecRate, outMDInd);
						if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))
							ex_ind = (String)outMDInd.get(0);
					}
				}
			}
		}

		if (!flag) //MDB CR-640 Rel7.1 9/21/11
		{
			if (ex_amount == null) {
				rate=null;
				ex_currency=null;
				s_ex_amount=null;
				ex_ind = null;																				//IAZ ER 5.2.1.3 06/29/10 Add
			}
			else {
				s_ex_amount = ex_amount.toString();
			}
			terms.setAttribute("equivalent_exch_amount", s_ex_amount);
			terms.setAttribute("transfer_fx_rate", rate);
			terms.setAttribute("equivalent_exch_amt_ccy", ex_currency);
			terms.setAttribute("display_fx_rate_method", ex_ind);											//IAZ ER 5.2.1.3 06/29/10 Add
		}
	}


	/**
	 * Invokes a method on DomesticPaymentBean to add new Payee to a Domestic Payment
	 * Transaction or update an existing one. Returns success flag
	 *
	 * @return boolean
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param outputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private boolean updateDomesticPayment (DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		DomesticPayment domesticPayment = (DomesticPayment) mediatorServices.createServerEJB("DomesticPayment");
		boolean isUpdated =domesticPayment.updateDomesticPayment(inputDoc, outputDoc);
		InvoiceDetails invoiceDetail = (InvoiceDetails)mediatorServices.createServerEJB("InvoiceDetails");
		if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/InvoiceDetails/changed"))) {
			invoiceDetail.updateInvoiceDetail(inputDoc, outputDoc, domesticPayment);
			invoiceDetail.save(true);
		}
		return isUpdated;
	}

	/**
	 * Invokes a method on InvoicePaymentInstructions to add new Payee to a Loan Request
	 * Transaction or update an existing one. Returns success flag
	 *
	 * @return boolean
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param outputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private boolean updateInvPaymentInstructions (DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		InvoicePaymentInstructions invoicePaymentInstructions = (InvoicePaymentInstructions) mediatorServices.createServerEJB("InvoicePaymentInstructions");

		boolean isUpdated =invoicePaymentInstructions.updateInvPaymentInstructions(inputDoc, outputDoc);
		
		return isUpdated;
	}
	
	private void updateTermsParty (DocumentHandler inputDoc, MediatorServices mediatorServices,int numberOfPayees)
	throws RemoteException, AmsException
	{
		if (numberOfPayees == 1)
		{
			//Beneficiary Name may be upto 140 chars long. Terms only allow upt 35 chars values: use substring with set.
			String partyName = inputDoc.getAttribute("/InvoicePaymentInstructions/ben_name/");
			int termSizeLimit = partyName.length();
			if (termSizeLimit > 35) termSizeLimit = 35;
			inputDoc.setAttribute("/Terms/FirstTermsParty/name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
					partyName.substring(0, termSizeLimit));
			inputDoc.setAttribute("/Terms/reference_number", inputDoc.getAttribute("/InvoicePaymentInstructions/customer_reference/"));
			inputDoc.setAttribute("/Terms/FirstTermsParty/terms_party_type", TermsPartyType.BENEFICIARY);
		}
		if (numberOfPayees > 1)
		{
			if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						mediatorServices.getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE));

				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						mediatorServices.getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE));
			}
			else
			{
				inputDoc.setAttribute("/Terms/FirstTermsParty/name",
						mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));

				inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name",
						mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE));
				inputDoc.setAttribute("/Terms/FirstTermsParty/terms_party_type", TermsPartyType.BENEFICIARY);
			}
			//IR - PAUJ083080997 - Set Address Line 1 , 2 and 3 to Empty in case of Multiple Payee.
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_1", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_2", "");
			inputDoc.setAttribute("/Terms/FirstTermsParty/address_line_3", "");
			inputDoc.setAttribute("/Terms/reference_number", mediatorServices.getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE));
		}
		
	}
	

	private DocumentHandler verifyDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices,
			Transaction transaction, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode) //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	throws RemoteException, AmsException
	{
		return verifyDomesticPayment (inputDoc, mediatorServices,
				transaction, pmValueDates,
				creditAccountBankCountryCode,debitAccountBankCountryCode, null, null, null, null);
	}

	/**
	 * Invokes a method on DomesticPaymentBean to verify whether entered Payees
	 * contains all needed data. Cancells "Ready to Authorize" status if error
	 * is found.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @param transaction com.ams.tradeportal.busobj.Transaction
	 * @param pmValueDates Hashtable - contains calculated Value Dates for each Payment Method
	 * @param creditAccountBankCountryCode country code used in bank branch validation
	 * @param debitAccountBankCountryCode country code used in bank branch validation
	 */
	//private DocumentHandler verifyDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices)
	//private DocumentHandler verifyDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices, Transaction transaction)
	private DocumentHandler verifyDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices,
			Transaction transaction, Hashtable pmValueDates,
			String creditAccountBankCountryCode, String debitAccountBankCountryCode, Panel panel, Map<String, String> dmstPymtPanelRangeMap, String baseCurrency, String transactionOrg) //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	throws RemoteException, AmsException
	{


		//Domestic Payment oid must be supplied
		long domesticPaymentOid = inputDoc.getAttributeLong("/DomesticPayment/domestic_payment_oid/");
		if (domesticPaymentOid == 0)
		{
   			mediatorServices.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_OID_NOT_SUPPLIED_WRN);
			return inputDoc;
		}


		long methStart = System.currentTimeMillis();

		DomesticPayment domesticPayment = (DomesticPayment) mediatorServices.createServerEJB("DomesticPayment",domesticPaymentOid);
		LOG.debug("\t[TransactionMediatorBean.verifyDomesticPayment(DocumentHandler,MediatorServices,Transaction,Hashtable)] "
				+"\t[PERFORMANCE]"
				+"\tcreateDPBeanInTran"
				+"\t"+(System.currentTimeMillis()-methStart)
				+"\tmilliseconds");

		LOG.debug("[PERFORMANCE-1]" + "\tCalling DPBean" + "\t" + System.currentTimeMillis());
		//Nar Rel8.3.0.0 CR 857 07/17/2013 Begin
		if(panel != null){
		  String amount = domesticPayment.getAttribute("amount");
		  String transferCurrency = inputDoc.getAttribute("/Terms/amount_currency_code");
		  Account account = (Account) mediatorServices.createServerEJB("Account", inputDoc.getAttributeLong("/Terms/debit_account_oid"));
		  String fromCurrency = account.getAttribute("currency");
		  boolean isAccountCurrecny = false;
          if("ACCT_CCY".equals(panel.getCcyBasis())){
          	isAccountCurrecny = true;
          }
		  BigDecimal amountToPanelGroup = PanelAuthUtility.getAmountToPanelGroup(transferCurrency, amount, baseCurrency, fromCurrency, transactionOrg,
				  isAccountCurrecny, mediatorServices.getErrorManager());
		  if (amountToPanelGroup.compareTo(BigDecimal.ZERO) < 0 ){
			  return inputDoc;
		  }
		  PanelRange panelRange = panel.getRange(amountToPanelGroup.toString());
		  if (panelRange == null) {
				String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
								TradePortalConstants.INSTRUMENT_TYPE,
								inputDoc.getAttribute("/Instrument/instrument_type_code"),
								mediatorServices.getResourceManager().getResourceLocale());
				String formattedAmountInBase = TPCurrencyUtility.getDisplayAmount(amount, transferCurrency,
								mediatorServices.getResourceManager().getResourceLocale());

				mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
								formattedAmountInBase, instrumentTypeDesc);
				return inputDoc;
		  }

		  String panelRangeOid = panelRange.getPanelRangeOid();
		  if(!dmstPymtPanelRangeMap.containsKey(panelRangeOid)){
			long dmstPmtPanelAuthRangeOid = transaction.newComponent("DmstPmtPanelAuthRangeList");
			DmstPmtPanelAuthRange dmstPmtPanelAuthRange = (DmstPmtPanelAuthRange) transaction.getComponentHandle("DmstPmtPanelAuthRangeList", dmstPmtPanelAuthRangeOid);
			dmstPmtPanelAuthRange.setAttribute("panel_auth_range_oid", panelRangeOid);
			dmstPymtPanelRangeMap.put(panelRangeOid,Long.toString(dmstPmtPanelAuthRangeOid));
		  }
		  domesticPayment.setAttribute("dmst_pmt_panel_auth_range", dmstPymtPanelRangeMap.get(panelRangeOid));
		}
		//Nar Rel8.3.0.0 CR 857 07/17/2013 End
		String verifyResult = domesticPayment.verifyDomesticPayment(inputDoc, pmValueDates,		//IAZ CR-507/509 02/22/10 CHG
				creditAccountBankCountryCode, debitAccountBankCountryCode); //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
		try {
			long removeStart = System.currentTimeMillis();
			if (verifyResult.equals(TradePortalConstants.DOM_PMT_VERIFY_SUCCESSFUL))					//IAZ IR-IIUK061560871 06/07/10 ADD
				domesticPayment.remove();
			LOG.debug("\t[TransactionMediatorBean.verifyDomesticPayment(DocumentHandler,MediatorServices,Transaction,Hashtable)] "
				+"\t[PERFORMANCE]"
				+"\tremoveDPBeanInTran"
				+"\t"+(System.currentTimeMillis()-removeStart)
				+"\tmilliseconds");
		}
		catch (Exception remexc) {
			System.err.println("[TransactionMediatorBean.verifyDomesticPayment(DocumentHandler,MediatorServices,Transaction,Hashtable)] "
					+"Exception caught while trying to remove a domesticPayment. Continue anyway. Exception message: "+remexc.getMessage());
			remexc.printStackTrace();
		}

		LOG.debug("TransactionMediator::verifyDomesticPayment:: " + verifyResult);
		if (!verifyResult.equals(TradePortalConstants.DOM_PMT_VERIFY_SUCCESSFUL))
		{
			LOG.debug("TransactionMediator::verifyDomesticPayment::Failed to Verify Successfully");
			LOG.info("TransactionMediator::verifyDomesticPayment::Set Transaction Status to " + TransactionStatus.STARTED);
			String templateTypeCode = inputDoc.getAttribute("/Instrument/instrument_type_code");			// IAZ 01/14/10 IR-PUK011354564 Add
			transaction.getErrorManager().issueError(getClass().getName(),
					TradePortalConstants.DOM_PMT_TRAN_NOT_UPDATED,
					mediatorServices.getResourceManager().getText(templateTypeCode + ".FirstParty",		// IAZ 01/14/10 IR-PUK011354564 Add
							TradePortalConstants.TEXT_BUNDLE));		// IAZ 01/14/10 IR-PUK011354564 Add


			transaction.setAttribute("transaction_status", TransactionStatus.STARTED);
		}

		LOG.debug("\t[TransactionMediatorBean.verifyDomesticPayment(DocumentHandler,MediatorServices,Transaction,Hashtable)] "
				+"\t[PERFORMANCE]"
				+"\tverifyDomesticPayment"
				+"\t"+(System.currentTimeMillis()-methStart)
				+"\tmilliseconds");
		return inputDoc;
	}


	/**
	 * Invokes a method on DomesticPaymentBean to delete a Payee from a Domestic
	 * Payment Transaction or Template. Raises an error if unsuccessful.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler deleteDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		return deleteDomesticPayment (inputDoc, mediatorServices, false, false);
	}
	private DocumentHandler deleteDomesticPayment (DocumentHandler inputDoc, MediatorServices mediatorServices, boolean verbose, boolean updateAmount)
	throws RemoteException, AmsException
	{
		LOG.debug("deleteDomPmt::tran amount" + inputDoc.getAttribute("/Terms/amount"));

		DomesticPayment domesticPayment = (DomesticPayment) mediatorServices.createServerEJB("DomesticPayment");
		String verifyResult = domesticPayment.deleteDomesticPayment(inputDoc);

		if (verifyResult.equals(TradePortalConstants.TASK_NOT_SUCCESSFUL))
		{
			mediatorServices.getErrorManager ().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PROVIDE_ALL_REQ_PARMS,
					" ");
		}
		else if (verifyResult.equals(TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE))
		{
			mediatorServices.getErrorManager ().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE);
		}
        else if (TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR.equals(verifyResult))
        {
        	mediatorServices.getErrorManager().issueError(
            	TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR);
        }
		else
		{
			LOG.debug("TransactionMediatorBean::deletePayee::OK verbose/updateAmount " + verbose + " " + updateAmount);
			if (verbose)
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYEE_DELETE_SUCCESSFUL,
						verifyResult);
			}
			if (updateAmount)
			{
				String transAmount = inputDoc.getAttribute("/Terms/amount");			//IAZ CR-483 08/13/09 CHT
				String domPaymentAmount = inputDoc.getAttribute("/DomesticPayment/amount");
				LOG.debug(transAmount);
				try
				{
					//calculate new tarnsaction amount
					BigDecimal newTransAmount = new BigDecimal(transAmount);
					BigDecimal dpAmount = null;
					try
					{
						dpAmount = new BigDecimal(domPaymentAmount);
					}
					catch(java.lang.NumberFormatException nf_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::NumberFormanException getting dom pmt amout.");
						LOG.info("Set amount to 0.00.");
						dpAmount = BigDecimal.ZERO;
					}
					newTransAmount = newTransAmount.subtract(dpAmount);
					inputDoc.setAttribute("/Terms/amount", newTransAmount.toString());
					LOG.debug("TransactionMediatorBean::deletePayee::amount " + inputDoc.getAttribute("/Terms/amount"));

					// Use TextBundles to Specify Payees/Payers and Ref No Constants, whenever appropriate
					//String payeeName = TradePortalConstants.MULTIPLE_PAYEES_ENTERED;
					String payeeReference =
						mediatorServices.getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE);
					String payeeName =
						mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE);
					if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						payeeName = mediatorServices.getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE);
					int prevNumberOfPayees = 0;
					try
					{
						prevNumberOfPayees = inputDoc.getAttributeInt("/prev_payee_number/");
						if (prevNumberOfPayees == 1) {
							payeeName = "";
							payeeReference = "";
						}
						if (prevNumberOfPayees == 2)
						{
							if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
								payeeName = "Payer";
							else
								payeeName = "Beneficiary";

							payeeReference	=
								mediatorServices.getResourceManager().getText("Common.Reference", TradePortalConstants.TEXT_BUNDLE);

						}
					}
					catch (Exception any_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::Exception setting number of payees:: " + any_exc.toString());
						LOG.info("This is OK: will update screen value on save/verify.");
					}

					long transactionOid = inputDoc.getAttributeLong("/Transaction/transaction_oid");
					Transaction transaction
					= (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);
					Terms terms
					= (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
					LOG.debug("TransactionBean::deletePayee:: terms are " + terms);

					terms.setAttribute("amount", newTransAmount.toString());
					if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						terms.setAttribute("funding_amount", newTransAmount.toString());
					terms.setAttribute("reference_number", payeeReference);								//IAZ IR-PMUK012801255 02/03/10 Add
					transaction.setAttribute("copy_of_amount", newTransAmount.toString());
					transaction.setAttribute("instrument_amount", transaction.getAttribute("copy_of_amount"));

					String instrumentOidStr = inputDoc.getAttribute("/Instrument/instrument_oid");
					if (StringFunction.isBlank(instrumentOidStr))
					{
						LOG.info("TransactionMediatorBean::deletePayee:: Instrument_oid not present.");
						LOG.info("This is OK: will update screen values on save/verify.");
					}
					else
					{
						long instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
						Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
						if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						{
							instrument.getData(instrumentOid);
							calculateDPEquaivalentPaymentAmount(inputDoc,instrument, transaction, terms, newTransAmount.toString(),
									TransactionStatus.STARTED, mediatorServices);
						}
						//IAZ IR-PMUK012801255 02/03/10 Begin: If there is only one Payee/Payer remains, update Transaction's Ref No.
						if (prevNumberOfPayees == 2) {
							instrument.setAttribute("copy_of_ref_num", payeeReference);
							instrument.save(false);
						}
						//IAZ IR-PMUK012801255 02/03/10 End
					}
					try
					{
						TermsParty firstParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
						firstParty.setAttribute("name", payeeName);
						firstParty.setAttribute("contact_name", payeeName);
						LOG.debug("TransactionMediatorBean::deletePayee::completed setting updated tran data for save:: " + firstParty.getAttribute("contact_name"));
					}
					catch (Exception any_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::Exception setting number of payees:: " + any_exc.toString());
						LOG.info("This is OK: will update screen value on save/verify.");
					}

					transaction.save(false);
				}

				catch(Exception any_exc)
				{
					LOG.info("TransactionMediatorBean::deletePayee::Exception setting new amount:: " + any_exc.toString());
					LOG.info("This is OK: will update screen values on save/verify.");

				}
			}
		}

		return inputDoc;
	}

	private DocumentHandler deleteInvoicePaymentInstruction (DocumentHandler inputDoc, MediatorServices mediatorServices, boolean verbose, boolean updateAmount)
	throws RemoteException, AmsException
	{
		LOG.debug("deleteDomPmt::tran amount" + inputDoc.getAttribute("/Terms/amount"));

		InvoicePaymentInstructions invoicePaymentInstructions = (InvoicePaymentInstructions) mediatorServices.createServerEJB("InvoicePaymentInstructions");
		String verifyResult = invoicePaymentInstructions.deleteInvoicePaymentInstruction(inputDoc);

		if (verifyResult.equals(TradePortalConstants.TASK_NOT_SUCCESSFUL))
		{
			mediatorServices.getErrorManager ().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PROVIDE_ALL_REQ_PARMS,
					" ");
		}

		else if (verifyResult.equals(TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE))
		{
			mediatorServices.getErrorManager ().issueError (
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DP_OID_NOT_PROVIDED_FOR_DELETE);
		}
        else if (TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR.equals(verifyResult))
        {
        	mediatorServices.getErrorManager().issueError(
            	TradePortalConstants.ERR_CAT_1,
                TradePortalConstants.CANT_DELETE_DOM_PMT_FIXED_TR);
        }

		else
		{

			LOG.debug("TransactionMediatorBean::deletePayee::OK verbose/updateAmount " + verbose + " " + updateAmount);
			if (verbose)
			{
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PAYEE_DELETE_SUCCESSFUL,
						verifyResult);
			}
			if (updateAmount)
			{
				//String transAmount = inputDoc.getAttribute("/Terms/funding_amount");
				String transAmount = inputDoc.getAttribute("/Terms/amount");
				String domPaymentAmount = inputDoc.getAttribute("/InvoicePaymentInstructions/amount");
				LOG.debug(transAmount);
				try
				{
					//calculate new tarnsaction amount
					BigDecimal newTransAmount = new BigDecimal(transAmount);
					BigDecimal dpAmount = null;
					try
					{
						dpAmount = new BigDecimal(domPaymentAmount);
					}
					catch(java.lang.NumberFormatException nf_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::NumberFormanException getting pmt amout.");
						LOG.info("Set amount to 0.00.");
						dpAmount = BigDecimal.ZERO;
					}
					newTransAmount = newTransAmount.subtract(dpAmount);
					inputDoc.setAttribute("/Terms/amount", newTransAmount.toString());
					LOG.debug("TransactionMediatorBean::deletePayee::amount " + inputDoc.getAttribute("/Terms/amount"));


					// Use TextBundles to Specify Payees/Payers and Ref No Constants, whenever appropriate
					//String payeeName = TradePortalConstants.MULTIPLE_PAYEES_ENTERED;
					String payeeReference =
						mediatorServices.getResourceManager().getText("Common.MultReferences", TradePortalConstants.TEXT_BUNDLE);
					String payeeName =
						mediatorServices.getResourceManager().getText("Common.MultBenesEntered", TradePortalConstants.TEXT_BUNDLE);
					if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						payeeName = mediatorServices.getResourceManager().getText("Common.MultPayersEntered", TradePortalConstants.TEXT_BUNDLE);
					int prevNumberOfPayees = 0;
					try
					{
						prevNumberOfPayees = inputDoc.getAttributeInt("/prev_payee_number/");
						if (prevNumberOfPayees == 1) {
							payeeName = "";
							payeeReference = "";
						}
						if (prevNumberOfPayees == 2)
						{
							if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
								payeeName = "Payer";
							else
								payeeName = "Beneficiary";

							payeeReference	=
								mediatorServices.getResourceManager().getText("Common.Reference", TradePortalConstants.TEXT_BUNDLE);

						}
					}
					catch (Exception any_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::Exception setting number of payees:: " + any_exc.toString());
						LOG.info("This is OK: will update screen value on save/verify.");
					}

					long transactionOid = inputDoc.getAttributeLong("/Transaction/transaction_oid");
					Transaction transaction
					= (Transaction) mediatorServices.createServerEJB("Transaction", transactionOid);
					Terms terms
					= (Terms)(transaction.getComponentHandle("CustomerEnteredTerms"));
					LOG.debug("TransactionBean::deletePayee:: terms are " + terms);

					terms.setAttribute("amount", newTransAmount.toString());
					if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						terms.setAttribute("funding_amount", newTransAmount.toString());
					terms.setAttribute("reference_number", payeeReference);
					transaction.setAttribute("copy_of_amount", newTransAmount.toString());
					transaction.setAttribute("instrument_amount", transaction.getAttribute("copy_of_amount"));

					String instrumentOidStr = inputDoc.getAttribute("/Instrument/instrument_oid");
					if (StringFunction.isBlank(instrumentOidStr))
					{
						LOG.info("TransactionMediatorBean::deletePayee:: Instrument_oid not present.");
						LOG.info("This is OK: will update screen values on save/verify.");
					}
					else
					{
						long instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
						Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
						if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(inputDoc.getAttribute("/Instrument/instrument_type_code")))
						{
							instrument.getData(instrumentOid);
							calculateDPEquaivalentPaymentAmount(inputDoc,instrument, transaction, terms, newTransAmount.toString(),
									TransactionStatus.STARTED, mediatorServices);
						}
						//If there is only one Payee/Payer remains, update Transaction's Ref No.
						if (prevNumberOfPayees == 2) {
							instrument.setAttribute("copy_of_ref_num", payeeReference);
							instrument.save(false);
						}
					}
					try
					{
						TermsParty firstParty = (TermsParty) terms.getComponentHandle("FirstTermsParty");
						firstParty.setAttribute("name", payeeName);
						firstParty.setAttribute("contact_name", payeeName);
						LOG.debug("TransactionMediatorBean::deletePayee::completed setting updated tran data for save:: " + firstParty.getAttribute("contact_name"));
					}
					catch (Exception any_exc)
					{
						LOG.info("TransactionMediatorBean::deletePayee::Exception setting number of payees:: " + any_exc.toString());
						LOG.info("This is OK: will update screen value on save/verify.");
					}

					transaction.save(false);
				}

				catch(Exception any_exc)
				{
					LOG.info("TransactionMediatorBean::deletePayee::Exception setting new amount:: " + any_exc.toString());
					LOG.info("This is OK: will update screen values on save/verify.");

				}
			}
		}

		return inputDoc;
	}

	private DocumentHandler processMultipleDomesticPayments (DocumentHandler inputDoc, MediatorServices mediatorServices, String performType, Transaction transaction)
	throws RemoteException, AmsException
	{
		return processMultipleDomesticPayments (inputDoc, mediatorServices, performType, transaction, null, null, null);

	}

	/**
	 * This method gest a list of all Payees for the given Domestic Payment
	 * Transaction or Template and invokes an action (e.g, Delete or Verify) specified
	 * by performType parameter for each Payee on the list.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @param transaction com.ams.tradeportal.busobj.Transaction
	 * @param pmValueDates Hashtable - contains calculated Value Dates for each Payment Method
	 */

	private DocumentHandler processMultipleDomesticPayments (DocumentHandler inputDoc, MediatorServices mediatorServices, String performType,
			Transaction transaction, Panel panel, String baseCurrency, String transactionOrg)
	throws RemoteException, AmsException
	{
		long methodStart = System.currentTimeMillis();

		DocumentHandler outputDoc = new DocumentHandler();

		if (StringFunction.isBlank(performType))
		{
			return inputDoc;
		}

		try
		{
			if (TradePortalConstants.BUTTON_VERIFY.equals(performType) && StringFunction.isNotBlank(transactionOrg))
			{
			  String currencyCode = inputDoc.getAttribute("/Terms/amount_currency_code");
			  CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
					Long.parseLong(transactionOrg));
			  InstrumentServices.verifyCurrencyDefineForFXGroup(corpOrg, currencyCode, transaction.getErrorManager());
			}
			String transactionOid = (new Long(inputDoc.getAttributeLong("/Transaction/transaction_oid"))).toString();

			String sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE, DP.SEQUENCE_NUMBER FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?";
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{transactionOid});

			int curIndex = 0;
			//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
			int verifyCommitCounter = 0;
			//trudden CR-564 08/31/10 end
			String domesticPaymentOidStr =
				resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

			//rkazi CR-596 11/16/2010 Verify payment method for all benefeciaries to be same
			String firstPaymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/PAYMENT_METHOD_TYPE");
			Map invalidDomesticPaymentMehtodMap = new HashMap();
			//rkazi CR-596 11/16/2010 End

			//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
			//removed logic from DomesticPaymentBean to instantiate an Account object for every domestic payment, because it is the
			//same Account for all Dom. Payments in one transaction.   The TransactionMediator will now
			//determine the account's bank country code before validating each Dom. Payment.

			String creditAccountBankCountryCode = this.getBankBranchCountryForAccount(inputDoc.getAttribute("/Terms/credit_account_oid"), mediatorServices);
			String debitAccountBankCountryCode = this.getBankBranchCountryForAccount(inputDoc.getAttribute("/Terms/debit_account_oid"), mediatorServices);
			//trudden CR-564 08/31/10 End


			Hashtable pmValueDates = new Hashtable();													//IAZ CR-507/509 02/22/10
			long whileStart = System.currentTimeMillis();
			Map<String, String> dmstPymtPanelRangeMap = new HashMap<String, String>();

			while (
					(domesticPaymentOidStr != null) &&
					(!domesticPaymentOidStr.equals(""))
			)
			{
				inputDoc.setAttribute("/DomesticPayment/domestic_payment_oid/",
						domesticPaymentOidStr);
				if (
						(performType.equals(TradePortalConstants.BUTTON_DELETE_PAYEE)) ||
						(performType.equals(TradePortalConstants.BUTTON_DELETE))
				)
				{
					long deleteStart = System.currentTimeMillis();
					outputDoc = deleteDomesticPayment (inputDoc, mediatorServices);
					LOG.debug("\t[TransactionMediatorBean.processMultipleDomesticPayments(DocumentHandler,MediatorServices,String,Transaction] "
							+"\t[PERFORMANCE]"
							+"\tdeleteDomesticPayment"
							+"\t"+(System.currentTimeMillis()-deleteStart)
							+"\tmilliseconds");
				}

				if (performType.equals(TradePortalConstants.BUTTON_VERIFY))
				{
					long verifyStart = System.currentTimeMillis();

					//rkazi CR-596 11/16/2010 Verify payment method for all benefeciaries to be same
					if (curIndex != 0){
						String paymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/PAYMENT_METHOD_TYPE");
						String sequenceNumber = resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/SEQUENCE_NUMBER");
						if (!firstPaymentMethodType.equalsIgnoreCase(paymentMethodType)){
							//Raise Error
							invalidDomesticPaymentMehtodMap.put(sequenceNumber, paymentMethodType);
						}
					}
					//rkazi CR-596 11/16/2010 End
					outputDoc = verifyDomesticPayment (inputDoc, mediatorServices, transaction, pmValueDates,	//IAZ CR-507/509 02/22/10 CHG
							creditAccountBankCountryCode, debitAccountBankCountryCode, panel, dmstPymtPanelRangeMap, baseCurrency, transactionOrg); //trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
					LOG.debug("\t[TransactionMediatorBean.processMultipleDomesticPayments(DocumentHandler,MediatorServices,String,Transaction] "
							+"\t[PERFORMANCE]"
							+"\tverifyDomesticPayment"
							+"\t"+(System.currentTimeMillis()-verifyStart)
							+"\tmilliseconds");
					if (transaction.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
					{
						break;
					}
				}

				curIndex++;

				domesticPaymentOidStr =
					resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");

				//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
				verifyCommitCounter++;
				if (verifyCommitCounter > TransactionMediatorBean.verifyCommitLimit) {
					//perform commit to avoid transaction timeouts while verifying
					verifyCommitCounter = commitTransaction();
				}
				//trudden CR-564 08/31/10 end
			}
			if(!dmstPymtPanelRangeMap.isEmpty()){
				String panelApprovalsList = PanelAuthUtility.getPanelAuthAwaitingApprovalForBene(dmstPymtPanelRangeMap, transaction, InstrumentType.DOM_PMT, mediatorServices.getErrorManager());
				transaction.setAttribute("panel_auth_next_approvals", panelApprovalsList);
			}

			//rkazi CR-596 11/16/2010 Verify payment method for all benefeciaries to be same
			if (!invalidDomesticPaymentMehtodMap.isEmpty()){
				LOG.debug("TransactionMediator::processMultipleDomesticPayments::Failed to Verify Successfully");
				System.out
						.println("TransactionMediator::processMultipleDomesticPayments::Set Transaction Status to "
								+ TransactionStatus.STARTED);
				// Need to get proper error message to display. Temporarily
				// using existing error message.

				transaction.getErrorManager().issueError(
						getClass().getName(),
						TradePortalConstants.DOM_PMT_MTHD_NOT_UNIQUE);
				transaction.setAttribute("transaction_status",
						TransactionStatus.STARTED);
			}
			//rkazi CR-596 11/16/2010 End

			LOG.debug("\t[TransactionMediatorBean.processMultipleDomesticPayments(DocumentHandler,MediatorServices,String,Transaction] "
					+"\t[PERFORMANCE]"
					+"\twhile-loop"
					+"\t"+(System.currentTimeMillis()-whileStart)
					+"\tmilliseconds");

			String typeCode = transaction.getAttribute("copy_of_instr_type_code");
			if ((StringFunction.isNotBlank(typeCode)) && (typeCode.equals(InstrumentType.DOM_PMT)))
			{
				if(StringFunction.isNotBlank(inputDoc.getAttribute("/Transaction/payment_date")))
				{
					if (DateTimeUtility.convertStringDateToDate(inputDoc.getAttribute("/Transaction/payment_date")).after(GMTUtility.getGMTDateTime()))
						transaction.setAttribute("payment_date", inputDoc.getAttribute("/Transaction/payment_date"));
				}
			}
		}
		catch (Exception any_e)
		{
			outputDoc = inputDoc;
		}

		LOG.debug("\t[TransactionMediatorBean.processMultipleDomesticPayments(DocumentHandler,MediatorServices,String,Transaction] "
				+"\t[PERFORMANCE]"
				+"\tprocessMultipleDomesticPayments"
				+"\t"+(System.currentTimeMillis()-methodStart)
				+"\tmilliseconds");

		return outputDoc;

	}

	/**
	 * Verify the uploaded domestic payment again on-line.  The user cannot update the uploaded
	 * domestica payment on-line.  So we do not need to verify them one by one.  We just need to
	 * calculate one value date and update all domestic payment.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @param transaction com.ams.tradeportal.busobj.Transaction
	 */

	/* POUL012577173 Add reVerifyUploadedDomesticPayments to improve performance */
	private DocumentHandler reVerifyUploadedDomesticPayments (DocumentHandler inputDoc, MediatorServices mediatorServices, Transaction transaction, Panel panel,
			String baseCurrency, String transactionOrg)
	throws RemoteException, AmsException
	{
		long methodStart = System.currentTimeMillis();
		DocumentHandler outputDoc = new DocumentHandler();
		boolean isBenePanelAuthCheck = TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"));

		try
		{
			// Get one domestic payment and calculate the value date.  The value date should be the same for all.
			String transactionOid =	(new Long(inputDoc.getAttributeLong("/Transaction/transaction_oid"))).toString();
			String sqlQuery = "";
			if(isBenePanelAuthCheck && !TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/ProcessBenePanelProcess"))){
			    sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE, DP.SEQUENCE_NUMBER, DP.AMOUNT FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ?";
			}else{
				sqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID, DP.PAYMENT_METHOD_TYPE, DP.SEQUENCE_NUMBER FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ? and rownum = 1";
			}
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery,true, new Object[]{transactionOid});
			if (resultsDoc == null) {
				return outputDoc;
			}
			String domesticPaymentOidStr = resultsDoc.getAttribute("/ResultSetRecord(0)/DOMESTIC_PAYMENT_OID");
			if (StringFunction.isBlank(domesticPaymentOidStr)) {
				return outputDoc;
			}
			String creditAccountBankCountryCode = this.getBankBranchCountryForAccount(inputDoc.getAttribute("/Terms/credit_account_oid"), mediatorServices);
			String debitAccountBankCountryCode = this.getBankBranchCountryForAccount(inputDoc.getAttribute("/Terms/debit_account_oid"), mediatorServices);
			inputDoc.setAttribute("/DomesticPayment/domestic_payment_oid/",	domesticPaymentOidStr);
			Hashtable pmValueDates = new Hashtable();
			outputDoc = verifyDomesticPayment (inputDoc, mediatorServices, transaction, pmValueDates,
							creditAccountBankCountryCode, debitAccountBankCountryCode);
			if (transaction.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
			{
				return outputDoc;
			}

			String valueDate = inputDoc.getAttribute("/DomesticPayment/value_date");

			// Use SQL to update the value date of all the domestic payment in order to gain performance.
			if (valueDate != null) {
				String updateValueDateSQL = "UPDATE DOMESTIC_PAYMENT SET VALUE_DATE = to_date(?, 'mm/dd/yyyy') where p_transaction_oid = ?";
				DatabaseQueryBean.executeUpdate(updateValueDateSQL, false, valueDate, transactionOid);
			}

			String typeCode = transaction.getAttribute("copy_of_instr_type_code");
			if ((StringFunction.isNotBlank(typeCode)) && (typeCode.equals(InstrumentType.DOM_PMT)))
			{
				if(StringFunction.isNotBlank(inputDoc.getAttribute("/Transaction/payment_date")))
				{
					if (DateTimeUtility.convertStringDateToDate(inputDoc.getAttribute("/Transaction/payment_date")).after(GMTUtility.getGMTDateTime()))
						transaction.setAttribute("payment_date", inputDoc.getAttribute("/Transaction/payment_date"));
				}
			}
            // Nar Rel8.3.0.0 CR587 Begin
			if (isBenePanelAuthCheck && !TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/ProcessBenePanelProcess")) && panel != null) {
				String domesticPaymentSQl = "UPDATE domestic_payment SET a_dmst_pmt_panel_auth_range = ? WHERE domestic_payment_oid = ?";
				List<DocumentHandler> domesticPaymentList = resultsDoc.getFragmentsList("/ResultSetRecord");
				Map<String, String> dmstPymtPanelRangeMap = new HashMap<String, String>();
                String transferCurrency = inputDoc.getAttribute("/Terms/amount_currency_code");
                Account account = (Account) mediatorServices.createServerEJB("Account", inputDoc.getAttributeLong("/Terms/debit_account_oid"));
                String fromCurrency = account.getAttribute("currency");
                boolean isAccountCurrecny = false;
                if("ACCT_CCY".equals(panel.getCcyBasis())){
                	isAccountCurrecny = true;
                }

				try(Connection con = DatabaseQueryBean.connect(true);
				PreparedStatement pstmt = con.prepareStatement(domesticPaymentSQl)){

				for (DocumentHandler doc : domesticPaymentList) {
					String amount = doc.getAttribute("/AMOUNT");
					BigDecimal amountToPanelGroup = PanelAuthUtility.getAmountToPanelGroup(transferCurrency, amount, baseCurrency, fromCurrency, transactionOrg,
							isAccountCurrecny, mediatorServices.getErrorManager());
					PanelRange panelRange = panel.getRange(amountToPanelGroup.toString());
					if (panelRange == null) {
							String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
											TradePortalConstants.INSTRUMENT_TYPE,
											inputDoc.getAttribute("/Instrument/instrument_type_code"),
											mediatorServices.getResourceManager().getResourceLocale());
							String formattedAmountInBase = TPCurrencyUtility.getDisplayAmount(amount, transferCurrency,
											mediatorServices.getResourceManager().getResourceLocale());

							mediatorServices.getErrorManager().issueError(
											TradePortalConstants.ERR_CAT_1,
											TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
											formattedAmountInBase, instrumentTypeDesc);
						return outputDoc;
					 }
					String panelRangeOid = panelRange.getPanelRangeOid();

					if (!dmstPymtPanelRangeMap.containsKey(panelRangeOid)) {
						long dmstPmtPanelAuthRangeOid = transaction.newComponent("DmstPmtPanelAuthRangeList");
						DmstPmtPanelAuthRange dmstPmtPanelAuthRange = (DmstPmtPanelAuthRange) transaction.getComponentHandle(
										"DmstPmtPanelAuthRangeList",
										dmstPmtPanelAuthRangeOid);
						dmstPmtPanelAuthRange.setAttribute("panel_auth_range_oid", panelRangeOid);
						dmstPymtPanelRangeMap.put(panelRangeOid, Long.toString(dmstPmtPanelAuthRangeOid));
					}
					String domesticPaymentOid = doc.getAttribute("/DOMESTIC_PAYMENT_OID");
					pstmt.setLong(1, Long.parseLong(dmstPymtPanelRangeMap.get(panelRangeOid)));
					pstmt.setLong(2, Long.parseLong(domesticPaymentOid));
					pstmt.addBatch();
				}
				pstmt.executeBatch();
				}
				
				//set the awaiting approvals for beneficiaries of transaction.
				if(!dmstPymtPanelRangeMap.isEmpty()){
					String panelApprovalsList = PanelAuthUtility.getPanelAuthAwaitingApprovalForBene(dmstPymtPanelRangeMap, transaction, InstrumentType.DOM_PMT, mediatorServices.getErrorManager());
					transaction.setAttribute("panel_auth_next_approvals", panelApprovalsList);
				}
			}

	     // Nar Rel8.3.0.0 CR587 End
		}
		catch (Exception any_e)
		{
			any_e.printStackTrace();
			outputDoc = inputDoc;
		}

		LOG.debug("\t[TransactionMediatorBean.reVerifyDomesticPayments(DocumentHandler,MediatorServices,Transaction] "
				+"\t[PERFORMANCE]"
				+"\treVerifyDomesticPayments"
				+"\t"+(System.currentTimeMillis()-methodStart)
				+"\tmilliseconds");

		return outputDoc;

	}

	/**
	 * This method replaces data on the current Domestic Payment transaction
	 * with the one from the template chosen on the page.  It replaces the payment
	 * date stored with Transaction object itself and then replaces all payees.
	 * No chnage in transaction status to occur.
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	private DocumentHandler performCopyFromTemplate(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("Performing Transaction CopyFromTemplate");

		DocumentHandler outputDoc = new DocumentHandler();

		try {
			// Since the transaction may have been partially authorized when the 'Edit Data'
			// button was pressed, the attributes that keep track of authorization information
			// must be cleared

			String sourceTransactionOid = inputDoc.getAttribute("/sourceTransactionOid");
			Transaction sourceTransaction
			= (Transaction) mediatorServices.createServerEJB("Transaction",
					Long.parseLong(sourceTransactionOid));
			String instrument_type = sourceTransaction.getAttribute("copy_of_instr_type_code");

			String termsOid = sourceTransaction.getAttribute("c_CustomerEnteredTerms");
			LOG.debug("TransactionBean::CopyFromTemplate:: source termsOid is " + termsOid);
			Terms sourceTerms
			= (Terms)(sourceTransaction.getComponentHandle("CustomerEnteredTerms"));
			LOG.debug("TransactionBean::CopyFromTemplate:: source terms are " + sourceTerms);


			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(inputDoc.getAttribute("/LoginUser/user_oid")));

            String confidIndicator = "";
			if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type))
			{
				confidIndicator = sourceTerms.getAttribute("confidential_indicator");
				LOG.debug("source ci " + confidIndicator);

	 			if ((TradePortalConstants.INDICATOR_YES.equals(confidIndicator))||
	 	    		((TradePortalConstants.CONF_IND_FRM_TMPLT.equals(confidIndicator))))
	 			{
					if (TradePortalConstants.INDICATOR_NO.equals(user.getAttribute("confidential_indicator")))
						{
							LOG.debug("Check ownership access status"); 				//IAZ CR-58609/29/10 Add
							if (TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level")))
                             {
                            if (TradePortalConstants.INDICATOR_NO.equals(user.getAttribute("subsid_confidential_indicator")))
							{
 							   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
 								TradePortalConstants.CANT_PROCESS_CONFID_PMTS);
 							   return inputDoc;
						    }
						}
				     }
				}
			}

			inputDoc.setAttribute("/Transaction/payment_date",
					sourceTransaction.getPaymentDate(user.getAttribute("timezone"), true));	//IAZ CR-586 08/18/10 Change


			inputDoc.setAttribute("/Terms/debit_account_oid",
					sourceTerms.getAttribute("debit_account_oid"));
			inputDoc.setAttribute("/Terms/amount",
					sourceTerms.getAttribute("amount"));
			inputDoc.setAttribute("/Terms/amount_currency_code",
					sourceTerms.getAttribute("amount_currency_code"));

			if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type))
			{
				LOG.debug("Making sure conf ind is set to corerct value: " + confidIndicator);
				if (!TradePortalConstants.CONF_IND_FRM_TMPLT.equals(confidIndicator))
				   confidIndicator = TradePortalConstants.INDICATOR_NO;

				   inputDoc.setAttribute("/Terms/confidential_indicator",
				                                 confidIndicator);

					inputDoc.setAttribute("/Instrument/confidential_indicator",
												confidIndicator);

			}

			if(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type))
				inputDoc.setAttribute("/Terms/credit_account_oid", sourceTerms.getAttribute("credit_account_oid"));

			if (!InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type))
			{
				inputDoc.setAttribute("/Terms/covered_by_fec_number",
						sourceTerms.getAttribute("covered_by_fec_number"));
				inputDoc.setAttribute("/Terms/fec_rate",
						sourceTerms.getAttribute("fec_rate"));
				inputDoc.setAttribute("/Terms/ex_rt_variation_adjust",
						sourceTerms.getAttribute("ex_rt_variation_adjust"));
			}

			if (InstrumentType.XFER_BET_ACCTS.equals(instrument_type)) {
				inputDoc.setAttribute("/Terms/credit_account_oid",
						sourceTerms.getAttribute("credit_account_oid"));
				inputDoc.setAttribute("/Terms/transfer_description",
						sourceTerms.getAttribute("transfer_description"));
			}

			if ((InstrumentType.DOM_PMT.equals(instrument_type)) ||
					(InstrumentType.FUNDS_XFER.equals(instrument_type)))
			{
				inputDoc.setAttribute("/Terms/finance_type_text",
						sourceTerms.getAttribute("finance_type_text"));
				inputDoc.setAttribute("/Terms/funding_amount",
						sourceTerms.getAttribute("funding_amount"));
			}

			if ( InstrumentType.FUNDS_XFER.equals(instrument_type)) {
				inputDoc=populateFTRQByTemplate(inputDoc,sourceTerms);
			}

			//For Domestic Payment transaction, remove old payee and insert ones
			//from source template
			if ((InstrumentType.DOM_PMT.equals(instrument_type)) ||
					(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type)))			//IAZ CM CR-509 12/29/09
			{
				outputDoc = processMultipleDomesticPayments(inputDoc, mediatorServices,TradePortalConstants.BUTTON_DELETE, null);

				String oid= sourceTransaction.getAttribute("instrument_oid");
				Instrument sourceInstrument
				= (Instrument) mediatorServices.createServerEJB("Instrument",
						Long.parseLong(oid));
				LOG.debug("TransactionMediatorbean::fixed payment flag " + sourceInstrument.getAttribute("fixed_payment_flag"));

				DomesticPayment domesticPayment
				= (DomesticPayment) mediatorServices.createServerEJB("DomesticPayment");
				domesticPayment.createNewDomesticPayments(sourceTransactionOid,
						inputDoc.getAttribute("/Transaction/transaction_oid"), //IAZ CR-586 IR-VRUK091652765 10/05/10 Add Parameter
						(TradePortalConstants.INDICATOR_YES.equals(sourceInstrument.getAttribute("fixed_payment_flag"))));

				if (InstrumentType.DOM_PMT.equals(instrument_type))
				{
					if (TradePortalConstants.INDICATOR_YES.equals(sourceInstrument.getAttribute("fixed_payment_flag")))
						inputDoc.setAttribute("/Terms/source_template_trans_oid", sourceTransactionOid);
					else
						inputDoc.setAttribute("/Terms/source_template_trans_oid", "");
				}

				inputDoc.setAttribute("/Terms/c_OrderingParty", copyOrderingParty(sourceTerms.getAttribute("c_OrderingParty"), mediatorServices));
				inputDoc.setAttribute("/Terms/c_OrderingPartyBank", copyOrderingParty(sourceTerms.getAttribute("c_OrderingPartyBank"), mediatorServices));

				if (StringFunction.isNotBlank(sourceTerms.getAttribute("c_FirstTermsParty")))
				{
					try
					{
						TermsParty sourceParty = (TermsParty) sourceTerms.getComponentHandle("FirstTermsParty");
						String partyName = sourceParty.getAttribute("name");
						if (StringFunction.isNotBlank(partyName))
						{
							LOG.debug("TransactionBean::CopyFromTemplate::name:: " + partyName);
							inputDoc.setAttribute("/Terms/FirstTermsParty/name", partyName);
							inputDoc.setAttribute("/Terms/FirstTermsParty/contact_name", partyName);
						}
					}
					catch (Exception any_e)
					{
						LOG.info("TransactionBean::CopyFromTemplate::Exception setting First Party Data for Payment/DDI");
						LOG.info(any_e.toString());
						LOG.info("TransactionBean::CopyFromTemplate::This is a recoverable issue... proceed to saving Transaction.");
					}
				}
				else
					LOG.debug("TransactionBean::CopyFromTemplate::No PAE Party defined yet..");

			}

            if ( InstrumentType.DOM_PMT.equals(instrument_type) ) {
                String individualDebitInd = sourceTerms.getAttribute("individual_debit_ind");
                inputDoc.setAttribute("/Terms/individual_debit_ind", individualDebitInd );
            }

            if ( InstrumentType.DOM_PMT.equals(instrument_type) ||
            	 InstrumentType.FUNDS_XFER.equals(instrument_type) ||
            	 InstrumentType.XFER_BET_ACCTS.equals(instrument_type) )  {
            	String requestMarketInd = sourceTerms.getAttribute("request_market_rate_ind");
            	inputDoc.setAttribute("/Terms/request_market_rate_ind", requestMarketInd );
            }


			outputDoc = saveTransaction(false, inputDoc, mediatorServices);
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performCopyTemplate");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						getCompleteInstrumentId());
			}

		}
		return outputDoc;
	}

	/**
	 * This method validates Payment Date not to be a past data and generates a
	 * worning in case this is future date.
	 * Transaction status should NOT be updated to READY_TO_AUTHORIZE if date valiadtion
	 * fails with error.
	 *
	 * @param com.ams.tradeportal.busobject.Transaction - Transaction interface
	 * @param String timezone
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */

	//private void validatePaymentDate(Transaction transaction, String timeZone)
	private void validatePaymentDate(Transaction transaction, String timeZone, MediatorServices mediatorServices, String buttonPressed)      //NSX IR# HRUL020839028   Rel. 6.1.0
	throws RemoteException, AmsException
	{
		try
		{
			//obtain payment date
			java.util.Date curDate = transaction.getAttributeDate("payment_date");

			//obtain current date with regards to user's timezone
			java.util.Date todaysDate = null;
			LOG.debug("TransactionMediator::validatePaymentDate::TimeZone: " + timeZone);
			if (StringFunction.isNotBlank(timeZone))
			{
				todaysDate
				= TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(),
						timeZone);
			}
			else
			{
				todaysDate = GMTUtility.getGMTDateTime();
			}

			//compare dates and set status/warning/error accordingly
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy/MM/dd");
			String curDateStr = formatter.format(curDate);
			String todaysDateStr = formatter.format(todaysDate);
			LOG.debug("TransactionMediator::validatePaymentDate:: " + curDateStr + " vs. " + todaysDateStr);

                       // do not show error if Confirming Payment
			if (curDateStr.compareTo(todaysDateStr) < 0 && !buttonPressed.equals(TradePortalConstants.BUTTON_CONFIRM))   //NSX IR# HRUL020839028   Rel. 6.1.0
			{
				transaction.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.DOM_PMT_DATE_CANT_BE_BACKDATED);
				transaction.setAttribute("transaction_status", TransactionStatus.STARTED);
			}
			else if (curDateStr.compareTo(todaysDateStr) > 0)
			{
				transaction.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.DOM_PMT_DATE_IS_FUTURE_DATE);
			}
		}
		catch (Exception exc)
		{
			LOG.debug("TransactionMediator::validatePaymentDate::ExecutionDate is not entered or is in invalid format");
			transaction.getErrorManager().issueError(getClass().getName(),
					//TradePortalConstants.DOM_PMT_DATE_CANT_BE_BACKDATED);
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText("DomesticPaymentRequest.ExecutionDate",
							TradePortalConstants.TEXT_BUNDLE));
			transaction.setAttribute("transaction_status", TransactionStatus.STARTED);

		}
	}

	/**
	 * This method populates the inputDoc Cache Document and sets value from the
	 * template selceted
	 *
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
	 * @param terms com.ams.tradeportal.Terms
	 **/
	private DocumentHandler populateFTRQByTemplate(DocumentHandler inputDoc, Terms sourceTerms)
	throws RemoteException, AmsException {


		inputDoc=populateFTRQParties(inputDoc,sourceTerms,"FirstTermsParty");
		inputDoc=populateFTRQParties(inputDoc,sourceTerms,"ThirdTermsParty");



		inputDoc.setAttribute("/Terms/message_to_beneficiary",sourceTerms.getAttribute("message_to_beneficiary"));
		inputDoc.setAttribute("/Terms/reference_number",sourceTerms.getAttribute("reference_number"));
		inputDoc.setAttribute("/Terms/amount_currency_code",sourceTerms.getAttribute("amount_currency_code"));
		inputDoc.setAttribute("/Terms/amount",sourceTerms.getAttribute("amount"));
		inputDoc.setAttribute("/Terms/ex_rt_variation_adjust",sourceTerms.getAttribute("ex_rt_variation_adjust"));
		inputDoc.setAttribute("/Terms/funds_xfer_settle_type",sourceTerms.getAttribute("funds_xfer_settle_type"));

		inputDoc.setAttribute("/Terms/bank_charges_type",sourceTerms.getAttribute("bank_charges_type"));
		inputDoc.setAttribute("/Terms/finance_type_text",sourceTerms.getAttribute("finance_type_text"));
		inputDoc.setAttribute("/Terms/use_mkt_rate",sourceTerms.getAttribute("use_mkt_rate"));
		inputDoc.setAttribute("/Terms/use_fec",sourceTerms.getAttribute("use_fec"));
		inputDoc.setAttribute("/Terms/covered_by_fec_number", sourceTerms.getAttribute("covered_by_fec_number"));
		inputDoc.setAttribute("/Terms/fec_rate", sourceTerms.getAttribute("fec_rate"));
		inputDoc.setAttribute("/Terms/use_other",sourceTerms.getAttribute("use_other"));
		inputDoc.setAttribute("/Terms/use_other_text",sourceTerms.getAttribute("use_other_text"));
		inputDoc.setAttribute("/Terms/release_by_bank_on",sourceTerms.getAttribute("release_by_bank_on"));//
		inputDoc.setAttribute("/Terms/debit_account_oid",sourceTerms.getAttribute("debit_account_oid"));
		inputDoc.setAttribute("/Terms/payment_charges_account_oid",sourceTerms.getAttribute("payment_charges_account_oid"));
		inputDoc.setAttribute("/Terms/additional_conditions",sourceTerms.getAttribute("additional_conditions"));
		inputDoc.setAttribute("/Terms/special_bank_instructions",sourceTerms.getAttribute("special_bank_instructions"));

		return inputDoc;
	}

	private DocumentHandler populateFTRQParties (DocumentHandler inputDoc,Terms sourceTerms,String partyId)
	throws RemoteException, AmsException{

		//Do not attempt to copy a party that is not registered with source template/tarnsaction
		//Remove the corresponding party from the target instrument
		if(StringFunction.isBlank(sourceTerms.getAttribute("c_"+partyId)))
		{
			inputDoc.setAttribute("/Terms/"+partyId+"/name","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_line_1","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_line_2","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_city","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_state_province","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_country","");
			inputDoc.setAttribute("/Terms/"+partyId+"/address_postal_code","");
			inputDoc.setAttribute("/Terms/"+partyId+"/OTL_customer_id","");
			if (partyId.equals("FirstTermsParty"))
			{
				inputDoc.setAttribute("/Terms/"+partyId+"/entered_account","");
				inputDoc.setAttribute("/Terms/FirstTermsParty/acct_num", "");
			}
			LOG.debug("CFT FRQ removed party " + partyId);
			return inputDoc;
		}

		TermsParty sourceParty = (TermsParty) sourceTerms.getComponentHandle(partyId);

		if (sourceParty!=null){
			inputDoc.setAttribute("/Terms/"+partyId+"/name",sourceParty.getAttribute("name"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_line_1",sourceParty.getAttribute("address_line_1"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_line_2",sourceParty.getAttribute("address_line_2"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_city",sourceParty.getAttribute("address_city"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_state_province",sourceParty.getAttribute("address_state_province"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_country",sourceParty.getAttribute("address_country"));
			inputDoc.setAttribute("/Terms/"+partyId+"/address_postal_code",sourceParty.getAttribute("address_postal_code"));
			inputDoc.setAttribute("/Terms/"+partyId+"/OTL_customer_id",sourceParty.getAttribute("OTL_customer_id"));
			if (partyId.equals("FirstTermsParty"))
			{
				inputDoc.setAttribute("/Terms/"+partyId+"/entered_account",sourceParty.getAttribute("acct_num"));
				inputDoc.setAttribute("/Terms/FirstTermsParty/acct_num", sourceParty.getAttribute("acct_num"));
			}
		}
		return inputDoc;
	}


	// IAZ CR-483B 08/13/09 Begin
	/**
	 * This method calculates Domestic Payment's Equaivalent Amount in Funding Account's CCY from amount in Credit (Transaction) CCY
	 * and performs related save/verify checks
	 *
	 * @return void
	 * @param DocumentHandler
	 * @param com.ams.tradeportal.Instrument instrument
	 * @param com.ams.tradeportal.Transaction transaction
	 * @param com.ams.tradeportal.terms com.ams.tradeportal.Terms
	 * @param String transStatus,
	 * @param String amount
	 * @param com.amsinc.ecsg.frame.MediatorServices mediatorServices
	 **/
	protected void calculateDPEquaivalentPaymentAmount(DocumentHandler inputDoc,Instrument instrument, Transaction transaction, Terms terms, String amount, String transStatus, MediatorServices mediatorServices) throws RemoteException, AmsException
	{

		String fromAcctOID = terms.getAttribute("debit_account_oid");
		String fromCurrency= "";
		String rate = null, ex_currency=null, s_ex_amount = null;             			//IR# PSUL110355116 Rel. 7.1.0 -
		String ex_ind = null;																				//IAZ ER 5.2.1.3 06/29/10 Add
		BigDecimal ex_amount = null;
		String rateType = TradePortalConstants.USE_MID_RATE;
		boolean flag = false; //MDB CR-640 Rel7.1 9/21/11

		if (StringFunction.isNotBlank(fromAcctOID))
		{
			Account fromAccount = (Account) mediatorServices.createServerEJB("Account");
			fromAccount.getData(Long.parseLong(fromAcctOID));
			fromCurrency = fromAccount.getAttribute("currency");
			LOG.debug("AC Data " + fromAcctOID + " " + fromCurrency);
			ex_currency = fromCurrency;               			//IR# PSUL110355116 Rel. 7.1.0 -

			if ((!TradePortalConstants.INDICATOR_YES.equals(instrument.getAttribute("template_flag"))) &&
					(StringFunction.isNotBlank(amount)))
			{
			    //MDB PIUL102167761 Rel7.1 10/21/11 Begin
				String corpOrgOid = fromAccount.getOwnerOfAccount();
				String baseCurrency = fromAccount.getBaseCurrencyOfAccount();
				LOG.debug("validateMaxSpotRateAmtThreshold::corpOrgOid: " + corpOrgOid);
				LOG.debug("validateMaxSpotRateAmtThreshold::baseCurrency: " + baseCurrency);
			    //MDB PIUL102167761 Rel7.1 10/21/11 End

				String transferCurrency = terms.getAttribute("amount_currency_code");
				LOG.debug("cursss " + baseCurrency + " " + transferCurrency);
				//String amount = terms.getAttribute("amount");

				if ((StringFunction.isNotBlank(transferCurrency)) &&
						(StringFunction.isNotBlank(fromCurrency)))
				{

					Vector outMDInd = new Vector(1,1);														//IAZ ER 5.2.1.3 06/29/10 Add
					BigDecimal bigDecRate = null;															//IAZ ER 5.2.1.3 06/29/10 Add

					//use rate from screen as is
					rate = terms.getAttribute("fec_rate");

					int tranAmt = (new Float(amount)).intValue();
					if (tranAmt == 0)
					{
						ex_amount = null;
					}
					else if (transferCurrency.equals(fromCurrency))
					{
						//update with nothing
					}
					else if (transferCurrency.equals(baseCurrency))
					{
						rateType = TradePortalConstants.USE_BUY_RATE;
						if (StringFunction.isBlank(rate))												//IAZ ER 5.2.1.3 06/29/10 Add
						{
							rate=InstrumentServices.getFXRate(fromCurrency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, fromCurrency, terms, mediatorServices, instrument, transaction); //MDB CR-640 Rel7.1 9/21/11

						}
						if (StringFunction.isNotBlank(rate))											//IAZ ER 5.2.1.3 06/29/10 Add
							bigDecRate = new BigDecimal(rate);												//IAZ ER 5.2.1.3 06/29/10 Add
						LOG.debug("ExRate is " + rate);

						ex_amount = instrument.getAmounInFXCurrency(fromCurrency, baseCurrency, amount, corpOrgOid,rateType,
							bigDecRate, outMDInd);															//IAZ ER 5.2.1.3 06/29/10 Add, //MDB PIUL102167761 Rel7.1 10/21/11

						if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))									//IAZ ER 5.2.1.3 06/29/10 Add
							ex_ind = (String)outMDInd.get(0);												//IAZ ER 5.2.1.3 06/29/10 Add
						LOG.debug("ExAmount is " + ex_amount + "ExIndicator " + ex_ind);
					}
					else if (fromCurrency.equals(baseCurrency))
					{
						rateType = TradePortalConstants.USE_SELL_RATE;
						if (StringFunction.isBlank(rate))												//IAZ ER 5.2.1.3 06/29/10 Add
						{
							rate=InstrumentServices.getFXRate(transferCurrency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction); //MDB CR-640 Rel7.1 9/21/11
						}

						if (StringFunction.isNotBlank(rate))											//IAZ ER 5.2.1.3 06/29/10 Add
							bigDecRate = new BigDecimal(rate);												//IAZ ER 5.2.1.3 06/29/10 Add

						ex_amount = instrument.getAmountInBaseCurrency(transferCurrency, amount, baseCurrency, corpOrgOid, rateType,
							bigDecRate, outMDInd);															//IAZ ER 5.2.1.3 06/29/10 Add, //MDB PIUL102167761 Rel7.1 10/21/11

						if (!outMDInd.isEmpty() && (outMDInd.size()>= 0))									//IAZ ER 5.2.1.3 06/29/10 Add
							ex_ind = (String)outMDInd.get(0);												//IAZ ER 5.2.1.3 06/29/10 Add
					}
					else
					{
						if (StringFunction.isNotBlank(rate))
						{
							if ((StringFunction.isNotBlank(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
								((inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY)) ||
								(inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY_FX)))  )
							{
								ex_ind = instrument.crossRateCriterionProcessing(terms, transferCurrency, fromCurrency, rate);

							    int dec = TPCurrencyUtility.getDecimalPrecision(fromCurrency);

								if (TradePortalConstants.MULTIPLY.equals(ex_ind))
									ex_amount = (new BigDecimal(amount)).multiply(new BigDecimal(rate)).setScale(dec, BigDecimal.ROUND_HALF_UP);
								else if (TradePortalConstants.DIVIDE.equals(ex_ind))
									ex_amount = (new BigDecimal(amount)).divide(new BigDecimal(rate), dec, BigDecimal.ROUND_HALF_UP);
							}
						}
						else if((!TradePortalConstants.BUTTON_CONFIRM.equals(inputDoc.getAttribute("/Update/ButtonPressed"))) &&     // NSX IR# PAUL012383540  Rel:6.1.0
								transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE) && !TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/VerifyAysnc")) ) //&&
						{
							rate=InstrumentServices.getFXRate(transferCurrency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction);
							if (instrument.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
							{
								instrument.fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, null, amount, terms);
								flag = true;
							}
						}
						else if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
					 	{
						    if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind"))) {
						    	// if Payment is created from file Upload, set status instead of displaying error
								rate=InstrumentServices.getFXRate(transferCurrency, fromAccount, rateType);
								fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction);
								if (instrument.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
								{
									instrument.fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, null, amount, terms);
									flag = true;
								}
						    }
						    else {
							rate=InstrumentServices.getFXRate(transferCurrency, fromAccount, rateType);
							fxRateProcessing(inputDoc, rate, transferCurrency, terms, mediatorServices, instrument, transaction);
							if (instrument.getErrorManager().getMaxErrorSeverity () < ErrorManager.ERROR_SEVERITY)
							{
								instrument.fxCrossRateProcessing(transferCurrency, fromCurrency, baseCurrency, null, amount, terms);
								flag = true;
							}
						}
					}
					}
				}


				String instrumentType = instrument.getAttribute("instrument_type_code"); //MDB DEUL123060525 Rel7.1 1/4/12
				if ((transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
					&& (InstrumentType.DOM_PMT.equals(instrumentType))) //MDB DEUL123060525 Rel7.1 1/4/12
				{

					User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));

					if ((StringFunction.isBlank(inputDoc.getAttribute("/Terms/fec_rate"))) &&
							(!(transferCurrency.equalsIgnoreCase(fromCurrency))) && TPDateTimeUtility.isFutureDate(inputDoc.getAttribute("/Transaction/payment_date"),user.getAttribute("timezone")) )
					{

						if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind")) &&
								(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/VerifyAysnc")) ||
								TradePortalConstants.BUTTON_CONFIRM.equals(inputDoc.getAttribute("/Update/ButtonPressed")))) {
							// if Payment is created from file Upload, set status instead of displaying error
							transaction.setAttribute("transaction_status", TransactionStatus.VERIFIED_PENDING_FX);
						}
						else {
							mediatorServices.getErrorManager().issueError(getClass().getName(),	TradePortalConstants.DOM_FUTURE_DEB_CRED_DIFF);
						}
					}

				}

			}

		}
		else {

			if (transStatus.equals(TransactionStatus.READY_TO_AUTHORIZE))
			{
				//Edit should not occur for Internatioanl Payment.
				if ( !(InstrumentType.FUNDS_XFER.equals(inputDoc.getAttribute("/Instrument/instrument_type_code"))))
				{
					mediatorServices.getErrorManager ().issueError (
							getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE,
							mediatorServices.getResourceManager().getText("DomesticPaymentRequest.DebitFrom",
									TradePortalConstants.TEXT_BUNDLE));
					transaction.setAttribute("transaction_status", TransactionStatus.STARTED);
				}
			}
		}

		if (!flag) //MDB CR-640 Rel7.1 9/21/11
		{
			if (ex_amount == null)
			{
				rate = null;
				s_ex_amount = null;
				ex_currency=null;                             		                 	                    //IR# PSUL110355116 Rel. 7.1.0 -
				ex_ind = null;																				//IAZ ER 5.2.1.3 06/29/10 Update-End
			}
			else {
				s_ex_amount = ex_amount.toString();
			}
			terms.setAttribute("equivalent_exch_amount", s_ex_amount);
			terms.setAttribute("transfer_fx_rate", rate);
			terms.setAttribute("equivalent_exch_amt_ccy", ex_currency);                 			        //IR# PSUL110355116 Rel. 7.1.0 -
			terms.setAttribute("display_fx_rate_method", ex_ind);											//IAZ ER 5.2.1.3 06/29/10 Add
		}
	}

	// Methods to Handle Ordering Party and Ordering Party Bank of the Payment Transactions

	/**
	 * This method saves/updates Ordering Party/ Ordering Party Bank data to the database, as needed, for the Payment Transactions.
	 * In a case where Ordering Party/ Bank is to be deleted, method returns OID so it can be removed at the end of SaveTransaction
	 * request
	 *
	 * @return String orderingPartyToRemoveOid
	 * @param com.amsinc.ecsg.util.DocumentHandler inputDoc
	 * @param String partyPointer
	 * @param com.amsinc.ecsg.frame.MediatorServices mediatorServices
	 **/

	private String updatePaymentOrderingParty (DocumentHandler inputDoc, String partyPointer, MediatorServices mediatorServices, Transaction transaction)
	throws RemoteException, AmsException
	{

		PaymentParty orderingParty = (PaymentParty) mediatorServices.createServerEJB("PaymentParty");
		String strPPOid = inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/payment_party_oid");
		String bankBranchCode = inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_branch_code");
		String bankName = inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_name");
		LOG.debug("Ordering Bank/Party Oid is " + strPPOid);
		boolean saveParty = false;
		String orderingPartyToRemoveOid = null;

		if (StringFunction.isNotBlank(strPPOid))
		{
			if ((StringFunction.isBlank(bankBranchCode)) &&
					(StringFunction.isBlank(bankName)))
			{
				orderingPartyToRemoveOid = strPPOid;
				LOG.debug("Need to delete Ordering Party/Bank: " + strPPOid);
			}
			else
			{
				orderingParty.getData(Long.parseLong(strPPOid));
				saveParty = true;
			}
		}
		else
		{
			if ((StringFunction.isNotBlank(bankBranchCode)) ||
					(StringFunction.isNotBlank(bankName)))
			{
				orderingParty.newObject();
				saveParty = true;
			}
		}
		if (saveParty)
		{

			orderingParty.setAttribute("bank_name", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_name"));
			orderingParty.setAttribute("bank_branch_code", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/bank_branch_code"));
			orderingParty.setAttribute("address_line_1", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_1"));
			orderingParty.setAttribute("address_line_2", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_2"));
			orderingParty.setAttribute("address_line_3", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_3"));
			orderingParty.setAttribute("address_line_4", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/address_line_4"));
			orderingParty.setAttribute("branch_name", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/branch_name"));
			orderingParty.setAttribute("party_type", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/party_type"));
			orderingParty.setAttribute("country", inputDoc.getAttribute("/DomesticPayment/" + partyPointer + "/country"));

			//Validate SWIFT chars for CBFT
			if (TradePortalConstants.CROSS_BORDER_FIN_TRANSFER.equals(inputDoc.getAttribute("/DomesticPayment/payment_method_type")))
			{

				final String[] attributesToExclude = {"payee_description", "other_charges_instructions", "payee_email"};
				InstrumentServices.checkForInvalidSwiftCharacters(orderingParty.getAttributeHash(),
						transaction.getErrorManager(),
						attributesToExclude,
						mediatorServices.getResourceManager().getText("DomesticPaymentRequest." + partyPointer,
								TradePortalConstants.TEXT_BUNDLE));
			}

			if (transaction.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
			{
				return null;
			}

			if (StringFunction.isNotBlank(orderingParty.getAttribute("payment_party_oid")))
				inputDoc.setAttribute("/Terms/c_"+ partyPointer, orderingParty.getAttribute("payment_party_oid"));

			orderingParty.save(true);
		}
		else
		{
			inputDoc.setAttribute("/Terms/c_"+ partyPointer, "");
		}


		return orderingPartyToRemoveOid;

	}

	/**
	 * This method removed Ordering Party/ Ordering Party Bank data from the Payment Transactions.
	 *
	 * @return void
	 * @param String orderingPartyToRemoveOid
	 * @param com.amsinc.ecsg.frame.MediatorServices mediatorServices
	 **/

	private void removePaymentOrderingPartyOrBank(String orderingPartyToRemoveOid, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		if (orderingPartyToRemoveOid != null)
		{
			PaymentParty orderingPartyToRemove = (PaymentParty) mediatorServices.createServerEJB("PaymentParty");
			orderingPartyToRemove.getData(Long.parseLong(orderingPartyToRemoveOid));
			orderingPartyToRemove.delete();
			LOG.debug("deleted ordering/bank party: " + orderingPartyToRemoveOid);
		}
	}

	/**
	 * This method copies Ordering Party/Bank data form Source to Target.
	 *
	 * @param String sourceOrdeiringPartyOid
	 * @param com.amsinc.ecsg.frame.MediatorServices mediatorServices
	 * @return String newParty.getAttribute("payment_party_oid")
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */

	private String copyOrderingParty(String sourceOrdeiringPartyOid, MediatorServices mediatorServices)
	throws RemoteException, AmsException
	{
		DocumentHandler tmpXML = new DocumentHandler();
		if (StringFunction.isNotBlank(sourceOrdeiringPartyOid))
		{
			PaymentParty originalParty = (PaymentParty) mediatorServices.createServerEJB("PaymentParty");
			originalParty.getData(Long.parseLong(sourceOrdeiringPartyOid));
			LOG.debug("orgOrderingPartyOid " + sourceOrdeiringPartyOid);
			PaymentParty newParty = (PaymentParty) mediatorServices.createServerEJB("PaymentParty");
			newParty.newObject();
			originalParty.populateXmlDoc(tmpXML);
			newParty.populateFromXmlDoc(tmpXML);
			String newOrderingPartyOid = newParty.getAttribute("payment_party_oid");
			newParty.save();
			return newOrderingPartyOid;
		}
		else
		{
			return "";
		}
	}


	//trudden CR-564 08/31/10 Performance improvement for VerifyDomesticPayment
	/**
	 * This method returns the country code for a given account, using either the bank_country_code
	 * stored with the account, or the translated ISO country code from REFDATA.
	 *
	 * @param accountOid primary key to Account
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return String bank branch country if available.
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private String getBankBranchCountryForAccount(String accountOid, MediatorServices mediatorServices)
          throws RemoteException, AmsException {

		String country = null;

		if (StringFunction.isNotBlank(accountOid))
		{
			Account account = instantiateAccount(accountOid, mediatorServices);
			country = account.getBankBranchCountryInISO();

		}

		return country;
     }

	/**
	 * This method instantiates a new Account object for the
	 * accountOid.
	 * @param accountOid primary key to Account
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return String bank branch country if available.
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private Account instantiateAccount(String accountOid, MediatorServices mediatorServices)
          throws RemoteException, AmsException {

			long startCreateAccountBean = System.currentTimeMillis();
			Account account = (Account) mediatorServices.createServerEJB("Account");
						LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] "
										+"\t[PERFORMANCE]"
										+"\tCreateAccountBean"
										+"\t"+(System.currentTimeMillis()-startCreateAccountBean)
										+"\tmilliseconds");

			long startLoadAccountBeanData = System.currentTimeMillis();
			account.getData(Long.parseLong(accountOid));
						LOG.debug("\t[DomesticPaymentBean.validateBankBranchRuleCombination(DocumentHandler,boolean,boolean)] "
										+"\t[PERFORMANCE]"
										+"\tLoadAccountBeanData"
										+"\t"+(System.currentTimeMillis()-startLoadAccountBeanData)
										+"\tmilliseconds");

            return account;

     }
	/**
	 * Utility method to find the active transaction, and perform an itermittent
	 * commit.  after committing, begin a new transaction to continue processing,
	 * and return 0 for the commit counter.
	 *
	 * @param accountOid primary key to Account
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 * @return String bank branch country if available.
	 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
	 */
	private int commitTransaction()
          throws RemoteException, AmsException {
		LOG.debug("entered commit transaction");
        try
        {
            Context jndiContext =
                GenericInitialContextFactory.getInitialContext ();
            UserTransaction trans = (UserTransaction)jndiContext.lookup (
                "javax.transaction.UserTransaction");
            trans.commit();
            trans.begin ();
        }
        catch (NamingException e)
        {
        	LOG.debug("Caught a Naming Exception looking up Initial Context or transaction: " +
                e.toString());
        	throw new AmsException(e.getMessage());
        }
        catch (Exception e)
        {
        	LOG.debug("Caught an Exception lduring commit: " +
                e.toString());
        	throw new AmsException(e.getMessage());
        }

		LOG.debug("\t[TransactionMediatorBean.processMultipleDomesticPayments] performed commit ");

        return 0;

     }


	private DocumentHandler performReject(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

		LOG.debug("TransactionMediatorBean:::performReject");

		DocumentHandler outputDoc = new DocumentHandler();
		Instrument instrument     = null;
		Transaction transaction   = null;
		PaymentFileUpload fileUpload = null;
		long instrumentOid        = 0;
		long transactionOid       = 0;
		long uploadOid   	      = 0;
		String completeInstId     = null;

		try {
			instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
			if (instrumentOid == 0)
			{
				LOG.debug("Error: instrument_oid not present");
				return null;
			}

			// Get a handle to an instance of the instrument, the transaction,
			// and the terms objects.
			instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
			LOG.debug("Mediator: Retrieving instrument " + instrumentOid);
			instrument.getData(instrumentOid);
			completeInstId = instrument.getAttribute("complete_instrument_id");

			// Get the transaction oid from the doc.
			transactionOid = instrument.getAttributeLong("original_transaction_oid");
			if (transactionOid == 0)
			{
				LOG.debug("Error: transaction_oid not present");
				return null;
			}

			// Get the payment upload file oid from the doc.
			uploadOid = inputDoc.getAttributeLong("/PaymentFileUpload/payment_file_upload_oid");
			if (uploadOid == 0)
			{
				LOG.debug("Error: uploadOid not present");
				return null;
			}

			if (StringFunction.isBlank(inputDoc.getAttribute("/LoginUser/user_oid")))
			{
				LOG.debug("Error: userOid not present");
				return null;
			}

			fileUpload = (PaymentFileUpload) mediatorServices.createServerEJB("PaymentFileUpload");
		    fileUpload.getData(uploadOid);

			LOG.debug("Mediator: Getting handle to transaction " + transactionOid);
			transaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);

			TransactionHistory tranHistory = (TransactionHistory) mediatorServices.createServerEJB("TransactionHistory");
			tranHistory.newObject();
			tranHistory.setAttribute("action_datetime", DateTimeUtility.getGMTDateTime());
			tranHistory.setAttribute("action", TradePortalConstants.TRAN_ACTION_REJECT);
			tranHistory.setAttribute("transaction_status", TransactionStatus.FILE_UPLOAD_REJECT);
			tranHistory.setAttribute("transaction_oid", transaction.getAttribute("transaction_oid"));
			tranHistory.setAttribute("user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));

			int ret = tranHistory.save(false);
			if (ret != 1)
				LOG.info("Error occured while creating Transaction History Log...");

			instrument.setAttribute("instrument_status", TradePortalConstants.INSTRUMENT_STATUS_FILE_UPLOAD_REJECTED);
			transaction.setAttribute("transaction_status", TransactionStatus.FILE_UPLOAD_REJECT);
			transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
			transaction.setAttribute("last_entry_user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));
			instrument.save(false);

			fileUpload.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_FILE_REJECTED);
			fileUpload.setAttribute("user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));
			fileUpload.save();


		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performReject");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						completeInstId);
			}

		}
		return outputDoc;
	}

	private DocumentHandler performConfirm(DocumentHandler inputDoc, MediatorServices mediatorServices)
		throws RemoteException, AmsException {

		LOG.debug("TransactionMediatorBean:::performConfirm");

		DocumentHandler outputDoc = new DocumentHandler();
		DocumentHandler verifyInputDoc = new DocumentHandler();
		Instrument instrument     = null;
		Transaction transaction   = null;
		PaymentFileUpload fileUpload = null;
		long instrumentOid        = 0;
		long transactionOid       = 0;
		long uploadOid   	      = 0;
		String completeInstId     = null;

		try {
			instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
			if (instrumentOid == 0)
			{
				LOG.debug("Error: instrument_oid not present");
				return null;
			}

			// Get a handle to an instance of the instrument, the transaction,
			// and the terms objects.
			instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
			LOG.debug("Mediator: Retrieving instrument " + instrumentOid);
			instrument.getData(instrumentOid);
			completeInstId = instrument.getAttribute("complete_instrument_id");

			// Get the transaction oid from the doc.
			transactionOid = instrument.getAttributeLong("original_transaction_oid");
			if (transactionOid == 0)
			{
				LOG.debug("Error: transaction_oid not present");
				return null;
			}

			// Get the payment upload file oid from the doc.
			uploadOid = inputDoc.getAttributeLong("/PaymentFileUpload/payment_file_upload_oid");
			if (uploadOid == 0)
			{
				LOG.debug("Error: uploadOid not present");
				return null;
			}

			if (StringFunction.isBlank(inputDoc.getAttribute("/LoginUser/user_oid")))
			{
				LOG.debug("Error: userOid not present");
				return null;
			}

			fileUpload = (PaymentFileUpload) mediatorServices.createServerEJB("PaymentFileUpload");
		    fileUpload.getData(uploadOid);
		    
		  //check if any errors still exist for the payment_file_upload.
	        //If exists, do not update status
	        String whereSqlStr = " p_payment_file_upload_oid = ? and reporting_code_error_ind=?";
	        List<Object> temp = new ArrayList<Object>();
			temp.add(uploadOid);
			temp.add(TradePortalConstants.INDICATOR_YES);
			boolean hasReportingCodeErrors = false;
			//obtain count.
			int errCount = DatabaseQueryBean.getCount("payment_upload_error_log_oid", "PAYMENT_UPLOAD_ERROR_LOG", whereSqlStr, false, temp);
			if(errCount>0){
				hasReportingCodeErrors = true;
			}
			LOG.debug("Mediator: Getting handle to transaction " + transactionOid);
			transaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
			Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");     //// NSX IR# PAUL012383540  Rel:6.1.0

			mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_CONFIRM);

			populateInputXmlDoc(verifyInputDoc, "/Transaction/",       transaction);
			populateInputXmlDoc(verifyInputDoc, "/Terms/", terms);       // NSX IR# PAUL012383540  Rel:6.1.0
			verifyInputDoc.setAttribute("/Terms/terms_oid",                 transaction.getAttribute("c_CustomerEnteredTerms"));
			verifyInputDoc.setAttribute("/Instrument/instrument_oid",       instrument.getAttribute("instrument_oid"));
			verifyInputDoc.setAttribute("/Instrument/instrument_type_code", instrument.getAttribute("instrument_type_code"));
			verifyInputDoc.setAttribute("/LoginUser/user_oid",              inputDoc.getAttribute("/LoginUser/user_oid"));
			verifyInputDoc.setAttribute("/Update/ButtonPressed",            inputDoc.getAttribute("/Update/ButtonPressed"));//MDB Rel6.1 IR#PUL011064558 1/12/11
			// CR 857 - include tag to not process panel again in reVerify method
			if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("bene_panel_auth_ind"))){ //CR 857
				verifyInputDoc.setAttribute("/ProcessBenePanelProcess", TradePortalConstants.INDICATOR_NO);
			}
			
			if(TransactionStatus.REPORTING_CODES_REQUIRED.equals(transaction.getAttribute("transaction_status"))){
				if(hasReportingCodeErrors){
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.FILE_REPORTING_ERRORS_MUST_BE_CONFIRMED);
					return outputDoc; //Do nothing
				}else{
					outputDoc = saveTransaction(TransactionStatus.READY_TO_AUTHORIZE, true, verifyInputDoc, mediatorServices);
					fileUpload.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_FILE_REPAIRED);
				}
			}else{
				if(hasReportingCodeErrors){
					outputDoc = saveTransaction(TransactionStatus.REPORTING_CODES_REQUIRED, true, verifyInputDoc, mediatorServices);
					fileUpload.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_VALIDATED_AWAITING_REPAIR);
					outputDoc.setAttribute("/forwardToLogDetail", TradePortalConstants.INDICATOR_YES); 
				}else{
					outputDoc = saveTransaction(TransactionStatus.READY_TO_AUTHORIZE, true, verifyInputDoc, mediatorServices);
					fileUpload.setAttribute("validation_status", TradePortalConstants.PYMT_UPLOAD_FILE_CONFIRMED);
				}
			
			}
			
			fileUpload.setAttribute("user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));
			fileUpload.save();
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performConfirm");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} finally {

			LOG.debug("Mediator: Result of update is "
					+ this.isTransSuccess(mediatorServices.getErrorManager()));

			if (this.isTransSuccess(mediatorServices.getErrorManager())) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.UPDATE_SUCCESSFUL,
						completeInstId);
			}

		}
		return outputDoc;
	}

	/**
	 * This method takes a document and populates this business object's
	 * attributes from the values in the document starting at the specified
	 * component path.
	 *
	 * @author  dmb
	 * @param doc The source DocumentHandler object that will be used to
	 * populated the object's attributes.
	 * @param componentPath The component path to go to to perform the
	 * business object population. See the method documentation for a note on
	 * how the format of this variable is relevant.
	 * @param tagTranslator XMLTagTranslater - an adaptor that translates the XML node tag name
	 * to Object attribute/component name, and tag value to attribute valu.
	 * If the attribute name is null, this node is ignored.
	 */
	public void populateInputXmlDoc (DocumentHandler doc, String componentPath, BusinessObject bean)
	throws RemoteException, AmsException
	{
		Hashtable attrList = bean.getAttributeHash();
		Iterator i = attrList.keySet().iterator();
		while (i.hasNext()) {
			String attributeComponentName = (String)i.next();
			if (StringFunction.isNotBlank(bean.getAttribute(attributeComponentName))) {
				doc.setAttribute (componentPath + attributeComponentName, bean.getAttribute(attributeComponentName));
			}
		}
	}



	/**
	 * Processing for Transaction involving FX rates and one currency different than base
	 *
	 * @param DocumentHandler - access to button pressed
	 * @param String - rate to be used
	 * @param String - currency to be used
	 * @param Terms - terms data
	 * @param Instrument - instrument data
	 * @param Transaction - transaction data
	 * @param mediatorServices com.amsinc.ecsg.frame.MediatorServices
	 */
	private void fxRateProcessing(DocumentHandler inputDoc, String rate, String currency, Terms terms, MediatorServices mediatorServices,
								  Instrument instrument, Transaction transaction) throws RemoteException, AmsException
	{
		if ((StringFunction.isNotBlank(inputDoc.getAttribute("/Update/ButtonPressed"))) &&
			((inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY)) ||
			(inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_CONFIRM)) || //MDB PRUL112347061 Rel7.1 12/15/11
			(inputDoc.getAttribute("/Update/ButtonPressed").equals(TradePortalConstants.BUTTON_VERIFY_FX)))  )
		{
			String instrumentType = instrument.getAttribute("instrument_type_code");
			if ((StringFunction.isNotBlank(instrumentType)) &&
			    ((instrumentType.equals(InstrumentType.DOM_PMT)) && (!TPDateTimeUtility.isFutureDate(transaction.getAttribute("payment_date")))) ||
			 	(!instrumentType.equals(InstrumentType.DOM_PMT)))
			{
				if (StringFunction.isNotBlank(rate))
				{
					if(instrument.checkFXOnlineIndicator(terms))
					{
						if (instrument.validateMaxDealamount(inputDoc, currency, terms, transaction)) //MDB PRUL112347061 Rel7.1 12/15/11
						{
							if (!(instrument.requestMarketRateIndicator(terms, transaction)))
								instrument.validateMaxSpotRateAmtThreshold(inputDoc, currency, terms, transaction); //MDB PRUL112347061 Rel7.1 12/15/11
						}//request market rate check
					}
					else
						instrument.validateMaxSpotRateAmtThreshold(inputDoc, currency, terms, transaction); //MDB PRUL112347061 Rel7.1 12/15/11
				}
			}
		}
	}

   /**
    * This  method calculate the inerest and discount calculation for given instrument type.
    * after calculation calculated value is set in inputDoc and then it is persist in terms.
    *
    * @return inputDoc
    *
    * @param inputDoc
    * @param mediatorServices
    * @param instrType
    *
    * @throws RemoteException, AmsException
    */

	private DocumentHandler calculateInterestDiscount(DocumentHandler inputDoc, MediatorServices mediatorServices, String instrType) throws RemoteException, AmsException{

		CorporateOrganization corp_org = null;
		String amount   = inputDoc.getAttribute("/Terms/amount");
		String currency = inputDoc.getAttribute("/Terms/amount_currency_code");
		int allowedDaystoInvoice = 0;
		String intDiscRateGroup ="";

		int numTenorDays = calculateNumTenorDays(inputDoc, mediatorServices);

		String owner_oid = inputDoc.getAttribute("/LoginUser/owner_org_oid");
		String interestDiscountInd =null;
		String calculateDiscountInd = null;
			if(StringFunction.isNotBlank(owner_oid)){
				corp_org = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(owner_oid));

				String allowedDaystoInvoiceStr = corp_org.getAttribute("fin_days_before_loan_maturity");
				if(StringFunction.isNotBlank(allowedDaystoInvoiceStr)){
				    allowedDaystoInvoice = Integer.parseInt(allowedDaystoInvoiceStr);
				}


		        intDiscRateGroup = corp_org.getAttribute("interest_disc_rate_group");
				if (intDiscRateGroup == null) {
						intDiscRateGroup = "";
				}

				// If the user selects 'Interest to be Paid', In Arrears then

				interestDiscountInd = inputDoc.getAttribute("/Terms/interest_to_be_paid");
				if(TradePortalConstants.IN_ADVANCE.equals(interestDiscountInd)){
					calculateDiscountInd = corp_org.getAttribute("calculate_discount_ind");
					if(StringFunction.isBlank(calculateDiscountInd)){
					    //cquinton 4/17/2013 Rel 8.2 ir#15565 use correct indicator constant for straight discount
						calculateDiscountInd = TradePortalConstants.STRAIGHT_DISCOUNT;
					}

				}

			}
			//IR 21174 start- If number of tenor days is greater than'the number of days' designated in the setting at corporate org level,
			// then the system will end processing and return error

		if(allowedDaystoInvoice < numTenorDays){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.LRQ_INV_TENOR_DAYS_GREATER);
			return inputDoc;
		}


		BigDecimal invoiceAmount = StringFunction.isBlank(amount) ? BigDecimal.ZERO :
			new BigDecimal(amount, MathContext.DECIMAL64);

		// commented below line as finace amount is alreay calculated and present in input doc
		//BigDecimal financeAmount = invoiceAmount.multiply(percentFinanceAllowed);
		BigDecimal financeAmount = invoiceAmount;

		DocumentHandler rateTypeAndMargin = InvoiceUtility.getMarginAndRateType(instrType, currency, amount, owner_oid);

		BigDecimal margin = BigDecimal.ZERO;
		String rateType = "";

		if (rateTypeAndMargin != null) {
			rateType = rateTypeAndMargin.getAttribute("/RATE_TYPE");
			String marginStr = rateTypeAndMargin.getAttribute("/MARGIN");
			if (StringFunction.isNotBlank(marginStr)) {
				margin = new BigDecimal(marginStr, MathContext.DECIMAL64);
			}
		}
		else {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.NO_MARGIN);
		}

       StringBuilder sqlQuery = new StringBuilder("SELECT CASE WHEN ");
	   		sqlQuery.append(numTenorDays).append(" <= 30 THEN rate_30_days WHEN ");
	   		sqlQuery.append(numTenorDays).append(" > 30 AND ");
	   		sqlQuery.append(numTenorDays).append(" <= 60 THEN rate_60_days WHEN ");
	   		sqlQuery.append(numTenorDays).append(" > 60 AND ");
	   		sqlQuery.append(numTenorDays).append(" <= 90 THEN rate_90_days WHEN ");
	   		sqlQuery.append(numTenorDays).append(" > 90 AND ");
	   		sqlQuery.append(numTenorDays).append(" <= 120 THEN rate_120_days WHEN ");
	   		sqlQuery.append(numTenorDays).append(" > 120 AND ");
	   		sqlQuery.append(numTenorDays).append(" <= 180 THEN rate_180_days WHEN ");
	   		sqlQuery.append(numTenorDays).append(" > 180 THEN rate_360_days END rate,");
	   		sqlQuery.append(" num_interest_days");
	   		sqlQuery.append(" FROM interest_discount_rate a, interest_discount_rate_group b");
	   		sqlQuery.append(" WHERE a.p_interest_disc_rate_group_oid = b.interest_disc_rate_group_oid");
	   		sqlQuery.append(" AND currency_code = ?");
	   		sqlQuery.append(" AND rate_type = ?");
	   		sqlQuery.append(" AND group_id = ?");
	   		sqlQuery.append(" AND effective_date  = (");
	   		sqlQuery.append(" SELECT max(effective_date)");
	   		sqlQuery.append(" FROM interest_discount_rate a2, interest_discount_rate_group b2");
	   		sqlQuery.append(" WHERE a2.p_interest_disc_rate_group_oid = b2.interest_disc_rate_group_oid");
	   		sqlQuery.append(" AND currency_code = ?");
	   		sqlQuery.append(" AND rate_type = ?");
	   		sqlQuery.append(" AND group_id = ?)");
	   		
	   		Object sqlParams[] = new Object[6];
	   		sqlParams[0] = currency;
	   		sqlParams[1] = rateType;
	   		sqlParams[2] = intDiscRateGroup;
	   		sqlParams[3] = currency;
	   		sqlParams[4] = rateType;
	   		sqlParams[5] = intDiscRateGroup;

	   		String rateStr = null;
	   		String numInterestDaysStr = null;

	   		LOG.debug("TransactionMediatorBean::calculateInterestDiscount::Running the following query: "
	   				+ sqlQuery.toString());
	   		DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), true, sqlParams);
	   		if (resultsDoc != null) {
	   			rateStr = resultsDoc.getAttribute("/ResultSetRecord/RATE");
	   			numInterestDaysStr = resultsDoc.getAttribute("/ResultSetRecord/NUM_INTEREST_DAYS");
	   		}

	   		if (StringFunction.isBlank(rateStr)) {
	   			rateStr = "0.0"; // Default if value not found in DB
	   		}

	   		if (StringFunction.isBlank(numInterestDaysStr)) {
	   			numInterestDaysStr = "360"; // Default if value not found in DB
	   		}

	   		BigDecimal rate = new BigDecimal(rateStr, MathContext.DECIMAL64);
	   		rate = rate.add(margin).divide(new BigDecimal("100.0"));

	   		BigDecimal numTenorDaysDecimal = new BigDecimal(numTenorDays);
		    BigDecimal netFinanceAmount = financeAmount;


		   BigDecimal numInterestDays = new BigDecimal(numInterestDaysStr);
		   BigDecimal rateByDays = rate.multiply(numTenorDaysDecimal).divide(
		   				numInterestDays, MathContext.DECIMAL64);

		   		if (TradePortalConstants.IN_ARREARS.equals(interestDiscountInd)) {

		   			BigDecimal interestAmount = financeAmount.multiply(rateByDays);

		   			inputDoc.setAttribute("/Terms/estimated_interest_amount", interestAmount.toString());
		   		}
		   		else if (TradePortalConstants.IN_ADVANCE.equals(interestDiscountInd)) {
		   			BigDecimal discountAmount = BigDecimal.ZERO;

		   			//cquinton 4/17/2013 Rel 8.2 ir#15565 use the correct indicator constants
		   			if (TradePortalConstants.STRAIGHT_DISCOUNT.equals(calculateDiscountInd)) {
		   				discountAmount = financeAmount.multiply(rateByDays);
		   			}
		   			else if (TradePortalConstants.DISCOUNT_YIELD.equals(calculateDiscountInd)) {
		   				discountAmount =
		   					financeAmount.multiply(BigDecimal.ONE.subtract(BigDecimal.ONE.divide(
		   							BigDecimal.ONE.add(rateByDays), MathContext.DECIMAL64)));
		   			}

		   			netFinanceAmount = financeAmount.subtract(discountAmount);
		   		}

		   		inputDoc.setAttribute("/Terms/net_finance_amount", netFinanceAmount.toString());


		return inputDoc;
	}

	/**
	 * This method calculate the number of tenor days using Loan Start Date and Laon Terms maturity Date.
	 * @return number of tenor days
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws AMSException
	 *
	 */
    private int calculateNumTenorDays(DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException{
    	int tenorDays=0;
    	if(StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/loan_terms_number_of_days"))){
    		tenorDays = Integer.parseInt(inputDoc.getAttribute("/Terms/loan_terms_number_of_days"));
    	}else{
    		Date loanStartDate    = DateTimeUtility.convertStringDateToDate(inputDoc.getAttribute("/Terms/loan_start_date"));
    		Date loanMatFixedDate = DateTimeUtility.convertStringDateToDate(inputDoc.getAttribute("/Terms/loan_terms_fixed_maturity_dt"));
    		tenorDays = InvoiceUtility.calculateDaysDiff(loanMatFixedDate, loanStartDate);
    	}

    	return tenorDays;
    }



    /**
     * this method is used to send transaction for authorization after ready to check status.
     * user ,who has right to check data, check the transaction data and send for authorization.
     * transaction status  set as 'Ready to Authorised' after 'Ready to Check'.
     * @return DocumentHandler
     * @param inputDoc xml doc has transaction data
     * @param MediatorService
     * @throws RemoteException, AmsException
     */
	private DocumentHandler performSendForAuth(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("TransactionMediatorBean:::performSendForAuth");

		DocumentHandler outputDoc = new DocumentHandler();
        int saveSuccess;

        String oldStatus = inputDoc.getAttribute("/Transaction/transaction_status");

        // 'Send for Authorization' action is valid only when transaction status is 'Ready To Check'
        if(!TransactionStatus.READY_TO_CHECK.equals(oldStatus)){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NOT_VALID_STATUS_FOR_ACTION,
					oldStatus, mediatorServices.getResourceManager().getText("common.SendForAuth", TradePortalConstants.TEXT_BUNDLE));
			return outputDoc;
        }
        inputDoc.setAttribute("/Transaction/last_entry_user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));
		// set transaction action
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_SEND_FOR_AUTH);
		// set transaction status to Ready to Authorise
		//MEerupula Rel 8.3 IR-18343 passing inputDoc as parameter instead of just transactionOid
		saveSuccess = setTransactionStatusforCheckFlow(mediatorServices, inputDoc,
				                                       TransactionStatus.READY_TO_AUTHORIZE);
		if(saveSuccess > -1){
			// display success message
        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SEND_FOR_AUTHORIZATION,
        			getInstrumentID(inputDoc.getAttribute("/Instrument/instrument_oid"),mediatorServices));

		}else{
			LOG.debug("Error occured while saving transaction status as SEND FOR AUTH");
		}
		return outputDoc;
	}

    /**
     * this method is used to send transaction for Repair after ready to check status.
     * user ,who has right to check data, check the transaction data and find incorrect and send for repair.
     * transaction status  set as 'Repair' after 'Ready to Check'.
     * @return DocumentHandler
     * @param inputDoc xml doc has transaction data
     * @param MediatorService
     * @throws RemoteException, AmsException
     */
	private DocumentHandler performSendForRepair(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("TransactionMediatorBean:::performSendForRepair");
		DocumentHandler outputDoc = new DocumentHandler();
        int saveSuccess;
		String repairReason = inputDoc.getAttribute("/Transaction/repairReason");

		if(StringFunction.isBlank(repairReason)){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PERAIR_REASON_REQ);
			return outputDoc;
		}
        if(repairReason.length()>1000){
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MAX_LENGTH_EXCEDDED,
					mediatorServices.getResourceManager().getText("CheckFlowTransaction.RepairReason", TradePortalConstants.TEXT_BUNDLE),
					"1000");
			return outputDoc;
		}
        // 'Send for Repair' action is valid only when transaction status 'Ready to Authorized', 'Authrized Failed', 'Partialy Authorized' or
		// 'Ready to check'
		String oldStatus = inputDoc.getAttribute("/Transaction/transaction_status");
        if(!(TransactionStatus.READY_TO_CHECK.equals(oldStatus) || TransactionStatus.AUTHORIZE_FAILED.equals(oldStatus) ||
        		TransactionStatus.PARTIALLY_AUTHORIZED.equals(oldStatus) || TransactionStatus.READY_TO_AUTHORIZE.equals(oldStatus))) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.NOT_VALID_STATUS_FOR_ACTION,
					oldStatus, mediatorServices.getResourceManager().getText("common.SendForRepair", TradePortalConstants.TEXT_BUNDLE));
			return outputDoc;
        }

		// set transaction action
		mediatorServices.getCSDB().setCSDBValue(TradePortalConstants.TRAN_ACTION, TradePortalConstants.TRAN_ACTION_REPAIR);
		inputDoc.setAttribute("/Transaction/last_entry_user_oid", inputDoc.getAttribute("/LoginUser/user_oid"));
		// Set repair reason. this will be used while logging action in transaction history table
		mediatorServices.getCSDB().setCSDBValue("RepairReason", repairReason);


		// set transaction status to Repair
		//MEerupula Rel 8.3 IR-18343 passing inputDoc as parameter instead of just transactionOid
		saveSuccess = setTransactionStatusforCheckFlow(mediatorServices, inputDoc, TransactionStatus.REPAIR);
        if (saveSuccess > -1){

        	// display success message

        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SEND_FOR_REPAIR,
        			getInstrumentID(inputDoc.getAttribute("/Instrument/instrument_oid"),mediatorServices));


		    // create email for all selected users to notification
		    Vector<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragments("/EmailList/user");
		    StringBuilder emailReceivers = new StringBuilder();
		    String userOid = null;
		    String ownerOrgOid = null;

		    for(DocumentHandler doc : beneficiaryDetailsList){
			    userOid = doc.getAttribute("/userOid");
				User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
				String userMailID = user.getAttribute("email_addr");
				ownerOrgOid = user.getAttribute("owner_org_oid");
				if(StringFunction.isNotBlank(userMailID)){
					emailReceivers.append(userMailID + ",");
				}
		    }

		    if(StringFunction.isNotBlank(emailReceivers.toString())){
		      createEmailMessage(inputDoc, mediatorServices, emailReceivers.toString(), ownerOrgOid);
		    }
        } else{
			LOG.debug("Error occured while saving transaction status as REPAIR");
		}

		return outputDoc;

	}

	/**
	 *
	 * @param instrument processing insturment
	 * @param mediatorServices
	 * @return true if checker work flow is enabled for product
	 *        false if checker work flow is disabled for product.
	 * @throws AmsException
	 * @throws RemoteException
	 */
	private boolean isCheckerWorkflowEnabled (Instrument instrument,  Transaction transaction, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

	    //get instrument type
	    String instrument_type = instrument.getAttribute("instrument_type_code");

	    // Checker flow is not applicable for instutrument 'Direct debit instruction'.
	    if(InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(instrument_type)){
	    	return false;
	    }

	    String transaction_type = transaction.getAttribute("transaction_type_code");
	    if(TransactionType.DISCREPANCY.equals(transaction_type) || TransactionType.APPROVAL_TO_PAY_RESPONSE.equals(transaction_type))
	    {
	    	instrument_type = TransactionType.DISCREPANCY;// both ATP response and DISCREPANCY response use same column value i.e dcr_checker_auth_ind
	    } else if ( TransactionType.SIR.equals(transaction_type) || TransactionType.SIM.equals(transaction_type) ) {
	    	instrument_type = TradePortalConstants.SETTLEMENT;
	    }

	    //Retrieve Corporate Org
	    CorporateOrganization corp_org = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
		                                                                                          Long.parseLong(instrument.getAttribute("corp_org_oid")));
	    //check if checker workflow is enabled for the instrument type
	    String val = corp_org.getAttribute(instrument_type.toLowerCase() + "_checker_auth_ind");
	    return TradePortalConstants.INDICATOR_YES.equals(val);
	}

	/**
	   * This method creates an email to all selected user to send notification
	   * informing reapir reason required on transaction.
	   *
	   * @param inputDoc document containing info related to repair reason
	   * @param mediatorServices
	   * @throws RemoteException, AmsException
	   */
	private void createEmailMessage( DocumentHandler inputDoc, MediatorServices mediatorServices, String emailReceivers, String ownerOrgOid)
		throws RemoteException, AmsException {
		String transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");
		 String instrumentOid = inputDoc.getAttribute("/Instrument/instrument_oid");
		 String repairReason = inputDoc.getAttribute("/Transaction/repairReason");

		//Retrieve Corporate Org
	    CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",
		                                                                                          Long.parseLong(ownerOrgOid));
		String bankOrgOid = corpOrg.getAttribute("bank_org_group_oid");

		// Get the sender name, sender email address, system name, and bank url from the corp org's bank org group
		BankOrganizationGroup bankOrg = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup",
				                                                                     Long.parseLong(bankOrgOid));

		 String senderName = bankOrg.getAttribute("repair_sender_name").trim();
		 String senderEmailAddress = bankOrg.getAttribute("repair_sender_email_address");
		 String tradeSystemName = bankOrg.getAttribute("repair_system_name").trim();
		 String doNotReplyInd = bankOrg.getAttribute("repair_do_not_reply_ind");
		 String bankUrl = bankOrg.getAttribute("repair_bank_url");
		 String incldBnkUrlInd = bankOrg.getAttribute("incld_bnk_url_repr_panel");
		 
		 
		// Get the data from instrument that will be used to build the email message
			String instrumentId = "";
			String referenceNumber = "";

			if (StringFunction.isNotBlank(instrumentOid)) {
				Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
				instrumentId = instrument.getAttribute("complete_instrument_id");
				referenceNumber = instrument.getAttribute("copy_of_ref_num");
			}

			// Get the data from transaction that will be used to build the email message
			String transactionAmount = "";
			String transactioncurrency = "";
			String transactionType = "";
			String transactionTypeDesc = "";
			if (StringFunction.isNotBlank(transactionOid)) {
				Transaction transaction =(Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
				transactioncurrency = transaction.getAttribute("copy_of_currency_code");
				transactionAmount = TPCurrencyUtility.getDisplayAmount(transaction.getAttribute("copy_of_amount"), transactioncurrency, mediatorServices.getResourceManager().getResourceLocale());
				transactionType = transaction.getAttribute("transaction_type_code");
				transactionTypeDesc =ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",transactionType,
						mediatorServices.getResourceManager().getResourceLocale());
			}

			// Build the subject line and content of the email based on the type of email (transaction, mail message)
			StringBuilder emailSubject = new StringBuilder();
			StringBuilder emailContent = new StringBuilder();
			StringBuilder emailFooter  = new StringBuilder();

			
		     emailSubject.append(tradeSystemName+" " + StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.SentForRepair", TradePortalConstants.TEXT_BUNDLE)));

			 emailContent.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.TransactionSentBack", TradePortalConstants.TEXT_BUNDLE)));
			 emailContent.append(TradePortalConstants.NEW_LINE);


			 if (StringFunction.isNotBlank(instrumentId)) {
				 emailContent.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.InstrumentId", TradePortalConstants.TEXT_BUNDLE))+" ");
				 emailContent.append(instrumentId);
				 emailContent.append(TradePortalConstants.NEW_LINE);
			 }

			 if (StringFunction.isNotBlank(referenceNumber)) {
				 emailContent.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.CopyOfRefNum", TradePortalConstants.TEXT_BUNDLE))+" ");
				 emailContent.append(referenceNumber);
				 emailContent.append(TradePortalConstants.NEW_LINE);
			 }
			 if (StringFunction.isNotBlank(transactionTypeDesc)) {
				 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.TransactionType", TradePortalConstants.TEXT_BUNDLE)+" ");
				 emailContent.append(transactionTypeDesc);
				 emailContent.append(TradePortalConstants.NEW_LINE);
			 }

			// Include Transaction Amount and currency
			if (StringFunction.isNotBlank(transactionAmount)) {
				emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
				emailContent.append(transactioncurrency+" "+transactionAmount);
				emailContent.append(TradePortalConstants.NEW_LINE);
			}
			emailContent.append(TradePortalConstants.NEW_LINE);

			// Include repair reason fot transaction which is sent back to recheck
	        if (StringFunction.isNotBlank(repairReason)){
	        	    emailContent.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.RepairReason", TradePortalConstants.TEXT_BUNDLE)) + " ");
				    emailContent.append(repairReason);
				    emailContent.append(TradePortalConstants.NEW_LINE);
	         }
	        emailContent.append(TradePortalConstants.NEW_LINE);

			// Build the email's footer
			emailFooter.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.LogIn", TradePortalConstants.TEXT_BUNDLE))+ " ");

			// Use default trade system name if bank org group has not specified one
			if (StringFunction.isBlank(tradeSystemName)) {
				tradeSystemName = mediatorServices.getResourceManager().getText("EmailTrigger.PaymentGroup", TradePortalConstants.TEXT_BUNDLE);
			}
			//IR-T36000019273 Rel8.3 Added period at the end of trade system name
			emailFooter.append(" " + tradeSystemName + " ");

	        emailFooter.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.RepairMessage", TradePortalConstants.TEXT_BUNDLE)));
	        
	        /*PGedupudi CR 1123 Rel 9.5 start*/
		if (TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlInd)) {
				emailFooter.append(StringFunction.asciiToUnicode(
						mediatorServices.getResourceManager().getText(
							"EmailTrigger.LogIn",
							TradePortalConstants.TEXT_BUNDLE))
							+ " ");

					
					emailFooter.append(tradeSystemName + " ");
					emailFooter.append(
							mediatorServices.getResourceManager().getText(
							"EmailTrigger.AdditionalDetails",
							TradePortalConstants.TEXT_BUNDLE)
							+ " ");
					
					
				        if (!StringFunction.isBlank(bankUrl))
			                {
					  emailFooter.append(StringFunction.asciiToUnicode(
							  mediatorServices.getResourceManager().getText(
							"EmailTrigger.WebsiteAccess",
							TradePortalConstants.TEXT_BUNDLE))
							+ " ");
					  emailFooter.append(bankUrl + ".");
			                }
					
			}else{
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.ForAdditionalDetails",TradePortalConstants.TEXT_BUNDLE)+ ", ")
				.append(mediatorServices.getResourceManager().getText("EmailTrigger.PleaseLogIn",TradePortalConstants.TEXT_BUNDLE)+ " "+tradeSystemName+ " ")
				.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.ByVisitingBankSite",TradePortalConstants.TEXT_BUNDLE)));
			}
			/*PGedupudi CR 1123 Rel 9.5 end*/

			if (TradePortalConstants.INDICATOR_YES.equals(doNotReplyInd)) {
	              emailFooter.append(TradePortalConstants.NEW_LINE + TradePortalConstants.NEW_LINE);

	              emailFooter.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.DoNotReply", TradePortalConstants.TEXT_BUNDLE)));
	        }
			
		
	        // Add the footer to the email content
	        emailContent.append(emailFooter);

	     // Create the format and content of the email
			DocumentHandler emailMessage = new DocumentHandler("<Email></Email>", false, true);
			emailMessage.setAttribute("/To", emailReceivers);
			emailMessage.setAttribute("/Cc", "");
			emailMessage.setAttribute("/Bcc", "");
			emailMessage.setAttribute("/From/SenderAddress", senderEmailAddress);
			emailMessage.setAttribute("/From/SenderName", senderName);
			emailMessage.setAttribute("/Subject", emailSubject.toString());
			emailMessage.setAttribute("/Content", emailContent.toString());

			// Create a new email trigger object containing the email content created above
			EmailTrigger emailTrigger = (EmailTrigger) mediatorServices.createServerEJB("EmailTrigger");
			emailTrigger.newObject();
			emailTrigger.setAttribute("agent_id", "");
			emailTrigger.setAttribute("status",TradePortalConstants.EMAIL_TRIGGER_NEW);
			emailTrigger.setAttribute("email_data", emailMessage.toString());
			emailTrigger.save();
	}
	//MEerupula Rel 8.3 IR-18343 changed method signature, passed inputDoc as parameter to get all the required parameters like user_oid, transaction_oid from it.
	private int setTransactionStatusforCheckFlow(MediatorServices mediatorServices, DocumentHandler inputDoc, String transactionStatus)
	throws NumberFormatException, RemoteException, AmsException{

        int saveSuccess;
        String transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");
        String userOid = inputDoc.getAttribute("/Transaction/last_entry_user_oid");
		Transaction transaction =(Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
		transaction.setAttribute("transaction_status", transactionStatus);
		transaction.setAttribute("last_entry_user_oid", userOid);
		transaction.setAttribute("transaction_status_date", DateTimeUtility.getGMTDateTime());
		saveSuccess = transaction.save(false);

		return saveSuccess;
	}


	
	private DocumentHandler performNotifyPanelAuthUser(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {

		LOG.debug("TransactionMediatorBean:::performNotifyPanelAuthUser");
		DocumentHandler outputDoc = new DocumentHandler();
		String transactionOid = inputDoc.getAttribute("/Transaction/transaction_oid");
		String instrumentOid = inputDoc.getAttribute("/Instrument/instrument_oid");

    	 // create email for all selected users to notification
	    Vector<DocumentHandler> beneficiaryDetailsList = inputDoc.getFragments("/EmailList/user");
	    StringBuilder emailReceivers = new StringBuilder();
	    String userOid = null;
	    String ownerOrgOid = null;

	    for(DocumentHandler doc : beneficiaryDetailsList){
		    userOid = doc.getAttribute("/userOid");
			User user = (User) mediatorServices.createServerEJB("User", Long.parseLong(userOid));
			String userMailID = user.getAttribute("email_addr");
			ownerOrgOid = user.getAttribute("owner_org_oid");
			if(StringFunction.isNotBlank(userMailID)){
				emailReceivers.append(userMailID + ",");
			}
	    }

	    if(StringFunction.isNotBlank(emailReceivers.toString())){

			 //Rel8.3 IR#T36000023107 END
			// Get the data from instrument that will be used to build the email message
				String instrumentId = "";
				String referenceNumber = "";

				if (StringFunction.isNotBlank(instrumentOid)) {
					Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
					instrumentId = instrument.getAttribute("complete_instrument_id");
					referenceNumber = instrument.getAttribute("copy_of_ref_num");
				}

				// Get the data from transaction that will be used to build the email message
				String transactionAmount = "";
				String transactioncurrency = "";
				String transactionType = "";
				String transactionTypeDesc = "";
				if (StringFunction.isNotBlank(transactionOid)) {
					Transaction transaction =(Transaction) mediatorServices.createServerEJB("Transaction", Long.parseLong(transactionOid));
					transactioncurrency = transaction.getAttribute("copy_of_currency_code");
					transactionAmount = TPCurrencyUtility.getDisplayAmount(transaction.getAttribute("copy_of_amount"), transactioncurrency, mediatorServices.getResourceManager().getResourceLocale());
					transactionType = transaction.getAttribute("transaction_type_code");
					transactionTypeDesc =ReferenceDataManager.getRefDataMgr().getDescr("TRANSACTION_TYPE",transactionType,
							mediatorServices.getResourceManager().getResourceLocale());
				}

				// Build the subject line and content of the email based on the type of email (transaction, mail message)
				StringBuilder emailContent = new StringBuilder();

				 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelBody1", TradePortalConstants.TEXT_BUNDLE));
				 emailContent.append(TradePortalConstants.NEW_LINE);

				 if (StringFunction.isNotBlank(instrumentId)) {
					 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.InstrumentId", TradePortalConstants.TEXT_BUNDLE)+" ");
					 emailContent.append(instrumentId);
					 emailContent.append(TradePortalConstants.NEW_LINE);
				 }
				 if (StringFunction.isNotBlank(referenceNumber)) {
					 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.CopyOfRefNum", TradePortalConstants.TEXT_BUNDLE)+" ");
					 emailContent.append(referenceNumber);
					 emailContent.append(TradePortalConstants.NEW_LINE);
				 }
				 if (StringFunction.isNotBlank(transactionTypeDesc)) {
					 emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.TransactionType", TradePortalConstants.TEXT_BUNDLE)+" ");
					 emailContent.append(transactionTypeDesc);
					 emailContent.append(TradePortalConstants.NEW_LINE);
				 }

				// Include Transaction Amount and currency
				if (StringFunction.isNotBlank(transactionAmount)) {
					emailContent.append(mediatorServices.getResourceManager().getText("EmailTrigger.Amount",TradePortalConstants.TEXT_BUNDLE)+ " ");
					emailContent.append(transactioncurrency+" "+transactionAmount);
					emailContent.append(TradePortalConstants.NEW_LINE);
				}
				emailContent.append(TradePortalConstants.NEW_LINE);
				//SSikhakolli - Rel-9.3.5 IR# T36000041674 09/22/2015 - Begin
				// call generic method to trigger email
				//InvoiceUtility.performNotifyPanelAuthUser(ownerOrgOid, emailContent, emailReceivers, mediatorServices);
				//Calling the above utility causing wrong wording in email alert.
				//It is adding Supplier Portal/RM wording to Trade/CM email alert formate. 
				//Hence writing the email trigger logic below to avoid wrong wording in email.
				
				// Retrieve Corporate Org
				CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(ownerOrgOid));
				String bankOrgOid = corpOrg.getAttribute("bank_org_group_oid");

				// Get the sender name, sender email address, system name, and bank
				// url from the corp org's bank org group
				BankOrganizationGroup bankOrg = (BankOrganizationGroup) mediatorServices.createServerEJB("BankOrganizationGroup", Long.parseLong(bankOrgOid));

				String senderName = bankOrg.getAttribute("repair_sender_name").trim();
				String senderEmailAddress = bankOrg.getAttribute("repair_sender_email_address");
				String tradeSystemName = bankOrg.getAttribute("repair_system_name").trim();
				String doNotReplyInd = bankOrg.getAttribute("repair_do_not_reply_ind");
				String bankUrl = bankOrg.getAttribute("repair_bank_url");
				 String incldBnkUrlInd = bankOrg.getAttribute("incld_bnk_url_repr_panel");
				// Build the subject line and content of the email based on the type
				// of email (transaction, mail message)
				StringBuilder emailSubject = new StringBuilder();
				StringBuilder emailFooter = new StringBuilder();

				emailSubject.append(tradeSystemName+ " "+ mediatorServices.getResourceManager().getText("EmailTrigger.PendingPanelAuthorisationAlert",TradePortalConstants.TEXT_BUNDLE));

				// Build the email's footer
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.LogIn", TradePortalConstants.TEXT_BUNDLE)+ " ");

				// Use default trade system name if bank org group has not specified one
				if (StringFunction.isBlank(tradeSystemName)) {
					tradeSystemName = mediatorServices.getResourceManager().getText("EmailTrigger.PaymentGroup", TradePortalConstants.TEXT_BUNDLE);
				}

				emailFooter.append(" " + tradeSystemName + " ");
				emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.AuthoriseMessage",	TradePortalConstants.TEXT_BUNDLE)+ " ");
				
				/*PGedupudi CR 1123 Rel 9.5 start*/
			if (TradePortalConstants.INDICATOR_YES.equals(incldBnkUrlInd)) {
					emailFooter.append(StringFunction.asciiToUnicode(
							mediatorServices.getResourceManager().getText(
								"EmailTrigger.LogIn",
								TradePortalConstants.TEXT_BUNDLE))
								+ " ");

						
						emailFooter.append(tradeSystemName + " ");
						emailFooter.append(
								mediatorServices.getResourceManager().getText(
								"EmailTrigger.AdditionalDetails",
								TradePortalConstants.TEXT_BUNDLE)
								+ " ");
						
						
					        if (!StringFunction.isBlank(bankUrl))
				                {
						  emailFooter.append(StringFunction.asciiToUnicode(
								  mediatorServices.getResourceManager().getText(
								"EmailTrigger.WebsiteAccess",
								TradePortalConstants.TEXT_BUNDLE))
								+ " ");
						  emailFooter.append(bankUrl + ".");
				                }
						
				}else{
					emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.ForAdditionalDetails",TradePortalConstants.TEXT_BUNDLE)+ ", ")
					.append(mediatorServices.getResourceManager().getText("EmailTrigger.PleaseLogIn",TradePortalConstants.TEXT_BUNDLE)+ " "+tradeSystemName+ " ")
					.append(StringFunction.asciiToUnicode(mediatorServices.getResourceManager().getText("EmailTrigger.ByVisitingBankSite",TradePortalConstants.TEXT_BUNDLE)));
				}
				/*PGedupudi CR 1123 Rel 9.5 end*/

				if (TradePortalConstants.INDICATOR_YES.equals(doNotReplyInd)) {
					emailFooter.append(TradePortalConstants.NEW_LINE+ TradePortalConstants.NEW_LINE);
					emailFooter.append(mediatorServices.getResourceManager().getText("EmailTrigger.DoNotReply",TradePortalConstants.TEXT_BUNDLE));
				}

				// Add the footer to the email content
				emailContent.append(emailFooter);

				// Create the format and content of the email
				DocumentHandler emailMessage = new DocumentHandler("<Email></Email>", false, true);
				emailMessage.setAttribute("/To", emailReceivers.toString());
				emailMessage.setAttribute("/Cc", "");
				emailMessage.setAttribute("/Bcc", "");
				emailMessage.setAttribute("/From/SenderAddress", senderEmailAddress);
				emailMessage.setAttribute("/From/SenderName", senderName);
				emailMessage.setAttribute("/Subject", StringFunction.asciiToUnicode(emailSubject.toString()));
				emailMessage.setAttribute("/Content", StringFunction.asciiToUnicode(emailContent.toString()));

				// Create a new email trigger object containing the email content
				// created above
				EmailTrigger emailTrigger = (EmailTrigger) mediatorServices.createServerEJB("EmailTrigger");
				emailTrigger.newObject();
				emailTrigger.setAttribute("agent_id", "");
				emailTrigger.setAttribute("status",TradePortalConstants.EMAIL_TRIGGER_NEW);
				emailTrigger.setAttribute("email_data", emailMessage.toString());
				emailTrigger.save();

				// display success message
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.MESSAGE_PROCESSED,
						new String[] {
						mediatorServices.getResourceManager().getText("Home.PanelAuthTransactions.Notify.Message",TradePortalConstants.TEXT_BUNDLE),
						mediatorServices.getResourceManager().getText("Home.PanelAuthTransactions.Notify.Sent",TradePortalConstants.TEXT_BUNDLE) });
				//SSikhakolli - Rel-9.3.5 IR# T36000041674 09/22/2015 - end
	    }

		return outputDoc;
	}

  /**
    *
    * @param transactionCorpOrg owner of transaction
    * @param loginUserCorpOrg corporate org of login user
    * @return true if it is subsidiary transaction
    *         false if it is not a subsiadiary transaction
    */
   private boolean isSubsidiaryTransaction(String transactionCorpOrg, String loginUserCorpOrg){
	   boolean isSubsidiary = false;
	   if(!transactionCorpOrg.equals(loginUserCorpOrg)){
		   isSubsidiary = true;
	   }

	   return isSubsidiary;
   }

   /**
    *
    * @param corpOrgOid
    * @param mediatorServices
    * @return  CorporateOrganization
    * @throws NumberFormatException
    * @throws RemoteException
    * @throws AmsException
    */
   private CorporateOrganization getCorporateOrg(String corpOrgOid, MediatorServices mediatorServices)
   throws NumberFormatException, RemoteException, AmsException{

	   CorporateOrganization corp_org = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization",Long.parseLong(corpOrgOid));
	   return corp_org;
   }

   /**
    *
    * @param instrType instrument type to search panel group
    * @param inputDoc form input data
    * @param corpOrg  Corporate Organization
    * @param mediatorServices
    * @return PanelAuthorizationGroup it is panel group for instrument type
    * @throws NumberFormatException
    * @throws RemoteException
    * @throws AmsException
    */
   private PanelAuthorizationGroup getPanelAuthorizationGroup(Transaction transaction, String instrType, DocumentHandler inputDoc, CorporateOrganization corpOrg, MediatorServices mediatorServices)
   throws NumberFormatException, RemoteException, AmsException{

		String panelGroupOid = null;
		String transactionType = transaction.getAttribute("transaction_type_code");
		if (InstrumentServices.isTradeTypeInstrument(instrType)) {

			panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(instrType, corpOrg, mediatorServices.getErrorManager(), transactionType, null, null, null);

		} else if (InstrumentServices.isCashManagementTypeInstrument(instrType)) {

			String paymentMethodType = null;
			// payment file doesn't have payment data in input doc. payment daat is already saved.
			// as per functionality all beneficiary will have same payment method.
			if(TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("uploaded_ind"))){
				//jgadela  R90 IR T36000026319 - SQL FIX
				String sql = "SELECT payment_method_type FROM domestic_payment WHERE p_transaction_oid = ? AND rownum = 1";
				DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{transaction.getAttribute("transaction_oid")});
				if(resultsDoc != null) {
				  paymentMethodType = resultsDoc.getAttribute("/ResultSetRecord(0)/PAYMENT_METHOD_TYPE");
				} else{
					return null;
				}
			}else{
			   paymentMethodType = inputDoc.getAttribute("/DomesticPayment/payment_method_type");
			}

			String confidentilInd = inputDoc.getAttribute("/Instrument/confidential_indicator");
			String debitAccountOid = inputDoc.getAttribute("/Terms/debit_account_oid");

			panelGroupOid = PanelAuthUtility.getPanelAuthorizationGroupOid(instrType, corpOrg, mediatorServices.getErrorManager(),
					transactionType, paymentMethodType, confidentilInd, debitAccountOid);
	    }

	   PanelAuthorizationGroup panelAuthGroup = null;
	   if(StringFunction.isNotBlank(panelGroupOid)){
	       panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup");
	       try{
	    	   panelAuthGroup.find("panel_auth_group_oid", panelGroupOid);
	       }catch(RemoteException exe){
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.PANEL_GROUP_NOT_PRESENT);
	       }
	   }else{
			 // issue error as panel group is required if panel auth defined for instrument type
		   mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText("CorpCust.PanelGroupId", TradePortalConstants.TEXT_BUNDLE));
	   }

	  return panelAuthGroup;
   }

  private String getBaseCurrency(Transaction transaction, String instrType, String transactionOrgOid, DocumentHandler inputDoc, MediatorServices mediatorServices)
  throws NumberFormatException, RemoteException, AmsException{

	  String baseCurrency = "";
	  CorporateOrganization corporateOrg = getCorporateOrg(transactionOrgOid, mediatorServices);
	  PanelAuthorizationGroup panelAuthGroup = getPanelAuthorizationGroup(transaction, instrType, inputDoc, corporateOrg, mediatorServices);
	  String debitAccountOid = inputDoc.getAttribute("/Terms/debit_account_oid");
	  Account debitAccount = (Account) mediatorServices.createServerEJB("Account", Long.parseLong(debitAccountOid));

	  baseCurrency = PanelAuthUtility.getBaseCurrency(corporateOrg, panelAuthGroup, debitAccount, instrType);

	  return baseCurrency;
  }

  /**
   *
   * @param transaction
   * @param instrument
   * @param panel
   * @param inputDoc
   * @param mediatorServices
   * @throws RemoteException
   * @throws AmsException
   */

	private void setPanelRangeDataInTransaction(Transaction transaction,
			Instrument instrument, Terms terms, DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		String transactionCorpOrg = instrument.getAttribute("corp_org_oid");
		CorporateOrganization corpOrg = getCorporateOrg(transactionCorpOrg,
				mediatorServices);
		// if user is corporate org, check for parent panel auth rights. admin
		// user can verify transaction.

		String debitAcctOid = inputDoc.getAttribute("/Terms/debit_account_oid");
		Account debitAccount = null;
		if (StringFunction.isNotBlank(debitAcctOid)) {
			debitAccount = (Account) mediatorServices.createServerEJB("Account", Long.parseLong(debitAcctOid));
		}

		String termCurrency = terms.getAttribute("amount_currency_code");
		String termAmount = terms.getAttribute("amount");
		String panelGroupOid = transaction.getAttribute("panel_auth_group_oid");
		PanelAuthorizationGroup panelAuthGroup = null;
		if (StringFunction.isNotBlank(panelGroupOid)) {
			panelAuthGroup = (PanelAuthorizationGroup) mediatorServices.createServerEJB("PanelAuthorizationGroup",
							Long.parseLong(panelGroupOid));

			BigDecimal amountInBaseToPanelGroup = PanelAuthUtility.getTransactionAmount(transaction, corpOrg, debitAccount,
							instrument, panelAuthGroup, termCurrency,
							termAmount, mediatorServices.getErrorManager());
			  if (amountInBaseToPanelGroup.compareTo(BigDecimal.ZERO) < 0 ){
				  return;
			  }
			String panelRangeOid = PanelAuthUtility.getPanelRange(panelGroupOid, amountInBaseToPanelGroup);
			if (StringFunction.isBlank(panelRangeOid)) {
				// Nar IR T36000019737 Release 8.3.0.0 Begin - modified to
				// display instr type code description and formated amount in
				// error message.
				String instrumentTypeDesc = ReferenceDataManager.getRefDataMgr().getDescr(
								TradePortalConstants.INSTRUMENT_TYPE,
								instrument.getAttribute("instrument_type_code"),
								mediatorServices.getResourceManager().getResourceLocale());
				String formattedAmountInBase = TPCurrencyUtility.getDisplayAmount(termAmount, termCurrency,
								mediatorServices.getResourceManager().getResourceLocale());

				mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.NO_PANEL_RANGE_FOR_AMOUNT_INST_TYPE,
								formattedAmountInBase, instrumentTypeDesc);
				// Nar IR T36000019737 Release 8.3.0.0 End
				return;
			}
			transaction.setAttribute("panel_auth_range_oid", panelRangeOid);

			// gettting panel object to store next awaiting approval to in attribute panel_Auth_next_approval of transaction object
			Panel panelObj = new Panel(panelGroupOid, new String[] {panelRangeOid}, transaction.getAttribute("panel_oplock_val"), instrument.getAttribute("instrument_type_code"), mediatorServices.getErrorManager());
			Map panelApproversSummaryMap = panelObj.getNextPanelApproverList("", null);
			String panelApprovalsList = StringFunction.getMapKeyToStringFormat(panelApproversSummaryMap);
			transaction.setAttribute("panel_auth_next_approvals", panelApprovalsList);

		}
	}

  /**
   * This method is used to set panel range Oid.
   * @param transaction
   * @param instrument
   * @param inputDoc
   * @param mediatorServices
   * @throws RemoteException
   * @throws AmsException
   */

  private void performPanelProcessing(CorporateOrganization transactionCorpOrg, Transaction transaction, Instrument instrument, DocumentHandler inputDoc,
		  MediatorServices mediatorServices)
  throws RemoteException, AmsException{

	  String transactionCorpOrgOid = instrument.getAttribute("corp_org_oid");
      String instrType = instrument.getAttribute("instrument_type_code");
      String transactionType = transaction.getAttribute("transaction_type_code");
	  User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));
	    // if user is corporate org, check for parent panel auth rights. admin user can verify transaction.
	    if ((user.getAttribute("ownership_level")).equals(TradePortalConstants.OWNER_CORPORATE)){
		   String loginUserCorpOrg = user.getAttribute("owner_org_oid");
		   // check for subsidiary transaction. if it is, both should be setup for panel auth
		   if (isSubsidiaryTransaction(transactionCorpOrgOid, loginUserCorpOrg)) {
			CorporateOrganization parentCorpOrg = getCorporateOrg(loginUserCorpOrg, mediatorServices);

			if (!parentCorpOrg.isPanelAuthEnabled(instrType, transactionType)) {
				// issue error if both parent and child corp not setup for panel Authorization
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);
			}else if (InstrumentType.DOM_PMT.equals(instrType) && TradePortalConstants.INDICATOR_YES.equals(transactionCorpOrg.getAttribute("pmt_bene_panel_auth_ind")) &&
					!TradePortalConstants.INDICATOR_YES.equals(parentCorpOrg.getAttribute("pmt_bene_panel_auth_ind"))){
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.PANEL_AUTH_NOT_SETUP_FOR_PARENT);
			}
		   }
		}
		PanelAuthorizationGroup panelAuthGroup = getPanelAuthorizationGroup(transaction, instrType, inputDoc, transactionCorpOrg, mediatorServices);
		// if panel group not found, return the flow back.
		if(panelAuthGroup == null){
			return;
		}
		transaction.setAttribute("panel_auth_group_oid", panelAuthGroup.getAttribute("panel_auth_group_oid"));
		transaction.setAttribute("panel_oplock_val", panelAuthGroup.getAttribute("opt_lock"));

		// Nar Rel 8.3 CR 857 07/15/2013 Begin
		// Set beneficiary panel check indicator at verify time. it will be using during authorization process to check whether
		// Beneficiary level panel authorization is enabled or not.
		if(InstrumentType.DOM_PMT.equals(instrType) &&
		        TradePortalConstants.INDICATOR_YES.equals(transactionCorpOrg.getAttribute("pmt_bene_panel_auth_ind"))){
			transaction.setAttribute("bene_panel_auth_ind", TradePortalConstants.INDICATOR_YES);
		}
  }

	private String getInstrumentID(String instrumentOid,MediatorServices mediatorServices) throws NumberFormatException, RemoteException, AmsException{

		// Get a handle to an instance of the instrument
		String id = "";
		if (StringFunction.isNotBlank(instrumentOid)){
			Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument", Long.parseLong(instrumentOid));
			id = instrument.getAttribute("complete_instrument_id");
		}

		return id;
	}
	
	/* KMehta IR-T36000034712 Rel 9500 on 10-Dec-2015 Add  - Begin*/
	/**
	 * This method is used to validate instrument Loan Request Data for TradeLoan Payables 
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws AmsException 
	 * @throws RemoteException 
	 * @throws NumberFormatException 
	 */
	private void validatePayablesLoanProceedsTo(DocumentHandler inputDoc, MediatorServices mediatorServices)
			throws NumberFormatException, RemoteException, AmsException {
				
		String loanProceedsCreditTypeTrd = inputDoc.getAttribute("/Terms/loan_proceeds_credit_type_trd");
		if(TradePortalConstants.CREDIT_BEN_ACCT.equalsIgnoreCase(loanProceedsCreditTypeTrd)){
			
			String transaction = inputDoc.getAttribute("/Transaction/transaction_oid");
		    String sb = "SELECT inv_pay_inst_oid from invoice_payment_instructions ipi where ipi.p_transaction_oid = ?";
			DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(sb, false, new Object[]{transaction});
			if (resultsDoc != null){
				List<DocumentHandler> resultsVec = resultsDoc.getFragmentsList("/ResultSetRecord");
				Vector newInvPayInsOidList = new Vector();
				try{
					sb = "DELETE FROM invoice_payment_instructions WHERE p_transaction_oid = ?";
					DatabaseQueryBean.executeUpdate(sb, true, new Object[]{transaction});					
				}
				catch (Exception e) {
					LOG.info("General exception caught in TransactionMediator:performEdit");
					LOG.info("   " + e.toString());
					e.printStackTrace();
				} finally {
					LOG.debug("Mediator: Result of update is ");
				}
			}
		
		}
	}
	
	/* KMehta IR-T36000034712 Rel 9500 on 10-Dec-2015 Add  - End*/
	
	// Nar IR-T36000037563 Rel 9.2.0.0 05/20/2015 Begin
	/**
	 * This method is used to validate instrument domestic payment data with fixed template domestic payment data. 
	 * if instrument is created with fixed template then fixed data, populated from template, should not get changed. 
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws AmsException 
	 * @throws RemoteException 
	 * @throws NumberFormatException 
	 */
	private void validateInstrumentDatawithFixedTempData(DocumentHandler inputDoc, MediatorServices mediatorServices)
			throws NumberFormatException, RemoteException, AmsException {
		//Srinivasu_D IR#T36000041155 Rel9.3 07/02/2015 - Handling NPE
		String tempDomesticPaymentOid = null;
		if(StringFunction.isNotBlank(inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid"))){
			 tempDomesticPaymentOid = getTempDomesticPaymentOid(inputDoc.getAttribute("/DomesticPayment/domestic_payment_oid"));
		}
		if (StringFunction.isNotBlank(tempDomesticPaymentOid)) {
			DocumentHandler tempDomesticPaymentDoc = new DocumentHandler();
			boolean isFixedDataChanged = false;
			DomesticPayment domesticPayment = (DomesticPayment) mediatorServices.createServerEJB("DomesticPayment", Long.parseLong(tempDomesticPaymentOid));
			domesticPayment.populateXmlDoc(tempDomesticPaymentDoc);
			for (String field : domesticPaymentField) {
				String sourceTemplateFieldVal = tempDomesticPaymentDoc.getAttribute("/DomesticPayment/" + field);
				if (StringFunction.isNotBlank(sourceTemplateFieldVal)) {
					if ("amount".equals(field)) {
						if (TradePortalConstants.INDICATOR_NO.equals(tempDomesticPaymentDoc.getAttribute("/DomesticPayment/allow_pay_amt_modification"))) {
							BigDecimal sourceTempAmt = new BigDecimal(sourceTemplateFieldVal);
							if (sourceTempAmt.compareTo(new BigDecimal(inputDoc.getAttribute("/DomesticPayment/"+ field))) != 0) {
								isFixedDataChanged = true;
							}
						}
					} else if ("payee_description".equals(field)) {
						if (TradePortalConstants.INDICATOR_NO.equals(tempDomesticPaymentDoc.getAttribute("/DomesticPayment/allow_pay_detail_modification"))
								&& !sourceTemplateFieldVal.equals(inputDoc.getAttribute("/DomesticPayment/"+ field))) {
							isFixedDataChanged = true;

						}
					} else if (!sourceTemplateFieldVal.equals(inputDoc.getAttribute("/DomesticPayment/" + field))) {
						isFixedDataChanged = true;
					}
				} else if ("payee_invoice_details".equals(field)) {
					if (TradePortalConstants.INDICATOR_NO.equals(tempDomesticPaymentDoc.getAttribute("/DomesticPayment/allow_inv_detail_modification"))) {
						String sourceTempInvDetail = getDomesticPaymentInvDetail(tempDomesticPaymentOid, mediatorServices);
						if (!sourceTempInvDetail.equals(inputDoc.getAttribute("/InvoiceDetails/payee_invoice_details"))) {
							isFixedDataChanged = true;
						}
					}
				}
			}
			if (isFixedDataChanged) {
				throw new AmsException("Data descrimination found in fixed template domestic payment transaction");
			}
		}

	}

	/**
	 * This method is used to find invoice detail data of domestic payment.
	 * @param tempDomesticPaymentOid
	 * @return
	 * @throws AmsException
	 * @throws RemoteException 
	 */
	private String getDomesticPaymentInvDetail(String domesticPaymentOid, MediatorServices mediatorServices) throws AmsException, RemoteException {
		String sql = " SELECT invoice_detail_oid FROM invoice_details WHERE domestic_payment_oid = ? ";
		DocumentHandler doc = DatabaseQueryBean.getXmlResultSet(sql, true, domesticPaymentOid);
		long invDetailOid = doc.getAttributeLong("/ResultSetRecord(0)/INVOICE_DETAIL_OID");
		InvoiceDetails invoiceDetails = (InvoiceDetails) mediatorServices.createServerEJB("InvoiceDetails", invDetailOid);		 
		return invoiceDetails.getAttribute("payee_invoice_details");
	}


	/**
	 * This method is used to find source template oid of Domestic Payment.
	 * @param domesticPaymentOid
	 * @return
	 * @throws AmsException
	 */
	private String getTempDomesticPaymentOid( String domesticPaymentOid ) throws AmsException {
		String sql = " SELECT source_template_dp_oid FROM domestic_payment WHERE domestic_payment_oid = ? ";
		DocumentHandler doc = DatabaseQueryBean.getXmlResultSet(sql, true, domesticPaymentOid);
		String tempDomesticPaymentOid = doc.getAttribute("/ResultSetRecord(0)/SOURCE_TEMPLATE_DP_OID");
		return tempDomesticPaymentOid;
	}
	
	/**
	 * This method is used to find whether instrument is created with fixed template or not.
	 * @param instrumentOid
	 * @return
	 * @throws AmsException
	 */
	private boolean isFixedPaymentInstrument(String instrumentOid) throws AmsException {
		boolean isFixedPayment = false;
		String sql = " SELECT fixed_payment_flag FROM instrument WHERE instrument_oid = ? ";
		DocumentHandler doc = DatabaseQueryBean.getXmlResultSet(sql, true, instrumentOid);
		if ( TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/ResultSetRecord(0)/FIXED_PAYMENT_FLAG"))) {
			isFixedPayment = true;
		}
	  return isFixedPayment;
	}
	// Nar IR-T36000037563 Rel 9.2.0.0 05/20/2015 End


	// Nar CR-818 Rel 9400 08/11/2015 Add- Begin
    /**
     * This method is used to Validate Settlement Instruction data for SIR, SIM or Discrepancy(which include Settlement data) transaction.
     * @param transaction
     * @param instrument
     * @param inputDoc
     * @param mediatorServices
     * @throws AmsException
     * @throws RemoteException
     */
	private void performVerificationOfSettlemntInstrDetail(Terms terms, Transaction transaction, Instrument instrument,
			DocumentHandler inputDoc, MediatorServices mediatorServices) throws AmsException, RemoteException {
		
		String workItemType = transaction.getAttribute("settle_instr_work_item_type");
		String instrumentType = instrument.getAttribute("instrument_type_code");
		String transactionCurrency = transaction.getAttribute("copy_of_currency_code");
		
		// Apply Payment on Date is required field for Settlement Instruction Request/Response.
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/apply_payment_on_date")) ) {
			mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.SETTLEMENT_PAYMENT_DATE_REQ );
		}
		
		// if Apply Payment on Date is entered by user, then it can not be past date.
		if ( StringFunction.isNotBlank(terms.getAttribute("apply_payment_on_date")) && 
				! isValidSettlementDate(terms.getAttributeDate("apply_payment_on_date"), inputDoc, mediatorServices) ) {
			mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.SETTLEMENT_PAYMENT_DATE_CAN_NOT_PAST_DATE );
		}
		
		// if Fixed Maturity Date is entered by user, then it can not be past date.
		if ( StringFunction.isNotBlank(terms.getAttribute("fin_roll_maturity_date")) && 
				! isValidSettlementDate(terms.getAttributeDate("fin_roll_maturity_date"), inputDoc, mediatorServices) ) {
			mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.SETTLEMENT_FIXED_MATURITY_DATE_CAN_NOT_PAST_DATE );
		}
		// for discrepancy response settlement instruction must be optional
		if (!(TransactionType.DISCREPANCY.equals(transaction.getAttribute("transaction_type_code")) && SettlementInstrUtility.isSettleInstrIncludeInDCR(instrument.getAttribute("corp_org_oid")))) {
			if (StringFunction.isBlank(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind"))) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.SETTLEMENT_INSTR_REQ);
			}
		}
		
		if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) || 
				TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL.equals(inputDoc.getAttribute("/Terms/fin_roll_full_partial_ind")) ) {		
			if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/account_remit_ind")) ) {
			    mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, TradePortalConstants.SETTLEMENT_PAYMENT_ACCT_MODE_REQ );
			}
		}
				
		if ( TradePortalConstants.SETTLEMENT_ACCOUNT_REMIT_TYPE_ACCOUNT.equals(inputDoc.getAttribute("/Terms/account_remit_ind")) ) {
			
			verifySettlementRequiredAccount(workItemType, instrumentType, inputDoc, mediatorServices);			           
		}
		
		verifySettlementDebitAcctCurrencyDetail(inputDoc, transactionCurrency, mediatorServices);		
		
		if ( TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_FXCONTRACT.equals(inputDoc.getAttribute("/Terms/daily_rate_fec_oth_ind")) ) {
			  
			verifySettlementFXContractDetail(inputDoc, mediatorServices);			
		}
		
		if ( TradePortalConstants.SETTLEMENT_DAILY_RATE_FEC_TYPE_OTHER.equals(inputDoc.getAttribute("/Terms/daily_rate_fec_oth_ind")) &&
				StringFunction.isBlank(inputDoc.getAttribute("/Terms/settle_other_fec_text")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_OTHER_FEC_TEXT_REQ );
		}
		
		if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_FINANCE.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) || 
				TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) ) {	
			
			verifySettlementFinanceOrRolloverReqOption(inputDoc, mediatorServices);			
		}
		
		if ( StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/fin_roll_full_partial_ind")) &&
                  StringFunction.isBlank(inputDoc.getAttribute("/Terms/rollover_terms_ind"))  ) {
			if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) ) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_ROLL_DAYS_OR_DATE_REQ );
			} else {
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FIN_DAYS_OR_DATE_REQ );
			}
		}
		
		if ( TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_DAYS.equals(inputDoc.getAttribute("/Terms/rollover_terms_ind")) && 
				StringFunction.isBlank(inputDoc.getAttribute("/Terms/fin_roll_number_of_days")) ) {
			if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) ) {
			    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_ROLL_DAYS_REQ );
			} else {
				 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FIN_DAYS_REQ );
			}
		}
		
		if ( TradePortalConstants.SETTLEMENT_ROLLOVER_TERMS_MATURITY_DATE.equals(inputDoc.getAttribute("/Terms/rollover_terms_ind")) && 
				StringFunction.isBlank(inputDoc.getAttribute("/Terms/fin_roll_maturity_date")) ) {
			if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) ) {
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_ROLL_MATURITY_DATE_REQ );
			} else {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FIN_MATURITY_DATE_REQ );
			}
		}
		
		if ( TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/Terms/other_addl_instructions_ind")) && 
				StringFunction.isBlank(inputDoc.getAttribute("/Terms/other_addl_instructions")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_ADDL_INSTR_TEXT_REQ );
		}
		
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/daily_rate_fec_oth_ind")) && 
				StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/fin_roll_curr")) && !inputDoc.getAttribute("/Terms/fin_roll_curr").equals(transactionCurrency)) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FX_RATE_REQ_FOR_FIN );
		}
		
		if ( this.isTransSuccess(mediatorServices.getErrorManager()) && StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/daily_rate_fec_oth_ind")) ) {
			checkFxRequiredDetails( terms, transactionCurrency, inputDoc );
		}
	}

	/**
	 * This method is used verify that all data avilable for account currency if conversion required.
	 * @param inputDoc
	 * @param transactionCurrency
	 * @param mediatorServices
	 * @throws AmsException
	 */
	private void verifySettlementDebitAcctCurrencyDetail( DocumentHandler inputDoc, String transactionCurrency, MediatorServices mediatorServices ) 
			throws AmsException {
		
    	int theCount = countForAcctWithDifferentCurr(inputDoc, transactionCurrency);
		if ( theCount > 0 && StringFunction.isBlank(inputDoc.getAttribute("/Terms/daily_rate_fec_oth_ind")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FX_RATE_REQ );
		}
		
	}
	
	/**
	 * This method is used verify all required FX Contract Detail data.
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void verifySettlementFXContractDetail( DocumentHandler inputDoc, MediatorServices mediatorServices ) 
			throws RemoteException, AmsException {
		
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/settle_covered_by_fec_num")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FX_CONTRACT_DETAIL_REQ,
					mediatorServices.getResourceManager().getText("SettlementInstruction.FXContractNum", TradePortalConstants.TEXT_BUNDLE));
		} 
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/settle_fec_rate")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FX_CONTRACT_DETAIL_REQ,
					mediatorServices.getResourceManager().getText("SettlementInstruction.rate", TradePortalConstants.TEXT_BUNDLE));
		}
	    if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/settle_fec_currency")) ) { 
	    	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FX_CONTRACT_DETAIL_REQ,
	    			mediatorServices.getResourceManager().getText("SettlementInstruction.currency", TradePortalConstants.TEXT_BUNDLE));
	    }
	}
	
	/**
	 * This method is used to check all debit account information is provided for settlement if account option is selected.
	 * @param workItemType
	 * @param instrumentType
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void verifySettlementRequiredAccount( String workItemType, String instrumentType, DocumentHandler inputDoc, MediatorServices mediatorServices ) 
			throws RemoteException, AmsException {
		
		if ( !(TradePortalConstants.SETTLEMENT_WORK_ITEM_TYPE_ROLLOVER.equals(workItemType) && 
				TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_FULL.equals(inputDoc.getAttribute("/Terms/fin_roll_full_partial_ind"))) &&
    		    StringFunction.isBlank(inputDoc.getAttribute("/Terms/principal_account_oid")) ) {
        	mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, 
        			TradePortalConstants.SETTLEMENT_DEBIT_ACCT_REQ,
        			mediatorServices.getResourceManager().getText("SettlementInstruction.principal", TradePortalConstants.TEXT_BUNDLE));
			
		}	
		// Nar IR-T36000044040 interest account is not required for loan request
/*		if ( InstrumentType.LOAN_RQST.equals(instrumentType) && 
				StringFunction.isBlank(inputDoc.getAttribute("/Terms/interest_account_oid")) ) {
			mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, 
        			TradePortalConstants.SETTLEMENT_DEBIT_ACCT_REQ,
        			mediatorServices.getResourceManager().getText("SettlementInstruction.interest", TradePortalConstants.TEXT_BUNDLE));
			
		}*/
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/charges_account_oid")) ) {
			mediatorServices.getErrorManager().issueError( TradePortalConstants.ERR_CAT_1, 
        			TradePortalConstants.SETTLEMENT_DEBIT_ACCT_REQ,
        			mediatorServices.getResourceManager().getText("SettlementInstruction.charge", TradePortalConstants.TEXT_BUNDLE));				
		}
	}
	
	/**
	 * This method is used to check all required data if 'Finance in Finace Currency' or 'Rollover in Finance Curreny' in case of rollover.
	 * @param inputDoc
	 * @param mediatorServices
	 * @throws AmsException
	 */
	private void verifySettlementFinanceOrRolloverReqOption( DocumentHandler inputDoc, MediatorServices mediatorServices ) 
			throws AmsException {
		
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/fin_roll_curr")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FINANCE_CURR_REQ );
		}
		if ( StringFunction.isBlank(inputDoc.getAttribute("/Terms/fin_roll_full_partial_ind")) ) {
			if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_ROLL.equals(inputDoc.getAttribute("/Terms/pay_in_full_fin_roll_ind")) ) {
				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_ROLL_FULL_OR_PARTIAL_IND_REQ );
			} else {
			   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_FIN_FULL_OR_PARTIAL_IND_REQ );
			}
		}
		if ( TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_PARTIAL.equals(inputDoc.getAttribute("/Terms/fin_roll_full_partial_ind")) && 
				 StringFunction.isBlank(inputDoc.getAttribute("/Terms/fin_roll_partial_pay_amt")) ) {
			mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1 ,TradePortalConstants.SETTLEMENT_PARTIAL_PAY_AMT_REQ );
		}
	}
	
	/**
	 * This method is used to return the count for debit account which has diffrent curreny compare to transaction currency
	 * @param inputDoc
	 * @param transactionCurrency
	 * @return
	 * @throws AmsException
	 */
	private int countForAcctWithDifferentCurr( DocumentHandler inputDoc, String transactionCurrency ) throws AmsException {
		
		// IR T36000049159 Rel9.5.0.3
		StringBuilder sqlStatement = new StringBuilder(" SELECT count(*) the_count FROM account WHERE  currency != ? AND account_oid in ( ? , ?)");
		String principalAcctOid = inputDoc.getAttribute("/Terms/principal_account_oid");
		String interestAcctOid = inputDoc.getAttribute("/Terms/interest_account_oid");		
		DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlStatement.toString(), true, 
				new Object[]{transactionCurrency, principalAcctOid, interestAcctOid});
    	int theCount = resultXML!=null?resultXML.getAttributeInt("/ResultSetRecord(0)/THE_COUNT"):0;
    	
    	return theCount;
	}	
	
	/**
	 * This method is used to check whether settlement FX details is required or not. if it is not required, then clear out details if entered by user.
	 * @param terms
	 * @param transactionCurrency
	 * @param inputDoc
	 * @throws AmsException
	 * @throws RemoteException 
	 */
	private void checkFxRequiredDetails(Terms terms, String transactionCurrency, DocumentHandler inputDoc) throws AmsException, RemoteException {

		boolean clearFxDetail = true;
		int theCount = countForAcctWithDifferentCurr(inputDoc, transactionCurrency);
		if ( theCount > 0 ) {
			clearFxDetail = false;
		} else if ( StringFunction.isNotBlank(transactionCurrency) &&  StringFunction.isNotBlank(inputDoc.getAttribute("/Terms/fin_roll_curr")) && 
				  !transactionCurrency.equals(inputDoc.getAttribute("/Terms/fin_roll_curr")) ) {
			clearFxDetail = false;
		}
		
		if ( clearFxDetail ) {
			clearSettlementFXDetails(terms);
		}
		
	}
	
	/**
	 * This method is used to clear settlement instrcution FX detail entered by user.
	 * @param terms
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void clearSettlementFXDetails(Terms terms) throws RemoteException, AmsException {

       terms.setAttribute("daily_rate_fec_oth_ind", null);
       terms.setAttribute("settle_covered_by_fec_num", null);
       terms.setAttribute("settle_fec_rate", null);
       terms.setAttribute("settle_fec_currency", null);
       terms.setAttribute("settle_other_fec_text", null);		
	}

    private boolean isValidSettlementDate( Date settlementDate, DocumentHandler inputDoc, MediatorServices mediatorServices ) 
    		throws AmsException, RemoteException{
    	
    	boolean validDate = true;
		User user = (User) mediatorServices.createServerEJB("User", inputDoc.getAttributeLong("/LoginUser/user_oid"));
		String timeZone = user.getAttribute("timezone");
    	//obtain current date with regards to user's timezone
		java.util.Date todaysDate = null;
		if (StringFunction.isNotBlank(timeZone))
		{
			todaysDate = TPDateTimeUtility.getLocalDateTime(DateTimeUtility.getGMTDateTime(),timeZone);
		} else {
			todaysDate = GMTUtility.getGMTDateTime();
		}
		//compare dates and set status/warning/error accordingly
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String settlementDateStr = formatter.format(settlementDate);
		String todaysDateStr = formatter.format(todaysDate);
		LOG.debug("TransactionMediator::isValidSettlementDate:: " + settlementDateStr + " vs. " + todaysDateStr);
		if ( settlementDateStr.compareTo(todaysDateStr) < 0 ) {
			validDate = false;
		}
    	return validDate;
    	
    }
	
	// Nar CR-818 Rel 9400 08/11/2015 Add- End
	
	/**
     * this method is used to update the Reporting Codes to the domestic payments having reporting code errors.
     * 
     * @return DocumentHandler
     * @param inputDoc xml doc has payment data
     * @param MediatorService
     * @throws RemoteException, AmsException
     */
	private DocumentHandler performReportingCodesUpdate(DocumentHandler inputDoc, MediatorServices mediatorServices)
	throws RemoteException, AmsException {
		
		LOG.debug("TransactionMediatorBean:::performReportingCodesUpdate");

		DocumentHandler outputDoc = new DocumentHandler();
		Instrument instrument     = null;
		Transaction transaction   = null;
		PaymentFileUpload fileUpload = null;
		long instrumentOid        = 0;
		long transactionOid       = 0;
		long uploadOid   	      = 0;
		String errorLogOid		  = null;
		
		try {
			instrumentOid = inputDoc.getAttributeLong("/Instrument/instrument_oid");
			if (instrumentOid == 0)
			{
				LOG.debug("Error: instrument_oid not present");
				return null;
			}

			// Get a handle to an instance of the instrument, the transaction,
			// and the terms objects.
			instrument = (Instrument) mediatorServices.createServerEJB("Instrument");
			LOG.debug("Mediator: Retrieving instrument " + instrumentOid);
			instrument.getData(instrumentOid);
			
			// Get the transaction oid from the doc.
			transactionOid = instrument.getAttributeLong("original_transaction_oid");
			if (transactionOid == 0)
			{
				LOG.debug("Error: transaction_oid not present");
				return null;
			}

			// Get the payment upload file oid from the doc.
			uploadOid = inputDoc.getAttributeLong("/PaymentFileUpload/payment_file_upload_oid");
			if (uploadOid == 0)
			{
				LOG.debug("Error: uploadOid not present");
				return null;
			}

			if (StringFunction.isBlank(inputDoc.getAttribute("/LoginUser/user_oid")))
			{
				LOG.debug("Error: userOid not present");
				return null;
			}

			fileUpload = (PaymentFileUpload) mediatorServices.createServerEJB("PaymentFileUpload");
		    fileUpload.getData(uploadOid);

			LOG.debug("Mediator: Getting handle to transaction " + transactionOid);
			transaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
			Terms terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");     //// NSX IR# PAUL012383540  Rel:6.1.0

			String debAccountOid = terms.getAttribute("debit_account_oid"); 
	        if (StringFunction.isBlank(debAccountOid))
			{
	        	LOG.debug("Error: debAccountOid not present");
	        	return null;
			}

			Account debitAccount = (Account) mediatorServices.createServerEJB("Account");
			debitAccount.getData(Long.parseLong(debAccountOid));
			String opBankOrgOid = debitAccount.getAttribute("op_bank_org_oid");
			if (StringFunction.isBlank(opBankOrgOid))
			{
				mediatorServices.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.REP_CODE_OP_ORG_ID_MISSING);
				return null;
			}

			OperationalBankOrganization opBankOrg = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization",
														  Long.parseLong(opBankOrgOid));
			String bankGroupOid = opBankOrg.getAttribute("bank_org_group_oid");
			if (StringFunction.isBlank(bankGroupOid))
			{
				mediatorServices.getErrorManager().issueError(getClass().getName(),
						TradePortalConstants.REP_CODE_BANK_GRP_MISSING);
				return null;
			}

			String reportingCode1ReqdForACH=null, reportingCode1ReqdForRTGS=null, reportingCode2ReqdForACH=null, reportingCode2ReqdForRTGS = null;
			String sqlQuery = "select reporting_code1_reqd_for_ach, reporting_code1_reqd_for_rtgs, reporting_code2_reqd_for_ach, reporting_code2_reqd_for_rtgs"+
						" from bank_organization_group where organization_oid=?";
			DocumentHandler resultDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, new Object[]{bankGroupOid}); 
	       	if(resultDoc!=null){
				reportingCode1ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_ACH");
				reportingCode1ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE1_REQD_FOR_RTGS");
				reportingCode2ReqdForACH = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_ACH");
				reportingCode2ReqdForRTGS = resultDoc.getAttribute("/ResultSetRecord/REPORTING_CODE2_REQD_FOR_RTGS");
	       	}
	       	
			DocumentHandler paymentUploadErrorLog = null;
			Vector paymentUploadErrorLogList = null;
	        
			String paymentMethodType = transaction.getAttribute("payment_method_type");
	        String reportingCode1 = inputDoc.getAttribute("/PaymentFileUpload/reporting_code_1");
	        String reportingCode2 = inputDoc.getAttribute("/PaymentFileUpload/reporting_code_2");
	        paymentUploadErrorLogList = inputDoc.getFragments("/PaymentUploadErrorLogList");
	        
	        for (int i = 0; i < paymentUploadErrorLogList.size(); i++) {
	        	String lineNum = null, bene_info = null, dp_oid=null, paymentUploadOid = null; 
	        	paymentUploadErrorLog = (DocumentHandler) paymentUploadErrorLogList.get(i);
	        	errorLogOid = paymentUploadErrorLog.getAttribute("paymentUploadErrorLogOid");
	        	String sqlPayErrQuery = "select line_number, beneficiary_info, p_domestic_payment_oid, p_payment_file_upload_oid "+
						" from payment_upload_error_log where payment_upload_error_log_oid=?";
	        	DocumentHandler resultPayErrDoc = DatabaseQueryBean.getXmlResultSet(sqlPayErrQuery, false, new Object[]{errorLogOid}); 
	        	if(resultPayErrDoc!=null){
		    		lineNum = resultPayErrDoc.getAttribute("/ResultSetRecord/LINE_NUMBER");
		    		bene_info = resultPayErrDoc.getAttribute("/ResultSetRecord/BENEFICIARY_INFO");
		    		dp_oid = resultPayErrDoc.getAttribute("/ResultSetRecord/P_DOMESTIC_PAYMENT_OID");
		    		paymentUploadOid = resultPayErrDoc.getAttribute("/ResultSetRecord/P_PAYMENT_FILE_UPLOAD_OID");
	        	}
	        	
	        	String dp_reporting_code_1 = null, dp_reporting_code_2 = null;
	        	String sqlDPQuery = "select reporting_code_1, reporting_code_2 from domestic_payment where domestic_payment_oid=?";
	        	DocumentHandler resultDPDoc = DatabaseQueryBean.getXmlResultSet(sqlDPQuery, false, new Object[]{dp_oid}); 
	        	if(resultDPDoc!=null){
	        		dp_reporting_code_1 = resultDPDoc.getAttribute("/ResultSetRecord/REPORTING_CODE_1");
	        		dp_reporting_code_2 = resultDPDoc.getAttribute("/ResultSetRecord/REPORTING_CODE_2");
	        	}
                
	        	String sqlStatementToRepCodes = null;
	    		if(StringFunction.isNotBlank(reportingCode1)){
		    		sqlStatementToRepCodes = "UPDATE domestic_payment SET reporting_code_1=? WHERE domestic_payment_oid = ?";
					DatabaseQueryBean.executeUpdate(sqlStatementToRepCodes, true, new Object[]{reportingCode1,dp_oid});
					dp_reporting_code_1 = reportingCode1;
	    		}
	    		if(StringFunction.isNotBlank(reportingCode2)){
		    		sqlStatementToRepCodes = "UPDATE domestic_payment SET reporting_code_2=? WHERE domestic_payment_oid = ?";
					DatabaseQueryBean.executeUpdate(sqlStatementToRepCodes, true, new Object[]{reportingCode2,dp_oid});
					dp_reporting_code_2 = reportingCode2;
	    		}
				
	    		String[] reportingCodes = new String[2];
	    		reportingCodes[0] = dp_reporting_code_1;
				reportingCodes[1] = dp_reporting_code_2;
				
	    		validateReportingCodes(reportingCodes, paymentMethodType, bankGroupOid, reportingCode1ReqdForACH, reportingCode2ReqdForACH,
	    				reportingCode1ReqdForRTGS, reportingCode2ReqdForRTGS, mediatorServices);

	    		List<IssuedError> errorList = mediatorServices.getErrorManager().getIssuedErrorsList();
 	            if (errorList.size() > 0) {
	                StringBuilder sb = new StringBuilder();
	                for (IssuedError errorObject : errorList) {
	                    sb.append("- ").append(errorObject.getErrorText());
	                }
	                Map<String, String> tempMap = new HashMap<>();
	                Map<String, Map<String, String>> uploadLog = new HashMap<>();
	                tempMap.put(bene_info, sb.toString());
	                uploadLog.put(lineNum, tempMap);
	                sqlStatementToRepCodes = "DELETE FROM payment_upload_error_log WHERE payment_upload_error_log_oid = ?";
					DatabaseQueryBean.executeUpdate(sqlStatementToRepCodes, true, new Object[]{errorLogOid});
	                insertIntoErrorLog(uploadLog, paymentUploadOid, dp_oid);
	            } else{
	            	sqlStatementToRepCodes = "DELETE FROM payment_upload_error_log WHERE payment_upload_error_log_oid = ?";
					DatabaseQueryBean.executeUpdate(sqlStatementToRepCodes, true, new Object[]{errorLogOid});
	            }
	            mediatorServices.getErrorManager().initialize();
	            
	        }
	   
		} catch (Exception e) {
			LOG.info("General exception caught in TransactionMediator:performReportingCodesUpdate");
			LOG.info("   " + e.toString());
			e.printStackTrace();
		} 
		mediatorServices.setTranSuccess(true);
		mediatorServices.getErrorManager().initialize();
		return outputDoc;
		
     }
	
	/**
	 * Cr-1001 Rel9.4
	 * @param uploadRepLog
	 * @param paymentUploadOid
	 * @param domesticPaymentOid
	 */
	private void insertIntoErrorLog(Map<String,Map<String, String>> uploadRepLog, String paymentUploadOid, String domesticPaymentOid ){
		if (uploadRepLog.size() > 0) {
			try {
            	String insertQuery = "insert into PAYMENT_UPLOAD_ERROR_LOG (payment_upload_error_log_oid, line_number, p_payment_file_upload_oid, beneficiary_info,error_msg, p_domestic_payment_oid, REPORTING_CODE_ERROR_IND)values(?,?,?,unistr(?),?,?,?)";
                for (String line_num : uploadRepLog.keySet()) {
                    Map tmpMap = uploadRepLog.get(line_num);
                    Iterator temp_it = tmpMap.keySet().iterator();
                    String bene_info = (String) temp_it.next();
                    String errMsg = (String) tmpMap.get(bene_info);
                    String oid = Long.toString(ObjectIDCache.getInstance(AmsConstants.OID).generateObjectID(true));
                    DatabaseQueryBean.executeUpdate(insertQuery, true, new Object[]{oid, line_num,paymentUploadOid, StringFunction.toUnistr(bene_info), errMsg, domesticPaymentOid, TradePortalConstants.INDICATOR_YES});
                }
                

            } catch (AmsException | SQLException e) {
                LOG.info("Error occured during saving Payment Upload Error log ..."+ e);
            }
		}

	}

	/**
	 * Cr-1001 Rel9.4
	 * @param reportingCodes
	 * @param paymentMethod
	 * @param bankGroupOid
	 * @param reportingCode1ReqdForACH
	 * @param reportingCode1ReqdForRTGS
	 * @param reportingCode2ReqdForACH
	 * @param reportingCode2ReqdForRTGS
	 * @param mediatorServices
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private void validateReportingCodes(String[] reportingCodes, String paymentMethod, String bankGroupOid, 
			String reportingCode1ReqdForACH, String reportingCode1ReqdForRTGS, String reportingCode2ReqdForACH, String reportingCode2ReqdForRTGS, MediatorServices mediatorServices)
			throws RemoteException, AmsException
			{
		        boolean rc1ReqdForACH=false, rc1ReqdForRTGS=false, rc2ReqdForACH=false, rc2ReqdForRTGS = false;
				
				if(TradePortalConstants.PAYMENT_METHOD_ACH_GIRO.equals(paymentMethod)){
					if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForACH) && StringFunction.isBlank(reportingCodes[0])){
    					rc1ReqdForACH = true;
    				}
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForACH) && StringFunction.isBlank(reportingCodes[1])){
    					rc2ReqdForACH = true;
    				}
    				if(rc1ReqdForACH && rc2ReqdForACH){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_AND_2_REQUIRED , paymentMethod);
    				}else if(rc1ReqdForACH){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
    				}else if(rc2ReqdForACH){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
    				}
				}
				else if(TradePortalConstants.PAYMENT_METHOD_RTGS.equals(paymentMethod)){
					if(TradePortalConstants.INDICATOR_YES.equals(reportingCode1ReqdForRTGS) && StringFunction.isBlank(reportingCodes[0])){
    					rc1ReqdForRTGS = true;
    				}
    				if(TradePortalConstants.INDICATOR_YES.equals(reportingCode2ReqdForRTGS) && StringFunction.isBlank(reportingCodes[1])){
    					rc2ReqdForRTGS = true;
    				}
    				if(rc1ReqdForRTGS && rc2ReqdForRTGS){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_AND_2_REQUIRED , paymentMethod);
    				}else if(rc1ReqdForRTGS){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_1_REQUIRED , paymentMethod);
    				}else if(rc2ReqdForRTGS){
    					mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
    							TradePortalConstants.REPORTING_CODE_2_REQUIRED , paymentMethod);
    				}
				}
				

				int currentReportCodeIndex = 0;
				List<String> repCodeErrors = new ArrayList<String>();
				while (currentReportCodeIndex < TradePortalConstants.MAX_REP_CODE_IDX)
				{
					validateReportingCode(reportingCodes[currentReportCodeIndex], bankGroupOid, Integer.toString(++currentReportCodeIndex), mediatorServices, repCodeErrors);
				}
				if(repCodeErrors.size() == TradePortalConstants.MAX_REP_CODE_IDX)
    			{
    				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE_1_AND_2 , reportingCodes[0], reportingCodes[1]);
    			}else if(repCodeErrors.contains(TradePortalConstants.INVALID_REPORTING_CODE1)){
    				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE1 , reportingCodes[0]);
    			}else if(repCodeErrors.contains(TradePortalConstants.INVALID_REPORTING_CODE2)){
    				mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.INVALID_REPORTING_CODE2 , reportingCodes[1]);
    			}
			}


			/**
			 * This method validates payment's Reporting Code(s) entered on the screen
			 * @param inputDoc com.amsinc.ecsg.util - the input doc
			 * @param bankGroupOid String - Bank Group OID to check for codes
			 * @param isForVerify boolean - If true, gets code values from bean rather than inputDoc
			 * @param repCodeIndex String - Reporting Code Index.
			 * @return void -- nothing is return; in case of error, an error condition is raised via ErrorManager
			 * @exception   com.amsinc.ecsg.frame.AmsException, RemoteException
			 */
			private void validateReportingCode(String reportingCode, String bankGroupOid, String repCodeIndex, MediatorServices mediatorServices, List<String> repCodeErrors)
			throws RemoteException, AmsException
			{
				boolean repSelected = false;
				String whereSqlStr = " p_bank_group_oid = ?";
				List<Object> temp = new ArrayList<Object>();
				Object intOb = new Object();
				intOb = (bankGroupOid !=null)?Long.parseLong(bankGroupOid):bankGroupOid;
				temp.add(intOb);
				if (StringFunction.isNotBlank(reportingCode))
				{
					repSelected = true;
					whereSqlStr += " and code = ?";
					temp.add(reportingCode);
				}

				//obtain count.
				int reportCount = DatabaseQueryBean.getCount("code", "PAYMENT_REPORTING_CODE_" + repCodeIndex, whereSqlStr, false, temp);

				//if reporting code was selected on payment but was not found in the database, issue an error.
				if (repSelected && reportCount < 1)
				{
					if("1".equals(repCodeIndex)){
						repCodeErrors.add(TradePortalConstants.INVALID_REPORTING_CODE1);
					}else if("2".equals(repCodeIndex)){
						repCodeErrors.add(TradePortalConstants.INVALID_REPORTING_CODE2);
					}
					return;
				}
				
			}



            
}       

		