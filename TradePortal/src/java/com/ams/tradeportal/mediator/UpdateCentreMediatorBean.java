package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.RemoteException;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import com.ams.tradeportal.busobj.BankTransactionUpdate;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.ams.tradeportal.mediator.util.BankUpdateCentreProcessor;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;


/**
 * 
 *
 */
public class UpdateCentreMediatorBean extends MediatorBean {
private static final Logger LOG = LoggerFactory.getLogger(UpdateCentreMediatorBean.class);

	private java.lang.String completeInstrumentId = "";

	public DocumentHandler execute(DocumentHandler inputDoc,
			DocumentHandler outputDoc, MediatorServices mediatorServices)
					throws RemoteException, AmsException {

		String buttonPressed = null;

		try {
			buttonPressed = inputDoc.getAttribute("/Update/ButtonPressed");
			LOG.debug("The button pressed is " + buttonPressed);

			if ( TradePortalConstants.BUTTON_SAVETRANS.equals(buttonPressed)
					|| TradePortalConstants.BUTTON_SAVEANDCLOSE.equals(buttonPressed)
					|| TradePortalConstants.BUTTON_SAVECLOSETRANS.equals(buttonPressed) ) {
				outputDoc =  performSave(inputDoc, mediatorServices);				
			} else if (TradePortalConstants.BUTTON_APPLY_UPDATES.equals(buttonPressed)) {
				outputDoc = performApplyUpdates(inputDoc, mediatorServices);
			} else if ( TradePortalConstants.BUTTON_APPROVE.equals(buttonPressed) ) {
				outputDoc = performApprove(inputDoc, mediatorServices);
			} else if ( TradePortalConstants.BUTTON_BANK_SEND_FOR_REPAIR.equals(buttonPressed) ) {
				outputDoc = performSendForRepair(inputDoc, mediatorServices);
			} else if ( TradePortalConstants.BUTTON_BANK_EDIT.equals(buttonPressed) ) {
				outputDoc = performEdit(inputDoc, mediatorServices);
			} else if ( TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS.equals(buttonPressed) ) {				
				outputDoc = deleteAttachedDocuments(inputDoc, mediatorServices);
			} else if ( TradePortalConstants.BUTTON_ATTACH_DOCUMENTS.equals(buttonPressed) ) {				
				outputDoc = performSave(inputDoc, mediatorServices);
			} else{
				LOG.debug("UpdateCentreMediatorBean : No Action Performed");
			}			

		} catch (AmsException e ) {
			LOG.debug("UpdateCentreMediatorBean : Error While TRANSACTION STATUS UPDATE : ");			
			e.printStackTrace();			
		}	
		return outputDoc;
	}

	private DocumentHandler performSave(DocumentHandler inputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

		DocumentHandler outputDoc = new DocumentHandler();

		String bankTransUpdateOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_update_oid");
		String instrumentOID = inputDoc.getAttribute("/Instrument/instrument_oid");
		String bankUserOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_user_oid");
		String bankInstrumentID = inputDoc.getAttribute("/Instrument/bank_instrument_id");
		String loginUserOID = inputDoc.getAttribute("/LoginUser/user_oid");
		
		mediatorServices.getCSDB().setCSDBValue( TradePortalConstants.TRAN_ACTION,
				TradePortalConstants.BANK_ADMIN_ACTION_SAVE );

		Instrument instrument = (Instrument) mediatorServices.createServerEJB("Instrument",
				Long.parseLong(instrumentOID));

		BankTransactionUpdate bankTransUpdate = (BankTransactionUpdate) mediatorServices
				.createServerEJB("BankTransactionUpdate",
						Long.parseLong(bankTransUpdateOID));
		
		//get and set the complete instrument id.  This is used for display
		setCompleteInstrumentId(instrument.getAttribute("complete_instrument_id"));
		
		//validate user input on save
		if(validateUserInputOnSave(inputDoc, mediatorServices)){
			return outputDoc;
		}
		
		instrument.setAttribute("bank_instrument_id", bankInstrumentID);
		instrument.save(false);

		bankTransUpdate.populateFromXmlDoc(inputDoc);
		bankTransUpdate.setAttribute("bank_update_status", TradePortalConstants.BANK_TRANS_UPDATE_STATUS_IN_PROGRESS);
		bankTransUpdate.setAttribute("bank_user_oid", loginUserOID);		
		bankTransUpdate.save(false);
		
		//Check for Errors in the transaction.
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.UPDATE_SUCCESSFUL,
					getCompleteInstrumentId());
		}else{
			throw new AmsException("performSave: Error while saving");
		}
		
		return outputDoc;

	}

	private DocumentHandler performApplyUpdates(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		DocumentHandler outputDoc = new DocumentHandler();


		String bankTransUpdateOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_update_oid");			
		String bankUserOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_user_oid");
		String bankInstrumentID = inputDoc.getAttribute("/Instrument/bank_instrument_id");
		String instrumentOID = inputDoc.getAttribute("/Instrument/instrument_oid");
		String loginUserOID = inputDoc.getAttribute("/LoginUser/user_oid");
		
		mediatorServices.getCSDB().setCSDBValue(
				TradePortalConstants.TRAN_ACTION,
				TradePortalConstants.BANK_ADMIN_ACTION_APPLIED);

		BankTransactionUpdate bankTransUpdate = (BankTransactionUpdate) mediatorServices
				.createServerEJB("BankTransactionUpdate",
						Long.parseLong(bankTransUpdateOID));

		Instrument instrument = (Instrument) mediatorServices
				.createServerEJB("Instrument",
						Long.parseLong(instrumentOID));
		//get and set complete instrument id
		setCompleteInstrumentId(instrument.getAttribute("complete_instrument_id"));

		//Validate User input data.
		if(validateUserInput(inputDoc, mediatorServices)){
			return outputDoc;
		}

		instrument.setAttribute("bank_instrument_id", bankInstrumentID);
		instrument.save(false);


		bankTransUpdate.populateFromXmlDoc(inputDoc);
		bankTransUpdate.setAttribute("bank_update_status",
				TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPLIED);
		bankTransUpdate.setAttribute("bank_user_oid", loginUserOID);
		bankTransUpdate.save(false);
		
		//Check for Errors in the transaction.
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.APPLY_UPDATES,
					getCompleteInstrumentId());
		}else{
			throw new AmsException("performApplyUpdates: Error while applying updates");
		}

		return outputDoc;
	}

	private DocumentHandler performApprove(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		DocumentHandler outputDoc = new DocumentHandler();

		String bankTransUpdateOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_update_oid");
		String bankUserOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_user_oid");
		String bankInternalInfo = inputDoc.getAttribute("/BankTransactionUpdate/bank_internal_info");
		String loginUserOID = inputDoc.getAttribute("/LoginUser/user_oid");
		String instrumentOID = inputDoc.getAttribute("/Instrument/instrument_oid");
		String completeInstrumentID = inputDoc.getAttribute("/Instrument/complete_instrument_id");

		//validate if the same user trying to perform approve
		if (loginUserOID.equals(bankUserOID)) {

			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					TradePortalConstants.DIFFERENT_APPROVER_REQUIRED);
			return outputDoc;
		}
		
		//get and set complete instrument id
		setCompleteInstrumentId(completeInstrumentID);
		
		mediatorServices.getCSDB().setCSDBValue( TradePortalConstants.TRAN_ACTION,
				TradePortalConstants.BANK_ADMIN_ACTION_APPROVE );
		BankTransactionUpdate bankTransUpdate = (BankTransactionUpdate) mediatorServices
				.createServerEJB("BankTransactionUpdate",
						Long.parseLong(bankTransUpdateOID));

		bankTransUpdate.populateFromXmlDoc(inputDoc);
		bankTransUpdate.setAttribute("bank_update_status",TradePortalConstants.BANK_TRANS_UPDATE_STATUS_APPROVED);
		bankTransUpdate.setAttribute("bank_user_oid", loginUserOID);
        String transactionOid = bankTransUpdate.getAttribute("transaction_oid");
        String bankTransactionStatus = bankTransUpdate.getAttribute("bank_transaction_status");
		int success = bankTransUpdate.save(false);
		if ( success != -1 ) {
			BankUpdateCentreProcessor.createTplMessageToCust(bankTransUpdateOID, bankTransactionStatus, transactionOid, inputDoc, mediatorServices);
		}
		
		//Check for Errors in the transaction.
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {

             mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.APPROVED,
					getCompleteInstrumentId());
		}else{
			throw new AmsException("performApprove: Error while Approving");
		}
		
		return outputDoc;

	}

	private DocumentHandler performSendForRepair(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		DocumentHandler outputDoc = new DocumentHandler();

		String bankTransUpdateOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_update_oid");
		String completeInstrumentID = inputDoc.getAttribute("/Instrument/complete_instrument_id");
		String bankUserOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_user_oid");
		String loginUserOID = inputDoc.getAttribute("/LoginUser/user_oid");
		
		mediatorServices.getCSDB().setCSDBValue(
				TradePortalConstants.TRAN_ACTION,
				TradePortalConstants.BANK_ADMIN_ACTION_SENT_FOR_REPAIR);

		BankTransactionUpdate bankTransUpdate = (BankTransactionUpdate) mediatorServices
				.createServerEJB("BankTransactionUpdate",
						Long.parseLong(bankTransUpdateOID));

		//validate if the same user trying to perform sendtorepair
		if (loginUserOID.equals(bankUserOID)) {

			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					TradePortalConstants.DIFFERENT_APPROVER_REQUIRED);
			return outputDoc;
		}

		//get and set complete instrument id
		setCompleteInstrumentId(completeInstrumentID);
		
		bankTransUpdate.populateFromXmlDoc(inputDoc);
		bankTransUpdate.setAttribute("bank_update_status", TradePortalConstants.BANK_TRANS_UPDATE_STATUS_REPAIR);
		bankTransUpdate.setAttribute("bank_user_oid", loginUserOID);
		bankTransUpdate.save(false);
		
		//Check for Errors in the transaction.
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.SEND_FOR_REPAIR,
					getCompleteInstrumentId());
		}else{
			throw new AmsException("performSendForRepair: Error while Send for Repair");
		}		
		return outputDoc;
	}

	private DocumentHandler performEdit(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		DocumentHandler outputDoc = new DocumentHandler();

		String bankTransUpdateOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_update_oid");
		String bankUserOID = inputDoc.getAttribute("/BankTransactionUpdate/bank_user_oid");
		String loginUserOID = inputDoc.getAttribute("/LoginUser/user_oid");
		String completeInstrumentID = inputDoc.getAttribute("/Instrument/complete_instrument_id");

		//validate if the same user trying to perform Edit
		if (loginUserOID.equals(bankUserOID)) {
			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					TradePortalConstants.DIFFERENT_APPROVER_REQUIRED);
			return outputDoc;
		}
		//get and set complete instrument id
		setCompleteInstrumentId(completeInstrumentID);
		mediatorServices.getCSDB().setCSDBValue( TradePortalConstants.TRAN_ACTION,
				TradePortalConstants.BANK_ADMIN_ACTION_SAVE );
		BankTransactionUpdate bankTransUpdate = (BankTransactionUpdate) mediatorServices
				.createServerEJB("BankTransactionUpdate",
						Long.parseLong(bankTransUpdateOID));

		bankTransUpdate.populateFromXmlDoc(inputDoc);
		bankTransUpdate.setAttribute("bank_update_status", TradePortalConstants.BANK_TRANS_UPDATE_STATUS_IN_PROGRESS);
		bankTransUpdate.setAttribute("bank_user_oid", loginUserOID);
		bankTransUpdate.save(false);
		
		//Check for Errors in the transaction.
		if (this.isTransSuccess(mediatorServices.getErrorManager())) {
			/*mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.UPDATE_SUCCESSFUL,
					getCompleteInstrumentId());*/
		}else{
			throw new AmsException("performEdit: Error while Edit");
		}
		return outputDoc;

	}
	
	private boolean validateUserInputOnSave(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {
		
		boolean flag = false;

		String bankInstrumentID = inputDoc.getAttribute("/Instrument/bank_instrument_id");
		String bankRejectionReason = inputDoc.getAttribute("/BankTransactionUpdate/bank_rejection_reason_text");
		String bankInternalInfo = inputDoc.getAttribute("/BankTransactionUpdate/bank_internal_info");		
		
		//validate swift characters on bankInstrumentID
		InstrumentServices.checkForInvalidSwiftCharacters(bankInstrumentID, 
				mediatorServices.getErrorManager(), mediatorServices.getResourceManager().getText(
						"BankTransactionUpdate.Label.BankInstrumentID",
						TradePortalConstants.TEXT_BUNDLE));
		
		/*Validate length of rejection reason text/Bank Internal Information.
		 * Text length should not exceed 4000 bytes*/
		if(StringFunction.isNotBlank(bankRejectionReason)){
			if(bankRejectionReason.getBytes().length > 4000){
				mediatorServices.getErrorManager().issueError(
						getClass().getName(),
						TradePortalConstants.STRING_LENGTH_EXCEED,mediatorServices.getResourceManager().getText(
								"BankTransactionUpdate.BankRejectReason",
								TradePortalConstants.TEXT_BUNDLE));
			}
		}
		if(StringFunction.isNotBlank(bankInternalInfo)){
			if(bankInternalInfo.getBytes().length > 4000){
				mediatorServices.getErrorManager().issueError(
						getClass().getName(),
						TradePortalConstants.STRING_LENGTH_EXCEED,mediatorServices.getResourceManager().getText(
								"BankTransactionUpdate.BankInternalInformation",
								TradePortalConstants.TEXT_BUNDLE));
			}
		}
		if (!this.isTransSuccess(mediatorServices.getErrorManager())) {
			LOG.debug("UpdateCentreMediatorBean : Error While Save or SaveAndClose or ApplyUpdates");
			flag = true;
		}
		
		return flag;
		
	}

	private boolean validateUserInput(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		
		String bankTransactionStatus = inputDoc.getAttribute("/BankTransactionUpdate/bank_transaction_status");
		String bankRejectionReason = inputDoc.getAttribute("/BankTransactionUpdate/bank_rejection_reason_text");
		String bankInstrumentID = inputDoc.getAttribute("/Instrument/bank_instrument_id");
		String transactionType = inputDoc.getAttribute("/BankTransactionUpdate/transaction_type");
		
		// validate for bank Transaction Status
		if (StringFunction.isBlank(bankTransactionStatus)) {
			mediatorServices.getErrorManager().issueError(
					getClass().getName(),
					AmsConstants.REQUIRED_ATTRIBUTE,
					mediatorServices.getResourceManager().getText(
							"BankTransactionUpdate.BankTransactionStatus",
							TradePortalConstants.TEXT_BUNDLE));

		}
		/*validate if bankTransactionStatus = rejected by bank and rejection reason missing*/
		if ((!StringFunction.isBlank(bankTransactionStatus))
				&& bankTransactionStatus
				.equalsIgnoreCase(TradePortalConstants.BANK_TRANSACTION_STATUS_REJECTED_BY_BANK)) {
			if (StringFunction.isBlank(bankRejectionReason)) {
				mediatorServices.getErrorManager().issueError(
						getClass().getName(),
						AmsConstants.REQUIRED_ATTRIBUTE,
						mediatorServices.getResourceManager().getText(
								"BankTransactionUpdate.BankRejectReasonText",
								TradePortalConstants.TEXT_BUNDLE));

			}
		}

		// validate bankInstrumentId. For amend transactions mandatory check is not needed. 
		if ( !TransactionType.AMEND.equals(transactionType) ){
			if(StringFunction.isBlank(bankInstrumentID)) {
				if ((!StringFunction.isBlank(bankTransactionStatus))
						&& bankTransactionStatus
						.equalsIgnoreCase(TradePortalConstants.BANK_TRANSACTION_STATUS_PROCESSED_BY_BANK)) {
	
					mediatorServices.getErrorManager().issueError(
							getClass().getName(),
							AmsConstants.REQUIRED_ATTRIBUTE,
							mediatorServices.getResourceManager().getText(
									"BankTransactionUpdate.Label.BankInstrumentID",
									TradePortalConstants.TEXT_BUNDLE));
				}
			}
		}		
		return validateUserInputOnSave(inputDoc, mediatorServices);		
	}

	/**
	 * @return com.amsinc.ecsg.util.DocumentHandler
	 * @param inputDoc
	 *            com.amsinc.ecsg.util.DocumentHandler
	 * @param mediatorServices
	 *            com.amsinc.ecsg.frame.MediatorServices
	 */
	private DocumentHandler deleteAttachedDocuments(DocumentHandler inputDoc,
			MediatorServices mediatorServices) throws RemoteException,
			AmsException {

		String secretKeyString = mediatorServices.getCSDB().getCSDBValue(
				"SecretKey");
		byte[] keyBytes = EncryptDecrypt.base64StringToBytes(secretKeyString);
		javax.crypto.SecretKey secretKey = new SecretKeySpec(keyBytes,
				"DESede/CBC/PKCS5Padding");
		DocumentHandler outputDoc = new DocumentHandler();

		List<String> attachedDocuments = inputDoc
				.getAttributes("/AttachedDocument");
		String completeInstrumentID = inputDoc.getAttribute("/Instrument/complete_instrument_id");
		setCompleteInstrumentId(completeInstrumentID);

		for (String attDoc : attachedDocuments) {
			if (StringFunction.isNotBlank(attDoc)) {
				long docId = Long.parseLong(EncryptDecrypt
						.decryptStringUsingTripleDes(attDoc, secretKey));
				
				// Save the transaction oid in the mail message oid column. Also
				// set transaction_oid to 0 to further flag that it was a
				// deleted image for a transaction.
				DocumentImage documentImageInstance = (DocumentImage) mediatorServices
						.createServerEJB("DocumentImage", docId);
				documentImageInstance.setAttribute("mail_message_oid",
						documentImageInstance.getAttribute("transaction_oid"));
				documentImageInstance.setAttribute("transaction_oid", "0");
				int docSave = documentImageInstance.save();
				
				if( (docSave < 0) ){
					throw new AmsException("deleteAttachedDocuments: Error while Send for AttachedDocument");
				}else{   //Rel 9.3.5 10/05/2015  IR-44362				
					mediatorServices.getErrorManager().issueError(
							TradePortalConstants.ERR_CAT_1,
							TradePortalConstants.UPDATE_SUCCESSFUL,
							getCompleteInstrumentId());
				}
				
			}
		}
		return outputDoc;
	}

	/**
	 * Setter for complete instrument id attribute
	 *
	 * @param newCompleteInstrumentId
	 *            java.lang.String
	 */
	private void setCompleteInstrumentId(
			java.lang.String newCompleteInstrumentId) {
		completeInstrumentId = newCompleteInstrumentId;
	}

	/**
	 * Getter for the complete instrument id attribute.
	 *
	 * @return java.lang.String
	 */
	private java.lang.String getCompleteInstrumentId() {
		return completeInstrumentId;
	}
}