package com.ams.tradeportal.mediator;

import javax.ejb.*;
import java.rmi.*;

import com.amsinc.ecsg.frame.*;/*
 *
 *     Copyright  �                          
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

public interface UpdateCentreMediatorHome extends javax.ejb.EJBHome {

	public UpdateCentreMediator create() throws CreateException,
			RemoteException;

}