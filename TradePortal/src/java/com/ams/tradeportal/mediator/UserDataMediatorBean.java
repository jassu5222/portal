package com.ams.tradeportal.mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.TradePortalConstants;

/*
 * Manages the SSO Registration / Unregistration transactions.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class UserDataMediatorBean extends MediatorBean
{
private static final Logger LOG = LoggerFactory.getLogger(UserDataMediatorBean.class);
   private static final String ATTR_SHOWTIPS = "showtips";
private static final String ATTR_DATEPATTERN = "datepattern";

/*
	* Called by the processRequest() method of the Ancestor class to process the designated
	* transaction.  The input parameters for the transaction are received in an XML document
	* (inputDoc).  The execute() method is responsible for calling one or more business objects
	* to process the transaction and populating the outputDoc XML document with the results
	* of the transaction.
	*/
   public DocumentHandler execute(DocumentHandler inputDoc,
		   DocumentHandler outputDoc, MediatorServices mediatorServices)
   throws RemoteException, AmsException
   {
        // Get necessary data from the input XML
	    long   userOID                     = inputDoc.getAttributeLong("/userOID");
	    String buttonPressed 			   = inputDoc.getAttribute("/buttonName");
	    
	    if (TradePortalConstants.BUTTON_SAVE_FORMATTING.equals(buttonPressed)){
	    	saveUIPreferences(inputDoc, mediatorServices, userOID);
	    } else {
	    	saveRegisteredSSOId(inputDoc, mediatorServices, userOID);
	    }
	    
	    return outputDoc;
   }

	private void saveRegisteredSSOId(DocumentHandler inputDoc,
			MediatorServices mediatorServices, long userOID) throws AmsException,
			RemoteException {
		String newRegisteredSSOId          = inputDoc.getAttribute("/newRegisteredSSOId");
		String changingRegisteredSSOIdFlag = inputDoc.getAttribute("/changingRegisteredSSOIdFlag");
	
		// Determine if the user is completing SSO registration
		boolean changingRegisteredSSOId    = "true".equals(changingRegisteredSSOIdFlag);
	
		//cquinton 1/23/2012 Rel 7.1 cnum012365273 start
		if ( changingRegisteredSSOId) {
			User user = (User) mediatorServices.createServerEJB("User", userOID);
			// if newRegisteredSSOId is blank, the user will be unregistered
			user.setAttribute("registered_sso_id", newRegisteredSSOId);
	
			user.setAttribute("invalid_login_attempt_count", "0");
	
		    // Save the user
			// instead of doing standard validation, use alternate validation
			// specific to changingRegisteredSSOId
			user.validateChangingRegisteredSSOId();
		    user.save(false);
		}
		//cquinton 1/23/2012 Rel 7.1 cnum012365273 end
	}

	private void saveUIPreferences(DocumentHandler inputDoc, MediatorServices mediatorServices, long userOID) 
	throws AmsException,RemoteException {
		
	    String datepattern 			       = inputDoc.getAttribute("/datepattern");
	    String showToolTipInd 			   = inputDoc.getAttribute("/showtips");
	    
		User user = (User) mediatorServices.createServerEJB("User", userOID);
	   
	   
	   String prevDatePattern = user.getAttribute(ATTR_DATEPATTERN);
	   String prevShowTipsInd = user.getAttribute(ATTR_SHOWTIPS);
	   boolean userUIPrefModified = false;
	   if(different(prevDatePattern, datepattern)) {
		user.setAttribute(ATTR_DATEPATTERN, datepattern);
		userUIPrefModified=true;
	   }
		
	   if(different(prevShowTipsInd, showToolTipInd)) {
		user.setAttribute(ATTR_SHOWTIPS, showToolTipInd);
		userUIPrefModified=true;
	   }
	   
	   if(userUIPrefModified == true) {
		user.save();
		if(user.getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY)
		{
			mediatorServices.getErrorManager().issueError(
		             TradePortalConstants.ERR_CAT_1, TradePortalConstants.UPDATE_SUCCESSFUL,
		             mediatorServices.getResourceManager().getText("UserDetail.UserPreferences", TradePortalConstants.TEXT_BUNDLE));
		}
	   }
	}
   
   private static final boolean different(String a, String b) {
   	if (a == null && b == null)
   		return false;
   	
   	return  
   			(a == null && b != null) || 
   			(a != null && b == null) || 
   			(! a.equals(b));
   }
}