
	package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

	/**
	 *    rbhaduri - 3rd March, 06 - CR239
	 *
	 *     Copyright  � 2006                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	 */

	import com.amsinc.ecsg.web.*;
	import javax.servlet.http.*;
	import java.util.*;

	import com.amsinc.ecsg.frame.ErrorManager;
	import com.amsinc.ecsg.util.*;
	import com.ams.tradeportal.common.*;

	public class ARMatchingRulePostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(ARMatchingRulePostEdit.class);



	 public void act(AmsServletInvocation reqInfo, 
					BeanManager beanMgr, 
					FormManager formMgr, 
					ResourceManager resMgr, 
					String serverLocation, 
					HttpServletRequest request, 
					HttpServletResponse response, 
					Hashtable inputParmMap,
					DocumentHandler inputDoc)
				throws com.amsinc.ecsg.frame.AmsException
	 {
	   /**
	    * In the corresponding pre-mediator action, decimal values are converted
	    * from their locale specific formats into a non-localized format.  Because
	    * the JSP expects to see in the XML the values that it sent, the original
	    * (formatted for the locale) values must be placed back into their original
	    * locations.
	    */
		 
		 // set the Mediator output to /In fragment if no error.
		 // The transaction page expects the results to be returned in /In fragment.
	     DocumentHandler outputDocument = formMgr.getFromDocCache(reqInfo.getDocCacheName ());
	     String errorSeverity =
	        outputDocument.getAttribute ("/Error/maxerrorseverity");
	     if (errorSeverity != null  && Integer.parseInt (errorSeverity) < ErrorManager.ERROR_SEVERITY)
	     {
	           inputDoc.setAttribute("/In/BuyerSearchInfo/BuyerIdAndName", inputDoc.getAttribute("/In/ArMatchingRule/buyer_id")+ "/"+ inputDoc.getAttribute("/In/ArMatchingRule/buyer_name"));
	     }
	   }
	}