package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class EditAllFXRatePostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(EditAllFXRatePostEdit.class);



 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */

      Vector fxRateList = inputDoc.getFragments("/In/FXRate");

      int numberOfFxRates = fxRateList.size();

         // For each FX rate being updated, instantiate the appropriate business object and
         // save all attributes
         for (int i = 0; i < numberOfFxRates; i++)
         {
             inputDoc.setAttribute("/In/FXRate("+i+")/rate", inputDoc.getAttribute("/In/FXRate("+i+")/userEnteredRate"));
             //CM IAZ 10/14/08 Begin
             inputDoc.setAttribute("/In/FXRate("+i+")/buy_rate", inputDoc.getAttribute("/In/FXRate("+i+")/userEnteredBuyRate"));
             inputDoc.setAttribute("/In/FXRate("+i+")/sell_rate", inputDoc.getAttribute("/In/FXRate("+i+")/userEnteredSellRate"));
             //CM IAZ 10/14/08 End

         }
 }


}