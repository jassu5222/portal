package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class FTDPPostEdit extends GenericTransactionPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FTDPPostEdit.class);



 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
     saveMyRecentInstrumtntInSession(reqInfo, formMgr, inputDoc, resMgr, beanMgr, request);
 }

 
}