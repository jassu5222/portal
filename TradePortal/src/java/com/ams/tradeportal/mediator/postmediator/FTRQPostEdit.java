package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class FTRQPostEdit extends GenericTransactionPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FTRQPostEdit.class);



 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
     doDecimalPostMediatorAction(inputDoc, resMgr, "amount");
     doDecimalPostMediatorAction(inputDoc, resMgr, "fec_amount");
     doDecimalPostMediatorAction(inputDoc, resMgr, "maturity_fec_amount");
     doDecimalPostMediatorAction(inputDoc, resMgr, "fec_rate");
     doDecimalPostMediatorAction(inputDoc, resMgr, "maturity_fec_rate");
     saveMyRecentInstrumtntInSession(reqInfo, formMgr, inputDoc, resMgr, beanMgr, request);
 }

 
}