package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class FXRatePostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FXRatePostEdit.class);



 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */
      if(inputDoc.getAttribute("/In/FXRate/userEnteredRate") != null)
       {
         inputDoc.setAttribute("/In/FXRate/rate", inputDoc.getAttribute("/In/FXRate/userEnteredRate"));
       }
      //CM IAZ 10/14/08 Begin

      if(inputDoc.getAttribute("/In/FXRate/userEnteredBuyRate") != null)
       {
         inputDoc.setAttribute("/In/FXRate/buy_rate",
         inputDoc.getAttribute("/In/FXRate/userEnteredBuyRate"));
	   }
      if(inputDoc.getAttribute("/In/FXRate/userEnteredSellRate") != null)
       {
         inputDoc.setAttribute("/In/FXRate/sell_rate",
         inputDoc.getAttribute("/In/FXRate/userEnteredSellRate"));
	   }
	   //CM IAZ 10/14/08 End


 }


}