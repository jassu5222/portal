package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class LCCreationRulePostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(LCCreationRulePostEdit.class);



 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */

     if(inputDoc.getAttribute("/In/LCCreationRule/userEnteredmaximum_amount") != null)
       {
         inputDoc.setAttribute("/In/LCCreationRule/maximum_amount", inputDoc.getAttribute("/In/LCCreationRule/userEnteredmaximum_amount"));
       }
 }



}