package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

public class ThresholdGroupPostEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(ThresholdGroupPostEdit.class);



 public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
   /**
    * In the corresponding pre-mediator action, decimal values are converted
    * from their locale specific formats into a non-localized format.  Because
    * the JSP expects to see in the XML the values that it sent, the original
    * (formatted for the locale) values must be placed back into their original
    * locations.
    */
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_amend_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_discr_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_amend_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_discr_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_amend_transfer_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_amend_transfer_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_issue_assign_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_issue_assign_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_discr_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/guarantee_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredguarantee_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/guarantee_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredguarantee_amend_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/airwaybill_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredairwaybill_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/ship_guarantee_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredship_guarantee_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_collection_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_collection_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_collection_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_collection_amend_thold"));
       //Vasavi CR 524 03/31/2010 Begin
       inputDoc.setAttribute("/In/ThresholdGroup/new_export_coll_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnterednew_export_coll_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/new_export_coll_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnterednew_export_coll_amend_thold"));
       //Vasavi CR 524 03/31/2010 End
       inputDoc.setAttribute("/In/ThresholdGroup/inc_standby_LC_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredinc_standby_LC_discr_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/funds_transfer_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredfunds_transfer_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/loan_request_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredloan_request_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_amend_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_discr_thold"));
       //Krishna CR 375-D ATP 07/19/2007 Begin
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_issue_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_amend_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_amend_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_discr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_discr_thold"));
       //Krishna CR 375-D ATP 07/19/2007 End
       //Chandrakanth CR 451 10/21/2008 Begin
	          inputDoc.setAttribute("/In/ThresholdGroup/transfer_btw_accts_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredtransfer_btw_accts_issue_thold"));
	          inputDoc.setAttribute("/In/ThresholdGroup/domestic_payment_issue_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEntereddomestic_payment_issue_thold"));
	          inputDoc.setAttribute("/In/ThresholdGroup/ar_dispute_invoice_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_dispute_invoice_thold"));
     //Chandrakanth CR 451 10/21/2008 End
	//Pratiksha CR-434 ARM 10/01/2008 Begin
	inputDoc.setAttribute("/In/ThresholdGroup/ar_match_response_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_match_response_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/ar_approve_discount_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_approve_discount_thold"));
       inputDoc.setAttribute("/In/ThresholdGroup/ar_close_invoice_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_close_invoice_thold"));
	inputDoc.setAttribute("/In/ThresholdGroup/ar_finance_invoice_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_finance_invoice_thold"));
	//Pratiksha CR-434 ARM 10/01/2008 End
	
	//SSikhakolli - Rel-9.4 CR-818 IR# T36000042464 - Begin
	inputDoc.setAttribute("/In/ThresholdGroup/imp_col_set_instr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimp_col_set_instr_thold"));
	inputDoc.setAttribute("/In/ThresholdGroup/loan_request_set_instr_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredloan_request_set_instr_thold"));
	inputDoc.setAttribute("/In/ThresholdGroup/imp_col_set_instr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimp_col_set_instr_dlimit"));
	inputDoc.setAttribute("/In/ThresholdGroup/loan_request_set_instr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredloan_request_set_instr_dlimit"));
	//SSikhakolli - Rel-9.4 CR-818 IR# T36000042464 - End
	
	inputDoc.setAttribute("/In/ThresholdGroup/direct_debit_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEntereddirect_debit_thold")); //Pratiksha CR-509 DDI 12/09/2009 
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_discr_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/import_LC_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredimport_LC_amend_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_amend_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/standby_LC_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredstandby_LC_discr_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_amend_transfer_dlim", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_amend_transfer_dlim"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_issue_assign_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_issue_assign_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_LC_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_LC_discr_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/guarantee_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredguarantee_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/guarantee_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredguarantee_amend_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/airwaybill_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredairwaybill_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/ship_guarantee_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredship_guarantee_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_collection_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_collection_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/export_collection_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredexport_collection_amend_dlimit"));
       //Vasavi CR 524 03/31/2010 Begin
       inputDoc.setAttribute("/In/ThresholdGroup/new_export_coll_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnterednew_export_coll_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/new_export_coll_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnterednew_export_coll_amend_dlimit"));
       //Vasavi CR 524 03/31/2010 End
       inputDoc.setAttribute("/In/ThresholdGroup/inc_standby_LC_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredinc_standby_LC_discr_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/funds_transfer_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredfunds_transfer_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/loan_request_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredloan_request_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_amend_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/request_advise_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredrequest_advise_discr_dlimit"));
       //Krishna CR 375-D ATP 07/19/2007 Begin
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_issue_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_amend_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_amend_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/approval_to_pay_discr_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredapproval_to_pay_discr_dlimit"));
       //Krishna CR 375-D ATP 07/19/2007 End
	//Pratiksha CR-434 ARM 10/01/2008 Begin
	inputDoc.setAttribute("/In/ThresholdGroup/ar_match_response_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_match_response_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/ar_approve_discount_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_approve_discount_dlimit"));
       inputDoc.setAttribute("/In/ThresholdGroup/ar_close_invoice_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_close_invoice_dlimit"));
	inputDoc.setAttribute("/In/ThresholdGroup/ar_finance_invoice_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_finance_invoice_dlimit"));
       //Pratiksha CR-434 ARM 10/01/2008 End
       //Chandrakanth CR 451 10/21/2008 Begin
	       inputDoc.setAttribute("/In/ThresholdGroup/transfer_btw_accts_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredtransfer_btw_accts_issue_dlimit"));
	       inputDoc.setAttribute("/In/ThresholdGroup/domestic_payment_issue_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEntereddomestic_payment_issue_dlimit"));
	       inputDoc.setAttribute("/In/ThresholdGroup/ar_dispute_invoice_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredar_dispute_invoice_dlimit"));
	   //Chandrakanth CR 451 10/21/2008 End
        inputDoc.setAttribute("/In/ThresholdGroup/direct_debit_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEntereddirect_debit_dlimit")); //Pratiksha CR-509 DDI 12/09/2009
        
        inputDoc.setAttribute("/In/ThresholdGroup/upload_rec_inv_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredupload_rec_inv_thold"));
        inputDoc.setAttribute("/In/ThresholdGroup/upload_pay_inv_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredupload_pay_inv_thold"));
        inputDoc.setAttribute("/In/ThresholdGroup/pyb_mgm_inv_thold", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredpyb_mgm_inv_thold"));        
        inputDoc.setAttribute("/In/ThresholdGroup/upload_rec_inv_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredupload_rec_inv_dlimit"));
        inputDoc.setAttribute("/In/ThresholdGroup/upload_pay_inv_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredupload_pay_inv_dlimit"));
        inputDoc.setAttribute("/In/ThresholdGroup/pyb_mgm_inv_dlimit", inputDoc.getAttribute("/In/ThresholdGroup/userEnteredpyb_mgm_inv_dlimit"));
  }





}