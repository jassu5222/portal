package com.ams.tradeportal.mediator.postmediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ErrorManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;
import com.amsinc.ecsg.web.WebAction;

public class UserDataPostMediator implements WebAction {
private static final Logger LOG = LoggerFactory.getLogger(UserDataPostMediator.class);

	@Override
	public void act(AmsServletInvocation reqInfo, BeanManager beanMgr,
			FormManager formMgr, ResourceManager resMgr, String serverLocation,
			HttpServletRequest request, HttpServletResponse response,
			Hashtable inputParmMap, DocumentHandler inputDoc)
			throws AmsException {
		
		SessionWebBean sessionWebBean = (SessionWebBean)request.getSession().getAttribute("userSession");
		
	   	if(sessionWebBean != null) {
	   		int errorMaxSeverity = inputDoc.getAttributeInt("/Error/maxerrorseverity");
	   		if (errorMaxSeverity < ErrorManager.ERROR_SEVERITY) {
		   		String showToolTipInd = inputDoc.getAttribute("/In/showtips");
		   		sessionWebBean.setShowToolTips(showToolTipInd == null || "Y".equals(showToolTipInd));
		   		
		   		String datepattern = inputDoc.getAttribute("/In/datepattern");
		   		if (datepattern != null)
		   			sessionWebBean.setDatePattern(datepattern);
	   		}
	   	}

	}

}
