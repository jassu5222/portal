package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.HSMEncryptDecrypt;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class AdminUserPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(AdminUserPreEdit.class);
	/**
	 * AdminUserPreEdit constructor comment.
	 */
	public AdminUserPreEdit() {
		super();
	}
	/**
	 * Extracts the OwnerOrgAndLevel value and parses it into two
	 * values: an owner_org_oid and an ownership_level.  These are
	 * placed in the inputDoc under /In/User.  The OwnerOrgAndLevel
	 * is assumed to be of this form: "org/level".
	 */
	public void act(AmsServletInvocation reqInfo, 
			BeanManager beanMgr, 
			FormManager formMgr, 
			ResourceManager resMgr, 
			String serverLocation, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

		String org = "";
		String level = "";
		String orgAndLevel = "";
		String newPassword = "";

		// get the input document 
		if (inputDoc == null) {
			LOG.info("Input document to AdminUserPreEdit is null");
			return;
		}

		orgAndLevel = inputDoc.getAttribute("/In/User/OwnerOrgAndLevel");

		int slashAt = orgAndLevel.indexOf("/");
		org = orgAndLevel.substring(0,slashAt);
		level = orgAndLevel.substring(slashAt+1, orgAndLevel.length());

		inputDoc.setAttribute("/In/User/owner_org_oid", org);
		inputDoc.setAttribute("/In/User/ownership_level", level);


		// [START] CR-482
		if(HSMEncryptDecrypt.isInstanceHSMEnabled())
		{
			// [START] PIUK012685963 - No need to decrypt when protectedNewPassword is blank or call the HSM twice to get the same value
			if(!StringFunction.isBlank(inputDoc.getAttribute("/In/User/protectedNewPassword")))
			{
				try
				{
					String decryptedPassword = HSMEncryptDecrypt.decryptForTransport(inputDoc.getAttribute("/In/User/protectedNewPassword"));
					// T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
					String decryptedRetyped = HSMEncryptDecrypt.decryptForTransport(inputDoc.getAttribute("/In/User/encryptedRetypedNewPassword"));
					
					// [START] CR-543
					inputDoc.setAttribute("/In/User/newPassword", decryptedPassword);
					// T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances--
					inputDoc.setAttribute("/In/User/retypePassword", decryptedRetyped);
					
				} catch (Exception e) {
					System.err.println("[AdminUserPreEdit] Caught exception while trying to decrypt protected password values: "+e.getMessage());
					// [END] CR-543
				}
			} 
		}
		// [END] CR-482
		
		// If passwords are not used for this user, there will be no password data 
		if(inputDoc.getAttribute("/In/User/newPassword") != null)        
			// If password is being reset, set the password change date to null
			if((!inputDoc.getAttribute("/In/User/newPassword").equals("")) ||
					(!inputDoc.getAttribute("/In/User/retypePassword").equals("")) )
			{
				inputDoc.setAttribute("/In/User/password_change_date","");
			}
                
                
		if (inputDoc.getAttribute("/In/User/allow_sso_password_ind") == null)
		{
		     inputDoc.setAttribute("/In/User/allow_sso_password_ind", TradePortalConstants.INDICATOR_NO);
		}
                
	}
}
