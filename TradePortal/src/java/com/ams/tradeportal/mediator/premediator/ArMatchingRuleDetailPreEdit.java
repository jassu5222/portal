package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class ArMatchingRuleDetailPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(ArMatchingRuleDetailPreEdit.class);
   /**
    * CorporateOrgPreEdit constructor comment.
    */
   public ArMatchingRuleDetailPreEdit()
   {
      super();
   }

   /**
    *
    */
   public void act(AmsServletInvocation reqInfo,
                   BeanManager beanMgr,
                   FormManager formMgr,
                   ResourceManager resMgr,
                   String serverLocation,
                   HttpServletRequest request,
                   HttpServletResponse response,
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
      // Retrieve the input document
      if (inputDoc == null)
      {
         LOG.info("Input document to ArMatchingRuleDetailPreEdit is null");

         return;
      }

    //PPRAKASH IR RIUJ020383070 Add Begin
 	 inputDoc = upperCaseConversion(inputDoc, "/In/ArMatchingRule/ArBuyerIdAliasList", "/buyer_id_alias","/ar_buyer_id_alias_oid")	 ;
 	 inputDoc = upperCaseConversion(inputDoc, "/In/ArMatchingRule/ArBuyerNameAliasList", "/buyer_name_alias","/ar_buyer_name_alias_oid")	 ;
 	 inputDoc = upperCaseConversion(inputDoc, "/In/ArMatchingRule/buyer_name");
 	 inputDoc = upperCaseConversion(inputDoc, "/In/ArMatchingRule/buyer_id");
 	 //PPRAKASH IR RIUJ020383070 Add End

      // Determine whether buyer_name are deleted or not entered from
	Vector buyerNameVector = inputDoc.getFragments("/In/ArMatchingRule/ArBuyerNameAliasList");
	int numItems = buyerNameVector.size();
	int iLoop;

	for (iLoop=0; iLoop<numItems; iLoop++)
	{
		DocumentHandler buyerNameDoc = (DocumentHandler) buyerNameVector.elementAt(iLoop);

		String buyerNameOid = buyerNameDoc.getAttribute("/ar_buyer_name_alias_oid");
		String buyerNameName = buyerNameDoc.getAttribute("/buyer_name_alias");

		if (StringFunction.isBlank(buyerNameName)) {
		    if (StringFunction.isBlank(buyerNameOid)) {
			// Oid and name are blank: remove this completely blank component
			// from the input doc.

			inputDoc.removeComponent("/In/ArMatchingRule/ArBuyerNameAliasList("+iLoop+")");

		    } else {
			// Oid is not blank but buyer_name_alias is.  The user wants to delete this
			// account.  Remove it from the input doc (so it doesn't get save and generate
			// an error), then add the oid to a new section for processing in the
			// mediator.

			DocumentHandler deleteDoc = new DocumentHandler();
			//Vshah - 11/19/08 - IR#PTUI110639634 - ADD Begin
			deleteDoc.setAttribute("/Name", "ArBuyerNameAlias");
			//Vshah - 11/19/08 - IR#PTUI110639634 - ADD End
			deleteDoc.setAttribute("/Oid",buyerNameOid);

			inputDoc.removeComponent("/In/ArMatchingRule/ArBuyerNameAliasList("+iLoop+")");

			inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
		    }
		}
	}

	buyerNameVector = inputDoc.getFragments("/In/ArMatchingRule/ArBuyerNameAliasList");
	numItems = buyerNameVector.size();

	//	 Determine whether buyer_id are deleted or not entered from
	Vector buyerIdVector = inputDoc.getFragments("/In/ArMatchingRule/ArBuyerIdAliasList");
	numItems = buyerIdVector.size();


	for (iLoop=0; iLoop<numItems; iLoop++)
	{
		DocumentHandler buyerIdDoc = (DocumentHandler) buyerIdVector.elementAt(iLoop);

		String buyerIdOid = buyerIdDoc.getAttribute("/ar_buyer_id_alias_oid");
		String buyerId = buyerIdDoc.getAttribute("/buyer_id_alias");

		if (StringFunction.isBlank(buyerId)) {
		    if (StringFunction.isBlank(buyerIdOid)) {
			// Oid and buyerId are blank: remove this completely blank component
			// from the input doc.

			inputDoc.removeComponent("/In/ArMatchingRule/ArBuyerIdAliasList("+iLoop+")");

		    } else {
			// Oid is not blank but buyer_id_alias is.  The user wants to delete this
			// account.  Remove it from the input doc (so it doesn't get save and generate
			// an error), then add the oid to a new section for processing in the
			// mediator.

			DocumentHandler deleteDoc = new DocumentHandler();
			//Vshah - 11/19/08 - IR#PTUI110639634 - ADD Begin
			deleteDoc.setAttribute("/Name", "ArBuyerIdAlias");
			//Vshah - 11/19/08 - IR#PTUI110639634 - ADD End
			deleteDoc.setAttribute("/Oid", buyerIdOid);

			inputDoc.removeComponent("/In/ArMatchingRule/ArBuyerIdAliasList("+iLoop+")");

			inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
		    }
		}
	}
	// Srini_D CR-710 11/12/2011 Rel8.0 Start
   //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 added condition to set the Indicator attributes - Start 
      if (inputDoc.getAttribute("/In/ArMatchingRule/individual_invoice_allowed") == null)
        {
                inputDoc.setAttribute("/In/ArMatchingRule/individual_invoice_allowed",
                                TradePortalConstants.INDICATOR_NO);
        }
        		
	 if (inputDoc.getAttribute("/In/ArMatchingRule/partial_pay_to_portal_indicator") == null)
     {
        inputDoc.setAttribute("/In/ArMatchingRule/partial_pay_to_portal_indicator",
                              TradePortalConstants.INDICATOR_NO);
     }

	 if (inputDoc.getAttribute("/In/ArMatchingRule/tolerance_amount_indicator") == null)
     {
        inputDoc.setAttribute("/In/ArMatchingRule/tolerance_amount_indicator",
                              TradePortalConstants.INDICATOR_NO);
     }

	 if (inputDoc.getAttribute("/In/ArMatchingRule/no_tolerance_indicator") == null)
     {
        inputDoc.setAttribute("/In/ArMatchingRule/no_tolerance_indicator",
                              TradePortalConstants.INDICATOR_NO);
     }

	 if (inputDoc.getAttribute("/In/ArMatchingRule/tolerance_percent_indicator") == null)
     {
        inputDoc.setAttribute("/In/ArMatchingRule/tolerance_percent_indicator",
                              TradePortalConstants.INDICATOR_NO);
     }	
     //	Narayan IR - ACUM031962698 Begin - added condition to set the Indicator attributes
     if (inputDoc.getAttribute("/In/ArMatchingRule/receivable_invoice") == null)
      {
           inputDoc.setAttribute("/In/ArMatchingRule/receivable_invoice",
                                 TradePortalConstants.INDICATOR_NO);
           
      }
   	 if (inputDoc.getAttribute("/In/ArMatchingRule/credit_note") == null)
      {
           inputDoc.setAttribute("/In/ArMatchingRule/credit_note",
                                 TradePortalConstants.INDICATOR_NO);

      }   
   	 //Narayan IR - ACUM031962698 End      	 
  // }
      
        
     //RKAZI IR MMUM020953219 Rel 8.0 02/22/2012 added condition to set the Indicator attributes - End
	 if(inputDoc.getAttribute("/In/ArMatchingRule/days_finance_of_payment")!=null){
		 
		 if (inputDoc.getAttribute("/In/ArMatchingRule/invoice_value_finance") != null){
			 
			 String invfin=inputDoc.getAttribute("/In/ArMatchingRule/invoice_value_finance");
			 if(invfin!=null && invfin.trim().equals(""))
			 	inputDoc.setAttribute("/In/ArMatchingRule/invoice_value_finance","100" );
	  }
	 }	 
	
	 Vector marginRuleVector = inputDoc.getFragments("/In/ArMatchingRule/TradePartnerMarginRulesList");
		numItems = marginRuleVector.size();
		for (iLoop=0; iLoop<numItems; iLoop++)
		{
			DocumentHandler marginRuleDoc = (DocumentHandler) marginRuleVector.elementAt(iLoop);

			String tradeMatchingOid = marginRuleDoc.getAttribute("/trade_margin_rule_oid");
			String currencyCode = marginRuleDoc.getAttribute("/currency_code");
			String instrumentType = marginRuleDoc.getAttribute("/margin_instrument_type");
			String thresholdAmount = marginRuleDoc.getAttribute("/threshold_amount");
			String rateType = marginRuleDoc.getAttribute("/rate_type");
			String margin = marginRuleDoc.getAttribute("/margin");
			if(StringFunction.isNotBlank(thresholdAmount)){
    			doDecimalPreMediatorAction(inputDoc, resMgr, "/In/ArMatchingRule/TradePartnerMarginRulesList(" + iLoop + ")/threshold_amount");
    		}
			
			if (StringFunction.isBlank(currencyCode)&& StringFunction.isBlank(instrumentType) && StringFunction.isBlank(thresholdAmount) && StringFunction.isBlank(rateType) && StringFunction.isBlank(margin)) {
			    	
					if(!StringFunction.isBlank(tradeMatchingOid))
			    	{
					// Oid is not blank but buyer_id_alias is.  The user wants to delete this
					// account.  Remove it from the input doc (so it doesn't get save and generate
					// an error), then add the oid to a new section for processing in the
					// mediator.
					
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "TradePartnerMarginRules");
					deleteDoc.setAttribute("/Oid", tradeMatchingOid);

					inputDoc.removeComponent("/In/ArMatchingRule/TradePartnerMarginRulesList("+iLoop+")");

					inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
				    }
			    	else{
			    		inputDoc.removeComponent("/In/ArMatchingRule/TradePartnerMarginRulesList("+iLoop+")");
			    	}
			}
		}	
		// Narayan CR-913 Rel9.0 01/23/2014 start		
		Vector<DocumentHandler> payableInvPayInstVector = inputDoc.getFragments("/In/ArMatchingRule/PayableInvPayInstList");
		int payInvCount = 0;
		for (DocumentHandler doc : payableInvPayInstVector) {
			String payableInvPayInstOid = doc.getAttribute("/payable_inv_pay_inst_oid");
			String currencyCode = doc.getAttribute("/currency_code");
			if (StringFunction.isBlank(currencyCode)) {
				if (StringFunction.isNotBlank(payableInvPayInstOid)) {
					// Oid is not blank but Payable Invoice Instruction currency is. The user wants to delete this
					// Payable Invoice instruction. Remove it from the input doc (so it doesn't get save and generate
					// an error), then add the oid to a new section for processing in the mediator.

					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "PayableInvPayInst");
					deleteDoc.setAttribute("/Oid", payableInvPayInstOid);

					inputDoc.removeComponent("/In/ArMatchingRule/PayableInvPayInstList(" + payInvCount + ")");
					inputDoc.addComponent("/In/ComponentToDelete(" + payInvCount + ")", deleteDoc);
				} else {
					inputDoc.removeComponent("/In/ArMatchingRule/PayableInvPayInstList(" + payInvCount + ")");
				}
			}else if (doc.getAttribute("/inv_pay_instruction_ind") == null) {
			         inputDoc.setAttribute("/In/ArMatchingRule/PayableInvPayInstList(" + payInvCount + ")/inv_pay_instruction_ind", 
			        		 TradePortalConstants.INDICATOR_NO);
			}
			payInvCount++;
		}	
		// Narayan CR-913 Rel9.0 01/23/2014 End
						
		// if user unselect receivable_invoice or credit_note check box  and don't select its rights option.
		// then it should throw error. this condition is present in bean. here making as blank bucause in bean it takes value from getAttribute and 
		//it is possible that previouly authorize option has value.

		if(TradePortalConstants.TPR_AUTHORIZATION.equals(inputDoc.getAttribute("/In/ArMatchingRule/part_to_validate"))){
			if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/ArMatchingRule/receivable_invoice")) &&
					              StringFunction.isBlank(inputDoc.getAttribute("/In/ArMatchingRule/receivable_authorise"))){
				inputDoc.setAttribute("/In/ArMatchingRule/receivable_authorise", "");
			}
			if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/ArMatchingRule/credit_note")) &&
		              StringFunction.isBlank(inputDoc.getAttribute("/In/ArMatchingRule/credit_authorise"))){
	         inputDoc.setAttribute("/In/ArMatchingRule/credit_authorise", "");
           }
		}
		
   }


   private DocumentHandler upperCaseConversion(DocumentHandler xml,String path,String tag,String extratag){

	   //for(int i = 0;i<path.length;i++){
		   Vector fragmentVector = xml.getFragments(path);
		   int numItems = fragmentVector.size();

		   for (int iLoop=0; iLoop<numItems; iLoop++){
			   DocumentHandler fragmentDoc = (DocumentHandler) fragmentVector.elementAt(iLoop);
			   String val = fragmentDoc.getAttribute(tag);

			   if (StringFunction.isNotBlank(val) )
				  xml.setAttribute(path+"("+iLoop+")"+tag, val.toUpperCase());

			}
	//   }
	return   xml;
   }

   private DocumentHandler upperCaseConversion(DocumentHandler xml,String xpath){
	  // for(int i = 0;i<xpath.length;i++){
		   String value = xml.getAttribute(xpath);
			  if (StringFunction.isNotBlank(value) )
				  xml.setAttribute(xpath, value.toUpperCase());

		//}
	   return xml;
   }
   /**
    * 
    * @param inputDoc
    * @param resMgr
    * @param pathToPopulate - amount path in matching rule list
    */
   
   private void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr, String pathToPopulate)
    {
   	// get the input document
   	if (inputDoc == null)
   	{
   		LOG.info("Input document to is null");
   		return;
   	}

   	String amount;

   	try
   	{
   		// Convert the amount from its localized form to standard (en_US) form
   		amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToPopulate), resMgr.getResourceLocale(), true);
   	}
   	catch(InvalidAttributeValueException iae)
   	{
   		// If the format was invalid, set amount equal to something that will trigger an error
   		// to be issued.
   		amount = TradePortalConstants.BAD_AMOUNT;
   	}

   	// Place the result of the above code into the amount location
   	// This is what will be populated into the business object.
   	inputDoc.setAttribute(pathToPopulate, amount);
   }
}