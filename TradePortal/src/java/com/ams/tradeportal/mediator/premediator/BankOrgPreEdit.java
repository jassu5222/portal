package com.ams.tradeportal.mediator.premediator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2004                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class BankOrgPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(BankOrgPreEdit.class);
   /**
    * BankOrgPreEdit constructor comment.
    */
   public BankOrgPreEdit()
   {
      super();
   }

   /**
    * 
    */
   public void act(AmsServletInvocation reqInfo, 
                   BeanManager beanMgr, 
                   FormManager formMgr, 
                   ResourceManager resMgr, 
                   String serverLocation, 
                   HttpServletRequest request, 
                   HttpServletResponse response, 
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
      // Retrieve the input document 
      if (inputDoc == null)
      {
         LOG.info("Input document to BankOrgPreEdit is null");
         return;
      }
      //CR-655 Suresh 03/03/2011 Begin
      Vector pymtVector = inputDoc.getFragments("/In/BankOrganizationGroup/PaymentReportingCode1List");
      Vector pymtVector2 = inputDoc.getFragments("/In/BankOrganizationGroup/PaymentReportingCode2List");
		int numItems = pymtVector.size();
		int numItems2 = pymtVector2.size();
		int iLoop;
		DocumentNode node = null;
		DocumentNode node2 = null;
		String id = "";
		String id2 = "";
        Map	pymtDeleteMap = new HashMap();
        Map	pymtDeleteMap2 = new HashMap();
        for (iLoop=0; iLoop<numItems; iLoop++)
		{
			DocumentHandler pymtDoc = (DocumentHandler) pymtVector.elementAt(iLoop);

			String paymentReportingCode1Oid = pymtDoc.getAttribute("/payment_reporting_code_1_oid");
			String code = pymtDoc.getAttribute("/code");
			String shortDescription = pymtDoc.getAttribute("/short_description");
			String description = pymtDoc.getAttribute("/description");

			if (StringFunction.isBlank(code) && StringFunction.isBlank(shortDescription) && StringFunction.isBlank(description)) {
				if (StringFunction.isBlank(paymentReportingCode1Oid)) {
					// Oid, num, and ccy are blank: remove this completely blank component
					// from the input doc.
					node = pymtDoc.getDocumentNode ("/");
					if(!StringFunction.isBlank(node.getIdName())) {
						id = node.getIdName();
					}
					inputDoc.removeComponent("/In/BankOrganizationGroup/PaymentReportingCode1List("+id+")");
				} else {
					// Oid is not blank but the num and ccy are.  The user wants to delete this
					// account.  Remove it from the input doc (so it doesn't get save and generate
					// an error), then add the oid to a new section for processing in the
					// mediator.

					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "PaymentReportingCode1");
					deleteDoc.setAttribute("/Oid", paymentReportingCode1Oid);
					pymtDeleteMap.put(iLoop+"",deleteDoc);
				}
			}
		}
		for (iLoop=0; iLoop<numItems2; iLoop++)
		{
			DocumentHandler pymtDoc2 = (DocumentHandler) pymtVector2.elementAt(iLoop);

			String paymentReportingCode2Oid = pymtDoc2.getAttribute("/payment_reporting_code_2_oid");
			String code2 = pymtDoc2.getAttribute("/code");
			String shortDescription2 = pymtDoc2.getAttribute("/short_description");
			String description2 = pymtDoc2.getAttribute("/description");

			if (StringFunction.isBlank(code2) && StringFunction.isBlank(shortDescription2) && StringFunction.isBlank(description2)) {
				if (StringFunction.isBlank(paymentReportingCode2Oid)) {
					// Oid, num, and ccy are blank: remove this completely blank component
					// from the input doc.
					node2 = pymtDoc2.getDocumentNode ("/");
					if(node2!=null && !StringFunction.isBlank(node2.getIdName())) {
						id2 = node2.getIdName();
					}
					inputDoc.removeComponent("/In/BankOrganizationGroup/PaymentReportingCode2List("+id2+")");
				} else {
					// Oid is not blank but the num and ccy are.  The user wants to delete this
					// account.  Remove it from the input doc (so it doesn't get save and generate
					// an error), then add the oid to a new section for processing in the
					// mediator.

					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "PaymentReportingCode2");
					deleteDoc.setAttribute("/Oid", paymentReportingCode2Oid);
					pymtDeleteMap2.put(iLoop+"",deleteDoc);
				}
			}
		}
		Iterator it = pymtDeleteMap.keySet().iterator();
		Iterator it2 = pymtDeleteMap2.keySet().iterator();
		int componentDeleteCount = 0;
		while (it.hasNext()) {
			String idx = (String)it.next();
			inputDoc.removeComponent("/In/BankOrganizationGroup/PaymentReportingCode1List("+idx+")");
			inputDoc.addComponent("/In/ComponentToDelete("+componentDeleteCount+")",(DocumentHandler)pymtDeleteMap.get(idx));
			componentDeleteCount++;
		}
		while (it2.hasNext()) {
			String idx = (String)it2.next();
			inputDoc.removeComponent("/In/BankOrganizationGroup/PaymentReportingCode2List("+idx+")");
			inputDoc.addComponent("/In/ComponentToDelete("+componentDeleteCount+")",(DocumentHandler)pymtDeleteMap2.get(idx));
			componentDeleteCount++;
		}
		// CR-655 Suresh 03/03/2011 End
      inputDoc = this.initializeDefaultSettings(inputDoc);
      inputDoc = this.setSelectedTypePDFForm(inputDoc, resMgr); //MEer CR-1027 Rel 9.3.5


  }

   /**
	* This method is used for initializing bank org default 
	* settings, in the event that they were not set by the user at all.
	* 
	* @param       inputDoc   DocumentHandler - the current input xml doc
	* @return      DocumentHandler - the current input xml doc with initialized attributes
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler initializeDefaultSettings(DocumentHandler inputDoc) throws AmsException
   {
      if (inputDoc.getAttribute("/In/BankOrganizationGroup/do_not_reply_ind") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/do_not_reply_ind", TradePortalConstants.INDICATOR_NO);
      }

      //Rel 9.5 IR#T36000046826 PGedupudi
      if (inputDoc.getAttribute("/In/BankOrganizationGroup/bene_do_not_reply_ind") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/bene_do_not_reply_ind", TradePortalConstants.INDICATOR_NO);
      }
      
     
      
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/brand_button_ind") == null)
	  {
	    inputDoc.setAttribute("/In/BankOrganizationGroup/brand_button_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  // DK CR-640 Rel7.1 BEGINS  
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/fx_online_avail_ind") == null)
	  {
	    inputDoc.setAttribute("/In/BankOrganizationGroup/fx_online_avail_ind", TradePortalConstants.INDICATOR_NO);
	  }
	  // DK CR-640 Rel7.1 ENDS  
	  
	  //CR 821 - Rel 8.3 START
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/repair_do_not_reply_ind") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/repair_do_not_reply_ind", TradePortalConstants.INDICATOR_NO);
      }
	  //CR 821 - Rel 8.3 END
	  //Narayan CR913 Rel9.0 29-Jan-2014 START
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/payable_do_not_reply_ind") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/payable_do_not_reply_ind", TradePortalConstants.INDICATOR_NO);
      }
	  //Narayan CR913 Rel9.0 29-Jan-2014 END
	  
	  //SSikhakolli Rel 9.3.5 CR-1029 - Begin
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/include_tt_reim_allowed") == null)
	  {
	    inputDoc.setAttribute("/In/BankOrganizationGroup/include_tt_reim_allowed", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/enable_admin_update_centre") == null)
	  {
	    inputDoc.setAttribute("/In/BankOrganizationGroup/enable_admin_update_centre", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/enable_session_sync_html") == null)
	  {
	    inputDoc.setAttribute("/In/BankOrganizationGroup/enable_session_sync_html", TradePortalConstants.INDICATOR_NO);
	  }
	  //SSikhakolli Rel 9.3.5 CR-1029 - End
	  //MEer CR-1027 Rel9.3.5  06/12/2015 START
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/atp_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/atp_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/airway_bill_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/airway_bill_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/exp_oco_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/exp_oco_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/lrq_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/lrq_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/exp_col_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/exp_col_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/rqa_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/rqa_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/shp_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/shp_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/gua_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/gua_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/slc_pdf_type") == null)
	  {
		  inputDoc.setAttribute("/In/BankOrganizationGroup/slc_pdf_type", TradePortalConstants.INDICATOR_NO);
	  }
	  //MEer CR-1027 Rel9.3.5  06/12/2015 END
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/reporting_code1_reqd_for_ach") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/reporting_code1_reqd_for_ach", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/reporting_code1_reqd_for_rtgs") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/reporting_code1_reqd_for_rtgs", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/reporting_code2_reqd_for_ach") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/reporting_code2_reqd_for_ach", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/reporting_code2_reqd_for_rtgs") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/reporting_code2_reqd_for_rtgs", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/no_sso_ind") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/no_sso_ind", TradePortalConstants.INDICATOR_NO);
      }
	  
	  /*CR1123 Start*/
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/incld_bnk_url_trade") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/incld_bnk_url_trade", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/incld_bnk_url_pmnt_ben") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/incld_bnk_url_pmnt_ben", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/incld_bnk_url_repr_panel") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/incld_bnk_url_repr_panel", TradePortalConstants.INDICATOR_NO);
      }
	  if (inputDoc.getAttribute("/In/BankOrganizationGroup/incld_bnk_url_payable_mgmt") == null)
      {
         inputDoc.setAttribute("/In/BankOrganizationGroup/incld_bnk_url_payable_mgmt", TradePortalConstants.INDICATOR_NO);
      }
	  /*CR1123 End*/
	  
	  return inputDoc;
   }

   
   /**
    * MEer CR-1027 Rel 9.3.5
    * This method is used to set the PDF form type selected by the user.
    * The check boxes are registered as reference data attributes, so when user selects a check box, 
    * this method maps the user selection to respective reference data item.  
  	* 
  	* @param       inputDoc   DocumentHandler - the current input xml doc
  	* @return      DocumentHandler - the current input xml doc with pdf types selected for each instrument type.
  	* @exception   com.amsinc.ecsg.frame.AmsException
  	*/  
   
   private DocumentHandler setSelectedTypePDFForm(DocumentHandler inputDoc, ResourceManager resMgr) throws AmsException
   {
	   String instrumentType = null;
	   String instrTypeCode = null;
	   MediatorServices medService =  new MediatorServices();
	   boolean error = false;
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/airway_bill_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/airway_bill_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/atp_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/atp_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/exp_oco_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/exp_oco_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/exp_col_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/exp_col_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type")) &&
			   TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type"))   ){
		   error= true;
		   instrTypeCode= "IMP_DLC";		  
		   instrumentType = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE", instrTypeCode, resMgr.getResourceLocale());
		   medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ONLY_ONE_PDF_TYPE_ALLOWED, instrumentType);

	   }else if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   } else if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/imp_dlc_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }

	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/lrq_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/lrq_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }

	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type")) &&
			   TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type"))   ){
		   error= true;
		   instrTypeCode= "GUA";
		   instrumentType = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE", instrTypeCode, resMgr.getResourceLocale());
		   medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ONLY_ONE_PDF_TYPE_ALLOWED ,  instrumentType);


	   }else if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/gua_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   else  if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/gua_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }

	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type")) &&
			   TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type"))   ){
		   error= true;
		   instrTypeCode= "SLC";
		   instrumentType = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_TYPE", instrTypeCode, resMgr.getResourceLocale());
		   medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ONLY_ONE_PDF_TYPE_ALLOWED , instrumentType);

	   }
	   else if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/slc_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   else if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
		   inputDoc.setAttribute("/In/BankOrganizationGroup/slc_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/rqa_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/rqa_pdf_type", TradePortalConstants.APPLICATION_PDF_BASIC_TYPE);
	   }
	   if(TradePortalConstants.INDICATOR_YES.equals(inputDoc.getAttribute("/In/BankOrganizationGroup/shp_pdf_type"))){
		   inputDoc.setAttribute("/In/BankOrganizationGroup/shp_pdf_type", TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE);
	   }
	   if(error){
		   medService.addErrorInfo();
		   inputDoc.setComponent ("/Error", medService.getErrorDoc ());
	   }
	   return inputDoc;

   }

   
}
