package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import java.math.BigInteger;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.busobj.util.InstrumentAuthentication;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class ClientBankPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(ClientBankPreEdit.class);
   /**
    * ClientBankPreEdit constructor comment.
    */
   public ClientBankPreEdit()
   {
      super();
   }

   /**
    *
    */
   public void act(AmsServletInvocation reqInfo,
                   BeanManager beanMgr,
                   FormManager formMgr,
                   ResourceManager resMgr,
                   String serverLocation,
                   HttpServletRequest request,
                   HttpServletResponse response,
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
      // Retrieve the input document
	   if (inputDoc == null)
	   {
		   LOG.info("Input document to ClientBankPreEdit is null");

		   return;
	   }

	   inputDoc = this.initializeDefaultSettings(inputDoc);

	   //cquinton cr498 - build the require_tran_auth attribute for save
	   String requireTranAuth = InstrumentAuthentication.NO_TRAN_AUTH.toString();
	   requireTranAuth = buildRequireTranAuth(inputDoc);
	   inputDoc.setAttribute("/In/ClientBank/require_tran_auth", requireTranAuth);

	   /*  MEer Rel 9.2 XSS IR-36837 If password coming from the widget is blank then get it from secure parameters map.  */

	   String imageServicePassword = inputDoc.getAttribute("/In/ClientBank/image_service_password");
	   if (StringFunction.isBlank(imageServicePassword)){	  
		   inputDoc.setAttribute("/In/ClientBank/image_service_password",inputDoc.getAttribute("/In/ClientBank/secure_image_password"));
	   }


  }

   /**
	* This method is used for initializing several corporate org default
	* settings, in the event that they were not set by the user at all.
	*
	* @param       inputDoc   DocumentHandler - the current input xml doc
	* @return      DocumentHandler - the current input xml doc with initialized attributes
	* @exception   com.amsinc.ecsg.frame.AmsException
	*/
   private DocumentHandler initializeDefaultSettings(DocumentHandler inputDoc) throws AmsException
   {
	  if (inputDoc.getAttribute("/In/ClientBank/corp_password_history") == null)
	  {
	    inputDoc.setAttribute("/In/ClientBank/corp_password_history", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/ClientBank/bank_password_history") == null)
	  {
	    inputDoc.setAttribute("/In/ClientBank/bank_password_history", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/ClientBank/corp_password_addl_criteria") == null)
	  {
	    inputDoc.setAttribute("/In/ClientBank/corp_password_addl_criteria", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/ClientBank/bank_password_addl_criteria") == null)
	  {
	    inputDoc.setAttribute("/In/ClientBank/bank_password_addl_criteria", TradePortalConstants.INDICATOR_NO);
	  }

	  if (inputDoc.getAttribute("/In/ClientBank/brand_button_ind") == null)
	  {
	    inputDoc.setAttribute("/In/ClientBank/brand_button_ind", TradePortalConstants.INDICATOR_NO);
	  }

		LOG.debug("can edit " + inputDoc.getAttribute("/In/ClientBank/can_edit_own_profile_ind"));
      //IAZ CR-475 06/21/09 Begin
      if (inputDoc.getAttribute("/In/ClientBank/can_edit_own_profile_ind") == null)
      {
         inputDoc.setAttribute("/In/ClientBank/can_edit_own_profile_ind", TradePortalConstants.INDICATOR_NO);
      }
      //IAZ CR-475 06/21/09 Begin

    //Vasavi CR 580 06/01/10 Begin
      if (inputDoc.getAttribute("/In/ClientBank/can_edit_id_ind") == null)
      {
         inputDoc.setAttribute("/In/ClientBank/can_edit_id_ind", TradePortalConstants.INDICATOR_NO);
      }
    //Vasavi CR 580 06/01/10 End

    //IR-VUK090959969 [START]
      if (inputDoc.getAttribute("/In/ClientBank/template_groups_ind") == null)
	  {
	     inputDoc.setAttribute("/In/ClientBank/template_groups_ind", TradePortalConstants.INDICATOR_NO);
      }
    //IR-VUK090959969 [END]
      
   // Suresh CR-603 03/08/11 Begin
      if (inputDoc.getAttribute("/In/ClientBank/report_categories_ind") == null)
	  {
	     inputDoc.setAttribute("/In/ClientBank/report_categories_ind", TradePortalConstants.INDICATOR_NO);
      }
   // Suresh CR-603 03/08/11 End
      if (inputDoc.getAttribute("/In/ClientBank/mobile_banking_access_ind") == null)
	  {
	     inputDoc.setAttribute("/In/ClientBank/mobile_banking_access_ind", TradePortalConstants.INDICATOR_NO);
      }
      //Nar Rel 9.5.0.0 CR 1132 01/27/2016 Begin
      if (inputDoc.getAttribute("/In/ClientBank/utilize_additional_bank_fields") == null)
      {
         inputDoc.setAttribute("/In/ClientBank/utilize_additional_bank_fields", TradePortalConstants.INDICATOR_NO);
      }
      //Nar Rel 9.5.0.0 CR 1132 01/27/2016 End
      //Rel 9.5.0.0 CR 927B Begin
      if (inputDoc.getAttribute("/In/ClientBank/control_NotifRule_byAdmin") == null)
      {
         inputDoc.setAttribute("/In/ClientBank/control_NotifRule_byAdmin", TradePortalConstants.INDICATOR_NO);
      }
      if (inputDoc.getAttribute("/In/ClientBank/allow_filtering_of_instruments") == null)
      {
         inputDoc.setAttribute("/In/ClientBank/allow_filtering_of_instruments", TradePortalConstants.INDICATOR_YES);
      }
      //Rel 9.5.0.0 CR 927B  End
	  return inputDoc;
   }

   /**
    * Build the require_tran_auth value from individual values
    * @param inputDoc
    * @return
    */
   public String buildRequireTranAuth(DocumentHandler inputDoc) {

        BigInteger requireTranAuth = InstrumentAuthentication.NO_TRAN_AUTH;
		
        DocumentHandler cbFrag = inputDoc.getFragment("/In/ClientBank");
		
        String value = "";

        value = cbFrag.getAttribute("/InstrumentAuthAirRel");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__AIR_REL );
		}

        value = cbFrag.getAttribute("/InstrumentAuthAtpIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__ATP_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthAtpAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__ATP_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthAtpApr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__ATP_APR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthDdiIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__DDI_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpColIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_COL_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpColAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_COL_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpOcoIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_OCO_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpOcoAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_OCO_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpDlcTrn");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_DLC_TRN );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpDlcAtr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ATR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpDlcAsn");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ASN );
		}

        value = cbFrag.getAttribute("/InstrumentAuthExpDlcDcr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__EXP_DLC_DCR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthImpDlcIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__IMP_DLC_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthImpDlcAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthImpDlcDcr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__IMP_DLC_DCR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthIncSlcDcr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__INC_SLC_DCR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthFtrqIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__FTRQ_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthLrqIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__LRQ_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthGuarIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__GUAR_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthGuarAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__GUAR_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthSlcIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__SLC_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthSlcAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__SLC_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthSlcDcr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__SLC_DCR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthFtdpIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__FTDP_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthReceivablesMatchResponse");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE );
		}

        value = cbFrag.getAttribute("/InstrumentAuthReceivablesApproveDiscount");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT );
		}

        value = cbFrag.getAttribute("/InstrumentAuthReceivablesCloseInvoice");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE );
		}

        value = cbFrag.getAttribute("/InstrumentAuthReceivablesFinInvoice");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE );
		}

        value = cbFrag.getAttribute("/InstrumentAuthReceivablesDisputeInvoice");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE );
		}

        value = cbFrag.getAttribute("/InstrumentAuthRqaIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RQA_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthRqaAmd");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RQA_AMD );
		}

        value = cbFrag.getAttribute("/InstrumentAuthRqaDcr");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__RQA_DCR );
		}

        value = cbFrag.getAttribute("/InstrumentAuthShpIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__SHP_ISS );
		}

        value = cbFrag.getAttribute("/InstrumentAuthFtbaIss");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__FTBA_ISS );
		}
        
        // Rel 9.0 CR 913 Start
        value = cbFrag.getAttribute("/InstrAuthPayablesMgmtUpdates");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__PAYABLES_MGMT_INV_UPDATES );
		}
        value = cbFrag.getAttribute("/InstrAuthInvoiceProcessFinance");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__PAYABLES_UPLOAD_INV_PROCESS );
		}
        //Rel 9.0 CR 913 End
        //AAlubala - CR710 Rel8.0 Invoice Management Instr Auth 04/20/2012 - START
        value = cbFrag.getAttribute("/instrumentAuthInvoiceMgntFinance");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE );
		}
        //CR710 - END
        
        // Nar CR-818 Rel9400 08/25/2015 Begin
        value = cbFrag.getAttribute("/instrumentAuthImpDlcSit");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__IMP_DLC_SIT );
		}
        value = cbFrag.getAttribute("/instrumentAuthImcSit");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__IMC_SIT );
		}
        value = cbFrag.getAttribute("/instrumentAuthLrqSit");
        if ( isSelected(value) ) {
            requireTranAuth = requireTranAuth.or( 
                InstrumentAuthentication.TRAN_AUTH__LRQ_SIT );
		}
        // Nar CR 818 Rel9400 08/25/2015 End
        
        return requireTranAuth.toString();
    }

    /**
     * If the parameter value = 'Y' it is selected.
     * Otherwise it is not selected.
     *
     * @param value parameter value
     * @return boolean - true if selected, false otherwise
     */
    private boolean isSelected(String value) {
    	if (TradePortalConstants.INDICATOR_YES.equals(value)) {
    		return true;
    	}
    	return false;
    }
}
