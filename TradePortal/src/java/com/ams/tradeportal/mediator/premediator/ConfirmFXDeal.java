package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *     Copyright  � 2004                        
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.agents.MarketRateDealRequest;
import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

/*
*
*/
public class ConfirmFXDeal implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(ConfirmFXDeal.class);
	
	
	
   /**
    * GetMarketRate constructor comment.
    */
   public ConfirmFXDeal()
   {
      super();
   }

   /**
    * 
    */
   public void act(AmsServletInvocation reqInfo, 
                   BeanManager beanMgr, 
                   FormManager formMgr, 
                   ResourceManager resMgr, 
                   String serverLocation, 
                   HttpServletRequest request, 
                   HttpServletResponse response, 
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws AmsException
   {
	   
	  

	   String selected = request.getParameter("selected") == null ? "" : request.getParameter("selected");	
	   SessionWebBean userSession = (SessionWebBean)request.getSession().getAttribute("userSession");
	   selected=EncryptDecrypt.decryptStringUsingTripleDes(selected, userSession.getSecretKey());

	   String[] arr=selected.split("/");
	   String transactionOid = arr[0];
	   String userOid = userSession.getUserOid();
	   String client_bankOID = userSession.getClientBankOid();
	   String corp_oid = userSession.getOwnerOrgOid();

	   DocumentHandler resp = (DocumentHandler)request.getSession().getAttribute(MarketRateDealRequest.FX_GETRATE_RESPONSE);

	   DocumentHandler doc = null;
	   try {
		   ClientBank clientBank = (ClientBank)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "ClientBank");
		   clientBank.setClientServerDataBridge(resMgr.getCSDB());
		   clientBank.getDataFromReferenceCache(Long.parseLong(client_bankOID));


		   CorporateOrganization corpOrg = (CorporateOrganization) 
				   EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "CorporateOrganization",Long.parseLong(corp_oid));

		   doc=(new MarketRateDealRequest()).queryMarketDeal(userOid, transactionOid, clientBank, corpOrg, beanMgr, resp);
		   inputDoc.setComponent ("/In/FXResp", doc);
		   inputDoc.setAttribute("/In/Update/ButtonPressed",TradePortalConstants.BUTTON_CONFIRM_FX);
		   
	   }
	   catch (Exception ex) {
		   String errorCode = TradePortalConstants.FX_GET_RATE_GENERAL_ERR;
		   if (ex instanceof AmsException) {
			   AmsException aex = (AmsException) ex;
			   IssuedError err = aex.getIssuedError();
			   if (err != null) {
				   errorCode = err.getErrorCode();
			   }
			   else {
				   ex.printStackTrace();
			   }
		   }else {
			   ex.printStackTrace();
		   }

		   MediatorServices medService = new MediatorServices();
		   medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,errorCode);
		   medService.addErrorInfo();
		   inputDoc.setComponent ("/Error", medService.getErrorDoc ());

	   }
   }

}
