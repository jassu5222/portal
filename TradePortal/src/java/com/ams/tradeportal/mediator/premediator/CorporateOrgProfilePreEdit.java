package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Hashtable;
import java.util.Vector;

public class CorporateOrgProfilePreEdit implements com.amsinc.ecsg.web.WebAction{
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrgProfilePreEdit.class);
	
	/**
	    * CorporateOrgProfilePreEdit constructor comment.
	    */
	    public CorporateOrgProfilePreEdit()
	    {
			super();
		}

		/**
	    *
	    */
	    public void act(AmsServletInvocation reqInfo,
	    		BeanManager beanMgr,
	    		FormManager formMgr,
	    		ResourceManager resMgr,
	    		String serverLocation,
	    		HttpServletRequest request,
	    		HttpServletResponse response,
	    		Hashtable inputParmMap,
	    		DocumentHandler inputDoc) throws AmsException{
	    		
	    			//Debug("In  CorporateOrgPProfilereEdit...... ");
	    			// Retrieve the input document
	    			if (inputDoc == null)
	    			{
	    				LOG.info("Input document to CorporateOrgPProfilpreEdit is null");
	    				return;
	    			}

	    			inputDoc = this.initializeDefaultSettings(inputDoc);
	    			// return;

	    }

	    /**
		* This method is used for initializing several corporate org default
		* settings, in the event that they were not set by the user at all.
		*
		* @param       inputDoc   DocumentHandler - the current input xml doc
		* @return      DocumentHandler - the current input xml doc with initialized attributes
		* @exception   com.amsinc.ecsg.frame.AmsException
		*/
	   private DocumentHandler initializeDefaultSettings(DocumentHandler inputDoc) throws AmsException
	   {
		   if (inputDoc.getAttribute("/In/CorporateOrganization/inherit_panel_auth_ind") == null)
			  {
			     inputDoc.setAttribute("/In/CorporateOrganization/inherit_panel_auth_ind", TradePortalConstants.INDICATOR_NO);
			  }

			Vector payAliasVector = inputDoc.getFragments("/In/CorporateOrganization/UserPreferencesPayDefList");
			int payAlias = payAliasVector.size();	
					
			
			for(int i=0; i<payAlias; i++) {		
				
				DocumentHandler aliasDoc = (DocumentHandler) payAliasVector.elementAt(i);
				String defaultInd = aliasDoc.getAttribute("/pmt_file_default_ind");	
				String payDefOid  = aliasDoc.getAttribute("/user_pref_pay_def_oid");	
				String aPmtDefOid  = aliasDoc.getAttribute("/a_pay_upload_definition_oid");
				LOG.info("defaultInd->"+defaultInd+"\t payDefOid;"+payDefOid+"\t aInvDefOid:"+aPmtDefOid);
				if (StringFunction.isBlank(inputDoc.getAttribute("/In/CorporateOrganization/UserPreferencesPayDefList("+i+")/pmt_file_default_ind"))){
					inputDoc.setAttribute("/In/CorporateOrganization/UserPreferencesPayDefList("+i+")/pmt_file_default_ind", TradePortalConstants.INDICATOR_NO);					
				}
				if (StringFunction.isBlank(aPmtDefOid)){		
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "UserPreferencesPayDef");
					deleteDoc.setAttribute("/Oid",payDefOid);

					inputDoc.removeComponent("/In/CorporateOrganization/UserPreferencesPayDefList("+i+")");	
					inputDoc.addComponent("/In/ComponentToDelete("+i+")", deleteDoc);
				}
				/*
				if ((StringFunction.isNotBlank(payDefOid)) && (StringFunction.isBlank(aInvDefOid) ||  aInvDefOid == null)){		
	   
					inputDoc.removeComponent("/In/CorporateOrganization/UserPreferencesPayDefList("+i+")");										
				}
				*/
			}
			LOG.info(" final inputDoc:"+inputDoc);
		      return inputDoc;
	   }
	    
	    

}
