package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;
/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.util.HashMap;

public class CrossRateCalcRulePreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(CrossRateCalcRulePreEdit.class);
	/**
	 * CrossRateCalcRulePreEdit constructor comment.
	 */
	public CrossRateCalcRulePreEdit()
	{
		super();
	}

	/**
	 *
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc) throws AmsException
			{
		// Retrieve the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to CrossRateCalcRulePreEdit is null");

			return;
		}


		// Determine whether rules are deleted or not entered from
		Vector ruleVector = inputDoc.getFragments("/In/CrossRateCalcRule/CrossRateRuleCriterionList");
		int numItems = ruleVector.size();
		int iLoop;
		DocumentNode node = null;
		String id = "";
		Map ruleDeleteMap = new HashMap();
		for (iLoop=0; iLoop<numItems; iLoop++)
		{
			DocumentHandler ruleDoc = (DocumentHandler) ruleVector.elementAt(iLoop);

			String ruleOid = ruleDoc.getAttribute("/criterion_oid");

			if (isEmptyRecord(ruleDoc)) {
				if (StringFunction.isBlank(ruleOid)) {
					// Oid, num, and ccy are blank: remove this completely blank component from the input doc.
					node = ruleDoc.getDocumentNode ("/");
					if(node!=null){
						id = node.getIdName();
						inputDoc.removeComponent("/In/CrossRateCalcRule/CrossRateRuleCriterionList("+id+")");
					}	
				} else {
					// Oid is not blank but the rest of fields are.  The user wants to delete this record.
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "CrossRateRuleCriterion");
					deleteDoc.setAttribute("/Oid", ruleOid);
					ruleDeleteMap.put(iLoop+"",deleteDoc);
					inputDoc.removeComponent("/In/CrossRateCalcRule/CrossRateRuleCriterionList("+iLoop+")");
					inputDoc.addComponent("/In/ComponentToDelete("+iLoop+")", deleteDoc);
				}
			}
		}


			}

	protected boolean isEmptyRecord(DocumentHandler ruleDoc) {
		boolean isEmpty = true;
		String[] attributeList = {"/from_ccy", "/to_ccy", "/lower_variance", "/upper_variance"}; // "/cross_rate_calc_rule_oid","/criterion_oid", };
		for (int i=0; i < attributeList.length; i++) {
			if (StringFunction.isNotBlank(ruleDoc.getAttribute(attributeList[i]))) {
				isEmpty = false;
				break;
			}

		}

		return isEmpty;  	   
	}
}
