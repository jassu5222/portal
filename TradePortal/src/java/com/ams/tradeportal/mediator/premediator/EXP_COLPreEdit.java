package com.ams.tradeportal.mediator.premediator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class EXP_COLPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(EXP_COLPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public EXP_COLPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

	String value;
	
	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_COLPreEdit is null");
		return;
	}
	/*KMehta @ Rel 8400 IR T36000026081 14/03/2013 Start*/ 
	if (!(TransactionType.AMEND).equals(inputDoc.getAttribute("/In/Transaction/transaction_type_code"))) {
	// DK IR T36000018006 Rel8.2 06/13/2013 starts
		if (!(TradePortalConstants.PMT_OTHER).equals(inputDoc
				.getAttribute("/In/Terms/pmt_terms_type"))) {
			for (int i = 1; i <= 6; i++) {
				if (StringFunction.isNotBlank(inputDoc
						.getAttribute("/In/Terms/tenor_" + i))) {
					inputDoc.setAttribute("/In/Terms/tenor_" + i, "");
				}
				if (StringFunction
						.isNotBlank(inputDoc.getAttribute("/In/Terms/tenor_"
								+ i + "_draft_number"))) {
					inputDoc.setAttribute("/In/Terms/tenor_" + i
							+ "_draft_number", "");
				}
				if (StringFunction.isNotBlank(inputDoc
						.getAttribute("/In/Terms/tenor_" + i + "_amount"))) {
					inputDoc.setAttribute("/In/Terms/tenor_" + i + "_amount",
							"");
				}
			}
		}
	// DK IR T36000018006 Rel8.2 06/13/2013 ends
	}
	/*KMehta @ Rel 8400 IR T36000026188 14/03/2013 End*/
	
        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_1_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_2_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_3_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_4_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_5_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_6_amount");
        //checkTenorTableDetails(inputDoc, resMgr);


	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if ((TransactionType.AMEND.equals(value)) ||
	    (TransactionType.TRACER.equals(value))) {
		doAMDPreEdit(inputDoc, request);
	} else {
		doISSPreEdit(inputDoc, resMgr);
	}
}

	
	
/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doAMDPreEdit(DocumentHandler inputDoc, HttpServletRequest request) 
        throws com.amsinc.ecsg.frame.AmsException {

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_COLPreEdit is null");
		return;
	}

    //Update the Collection date based on todays date if the user presses Save -or- Verify
    String gmtDateTimeNow = DateTimeUtility.getGMTDateTime();		//Find out what today's date is...
    SessionWebBean sessionWB = (SessionWebBean) request.getSession(false).getValue( "userSession" );
    Date nowCollectionDate = TPDateTimeUtility.getLocalDateTime( gmtDateTimeNow, sessionWB.getTimeZone() );

    GregorianCalendar myCalendar = new GregorianCalendar();
    myCalendar.setTime( nowCollectionDate );     			//******* Based on TODAY'S DATE *******
								                            //Build a saveable version of the date
    //** of Note ** For some reason the Month constant is zero based so you have to add 1 for the correct month int...

    String jPylonDate = TPDateTimeUtility.buildJPylonDateTime( String.valueOf( myCalendar.get(myCalendar.DATE) ),
							                                   String.valueOf( myCalendar.get(myCalendar.MONTH) + 1 ),
							                                   String.valueOf( myCalendar.get(myCalendar.YEAR) ));
    //If the user Save's or Verify's then this secureParm will be saved...
    //ie: the collection date will be updated with todays date.
    //secureParms.put("CollectionDate", jPylonDate);
    inputDoc.setAttribute("/In/Terms/collection_date", jPylonDate);


}

/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doISSPreEdit(DocumentHandler inputDoc, ResourceManager resMgr) {

	
	String date;
	String newDate;
	//String partNumber = inputDoc.getAttribute("/In/partNumber");
	// The part currently being edited.  If the user is NOT in multi-part mode, 
	// the partNumber will be set to an empty string.

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_COLPreEdit is null");
		return;
	}
	checkTenorTableDetails(inputDoc, resMgr);
        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");

    
	    // Need to convert all three part date fields into single date
	    // values.
    	date = inputDoc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_date");	    

	    //LOG.info("dmy are " + day + ":" + month + ":" + year);

	    newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);

	    //LOG.info("new date is " + newDate);
	    inputDoc.setAttribute("/In/Terms/pmt_terms_fixed_maturity_date", newDate);
    
	// Checkboxes do not send values if the checkbox is unchecked.
	// Therefore, for each checkbox box, determine if we have a
	// value in the document.  If not, add an explicit N value for
	// each one that is missing.  This ensures that checkboxes that
	// are unchecked by the user get reset to N.
   
	    resetCheckBox( inputDoc, "/In/Terms/present_bills_of_exchange" );
	    resetCheckBox( inputDoc, "/In/Terms/present_commercial_invoice" );
	    resetCheckBox( inputDoc, "/In/Terms/present_bill_of_lading" );
	    resetCheckBox( inputDoc, "/In/Terms/present_non_neg_bill_of_lading" );
	    resetCheckBox( inputDoc, "/In/Terms/present_air_waybill" );
	    resetCheckBox( inputDoc, "/In/Terms/present_insurance" );
	    resetCheckBox( inputDoc, "/In/Terms/present_cert_of_origin" );
	    resetCheckBox( inputDoc, "/In/Terms/present_packing_list" );
	    resetCheckBox( inputDoc, "/In/Terms/present_other");
   
	    
	    date = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
	    //LOG.info("dmy are " + day + ":" + month + ";:" + year);

	    newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);

	    //LOG.info("new date is " + newDate);
	    inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

	    resetCheckBox( inputDoc, "/In/Terms/instr_release_doc_on_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_release_doc_on_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_pmt_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_accept_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_non_pmt_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_non_accept_by_tel" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_note_for_non_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_note_for_non_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_protest_for_non_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_protest_for_non_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/in_case_of_need_contact" );
	    resetCheckBox( inputDoc, "/In/Terms/do_not_waive_refused_charges" );
	    resetCheckBox( inputDoc, "/In/Terms/do_not_waive_refused_interest" );
	    resetCheckBox( inputDoc, "/In/Terms/collect_interest_at_indicator" );
	

}


/**
 * This is a method that gets used a large number of times.  This is meant to localize
 * the code.  Basically we check for a value at a given path in the Xml Doc, if it
 * Does'nt exist then we set that value in the document to 'N'.  This is used for 
 * resetting a Check boxes value since deselection of a check box does NOT update the
 * Xml Doc with a revised value.
 *
 * @param DocumentHandler inputDoc - the document to get and set data against. 
 * @param String xmlPath           - this the the path to use to get/set 
 *                                   attributes in the xml Doc.
 */
private void resetCheckBox( DocumentHandler inputDoc, String xmlPath) {

    String value;
    
    value = inputDoc.getAttribute( xmlPath );
    if (value == null)
        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
}

/*get all tenor details and align it properly*/
public void checkTenorTableDetails(DocumentHandler inputDoc, ResourceManager resMgr){
	 
		// get the input document 
		if (inputDoc == null)
		{
		   LOG.info("GenericTransactionPreEdit.java : Input document is null");
		   return;
		}
		boolean flag = false;
		
		
		Map<Integer, Map> primaryMap = new LinkedHashMap<Integer, Map>();
		Map<String, String> secondaryMap = null;
		
		
		int i=-1;
		int j=0;;
		int index=0;
		for(i=1; i<=TradePortalConstants.NUMBER_OF_TENORS; i++){
			flag = false;
			secondaryMap = new HashMap<String, String>();
			
			if(null != inputDoc.getAttribute("/In/Terms/tenor_"+i)){
				
				if(!"".equals(inputDoc.getAttribute("/In/Terms/tenor_"+i))){
					flag = true;
					secondaryMap.put("tenor_"+j, inputDoc.getAttribute("/In/Terms/tenor_"+i));
				}else
				{
					secondaryMap.put("tenor_"+j, "");
				}
			}else{
				secondaryMap.put("tenor_"+j, "");
			}
			
			if(null != inputDoc.getAttribute("/In/Terms/tenor_"+i+"_amount")){
				
				if(!"".equals(inputDoc.getAttribute("/In/Terms/tenor_"+i+"_amount"))){
					flag = true;
					secondaryMap.put("tenor_"+j+"_amount", inputDoc.getAttribute("/In/Terms/tenor_"+i+"_amount"));
				}else
				{
					secondaryMap.put("tenor_"+j+"_amount", "");
				}
			}else{
				secondaryMap.put("tenor_"+j+"_amount", "");
			}
			
			if(null != inputDoc.getAttribute("/In/Terms/tenor_"+i+"_draft_number")){
				
				if(!"".equals(inputDoc.getAttribute("/In/Terms/tenor_"+i+"_draft_number"))){
					flag = true;
					secondaryMap.put("tenor_"+j+"_draft_number", inputDoc.getAttribute("/In/Terms/tenor_"+i+"_draft_number"));
				}else
				{
					secondaryMap.put("tenor_"+j+"_draft_number", "");
				}
			}else{
				secondaryMap.put("tenor_"+j+"_draft_number", "");
			}
				
			if(flag){
				primaryMap.put(index++, secondaryMap);
				j++;
			}
				
				
			
		}
		LOG.info("Primary Map : "+primaryMap);
		index = 0;
		Map tempMap = null;
		for(i=0; i<primaryMap.size();i++){
			
			tempMap = primaryMap.get(i);
			
			inputDoc.setAttribute("/In/Terms/tenor_"+(i+1), (String)tempMap.get("tenor_"+(i)));			
			inputDoc.setAttribute("/In/Terms/tenor_"+(i+1)+"_amount", (String)tempMap.get("tenor_"+(i)+"_amount"));
			inputDoc.setAttribute("/In/Terms/tenor_"+(i+1)+"_draft_number", (String)tempMap.get("tenor_"+(i)+"_draft_number"));
			
		}
		if(primaryMap.size() < TradePortalConstants.NUMBER_OF_TENORS){
			
			for(i=primaryMap.size()+1; i<=TradePortalConstants.NUMBER_OF_TENORS; i++){
				
				inputDoc.setAttribute("/In/Terms/tenor_"+(i), "");
				inputDoc.setAttribute("/In/Terms/tenor_"+(i)+"_amount", "");
				inputDoc.setAttribute("/In/Terms/tenor_"+(i)+"_draft_number", "");
				
			}
			
		}
		
	}

	 
}


