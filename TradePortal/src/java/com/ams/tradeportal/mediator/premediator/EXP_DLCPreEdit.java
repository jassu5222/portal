package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

/*
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EXP_DLCPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(EXP_DLCPreEdit.class);
   public EXP_DLCPreEdit()
   {
      super();
   }

   /**
    * Performs different logic based on the type of transaction (ISS or AMD)
    * 
    * But basically converts the month, day, year fields for each of the 
    * dates into single date fields.  This allows populateFromXml to work.
    *
    * In addition, explicitly sets all 'missing' checkbox fields to N.
    * (Checkbox fields which are not checked are not sent in the document,
    * we must force an N to cause unchecked values to be save correctly.)
    * 
    */
   public void act(AmsServletInvocation reqInfo, 
                   BeanManager beanMgr, 
                   FormManager formMgr, 
                   ResourceManager resMgr, 
                   String serverLocation, 
                   HttpServletRequest request, 
                   HttpServletResponse response, 
                   Hashtable inputParmMap,
                   DocumentHandler inputDoc) throws com.amsinc.ecsg.frame.AmsException 
   {
      String   value = null;

      if (inputDoc == null)
      {
         LOG.info("Input document to EXP_DLCPreEdit is null");
         return;
      }

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

      value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");

	  if (value.equals(TransactionType.TRANSFER) || (value.equals(TransactionType.AMEND_TRANSFER)))
	  {
         doPreEdit(inputDoc);
      }
   }

   /**
    * Handles pre-edits for Export Letter of Credit TRN and ATR type 
    * transactions.  This includes converting 3part date field into 
    * a single date value.
    *
    * @param doc com.amsinc.ecsg.util.DocumentHandler
    */
   private void doPreEdit(DocumentHandler inputDoc)
   {
     
      String   date;
      String   newDate;     

      // get the part number of the page being looked at (if one exists)      

      // Since this method is used by some pages that have parts (like the issue transfer) 
      // and some that don't (like the amend transfer), we have to take into consideration
      // that part number might be null (because there are no parts on the amend)

          // Need to convert all three part date fields into single date
          // values.        
          date  = inputDoc.getAttribute("/In/Terms/expiry_date");

          newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);

          inputDoc.setAttribute("/In/Terms/expiry_date", newDate);
          
          date  = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");

          newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);

          inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newDate);
   }
}
