package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;

import java.math.*;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.busobj.util.*;

//Vasavi CR 524 03/31/2010 
public class EXP_OCOPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(EXP_OCOPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public EXP_OCOPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

	String value;
	
	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_OCOPreEdit is null");
		return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_1_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_2_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_3_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_4_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_5_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "tenor_6_amount");



	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if ((TransactionType.AMEND.equals(value)) ||
	    (TransactionType.TRACER.equals(value))) {
		doAMDPreEdit(inputDoc, request);
	} else {
		doISSPreEdit(inputDoc, resMgr);
	}
}

	
	
/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doAMDPreEdit(DocumentHandler inputDoc, HttpServletRequest request) 
        throws com.amsinc.ecsg.frame.AmsException {

	String day;
	String month;
	String year;
	String newDate;

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_OCOPreEdit is null");
		return;
	}

    //Update the Collection date based on todays date if the user presses Save -or- Verify
    String gmtDateTimeNow = DateTimeUtility.getGMTDateTime();		//Find out what today's date is...
    SessionWebBean sessionWB = (SessionWebBean) request.getSession(false).getValue( "userSession" );
    Date nowCollectionDate = TPDateTimeUtility.getLocalDateTime( gmtDateTimeNow, sessionWB.getTimeZone() );

    GregorianCalendar myCalendar = new GregorianCalendar();
    myCalendar.setTime( nowCollectionDate );     			//******* Based on TODAY'S DATE *******
								                            //Build a saveable version of the date
    //** of Note ** For some reason the Month constant is zero based so you have to add 1 for the correct month int...

    String jPylonDate = TPDateTimeUtility.buildJPylonDateTime( String.valueOf( myCalendar.get(myCalendar.DATE) ),
							                                   String.valueOf( myCalendar.get(myCalendar.MONTH) + 1 ),
							                                   String.valueOf( myCalendar.get(myCalendar.YEAR) ));
    //If the user Save's or Verify's then this secureParm will be saved...
    //ie: the collection date will be updated with todays date.
    //secureParms.put("CollectionDate", jPylonDate);
    inputDoc.setAttribute("/In/Terms/collection_date", jPylonDate);


}

/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doISSPreEdit(DocumentHandler inputDoc, ResourceManager resMgr) {

	String day;
	String month;
	String year;
	String newDate;
	//String partNumber = inputDoc.getAttribute("/In/partNumber");
	// The part currently being edited.  If the user is NOT in multi-part mode, 
	// the partNumber will be set to an empty string.

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to EXP_OCOPreEdit is null");
		return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");
    
        //IR 21994 start - use single date value populated
        /*
	    // Need to convert all three part date fields into single date
	    // values.
       
        day = inputDoc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_day");
	    month = inputDoc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_month");
	    year = inputDoc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_year");
	    
	    newDate = TPDateTimeUtility.buildJPylonDateTime(day, month, year); */

	    newDate= inputDoc.getAttribute("/In/Terms/pmt_terms_fixed_maturity_date");
	    newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
	    //IR 21994 end
	    inputDoc.setAttribute("/In/Terms/pmt_terms_fixed_maturity_date", newDate);
    
	// Checkboxes do not send values if the checkbox is unchecked.
	// Therefore, for each checkbox box, determine if we have a
	// value in the document.  If not, add an explicit N value for
	// each one that is missing.  This ensures that checkboxes that
	// are unchecked by the user get reset to N.
  
	    resetCheckBox( inputDoc, "/In/Terms/present_bills_of_exchange" );
	    resetCheckBox( inputDoc, "/In/Terms/present_commercial_invoice" );
	    resetCheckBox( inputDoc, "/In/Terms/present_bill_of_lading" );
	    resetCheckBox( inputDoc, "/In/Terms/present_non_neg_bill_of_lading" );
	    resetCheckBox( inputDoc, "/In/Terms/present_air_waybill" );
	    resetCheckBox( inputDoc, "/In/Terms/present_insurance" );
	    resetCheckBox( inputDoc, "/In/Terms/present_cert_of_origin" );
	    resetCheckBox( inputDoc, "/In/Terms/present_packing_list" );
	    resetCheckBox( inputDoc, "/In/Terms/present_other");
	    
	    // jgadela 09/30/2015 R94   IR T36000044286 - fixed to save the feild - fec_maturity_date
	    newDate = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
	    newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
	    inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

	    resetCheckBox( inputDoc, "/In/Terms/instr_release_doc_on_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_release_doc_on_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_pmt_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_accept_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_non_pmt_by_telco" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_advise_non_accept_by_tel" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_note_for_non_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_note_for_non_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_protest_for_non_pmt" );
	    resetCheckBox( inputDoc, "/In/Terms/instr_protest_for_non_accept" );
	    resetCheckBox( inputDoc, "/In/Terms/in_case_of_need_contact" );
	    resetCheckBox( inputDoc, "/In/Terms/do_not_waive_refused_charges" );
	    resetCheckBox( inputDoc, "/In/Terms/do_not_waive_refused_interest" );
	    resetCheckBox( inputDoc, "/In/Terms/collect_interest_at_indicator" );
	

}


/**
 * This is a method that gets used a large number of times.  This is meant to localize
 * the code.  Basically we check for a value at a given path in the Xml Doc, if it
 * Does'nt exist then we set that value in the document to 'N'.  This is used for 
 * resetting a Check boxes value since deselection of a check box does NOT update the
 * Xml Doc with a revised value.
 *
 * @param DocumentHandler inputDoc - the document to get and set data against. 
 * @param String xmlPath           - this the the path to use to get/set 
 *                                   attributes in the xml Doc.
 */
private void resetCheckBox( DocumentHandler inputDoc, String xmlPath) {

    String value;
    
    value = inputDoc.getAttribute( xmlPath );
    if (value == null)
        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
}


}