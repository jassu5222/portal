package com.ams.tradeportal.mediator.premediator;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class FTDPPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FTDPPreEdit.class);
	/**
	 * FTDPPreEdit constructor comment.
	 */
	public FTDPPreEdit() {
		super();
	}

	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 *
	 * But basically converts the month, day, year fields for each of the
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 *
	 * Also, perform logic to determine the account number and currency
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
		String value;

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to FTDPPreEdit is null");
			return;
		}

		// Convert decimal number from its locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

		doFTDPPreEdit(inputDoc, resMgr);
	}

	/**
	 * Performs pre-editing of the input document that is specific to ISS
	 * transactions.  This includes converting the three part date fields
	 * into a single date values and explicitly setting checkbox values
	 * to N when they are not already present in the document.
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void doFTDPPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
	{
		//String day;
		//String month;
		//String year;
		String newDate;

		String partNumber;
		String value;
		String acctNum = "";
		String acctCcy = "";

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to FTDPPreEdit is null");
			return;
		}

		// Convert decimal numbers from their locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
		doDecimalPreMediatorAction(inputDoc, resMgr, "funding_amount");
		doDecimalPreMediatorActionForDomesticAmt(inputDoc,resMgr,"amount");
		LOG.debug("FTDPPreEdit::doFTDPPreEdit: " + inputDoc.getAttribute("/In/Transaction/payment_date"));
		String paymentDate = inputDoc.getAttribute("/In/Transaction/payment_date");
		paymentDate = TPDateTimeUtility.convertISODateToJPylonDate(paymentDate);

		if ((paymentDate == null) || (paymentDate.indexOf("-1") != -1))
		{
			inputDoc.setAttribute("/In/Transaction/payment_date", "");
		} else {
			inputDoc.setAttribute("/In/Transaction/payment_date", paymentDate);
		}

		// get the part number of the page being looked at (if one exists)
		partNumber = inputDoc.getAttribute("/In/partNumber");

		if (partNumber.equals("") || partNumber.equals(TradePortalConstants.PART_ONE))
		{
			// Save the selected Payee account/ccy.  This will either be one of the
			// predefined accounts or a user enterable value.  For the predefined accts,
			// the radio button value consists of the acct number and currency (separated
			// by DLIMITER_CHAR, e.g., Acct1234DELIMITER_CHARUSD).  For the entered value,
			// pull the values from the enterable account number and currency fields.

			value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");
			if (TradePortalConstants.USER_ENTERED_ACCT.equals(value) || (value == null)) {

				// The user entered an account number and currency.  Move these values to
				// the real object/database field.  (If value is null it means that either
				// the user didn't select a radio button or the radio buttons are not
				// available and only the entereable fields are shown.  In either case,
				// take what was entered.  We'll catch errors later as part of required value
				// editing.

				acctNum = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_account");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);

				acctCcy = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_currency");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);

			} else {
				// The user selected one of the predefined accounts.  Parse the radio button
				// value into account number and currency and place in the real object/db field.
				value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");

				if (value != null) {
					// selected_account (value) can be null in two cases.  one is if the user
					// doesn't select the radio button.  two is when there is no radio button
					// to select.
					int delimeterAt = value.indexOf(TradePortalConstants.DELIMITER_CHAR);
					acctNum = value.substring(0,delimeterAt);
					acctCcy = value.substring(delimeterAt+TradePortalConstants.DELIMITER_CHAR.length(),
							value.length());

					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);
				}
			}
		}
		// DK CR-640 Rel7.1
		if (inputDoc.getAttribute("/In/Terms/request_market_rate_ind") == null)
		{
			inputDoc.setAttribute("/In/Terms/request_market_rate_ind", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/individual_debit_ind") == null)
		{
			inputDoc.setAttribute("/In/Terms/individual_debit_ind", TradePortalConstants.INDICATOR_NO);
		}
		// Nar CR 966 Rel 9.2 09/17/2014 Add - Begin 
		// check box will be available for template only so set 'N' only when it is template action.
		if (inputDoc.getDocumentNode("/In/Template") != null) {
			if (inputDoc.getAttribute("/In/DomesticPayment/allow_pay_amt_modification") == null) {
				inputDoc.setAttribute("/In/DomesticPayment/allow_pay_amt_modification", TradePortalConstants.INDICATOR_NO);
			}
			if (inputDoc.getAttribute("/In/DomesticPayment/allow_pay_detail_modification") == null) {
				inputDoc.setAttribute("/In/DomesticPayment/allow_pay_detail_modification", TradePortalConstants.INDICATOR_NO);
			}
			if (inputDoc.getAttribute("/In/DomesticPayment/allow_inv_detail_modification") == null) {
				inputDoc.setAttribute("/In/DomesticPayment/allow_inv_detail_modification",TradePortalConstants.INDICATOR_NO);
			}
		}
		// Nar CR 966 Rel 9.2 09/17/2014 Add - End
		if (StringFunction.isNotBlank(inputDoc.getAttribute("/In/InvoiceDetails/payee_invoice_details"))) {
			inputDoc.setAttribute("/In/InvoiceDetails/changed", TradePortalConstants.INDICATOR_YES);
		}
		
		String bene_party_oid = inputDoc.getAttribute("/In/DomesticPayment/beneficiary_party_oid");
		if (StringFunction.isNotBlank(bene_party_oid)) {
			String[] arr = bene_party_oid.split("/");
			bene_party_oid = arr[0];
			inputDoc.setAttribute("/In/DomesticPayment/beneficiary_party_oid", bene_party_oid);
		}
		
	}

}