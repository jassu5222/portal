
package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class FTRQPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(FTRQPreEdit.class);
	/**
	 *  FTRQPreEdit constructor comment.
	 */
	public FTRQPreEdit() {
		super();
	}

	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 *
	 * But basically converts the month, day, year fields for each of the
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 *
	 * Also, perform logic to determine the account number and currency
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
		String value;

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to FTRQPreEdit is null");
			return;
		}

		// Convert decimal number from its locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		//doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

		doFTRQPreEdit(inputDoc, resMgr);
	}

	/**
	 * Performs pre-editing of the input document that is specific to ISS
	 * transactions.  This includes converting the three part date fields
	 * into a single date values and explicitly setting checkbox values
	 * to N when they are not already present in the document.
	 *
	 * @param doc com.amsinc.ecsg.util.DocumentHandler
	 */
	private void doFTRQPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
	{
		String partNumber;
		String value;
		String acctNum = "";
		String acctCcy = "";

		// get the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to FTRQPreEdit is null");
			return;
		}

		// Convert decimal numbers from their locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
		doDecimalPreMediatorAction(inputDoc, resMgr, "funding_amount");

        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
		doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");

		// get the part number of the page being looked at (if one exists)
		partNumber = inputDoc.getAttribute("/In/partNumber");

		//commented for TP UI Refresh change
		//if (partNumber.equals("") || partNumber.equals(TradePortalConstants.PART_ONE))
		//{
			// Save the selected Payee account/ccy.  This will either be one of the
			// predefined accounts or a user enterable value.  For the predefined accts,
			// the radio button value consists of the acct number and currency (separated
			// by DLIMITER_CHAR, e.g., Acct1234DELIMITER_CHARUSD).  For the entered value,
			// pull the values from the enterable account number and currency fields.

			value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");
			if (TradePortalConstants.USER_ENTERED_ACCT.equals(value) || (value == null)) {

				// The user entered an account number and currency.  Move these values to
				// the real object/database field.  (If value is null it means that either
				// the user didn't select a radio button or the radio buttons are not
				// available and only the entereable fields are shown.  In either case,
				// take what was entered.  We'll catch errors later as part of required value
				// editing.

				acctNum = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_account");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);

				acctCcy = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_currency");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);

			} else {
				// The user selected one of the predefined accounts.  Parse the radio button
				// value into account number and currency and place in the real object/db field.
				value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");

				if (value != null) {
					// selected_account (value) can be null in two cases.  one is if the user
					// doesn't select the radio button.  two is when there is no radio button
					// to select.
					int delimeterAt = value.indexOf(TradePortalConstants.DELIMITER_CHAR);
					acctNum = value.substring(0,delimeterAt);
					acctCcy = value.substring(delimeterAt+TradePortalConstants.DELIMITER_CHAR.length(),
							value.length());

					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);
				}
			}
		//}//commented for TP UI Refresh change
		//if (partNumber.equals("") || partNumber.equals(TradePortalConstants.PART_TWO))
		//{
			// Need to convert the release date fields into a single date value.
		// date change for TP UI Refresh change
		//	day   = inputDoc.getAttribute("/In/Terms/release_day");
		//	month = inputDoc.getAttribute("/In/Terms/release_month");
		//	year  = inputDoc.getAttribute("/In/Terms/release_year");
			
            String date = inputDoc.getAttribute("/In/Terms/release_by_bank_on");
            String newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
			inputDoc.setAttribute("/In/Terms/release_by_bank_on", newDate);
            inputDoc.setAttribute("/In/Transaction/payment_date", newDate);

			/*day   = inputDoc.getAttribute("/In/Terms/loan_terms_fixed_maturity_day");
			month = inputDoc.getAttribute("/In/Terms/loan_terms_fixed_maturity_month");
			year  = inputDoc.getAttribute("/In/Terms/loan_terms_fixed_maturity_year");

			newDate = TPDateTimeUtility.buildJPylonDateTime(day, month, year);
			inputDoc.setAttribute("/In/Terms/loan_terms_fixed_maturity_dt", newDate);

            day   = inputDoc.getAttribute("/In/Terms/fec_maturity_day");
			month = inputDoc.getAttribute("/In/Terms/fec_maturity_month");
			year  = inputDoc.getAttribute("/In/Terms/fec_maturity_year");
			newDate = TPDateTimeUtility.buildJPylonDateTime(day, month, year);
			inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);
			*/
			if (inputDoc.getAttribute("/In/Terms/use_fec") == null)
			{
				inputDoc.setAttribute("/In/Terms/use_fec", TradePortalConstants.INDICATOR_NO);
			}
			if (inputDoc.getAttribute("/In/Terms/use_other") == null)
			{
				inputDoc.setAttribute("/In/Terms/use_other", TradePortalConstants.INDICATOR_NO);
			}
			if (inputDoc.getAttribute("/In/Terms/use_mkt_rate") == null)
			{
				inputDoc.setAttribute("/In/Terms/use_mkt_rate", TradePortalConstants.INDICATOR_NO);
			}
			// DK CR-640 Rel7.1
			if (inputDoc.getAttribute("/In/Terms/request_market_rate_ind") == null)
			{
				inputDoc.setAttribute("/In/Terms/request_market_rate_ind", TradePortalConstants.INDICATOR_NO);
			}
		//}
	}

}