package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class GUARPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(GUARPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public GUARPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

	String value;
	
	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to IMP_DLCPreEdit is null");
		return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if (TransactionType.AMEND.equals(value)) {
		doAMDPreEdit(inputDoc);
	} else if (TransactionType.PAYMENT.equals(value)){
		doPAYEdit(inputDoc);
	}else{
		doISSPreEdit(inputDoc);
	}
}

	
	
private void doPAYEdit(DocumentHandler inputDoc) {
	deleteChargesIncurred(inputDoc);
	
}

/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doAMDPreEdit(DocumentHandler inputDoc) {

	setDateValues(inputDoc);
    deleteChargesIncurred(inputDoc);
}


/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void doISSPreEdit(DocumentHandler inputDoc) {
    
    	setDateValues(inputDoc);
    	resetCheckBox( inputDoc, "/In/Terms/guar_accept_instr" );
    	resetCheckBox( inputDoc, "/In/Terms/guar_advise_issuance" );
	    resetCheckBox( inputDoc, "/In/Terms/ucp_version_details_ind" );
	    resetCheckBox( inputDoc, "/In/Terms/irrevocable" );
	  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
	    resetCheckBoxAttribute(inputDoc, "/In/Terms/auto_extension_ind");
	  //Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
	  
        //cquinton 10/16/2013 ir#21872 Rel 8.3 start
        //when ucp version is not 'OTHER', ucp version details should be blanked out
        String ucpVersion = inputDoc.getAttribute("/In/Terms/ucp_version");
        if ( !TradePortalConstants.GUA_OTHER.equals(ucpVersion) ) {
             inputDoc.setAttribute("/In/Terms/ucp_details", "");
        }
        //cquinton 10/16/2013 ir#21872 Rel 8.3 end	
		if(TradePortalConstants.INDICATOR_NO.equals(inputDoc.getAttribute("/In/Terms/auto_extension_ind"))){
        	inputDoc.setAttribute("/In/Terms/max_auto_extension_allowed", "");
        	inputDoc.setAttribute("/In/Terms/auto_extension_period", "");
        	inputDoc.setAttribute("/In/Terms/auto_extension_days", "");
        	inputDoc.setAttribute("/In/Terms/final_expiry_date", "");
        	inputDoc.setAttribute("/In/Terms/notify_bene_days", "");        	
        }else{
          if(!TradePortalConstants.DAY.equals(inputDoc.getAttribute("/In/Terms/auto_extension_period"))){
        	inputDoc.setAttribute("/In/Terms/auto_extension_days", "");
          }
          if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/final_expiry_date"))){
        	  inputDoc.setAttribute("/In/Terms/max_auto_extension_allowed", "");
          }else{
        	 inputDoc.setAttribute("/In/Terms/final_expiry_date", "");
          }
       }  
	    resetCheckBoxAttribute(inputDoc, "/In/Terms/close_and_deactivate");
	    deleteChargesIncurred(inputDoc);


}

private void deleteChargesIncurred(DocumentHandler inputDoc){
	int chrgIncRowCount=inputDoc.getFragmentsList("/In/Transaction/FeeList").size();
	StringBuffer feeOidStr = new StringBuffer();
	for(int iLoop=0; iLoop<(chrgIncRowCount); iLoop++){
	
	if( StringFunction.isBlank(inputDoc.getAttribute("/In/Transaction/FeeList("+iLoop+")/fee_oid"))
			&& StringFunction.isBlank(inputDoc.getAttribute("/In/Transaction/FeeList("+iLoop+")/settlement_curr_code"))
			&& StringFunction.isBlank(inputDoc.getAttribute("/In/Transaction/FeeList("+iLoop+")/settlement_curr_amount"))
			&& StringFunction.isBlank(inputDoc.getAttribute("/In/Transaction/FeeList("+iLoop+")/charge_type"))
			
		){
		inputDoc.removeAllChildren("/In/Transaction/FeeList("+iLoop+")");
		inputDoc.removeComponent("/In/Transaction/FeeList("+iLoop+")");
		}
	else{
		String feeOid = inputDoc.getAttribute("/In/Transaction/FeeList("+iLoop+")/fee_oid");
		if (StringFunction.isNotBlank(feeOid)) {
			feeOidStr.append(feeOid);
			feeOidStr.append(",");
		}
	}
	}

	if (StringFunction.isNotBlank(feeOidStr.toString())){
		String deleteSql = "delete from fee where p_transaction_oid = ? and fee_oid not in (";
		List<Object> sqlPrmLst = new ArrayList<Object>();
		sqlPrmLst.add(inputDoc.getAttribute("/In/Transaction/transaction_oid"));
		String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(feeOidStr.substring(0,feeOidStr.length()-1), sqlPrmLst);
		deleteSql += placeHolderStr + ")";

		try(Connection con = DatabaseQueryBean.connect(false);
 			PreparedStatement pStmt = con.prepareStatement(deleteSql)) {
 			
 			Object [] sqlPrmArray = sqlPrmLst.toArray();

 			for (int i=0; i<sqlPrmArray.length; i++) {
				if (sqlPrmArray[i] != null) {
					pStmt.setObject(i + 1, sqlPrmArray[i]);
				} else {

					pStmt.setNull(i + 1, Types.OTHER);
				}
			}
 			con.setAutoCommit (false);
 			pStmt.executeUpdate();
 			con.commit();
 			sqlPrmArray = null;

 		}catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception removing authentication log entry: " + ex.toString());
		}
	}
	chrgIncRowCount=inputDoc.getFragmentsList("/In/Transaction/FeeList").size();
	if (chrgIncRowCount == 0){
		String deleteSql = "delete from fee where p_transaction_oid = ?";

 		try(Connection con = DatabaseQueryBean.connect(false);
 			PreparedStatement pStmt = con.prepareStatement(deleteSql)) {
 			
 			if (inputDoc.getAttribute("/In/Transaction/transaction_oid") != null) {
				pStmt.setObject(1, inputDoc.getAttribute("/In/Transaction/transaction_oid"));
			} else {
				pStmt.setNull(1, Types.OTHER);
			}
 			con.setAutoCommit (false);
 			pStmt.executeUpdate();
 			con.commit();
 			
 		}catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception removing authentication log entry: " + ex.toString());
		}
	}

}
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
/**
 * We check for a value at a given path in the Xml Doc, if it
 * Does'nt exist then we set that value in the document to 'N'.  This is used for
 * resetting a Check boxes value since deselection of a check box does NOT update the
 * Xml Doc with a revised value.
 *
 * @param DocumentHandler inputDoc - the document to get and set data against.
 * @param String xmlPath           - this the the path to use to get/set
 *                                   attributes in the xml Doc.
 */
private static void resetCheckBoxAttribute( DocumentHandler inputDoc, String xmlPath) {

    String value;

    value = inputDoc.getAttribute( xmlPath );
    if (value == null)
        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
}
//Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
private void setDateValues(DocumentHandler inputDoc) {

	String date;
	String newDate;

	// get the input document 
	if (inputDoc == null) {
		LOG.info("Input document to IMP_DLCPreEdit is null");
		return;
	}

	// Need to convert all three part date fields into single date
	// values.

	date = inputDoc.getAttribute("/In/Terms/expiry_date");	
	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	inputDoc.setAttribute("/In/Terms/expiry_date", newDate);
	
	
	date = inputDoc.getAttribute("/In/Terms/guar_valid_from_date");	
	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	inputDoc.setAttribute("/In/Terms/guar_valid_from_date", newDate);
	
	
	date = inputDoc.getAttribute("/In/Terms/overseas_validity_date");	
	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	inputDoc.setAttribute("/In/Terms/overseas_validity_date", newDate);
	//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - Start
	date = inputDoc.getAttribute("/In/Terms/final_expiry_date");
	if(StringFunction.isNotBlank(date)){
	 newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	 inputDoc.setAttribute("/In/Terms/final_expiry_date", newDate);
	}
	//Narayan - 09-Jan-2014 - Rel9.0 CR-831 - End
 }


/**
 * This is a method that gets used a large number of times.  This is meant to localize
 * the code.  Basically we check for a value at a given path in the Xml Doc, if it
 * Does'nt exist then we set that value in the document to 'N'.  This is used for 
 * resetting a Check boxes value since deselection of a check box does NOT update the
 * Xml Doc with a revised value.
 *
 * @param DocumentHandler inputDoc - the document to get and set data against. 
 * @param String xmlPath           - this the the path to use to get/set 
 *                                   attributes in the xml Doc.
 */
private void resetCheckBox( DocumentHandler inputDoc, String xmlPath) {

    String value;
    
    value = inputDoc.getAttribute( xmlPath );
    
    if (value == null)
        inputDoc.setAttribute( xmlPath, TradePortalConstants.INDICATOR_NO );
}


}
