package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;

import javax.servlet.http.*;

import java.util.*;

import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(GenericTransactionPreEdit.class);

/**
 * Handles the conversion of the user-entered amount field for the user's 
 * locale.   The JSP submits what the user entered, and therefor expects
 * to always see that in the /In section of the XML.  This pre-mediator
 * stores the user entered amount in a temporary spot, then changes
 * the actual amount data to reflect the standard numerical representation
 * (no commas, locale-specific decimal points, etc).  
 * The corresponding post-mediator action places the user entered amount
 * back into the proper spot in the XML.
 * 
 * This pre-mediator action must be called for all transaction forms that 
 * have an amount field.
 */
 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
    doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
    doSettlementInstrPreMediatorAction( inputDoc, resMgr );
	
 } 

		/** Convert decimal number from its locale-specific entry format into
         * a standard format that can be understood by the database.
         * If the format is invalid, an error value will be placed into
         * the XML that will trigger an error later.
         * In the corresponding post mediator action, the passed-in value
         * is placed back into its original location.
         *
         * @param inputDoc DocumentHandler - the input document of the mediator
         * @param resMgr ResourceManager - used to determine locale of the user
         * @param attribute String - the decimal attribute being handled
         */
 public void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr, String attribute) 
  {
   	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to is null");
	   return;
	}

        String pathToDataUsedToPopulate = "/In/Terms/"+attribute;
        String pathToTemporaryStorage = "/In/Terms/userEntered"+attribute;

        // Place the user entered amount into a temporary location in the XML
        // The post-mediator action copies /In/Terms/userEntered[attribute] back into /In/Terms/[attribute]
        if(inputDoc.getAttribute(pathToDataUsedToPopulate) != null)
           inputDoc.setAttribute(pathToTemporaryStorage, inputDoc.getAttribute(pathToDataUsedToPopulate));
        else
           return;


        String amount;
 
        try 
         {
            // Convert the amount from its localized form to en_US form
            amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToDataUsedToPopulate), resMgr.getResourceLocale(), true);
         }
        catch(InvalidAttributeValueException iae)
         {
            // If the format was invalid, set amount equal to something that will trigger an error
            // to be issued.
            amount = TradePortalConstants.BAD_AMOUNT;
         } 

        // Place the result of the above code into the amount location
        // This is what will be populated into the business object.
        inputDoc.setAttribute(pathToDataUsedToPopulate, amount);
  }
	
	public void doDecimalPreMediatorActionForDomesticAmt(DocumentHandler inputDoc, ResourceManager resMgr, String attribute) 
	  {	
	   	// get the input document 
		if (inputDoc == null)
		{
		   LOG.info("Input document to is null");
		   return;
		}

	        String pathToDataUsedToPopulate = "/In/DomesticPayment/"+attribute;
	        String pathToTemporaryStorage = "/In/DomesticPayment/userEntered"+attribute;

	        // Place the user entered amount into a temporary location in the XML
	        // The post-mediator action copies /In/Terms/userEntered[attribute] back into /In/Terms/[attribute]
	        if(inputDoc.getAttribute(pathToDataUsedToPopulate) != null)
	           inputDoc.setAttribute(pathToTemporaryStorage, inputDoc.getAttribute(pathToDataUsedToPopulate));
	        else
	           return;


	        String amount;
	 
	        try 
	         {
	            // Convert the amount from its localized form to en_US form
	            amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToDataUsedToPopulate), resMgr.getResourceLocale(), true);
	         }
	        catch(InvalidAttributeValueException iae)
	         {
	            // If the format was invalid, set amount equal to something that will trigger an error
	            // to be issued.
	            amount = TradePortalConstants.BAD_AMOUNT;
	         } 

	        // Place the result of the above code into the amount location
	        // This is what will be populated into the business object.
	        inputDoc.setAttribute(pathToDataUsedToPopulate, amount);
			
	  }
	
    /**
     * 
     * @param inputDoc
     * @throws AmsException 
     */
    private void doSettlementInstrPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr ) throws AmsException {

		String newDate = inputDoc.getAttribute("/In/Terms/apply_payment_on_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/apply_payment_on_date", newDate);
		
		newDate = inputDoc.getAttribute("/In/Terms/fin_roll_maturity_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/fin_roll_maturity_date", newDate);
		
		doDecimalPreMediatorAction(inputDoc, resMgr, "fin_roll_partial_pay_amt");
		doDecimalPreMediatorAction(inputDoc, resMgr, "settle_fec_rate");
		
		// if 'For Full Amount' option is selected, then account details are not required.
		if ( TradePortalConstants.SETTLEMENT_FIN_ROLL_FULL_PARTIAL_TYPE_FULL.equals(inputDoc.getAttribute("/In/Terms/fin_roll_full_partial_ind")) ) {
			inputDoc.setAttribute("/In/Terms/account_remit_ind", null);
			inputDoc.setAttribute("/In/Terms/principal_account_oid", null);
			inputDoc.setAttribute("/In/Terms/charges_account_oid", null);
			inputDoc.setAttribute("/In/Terms/interest_account_oid", null);
		}
		// if 'Pay Full Amount by selection an option to right' is selected, then other option 'Finance Terms', 'For Full OR Patial OR Rollover' optioned are not
		// required.
		if ( TradePortalConstants.SETTLEMENT_PAY_FULL_FIN_ROLL_TYPE_PAY_FULL.equals(inputDoc.getAttribute("/In/Terms/pay_in_full_fin_roll_ind")) ) {
			inputDoc.setAttribute("/In/Terms/fin_roll_full_partial_ind", null);
			inputDoc.setAttribute("/In/Terms/rollover_terms_ind", null);
		}
		
		String otherAddlInstructionsInd = inputDoc.getAttribute("/In/Terms/other_addl_instructions_ind");
		if ( StringFunction.isBlank(otherAddlInstructionsInd) ) {
			inputDoc.setAttribute("/In/Terms/other_addl_instructions_ind", TradePortalConstants.INDICATOR_NO);
		}
		
   }

}