package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;

import javax.servlet.http.*;

import java.util.*;

import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;

public class LRQPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(LRQPreEdit.class);
	/**
	 * LRQPreEdit constructor comment.
	 */
	public LRQPreEdit() {
		super();
	}

	/**
	 * Performs different logic based on the type of transaction (ISS or AMD)
	 *
	 * But basically converts the month, day, year fields for each of the
	 * dates into single date fields.  This allows populateFromXml to work.
	 *
	 * In addition, explicitly sets all 'missing' checkbox fields to N.
	 * (Checkbox fields which are not checked are not sent in the document,
	 * we must force an N to cause unchecked values to be save correctly.)
	 *
	 * Also, perform logic to determine the account number and currency
	 * selected for the beneficiary -- this may come from a radio button
	 * value or an enterable field.
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{

		// Retrieve the input document
		if (inputDoc == null)
		{
			LOG.info("Input document to LRQPreEdit is null");

			return;
		}
		

		// Convert decimal number from its locale-specific entry format into
		// a standard format that can be understood by the database.
		// If the format is invalid, an error value will be placed into
		// the XML that will trigger an error later.
		// In the corresponding post mediator action, the passed-in value
		// is placed back into its original location.
		doDecimalPreMediatorAction(inputDoc, resMgr, "amount");
		doDecimalPreMediatorAction(inputDoc, resMgr, "loan_proceeds_pmt_amount_imp");
		doDecimalPreMediatorAction(inputDoc, resMgr, "loan_proceeds_pmt_amount_exp");
		doDecimalPreMediatorAction(inputDoc, resMgr, "loan_proceeds_pmt_amount_rec");
        doDecimalPreMediatorAction(inputDoc, resMgr, "loan_proceeds_pmt_amount_trd"); //RKAZI CR709 Rel 8.2 01/25/2013
		doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
		doDecimalPreMediatorAction(inputDoc, resMgr, "maturity_fec_amount");

		doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");
		doDecimalPreMediatorAction(inputDoc, resMgr, "maturity_fec_rate");

		doLRQPreEdit(inputDoc, resMgr);
		
		//CR-1026
		SessionWebBean sessionWebBean = (SessionWebBean)request.getSession().getAttribute("userSession");
		
		if(sessionWebBean.isCustNotIntgTPS()){
			clearSellerDetails(inputDoc);
		}
		  
		  
	}

	/**
	 * This method is used for initializing several Loan Request default
	 * settings, in the event that they were not set by the user at all.
	 *
	 * @param       inputDoc   DocumentHandler - the current input xml doc
	 * @return      DocumentHandler - the current input xml doc with initialized attributes
	 * @exception   com.amsinc.ecsg.frame.AmsException
	 */
	private void doLRQPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)throws com.amsinc.ecsg.frame.AmsException
	{

		// IR-AOUH021360248 - pcutrone - 22-FEB-07 - [BEGIN]-Wrapped code with if statement for Part Number
		String partNumber;
		// get the part number of the page being looked at (if one exists)
		partNumber = inputDoc.getAttribute("/In/partNumber");

		// IR T36000015310, Part number is not relevant any more
		
		//if (partNumber.equals("") || partNumber.equals(TradePortalConstants.PART_TWO))
		//{
		
		String newDate;

		newDate = inputDoc.getAttribute("/In/Terms/loan_start_date");
		//newDate = TPDateTimeUtility.buildJPylonDateTime(newDate);
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/loan_start_date", newDate);


		newDate	 = inputDoc.getAttribute("/In/Terms/loan_terms_fixed_maturity_dt");
		//newDate = TPDateTimeUtility.buildJPylonDateTime(newDate);
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/loan_terms_fixed_maturity_dt", newDate);


		newDate  = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
		//newDate = TPDateTimeUtility.buildJPylonDateTime(newDate);
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

		newDate  = inputDoc.getAttribute("/In/Terms/maturity_fec_maturity_date");
		//newDate = TPDateTimeUtility.buildJPylonDateTime(newDate);
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(newDate);
		inputDoc.setAttribute("/In/Terms/maturity_fec_maturity_date", newDate);

		if (inputDoc.getAttribute("/In/Terms/use_mkt_rate") == null)
		{
			inputDoc.setAttribute("/In/Terms/use_mkt_rate", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/use_fec") == null)
		{
			inputDoc.setAttribute("/In/Terms/use_fec", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/use_other") == null)
		{
			inputDoc.setAttribute("/In/Terms/use_other", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/maturity_use_mkt_rate") == null)
		{
			inputDoc.setAttribute("/In/Terms/maturity_use_mkt_rate", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/maturity_use_fec") == null)
		{
			inputDoc.setAttribute("/In/Terms/maturity_use_fec", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/maturity_use_other") == null)
		{
			inputDoc.setAttribute("/In/Terms/maturity_use_other", TradePortalConstants.INDICATOR_NO);
		}
		if (inputDoc.getAttribute("/In/Terms/financing_backed_by_buyer_ind") == null)
		{
			inputDoc.setAttribute("/In/Terms/financing_backed_by_buyer_ind", TradePortalConstants.INDICATOR_NO);
		}

		// The page has Import, Export, and Receivables Financing section
		String importIndicator = inputDoc.getAttribute("/In/Instrument/import_indicator");
		if (importIndicator != null) {
			String impExpAttributes[] = {"/In/Terms/loan_proceeds_pmt_curr", "/In/Terms/loan_proceeds_pmt_amount",
					"/In/Terms/loan_proceeds_credit_type", "/In/Terms/multiple_related_instruments",
					"/In/Terms/FirstTermsParty/terms_party_oid", "/In/Terms/FirstTermsParty/terms_party_type",
					"/In/Terms/FirstTermsParty/name", "/In/Terms/FirstTermsParty/address_line_1",
					"/In/Terms/FirstTermsParty/address_line_2", "/In/Terms/FirstTermsParty/address_city",
					"/In/Terms/FirstTermsParty/address_state_province", "/In/Terms/FirstTermsParty/address_country",
					"/In/Terms/FirstTermsParty/address_postal_code", "/In/Terms/FirstTermsParty/OTL_customer_id",
					"/In/Terms/FirstTermsParty/entered_account", "/In/Terms/FirstTermsParty/entered_currency",
					"/In/Terms/FirstTermsParty/selected_account", "/In/Terms/ThirdTermsParty/terms_party_oid",
					"/In/Terms/ThirdTermsParty/terms_party_type", "/In/Terms/ThirdTermsParty/name",
					"/In/Terms/ThirdTermsParty/address_line_1", "/In/Terms/ThirdTermsParty/address_line_2",
					"/In/Terms/ThirdTermsParty/address_city", "/In/Terms/ThirdTermsParty/address_country",
					"/In/Terms/ThirdTermsParty/address_state_province", "/In/Terms/ThirdTermsParty/address_postal_code",
					"/In/Terms/ThirdTermsParty/OTL_customer_id", "/In/Terms/loan_maturity_debit_type",
					"/In/Terms/loan_maturity_debit_other_txt","/In/Terms/loan_proceeds_credit_other_txt",
					"/In/Terms/loan_proceeds_credit_acct","/In/Terms/loan_maturity_debit_acct",
					"/In/Terms/FirstTermsParty/vendor_id", "/In/Terms/finance_type_text", "/In/Terms/financed_invoices_details_text"};
			if (importIndicator.equals(TradePortalConstants.INDICATOR_YES)) {
				for (int i = 0 ; i<impExpAttributes.length; i++) {
					inputDoc.setAttribute(impExpAttributes[i], inputDoc.getAttribute(impExpAttributes[i]+"_imp"));
				}
				inputDoc.setAttribute("/In/Terms/linked_instrument_id", "");
				inputDoc.setAttribute("/In/Terms/finance_type", "");
			}
			else if (importIndicator.equals(TradePortalConstants.INDICATOR_NO)) {
				for (int i = 0 ; i<impExpAttributes.length; i++) {
					inputDoc.setAttribute(impExpAttributes[i], inputDoc.getAttribute(impExpAttributes[i]+"_exp"));
				}
				//pcutrone - 04-MAR-07 - IR-NBUH020756607 - related instrument error
				inputDoc.setAttribute("/In/Terms/related_instrument_id", "");
			}
			else if (importIndicator.equals(TradePortalConstants.INDICATOR_X)) {
				/*  DONT DO ANYTHINBG YET FOR RECEIVABLES */
				for (int i = 0 ; i<impExpAttributes.length; i++) {
					inputDoc.setAttribute(impExpAttributes[i], inputDoc.getAttribute(impExpAttributes[i]+"_rec"));
				}
				//pcutrone - 04-MAR-07 - IR-NBUH020756607 - related instrument error
				inputDoc.setAttribute("/In/Terms/related_instrument_id", "");
				/* */
			}
			//RKAZI CR709 Rel 8.2 01/25/2013 -Start
            else if (importIndicator.equals(TradePortalConstants.INDICATOR_T)) {
				/*  DONT DO ANYTHINBG YET FOR RECEIVABLES */
                for (int i = 0 ; i<impExpAttributes.length; i++) {
                    inputDoc.setAttribute(impExpAttributes[i], inputDoc.getAttribute(impExpAttributes[i]+"_trd"));
                }
                //pcutrone - 04-MAR-07 - IR-NBUH020756607 - related instrument error
                inputDoc.setAttribute("/In/Terms/related_instrument_id", "");
				/* */
            }

//			}// END of IR T36000015310, Part number is not relevant any more
			
				//RKAZI CR709 Rel 8.2 01/25/2013 - End
			String financeType = inputDoc.getAttribute("/In/Terms/finance_type");
			String linkedInstrumentID;
			if (financeType != null) {
				if (financeType.equals(InstrumentType.EXPORT_DLC)) {
					inputDoc.setAttribute("/In/Terms/linked_instrument_id", inputDoc.getAttribute("/In/Terms/linked_instrument_id_dlc"));
				}
				else if (financeType.equals(InstrumentType.EXPORT_COL)) {
					inputDoc.setAttribute("/In/Terms/linked_instrument_id", inputDoc.getAttribute("/In/Terms/linked_instrument_id_col"));
				}
				else {
					inputDoc.setAttribute("/In/Terms/linked_instrument_id", "");
				}
			}

			String acctNum = "";
			String acctCcy = "";
			String value;

			// Save the selected beneficiary account/ccy.  This will either be one of the
			// predefined accounts or a user enterable value.  For the predefined accts,
			// the radio button value consists of the acct number and currency (separated
			// by DELIMITER_CHAR, e.g., Acct1234DELIMITER_CHARUSD).  For the entered value,
			// pull the values from the enterable account number and currency fields.

			value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");
			if (TradePortalConstants.USER_ENTERED_ACCT.equals(value) || (value == null) || (value.equals(""))) {

				// The user entered an account number and currency.  Move these values to
				// the real object/database field.  (If value is null it means that either
				// the user didn't select a radio button or the radio buttons are not
				// available and only the entereable fields are shown.  In either case,
				// take what was entered.  We'll catch errors later as part of required value
				// editing.

				acctNum = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_account");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);

				acctCcy = inputDoc.getAttribute("/In/Terms/FirstTermsParty/entered_currency");
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);
			} else {
				// The user selected one of the predefined accounts.  Parse the radio button
				// value into account number and currency and place in the real object/db field.
				value = inputDoc.getAttribute("/In/Terms/FirstTermsParty/selected_account");

				if (value != null) {
					// selected_account (value) can be null in two cases.  one is if the user
					// doesn't select the radio button.  two is when there is no radio button
					// to select.
					int delimeterAt = value.indexOf(TradePortalConstants.DELIMITER_CHAR);
					acctNum = value.substring(0,delimeterAt);
					acctCcy = value.substring(delimeterAt+TradePortalConstants.DELIMITER_CHAR.length(),
							value.length());

					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", acctNum);
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_currency", acctCcy);
				}
			}

			value = inputDoc.getAttribute("/In/Terms/FourthTermsParty/selected_account");
			if (TradePortalConstants.USER_ENTERED_ACCT.equals(value) || (value == null) || (value.equals(""))) {

				// The user entered an account number and currency.  Move these values to
				// the real object/database field.  (If value is null it means that either
				// the user didn't select a radio button or the radio buttons are not
				// available and only the entereable fields are shown.  In either case,
				// take what was entered.  We'll catch errors later as part of required value
				// editing.

				acctNum = inputDoc.getAttribute("/In/Terms/FourthTermsParty/entered_account");
				inputDoc.setAttribute("/In/Terms/FourthTermsParty/acct_num", acctNum);

				acctCcy = inputDoc.getAttribute("/In/Terms/FourthTermsParty/entered_currency");
				inputDoc.setAttribute("/In/Terms/FourthTermsParty/acct_currency", acctCcy);
			} else {
				// The user selected one of the predefined accounts.  Parse the radio button
				// value into account number and currency and place in the real object/db field.
				value = inputDoc.getAttribute("/In/Terms/FourthTermsParty/selected_account");

				if (value != null) {
					// selected_account (value) can be null in two cases.  one is if the user
					// doesn't select the radio button.  two is when there is no radio button
					// to select.
					int delimeterAt = value.indexOf(TradePortalConstants.DELIMITER_CHAR);
					acctNum = value.substring(0,delimeterAt);
					acctCcy = value.substring(delimeterAt+TradePortalConstants.DELIMITER_CHAR.length(),
							value.length());

					inputDoc.setAttribute("/In/Terms/FourthTermsParty/acct_num", acctNum);
					inputDoc.setAttribute("/In/Terms/FourthTermsParty/acct_currency", acctCcy);
				}
			}
			
			// IR-T36000031438 Begin
			
			value = inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_acct_num_selected");
			if (TradePortalConstants.USER_ENTERED_ACCT.equals(value) || (value == null) || (value.equals(""))) {

				// The user entered an account number and currency.  Move these values to
				// the real object/database field.  (If value is null it means that either
				// the user didn't select a radio button or the radio buttons are not
				// available and only the entereable fields are shown.  In either case,
				// take what was entered.  We'll catch errors later as part of required value
				// editing.

				acctNum = inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_acct_num_entered");
				inputDoc.setAttribute("/In/InvoicePaymentInstructions/ben_acct_num", acctNum);

			} else {
				// The user selected one of the predefined accounts.  Parse the radio button
				// value into account number and currency and place in the real object/db field.
				value = inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_acct_num_selected");

				if (value != null) {
					// selected_account (value) can be null in two cases.  one is if the user
					// doesn't select the radio button.  two is when there is no radio button
					// to select.
					int delimeterAt = value.indexOf(TradePortalConstants.DELIMITER_CHAR);
					acctNum = value.substring(0,delimeterAt);
					inputDoc.setAttribute("/In/InvoicePaymentInstructions/ben_acct_num", acctNum);
				}
			}
			
			// IR T36000031438 End
			
			// DK IR T36000014775  Rel8.2 04/02/2013 Begins
			String loanProceedsCreditType = inputDoc.getAttribute("/In/Terms/loan_proceeds_credit_type"); 
			if (!inputDoc.getAttribute("/In/InvoicePaymentInstructions").equals(null) && !TradePortalConstants.CREDIT_MULTI_BEN_ACCT.equals(loanProceedsCreditType) && TradePortalConstants.INDICATOR_T.equals(inputDoc.getAttribute("/In/Instrument/import_indicator"))){
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/name", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_name"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_line_1", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_address_one"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_line_2", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_address_two"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_line_3", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_address_three"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_city", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_city"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_country", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_country"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/acct_num", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_acct_num"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/OTL_customer_id", inputDoc.getAttribute("/In/InvoicePaymentInstructions/Ben_OTL_customer_id_trd"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/central_bank_rep1", inputDoc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep1"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/central_bank_rep2", inputDoc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep2"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/central_bank_rep3", inputDoc.getAttribute("/In/InvoicePaymentInstructions/central_bank_rep3"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/customer_reference", inputDoc.getAttribute("/In/InvoicePaymentInstructions/customer_reference"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/terms_party_type", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_terms_party_type_trd"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/terms_party_oid", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_terms_party_oid_trd"));
				inputDoc.setAttribute("/In/Terms/payment_method", inputDoc.getAttribute("/In/InvoicePaymentInstructions/pay_method"));
				inputDoc.setAttribute("/In/Terms/bank_charges_type", inputDoc.getAttribute("/In/InvoicePaymentInstructions/payment_charges"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/reporting_code_1", inputDoc.getAttribute("/In/InvoicePaymentInstructions/reporting_code_1"));
				inputDoc.setAttribute("/In/Terms/FirstTermsParty/reporting_code_2", inputDoc.getAttribute("/In/InvoicePaymentInstructions/reporting_code_2"));
				String loanProceedsCurrency = inputDoc.getAttribute("/In/InvoicePaymentInstructions/amount_currency_code");
				if (StringFunction.isNotBlank(loanProceedsCurrency)){
					inputDoc.setAttribute("/In/Terms/amount_currency_code", loanProceedsCurrency);
				}
				String loanProceedsAmount = NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute("/In/InvoicePaymentInstructions/amount"), resMgr.getResourceLocale(), true);
				if (StringFunction.isNotBlank(loanProceedsAmount)){
					inputDoc.setAttribute("/In/Terms/amount", loanProceedsAmount);
				}
				
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/name", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_name"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_line_1", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_address1"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_line_2", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_address2"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/branch_code", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_code"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_country", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_branch_country"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_city", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_city"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/OTL_customer_id", inputDoc.getAttribute("/In/InvoicePaymentInstructions/Bbk_OTL_customer_id_trd"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_state_province", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_prvnce"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/terms_party_type", inputDoc.getAttribute("/In/InvoicePaymentInstructions/bbk_terms_party_type_trd"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/terms_party_oid", inputDoc.getAttribute("/In/InvoicePaymentInstructions/bbk_terms_party_oid_trd"));
				inputDoc.setAttribute("/In/Terms/ThirdTermsParty/ben_bank_sort_code", inputDoc.getAttribute("/In/InvoicePaymentInstructions/ben_bank_sort_code"));//CR 709A
				

			}
			// DK IR T36000014775  Rel8.2 04/02/2013 Ends

		}
		// IR-AOUH021360248 - pcutrone - 22-FEB-07 - [END]-Wrapped code with if statement for Part Number

		// Nar CR 709  Start
		String pressedButton = inputDoc.getAttribute("/In/Update/ButtonPressed");
		
		if(TradePortalConstants.BUTTON_CALC_DIST_AND_INTRST.equals(pressedButton)){
			if(!isRequiredDataPresent(inputDoc)){ 			  			  			  						
			  MediatorServices medService = new MediatorServices();
			  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.LOAN_DETAIL_REQ_TO_CALC_DIST_AND_INST);
			  medService.addErrorInfo();
		      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			  return;
			}
		}
		// Nar CR 709  End

	}

	/**
	 * This method is used to check whether user entered all required data to calculate Discount and Interest
	 * amount for Trade Loan for Payable and receivables.
	 * 
	 * @return boolean true if : 'Ccy of Loan', 'Interest to be Paid' , 'Loan Terms',
	 *                         (at 'fixed maturity date' or xx days from the Loan Start Date') and amount.
	 *                 false if required value not present to perform interest and discount calculation
	 * @param inputDoc
	 */
	
	private boolean isRequiredDataPresent(DocumentHandler inputDoc){
		  boolean isCurrencyEntered     = false;
		  boolean isAmountEntered       = false;
		  boolean isInterestPaidEntered = false;
		  boolean isLoanTermDateEntered = false;
		  boolean isLoanTermDaysEntered = false;			  
		  
		  if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/amount_currency_code"))){
			  isCurrencyEntered = true; 
		  }
		  
		  if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/amount"))){
			  isAmountEntered = true;
		  }
		  
		  if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/interest_to_be_paid"))){
			  isInterestPaidEntered = true; 
		  }
		  
		  if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/loan_terms_fixed_maturity_dt"))){
			  isLoanTermDateEntered = true; 
		  }
		  
		  if(StringFunction.isNotBlank(inputDoc.getAttribute("/In/Terms/loan_terms_number_of_days"))){
			  isLoanTermDaysEntered = true; 
		  }		  
		  
		  return isCurrencyEntered && isAmountEntered && isInterestPaidEntered && (isLoanTermDateEntered || isLoanTermDaysEntered);
	}
	/*
	 * CR-1026
	 * This method is used to clear seller details in the Buyer-Requested Financing Transaction, when customer is NOT 
	 * integrated with TPS. For this type of customer, the default loan type is Invoice Financing with default type of transaction
	 * being financed is  Receivables-Financing.
	 * 
	 */
	private void clearSellerDetails(DocumentHandler inputDoc)throws com.amsinc.ecsg.frame.AmsException{
		
		String importIndicator = inputDoc.getAttribute("/In/Instrument/import_indicator");
		if (importIndicator != null) {			
			if(TradePortalConstants.INDICATOR_X.equals(importIndicator)){
				if(TradePortalConstants.BUYER_REQUESTED_FINANCING.equals("/In/Terms/finance_type")){

					inputDoc.setAttribute("/In/Terms/FirstTermsParty/name_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_line_1_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_line_2_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_city_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_state_province_rec", "");	
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_country_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/address_postal_code_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/OTL_customer_id_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/vendor_id_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/terms_party_oid_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/terms_party_type_rec", "");
					inputDoc.setAttribute("/In/Terms/FirstTermsParty/OTL_customer_id_rec", "");
					

					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/name_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_line_1_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_line_2_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_city_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_state_province_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_country_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/address_postal_code_rec", "");
					if (inputDoc.getAttribute("/In/Terms/financing_backed_by_buyer_ind") != null)
					{
						inputDoc.setAttribute("/In/Terms/financing_backed_by_buyer_ind", TradePortalConstants.INDICATOR_NO);
					}
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/terms_party_type_rec", "");						
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/terms_party_oid_rec", "");
					inputDoc.setAttribute("/In/Terms/ThirdTermsParty/OTL_customer_id_rec", "");

				}

			}
		}

	}

}