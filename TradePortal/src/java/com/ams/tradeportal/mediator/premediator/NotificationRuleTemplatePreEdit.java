package com.ams.tradeportal.mediator.premediator;

/**
 * Premediator for PartyDetail.  Handles Account List.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import java.rmi.*;

/**
 * This pre edit class formats inputDoc in order to process by Bean

 * @version 0.1
 */
public class NotificationRuleTemplatePreEdit implements com.amsinc.ecsg.web.WebAction
{
	/**
	 * NotificationRuleTemplatePreEdit constructor comment.
	 */
	public NotificationRuleTemplatePreEdit()
	{
		super();
	}


	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc) throws AmsException
			{

		// Retrieve the input document
		if (inputDoc == null)
		{
			System.out.println("Input document to NotificationRuleTemplatePreEdit is null");

			return;
		}
		System.out.println("Raw InputDoc:"+inputDoc); 

		int iLoop;
		Vector criterionVector = inputDoc.getFragments("/In/NotificationRuleTemplate/NotificationRuleCriterionList");
		if(criterionVector != null && criterionVector.size()>0) {
			int numItems = criterionVector.size();
			System.out.println(numItems);		   
			int rLoop=0;
			int dLoop = 0;
			int delLoop = 0;
			inputDoc.setAttribute("/In/NotificationRuleTemplate/opt_lock", "");
			DocumentHandler tempDoc = new DocumentHandler();
			for (iLoop=0; iLoop<=numItems; iLoop++)
			{
				String oid = inputDoc.getAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/criterion_oid");
				String sendEmailSetting = inputDoc.getAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/send_email_setting");
				String sendNotifSetting = inputDoc.getAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/send_notif_setting");
				String freqSetting = inputDoc.getAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/notify_email_freq");
				System.out.print("oid ():"+oid+"\t sendEmailSetting:"+sendEmailSetting+"\t notfi setting:"+sendNotifSetting+"\t freqSetting:"+freqSetting);
				if(StringFunction.isNotBlank(freqSetting) && TradePortalConstants.INDICATOR_NO.equals(sendEmailSetting)){
					 inputDoc.setAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/notify_email_freq","");
				}
				if(StringFunction.isNotBlank(sendEmailSetting) || StringFunction.isNotBlank(sendNotifSetting)){

					if(StringFunction.isNotBlank(oid) && "0".equals(oid)){
						inputDoc.setAttribute("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")/criterion_oid", "");
					}
					tempDoc.addComponent("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+rLoop+")",	
							inputDoc.getFragment("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")"));
					rLoop++;
					inputDoc.removeComponent("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")");

				}
				else if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)){	

					if(StringFunction.isNotBlank(oid) && !"0".equals(oid)) {						

						DocumentHandler deleteDoc = new DocumentHandler();
						deleteDoc.setAttribute("/Name", "NotificationRuleCriterion");
						deleteDoc.setAttribute("/Oid",oid);
						inputDoc.addComponent("/In/ComponentToDelete("+delLoop+")", deleteDoc);
						inputDoc.removeComponent("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")");
						delLoop++;	

					}else{	
						if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)) {
							inputDoc.removeComponent("/In/NotificationRuleTemplate/NotificationRuleCriterionList("+iLoop+")");

						}
					}								
				}
			}

			if(null != tempDoc.getFragment("/In/NotificationRuleTemplate")){
				inputDoc.addComponent("/In/NotificationRuleTemplate/", tempDoc.getFragment("/In/NotificationRuleTemplate"));
			}		    
		}			
		//instrument header setup start

		Vector defListVector = inputDoc.getFragments("/In/NotificationRuleTemplate/DefaultList");
		int defSize = defListVector.size();

		for (iLoop=1; iLoop<=defSize; iLoop++)
		{					    	
			String sendEmailSetting = inputDoc.getAttribute("/In/NotificationRuleTemplate/DefaultList("+iLoop+")/send_email_setting");
			String sendNotifSetting = inputDoc.getAttribute("/In/NotificationRuleTemplate/DefaultList("+iLoop+")/send_notif_setting");
			String applyToAllTran = inputDoc.getAttribute("/In/NotificationRuleTemplate/DefaultList("+iLoop+")/apply_to_all_tran");
			String clearToAllTran = inputDoc.getAttribute("/In/NotificationRuleTemplate/DefaultList("+iLoop+")/clear_to_all_tran");
			if(StringFunction.isBlank(sendEmailSetting) && StringFunction.isBlank(sendNotifSetting)){
				applyToAllTran = "";
			}
			inputDoc.setAttribute("/In/NotificationRuleTemplate/send_email_setting"+iLoop,sendEmailSetting);
			inputDoc.setAttribute("/In/NotificationRuleTemplate/send_notif_setting"+iLoop,sendNotifSetting);
			inputDoc.setAttribute("/In/NotificationRuleTemplate/apply_to_all_tran"+iLoop,applyToAllTran);
			inputDoc.setAttribute("/In/NotificationRuleTemplate/clear_to_all_tran"+iLoop,clearToAllTran);								
		}										

		//instrument header setup start
		System.out.println("final InputDoc:"+inputDoc);
	}

}
