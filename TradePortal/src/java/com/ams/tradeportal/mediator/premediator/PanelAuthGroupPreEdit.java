package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert the type's description here.
 *
 *     Copyright  © 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;

// import javax.servlet.ServletRequest; // PRUK010444171
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import com.ams.tradeportal.common.*;
// import com.ams.tradeportal.busobj.*; // PRUK010444171

//import java.rmi.*; // PRUK010444171

import com.ams.tradeportal.busobj.util.*;

public class PanelAuthGroupPreEdit implements com.amsinc.ecsg.web.WebAction
{
private static final Logger LOG = LoggerFactory.getLogger(PanelAuthGroupPreEdit.class);
	/**
	 * PanelAuthGroupPreEdit constructor comment.
	 */
	public PanelAuthGroupPreEdit()
	{
		super();
	}

	/**
	 *
	 */
	public void act(AmsServletInvocation reqInfo,
			BeanManager beanMgr,
			FormManager formMgr,
			ResourceManager resMgr,
			String serverLocation,
			HttpServletRequest request,
			HttpServletResponse response,
			Hashtable inputParmMap,
			DocumentHandler inputDoc) throws AmsException {
		
		LOG.debug("[PanelAuthGroupPreEdit.act()] "+"PanelAuthGroupPreEdit started");
		
		if (inputDoc == null)
		{
			System.err.println("[PanelAuthGroupPreEdit.act()] Input document to PanelAuthGroupPreEdit is null");
			return;
		}
		
		// jgadela - Rel8.3 ANZCR501 03/28/2013 START  - if user clicks APPROVE/REJECT button skip all the below validation parts.
		  String buttonPressed = inputDoc.getAttribute("/In/Update/ButtonPressed");
		  if (buttonPressed.equals(TradePortalConstants.BUTTON_APPROVE_PENDING_REF_DATA) 
		  			|| buttonPressed.equals(TradePortalConstants.BUTTON_REJECT_PENDING_REF_DATA)
		  			|| buttonPressed.equals(TradePortalConstants.BUTTON_DELETE)){
			  return;
		  }
		  //jgadela - Rel8.3 ANZCR501 03/28/2013 END.
		
		
		/*Check for indicators and set to NO if it is null start*/
		if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/panelgroupconfpaymentinst") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/panelgroupconfpaymentinst", TradePortalConstants.INDICATOR_NO);
		  }
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/ACH_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/ACH_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/BCHK_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/BCHK_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/BKT_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/BKT_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/CBFT_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/CBFT_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/CCHK_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/CCHK_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/RTGS_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/RTGS_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/FTBA_pymt_method_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/FTBA_pymt_method_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/matchin_approval_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/matchin_approval_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/inv_auth_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/inv_auth_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/credit_auth_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/credit_auth_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/AIR_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/AIR_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/ATP_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/ATP_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/EXP_COL_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/EXP_COL_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/EXP_DLC_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/EXP_DLC_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/EXP_OCO_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/EXP_OCO_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/IMP_DLC_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/IMP_DLC_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/LRQ_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/LRQ_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/GUA_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/GUA_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/SLC_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/SLC_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/RQA_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/RQA_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/SHP_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/SHP_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/SP_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/SP_ind", TradePortalConstants.INDICATOR_NO);
			  }

		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/DCR_ind") == null)
			  {
			     inputDoc.setAttribute("/In/PanelAuthorizationGroup/DCR_ind", TradePortalConstants.INDICATOR_NO);
			  }
		 
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/settlement_instr_ind") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/settlement_instr_ind", TradePortalConstants.INDICATOR_NO);
		  }
		 
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/import_col_ind") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/import_col_ind", TradePortalConstants.INDICATOR_NO);
		  }
		 
		 //Rel9.0 CR 913 Start
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/pay_mgm_inv_auth_ind") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/pay_mgm_inv_auth_ind", TradePortalConstants.INDICATOR_NO);
		  }
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/pay_upl_inv_auth_ind") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/pay_upl_inv_auth_ind", TradePortalConstants.INDICATOR_NO);
		  }
		 //Rel9.0 CR 913 End
		 //Rel9.2 CR 914A Start
		 if (inputDoc.getAttribute("/In/PanelAuthorizationGroup/pay_credit_note_auth_ind") == null)
		  {
		     inputDoc.setAttribute("/In/PanelAuthorizationGroup/pay_credit_note_auth_ind", TradePortalConstants.INDICATOR_NO);
		  }
		 //Rel9.2 CR 914A End
		/*Check for indicators and set to NO if it is null end*/
		
		DocumentNode node = null;
		String id = "";
		Vector panelAuthList = new Vector();
		
		for(int i=1;i<7;i++){
			panelAuthList.addElement(inputDoc.getComponent("/In/PanelAuthorizationGroup/panel_range_"+i)) ;
		}
		int noOfItems = panelAuthList.size();

		
		int jLoop;
		String pathToDataUsedToPopulate = null;
		String pathToTemporaryStorage = null;
		int numberOfBlankMinMaxPairs = 0;
		/*Modified validation code to display range amount and rule number at error*/
		MediatorServices medServiceForPanels = new MediatorServices();
		for (jLoop=0; jLoop<noOfItems; jLoop++)
		{
			DocumentHandler panelAuthRangeDoc = (DocumentHandler) panelAuthList.elementAt(jLoop);

			String panelAuthOid = panelAuthRangeDoc.getAttribute("/panel_auth_range_oid");
			String panelAuthRangeMinAmount = panelAuthRangeDoc.getAttribute("/panel_auth_range_min_amt");
			String panelAuthRangeMaxAmount = panelAuthRangeDoc.getAttribute("/panel_auth_range_max_amt");
			Vector rules= panelAuthRangeDoc.getFragments("/PanelAuthorizationRuleList");
			int numberOfBlankRules =0;
			if(StringFunction.isBlank(panelAuthRangeMinAmount) && StringFunction.isBlank(panelAuthRangeMaxAmount)) {
					//inputDoc.removeComponent("/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1));
					numberOfBlankMinMaxPairs++;
			} else{ 
				
				/*IR#T36000021418 Start*/
				pathToDataUsedToPopulate = "/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/panel_auth_range_min_amt";
				pathToTemporaryStorage = "/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/userEnteredpanel_auth_range_min_amt";
				doDecimalPreMediatorAction(inputDoc, resMgr, pathToDataUsedToPopulate,pathToTemporaryStorage);

				pathToDataUsedToPopulate = "/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/panel_auth_range_max_amt";
				pathToTemporaryStorage = "/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/userEnteredpanel_auth_range_max_amt";
				doDecimalPreMediatorAction(inputDoc, resMgr, pathToDataUsedToPopulate,pathToTemporaryStorage);
				/*IR#T36000021418 End*/
				
				for(int i=0;i<rules.size();i++){
					DocumentHandler rule = (DocumentHandler) rules.elementAt(i);
					int aprroverscount=0;
					String approver_sequence=rule.getAttribute("/approver_sequence");
					String panel_auth_rule_oid=rule.getAttribute("/panel_auth_rule_oid");
					for (int j= 1; j < 21; j++) {
						if(! StringFunction.isBlank(rule.getAttribute("/approver_"+j)))
							aprroverscount++;
					}
					if(StringFunction.isBlank(approver_sequence) && aprroverscount==0 && StringFunction.isBlank(panel_auth_rule_oid)) {
						DocumentNode rulenode = rule.getDocumentNode ("/");
						String ruleid = "";
						if (!StringFunction.isBlank(rulenode.getIdName())) {
							ruleid = rulenode.getIdName();
						}
						inputDoc.removeComponent("/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/PanelAuthorizationRuleList("+ruleid+")");
						numberOfBlankRules++;
					}else if(StringFunction.isBlank(approver_sequence) && aprroverscount==0 && !StringFunction.isBlank(panel_auth_rule_oid)) {
						DocumentNode rulenode = rule.getDocumentNode ("/");
						String ruleid = "";
						if (!StringFunction.isBlank(rulenode.getIdName())) {
							ruleid = rulenode.getIdName();
						}
						DocumentHandler deleteDoc = new DocumentHandler();
						deleteDoc.setAttribute("/Name", "PanelAuthorizationRule");
						deleteDoc.setAttribute("/Oid", panel_auth_rule_oid);
						inputDoc.removeComponent("/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1)+"/PanelAuthorizationRuleList("+ruleid+")");
						inputDoc.addComponent("/In/ComponentToDelete("+jLoop+""+i+")", deleteDoc);
						numberOfBlankRules++;
					}else if(StringFunction.isBlank(approver_sequence) && aprroverscount>0) {
						/*Modified validation code to display range amount and rule number at error*/
						medServiceForPanels.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PANEL_RULE_SEQUENCE_APPROVER_CHECK,String.valueOf(i+1-numberOfBlankRules),panelAuthRangeMinAmount,panelAuthRangeMaxAmount);
						
					}else if(!StringFunction.isBlank(approver_sequence) && aprroverscount==0){
						/*Modified validation code to display range amount and rule number at error*/
						medServiceForPanels.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.PANEL_RULE_APPROVER_SEQUENCE_CHECK,String.valueOf(i+1-numberOfBlankRules),panelAuthRangeMinAmount,panelAuthRangeMaxAmount);
					}
					
					
				}
				/*Modified validation code to display range amount and rule number at error*/
				if(numberOfBlankRules == rules.size() && !StringFunction.isBlank(panelAuthRangeMinAmount)){
					medServiceForPanels.getErrorManager().issueError(
							  TradePortalConstants.ERR_CAT_1,
							  TradePortalConstants.PANEL_RANGE_RULES_CHECK,panelAuthRangeMinAmount,panelAuthRangeMaxAmount);
					
				}
				
			}
			
			if(numberOfBlankRules == 0 && StringFunction.isBlank(panelAuthRangeMinAmount) && StringFunction.isBlank(panelAuthRangeMaxAmount)){
				inputDoc.removeComponent("/In/PanelAuthorizationGroup/panel_range_"+(jLoop+1));
			}
			
			
			
		}
		
		if(numberOfBlankMinMaxPairs == noOfItems){
			medServiceForPanels.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PANEL_AMOUNT_RANGE_DOES_NOT_EXIST);
			
		}
		
		/*Modified validation code to display range amount and rule number at error*/
		if(medServiceForPanels.getErrorManager().getErrorCount()>0){
			medServiceForPanels.addErrorInfo();
		  	inputDoc.setComponent ("/Error", medServiceForPanels.getErrorDoc ());
		  	return;
		}
		
		}
		
		/*DocumentNode node = null;
		String id = "";
		Vector panelAuthList = null;
		
		for(int i=1;i<7;i++){
			panelAuthList.addElement(inputDoc.getDocumentNode("/In/PanelAuthorizationGroup/panel_range_"+i)) ;
		}
		int noOfItems = panelAuthList.size();

		
		int jLoop;
		String pathToDataUsedToPopulate = null;
		String pathToTemporaryStorage = null;
		int numberOfBlankMinMaxPairs = 0; 
		for (jLoop=0; jLoop<noOfItems; jLoop++)
		{
			DocumentHandler panelAuthRangeDoc = (DocumentHandler) panelAuthList.elementAt(jLoop);

			String panelAuthOid = panelAuthRangeDoc.getAttribute("/panel_auth_range_oid");
			String panelAuthRangeMinAmount = panelAuthRangeDoc.getAttribute("/panel_auth_range_min_amt");
			String panelAuthRangeMaxAmount = panelAuthRangeDoc.getAttribute("/panel_auth_range_max_amt");
			boolean blankFound = false;
			

			if(blankFound && StringFunction.isBlank(panelAuthRangeMinAmount) && StringFunction.isBlank(panelAuthRangeMaxAmount)) {
				numberOfBlankMinMaxPairs++;
				if (StringFunction.isBlank(panelAuthOid)) {
					
					node = panelAuthRangeDoc.getDocumentNode ("/");
					if (!StringFunction.isBlank(node.getIdName())) {
						id = node.getIdName();
					}
					inputDoc.removeComponent("/In/PanelAuthorizationGroup/PanelAuthorizationRangeList("+id+")");
					LOG.debug("[PanelAuthGroupPreEdit.act()] "+"PanelAuthGroupPreEdit removed " + id);
					numberOfBlankMinMaxPairs++;
				} else {
					DocumentHandler deleteDoc = new DocumentHandler();
					deleteDoc.setAttribute("/Name", "PanelAuthorizationRange");
					deleteDoc.setAttribute("/Oid", panelAuthOid);
					inputDoc.removeComponent("/In/PanelAuthorizationGroup/PanelAuthorizationRangeList("+jLoop+")");
					inputDoc.addComponent("/In/ComponentToDelete("+jLoop+")", deleteDoc);
					LOG.info("PanelAuthGroupPreEdit removed doc " + jLoop);
					numberOfBlankMinMaxPairs++;
				}
			} else { 
				node = panelAuthRangeDoc.getDocumentNode ("/");
				if (!StringFunction.isBlank(node.getIdName())) {
					id = node.getIdName();
				}
				pathToDataUsedToPopulate = "/In/PanelAuthorizationGroup/panel_range_"+jLoop+1+"/panel_auth_range_min_amt";
				pathToTemporaryStorage = "/In/PanelAuthorizationGroup/panel_range_"+jLoop+1+"/userEnteredpanel_auth_range_min_amt";

				doDecimalPreMediatorAction(inputDoc, resMgr, pathToDataUsedToPopulate,pathToTemporaryStorage);

				pathToDataUsedToPopulate = "/In/PanelAuthorizationGroup/panel_range_"+jLoop+1+"/panel_auth_range_max_amt";
				pathToTemporaryStorage = "/In/PanelAuthorizationGroup/panel_range_"+jLoop+1+"/userEnteredpanel_auth_range_max_amt";

				
				doDecimalPreMediatorAction(inputDoc, resMgr, pathToDataUsedToPopulate,pathToTemporaryStorage);
			}				
		}
		
		}
		LOG.debug("[PanelAuthGroupPreEdit.act()] "
				+"numberOfBlankMinMaxPairs after for-loop is "+numberOfBlankMinMaxPairs);
		if(numberOfBlankMinMaxPairs == noOfItems)
		{
			MediatorServices medService = new MediatorServices();
			medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.PANEL_AMOUNT_RANGE_DOES_NOT_EXIST);
			medService.addErrorInfo();
			inputDoc.setComponent ("/Error", medService.getErrorDoc ());
			
			return;
		}*/
		
		
	

	/** Convert decimal number from its locale-specific entry format into
	 * a standard format that can be understood by the database.
	 * If the format is invalid, an error value will be placed into
	 * the XML that will trigger an error later.
	 * In the corresponding post mediator action, the passed-in value
	 * is placed back into its original location.
	 *
	 * @param inputDoc DocumentHandler - the input document of the mediator
	 * @param resMgr ResourceManager - used to determine locale of the user
	 * @param attribute String - the decimal attribute being handled
	 */
	public void doDecimalPreMediatorAction(DocumentHandler inputDoc, ResourceManager resMgr,
			String pathToPopulate, String tempPath)
	{
		
		if (inputDoc == null)
		{
			
			System.err.println("Input document to is null");
			
			return;
		}


		if(inputDoc.getAttribute(pathToPopulate) != null)
			inputDoc.setAttribute(tempPath, inputDoc.getAttribute(pathToPopulate));
		else
			inputDoc.setAttribute(tempPath, "");

		
		LOG.debug("[PanelAuthGroupPreEdit.act()] "+"amount was " + inputDoc.getAttribute(pathToPopulate));
		
		String amount;

		try
		{
			// Convert the amount from its localized form to standard (en_US) form
			amount =  NumberValidator.getNonInternationalizedValue(inputDoc.getAttribute(pathToPopulate), resMgr.getResourceLocale(), true);
		}
		catch(InvalidAttributeValueException iae)
		{
			// If the format was invalid, set amount equal to something that will trigger an error
			// to be issued.
			amount = TradePortalConstants.BAD_AMOUNT;
		}

		// Place the result of the above code into the amount location
		// This is what will be populated into the business object.
		inputDoc.setAttribute(pathToPopulate, amount);

		LOG.debug("[PanelAuthGroupPreEdit.act()] "+"amount is " + amount);
		

	}
}
