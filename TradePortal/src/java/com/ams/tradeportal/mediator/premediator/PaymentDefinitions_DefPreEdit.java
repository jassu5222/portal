package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

import javax.servlet.http.*;
import java.util.Hashtable;


public class PaymentDefinitions_DefPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(PaymentDefinitions_DefPreEdit.class);

    /**
     * AdminUserPreEdit constructor comment.
     */
    public PaymentDefinitions_DefPreEdit() {
        super();
    }

    /**
     * Explicitly sets all 'missing' checkbox fields to N.
     * (Checkbox fields which are not checked are not sent in the document,
     * we must force an N to cause unchecked values to be save correctly.)
     */
    public void act(AmsServletInvocation reqInfo,
                    BeanManager beanMgr,
                    FormManager formMgr,
                    ResourceManager resMgr,
                    String serverLocation,
                    HttpServletRequest request,
                    HttpServletResponse response,
                    Hashtable inputParmMap,
                    DocumentHandler inputDoc)
            throws com.amsinc.ecsg.frame.AmsException {

        String value;

        // get the input document
        if (inputDoc == null) {
            LOG.info("Input document to PaymentDefinitions_DefPreEdit is null");
            return;
        }

        doPaymentDefPreEdit(inputDoc);
    }

    /**
     * Performs pre-editing of the input document that is specific to the
     * Payment Method Definition Detail JSP. Also, we need to explicitly set the
     * default checkbox value to N when it is not already present in the document.
     *
     * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
     */
    private void doPaymentDefPreEdit(DocumentHandler inputDoc) {

        // get the input document
        if (inputDoc == null) {
            LOG.info("Input document to PaymentDefinitions_PreEdit is null");
            return;
        }
        // handle amount size.
        String paymentAmountSize = inputDoc.getAttribute("/In/PaymentDefinitions/payment_amount_size");
        String[] amtSizeArr = paymentAmountSize.split(",");
        int amtSize = Integer.parseInt(amtSizeArr[0]) + 1 + Integer.parseInt(amtSizeArr[1]);
        inputDoc.setAttribute("/In/PaymentDefinitions/payment_amount_size",String.valueOf(amtSize));
        
        //MEer Rel 9.0 CR-921 FX Contract Rate size
        String foreignExchRateSize = inputDoc.getAttribute("/In/PaymentDefinitions/fx_contract_rate_size");
        String[] fxRateSizeArr = foreignExchRateSize.split(",");
        int fRateSize = Integer.parseInt(fxRateSizeArr [0]) + 1 + Integer.parseInt(fxRateSizeArr [1]);
        inputDoc.setAttribute("/In/PaymentDefinitions/fx_contract_rate_size",String.valueOf(fRateSize));
        

        // Checkboxes do not send values if the checkbox is unchecked.
        // Therefore, for each checkbox box, determine if we have a
        // value in the document.  If not, add an explicit N value for
        // each one that is missing.  This ensures that checkboxes that
        // are unchecked by the user get reset to N.


        // Checking Invoice Summary Deatil field required check box
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_method_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/debit_account_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_account_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bank_branch_code_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_currency_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_amount_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_date_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/include_column_headers"); // Nar IR-NNUM022937252 03/24/2012

        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/users_def" + x + "_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_req");
        }

        //Checking line item field required check box

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/customer_reference_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_country_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/execution_date_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address3_req");


        for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/prod_chars_ud" + x + "_type_req");
        }

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address4_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_fax_no_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_email_id_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/charges_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bank_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_addr1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_addr2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_city_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_prvnc_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_cntry_req");
        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/users_def" + x + "_data_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_data_req");
        }

        //Checking line item field required check box

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payable_location_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/print_location_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/delvry_mth_n_delvrto_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address2_req");

        for (int x = 1; x <= TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; x++) {
           // resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/prod_chars_ud" + x + "_type_data_req");
        }
        //Rel8.2 CR-741 Discount Code checkboxes reset - start
        //
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address3_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address4_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/details_of_payment_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/instruction_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_cd_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_name_req");

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_nm_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_addr1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_addr2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_city_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_prvnc_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_cntry_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep3_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/confidential_ind_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/indv_acct_entry_ind_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/header_identifier_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/file_reference_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_code_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/reporting_code1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/reporting_code2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/inv_details_header_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/inv_details_lineitm_req");

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_method_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/debit_account_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_account_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bank_branch_code_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_currency_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payment_amount_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/customer_reference_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_country_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/execution_date_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_address4_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_fax_no_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_email_id_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/charges_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bank_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_addr1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_addr2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_city_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_prvnc_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/bene_bnk_brnch_cntry_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/payable_location_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/print_location_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/delvry_mth_n_delvrto_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/mailing_address4_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/details_of_payment_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/instruction_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_cd_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_nm_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_addr1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_addr2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_city_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_prvnc_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/f_int_bnk_brnch_cntry_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/central_bank_rep3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/confidential_ind_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/indv_acct_entry_ind_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/header_identifier_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/file_reference_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/detail_identifier_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/beneficiary_code_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/reporting_code1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/reporting_code2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/inv_details_header_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/inv_details_lineitm_data_req");

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_method_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hdebit_account_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hconfidential_ind_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_currency_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hexecution_date_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hheader_identifier_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfile_reference_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hindv_acct_entry_ind_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_account_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcustomer_reference_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_branch_code_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fpayment_amount_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_country_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address3_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address4_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_fax_no_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_email_id_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcharges_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_city_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_prvnc_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_cntry_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fpayable_location_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fprint_location_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdelvry_mth_n_delvrto_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address3_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address4_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdetails_of_payment_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/finstruction_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cd_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_name_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_nm_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_city_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_prvnc_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cntry_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep3_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdetail_identifier_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_code_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code1_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code2_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_header_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_lineitm_req");

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_method_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hdebit_account_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hconfidential_ind_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_currency_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hexecution_date_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hheader_identifier_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfile_reference_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hindv_acct_entry_ind_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_account_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcustomer_reference_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_branch_code_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fpayment_amount_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_country_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address4_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_fax_no_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_email_id_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcharges_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_city_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_prvnc_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_cntry_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fpayable_location_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fprint_location_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdelvry_mth_n_delvrto_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address4_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdetails_of_payment_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/finstruction_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cd_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_name_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_nm_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_city_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_prvnc_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cntry_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fdetail_identifier_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_code_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_header_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_lineitm_data_req");
        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_data_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_data_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_data_req");
        }
        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_data_req");
        }

        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_bsb_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_account_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_reserved1_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_reserved2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_time_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fd_reserved3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/dr_withhold_amt_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/dr_reserved_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/br_reserved2_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/br_btch_cred_tot_amt_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/br_btch_deb_tot_amt_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/br_reserved3_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/br_reserved4_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/dr_withhold_tax_ind_data_req");
        
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fx_contract_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fx_contract_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_number_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_number_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_rate_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_rate_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fx_contract_rate_data_req");
        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/fx_contract_rate_req");

        String fileFormatType = inputDoc.getAttribute("/In/PaymentDefinitions/file_format_type");
        if (TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)){
            handleFields(inputDoc);
        }
        if (TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)){
        	inputDoc.setAttribute("/In/PaymentDefinitions/charges_size","1");
        }
//        resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/file_reference_data_req");
        //
        //CR-741 - end
    }

    private void handleFields(DocumentHandler inputDoc) {
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_method_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hdebit_account_number_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hconfidential_ind_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_currency_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hexecution_date_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hheader_identifier_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfile_reference_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hindv_acct_entry_ind_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_account_number_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_name_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcustomer_reference_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_branch_code_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fpayment_amount_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_country_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address2_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address3_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address4_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_fax_no_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_email_id_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcharges_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_name_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_name_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr2_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_city_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_prvnc_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_cntry_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fpayable_location_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fprint_location_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdelvry_mth_n_delvrto_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address2_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address3_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address4_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdetails_of_payment_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/finstruction_number_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cd_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_name_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_nm_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr2_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_city_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_prvnc_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cntry_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep2_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep3_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdetail_identifier_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_code_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code1_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code2_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_header_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_lineitm_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_method_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hdebit_account_number_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hconfidential_ind_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hpayment_currency_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hexecution_date_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hheader_identifier_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfile_reference_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hindv_acct_entry_ind_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_account_number_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_name_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcustomer_reference_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_branch_code_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fpayment_amount_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_country_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address2_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address3_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_address4_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_fax_no_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_email_id_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcharges_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bank_name_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_name_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_addr2_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_city_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_prvnc_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbene_bnk_brnch_cntry_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fpayable_location_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fprint_location_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdelvry_mth_n_delvrto_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address2_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address3_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fmailing_address4_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdetails_of_payment_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/finstruction_number_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cd_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_name_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_nm_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_addr2_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_city_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_prvnc_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/ff_int_bnk_brnch_cntry_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep2_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fcentral_bank_rep3_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fdetail_identifier_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/fbeneficiary_code_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code1_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/freporting_code2_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_header_data_req");
//        reMapAttribute(inputDoc, "/In/PaymentDefinitions/iinv_details_lineitm_data_req");
        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
//            reMapAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_data_req");
            reMapAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_data_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_data_req");
        }
        for (int x = 1; x <= TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; x++) {
            reMapAttribute(inputDoc, "/In/PaymentDefinitions/fusers_def" + x + "_req");
//            resetCheckBoxAttribute(inputDoc, "/In/PaymentDefinitions/seller_users_def" + x + "_data_req");
        }
        
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_number_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_number_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_rate_data_req");
        reMapAttribute(inputDoc, "/In/PaymentDefinitions/hfx_contract_rate_req");
       

    }

    private void reMapAttribute(DocumentHandler inputDoc, String attributePath) {
        String attrValue = inputDoc.getAttribute(attributePath);
        if (StringFunction.isNotBlank(attrValue)){
            String fieldName = attributePath.substring(attributePath.lastIndexOf("/")+1);
            inputDoc.setAttribute("/In/PaymentDefinitions/"+fieldName.substring(1),attrValue);
        }

    }

    /**
     * We check for a value at a given path in the Xml Doc, if it
     * Does'nt exist then we set that value in the document to 'N'.  This is used for
     * resetting a Check boxes value since deselection of a check box does NOT update the
     * Xml Doc with a revised value.
     *
     * @param inputDoc inputDoc - the document to get and set data against.
     * @param xmlPath          xmlPath           - this the the path to use to get/set
     *                        attributes in the xml Doc.
     */
    private static void resetCheckBoxAttribute(DocumentHandler inputDoc, String xmlPath) {

        String value;

        value = inputDoc.getAttribute(xmlPath);
        if (value == null)
            inputDoc.setAttribute(xmlPath, TradePortalConstants.INDICATOR_NO);
    }
}