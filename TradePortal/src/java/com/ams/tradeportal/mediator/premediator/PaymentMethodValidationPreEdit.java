package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;

import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.util.*;

public class PaymentMethodValidationPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodValidationPreEdit.class);
	/**
	 * LRQPreEdit constructor comment.
	 */
	public PaymentMethodValidationPreEdit() {
		super();
	}
	
	/**
	 */
	public void act(AmsServletInvocation reqInfo, 
			BeanManager beanMgr, 
			FormManager formMgr, 
			ResourceManager resMgr, 
			String serverLocation, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Hashtable inputParmMap,
			DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException
	{
        ClientServerDataBridge csdb = resMgr.getCSDB ();

        inputDoc.setAttribute("/In/PaymentMethodValidation/description", 
                ReferenceDataManager.getRefDataMgr().getDescr("PAYMENT_METHOD", inputDoc.getAttribute("/In/PaymentMethodValidation/payment_method"), csdb.getLocaleName())
                + ", " + ReferenceDataManager.getRefDataMgr().getDescr("COUNTRY", inputDoc.getAttribute("/In/PaymentMethodValidation/country"), csdb.getLocaleName())
                + ", " + ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_CODE", inputDoc.getAttribute("/In/PaymentMethodValidation/currency"), csdb.getLocaleName()));
	} 		
	
}