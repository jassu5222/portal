package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.SQLParamFilter;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.AmsServletInvocation;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;

public class RQAPreEdit extends GenericTransactionPreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(RQAPreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public RQAPreEdit() {
	super();
}

/**
 * Performs different logic based on the type of transaction (ISS or AMD)
 * 
 * But basically converts the month, day, year fields for each of the 
 * dates into single date fields.  This allows populateFromXml to work.
 *
 * In addition, explicitly sets all 'missing' checkbox fields to N.
 * (Checkbox fields which are not checked are not sent in the document,
 * we must force an N to cause unchecked values to be save correctly.)
 * 
 */
 public void act(AmsServletInvocation reqInfo, 
				BeanManager beanMgr, 
				FormManager formMgr, 
				ResourceManager resMgr, 
				String serverLocation, 
				HttpServletRequest request, 
				HttpServletResponse response, 
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
			throws com.amsinc.ecsg.frame.AmsException
 {
	String value;

	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to RQAPreEdit is null");
	   return;
	}

        // Convert decimal number from its locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "amount");

	value = inputDoc.getAttribute("/In/Transaction/transaction_type_code");
	if (TransactionType.AMEND.equals(value))
	{
	   doAMDPreEdit(inputDoc, resMgr);
	}
	else
	{
	   doISSPreEdit(inputDoc, resMgr);
	}
 } 

/**
 * Handles pre-edits for the AMD type transactions.  This includes
 * converting 3part date field into single date values and then to 
 * check the increase/decrease flag and reset the amount to a negative
 * value if the flag is decrease.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doAMDPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {
	String date;
	String newDate;

	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to RQAPreEdit is null");
	   return;
	}

	date = inputDoc.getAttribute("/In/Terms/expiry_date");
	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	inputDoc.setAttribute("/In/Terms/expiry_date", newDate);

	String transactionType = inputDoc.getAttribute("/Transaction/transaction_type_code");
	// Issue date can only be set during issuance.  Trying to set it in other transactions will blank it out.  

	if (StringFunction.isNotBlank(transactionType) && transactionType.equals(TransactionType.ISSUE)) { 

		date = inputDoc.getAttribute("/In/Instrument/issue_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Instrument/issue_date", newDate);
	}
	
	date = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
	newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
	inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newDate);

	
	// Clear or set maximum_credit_amount field based on amount tolerance type radio button. GUI javascript handles clearing amount_tolerance(+/-) fields.
	String amountToleranceType = inputDoc.getAttribute("/In/Terms/amount_tolerance_type");
	if (amountToleranceType != null) {
		if (amountToleranceType.equals(TradePortalConstants.NOT_EXCEEDING)) {
		   inputDoc.setAttribute("/In/Terms/maximum_credit_amount", TradePortalConstants.NOT_EXCEEDING);
		} else {
		   inputDoc.setAttribute("/In/Terms/maximum_credit_amount", "");
		}
	}
 } 

/**
 * Performs pre-editing of the input document that is specific to ISS
 * transactions.  This includes converting the three part date fields
 * into a single date values and explicitly setting checkbox values
 * to N when they are not already present in the document.
 *
 * @param doc com.amsinc.ecsg.util.DocumentHandler
 */
 private void doISSPreEdit(DocumentHandler inputDoc, ResourceManager resMgr)
 {
	String date;
	String newDate;
	String value;

	// get the input document 
	if (inputDoc == null)
	{
	   LOG.info("Input document to RQAPreEdit is null");
	   return;
	}

        // Convert decimal numbers from their locale-specific entry format into
        // a standard format that can be understood by the database.
        // If the format is invalid, an error value will be placed into
        // the XML that will trigger an error later.
        // In the corresponding post mediator action, the passed-in value
        // is placed back into its original location.
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_rate");
        doDecimalPreMediatorAction(inputDoc, resMgr, "fec_amount");
        doDecimalPreMediatorAction(inputDoc, resMgr, "insurance_endorse_value_plus");

	
	
	   // Need to convert the expiry date fields into a single date value.
		date = inputDoc.getAttribute("/In/Terms/expiry_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Terms/expiry_date", newDate);
	   
		date = inputDoc.getAttribute("/In/Instrument/issue_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Instrument/issue_date", newDate);
	   
	   if (newDate != "") {
		   // Validate Instrument Issue Date
		   String instrumentOid = inputDoc.getAttribute("/In/Instrument/instrument_oid");
		   String sqlUpdate = "update instrument set issue_date = to_date(?,'mm/dd/yyyy HH24:MI:SS') where instrument_oid = ?";
		   try {
			   DatabaseQueryBean.executeUpdate(sqlUpdate, false, newDate, instrumentOid);
		   }catch(Exception e){
			   LOG.debug("*******Instrument.ISSUE_DATE UPDATE from RQAPreEdit.java  Error in calling DatabaseQueryBean: Related to the RQAPreEdit.java *******");
			   e.printStackTrace();
		   } 
	   }
	   
	   // Clear or set maximum_credit_amount field based on amount tolerance type radio button. GUI javascript handles clearing amount_tolerance(+/-) fields.
	   String amountToleranceType = inputDoc.getAttribute("/In/Terms/amount_tolerance_type");
	   if (amountToleranceType != null) {
		   if (amountToleranceType.equals(TradePortalConstants.NOT_EXCEEDING)) {
			   inputDoc.setAttribute("/In/Terms/maximum_credit_amount", TradePortalConstants.NOT_EXCEEDING);
		   } else {
			   inputDoc.setAttribute("/In/Terms/maximum_credit_amount", "");
		   }
	   }
	   
	
	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/comm_invoice_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/comm_invoice_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/packing_list_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/packing_list_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/cert_origin_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/cert_origin_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ins_policy_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ins_policy_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_1_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_1_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_2_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_2_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_3_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_3_indicator", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/other_req_doc_4_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/other_req_doc_4_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/addl_doc_indicator");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/addl_doc_indicator", TradePortalConstants.INDICATOR_NO);
	   }

	 
	   date = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Terms/ShipmentTermsList/latest_shipment_date", newDate);

	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_doc_included", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/trans_addl_doc_included", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ShipmentTermsList/transshipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }

	   value = inputDoc.getAttribute("/In/Terms/partial_shipment_allowed");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/partial_shipment_allowed", TradePortalConstants.INDICATOR_NO);
	   }
	
	   date = inputDoc.getAttribute("/In/Terms/fec_maturity_date");
		newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
		inputDoc.setAttribute("/In/Terms/fec_maturity_date", newDate);

	   
	   // Checkboxes do not send values if the checkbox is unchecked.
	   // Therefore, for each checkbox box, determine if we have a
	   // value in the document.  If not, add an explicit N value for
	   // each one that is missing.  This ensures that checkboxes that
	   // are unchecked by the user get reset to N.
	   value = inputDoc.getAttribute("/In/Terms/transferrable");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/transferrable", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/revolve");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/revolve", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/finance_drawing");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/finance_drawing", TradePortalConstants.INDICATOR_NO);
	   }
	   
	   value = inputDoc.getAttribute("/In/Terms/ucp_version_details_ind");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/ucp_version_details_ind", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/irrevocable");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/irrevocable", TradePortalConstants.INDICATOR_NO);
	   }
	   value = inputDoc.getAttribute("/In/Terms/operative");
	   if (value == null)
	   {
		  inputDoc.setAttribute("/In/Terms/operative", TradePortalConstants.INDICATOR_NO);
	   }
	

	
		
		value = inputDoc.getAttribute("/In/Terms/percent_amount_dis_ind");
		
		String paymentType=inputDoc.getAttribute("/In/Terms/payment_type");
		int pmtTermsRowCount=inputDoc.getFragments("/In/Terms/PmtTermsTenorDtlList").size();
		StringBuilder pmtTermsOidStr = new StringBuilder();
		StringBuilder deleteSql = new StringBuilder();
		List<Object> sqlPrmLst = new ArrayList<Object>();
		deleteSql.append("delete from pmt_terms_tenor_dtl where p_terms_oid = ? ");
		sqlPrmLst.add(inputDoc.getAttribute("/In/Terms/terms_oid"));
		if ("SPEC".equals(paymentType)){
			for (int i = 0; i < pmtTermsRowCount; i++){
				inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList(" + i +")");
			}
		}else if("PAYM".equals(paymentType)){
			
			for (int i = 1; i < pmtTermsRowCount; i++){
				inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList(" + i +")");
			}
			String pmtTermsOid = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/pmt_terms_tenor_dtl_oid");
			if (StringFunction.isNotBlank(pmtTermsOid)) {
				deleteSql.append(" and pmt_terms_tenor_dtl_oid not in (?) ");
				sqlPrmLst.add(pmtTermsOid);
			}
			
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/percent", "100");
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/tenor_type", "SGT");
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/amount", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/num_days_after", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/days_after_type", null);
			inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+0+")/maturity_date", null);
			inputDoc.setAttribute("/In/Terms/special_tenor_text", null);
		}else{
			inputDoc.setAttribute("/In/Terms/special_tenor_text", null);
		
			int numOfEmptyRows=0;
			//Check if all rows are empty
			for( int iLoop=0; iLoop<pmtTermsRowCount; iLoop++){
				String percent=inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent") ;
				String amount= inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount") ;
				if(  ( StringFunction.isBlank(percent) ||
								(StringFunction.isNotBlank(percent) && "0".equals(percent.trim()) ))
						&& (StringFunction.isBlank(amount) ||
								(StringFunction.isNotBlank(amount) && "0".equals(amount.trim()) ))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/tenor_type"))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/num_days_after"))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/days_after_type") )
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date"))
					){
					numOfEmptyRows++;
					}
			}
			//Display error when all rows are empty
	
			if(numOfEmptyRows == pmtTermsRowCount && StringFunction.isNotBlank(paymentType) && !"SPEC".equals(paymentType) ){
			
				MediatorServices medService = new MediatorServices();
				//IR T36000048558 Rel9.5 05/09/2016
				medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
				try{
				  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.ROW_ATTRIBUTE_REQUIRED_CONDITION);
				  medService.addErrorInfo();
			      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
				  return;
				  }catch(AmsException amsEx){
					  amsEx.printStackTrace();  
				  }
				
			}
			
			String termsAmount=inputDoc.getAttribute("/In/Terms/amount");
			BigDecimal instrumentAmount = BigDecimal.valueOf(
												new Double(
														StringFunction.isNotBlank(termsAmount)? termsAmount:"0"));
			BigDecimal totalPmtAmount =  BigDecimal.ZERO;
			
			for(int iLoop=0; iLoop<pmtTermsRowCount; iLoop++){
				date = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date");
				newDate = TPDateTimeUtility.convertISODateToJPylonDate(date);
				inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date", newDate);
				if("A".equals(value)){
					inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount", 
							inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent"));
					inputDoc.setAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent", null);
					
					 String amountStr=inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount");
		             if(StringFunction.isNotBlank(amountStr))
		                               totalPmtAmount = totalPmtAmount.add(new BigDecimal(amountStr));
		           
				}
				
				String percent=inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/percent") ;
				String amount= inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/amount") ;
				
				if( ( StringFunction.isBlank(percent) ||
								(StringFunction.isNotBlank(percent) && "0".equals(percent.trim()) ))
						&& (StringFunction.isBlank(amount) ||
								(StringFunction.isNotBlank(amount) && "0".equals(amount.trim()) ))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/tenor_type"))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/num_days_after"))
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/days_after_type") )
						&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/maturity_date"))
					){
					inputDoc.removeAllChildren("/In/Terms/PmtTermsTenorDtlList("+iLoop+")");
					inputDoc.removeComponent("/In/Terms/PmtTermsTenorDtlList("+iLoop+")");
					}
				else{
					
					//Added to fix the Issue with Available by Mixed/Negotiation Payment Terms where the Add 2 More Lines button is not working.
					String pmtTermsOid = inputDoc.getAttribute("/In/Terms/PmtTermsTenorDtlList("+iLoop+")/pmt_terms_tenor_dtl_oid");
					if (StringFunction.isNotBlank(pmtTermsOid)) {
						pmtTermsOidStr.append(pmtTermsOid);
						pmtTermsOidStr.append(",");
					}
					
				}
				
			}
			
			if ( pmtTermsOidStr.length()>0 ) {
			    deleteSql.append(" and pmt_terms_tenor_dtl_oid not in (");		
			    String placeHolderStr = SQLParamFilter.preparePlaceHolderStrForInClause(pmtTermsOidStr.substring(0,pmtTermsOidStr.length()-1), sqlPrmLst);
				deleteSql.append(placeHolderStr);
			    deleteSql.append(" ) ");
			}
      
			 if ( "A".equals(value) &&  totalPmtAmount.compareTo(instrumentAmount)!=0){
			    //�Total of all Tenor Amounts do not equal Instrument Amount�
				 MediatorServices medService = new MediatorServices();
				//IR T36000048558 Rel9.5 05/09/2016
				medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
					try{
					  medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,TradePortalConstants.PMT_AMOUNT_TOT_IS_NOTEQUAL_TO_INSTRUMENT_AMOUNT);
					  medService.addErrorInfo();
				      inputDoc.setComponent ("/Error", medService.getErrorDoc ());
					  return;
					  }catch(AmsException amsEx){
						  amsEx.printStackTrace();  
					  }
			}
		}
		
		//Added to fix the Issue with Available by Mixed/Negotiation Payment Terms where the Add 2 More Lines button is not working.
		try (Connection con = DatabaseQueryBean.connect(false);
 			PreparedStatement pStmt = con.prepareStatement(deleteSql.toString())){
			Object [] sqlPrmArray = sqlPrmLst.toArray();
 			for (int i=0; i<sqlPrmArray.length; i++) {
				if (sqlPrmArray[i] != null) {
					pStmt.setObject(i + 1, sqlPrmArray[i]);
				} else {
					pStmt.setNull(i + 1, Types.OTHER);
				}
			}
 			con.setAutoCommit (false);
 			pStmt.executeUpdate();
 			con.commit();
 			sqlPrmArray = null;

 		}catch (Exception ex) {
            //because this is not vital to processing, ignore it but create a log message
            System.err.println("Exception removing authentication log entry: " + ex.toString());
		}
 	
		int addDocRowCount=inputDoc.getFragments("/In/Terms/AdditionalReqDocList").size();
		for(int iLoop=8; iLoop<(8+addDocRowCount); iLoop++){
			
		if( StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_oid"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_name"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_originals"))
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_copies")) 
				&& StringFunction.isBlank(inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_text"))
				
			){
			inputDoc.removeAllChildren("/In/Terms/AdditionalReqDocList("+iLoop+")");
			inputDoc.removeComponent("/In/Terms/AdditionalReqDocList("+iLoop+")");
			}
		
		else{
		   value = inputDoc.getAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind");
		   if (value == null)
		   {
			  inputDoc.setAttribute("/In/Terms/AdditionalReqDocList("+iLoop+")/addl_req_doc_ind", TradePortalConstants.INDICATOR_NO);
		   }
		}
				
		}
		

        //cquinton 10/16/2013 ir#21872 Rel 8.3 start
        //when ucp version is not 'OTHER', ucp version details should be blanked out
        value = inputDoc.getAttribute("/In/Terms/ucp_version");
        if ( !TradePortalConstants.RQA_OTHER.equals(value) ) {
            inputDoc.setAttribute("/In/Terms/ucp_details", "");
        }
        //cquinton 10/16/2013 ir#21872 Rel 8.3 end
 }
 
}