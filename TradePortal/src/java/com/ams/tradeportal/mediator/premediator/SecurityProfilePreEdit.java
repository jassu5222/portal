package com.ams.tradeportal.mediator.premediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert the type's description here.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

import com.amsinc.ecsg.web.*;
import javax.servlet.http.*;
import java.util.*;
import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.util.*;

import com.ams.tradeportal.common.*;import java.math.*;

public class SecurityProfilePreEdit implements com.amsinc.ecsg.web.WebAction {
private static final Logger LOG = LoggerFactory.getLogger(SecurityProfilePreEdit.class);
/**
 * AdminUserPreEdit constructor comment.
 */
public SecurityProfilePreEdit() {
	super();
}
/**
 * Extracts the individual security rights from the input document
 * and creates a single securityRights value that is then set in
 * the input document.
 */
public void act(AmsServletInvocation reqInfo,
				BeanManager beanMgr,
				FormManager formMgr,
				ResourceManager resMgr,
				String serverLocation,
				HttpServletRequest request,
				HttpServletResponse response,
				Hashtable inputParmMap,
				DocumentHandler inputDoc)
	throws com.amsinc.ecsg.frame.AmsException {

	String securityRights = "0";

	// get the input document
	if (inputDoc == null) {
		LOG.info("Input document to AdminUserPreEdit is null");
		return;
	}

	securityRights = determineSecurityRights(inputDoc.getFragment("/In"));
	inputDoc.setAttribute("/In/SecurityProfile/security_rights", securityRights);

}



	  public static final String MAINTAIN  = "M";	  	  public static final String VIEW_ONLY = "V";

 /**
 * Checks if a given value is M (for maintain).  (Also checks for null value).
 * If so, true is returned, otherwise false.
 *
 * @return boolean
 * @param value java.lang.String
 */
private boolean canMaintain(String value) {
	if (value == null) return false;
	else if (value.equals(MAINTAIN)) return true;

	return false;
}


/**
 * Checks if a given value is V (for view only).  (Also checks for null value).
 * If so, true is returned, otherwise false.
 *
 * @return boolean
 * @param value java.lang.String
 */

private boolean canViewOnly(String value) {
	if (value == null) return false;
	else if (value.equals(VIEW_ONLY)) return true;

	return false;
}


/**
 * Goes through the input document and computes the security rights
 * based on the selected values.  The computed rights is converted
 * to a string value and returned.
 *
 * @param inputDoc com.amsinc.ecsg.util.DocumentHandler
 */
public String determineSecurityRights(DocumentHandler inputDoc) {

	BigInteger securityRights = SecurityAccess.NO_ACCESS;
	String value = "";

 	String noAccessFlag = inputDoc.getAttribute("/SecurityProfile/no_access_flag");
	// if user clicks save as button, changing to insert mode with niwly given name.
	String saveAsFlag = inputDoc.getAttribute("/SecurityProfile/save_as_flag");
	if("Y".equals(saveAsFlag))
	{	
		inputDoc.setAttribute("/SecurityProfile/security_profile_oid","0");
	}
	// Now methodically plod through the input doc to determine
	// what rights are turned on or off.

	//SSikhakolli - Rel-9.3.5 CR-1029 - Begin
	value = inputDoc.getAttribute("/SecurityProfile/access_admin_update_center");
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_admin_update_center","N");
	}
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_ADMIN_UPDATE_CENTRE);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_trans_apply_updates");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_TRANS_APPLY_UPDATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_trans_approve");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_TRANS_APPROVE);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_trans_send_for_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_TRANS_SEND_FOR_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_trans_attach_docs");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_TRANS_ATTACH_DOC);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_trans_delete_docs");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_TRANS_DELETE_DOC);
		//IR-43061 Rel 9.3.5
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_xml_download");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_XML_DOWNLOAD);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_mail_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_MAIL_DELETE);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_mail_create_reply");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_MAIL_CREATE_REPLY);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_mail_send_to_cust");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_MAIL_SEND_TO_CUST);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_mail_attach_docs");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_MAIL_ATTACH_DOC);
		
		value = inputDoc.getAttribute("/SecurityProfile/bank_mail_delete_docs");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.BANK_MAIL_DELETE_DOC);
	}
	//SSikhakolli - Rel-9.3.5 CR-1029 - End
		
	value = inputDoc.getAttribute("/SecurityProfile/access_message");
	
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_message","N");
	}
	
	
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_MESSAGES_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/send_message");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SEND_MESSAGE);

		value = inputDoc.getAttribute("/SecurityProfile/create_message");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CREATE_MESSAGE);

		value = inputDoc.getAttribute("/SecurityProfile/delete_message");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_MESSAGE);

		value = inputDoc.getAttribute("/SecurityProfile/route_message");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ROUTE_MESSAGE);

		// CR-186 - pcutrone - Attach Document to Message
		value = inputDoc.getAttribute("/SecurityProfile/message_attach_document");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ATTACH_DOC_MESSAGE);

		// CR-186 - pcutrone - Delete Document from Message
		value = inputDoc.getAttribute("/SecurityProfile/message_delete_document");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_DOC_MESSAGE);

		value = inputDoc.getAttribute("/SecurityProfile/view_child_org_messages");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CHILD_ORG_MESSAGES);

		value = inputDoc.getAttribute("/SecurityProfile/route_discrepancy_msg");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ROUTE_DISCREPANCY_MSG);

		value = inputDoc.getAttribute("/SecurityProfile/delete_discrepancy_msg");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_DISCREPANCY_MSG);

		value = inputDoc.getAttribute("/SecurityProfile/delete_notify");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_NOTIFICATION);
		
		//Rel9.0 CR 913 Start
		value = inputDoc.getAttribute("/SecurityProfile/route_predebit_funding_notif");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ROUTE_PREDEBIT_FUNDING_NOTIFICATION);

		value = inputDoc.getAttribute("/SecurityProfile/delete_predebit_funding_notif");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION);
		//Rel9.0 CR 913 End
	}

	value = inputDoc.getAttribute("/SecurityProfile/access_instrument");
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_instrument","N");
	}
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_INSTRUMENTS_AREA);
		
		
	
		//added direct debits to instruments for TP refresh
		/*Prateep Gedupudi 19th Dec. 2012, To make Direct Debit settings to separate section from Instrument Section START*/
		/*Commented following code because of we are moving DirectDebit setting to saparate section(Section 5) 
		 * based on the discussion with ANZ with the reference of Kim Mail on 19th Dec. 2012*/
		
		/*value = inputDoc.getAttribute("/SecurityProfile/view_child_org_directdebit");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CHILD_ORG_DIRECTDEBIT);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_upload_file");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_UPLOAD_FILE);*/
		/*Prateep Gedupudi 19th Dec. 2012, To make Direct Debit settings to separate section from Instrument Section End*/
		
		

		value = inputDoc.getAttribute("/SecurityProfile/work_subsidiary");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CHILD_ORG_WORK);

		value = inputDoc.getAttribute("/SecurityProfile/dlc_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/dlc_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/dlc_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/dlc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/dlc_process_po");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_PROCESS_PO);

		value = inputDoc.getAttribute("/SecurityProfile/slc_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/slc_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/slc_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/slc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_AUTHORIZE);

		//value = inputDoc.getAttribute("/SecurityProfile/lct_create_mod");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_TRANSFER_CREATE_MODIFY);

		//value = inputDoc.getAttribute("/SecurityProfile/lct_delete");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_TRANSFER_DELETE);

		//value = inputDoc.getAttribute("/SecurityProfile/lct_route");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_TRANSFER_ROUTE);

		//value = inputDoc.getAttribute("/SecurityProfile/lct_authorise");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_TRANSFER_AUTHORIZE);

		//value = inputDoc.getAttribute("/SecurityProfile/lca_create_mod");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_ASSIGN_CREATE_MODIFY);

		//value = inputDoc.getAttribute("/SecurityProfile/lca_delete");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_ASSIGN_DELETE);

		//value = inputDoc.getAttribute("/SecurityProfile/lca_route");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_ASSIGN_ROUTE);

		//value = inputDoc.getAttribute("/SecurityProfile/lca_authorise");
		//if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LC_ASSIGN_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/expdlc_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/expdlc_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/expdlc_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/expdlc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/expcoll_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/expcoll_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/expcoll_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/expcoll_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_AUTHORIZE);
		//Vasavi CR 524 03/31/2010 Begin
		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_AUTHORIZE);
		//Vasavi CR 524 03/31/2010 End
		value = inputDoc.getAttribute("/SecurityProfile/guar_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/guar_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/guar_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/guar_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/awr_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/awr_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/awr_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/awr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/sg_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/sg_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/sg_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/sg_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/ft_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/ft_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/ft_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/ft_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/lr_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/lr_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/lr_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/lr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_AUTHORIZE);
		
		//DK CR709 Rel8.2 10/31/2012 Begin
		value = inputDoc.getAttribute("/SecurityProfile/lr_process_invoices");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_PROCESS_INVOICES);
		//DK CR709 Rel8.2 10/31/2012	End

		value = inputDoc.getAttribute("/SecurityProfile/ra_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/ra_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/ra_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/ra_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_AUTHORIZE);

		//Krishna CR 375-D ATP 07/19/2007 Begin
                value = inputDoc.getAttribute("/SecurityProfile/atp_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/atp_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/atp_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/atp_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/atp_process_po");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_PROCESS_PO);
                //Krishna CR 375-D ATP 07/19/2007 End

		// CR-186 - pcutrone - Attach Document to Transaction
		value = inputDoc.getAttribute("/SecurityProfile/transaction_attach_document");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ATTACH_DOC_TRANSACTION);

		// CR-186 - pcutrone - Delete Document from Transaction
		value = inputDoc.getAttribute("/SecurityProfile/transaction_delete_document");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DELETE_DOC_TRANSACTION);

		value = inputDoc.getAttribute("/SecurityProfile/dr_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/dr_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/dr_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/dr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_AUTHORIZE);

		// crhodes CR 451 10/15/2008 Begin
		// Transfer
		value = inputDoc.getAttribute("/SecurityProfile/tf_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/tf_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/tf_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/tf_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_AUTHORIZE);

		// Domestic Payment
		value = inputDoc.getAttribute("/SecurityProfile/dp_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/dp_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/dp_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/dp_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/dp_upload_bulk_file");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_UPLOAD_BULK_FILE);
		// crhodes CR 451 10/15/2008 End
		
		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields - 10/11/2011 - start
		//Instruments section
		value = inputDoc.getAttribute("/SecurityProfile/proxy_dlc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_DLC_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_slc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_SLC_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_expdlc_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_EXP_DLC_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_expcoll_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_EXP_COLL_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_guar_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_GUAR_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_new_exp_coll_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_NEW_EXP_COLL_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_sg_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_SG_AUTHORIZE);	
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_awr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_AWR_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_lr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_LR_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_tf_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_TF_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_ra_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_RA_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_ft_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_FT_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_atp_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_ATP_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_dp_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_DP_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_dr_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_DR_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_ddi_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_DDI_AUTHORIZE);
		
		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields (instruments section) - 10/11/2011 - end
       //SHR CR-708 Rel8.1.1 Start
		value = inputDoc.getAttribute("/SecurityProfile/process_atp_invoices");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.ATP_PROCESS_INVOICES);
		//SHR CR-708 Rel8.1.1 End

		//CR 821 - Rel 8.3 START
		value = inputDoc.getAttribute("/SecurityProfile/atp_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/atp_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.APPROVAL_TO_PAY_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/dp_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/dp_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DOMESTIC_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/slc_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/slc_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SLC_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/expdlc_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/expdlc_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_LC_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/dlc_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/dlc_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DLC_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/expcoll_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/expcoll_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.EXPORT_COLL_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/guar_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/guar_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.GUAR_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/new_exp_coll_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.NEW_EXPORT_COLL_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/sg_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/sg_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SHIP_GUAR_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/awr_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/awr_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.AIR_WAYBILL_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/lr_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/lr_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.LOAN_RQST_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/tf_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/tf_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.TRANSFER_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/ra_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/ra_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REQUEST_ADVISE_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/ft_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_CHECKER);
		
		value = inputDoc.getAttribute("/SecurityProfile/ft_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.FUNDS_XFER_REPAIR);
		
		value = inputDoc.getAttribute("/SecurityProfile/dr_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_CHECKER);	
		
		value = inputDoc.getAttribute("/SecurityProfile/dr_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DISCREPANCY_REPAIR);
		//CR 821 - Rel 8.3 END
		
		//SSikhakolli - Rel-9.4 CR-818 - Begin
		value = inputDoc.getAttribute("/SecurityProfile/si_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/si_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/si_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/si_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_si_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_SIT_AUTHORIZE);
		
		value = inputDoc.getAttribute("/SecurityProfile/si_checker");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_CHECKER);	
		
		value = inputDoc.getAttribute("/SecurityProfile/si_repair");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SIT_REPAIR);
		//SSikhakolli - Rel-9.4 CR-818 - End
	}
	// crhodes CR 434 10/15/2008 Begin
	value = inputDoc.getAttribute("/SecurityProfile/access_receivables");
	if(hasAccess(value)){
		securityRights = securityRights.or(SecurityAccess.ACCESS_RECEIVABLES_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/view_child_org_receivables");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CHILD_ORG_RECEIVABLES);

		value = inputDoc.getAttribute("/SecurityProfile/receivables_match");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_MATCH);

		value = inputDoc.getAttribute("/SecurityProfile/receivables_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/receivables_authorize");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/receivables_app_dis_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_APPROVE_DISCOUNT_AUTHORIZE);

		value = inputDoc.getAttribute("/SecurityProfile/receivables_add_buyer");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_ADD_BUYER_TO_PAYMENT);

		value = inputDoc.getAttribute("/SecurityProfile/match_dispute_undispute");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_DISPUTE_UNDISPUTE);

		value = inputDoc.getAttribute("/SecurityProfile/match_close");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_CLOSE);

		value = inputDoc.getAttribute("/SecurityProfile/match_finance");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_FINANCE);
	}

	value = inputDoc.getAttribute("/SecurityProfile/access_receivable_notices");
	if(hasAccess(value)){
		securityRights = securityRights.or(SecurityAccess.ACCESS_REC_NOTICES_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/receivable_notices_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REC_NOTICES_ROUTE);
		
 		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields (receivables section) - 10/11/2011 - start	
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_receivables_authorize");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_RECEIVABLES_AUTHORIZE);		
		
		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields (receivables section) - 10/11/2011 - end
		
		//Ravindra 5th Mar 2012 Rel800 IR#LKUM021742692 - New proxy Authorize fields (receivables section) - start	
		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_receivables_app_dis_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_PROXY_APPROVE_DISCOUNT_AUTHORIZE);		
		//
		//Ravindra 5th Mar 2012 Rel800 IR#LKUM021742692 - New proxy Authorize fields (receivables section) -  end

//Srini_D CR-710 11/13/2011 Rel8.0 Start
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_approve_financing");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_APPROVE_FINANCING);		
		
		value = inputDoc.getAttribute("/SecurityProfile/recievables_remove_invoice");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_REMOVE_INVOICE);		
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_authorize_invoice");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE);	
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_delete_invoice");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_DELETE_INVOICE);
		//Srini_D CR-710 11/13/2011 Rel8.0 End

		// DK CR709 Rel8.2 10/31/2012 BEGINS
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_apply_payment_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_APPLY_PAYMENT_DATE);		
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_clear_payment_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_CLEAR_PAYMENT_DATE);		
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_assign_instr_type");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_ASSIGN_INSTR_TYPE);	
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_assign_loan_type");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_ASSIGN_LOAN_TYPE);
		
		value = inputDoc.getAttribute("/SecurityProfile/receivables_create_loan_req");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_clear_payment_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CLEAR_PAYMENT_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_assign_instr_type");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_ASSIGN_INSTR_TYPE);	
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_assign_loan_type");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_ASSIGN_LOAN_TYPE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_create_loan_req");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREATE_LOAN_REQ);
		
		// DK CR709 Rel8.2 10/31/2012 ENDS		
		
		//AAlubala CR710 Rel8.0 - VASCO - Offline Auth - START
		value = inputDoc.getAttribute("/SecurityProfile/proxy_receivables_authorize_invoice");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_RECEIVABLES_AUTHORIZE_INVOICE);	
		//CR710 - END
		
		//SHR CR-708 Rel8.1.1 Start
		value = inputDoc.getAttribute("/SecurityProfile/payables_apply_payment_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAYABLES_APPLY_PAYDATE);		
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_create_atp");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAYABLES_CREATE_ATP);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_delete_invoice");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAYABLES_DELETE_INVOICE);

		//R9.2 CR914 Start

		//#RKAZI CR1006 04-24-2015 - BEGIN
		value = inputDoc.getAttribute("/SecurityProfile/rec_invoice_decline_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REC_INVOICE_DECLINE_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_invoice_decline_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_INVOICE_DECLINE_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_decline_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_DECLINE_AUTH);
 		//#RKAZI CR1006 04-24-2015 - END
		
		//#PGedupudi CR1006 05-14-2015
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_approve_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_APPROVE_AUTH);
		
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_AUTH);

		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_offline_auth");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_OFFLINE_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_DELETE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_close");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_CLOSE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_manual_apply");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_MANUAL_APPLY);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_credit_note_manual_unapply");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_CREDIT_NOTE_MANUAL_UNAPPLY);
		
		//R9.2 CR914 End
		
		
		//SHR CR-708 Rel8.1.1 End	
		//Ravindra - CR-708B - Start
		value = inputDoc.getAttribute("/SecurityProfile/sp_inv_offered");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_INVOICE_OFFERED);
		value = inputDoc.getAttribute("/SecurityProfile/sp_history");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_HISTORY);
		value = inputDoc.getAttribute("/SecurityProfile/sp_accept_offer");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_ACCEPT_OFFER);
		value = inputDoc.getAttribute("/SecurityProfile/sp_auth_offer");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_AUTHORIZE_OFFER);
		value = inputDoc.getAttribute("/SecurityProfile/sp_decline_offer");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_DECLINE_OFFER);
		value = inputDoc.getAttribute("/SecurityProfile/sp_asgn_fvd");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_ASSIGN_FVD);
		value = inputDoc.getAttribute("/SecurityProfile/sp_remove_fvd");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_REMOVE_FVD);
		value = inputDoc.getAttribute("/SecurityProfile/sp_remove_inv");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_REMOVE_INVOICE);
		value = inputDoc.getAttribute("/SecurityProfile/sp_reset_to_offered");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.SP_RESET_TO_OFFERED);
		//Ravindra - CR-708B - End
		//Rel 9.0 CR 913 Start
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_authorise_offline");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_OFFLINE_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_approve_payment");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_APPLY_PAYMENT_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_clear_payment");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_CLEAR_PAYMENT_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_apply_early_pay_amt");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_APPLY_EARLY_PAY_AMOUNT);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_reset_early_pay_amt");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_RESET_EARLY_PAY_AMOUNT);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_apply_send_to_supplier_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_APPLY_SUPPLIER_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/pay_inv_reset_send_to_supplier_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PAY_MGM_RESET_SUPPLIER_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_authorise_offline");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_OFFLINE_AUTH);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_apply_early_pay_amt");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_APPLY_EARLY_PAY_AMOUNT);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_reset_early_pay_amt");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_apply_send_to_supplier_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/payables_reset_send_to_supplier_date");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE);
		//Rel 9.0 CR 913 End
	}
	// crhodes CR 434 10/15/2008 End

	
	
	
	
	
	
	/*Prateep Gedupudi 19th Dec. 2012, To make Direct Debit settings to separate section from Instrument Section START*/
	value = inputDoc.getAttribute("/SecurityProfile/access_directdebit");
	if(hasAccess(value)){
		securityRights = securityRights.or(SecurityAccess.ACCESS_DIRECTDEBIT_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/view_child_org_directdebit");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CHILD_ORG_DIRECTDEBIT);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_create_mod");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_CREATE_MODIFY);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_delete");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_DELETE);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_route");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_ROUTE);

		value = inputDoc.getAttribute("/SecurityProfile/ddi_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_AUTHORIZE);
		
		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields (direct debit section) - 10/11/2011 - start		
		value = inputDoc.getAttribute("/SecurityProfile/proxy_ddi_authorise");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.PROXY_DDI_AUTHORIZE);
		//AAlubala CR-711 Rel8.0 - New proxy Authorize fields (direct debit section) - 10/11/2011 - end
		
		value = inputDoc.getAttribute("/SecurityProfile/ddi_upload_file");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.DDI_UPLOAD_FILE);

	}
	/*Prateep Gedupudi 19th Dec. 2012, To make Direct Debit settings to separate section from Instrument Section END*/

	value = inputDoc.getAttribute("/SecurityProfile/access_refdata");
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_refdata","N");
	}
	
	
	
	
	
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_REFDATA_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/maint_parties");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PARTIES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PARTIES);

		value = inputDoc.getAttribute("/SecurityProfile/maint_fx_rate");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_FX_RATE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_FX_RATE);
		
		//Added for Rel9.5 CR927B - START
		value = inputDoc.getAttribute("/SecurityProfile/maint_notif_rule_templates");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_NOTIF_RULE_TEMPLATES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_NOTIF_RULE_TEMPLATES);
		
		value = inputDoc.getAttribute("/SecurityProfile/maintain_notification_rules");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_NOTIFICATION_RULE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_NOTIFICATION_RULE);
		//Added for Rel9.5 CR927B - END
		
		value = inputDoc.getAttribute("/SecurityProfile/maint_phrase");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PHRASE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PHRASE);

		value = inputDoc.getAttribute("/SecurityProfile/maint_sec_prof");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_SEC_PROF);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_SEC_PROF);

		value = inputDoc.getAttribute("/SecurityProfile/maint_template");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_TEMPLATE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_TEMPLATE);
		
		value = inputDoc.getAttribute("/SecurityProfile/maint_thresh_grp");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_THRESH_GRP);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_THRESH_GRP);

		value = inputDoc.getAttribute("/SecurityProfile/maint_users");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_USERS);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_USERS);

		value = inputDoc.getAttribute("/SecurityProfile/maint_lc_create_rule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_LC_CREATE_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_LC_CREATE_RULES);

            //TLE - 07/20/07 - CR-375 - Add Begin
		value = inputDoc.getAttribute("/SecurityProfile/maint_atp_create_rule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ATP_CREATE_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ATP_CREATE_RULES);
            //TLE - 07/20/07 - CR-375 - Add End

		// crhodes CR434 10/20/2008 Begin
		value = inputDoc.getAttribute("/SecurityProfile/maint_receivables_match_rule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_RECEIVABLES_MATCH_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_RECEIVABLES_MATCH_RULES);
		// crhodes CR434 10/20/2008 End

		value = inputDoc.getAttribute("/SecurityProfile/maint_po_upload_defn");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PO_UPLOAD_DEFN);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN);

		value = inputDoc.getAttribute("/SecurityProfile/maint_work_groups");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_WORK_GROUPS);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_WORK_GROUPS);

		//IAZ CR-511 11/09/09 Begin
		value = inputDoc.getAttribute("/SecurityProfile/maint_panel_auth_grp");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PANEL_AUTH_GRP);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PANEL_AUTH_GRP);
		//IAZ CR-511 11/09/09 End

		//rbhaduri - 21st Feb 06 - CR239
		value = inputDoc.getAttribute("/SecurityProfile/create_new_transaction_party");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CREATE_NEW_TRANSACTION_PARTY);

        //Tang CR-507 12/01/09 - Begin
        value = inputDoc.getAttribute("/SecurityProfile/maint_bank_branch_rules");
        if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_BANK_BRANCH_RULES);
        else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_BANK_BRANCH_RULES);
        
        //pgedupudi - Rel9.3 CR976A 03/09/2015
        value = inputDoc.getAttribute("/SecurityProfile/maint_bank_grp_restrict_rules");
        if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_BANK_GROUP_RESTRICT_RULES);
        else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES);

        value = inputDoc.getAttribute("/SecurityProfile/maint_payment_meth_val");
        if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PAYMENT_METH_VAL);
        else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PAYMENT_METH_VAL);
        //Tang CR-507 12/01/09 - End

        // [START] 2010106 CR-507
        value = inputDoc.getAttribute("/SecurityProfile/maint_calendar");
        if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CALENDAR);
        else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CALENDAR);

        value = inputDoc.getAttribute("/SecurityProfile/maint_currency_calendar_rules");
        if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CURRENCY_CALENDAR_RULES);
        else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CURRENCY_CALENDAR_RULES);
        //[END] 20100106 CR-507

		//Vshah CR-586 [BEGIN]
		value = inputDoc.getAttribute("/SecurityProfile/create_fixed_payment_template");
		if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CREATE_FIXED_PAYMENT_TEMPLATE);

		value = inputDoc.getAttribute("/SecurityProfile/maint_payment_template_grp");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PAYMENT_TEMPLATE_GRP);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP);
		//Vshah CR-586 [END]
		
		//AAlubala CR610 Rel7000 [BEGIN] 02/10/2011
		value = inputDoc.getAttribute("/SecurityProfile/maintain_forex_rates");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_FX_RATE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_FX_RATE);		
		//CR610 [END]
		//Srini_D CR-713 10/10/2011 Rel8.0 Start
		value = inputDoc.getAttribute("/SecurityProfile/maintain_interest_rates");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_INTEREST_RATE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_INTEREST_RATE);
		//Srini_D CR-713 10/10/2011 Rel8.0 End
		
		//Srini_D CR-713 11/10/2011 Rel8.0 Start
		value = inputDoc.getAttribute("/SecurityProfile/maintain_invoice_definitions");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_INVOICE_DEFINITION);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_INVOICE_DEFINITION);
		
		value = inputDoc.getAttribute("/SecurityProfile/maintain_trading_partnerrule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_RECEIVABLES_TRADING_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_RECEIVABLES_TRADING_RULES);
		
		//Srini_D CR-713 11/10/2011 Rel8.0 End
		
		//AAlubala - Rel8.2 CR741 - 12/17/2012 - BEGIN
		//erp gl and discount codes
		value = inputDoc.getAttribute("/SecurityProfile/maintain_erp_gl_codes");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ERP_GL_CODES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ERP_GL_CODES);
		
		value = inputDoc.getAttribute("/SecurityProfile/maintain_discount_codes");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_DISCOUNT_CODES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_DISCOUNT_CODES);	
		
		// smanohar PPX268 start
		value = inputDoc.getAttribute("/SecurityProfile/maintain_external_banks");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_EXTERNAL_BANK);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_EXTERNAL_BANK);	
		
		//smanohar PPX268 end
		
		
		//
		//CR741 - END
		
		//M Eerupula Rel8.3 CR776
		value = inputDoc.getAttribute("/SecurityProfile/maint_org_profile");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ORG_PROFILE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ORG_PROFILE);

		// Srinivasu_D CR-599 Rel8.4 11/21/2013 - Start
		value = inputDoc.getAttribute("/SecurityProfile/maint_payment_def");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_PAYMENT_FILE_DEF);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF);
		// Srinivasu_D CR-599 Rel8.4 11/21/2013 - End
		
	}

	value = inputDoc.getAttribute("/SecurityProfile/access_reports");
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_reports","N");
	}
	//Suresh CR-603 03/23/2011 Begin
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_REPORTS_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/maint_standard_report");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_STANDARD_REPORT);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_STANDARD_REPORT);

		value = inputDoc.getAttribute("/SecurityProfile/maint_custom_report");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CUSTOM_REPORT);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CUSTOM_REPORT);
	}
	//Suresh CR-603 END
	
	//CR-587 Rel7.1 [BEGIN]
		value = inputDoc.getAttribute("/SecurityProfile/maintain_gen_msg_cat");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_GM_CAT_GRP);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_GM_CAT_GRP);
	//CR-587 Rel7.1 [END]
	
	//DK - CR-581/640 Rel7.1 [BEGIN]
		value = inputDoc.getAttribute("/SecurityProfile/maintain_cross_rate_rule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CROSS_RATE_RULE);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CROSS_RATE_RULE);
	//DK - CR-581/640 Rel7.1 [END]

        // DK CR709 Rel8.2 Start
		value = inputDoc.getAttribute("/SecurityProfile/maint_lr_create_rule"); // IR T36000012189  
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_LR_CREATE_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_LR_CREATE_RULES);
		
		value = inputDoc.getAttribute("/SecurityProfile/atp_invoice_creation_rule");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ATP_INV_CREATE_RULES);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES);
		
		// DK CR709 Rel8.2 End	
	
	value = inputDoc.getAttribute("/SecurityProfile/access_adm_refdata");
	if(value ==null){
		inputDoc.setAttribute("/SecurityProfile/access_adm_refdata","N");
	}
	if (hasAccess(value)) {
		securityRights = securityRights.or(SecurityAccess.ACCESS_ADM_REFDATA_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/maint_adm_sec_prof");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ADM_SEC_PROF);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ADM_SEC_PROF);

		value = inputDoc.getAttribute("/SecurityProfile/maint_adm_users");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ADM_USERS);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ADM_USERS);

		value = inputDoc.getAttribute("/SecurityProfile/maint_announcements");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_ANNOUNCEMENT);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_ANNOUNCEMENT);
		
		value = inputDoc.getAttribute("/SecurityProfile/maint_locked_users");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_LOCKED_OUT_USERS);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
	}
	
		value = inputDoc.getAttribute("/SecurityProfile/access_org");
		
		if(value ==null){
			inputDoc.setAttribute("/SecurityProfile/access_org","N");
		}
		if (hasAccess(value)) {
			securityRights = securityRights.or(SecurityAccess.ACCESS_ORGANIZATION_AREA);

		value = inputDoc.getAttribute("/SecurityProfile/maint_cb_by_px");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CB_BY_PX);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CB_BY_PX);

		value = inputDoc.getAttribute("/SecurityProfile/maint_bg_by_cb");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_BG_BY_CB);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_BG_BY_CB);

		value = inputDoc.getAttribute("/SecurityProfile/maint_corp_by_cb");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CORP_BY_CB);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CORP_BY_CB);

		value = inputDoc.getAttribute("/SecurityProfile/maint_opbank_by_cb");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_OPBANK_BY_CB);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_OPBANK_BY_CB);

		value = inputDoc.getAttribute("/SecurityProfile/maint_corp_by_bg");
		if (canViewOnly(value)) securityRights = securityRights.or(SecurityAccess.VIEW_CORP_BY_BG);
		else if (canMaintain(value)) securityRights = securityRights.or(SecurityAccess.MAINTAIN_CORP_BY_BG);
		}
		

		if(isSelected(inputDoc.getAttribute("/SecurityProfile/view_uploaded_invoice")) || isSelected(inputDoc.getAttribute("/SecurityProfile/upload_invoice")) 
				  || isSelected(inputDoc.getAttribute("/SecurityProfile/remove_uploaded_files"))){
			inputDoc.setAttribute("/SecurityProfile/invoice_mgmt_view",TradePortalConstants.INDICATOR_YES);
		}else{
			inputDoc.setAttribute("/SecurityProfile/invoice_mgmt_view",TradePortalConstants.INDICATOR_NO);
		}
		
		if (isSelected(inputDoc.getAttribute("/SecurityProfile/invoice_mgmt_view"))) {
			securityRights = securityRights.or(SecurityAccess.ACCESS_INVOICE_MGT_AREA);

			value = inputDoc.getAttribute("/SecurityProfile/view_uploaded_invoice");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.VIEW_UPLOADED_INVOICES);

			value = inputDoc.getAttribute("/SecurityProfile/upload_invoice");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.UPLOAD_INVOICES);

			value = inputDoc.getAttribute("/SecurityProfile/remove_uploaded_files");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.REMOVE_UPLOADED_FILES);
		}
		// Srinivasu_D CR#269 Rel8.4 09/02/2013 - start

		if(isSelected(inputDoc.getAttribute("/SecurityProfile/cc_gua_create_mod")) || isSelected(inputDoc.getAttribute("/SecurityProfile/cc_gua_delete")) 
				  || isSelected(inputDoc.getAttribute("/SecurityProfile/cc_gua_route"))  || isSelected(inputDoc.getAttribute("/SecurityProfile/cc_gua_authorise"))){

			inputDoc.setAttribute("/SecurityProfile/access_conver_center",TradePortalConstants.INDICATOR_YES);
		}else{
			inputDoc.setAttribute("/SecurityProfile/access_conver_center",TradePortalConstants.INDICATOR_NO);
		}

		if (isSelected(inputDoc.getAttribute("/SecurityProfile/access_conver_center"))) {
			securityRights = securityRights.or(SecurityAccess.ACCESS_CONVERSION_CENTER_AREA);
	
			value = inputDoc.getAttribute("/SecurityProfile/cc_gua_create_mod");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CC_GUA_CREATE_MODIFY);

			value = inputDoc.getAttribute("/SecurityProfile/cc_gua_delete");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CC_GUA_DELETE);

			value = inputDoc.getAttribute("/SecurityProfile/cc_gua_route");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CC_GUA_ROUTE);

			value = inputDoc.getAttribute("/SecurityProfile/cc_gua_authorise");
			if (isSelected(value)) securityRights = securityRights.or(SecurityAccess.CC_GUA_CONVERT);
		}
		// Srinivasu_D CR#269 Rel8.4 09/02/2013 - end
	
		
		
		
		
		
		
		
		return securityRights.toString();
}
/**
 * Checks if a given value is Y.  (Also checks for null value).
 * If Y, true is returned, otherwise false.
 *
 * @return boolean
 * @param value java.lang.String
 */

private boolean isSelected(String value) {
	if (value == null) return false;
	else if (value.equals(TradePortalConstants.INDICATOR_YES)) return true;//changed indicator for TP refresh

	return false;
}

/**
 * Checks if a given value is Y.  (Also checks for null value).
 * If Y, true is returned, otherwise false.
 *
 * @return boolean
 * @param value java.lang.String
 */

private boolean hasAccess(String value) {
	if (value == null) return true;
	else if (value.equals(TradePortalConstants.INDICATOR_YES)) return false;//changed indicator for TP refresh

	return false;
}

}



















