package com.ams.tradeportal.mediator.util;
import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.IncomingInterfaceQueue;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.UniversalMessage;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AdminMailOutPackager {
private static final Logger LOG = LoggerFactory.getLogger(AdminMailOutPackager.class);

	
	
	public static void process( String bankMailMessageOid, String assignCorpOrgOid, DocumentHandler inputDoc, MediatorServices mediatorServices ) 
			throws NumberFormatException, RemoteException, AmsException {
		
	  try {
		DocumentHandler outputDoc =new DocumentHandler();
		String messageType = MessageType.MAIL;
		String messageID = InstrumentServices.getNewMessageID();
		String messageSubject = inputDoc.getAttribute("/BankMailMessage/message_subject");
		String messageText = inputDoc.getAttribute("/BankMailMessage/message_text");
		String instrumentID = inputDoc.getAttribute("/Instrument/complete_instrument_id"); 
		boolean packageStatus = true;
		CorporateOrganization corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization");
		ClientBank clientBank         = (ClientBank) mediatorServices.createServerEJB("ClientBank");
		OperationalBankOrganization  operationalBankOrganization = (OperationalBankOrganization) mediatorServices.createServerEJB("OperationalBankOrganization");
				
		//String assign_corp_org_oid = bankMailMessage.getAttribute("assigned_to_corp_org_oid");
		
		if(StringFunction.isNotBlank(messageSubject)){
			messageSubject = StringFunction.xssHtmlToChars(messageSubject);
		}
		
		if( StringFunction.isNotBlank(assignCorpOrgOid) ) {
        	corpOrg.getData(Long.parseLong(assignCorpOrgOid));
		} else {
        	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE ,TradePortalConstants.SEND_TO_CUST_REQ,"CorporateOrg","BankMailMessage");
        	packageStatus = false;
	    }
		
		String a_client_bank_oid = corpOrg.getAttribute("client_bank_oid");
		if (StringFunction.isNotBlank(a_client_bank_oid)) {
			clientBank.getData(Long.parseLong(a_client_bank_oid));
		}

		String operationOrganizationID = "";
		String first_op_bank_org_oid = corpOrg.getAttribute("first_op_bank_org");
		if ( StringFunction.isNotBlank(first_op_bank_org_oid) ) {
			operationalBankOrganization.getData(Long.parseLong(first_op_bank_org_oid));
			operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
		}
		
		
		prePopulateMessage(outputDoc);
		
		 if(packageStatus == true) {
			 
			 mediatorServices.debug("Ready to get Header Attributes : ");
		     packageHeader( messageType, messageID, operationOrganizationID, outputDoc );
		     
		     mediatorServices.debug("Ready to get subHeader Attributes : ");
		     packagesubHeader( corpOrg, clientBank, instrumentID, outputDoc );
		     
		     mediatorServices.debug("Ready to get Message Body Attributes : ");
		     packagemailMessage( messageSubject, messageText, outputDoc );
		     
		     mediatorServices.debug("Ready to get Attached doc Attributes : ");
		     packageAttachedDocuments( bankMailMessageOid, outputDoc );
		     
		     mediatorServices.debug("Ready to get timestamp Attributes : ");
		     packageTimestamp( outputDoc ); 
		     
		     mediatorServices.debug("creating entry in incoming queue : ");
		     createIncomingQueueEntry(messageType, (outputDoc.getFragment("/Proponix")).toString(), mediatorServices);
		     
		     mediatorServices.debug("MAIL message is created for customer - " + corpOrg.getAttribute("name") + " with messageID: " + messageID);
		     
	      }
	
	  } catch( RemoteException | AmsException e ) {
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,TradePortalConstants.TASK_NOT_SUCCESSFUL,"Bank MailMessage Packaging");
	    e.printStackTrace();
	  }
	
	}	

	/**
	 * 
	 * @param bankMailMessage
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	private static void packagemailMessage( String messageSubject, String messageText, DocumentHandler outputDoc ) throws RemoteException, AmsException {
		
		outputDoc.setAttribute("/Proponix/Body/MessageSubject", messageSubject );
		outputDoc.setAttribute("/Proponix/Body/MessageText", messageText );				
	}

	private static void packagesubHeader(CorporateOrganization corpOrg, ClientBank clientBank, String instrumentID, DocumentHandler outputDoc) 
			throws RemoteException, AmsException {
		
		 outputDoc.setAttribute("/Proponix/SubHeader/CustomerID", corpOrg.getAttribute("Proponix_customer_id"));
		 outputDoc.setAttribute("/Proponix/SubHeader/ClientBank/OTLID", clientBank.getAttribute("OTL_id"));	
		 outputDoc.setAttribute("/Proponix/SubHeader/InstrumentID", instrumentID);
	}

	/**
	 * 
	 * @param messageType
	 * @param messageID
	 * @param operationOrganizationID
	 * @param outputDoc
	 * @throws AmsException
	 */
	private static void packageHeader(String messageType, String messageID, String operationOrganizationID, DocumentHandler outputDoc) 
			throws AmsException {
		
		 String headerPath = UniversalMessage.headerPath;

	     String destinationID    = TradePortalConstants.DESTINATION_ID_TPL;
	     String senderID         = TradePortalConstants.DESTINATION_ID_PROPONIX;
	     String dateSent         = null;
	     String timeSent         = null;
         String timeStamp   = DateTimeUtility.getGMTDateTime(false);
         dateSent    =  timeStamp;
         timeSent    =  timeStamp.substring(11,19);

         outputDoc.setAttribute(headerPath+"/DestinationID",destinationID);
         outputDoc.setAttribute(headerPath+"/SenderID",senderID);
         outputDoc.setAttribute(headerPath+"/OperationOrganizationID", operationOrganizationID);
         outputDoc.setAttribute(headerPath+"/MessageType",messageType);
         outputDoc.setAttribute(headerPath+"/DateSent", dateSent);
         outputDoc.setAttribute(headerPath+"/TimeSent",timeSent);
         outputDoc.setAttribute(headerPath+"/MessageID",messageID);
		
	}

	/**
	 * This method is used to create mail message for required element to process.
	 * @param outputDoc
	 * @throws RemoteException
	 * @throws AmsException
	 */
	public static void prePopulateMessage(DocumentHandler outputDoc) throws RemoteException, AmsException {
		outputDoc.setAttribute("/Proponix/Header/DestinationID", "");
		outputDoc.setAttribute("/Proponix/Header/SenderID", "");
		outputDoc.setAttribute("/Proponix/Header/OperationOrganizationID", "");
		outputDoc.setAttribute("/Proponix/Header/MessageType", "");
		outputDoc.setAttribute("/Proponix/Header/DateSent", "");
		outputDoc.setAttribute("/Proponix/Header/TimeSent", "");
		outputDoc.setAttribute("/Proponix/Header/MessageID", "");

		outputDoc.setAttribute("/Proponix/SubHeader/CustomerID", "");
		outputDoc.setAttribute("/Proponix/SubHeader/ClientBank/OTLID", "");
		outputDoc.setAttribute("/Proponix/SubHeader/InstrumentID", "");

		outputDoc.setAttribute("/Proponix/Body/MessageSubject", "");
		outputDoc.setAttribute("/Proponix/Body/MessageText", "");
		outputDoc.setAttribute("/Proponix/Body/SettlementInstructionFlag", "");
		outputDoc.setAttribute("/Proponix/Body/DiscrepancyFlag", "");
		outputDoc.setAttribute("/Proponix/Body/DiscrepancyAmount", "");
		outputDoc.setAttribute("/Proponix/Body/DiscrepancyCurrency", "");
		outputDoc.setAttribute("/Proponix/Body/ProcessedByBank", "");
		outputDoc.setAttribute("/Proponix/Body/WorkItemNumber", "");
		
		outputDoc.setAttribute("/Proponix/Timestamp/Date", "");
		outputDoc.setAttribute("/Proponix/Timestamp/Time", "");
	}

    /**
     * 
     * @param bankMailMessageOid
     * @param outputDoc
     * @throws AmsException 
     */
	private static void packageAttachedDocuments( String bankMailMessageOid, DocumentHandler outputDoc ) throws AmsException {
		
		if ( StringFunction.isNotBlank(bankMailMessageOid) ) {
			
  			 String documentImagePath = "/Proponix/Body/DocumentImage";
  			 outputDoc.setAttribute(documentImagePath+"/NumberOfEntries", "0");
			 String sql = "select IMAGE_ID, NUM_PAGES, IMAGE_FORMAT, IMAGE_BYTES, DOC_NAME, HASH from DOCUMENT_IMAGE where FORM_TYPE = ? and MAIL_MESSAGE_OID  = ? ";
			 DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED, bankMailMessageOid});
	  		 if((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {
	  			List<DocumentHandler> resultList = resultXML.getFragmentsList("/ResultSetRecord/");
				outputDoc.setAttribute(documentImagePath+"/NumberOfEntries", String.valueOf(resultList.size()));
	  			int i = 0;
	  			for( DocumentHandler myDoc : resultList ) {	  				
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/FormType",
	  						TradePortalConstants.DOC_IMG_FORM_TYPE_OTL_ATTACHED.substring(0,1).toUpperCase());
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageID", myDoc.getAttribute("/IMAGE_ID"));
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/Text", myDoc.getAttribute("/DOC_NAME"));
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageFormat", myDoc.getAttribute("/IMAGE_FORMAT"));
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/NumPages", myDoc.getAttribute("/NUM_PAGES"));
	  				outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/ImageBytes", myDoc.getAttribute("/IMAGE_BYTES"));	  				
	  				String hash = myDoc.getAttribute("/HASH");
	  				if ( hash != null && hash.length()>0 ) {
	                    outputDoc.setAttribute(documentImagePath+"/FullDocumentImage("+i+")/Hash", myDoc.getAttribute("/HASH"));
	  				}
	  				i++;
	  			} 
		    }
	   }
	}

	/**
	 * 
	 * @param outputDoc
	 */
	private static void packageTimestamp(DocumentHandler outputDoc) {
		
		outputDoc.setAttribute("/Proponix/Timestamp/Date",  DateTimeUtility.getCurrentDateTime( "yyyyMMdd" ) );
		outputDoc.setAttribute("/Proponix/Timestamp/Time",  DateTimeUtility.getCurrentDateTime( "HHmmss" ) );
		
	}
	
	public static void createIncomingQueueEntry( String messageType, String messageText, MediatorServices mediatorServices ) 
			throws RemoteException, AmsException {
		 IncomingInterfaceQueue incoming_queue = (IncomingInterfaceQueue) mediatorServices.createServerEJB("IncomingInterfaceQueue");
         incoming_queue.newObject();
         String queueOid = incoming_queue.getAttribute("queue_oid");
         incoming_queue.setAttribute("date_received",   DateTimeUtility.getGMTDateTime(false) );
         incoming_queue.setAttribute("msg_text",        messageText);
         incoming_queue.setAttribute("status",          TradePortalConstants.INCOMING_STATUS_RECEIVED);
         incoming_queue.setAttribute("msg_type",        messageType);
         int saveSuccess = incoming_queue.save();
         mediatorServices.debug("Save new Bank Mail message-  queue_oid="+ queueOid +", msg_type="+messageType);
         if (saveSuccess < 0) {
        	 throw new AmsException(" Error Occured while creating Mail message entry in incoming queue for Bank Admin User ");
         }            
	}
	
}
