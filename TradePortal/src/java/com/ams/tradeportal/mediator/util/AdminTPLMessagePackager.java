package com.ams.tradeportal.mediator.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.CorporateOrganization;
import com.ams.tradeportal.busobj.IncomingInterfaceQueue;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.MessageType;
import com.ams.tradeportal.common.TPLMessage;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionStatus;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;

public class AdminTPLMessagePackager {
private static final Logger LOG = LoggerFactory.getLogger(AdminTPLMessagePackager.class);

   public static void process( String bankUpdateTransOid, String bankTransStatus, String transactionOid, 
		   DocumentHandler inputDoc, MediatorServices mediatorServices ) throws AmsException {
      
	try {			
		 boolean packageStatus = true; 
		 ClientBank clientBank = null;
		 OperationalBankOrganization operationalBankOrganization = null;
		 CorporateOrganization corpOrg = null;

		 Transaction transaction = (Transaction) mediatorServices.
				 createServerEJB( "Transaction", Long.parseLong(transactionOid) );
		 String instrumentOid = transaction.getAttribute("instrument_oid");
		 Instrument instrument = (Instrument) mediatorServices.
				 createServerEJB( "Instrument", Long.parseLong(instrumentOid) );
		 String clientBankOid = instrument.getAttribute("client_bank_oid");
		 Terms customerEnteredTerms = (Terms) (transaction.getComponentHandle("CustomerEnteredTerms"));
		 
		 if ( StringFunction.isNotBlank(clientBankOid) ) {			 
			 clientBank = (ClientBank) mediatorServices.
					 createServerEJB("ClientBank", Long.parseLong(clientBankOid) );
		 } else {
			 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.NO_ASSOCIATED_VALUE, "ClientBank", "Instrument");
			 packageStatus = false;
		 }
		 
		 String opBankOrgOid = instrument.getAttribute("op_bank_org_oid");
		 if ( StringFunction.isNotBlank(opBankOrgOid) ) {
			 operationalBankOrganization = (OperationalBankOrganization) mediatorServices
						.createServerEJB("OperationalBankOrganization", Long.parseLong(opBankOrgOid));
		 } else {
			 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.NO_ASSOCIATED_VALUE, "Operational Bank Org", "Instrument");
				packageStatus = false;
		 }
		 
		 String corpOrgOid = instrument.getAttribute("corp_org_oid");
		 if ( StringFunction.isNotBlank(corpOrgOid) ) {
			 corpOrg = (CorporateOrganization) mediatorServices.createServerEJB("CorporateOrganization", Long.parseLong(corpOrgOid));
		 } else {
			 mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
						TradePortalConstants.NO_ASSOCIATED_VALUE, "CorporateOrg", "Instrument");
			 packageStatus = false;
		 }
			
		if ( packageStatus ) {
			
			DocumentHandler outputDoc = new DocumentHandler();
			TPLMessage.getUniversalMessage(outputDoc);
			String messageType = MessageType.TPL;
			String messageID = InstrumentServices.getNewMessageID();
			
			mediatorServices.debug("Packaging Started for TPS message: ");
			
			packageHeader( messageType, messageID, operationalBankOrganization, outputDoc);
			
			packageSubHeader( instrument, bankTransStatus, clientBank, corpOrg, outputDoc );
			
			packageBody( corpOrg, instrument, customerEnteredTerms, transaction, bankTransStatus, bankUpdateTransOid, inputDoc, outputDoc );
									 			
			packageTimestamp(outputDoc);
			
			mediatorServices.debug("Packaging Finished for TPS message: ");
			
			mediatorServices.debug("creating entry in incoming queue : ");
		    createIncomingQueueEntry(messageType, (outputDoc.getFragment("/Proponix")).toString(), mediatorServices);
		}	
			
	} catch ( RemoteException | AmsException e) {
		mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_MIDDLEWARE,
				TradePortalConstants.BANK_TASK_NOT_SUCCESSFUL);
		e.printStackTrace();
	}
		
  }
   
   /**
    * 
    * @param messageType
    * @param messageID
    * @param operationalBankOrganization
    * @param outputDoc
    * @throws RemoteException
    * @throws AmsException
    */
   private static void packageHeader(String messageType, String messageID, OperationalBankOrganization operationalBankOrganization,
   		DocumentHandler outputDoc) throws RemoteException, AmsException {
   	
   	String headerPath = TPLMessage.headerPath;
   	String destinationID = TradePortalConstants.DESTINATION_ID_TPL;
   	String senderID = TradePortalConstants.SENDER_ID_PROPONIX;
   	String dateSent = null;
   	String timeSent = null;
   	String operationOrganizationID = operationalBankOrganization.getAttribute("Proponix_id");
   	String timeStamp = DateTimeUtility.getGMTDateTime(false);
   	dateSent = timeStamp;
   	timeSent = timeStamp.substring(11, 19);

   	outputDoc.setAttribute(headerPath + "/DestinationID", destinationID);
   	outputDoc.setAttribute(headerPath + "/SenderID", senderID);
   	outputDoc.setAttribute(headerPath + "/OperationOrganizationID", operationOrganizationID);
   	outputDoc.setAttribute(headerPath + "/DateSent", dateSent);
   	outputDoc.setAttribute(headerPath + "/TimeSent", timeSent);
   	outputDoc.setAttribute(headerPath + "/MessageType", messageType);
   	outputDoc.setAttribute(headerPath + "/MessageID", messageID);
   	
   }
   
   /**
    * 
    * @param instrument
    * @param bankTransStatus
    * @param clientBank
    * @param corpOrg 
    * @param outputDoc
    * @throws RemoteException
    * @throws AmsException
    */
   private static void packageSubHeader(Instrument instrument, String bankTransStatus, ClientBank clientBank, CorporateOrganization corpOrg, 
		   DocumentHandler outputDoc) throws RemoteException, AmsException {
	   
	     outputDoc.setAttribute( TPLMessage.subHeaderPath + "/CustomerID", corpOrg.getAttribute("Proponix_customer_id") );
	
	     packageInstrument( instrument, bankTransStatus, outputDoc );
		
		 packageClientBankAttributes( clientBank, outputDoc );
	
   }

   /**
    * 
    * @param corpOrg 
 * @param instrument 
 * @param customerEnteredTerms
    * @param transaction
    * @param bankTransStatus
    * @param bankUpdateTransOid
    * @param outputDoc
    * @throws RemoteException
    * @throws AmsException
    */
   private static void packageBody(CorporateOrganization corpOrg, Instrument instrument, Terms customerEnteredTerms, Transaction transaction, String bankTransStatus, String bankUpdateTransOid, 
		   DocumentHandler inputDoc, DocumentHandler outputDoc) throws RemoteException, AmsException {
	
	    packageTransaction( instrument, transaction, bankTransStatus, inputDoc, outputDoc );
		
		packageTerms( instrument, customerEnteredTerms, outputDoc );
		
		packageTermsParty( instrument, transaction, customerEnteredTerms, outputDoc );
		
		packagePosition( corpOrg, instrument, transaction, bankTransStatus, customerEnteredTerms, outputDoc );
		
		packageAttachedDocument( transaction, bankUpdateTransOid, outputDoc );
	
}




/**
    * 
    * @param corpOrg 
 * @param transaction 
 * @param instrument 
 * @param bankTransStatus 
 * @param customerEnteredTerms
    * @param outputDoc
    * @throws RemoteException
    * @throws AmsException
    */
private static void packagePosition(CorporateOrganization corpOrg, Instrument instrument, Transaction transaction, String bankTransStatus, Terms customerEnteredTerms, DocumentHandler outputDoc) throws RemoteException, AmsException {
	
	String positionPath = TPLMessage.positionPath;	
	BigDecimal availableAmount = BigDecimal.ZERO;
	
	/*for amend add or sub transaction amount from instrument amount*/
	if(TradePortalConstants.BANK_TRANSACTION_STATUS_PROCESSED_BY_BANK.equals(bankTransStatus)){

		if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
			if(customerEnteredTerms.getAttributeDecimal("amount").signum() == -1){
				availableAmount = ( instrument.getAttributeDecimal("copy_of_instrument_amount")
						.subtract(customerEnteredTerms.getAttributeDecimal("amount").abs()) ) ;

			}else{
				availableAmount = ( instrument.getAttributeDecimal("copy_of_instrument_amount")
						.add(customerEnteredTerms.getAttributeDecimal("amount")) ) ;
			}
		}else{

			availableAmount = instrument.getAttributeDecimal("copy_of_instrument_amount");
		}
	}
	
	outputDoc.setAttribute(positionPath + "/InstrumentBaseCurrencyCode", corpOrg.getAttribute("base_currency_code"));
	outputDoc.setAttribute(positionPath + "/InstrumentLimitCurrencyCode", customerEnteredTerms.getAttribute("amount_currency_code"));
	if(TradePortalConstants.BANK_TRANSACTION_STATUS_PROCESSED_BY_BANK.equals(bankTransStatus)){
		outputDoc.setAttribute(positionPath + "/CurrentInstrumentAmount", availableAmount.toPlainString());
	}	
	outputDoc.setAttribute(positionPath + "/RevalueDate", DateTimeUtility.getGMTDateTime(false));
	
}

/**
 * 
 * @param instrument 
 * @param customerEnteredTerms
 * @param outputDoc
 * @throws AmsException
 * @throws RemoteException
 */
private static void packageTerms(Instrument instrument, Terms customerEnteredTerms, DocumentHandler outputDoc) 
		throws AmsException, RemoteException {
	
	String termsPath = TPLMessage.termsPath;
	String expiryDate = "";
	outputDoc.setAttribute(termsPath + "/TermsDetails/ValueDate", DateTimeUtility.getGMTDateTime(false));
	if ( customerEnteredTerms.isAttributeRegistered("expiry_date") ) {
		/*if expiry date (new) is blank, get it from instument (current expiry date)*/
		if(StringFunction.isBlank(customerEnteredTerms.getAttribute("expiry_date"))){
			expiryDate = instrument.getAttribute("copy_of_expiry_date");
		}else{
			expiryDate = customerEnteredTerms.getAttribute("expiry_date");
		}
		outputDoc.setAttribute(termsPath + "/TermsDetails/ExpiryDate", expiryDate);
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/ExpiryDate", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("reference_number") ) {
		outputDoc.setAttribute(termsPath + "/TermsDetails/ReferenceNumber", customerEnteredTerms.getAttribute("reference_number"));
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/ReferenceNumber", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("amount_currency_code") ) {
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountCurrencyCode", customerEnteredTerms.getAttribute("amount_currency_code"));
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountCurrencyCode", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("amount") ) {
		outputDoc.setAttribute(termsPath + "/TermsDetails/Amount", customerEnteredTerms.getAttribute("amount"));
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/Amount", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("amt_tolerance_pos") ) {
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountTolerancePositive", customerEnteredTerms.getAttribute("amt_tolerance_pos"));
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountTolerancePositive", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("amt_tolerance_neg") ) {
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountToleranceNegative", customerEnteredTerms.getAttribute("amt_tolerance_neg"));
	}else{
		outputDoc.setAttribute(termsPath + "/TermsDetails/AmountToleranceNegative", "");
	}
	String latestShipmentDate = "";
	ShipmentTerms shipmentTerms = customerEnteredTerms.getFirstShipment();
	if ( shipmentTerms != null ) {
		latestShipmentDate = shipmentTerms.getAttribute("latest_shipment_date");
	}
	outputDoc.setAttribute(termsPath + "/ShipmentTerms/LatestShipmentDate", latestShipmentDate );
	
	if ( customerEnteredTerms.isAttributeRegistered("present_docs_within_days") ) {
		outputDoc.setAttribute(termsPath + "/PresentationDays", customerEnteredTerms.getAttribute("present_docs_within_days"));
	}else{
		outputDoc.setAttribute(termsPath + "/PresentationDays", "");
	}
	
	if ( customerEnteredTerms.isAttributeRegistered("loan_terms_type") ) {
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsType", customerEnteredTerms.getAttribute("loan_terms_type"));
	}else{
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsType", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("loan_rate") ) {
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanRate", customerEnteredTerms.getAttribute("loan_rate"));
	}else{
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanRate", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("loan_terms_fixed_maturity_dt") ) {
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsFixedMaturityDate", customerEnteredTerms.getAttribute("loan_terms_fixed_maturity_dt"));
	}else{
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsFixedMaturityDate", "");
	}
	if ( customerEnteredTerms.isAttributeRegistered("loan_terms_number_of_days") ) {
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsNumberOfDays", customerEnteredTerms.getAttribute("loan_terms_number_of_days"));
	}else{
		outputDoc.setAttribute(termsPath + "/LoanTerms/LoanTermsNumberOfDays", "");
	}
	
	//Payment Terms
	String tenorSql = "select PMT_TERMS_TENOR_DTL_OID, PERCENT, TENOR_TYPE, NUM_DAYS_AFTER, DAYS_AFTER_TYPE, "
			+ "TO_CHAR(MATURITY_DATE,'MM/dd/yyyy HH:mm:ss') as MATURITY_DATE from PMT_TERMS_TENOR_DTL "
			+ "where P_TERMS_OID = ?";
	DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(tenorSql, false , new Object[] {customerEnteredTerms.getAttribute("terms_oid")});
	
	int recordSize = -1;
	if (null != resultSet){
		List<DocumentHandler> tempResults = resultSet.getFragmentsList("/ResultSetRecord");		
    	recordSize = tempResults.size();
    	if(recordSize == 1){
    		for(DocumentHandler mydoc : tempResults){
    			
    			if(StringFunction.isNotBlank(mydoc.getAttribute("/TENOR_TYPE"))){
    				if(StringFunction.isBlank(mydoc.getAttribute("/MATURITY_DATE"))){
    					if( (TradePortalConstants.BANK_ACCEPTANCE.equals(mydoc.getAttribute("/TENOR_TYPE")))
    							|| (TradePortalConstants.BANK_DEFERED_PAY.equals(mydoc.getAttribute("/TENOR_TYPE"))) ){
    						outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsType", TradePortalConstants.BANK_DAYS);	
    					}else{
    						outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsType", mydoc.getAttribute("/TENOR_TYPE"));	
    					}
    					
    				}else if( !(TradePortalConstants.BANK_ACCEPTANCE.equals(mydoc.getAttribute("/TENOR_TYPE")))
							&& !(TradePortalConstants.BANK_DEFERED_PAY.equals(mydoc.getAttribute("/TENOR_TYPE"))) ){
    					outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsType", mydoc.getAttribute("/TENOR_TYPE"));
    				}
    			}
    			if(StringFunction.isNotBlank(mydoc.getAttribute("/NUM_DAYS_AFTER"))){
    				outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsNumberDaysAfter", mydoc.getAttribute("/NUM_DAYS_AFTER"));
    			}
    			if(StringFunction.isNotBlank(mydoc.getAttribute("/DAYS_AFTER_TYPE"))){
    				outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsDaysAfterType", mydoc.getAttribute("/DAYS_AFTER_TYPE"));
    			}
    			if(StringFunction.isNotBlank(mydoc.getAttribute("/PERCENT"))){
    				outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsPercentInvoice", mydoc.getAttribute("/PERCENT"));
    			}
    			if(StringFunction.isNotBlank(mydoc.getAttribute("/MATURITY_DATE"))){
    				outputDoc.setAttribute(termsPath + "/PaymentTerms/PaymentTermsFixedMaturityDate",  mydoc.getAttribute("/MATURITY_DATE"));
    			}
    		}
    	}
	}
}

/**
 * 
 * @param instrument 
 * @param transaction
 * @param bankTransactionStatus
 * @param outputDoc
 * @throws RemoteException
 * @throws AmsException
 */
private static void packageTransaction(Instrument instrument, Transaction transaction, String bankTransactionStatus, DocumentHandler inputDoc, DocumentHandler outputDoc) 
		throws RemoteException, AmsException {
	
	String transactionPath = TPLMessage.transactionPath;
	String timeStamp = DateTimeUtility.getGMTDateTime(false);
	int sequence = 0;
	List<Object> sqlParam = new ArrayList<Object>();
	DocumentHandler sequenceCount = null;	
	String seqSql = null;
	
	if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
		
		seqSql = "select count(transaction_oid) as SEQ_COUNT from transaction "
				+ "where TRANSACTION_TYPE_CODE=? and TRANSACTION_STATUS =? AND P_INSTRUMENT_OID = ?";
		
		sqlParam.add(TransactionType.AMEND);
		sqlParam.add(TransactionStatus.PROCESSED_BY_BANK);
		sqlParam.add(instrument.getAttribute("instrument_oid"));

		sequenceCount = DatabaseQueryBean.getXmlResultSet(seqSql, false, sqlParam);

		if(null != sequenceCount){
			sequence = sequenceCount.getAttributeInt("/ResultSetRecord/SEQ_COUNT");
		}
		
		outputDoc.setAttribute(transactionPath + "/SequenceNumber", ""+(++sequence));
	}
	
	outputDoc.setAttribute(transactionPath + "/TransactionTypeCode", transaction.getAttribute("transaction_type_code"));	
	outputDoc.setAttribute(transactionPath + "/TransactionStatus", bankTransactionStatus);
	outputDoc.setAttribute(transactionPath + "/TransactionStatusDate", timeStamp);
	outputDoc.setAttribute(transactionPath + "/SequenceDate", timeStamp);
	outputDoc.setAttribute(transactionPath + "/ReturnMessageID", transaction.getAttribute("return_message_id"));
	if ( TransactionStatus.REJECTED_BY_BANK.equals(bankTransactionStatus)) {
		outputDoc.setAttribute( transactionPath + "/RejectReason", inputDoc.getAttribute("/BankTransactionUpdate/bank_rejection_reason_text") );
	}
	
}

/**
 * 
 * @param clientBank
 * @param outputDoc
 * @throws RemoteException
 * @throws AmsException
 */
private static void packageClientBankAttributes(ClientBank clientBank, DocumentHandler outputDoc) 
		throws RemoteException, AmsException {
	
	String clientBankPath = TPLMessage.clientBankPath;
	String OTL_id = clientBank.getAttribute("OTL_id");
	outputDoc.setAttribute(clientBankPath + "/OTLID", OTL_id);
	
}

/**
 * 
 * @param instrument
 * @param bankTransactionStatus
 * @param outputDoc
 * @throws RemoteException
 * @throws AmsException
 */
private static void packageInstrument(Instrument instrument, String bankTransactionStatus, DocumentHandler outputDoc) 
		throws RemoteException, AmsException {
	
	String instrumentPath = TPLMessage.instrumentPath;
	String instrumentStatus = null;
	if ( TransactionStatus.REJECTED_BY_BANK.equals(bankTransactionStatus)) {
		instrumentStatus = TradePortalConstants.INSTR_STATUS_PENDING;
	} else {
		instrumentStatus = TradePortalConstants.INSTR_STATUS_ACTIVE;
	}
	outputDoc.setAttribute(instrumentPath + "/InstrumentIDNumber", instrument.getAttribute("instrument_num"));
	outputDoc.setAttribute(instrumentPath + "/InstrumentIDPrefix", instrument.getAttribute("instrument_prefix"));
	outputDoc.setAttribute(instrumentPath + "/InstrumentIDSuffix", instrument.getAttribute("instrument_suffix"));
	outputDoc.setAttribute(instrumentPath + "/InstrumentID", instrument.getAttribute("complete_instrument_id"));
	outputDoc.setAttribute(instrumentPath + "/InstrumentStatus", instrumentStatus);
	outputDoc.setAttribute(instrumentPath + "/InstrumentTypeCode", instrument.getAttribute("instrument_type_code"));
	outputDoc.setAttribute(instrumentPath + "/IssueDate", DateTimeUtility.getGMTDateTime(false) );	
}

/**
 * 
 * @param bankUpdateTransactionOid
 * @param outputDoc
 * @throws AmsException
 */
private static void packageAttachedDocument(Transaction transaction, String bankUpdateTransactionOid, DocumentHandler outputDoc) 
		throws AmsException, RemoteException {
	
	String sql = "select IMAGE_ID, NUM_PAGES, IMAGE_FORMAT, IMAGE_BYTES, DOC_NAME, HASH from DOCUMENT_IMAGE where FORM_TYPE = ?"
			+ " and P_TRANSACTION_OID  = ?";	
	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED, 
			bankUpdateTransactionOid });
	
	if ((resultXML != null) && (resultXML.getFragmentsList("/ResultSetRecord/").size() > 0)) {
		List<DocumentHandler> resultList = resultXML.getFragmentsList("/ResultSetRecord/");
		String documentImagePath = TPLMessage.documentImagePath;
		outputDoc.setAttribute(documentImagePath+ "/TotalNumberOfEntries", String.valueOf(resultList.size()));
		int i = 0;
		for (DocumentHandler myDoc : resultList) {
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage("+ i + ")/FormType", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageID", myDoc.getAttribute("/IMAGE_ID"));
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/DocumentText",myDoc.getAttribute("/DOC_NAME"));
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/NumPages", myDoc.getAttribute("/NUM_PAGES"));
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageFormat", myDoc.getAttribute("/IMAGE_FORMAT"));
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/ImageBytes", myDoc.getAttribute("/IMAGE_BYTES"));
			outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/DocName", myDoc.getAttribute("/DOC_NAME"));
			String imageHash = myDoc.getAttribute("/HASH");
			if ( imageHash != null && imageHash.length()>0 ) {
			    outputDoc.setAttribute(documentImagePath + "/FullDocumentImage(" + i + ")/Hash", imageHash );
			}
		  i++;
		} 
	}
}

/**
 * 
 * @param instrument 
 * @param transaction 
 * @param customerEnteredTerms
 * @param outputDoc
 * @throws RemoteException
 * @throws AmsException
 */
private static void packageTermsParty(Instrument instrument, Transaction transaction, Terms customerEnteredTerms, DocumentHandler outputDoc) throws RemoteException, AmsException {
	
	DocumentHandler termsDoc = new DocumentHandler();
	String termsPartyPath = TPLMessage.termsPartyPath;
	
	
	long transactionOid = instrument.getAttributeLong("original_transaction_oid");	
	Transaction amendTransaction = (Transaction) instrument.getComponentHandle("TransactionList", transactionOid);
	Terms amendTerms = (Terms)amendTransaction.getComponentHandle("CustomerEnteredTerms");
	
	termsDoc.addComponent("/TermsParty", outputDoc.getFragment(termsPartyPath));
	//for amend there is no direct association of termsparty. get it from issue
	if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
		termsDoc = amendTerms.getTermsAttributesDoc(termsDoc);
	}else{
		termsDoc = customerEnteredTerms.getTermsAttributesDoc(termsDoc);
	}	
	outputDoc.setComponent(termsPartyPath, termsDoc.getFragment("/TermsParty"));
	int tpXMLCount =new Integer(outputDoc.getAttribute(TPLMessage.instrumentTransactionPath + "/TermsParty/TotalNumberOfEntries"));
	String OTLAddressSequenceNo = null;
	for (int tpXMLIndex = 0; tpXMLIndex < tpXMLCount; tpXMLIndex++)
	{
		OTLAddressSequenceNo = outputDoc.getAttribute(TPLMessage.instrumentTransactionPath+ "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/OTLAddressSequenceNum");
		outputDoc.removeComponent(TPLMessage.instrumentTransactionPath+ "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/OTLAddressSequenceNum");
		outputDoc.setAttribute(TPLMessage.instrumentTransactionPath+ "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/OTLAddressSequenceNo", OTLAddressSequenceNo);
		outputDoc.setAttribute(TPLMessage.instrumentTransactionPath+ "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/VendorID", "");
		outputDoc.setAttribute(TPLMessage.instrumentTransactionPath+ "/TermsParty/FullTermsParty(" + tpXMLIndex + ")/ReferenceNumber", "");
	}
	
	
}

/**
 * This method is used to create timestamp.
 * @param outputDoc
 */
private static void packageTimestamp(DocumentHandler outputDoc) {
	
	outputDoc.setAttribute("/Proponix/Timestamp/Date",  DateTimeUtility.getCurrentDateTime( "yyyyMMdd" ) );
	outputDoc.setAttribute("/Proponix/Timestamp/Time",  DateTimeUtility.getCurrentDateTime( "HHmmss" ) );
	
}

/**
 * This method is used to create TPL message entry in incoming queue for corporate customer.
 * @param messageType
 * @param messageText
 * @param mediatorServices
 * @throws RemoteException
 * @throws AmsException
 */
public static void createIncomingQueueEntry( String messageType, String messageText, MediatorServices mediatorServices ) 
		throws RemoteException, AmsException {
	 IncomingInterfaceQueue incoming_queue = (IncomingInterfaceQueue) mediatorServices.createServerEJB("IncomingInterfaceQueue");
     incoming_queue.newObject();
     String queueOid = incoming_queue.getAttribute("queue_oid");
     incoming_queue.setAttribute("date_received",   DateTimeUtility.getGMTDateTime(false) );
     incoming_queue.setAttribute("msg_text",        messageText);
     incoming_queue.setAttribute("status",          TradePortalConstants.INCOMING_STATUS_RECEIVED);
     incoming_queue.setAttribute("msg_type",        messageType);
     int saveSuccess = incoming_queue.save();
     mediatorServices.debug("Save new Bank Mail message-  queue_oid="+ queueOid +", msg_type="+messageType);
     if (saveSuccess < 0) {
    	 throw new AmsException(" Error Occured while creating Mail message entry in incoming queue for Bank Admin User ");
     }            
}


}
