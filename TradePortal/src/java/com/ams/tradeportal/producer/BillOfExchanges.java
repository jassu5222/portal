package com.ams.tradeportal.producer;
import java.rmi.RemoteException;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.ClientBank;
import com.ams.tradeportal.busobj.Instrument;
import com.ams.tradeportal.busobj.OperationalBankOrganization;
import com.ams.tradeportal.busobj.Terms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.TradePortalBusinessObject;
import com.ams.tradeportal.busobj.Transaction;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TermsPartyType;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.common.TransactionType;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: BillOfExchanges.xsl.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
//BSL Cocoon Upgrade 10/13/11 Rel 7.0 BEGIN
//public class BillOfExchanges extends AbstractProducer implements Status {
//String headerStringPart1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "<?xml-stylesheet href=\"";
//String headerStringPart2 = "/xsl/BillOfExchanges.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";
//
//public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//	DocumentHandler xmlDoc = new DocumentHandler("<BillOfExchanges></BillOfExchanges>",false);
//	String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix")).toString();
//    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//	String producerRoot = portalProperties.getString("producerRoot");
//	String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//	return new StringReader(myXmlString);
//}
//

public class BillOfExchanges extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(BillOfExchanges.class);
	protected DocumentHandler getBlankDocumentHandler() {
		return new DocumentHandler("<BillOfExchanges></BillOfExchanges>", false);
	}

	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix,
			String contextPath) throws RemoteException, AmsException
	{
		return populateXmlDoc(outputDoc, key, locale, bankPrefix);
	}

	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  The following is an example of what our output will look like:
	 * <P>
	 *<Document>
	 * <BillOfExchanges>
	 *   <BillOfExchange(1)>
	 *      <SectionA>
	 *        <Amount>USD 108,650.06</Amount>
	 *        <Date>27 Feburary 2001</Date>
	 *      </SectionA>
	 *      <SectionB>Fixed Maturity Date - 27 May 2001</SectionB>
	 *      <SectionC>ANZ Banking Group (New Zealand) Limited</SectionC>
	 *      <SectionD>
	 *        <DraftNumber>123456789</DraftNumber>
	 *        <!-- If we're no dealing with a Multi-tenor then the draft number won't appear
	 *             and instead an 'empty' place holder tage will be sent out in it's place.
	 *          -->
	 *        <Empty></Empty>
	 *        <CollectionReference>123456789012345678901234</CollectionReference>
	 *      </SectionD>
	 *      <SectionE>United States Dollars One hundred and eight thousand six hundred and fifty and 6/100</SectionE>
	 *      <SectionF>
	 *        <FirstAddressLine>Rothmans Co.</FirstAddressLine>
	 *        <AddressLine>100 Sauva St.</AddressLine>
	 *        <AddressLine>French Quarter</AddressLine>
	 *        <AddressLine>Montreal QU</AddressLine>
	 *        <AddressLine>Canada H2Y 1M2</AddressLine>
	 *      </SectionF>
	 *      <SectionG>Sinochem Jiangsu Wiaxi</SectionG>
	 *   </BillOfExchange(1)>
	 * </BillOfExchanges>
	 *</Document>
	 * </P>
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 *
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix)
			  throws RemoteException, AmsException
	{
		Long transactionOid         = new Long(key);
		String transactionType      = null;
		String codeAndCurrency      = null;
		String collectionDate       = null;
		String currencyCode         = null;
		String currencyAmount       = null;
		String displayAmount        = null;
		String tenorTest            = null;
		boolean tenorFlag           = false;
		int cntr                    = 1;

		//Business Objects needed for outgoing data
		Transaction trans           = null;     //Based on passed in oid
		Transaction transOriginal   = null;     //If we're in amendment mode, then we need to original data
		Instrument instrument       = null;     //Helps establish other business objects
		OperationalBankOrganization opBankOrg = null;
		Terms terms                 = null;     //Current ACTIVE terms
		Terms termsOriginal         = null;     //Original Terms account prior to amendment.
		TermsParty draweeTermsParty = null;     //Drawee  (as defined in the Terms oid fields)
		TermsParty drawerTermsParty = null;     //Drawer
		ClientBank clientBank = null; // VSHAH Vasavi Rework

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		JPylonProperties jPylonProperties = JPylonProperties.getInstance();
		String serverLocation             = jPylonProperties.getString("serverLocation");

		//Get access to the Reference Data manager  -- this is a singleton
		ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();

	  //Create Transaction
		trans = (Transaction)EJBObjectFactory.createClientEJB( serverLocation,
									 "Transaction",transactionOid.longValue(), csdb);

		//A transaction type can be: "ISS" / "AMD" / "TRC" -- this variable will help
		//reduce the number of times we need to fetch this information, by keeping it in resident memory.
		transactionType = trans.getAttribute("transaction_type_code");

	  //set up a Instrument object
		instrument = (Instrument)EJBObjectFactory.createClientEJB( serverLocation,
													"Instrument", trans.getAttributeLong("instrument_oid"), csdb);

	  //set up a Operational Bank Org object
		opBankOrg = (OperationalBankOrganization)EJBObjectFactory.createClientEJB( serverLocation,
					"OperationalBankOrganization", instrument.getAttributeLong("op_bank_org_oid"), csdb);

		clientBank = (ClientBank)EJBObjectFactory.createClientEJB( serverLocation, "ClientBank", instrument.getAttributeLong("client_bank_oid"), csdb); // VSHAH Vasavi Rework

	  //set up a Terms component off of the transaction
		terms = (Terms)trans.getComponentHandle( "CustomerEnteredTerms" );
		if( !transactionType.equals( TransactionType.ISSUE ) )
		{  transOriginal = (Transaction)EJBObjectFactory.createClientEJB(serverLocation,
						   "Transaction", instrument.getAttributeLong("original_transaction_oid"), csdb);

		   if (StringFunction.isBlank(transOriginal.getAttribute("c_CustomerEnteredTerms"))) {
		   		termsOriginal = (Terms) transOriginal.getComponentHandle( "BankReleasedTerms" );
		   		draweeTermsParty = termsOriginal.getTermsPartyByPartyType(TermsPartyType.DRAWEE_BUYER); //Drawee
		   		drawerTermsParty = termsOriginal.getTermsPartyByPartyType(TermsPartyType.DRAWER_SELLER);  //Drawer
		   }else {
		   		termsOriginal = (Terms) transOriginal.getComponentHandle( "CustomerEnteredTerms" );
		   		draweeTermsParty = (TermsParty)termsOriginal.getComponentHandle("FirstTermsParty");  //Drawee
		   		drawerTermsParty = (TermsParty)termsOriginal.getComponentHandle("SecondTermsParty"); //Drawer
		   }
		}else{
		//*** Terms is a 1 to 1 component ***

	  //set up a Terms Party Object(s)
		   draweeTermsParty = (TermsParty)terms.getComponentHandle("FirstTermsParty");      //Drawee
		   drawerTermsParty = (TermsParty)terms.getComponentHandle("SecondTermsParty");     //Drawer
		}
		//TermsParty(s) are a 1 to 1 components


	 //This will force execution atleast once, If the stored data is a muti-tenor,
	 //Then the loop continues creating additional Bill of Exchanges (xml) until there's
	 //no more tenors...Each tenor needs to get it's own XML page.  If it's not a tenor,
	 //then the loop only needs to be executed once anyway: hence do / while.

	 try{
		do{

		//This sets up variables based on Transaction Type: To be used later in Sections A & E
			if ( transactionType.equals( TransactionType.AMEND ) )                      //Amendment
			{
				collectionDate = formatDate( terms, "collection_date", locale);

                                // Get the currency from the original terms because the currency cannot
                                // be changed on an amendment and therefore is not stored on the new terms
				if (termsOriginal!=null) currencyCode   = termsOriginal.getAttribute("amount_currency_code");

				if( isNotEmpty(terms.getAttribute("tenor_" + cntr)) ){
					currencyAmount = terms.getAttribute("tenor_" + cntr + "_amount");
				} else {
					currencyAmount = terms.getAttribute("amount");
				}
				displayAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount, currencyCode, locale);
				codeAndCurrency = appendAttributes ( currencyCode, displayAmount, " " );

			}else                                                                            //Issue
			{
				collectionDate = formatDate( terms, "collection_date", locale);

				currencyCode   = terms.getAttribute("amount_currency_code");

				if( isNotEmpty(terms.getAttribute("tenor_" + cntr)) ){
					currencyAmount = terms.getAttribute("tenor_" + cntr + "_amount");
				} else {
					currencyAmount = terms.getAttribute("amount");
				}
				displayAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount, currencyCode, locale);
				codeAndCurrency = appendAttributes ( currencyCode, displayAmount, " " );
			}

 //Section 'A' -- Amount and Date
			sectionA( outputDoc, cntr, codeAndCurrency, collectionDate );

 //Section 'B' -- Tenor info
			tenorFlag = sectionB( outputDoc, cntr, tenorFlag, terms, transactionType, refDataMgr, locale );

 //Section 'C' -- Bank Organization Name

			addNonNullDocData( clientBank.getAttribute("name"), outputDoc,
							   "/BillOfExchange(" + cntr + ")/SectionC/");
 //Section 'D' -- Reference Number
		//If there's muti-tenors display the draft number other wise send out a place holder - the empty string
			if( tenorFlag )
				addNonNullDocData( terms.getAttribute("tenor_" + cntr + "_draft_number"),
								   outputDoc, "/BillOfExchange(" + cntr + ")/SectionD/DraftNumber/");
			else
				outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionD/Empty/", "");

			outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionD/CollectionReference/",
									instrument.getAttribute("complete_instrument_id") );

 //Section 'E' -- Amount and Date
			sectionE( outputDoc, cntr, currencyCode, refDataMgr, codeAndCurrency, currencyAmount, locale );

 //Section 'F' -- Drawee info
			sectionF( outputDoc, cntr, draweeTermsParty, refDataMgr, locale );

 //Section 'G' -- Drawer Name
			addNonNullDocData( drawerTermsParty.getAttribute("name"), outputDoc,
							   "/BillOfExchange(" + cntr + ")/SectionG/");

			addNonNullDocData( clientBank.getAttribute("name"), outputDoc,
					   "/BillOfExchange(" + cntr + ")/SectionH/");
			cntr += 1;
		  //There can only be a max of 6 tenors, therefore the loop cannot check for a 7th tenor-
		  //otherwise an exception will be thrown.
			if( (tenorFlag == false) || ( cntr == 7 ) )
				tenorTest = null;
			else
				tenorTest = terms.getAttribute("tenor_" + cntr );

		}while( isNotEmpty( tenorTest ) );

	  }finally{  //remove any beans for clean up if they have data
		beanRemove( trans, "Transaction");
		beanRemove( transOriginal, "TransactionOriginal");
		beanRemove( instrument, "Instrument");
		beanRemove( opBankOrg, "Operational Bank Org");
		beanRemove( termsOriginal, "TermsOriginal");
	  }


					  // LOG.debug( outputDoc.toString() );
	  return outputDoc;
	}


	/**
	 * This is the section A portion of the outgoing XML document.
	 *
	 * @param OutputDoc         - This is the resulting Xml Document.
	 * @param Counter           - This is the current Bill of Exchange counter.
	 * @param Code and Currency - This is the appended string for the code and Currency.
	 * @param Collection Date   - this is the collection date that was set in the DB.
	 *
	 */
	private void sectionA ( DocumentHandler outputDoc, int cntr, String codeAndCurrency,
																		String collectionDate)
			throws RemoteException, AmsException
	{
		//send out the CurrencyCode and Amounts to Doc
		addNonNullDocData( codeAndCurrency, outputDoc, "/BillOfExchange(" + cntr + ")/SectionA/Amount/");

		//if a collection date is defined then we know we're in amend/tracer mode...
		addNonNullDocData( collectionDate, outputDoc, "/BillOfExchange(" + cntr + ")/SectionA/Date/");
	}


	/**
	 * This is the section B portion of the outgoing XML document.
	 *
	 * @param OutputDoc         - This is the resulting Xml Document. <BR>
	 * @param Counter           - This is the current Bill of Exchange counter. <BR>
	 * @param tenorFlag         - This is the flag to indicate whethere we're dealing
	 *                            multi-tenors or not. <BR>
	 * @param Terms Bus. Object - This is the business object we'll be getting our data from. <BR>
	 * @param Transaction Type  - This is the current transaction type. <BR>
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description. <BR>
	 * @param Locale            - This is the users current locale. <BR>
	 */
	private boolean sectionB ( DocumentHandler outputDoc, int cntr, boolean tenorFlag,
								   TradePortalBusinessObject terms,     String transactionType,
								   ReferenceDataManager refDataMgr,     String locale)
			throws RemoteException, AmsException
	{
		String dateString = "";
		String tenorType = "";
		if( transactionType.equals( TransactionType.ISSUE ) )
		{
			String desc = "";
			String paymentTermsType = terms.getAttribute("pmt_terms_type");

			if( paymentTermsType.equals( TradePortalConstants.PMT_SIGHT ) )                   //Sight
			{
			  desc = refDataMgr.getDescr( TradePortalConstants.PMT_TERMS_TYPE,
										  TradePortalConstants.PMT_SIGHT, locale);

			  outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionB/text", desc);


			}else if( paymentTermsType.equals( TradePortalConstants.PMT_DAYS_AFTER ) )        //Days After Event
			{
			  //Get the ref data description for the stored code
			  String daysAfterText = refDataMgr.getDescr( TradePortalConstants.PMT_TERMS_TYPE,
											   TradePortalConstants.PMT_DAYS_AFTER, locale);

			  tenorType = refDataMgr.getDescr( TradePortalConstants.PMT_TERMS_DAYS_AFTER,
										  terms.getAttribute("pmt_terms_days_after_type"),
										  locale);

			  outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionB/text",
									  terms.getAttribute("pmt_terms_num_days_after")
									  + " " + daysAfterText + " " + tenorType );


			}else if( paymentTermsType.equals( TradePortalConstants.PMT_FIXED_MATURITY ) )    //At Fixed Maturity
			{
			  dateString = formatDate( terms, "pmt_terms_fixed_maturity_date", locale);
			  if( !dateString.equals("") )
			  {
				  tenorType = refDataMgr.getDescr( TradePortalConstants.PMT_TERMS_TYPE,
											   TradePortalConstants.PMT_FIXED_MATURITY, locale);
				  outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionB/text",
										  tenorType + " " + dateString );

                                  // Set this flag to indicate that the text 'At' should be placed in
                                  // front of the payment terms description
                                  // For Fixed Maturity, the description is "At Fixed Maturity", so a flag
                                  // must be set so to false so that it does not read "At At Fixed Maturity"
				  outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionB/showAt",
										  "false" );
			  }


			}else if( paymentTermsType.equals( TradePortalConstants.PMT_CASH_AGAINST ) )      //Cash Against Documents
			{
			   tenorType = refDataMgr.getDescr( TradePortalConstants.PMT_TERMS_TYPE,
												TradePortalConstants.PMT_CASH_AGAINST, locale);
			   outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionB/text", tenorType );

			}else                                                                    //Multi-Tenor (other)
			{
			   addNonNullDocData( terms.getAttribute("tenor_" + cntr), outputDoc,
								  "/BillOfExchange(" + cntr + ")/SectionB/text");
			   if( isNotEmpty(terms.getAttribute("tenor_" + cntr)) )
					tenorFlag = true;

			}

		//End ofTransactionType == 'Issue'

		}else if( transactionType.equals( TransactionType.AMEND ) )
		{
		  //If the transactionType is Amendment, then just buid whatever List of tenors...

			   addNonNullDocData( terms.getAttribute("tenor_" + cntr), outputDoc,
								  "/BillOfExchange(" + cntr + ")/SectionB/text");
			   if( isNotEmpty(terms.getAttribute("tenor_" + cntr)) )
					tenorFlag = true;

		 }//End of paymentTermsType
	   return tenorFlag;
	}



	/**
	 * This is the section E portion of the outgoing XML document.
	 *
	 * @param OutputDoc         - This is the resulting Xml Document. <BR>
	 * @param Counter           - This is the current Bill of Exchange counter. <BR>
	 * @param CurrencyCode      - This is the literal code of the currency for the users locale. <BR>
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description. <BR>
	 * @param Code and Currency - This is the appended string for the code and Currency. <BR>
	 * @param Currency Amount   - This is the current Amount to be displayed. <BR>
	 * @param Locale            - This is the users current locale. <BR>
	 */
	private void sectionE ( DocumentHandler outputDoc, int cntr, String currencyCode,
								   ReferenceDataManager refDataMgr,     String codeAndCurrency,
																		String currencyAmount,
																		String locale)
			throws RemoteException, AmsException
	{
		//Textual representation of the Currency Amount
		if( isNotEmpty( codeAndCurrency ) )
		{
			String decimalPoint = refDataMgr.getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currencyCode, locale);

		   //Get the ref data description for the stored code
		   currencyCode   = refDataMgr.getDescr( TradePortalConstants.CURRENCY_CODE, currencyCode, locale );
			// Convert the amount to it's textual representation....(all lower case)
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount, locale, decimalPoint);
		   outputDoc.setAttribute( "/BillOfExchange(" + cntr + ")/SectionE/",
									currencyCode + " " + currencyAmount);
		}
	}


	/**
	 * This is the section F portion of the outgoing XML document.
	 *
	 * @param OutputDoc         - This is the resulting Xml Document. <BR>
	 * @param Counter           - This is the current Bill of Exchange counter. <BR>
	 * @param Drawee Terms Party Bus. Object - This is the business object we'll
	 *                                          be getting our data from. <BR>
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description. <BR>
	 * @param Locale            - This is the users current locale. <BR>
	 */
	private void sectionF ( DocumentHandler outputDoc, int cntr,
								   TradePortalBusinessObject draweeTermsParty, ReferenceDataManager refDataMgr,  String locale)
			throws RemoteException, AmsException
	{
		Vector drawee     = new Vector();
		String cityState  = "";
		String countryZip = "";

		//Get the Drawee info and stuff it into a vector
		drawee = addNonNullElement( draweeTermsParty.getAttribute("name"), drawee);
		drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_1"), drawee);
		drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_2"), drawee);
		drawee = addNonNullElement( draweeTermsParty.getAttribute("address_line_3"), drawee); //VSHAH Vasavi Rework - Comment Out

		cityState =  appendAttributes(draweeTermsParty.getAttribute("address_city"),
									  draweeTermsParty.getAttribute("address_state_province"), ", " );
		if( isNotEmpty( cityState ) )
			drawee.addElement( cityState );


		String countryDescr;
		if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, draweeTermsParty.getAttribute("address_country"), locale))
		 {
			countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, draweeTermsParty.getAttribute("address_country"), locale);
		 }
		else
		 {
			countryDescr = "";
		 }

		countryZip = appendAttributes(countryDescr,
									  draweeTermsParty.getAttribute("address_postal_code"), ", " );
		if( isNotEmpty( countryZip ) )
			drawee.addElement( countryZip );

		//Loop thru the drawee info and insert it into the outgoing Xml doc.
		int max = drawee.size();
		for(int x = 0; x < max; x++)
		{
			if( x == 0 )
				outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionF/FirstAddressLine/",
										(String)drawee.elementAt(x) );
			else
				outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionF/AddressLine(" + x + ")/",
										(String)drawee.elementAt(x) );
		}
	}


	/**
	 * This is the section F portion of the outgoing XML document.
	 *
	 * @param OutputDoc         - This is the resulting Xml Document. <BR>
	 * @param Counter           - This is the current Bill of Exchange counter. <BR>
	 * @param Drawee Terms Party Bus. Object - This is the business object we'll
	 *                                          be getting our data from. <BR>
	 * @param Reference DataMgr - The RefData Mgr will help us convert the refdata code to it's description. <BR>
	 * @param Locale            - This is the users current locale. <BR>
	 */
	private void sectionFOpBank ( DocumentHandler outputDoc, int cntr,
								   TradePortalBusinessObject opBankOrg, ReferenceDataManager refDataMgr,  String locale)
			throws RemoteException, AmsException
	{
		Vector drawee     = new Vector();
		String cityState  = "";
		String countryZip = "";

		//Get the Drawee info and stuff it into a vector
		drawee = addNonNullElement( opBankOrg.getAttribute("name"), drawee);
		drawee = addNonNullElement( opBankOrg.getAttribute("address_line_1"), drawee);
		drawee = addNonNullElement( opBankOrg.getAttribute("address_line_2"), drawee);

		cityState =  appendAttributes(opBankOrg.getAttribute("address_city"),
									  opBankOrg.getAttribute("address_state_province"), ", " );
		if( isNotEmpty( cityState ) )
			drawee.addElement( cityState );


		String countryDescr;
		if(refDataMgr.checkCode(TradePortalConstants.COUNTRY, opBankOrg.getAttribute("address_country"), locale))
		 {
			countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY, opBankOrg.getAttribute("address_country"), locale);
		 }
		else
		 {
			countryDescr = "";
		 }

		countryZip = appendAttributes(countryDescr,
									  opBankOrg.getAttribute("address_postal_code"), ", " );
		if( isNotEmpty( countryZip ) )
			drawee.addElement( countryZip );

		//Loop thru the drawee info and insert it into the outgoing Xml doc.
		int max = drawee.size();
		for(int x = 0; x < max; x++)
		{
			if( x == 0 )
				outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionF/FirstAddressLine/",
										(String)drawee.elementAt(x) );
			else
				outputDoc.setAttribute("/BillOfExchange(" + cntr + ")/SectionF/AddressLine(" + x + ")/",
										(String)drawee.elementAt(x) );
		}
	}




	/**
	 * This method is meant simply to test an object to see if it needs to be removed,
	 * If so, then we try to remove it.  If we get an exception, It would be probably
	 * Because we removed a parent of a component when we tried to remove the component
	 * object.
	 *
	 * @param BusinessObject - The object we're trying to remove.
	 * @param Object Type    - This is the type of object being sent out in the error message.
	 */
	private void beanRemove( TradePortalBusinessObject busObj, String objectType )
	{
		if( busObj != null )
		{   try{
					busObj.remove();
			}catch(Exception e)
			{   LOG.info("Failed in removal of " + objectType + " Bean: Bill Of Exchanges Class");
				LOG.info("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

}