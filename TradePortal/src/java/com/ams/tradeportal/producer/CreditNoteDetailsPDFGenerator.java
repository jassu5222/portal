package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various
 * business objects then pass this data through an XSL stylesheet format the
 * data for display then pass the result on to a cocoon wrapper to display the
 * result in a PDF format. The method: populateXmlDoc does most of the work,
 * however get stream puts it all together and returns the result (a Reader
 * object). The Corresponding XSL file is: CreditNoteDetailsPDFGenerator.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
public class CreditNoteDetailsPDFGenerator extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(CreditNoteDetailsPDFGenerator.class);

	private InvoicesSummaryData invoicesSummaryData = null;
	private CreditNotes creditNotes = null;
	private String useCurrency = null;
	private ResourceManager resMgr = null;
	private DocumentHandler invoiceSummaryGoodsResult = null;
	private DocumentHandler invoiceListDetailResult = null;
	private ArMatchingRule invoiceTradingPartnerDetailResult = null;
	SimpleDateFormat isoDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.S");
	private DocumentHandler creditNoteAppliedInvoicesResult = null;
	private Vector creditNoteAppliedInvoicesList = null;
	private int numberOfAppliedInvoices = 0;

	/**
	 * This method is the method that gathers all the data and builds the Xml
	 * document to be returned to the caller for display. The Xsl template is
	 * expecting the Xml doc to send data out in a specific format...ie in
	 * sections. If you scroll down the left side you can easily jump around the
	 * xml doc looking for specific section letters.
	 * 
	 * @param Output
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key
	 *            - This is the Transaction oid the will help us get all our
	 *            data. With out this oid, no data can be retrieved.
	 * @param Locale
	 *            - This is the current users locale, it will help in
	 *            establishing the client Server data bridge as well as building
	 *            various B.O.s
	 * @param BankPrefix
	 *            - This may be removed in the future.
	 * @param ContextPath
	 *            - the context path under which this request was made
	 * @return OutputDoc - Send out the result of building our Document.
	 */

	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix, String contextPath)
			throws RemoteException, AmsException {

		Long invoiceOid = new Long(key);

		// Setup the logo file location, context path and locale instance
		// variables
		if ("blueyellow".equals(bankPrefix)) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		}
		if ("anztransac".equals(bankPrefix)) {
			bankPrefix = "anz";
		}
		bankLogo = bankPrefix + "/images";

		formContextPath = contextPath;
		formLocale = locale;

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");

		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();

		creditNotes = (CreditNotes) EJBObjectFactory.createClientEJB(
				serverLocation, "CreditNotes", invoiceOid.longValue(), csdb);

		useCurrency = creditNotes.getAttribute("currency");
		if (StringFunction.isBlank(useCurrency)) {
			useCurrency = "2";
		}
		resMgr = new ResourceManager(csdb);
		String invoiceID = creditNotes.getAttribute("invoice_id");

		String invClassification = creditNotes
				.getAttribute("invoice_classification");
		String buyerOrSeller = null;
		if (StringFunction.isNotBlank(invClassification)
				&& TradePortalConstants.INVOICE_TYPE_REC_MGMT
						.equals(invClassification)) {
			buyerOrSeller = creditNotes.getAttribute("buyer_name");
		} else if (StringFunction.isNotBlank(invClassification)
				&& TradePortalConstants.INVOICE_TYPE_PAY_MGMT
						.equals(invClassification)) {
			buyerOrSeller = creditNotes.getAttribute("seller_name");
		}
		
		StringBuilder invoiceTradingPartnerDetailSQL = new StringBuilder();
		invoiceTradingPartnerDetailSQL
				.append("select address_line_1, address_line_2,")
				.append(" address_city ").append(" from ar_matching_rule")
				.append(" where buyer_name = " + "'"
						+ buyerOrSeller + "'");

		 invoiceTradingPartnerDetailResult = creditNotes.getMatchingRule();
		//jgadela R92 - SQL INJECTION FIX
		String invoiceSummaryGoodsSQL = "select * from credit_notes_goods  where p_credit_note_oid = ? ";		
		invoiceSummaryGoodsResult = DatabaseQueryBean.getXmlResultSet(invoiceSummaryGoodsSQL, false, new Object[]{invoiceOid});

		//jgadela R92 - SQL INJECTION FIX
		String creditNoteInvoicesSQL = "select i.invoice_id, r.credit_note_applied_amount from invoices_summary_data i, invoices_credit_notes_relation r"
	               + " where r.a_upload_invoice_oid = i.upload_invoice_oid and r.a_upload_credit_note_oid = ? ";		
		creditNoteAppliedInvoicesResult = DatabaseQueryBean.getXmlResultSet(creditNoteInvoicesSQL, false, new Object[]{invoiceOid});
		if(creditNoteAppliedInvoicesResult!=null){
		creditNoteAppliedInvoicesList = creditNoteAppliedInvoicesResult.getFragments("/ResultSetRecord");
		numberOfAppliedInvoices = creditNoteAppliedInvoicesList.size();
		}
		
		//

		try {
			// Section 'A' -- Title Bank Logo and document reference #
			sectionA(outputDoc, invoiceID);
			// Section 'B' -- Invoice Details
			sectionB1(outputDoc); 
			sectionB2(outputDoc);
			// Section 'C' -- Buyer & Trading Partner Details
			 sectionC(outputDoc, invoiceTradingPartnerDetailResult);
			// Section 'D' -- Invoice Summary details
			sectionD(outputDoc, invoiceSummaryGoodsResult);
			// Section 'E' -- Applied Invoices Details Details
			sectionE(outputDoc, creditNoteAppliedInvoicesResult);


		} finally { // remove any beans for clean up if they have data
			beanRemove(creditNotes, "CreditNotes");
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled
	 * 'SectionA'. The following is an example of what the resulting Xml should
	 * look like: <SectionA><!-- Header Info --> <Title> <Line1>Invoice
	 * Details</Line1> <Line2></Line2> </Title>
	 * <Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 * </SectionA>
	 * 
	 * DocumentHandler.setAttribute does not allow us to add same elements
	 * multiple times as it just updates existing elements. To overcome above
	 * limitation elements are added to the output document using the
	 * DocumentHandler.addComponent method. This method always adds elements to
	 * the top of the document. So to maintain the order of the elements we need
	 * to add the elements in reverse order.
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionA(DocumentHandler outputDoc, String invoiceId)
			throws RemoteException, AmsException {
		// Customizations Here
		outputDoc.setAttribute("/SectionA/Title/Line1", "Credit Note Details");
		// Call Parent Method
	}

	/**
	 * SectionB corresponds to the portion of the XML document labeled
	 * 'SectionB'. The following is an example of what the resulting Xml should
	 * look like: <SectionB><!-- Invoice Details Info --> <InvoiceDetails>
	 * <field> <!-- each field --> <name> <!-- field name --> </name> <value>
	 * <!-- field value --> </value> </field> </InvoiceDetails> </SectionB>
	 * DocumentHandler.setAttribute does not allow us to add same elements
	 * multiple times as it just updates existing elements. To overcome above
	 * limitation elements are added to the output document using the
	 * DocumentHandler.addComponent method. This method always adds elements to
	 * the top of the document. So to maintain the order of the elements we need
	 * to add the elements in reverse order.
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionB1(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// Customize Code
		DocumentHandler tempDoc = null;
		String date = "";
		
		//credit note status
		tempDoc = new DocumentHandler();
		String creditNoteStatus = refDataMgr.getDescr(TradePortalConstants.CREDIT_NOTE_STATUS, creditNotes.getAttribute("credit_note_status"), formLocale);
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.CreditNoteStatus",
				TradePortalConstants.TEXT_BUNDLE));		
		tempDoc.setAttribute("/field/value", 
				creditNoteStatus);	
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);		
		
		//Remaining Amount
		String remainingAmount = null;
		if(StringFunction.isNotBlank(creditNotes.getAttribute("credit_note_applied_amount"))){
			remainingAmount = String.valueOf(Integer.parseInt(creditNotes.getAttribute("amount")) 
			   		- Integer.parseInt(creditNotes.getAttribute("credit_note_applied_amount")));
		}else{
			remainingAmount = creditNotes.getAttribute("amount");
		}
		tempDoc = new DocumentHandler();		
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.RemainingAmount",
				TradePortalConstants.TEXT_BUNDLE));		
		tempDoc.setAttribute("/field/value", TPCurrencyUtility
				.getDisplayAmountWithCurrency(remainingAmount,
						useCurrency, formLocale));	
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);		
		
		//Applied Amount
		tempDoc = new DocumentHandler();		
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.AppliedAmount",
				TradePortalConstants.TEXT_BUNDLE));	
		if(StringFunction.isNotBlank(creditNotes.getAttribute("credit_note_applied_amount"))){
			if(creditNotes.getAttribute("credit_note_applied_amount").startsWith("-")){
				tempDoc.setAttribute("/field/value", TPCurrencyUtility
						.getDisplayAmountWithCurrency(creditNotes
								.getAttribute("credit_note_applied_amount").substring(1),
								useCurrency, formLocale));
			}else{
				tempDoc.setAttribute("/field/value", TPCurrencyUtility
						.getDisplayAmountWithCurrency(creditNotes
								.getAttribute("credit_note_applied_amount"),
								useCurrency, formLocale));
			}
		}else{
			tempDoc.setAttribute("/field/value", "0.00"); 
		}
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);		
		
		//Credit Note Amount
		tempDoc = new DocumentHandler();		
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.CreditNoteAmount",
				TradePortalConstants.TEXT_BUNDLE));		
		tempDoc.setAttribute("/field/value", TPCurrencyUtility
				.getDisplayAmountWithCurrency(creditNotes
						.getAttribute("amount"),
						useCurrency, formLocale));	
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
		
		//Issue Date
		tempDoc = new DocumentHandler();		
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.IssueDate",
				TradePortalConstants.TEXT_BUNDLE));	
		date = creditNotes.getAttribute("issue_date");
		if (StringFunction.isNotBlank(date)) {
			date = isoDateFormat.format(creditNotes
					.getAttributeDate("issue_date"));
			date = TPDateTimeUtility.formatDate(date,
					TPDateTimeUtility.SHORT, formLocale);
		}
		tempDoc.setAttribute("/field/value", date);		
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);		
		
		//sub-header
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"CreditNoteDetails.CreditNoteID",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/section-header/value",
				creditNotes.getAttribute("invoice_id"));
		outputDoc.addComponent("/SectionB1/InvoiceDetails/", tempDoc);
	}
	protected void sectionB2(DocumentHandler outputDoc) throws RemoteException,
	AmsException {
// Customize Code
DocumentHandler tempDoc = null;
String date = "";
//Linked To
tempDoc = new DocumentHandler();		
tempDoc.setAttribute("/field/name", resMgr.getText(
		"CreditNoteDetails.LinkedToInstType",
		TradePortalConstants.TEXT_BUNDLE));	
tempDoc.setAttribute("/field/value", refDataMgr.getDescr(
		TradePortalConstants.UPLOAD_INV_LINKED_INSTR_TY,
		creditNotes.getAttribute("linked_to_instrument_type"),
		formLocale));		
outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);		

//credit note utilised status
tempDoc = new DocumentHandler();
String utilizedStatus = refDataMgr.getDescr(TradePortalConstants.CREDIT_NOTE_UTILISED_STATUS, creditNotes.getAttribute("credit_note_utilised_status"), formLocale);
tempDoc.setAttribute("/field/name", resMgr.getText(
		"CreditNoteDetails.CreditNoteUtilizedStatus",
		TradePortalConstants.TEXT_BUNDLE));	
tempDoc.setAttribute("/field/value", 
		utilizedStatus);		
outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);		

//end-to-end id
tempDoc = new DocumentHandler();		
tempDoc.setAttribute("/field/name", resMgr.getText(
		"CreditNoteDetails.EndToEndID",
		TradePortalConstants.TEXT_BUNDLE));	
tempDoc.setAttribute("/field/value", 
		creditNotes.getAttribute("end_to_end_id"));			
outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

//credit note applied status
tempDoc = new DocumentHandler();	
String appliedStatus = refDataMgr.getDescr(TradePortalConstants.CREDIT_NOTE_APPLIED_STATUS, creditNotes.getAttribute("credit_note_applied_status"), formLocale);
tempDoc.setAttribute("/field/name", resMgr.getText(
		"CreditNoteDetails.CreditNoteAppliedStatus",
		TradePortalConstants.TEXT_BUNDLE));		
tempDoc.setAttribute("/field/value", 
		appliedStatus);		
outputDoc.addComponent("/SectionB2/InvoiceDetails/", tempDoc);

}
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled
	 * 'SectionC'. The following is an example of what the resulting Xml should
	 * look like: <BuyerDetails> <field> <!-- each field --> <name> <!-- field
	 * name --> </name> <value> <!-- field value --> </value> </field>
	 * </BuyerDetails>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionC(DocumentHandler outputDoc, ArMatchingRule rule)
			throws RemoteException, AmsException {

		DocumentHandler tempDoc = null;
		// Override Code goes here
		String addressLine1 = null;
		String addressLine2 = null;
		String addressCity = null;
		String addressCountry = "";
		// Buyer details added
		String buyerId = null;
		String buyerAddLine1 = null;
		String buyerAddLine2 = null;
		String buyerAddCity = null;
		String buyerAddCtry = null;
		String buyerName = null;
		if (rule != null) {
			addressLine1 = rule.getAttribute("address_line_1");
			addressLine2 = rule.getAttribute("address_line_2");
			addressCity = rule.getAttribute("address_city");
			addressCountry = rule.getAttribute("address_country");
			buyerName = rule.getAttribute("buyer_name");
		}
		String corpOrgOid = creditNotes.getAttribute("corp_org_oid");

		//jgadela R92 - SQL INJECTION FIX
		String buyerDetailsSQL = " select name, address_line_1, address_line_2, address_city, address_country from "
								+" corporate_org where organization_oid = ? ";
		LOG.info("buyerDetailsSQL.toString():"
				+ buyerDetailsSQL.toString());

		DocumentHandler buyerDetails = DatabaseQueryBean.getXmlResultSet(buyerDetailsSQL, false, new Object[]{corpOrgOid});

		if (buyerDetails != null) {

			buyerId = buyerDetails.getAttribute("/ResultSetRecord(0)/NAME");
			buyerAddLine1 = buyerDetails
					.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_1");
			buyerAddLine2 = buyerDetails
					.getAttribute("/ResultSetRecord(0)/ADDRESS_LINE_2");
			buyerAddCity = buyerDetails
					.getAttribute("/ResultSetRecord(0)/ADDRESS_CITY");
			buyerAddCtry = buyerDetails
					.getAttribute("/ResultSetRecord(0)/ADDRESS_COUNTRY");
		}
		LOG.info("buyerId:" + buyerId + "\t buyerAddLine1:"
				+ buyerAddLine1 + "\t buyerAddLine2:" + buyerAddLine2
				+ "\t buyerAddCity:" + buyerAddCity + "\t buyerAddCtry:"
				+ buyerAddCtry);
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressCity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressCity + " " + addressCountry);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressLine2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressLine2);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.AddressLine1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", addressLine1);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.NameIdentity",
				TradePortalConstants.TEXT_BUNDLE));

		tempDoc.setAttribute("/field/value", buyerName);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressCity",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddCity);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getTextIncludingEmptyString(
				"UploadInvoiceDetail.AddressLine2",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddLine2);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.AddressLine1",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerAddLine1);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"UploadInvoiceDetail.Identifier",
				TradePortalConstants.TEXT_BUNDLE));
		tempDoc.setAttribute("/field/value", buyerId);
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.TradingPartnerInfo",
				TradePortalConstants.TEXT_BUNDLE));
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);

		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"UploadInvoiceDetail.BuyerInfo",
				TradePortalConstants.TEXT_BUNDLE));
		outputDoc.addComponent("/SectionC/BuyerDetails", tempDoc);
	}

	/**
	 * SectionD corresponds to the portion of the XML document labeled
	 * 'SectionD'. The following is an example of what the resulting Xml should
	 * look like: <SectionD><!-- Applicant and Beneficiary-->
	 * <InvSummGoodsDetails> <field> <!-- each field --> <name> <!-- field name
	 * --> </name> <value> <!-- field value --> </value> </field>
	 * <LineItemDetails> <field> <!-- each field --> <name> <!-- field name -->
	 * </name> <value> <!-- field value --> </value> </field> </LineItemDetails>
	 * </InvSummGoodsDetails> </SectionD> DocumentHandler.setAttribute does not
	 * allow us to add same elements multiple times as it just updates existing
	 * elements. To overcome above limitation elements are added to the output
	 * document using the DocumentHandler.addComponent method. This method
	 * always adds elements to the top of the document. So to maintain the order
	 * of the elements we need to add the elements in reverse order.
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionD(DocumentHandler outputDoc,
			DocumentHandler invoiceSummaryGoodsResult) throws RemoteException,
			AmsException {
		// Overridden Code
		DocumentHandler tempDoc = null;
		DocumentHandler tempDoc1 = null;
		if (invoiceSummaryGoodsResult != null) {
			Vector summaryGoodsVector = invoiceSummaryGoodsResult
					.getFragments("/ResultSetRecord/");
			int totalSummaryGoods = summaryGoodsVector.size();
			for (int goods = totalSummaryGoods - 1; goods >= 0; goods--) {
				tempDoc1 = new DocumentHandler();
				tempDoc1.setAttribute("/InvSummGoodsDetails", "");

				for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
					if (!StringFunction.isBlank(invoiceSummaryGoodsResult
							.getAttribute("/ResultSetRecord(" + goods
									+ ")/SELLER_USERS_DEF" + i + "_LABEL"))) {
						tempDoc = new DocumentHandler();
						/*
						 * tempDoc.setAttribute( "/field/name", resMgr.getText(
						 * "UploadInvoiceDetail.SellerUserDfn",
						 * TradePortalConstants.TEXT_BUNDLE) + " " + i);
						 */
						tempDoc.setAttribute("/field/name",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/SELLER_USERS_DEF"
												+ i + "_LABEL"));
						tempDoc.setAttribute("/field/value",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/SELLER_USERS_DEF"
												+ i + "_VALUE"));
						tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
					}
				}
				for (int i = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD; i >= 1; i--) {
					if (!StringFunction.isBlank(invoiceSummaryGoodsResult
							.getAttribute("/ResultSetRecord(" + goods
									+ ")/BUYER_USERS_DEF" + i + "_LABEL"))) {
						tempDoc = new DocumentHandler();
						/*
						 * tempDoc.setAttribute( "/field/name", resMgr.getText(
						 * "UploadInvoiceDetail.BuyerUserDfn",
						 * TradePortalConstants.TEXT_BUNDLE) + " " + i);
						 */
						tempDoc.setAttribute("/field/name",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/BUYER_USERS_DEF"
												+ i + "_LABEL"));
						tempDoc.setAttribute("/field/value",
								invoiceSummaryGoodsResult
										.getAttribute("/ResultSetRecord("
												+ goods + ")/BUYER_USERS_DEF"
												+ i + "_VALUE"));
						tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);
					}
				}

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PurchaseOrderId",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/PURCHASE_ORDER_ID"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.ActualShpDateNoBreak",
						TradePortalConstants.TEXT_BUNDLE));

				String date = invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/ACTUAL_SHIP_DATE");
				if (StringFunction.isNotBlank(date)) {
					date = TPDateTimeUtility.formatDate(date,
							TPDateTimeUtility.SHORT, formLocale);
				}
				tempDoc.setAttribute("/field/value", date);
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.Carrier",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value",
						invoiceSummaryGoodsResult
								.getAttribute("/ResultSetRecord(" + goods
										+ ")/CARRIER"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.Vessel",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods + ")/VESSEL"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PortofDischarge",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/COUNTRY_OF_DISCHARGE"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.PortofLoading",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/COUNTRY_OF_LOADING"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.Incoterm",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/INCOTERM"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);


				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoiceDetail.GoodsDescr",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/GOODS_DESCRIPTION"));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/section-header/name", resMgr.getText(
						"UploadInvoiceDetail.InvoiceSummaryDetail",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc1.addComponent("/InvSummGoodsDetails", tempDoc);

				
				String summaryGoodsOid = invoiceSummaryGoodsResult
						.getAttribute("/ResultSetRecord(" + goods
								+ ")/UPLOAD_CREDIT_LINE_ITEM_OID");
				
				//jgadela R92 - SQL INJECTION FIX
				String invoiceListDetailSQL = "select * from credit_notes_line_items  where p_credit_notes_goods_oid = ? ";
				if(StringFunction.isNotBlank(summaryGoodsOid)){
					invoiceListDetailResult = DatabaseQueryBean.getXmlResultSet(invoiceListDetailSQL, false, new Object[]{summaryGoodsOid});
				}
				if (invoiceListDetailResult != null) {
					Vector lineItemVector = invoiceListDetailResult
							.getFragments("/ResultSetRecord/");
					int totalLineItem = lineItemVector.size();
					DocumentHandler tempLineDoc = null;
					DocumentHandler tempLineDoc1 = null;
					for (int lineItem = totalLineItem - 1; lineItem >= 0; lineItem--) {
						tempLineDoc1 = new DocumentHandler();
						tempLineDoc1.setAttribute("/LineItems", "");

						for (int i = TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD; i >= 1; i--) {
							if (!StringFunction.isBlank(invoiceListDetailResult
									.getAttribute("/ResultSetRecord("
											+ lineItem + ")/PROD_CHARS_UD" + i
											+ "_VAL"))) {
								tempLineDoc = new DocumentHandler();
								tempLineDoc
										.setAttribute(
												"/field/name",
												resMgr.getText(
														"UploadInvoiceDetail.ProdUserDfn",
														TradePortalConstants.TEXT_BUNDLE)
														+ " " + i + " Value");
								tempLineDoc
										.setAttribute(
												"/field/value",
												invoiceListDetailResult
														.getAttribute("/ResultSetRecord("
																+ lineItem
																+ ")/PROD_CHARS_UD"
																+ i + "_VAL"));
								tempLineDoc1.addComponent("/LineItems",
										tempLineDoc);
							}
							if (!StringFunction.isBlank(invoiceListDetailResult
									.getAttribute("/ResultSetRecord("
											+ lineItem + ")/PROD_CHARS_UD" + i
											+ "_TYPE"))) {
								tempLineDoc = new DocumentHandler();
								tempLineDoc
										.setAttribute(
												"/field/name",
												resMgr.getText(
														"UploadInvoiceDetail.ProdUserDfn",
														TradePortalConstants.TEXT_BUNDLE)
														+ " " + i + " Type");
								tempLineDoc
										.setAttribute(
												"/field/value",
												invoiceListDetailResult
														.getAttribute("/ResultSetRecord("
																+ lineItem
																+ ")/PROD_CHARS_UD"
																+ i + "_TYPE"));
								tempLineDoc1.addComponent("/LineItems",
										tempLineDoc);
							}
						}

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.UnitOfMeasureQuantity",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc
								.setAttribute(
										"/field/value",
										invoiceListDetailResult
												.getAttribute("/ResultSetRecord("
														+ lineItem
														+ ")/UNIT_OF_MEASURE_QUANTITY"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.UnitOfMeasurePrice",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult
										.getAttribute("/ResultSetRecord("
												+ lineItem
												+ ")/UNIT_OF_MEASURE_PRICE"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.InvolvedQuty",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult
										.getAttribute("/ResultSetRecord("
												+ lineItem + ")/INV_QUANTITY"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/field/name", resMgr.getText(
								"UploadInvoiceDetail.unitPrice",
								TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc.setAttribute("/field/value",
								invoiceListDetailResult
										.getAttribute("/ResultSetRecord("
												+ lineItem + ")/UNIT_PRICE"));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						//sub-header
						tempLineDoc = new DocumentHandler();
						tempLineDoc.setAttribute("/section-header/name", resMgr
								.getText("CreditNoteDetails.InvoiceGoodsDesc",
										TradePortalConstants.TEXT_BUNDLE));
						tempLineDoc1.addComponent("/LineItems", tempLineDoc);

						tempDoc1.addComponent("/InvSummGoodsDetails",
								tempLineDoc1);
					}
				}
				outputDoc.addComponent("/SectionD", tempDoc1);
			}
		}

	}


	/**
	 * SectionE 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionE(DocumentHandler outputDoc, DocumentHandler creditNoteInvoices) throws RemoteException,
			AmsException {

		DocumentHandler tempDoc = null;
		
		if(null != creditNoteInvoices){
			List<DocumentHandler> creditNoteResult = creditNoteInvoices.getFragmentsList("/ResultSetRecord/");
			
			
			for (DocumentHandler temp : creditNoteResult) {

				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoices.CreditNoteId",
						TradePortalConstants.TEXT_BUNDLE));
				tempDoc.setAttribute("/field/value", temp.getAttribute("/INVOICE_ID"));		
				outputDoc.addComponent("/SectionE/CreditNotes/InvoiceID", tempDoc);
				
				tempDoc = new DocumentHandler();
				tempDoc.setAttribute("/field/name", resMgr.getText(
						"UploadInvoices.AppliedAmt",
						TradePortalConstants.TEXT_BUNDLE));
				if(temp.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT").startsWith("-")){
					tempDoc.setAttribute("/field/value", TPCurrencyUtility
							.getDisplayAmountWithCurrency(temp
									.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT").substring(1),
									useCurrency, formLocale));
				}else{
					tempDoc.setAttribute("/field/value", TPCurrencyUtility
							.getDisplayAmountWithCurrency(temp
									.getAttribute("/CREDIT_NOTE_APPLIED_AMOUNT"),
									useCurrency, formLocale));
				}
				outputDoc.addComponent("/SectionE/CreditNotes/CreditAmount", tempDoc);
			}
		}
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.AppliedAmount",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionE/CreditNotes/Label", tempDoc);
		
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/field/name", resMgr.getText(
				"CreditNoteDetails.InvoiceID",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionE/CreditNotes/Label", tempDoc);
		tempDoc = new DocumentHandler();
		tempDoc.setAttribute("/section-header/name", resMgr.getText(
				"CreditNoteDetails.InvoicesAppliedDetails",
				TradePortalConstants.TEXT_BUNDLE));

		outputDoc.addComponent("/SectionE/CreditNoteDetails/", tempDoc);
	}

	/**
	 * This method is meant simply to test an object to see if it needs to be
	 * removed, If so, then we try to remove it. If we get an exception, It
	 * would be probably Because we removed a parent of a component when we
	 * tried to remove the component object.
	 * 
	 * @param BusinessObject
	 *            - The object we're trying to remove.
	 * @param Object
	 *            Type - This is the type of object being sent out in the error
	 *            message.
	 */
	private void beanRemove(TradePortalBusinessObject busObj, String objectType) {
		if (busObj != null) {
			try {
				busObj.remove();
			} catch (Exception e) {
				LOG.info("Failed in removal of " + objectType
						+ " Bean: Invoice Details PDF Generator");
				System.out
						.println("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

}