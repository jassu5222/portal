package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.*;
import java.text.SimpleDateFormat;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.PmtTermsTenorDtl;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class DLCIssueApplication extends DocumentProducer implements Status {
//	String headerStringPart2 = "/xsl/DLCIssueApplication.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//		return new StringReader(myXmlString);
//	}
//
//	public String getStatus() {
//		return "DLCIssueApplication Producer";
//	}
public class DLCIssueApplication extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(DLCIssueApplication.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);
			//Section 'D' -- Applicant and Beneficiary
			sectionD( outputDoc);
			//Section 'E' -- Account Party and Advising Bank
			sectionE( outputDoc);
			//Section 'F' -- Amount Details
			sectionF( outputDoc);
			//Section 'G' -- Presentation Days
		    sectionG( outputDoc);
			//Section 'H' -- Payment Terms and Bank Charges
			sectionH1( outputDoc);
			sectionH( outputDoc);
			//Section 'I' -- Required Documents
			sectionI( outputDoc);
			//Section 'J' -- Addtional Documents
			sectionJ( outputDoc);
			//Section 'K' -- Transport Document/Shipment # of Total
			sectionK( outputDoc);
			//Section 'L' -- Other Conditions
			sectionL( outputDoc);
			
			//Sections that are only available as part of Expanded Forms
			//
			if(isExpandedFormRequest(instrument.getAttribute("instrument_type_code"))){
				sectionT1( outputDoc,locale);
				checkForSettlementInstructions(outputDoc); //New design update
				sectionT2( outputDoc);
				sectionU1( outputDoc);
				sectionU2( outputDoc);
				sectionU3( outputDoc);			
			}			
			//Section 'M' -- Important Notice
			// Nar CR-938 rel9500 02/25/2016 -  changed notification for customized PDF
			if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			  outputDoc.setAttribute( "/SectionM/NoticeLine1"," The opening of the Credit shall be subject to the terms of the Continuing Letter of Credit Agreement entered into between Applicant and Bank.");
			} else {
				outputDoc.setAttribute( "/SectionM/","");
			}

			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customizations Here
		if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			outputDoc.setAttribute( "/SectionA/Title/Line1", "Commercial Letter of Credit Application");
		} else {
			outputDoc.setAttribute( "/SectionA/Title/Line1", "Application and Agreement for");
			outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Commercial Letter of Credit");
		}
		

		// Call Parent Method
		super.sectionA(outputDoc);
	}


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionB(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customize Code

		// Call Parent Method
		super.sectionB(outputDoc);
	}


	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Override Code goes here

		// Call Parent Method
		super.sectionC(outputDoc);
	}


	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionD><!-- Applicant and Beneficiary-->
	 *		<Applicant>
	 *			<Name>Applicant Name</Name>
	 *			<AddressLine1>Applicant Address Line 1</AddressLine1>
	 *			<AddressLine2>Applicant Address Line 2</AddressLine2>
	 *			<AddressLine3>Applicant Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 *		</Applicant>
	 *		<Beneficiary>
	 *			<Name>Beneficiary Name</Name>
	 *			<AddressLine1>Beneficiary Address Line 1</AddressLine1>
	 *			<AddressLine2>Beneficiary Address Line 2</AddressLine2>
	 *			<AddressLine3>Beneficiary Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Beneficiary City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Beneficiary Country + Post Code</AddressCountryPostCode>
	 *			<PhoneNumber>Phone Number</PhoneNumber>
	 *		</Beneficiary>
	 *	</SectionD>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionD(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Override Code Here

		// Call Parent Method
		super.sectionD(outputDoc);

	}

	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionE><!-- Account Party and Advising/Corresponding Bank -->
	 *		<AccountParty>
	 *			<Name>Account Party Name</Name>
	 *			<AddressLine1>Account Party Address Line 1</AddressLine1>
	 *			<AddressLine2>Account Party Address Line 2</AddressLine2>
	 *			<AddressLine3>Account Party Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Account Party City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Account Party Country + Post Code</AddressCountryPostCode>
	 *		</AccountParty>
	 *		<CorrespondentBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</CorrespondentBank>
	 *		<AdvisingBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</AdvisingBank>
	 *	</SectionE>
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionE(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Override Code

		// Call Parent Method
		super.sectionE(outputDoc);
	}


	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionF><!-- Amount Detail and Expiration -->
	 *		<AmountDetail>
	 *			<CurrencyCodeAmount>USD 12345.01</CurrencyCodeAmount>
	 *			<CurrencyValueAmountInWords>US Dollars Twelve Thousand Three Hundred Fourty Five and .01</CurrencyValueAmountInWords>
	 *			<Tolerance>
	 *				<Pos>23</Pos>
	 *				<Neg>34</Neg>
	 *			</Tolerance>
	 *		</AmountDetail>
	 *		<Expiration>
	 *			<Date>23 December 2005</Date>
	 *			<Place>A 29 character Text Field</Place>
	 *		</Expiration>
	 *	</SectionF>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionF(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Code
		super.sectionF(outputDoc);

		//
		String placeOfExpiry = terms.getAttribute("place_of_expiry");
		if("Other".equalsIgnoreCase(placeOfExpiry))	{
			placeOfExpiry = placeOfExpiry +" (See Additional Conditions)";
		}
		// Override Ancestor Code here
		addNonNullDocData( placeOfExpiry, outputDoc, "/SectionF/Expiration/Place");
	}


	/**
	 * SectionG corresponds to the portion of the XML document labeled 'SectionG'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionG><!-- Presentation Days -->
	 *		<PresentationDays>24</PresentationDays>
	 *	</SectionG>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionG(DocumentHandler outputDoc) throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionG(outputDoc);
	}

	 
	protected void sectionH1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		 //overridden in expanded
	}

	/**
	 * SectionH corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionH><!--Payment Terms and Bank Charges -->
	 *		<PaymentTerms>
	 *			<Sight></Sight>
	 *			<DaysAfter>
	 *				<Days>29</Days>
	 *				<After>Sight</After>
	 *			</DaysAfter>
	 *			<Other></Other>
	 *			<For>89</For>
	 *			<DrawnOnParty>Drawn On Party</DrawnOnParty>
	 *		</PaymentTerms>
	 *		<BankCharges>
	 *			<AllOurAccount></AllOurAccount>
	 *			<AllBankCharges></AllBankCharges>
	 *			<Other></Other>
	 *		</BankCharges>
	 *	</SectionH>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionH(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// call Parent Method
		super.sectionH(outputDoc);

		// Additional code here
		String paymentTermsPercentInvoice = terms.getAttribute("pmt_terms_percent_invoice");
//		if( isNotEmpty( paymentTermsPercentInvoice ) )
//			outputDoc.setAttribute( "/SectionH/PaymentTerms/For/", paymentTermsPercentInvoice );
		setPaymentTenorTerms(outputDoc);
        // TLE - 01/03/07 - IR#ERUH010343869 - Add Begin
		//String drawnOnParty = refDataMgr.getDescr(TradePortalConstants.DRAWN_ON_PARTY, terms.getAttribute("drawn_on_party"), formLocale );
		//if( isNotEmpty( drawnOnParty ) )
		//	outputDoc.setAttribute( "/SectionH/PaymentTerms/DrawnOnParty", drawnOnParty);

        String drawnOnParty = terms.getAttribute("drawn_on_party");
            if( isNotEmpty( drawnOnParty ) )
                  outputDoc.setAttribute( "/SectionH/PaymentTerms/DrawnOnParty", refDataMgr.getDescr(TradePortalConstants.DRAWN_ON_PARTY, drawnOnParty, formLocale ));
        // TLE - 01/03/07 - IR#ERUH010343869 - Add End



	}




	/**
	 * SectionI corresponds to the portion of the XML document labeled 'SectionI'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionI><!-- Documents Required -->
	 * 		<comm_invoice>
	 * 			<Originals>2</Originals> or <Original>1</Original>
	 * 			<Copies>3</Copies> or <Copy>1</Copy>
	 * 			<Text>Commercial Invoice Text</Text>
	 * 		</comm_invoice>
	 * 		<packing_list>
	 * 			<Originals>7</Originals>
	 * 			<Copies>2</Copies>
	 * 			<Text>Packing List Text</Text>
	 * 		</packing_list>
	 * 		<cert_origin>
	 * 			<Original>1</Original>
	 * 			<Copies>2</Copies>
	 * 			<Text>Certificate of Origin Text.  Here is some more text as well to test word wrapping and stuff.  This is only a test. Thank you.  You are a valued customer.</Text>
	 * 		</cert_origin>
	 * 		<ins_policy>
	 * 			<Originals>2</Originals>
	 * 			<Copies>3</Copies>
	 * 			<Plus>23</Plus>
	 * 			<Covering>Institute Cargo Clause (A)</Covering>
	 * 			<Text>Insurance Policy Text</Text>
	 * 		</ins_policy>
	 * 		<other_req_doc_1>
	 * 			<Original>1</Original>
	 * 			<Copy>1</Copy>
	 * 			<Name>Document Number 1</Name>
	 * 			<Text>Document 1 Text Text Text</Text>
	 * 		</other_req_doc_1>
	 * 		<other_req_doc_2>
	 * 			<Originals>3</Originals>
	 * 			<Copies>9</Copies>
	 * 			<Name>Document Number 2</Name>
	 * 			<Text>Document 2 Text Text Text</Text>
	 * 		</other_req_doc_2>
	 * 		<other_req_doc_3>
	 * 			<Originals>2</Originals>
	 * 			<Copies>5</Copies>
	 * 			<Name>Document Number 3</Name>
	 * 			<Text>Document 3 Text Text Text</Text>
	 * 		</other_req_doc_3>
	 * 		<other_req_doc_4>
	 * 			<Originals>8</Originals>
	 * 			<Copies>11</Copies>
	 * 			<Name>Document Number 4</Name>
	 * 			<Text>Document 4 Text Text Text</Text>
	 * 		</other_req_doc_4>
	 * 	</SectionI>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionI(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		// Call parent Function
		super.sectionI(outputDoc);
	}


	/**
	 * SectionJ corresponds to the portion of the XML document labeled 'SectionJ'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionJ><!-- Additional Documents -->
	 * 		<AdditionalDocuments>Here is where the Additional Documents text should go.  Not sure how much info we will need to display here.  Just babbling away as i am not sure what to put here.</AdditionalDocuments>
	 * 	</SectionJ>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */

	protected void sectionJ(DocumentHandler outputDoc)
		throws RemoteException, AmsException
	{
		// call parent function
		super.sectionJ(outputDoc);
	}


	/**
	 * SectionK corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionK><!-- Transport Document -->
	 * 		<TransportDocument ID="1">
	 * 			<TransDoc ID="1 of 2">
	 * 				<TransShipDeclaration>Here is some Transport Document Shipment Declaration Text</TransShipDeclaration>
	 * 				<TransDocType>Air Transport Document</TransDocType>
	 * 				<Originals>3</Originals>
	 * 				<Copies>4</Copies>
	 * 				<ConsignedOrder>Applicant</ConsignedOrder>
	 * 				<ConsignedTo>Issuing Bank</ConsignedTo>
	 * 				<MarkedFreight>Prepaid</MarkedFreight>
	 * 				<NotifyParty>
	 * 					<Name>Notify Party Name</Name>
	 * 					<AddressLine1>Address Line 1</AddressLine1>
	 * 					<AddressLine2>Address Line 2</AddressLine2>
	 * 					<AddressLine3>Address Line 3</AddressLine3>
	 * 				</NotifyParty>
	 * 				<OtherConsignee>
	 * 					<Name>Other Consignee Party Name</Name>
	 * 					<AddressLine1>Address Line 1</AddressLine1>
	 * 					<AddressLine2>Address Line 2</AddressLine2>
	 * 					<AddressLine3>Address Line 3</AddressLine3>
	 * 				</OtherConsignee>
	 * 				<TransDocText>Here is some Transport Document text to test it out.</TransDocText>
	 * 				<AddlTransDocs>Here is some Additional Transport Documents 2/3</AddlTransDocs>
	 * 			</TransDoc>
	 * 			<ShipmentTerms><!-- Shipment Terms -->
	 * 				<PartialShipment>permitted</PartialShipment>
	 * 				<TransShipment>not permitted</TransShipment>
	 * 				<ShipmentDate>11 December 2005</ShipmentDate>
	 * 				<Incoterm>Delivery Ex Quay (Duty Paid) Somewhere</Incoterm>
	 * 				<From>Some Load of Port</From>
	 * 				<To>Final Destination 3 the movie</To>
	 * 			<!-- Goods Descriptions -->
	 * 			<GoodsDescription>Here is the goods Description if anybody is interested.</GoodsDescription>
	 *		</TransportDocument>
	 * 	</SectionK>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionK(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		// call Parent function
		super.sectionK(outputDoc);

	}


	/**
	 * SectionL corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionL><!-- Other Conditions -->
	 * 		<Confirmation>Advising bank to add their confirmation</Confirmation>
	 * 		<Transferable>Yes</Transferable>
	 * 		<Revolve>Yes</Revolve>
	 * 		<AdditionalConditions>Additional Conditions Text goes here</AdditionalConditions>
	 * 	</SectionL>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionL(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionL(outputDoc);


	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT1>
        <InstructionsToBank>
            <Issue>
                Issue instrument in dropdown
            </Issue>
            <Additional>
                Additional Instructions
            </Additional>
        </InstructionsToBank>     
    </SectionT1>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT1(DocumentHandler outputDoc, String locale)
	throws RemoteException, AmsException
	{
        String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",instrument.getAttribute("language"),locale);		
		addNonNullDocData(languageDesc, outputDoc,"/SectionT1/InstructionsToBank/Issue");
		addNonNullDocData( terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT1/InstructionsToBank/Additional");
	}	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT2>
        <SettlementInstructions>
            <DebitOurAccount>
                Debit: Our Account Number 
            </DebitOurAccount>
            <BranchCode>
                Branch Code
            </BranchCode>
            <DebitForeignAccount>
                Debit: Foreign Currency Account Number 
            </DebitForeignAccount>
            <CurrencyOfAccount>
                Currency of Account dropdown
            </CurrencyOfAccount>            
        </SettlementInstructions>  
    </SectionT2>  
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT2(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("settlement_our_account_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("branch_code"), outputDoc, "/SectionT2/SettlementInstructions/BranchCode");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_curr"), outputDoc, "/SectionT2/SettlementInstructions/CurrencyOfAccount");		
	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionU1>
        <CommisionAndCharges>  
            <DebitOurAccount>
                Debit: Our Account Number 
            </DebitOurAccount>
            <DebitForeignAccount>
                Debit: Foreign Currency Account Number 
            </DebitForeignAccount>
           <CurrencyOfAccount>
               Currency of Account dropdown
           </CurrencyOfAccount>
        </CommisionAndCharges>
    </SectionU1> 
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */	
	protected void sectionU1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("coms_chrgs_our_account_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_curr"), outputDoc, "/SectionU1/CommisionAndCharges/CurrencyOfAccount");
	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionU2>
        <FXContract>
            <Fec>Forward Exchange Contract (FEC)</Fec>
            <FecNumber>FEC Number: xxxxxxxxxx</FecNumber>
            <Rate>Rate: xxxxxxxxxxx</Rate>
            <Amount>Amount: xxxxxxxxx</Amount>
            <MaturityDate>Maturity Date: xxxxxxxxx </MaturityDate>                       
        </FXContract>
    </SectionU2>  
	 *
	 *
	 *If �Covered by FEC Number� textbox has value then display label �Forward Exchange Contract (FEC)� 
	 *followed by, label name �Rate� and value from �Rate� field, label name �Amount� 
	 *and value from �Amount� field, label name �Maturity Date� and value from �Maturity Date�
		Example: Forward Exchange Contract (FEC)
		FEC Number: xxxxxxxxxx
		Rate: xxxxxxxxxxx
		Amount: xxxxxxxxx
		Maturity Date: xxxxxxxxx

	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */	
	protected void sectionU2(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		//addNonNullDocData( " ", outputDoc, "/SectionU2/FXContract/Fec");
		addNonNullDocData(terms.getAttribute("covered_by_fec_number"), outputDoc, "/SectionU2/FXContract/FecNumber");
		addNonNullDocData(terms.getAttribute("fec_rate"), outputDoc, "/SectionU2/FXContract/Rate");
		addNonNullDocData(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), terms.getAttribute("amount_currency_code"), formLocale), outputDoc, "/SectionU2/FXContract/Amount");
		//addNonNullDocData(terms.getAttribute("fec_amount"), outputDoc, "/SectionU2/FXContract/Amount");
		//addNonNullDocData(terms.getAttribute("fec_maturity_date"), outputDoc, "/SectionU2/FXContract/MaturityDate");	
		addNonNullDocData(formatDate(terms, "fec_maturity_date",formLocale), outputDoc, "/SectionU2/FXContract/MaturityDate");
	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionU3>
        <FinanceInstructions>Finance drawing for a period of []  days in [] at our cost</FinanceInstructions>
        <AdditionalInstructions>Additional Instructions </AdditionalInstructions>        
    </SectionU3>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */	
	protected void sectionU3(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("finance_drawing"))){
			String drawingType = "";
			if(refDataMgr.checkCode(TradePortalConstants.DRAWING_TYPE, terms.getAttribute("finance_drawing_type"), formLocale))
				drawingType = refDataMgr.getDescr(TradePortalConstants.DRAWING_TYPE, terms.getAttribute("finance_drawing_type"), formLocale);

			String financeInstructions = "Finance drawing for a period of "+ terms.getAttribute("finance_drawing_num_days")+ " days in "+ drawingType +" at our cost";
			addNonNullDocData(financeInstructions, outputDoc, "/SectionU3/FinanceInstructions");
		}		
		addNonNullDocData(terms.getAttribute("coms_chrgs_other_text"), outputDoc, "/SectionU3/AdditionalInstructions");
	}
	

}