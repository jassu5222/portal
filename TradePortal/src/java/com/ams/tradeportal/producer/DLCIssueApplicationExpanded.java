package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.PmtTermsTenorDtl;
import com.ams.tradeportal.busobj.ShipmentTerms;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */

public class DLCIssueApplicationExpanded extends DLCIssueApplication {
private static final Logger LOG = LoggerFactory.getLogger(DLCIssueApplicationExpanded.class);


	protected void sectionA(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		outputDoc.setAttribute("/SectionA/Title/Line1",
				"Application and Agreement for");
		outputDoc.setAttribute("/SectionA/Title/Line2",
				"Irrevocable Commercial Letter of Credit");

		String imageLocation = getImageLocation();

		outputDoc
				.setAttribute("/SectionA/Logo", imageLocation + bankLogo + "/");

		addNonNullDocData(instrument.getAttribute("bank_instrument_id"),
				outputDoc, "/SectionA/TransInfo/BankInstrumentID");

		outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
				instrument.getAttribute("complete_instrument_id"));

		if (isExpandedFormRequest(instrument
				.getAttribute("instrument_type_code"))) {
			if (isCustomerNotIntegratedWithTPS()) {
				addNonNullDocData(
						transaction.getAttribute("display_transaction_oid"),
						outputDoc, "/SectionA/TransInfo/TransactionID");
			}

			if (InstrumentType.AIR_WAYBILL.equals(instrument
					.getAttribute("instrument_type_code"))
					|| InstrumentType.SHIP_GUAR
							.equalsIgnoreCase(instrument
									.getAttribute("instrument_type_code"))
					|| InstrumentType.LOAN_RQST
							.equalsIgnoreCase(instrument
									.getAttribute("instrument_type_code"))) {
				outputDoc.setAttribute("/SectionA/TransInfo/ReferenceNumber",
						terms.getAttribute("reference_number"));
			} else {			
				outputDoc.setAttribute("/SectionA/TransInfo/ReferenceNumber/",
						instrument.getAttribute("copy_of_ref_num"));
			}
		}

		if (transaction.getAttribute("transaction_type_code").equals(
				TransactionType.AMEND))
			outputDoc.setAttribute(
					"/SectionA/TransInfo/AmendmentDate/",
					formatDate(transaction, "transaction_status_date",formLocale));
		else if (transaction.getAttribute("transaction_type_code").equals(
				TransactionType.ISSUE)) {
			if (instrument.getAttribute("instrument_type_code").equals(
					InstrumentType.REQUEST_ADVISE)) {

			} else
				outputDoc.setAttribute(
						"/SectionA/TransInfo/ApplicationDate/",
						formatDate(transaction, "transaction_status_date",
								formLocale));
		} else {
			// Expanded forms that are AMEND need to have Application Date not
			// Amendment date
			if (isExpandedFormRequest(instrument
					.getAttribute("instrument_type_code"))) {
				outputDoc.setAttribute(
						"/SectionA/TransInfo/ApplicationDate/",
						formatDate(transaction, "transaction_status_date",
								formLocale));
			}
		}

	}	
	
	protected void sectionG(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		//do nothing - override parent
	}
	
	
/*	protected void sectionE(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		//do nothing - override parent
	}*/	
	
	
	/**
	 * SectionL corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: 
	 * 
	<SectionL>
        <Confirmation>Advising Bank not required to add confirmation</Confirmation>
        <Transferable>No</Transferable>
        <Revolve>No</Revolve>
        <TTReimbursement>Allowed</TTReimbursement>
        <AdditionalConditions>This is additional conditions text</AdditionalConditions>
    </SectionL>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionL(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		String bankString = null;
		String confirmationType = null;

		String instrumentType = instrument.getAttribute("instrument_type_code");
		if (instrumentType.equals(InstrumentType.STANDBY_LC))
			bankString = "Correspondent Bank";
		else
			bankString = "Advising Bank";

		// <Confirmation>
		confirmationType = terms.getAttribute("confirmation_type");
		if (confirmationType.equals(TradePortalConstants.CONFIRM_NOT_REQD))
			outputDoc.setAttribute("/SectionL/Confirmation/", bankString
					+ " not required to add confirmation");
		if (confirmationType.equals(TradePortalConstants.BANK_TO_ADD))
			outputDoc.setAttribute("/SectionL/Confirmation/", bankString
					+ " to add their confirmation");
		if (confirmationType.equals(TradePortalConstants.BANK_MAY_ADD))
			outputDoc
					.setAttribute(
							"/SectionL/Confirmation/",
							bankString
									+ " may add their confirmation subject to Beneficiary's approval");

		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			// <AutoExtend>
			// String autoExtend = terms.getAttribute("auto_extend");
			// if (autoExtend.equals(TradePortalConstants.INDICATOR_YES))
			// outputDoc.setAttribute( "/SectionL/AutoExtend/", "Yes");
			// else
			// outputDoc.setAttribute( "/SectionL/AutoExtend/", "No");

		} else {
			// <Transferable>
			String transferrable = terms.getAttribute("transferrable");
			if (transferrable.equals(TradePortalConstants.INDICATOR_YES))
				outputDoc.setAttribute("/SectionL/Transferable/", "Yes");
			else
				outputDoc.setAttribute("/SectionL/Transferable/", "No");

			// <Revolve>
			String revolve = terms.getAttribute("revolve");
			if (revolve.equals(TradePortalConstants.INDICATOR_YES))
				outputDoc.setAttribute("/SectionL/Revolve/", "Yes");
			else
				outputDoc.setAttribute("/SectionL/Revolve/", "No");
		}

		String includeTTReimburseAllowInd =  bankGroup.getAttribute("include_tt_reim_allowed");
		if (TradePortalConstants.INDICATOR_YES.equals(includeTTReimburseAllowInd)){
			if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("tt_reimbursement_allow_ind"))){
				outputDoc.setAttribute("/SectionL/TTReimbursement/", "Allowed");
			}else{
				outputDoc.setAttribute("/SectionL/TTReimbursement/", "Not Allowed");
			}
		}
		
		String value = terms.getAttribute("ucp_version_details_ind");
		if(StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)){
			outputDoc.setAttribute( "/SectionL/ICCPublications", ""); 
			outputDoc.setAttribute( "/SectionL/ICCApplicableRules", "Version: "+ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));

			value = terms.getAttribute("ucp_details");
			if(StringFunction.isNotBlank(value))
				outputDoc.setAttribute( "/SectionL/ICCPDetails", value);
		}		
		
		// <AdditionalConditions>
		addNonNullDocData(terms.getAttribute("additional_conditions"),
				outputDoc, "/SectionL/AdditionalConditions/");

	}
	
	/**
	 * SectionH1 corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionH1>
	 *<!--Payment Terms and Bank Charges -->
        <BankCharges>
            <AllOurAccount/>
        </BankCharges>       
    </SectionH1>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionH1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Bank Charges - TODO
		String bankChargesType = terms.getAttribute("bank_charges_type");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT))
			outputDoc.setAttribute("/SectionH1/BankCharges/AllOurAccount/", "");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE))
			outputDoc.setAttribute("/SectionH1/BankCharges/AllBankCharges/", "");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_OTHER))
			outputDoc.setAttribute("/SectionH1/BankCharges/Other", "");

	}	
	
	/**
	 * SectionH corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
        <PaymentTerms>
            <Sight/>
            <Special/>
            <PayTermsTable>    
                <TableLabel>Acceptance</TableLabel>
                <RowEntry>
                    <Column1Label>Percent</Column1Label>
                    <Column1>95</Column1>
                    <TenorType>tenor type</TenorType>
                    <TenorDetails>tenor details</TenorDetails>
                    <MaturityDate>01 Dec 2014</MaturityDate>
                </RowEntry>      
                <RowEntry>
                    <TableLabel>Acceptance</TableLabel>
                    <Column1Label>Percent</Column1Label>
                    <Column1>95</Column1>
                    <TenorType>tenor type</TenorType>
                    <TenorDetails>tenor details</TenorDetails>
                    <MaturityDate>01 Dec 2014</MaturityDate>
                </RowEntry>                
            </PayTermsTable>
        </PaymentTerms>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionH(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{		
		//Payment terms table
		String paymentTermsType = terms.getAttribute("payment_type");
		//sight
		if ((TradePortalConstants.SIGHT).equals(paymentTermsType)){
			outputDoc.setAttribute("/SectionH/PaymentTerms/Sight", "Sight Payment");
		}else if ((TradePortalConstants.SPECIAL).equals(paymentTermsType)){
			//special
			outputDoc.setAttribute("/SectionH/PaymentTerms/Special", terms.getAttribute("special_tenor_text"));
		}else {
			String tableLabel = "";
			if(TradePortalConstants.ACCEPTANCE.equals(paymentTermsType))	{
				tableLabel = "Acceptance";
			}else if(TradePortalConstants.DEFERED_PAY.equals(paymentTermsType)){
				tableLabel = "Deferred Payment";
			}else if(TradePortalConstants.MIXED_PAY.equals(paymentTermsType)){
				tableLabel = "Mixed Payment";
			}else if(TradePortalConstants.NEGIOTIATION.equals(paymentTermsType)){
				tableLabel = "Negotiation";
			}
			
			//set the table label
			outputDoc.setAttribute("/SectionH/PaymentTerms/TableLabel", tableLabel);
			
			//obtain content for individual rows in the table
			ComponentList pmtTermsTenorDtlList = (ComponentList) terms.getComponentHandle("PmtTermsTenorDtlList");

			int pmtTermsCount = pmtTermsTenorDtlList.getObjectCount();
			String amountPercentInd = terms.getAttribute("percent_amount_dis_ind");
			String tenor="";
			
			DocumentHandler columnEntryDoc = null;
			DocumentHandler rowEntryDoc = null;

			for (int pmtTermsIndex = 0; pmtTermsIndex < pmtTermsCount; pmtTermsIndex++){
				pmtTermsTenorDtlList.scrollToObjectByIndex(pmtTermsIndex);
				String percent= pmtTermsTenorDtlList.getListAttribute("percent");
				String amount= pmtTermsTenorDtlList.getListAttribute("amount");
				String tenorType= pmtTermsTenorDtlList.getListAttribute("tenor_type");
				tenor =  refDataMgr.getDescr(TradePortalConstants.TENOR,tenorType,formLocale);
				String numDaysAfter= pmtTermsTenorDtlList.getListAttribute("num_days_after");
				String daysAfterType= pmtTermsTenorDtlList.getListAttribute("days_after_type");
				String maturityDate= pmtTermsTenorDtlList.getListAttribute("maturity_date");
				try {
					maturityDate = formatDate(maturityDate, formLocale);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//String formatedDate = TPDateTimeUtility.convertJPylonDateToISODate(maturityDate);
				String column1Label = "";
				String column1 = "";
				String paymentTermsDaysAfterType = refDataMgr.getDescr(TradePortalConstants.PMT_TERMS_DAYS_AFTER,daysAfterType,formLocale);
				String tenorDetails = "";
				if(StringFunction.isNotBlank(numDaysAfter) && StringFunction.isNotBlank(paymentTermsDaysAfterType)){
					tenorDetails = numDaysAfter+" days after "+paymentTermsDaysAfterType;
				}
				
				
				if(TradePortalConstants.NEGIOTIATION.equals(paymentTermsType) || TradePortalConstants.MIXED_PAY.equals(paymentTermsType)){
					 if( "P".equals(amountPercentInd)){
						    if(StringFunction.isNotBlank(percent)){
						    	column1 = percent;
						    }
						    column1Label = "Percent";
						}else if ( "A".equals(amountPercentInd)){							
							if (StringFunction.isNotBlank(amount))
				            {	
								//format amount
								amount = TPCurrencyUtility.getDisplayAmount(amount, terms.getAttribute("amount_currency_code"), formLocale);
								column1 = amount;							
				            }
							column1Label = "Amount";
						}

				 }else{
					 column1 = percent;
					 column1Label = "Percent";
				 }
				
				//start a new row
				rowEntryDoc = new DocumentHandler();
				columnEntryDoc = new DocumentHandler();	
				addNonNullDocData(column1Label, columnEntryDoc,"/Column1Label");	
				rowEntryDoc.addComponent("/RowEntry", columnEntryDoc);
				
				columnEntryDoc = new DocumentHandler();
				addNonNullDocData(column1, columnEntryDoc,"/Column1");
				rowEntryDoc.addComponent("/RowEntry", columnEntryDoc);
				
				//tenor type
				columnEntryDoc = new DocumentHandler();
				addNonNullDocData(tenor, columnEntryDoc,"/TenorType");
				rowEntryDoc.addComponent("/RowEntry", columnEntryDoc);
				
				//tenor details
				columnEntryDoc = new DocumentHandler();
				addNonNullDocData(tenorDetails, columnEntryDoc,"/TenorDetails");
				rowEntryDoc.addComponent("/RowEntry", columnEntryDoc);
				
				//maturity date
				columnEntryDoc = new DocumentHandler();
				addNonNullDocData(maturityDate, columnEntryDoc,"/MaturityDate");	
				rowEntryDoc.addComponent("/RowEntry", columnEntryDoc);
				
				outputDoc.addComponent("/SectionH/PaymentTerms/PayTermsTable", rowEntryDoc);

			}
						

		
		}

	}
	
	
	//section k (expanded - mostly same content but very different order and format
	/**
	 * SectionK corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like:
	 * 
	 * 
<SectionK>
        <TransportDocument ID="1">
            <TransDoc ID="1 of 1">                
                <PartialShipment>Not Permitted</PartialShipment>
                <PresentationDays>21</PresentationDays>
                <TransDocHeader>Transport Document</TransDocHeader>
                <TransDocLabel>Transport Document</TransDocLabel>
                <TransDocType>Air Transport Document</TransDocType>                
                <Originals>1</Originals>
                <Copies>1</Copies>
                <TransShipDeclaration>desc</TransShipDeclaration>
                <ConsignedTo>Other</ConsignedTo>
                <MarkedFreight>Prepaid</MarkedFreight>
                <NotifyParty>
                    <Name>Balls</Name>
                    <AddressLine1>573 Santa Monica Pier</AddressLine1>
                    <AddressLine2>2222</AddressLine2>
                    <AddressLine3>Santa Monica,CA US</AddressLine3>
                </NotifyParty>
                <OtherConsignee>
                    <Name>Bats</Name>
                    <AddressLine1>484 River Road</AddressLine1>
                    <AddressLine3>Edgewater,NJ US</AddressLine3>
                </OtherConsignee>
                <TransDocText>yyy</TransDocText>
                <AddlTransDocs>ytttrtdyuyt</AddlTransDocs>
            </TransDoc>
            <ShipmentTerms>
                <TransShipment>Permitted</TransShipment>
                <Incoterm>Free on Board yyyy</Incoterm>
                <From>gffggh</From>
                <FromLoad>tjtyjtyj</FromLoad>
                <To>fghnfhjt</To>
                <ToDischarge>tjtyjytjytkj</ToDischarge>
            </ShipmentTerms>
            <GoodsDescription>9500 MT Refined Crude OIl</GoodsDescription>
        </TransportDocument>
    </SectionK>	 
	 
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionK(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		ComponentList shipmentTermsList = (ComponentList) terms
				.getComponentHandle("ShipmentTermsList");
		if (shipmentTermsList != null) {

			// Get the list of shipment terms and sort by creation date
			int numberShipmentTerms = shipmentTermsList.getObjectCount();
			Hashtable h = new Hashtable();
			Date creationDate = null;
			for (int i = 0; i < numberShipmentTerms; i++) {
				shipmentTermsList.scrollToObjectByIndex(i);
				creationDate = shipmentTermsList.getBusinessObject()
						.getAttributeDateTime("creation_date");
				h.put(creationDate, new Integer(i));
			}
			Vector v = new Vector(h.keySet());
			Collections.sort(v);

			int sortedIndex = 0;
			for (Iterator it = v.iterator(); it.hasNext(); sortedIndex++) {
				creationDate = (Date) it.next();
				int index = ((Integer) h.get(creationDate)).intValue();
				// Setup ID tag Values and retrieve each ShipmentTerms object
				String currentSection = "/SectionK/TransportDocument("
						+ String.valueOf(sortedIndex + 1) + ")/";
				
				String transDocSection = currentSection + "/TransDoc("
						+ String.valueOf(sortedIndex + 1) + " of "
						+ String.valueOf(numberShipmentTerms) + ")/";
				
				shipmentTermsList.scrollToObjectByIndex(index);
				ShipmentTerms transportDocument = (ShipmentTerms) shipmentTermsList
						.getBusinessObject();

				
				// TransDocType
				String transDocType = transportDocument
						.getAttribute("trans_doc_type");
				if (isNotEmpty(transDocType))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.TRANS_DOC_TYPE, transDocType,
							formLocale), outputDoc, transDocSection
							+ "/TransDocType/");				
				// Partial Shipment
				String partialShipment = terms
						.getAttribute("partial_shipment_allowed");
				if (partialShipment.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(transDocSection
							+ "/PartialShipment/", "Permitted");
				else
					outputDoc.setAttribute(transDocSection
							+ "/PartialShipment/",
							"Not Permitted");	
				
				//presentation days
				String presentDocsWithinDays = null;

				presentDocsWithinDays = terms.getAttribute("present_docs_within_days");
				if (isNotEmpty(presentDocsWithinDays))
					addNonNullDocData(presentDocsWithinDays, outputDoc,
							transDocSection+"/PresentationDays/");				

				//docheader
				outputDoc.setAttribute(transDocSection
						+ "/TransDocHeader/",
						"Transport Document");		
				
				//doclabel
				//outputDoc.setAttribute(transDocSection+ "/TransDocLabel/","Transport Document");	
				// Transport Document Text
				//String transDocText = transportDocument.getAttribute("trans_doc_text");
				addNonNullDocData(transportDocument.getAttribute("description"), outputDoc, transDocSection	+ "/TransDocLabel");
				
				// TransDocType

				if (isNotEmpty(transDocType))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.TRANS_DOC_TYPE, transDocType,
							formLocale), outputDoc, transDocSection
							+ "/TransDocumentType/");				
				

				// Originals
				addNonNullDocData(
						transportDocument.getAttribute("trans_doc_originals"),
						outputDoc, transDocSection + "/Originals/");
				// Copies
				addNonNullDocData(
						transportDocument.getAttribute("trans_doc_copies"),
						outputDoc, transDocSection + "/Copies/");
				
				//transshipdeclaration
				//String transDocText = transportDocument.getAttribute("trans_doc_text");
				
				addNonNullDocData(transportDocument.getAttribute("trans_doc_text"),outputDoc, transDocSection + "/TransShipDeclaration/");
				
				// ConsignedOrder and Consigned to
				String consignedOrder, consignedTo;
				consignedOrder = transportDocument
						.getAttribute("trans_doc_consign_order_of_pty");
				if (isNotEmpty(consignedOrder))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
							consignedOrder, formLocale), outputDoc,
							transDocSection + "/ConsignedOrder/");
				
				consignedTo = transportDocument
						.getAttribute("trans_doc_consign_to_pty");
				if (isNotEmpty(consignedTo))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
							consignedTo, formLocale), outputDoc,
							transDocSection + "/ConsignedTo/");
				
				// Marked Freight
				String markedFreightType = transportDocument
						.getAttribute("trans_doc_marked_freight");
				if (markedFreightType.equals(TradePortalConstants.PREPAID))
					outputDoc.setAttribute(transDocSection + "/MarkedFreight/",
							"Prepaid");
				if (markedFreightType.equals(TradePortalConstants.COLLECT))
					outputDoc.setAttribute(transDocSection + "/MarkedFreight/",
							"Collect");

				// Notify and Other Consignee Party
				TermsParty notifyParty = null; // Notify Party
				String notifyPartyOid = null;
				TermsParty otherConsigneeParty = null; // Other Consignee Party
				String otherConsigneePartyOid = null;
				String name, addressLine1, addressLine2, addressLine3;

				notifyPartyOid = transportDocument
						.getAttribute("c_NotifyParty");
				if (isNotEmpty(notifyPartyOid)) {
					notifyParty = (TermsParty) transportDocument
							.getComponentHandle("NotifyParty");
					name = notifyParty.getAttribute("name");
					addNonNullDocData(name, outputDoc, transDocSection
							+ "/NotifyParty/Name");
					addressLine1 = notifyParty.getAttribute("address_line_1");
					addNonNullDocData(addressLine1, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine1");
					addressLine2 = notifyParty.getAttribute("address_line_2");
					addNonNullDocData(addressLine2, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine2");
					addressLine3 = notifyParty.getAttribute("address_line_3");
					addNonNullDocData(addressLine3, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine3");
				}
				otherConsigneePartyOid = transportDocument
						.getAttribute("c_OtherConsigneeParty");
				if (isNotEmpty(otherConsigneePartyOid)) {
					otherConsigneeParty = (TermsParty) transportDocument
							.getComponentHandle("OtherConsigneeParty");
					name = otherConsigneeParty.getAttribute("name");
					addNonNullDocData(name, outputDoc, transDocSection
							+ "/OtherConsignee/Name");
					addressLine1 = otherConsigneeParty
							.getAttribute("address_line_1");
					addNonNullDocData(addressLine1, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine1");
					addressLine2 = otherConsigneeParty
							.getAttribute("address_line_2");
					addNonNullDocData(addressLine2, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine2");
					addressLine3 = otherConsigneeParty
							.getAttribute("address_line_3");
					addNonNullDocData(addressLine3, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine3");
				}


				// Additional Transport Document Text
				String addlTransDocs = transportDocument
						.getAttribute("trans_addl_doc_included");
				if (addlTransDocs.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(transDocSection + "/AddlTransDocs/",
							transportDocument
									.getAttribute("trans_addl_doc_text"));

	
				//Shipment Details
				// Transhipment
				String transShipment = transportDocument
						.getAttribute("transshipment_allowed");
				if (transShipment.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/TransShipment/", "Permitted");
				else
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/TransShipment/", "Not Permitted");
				// Latest Shipment Date
				String latestShipmentDate = transportDocument
						.getAttribute("latest_shipment_date");
				if (isNotEmpty(latestShipmentDate))
					addNonNullDocData(
							formatDate(transportDocument,
									"latest_shipment_date", formLocale),
							outputDoc, currentSection
									+ "/ShipmentTerms/ShipmentDate");
				// Incoterm (Incoterm/)
				String incoterm = transportDocument.getAttribute("incoterm");
				if (isNotEmpty(incoterm))
					incoterm = refDataMgr
							.getDescr(TradePortalConstants.INCOTERM, incoterm,
									formLocale);
				String shippingTerm = transportDocument
						.getAttribute("incoterm_location");
				incoterm = appendAttributes(incoterm, shippingTerm, " ");
				addNonNullDocData(incoterm, outputDoc, currentSection
						+ "/ShipmentTerms/Incoterm");

				// Shipment From and To Fields
				String shipFrom = transportDocument
						.getAttribute("shipment_from");
				String shipLoad = transportDocument
						.getAttribute("shipment_from_loading");
				String shipTo = transportDocument.getAttribute("shipment_to");
				String shipDischarge = transportDocument
						.getAttribute("shipment_to_discharge");

				if (!isNotEmpty(shipLoad) && !isNotEmpty(shipTo)) {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipDischarge)) {
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipLoad) && !isNotEmpty(shipDischarge)) {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipTo)) {
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/FromLoad");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/ToDischarge");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");					
				}

				// Goods description
				addNonNullDocData(
						transportDocument.getAttribute("goods_description"),
						outputDoc, currentSection + "/GoodsDescription");	

				// IR-44732  Adding PO Line Items

				addNonNullDocData(
						transportDocument.getAttribute("po_line_items"),
						outputDoc, currentSection + "/POLineItems");

				String shipmentTermsOid = transportDocument
						.getAttribute("shipment_oid");
				addStructuredPOLineItems(shipmentTermsOid, outputDoc,
						currentSection);
								

			}

		}
		
		

	}

}