package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.text.*;
import java.rmi.*;

import javax.servlet.http.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.busobj.util.*;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various
 * business objects then pass this data through an XSL stylesheet format the
 * data for display then pass the result on to a cocoon wrapper to display the
 * result in a PDF format. The method: populateXmlDoc does most of the work,
 * however get stream puts it all together and returns the result (a Reader
 * object). The Corresponding XSL file is: DLCIssueApplication.xsl.
 * 
 * Copyright � 2001 American Management Systems, Incorporated All rights
 * reserved
 */
// BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
// public class DocumentProducer extends AbstractProducer implements Status {
// String headerStringPart1 =
// "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
// "<?xml-stylesheet href=\"";
public class DocumentProducer extends AbstractProducerGenerator {
	private static final Logger LOG = LoggerFactory.getLogger(DocumentProducer.class);
	// BSL Cocoon Upgrade 09/28/11 Rel 7.0 END

	// Declare Instance Variables for Business Objects Used to get data for
	// forms
	protected Transaction transaction = null;
	protected Instrument instrument = null;
	protected OperationalBankOrganization opBankOrg = null;
	protected CorporateOrganization corpOrg = null;
	protected Terms terms = null; // Customer Entered terms
	protected TermsParty counterParty = null;  
	// Other variables for computing fields
	protected ReferenceDataManager refDataMgr = null;
	protected String formLocale = null;
	protected String bankLogo = null;
	protected String formContextPath = null;
	protected Transaction transactionOriginal = null; // original Transaction
	protected Transaction transactionActive = null; // active Transaction
	protected Terms termsOriginal = null; // original Transaction Customer entered Terms
	protected Terms bankTermsOriginal = null;
	protected Terms bankTermsActive = null;
	protected BankOrganizationGroup bankGroup = null;
	protected String enableCustomPDF = null;

	/**
	 * getStream's goal is to build a long string with all the data and return a
	 * reader based on the string's data.
	 */
	public Reader getStream(HttpServletRequest request) throws IOException,
			AmsException {
		DocumentHandler xmlDoc = populateXmlDoc(getBlankDocumentHandler(),
				EncryptDecrypt.decryptStringUsingTripleDes(request
						.getParameter("transaction")),
				request.getParameter("locale"),
				request.getParameter("bank_prefix"), request.getContextPath());

		return new StringReader(xmlDoc.toString());
	}

	/**
	 * Returns an empty DocumentHandler. Subclasses can override to specify an
	 * alternate root element.
	 */
	protected DocumentHandler getBlankDocumentHandler() {
		return new DocumentHandler("<Document></Document>", false);
	}

	/**
	 * Returns root of the URL for image files located on Portal server.
	 */
	protected String getImageLocation() {
		String bundleName = "TradePortal";
		String imageLocation = "";

		try {
			ResourceBundle portalProperties = PropertyResourceBundle
					.getBundle(bundleName);
			imageLocation = portalProperties.getString("producerRoot")
					+ TradePortalConstants.IMAGES_PATH;
		} catch (Exception e) {
			LOG.debug("Cannot resolve image location from " + bundleName
					+ " resource bundle. Exception is " + e);
			imageLocation = TradePortalConstants.IMAGES_PATH;
		}

		return imageLocation;
	}
	
	/**
	 * Returns root of the URL for image files located on Portal server.
	 */
	protected String getAccountsDisplayValue(String accountOid) throws AmsException{
		String displayAccNum = "";
		String acct_num = "";
		String acct_currency = "";
		String acct_disc = "";
		
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(
	  			"select account_oid, account_number, account_name, currency from account where account_oid = ?",
	  			//+ "account_description, from account, user_authorized_acct where account_oid = a_account_oid and account_oid = ?",
	  			false, new Object[]{accountOid});

	  	if(results != null){
			try {
				acct_num = results.getAttribute("/ResultSetRecord(0)/ACCOUNT_NUMBER");
			} catch (Exception e) {
				acct_num = "";
			}
			try {
				acct_currency = results.getAttribute("/ResultSetRecord(0)/CURRENCY");
			} catch (Exception e) {
				acct_currency = "";
			}
			try {
				acct_disc = results.getAttribute("/ResultSetRecord(0)/ACCOUNT_NAME");
			} catch (Exception e) {
				acct_disc = "";
			}
			
			if (StringFunction.isNotBlank(acct_disc)) {
				displayAccNum = acct_num + "-" + acct_disc;
			}
			if (StringFunction.isNotBlank(acct_currency)) {
				displayAccNum += " (" + acct_currency + ")";
			}
	  	}

		return displayAccNum;
	}
	
	protected String getAccountCurrency(String accountOid) throws AmsException{
		String acct_currency = "";
		
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(
	  			"select account_oid, currency from account, user_authorized_acct where account_oid = a_account_oid and account_oid = ?",
	  			false, new Object[]{accountOid});

	  	if(results != null){
			try {
				acct_currency = results.getAttribute("/ResultSetRecord(0)/CURRENCY");
			} catch (Exception e) {
				acct_currency = "";
			}
	  	}

		return acct_currency;
	}

	// BSL Cocoon Upgrade 10/13/11 Rel 7.0 END

	/**
	 * This method is the method that gathers all the data and builds the Xml
	 * document to be returned to the caller for display. The Xsl template is
	 * expecting the Xml doc to send data out in a specific format...ie in
	 * sections. If you scroll down the left side you can easily jump around the
	 * xml doc looking for specific section letters.
	 * 
	 * @param Output
	 *            Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key
	 *            - This is the Transaction oid the will help us get all our
	 *            data. With out this oid, no data can be retrieved.
	 * @param Locale
	 *            - This is the current users locale, it will help in
	 *            establishing the client Server data bridge as well as building
	 *            various B.O.s
	 * @param BankPrefix
	 *            - This may be removed in the future.
	 * @param ContextPath
	 *            - the context path under which this request was made
	 * @return OutputDoc - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc,
			String key, String locale, String bankPrefix, String contextPath)
			throws RemoteException, AmsException {
		Long transactionOid = new Long(key);

		// Setup the logo file location, context path and locale instance
		// variables
		if ("blueyellow".equals(bankPrefix)) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		}
		if ("anztransac".equals(bankPrefix)) {
			bankPrefix = "anz";
		}
		bankLogo = bankPrefix + "/images";
		formContextPath = contextPath;
		formLocale = locale;

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);

		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");
		/*KMehta - 20 Aug 2014 - Rel9.1 IR-T36000030259 - Add  - Begin*/
		if (formLocale.equals("fr_CA")) {
			formLocale = "en_GB";
		}
		/*KMehta - 20 Aug 2014 - Rel9.1 IR-T36000030259 - Add  - End*/
		
		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();
		// Nar CR-938 rel9500 02/25/2016 - set indicator for custom Enable PDF config setting.
		// get Custom PDF indicator
		enableCustomPDF = ConfigSettingManager.getConfigSetting("PORTAL_APP", "PORTAL", "EnableCustomPDF", "N");

		// Create Transaction
		transaction = (Transaction) EJBObjectFactory
				.createClientEJB(serverLocation, "Transaction",
						transactionOid.longValue(), csdb);

		// set up a Instrument object
		instrument = (Instrument) EJBObjectFactory.createClientEJB(
				serverLocation, "Instrument",
				transaction.getAttributeLong("instrument_oid"), csdb);

		// set up a Operational Bank Org object
		opBankOrg = (OperationalBankOrganization) EJBObjectFactory
				.createClientEJB(serverLocation, "OperationalBankOrganization",
						instrument.getAttributeLong("op_bank_org_oid"), csdb);

		// set up a Corporate Organization object
		corpOrg = (CorporateOrganization) EJBObjectFactory.createClientEJB(
				serverLocation, "CorporateOrganization",
				instrument.getAttributeLong("corp_org_oid"), csdb);

		// set up a Terms component off of the transaction
		terms = (Terms) transaction.getComponentHandle("CustomerEnteredTerms");
		
		//set up a TermsParty object
		if(StringFunction.isNotBlank(instrument.getAttribute("a_counter_party_oid"))){
			counterParty = (TermsParty) EJBObjectFactory.createClientEJB(
				serverLocation, "TermsParty",
				instrument.getAttributeLong("a_counter_party_oid"), csdb);
		}
		
		// set up a Bank Org Group object
		bankGroup = (BankOrganizationGroup) EJBObjectFactory
				.createClientEJB(serverLocation, "BankOrganizationGroup",
						opBankOrg.getAttributeLong("bank_org_group_oid"), csdb);

		// set up a Terms component off of the transaction
		if (!transaction.getAttribute("transaction_type_code").equals(TransactionType.ISSUE)) {
			transactionOriginal = (Transaction) EJBObjectFactory.createClientEJB(serverLocation, "Transaction", instrument.getAttributeLong("original_transaction_oid"), csdb);
			
			try {
				termsOriginal = (Terms) transactionOriginal.getComponentHandle("CustomerEnteredTerms");
			} catch (Exception e) {
				LOG.info("Failed to getComponentHandle for CustomerEnteredTerms off of the original Transaction UOID = ["
								+ instrument.getAttribute("original_transaction_oid")
								+ "] for the instrument");
			}
			
			try {
				bankTermsOriginal = (Terms) transactionOriginal.getComponentHandle("BankReleasedTerms");
			} catch (Exception e) {
				LOG.info("Failed to getComponentHandle for BankReleasedTerms off of the original Transaction UOID = ["
								+ instrument.getAttribute("original_transaction_oid")
								+ "] for the instrument");
			}
			
			if( StringFunction.isNotBlank(instrument.getAttribute("active_transaction_oid")) ) {
        		transactionActive = (Transaction) EJBObjectFactory.createClientEJB(serverLocation, "Transaction", instrument.getAttributeLong("active_transaction_oid"), csdb);
				try{
					bankTermsActive = (Terms) transactionOriginal.getComponentHandle("BankReleasedTerms");
				}catch (Exception e) {
					LOG.info("Failed to getComponentHandle for BankReleasedTerms off of the Active Transaction UOID = ["
							+ instrument.getAttribute("active_transaction_oid")
							+ "] for the instrument");
				}
			}
		}
		

		return outputDoc;
	}

	/**
	 * cleanUpBeans just performs beanRemove for all objects created'.
	 * 
	 * @param OpBankOrg
	 *            opBankOrg - This is the object which we will get our data from
	 *            for the XML doc.
	 * @param CorpOrg
	 *            corpOrg - This is the location of the bank logo file
	 * @param Instrument
	 *            instrument - This is the object which we will get our data
	 *            from for the XML doc.
	 * @param Transaction
	 *            transaction - This is the object which we will get our data
	 *            from for the XML doc.
	 */
	protected void cleanUpBeans() throws RemoteException, AmsException {
		beanRemove(transaction, "Transaction");
		beanRemove(instrument, "Instrument");
		beanRemove(opBankOrg, "Operational Bank Org");
		beanRemove(corpOrg, "Corporate Organization");
		beanRemove(transactionOriginal, "Original Transaction");
	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled
	 * 'SectionA'. The following is an example of what the resulting Xml should
	 * look like: <SectionA><!-- Header Info --> <Title> <Line1>Application and
	 * Agreement for</Line1> <Line2>Irrevocable Commercial Letter of
	 * Credit</Line2> </Title>
	 * <Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 * <TransInfo> <InstrumentID>ILC80US01P</InstrumentID>
	 * <ReferenceNumber>123456REF</ReferenceNumber> <ApplicationDate>05 October
	 * 2005</ApplicationDate> <AmendmentDate>05 November 2005</AmendmentDate>
	 * </TransInfo> </SectionA>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionA(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		String imageLocation = getImageLocation();
		String instrumentType = instrument.getAttribute("instrument_type_code");
		String transactionType = transaction.getAttribute("transaction_type_code");


		outputDoc.setAttribute("/SectionA/Logo", imageLocation + bankLogo + "/");

		if(isExpandedFormRequest(instrumentType)){
			addNonNullDocData( instrument.getAttribute("bank_instrument_id"), outputDoc, "/SectionA/TransInfo/BankInstrumentID");		
		}
		
		if (instrumentType.equals(
				InstrumentType.REQUEST_ADVISE))
			if (transactionType.equals(
					TransactionType.ISSUE))
				outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
						terms.getAttribute("issuer_ref_num"));
			else
				outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
						termsOriginal.getAttribute("issuer_ref_num"));
		else {
			//InstrumentType.STANDBY_LC
			if ( !TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			   if(((InstrumentType.IMPORT_DLC.equals(instrumentType) || InstrumentType.STANDBY_LC.equals(instrumentType)) 
					   && TransactionType.AMEND.equals(transactionType)) || (InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType))){
				    if(isExpandedFormRequest(instrumentType)){
					    outputDoc.setAttribute("/SectionA/TransInfo/InstrumentIDExpanded",	instrument.getAttribute("complete_instrument_id"));
				    }else{
					    outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
							instrument.getAttribute("complete_instrument_id"));
				    }
			    }else{
				   outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
						instrument.getAttribute("complete_instrument_id"));
			   }
			}else{
				//Rel 9.5 IR T36000048115 - Start
				if( (InstrumentType.IMPORT_DLC.equals(instrumentType)) || (InstrumentType.STANDBY_LC.equals(instrumentType)) ){
					if( TransactionType.AMEND.equals(transactionType) ){
						if(isExpandedFormRequest(instrumentType)){
						    outputDoc.setAttribute("/SectionA/TransInfo/InstrumentIDExpanded",	instrument.getAttribute("complete_instrument_id"));
					    }else{
					    	if(InstrumentType.STANDBY_LC.equals(instrumentType)){
					    		outputDoc.setAttribute( "/SectionA/TransInfo/InstrumentNumLabel", "Letter of Credit Number:");
					    		outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
										instrument.getAttribute("complete_instrument_id"));
					    	}else{
					    		outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/",
										instrument.getAttribute("complete_instrument_id"));
					    	}
						    
					    }
					}
				}
			}//Rel 9.5 IR T36000048115 - End
			if(isExpandedFormRequest(instrumentType)){
				if(isCustomerNotIntegratedWithTPS()){
					addNonNullDocData(transaction.getAttribute("display_transaction_oid"), outputDoc, "/SectionA/TransInfo/TransactionID");
				}		
			}	
			if(InstrumentType.AIR_WAYBILL.equals(instrumentType) || InstrumentType.SHIP_GUAR.equalsIgnoreCase(instrumentType) || InstrumentType.LOAN_RQST.equalsIgnoreCase(instrumentType)){
				outputDoc.setAttribute("/SectionA/TransInfo/ReferenceNumber",	 terms.getAttribute("reference_number"));
			}else{
				outputDoc.setAttribute("/SectionA/TransInfo/ReferenceNumber/", instrument.getAttribute("copy_of_ref_num"));
			}
		}
		
		if(isExpandedFormRequest(instrumentType)){
				outputDoc.setAttribute(	"/SectionA/TransInfo/ApplicationDate/",	formatDate(transaction, "transaction_status_date",  formLocale));
		}else{

			if (transactionType.equals(
					TransactionType.AMEND))
				outputDoc.setAttribute(
						"/SectionA/TransInfo/AmendmentDate/",
						formatDate(transaction, "transaction_status_date",
								formLocale));
			else if (transactionType.equals(
					TransactionType.ISSUE)) {
				if (instrumentType.equals(
						InstrumentType.REQUEST_ADVISE)) {

				} else
					outputDoc.setAttribute(
							"/SectionA/TransInfo/ApplicationDate/",
							formatDate(transaction, "transaction_status_date",
									formLocale));
			}
		}
		
	}
	
	protected void sectionASettleInstr(DocumentHandler outputDoc, String titleLine2) throws RemoteException, AmsException {
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Settlement Instructions");
		outputDoc.setAttribute( "/SectionA/Title/Line2", titleLine2);
		
		String imageLocation = getImageLocation();

		outputDoc.setAttribute("/SectionA/Logo", imageLocation + bankLogo + "/");
		
		outputDoc.setAttribute("/SectionA/TransInfo/InstrumentID/", instrument.getAttribute("complete_instrument_id"));
		outputDoc.setAttribute("/SectionA/TransInfo/ReferenceNumber", terms.getAttribute("our_reference"));
		
		//Prepare custom footer items for Settlement Instructions per design CR-818 Rel-9.4
		String firstName = "", lastName = "";
		DocumentHandler results = DatabaseQueryBean.getXmlResultSet(
	  			"Select FIRST_NAME, LAST_NAME from users where user_oid = ?",
	  			false, new Object[]{transaction.getAttribute("last_entry_user_oid")});

	  	if(results != null){
	  		firstName = results.getAttribute("/ResultSetRecord(0)/FIRST_NAME");
	  		lastName = results.getAttribute("/ResultSetRecord(0)/LAST_NAME");
	  	}
	  	
	  	SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentGMTDateTime = sdf.format(new Date());
        
        outputDoc.setAttribute("/SettleInstrFooter/footerItems/FooterItem", currentGMTDateTime+" - "+firstName+" "+lastName);
	}

	/**
	 * SectionB corresponds to the portion of the XML document labeled
	 * 'SectionB'. The following is an example of what the resulting Xml should
	 * look like: <SectionB><!-- Operational Bank Org Info --> <Name>Operational
	 * Bank Org Name</Name> <AddressLine1>Operational Bank Org Addr Line
	 * 1</AddressLine1> <AddressLine2>Operational Bank Org Addr Line
	 * 2</AddressLine2> <AddressStateProvince>Operational Bank Org City +
	 * Province</AddressStateProvince> <AddressCountryPostCode>Operational Bank
	 * Org Country + Post Code</AddressCountryPostCode> <PhoneNumber>Operational
	 * Bank Org Telephone #</PhoneNumber> <Fax>Operational Bank Org Fax #</Fax>
	 * <Swift>Operational Bank Org Address</Swift> <Telex>Operational Bank Org
	 * Telex # Operational Bank Org Answer Back</Telex> </SectionB>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 * @param opBankOrg
	 *            Business Object - This is the object which we will get our
	 *            data from for the XML doc.
	 * @param locale
	 *            - This is the locale for the current user.
	 * @param Reference
	 *            DataMgr - The RefData Mgr will help us convert the refdata
	 *            code to it's description. <BR>
	 */
	protected void sectionB(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		// Build first part of address
		createPartyAddressXml(outputDoc, opBankOrg, "SectionB",
				"Operational Org");

		String sectionAndParty = "/SectionB/Party(Operational Org)";

		// [PhoneNumber]
		addNonNullDocData(opBankOrg.getAttribute("phone_number"), outputDoc,
				sectionAndParty + "/PhoneNumber/");
		// [Fax]
		addNonNullDocData(opBankOrg.getAttribute("fax_1"), outputDoc,
				sectionAndParty + "/Fax/");
		// [Swift]
		String swift = opBankOrg.getAttribute("swift_address_part1")
				+ opBankOrg.getAttribute("swift_address_part2");
		if (isNotEmpty(swift))
			outputDoc.setAttribute(sectionAndParty + "/Swift/", swift);
		// [Telex]
		addNonNullDocData(opBankOrg.getAttribute("telex_1"), outputDoc,
				sectionAndParty + "/Telex/");
	}

	/**
	 * SectionC corresponds to the portion of the XML document labeled
	 * 'SectionC'. The following is an example of what the resulting Xml should
	 * look like: <SectionC><!-- Operational Bank org Disclaimer -->
	 * <Name>Operational Bank Org Name</Name> </SectionC>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// [Name]
		outputDoc.setAttribute("/SectionC/Name/", opBankOrg.getAttribute("name"));
		

	}

	/**
	 * SectionD corresponds to the portion of the XML document labeled
	 * 'SectionD'. The following is an example of what the resulting Xml should
	 * look like: <SectionD><!-- Applicant and Beneficiary--> <Applicant>
	 * <Name>Applicant Name</Name> <AddressLine1>Applicant Address Line
	 * 1</AddressLine1> <AddressLine2>Applicant Address Line 2</AddressLine2>
	 * <AddressLine3>Applicant Address Line 3</AddressLine3>
	 * <AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 * <AddressCountryPostCode>Applicant Country + Post
	 * Code</AddressCountryPostCode> </Applicant> <Beneficiary>
	 * <Name>Beneficiary Name</Name> <AddressLine1>Beneficiary Address Line
	 * 1</AddressLine1> <AddressLine2>Beneficiary Address Line 2</AddressLine2>
	 * <AddressLine3>Beneficiary Address Line 3</AddressLine3>
	 * <AddressStateProvince>Beneficiary City + Province</AddressStateProvince>
	 * <AddressCountryPostCode>Beneficiary Country + Post
	 * Code</AddressCountryPostCode> <PhoneNumber>Phone Number</PhoneNumber>
	 * </Beneficiary> </SectionD>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionD(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		String benePArty = terms.getAttribute("c_FirstTermsParty");
		if (InstrumentServices.isBlank(benePArty)) {
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");
			createPartyAddressXml(outputDoc, applicantParty, "SectionD",
					"Applicant");
		} else {
			TermsParty beneficiaryParty = (TermsParty) terms
					.getComponentHandle("FirstTermsParty");
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");

			createPartyAddressXml(outputDoc, applicantParty, "SectionD",
					"Applicant");
			createPartyAddressXml(outputDoc, beneficiaryParty, "SectionD",
					"Beneficiary");

			addNonNullDocData(beneficiaryParty.getAttribute("phone_number"),
					outputDoc, "/SectionD/Party(Beneficiary)/PhoneNumber/");
		}
	}
	
	//This method will get called if Settle Instruction is in FIFC: Finance in Finance Currency
	protected void sectionDSettleInstrFIFC(DocumentHandler outputDoc) throws RemoteException, AmsException {
		String finRollCurr = "";
		if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_curr"))) {
			finRollCurr = terms.getAttribute("fin_roll_curr");
		}
		String finRollPartialPayAmt = "";
		if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_partial_pay_amt"))){
			finRollPartialPayAmt = terms.getAttribute("fin_roll_partial_pay_amt");
		}
		String transCurrency = "";
		if(StringFunction.isNotBlank(transaction.getAttribute("copy_of_currency_code"))){
			transCurrency = transaction.getAttribute("copy_of_currency_code");
		}
		
		String principal_acct_num = getAccountsDisplayValue(terms.getAttribute("principal_account_oid"));
		String charges_acct_num = getAccountsDisplayValue(terms.getAttribute("charges_account_oid"));
		
		outputDoc.setAttribute( "/SectionD/PayDtls/SubTitle", "Please use the following instructions for settlement of the above item:");

		if(terms.getAttribute("pay_in_full_fin_roll_ind").equals("P")){
			outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/SubTitle", "Pay Full Amount (refer payment details directly below):");
			if(terms.getAttribute("account_remit_ind").equals("A")){
				outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/Account/SubTitle", "Debit the following Accounts:");
				addNonNullDocData(principal_acct_num, outputDoc, "/SectionD/PayDtls/PayFull/Account/DebitAccForPrinciple");
				
				addNonNullDocData(charges_acct_num, outputDoc, "/SectionD/PayDtls/PayFull/Account/DebitAccForCharges");
			}else if(terms.getAttribute("account_remit_ind").equals("R")){
				outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/Account/Remitted", "We have remitted funds to your branch");
			}
		}else if (terms.getAttribute("pay_in_full_fin_roll_ind").equals("F")){
			if (terms.getAttribute("fin_roll_full_partial_ind").equals("F")){
				outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/SubTitle", "Finance in Finance Currency "+finRollCurr+" for full amount");
				
				outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/SubTitle", "Finance Terms: ");
				
				if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_number_of_days"))){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/Details", "Finance for "+terms.getAttribute("fin_roll_number_of_days")+" days");
				}else if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_maturity_date"))){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/Details", "At Fixed Maturity Date "
							+ TPDateTimeUtility.formatMessageDate(new Date(terms.getAttribute("fin_roll_maturity_date")), formLocale));
				}
			}else if(terms.getAttribute("fin_roll_full_partial_ind").equals("P")){
				if(terms.getAttribute("account_remit_ind").equals("A")){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/SubTitle", 
							"Finance in Finance Currency "+finRollCurr+" and pay partial payment amount of "+transCurrency+" "+finRollPartialPayAmt+" by debiting the following accounts:");
					
					addNonNullDocData(principal_acct_num, outputDoc, "/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForPrinciple");
					
					addNonNullDocData(charges_acct_num, outputDoc, "/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForCharges");
					
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/SubTitle", "Finance remaining balance at the following Finance Terms: ");
					
					if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_number_of_days"))){
						outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/Details", "Finance for "+terms.getAttribute("fin_roll_number_of_days")+" days");
					}else if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_maturity_date"))){
						outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/Details", "At Fixed Maturity Date "
								+ TPDateTimeUtility.formatMessageDate(new Date(terms.getAttribute("fin_roll_maturity_date")), formLocale));
					}
				}else if(terms.getAttribute("account_remit_ind").equals("R")){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Remitted", "We have remitted funds to your branch");
				}
			}
		}
		addNonNullDocData(terms.getAttribute("other_addl_instructions"), outputDoc, "/SectionD/Other");
	}
	
	//This method will get called if Settle Instruction is in RIFC: Rollover in Finance Currency
	protected void sectionDSettleInstrRIFC(DocumentHandler outputDoc) throws RemoteException, AmsException {
		String finRollCurr = "";
		if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_curr"))) {
			finRollCurr = terms.getAttribute("fin_roll_curr");
		}
		String finRollPartialPayAmt = "";
		if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_partial_pay_amt"))){
			finRollPartialPayAmt = terms.getAttribute("fin_roll_partial_pay_amt");
		}
		String transCurrency = "";
		if(StringFunction.isNotBlank(transaction.getAttribute("copy_of_currency_code"))){
			transCurrency = transaction.getAttribute("copy_of_currency_code");
		}
		
		String principal_acct_num = getAccountsDisplayValue(terms.getAttribute("principal_account_oid"));
		String charges_acct_num = getAccountsDisplayValue(terms.getAttribute("charges_account_oid"));
		String interest_acct_num = getAccountsDisplayValue(terms.getAttribute("interest_account_oid"));
		
		outputDoc.setAttribute( "/SectionD/PayDtls/SubTitle", "Please use the following instructions for settlement of the above item:");

		if(terms.getAttribute("pay_in_full_fin_roll_ind").equals("P")){
			outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/SubTitle", "Pay Full Amount (refer payment details directly below):");
			if(terms.getAttribute("account_remit_ind").equals("A")){
				outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/Account/SubTitle", "Debit the following Accounts: ");
				
				addNonNullDocData(principal_acct_num, outputDoc, "/SectionD/PayDtls/PayFull/Account/DebitAccForPrinciple");
				
				addNonNullDocData(interest_acct_num, outputDoc, "/SectionD/PayDtls/PayFull/Account/DebitAccForInterest");
				
				addNonNullDocData(charges_acct_num, outputDoc, "/SectionD/PayDtls/PayFull/Account/DebitAccForCharges");
			}else if(terms.getAttribute("account_remit_ind").equals("R")){
				outputDoc.setAttribute( "/SectionD/PayDtls/PayFull/Account/Remitted", "We have remitted funds to your branch");
			}
		}else if (terms.getAttribute("pay_in_full_fin_roll_ind").equals("R")){
			if (terms.getAttribute("fin_roll_full_partial_ind").equals("F")){
				outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/SubTitle", "Rollover in Finance Currency "+finRollCurr+" for full amount");
				
				outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/SubTitle", "Rollover Terms: ");
				
				if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_number_of_days"))){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/Details", "Rollover for "+terms.getAttribute("fin_roll_number_of_days")+" days");
				}else if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_maturity_date"))){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/FullAmt/Terms/Details", "At Fixed Maturity Date "
							+ TPDateTimeUtility.formatMessageDate(new Date(terms.getAttribute("fin_roll_maturity_date")), formLocale));
				}
			}else if(terms.getAttribute("fin_roll_full_partial_ind").equals("P")){
				if(terms.getAttribute("account_remit_ind").equals("A")){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/SubTitle", 
							"Rollover in Finance Currency "+finRollCurr+" and pay partial payment amount of "+transCurrency+" "+finRollPartialPayAmt+" by debiting the following accounts:");
					
					addNonNullDocData(principal_acct_num, outputDoc, "/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForPrinciple");
					
					addNonNullDocData(interest_acct_num, outputDoc, "/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForInterest");
					
					addNonNullDocData(charges_acct_num, outputDoc, "/SectionD/PayDtls/Rollover/PartialAmt/Account/DebitAccForCharges");
					
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/SubTitle", "Rollover remaining balance at the following Rollover Terms: ");
					
					if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_number_of_days"))){
						outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/Details", "Rollover for "+terms.getAttribute("fin_roll_number_of_days")+" days");
					}else if(StringFunction.isNotBlank(terms.getAttribute("fin_roll_maturity_date"))){
						outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Terms/Details", "At Fixed Maturity Date "
								+ TPDateTimeUtility.formatMessageDate(new Date(terms.getAttribute("fin_roll_maturity_date")), formLocale));
					}
				}else if(terms.getAttribute("account_remit_ind").equals("R")){
					outputDoc.setAttribute( "/SectionD/PayDtls/Rollover/PartialAmt/Remitted", "We have remitted funds to your branch");
				}
			}
		}
		addNonNullDocData(terms.getAttribute("other_addl_instructions"), outputDoc, "/SectionD/Other");
	}

	/**
	 * SectionE corresponds to the portion of the XML document labeled
	 * 'SectionE'. The following is an example of what the resulting Xml should
	 * look like: <SectionE><!-- Account Party and Advising/Corresponding Bank
	 * --> <AccountParty> <Name>Account Party Name</Name> <AddressLine1>Account
	 * Party Address Line 1</AddressLine1> <AddressLine2>Account Party Address
	 * Line 2</AddressLine2> <AddressLine3>Account Party Address Line
	 * 3</AddressLine3> <AddressStateProvince>Account Party City +
	 * Province</AddressStateProvince> <AddressCountryPostCode>Account Party
	 * Country + Post Code</AddressCountryPostCode> </AccountParty>
	 * <CorrespondentBank> <Name>Advising Bank Name</Name>
	 * <AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 * <AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 * <AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 * <AddressStateProvince>Advising Bank City +
	 * Province</AddressStateProvince> <AddressCountryPostCode>Advising Bank
	 * Country + Post Code</AddressCountryPostCode> </CorrespondentBank>
	 * <AdvisingBank> <Name>Advising Bank Name</Name> <AddressLine1>Advising
	 * Bank Address Line 1</AddressLine1> <AddressLine2>Advising Bank Address
	 * Line 2</AddressLine2> <AddressLine3>Advising Bank Address Line
	 * 3</AddressLine3> <AddressStateProvince>Advising Bank City +
	 * Province</AddressStateProvince> <AddressCountryPostCode>Advising Bank
	 * Country + Post Code</AddressCountryPostCode> </AdvisingBank> </SectionE>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionE(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		String thirdTermsPartyOid = terms.getAttribute("c_ThirdTermsParty");
		String fourthTermsPartyOid = terms.getAttribute("c_FourthTermsParty");
		String fifthTermsPartyOid = terms.getAttribute("c_FifthTermsParty");

		TermsParty bankParty;

		// Populate the Account Party with the operational org info if the
		// applicant matches the corporation who created the transaction
		if (!instrument.getAttribute("instrument_type_code").equals(
				InstrumentType.REQUEST_ADVISE)) {
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");

			// Krishna IR- FDUH121460387 09/01/2008 wrapped in if loop as -for
			// ATP there is no
			// concept of Account Party
			
			if (!instrument.getAttribute("instrument_type_code").equals(
					InstrumentType.APPROVAL_TO_PAY)) {
				if (!applicantParty.getAttribute("name").equals(
						corpOrg.getAttribute("name")))
					createPartyAddressXml(outputDoc, corpOrg, "SectionE",
							"Account Party");
			} else
				;
			// keep the cell blank

			if (isNotEmpty(thirdTermsPartyOid)) {
				bankParty = (TermsParty) terms
						.getComponentHandle("ThirdTermsParty");
				if ((bankParty.getAttribute("terms_party_type")
						.equals(TermsPartyType.ADVISING_BANK))
						&& (instrument.getAttribute("instrument_type_code")
								.equals(InstrumentType.STANDBY_LC)))
					createPartyAddressXml(outputDoc, bankParty, "SectionE",
							"Correspondent Bank");
				else if ((bankParty.getAttribute("terms_party_type")
						.equals(TermsPartyType.ADVISING_BANK))
						&& (instrument.getAttribute("instrument_type_code")
								.equals(InstrumentType.IMPORT_DLC)))
					createPartyAddressXml(outputDoc, bankParty, "SectionE",
							"Advising Bank");
				else if ((bankParty.getAttribute("terms_party_type")
						.equals(TermsPartyType.INSURING_PARTY))
						&& (instrument.getAttribute("instrument_type_code")
								.equals(InstrumentType.APPROVAL_TO_PAY)))
					createPartyAddressXml(outputDoc, bankParty, "SectionE",
							"Credit Insurer");
				else
					;
				// Second Cell is Blank

			} else
				;

		} else { // It is a request to advise, so put in the applicant bank here
					// if there is one
			if (isNotEmpty(thirdTermsPartyOid)) {
				bankParty = (TermsParty) terms
						.getComponentHandle("ThirdTermsParty");
				createPartyAddressXml(outputDoc, bankParty, "SectionE",
						"Applicant Bank");
			}

			if (isNotEmpty(fourthTermsPartyOid)) {
				TermsParty adviseThroughBank = (TermsParty) terms
						.getComponentHandle("FourthTermsParty");
				createPartyAddressXml(outputDoc, adviseThroughBank, "SectionE",
						"Advise Through Bank");
			}

			if (isNotEmpty(fifthTermsPartyOid)) {
				TermsParty reimbursingBank = (TermsParty) terms
						.getComponentHandle("FifthTermsParty");
				createPartyAddressXml(outputDoc, reimbursingBank, "SectionE",
						"Reimbursing Bank");
			}

		}

	}
	
	protected void sectionESettleInstr(DocumentHandler outputDoc) throws RemoteException, AmsException{
		//FX should be provided only iff account_remit_ind is set to A/ any account is selected for full/partial payment and
		// iff currency of principle and charges account# is not same

		//String principal_acct_cur = getAccountCurrency(terms.getAttribute("principal_account_oid"));
		//String charges_acct_cur = getAccountCurrency(terms.getAttribute("charges_account_oid"));
		
		//if(terms.getAttribute("account_remit_ind").equals("A") && !principal_acct_cur.equals(charges_acct_cur)){
		if(StringFunction.isNotBlank(terms.getAttribute("daily_rate_fec_oth_ind"))){
			outputDoc.setAttribute( "/SectionE/FXRates/Title", "FOREIGN EXCHANGE RATE DETAILS");
			outputDoc.setAttribute( "/SectionE/FXRates/SubTitle", "Please apply the following FX Details against the above settlement instructions:");
			
			if(terms.getAttribute("daily_rate_fec_oth_ind").equals("D")){
				outputDoc.setAttribute( "/SectionE/FXRates/ExRate", "Use Daily Exchange Rate");
			}
			
			if(terms.getAttribute("daily_rate_fec_oth_ind").equals("F")){
				outputDoc.setAttribute( "/SectionE/FXRates/FXContract/SubTitle", "Use FX Contract:");
				//outputDoc.setAttribute( "/SectionE/FXRates/FXContract/Num/Lable", "FX Contract Number:");
				addNonNullDocData(terms.getAttribute("settle_covered_by_fec_num"), outputDoc, "/SectionE/FXRates/FXContract/Num");
				//outputDoc.setAttribute( "/SectionE/FXRates/FXContract/Curr/Lable", "Currency:");
				addNonNullDocData(terms.getAttribute("settle_fec_currency"), outputDoc, "/SectionE/FXRates/FXContract/Curr");
				//outputDoc.setAttribute( "/SectionE/FXRates/FXContract/Rate/Lable", "Rate:");
				addNonNullDocData(terms.getAttribute("settle_fec_rate"), outputDoc, "/SectionE/FXRates/FXContract/Rate");
			}
			
			if(terms.getAttribute("daily_rate_fec_oth_ind").equals("O")){
				//outputDoc.setAttribute( "/SectionE/FXRates/Other/Lable", "Other FX Instructions:");
				addNonNullDocData(terms.getAttribute("settle_other_fec_text"), outputDoc, "/SectionE/FXRates/Other");
			}
		}
	}

	/**
	 * SectionF corresponds to the portion of the XML document labeled
	 * 'SectionF'. The following is an example of what the resulting Xml should
	 * look like: <SectionF><!-- Amount Detail and Expiration --> <AmountDetail>
	 * <CurrencyCodeAmount>USD 12345.01</CurrencyCodeAmount>
	 * <CurrencyValueAmountInWords>US Dollars Twelve Thousand Three Hundred
	 * Fourty Five and .01</CurrencyValueAmountInWords> <Tolerance>
	 * <Pos>23</Pos> <Neg>34</Neg> </Tolerance> </AmountDetail> <Expiration>
	 * <Date>23 December 2005</Date> <CharacteristicType>A 29 character Text
	 * Field</CharacteristicType> </Expiration> </SectionF>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionF(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		String currencyCode = null;
		String currencyAmount = null;
		String currencyCodeAndAmount = null;

		currencyCode = terms.getAttribute("amount_currency_code");
		currencyAmount = terms.getAttribute("amount");
		currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
				currencyCode, formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		outputDoc.setAttribute("/SectionF/AmountDetail/CurrencyCodeAmount/",
				currencyCodeAndAmount);

		if (isNotEmpty(currencyCode))
			currencyCode = refDataMgr.getDescr(
					TradePortalConstants.CURRENCY_CODE, currencyCode,
					formLocale);
		if (isNotEmpty(currencyAmount))
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount,
					formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		outputDoc.setAttribute(
				"/SectionF/AmountDetail/CurrencyValueAmountInWords/",
				currencyCodeAndAmount);

		String amountTolerance = null;
		String amountTolerancePos = terms.getAttribute("amt_tolerance_pos");
		String amountToleranceNeg = terms.getAttribute("amt_tolerance_neg");
		if (isNotEmpty(amountTolerancePos) && isNotEmpty(amountToleranceNeg))
			amountTolerance = "+ " + amountTolerancePos + "% / - "
					+ amountToleranceNeg + "%";
		else if (isNotEmpty(amountTolerancePos))
			amountTolerance = "+ " + amountTolerancePos + "%";
		else if (isNotEmpty(amountToleranceNeg))
			amountTolerance = "- " + amountToleranceNeg + "%";
		else
			;
		// Do Nothing

		addNonNullDocData(amountTolerance, outputDoc,
				"/SectionF/AmountDetail/Tolerance");

		String expiryDate = terms.getAttribute("expiry_date");
		if (isNotEmpty(expiryDate))
			addNonNullDocData(formatDate(terms, "expiry_date", formLocale),
					outputDoc, "/SectionF/Expiration/Date");

	}

	/**
	 * SectionG corresponds to the portion of the XML document labeled
	 * 'SectionG'. The following is an example of what the resulting Xml should
	 * look like: <SectionG><!-- Presentation Days -->
	 * <PresentationDays>24</PresentationDays> </SectionG>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionG(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		String presentDocsWithinDays = null;

		presentDocsWithinDays = terms.getAttribute("present_docs_within_days");
		if (isNotEmpty(presentDocsWithinDays))
			addNonNullDocData(presentDocsWithinDays, outputDoc,
					"/SectionG/PresentationDays/");
	}

	/**
	 * SectionH corresponds to the portion of the XML document labeled
	 * 'SectionH'. The following is an example of what the resulting Xml should
	 * look like: <SectionH><!--Payment Terms and Bank Charges -->
	 * <PaymentTerms> <Sight></Sight> <DaysAfter> <Days>29</Days>
	 * <After>Sight</After> </DaysAfter> <Other></Other> <For>89</For>
	 * <DrawnOnParty>Drawn On Party</DrawnOnParty> </PaymentTerms> <BankCharges>
	 * <AllOurAccount></AllOurAccount> <AllBankCharges></AllBankCharges>
	 * <Other></Other> </BankCharges> </SectionH>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionH(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		// Payment Terms
		String paymentTermsType = terms.getAttribute("pmt_terms_type");
		if (paymentTermsType.equals(TradePortalConstants.PMT_SIGHT))
			outputDoc.setAttribute("/SectionH/PaymentTerms/Sight/", "");
		if (paymentTermsType.equals(TradePortalConstants.PMT_DAYS_AFTER)) {
			String paymentTermsDaysAfterType = refDataMgr
					.getDescr(TradePortalConstants.PMT_TERMS_DAYS_AFTER,
							terms.getAttribute("pmt_terms_days_after_type"),
							formLocale);
			outputDoc.setAttribute("/SectionH/PaymentTerms/DaysAfter/Days",
					terms.getAttribute("pmt_terms_num_days_after"));
			outputDoc.setAttribute("/SectionH/PaymentTerms/DaysAfter/After",
					paymentTermsDaysAfterType);
		}
		if (paymentTermsType.equals(TradePortalConstants.PMT_OTHER))
			outputDoc.setAttribute("/SectionH/PaymentTerms/Other/", "");

		// Bank Charges
		String bankChargesType = terms.getAttribute("bank_charges_type");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT))
			outputDoc.setAttribute("/SectionH/BankCharges/AllOurAccount/", "");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE))
			outputDoc.setAttribute("/SectionH/BankCharges/AllBankCharges/", "");
		if (bankChargesType.equals(TradePortalConstants.CHARGE_OTHER))
			outputDoc.setAttribute("/SectionH/BankCharges/Other", "");
	}

	/**
	 * SectionI corresponds to the portion of the XML document labeled
	 * 'SectionI'. The following is an example of what the resulting Xml should
	 * look like: <SectionI><!-- Documents Required --> <comm_invoice>
	 * <Originals>2</Originals> or <Original>1</Original> <Copies>3</Copies> or
	 * <Copy>1</Copy> <Text>Commercial Invoice Text</Text> </comm_invoice>
	 * <packing_list> <Originals>7</Originals> <Copies>2</Copies> <Text>Packing
	 * List Text</Text> </packing_list> <cert_origin> <Original>1</Original>
	 * <Copies>2</Copies> <Text>Certificate of Origin Text. Here is some more
	 * text as well to test word wrapping and stuff. This is only a test. Thank
	 * you. You are a valued customer.</Text> </cert_origin> <ins_policy>
	 * <Originals>2</Originals> <Copies>3</Copies> <Plus>23</Plus>
	 * <Covering>Institute Cargo Clause (A)</Covering> <Text>Insurance Policy
	 * Text</Text> </ins_policy> <other_req_doc_1> <Original>1</Original>
	 * <Copy>1</Copy> <Name>Document Number 1</Name> <Text>Document 1 Text Text
	 * Text</Text> </other_req_doc_1> <other_req_doc_2> <Originals>3</Originals>
	 * <Copies>9</Copies> <Name>Document Number 2</Name> <Text>Document 2 Text
	 * Text Text</Text> </other_req_doc_2> <other_req_doc_3>
	 * <Originals>2</Originals> <Copies>5</Copies> <Name>Document Number
	 * 3</Name> <Text>Document 3 Text Text Text</Text> </other_req_doc_3>
	 * <other_req_doc_4> <Originals>8</Originals> <Copies>11</Copies>
	 * <Name>Document Number 4</Name> <Text>Document 4 Text Text Text</Text>
	 * </other_req_doc_4> </SectionI>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionI(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		copiesOriginalsText(outputDoc, "comm_invoice", "SectionI");
		copiesOriginalsText(outputDoc, "packing_list", "SectionI");
		copiesOriginalsText(outputDoc, "ins_policy", "SectionI");
		copiesOriginalsText(outputDoc, "cert_origin", "SectionI");
		copiesOriginalsText(outputDoc, "other_req_doc_1", "SectionI");
		copiesOriginalsText(outputDoc, "other_req_doc_2", "SectionI");
		copiesOriginalsText(outputDoc, "other_req_doc_3", "SectionI");
		copiesOriginalsText(outputDoc, "other_req_doc_4", "SectionI");
		copiesAdditionalDocsText(outputDoc, "addl_req_doc", "SectionI");
	}

	private void copiesAdditionalDocsText(DocumentHandler outputDoc,
			String documentName, String sectionName)
	throws RemoteException, AmsException
	{
		String originals = null;
		String copies = null;
		String text = null;

		String documentNameText = null;

		String insuranceEndorsePlusValue = null;
		String insuranceRiskType = null;
		StringBuffer sb = new StringBuffer("SELECT addl_req_doc_oid, addl_req_doc_ind, addl_req_doc_name, addl_req_doc_originals, ");
		sb.append(" addl_req_doc_copies, addl_req_doc_text from ADDTIONAL_REQ_DOCS ");
		sb.append(" where p_terms_oid = ?");
		sb.append(" order by addl_req_doc_oid");
		DocumentHandler childrenDoc = DatabaseQueryBean.getXmlResultSet(sb.toString(), false, new Object[]{terms.getAttribute("terms_oid")});

		Vector additionalReqDocList = new Vector();
		if (childrenDoc != null){
			additionalReqDocList = childrenDoc.getFragments("/ResultSetRecord");
		}
		DocumentHandler additionalReqDoc = null;
		
		for(int index = 0; index < additionalReqDocList.size() ; index++) {      
			
			additionalReqDoc = (DocumentHandler)additionalReqDocList.get(index);

			if( checkFlagValue( additionalReqDoc.getAttribute("/" + documentName.toUpperCase() +"_ind".toUpperCase()) ) ) {
				originals = additionalReqDoc.getAttribute("/" + documentName.toUpperCase() + "_originals".toUpperCase());
				if( isNotEmpty( originals ) && !originals.equals("0")) {
					if (originals.equals("1"))
						outputDoc.setAttribute( "/" + sectionName + "/" + documentName + "/addl_docs_" + index + "/Original", "1");
					else
						outputDoc.setAttribute( "/" + sectionName + "/" + documentName +"/addl_docs_" + index + "/Originals", originals);
				}
				copies = additionalReqDoc.getAttribute("/" +documentName.toUpperCase() + "_copies".toUpperCase());
				if( isNotEmpty( copies ) && !copies.equals("0")) {
					if (copies.equals("1"))
						outputDoc.setAttribute(  "/" + sectionName + "/" + documentName + "/addl_docs_" + index +"/Copy", "1");
					else
						outputDoc.setAttribute(  "/" + sectionName + "/" + documentName + "/addl_docs_" + index + "/Copies", copies);
				}
	
				if (documentName.substring(0,4).equals("addl")) {
					documentNameText = additionalReqDoc.getAttribute("/" + documentName.toUpperCase() + "_name".toUpperCase());
					if ( isNotEmpty(documentNameText))
						outputDoc.setAttribute( "/" + sectionName +"/" + documentName + "/addl_docs_" + index +"/Name", documentNameText);
				} else {
					outputDoc.setAttribute( "/" + sectionName +"/" + documentName + "/addl_docs_" + index + "/Name", "");
				}
	
				text = additionalReqDoc.getAttribute("/" + documentName.toUpperCase() + "_text".toUpperCase());
				if( isNotEmpty( text ))
					outputDoc.setAttribute(  "/" + sectionName + "/" + documentName + "/addl_docs_" + index + "/Text", text);
	
			}
		}
	}

	/**
	 * SectionJ corresponds to the portion of the XML document labeled
	 * 'SectionJ'. The following is an example of what the resulting Xml should
	 * look like: <SectionJ><!-- Additional Documents -->
	 * <AdditionalDocuments>Here is where the Additional Documents text should
	 * go. Not sure how much info we will need to display here. Just babbling
	 * away as i am not sure what to put here.</AdditionalDocuments> </SectionJ>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 * 
	 */

	protected void sectionJ(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		if (checkFlagValue(terms.getAttribute("addl_doc_indicator")))
			addNonNullDocData(terms.getAttribute("addl_doc_text"), outputDoc,
					"/SectionJ/AdditionalDocuments");
	}

	/**
	 * SectionK corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionK><!-- Transport Document --> <TransportDocument
	 * ID="1"> <TransDoc ID="1 of 2"> <TransShipDeclaration>Here is some
	 * Transport Document Shipment Declaration Text</TransShipDeclaration>
	 * <TransDocType>Air Transport Document</TransDocType>
	 * <Originals>3</Originals> <Copies>4</Copies>
	 * <ConsignedOrder>Applicant</ConsignedOrder> <ConsignedTo>Issuing
	 * Bank</ConsignedTo> <MarkedFreight>Prepaid</MarkedFreight> <NotifyParty>
	 * <Name>Notify Party Name</Name> <AddressLine1>Address Line
	 * 1</AddressLine1> <AddressLine2>Address Line 2</AddressLine2>
	 * <AddressLine3>Address Line 3</AddressLine3> </NotifyParty>
	 * <OtherConsignee> <Name>Other Consignee Party Name</Name>
	 * <AddressLine1>Address Line 1</AddressLine1> <AddressLine2>Address Line
	 * 2</AddressLine2> <AddressLine3>Address Line 3</AddressLine3>
	 * </OtherConsignee> <TransDocText>Here is some Transport Document text to
	 * test it out.</TransDocText> <AddlTransDocs>Here is some Additional
	 * Transport Documents 2/3</AddlTransDocs> </TransDoc> <ShipmentTerms><!--
	 * Shipment Terms --> <PartialShipment>permitted</PartialShipment>
	 * <TransShipment>not permitted</TransShipment> <ShipmentDate>11 December
	 * 2005</ShipmentDate> <Incoterm>Delivery Ex Quay (Duty Paid)
	 * Somewhere</Incoterm> <From>Place of Take Charge</From> <FromLoad>Port of
	 * Loading</FromLoad> <To>Place of Final Destination</To> <ToDischarge>Port
	 * of Discharge</ToDischarge> <!-- Goods Descriptions -->
	 * <GoodsDescription>Here is the goods Description if anybody is
	 * interested.</GoodsDescription> <!-- PO Line Items --> <POLineItems>List
	 * PO Items if any.</POLineItems> </TransportDocument> </SectionK>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */

	protected void sectionK(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		ComponentList shipmentTermsList = (ComponentList) terms
				.getComponentHandle("ShipmentTermsList");
		if (shipmentTermsList != null) {

			// Get the list of shipment terms and sort by creation date
			int numberShipmentTerms = shipmentTermsList.getObjectCount();
			Hashtable h = new Hashtable();
			Date creationDate = null;
			for (int i = 0; i < numberShipmentTerms; i++) {
				shipmentTermsList.scrollToObjectByIndex(i);
				creationDate = shipmentTermsList.getBusinessObject()
						.getAttributeDateTime("creation_date");
				h.put(creationDate, new Integer(i));
			}
			Vector v = new Vector(h.keySet());
			Collections.sort(v);

			int sortedIndex = 0;
			for (Iterator it = v.iterator(); it.hasNext(); sortedIndex++) {
				creationDate = (Date) it.next();
				int index = ((Integer) h.get(creationDate)).intValue();
				// Setup ID tag Values and retrieve each ShipmentTerms object
				String currentSection = "/SectionK/TransportDocument("
						+ String.valueOf(sortedIndex + 1) + ")/";
				String transDocSection = currentSection + "/TransDoc("
						+ String.valueOf(sortedIndex + 1) + " of "
						+ String.valueOf(numberShipmentTerms) + ")/";
				shipmentTermsList.scrollToObjectByIndex(index);
				ShipmentTerms transportDocument = (ShipmentTerms) shipmentTermsList
						.getBusinessObject();

				// TransShipDeclaration
				addNonNullDocData(
						transportDocument.getAttribute("description"),
						outputDoc, transDocSection + "/TransShipDeclaration/");
				// TransDocType
				String transDocType = transportDocument
						.getAttribute("trans_doc_type");
				if (isNotEmpty(transDocType))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.TRANS_DOC_TYPE, transDocType,
							formLocale), outputDoc, transDocSection
							+ "/TransDocType/");
				// Originals and Copies
				addNonNullDocData(
						transportDocument.getAttribute("trans_doc_originals"),
						outputDoc, transDocSection + "/Originals/");
				addNonNullDocData(
						transportDocument.getAttribute("trans_doc_copies"),
						outputDoc, transDocSection + "/Copies/");
				// ConsignedOrder and Consigned to
				String consignedOrder, consignedTo;
				consignedOrder = transportDocument
						.getAttribute("trans_doc_consign_order_of_pty");
				if (isNotEmpty(consignedOrder))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
							consignedOrder, formLocale), outputDoc,
							transDocSection + "/ConsignedOrder/");
				consignedTo = transportDocument
						.getAttribute("trans_doc_consign_to_pty");
				if (isNotEmpty(consignedTo))
					addNonNullDocData(refDataMgr.getDescr(
							TradePortalConstants.CONSIGNMENT_PARTY_TYPE,
							consignedTo, formLocale), outputDoc,
							transDocSection + "/ConsignedTo/");
				// Marked Freight
				String markedFreightType = transportDocument
						.getAttribute("trans_doc_marked_freight");
				if (markedFreightType.equals(TradePortalConstants.PREPAID))
					outputDoc.setAttribute(transDocSection + "/MarkedFreight/",
							"Prepaid");
				if (markedFreightType.equals(TradePortalConstants.COLLECT))
					outputDoc.setAttribute(transDocSection + "/MarkedFreight/",
							"Collect");

				// Notify and Other Consignee Party
				TermsParty notifyParty = null; // Notify Party
				String notifyPartyOid = null;
				TermsParty otherConsigneeParty = null; // Other Consignee Party
				String otherConsigneePartyOid = null;
				String name, addressLine1, addressLine2, addressLine3;

				notifyPartyOid = transportDocument
						.getAttribute("c_NotifyParty");
				if (isNotEmpty(notifyPartyOid)) {
					notifyParty = (TermsParty) transportDocument
							.getComponentHandle("NotifyParty");
					name = notifyParty.getAttribute("name");
					addNonNullDocData(name, outputDoc, transDocSection
							+ "/NotifyParty/Name");
					addressLine1 = notifyParty.getAttribute("address_line_1");
					addNonNullDocData(addressLine1, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine1");
					addressLine2 = notifyParty.getAttribute("address_line_2");
					addNonNullDocData(addressLine2, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine2");
					addressLine3 = notifyParty.getAttribute("address_line_3");
					addNonNullDocData(addressLine3, outputDoc, transDocSection
							+ "/NotifyParty/AddressLine3");
				}
				otherConsigneePartyOid = transportDocument
						.getAttribute("c_OtherConsigneeParty");
				if (isNotEmpty(otherConsigneePartyOid)) {
					otherConsigneeParty = (TermsParty) transportDocument
							.getComponentHandle("OtherConsigneeParty");
					name = otherConsigneeParty.getAttribute("name");
					addNonNullDocData(name, outputDoc, transDocSection
							+ "/OtherConsignee/Name");
					addressLine1 = otherConsigneeParty
							.getAttribute("address_line_1");
					addNonNullDocData(addressLine1, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine1");
					addressLine2 = otherConsigneeParty
							.getAttribute("address_line_2");
					addNonNullDocData(addressLine2, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine2");
					addressLine3 = otherConsigneeParty
							.getAttribute("address_line_3");
					addNonNullDocData(addressLine3, outputDoc, transDocSection
							+ "/OtherConsignee/AddressLine3");
				}

				// Transport Document Text
				String transDocText = transportDocument
						.getAttribute("trans_doc_text");
				addNonNullDocData(transDocText, outputDoc, transDocSection
						+ "/TransDocText");
				// Additional Transport Document Text
				String addlTransDocs = transportDocument
						.getAttribute("trans_addl_doc_included");
				if (addlTransDocs.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(transDocSection + "/AddlTransDocs/",
							transportDocument
									.getAttribute("trans_addl_doc_text"));

				// ****Shipment Terms****
				// Partial Shipment
				String partialShipment = terms
						.getAttribute("partial_shipment_allowed");
				if (partialShipment.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/PartialShipment/", "Permitted");
				else
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/PartialShipment/",
							"Not Permitted");
				// Transhipment
				String transShipment = transportDocument
						.getAttribute("transshipment_allowed");
				if (transShipment.equals(TradePortalConstants.INDICATOR_YES))
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/TransShipment/", "Permitted");
				else
					outputDoc.setAttribute(currentSection
							+ "/ShipmentTerms/TransShipment/", "Not Permitted");
				// Latest Shipment Date
				String latestShipmentDate = transportDocument
						.getAttribute("latest_shipment_date");
				if (isNotEmpty(latestShipmentDate))
					addNonNullDocData(
							formatDate(transportDocument,
									"latest_shipment_date", formLocale),
							outputDoc, currentSection
									+ "/ShipmentTerms/ShipmentDate");
				// Incoterm (Incoterm/)
				String incoterm = transportDocument.getAttribute("incoterm");
				if (isNotEmpty(incoterm))
					incoterm = refDataMgr
							.getDescr(TradePortalConstants.INCOTERM, incoterm,
									formLocale);
				String shippingTerm = transportDocument
						.getAttribute("incoterm_location");
				incoterm = appendAttributes(incoterm, shippingTerm, " ");
				addNonNullDocData(incoterm, outputDoc, currentSection
						+ "/ShipmentTerms/Incoterm");

				// Shipment From and To Fields
				String shipFrom = transportDocument
						.getAttribute("shipment_from");
				String shipLoad = transportDocument
						.getAttribute("shipment_from_loading");
				String shipTo = transportDocument.getAttribute("shipment_to");
				String shipDischarge = transportDocument
						.getAttribute("shipment_to_discharge");

				if (!isNotEmpty(shipLoad) && !isNotEmpty(shipTo)) {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipDischarge)) {
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipLoad) && !isNotEmpty(shipDischarge)) {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipTo)) {
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/To");
				} else {
					addNonNullDocData(shipFrom, outputDoc, currentSection
							+ "/ShipmentTerms/From");
					addNonNullDocData(shipLoad, outputDoc, currentSection
							+ "/ShipmentTerms/FromLoad");
					addNonNullDocData(shipTo, outputDoc, currentSection
							+ "/ShipmentTerms/To");
					addNonNullDocData(shipDischarge, outputDoc, currentSection
							+ "/ShipmentTerms/ToDischarge");
				}

				// Goods description
				addNonNullDocData(
						transportDocument.getAttribute("goods_description"),
						outputDoc, currentSection + "/GoodsDescription");

				// PO Line Items

				addNonNullDocData(
						transportDocument.getAttribute("po_line_items"),
						outputDoc, currentSection + "/POLineItems");

				String shipmentTermsOid = transportDocument
						.getAttribute("shipment_oid");
				addStructuredPOLineItems(shipmentTermsOid, outputDoc,
						currentSection);

			}

		}

	}

	protected void addStructuredPOLineItems(String shipmentTermsOid,
			DocumentHandler outputDoc, String currentSection)
			throws RemoteException, AmsException {
		StringBuilder sb = new StringBuilder(
				"SELECT PURCHASE_ORDER_NUM,TO_CHAR(ISSUE_DATE,'MM-dd-yyyy') ISSUE_DATE ,");
		sb.append("TO_CHAR(LATEST_SHIPMENT_DATE,'MM-dd-yyyy') LATEST_SHIPMENT_DATE, CURRENCY, ");
		sb.append("AMOUNT, SELLER_NAME FROM PURCHASE_ORDER WHERE A_SHIPMENT_OID = ? ");

		DocumentHandler resultsSetDoc = DatabaseQueryBean.getXmlResultSet(sb
				.toString(), false, new Object[]{shipmentTermsOid});
		if (resultsSetDoc != null) {
			outputDoc.addComponent(currentSection + "/PurchaseOrderLists",
					resultsSetDoc);
		}

	}

	/**
	 * SectionL corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionL><!-- Other Conditions --> <Confirmation>Advising
	 * bank to add their confirmation</Confirmation>
	 * <Transferable>Yes</Transferable> <Revolve>Yes</Revolve>
	 * <AutoExtend>No</AutoExtend> <AdditionalConditions>Additional Conditions
	 * Text goes here</AdditionalConditions> </SectionL>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionL(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		String bankString = null;
		String confirmationType = null;

		String instrumentType = instrument.getAttribute("instrument_type_code");
		if (instrumentType.equals(InstrumentType.STANDBY_LC))
			bankString = "Correspondent Bank";
		else
			bankString = "Advising Bank";

		// <Confirmation>
		confirmationType = terms.getAttribute("confirmation_type");
		if (confirmationType.equals(TradePortalConstants.CONFIRM_NOT_REQD))
			outputDoc.setAttribute("/SectionL/Confirmation/", bankString
					+ " not required to add confirmation");
		if (confirmationType.equals(TradePortalConstants.BANK_TO_ADD))
			outputDoc.setAttribute("/SectionL/Confirmation/", bankString
					+ " to add their confirmation");
		if (confirmationType.equals(TradePortalConstants.BANK_MAY_ADD))
			outputDoc
					.setAttribute(
							"/SectionL/Confirmation/",
							bankString
									+ " may add their confirmation subject to Beneficiary's approval");

		if (instrumentType.equals(InstrumentType.STANDBY_LC)) {
			// <AutoExtend>
			// String autoExtend = terms.getAttribute("auto_extend");
			// if (autoExtend.equals(TradePortalConstants.INDICATOR_YES))
			// outputDoc.setAttribute( "/SectionL/AutoExtend/", "Yes");
			// else
			// outputDoc.setAttribute( "/SectionL/AutoExtend/", "No");

		} else {
			// <Transferable>
			String transferrable = terms.getAttribute("transferrable");
			if (transferrable.equals(TradePortalConstants.INDICATOR_YES))
				outputDoc.setAttribute("/SectionL/Transferable/", "Yes");
			else
				outputDoc.setAttribute("/SectionL/Transferable/", "No");

			// <Revolve>
			String revolve = terms.getAttribute("revolve");
			if (revolve.equals(TradePortalConstants.INDICATOR_YES))
				outputDoc.setAttribute("/SectionL/Revolve/", "Yes");
			else
				outputDoc.setAttribute("/SectionL/Revolve/", "No");
		}

		if (
				instrumentType.equals(InstrumentType.IMPORT_DLC) ||
				instrumentType.equals(InstrumentType.STANDBY_LC) ||
				instrumentType.equals(InstrumentType.GUARANTEE) ||
				instrumentType.equals(InstrumentType.REQUEST_ADVISE)
			) {
			
			String value = terms.getAttribute("ucp_version_details_ind");
			if(StringFunction.isNotBlank(value) && value.equals(TradePortalConstants.INDICATOR_YES)){
				outputDoc.setAttribute( "/SectionL/ICCPublications/", "");
				outputDoc.setAttribute( "/SectionL/ICCApplicableRules/", "Yes");
				outputDoc.setAttribute( "/SectionL/ICCPVersion/", ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));
				
				value = terms.getAttribute("ucp_details");
				if(StringFunction.isNotBlank(value))
					outputDoc.setAttribute( "/SectionL/ICCPDetails/", value);
			}
		}
		
		// <AdditionalConditions>
		addNonNullDocData(terms.getAttribute("additional_conditions"),
				outputDoc, "/SectionL/AdditionalConditions/");

	}

	/**
	 * SectionN corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionN><!-- Amendment Details --> <IssueDate>11 December
	 * 2005</IssueDate> <LCCurrencyAmount>USD 12345.00</LCCurrencyAmount>
	 * <IncreaseBy>GBP 1234.12</IncreaseBy> <DecreaseBy>EUR 123.32</DecreaseBy>
	 * <NewCurrencyAmount>USD 4323.43</NewCurrencyAmount>
	 * <AmtTolerancePos>32</AmtTolerancePos>
	 * <AmtToleranceNeg>3</AmtToleranceNeg> <ExpiryDate>4 December
	 * 2003</ExpiryDate> </SectionN>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 * 
	 */
	protected void sectionN(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		String currencyCode = null;
		String currencyAmount = null;
		String currencyCodeAndAmount = null;
		BigDecimal offsetAmount = BigDecimal.ZERO;
		BigDecimal calculatedTotal = BigDecimal.ZERO;

		// Issue Date
		String issueDate = instrument.getAttribute("issue_date");
		if (isNotEmpty(issueDate))
			addNonNullDocData(formatDate(instrument, "issue_date", formLocale),
					outputDoc, "/SectionN/IssueDate");

		// LC Currency/Amount
		currencyCode = transaction.getAttribute("copy_of_currency_code");
		//SSikhakolli - Rel 9.3 IR#T36000038838 - 06/25/2015 - Begin
		if (!isNotEmpty(currencyCode)){
			currencyCode = transactionOriginal.getAttribute("copy_of_currency_code");
		}
		//SSikhakolli - Rel 9.3 IR#T36000038838 - 06/25/2015 - End	
		currencyAmount = transactionOriginal.getAttribute("instrument_amount");
		currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
				currencyCode, formLocale);
		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");
		
		if(!isExpandedFormRequest(instrument.getAttribute("instrument_type_code"))){
			outputDoc.setAttribute("/SectionN/LCCurrencyAmount",	currencyCodeAndAmount);
		}else{
			if (InstrumentType.GUARANTEE.equals(instrument.getAttribute("instrument_type_code"))) {
				outputDoc.setAttribute("/SectionN/GuaranteeCurrencyAmount",  currencyCodeAndAmount);
			}else if (InstrumentType.STANDBY_LC.equals(instrument.getAttribute("instrument_type_code"))||InstrumentType.IMPORT_DLC.equals(instrument.getAttribute("instrument_type_code"))) {
				outputDoc.setAttribute("/SectionN/LCCurrencyAmount",	currencyCodeAndAmount);
			}
		}
		

		// Increase/Decrease By
		currencyAmount = terms.getAttribute("amount");
		calculatedTotal = new BigDecimal(
				instrument.getAttribute("copy_of_instrument_amount"));
		if (isNotEmpty(currencyAmount)) {
			try {
				offsetAmount = new BigDecimal(currencyAmount);
			} catch (Exception e) {
				offsetAmount = BigDecimal.ZERO;
			}
			if (offsetAmount.doubleValue() < 0) {
				currencyAmount = TPCurrencyUtility
						.getDisplayAmount(offsetAmount.abs().toString(),
								currencyCode, formLocale);
				outputDoc.setAttribute("/SectionN/DecreaseBy", currencyCode
						+ " " + currencyAmount);
				offsetAmount.negate();
			} else {
				currencyAmount = TPCurrencyUtility
						.getDisplayAmount(offsetAmount.abs().toString(),
								currencyCode, formLocale);
				outputDoc.setAttribute("/SectionN/IncreaseBy", currencyCode
						+ " " + currencyAmount);
			}
			// VShah - 07/29/08 - IR#DEUH082437282 - ADD BEGIN
			// Add the below condition - Because the New LC Amount should not be
			// calculated by adding offsetAmount to it once the
			// transaction's status changes to PROCESSED BY BANK as it is
			// already been updated in COPY_OF_INSTRUMENT_AMOUNT field
			// by MW from where we are forming calculatedTotal.
			if (!(transaction.getAttribute("transaction_status")
					.equals(TransactionStatus.PROCESSED_BY_BANK))) {
				calculatedTotal = calculatedTotal.add(offsetAmount);
			}
		
		}
		
	
		// New LC Amount
		
		if (isNotEmpty(currencyAmount)) {
			currencyAmount = calculatedTotal.toString();
			currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
					currencyCode, formLocale);
			currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
					" ");

			if(!isExpandedFormRequest(instrument.getAttribute("instrument_type_code"))){
				outputDoc.setAttribute("/SectionN/NewCurrencyAmount",
						currencyCodeAndAmount);

			}else{
				if (InstrumentType.GUARANTEE.equals(instrument.getAttribute("instrument_type_code"))) {
					outputDoc.setAttribute("/SectionN/NewGuaranteeCurrencyAmount",  currencyCodeAndAmount);
				}else if (InstrumentType.STANDBY_LC.equals(instrument.getAttribute("instrument_type_code"))||InstrumentType.IMPORT_DLC.equals(instrument.getAttribute("instrument_type_code"))) {
					outputDoc.setAttribute("/SectionN/NewCurrencyAmount",  currencyCodeAndAmount);
				}
			}

		}
		
		
		String instrumentType = instrument.getAttribute("instrument_type_code");
		// Customizations Here
		if(!InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
				// New Amount Tolerance
				String amountTolerance = null;
				String amountTolerancePos = terms.getAttribute("amt_tolerance_pos");
				String amountToleranceNeg = terms.getAttribute("amt_tolerance_neg");
				if (isNotEmpty(amountTolerancePos) && isNotEmpty(amountToleranceNeg))
					amountTolerance = "+ " + amountTolerancePos + "% / - "
							+ amountToleranceNeg + "%";
				else if (isNotEmpty(amountTolerancePos))
					amountTolerance = "+ " + amountTolerancePos + "%";
				else if (isNotEmpty(amountToleranceNeg))
					amountTolerance = "- " + amountToleranceNeg + "%";
				else
					;
				addNonNullDocData(amountTolerance, outputDoc, "/SectionN/Tolerance");
				// New Expiry date
				String expiryDate = terms.getAttribute("expiry_date");
					if (isNotEmpty(expiryDate))
						outputDoc.setAttribute("/SectionN/ExpiryDate",
								formatDate(terms, "expiry_date", formLocale));				
		}
/*		// New Expiry date
		String expiryDate = terms.getAttribute("expiry_date");
		//if(!isExpandedFormRequest(instrument.getAttribute("instrument_type_code"))){
			if (isNotEmpty(expiryDate))
				outputDoc.setAttribute("/SectionN/ExpiryDate",
						formatDate(terms, "expiry_date", formLocale));
		//}
*//*		else{
			if(InstrumentType.IMPORT_DLC.equals(instrumentType) && TradePortalConstants.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
				if (isNotEmpty(expiryDate))
					outputDoc.setAttribute("/SectionN/ExpiryDate",
							formatDate(terms, "expiry_date", formLocale));
			}
		}*/
		
		

	}

	/**
	 * SectionO corresponds to the portion of the XML document labeled
	 * 'SectionK' The following is an example of what the resulting Xml should
	 * look like: <SectionO><!-- Account Party --> <Name>Applicant Bank Name
	 * Goes Here</Confirmation> </SectionO>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionO(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// Crazy SQL to get info...
		StringBuffer sqlQuery = new StringBuffer();
		String instrumentOid = instrument.getAttribute("instrument_oid");

		sqlQuery.append("select p.name")
				.append(" from terms_party p, terms t, transaction tr")
				.append(" where tr.transaction_oid =")
				.append("       (select max(transaction_oid)")
				.append("          from transaction")
				.append("         where transaction_status_date = (select max(transaction_status_date)")
				.append("                                            from transaction")
				.append("                                           where p_instrument_oid = ? ")
				.append("                                             and (transaction_type_code = '")
				.append(TransactionType.ISSUE)
				.append("' or transaction_type_code = '")
				.append(TransactionType.AMEND)
				.append("' or transaction_type_code = '")
				.append(TransactionType.CHANGE)
				.append("')")
				.append("                                             and transaction_status = '")
				.append(TransactionStatus.PROCESSED_BY_BANK)
				.append("')")
				.append("           and p_instrument_oid = ? ")
				.append("           and (transaction_type_code = '")
				.append(TransactionType.ISSUE)
				.append("' or transaction_type_code = '")
				.append(TransactionType.AMEND)
				.append("' or transaction_type_code = '")
				.append(TransactionType.CHANGE)
				.append("')")
				.append("           and transaction_status = '")
				.append(TransactionStatus.PROCESSED_BY_BANK)
				.append("')")
				.append(" and tr.c_bank_release_terms_oid = t.terms_oid")
				.append(" and (p.terms_party_oid = t.c_first_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_second_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_third_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_fourth_terms_party_oid")
				.append("      or p.terms_party_oid = t.c_fifth_terms_party_oid)")
				.append(" and p.terms_party_type = '")
				.append(TermsPartyType.APPLICANT).append("'");

		DocumentHandler sqlQueryResult = DatabaseQueryBean.getXmlResultSet(
				sqlQuery.toString(), false, new Object[]{instrumentOid, instrumentOid});
		
		if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			outputDoc.setAttribute("/SectionO/Label/","Applicant:");
		} else {
			outputDoc.setAttribute("/SectionO/Label/","Account Party:");
		}
		if (sqlQueryResult != null)
			outputDoc.setAttribute("/SectionO/Name/",
					sqlQueryResult.getAttribute("/ResultSetRecord(0)/NAME"));		

	}

	/**
	 * SectionP corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionP><!-- New Shipment Information --> <ShipmentDate>11
	 * December 2005</ShipmentDate> <Incoterm>Delivery Ex Quay (Duty Paid)
	 * Somewhere</Incoterm> <From>Place of Take Charge</From> <FromLoad>Port of
	 * Loading</FromLoad> <To>Place of Final Destination</To> <ToDischarge>Port
	 * of Discharge</ToDischarge> </SectionP>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionP(DocumentHandler outputDoc) throws RemoteException,
			AmsException {

		// Latest Shipment Date
		String shipmentDate = terms.getFirstShipment().getAttribute(
				"latest_shipment_date");
		if (isNotEmpty(shipmentDate))
			addNonNullDocData(
					formatDate(terms.getFirstShipment(),
							"latest_shipment_date", formLocale), outputDoc,
					"/SectionP/ShipmentDate");

		// Incoterm (Incoterm/)
		String incoterm = terms.getFirstShipment().getAttribute("incoterm");
		if (isNotEmpty(incoterm))
			incoterm = refDataMgr.getDescr(TradePortalConstants.INCOTERM,
					incoterm, formLocale);
		String shippingTerm = terms.getFirstShipment().getAttribute(
				"incoterm_location");
		incoterm = appendAttributes(incoterm, shippingTerm, " ");
		addNonNullDocData(incoterm, outputDoc, "/SectionP/Incoterm");

		// Shipment From and To Fields
		String shipFrom = terms.getFirstShipment()
				.getAttribute("shipment_from");
		String shipLoad = terms.getFirstShipment().getAttribute(
				"shipment_from_loading");
		String shipTo = terms.getFirstShipment().getAttribute("shipment_to");
		String shipDischarge = terms.getFirstShipment().getAttribute(
				"shipment_to_discharge");

		if (!isNotEmpty(shipLoad) && !isNotEmpty(shipTo)) {
			addNonNullDocData(shipFrom, outputDoc, "/SectionP/From");
			addNonNullDocData(shipDischarge, outputDoc, "/SectionP/To");
		} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipDischarge)) {
			addNonNullDocData(shipLoad, outputDoc, "/SectionP/From");
			addNonNullDocData(shipTo, outputDoc, "/SectionP/To");
		} else if (!isNotEmpty(shipLoad) && !isNotEmpty(shipDischarge)) {
			addNonNullDocData(shipFrom, outputDoc, "/SectionP/From");
			addNonNullDocData(shipTo, outputDoc, "/SectionP/To");
		} else if (!isNotEmpty(shipFrom) && !isNotEmpty(shipTo)) {
			addNonNullDocData(shipLoad, outputDoc, "/SectionP/From");
			addNonNullDocData(shipDischarge, outputDoc, "/SectionP/To");
		} else {
			addNonNullDocData(shipFrom, outputDoc, "/SectionP/From");
			addNonNullDocData(shipLoad, outputDoc, "/SectionP/FromLoad");
			addNonNullDocData(shipTo, outputDoc, "/SectionP/To");
			addNonNullDocData(shipDischarge, outputDoc, "/SectionP/ToDischarge");
		}

	}

	/**
	 * SectionQ corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionQ><!-- Goods Description --> <GoodsDescription>Goods
	 * Description</GoodsDescription> </SectionQ>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 * 
	 */
	protected void sectionQ(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// <Goods Description>
		addNonNullDocData(
				terms.getFirstShipment().getAttribute("goods_description"),
				outputDoc, "/SectionQ/GoodsDescription/");

		// PO Line Items
		addNonNullDocData(terms.getFirstShipment()
				.getAttribute("po_line_items"), outputDoc,
				"/SectionQ/POLineItems/");

		String shipmentTermsOid = terms.getFirstShipment().getAttribute(
				"shipment_oid");
		addStructuredPOLineItems(shipmentTermsOid, outputDoc, "/SectionQ");

	}

	/**
	 * SectionR corresponds to the portion of the XML document labeled
	 * 'SectionK'. The following is an example of what the resulting Xml should
	 * look like: <SectionR><!-- Account Party -->
	 * <AdditionalConditions>Additional Conditions go here</Confirmation>
	 * </SectionR>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 */
	protected void sectionR(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		// <AdditionalConditions>
		addNonNullDocData(terms.getAttribute("additional_conditions"),
				outputDoc, "/SectionR/AdditionalConditions/");
		
	}

	/**
	 * This method simply checks to see if a string value is not empty
	 * 
	 * @param value
	 *            - the string value to check for emptiness
	 * 
	 * @return boolean - whether the string is empty
	 */
	protected static boolean isNotEmpty(String value) {
		if ((value == null) || (value.equals("")))
			return false;
		else
			return true;
	}

	/**
	 * This method simply creates a delimiter seperated string between 2
	 * attributes. This was necessary since the logic gets executed 6 times. the
	 * parameter value is what will be built and returned to the caller.
	 * 
	 * @param Attribute
	 *            #1 - the first attribute to compare and get data from.
	 * @param Attribute
	 *            #2 - the second attribute to compare and get data from.
	 * @param Delimiter
	 *            - this is how the two attributes will be seperated in one
	 *            string.
	 * 
	 * @return Value - the resulting string is returned.
	 */
	protected static String appendAttributes(String attr1, String attr2,
			String delimiter) throws RemoteException, AmsException {

		String value = null;

		if (isNotEmpty(attr1))
			value = attr1.substring(0, attr1.length());

		if (isNotEmpty(attr2)) {
			if (value != null)
				value = value + delimiter + attr2.substring(0, attr2.length());
			else
				value = attr2;
		}

		return value;

	}

	/**
	 * This method simply sets a attribute up on the passed in DocumentHandler
	 * based on the current transaction type. This was localized because it's
	 * called more than once. It can be genericized further by passing in a XML
	 * value to be assigned. Right Now all that is assigned is literally "".
	 * 
	 * @param Output
	 *            doc - to get the associated data from the db with...
	 * @param BaseXmlPath
	 *            - the Base Path the xml needs to follow.
	 * @param Destination
	 *            #1 - the first possible xml destination.
	 * @param Destination
	 *            #2 - the second possible xml destination.
	 * @param Destination
	 *            #3 - the third possible xml destination.
	 * @param Value
	 *            - The value to be sent to the Xml Doc.
	 * 
	 */
	protected void appendXmlAttribute(DocumentHandler outputDoc,
			String transactionType, String basePath, String dest1,
			String dest2, String dest3, String value) throws RemoteException,
			AmsException {

		// current Transaction is: 'Issue'
		if (transactionType.equals(TransactionType.ISSUE)) {
			outputDoc.setAttribute(basePath + dest1, value);

			// current Transaction is: 'Amendment'
		} else if (transactionType.equals(TransactionType.AMEND)) {
			outputDoc.setAttribute(basePath + dest2, value);

			// current Transaction is: 'Tracer'
		} else if (transactionType.equals(TransactionType.TRACER)) {
			outputDoc.setAttribute(basePath + dest3, value);
		}

	}

	/**
	 * This method checks an attribute of a business Object, and if it's found
	 * that it has an actual piece of data, then it's added to the passed in
	 * Vector.
	 * 
	 * Adding data to a Vector after a null check.
	 * 
	 * @param Attribute
	 *            - the actual attribute in the B.O. to be compared.
	 * @param Vector
	 *            - Add the data into this vector if it's not null.
	 * 
	 * @return Vector - send out the resulting vector
	 */
	protected static Vector addNonNullElement(String attr, Vector vec)
			throws RemoteException, AmsException {
		if (isNotEmpty(attr)) {
			vec.addElement(attr);
		}

		return vec;
	}

	/**
	 * This method checks an attribute of a business Object, and if it's found
	 * that it has an actual piece of data, then it's added to the passed in
	 * XmlDocument (doc handler).
	 * 
	 * Adding data to an xml doc after a null check.
	 * 
	 * @param Attribute
	 *            - the actual attribute in the B.O. to be compared.
	 * @param DocumentHandler
	 *            - This is the Xml Document the data will get put into.
	 * @param XmlPath
	 *            - This is the path to store the data in the Xml document.
	 */
	protected static void addNonNullDocData(String attr,
			DocumentHandler outputDoc, String xmlPath) throws RemoteException,
			AmsException {

		if (isNotEmpty(attr)) {
			outputDoc.setAttribute(xmlPath, attr);
		}

	}

	/**
	 * checkFlagValue gets the passed in Business Object to check the Db for a
	 * stored specified value. If that value is NOT null, empty and has a value
	 * of 'Y' then return a positive value, else it will return false.
	 * 
	 * @param Value
	 *            - This is the value that needs to be checked against the
	 *            TradePortalConstants.INDICATOR_YES value.
	 * 
	 * @return boolean flag = false;
	 */
	protected boolean checkFlagValue(String value) throws RemoteException,
			AmsException {
		boolean flag = false;

		// If the checked value is NOT NULL NOR empty AND it is 'Yes' return a
		// 'true'
		if ((value != null)
				&& (value.equals(TradePortalConstants.INDICATOR_YES)))
			flag = true;

		return flag;
	}

	/**
	 * This method helps format the date data in a specific format : dd MONTH
	 * yyyyy. This method needs to be moved to TPDateUtility.
	 * 
	 * @param BusinessObject
	 *            - This is the Business Object used to check the check box
	 *            value against.
	 * @param Attribute
	 *            - This is the specific attribute that represents the
	 * @param locale
	 *            - The current locale.
	 * 
	 * @return FormattedDate - The string value of the formatted date..If there
	 *         was a null in the object(ie db), then an empty string is
	 *         returned.
	 */
	protected String formatDate(TradePortalBusinessObject busObj, String attr,
			String locale) throws RemoteException, AmsException {
		// First get the data from the db, then format the date and send back
		// the resulting string.

		Date date = busObj.getAttributeDate(attr);
		if (date != null) {
			Locale myLocale = new Locale(locale.substring(0, 2),
					locale.substring(3, 5));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMM yyyy",
					myLocale);
			return (formatter.format(date));

		} else
			return "";

	}

	//overloaded - input as string when busobj not avaiable
	protected String formatDate(String aDate,String locale) throws RemoteException, AmsException, ParseException {	
		String formattedDate = "";
		if(StringFunction.isNotBlank(aDate)){
			Date date = DateTimeUtility.convertStringDateTimeToDate(aDate);
			if (date != null) {
				Locale myLocale = new Locale(locale.substring(0, 2),
						locale.substring(3, 5));
				SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMM yyyy",
						myLocale);
				formattedDate = (formatter.format(date));
	
			} 
		}
			return formattedDate;

	}	
	
	/**
	 * This method is meant to build the /Copy /Copies /Original /Originals
	 * /Text for Required Documents on Terms
	 * 
	 * @param DocumentHandler
	 *            - This is the Xml Document the data will get put into.
	 * @param BusinessObject
	 *            - This is the terms object.
	 * @param RefDataMgr
	 *            - This is the Reference Data manager so that we can convert
	 *            the code in the in the refdata table to it's corresponding
	 *            description.
	 * @param documentName
	 *            - This is the name of the Required Document to build
	 * @param sectionName
	 *            - This is the section of the XML document.
	 * @param locale
	 *            - This is the users current Locale.
	 */
	protected void copiesOriginalsText(DocumentHandler outputDoc,
			String documentName, String sectionName) throws RemoteException,
			AmsException {

		String originals = null;
		String copies = null;
		String text = null;

		String documentNameText = null;

		String insuranceEndorsePlusValue = null;
		String insuranceRiskType = null;

		if (checkFlagValue(terms.getAttribute(documentName + "_indicator"))) {
			originals = terms.getAttribute(documentName + "_originals");
			if (isNotEmpty(originals) && !originals.equals("0")) {
				if (originals.equals("1"))
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Original", "1");
				else
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Originals", originals);
			}
			copies = terms.getAttribute(documentName + "_copies");
			if (isNotEmpty(copies) && !copies.equals("0")) {
				if (copies.equals("1"))
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Copy", "1");
				else
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Copies", copies);
			}

			if (documentName.equals("ins_policy")) {
				insuranceEndorsePlusValue = terms
						.getAttribute("insurance_endorse_value_plus");
				if (isNotEmpty(insuranceEndorsePlusValue))
					outputDoc
							.setAttribute("/" + sectionName + "/"
									+ documentName + "/Plus",
									insuranceEndorsePlusValue);
				insuranceRiskType = refDataMgr.getDescr(
						TradePortalConstants.INSURANCE_RISK_TYPE,
						terms.getAttribute("insurance_risk_type"), formLocale);
				if (isNotEmpty(insuranceRiskType))
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Covering", insuranceRiskType);
			}

			if (documentName.substring(0, 5).equals("other")) {
				documentNameText = terms.getAttribute(documentName + "_name");
				if (isNotEmpty(documentNameText))
					outputDoc.setAttribute("/" + sectionName + "/"
							+ documentName + "/Name", documentNameText);
			} else {
				outputDoc.setAttribute("/" + sectionName + "/" + documentName
						+ "/Name", "");
			}

			text = terms.getAttribute(documentName + "_text");
			if (isNotEmpty(text))
				outputDoc.setAttribute("/" + sectionName + "/" + documentName
						+ "/Text", text);

		}

	}

	/**
	 * This method is meant simply to test an object to see if it needs to be
	 * removed, If so, then we try to remove it. If we get an exception, It
	 * would be probably Because we removed a parent of a component when we
	 * tried to remove the component object.
	 * 
	 * @param BusinessObject
	 *            - The object we're trying to remove.
	 * @param Object
	 *            Type - This is the type of object being sent out in the error
	 *            message.
	 */
	private void beanRemove(TradePortalBusinessObject busObj, String objectType) {
		if (busObj != null) {
			try {
				busObj.remove();
			} catch (Exception e) {
				LOG.info("Failed in removal of " + objectType
						+ " Bean: Export Collection Class");
				System.out
						.println("This could be because this object is a component of a parent object previously removed.");
			}
		}
	}

	/**
	 * This function will populate a party address <Party
	 * ID=Applicant/Beneficiary
	 * /ApplicantBank/AdviseThroughBank/Issuer/ReimbursingBank
	 * /CorrespondingBank/Overseas> <Name>Name</Name> <AddressLine1>Addr Line
	 * 1</AddressLine1> <AddressLine2>Addr Line 2</AddressLine2>
	 * <AddressStateProvince>City + Province</AddressStateProvince>
	 * <AddressCountryPostCode>Country + Post Code</AddressCountryPostCode>
	 * 
	 * </Party>
	 * 
	 * @param Output
	 *            Document - This is the Xml document being built to be sent out
	 *            for display.
	 * @param TradePortalBusinessObject
	 *            Party - Terms Party to retrieve address information
	 * @param String
	 *            sectionName - Name of section to insert party address
	 *            information into
	 * @param String
	 *            partyName - Name of Party for the ID XML tag
	 */
	protected void createPartyAddressXml(DocumentHandler outputDoc,
			TradePortalBusinessObject party, String sectionName,
			String partyName) throws RemoteException, AmsException {

		String sectionAndParty = "/" + sectionName + "/Party(" + partyName
				+ ")";

		String cityState = "";
		String countryZip = "";
		String countryDescr = "";

		addNonNullDocData(partyName, outputDoc, sectionAndParty + "/Label/");
		addNonNullDocData(party.getAttribute("name"), outputDoc,
				sectionAndParty + "/Name/");
		addNonNullDocData(party.getAttribute("address_line_1"), outputDoc,
				sectionAndParty + "/AddressLine1/");
		addNonNullDocData(party.getAttribute("address_line_2"), outputDoc,
				sectionAndParty + "/AddressLine2/");
		cityState = appendAttributes(party.getAttribute("address_city"),
				party.getAttribute("address_state_province"), ", ");
		addNonNullDocData(cityState, outputDoc, sectionAndParty
				+ "/AddressStateProvince/");
		if (refDataMgr.checkCode(TradePortalConstants.COUNTRY,
				party.getAttribute("address_country"), formLocale))
			countryDescr = refDataMgr.getDescr(TradePortalConstants.COUNTRY,
					party.getAttribute("address_country"), formLocale);
		else
			countryDescr = "";
		countryZip = appendAttributes(countryDescr,
				party.getAttribute("address_postal_code"), ", ");
		addNonNullDocData(countryZip, outputDoc, sectionAndParty
				+ "/AddressCountryPostCode/");

	}

	protected void setPaymentTenorTerms(DocumentHandler outputDoc) {
		try {
			String percentAmountRadio = terms
					.getAttribute("percent_amount_dis_ind");
			String currencyCode = terms.getAttribute("amount_currency_code");
			String termsAmount = terms.getAttribute("amount");
			String termsPmtType = terms.getAttribute("payment_type");
			String specialTenorTermsText = terms
					.getAttribute("special_tenor_text");
			SimpleDateFormat isoDateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss.S");
			if (TradePortalConstants.SPECIAL.equals(termsPmtType)) {
				if (StringFunction.isNotBlank(specialTenorTermsText))
					outputDoc.setAttribute(
							"/SectionH/PaymentTerms/PmtTenorDetails",
							specialTenorTermsText);

				return;
			}
			ComponentList pmtTermsTenorDtlList = (ComponentList) terms
					.getComponentHandle("PmtTermsTenorDtlList");
			PmtTermsTenorDtl pmtTermsTenorDtl = null;
			StringBuilder outputString = new StringBuilder();
			for (int index = 0; index < pmtTermsTenorDtlList.getObjectCount(); index++) {
				pmtTermsTenorDtlList.scrollToObjectByIndex(index);
				pmtTermsTenorDtl = (PmtTermsTenorDtl) pmtTermsTenorDtlList
						.getComponentObject(Long.parseLong(pmtTermsTenorDtlList
								.getListAttribute(pmtTermsTenorDtlList
										.getIDAttributeName())));
				String tenorType = pmtTermsTenorDtl.getAttribute("tenor_type");
				String daysAfterType = pmtTermsTenorDtl
						.getAttribute("days_after_type");
				String maturityDate = pmtTermsTenorDtl
						.getAttribute("maturity_date");
				String numberOfDaysAfter = pmtTermsTenorDtl
						.getAttribute("num_days_after");
				String tenorAmount = pmtTermsTenorDtl.getAttribute("amount");
				String tenorPercent = pmtTermsTenorDtl.getAttribute("percent");

				String tempAmountStr = "";
				if (percentAmountRadio.equalsIgnoreCase("A")) {
					tempAmountStr = TPCurrencyUtility.getDisplayAmount(
							tenorAmount, currencyCode, formLocale);

				} else if (percentAmountRadio.equalsIgnoreCase("P")) {
					BigDecimal finPCTValue = BigDecimal.ZERO;
					if ((tenorPercent != null) && (!tenorPercent.equals(""))) {
						finPCTValue = (new BigDecimal(tenorPercent))
								.divide(new BigDecimal(100.00));
					}
					if (finPCTValue.compareTo(BigDecimal.ZERO) != 0) {
						BigDecimal tempAmount = BigDecimal.ZERO;
						BigDecimal tenorAmountTmp = terms
								.getAttributeDecimal("amount");
						tempAmount = finPCTValue.multiply(tenorAmountTmp);
						String trnxCurrencyCode = transaction
								.getAttribute("copy_of_currency_code");
						int numberOfCurrencyPlaces = 2;

						if (trnxCurrencyCode != null
								&& !trnxCurrencyCode.equals("")) {
							numberOfCurrencyPlaces = Integer
									.parseInt(ReferenceDataManager
											.getRefDataMgr()
											.getAdditionalValue(
													TradePortalConstants.CURRENCY_CODE,
													trnxCurrencyCode));
						}
						tempAmount = tempAmount.setScale(
								numberOfCurrencyPlaces,
								BigDecimal.ROUND_HALF_UP);
						tempAmountStr = TPCurrencyUtility
								.getDisplayAmount(tempAmount.toString(),
										currencyCode, formLocale);
					}

				}
				outputString.append(currencyCode);
				outputString.append(" ");
				outputString.append(tempAmountStr);
				if (TradePortalConstants.PMT_SIGHT.equals(tenorType)) {
					if (formLocale.equals("fr_CA")) {
						outputString.append(" ");
					}
					else{
						outputString.append(" at ");						
					}
				} else {
					outputString.append(" by ");
				}
				outputString.append(refDataMgr.getDescr(
						TradePortalConstants.TENOR, tenorType, formLocale));
				outputString.append(" ");
				if (StringFunction.isNotBlank(daysAfterType)) {
					outputString.append(numberOfDaysAfter);
					outputString.append(" days after ");
					outputString.append(refDataMgr.getDescr(
							TradePortalConstants.PMT_TERMS_DAYS_AFTER,
							daysAfterType, formLocale));
				}
				if (StringFunction.isNotBlank(maturityDate)) {
					outputString
							.append(" at fixed maturity. Maturity date of ");
					String date = pmtTermsTenorDtl
							.getAttribute("maturity_date");
					if (StringFunction.isNotBlank(date)) {
						date = isoDateFormat.format(pmtTermsTenorDtl
								.getAttributeDate("maturity_date"));
						date = TPDateTimeUtility.formatDate(date,
								TPDateTimeUtility.SHORT, formLocale);
					}
					outputString.append(date);

				}
				outputString.append("");
				outputString.append(System.getProperty("line.separator"));
			}
			if (StringFunction.isNotBlank(outputString.toString())) {
				outputDoc.setAttribute(
						"/SectionH/PaymentTerms/PmtTenorDetails",
						outputString.toString());
			}

		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AmsException e) {
			e.printStackTrace();
		}
	}

	protected void setAutoExtTerms(DocumentHandler outputDoc) {

		try {
			String autoExtInd = terms.getAttribute("auto_extension_ind");

			if (TradePortalConstants.INDICATOR_YES.equals(autoExtInd)) {
				
				String maxAutoExtAllowed = terms.getAttribute("max_auto_extension_allowed");
				String autoExtPeriod     = terms.getAttribute("auto_extension_period");
				String autoExtDays       = terms.getAttribute("auto_extension_days");
				String notifyBeneDays    = terms.getAttribute("notify_bene_days");
				
				outputDoc.setAttribute("/SectionL/AutoExtension/ExtAllowed", "Yes");
				outputDoc.setAttribute("/SectionL/AutoExtension/MaxExtAllowed", maxAutoExtAllowed);
				outputDoc.setAttribute("/SectionL/AutoExtension/AutoExtPeriod",
						refDataMgr.getDescr(TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, autoExtPeriod, formLocale));
				outputDoc.setAttribute("/SectionL/AutoExtension/AutoExtDays", autoExtDays);
				outputDoc.setAttribute("/SectionL/AutoExtension/FinalExpiryDate", formatDate(terms, "final_expiry_date", formLocale));
				outputDoc.setAttribute("/SectionL/AutoExtension/NotifyBeneDays", notifyBeneDays);

			}

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AmsException e) {
			e.printStackTrace();
		}

	}
	//
	//This function is used to determine if a given PDF generation request should include expanded fields (i.e expanded format)
	//or not. It looks up the value from the Bank Group object for the given user. 
	//The bank group has this information based on the Instrument Type.
	protected Boolean isExpandedFormRequest(String InstrType) throws RemoteException, AmsException{

		if(InstrumentType.AIR_WAYBILL.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("airway_bill_pdf_type"))){
				return true;
			}
		}else if(InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("atp_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.EXPORT_COL.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("exp_col_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.NEW_EXPORT_COL.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("exp_oco_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.IMPORT_DLC.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("imp_dlc_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.LOAN_RQST.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("lrq_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.GUARANTEE.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("gua_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.STANDBY_LC.equalsIgnoreCase(InstrType) || InstrumentType.INCOMING_SLC.equalsIgnoreCase(InstrType)  ){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("slc_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.REQUEST_ADVISE.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("rqa_pdf_type"))){
				return true;
			}			
		}else if(InstrumentType.SHIP_GUAR.equalsIgnoreCase(InstrType)){
			if( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(bankGroup.getAttribute("shp_pdf_type"))){
				return true;
			}			
		}

		return false;
		
	}
	
	//Will return true if the customer is not intergrated with TPS
	protected boolean isCustomerNotIntegratedWithTPS() throws RemoteException, AmsException{
		if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("cust_is_not_intg_tps"))){
			return true;
		}		
		return false;
	}
	
	
	/*
	 * This method checks if any value exists under Settlement Instructions and 
	 * Commissions and Charges to add "Settlement Instructions" heading for the data.
	 * 
	 */
	
	protected void checkForSettlementInstructions(DocumentHandler outputDoc) 	throws RemoteException, AmsException	{
		boolean  dataExists = false;

		if(isNotEmpty(terms.getAttribute("settlement_our_account_num")) || isNotEmpty(terms.getAttribute("branch_code")) || isNotEmpty(terms.getAttribute("settlement_foreign_acct_num"))  || isNotEmpty(terms.getAttribute("settlement_foreign_acct_curr"))){
			dataExists = true;
		}
		if(!dataExists){
			if(isNotEmpty(terms.getAttribute("coms_chrgs_our_account_num")) || isNotEmpty(terms.getAttribute("coms_chrgs_foreign_acct_num"))  || isNotEmpty(terms.getAttribute("coms_chrgs_foreign_acct_curr")) )
				dataExists = true;
		}
		if(InstrumentType.IMPORT_DLC.equals(instrument.getAttribute("instrument_type_code"))){

			if(!dataExists){
				if(isNotEmpty(terms.getAttribute("covered_by_fec_number")) || isNotEmpty(terms.getAttribute("fec_rate"))  || isNotEmpty(terms.getAttribute("fec_amount")) || isNotEmpty(terms.getAttribute("fec_maturity_date")) )
					dataExists = true;
			}
			if(!dataExists){
				if(isNotEmpty(terms.getAttribute("finance_drawing_num_days")) || isNotEmpty(terms.getAttribute("finance_drawing_type"))  || TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("finance_drawing")) )
					dataExists = true;
			}	
		}
		if(!dataExists){
			if(isNotEmpty(terms.getAttribute("coms_chrgs_other_text")))
				dataExists = true;
		}


		if(dataExists)
			outputDoc.setAttribute("/SectionT2/SettlementInstructions/Heading", "Settlement Instructions: ");


	}
	

}