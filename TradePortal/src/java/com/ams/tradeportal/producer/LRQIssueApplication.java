package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.TermsParty;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: LRQIssueApplication.xsl.
 */

public class LRQIssueApplication extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(LRQIssueApplication.class);
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);
			//Section 'D' -- Applicant and Beneficiary
			sectionD1( outputDoc);
			//Section 'E' -- Account Party and Advising Bank
			sectionE( outputDoc);
			sectionG( outputDoc);
		    sectionH1( outputDoc);
			//sectionH2( outputDoc);
		    sectionH3( outputDoc);
			sectionI( outputDoc,locale);
		    sectionT( outputDoc,locale);
			sectionM( outputDoc);

			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}

		return outputDoc;

	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
        <Title>
            <Line1>Application and Agreement for</Line1>
            <Line2>Receivables Purchase</Line2>
        </Title>
        <Logo>http://localhost:7003/portal/themes/default/images/</Logo>
        <TransInfo>
            <BankInstrumentID>Bank Instrument ID</BankInstrumentID>
            <InstrumentID>ILC1256US01P</InstrumentID>  
            <TransactionID>ILC1256US01P</TransactionID>
            <ReferenceNumber>Applicant's ref Number</ReferenceNumber>
            <ApplicationDate>17 May 2012</ApplicationDate>
        </TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Application and Agreement for");
		outputDoc.setAttribute( "/SectionA/Title/Line2", "Receivables Purchase");

		// Call Parent Method
		super.sectionA(outputDoc);
	}


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionB(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionB(outputDoc);
	}


	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionC(outputDoc);
	}


	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionD1>
        <Party ID="Borrower">
            <Label>Borrower</Label>
            <Name>Bases</Name>
            <AddressLine1>928 Willow Ave</AddressLine1>
            <AddressLine2>Line2</AddressLine2>
            <AddressStateProvince>Hoboken</AddressStateProvince>
            <AddressCountryPostCode>United States</AddressCountryPostCode>
        </Party>
    </SectionD1>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionD1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		//
		try {
			TermsParty borrower = (TermsParty) terms.getComponentHandle("SecondTermsParty");	
			createPartyAddressXml(outputDoc, borrower, "SectionD1","Borrower");
		} catch (Exception e) {
			e.printStackTrace();
		}		

	}

	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionE>
        <LoanRequestDetails>
            <StartDate>loan start date</StartDate>
            <LoanTerms> At fixed maturity date or </LoanTerms>
            <AmountDetail>
                <CurrencyCodeAmount>USD 300.00</CurrencyCodeAmount>
                <CurrencyValueAmountInWords>United States Dollars three hundred and 00/100
                </CurrencyValueAmountInWords>
            </AmountDetail>
            <InterestToBePaid> in advance or in arrears</InterestToBePaid>
        </LoanRequestDetails>
    </SectionE>
 	 *
 	 *Loan Terms: Display words �At fixed maturity date (concatenated with date)� if �At fixed maturity date (date)�             
                       radio button selected
                       Display words �[number] days from the Loan Start Date� if �[ ] days from the Loan Start Date� 
                       radio button selected
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionE(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		String interestToBePaid = terms.getAttribute("interest_to_be_paid");
		String displayString = "";

		if(TradePortalConstants.IN_ADVANCE.equals(interestToBePaid)){
			displayString = "In Advance";
		}if(TradePortalConstants.IN_ARREARS.equals(interestToBePaid)){ 
			displayString = "In Arrears";
		}		

		String lrnDate = formatDate(terms, "loan_start_date",	formLocale);		
		addNonNullDocData(lrnDate, outputDoc, "/SectionE/LoanRequestDetails/StartDate");
		
		//loan terms
		 String	loanTermsType = terms.getAttribute("loan_terms_type");
		 String loanTerms="";
		 String days = terms.getAttribute("loan_terms_number_of_days");		 
		 String maturityDate = formatDate(terms, "loan_terms_fixed_maturity_dt",	formLocale);
		 
		 if(TradePortalConstants.LOAN_DAYS_AFTER.equals(loanTermsType)){
			 loanTerms = days+" days from the Loan Start Date";
		 }else if(TradePortalConstants.LOAN_FIXED_MATURITY.equals(loanTermsType)){
			 loanTerms = " At fixed maturity date "+maturityDate;
		 }
		addNonNullDocData(loanTerms, outputDoc, "/SectionE/LoanRequestDetails/LoanTerms");
		
		
		String currencyCode = null;
		String currencyAmount = null;
		String currencyCodeAndAmount = null;

		currencyCode = terms.getAttribute("amount_currency_code");
		currencyAmount = terms.getAttribute("amount");
		currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
				currencyCode, formLocale);

		String CurrAmountNumber;
		String CurrAmountSpelledOut;
		String concatenatedAmountNumberAndWords;
		
		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");
		
		CurrAmountNumber = currencyCodeAndAmount;

		if (isNotEmpty(currencyCode))
			currencyCode = refDataMgr.getDescr(
					TradePortalConstants.CURRENCY_CODE, currencyCode,
					formLocale);
		if (isNotEmpty(currencyAmount))
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount,
					formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");
		
		CurrAmountSpelledOut = currencyCodeAndAmount;
		concatenatedAmountNumberAndWords = CurrAmountNumber +" ["+CurrAmountSpelledOut+"] ";		
	
		outputDoc.setAttribute("/SectionE/LoanRequestDetails/AmountDetail/CurrencyValueAmountInNumbersAndWords/",	concatenatedAmountNumberAndWords);			

		
		// interest to be paid
		addNonNullDocData(displayString, outputDoc, "/SectionE/LoanRequestDetails/InterestToBePaid");
	}


	/**
	 * SectionG corresponds to the portion of the XML document labeled 'SectionG'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionG>
        <ShipmentDetails>
            <DispatchFrom>dispatch </DispatchFrom>
            <AirportOfDeparture> Port of Loading/Airport of Departure</AirportOfDeparture>
            <AirportOfDestination>Port of Discharge/Airport of Destination </AirportOfDestination>
            <FinalDestination>Place of Final Destination/Delivery/For Transport To</FinalDestination>
            <VesselName>Vessel Name/Carrier</VesselName>
            <AirWayBill>Bill of Lading/Air Waybill</AirWayBill>
            <TextBox>Display value of <Text Box> if entered.</TextBox>
        </ShipmentDetails>
    </SectionG>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionG(DocumentHandler outputDoc) throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getFirstShipment().getAttribute("shipment_from"), outputDoc, "/SectionG/ShipmentDetails/DispatchFrom");
		addNonNullDocData(terms.getFirstShipment().getAttribute("shipment_from_loading"), outputDoc, "/SectionG/ShipmentDetails/AirportOfDeparture");
		addNonNullDocData(terms.getFirstShipment().getAttribute("shipment_to_discharge"), outputDoc, "/SectionG/ShipmentDetails/AirportOfDestination");
		addNonNullDocData(terms.getFirstShipment().getAttribute("shipment_to"), outputDoc, "/SectionG/ShipmentDetails/FinalDestination");
		addNonNullDocData(terms.getFirstShipment().getAttribute("vessel_name_voyage_num"), outputDoc, "/SectionG/ShipmentDetails/VesselName");
		addNonNullDocData(terms.getFirstShipment().getAttribute("air_waybill_num"), outputDoc, "/SectionG/ShipmentDetails/AirWayBill");
		addNonNullDocData(terms.getFirstShipment().getAttribute("goods_description"), outputDoc, "/SectionG/ShipmentDetails/TextBox");
		
	}

	/**
	 * SectionH1 corresponds to the portion of the XML document labeled 'SectionH1'.
	 * The following is an example of what the resulting Xml should look like:
   <SectionH1>
       <LoanInstructions>
           <LoanProceeds>
               <CurrencyCodeAmount>USD 300.00</CurrencyCodeAmount>
               <CurrencyValueAmountInWords>United States Dollars three hundred and 00/100
               </CurrencyValueAmountInWords>                
           </LoanProceeds>
           <TransactionFinanced> Buyer-Request Financing for Seller (Financing Backed By Buyer) </TransactionFinanced>                  
       </LoanInstructions>           
   </SectionH1>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */

	protected void sectionH1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		String currencyCode = null;
		String currencyAmount = null;
		String currencyCodeAndAmount = null;
		String CurrAmountNumber;
		String CurrAmountSpelledOut;
		String concatenatedAmountNumberAndWords;

		currencyCode = terms.getAttribute("amount_currency_code");
		currencyAmount = terms.getAttribute("amount");
		currencyAmount = TPCurrencyUtility.getDisplayAmount(currencyAmount,
				currencyCode, formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		CurrAmountNumber = currencyCodeAndAmount;

		if (isNotEmpty(currencyCode))
			currencyCode = refDataMgr.getDescr(
					TradePortalConstants.CURRENCY_CODE, currencyCode,
					formLocale);
		if (isNotEmpty(currencyAmount))
			currencyAmount = SpellItOut.spellOutNumber(currencyAmount,
					formLocale);

		currencyCodeAndAmount = appendAttributes(currencyCode, currencyAmount,
				" ");

		CurrAmountSpelledOut = currencyCodeAndAmount;
		concatenatedAmountNumberAndWords = CurrAmountNumber +" ["+CurrAmountSpelledOut+"] ";		
	
		outputDoc.setAttribute("/SectionH1/LoanInstructions/LoanProceeds/CurrencyValueAmountInNumbersAndWords/",	concatenatedAmountNumberAndWords);		
		// transaction financed
		String transactionFinanced = "";
		String financingBackedByBuyer = terms.getAttribute("financing_backed_by_buyer_ind");
		String financeType = terms.getAttribute("finance_type");
		boolean doSectionH2 = false;

		if(financeType.equals(TradePortalConstants.SELLER_REQUESTED_FINANCING)){
			transactionFinanced = "Receivables Financing";		
		}else if(financeType.equals(TradePortalConstants.BUYER_REQUESTED_FINANCING)){
			if(financingBackedByBuyer.equals(TradePortalConstants.INDICATOR_YES)){			
				transactionFinanced = "Buyer-Request Financing for Seller (Financing Backed By Buyer)";
			}else{
				transactionFinanced = "Buyer-Request Financing for Seller";
			}
			doSectionH2=true;
		}
		
		addNonNullDocData(transactionFinanced, outputDoc, "/SectionH1/LoanInstructions/TransactionFinanced");
		
		if(doSectionH2){
			sectionH2(outputDoc);
		}
		
	}

	/**
	 * 
	 * 
    <SectionH2>
        <LoanInstructions>
            <Party ID="SellerDetails">
                <Label>Seller Details:</Label>
                <Name>Bases</Name>
                <AddressLine1>928 Willow Ave</AddressLine1>
                <AddressLine2>Line2</AddressLine2>
                <AddressStateProvince>Hoboken</AddressStateProvince>
                <AddressCountryPostCode>United States</AddressCountryPostCode>
            </Party>
            <Party ID="SellerBank">
                <Label>Seller�s Bank:</Label>
                <Name>Bases</Name>
                <AddressLine1>445 Lexington Ave</AddressLine1>
                <AddressStateProvince>New York, NY</AddressStateProvince>
                <AddressCountryPostCode>United States</AddressCountryPostCode>
            </Party>        
        </LoanInstructions>
    </SectionH2> 
	 * 
	 * 
	 */

	protected void sectionH2(DocumentHandler outputDoc)
			throws RemoteException, AmsException{
		
		String benePArty = terms.getAttribute("c_FirstTermsParty");
		if (StringFunction.isBlank(benePArty)) {
			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");
			createPartyAddressXml(outputDoc, applicantParty, "SectionH2/LoanInstructions",
					"Seller Details");
		} else {
			TermsParty beneficiaryParty = (TermsParty) terms
					.getComponentHandle("FirstTermsParty");
/*			TermsParty applicantParty = (TermsParty) terms
					.getComponentHandle("SecondTermsParty");*/
			TermsParty sellersBank = (TermsParty) terms
					.getComponentHandle("ThirdTermsParty");			
			

			createPartyAddressXml(outputDoc, beneficiaryParty, "SectionH2/LoanInstructions",
					"Seller Details");
			createPartyAddressXml(outputDoc, sellersBank, "SectionH2/LoanInstructions",
					"Seller's Bank");
		}		
		
	}
	
	/**
	 * 
	 * 
    <SectionH3>
        <InvoiceDetails>Invoice Details </InvoiceDetails>
        <FinancedInvoices>Enter details of invoices being financed </FinancedInvoices>
        <LoanMaturity> Debit the Borrower�s account: xxxxxxxxxxx </LoanMaturity>       
    </SectionH3>
	 * 
	 */
	protected void sectionH3(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		addNonNullDocData(terms.getAttribute("invoice_details"), outputDoc,
				"/SectionH3/InvoiceDetails");
		addNonNullDocData(terms.getAttribute("financed_invoices_details_text"),
				outputDoc, "/SectionH3/FinancedInvoices");

		String loanMaturity="";
		 String loanMaturityDebitType = terms.getAttribute("loan_maturity_debit_type");

         if(TradePortalConstants.DEBIT_APP.equals(loanMaturityDebitType)){
        	 loanMaturity = "Debit the Borrower�s account: "+terms.getAttribute("loan_maturity_debit_acct");
         }else if(TradePortalConstants.DEBIT_OTHER.equals(loanMaturityDebitType)) {
        	 loanMaturity = "Other: "+terms.getAttribute("loan_maturity_debit_other_txt");
         }
         
		addNonNullDocData(loanMaturity,	outputDoc, "/SectionH3/LoanMaturity");

	}
	
/**
 * 
 * 
    <SectionI>
        <ChargesAndInterest> Debit Accounts: Debit the Borrower�s Account for Charges: xxxxxxxxx :
            Debit the Borrower�s Account for Interest: xxxxxxxxx </ChargesAndInterest>
        <FxConversionDetails>
            
        <LoanProceeds>
            <Fec>Forward Exchange Contract (FEC)</Fec>
            <FecNumber>FEC Number: xxxxxxxxxx</FecNumber>
            <Rate>Rate: xxxxxxxxxxx</Rate>
            <Amount>Amount: xxxxxxxxx</Amount>
            <MaturityDate>Maturity Date: xxxxxxxxx </MaturityDate>                       
        </LoanProceeds>        
        <LoanMaturity>
            <Fec>Forward Exchange Contract (FEC)</Fec>
            <FecNumber>FEC Number: xxxxxxxxxx</FecNumber>
            <Rate>Rate: xxxxxxxxxxx</Rate>
            <Amount>Amount: xxxxxxxxx</Amount>
            <MaturityDate>Maturity Date: xxxxxxxxx </MaturityDate>                       
        </LoanMaturity>        
            
        </FxConversionDetails>
    </SectionI> 
 * 
 * 	
 */
	
	protected void loanProceedsSection(DocumentHandler outputDoc, String loginLocale)
	throws RemoteException, AmsException
	{	
		String bankToBook="";
		String fecNum = "";
		String other ="";	
		
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("use_mkt_rate"))){
			bankToBook = "Exchange Rate Details: Bank to Book Exchange Rate ";			
		}
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("use_fec"))){
			fecNum = "Exchange Rate Details: Forward Exchange Contract (FEC) ";		
		}
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("use_other"))){
			other = "Other: "+terms.getAttribute("use_other_text");
		}
		
		addNonNullDocData(bankToBook, outputDoc, "/SectionI/FxConversionDetails/loanProceeds/bankToBook");
		addNonNullDocData(fecNum, outputDoc, "/SectionI/FxConversionDetails/loanProceeds/fecNum");
		addNonNullDocData(other, outputDoc, "/SectionI/FxConversionDetails/loanProceeds/other");
		addNonNullDocData(terms.getAttribute("covered_by_fec_number"), outputDoc, "/SectionI/FxConversionDetails/loanProceeds/fecNum/FecNumber");
		addNonNullDocData(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_rate"), "", loginLocale), outputDoc, "/SectionI/FxConversionDetails/loanProceeds/fecNum/Rate");
		addNonNullDocData(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("fec_amount"), terms.getAttribute("loan_proceeds_pmt_curr"), loginLocale), outputDoc, "/SectionI/FxConversionDetails/loanProceeds/fecNum/Amount");
		addNonNullDocData(formatDate(terms, "fec_maturity_date", formLocale), outputDoc, "/SectionI/FxConversionDetails/loanProceeds/fecNum/MaturityDate");		
				
	}	
	
	protected void loanMaturitySection(DocumentHandler outputDoc, String loginLocale)
	throws RemoteException, AmsException
	{

		String bankToBook="";
		String fecNum = "";
		String other ="";
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("maturity_use_mkt_rate"))){
			bankToBook = "Exchange Rate Details: Bank to Book Exchange Rate ";			
		}
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("maturity_use_fec"))){		
			fecNum = "Exchange Rate Details: Forward Exchange Contract (FEC) ";			
		}
		if(TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("maturity_use_other"))){
			other = "Other: "+terms.getAttribute("maturity_use_other_text");
		}
		
		addNonNullDocData(bankToBook, outputDoc, "/SectionI/FxConversionDetails/loanMaturity/bankToBook");
		addNonNullDocData(fecNum, outputDoc, "/SectionI/FxConversionDetails/loanMaturity/fecNum");
		addNonNullDocData(other, outputDoc, "/SectionI/FxConversionDetails/loanMaturity/other");
		addNonNullDocData(terms.getAttribute("maturity_covered_by_fec_number"), outputDoc, "/SectionI/FxConversionDetails/loanMaturity/fecNum/FecNumber");
		addNonNullDocData(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("maturity_fec_rate"), "", loginLocale), outputDoc, "/SectionI/FxConversionDetails/loanMaturity/fecNum/Rate");
		addNonNullDocData(TPCurrencyUtility.getDisplayAmount(terms.getAttribute("maturity_fec_amount"), terms.getAttribute("loan_proceeds_pmt_curr"), loginLocale), outputDoc, "/SectionI/FxConversionDetails/loanMaturity/fecNum/Amount");
		addNonNullDocData(formatDate(terms, "maturity_fec_maturity_date", formLocale), outputDoc, "/SectionI/FxConversionDetails/loanMaturity/fecNum/MaturityDate");		
				
	}	
		
	
	protected void sectionI(DocumentHandler outputDoc, String loginLocale) throws RemoteException,
			AmsException {

		String debit = "";		
		if(TradePortalConstants.DEBIT_OUR_ACCOUNTS.equals(terms.getAttribute("coms_chrgs_type"))){
			debit = "Debit Accounts: Debit the Borrower�s Account for Charges: "+terms.getAttribute("coms_chrgs_our_account_num")+" : Debit the Borrower�s Account for Interest: "+terms.getAttribute("coms_chrgs_our_account_num");
		}else if(TradePortalConstants.DEBIT_LOAN_PROCEEDS.equals(terms.getAttribute("coms_chrgs_type"))){
			debit = "Deduct Charges from Loan Proceeds";
		}else if(TradePortalConstants.DEBIT_ACCT_REC.equals(terms.getAttribute("coms_chrgs_type"))){
			debit = "Deduct Charges from Accounts Receivables";
		}

		//add the values to output doc
		addNonNullDocData(debit, outputDoc,	"/SectionI/ChargesAndInterest");
		//loan proceeds
		loanProceedsSection(outputDoc, loginLocale);
		//loan maturity
		loanMaturitySection(outputDoc,loginLocale);

	}
	
	/**
	 * SectionL corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
    <SectionT>
        <InstructionsToBank>instructions to bank details text goes here</InstructionsToBank>
        <AdditionalInstructions>instructions to bank details text goes here</AdditionalInstructions>
    </SectionT>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT(DocumentHandler outputDoc, String locale)
	throws RemoteException, AmsException
	{
        String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",instrument.getAttribute("language"),locale);		
		addNonNullDocData(languageDesc, outputDoc, "/SectionT/InstructionsToBank");
		
		addNonNullDocData(terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT/AdditionalInstructions");
		
	}
	
	protected void sectionM(DocumentHandler outputDoc) throws RemoteException,
			AmsException {
		outputDoc
				.setAttribute(
						"/SectionM/NoticeLine1",
						""
								+ "  With respect to transactions for the purchase of receivables (limited to receivables "
								+ "[a] other than bills of exchange, [b] which do not use financial documents, letters of "
								+ "credit, letters of guaranty or the like and [c] for which transport documents are not"
        + " delivered to Bank) pursuant to export transactions that we perform, in addition to the"
       + " provisions of the \"Bank Transaction Agreement\" separately delivered to and entered into with"
        + " the Bank, we hereby represent and warrant the matters set forth in the Receivables Purchase"
        + " Agreement. ");
		
		outputDoc
		.setAttribute(
				"/SectionM/NoticeLine2",
				""
						+ "The purchase of receivables is subject to the terms and conditions as set forth"
+ " in the Receivables Purchase Agreement to which the Seller agrees and, if Seller�s continuing"
+ " agreement is lodged with Bank, subject to the terms and provisions set forth therein.");		

	}	
	
}