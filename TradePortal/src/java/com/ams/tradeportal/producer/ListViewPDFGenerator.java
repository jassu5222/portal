package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.List;

import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.ams.tradeportal.common.TradePortalConstants;
import com.ams.tradeportal.dataview.DataViewManager;
import com.ams.tradeportal.dataview.InvoiceDataViewUtil;
import com.ams.tradeportal.html.AbstractCustomListViewColumnFormatter;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.ReferenceDataManager;
import com.amsinc.ecsg.frame.XmlConfigFileCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import java.util.Map;


/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: InvoiceDetails.xsl.
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class ListViewPDFGenerator extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(ListViewPDFGenerator.class);

	
	ResourceManager resMgr = null;


	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param outputDoc Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param bankPrefix     - This may be removed in the future.
	 * @param contextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
	
		DocumentHandler resultsList;
		if ( "blueyellow".equals(bankPrefix) ) {
			bankPrefix = TradePortalConstants.DEFAULT_BRANDING;
		  }
		 if ( "anztransac".equals(bankPrefix) ) {
			 bankPrefix = "anz";			 
		  }
		 bankLogo = bankPrefix + "/images";

		formContextPath = contextPath;
		formLocale = locale;

		ClientServerDataBridge csdb = new ClientServerDataBridge();
		csdb.setLocaleName(locale);


		// Get access to the Reference Data manager -- this is a singleton
		refDataMgr = ReferenceDataManager.getRefDataMgr();

		resMgr = new ResourceManager(csdb);
		
		String [] keyValue = key.split("`");
		
		String listViewName = keyValue[0].split("~")[1];
		String resourcePrefix =    keyValue[1].split("~")[1];

		
		Map map = InvoiceDataViewUtil.getMoreSQL().getMap();
		String dynamicWhereClause = (String) map.get(listViewName);
		HashMap parameters = map.get("params")!=null ?(HashMap) map.get("params"): new HashMap();
		
		List<Object>  paramsDynamicSqlPlaceHolder = map.get("paramsDynamicSqlPlaceHolder")!=null ?(ArrayList) map.get("paramsDynamicSqlPlaceHolder"): new ArrayList<Object>();

        
        String heading = "";
		
		DocumentHandler doc = XmlConfigFileCache.readConfigFile("/dataview/".concat(listViewName+".xml"));

        heading = resMgr.getText(resourcePrefix + ".TabName", TradePortalConstants.TEXT_BUNDLE);
        
		String selectClause = doc.getAttribute("/SQL/selectClause");
		String fromClause = doc.getAttribute("/SQL/fromClause");
		String whereClause = doc.getAttribute("/SQL/whereClause");
		Vector columnList = doc.getFragments("/column");
		
		String columnString = generateColumnString(columnList, columnList.size(), "");
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append(columnString).append(" from (SELECT ");
		sql.append(columnString).append(" , ROWNUM ROW_NUM FROM (");

		sql.append(selectClause).append(" ").append(fromClause).append(" ");
		if (StringFunction.isNotBlank(whereClause)){
			sql.append(" ").append(whereClause);
		}
		sql.append(" ").append(dynamicWhereClause);
		if(!parameters.isEmpty()){
			String sort  = (String) parameters.get("sort");
			if(StringFunction.isNotBlank(sort)){
				String sortOrder = sort.startsWith("-")?"D":"A";
				String sortColumnName  = sort.startsWith("-")?sort.substring(1,sort.length()):sort;
		
				sql.append(" ORDER BY ").append(sortColumnName);
				if ("A".equalsIgnoreCase(sortOrder)){
					sql.append(" ASC ");
				}else if ("D".equalsIgnoreCase(sortOrder)){
					sql.append(" DESC ");
				}
			}
		}
		sql.append(") ").append(" ) ");
		resultsList = DatabaseQueryBean.getXmlResultSet(
				sql.toString(), false, paramsDynamicSqlPlaceHolder);
		
		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc, heading);
//			Section 'B' -- Operational Bank Org info
			sectionB( outputDoc, resultsList, columnList, resourcePrefix, columnList.size(), "");

			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			//cleanUpBeans();
		}

		return outputDoc;

	}

	private String generateColumnString(Vector columnList, int noOfColumns, String hideColumn) {
		StringBuilder sb = new StringBuilder();
        String colName = "";
        String isColHidden = "N";
		for (int i = 0; i < noOfColumns; i++ ){
            colName =   ((DocumentHandler)columnList.get(i)).getAttribute("/displayNameKey");
            isColHidden =  ((DocumentHandler)columnList.get(i)).getAttribute("/hidden");
            if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(isColHidden) | colName.equalsIgnoreCase(hideColumn)){
                continue;
            }
			sb.append(colName).append(",");
		}
		sb.setLength(sb.length()-1);
		return sb.toString();
	}

	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>heading</Line1>
	 *			<Line2></Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *	</SectionA>
	 *
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * 
	 *       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc, String heading)
	throws RemoteException, AmsException
	{
		outputDoc.setAttribute("/SectionA/Title/Line1", heading);
	}


	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionB><!-- List -->
	 *		<ResultSetRecord> <!-- Each record in the list -->
	 *			<field> <!-- each column in the record -->
	 *				<name></name> <!-- column name -->
     *				<nameSize></nameSize> <!-- column name string size-->
	 *				<value></value> <!-- column value -->
     *				<valueSize></valueSize> <!-- column value string size-->
	 *			</field>
	 *		</ResultSetRecord>
	 *	</SectionB>
	 *
 	 * 			DocumentHandler.setAttribute does not allow us to add same 
 	 * 			elements multiple times as it just updates existing elements.
	 * 			To overcome above limitation elements are added to the output 
	 * 			document using the DocumentHandler.addComponent method. 
	 * 			This method always adds elements to the top of the document. 
	 * 			So to maintain the order of the elements we need to add the 
	 * 			elements in reverse order.
	 * 
	 */

	protected void sectionB(DocumentHandler outputDoc, DocumentHandler resultsListDoc, Vector columnListDoc, String listViewName, int noOfColumns, String hideColumn)
	throws RemoteException, AmsException
	{
		// Customize Code
		// Customizations Here
		DocumentHandler tempDoc = null;
		DocumentHandler tempDoc1 = null;
		//
		
		if (resultsListDoc != null) {
			Vector resultSetVector = resultsListDoc
					.getFragments("/ResultSetRecord/");
			int totalResultSets = resultSetVector.size();
            String isColHidden = TradePortalConstants.INDICATOR_NO;
            String val = "";
            String formattedValue ="";
			for (int i = totalResultSets - 1 ; i >= 0; i--) {
				tempDoc1 = new DocumentHandler();
				tempDoc1.setAttribute("/ResultSetRecord", "");
				for (int j = noOfColumns - 1 ; j>= 0 ; j--  ){
					String columnName = ((DocumentHandler)columnListDoc.get(j)).getAttribute("/displayNameKey");
                    isColHidden =  ((DocumentHandler)columnListDoc.get(j)).getAttribute("/hidden");

                    if (TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(isColHidden) | columnName.equalsIgnoreCase(hideColumn)){
                        continue;
                    }
					tempDoc = new DocumentHandler();
                    String colName = resMgr.getText(listViewName + "." + columnName, TradePortalConstants.TEXT_BUNDLE);
                    int size = colName.length();
					tempDoc.setAttribute("/field/name",colName);
                    tempDoc.setAttribute("/field/nameSize",String.valueOf(size));

                     val = "";
                     formattedValue ="";
                    String dataType =   ((DocumentHandler)columnListDoc.get(j)).getAttribute("/dataType");
                    
                    val = resultsListDoc
                    .getAttribute("/ResultSetRecord("
                            + i + ")/" + columnName.toUpperCase() );

					if (DataViewManager.REFDATA.equalsIgnoreCase(dataType)){
                        String tableType =  ((DocumentHandler)columnListDoc.get(j)).getAttribute("/tabletype");
						val = refDataMgr.getDescr(
                                tableType,
								resultsListDoc
								.getAttribute("/ResultSetRecord("
										+ i + ")/" + columnName.toUpperCase() ), formLocale);
					//
					//AAlubala - IR#DAUM050238861 Format Date, Number, Decimals and Currency - START
					//
					} else if (dataType.equals(DataViewManager.DATE)) {						
						formattedValue = TPDateTimeUtility.formatDate(val, TPDateTimeUtility.SHORT, resMgr.getResourceLocale());
						val = formattedValue;
					} else if (dataType.equals(DataViewManager.NUMBER)) {
						try
						{
							long longValue = Long.parseLong(val);

							NumberFormat lf = resMgr.getNumberFormatter();

							formattedValue = lf.format(longValue);
							val = formattedValue;
						}
						catch (NumberFormatException e)
						{
							LOG.info("ListViewPDFGenerator: Unable to format " + val+ " as a number ");
							LOG.info("ListViewPDFGenerator: Exception is " +e.toString());
							formattedValue = val;
						}
					} else if (dataType.equals(DataViewManager.DECIMAL)) {
						formattedValue = TPCurrencyUtility.getDecimalDisplayAmount(val, 2, resMgr.getResourceLocale());
						val = formattedValue;
					}  else if (dataType.equals(DataViewManager.CURRENCY)) {
						formattedValue = TPCurrencyUtility.getDecimalDisplayAmount(val, 2, resMgr.getResourceLocale());
						val = formattedValue;
					}  else if (dataType.equals(DataViewManager.GMTDATE)) {
				              if ((val == null) || (val.equals("")))
						{
						   formattedValue = "";
						}
						else
						{					   
					        formattedValue = TPDateTimeUtility.formatDate(val, TPDateTimeUtility.SHORT, resMgr.getResourceLocale());
				  		}
				        val = formattedValue;
				      //IR#DAUM050238861 - END
				      //
					} else if (DataViewManager.CUSTOM.equals(dataType)) {
                        String customFormat =  ((DocumentHandler)columnListDoc.get(j)).getAttribute("/customFormatter");
                        val = resultsListDoc
                                .getAttribute("/ResultSetRecord("
                                        + i + ")/" + columnName.toUpperCase() );
                         formattedValue ="";
                        try {
                            Class customFormatterClass = Class.forName(customFormat);
                            AbstractCustomListViewColumnFormatter customFormatter =
                                    (AbstractCustomListViewColumnFormatter) customFormatterClass.newInstance();
                            customFormatter.setResourceManager(resMgr);
                            formattedValue = customFormatter.format(val);
                        } catch (Exception ex) {
                            //if there is any issue just return the select data value
                            formattedValue = val;
                        }
                        val = formattedValue;
                    }else{
                        val = resultsListDoc
                                .getAttribute("/ResultSetRecord("
                                        + i + ")/" + columnName.toUpperCase() );
					}
                    int valSize = StringFunction.isNotBlank(val)?val.length():0;
                    tempDoc.setAttribute("/field/value",val);
                    tempDoc.setAttribute("/field/valueSize",String.valueOf(valSize));
                    tempDoc1.addComponent("/ResultSetRecord", tempDoc);
				}
				outputDoc.addComponent("/SectionB", tempDoc1);
			}
		}

	}

}