package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class RQAAmendmentApplication extends DocumentProducer implements Status {
//String headerStringPart2 = "/xsl/RQAAmendmentApplication.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";        
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//		return new StringReader(myXmlString);
//	}
//
//	public String getStatus() {
//		return "RQAAmendmentApplication Producer";
//	}
public class RQAAmendmentApplication extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(RQAAmendmentApplication.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{

		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{     
			//Section 'A' -- Title Bank Logo and document reference #            
			sectionA( outputDoc);
			//Section 'B' -- Issuer Info    
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);			

		
			//Section 'N' -- Amendment Details
			sectionN( outputDoc);	
			//Section 'O' -- Account Party
			sectionO( outputDoc);
			//Section 'P' -- Shipment Information
			sectionP( outputDoc);
			//Section 'Q' -- Goods Description
			sectionQ( outputDoc);
			//Section 'R' -- Additional Conditions
			sectionR( outputDoc);

			//Section 'M' -- Important Notice              
			outputDoc.setAttribute( "/SectionM/","");   
			
			
			LOG.debug("OutputDoc == " + outputDoc.toString() );
			
		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}
		
		return outputDoc;
		
	}
	
	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionA(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Request to Advise");       
		outputDoc.setAttribute( "/SectionA/Title/Line2", "Amendment to Irrevocable Commercial Letter of Credit");       
		
		// Call Parent Method
		super.sectionA(outputDoc);
	}
	
	
	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:	 
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionB(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Customize Code (Get Issuer Here)
		TermsParty termsParty = (TermsParty)terms.getComponentHandle("SixthTermsParty");
		
		createPartyAddressXml(outputDoc, termsParty, "SectionB", "Issuer");
			
		// Dont Call Parent Method
		//super.sectionB(outputDoc);
	}
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code goes here
		
		// Call Parent Method
		super.sectionC(outputDoc);
	}
	
	/**
	 * SectionN corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionN><!-- Amendment Details -->
	 * 		<IssueDate>11 December 2005</IssueDate>
	 * 		<LCCurrencyAmount>USD 12345.00</LCCurrencyAmount>
	 * 		<IncreaseBy>GBP 1234.12</IncreaseBy>
	 * 		<DecreaseBy>EUR 123.32</DecreaseBy>
	 * 		<NewCurrencyAmount>USD 4323.43</NewCurrencyAmount>
	 * 		<AmtTolerancePos>32</AmtTolerancePos>
	 * 		<AmtToleranceNeg>3</AmtToleranceNeg>
	 * 		<ExpiryDate>4 December 2003</ExpiryDate>
	 *		<MaximumCreditAmount>Not Exceeding</MaximumCreditAmount>
	 *		<AdditionalAmountsCovered>Some text here</AdditionalAmountsCovered>
	 * 	</SectionN>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	protected void sectionN(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call ancestor Script
		super.sectionN(outputDoc);
		
		// Extend Ancestor Code here
		addNonNullDocData( terms.getAttribute("maximum_credit_amount"), outputDoc, "/SectionN/MaximumCreditAmount");
		addNonNullDocData( terms.getAttribute("addl_amounts_covered"), outputDoc, "/SectionN/AdditionalAmountsCovered");
		
	}
	
	/**
	 * SectionO corresponds to the portion of the XML document labeled 'SectionK'
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionO><!-- Account Party -->
	 * 		<Name>Applicant Bank Name Goes Here</Confirmation>
	 * 		<ApplRefNum>Applicant Bank Name Goes Here</ApplRefNum>
	 * 	</SectionO>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionO(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Ancestor Script
		super.sectionO(outputDoc);
		
		// Extend Ancestor Script here
		addNonNullDocData( instrument.getAttribute("copy_of_ref_num"), outputDoc, "/SectionO/ApplRefNum");

	}

	
	/**
	 * SectionP corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionP><!-- New Shipment Information -->
	 * 		<ShipmentDate>11 December 2005</ShipmentDate>
	 * 		<Incoterm>Delivery Ex Quay (Duty Paid) Somewhere</Incoterm>
	 * 		<From>Some Load of Port</From>
	 * 		<To>Final Destination 3 the movie</To>
	 * 	</SectionP>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionP(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionP(outputDoc);

	}
	
	
	/**
	 * SectionQ corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionQ><!-- Goods Description -->
	 * 		<GoodsDescription>Goods Description</GoodsDescription>
	 * 	</SectionQ>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	protected void sectionQ(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionQ(outputDoc);

	}
	
	/**
	 * SectionR corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionR><!-- Account Party -->
	 * 		<AdditionalConditions>Additional Conditions go here</Confirmation>
	 * 	</SectionR>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionR(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionR(outputDoc);
	}
}