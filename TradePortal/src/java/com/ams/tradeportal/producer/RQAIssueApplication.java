package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class RQAIssueApplication extends DocumentProducer implements Status {
//String headerStringPart2 = "/xsl/RQAIssueApplication.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";        
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;		
//		return new StringReader(myXmlString);
//	}
//
//	public String getStatus() {
//		return "RQAIssueApplication Producer";
//	}
public class RQAIssueApplication extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(RQAIssueApplication.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{     
			//Section 'A' -- Title Bank Logo and document reference #            
			sectionA( outputDoc);
			//Section 'B' -- Issuing Bank info    
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name and Irrevocability
			sectionC( outputDoc);			
			//Section 'D' -- Applicant and Beneficiary        
			sectionD( outputDoc);
			//Section 'E' -- Account Party and Advising Bank
			sectionE( outputDoc);
			//Section 'F' -- Amount Details
			sectionF( outputDoc);
			//Section 'G' -- Presentation Days    
		    sectionG( outputDoc);           
			//Section 'H' -- Payment Terms and Bank Charges  
			sectionH( outputDoc);
			//Section 'I' -- Required Documents       
			sectionI( outputDoc);
			//Section 'J' -- Additional Documents
			sectionJ( outputDoc);
			//Section 'K' -- Transport Document/Shipment # of Total
			sectionK( outputDoc);
			//Section 'L' -- Other Conditions          
			sectionL( outputDoc);
			//Section 'M' -- Important Notice              
			sectionM( outputDoc);   
			
			LOG.debug("OutputDoc == " + outputDoc.toString() );
			
		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}
		
		return outputDoc;
		
	}
	
	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Request to Advise</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		// Customizations Here
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Request to Advise");       
		outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Commercial Letter of Credit");       
		
		// Call Parent Method
		super.sectionA(outputDoc);
	}	
	
	
	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:	 
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionB(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Customize Code (Get Issuer Here)
		TermsParty termsParty = (TermsParty)terms.getComponentHandle("SixthTermsParty");
		
		createPartyAddressXml(outputDoc, termsParty, "SectionB", "Issuer");
			
		// Dont Call Parent Method
		//super.sectionB(outputDoc);
	}
	
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 * 		<Revocablility>an Irrevocable</Revocablility>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		
		// Call Parent Method
		super.sectionC(outputDoc);
		
		// Override Code goes here
		// [Irrevocablility]
		if (terms.getAttribute("irrevocable").equals(TradePortalConstants.INDICATOR_YES))
			outputDoc.setAttribute( "/SectionC/Revocablility/", "an Irrevocable");
		else
			outputDoc.setAttribute( "/SectionC/Revocablility/", "a Revocable");
			
	}
	
	
	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionD><!-- Applicant and Beneficiary-->
	 *		<Applicant>
	 *			<Name>Applicant Name</Name>
	 *			<AddressLine1>Applicant Address Line 1</AddressLine1>
	 *			<AddressLine2>Applicant Address Line 2</AddressLine2>
	 *			<AddressLine3>Applicant Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 *			<RefNum>ABC 123</RefNum>
	 *		</Applicant>
	 *		<Beneficiary>
	 *			<Name>Beneficiary Name</Name>
	 *			<AddressLine1>Beneficiary Address Line 1</AddressLine1>
	 *			<AddressLine2>Beneficiary Address Line 2</AddressLine2>
	 *			<AddressLine3>Beneficiary Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Beneficiary City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Beneficiary Country + Post Code</AddressCountryPostCode>
	 *			<PhoneNumber>Phone Number</PhoneNumber>
	 *		</Beneficiary>
	 *	</SectionD>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionD(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		
		// Call Parent Method
		super.sectionD(outputDoc);
		
		// Override Code Here
		addNonNullDocData( terms.getAttribute("reference_number"), outputDoc, "/SectionD/Party(Applicant)/RefNum/");
	}
	
	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionE><!-- Account Party and Advising/Corresponding Bank -->
	 *		<AccountParty>
	 *			<Name>Account Party Name</Name>
	 *			<AddressLine1>Account Party Address Line 1</AddressLine1>
	 *			<AddressLine2>Account Party Address Line 2</AddressLine2>
	 *			<AddressLine3>Account Party Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Account Party City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Account Party Country + Post Code</AddressCountryPostCode>
	 *		</AccountParty>
	 *		<CorrespondentBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</CorrespondentBank>
	 *		<AdvisingBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</AdvisingBank>
	 *	</SectionE>
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionE(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code
		
		// Call Parent Method
		super.sectionE(outputDoc);
	}
	
	
	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionF><!-- Amount Detail and Expiration -->
	 *		<AmountDetail>
	 *			<IssueDate>15 July 2006</IssueDate>
	 *			<CurrencyCodeAmount>USD 12345.01</CurrencyCodeAmount>
	 *			<CurrencyValueAmountInWords>US Dollars Twelve Thousand Three Hundred Fourty Five and .01</CurrencyValueAmountInWords>
	 *			<Tolerance>
	 *				<Pos>23</Pos>
	 *				<Neg>34</Neg>
	 *			</Tolerance>
	 *			<MaximumCreditAmount>Not Exceeding</MaximumCreditAmount>
	 *			<AdditionalAmountsCovered>Some text here</AdditionalAmountsCovered>
	 *			<AvailableBy>Acceptance</AvailableBy>
	 *			<AvailableWithParty>Advising Bank</AvailableWithParty>
	 *			<DraftsRequired>Yes</DraftsRequired>
	 *			<DraftsDrawnOn>Applicant</DraftsDrawnOn>
	 *		</AmountDetail>
	 *		<Expiration>
	 *			<Date>23 December 2005</Date>
	 *			<Place>A 29 character Text Field</Place>
	 *		</Expiration>
	 *	</SectionF>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionF(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{
		String issueDate = instrument.getAttribute("issue_date");
		if (isNotEmpty(issueDate))
			addNonNullDocData(formatDate(instrument, "issue_date", formLocale), outputDoc, "/SectionF/AmountDetail/IssueDate");
		
		// Call Parent Code
		super.sectionF(outputDoc);
		
		// Extend Ancestor Code here
		addNonNullDocData( terms.getAttribute("maximum_credit_amount"), outputDoc, "/SectionF/AmountDetail/MaximumCreditAmount");
		addNonNullDocData( terms.getAttribute("addl_amounts_covered"), outputDoc, "/SectionF/AmountDetail/AdditionalAmountsCovered");
		//Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 Begin
		//commenting out, because RQA instrument has Available_by at two places.
		/*String availableBy = terms.getAttribute("available_by");*/
		String availableBy = terms.getAttribute("payment_type");
		//Leelavathi IR#T36000016452 Rel-8.2 06/10/2013 End
		if (isNotEmpty(availableBy))
			availableBy = refDataMgr.getDescr(TradePortalConstants.AVAILABLE_BY, availableBy, formLocale );
		addNonNullDocData( availableBy, outputDoc, "/SectionF/AmountDetail/AvailableBy");
		
		String availableWithParty = terms.getAttribute("available_with_party");
		if (isNotEmpty(availableWithParty))
			availableWithParty = refDataMgr.getDescr(TradePortalConstants.AVAILABLE_WITH_PARTY, availableWithParty, formLocale );
		addNonNullDocData( availableWithParty, outputDoc, "/SectionF/AmountDetail/AvailableWithParty");
		
		if (terms.getAttribute("drafts_required").equals(TradePortalConstants.INDICATOR_YES))
			outputDoc.setAttribute( "/SectionF/AmountDetail/DraftsRequired/", "Yes" );
		else
			outputDoc.setAttribute( "/SectionF/AmountDetail/DraftsRequired/", "No" );
		
		String draftsDrawnOn = terms.getAttribute("drawn_on_party");
		if (isNotEmpty(draftsDrawnOn))
			draftsDrawnOn = refDataMgr.getDescr(TradePortalConstants.DRAWN_ON_PARTY, draftsDrawnOn, formLocale );
		addNonNullDocData( draftsDrawnOn, outputDoc, "/SectionF/AmountDetail/DraftsDrawnOn");

		addNonNullDocData( terms.getAttribute("place_of_expiry"), outputDoc, "/SectionF/Expiration/Place");
	}
	
	
	/**
	 * SectionG corresponds to the portion of the XML document labeled 'SectionG'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionG><!-- Presentation Days -->
	 *		<PresentationDays>24</PresentationDays>
	 *		<Credit>Operative/Inoperative</Credit>
	 *	</SectionG> 	
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionG(DocumentHandler outputDoc) throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionG(outputDoc);
		
		if (terms.getAttribute("operative").equals(TradePortalConstants.INDICATOR_YES))
			outputDoc.setAttribute( "/SectionG/Credit/", "Operative" );
		else
			outputDoc.setAttribute( "/SectionG/Credit/", "inoperative" );
		
	}
	
	
	/**
	 * SectionH corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionH><!--Payment Terms and Bank Charges -->
	 *		<PaymentTerms>
	 *			<Sight></Sight>
	 *			<DaysAfter>
	 *				<Days>29</Days>
	 *				<After>Sight</After>
	 *			</DaysAfter>
	 *			<Other></Other>
	 *			<For>89</For>
	 *			<DrawnOnParty>Drawn On Party</DrawnOnParty>
	 *		</PaymentTerms>
	 *		<BankCharges>
	 *			<AllOurAccount></AllOurAccount>
	 *			<AllBankCharges></AllBankCharges>
	 *			<Other></Other>
	 *		</BankCharges>
	 *	</SectionH>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionH(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// call Parent Method
		super.sectionH(outputDoc);
		setPaymentTenorTerms(outputDoc);
	}
	
	
	/**
	 * SectionI corresponds to the portion of the XML document labeled 'SectionI'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionI><!-- Documents Required -->
	 * 		<comm_invoice>
	 * 			<Originals>2</Originals> or <Original>1</Original>
	 * 			<Copies>3</Copies> or <Copy>1</Copy>
	 * 			<Text>Commercial Invoice Text</Text>
	 * 		</comm_invoice>
	 * 		<packing_list>
	 * 			<Originals>7</Originals>
	 * 			<Copies>2</Copies>
	 * 			<Text>Packing List Text</Text>
	 * 		</packing_list>
	 * 		<cert_origin>
	 * 			<Original>1</Original>
	 * 			<Copies>2</Copies>
	 * 			<Text>Certificate of Origin Text.  Here is some more text as well to test word wrapping and stuff.  This is only a test. Thank you.  You are a valued customer.</Text>
	 * 		</cert_origin>
	 * 		<ins_policy>
	 * 			<Originals>2</Originals>
	 * 			<Copies>3</Copies>
	 * 			<Plus>23</Plus>
	 * 			<Covering>Institute Cargo Clause (A)</Covering>
	 * 			<Text>Insurance Policy Text</Text>
	 * 		</ins_policy>
	 * 		<other_req_doc_1>
	 * 			<Original>1</Original>
	 * 			<Copy>1</Copy>
	 * 			<Name>Document Number 1</Name>
	 * 			<Text>Document 1 Text Text Text</Text>
	 * 		</other_req_doc_1>
	 * 		<other_req_doc_2>
	 * 			<Originals>3</Originals>
	 * 			<Copies>9</Copies>
	 * 			<Name>Document Number 2</Name>
	 * 			<Text>Document 2 Text Text Text</Text>
	 * 		</other_req_doc_2>
	 * 		<other_req_doc_3>
	 * 			<Originals>2</Originals>
	 * 			<Copies>5</Copies>
	 * 			<Name>Document Number 3</Name>
	 * 			<Text>Document 3 Text Text Text</Text>
	 * 		</other_req_doc_3>
	 * 		<other_req_doc_4>
	 * 			<Originals>8</Originals>
	 * 			<Copies>11</Copies>
	 * 			<Name>Document Number 4</Name>
	 * 			<Text>Document 4 Text Text Text</Text>
	 * 		</other_req_doc_4>
	 * 	</SectionI>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionI(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{		
		
		// Call parent Function
		super.sectionI(outputDoc);
	}
	
	
	/**
	 * SectionJ corresponds to the portion of the XML document labeled 'SectionJ'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionJ><!-- Additional Documents -->
	 * 		<AdditionalDocuments>Here is where the Additional Documents text should go.  Not sure how much info we will need to display here.  Just babbling away as i am not sure what to put here.</AdditionalDocuments>
	 * 	</SectionJ>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	
	protected void sectionJ(DocumentHandler outputDoc)
		throws RemoteException, AmsException
	{
		// call parent function
		super.sectionJ(outputDoc);
	}
	
	/**
	 * SectionK corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionK><!-- Transport Document -->
	 * 		<TransportDocument ID="1">
	 * 			<TransDoc ID="1 of 2">
	 * 				<TransShipDeclaration>Here is some Transport Document Shipment Declaration Text</TransShipDeclaration>
	 * 				<TransDocType>Air Transport Document</TransDocType>
	 * 				<Originals>3</Originals>
	 * 				<Copies>4</Copies>
	 * 				<ConsignedOrder>Applicant</ConsignedOrder>
	 * 				<ConsignedTo>Issuing Bank</ConsignedTo>
	 * 				<MarkedFreight>Prepaid</MarkedFreight>
	 * 				<NotifyParty>
	 * 					<Name>Notify Party Name</Name>
	 * 					<AddressLine1>Address Line 1</AddressLine1>
	 * 					<AddressLine2>Address Line 2</AddressLine2>
	 * 					<AddressLine3>Address Line 3</AddressLine3>
	 * 				</NotifyParty>
	 * 				<OtherConsignee>
	 * 					<Name>Other Consignee Party Name</Name>
	 * 					<AddressLine1>Address Line 1</AddressLine1>
	 * 					<AddressLine2>Address Line 2</AddressLine2>
	 * 					<AddressLine3>Address Line 3</AddressLine3> 		
	 * 				</OtherConsignee>
	 * 				<TransDocText>Here is some Transport Document text to test it out.</TransDocText>
	 * 				<AddlTransDocs>Here is some Additional Transport Documents 2/3</AddlTransDocs>
	 * 			</TransDoc>
	 * 			<ShipmentTerms><!-- Shipment Terms -->
	 * 				<PartialShipment>permitted</PartialShipment>
	 * 				<TransShipment>not permitted</TransShipment>
	 * 				<ShipmentDate>11 December 2005</ShipmentDate>
	 * 				<Incoterm>Delivery Ex Quay (Duty Paid) Somewhere</Incoterm>
	 * 				<From>Some Load of Port</From>
	 * 				<To>Final Destination 3 the movie</To>
	 * 			<!-- Goods Descriptions -->
	 * 			<GoodsDescription>Here is the goods Description if anybody is interested.</GoodsDescription>
	 *		</TransportDocument>
	 * 	</SectionK>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionK(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{

		// call Parent function
		super.sectionK(outputDoc);

	}

	
	/**
	 * SectionL corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionL><!-- Other Conditions -->
	 * 		<Confirmation>Advising bank to add their confirmation</Confirmation>
	 * 		<Transferable>Yes</Transferable>
	 * 		<Revolve>Yes</Revolve>
	 * 		<AdditionalConditions>Additional Conditions Text goes here</AdditionalConditions>
	 * 	</SectionL>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionL(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionL(outputDoc);
		
		// Additional Code here..
		
	}
	
	
	/**
	 * SectionM corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionM><!-- Other Conditions -->
	 * 		<UCPVersion>ICC/ISP/ETC</UCPVersion>
	 * 		<UCPDetails>Some Text Details</UCPDetails>
	 * 	</SectionM>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	private void sectionM(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		
		outputDoc.setAttribute( "/SectionM/", "" );
		
		String ucpVersion = terms.getAttribute("ucp_version");
		if (isNotEmpty(ucpVersion))
			ucpVersion = refDataMgr.getDescr(TradePortalConstants.ICC_GUIDELINES, ucpVersion, formLocale );
		addNonNullDocData( ucpVersion, outputDoc, "/SectionM/UCPVersion");
		
		addNonNullDocData( terms.getAttribute("ucp_details"), outputDoc, "/SectionM/UCPDetails");
		
	}
}