package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class SLCAmendmentApplication extends DocumentProducer implements Status {
//	String headerStringPart2 = "/xsl/SLCAmendmentApplication.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";        
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//		return new StringReader(myXmlString);
//	}   
//
//	public String getStatus() {
//		return "SLCAmendmentApplication Producer";
//	}
public class SLCAmendmentApplication extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(SLCAmendmentApplication.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);
		
		String instrumentType = instrument.getAttribute("instrument_type_code");	
		LOG.info("Inside Guarantee/Standby Amendment -  InstrumentType ["+instrumentType+"]");
		
		try{     
			//Section 'A' -- Title Bank Logo and document reference #            
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info    
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);			
		
			//Section 'N' -- Amendment Details
			sectionN( outputDoc);	
			
			//Section 'S' -- Validity Dates
			if(isExpandedFormRequest(InstrumentType.GUARANTEE))
				sectionS( outputDoc);	
			
			//Section 'O' -- Account Party
			sectionO( outputDoc);
			//Section 'J' -- Amendment Details Text Description
			sectionJ( outputDoc);	
			
			if(isExpandedFormRequest(instrumentType)){
				sectionT( outputDoc);
			}
			//Section 'M' -- Important Notice     
			sectionM( outputDoc);	
			//outputDoc.setAttribute( "/SectionM/","");   
			
			
			LOG.debug("OutputDoc == " + outputDoc.toString() );
			
		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}
		
		return outputDoc;
		
	}
	
	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		// Customizations Here
		
		// jgadela 06/18.2014 IR T36000026067 - made the label dynamic [begin]
	   String instrumentType = instrument.getAttribute("instrument_type_code");
	    // Nar CR-938 rel9500 02/25/2016 -  changed the PDF Headline name for customized PDF
	   if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
		   outputDoc.setAttribute( "/SectionA/Title/Line1", "Request for Standby Letter of Credit Amendment");
	   }else {
		outputDoc.setAttribute( "/SectionA/Title/Line1", "Application for Amendment to");
		// Customizations Here
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			if(isExpandedFormRequest(instrumentType)){
				outputDoc.setAttribute( "/SectionA/Title/Line2", "Guarantee"); 
			}else{
				outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Outgoing Guarantee"); 
				outputDoc.setAttribute( "/SectionA/TransInfo/InstrumentNumLabel", "Guarantee Number:"); 
			}
		}else{
			outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Standby Letter of Credit"); 
			if(!isExpandedFormRequest(instrumentType))
				outputDoc.setAttribute( "/SectionA/TransInfo/InstrumentNumLabel", "Letter of Credit Number:"); 
		}
		// jgadela 06/18.2014 IR T36000026067 - made the label dynamic [end]
	   }
		// Call Parent Method
		super.sectionA(outputDoc);
				
	}
	
	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:	 
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionB(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Customize Code
		
		// Call Parent Method
		super.sectionB(outputDoc);
	}
	
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// jgadela 06/18.2014 IR T36000026067 - Implemented the pdf download for guarantee and made the text dynamic [begin]
		String name = opBankOrg.getAttribute("name");
		// Override Code goes here
		String instrumentType = instrument.getAttribute("instrument_type_code");
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			outputDoc.setAttribute( "/SectionC/Name", "We (\"Applicant\") request you, "+name+" (\"Bank\") to amend the captioned Guarantee as follows:"); 
		}else{
			outputDoc.setAttribute( "/SectionC/Name", "We (\"Applicant\") request you, "+name+" (\"Bank\") to amend the captioned standby letter of credit (\"Credit\") as follows:"); 
		}
		// Call Parent Method
		//super.sectionC(outputDoc);
		// jgadela 06/18.2014 IR T36000026067 - made the text dynamic [end]
	}	
	
	/**
	 * SectionJ corresponds to the portion of the XML document labeled 'SectionJ'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionJ><!-- Amendment Details -->
	 * 		<AmendmentDetails>Here is where the Amendment Details text should go.  Not sure how much info we will need to display here.  Just babbling away as i am not sure what to put here.</AmendmentDetails>
	 * 	</SectionJ>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	
	protected void sectionJ(DocumentHandler outputDoc)
		throws RemoteException, AmsException
	{
		// override parent code
		String instrumentType = instrument.getAttribute("instrument_type_code");
		String guarCustomerText = null;
		
		// Customizations Here
		if(!instrumentType.equals(InstrumentType.GUARANTEE)){
			addNonNullDocData(terms.getAttribute("amendment_details"), outputDoc, "/SectionJ/AmendmentDetails");
		}
		/*KMehta - 21 Nov 2014 - Rel9.2 IR-T36000031989 - Add  - Begin*/
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			guarCustomerText = terms.getAttribute("guar_customer_text");
			if (isNotEmpty(guarCustomerText))
				addNonNullDocData(guarCustomerText, outputDoc,
						"/SectionJ/AmendmentDetails/");
		}
		/*KMehta - 21 Nov 2014 - Rel9.2 IR-T36000031989 - Add  - End*/
	}
	
	
	/**
	 * SectionN corresponds to the portion of the XML document labeled 'SectionK'.
	  * The following is an example of what the resulting Xml should look like:
	 * 	<SectionN><!-- Amendment Details -->
	 * 		<IssueDate>11 December 2005</IssueDate>
	 * 		<LCCurrencyAmount>USD 12345.00</LCCurrencyAmount>
	 * 		<IncreaseBy>GBP 1234.12</IncreaseBy>
	 * 		<DecreaseBy>EUR 123.32</DecreaseBy>
	 * 		<NewCurrencyAmount>USD 4323.43</NewCurrencyAmount>
	 * 		<AmtTolerancePos>32</AmtTolerancePos>
	 * 		<AmtToleranceNeg>3</AmtToleranceNeg>
	 * 		<ExpiryDate>4 December 2003</ExpiryDate>
	 * 	</SectionN>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 *
	 */
	protected void sectionN(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call ancestor Script
		super.sectionN(outputDoc);

		// Override code
		
	}
	
	/**
	 * SectionO corresponds to the portion of the XML document labeled 'SectionK'
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionO><!-- Account Party -->
	 * 		<Name>Applicant Bank Name Goes Here</Confirmation>
	 * 	</SectionO>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionO(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Ancestor Script
		super.sectionO(outputDoc);
		
		// Override
	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT>
        <InstructionsToBank>instructions to bank details text goes here</InstructionsToBank>
        <CommissionsAndCharges>
            <DebitOurs>Debit: Our Account Number  </DebitOurs>
            <DebitForeign>Debit: Foreign Currency Account Number  </DebitForeign>
            <Currency> Currency of Account dropdown</Currency>
            <AdditionalInstructions>Additional Instructions </AdditionalInstructions>
        </CommissionsAndCharges>
    </SectionT>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{		
			addNonNullDocData( terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT/InstructionsToBank");	
	}
	
	/* @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionM(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	// jgadela 06/18.2014 IR T36000026067 - Implemented the pdf download for guarantee and made the important notice text dynamic [end]
		String instrumentType = instrument.getAttribute("instrument_type_code");
		// Customizations Here
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that this amendment is subject to acceptance by the beneficiary and Bank.  All other terms and conditions of Guarantee shall remain unchanged, and all of our obligations and liabilities to you with respect to Guarantee shall apply to Guarantee as so amended.");
			outputDoc.setAttribute( "/SectionM/NoticeLine2", "Applicant understands that the final form of the amendment to Guarantee may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes."); 
		}else{	
			outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that this amendment is subject to acceptance by the beneficiary and Bank.  All other terms and conditions of Credit shall remain unchanged, and all of our obligations and liabilities to you with respect to Credit shall apply to Credit as so amended.");
			outputDoc.setAttribute( "/SectionM/NoticeLine2", "Applicant understands that the final form of the amendment to Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes.");
			if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
				outputDoc.setAttribute( "/SectionM/NoticeLine3", " The amendment to the Credit shall be subject to the terms of the Continuing Letter of Credit Agreement or Continuing Standby Letter of Credit Agreement entered into between Applicant and Bank.");		    	
		    }
		}
		//outputDoc.setAttribute( "/SectionM/","");  
		// jgadela 06/18.2014 IR T36000026067 - made the important notice text dynamic [end]
	}
	
	
	
	protected void sectionS(DocumentHandler outputDoc) throws RemoteException, AmsException {
		 //MEer CR-1027  Validity Dates
		String instrumentType = instrument.getAttribute("instrument_type_code");
		if(InstrumentType.GUARANTEE.equalsIgnoreCase(instrumentType)){
			String validFromType = terms.getAttribute("guar_valid_from_date_type");
			if (validFromType.equals(TradePortalConstants.CURRENT_VALIDITY_FROM_DATE))
				outputDoc.setAttribute("/SectionS/Validity/ValidFrom", "Current Validity From Date");			
			if (validFromType.equals(TradePortalConstants.DATE_OF_ISSUE))
				outputDoc.setAttribute("/SectionS/Validity/ValidFrom", "Date of Issue");
			if (validFromType.equals(TradePortalConstants.OTHER_VALID_FROM_DATE))
				outputDoc.setAttribute("/SectionS/Validity/ValidFrom", formatDate(terms, "guar_valid_from_date", formLocale));

			String validToType = terms.getAttribute("guar_expiry_date_type");
			if (validToType.equals(TradePortalConstants.CURRENT_VALIDITY_TO_DATE))
				outputDoc.setAttribute("/SectionS/Validity/ValidTo", formatDate(instrument, "copy_of_expiry_date", formLocale));			
			if (validToType.equals(TradePortalConstants.VALIDITY_DATE))
				outputDoc.setAttribute("/SectionS/Validity/ValidTo", formatDate(terms, "expiry_date", formLocale));
			if (validToType.equals(TradePortalConstants.OTHER_EXPIRY_DATE))
				outputDoc.setAttribute("/SectionS/Validity/ValidTo", "Refer Instructions to Bank");
		}

	}

}