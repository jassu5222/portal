package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.*;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the 
 * result on to a cocoon wrapper to display the result in a PDF format.  The method: 
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: DLCIssueApplication.xsl.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 BEGIN
//public class SLCIssueApplicationS extends DocumentProducer implements Status {
//	String headerStringPart2 = "/xsl/SLCIssueApplicationS.xsl\" type=\"text/xsl\"?>" + "<?cocoon-process type=\"xslt\"?>";        
//
//	public Reader getStream(HttpServletRequest request) throws IOException, AmsException {
//		DocumentHandler xmlDoc = new DocumentHandler("<Document></Document>",false);
//		String myChildString = this.populateXmlDoc(xmlDoc, EncryptDecrypt.decryptAlphaNumericString(request.getParameter("transaction")), request.getParameter("locale"), request.getParameter("bank_prefix"), request.getContextPath()).toString();
//	    PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
//		String producerRoot = portalProperties.getString("producerRoot");
//		String myXmlString = headerStringPart1+producerRoot+"/"+request.getContextPath()+headerStringPart2+myChildString;
//		return new StringReader(myXmlString);
//	}   
//
//	public String getStatus() {
//		return "SLCIssueApplicationS Producer";
//	}
public class SLCIssueApplicationS extends DocumentProducer {
	private static final Logger LOG = LoggerFactory.getLogger(SLCIssueApplicationS.class);
//BSL Cocoon Upgrade 09/28/11 Rel 7.0 END
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific 
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.  
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing 
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{  
			String instrumentType = instrument.getAttribute("instrument_type_code");
			//Section 'A' -- Title Bank Logo and document reference #            
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info    
			sectionB( outputDoc);
			//Section 'C' -- Op Bank Org Name Only
			sectionC( outputDoc);			
			//Section 'D' -- Applicant and Beneficiary        
			sectionD( outputDoc);			
			//Section 'E' -- Account Party and Correspondent Bank
			sectionE( outputDoc);
			sectionE0(outputDoc);
			sectionE1(outputDoc);
			sectionE2(outputDoc);
			sectionE3(outputDoc);			
			//Section 'F' -- Amount Details
			sectionF( outputDoc);
			//Section 'H' -- Payment Terms and Bank Charges  
			sectionH( outputDoc);
			sectionH4( outputDoc);
			sectionH5( outputDoc);
			//Section 'J' -- Required Documents
			sectionJ( outputDoc);
			//Section 'L' -- Other Conditions          
			sectionL( outputDoc);
			//

			if(isExpandedFormRequest(instrumentType)){
				//sectionT( outputDoc);
				sectionT1( outputDoc, locale);
				checkForSettlementInstructions(outputDoc); //New design update
				sectionT2( outputDoc);
				sectionU1( outputDoc);
				sectionU3( outputDoc);			
			}			
			//Section 'M' -- Important Notice              
			// jgadela 06/18.2014 IR T36000026067 - Added code for GUA pdf and made the important notice text dynamic 
			sectionM( outputDoc);
			//outputDoc.setAttribute( "/SectionM/",""); 
			
			LOG.debug("OutputDoc == " + outputDoc.toString() );
			
		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}
		
		return outputDoc;
		
	}
	
	
	/**
	 * SectionA corresponds to the portion of the XML document labeled 'SectionA'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionA><!-- Header Info -->
	 *		<Title>
	 *			<Line1>Application and Agreement for</Line1>
	 *			<Line2>Irrevocable Commercial Letter of Credit</Line2>
	 *		</Title>
	 *		<Logo>http://localhost:7001//portal/images/blueyellow/en/</Logo>
	 *		<TransInfo>
	 *			<InstrumentID>ILC80US01P</InstrumentID>
	 *			<ReferenceNumber>123456REF</ReferenceNumber>
	 *			<ApplicationDate>05 October 2005</ApplicationDate>
	 *			<AmendmentDate>05 November 2005</AmendmentDate>
	 *		</TransInfo>
	 *	</SectionA>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
 	 */
	protected void sectionA(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	
		// Customizations Here
		 // Nar CR-938 rel9500 02/25/2016 -  changed the PDF Headline name for customized PDF
		if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
			outputDoc.setAttribute( "/SectionA/Title/Line1", "Standby Letter of Credit Application");
		} else {
		    outputDoc.setAttribute( "/SectionA/Title/Line1", "Application and Agreement for");       
		    outputDoc.setAttribute( "/SectionA/Title/Line2", "Irrevocable Standby Letter of Credit"); 
		}
		
		// Call Parent Method
		super.sectionA(outputDoc);
	
		

	}	
		
	
	/**
	 * SectionB corresponds to the portion of the XML document labeled 'SectionB'.
	 * The following is an example of what the resulting Xml should look like:	 
	 *	<SectionB><!-- Operational Bank Org Info -->
	 *		<Name>Operational Bank Org Name</Name>
	 *		<AddressLine1>Operational Bank Org Addr Line 1</AddressLine1>
	 *		<AddressLine2>Operational Bank Org Addr Line 2</AddressLine2>
	 *		<AddressStateProvince>Operational Bank Org City + Province</AddressStateProvince>
	 *		<AddressCountryPostCode>Operational Bank Org Country + Post Code</AddressCountryPostCode>
	 *		<PhoneNumber>Operational Bank Org Telephone #</PhoneNumber>
	 *		<Fax>Operational Bank Org Fax #</Fax>
	 *		<Swift>Operational Bank Org Address</Swift>
	 *		<Telex>Operational Bank Org Telex # Operational Bank Org Answer Back</Telex>
	 *	</SectionB>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionB(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Customize Code
		
		// Call Parent Method
		super.sectionB(outputDoc);
	}
	
	
	/**
	 * SectionC corresponds to the portion of the XML document labeled 'SectionC'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionC><!-- Operational Bank org Disclaimer -->
	 * 		<Name>Operational Bank Org Name</Name>
	 *	</SectionC>
	 *
	 * @param Output Document           - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionC(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code goes here
		// jgadela 06/18.2014 IR T36000026067 - made the text dynamic 
		String name = opBankOrg.getAttribute("name");
		
		//Rel-9.2 IR#T36000021894 Vsarkary validating and loading delivery instructions dynamically- Start
		String deliverTo = "";
        String deliverBy = "";
        if (terms.isAttributeRegistered("guar_deliver_to")){
		    deliverTo = terms.getAttribute("guar_deliver_to");//"BEN";//
            if (TradePortalConstants.DELIVER_TO_APPLICANT.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_APPLICANT_OPTION;
            if (TradePortalConstants.DELIVER_TO_BENEFICIARY.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_BENEFICIARY_OPTION;
            if (TradePortalConstants.DELIVER_TO_AGENT.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_AGENT_OPTION;
            if (TradePortalConstants.DELIVER_TO_OTHER.equals(deliverTo))
                deliverTo = TradePortalConstants.DELIVARY_TO_OTHER_OPTION;
        }
        if (terms.isAttributeRegistered("guar_deliver_by")){
            deliverBy = terms.getAttribute("guar_deliver_by");//"S";//
            if (TradePortalConstants.DELIVER_BY_TELEX.equals(deliverBy) || TradePortalConstants.DELIVER_BY_SWIFT.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_TELEX_SWIFT_OPTION;
            if (TradePortalConstants.DELIVER_BY_REG_MAIL.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_REGISTERED_MAIL_OPTION;
            if (TradePortalConstants.DELIVER_BY_MAIL.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_MAIL_OPTION;
            if (TradePortalConstants.DELIVER_BY_COURIER.equals(deliverBy))
                deliverBy = TradePortalConstants.DELIVARY_BY_COURIER_OPTION;
        }
		
        outputDoc.setAttribute( "/SectionC/Name", "We (\"Applicant\") request you, "+name+" (\"Bank\") to issue an irrevocable standby letter of credit  (\"Credit\") with the following terms and conditions for delivery to the  beneficiary named below (\""+deliverTo+"\") by: "+deliverBy); 
        //Rel-9.2 IR#T36000021894 Vsarkary validating and loading delivery instructions dynamically- End
		
		
		// Call Parent Method
		//super.sectionC(outputDoc);
	}
	
	
	/**
	 * SectionD corresponds to the portion of the XML document labeled 'SectionD'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionD><!-- Applicant and Beneficiary-->
	 *		<Applicant>
	 *			<Name>Applicant Name</Name>
	 *			<AddressLine1>Applicant Address Line 1</AddressLine1>
	 *			<AddressLine2>Applicant Address Line 2</AddressLine2>
	 *			<AddressLine3>Applicant Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Applicant City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Applicant Country + Post Code</AddressCountryPostCode>
	 *		</Applicant>
	 *		<Beneficiary>
	 *			<Name>Beneficiary Name</Name>
	 *			<AddressLine1>Beneficiary Address Line 1</AddressLine1>
	 *			<AddressLine2>Beneficiary Address Line 2</AddressLine2>
	 *			<AddressLine3>Beneficiary Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Beneficiary City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Beneficiary Country + Post Code</AddressCountryPostCode>
	 *			<PhoneNumber>Phone Number</PhoneNumber>
	 *		</Beneficiary>
	 *	</SectionD>
	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionD(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code Here
		
		// Call Parent Method
		super.sectionD(outputDoc);

	}
	
	/**
	 * SectionE corresponds to the portion of the XML document labeled 'SectionE'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionE><!-- Account Party and Advising/Corresponding Bank -->
	 *		<AccountParty>
	 *			<Name>Account Party Name</Name>
	 *			<AddressLine1>Account Party Address Line 1</AddressLine1>
	 *			<AddressLine2>Account Party Address Line 2</AddressLine2>
	 *			<AddressLine3>Account Party Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Account Party City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Account Party Country + Post Code</AddressCountryPostCode>
	 *		</AccountParty>
	 *		<CorrespondentBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</CorrespondentBank>
	 *		<AdvisingBank>
	 *			<Name>Advising Bank Name</Name>
	 *			<AddressLine1>Advising Bank Address Line 1</AddressLine1>
	 *			<AddressLine2>Advising Bank Address Line 2</AddressLine2>
	 *			<AddressLine3>Advising Bank Address Line 3</AddressLine3>
	 *			<AddressStateProvince>Advising Bank City + Province</AddressStateProvince>
	 *			<AddressCountryPostCode>Advising Bank Country + Post Code</AddressCountryPostCode>
	 *		</AdvisingBank>
	 *	</SectionE>
 	 *
	 * @param Output Document             - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionE(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Override Code
		
		// Call Parent Method
		super.sectionE(outputDoc);
	}	
	
	/**
	 * SectionF corresponds to the portion of the XML document labeled 'SectionF'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionF><!-- Amount Detail and Expiration -->
	 *		<AmountDetail>
	 *			<CurrencyCodeAmount>USD 12345.01</CurrencyCodeAmount>
	 *			<CurrencyValueAmountInWords>US Dollars Twelve Thousand Three Hundred Fourty Five and .01</CurrencyValueAmountInWords>
	 *			<Tolerance>
	 *				<Pos>23</Pos>
	 *				<Neg>34</Neg>
	 *			</Tolerance>
	 *		</AmountDetail>
	 *		<Expiration>
	 *			<Date>23 December 2005</Date>
	 *			<CharacteristicType>A 29 character Text Field</CharacteristicType>
	 *		</Expiration>
	 *	</SectionF>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionF(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{
		// Call Parent Code
		super.sectionF(outputDoc);
		
		// Override Ancestor Code here
		String characteristicType = terms.getAttribute("characteristic_type");
		if( isNotEmpty( characteristicType )) {
			characteristicType = refDataMgr.getDescr( TradePortalConstants.CHARACTERISTIC_TYPE, characteristicType, formLocale);
			addNonNullDocData( characteristicType, outputDoc, "/SectionF/Expiration/CharacteristicType");}

		//rbhaduri - 20th Sep 06 - IR CUG091550541 - add place of expiry
		String placeofExpiry = terms.getAttribute("place_of_expiry");
		if( isNotEmpty( placeofExpiry )) {
			placeofExpiry = refDataMgr.getDescr( TradePortalConstants.PLACE_OF_EXPIRY_TYPE, placeofExpiry, formLocale);
		addNonNullDocData( placeofExpiry, outputDoc, "/SectionF/Expiration/Place");}
	}
	
	
	/**
	 * SectionH corresponds to the portion of the XML document labeled 'SectionH'.
	 * The following is an example of what the resulting Xml should look like:
	 *	<SectionH><!--Payment Terms and Bank Charges -->
	 *		<PaymentTerms>
	 *			<Sight></Sight>
	 *			<DaysAfter>
	 *				<Days>29</Days>
	 *				<After>Sight</After>
	 *			</DaysAfter>
	 *			<Other></Other>
	 *			<For>89</For>
	 *			<DrawnOnParty>Drawn On Party</DrawnOnParty>
	 *		</PaymentTerms>
	 *		<BankCharges>
	 *			<AllOurAccount></AllOurAccount>
	 *			<AllBankCharges></AllBankCharges>
	 *			<Other></Other>
	 *		</BankCharges>
	 *	</SectionH>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionH(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// call Parent Method
		super.sectionH(outputDoc);
		setPaymentTenorTerms(outputDoc);
		// Additional code here

	}
	
	
	/**
	 * SectionJ corresponds to the portion of the XML document labeled 'SectionJ'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionJ><!-- Documents Required -->
	 * 		<DocumentsRequired>Here is where the Additional Documents text should go.  Not sure how much info we will need to display here.  Just babbling away as i am not sure what to put here.</DocumentsRequired>
	 * 	</SectionJ>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	
	protected void sectionJ(DocumentHandler outputDoc)
		throws RemoteException, AmsException
	{
		// Override Parent code
		addNonNullDocData(terms.getAttribute("addl_doc_text"), outputDoc, "/SectionJ/DocumentsRequired");
	}
	
	
	/**
	 * SectionL corresponds to the portion of the XML document labeled 'SectionK'.
	 * The following is an example of what the resulting Xml should look like:
	 * 	<SectionL><!-- Other Conditions -->
	 * 		<Confirmation>Advising bank to add their confirmation</Confirmation>
	 * 		<Transferable>Yes</Transferable>
	 * 		<Revolve>Yes</Revolve>
	 * 		<AdditionalConditions>Additional Conditions Text goes here</AdditionalConditions>
	 * 	</SectionL>
	 * 
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionL(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionL(outputDoc);
		setAutoExtTerms(outputDoc);
		// Additional Code here...
	}
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT>
        <InstructionsToBank>instructions to bank details text goes here</InstructionsToBank>
        <CommissionsAndCharges>
            <DebitOurs>Debit: Our Account Number  </DebitOurs>
            <DebitForeign>Debit: Foreign Currency Account Number  </DebitForeign>
            <Currency> Currency of Account dropdown</Currency>
            <AdditionalInstructions>Additional Instructions </AdditionalInstructions>
        </CommissionsAndCharges>
    </SectionT>
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		addNonNullDocData(terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT/InstructionsToBank");
		addNonNullDocData(terms.getAttribute("coms_chrgs_our_account_num"), outputDoc, "/SectionT/CommissionsAndCharges/DebitOurs");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_num"), outputDoc, "/SectionT/CommissionsAndCharges/DebitForeign");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_curr"), outputDoc, "/SectionT/CommissionsAndCharges/Currency");
		addNonNullDocData(terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT/CommissionsAndCharges/AdditionalInstructions");
		
	}	
	
	/* @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionM(DocumentHandler outputDoc) 
	throws RemoteException, AmsException
	{	// jgadela 06/18.2014 IR T36000026067 - made the important notice text dynamic [end]
		    if ( TradePortalConstants.INDICATOR_YES.equals(enableCustomPDF) ) {
		    	String NoticeLine3 = " The opening of the Credit shall be subject to the terms of the Continuing Letter of Credit Agreement or Continuing Standby Letter of Credit Agreement entered into between Applicant and Bank.";
		    	outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that the final form of Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes."+NoticeLine3);
		    	//outputDoc.setAttribute( "/SectionM/NoticeLine3", "The opening of the Credit shall be subject to the terms of the Continuing Letter of Credit Agreement or Continuing Standby Letter of Credit Agreement entered into between Applicant and Bank.");
		    } else {
		    	outputDoc.setAttribute( "/SectionM/NoticeLine1", "Applicant understands that the risk is greater if Applicant requests a standby letter of credit which requires only a draft without any supporting documentation. Typically, standby letters of credit require the beneficiary to provide some statement of alleged non-performance or default in order to obtain payment. However, a beneficiary that can obtain a standby letter of credit available only against presentation of a draft or a demand, relieves itself of any documentary requirements.");
		    	outputDoc.setAttribute( "/SectionM/NoticeLine2", "Applicant understands that the final form of Credit may be subject to such revisions and changes as are deemed necessary or appropriate by Bank and Applicant hereby consents to such revisions and changes.");
		    	outputDoc.setAttribute( "/SectionM/NoticeLine3", "The opening of the Credit is subject to the terms and conditions as set forth in the Standby Letter of Credit Agreement to which the Applicant agrees and, if Applicant's continuing agreement is lodged with Bank, subject to the terms and provisions set forth therein.");
		    }
			 
			 
		//outputDoc.setAttribute( "/SectionM/","");  
		// jgadela 06/18.2014 IR T36000026067 - made the important notice text dynamic [end]
	}

	protected void sectionE0(DocumentHandler outputDoc) 
			throws RemoteException, AmsException
			{
			//implemented in expanded class
			}
	
	protected void sectionE1(DocumentHandler outputDoc) 
			throws RemoteException, AmsException
			{
			//implemented in expanded class
			}
	
	protected void sectionE2(DocumentHandler outputDoc) 
			throws RemoteException, AmsException
			{
			//implemented in expanded class
			}
	
	protected void sectionE3(DocumentHandler outputDoc) 
			throws RemoteException, AmsException
			{
			//implemented in expanded class
			}
	protected void sectionT1(DocumentHandler outputDoc, String locale)
	throws RemoteException, AmsException
	{
        String languageDesc = ReferenceDataManager.getRefDataMgr().getDescr("INSTRUMENT_LANGUAGE",instrument.getAttribute("language"),locale);		
		addNonNullDocData(languageDesc, outputDoc,"/SectionT1/InstructionsToBank/Issue");
		addNonNullDocData( terms.getAttribute("special_bank_instructions"), outputDoc, "/SectionT1/InstructionsToBank/Additional");
	}	
	
	/**
	 *	 * The following is an example of what the resulting Xml should look like:
    <SectionT2>
        <SettlementInstructions>
            <DebitOurAccount>
                Debit: Our Account Number 
            </DebitOurAccount>
            <BranchCode>
                Branch Code
            </BranchCode>
            <DebitForeignAccount>
                Debit: Foreign Currency Account Number 
            </DebitForeignAccount>
            <CurrencyOfAccount>
                Currency of Account dropdown
            </CurrencyOfAccount>            
        </SettlementInstructions>  
    </SectionT2>  
	 *
	 * @param Output Document       - This is the Xml document being built to be sent out for display.
	 */
	protected void sectionT2(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("settlement_our_account_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("branch_code"), outputDoc, "/SectionT2/SettlementInstructions/BranchCode");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_num"), outputDoc, "/SectionT2/SettlementInstructions/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("settlement_foreign_acct_curr"), outputDoc, "/SectionT2/SettlementInstructions/CurrencyOfAccount");		
	}
	
	
	protected void sectionU1(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{

		addNonNullDocData(terms.getAttribute("coms_chrgs_our_account_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitOurAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_num"), outputDoc, "/SectionU1/CommisionAndCharges/DebitForeignAccount");
		addNonNullDocData(terms.getAttribute("coms_chrgs_foreign_acct_curr"), outputDoc, "/SectionU1/CommisionAndCharges/CurrencyOfAccount");
	}
	
	protected void sectionU3(DocumentHandler outputDoc) throws RemoteException, AmsException{
		addNonNullDocData(terms.getAttribute("coms_chrgs_other_text"), outputDoc, "/SectionU3/AdditionalInstructions");
		// terms.getAttribute("internal_instructions")-for Internal Instructions
		
	}
	
	protected void sectionH4(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{
		//implemented in child (expanded form)
	}	
	
	protected void sectionH5(DocumentHandler outputDoc)    
	throws RemoteException, AmsException
	{
		//implemented in child (expanded form)
	}	
	
}