package com.ams.tradeportal.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.amsinc.ecsg.util.*;
import com.ams.tradeportal.busobj.PmtTermsTenorDtl;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;

/**
 * This class is designed to Build an XML Document with values from various business objects
 * then pass this data through an XSL stylesheet format the data for display then pass the
 * result on to a cocoon wrapper to display the result in a PDF format.  The method:
 * populateXmlDoc does most of the work, however get stream puts it all together and returns
 * the result (a Reader object).  The Corresponding XSL file is: SettlementInstructionsDiscrepancyResponse.xsl.
 *
 *     Copyright  � 2001
 *     CGI, Incorporated
 *     All rights reserved
 */

public class SettlementInstructionsDiscrepancyResponse extends DocumentProducer {
private static final Logger LOG = LoggerFactory.getLogger(SettlementInstructionsDiscrepancyResponse.class);
	/**
	 * This method is the method that gathers all the data and builds the Xml document to be returned
	 * to the caller for display.  The Xsl template is expecting the Xml doc to send data out in a specific
	 * format...ie in sections.  If you scroll down the left side you can easily jump around the xml doc
	 * looking for specific section letters.
	 *
	 * @param Output Xml Doc - This is the resulting Xml doc that will be returned.
	 * @param Key            - This is the Transaction oid the will help us get all our data.
	 *                         With out this oid, no data can be retrieved.
	 * @param Locale         - This is the current users locale, it will help in establishing
	 *                         the client Server data bridge as well as building various B.O.s
	 * @param BankPrefix     - This may be removed in the future.
	 * @param ContextPath    - the context path under which this request was made
	 * @return OutputDoc     - Send out the result of building our Document.
	 */
	protected DocumentHandler populateXmlDoc(DocumentHandler outputDoc, String key, String locale, String bankPrefix, String contextPath)
	throws RemoteException, AmsException
	{
		// Call Ancestor Code first to setup all transaction objects
		super.populateXmlDoc(outputDoc, key, locale, bankPrefix, contextPath);

		try{
			//Section 'A' -- Title Bank Logo and document reference #
			sectionA( outputDoc);
			//Section 'B' -- Operational Bank Org info
			sectionB( outputDoc);
			
			//Section 'C' -- Settlement Instructions
			sectionC( outputDoc);
			
			//Section 'D' -- Payment Details & Other/Additional Instructions
			sectionD( outputDoc);
			
			//Section 'E' -- FX Rate Details
			sectionE( outputDoc);

			LOG.debug("OutputDoc == " + outputDoc.toString() );

		}finally{  //remove any beans for clean up if they have data
			cleanUpBeans();
		}

		return outputDoc;
	}

	protected void sectionA(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionASettleInstr(outputDoc, "Discrepancy Response");
	}

	protected void sectionB(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Customize Code

		// Call Parent Method
		super.sectionB(outputDoc);
	}

	protected void sectionC(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		String counterPartyName = "";
		if (StringFunction.isNotBlank(counterParty.getAttribute("name"))){
		      counterPartyName = counterParty.getAttribute("name");
		}
		
		String amtCurrCode = "";
		if(StringFunction.isNotBlank(terms.getAttribute("amount_currency_code"))){
			amtCurrCode = terms.getAttribute("amount_currency_code");
		}
		String amount = "";
		if(StringFunction.isNotBlank(terms.getAttribute("amount"))){
			amount = terms.getAttribute("amount");
		}
		
		String presentationDate = "";
	  	DocumentHandler results = DatabaseQueryBean.getXmlResultSet(
	  			"select presentation_date from mail_message where  response_transaction_oid= ?",
	  			false, new Object[]{transaction.getAttribute("transaction_oid")});

	  	if(results != null) {
			if(results.getAttribute("/ResultSetRecord(0)/PRESENTATION_DATE") != null){
				presentationDate = TPDateTimeUtility.formatDate(results.getAttribute("/ResultSetRecord(0)/PRESENTATION_DATE"), TPDateTimeUtility.SHORT, formLocale);
			}
		}
		
		//Settlement Instructions
		outputDoc.setAttribute( "/SectionC/SettleInstr/Title", "SETTLEMENT INSTRUCTIONS");
		
		String presentAmt = amtCurrCode+" "+TPCurrencyUtility.getDisplayAmount(amount, amtCurrCode, formLocale);
		outputDoc.setAttribute( "/SectionC/SettleInstr/PresentAmt", presentAmt);
		
		addNonNullDocData(terms.getAttribute("drawing_number"), outputDoc, "/SectionC/SettleInstr/PresentNum");
		
		addNonNullDocData(presentationDate, outputDoc, "/SectionC/SettleInstr/PresentDate");
		
		addNonNullDocData(counterPartyName, outputDoc, "/SectionC/SettleInstr/OtherParty");
		
		if(StringFunction.isNotBlank(terms.getAttribute("apply_payment_on_date"))){
			outputDoc.setAttribute( "/SectionC/SettleInstr/ApplyPaymentOn", 
					TPDateTimeUtility.formatMessageDate(new Date(terms.getAttribute("apply_payment_on_date")), formLocale));
		}
		
		outputDoc.setAttribute( "/SectionC/SettleInstr/SubTitle", 
				"Please see the following instructions with regards to discrepancy notice under the above drawing:");
			
		if(instrument.getAttribute("instrument_type_code").equals(InstrumentType.EXPORT_DLC) ||
				instrument.getAttribute("instrument_type_code").equals(InstrumentType.INCOMING_SLC)){
			if(StringFunction.isNotBlank(terms.getAttribute("export_discrepancy_instr"))){
				if(TradePortalConstants.EXP_SEND_APPR.equals(terms.getAttribute("export_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Send documents for approval");
				}else if(TradePortalConstants.EXP_SWIFT_AND_HOLD.equals(terms.getAttribute("export_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Seek approval via SWIFT and hold documents");
				}else if(TradePortalConstants.EXP_SWIFT_AND_SEND.equals(terms.getAttribute("export_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Seek approval via SWIFT and send documents");
				}else if(TradePortalConstants.EXP_OTHER.equals(terms.getAttribute("export_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Other: "+terms.getAttribute("instr_other"));
				}
			}
		}else{
			if(StringFunction.isNotBlank(terms.getAttribute("import_discrepancy_instr"))) {
				if(TradePortalConstants.IMP_ACC_NO_DCR.equals(terms.getAttribute("import_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Documents are accepted notwithstanding discrepancies");
				}else if(TradePortalConstants.IMP_REJ.equals(terms.getAttribute("import_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Documents are rejected");
				}else if(TradePortalConstants.IMP_OTHER.equals(terms.getAttribute("import_discrepancy_instr"))){
					outputDoc.setAttribute( "/SectionC/SettleInstr/Discrepancies", "Other: "+terms.getAttribute("instr_other"));
				}
			}
		}
	}

	protected void sectionD(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionDSettleInstrFIFC(outputDoc);
	}

	protected void sectionE(DocumentHandler outputDoc)
	throws RemoteException, AmsException
	{
		// Call Parent Method
		super.sectionESettleInstr(outputDoc);
	}
}
