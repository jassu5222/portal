package com.ams.tradeportal.producer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.common.NumberValidator;
import com.ams.tradeportal.common.TPCurrencyUtility;
import com.amsinc.ecsg.frame.InvalidAttributeValueException;
import com.amsinc.ecsg.util.StringFunction;

/**
 * This class is used to convert a number into it's textual representation.
 *
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class SpellItOut
 {
private static final Logger LOG = LoggerFactory.getLogger(SpellItOut.class);


    /**
     * This method calls the spellout method and passes the parsed whole number portion
     * of the passed in number.  The idea is, we get a number passed in, then we break it
     * up into 2 pieces: data left and right of the decimal seperator.  Finally, we call
     * the spell out to get the textual representation of the number passed to it.  With
     * the result string, we append the decimal value by creating a string: the decimal
     * value + "/100".   the net result then is returned.
     *
     * @param Number - number to be spelled out.
     * @param locale - Users Current Locale.
     *
     * @return Text data of number passed in.
     */
	public static String spellOutNumber(String number, String localeStr)
	       throws InvalidAttributeValueException
 {
		return spellOutNumber(number, localeStr, null);
	}

	public static String spellOutNumber(String number, String localeStr, String decimalPoint) throws InvalidAttributeValueException
    {
        String resultString = "** Building textual representation of number failed **";

        //SSikhakolli - Rel 9.3.5 BTMU PreUAT IR#T36000045432 - Begin
        //Find out the decimal placeses in the passed in Amount. if it exists split the Amount or just pass it to spellOut().
        if (getNumberOfDecimalPlace(number) == 0){
        	number = NumberValidator.getNonInternationalizedValue( number, localeStr, false );
        	resultString = spellOut( Long.parseLong(number) );
        }else{
        	String[] numberArray = new String[2];
			numberArray = splitNumber(number, localeStr, decimalPoint);
            Long spellNum = new Long( numberArray[0] );
            resultString = spellOut( spellNum.longValue() );
            
            int decNum = numberArray[1].toString().length();
            if (decNum == 3){
            	resultString = resultString + " and " + numberArray[1] + "/1000 ";
            }else{
            	resultString = resultString + " and " + numberArray[1] + "/100 ";
            }
        }

        return resultString;
    }


    /**
     * This method simply returns the number in an array after breaking up the number into
     * 2 pieces: index #1 == the data left of the decimal separator, and index #2 == the
     * data to the right of the decimal separator.
     *
     * @param Number        - to be broken up and passed out.
     * @param localeStr     - the users current locale
     *
     * @return NumberArray  - the resulting array from the number.
     */
	private static String[] splitNumber(String number, String localeStr, String decimalPoint)
	       throws InvalidAttributeValueException
    {
        String [] numberArray = {"0", "00"};            //default if all else fails...
        try
        {
            if( (number == null) || (number.equals("")) )
                return numberArray;
            else
            {
				int i = 2;
				if (StringFunction.isNotBlank(decimalPoint)) {
					i = Integer.valueOf(decimalPoint);
				}
         		// Format the number so that to the right of the decimal is always two digits
         		// This is how it was designed for the PDF form.
				number = TPCurrencyUtility.getDecimalDisplayAmount(number, i, localeStr);

                String num = "";
                num = NumberValidator.getNonInternationalizedValue( number, localeStr, false );

		        // In the off chance that a null value for the locale was entered...Default to US
		        if( localeStr == null)
			        localeStr = "en_US";

                Locale locale = new Locale( localeStr.substring(0,2), localeStr.substring(3,5) );
                DecimalFormatSymbols dfs =
			        ( (DecimalFormat) NumberFormat.getNumberInstance(locale)).getDecimalFormatSymbols();
               
                //Split up the number here (based on current locales decimal separator
                Character decimalSeparator = new Character( dfs.getDecimalSeparator() );
		        /* KMehta IR-T36000025561 @Rel-8400 on 5 Mar 2014
		         * Changed locale specific decimalSeparator to dot operator */ 		        
                StringTokenizer tokens = new StringTokenizer( num, ".", false );

                //Assign the array here
                int max = tokens.countTokens();
                for( int cnt = 0; cnt < max; cnt++)
                {
                    numberArray[cnt] = tokens.nextToken();
                }
            }
            //If this fails becasue someone typed in text rather than a valid # - send out 0.00
        }catch( NumberFormatException e ){ }

        return numberArray;
    }


    /**
     * This method simply looks up a value of a number in constant arrays to spell out the number.
     * The trick of it is to use recursion for larger numbers.
     *
     * @param Number to be spelled out (integer)
     *
     * @return Resulting String
     */
    private static String spellOut(long x)
    {
        final String[] ones      = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        final String[] teens     = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        final String[] tens      = {"", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        final String   hundred   = " hundred";
        final String[] thousands = {" thousand", " million", " billion", " trillion" };

        if (x < 10)
            return ones[new Long(x).intValue()];

        if (x < 20)
            return teens[new Long(x).intValue() - 10];

        if (x < 100)
        {
            if(x % 10 == 0)
                return tens[new Long(x).intValue() / 10];
            else
                return tens[new Long(x).intValue() / 10] + "-" + ones[new Long(x).intValue() % 10];
        }
        if (x < 1000)
        {
            if (x %100 == 0)
                return ones[new Long(x).intValue() / 100] + hundred;
            else
                return ones[new Long(x).intValue() / 100] + hundred + " " + spellOut(x % 100);
        }
        if (x < 1000000)
        {
            if (x % 1000 == 0)
                return spellOut(x / 1000) + thousands[0];
            else
                return spellOut(x / 1000) + thousands[0] + " " + spellOut(x % 1000);
        }
        if (x < 1000000000)
        {
            if (x % 1000000 == 0)
                return spellOut(x / 1000000) + thousands[1];
            else
                return spellOut(x / 1000000) + thousands[1] + " " + spellOut(x % 1000000);
        }
        if (x < 1000000000000l)
        {
            if (x % 1000000000 == 0)
                return spellOut(x / 1000000000) + thousands[2];
            else
                return spellOut(x / 1000000000) + thousands[2] + " " + spellOut(x % 1000000000);
        }

        //If we get here then the number exceeded 1 tillion? Is that possible?
        Long failed = new Long(x);
        return failed.toString();
    }
    
    /**
     * This method simply looks up for a decimal place in passed value
     *
     * @param String
     * @return int
     */
    private static int getNumberOfDecimalPlace(String value) {
        final int index = value.indexOf('.');
        if (index < 0) {
            return 0;
        }
        return value.length() - 1 - index;
    }

 }