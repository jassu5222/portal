package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.AutoExtendSession;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.StringFunction;

public class CheckSessionTimeoutServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(CheckSessionTimeoutServlet.class);

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles check session timeout.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */
	

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}

        HttpSession httpSession = request.getSession(false); //don't create a new one, return null if none
        PrintWriter out = response.getWriter();
        javax.crypto.SecretKey secretKey = null;
        if (httpSession!=null) {
       	   SessionWebBean userSession = (SessionWebBean)httpSession.getAttribute("userSession");
      	   secretKey = userSession.getSecretKey();
      	 LOG.info("Session ID in CheckSessionTimeOutServlet: ====> " + httpSession.getId());
		 LOG.info("Last Accessed Time  in CheckSessionTimeOutServlet: ====> " + httpSession.getLastAccessedTime());
		 LOG.info("Session Max In Active Interval seconds  in CheckSessionTimeOutServlet: ====> " + httpSession.getMaxInactiveInterval());
        }
        LOG.info("CheckSessionTimeOutServlet called at : ====> " + (new java.util.Date()).toGMTString());
		LOG.info("Current System Time in Milli seconds  in CheckSessionTimeOutServlet: ====> " + System.currentTimeMillis());
		
        String newMaxInactiveTimeout = new AutoExtendSession().checkIfAutoExtendSession(httpSession);
        out.print(newMaxInactiveTimeout);
        out.flush();
	}

}



