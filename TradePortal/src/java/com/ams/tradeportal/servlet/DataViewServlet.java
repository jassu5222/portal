package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jassu.factor:

import org.jassu.factorrygfvbehj

sfafasfsfadfafaf

12345678910



import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.amsinc.ecsg.frame.ErrorManager;
import com.ams.tradeportal.dataview.DataView;
import com.amsinc.ecsg.util.ResourceManager;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.amsinc.ecsg.web.FormManager;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.util.PerformanceStatistic;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.EncryptDecrypt;


/**
 * Servlet implementation class DataViewServlet
 */
public class DataViewServlet extends HttpServlet {
private static final String ACTION_LISTVIEW = "ListView";
private static final String SORT_ORDER_ASCENDING = "A";
private static final String SORT_ORDER_DESCENDING = "D";
private static final String LISTVIEW_SORT = "SORT";
private static final String LISTVIEW_SCROLL = "SCROLL";
private static final String LISTVIEW_LOAD = "LOAD";
private static final Logger LOG = LoggerFactory.getLogger(DataViewServlet.class);
	private static final long serialVersionUID = 1L;
    

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doTheWork(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException {
    	
    	//RKAZI IR 47582 24-03-2016 REL 9.5 - START
        // Create a performance statistic object to keep track of 
        // start time, action taken, and stop time
        PerformanceStatistic perfStat = new PerformanceStatistic ();


        // Start the timer
        perfStat.startTimer();

      //RKAZI IR 47582 24-03-2016 REL 9.5 - END
        
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}

	    //get the user's resource manager from the session
        HttpSession session = request.getSession(false); //don't create a new one, return null if none
        ResourceManager resMgr = null;
        SessionWebBean userSession = null;
        FormManager formMgr = null;
        BeanManager beanMgr = null;
        if (session!=null) {
            resMgr = (ResourceManager) session.getAttribute("resMgr");
            userSession = (SessionWebBean) session.getAttribute("userSession");
            formMgr = (FormManager) session.getAttribute("formMgr");
            beanMgr = (BeanManager) session.getAttribute("beanMgr");
        }
        else {
            throw new ServletException("Could not obtain session.");
        }
        if (resMgr==null) {
            //this would only happen if the calling servlet did not set the resource manager bean
            throw new ServletException("Could not obtain resource manager.");
        }
        if (userSession==null) {
            //this would only happen if the calling servlet did not set the user session bean
            throw new ServletException("Could not obtain user session bean.");
        }
        if (formMgr==null) {
            //this would only happen if the calling servlet did not set the form manager bean
            throw new ServletException("Could not obtain form manager.");
        }
        if (beanMgr==null) {
            //this would only happen if the calling servlet did not set the bean manager bean
            throw new ServletException("Could not obtain bean manager.");
        }
        
        response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
        response.setHeader("Pragma","no-cache"); //HTTP 1.0
        response.setDateHeader ("Expires", 0);

	    //build parameter map
        HashMap<String,String> parameterMap = new HashMap<String,String>();

	    //loop through - just stuff everything in there
        Enumeration<String> parameterNames = request.getParameterNames();
        if(null != parameterNames){
            while(parameterNames.hasMoreElements()){
                String paramName = parameterNames.nextElement();
                String paramValue = request.getParameter(paramName);
                
                	if(!"vName".equals(paramName)) {
                	  paramValue = paramValue.trim();
                	}
                //cquinton 3/6/2013 reconstitute url parameters
                // this is safe here only because we know we are NOT
                // inserting this data into the database!
                paramValue = StringFunction.xssHtmlToChars(paramValue);
                
                parameterMap.put(paramName, paramValue);
            }
        }

        //now verify we got the required ones and default those that need it

        String viewName = parameterMap.get("vName");
        viewName = EncryptDecrypt.decryptStringUsingTripleDes(viewName, userSession.getSecretKey()); //Rel9.2 IR T36000032596 - Decrypting viewName
        if ( null == viewName ) {
            throw new ServletException("Missing required vName parameter");
        }

       
        String formatType = parameterMap.get("fmt");
        if ( null == formatType ) {
            formatType = "JSON";
        }

        // Get Start Index and fetch size from request. These are appended to service from ItemDataStore AJAX call
        String start = parameterMap.get("start");
        if ( null == start ) {
            throw new ServletException("Missing required start parameter");
        }
        String count = parameterMap.get("count");
        if ( null == count ) {
            throw new ServletException("Missing required count parameter");
        }


        //now pull in the data view based on the view name
        //use reflection -- the dataview name must match object of same name
        DataView dataViewObject = null;
        try {
            String className = "com.ams.tradeportal.dataview." + viewName;
            Class dataViewClass = Class.forName(className);
            dataViewObject =
                (DataView) dataViewClass.newInstance();
        } catch (Exception ex) {
            throw new ServletException("Problem instantiating " + viewName + " data view", ex);
        }
        
        //todo: include this in the constructor
        dataViewObject.setResourceManager(resMgr);
        dataViewObject.setUserSession(userSession);
        dataViewObject.setFormManager(formMgr);
        dataViewObject.setBeanManager(beanMgr);
        dataViewObject.setHttpResponse(response);
        //set the error manager locale
        dataViewObject.getErrorManager().setLocaleName(resMgr.getResourceLocale());

        String payload = "";
        try {
            payload = dataViewObject.getData(viewName, parameterMap, formatType, true);
        } catch (Exception ex) {
            LOG.error("Problem getting {} data view data.", viewName, ex);
            throw new ServletException("Problem getting data view data.", ex);
            
        }
        if(null != payload){
			//LOG.info("DataViewServlet: Payload for " + request.getRequestURI() + "?" + request.getQueryString() + " is - " + payload);
        	LOG.info("DataViewServlet: Payload for " + request.getRequestURI() + "?" + request.getQueryString() + " is - " + payload);
		}else{
			payload = "";
		}
        
        //RKAZI IR 47582 24-03-2016 REL 9.5 - START
        //Store grids sortcolumn and sortorder into session so that when grid data is fetched, based on the saved
        //sort column and sort order we can determine if the action performed
        // is SORT. we need to save this is session since from UI we do not send
        //any info back to DataView Servlet indicating the fetch is for SORT
        // or LOAD or SCROLL ACTION performed on the UI.
        @SuppressWarnings("unchecked")
		Map<String,String> prevSortColumnMap = (Map<String,String>)session.getAttribute("prevSortColumnMap");
        if(prevSortColumnMap ==null){
        	prevSortColumnMap = new HashMap<String,String>();
        	String val = dataViewObject.getSortColumn() +"~"+(parameterMap.get("sort").startsWith("-") ? SORT_ORDER_DESCENDING:SORT_ORDER_ASCENDING);
        	prevSortColumnMap.put(viewName, val);
        }
        //RKAZI IR 47582 24-03-2016 REL 9.5 - END
        
        //Dec. 15 2012 Prateep Fix for Problem of loading non english characters in json data Start 
        response.setCharacterEncoding("UTF-8");
        //Dec. 15 2012 Prateep Fix for Problem of loading non english characters in json data End
        if ( dataViewObject.getErrorManager().getErrorCount()>0 &&
             dataViewObject.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY ) {
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR); //500
            //get xml from the error manager            
            payload = dataViewObject.getErrorDoc().toString();
            PrintWriter writer = response.getWriter();
            writer.write(payload);
        }
        else {
            response.setContentType("application/json");       
            PrintWriter writer = response.getWriter();
            writer.write(payload);
        }
        
        //RKAZI IR 47582 24-03-2016 REL 9.5 - START
        // Save performance stat data
        if (perfStat != null){
        	if (session.getAttribute("performanceStatistic")  != null){
        		PerformanceStatistic perfSt = (PerformanceStatistic) session.getAttribute("performanceStatistic");
	        	String context = perfSt.getContext();
	        	if (StringFunction.isBlank(context)){
	        		context = perfSt.getAction();
	        	}
	        	
	        	perfStat.setAction(ACTION_LISTVIEW); // Since this is action performed on GRIDS. save the action as LISTVIEW.
	        	perfStat.setContext(context+"."+viewName);// Context created based on the page name and the gird for which action is performed.
	        	perfStat.setNavId(perfSt.getNavId());
	        	perfStat.setUserOid(userSession.getActiveUserSessionOid());
	        	perfStat.setOrganizationCode(userSession.getClientBankCode());
	        	perfStat.setServerName(userSession.getServerName());
	        	perfStat.setTimeType(perfSt.getTimeType());
	        	perfStat.setToPage(perfSt.getToPage());
	        	perfStat.setFromPage(perfSt.getFromPage());
	        	perfStat.setaObjectOid(perfSt.getaObjectOid());
	        	int metaDataSortColumn = com.ams.tradeportal.dataview.DataViewManager.getDataViewMetadata(viewName).sortColumn;
	        	
	        	int currentSortColumn = dataViewObject.getSortColumn();
	        	// if the sort parameter value starts with "-" it means  
	        	// column is sorted in descending order and vice versa.
				//Srinivasu_D IR#T36000047761 Rel9.5 04/05/2016 handling NPE in case of sort is null - start
	        	//String currentSortOrder = parameterMap.get("sort").startsWith("-") ? SORT_ORDER_DESCENDING:SORT_ORDER_ASCENDING;
				String currentSortOrder = SORT_ORDER_ASCENDING;
				if(StringFunction.isNotBlank(parameterMap.get("sort"))){
					currentSortOrder = parameterMap.get("sort").startsWith("-") ? SORT_ORDER_DESCENDING:SORT_ORDER_ASCENDING;
				}
				//Srinivasu_D IR#T36000047761 Rel9.5 04/05/2016 handling NPE in case of sort is null - End
	        	//currentSortColumn = sortDesc ? -currentSortColumn : currentSortColumn;
	        	String prevSortOrder = currentSortOrder;
	        	int prevSortColumn = currentSortColumn;
	        	if (prevSortColumnMap.get(viewName) != null){
	        		String prevVal = (String)prevSortColumnMap.get(viewName);
	        		String prevSortCol = prevVal.split("~")[0];
	        		 prevSortOrder = prevVal.split("~")[1];
	        		prevSortColumn= Integer.valueOf(prevSortCol).intValue();
	        		
	        	}
	        	//isSortAction uses currentSortColumn and prevSort column values to determine if grid data fetch 
	        	// is invoked due to column SORT. TO achieve this it looks at the 
	        	//current sort column value and the prev Sort column value, if both
	        	// different then the grid data fetch was done due to column sort
	        	// If the same column is sorted in Ascending/Descending order then
	        	// currentSortColumn and prevSort column are same but currentsortorder
	        	// is different from prevSortOrder then also it is a SORT Action.
	        	boolean isSortAction = ((currentSortColumn != prevSortColumn)) | (!currentSortOrder.equalsIgnoreCase(prevSortOrder));
	        	String actionType = LISTVIEW_LOAD;
	        	if (Integer.valueOf(start).intValue() != 0){ // if the start parameter value is greater then 0 it means grid scroll action is performed.
	        		actionType =LISTVIEW_SCROLL;
	        	}else if (isSortAction){
	        		actionType =LISTVIEW_SORT;
	        	}
	        	
	        	// Save current currentSortColumn and currentSortOder in session as prev values for next data fetch action determination
	        	prevSortColumnMap.put(viewName, currentSortColumn+"~"+currentSortOrder);
	        	session.setAttribute("prevSortColumnMap", prevSortColumnMap);
	        	
	        	//Set actionType
	        	perfStat.setActionType(actionType);
	        	//add performance data to cache for saving.
	        	perfStat.stopTimer();
        	}
        }
        //RKAZI IR 47582 24-03-2016 REL 9.5 - END
	}

}
