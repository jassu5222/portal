package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @(#)DirectDebitFileUploadServlet
 *
 */

import com.ams.tradeportal.busobj.*;

import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.*;
import javax.ejb.RemoveException;

/**
 * The DirectDebitFileUploadServlet class is used when a user
 * uploads a Payment data file to the Trade Portal for processing.
 *
 * CR-507 12/2009 - Payment Transaction Enhancement
 * Release 5.2.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */

/**
 *
 *  Payment File Format - pipe-delimited
 */

public class DirectDebitFileUploadServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(DirectDebitFileUploadServlet.class);

    private static final String paymentDataDelimiter         = "\\|";

    // Payment file layout
    private static final int   PAYMENT_METHOD                                  = 0;
    private static final int   DEBIT_ACCOUNT_NO                                = 1;
    private static final int   BENEFICIARY_NAME                                = 2;
    private static final int   BENEFICIARY_ACCOUNT_NO                          = 3;
    private static final int   BENEFICIARY_BANK_BRANCH_CODE                    = 4;
    private static final int   PAYMENT_CURRENCY                                = 5;
    private static final int   PAYMENT_AMOUNT                                  = 6;
    private static final int   CUSTOMER_REFERENCE                              = 7;
    private static final int   COUNTRY                                         = 8;
    private static final int   EXECUTION_DATE                                  = 9;
    private static final int   BENEFICIARY_ADDRESS1                            = 10;
    private static final int   BENEFICIARY_ADDRESS2                            = 11;
    private static final int   BENEFICIARY_ADDRESS3                            = 12;
    private static final int   BENEFICIARY_ADDRESS4                            = 13;
    private static final int   BENEFICIARY_FAX_NO                              = 14;
    private static final int   BENEFICIARY_EMAIL_ID                            = 15;
    private static final int   BENEFICIARY_BANK_NAME                           = 16;
    private static final int   BENEFICIARY_BANK_BRANCH_NAME                    = 17;
    private static final int   BENEFICIARY_BANK_BRANCH_ADDRESS1                = 18;
    private static final int   BENEFICIARY_BANK_BRANCH_ADDRESS2                = 19;
    private static final int   BENEFICIARY_BANK_BRANCH_CITY                    = 20;
    private static final int   BENEFICIARY_BANK_BRANCH_PROVINCE                = 21;
    private static final int   BENEFICIARY_BANK_BRANCH_COUNTRY                 = 22;
    private static final int   CHARGES                                         = 23;
    private static final int   PAYMENT_DETAILS                                 = 24;
    private static final int   ORDERING_PARTY                                  = 25;
    private static final int   ORDERING_PARTY_ADDRESS1                         = 26;
    private static final int   ORDERING_PARTY_ADDRESS2                         = 27;
    private static final int   ORDERING_PARTY_ADDRESS3                         = 28;
    private static final int   ORDERING_PARTY_ADDRESS4                         = 29;
    private static final int   ORDERING_PARTY_COUNTRY                          = 30;	//Vshah IR-SLUK021033331 05/05/10 ADD
    private static final int   ORDERING_PARTY_BANK_BRANCH_CODE                 = 31;
    private static final int   ORDERING_PARTY_BANK_NAME                        = 32;
    private static final int   ORDERING_PARTY_BANK_BRANCH_ADDRESS1             = 33;
    private static final int   ORDERING_PARTY_BANK_BRANCH_ADDRESS2             = 34;

    // max # of fields per line.
    private static final int   MAX_PAYMENT_FILE_FIELDS_NUMBER                  = ORDERING_PARTY_BANK_BRANCH_ADDRESS2 + 1;
    // Size of each field.  Only applicable to text fields
    private int[] fieldSize = new int[MAX_PAYMENT_FILE_FIELDS_NUMBER];
    // The TextResrouce key of the name of the fields, used for error message.
    private String[] fieldNameKey = new String[MAX_PAYMENT_FILE_FIELDS_NUMBER];

    {
        // Static initialization of fieldSize[]
        fieldSize[PAYMENT_METHOD]                                  = 0;
        fieldSize[DEBIT_ACCOUNT_NO]                                = 30;
        fieldSize[BENEFICIARY_NAME]                                = 140;
        fieldSize[BENEFICIARY_ACCOUNT_NO]                          = 34;
        fieldSize[BENEFICIARY_BANK_BRANCH_CODE]                    = 35;
        fieldSize[PAYMENT_CURRENCY]                                = 3;
        fieldSize[PAYMENT_AMOUNT]                                  = 0;
        fieldSize[CUSTOMER_REFERENCE]                              = 20;
        fieldSize[COUNTRY]                                         = 0;
        fieldSize[EXECUTION_DATE]                                  = 0;
        fieldSize[BENEFICIARY_ADDRESS1]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS2]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS3]                            = 35;
        fieldSize[BENEFICIARY_ADDRESS4]                            = 35;
        fieldSize[BENEFICIARY_FAX_NO]                              = 15;
        fieldSize[BENEFICIARY_EMAIL_ID]                            = 255;
        fieldSize[BENEFICIARY_BANK_NAME]                           = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_NAME]                    = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS1]                = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_ADDRESS2]                = 35;
        fieldSize[BENEFICIARY_BANK_BRANCH_CITY]                    = 0;
        fieldSize[BENEFICIARY_BANK_BRANCH_PROVINCE]                = 0;
        fieldSize[BENEFICIARY_BANK_BRANCH_COUNTRY]                 = 0;
        fieldSize[CHARGES]                                         = 1;
        fieldSize[PAYMENT_DETAILS]                                 = 140;
        fieldSize[ORDERING_PARTY]                                  = 35;
        fieldSize[ORDERING_PARTY_ADDRESS1]                         = 35;
        fieldSize[ORDERING_PARTY_ADDRESS2]                         = 35;
        fieldSize[ORDERING_PARTY_ADDRESS3]                         = 35;
        fieldSize[ORDERING_PARTY_ADDRESS4]                         = 35;
        fieldSize[ORDERING_PARTY_COUNTRY]                          = 0;  //Vshah IR-SLUK021033331 05/05/10 ADD
        fieldSize[ORDERING_PARTY_BANK_BRANCH_CODE]                 = 35;
        fieldSize[ORDERING_PARTY_BANK_NAME]                        = 35;
        fieldSize[ORDERING_PARTY_BANK_BRANCH_ADDRESS1]             = 35;
        fieldSize[ORDERING_PARTY_BANK_BRANCH_ADDRESS2]             = 35;

        // Static initialization of fieldNameKey[]
        fieldNameKey[PAYMENT_METHOD]                                  = "DirectDebitInstruction.PaymentMethod";
        fieldNameKey[DEBIT_ACCOUNT_NO]                                = "DirectDebitInstruction.CreditToAccount";
        fieldNameKey[BENEFICIARY_NAME]                                = "DirectDebitInstruction.Payer";
        fieldNameKey[BENEFICIARY_ACCOUNT_NO]                          = "DirectDebitInstruction.PayerAccountNumber";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CODE]                    = "DirectDebitInstruction.PayerBankCode";
        fieldNameKey[PAYMENT_CURRENCY]                                = "DomesticPaymentRequest.PaymentCurrency";
        fieldNameKey[PAYMENT_AMOUNT]                                  = "";
        fieldNameKey[CUSTOMER_REFERENCE]                              = "DirectDebitInstruction.BOReference";
        fieldNameKey[COUNTRY]                                         = "";
        fieldNameKey[EXECUTION_DATE]                                  = "";
        fieldNameKey[BENEFICIARY_ADDRESS1]                            = "DirectDebitInstruction.PayerAddress";
        fieldNameKey[BENEFICIARY_ADDRESS2]                            = "DirectDebitInstruction.PayerAddress";
        fieldNameKey[BENEFICIARY_ADDRESS3]                            = "DirectDebitInstruction.PayerAddress";
        fieldNameKey[BENEFICIARY_ADDRESS4]                            = "DirectDebitInstruction.PayerAddress";
        fieldNameKey[BENEFICIARY_FAX_NO]                              = "DirectDebitInstruction.PayerFax";
        fieldNameKey[BENEFICIARY_EMAIL_ID]                            = "DirectDebitInstruction.PayerEmail";
        fieldNameKey[BENEFICIARY_BANK_NAME]                           = "DirectDebitInstruction.PayerBankName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_NAME]                    = "DirectDebitInstruction.PayerBankBranchName";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS1]                = "DirectDebitInstruction.PayerBankTheAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_ADDRESS2]                = "DirectDebitInstruction.PayerBankTheAddress";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_CITY]                    = "";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_PROVINCE]                = "";
        fieldNameKey[BENEFICIARY_BANK_BRANCH_COUNTRY]                 = "";
        fieldNameKey[CHARGES]                                         = "DirectDebitInstruction.Charges";
        fieldNameKey[PAYMENT_DETAILS]                                 = "DirectDebitInstruction.PaymentDetails";
        fieldNameKey[ORDERING_PARTY]                                  = "DirectDebitInstruction.OrderingParty";
        fieldNameKey[ORDERING_PARTY_ADDRESS1]                         = "DirectDebitInstruction.OrderPartyAddress";
        fieldNameKey[ORDERING_PARTY_ADDRESS2]                         = "DirectDebitInstruction.OrderPartyAddress";
        fieldNameKey[ORDERING_PARTY_ADDRESS3]                         = "DirectDebitInstruction.OrderPartyAddress";
        fieldNameKey[ORDERING_PARTY_ADDRESS4]                         = "DirectDebitInstruction.OrderPartyAddress";
        fieldNameKey[ORDERING_PARTY_COUNTRY]						  = "DirectDebitInstruction.Country";  //Vshah IR-SLUK021033331 05/05/10 ADD
        fieldNameKey[ORDERING_PARTY_BANK_BRANCH_CODE]                 = "DirectDebitInstruction.OrderPartyBankBranchCode";
        fieldNameKey[ORDERING_PARTY_BANK_NAME]                        = "DirectDebitInstruction.OrderPartyBank";
        fieldNameKey[ORDERING_PARTY_BANK_BRANCH_ADDRESS1]             = "DirectDebitInstruction.OrderPartyBankAddress";
        fieldNameKey[ORDERING_PARTY_BANK_BRANCH_ADDRESS2]             = "DirectDebitInstruction.OrderPartyBankAddress";
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doTheWork(request, response);
    }

    /**
     * This method implements the standard servlet API for GET and POST
     * requests. It handles uploading of files to the Trade Portal.
     *
     * @param      javax.servlet.http.HttpServletRequest  request  - the Http servlet request object
     * @param      javax.servlet.http.HttpServletResponse response - the Http servlet response object
     * @exception  javax.servlet.ServletException
     * @exception  java.io.IOException
     * @return     void
     */
    public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}

        RequestParser   requestParser     = new RequestParser();
        Hashtable       formData          = null;

        // Retrieve the form manager and resource manager from the session so that
        // we can issue errors or create an payment upload record if necessary
        HttpSession session = request.getSession(true);
        BeanManager beanMgr = (BeanManager) session.getAttribute("beanMgr");
        ResourceManager resourceManager = (ResourceManager) session.getAttribute("resMgr");
        FormManager formManager = (FormManager) session.getAttribute("formMgr");
        SessionWebBean userSession = (SessionWebBean) session.getAttribute("userSession");

        String pageOriginator = "InstrumentNavigator";

        // Retrieve all form data, including the file contents
        formData = requestParser.parseRequest(request);

        try {
            MediatorServices mediatorServices = new MediatorServices();
            mediatorServices.getErrorManager().setLocaleName(userSession.getUserLocale());
            DocumentHandler xmlDoc = new DocumentHandler();
            // Get the full path of the Payment data file
            String paymentDataFilePath = (String) formData.get("filePath");

            // Get the actual Payment data file
           
            String  paymentDataFileContents = requestParser.getFileContentAsString();
           
            if ((paymentDataFilePath.equals("")) || (paymentDataFileContents.trim().equals(""))) {
                String[] substitutionValues = new String[1];
                substitutionValues[0] = paymentDataFilePath;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_NOT_FOUND, substitutionValues);
                formManager.setCurrPage("UploadDirectDebitFile", true);
            }
            else if  (!uploadPaymentData(paymentDataFileContents, beanMgr, resourceManager, userSession, mediatorServices)) {
               formManager.setCurrPage("UploadDirectDebitFile", true);
            }
            else {
               formManager.setCurrPage(pageOriginator, true);
            }
            mediatorServices.addErrorInfo();
            xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
            xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
            formManager.storeInDocCache("default.doc", xmlDoc);
            return;
        } catch (AmsException e) {
            e.printStackTrace();
            formManager.setCurrPage("UploadDirectDebitFile", true);
            return;
        }
        catch (RemoveException e) {
            e.printStackTrace();
            formManager.setCurrPage("UploadPaymentFile", true);
            return;

        }
    }

    /**
     *
     * Upload the payment data in a file.
     *
     * @param paymentDataFileContents
     * @param beanMgr
     * @param resourceManager
     * @param userSession
     * @param mediatorServices
     * @return boolean
     *         true - success
     *         false = failure
     * @throws AmsException
     * @throws RemoteException
     */
    private boolean uploadPaymentData(String paymentDataFileContents, BeanManager beanMgr, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices)
    throws AmsException, RemoteException, RemoveException {

        int lineBeginIndex = 0;
        int lineEndIndex = 0;
        String currentPaymentLineItem = null;

        // Get the current TransactionWebBean, which has the info of the current transaction
        TransactionWebBean transaction  = (TransactionWebBean)beanMgr.getBean("Transaction");
        Long transactionOidL = new Long(transaction.getAttribute("transaction_oid"));

        String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        ClientServerDataBridge csdb = resourceManager.getCSDB();
        // Delete each DomesticPayment individually since deleteComponent is restricted by the EJB pool limit.
        try {
            String dpSqlQuery = "SELECT DP.DOMESTIC_PAYMENT_OID FROM DOMESTIC_PAYMENT DP WHERE DP.p_transaction_oid = ? ";
            DocumentHandler resultsDoc = DatabaseQueryBean.getXmlResultSet(dpSqlQuery,true, new Object[]{String.valueOf(transactionOidL)});

            int curIndex = 0;
            String domesticPaymentOidStr = null;
            if (resultsDoc != null)
                domesticPaymentOidStr = resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");
            else
                LOG.debug("No DomesticPayment to Delete");

            while (StringFunction.isNotBlank(domesticPaymentOidStr))  {
               DomesticPayment domesticPayment = (DomesticPayment) EJBObjectFactory.createClientEJB(serverLocation, "DomesticPayment", Long.parseLong(domesticPaymentOidStr), csdb);
               domesticPayment.delete();
               domesticPayment.remove();
               curIndex++;
               domesticPaymentOidStr = resultsDoc.getAttribute("/ResultSetRecord(" + curIndex + ")/DOMESTIC_PAYMENT_OID");
            }
        }
        catch(Exception any_exc) {
            LOG.info("Exception getting a list of Dom Pmts to delete in File Upload Process: " + any_exc.toString());
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DP_FU_CANT_DELETE_DPS_WRN);
            return false;
        }
        Transaction trans = (Transaction) EJBObjectFactory.createClientEJB(serverLocation, "Transaction", transactionOidL.longValue(), csdb);
        trans.setAttribute("copy_of_amount", "0"); // Initialize to 0.  Will add payment amount cumulatively.

        int paymentCount = 0;
        String orderingParty = null;
        String orderingPartyBank = null;
        String accountNo = null;
        String currency = null;
        while (lineBeginIndex < paymentDataFileContents.length()) {
            // Retrieve the index of the return and new line characters, which separate Payment
            // line items
            lineEndIndex = paymentDataFileContents.indexOf("\r\n", lineBeginIndex);

            // If there are no more Payment line items to process, stop processing them; otherwise,
            // retrieve the next Payment line item; if we have a blank line
            if (lineEndIndex < 0) {
                lineEndIndex = paymentDataFileContents.length();
            }

            // Get the current Payment Line item string, excluding the new line characters
            currentPaymentLineItem = paymentDataFileContents.substring(lineBeginIndex, lineEndIndex);
            lineBeginIndex = lineEndIndex + 2;
            LOG.debug("DirectDebitFileUploadServlet: currentPaymentLineItem [ " + currentPaymentLineItem + "]");

            // If we have a blank line, skip to the next Payment line item
            if ( currentPaymentLineItem == null || (currentPaymentLineItem.trim().equals(""))) {
                continue;
            }

            String[] paymentRecord = parsePaymentLineItem(currentPaymentLineItem, resourceManager, mediatorServices);

            // If the line has no | delimiter, it returns null;
            if (paymentRecord == null) {
                trans.remove();
                return false;
            }

            // Cross-line validation
            // Check values are the same for all lines.
            if (paymentCount == 0) {
                orderingParty = paymentRecord[ORDERING_PARTY];
                if (orderingParty == null) orderingParty = "";
                orderingPartyBank = paymentRecord[ORDERING_PARTY_BANK_NAME];
                if (orderingPartyBank == null) orderingPartyBank = "";
                accountNo = paymentRecord[DEBIT_ACCOUNT_NO];
                if (accountNo == null) accountNo = "";
                currency = paymentRecord[PAYMENT_CURRENCY];
                if (currency == null) currency = "";
            }
            else {
                if (paymentRecord[ORDERING_PARTY] == null) paymentRecord[ORDERING_PARTY] = "";
                if (paymentRecord[ORDERING_PARTY_BANK_NAME] == null) paymentRecord[ORDERING_PARTY_BANK_NAME] = "";
                if (paymentRecord[DEBIT_ACCOUNT_NO] == null) paymentRecord[DEBIT_ACCOUNT_NO] = "";
                if (paymentRecord[PAYMENT_CURRENCY] == null) paymentRecord[PAYMENT_CURRENCY] = "";
                if (!orderingParty.equals(paymentRecord[ORDERING_PARTY])
                    || !orderingPartyBank.equals(paymentRecord[ORDERING_PARTY_BANK_NAME])){
                    LOG.debug("DirectDebitFileUploadServlet: OrderingParty[" + paymentRecord[ORDERING_PARTY] + "] and OrderingPartyBank [" + paymentRecord[ORDERING_PARTY_BANK_NAME] + "] not the same as first line");
                    String[] substitutionValues = new String[1];
                    substitutionValues[0] = String.valueOf(paymentCount+1);
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ORDERING_PARTY_INVALID, substitutionValues);
                    return false;
                }
                if (!accountNo.equals(paymentRecord[DEBIT_ACCOUNT_NO])) {
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AC_NOT_SAME_FOR_PAYER);
                    return false;
                }
                if (!currency.equals(paymentRecord[PAYMENT_CURRENCY])) {
                    String[] substitutionValues = new String[2];
                    substitutionValues[0] = paymentRecord[PAYMENT_CURRENCY];
                    substitutionValues[1] = String.valueOf(paymentCount+1);
                    mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CURRENCY_NOT_SAME, substitutionValues);
                    return false;
                }
            }

            // Validate fields of the current Payment Line Item
            if (!validatePaymentLineItem(paymentRecord, paymentCount, beanMgr, resourceManager, userSession, mediatorServices)) {
                return false;
            }

            //Upload file to Database
            if (!uploadPaymentLineItem(trans, paymentRecord, paymentCount, beanMgr, resourceManager, userSession, mediatorServices)) {
                mediatorServices.addErrorInfo(trans.getIssuedErrors());
                trans.remove();
                return false;
            }
            paymentCount++;
        }

        if (trans.save(false) < 0) {
            mediatorServices.addErrorInfo(trans.getIssuedErrors());
            trans.remove();
            return false;
        }

        trans.remove();
        return true;
    }


/**
 * Parse one payment line item.
 *
 * @param currentPaymentLineItem
 * @param resourceManager
 * @param mediatorServices
 * @return
 * @throws AmsException
 */
    private String[] parsePaymentLineItem(String currentPaymentLineItem, ResourceManager resourceManager, MediatorServices mediatorServices)
    throws AmsException {

        String[] origPaymentRecord = currentPaymentLineItem.split(paymentDataDelimiter);
        // If there is only one field, the format cannot be valid.  The user probably has used wrong delimiter.
        if (origPaymentRecord.length == 1) {
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FILE_FORMAT_NOT_PIPE_DELIMITED);
            return null;
        }
        String[] paymentRecord = new String[MAX_PAYMENT_FILE_FIELDS_NUMBER];
        int numberOfFields = origPaymentRecord.length;
        if (numberOfFields > paymentRecord.length) numberOfFields = paymentRecord.length;
        for (int i = 0; i < numberOfFields; i++) {
            paymentRecord[i] = origPaymentRecord[i];
        }

        return paymentRecord;

    }



/**
 * Parse one payment line item.
 *
 * @param trans
 * @param paymentRecord
 * @param paymentCount
 * @param beanMgr
 * @param resourceManager
 * @param userSession
 * @param mediatorServices
 * @return
 * @throws AmsException
 * @throws RemoteException
 */

    private boolean uploadPaymentLineItem(Transaction trans, String[] paymentRecord, int paymentCount, BeanManager beanMgr, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices)
           throws AmsException, RemoteException, RemoveException {

        boolean errorFound = false;
        if (paymentCount == 0) {
            Date executionDate = null;
            executionDate = getExecutionDate(paymentRecord, userSession, mediatorServices);
            if (executionDate == null) {
               return false;
            }
            trans.setAttribute("payment_date",  new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(executionDate));
        }
        String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        ClientServerDataBridge csdb = resourceManager.getCSDB();
        DomesticPayment domesticPayment = (DomesticPayment) EJBObjectFactory.createClientEJB(serverLocation, "DomesticPayment", csdb);
        domesticPayment.newObject();
        domesticPayment.setAttribute("transaction_oid",trans.getAttribute("transaction_oid"));
        domesticPayment.setAttribute("amount",paymentRecord[PAYMENT_AMOUNT]);
        // Note: do not set value date.  It will be calculated during Verify.
        domesticPayment.setAttribute("amount_currency_code",paymentRecord[PAYMENT_CURRENCY]);
        domesticPayment.setAttribute("payment_method_type",paymentRecord[PAYMENT_METHOD]);
        domesticPayment.setAttribute("payee_name",paymentRecord[BENEFICIARY_NAME]);
        domesticPayment.setAttribute("payee_account_number",paymentRecord[BENEFICIARY_ACCOUNT_NO]);
        domesticPayment.setAttribute("payee_address_line_1",paymentRecord[BENEFICIARY_ADDRESS1]);
        domesticPayment.setAttribute("payee_address_line_2",paymentRecord[BENEFICIARY_ADDRESS2]);
        domesticPayment.setAttribute("payee_address_line_3",paymentRecord[BENEFICIARY_ADDRESS3]);
        domesticPayment.setAttribute("payee_address_line_4",paymentRecord[BENEFICIARY_ADDRESS4]);
        domesticPayment.setAttribute("payee_bank_name",paymentRecord[BENEFICIARY_BANK_NAME]);
        domesticPayment.setAttribute("payee_bank_code",paymentRecord[BENEFICIARY_BANK_BRANCH_CODE]);
        domesticPayment.setAttribute("payee_branch_name",paymentRecord[BENEFICIARY_BANK_BRANCH_NAME]);
        domesticPayment.setAttribute("address_line_1",paymentRecord[BENEFICIARY_BANK_BRANCH_ADDRESS1]);
        domesticPayment.setAttribute("address_line_2",paymentRecord[BENEFICIARY_BANK_BRANCH_ADDRESS2]);
        domesticPayment.setAttribute("address_line_3",paymentRecord[BENEFICIARY_BANK_BRANCH_CITY] + " " + paymentRecord[BENEFICIARY_BANK_BRANCH_PROVINCE]);
        domesticPayment.setAttribute("payee_bank_country",paymentRecord[BENEFICIARY_BANK_BRANCH_COUNTRY]);
        domesticPayment.setAttribute("country",paymentRecord[COUNTRY]);
        domesticPayment.setAttribute("customer_reference",paymentRecord[CUSTOMER_REFERENCE]);
        domesticPayment.setAttribute("payee_fax_number",paymentRecord[BENEFICIARY_FAX_NO]);
        domesticPayment.setAttribute("payee_email",paymentRecord[BENEFICIARY_EMAIL_ID]);
        domesticPayment.setAttribute("payee_description",paymentRecord[PAYMENT_DETAILS]);
        // Charge type defaults to OUR.  Translate from upload values to TP code values.
        if (paymentRecord[CHARGES]==null || paymentRecord[CHARGES].trim().length() == 0) {
            paymentRecord[CHARGES] = TradePortalConstants.CHARGE_OUR;
        }
        else if (paymentRecord[CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_OURS)) paymentRecord[CHARGES] = TradePortalConstants.CHARGE_OUR;
        else if (paymentRecord[CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_BEN)) paymentRecord[CHARGES] = TradePortalConstants.CHARGE_PAYER;
        else if (paymentRecord[CHARGES].equals(TradePortalConstants.CHARGE_UPLOAD_OTHER)) paymentRecord[CHARGES] = TradePortalConstants.CHARGE_SHARED;
        domesticPayment.setAttribute("bank_charges_type",paymentRecord[CHARGES]);


        // Ordering Party
        if (paymentCount == 0) {
            // Terms
            Terms terms = (Terms) trans.getComponentHandle("CustomerEnteredTerms");

            // Get Account OID
            //String accountSqlStr = "select account_oid from account where account_number = '" + paymentRecord[DEBIT_ACCOUNT_NO].trim() + "' and currency = '" + paymentRecord[PAYMENT_CURRENCY] + "' and p_owner_oid = '" + userSession.getOwnerOrgOid() + "'";     // NSX - 07/01/10 PDUK030948788
            String accountSqlStr = "select account_oid from account where account_number = ? and p_owner_oid = ?";               // NSX - 07/01/10 PDUK030948788
            DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, paymentRecord[DEBIT_ACCOUNT_NO].trim(), userSession.getOwnerOrgOid());
            if ( acctResultSet != null && !StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID"))) {
                String debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord/ACCOUNT_OID");
                terms.setAttribute("credit_account_oid",debitAccountOid);
            }
            terms.setAttribute("amount_currency_code",paymentRecord[PAYMENT_CURRENCY]);

            if (StringFunction.isBlank(terms.getAttribute("c_OrderingParty"))) {
                terms.newComponent("OrderingParty");
            }
            PaymentParty orderingParty = (PaymentParty) terms.getComponentHandle("OrderingParty");
            orderingParty.setAttribute("bank_name",paymentRecord[ORDERING_PARTY]);
            orderingParty.setAttribute("address_line_1",paymentRecord[ORDERING_PARTY_ADDRESS1]);
            orderingParty.setAttribute("address_line_2",paymentRecord[ORDERING_PARTY_ADDRESS2]);
            orderingParty.setAttribute("address_line_3",paymentRecord[ORDERING_PARTY_ADDRESS3]);
            orderingParty.setAttribute("address_line_4",paymentRecord[ORDERING_PARTY_ADDRESS4]);
            orderingParty.setAttribute("country",paymentRecord[ORDERING_PARTY_COUNTRY]);  //Vshah IR-SLUK021033331 05/05/10 ADD
            if (StringFunction.isBlank(terms.getAttribute("c_OrderingPartyBank"))) {
                terms.newComponent("OrderingPartyBank");
            }
            PaymentParty orderPartyBank = (PaymentParty) terms.getComponentHandle("OrderingPartyBank");
            orderPartyBank.setAttribute("bank_name",paymentRecord[ORDERING_PARTY_BANK_NAME]);
            orderPartyBank.setAttribute("bank_branch_code",paymentRecord[ORDERING_PARTY_BANK_BRANCH_CODE]);
            orderPartyBank.setAttribute("address_line_1",paymentRecord[ORDERING_PARTY_BANK_BRANCH_ADDRESS1]);
            orderPartyBank.setAttribute("address_line_2",paymentRecord[ORDERING_PARTY_BANK_BRANCH_ADDRESS2]);
            trans.setAttribute("copy_of_currency_code", paymentRecord[PAYMENT_CURRENCY]);
        }
        trans.setAttribute("copy_of_amount", new BigDecimal(trans.getAttribute("copy_of_amount")).add( new BigDecimal(paymentRecord[PAYMENT_AMOUNT])).toString());

        domesticPayment.save(true);
        trans.getErrorManager().addErrors(domesticPayment.getIssuedErrors());

        if (trans.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY
                || domesticPayment.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY) {
            errorFound = true;
        }
        domesticPayment.remove();
        return !errorFound;
    }

    /**
     * Verfies the Payment Line Item.  Doing the validation here instead of
     * at the business object user_validate() gives better error message information.
     *
     *
     * @exception  com.amsinc.ecsg.frame.AmsException
     * @return     boolean - indicates whether or not a valid Payment Data file
     *                       (true  - valid
     *                        false - invalid)
     */
    private boolean validatePaymentLineItem(String[] paymentRecord, int paymentCount, BeanManager beanMgr, ResourceManager resourceManager, SessionWebBean userSession, MediatorServices mediatorServices)
    throws AmsException {

        boolean invalidFormat = false;
        String[] substitutionValues = {"", ""};

        //Payment Method
        String paymentMethod = paymentRecord[PAYMENT_METHOD];
        if (paymentMethod == null || paymentMethod.trim().equals("")) {
            substitutionValues[0] = //"Payment Method";
                    resourceManager.getText("DomesticPaymentRequest.PaymentMethod", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields [" + substitutionValues + "]");
            invalidFormat = true;
        } else {
            String sqlSelect = "select count (*) as acount from refdata where table_type = 'PAYMENT_METHOD' and code = ? and upper(addl_value) = ?";
            DocumentHandler resultSet = DatabaseQueryBean.getXmlResultSet(sqlSelect, false, paymentMethod, "COLLECTION");
            String aReqCount = resultSet.getAttribute("/ResultSetRecord/ACOUNT");

            if (StringFunction.isBlank(aReqCount)||aReqCount.equals("0")) {
                substitutionValues[0] = String.valueOf(paymentCount+1);
                substitutionValues[1] = paymentMethod;
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.DP_FILE_UPLD_INVLD_PMT_MTH_CDE, substitutionValues, null);
                LOG.debug("DirectDebitFileUploadServlet: Payment Method [" + substitutionValues + "] not in RefData [" + aReqCount + "]");
                invalidFormat = true;
            }
         }

        //Payment Currency
        String paymentCurrency = paymentRecord[PAYMENT_CURRENCY];
        if (paymentCurrency == null || paymentCurrency.trim().equals("")) {
            substitutionValues[0] = //"Payment Currency";
                    resourceManager.getText("DomesticPaymentRequest.PaymentCurrency", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        //Debit Account
        String debitAccountNo = paymentRecord[DEBIT_ACCOUNT_NO];
        if (debitAccountNo == null || debitAccountNo.trim().equals("")) {
            substitutionValues[0] = //"Debit Account No";
                    resourceManager.getText("DirectDebitInstruction.CreditToAccount", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        } else {
            debitAccountNo = debitAccountNo.trim();
            String debitAccountOid = null;

            LOG.debug("DirectDebitFileUploadServlet: usp oid " + userSession.getOwnerOrgOid());
            // We only check account no here, without currency.
            String accountSqlStr = "select account_oid, available_for_direct_debit, deactivate_indicator from account where account_number = ? and p_owner_oid = ? ";

            DocumentHandler acctResultSet = DatabaseQueryBean.getXmlResultSet(accountSqlStr, false, debitAccountNo, userSession.getOwnerOrgOid());

            if ((acctResultSet == null) || (StringFunction.isBlank(acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID")))) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_CREDIT_AC);
                invalidFormat = true;
            } else if (TradePortalConstants.INDICATOR_YES.equals(acctResultSet.getAttribute("/ResultSetRecord(0)/DEACTIVATE_INDICATOR"))) {
                invalidFormat = true;
                String[] substValues = new String[1];
                substValues[0] = debitAccountNo + " (" + paymentCurrency + ")";
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.ACCOUNT_DEACTIVATED, substValues);
            } else if (! TradePortalConstants.INDICATOR_YES.equals(acctResultSet.getAttribute("/ResultSetRecord(0)/AVAILABLE_FOR_DIRECT_DEBIT"))) {
                invalidFormat = true;
                String[] substValues = new String[1];
                substValues[0] = debitAccountNo + " (" + paymentCurrency + ")";
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AC_NOTAVAIL_FOR_DDI, substValues);
            } else {
				//Vshah - IR#LKUK072938359 - Need to by-passe below verification, when File Upload is performed by a user
            	//on behalf of a Subsidiary while using a Subsidiary Access mode.
            	if (!userSession.hasSavedUserSession()) {
                	debitAccountOid = acctResultSet.getAttribute("/ResultSetRecord(0)/ACCOUNT_OID");

                	String userSecurityType = null;
                   	userSecurityType = userSession.getSecurityType();

                	if (!userSecurityType.equals(TradePortalConstants.ADMIN)) {
                	    String selectClause = "a_account_oid";
                	    String fromClause   = "USER_AUTHORIZED_ACCT";
                	    String whereClause = " p_user_oid = ?  and a_account_oid = ? ";
                	    int accountCount = DatabaseQueryBean.getCount(selectClause, fromClause, whereClause, false, userSession.getUserOid(),  debitAccountOid);
                	    if (accountCount == 0) {
                	        invalidFormat = true;
                	        String[] substValues = new String[1];
                	        substValues[0] = debitAccountNo + "(" + paymentCurrency + ")";
                	        mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.AC_NOTVALID_FOR_USER);
                	    }
					}
                }

            }
        }

        //Beneficiary Name
        String beneficiaryName = paymentRecord[BENEFICIARY_NAME];
        if (beneficiaryName == null || beneficiaryName.trim().equals("")) {
            substitutionValues[0] = resourceManager.getText("DirectDebitInstruction.Payer", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        //Beneficiary Account Number
        String beneficiaryAccountNo = paymentRecord[BENEFICIARY_ACCOUNT_NO];
        if (beneficiaryAccountNo == null || beneficiaryAccountNo.trim().equals("")) {
            substitutionValues[0] = resourceManager.getText("DirectDebitInstruction.PayerAccountNumber", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }

        //Payment Amount
        String paymentAmount = paymentRecord[PAYMENT_AMOUNT];
        if (paymentAmount == null || paymentAmount.trim().equals("")) {
            substitutionValues[0] = //"Payment Amount";
                    resourceManager.getText("DomesticPaymentRequest.PaymentAmount", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required fields : " + substitutionValues);
            invalidFormat = true;
        }
        else {
            try
            {
               // Convert the amount from its localized form to en_US form
                paymentRecord[PAYMENT_AMOUNT] =  NumberValidator.getNonInternationalizedValue(paymentRecord[PAYMENT_AMOUNT], resourceManager.getResourceLocale(), false);
                if ((StringFunction.isNotBlank(paymentRecord[PAYMENT_AMOUNT]))&&(new Float(paymentRecord[PAYMENT_AMOUNT]).intValue() != 0))
                {
                        TPCurrencyUtility.validateAmount(paymentRecord[PAYMENT_CURRENCY],
                                  paymentRecord[PAYMENT_AMOUNT],
                                  resourceManager.getText("DomesticPaymentBeanAlias.amount", TradePortalConstants.TEXT_BUNDLE),
                                  mediatorServices.getErrorManager() );
                }
            }
            catch(InvalidAttributeValueException iae)
             {
                substitutionValues[0] = resourceManager.getText("DomesticPaymentBeanAlias.amount", TradePortalConstants.TEXT_BUNDLE);
                substitutionValues[1] = paymentRecord[PAYMENT_AMOUNT];
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                          TradePortalConstants.DOM_PMT_AMOUNT_IS_INVALID_ERR, substitutionValues);
                invalidFormat = true;
             }

            if (mediatorServices.getErrorManager().getMaxErrorSeverity () >= ErrorManager.ERROR_SEVERITY)
            {
                invalidFormat = true;
            }

        }

        //Beneficiary Bank/Branch Code
        String beneficiaryBankBranchCode = paymentRecord[BENEFICIARY_BANK_BRANCH_CODE];
        if (beneficiaryBankBranchCode == null || beneficiaryBankBranchCode.trim().equals("")) {
            substitutionValues[0] = //"Beneficiary Bank Branch Code";
                    resourceManager.getText("DirectDebitInstruction.PayerBankCode", TradePortalConstants.TEXT_BUNDLE);
            substitutionValues[1] = String.valueOf(paymentCount+1);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.MISSING_REQUIRED_FILES, substitutionValues, null);
            LOG.debug("DirectDebitFileUploadServlet: Missing Required Beneficiary Bank Branch Code fields : " + substitutionValues);
            invalidFormat = true;
        }

        // Check City + State <= 31
        if ((paymentRecord[BENEFICIARY_BANK_BRANCH_CITY] + " " + paymentRecord[BENEFICIARY_BANK_BRANCH_PROVINCE]).length() > 32) {
               mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CITY_PROVINCE_MORE_THAN_31);
               invalidFormat = true;
           }

        // Check the length of individual fields.
        for (int i = 0; i< MAX_PAYMENT_FILE_FIELDS_NUMBER; i++) {

            if (paymentRecord[i] != null && fieldSize[i] > 0 && paymentRecord[i].length() > fieldSize[i]) {
                String fieldName = resourceManager.getText(fieldNameKey[i], TradePortalConstants.TEXT_BUNDLE);
                // Some fields have only generic text resources.  Need to add extra numbering.
                switch (i) {
                    case BENEFICIARY_ADDRESS1: fieldName += " 1"; break;
                    case BENEFICIARY_ADDRESS2: fieldName += " 2"; break;
                    case BENEFICIARY_ADDRESS3: fieldName += " 3"; break;
                    case BENEFICIARY_ADDRESS4: fieldName += " 4"; break;
                    case BENEFICIARY_BANK_BRANCH_ADDRESS1: fieldName += " 1"; break;
                    case BENEFICIARY_BANK_BRANCH_ADDRESS2: fieldName += " 2"; break;
                    case ORDERING_PARTY_ADDRESS1: fieldName += " 1"; break;
                    case ORDERING_PARTY_ADDRESS2: fieldName += " 2"; break;
                    case ORDERING_PARTY_ADDRESS3: fieldName += " 3"; break;
                    case ORDERING_PARTY_ADDRESS4: fieldName += " 4"; break;
                    case ORDERING_PARTY_BANK_BRANCH_ADDRESS1: fieldName += " 1"; break;
                    case ORDERING_PARTY_BANK_BRANCH_ADDRESS2: fieldName += " 2"; break;
                }
                substitutionValues[0] = fieldName;
                substitutionValues[1] = String.valueOf(paymentCount + 1);
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.FIELD_TOO_LONG, substitutionValues);
                invalidFormat = true;
            }
        }

        // Validate Payer Bank Country.  We do it here to make sure we use meaningful name for the attribute payee_bank_country in the error message.
        if(paymentRecord[BENEFICIARY_BANK_BRANCH_COUNTRY]!=null
           && !paymentRecord[BENEFICIARY_BANK_BRANCH_COUNTRY].trim().equals("")
           && !ReferenceDataManager.getRefDataMgr().checkCode("COUNTRY", paymentRecord[BENEFICIARY_BANK_BRANCH_COUNTRY])){
            substitutionValues[0] = paymentRecord[BENEFICIARY_BANK_BRANCH_COUNTRY];
            substitutionValues[1] = resourceManager.getText("DirectDebitInstruction.PayerBankCountry", TradePortalConstants.TEXT_BUNDLE);
            mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, AmsConstants.INVALID_ATTRIBUTE, substitutionValues);
            invalidFormat = true;
        }


        return !invalidFormat;
    }


    /**
     * Get the execution date.
     *
     * i.   If execution date is found it will be used for all beneficiaries
     * ii.  If no execution date is provided, the system will set the execution date to the current date
     * iii. Validate that the date is on the correct format.
     *
     * @return     Date execution_date)
     */
    private Date getExecutionDate(String[] paymentRecord, SessionWebBean userSession, MediatorServices mediatorServices)
    throws AmsException {

        String executionDate = paymentRecord[EXECUTION_DATE];
        Date execDate = null;
        LOG.debug("DirectDebitFileUploadServlet: getExecutionDate from file [ " + executionDate + "]");
        Date currentDate = GMTUtility.getGMTDateTime(true);
        Date currentLocalDate = TPDateTimeUtility.getLocalDateTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(currentDate), userSession.getTimeZone());
        currentLocalDate.setHours(0);
        currentLocalDate.setMinutes(0);
        currentLocalDate.setSeconds(0);
        if (executionDate == null || executionDate.trim().equals("")) {
            // If no execution date is provided, set the execution date to the current date
            execDate = currentLocalDate;
        } else {
            execDate = TPDateTimeUtility.convertDateStringToDate(executionDate.trim(), "dd/MM/yyyy");
            LOG.debug("DirectDebitFileUploadServlet: getExecutionDate converted [ " + executionDate + "]");
            // 2-digt year is not allowed.  execDate.getYear() returns year based on year 1900.  So any year < 1000 would have getYear() < -900.
            if (execDate == null || execDate.getYear() < -900) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.INVALID_PAYMENT_DATE, "1");
                execDate = null;
            }
            else if (execDate.before(currentLocalDate)) {
                mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.EXEC_DATE_LESS_THAN_CURRENT);
                execDate = null;
            }
        }
        LOG.debug("DirectDebitFileUploadServlet: getExecutionDate [ " + executionDate + "]");
        return execDate;
    }

}
