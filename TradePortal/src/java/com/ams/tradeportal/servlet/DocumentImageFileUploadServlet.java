package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @(#)DocumentImageFileUploadServlet
 *
 */

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.MissingResourceException;
import java.util.Vector;
import java.net.URL;
import java.net.HttpURLConnection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.IndexedRecord;
import javax.resource.cci.Interaction;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;

import com.adobe.acrobat.util.Util;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.busobj.DocumentImage;
import com.ams.tradeportal.busobj.InvoiceGroup;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.ImagingIdAndPassword;
import com.ams.tradeportal.common.ImagingServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;
import com.filenet.is.ra.cci.FN_IS_CciConnectionSpec;
import com.filenet.is.ra.cci.FN_IS_CciInteractionSpec;
import com.ams.tradeportal.mediator.InvoiceOfferMediator;



/**
 *

 *
 */
public class DocumentImageFileUploadServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(DocumentImageFileUploadServlet.class);

    public static final int DEFAULT_ADD_IMAGE_MAX_ATTEMPTS = 3;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    /**
     * This method implements the standard servlet API for GET and POST
     * requests. It handles uploading of files to the Trade Portal.
     *
     * @param javax.servlet.http.HttpServletRequest
     *            request - the Http servlet request object
     * @param javax.servlet.http.HttpServletResponse
     *            response - the Http servlet response object
     * @exception javax.servlet.ServletException
     * @exception java.io.IOException
     * @return void
     */
    private void doTheWork(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}



        HttpSession session = request.getSession(true);
        FormManager formManager = (FormManager) session.getAttribute("formMgr");
        SessionWebBean userSession = (SessionWebBean) session.getAttribute("userSession");
        ResourceManager resourceManager = (ResourceManager) session.getAttribute("resMgr");

        String pageOriginator = (String)session.getAttribute("documentImageFileUploadPageOriginator");
		
        try {
            if (TradePortalConstants.INVOICE_MGMT.equals(pageOriginator) || "InvoiceGroupDetail".equals(pageOriginator) || TradePortalConstants.INV_MGMT_LOAN_REQ_PAGE.equals(pageOriginator) ||
            		TradePortalConstants.PAY_INV_ATP_GROUP_DETAIL_PAGE.equals(pageOriginator) || TradePortalConstants.PAY_INV_LRQ_GROUP_DETAIL_PAGE.equals(pageOriginator)
            		|| TradePortalConstants.PAY_PRGM_DETAIL_PAGE.equals(pageOriginator)) {
            	processItemList(request, response, session, resourceManager,
            			formManager, userSession);
            	pageOriginator = (String)session.getAttribute("documentImageFileUploadPageOriginator");
            }
            else if ("InvoiceOffersTransactionsHome".equals(pageOriginator) || "InvoiceOffersGroupDetail".equals(pageOriginator)) {
                processInvoiceOfferAttachDoc (request, response, session, resourceManager,
            			formManager, userSession);
            }
            else   {
            	processSingleItem(request, response, session, resourceManager,
            			formManager, userSession);
            }

            formManager.setCurrPage(pageOriginator, true);
        }
        catch (AmsException e) {
            issueSystemError("doTheWork",
                    "Caught an exception with following message: "
                            + e.getMessage(), e);

            formManager.setCurrPage(pageOriginator, true);
        }

    }

    private void processSingleItem(HttpServletRequest request,
    		HttpServletResponse response, HttpSession session,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession)
    throws AmsException, ServletException, IOException {
    	String mailMessageOid = getMailMessageOid(session);
        String transactionOid = getTransactionOid(session);

        Hashtable<String, String> documentImageValues = uploadDocument(request,
        		response, session, resourceManager, formManager, userSession);

        commitToDocumentImage(documentImageValues, resourceManager,
        		formManager, userSession, transactionOid, mailMessageOid);
    }

    /*
     * CR-708B
     */
    private void processInvoiceOfferAttachDoc(HttpServletRequest request,
    		HttpServletResponse response, HttpSession session,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession)
    throws AmsException, ServletException, IOException {
    	//First upload the document to the image server
    	Hashtable<String, String> documentImageValues = uploadDocument(request,
    			response, session, resourceManager, formManager, userSession);

    	DocumentHandler inputDoc = formManager.getFromDocCache();
        java.util.Enumeration<String> keys;
        if (documentImageValues!=null){
        	keys = documentImageValues.keys();
        	while( keys.hasMoreElements() ) {
                String key = keys.nextElement();
                String value = documentImageValues.get(key);
              inputDoc.setAttribute("/In/DocImage/"+key, value);
              }
        }
       try {
          // Call the mediator
        	String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        	ClientServerDataBridge csdb = resourceManager.getCSDB();
    	  InvoiceOfferMediator mediator = (InvoiceOfferMediator) 
			         EJBObjectFactory.createClientEJB(serverLocation, "InvoiceOfferMediator");
          DocumentHandler outputDoc =  mediator.performService(inputDoc, resourceManager.getCSDB());
          mediator.remove();
       }
       catch(Exception e) {
 	      LOG.error("Error calling InvoiceOfferMediator for authentication.  System cannot proceed", e);
	      return;
       }
        
    }
    
    private void processItemList(HttpServletRequest request,
    		HttpServletResponse response, HttpSession session,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession)
    throws AmsException, ServletException, IOException {
    	//First upload the document to the image server
    	Hashtable<String, String> documentImageValues = uploadDocument(request,
    			response, session, resourceManager, formManager, userSession);
        // Calls to the uploadDocument method can be replaced with calls to
        // testUploadDocument to test this servlet in environments that are
        // not connected an imaging server.


    	DocumentHandler inputDoc = formManager.getFromDocCache();

		//CR-708 for handling Payable invoice Start
		String isPayable = inputDoc.getAttribute("/In/UploadInvoiceList/fromPayDetail");
		String isPayableGroup = inputDoc.getAttribute("/In/InvoiceGroupList/fromPayGroup");	
		String isRecGroup = inputDoc.getAttribute("/In/InvoiceGroupList/fromRecGroup");
		String isRec = inputDoc.getAttribute("/In/UploadInvoiceList/fromRecDetail");
		if("yes".equals(isPayable) || "yes".equals(isRec)){		
	    	
	    	inputDoc = updateAttachmentForPayableDetail(resourceManager,inputDoc);
		}
		//CR-708 for handling Payable invoice End
		String idTag = "InvoiceData";
		List<DocumentHandler> itemList = inputDoc.getFragmentsList("/In/UploadInvoiceList/InvoicesSummaryData");
		if((itemList == null) || (itemList.size() == 0)){
			itemList = inputDoc.getFragmentsList("/In/UploadCreditNote/CreditNote");
			if(itemList != null && itemList.size() > 0){
				idTag = "CreditData";
			}
		}
    	MediatorServices mediatorServices = new MediatorServices();
    	DocumentHandler xmlDoc = formManager.getFromDocCache();	
    	boolean isGroup = false;
    	if ((itemList == null) || (itemList.size() == 0)) {
    		isGroup = true;
    		itemList = inputDoc.getFragmentsList("/In/InvoiceGroupList/InvoiceGroup");
    	}    	
    	// DK IR-LRUM050956689 Rel8.0 06/07/2012 Begin
    	String hasError = inputDoc.getAttribute("/In/HasErrors");
   
    	// DK IR-LRUM050956689 Rel8.0 06/07/2012 End
		// Loop through the list of OIDs (could be either invoices or
    	// invoice groups), associating the uploaded document to each
        long imageOID = 0;
		for (DocumentHandler item : itemList) {
			String[] itemData = item.getAttribute("/"+idTag).split("/");
			String itemOid = itemData[0]; // OID of InvoicesSummaryData or InvoiceGroup

			imageOID = commitToDocumentImage(documentImageValues, resourceManager,
            		formManager, userSession, itemOid, null);

            if (isGroup) {
            	processInvoiceGroupChildren(itemOid, documentImageValues,
            			resourceManager, formManager, userSession);
            	//CR-708 for Attachment for Payablegroup
            	if("yes".equals(isPayableGroup)  || "yes".equals(isRecGroup)){
            		updateAttachmentForPayableGroup(itemOid,resourceManager);
            	}
            }
		}
       // Nar IR-MMUM050954397 if document is attached successfully, then display success message.
	   // group can have many invoices but should display only one message as PDF name is same for all.
		if(imageOID != 0){
			mediatorServices.getErrorManager().issueError(
					TradePortalConstants.ERR_CAT_1,
					TradePortalConstants.DOC_ATTACHED_SUCCESSFULLY, documentImageValues.get("documentName"));
            mediatorServices.addErrorInfo();
            xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
            xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
            formManager.storeInDocCache("default.doc", xmlDoc);       		
		}
    }

    private void processInvoiceGroupChildren(String invoiceGroupOid,
    		Hashtable<String, String> documentImageValues,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession)
    throws AmsException {
		String sql = "select i.UPLOAD_INVOICE_OID, d.DOC_IMAGE_OID from INVOICES_SUMMARY_DATA i"
		            + " left outer join DOCUMENT_IMAGE d on i.UPLOAD_INVOICE_OID = d.P_TRANSACTION_OID"
		            + " where i.A_INVOICE_GROUP_OID = ? ";

		DocumentHandler	invoiceListDoc = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{invoiceGroupOid});
		if (invoiceListDoc != null) {
			Vector<DocumentHandler> invoiceList = invoiceListDoc.getFragments("/ResultSetRecord");
			for (DocumentHandler invoiceDoc : invoiceList) {
				String invoiceOid = invoiceDoc.getAttribute("UPLOAD_INVOICE_OID");
				String docImageOid = invoiceDoc.getAttribute("DOC_IMAGE_OID");

				// Associate the document to this invoice if no other
				// document is already associated.
				if (StringFunction.isNotBlank(invoiceOid) &&
						StringFunction.isBlank(docImageOid)) {
                    commitToDocumentImage(documentImageValues,
                    		resourceManager, formManager,
                    		userSession, invoiceOid, null);
				}
			}
		}
    }

    // Calls to the uploadDocument method can be replaced with calls to
    // testUploadDocument to test this servlet in environments that are
    // not connected an imaging server.
    private Hashtable<String, String> testUploadDocument(HttpServletRequest request,
    		HttpServletResponse response, HttpSession session,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession) throws ServletException, IOException {

    	formManager.storeInDocCache("default.doc", formManager.getFromDocCache());

        Hashtable<String, String> documentImageValues = new Hashtable<String, String>();
        documentImageValues.put("documentName", "name");
        documentImageValues.put("image_bytes", "999");
        documentImageValues.put("image_format", "PDF");
        documentImageValues.put("hash", "hash");
        documentImageValues.put("num_pages", "5");
        documentImageValues.put("image_id", "image_id");

        return documentImageValues;
    }

    /**
     * This method does the task of uploading a document to Imaging server.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    private Hashtable<String, String> uploadDocument(HttpServletRequest request,
    		HttpServletResponse response, HttpSession session,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession) throws ServletException, IOException {

        PropertyResourceBundle servletProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

        Hashtable<String, String> documentImageValues = new Hashtable<String, String>();

        formManager.storeInDocCache("default.doc", formManager.getFromDocCache());

        //cquinton 11/2/2011 Rel 7.1 start - default to use ISRA, but if property is set use VeniceBridge
        boolean useISRA = true;
        

    	try {
            // Check that we have a file upload request
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                // Create a factory for disk-based file items
                DiskFileItemFactory factory = new DiskFileItemFactory();

                // Create a new file upload handler
                ServletFileUpload upload = new ServletFileUpload(factory);

                // Set overall request size constraint
                upload.setSizeMax(Long.parseLong(servletProperties.getString("maxRequestSizeInBytes")));

                // Parse the request
                // [BEGIN] IR-ALUH072786367 - jkok
                List items;
                try {
                    items = upload.parseRequest(request);
                } catch (FileUploadException e) {

                    issueSystemError("doTheWork",
                            "Caught a FileUploadException with following message: "
                            + e.getMessage(), e);

                    issueError(TradePortalConstants.FILE_UPLOAD_EXCEPTION, formManager, userSession.getUserLocale(), e.getMessage());

                    return null;
                }
                // [END] IR-ALUH072786367 - jkok

                // Process the uploaded items
                Iterator iter = items.iterator();
                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.isFormField()) {
                        // Process a regular form field
                        if (item.isFormField()) {
                            String name = item.getFieldName();
                            String value = item.getString();

                            if (name.startsWith("documentName"))
                            {
                            	// DK IR-LRUM050956689 Rel8.0 06/14/2012 Begin
                                validateDocumentNameValue(value, formManager, userSession, session);
                               // DK IR-LRUM050956689 Rel8.0 06/14/2012 End
                                documentImageValues.put("documentName", value);
                            }
                            else {
                                documentImageValues.put(name, value);
                            }
                        } 
                    } 
                    else {
                        

                        // Assuming that the item is a file

                        long sizeInBytes = item.getSize();
                        if (sizeInBytes > 0) {
                            documentImageValues.put("image_bytes", Long
                                    .toString(sizeInBytes));

                            String contentType = item.getContentType();
                            // PDF
                            if ((contentType != null)
                                    && (contentType.equals("application/pdf"))) {
                                documentImageValues.put("image_format", "PDF");
                                PDDocument doc = null;
                                int pageCount = 0;
                                if ( useISRA ) {
                                    //cquinton 8/16/2011 Rel 7.1.0
                                    InputStream hashInputStream = item.getInputStream();
                                    int availableBytes = hashInputStream.available();
                                    byte[] data = new byte[availableBytes] ;
                                    hashInputStream.read(data);
                                    hashInputStream.close();
                                    String hash = createImageHash(data);
                                    documentImageValues.put("hash", hash);

                                  
                                    doc = PDDocument.load(item.getInputStream());

                                    // Count the pages in the file
                                    
                                    pageCount = countPagesInPDF(doc);
                                } else {
                                    pageCount = countPagesInPDF(item
                                        .getInputStream());
                                }
                                if (pageCount > 0) {
                                    documentImageValues.put("num_pages",
                                            Integer.toString(pageCount));

                                    //IAZ IR RRUL061611265 06/16/2011 Begin
                                    if (pageCount > TradePortalConstants.MAX_NUM_PDF_PAGES)
                                        issueError(TradePortalConstants.MAX_NUM_PDF_PAGES_EXD, formManager, userSession.getUserLocale(), item.getName());
                                    //IAZ IR RRUL061611265 06/16/2011 End

                                    // Send the PDF to Imaging (Web) Server
                                    //cquinton 11/15/2011 Rel 7.1 rename property to distinguish with getImage version                                    
                                    int maxNumberOfAttempts = DEFAULT_ADD_IMAGE_MAX_ATTEMPTS;
                                    try {
                                        maxNumberOfAttempts = Integer.parseInt(servletProperties.getString("addImageMaxNumberOfAttempts"));
                                    } catch (Exception ex) {
                                        //do nothing just attempt once
                                        maxNumberOfAttempts = DEFAULT_ADD_IMAGE_MAX_ATTEMPTS;
                                    }
                                    for (int attempt = 0; attempt < maxNumberOfAttempts; attempt++) {
                                          
                                        int docID = 0;
                                        if ( useISRA ) {
                                            docID = sendDocToImagingServer(
                                                doc, pageCount, contentType,
                                                userSession.getUserId(),
                                                servletProperties,
                                                item.getInputStream());
                                        } else {
                                            docID = sendDocToImagingServer(
                                                item.getInputStream(),
                                                pageCount, contentType,
                                                userSession.getUserId(),
                                                servletProperties);
                                        }

                                        
                                        if (docID > 0) {
                                            documentImageValues.put("image_id",
                                                    Integer.toString(docID));
                                            attempt = maxNumberOfAttempts;
                                        }
                                        else
                                        {
                                            issueSystemError("doTheWork", "Attempt "+(attempt+1)+" of sendDocToImagingServer returned -1.  Check logs for further information.", null);
                                        }
                                    }
                                    if(!documentImageValues.containsKey("image_id")) {
                                        issueError(TradePortalConstants.UNABLE_TO_SEND_TO_IMG_SERVER, formManager, userSession.getUserLocale());
                                        throw new AmsException("Unable to send document to Imaging Server. Please refer to logs for more information.");
                                    }
                                }
                                else {
                                    issueError(TradePortalConstants.FAILED_DOCUMENT_VALIDATION, formManager, userSession.getUserLocale(), item.getName());
                                    throw new AmsException("Page count failed for document "+item.getName()+" of user "+userSession.getUserId());
                                }
                            } 
                          
                            else {
                                // [BEGIN] IR-EGUH071735984 - jkok - Return file extension to the user
                                // instead of the content-type/MIME-type because it's more user friendly
                                String originalFilename = item.getName();
                                String fileExtension = "";
                                for(int i=originalFilename.length()-1; i > 0; i--) {
                                    if(originalFilename.charAt(i) == '.') {
                                        fileExtension = originalFilename.substring(i+1).toUpperCase();
                                        i = -1;
                                    }
                                }
                                issueError(TradePortalConstants.FILE_TYPE_NOT_SUPPORTED, formManager, userSession.getUserLocale(), fileExtension);
                                throw new AmsException("Unable to process file type "+fileExtension);
                                // [END] IR-EGUH071735984 - jkok
                            }

                        } 
                        else {
                            issueError(TradePortalConstants.FILE_UPLOAD_EXCEPTION, formManager, userSession.getUserLocale(), item.getName());
                            throw new AmsException("Unable to process file ("+item.getName()+") with size "+sizeInBytes+" bytes");
                        }
                    } 
                } 
            } 


            return documentImageValues;

        } 
        catch (AmsException e) {
            issueSystemError("doTheWork",
                    "Caught an exception with following message: "
                            + e.getMessage(), e);

  
            return documentImageValues;
        } 
    }
	

    /**
     *
     * @param PDDocument
     * @return
     */
    private int countPagesInPDF(PDDocument doc) {
        try {
            return doc.getNumberOfPages();
        } catch (Exception e) {
            issueSystemError("countPagesInPDF",
                    "Caught an exception while trying to count pages in PDF file: "
                            + e.getMessage(), e);
            return -1;
        }
    } 
   

   

    /**
     *
     * @param in
     * @return
     */
    private int countPagesInPDF(InputStream in) {
        byte[] fileAsBytes = null;

        try {
            fileAsBytes = Util.readStreamIntoByteArray(in);
        } catch (Exception e) {
            issueSystemError("countPagesInPDF",
                    "Caught an exception while trying read PDF inputstream into a byte array: "
                            + e.getMessage(), e);
            return -1;
        }

        PDDocument pdDocument = null;
        try {
            pdDocument = PDDocument.load(new ByteArrayInputStream(fileAsBytes));
        } catch (Exception e) {
            issueSystemError("countPagesInPDF",
                    "Caught an exception while trying to create PDFDocument: "
                            + e.getMessage(), e);
            return -1;
        }

        try {
            return pdDocument.getNumberOfPages();
        } catch (Exception e) {
            issueSystemError("countPagesInPDF",
                    "Caught an exception while trying to get number of pages in PDF document: "
                            + e.getMessage(), e);
            return -1;
        }
    } 


    /**
     * Send the image to the image server.
     * This is the ISRA specific version for 7.1 release onward.
     *
     * @param doc - the document as a PDDocument
     * @param pageCount
     * @param contentType
     * @param loginID
     * @param is - document as inputstream
     * @return
     */
    //cquinton 9/16/2011 Rel 7.1 start - ppx166 - new version of this method. ISRA specific
    private int sendDocToImagingServer(PDDocument doc, int pageCount, String contentType, String loginID, PropertyResourceBundle servletProperties,InputStream is) {

       
        javax.resource.cci.Connection connection = null;
        try
        {
            Context context = new InitialContext();
            javax.resource.cci.ConnectionFactory connectionFactory = (ConnectionFactory)context.lookup(servletProperties.getString(TradePortalConstants.ISRA_JNDI));

            ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
            FN_IS_CciConnectionSpec connectionSpec = new com.filenet.is.ra.cci.FN_IS_CciConnectionSpec(idandpass.getId(),idandpass.getPassword(),0);
            connection = connectionFactory.getConnection(connectionSpec);

            if (connection != null)
            {
                LOG.debug("*************** Inside sendDocToImagingServer, connected");
                Interaction interaction = connection.createInteraction();
                FN_IS_CciInteractionSpec interactionSpec = new FN_IS_CciInteractionSpec();
                interactionSpec.setFunctionName("AddDoc");
                RecordFactory recordFactory = connectionFactory.getRecordFactory();
                MappedRecord inputRecord = recordFactory.createMappedRecord("AddDocRequest");
                IndexedRecord collectionDocProperties = recordFactory.createIndexedRecord("DocProperties");
                PDDocumentCatalog cat = doc.getDocumentCatalog();

                LOG.debug("*************** Inside sendDocToImagingServer, got catalog");
                List pages = cat.getAllPages();

                LOG.debug("*************** Inside sendDocToImagingServer, got pages");
                InputStream[] streams = new InputStream[1];
                Iterator itr = pages.iterator();
                
                streams[0]=is;
               

                java.util.List docProperties= new ArrayList();
                Map docpropMap = new HashMap();
                docpropMap.put("index_name","F_DOCFORMAT");
                docpropMap.put("index_type", new Short("50"));
                docpropMap.put("index_value", "application/pdf;name=\"abc.pdf\"");
                docProperties.add(docpropMap);

                
                ListIterator iterProps = docProperties.listIterator();
                while (iterProps.hasNext())
                {
                    Map docProp = (Map)iterProps.next();
                    MappedRecord docPropertyRecord = recordFactory.createMappedRecord("DocProperty");
                    docPropertyRecord.putAll(docProp);
                   
                    collectionDocProperties.add(docPropertyRecord);
                }

                String docClass = servletProperties.getString("docClass");

                LOG.debug("*************** Inside sendDocToImagingServer, docClass: {}" , docClass);

                inputRecord.put ("doc_class_name", docClass);
                inputRecord.put ("index_records", collectionDocProperties);
                
                inputRecord.put ("doc_page_streams", streams);
                inputRecord.put ("is_duplication_ok", Boolean.TRUE);
                
                LOG.debug("*************** Inside sendDocToImagingServer, about to execute");
                MappedRecord outputRecord = (MappedRecord) interaction.execute(interactionSpec, inputRecord);
                LOG.debug("*************** Inside sendDocToImagingServer, executed");

                int resultDocID = Integer.parseInt((outputRecord.get("doc_id")).toString());
                LOG.info("Document ID for added record is: {}" , resultDocID);
                return resultDocID;
            }
        }
        catch(ResourceException re){
            LOG.error("DocumentImageFileUploadServlet: Failed to establish connection: " , re);
        }
        catch(NamingException ne){
            LOG.error("DocumentImageFileUploadServlet: Naming Exception: " , ne);
        }
        catch(Exception e){
            LOG.error("DocumentImageFileUploadServlet: Exception: " , e);
        }
       
        finally {
            if ( connection!=null ) {
                try {
                    connection.close();
                    connection=null;
                }
                catch(Exception e){
                    LOG.error("sendDocToImagingServer: Problem closing connection: " , e );
                    connection=null;
                }
            }
        }
       
        return -1;
    }
    
    // this is the old version used with VeniceBridge
    // we leave it uncommented to allow its use if necessary
    private int sendDocToImagingServer(InputStream documentAsInputStream,
            int pageCount, String contentType, String loginID, PropertyResourceBundle servletProperties) {

        String imageServerURL = servletProperties.getString("imageServerURL");
        String imageUploadVirtualDir = servletProperties.getString("imageUploadVirtualDir");

        String imageFileName = null;
        if ((contentType != null) && (contentType.equals("application/pdf"))) {
            imageFileName = System.currentTimeMillis() + ".pdf";
        } else if ((contentType != null)
                && (contentType.equals("application/tif"))) {
            imageFileName = System.currentTimeMillis() + ".tif";
        }
        HttpURLConnection commitConnection = null;
        try {
            // Read the document into a byte array
            byte[] bytes = null;
            try {
                bytes = Util.readStreamIntoByteArray(documentAsInputStream);
            } catch (Exception e) {
                issueSystemError("sendDocToImagingServer",
                        "An exception was caught while trying to read inputstream into byte array: "
                                + e.getMessage(), e);
                return -1;
            }

            // Upload the document to the Imaging (Web) Server

            //Narayan - PPX-204 15-DEC-2010 Begin
            String httpRequestMethod = null;
            try{
                 httpRequestMethod = servletProperties.getString("UseHttpPutForImaging");
            }
            catch(MissingResourceException e){
                 //ignore
            }
            // Code will get execute only if UseHttpPutForImaging is set to Y
            if ("Y".equalsIgnoreCase(httpRequestMethod)){
                URL uploadURL = new URL(imageServerURL + imageUploadVirtualDir
                     + imageFileName);
                HttpURLConnection uploadConnection = (HttpURLConnection) uploadURL
                    .openConnection();
                uploadConnection.setRequestMethod("PUT");
                uploadConnection.setDoInput(true);
                uploadConnection.setDoOutput(true);
                uploadConnection.setUseCaches(true);

                BufferedOutputStream uploadOutputStream = new BufferedOutputStream(
                    uploadConnection.getOutputStream());

                uploadOutputStream.write(bytes);

                uploadOutputStream.flush();
                uploadOutputStream.close();
                uploadConnection.disconnect();

                int responseCode = uploadConnection.getResponseCode();

                String responseMessage = uploadConnection.getResponseMessage();

                if (responseCode == 200 || responseCode == 201) {
                    LOG.debug("[DocumentImageFileUploadServlet.sendDocToImagingServer] Successfully sent document with {} pages", pageCount);
                } else {
                    issueSystemError("sendDocToImagingServer",
                        "Received the following response message for URL("
                        + uploadURL.toString() + "): "
                        + responseMessage, null);
                    return -1;
                }
            }
            // Narayan - PPX-204 15-DEC-2010 End
            String imageWebDir = servletProperties.getString("imageWebDir");
            String addDocASP = servletProperties.getString("addDocASP");
            String library = servletProperties.getString("library");
            String docClass = servletProperties.getString("docClass");
            String indexField = servletProperties.getString("indexField");
            String PREFIX_SUCCESS = servletProperties.getString("PREFIX_SUCCESS");
            String PREFIX_ERROR = servletProperties.getString("PREFIX_ERROR");

            ImagingIdAndPassword idAndPass = ImagingServices.getNextIdAndPassword();
            String userID = idAndPass.getId();
            String password = idAndPass.getPassword();

            String fullURL = imageServerURL + imageWebDir + addDocASP
                + "?library=" + library + "&FileNm=" + imageFileName
                + "&userid=" + userID + "&password=" + password + "&DocCls=" + docClass
                + "&IndexFld=" + indexField + "&IndexVal=" + pageCount
                + "&PageCount=" + pageCount;

            URL commitDocURL = new URL(fullURL);
            commitConnection = (HttpURLConnection) commitDocURL
                .openConnection();
            int responseCode;
            String responseMessage = null;
            //Narayan - PPX-204 15-DEC-2010 Begin
            if ("Y".equalsIgnoreCase(httpRequestMethod)){
                commitConnection.setRequestMethod("GET");
                commitConnection.setDoInput(true);
                commitConnection.setDoOutput(true);
                commitConnection.setUseCaches(true);

                responseCode = commitConnection.getResponseCode();
                responseMessage = commitConnection.getResponseMessage();
            }
            else {
                commitConnection.setRequestMethod("POST"); // Change from GET to POST
                commitConnection.setRequestProperty( "Content-Type", "application/octet-stream");
                commitConnection.setRequestProperty( "Content-Length", String.valueOf(bytes.length));
                commitConnection.setDoInput(true);
                commitConnection.setDoOutput(true);
                commitConnection.setUseCaches(true);
                commitConnection.connect();
                DataOutputStream out = new DataOutputStream(commitConnection.getOutputStream());
                out.write(bytes);
                out.flush();
                out.close();
                responseCode = commitConnection.getResponseCode();
                responseMessage = commitConnection.getResponseMessage();               
            }
            //Narayan - PPX-204 15-DEC-2010 End
            if (responseCode == 200) {
                InputStream commitResponse = commitConnection.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                    commitResponse));

                String response = in.readLine();
                if ((response != null)
                        && (response.indexOf(PREFIX_SUCCESS) != -1)) {
                    String docIDAsString = response.substring(
                            PREFIX_SUCCESS.length()).trim();
                    if (docIDAsString.length() > 0) {
                        try {
                            return Integer.parseInt(docIDAsString);
                        } catch (NumberFormatException e) {
                            issueSystemError("sendDocToImagingServer",
                                    "Caught a NumberFormatException when trying to return '"
                                            + docIDAsString + "' as an int", e);
                            return -1;
                        }
                    }
                    else {
                        issueSystemError("sendDocToImagingServer", "Received a blank docID from '"
                                        + fullURL + "'", null);
                        return -1;
                    }
                }
                else if ((response != null)
                        && (response.indexOf(PREFIX_ERROR) != -1)) {
                    String docIDError = response.substring(
                            PREFIX_ERROR.length()).trim();
                    if ((docIDError != null) && (docIDError.length() > 0)) {
                        issueSystemError("sendDocToImagingServer",
                                        "Received the following error when processing '"
                                        + fullURL + "': " + docIDError, null);
                    }
                    return -1;
                }
            } 
            else {
                issueSystemError(
                        "sendDocToImagingServer",
                        "Received a responseCode other than 200 when trying to commit document to Imaging (Web) Server. responseCode='"
                                + responseCode
                                + "', responseMessage='"
                                + responseMessage + "'", null);
                return -1;
            }
        } catch (FileNotFoundException e) {
            issueSystemError("sendDocToImagingServer",
                    "Caught the folowing FileNotFoundException: "
                            + e.getMessage(), e);
            return -1;
        } catch (IOException e) {
            issueSystemError("sendDocToImagingServer",
                    "Caught the following IOException: " + e.getMessage(), e);
            return -1;
        } catch (Exception e) {
            issueSystemError("sendDocToImagingServer",
                    "Caught the following Exception: " + e.getMessage(), e);
            return -1;
        }
        
        finally {
            if ( commitConnection!=null ) {
                try {
                	commitConnection.disconnect();
                	commitConnection=null;
                }
                catch(Exception e){
                    LOG.error("sendDocToImagingServer: Problem closing connection: " , e );
                    commitConnection=null;
                }
            }
        }
       

        return -1;
    } 

    //BSL CR710 03/08/2012 Rel8.0 - refactor commitToDocumentImage to call issueError directly
    /**
     *
     * @param documentImageValues
     * @param resourceManager
     * @return
     */

    private long commitToDocumentImage(Hashtable<String, String> documentImageValues,
    		ResourceManager resourceManager, FormManager formManager,
    		SessionWebBean userSession, String transactionOid, String mailMessageOid)
    throws AmsException {
    	if (documentImageValues == null) {
            issueSystemError("commitToDocumentImage",
            		"Unable to commit to DOCUMENT_IMAGE because provided values are empty.",
            		null);
            issueError(TradePortalConstants.COMMIT_TO_IMAGE_DB_FAILED, formManager, userSession.getUserLocale());
            throw new AmsException("commitToDocumentImage of null failed.");
    	}

    	long docImageOid = 0;
    	boolean commitFailed = false;
        try {
        	String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        	ClientServerDataBridge csdb = resourceManager.getCSDB();

        	DocumentImage documentImage = (DocumentImage) EJBObjectFactory.createClientEJB(serverLocation,
        			"DocumentImage", csdb);
        	documentImage.newObject();

        	String docName = documentImageValues.get("documentName");
        	String imageId = documentImageValues.get("image_id");
        	String imageFormat = documentImageValues.get("image_format");
        	String numPages = documentImageValues.get("num_pages");
        	String imageBytes = documentImageValues.get("image_bytes");


        	if (((docName != null) && (imageId != null)
        			&& (imageFormat != null) && (numPages != null) && (imageBytes != null))
        			&& (transactionOid != null || mailMessageOid != null)) {
        		documentImage.setAttribute("doc_name", docName);
        		documentImage.setAttribute("image_id", imageId);
        		documentImage.setAttribute("image_format", imageFormat);
        		documentImage.setAttribute("num_pages", numPages);
        		documentImage.setAttribute("image_bytes", imageBytes);
        		documentImage.setAttribute("form_type", TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

        		String imageHash = documentImageValues.get("hash");
        		if (imageHash != null) {
        			documentImage.setAttribute("hash", imageHash);
        		}

        		if (transactionOid != null) {
        			documentImage.setAttribute("transaction_oid", transactionOid);
        		}
        		else if (mailMessageOid != null) {
        			documentImage.setAttribute("mail_message_oid", mailMessageOid);
        		}

        		documentImage.save();
        		docImageOid = documentImage.getAttributeLong("doc_image_oid");
        	}
        	else {
        		issueSystemError("commitToDocumentImage",
        				"Unable to commit to DOCUMENT_IMAGE with the following values: "
        				+"docName="+docName+","
        				+"imageId="+imageId+","
        				+"imageFormat="+imageFormat+","
        				+"numPages="+numPages+","
        				+"imageBytes="+imageBytes+","
        				+"transactionOid="+transactionOid+","
        				+"mailMessageOid="+mailMessageOid,
        				null);
        		commitFailed = true;
        	}
        }
        catch (AmsException e) {
            issueSystemError("commitToDocumentImage", "Caught the following AmsException: "+e.getMessage(), e);
            commitFailed = true;
        }
        catch (RemoteException e) {
            issueSystemError("commitToDocumentImage", "Caught the following RemoteException: "+e.getMessage(), e);
            commitFailed = true;
        }

        if (commitFailed) {
            issueError(TradePortalConstants.COMMIT_TO_IMAGE_DB_FAILED, formManager, userSession.getUserLocale());
            throw new AmsException("commitToDocumentImage of "+documentImageValues.toString()+" failed.");
        }

        return docImageOid;
    }

    /**
     *
     * @param errorCode
     * @param formManager
     * @param localeName
     */
    private void issueError(String errorCode, FormManager formManager, String localeName) {
        issueError(errorCode, formManager, localeName, null);
    }

    /**
     *
     * @param errorCode
     * @param formManager
     * @param localeName
     * @param errorMessage
     */
    private void issueError(String errorCode, FormManager formManager,
            String localeName, String errorMessage) {

        try {

            MediatorServices mediatorServices = null;
            DocumentHandler xmlDoc = null;

            xmlDoc = formManager.getFromDocCache();

            mediatorServices = new MediatorServices();
            mediatorServices.getErrorManager().setLocaleName(localeName);

            if (errorMessage != null) {
                mediatorServices.getErrorManager()
                        .issueError(TradePortalConstants.ERR_CAT_1, errorCode,
                                errorMessage);
            } else {
                mediatorServices.getErrorManager().issueError(
                        TradePortalConstants.ERR_CAT_1, errorCode);
            }

            mediatorServices.addErrorInfo();

            xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
            xmlDoc.setAttribute("/In/HasErrors",
                    TradePortalConstants.INDICATOR_YES);

            formManager.storeInDocCache("default.doc", xmlDoc);

        } catch (AmsException e) {
            issueSystemError(
                    "issueError",
                    "Caught AmsException while trying to issue an error to the Error Manager",
                    e);
        }
    } 

    /**
     *
     * @param callingMethodName
     * @param errorMessage
     * @param e
     */
    private void issueSystemError(String callingMethodName,
            String errorMessage, Exception e) {

        StringBuilder messageToOutput = new StringBuilder();

        messageToOutput.append("[" + "DocumentImageFileUploadServlet" + "."
                + callingMethodName + "] ");

        messageToOutput.append(errorMessage);

        System.err.println(messageToOutput);

        if (e != null) {
            e.printStackTrace();
        }
    } 

    /**
     *
     * @param session
     * @return
     */
    private String getMailMessageOid(HttpSession session) {
    	if(session==null) return null;

        String page = (String)session.getAttribute("documentImageFileUploadPageOriginator");

        if(page == null) {
            return null;
        }
        else if ( "MailMessageDetail".equals(page) || "BankMailMessageDetail".equals(page) ) {
            String oid = (String) session.getAttribute("documentImageFileUploadOid");

            if ((oid != null) && (oid.trim().length() > 0)) {
                session.setAttribute("mailMessageOid", oid);
                session.removeAttribute("documentImageFileUploadOid");//RAUJ100774899 Remove attribute to avoid confusion for later upload
                return oid;
            }
            else
            {
                FormManager formManager = (FormManager) session.getAttribute("formMgr");
                DocumentHandler defaultDoc = formManager.getFromDocCache();

                if (defaultDoc != null) {
                    oid = defaultDoc.getAttribute("/Out/message_oid");
                    if ((oid != null) && (oid.trim().length() > 0)) {
                        session.setAttribute("mailMessageOid", oid);
                        return oid;
                    }
                }
            }
        }
        return null;
    } 

    /**
     *
     * @param session
     * @return
     */
    private String getTransactionOid(HttpSession session) {
    	if(session==null) return null;    	
    	String page = null;
    	String oid  = null;
        page = (String)session.getAttribute("documentImageFileUploadPageOriginator");        
        if(page==null) {
        	return null;
        }
        else if (page.equals("InstrumentNavigator")) {
            oid = (String) session.getAttribute("documentImageFileUploadOid");

            if ((oid != null) && (oid.trim().length() > 0)) {
                session.removeAttribute("documentImageFileUploadOid");//RAUJ100774899 Remove attribute to avoid confusion for later upload
                return oid;
            }
        }
        else if (page.equals("NewTransactionDirector")) {
            FormManager formManager = (FormManager) session.getAttribute("formMgr");
            DocumentHandler defaultDoc = formManager.getFromDocCache();

            if (defaultDoc != null) {
                oid = defaultDoc.getAttribute("/In/Transaction/transaction_oid");
                if ((oid != null) && (oid.trim().length() > 0)) {
                    session.setAttribute("documentImageFileUploadPageOriginator",
                            "InstrumentNavigator");

                    return oid;
                }
            }
        }
        else {
            oid = (String) session.getAttribute("documentImageFileUploadOid");

            if ((oid != null) && (oid.trim().length() > 0)) {
                session.removeAttribute("documentImageFileUploadOid");//RAUJ100774899 Remove attribute to avoid confusion for later upload
                return oid;
            }


        }
        return null;
    } 

    /**
     *
     * @param value
     * @param formManager
     * @param userSession
     * @throws AmsException
     */
    private void validateDocumentNameValue(String value, FormManager formManager, SessionWebBean userSession, HttpSession session) throws AmsException {        
        String pageOriginator = (String)session.getAttribute("documentImageFileUploadPageOriginator"); // DK IR-LRUM050956689 Rel8.0 06/14/2012
        if((value == null) || (value.trim().length() == 0)) {
            issueError(TradePortalConstants.INVALID_DOCUMENT_NAME, formManager, userSession.getUserLocale());
            throw new AmsException("An invalid document name was submitted by user "+userSession.getUserId()+": ["+value+"]");
        }
        // DK IR-LRUM050956689 Rel8.0 06/14/2012 Begin
        if (TradePortalConstants.INVOICE_MGMT.equals(pageOriginator) || "InvoiceGroupDetail".equals(pageOriginator) || TradePortalConstants.INV_MGMT_LOAN_REQ_PAGE.equals(pageOriginator) ||
             		TradePortalConstants.PAY_INV_ATP_GROUP_DETAIL_PAGE.equals(pageOriginator) || TradePortalConstants.PAY_INV_LRQ_GROUP_DETAIL_PAGE.equals(pageOriginator)
             		|| TradePortalConstants.PAY_PRGM_DETAIL_PAGE.equals(pageOriginator)) {
    			if(!value.trim().matches("[a-zA-Z0-9\\s]*")){ // NAr IR-T36000015237 added space also for matcher but it should throw error if it is not valid name as field in DB is not unicode enabled
    				//Error message displayed for Invoice only    				
    				issueError(TradePortalConstants.INVALID_DOCUMENT_NAME, formManager, userSession.getUserLocale());
    	            throw new AmsException("An invalid document name was submitted by user"+userSession.getUserId()+": ["+value+"]");
    			}            	
         }
     // DK IR-LRUM050956689 Rel8.0 06/14/2012 End
    } 

    /**
     * Create the image hash.
     *
     * @param data - the image as a byte array - the byte array
     *               length must be the actual length of the image
     * @return hash value
     */
    private String createImageHash( byte[] data ) {
        String calculatedHash = null;

        //use image length as the secret key
        String hashKey = String.valueOf(data.length);
        try {
            byte[] keyBytes = hashKey.getBytes( AmsConstants.UTF8_ENCODING );
            //the hash result will be exactly 128bits because its md5
            //this is 16 bytes
            byte[] hashValueBytes =
                EncryptDecrypt.createHmacMD5Hash( data, keyBytes );
            //the following converts the hash bytes as a hex string
            //it doubles the length using the left shift operator
            //because each byte is represented by 2 characters -
            // which means that calculated hash is always 32 characters long
            calculatedHash = EncryptDecrypt.bytesToHexString( hashValueBytes );
        } catch ( Exception ex ) {
            System.err.println("DocumentImageFileUploadServlet: Problem creating image hash: " + ex.toString() );
            ex.printStackTrace();
            //we simply write out the error, but continue on with a blank hash
        }
        return calculatedHash;
    }
    
	/**
	 * This method checks if the group invoice has image created or not
	 *  otherwise updates the inputdoc
	 */
     private DocumentHandler updateAttachmentForPayableDetail(ResourceManager resourceManager, DocumentHandler inputDoc) throws RemoteException, AmsException{
    	
    	 Vector<DocumentHandler> invList = inputDoc.getFragments("/In/UploadInvoiceList/InvoicesSummaryData");
    	 int size = invList.size();
    	 for (int i=0; i<size; i++){
		 DocumentHandler invoiceItem = invList.get(i);
			String[] invData = invoiceItem.getAttribute("/InvoiceData").split("/");
			String invoiceOid = invData[0];
			String groupOid =null;
			if(invoiceOid!=null){
						
				String sql = "select a_invoice_group_oid from invoices_summary_data where upload_invoice_oid = ? ";
 				DocumentHandler	groupListDoc = DatabaseQueryBean.getXmlResultSet(sql, true, new Object[]{invoiceOid});
 				
 				if (groupListDoc != null){
 					groupOid = groupListDoc.getAttribute("/ResultSetRecord(0)/A_INVOICE_GROUP_OID"); 
 				}
 				if(groupOid!=null){
 						
 					String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
 		        	ClientServerDataBridge csdb = resourceManager.getCSDB();
 					InvoiceGroup invoiceGroup = (InvoiceGroup) EJBObjectFactory.createClientEJB(serverLocation,
 		        			"InvoiceGroup", Long.parseLong(groupOid),csdb);
 					
 					invoiceGroup.setAttribute("attachment_ind", TradePortalConstants.INDICATOR_YES);
	 				int status = invoiceGroup.save();
	 				LOG.info("In updateAttachmentForPayableDetail(), invoiceGroup save Updated status: {}", status);			
 				}
 			
			}
         }
			return inputDoc;
     }
     
     /**
      * THis method finds if any attachments available or not.
      * @param invGroupOid
      * @param resourceManager
      * @return
      * @throws RemoteException
      * @throws AmsException
      */
     private int updateAttachmentForPayableGroup(String invGroupOid, ResourceManager resourceManager) 
 			throws RemoteException, AmsException {
 		
 		String attachmentStatus = "";
 		int	imagesCount = DatabaseQueryBean.getCount("DOC_IMAGE_OID", "DOCUMENT_IMAGE", "P_TRANSACTION_OID in " +
 				" (select upload_invoice_oid from invoices_summary_data where  a_invoice_group_oid = ? )", false, new Object[]{invGroupOid});
 		LOG.info("In updateAttachmentForPayableGroup, imagesCount: ()", imagesCount);
 		
 		if(imagesCount>0)
 			attachmentStatus = TradePortalConstants.INDICATOR_YES;
 		else
 			attachmentStatus = TradePortalConstants.INDICATOR_NO;
 		
 		String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
     	ClientServerDataBridge csdb = resourceManager.getCSDB();
			InvoiceGroup invoiceGroup = (InvoiceGroup) EJBObjectFactory.createClientEJB(serverLocation,
     			"InvoiceGroup", Long.parseLong(invGroupOid),csdb);
	
			
			invoiceGroup.setAttribute("attachment_ind", TradePortalConstants.INDICATOR_YES);
			int status = invoiceGroup.save();
			LOG.info("Updated status:"+status);				
 		
 		return status;
 	}
} 
