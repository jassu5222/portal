package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.PropertyResourceBundle;

import javax.crypto.SecretKey;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.Interaction;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.ImagingIdAndPassword;
import com.ams.tradeportal.common.ImagingServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.ResourceManager;
import com.filenet.is.ra.cci.FN_IS_CciConnectionSpec;
import com.filenet.is.ra.cci.FN_IS_CciInteractionSpec;

/**
 * The GetImageAnnotationsServlet class is used by the Daeja viewONE java applet to pull down image annotation content returned by the IS resource adapter
 * to the browser.
 *
 * PPX-166 5/27/2011 - VeniceBridge Replacement
 * Release 7.1.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */
public class GetImageAnnotationsServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(GetImageAnnotationsServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doTheWork(request, response);
    }

    public void doTheWork(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
    {

    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}


        boolean error = false;
        request.setCharacterEncoding("UTF-8");

        //cquinton 11/3/2011 Rel 7.1 handle situations where
        // session is not passed through.
        // generally this means we can't get error messages from resource manager
		//	W Zhu 8/22/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
        HttpSession session = request.getSession(false); //don't create a new one, return null if none
        ResourceManager resourceManager = null;
        SecretKey secretKey = null;
        if (session!=null) {
        	LOG.info("GetImageAnnotationServlet: session==null");
            resourceManager = (ResourceManager) session.getAttribute("resMgr");
      	   SessionWebBean userSession = (SessionWebBean)session.getAttribute("userSession");
      	   secretKey = userSession.getSecretKey();
        }

        String encryptedDocId =request.getParameter(TradePortalConstants.DOCUMENT_ID);
        String docId = EncryptDecrypt.decryptStringUsingTripleDes( encryptedDocId, secretKey );
        //cquinton 11/29/2011 Rel 7.1 changes to close the connection
        javax.resource.cci.Connection connection = null;
        try
        {
            PropertyResourceBundle servletProperties = (PropertyResourceBundle) PropertyResourceBundle
                .getBundle("TradePortal");
            Context context = new InitialContext();
            javax.resource.cci.ConnectionFactory connectionFactory = (ConnectionFactory)context.lookup(servletProperties.getString(TradePortalConstants.ISRA_JNDI));

            ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
            FN_IS_CciConnectionSpec connectionSpec = new com.filenet.is.ra.cci.FN_IS_CciConnectionSpec(idandpass.getId(),idandpass.getPassword(),0);
            //cquinton 11/29/2011 Rel 7.1 changes to close the connection
            connection = connectionFactory.getConnection(connectionSpec);

            if (connection != null)
            {
                Interaction interaction = connection.createInteraction();
                FN_IS_CciInteractionSpec interactionSpec = new FN_IS_CciInteractionSpec();
                interactionSpec.setFunctionName("GetAnnotations");
                RecordFactory recordFactory = connectionFactory.getRecordFactory();
                MappedRecord inputRecord = recordFactory.createMappedRecord("DocumentPage");
                inputRecord.put(TradePortalConstants.DOCUMENT_ID, Long.valueOf(docId));
                inputRecord.put(TradePortalConstants.PAGE_NUMBER, Integer.valueOf(request.getParameter(TradePortalConstants.PAGE_NUMBER)));

                MappedRecord outputRecord = (MappedRecord) interaction.execute(interactionSpec, inputRecord);

                InputStream getAnnotationStream = (InputStream)outputRecord.get("XMLStream");
                String clientCodepage =  (String)outputRecord.get("ClientCodepage");

//connection.close
                URL xslUrl = null;
                String annoIniData = null;
                //cquinton 10/27/2011 Rel 7.1 start
                // error handling cleanup
                // if we do get an error on annotations, we want the image to still display,
                // just without the annotation and problems written to log for research!
                try {
                    if (getAnnotationStream.available()>0)
                    {
                        //comment out the try block here
                        // so exceptions drop to the below exception handling
                        //try {
                            //move reading the annotations URL to getAnnotationXsl() method
                            // to be able to handle problems more cleanly. instead just pass the request here
                            //xslUrl = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath() + "/xsl/Annot.xsl");
                            //AnnoIniData = getINIFileForXMLFile(getAnnotationStream, xslUrl, clientCodepage);
                        annoIniData = getAnnoIniData(getAnnotationStream, request, clientCodepage);
                        //}catch(MalformedURLException  me){
                        //    me.printStackTrace();
                        //}catch(IOException io){
                        //    io.printStackTrace();
                        //}catch(Exception e){
                        //    e.printStackTrace();
                        //}
                    } else {
                        //throw an exception so it drops to standard error block below
                        throw new AmsException( "no data on annotation stream" );
                    }
                } finally {
                    if ( getAnnotationStream != null ) {
                        getAnnotationStream.close();
                    }
                }
                //cquinton 10/27/2011 Rel 7.1 end

                response.setContentType("text/plain; charset=UTF-8");
                byte[] byteData = annoIniData.getBytes("UTF-8");
                response.setContentLength(byteData.length);
                ServletOutputStream sos = response.getOutputStream();
                sos.write(byteData);
                sos.flush();
                sos.close();
            }
            else {
                LOG.info("GetImageAnnotationsServlet: no connection" );
                error = true;
            }
        }
        catch(ResourceException re){
            //cquinton 11/2/2011 Rel 7.1 start - catch special condition of record not found.
            // print out an error, but no need to do a stack trace...
            if ( "FN_IS_RA_11038".equalsIgnoreCase( re.getErrorCode() ) ) {
                LOG.info("GetImageAnnotationsServlet: Document ID " +
                    docId + " not found! " + re.getMessage() );
            } else {
                LOG.info("GetImageAnnotationsServlet: Resource Exception: " + re.getMessage());
                re.printStackTrace();
            }
            //cquinton 11/2/2011 Rel 7.1 end
            error = true;
         }
        catch(NamingException ne){
            LOG.info("GetImageAnnotationsServlet: Naming Exception: " + ne.getMessage());
            ne.printStackTrace();
            error = true;
        }
        catch(Exception e){
            LOG.info("GetImageAnnotationsServlet: Exception: " + e.getMessage());
            e.printStackTrace();
            error = true;
        }
        //cquinton 11/29/2011 Rel 7.1 close the connection
        finally {
            if ( connection!=null ) {
                try {
                    connection.close();
                    connection=null;
                }
                catch(Exception e){
                    LOG.info("GetImageAnnotationsServlet: Problem closing connection: " +
                        " " + e.toString() );
                    connection=null;
                }
            }
        }

        if (error)
        {
            //by default on error, return empty annotations file so the
            // image will display
            response.setContentType("text/plain; charset=UTF-8");
            ServletOutputStream sos = response.getOutputStream();
            String errorMessage = "[EMPTY]";
            response.setContentLength(errorMessage.getBytes("UTF-8").length);
            sos.write(errorMessage.getBytes("UTF-8"));
            sos.flush();
            sos.close();
        }
    }

    /**
     * This method converts the XML input annotation stream to an ini-type output string.
     *
     * @param getAnnotationStream
     * @param request
     * @param clientCodepage
     * @return java.lang.String - the annotation data in ini format
     * @throws AmsException if there is any problem doing the conversion
     */
    //cquinton 10/27/2011 Rel 7.1 change the name of this method to be more representative
    // of its role in generating annotations ini data - also make it private
    //public String getINIFileForXMLFile(InputStream getAnnotationStream, URL xslURL, String clientCodepage) throws IOException
    private String getAnnoIniData(InputStream getAnnotationStream, HttpServletRequest request, String clientCodepage)
        throws AmsException
    {
        String iniData = "";

        //cquinton 10/27/2011 - move getting annot.xsl to a
        // separate method so we can more readily see the content
        // and handle problems better
        //InputStream xslis = xslURL.openStream();
        String xslContent = getAnnotationXsl(request); //note throws AmsException if a problem
        StringReader xslReader = new StringReader(xslContent);

        Transformer transformer;
        TransformerFactory factory = TransformerFactory.newInstance();

        OutputStream os = new ByteArrayOutputStream();
        try
        {
            //convert the annotation stream from the given codepage to utf-8
            byte [] by = new byte[getAnnotationStream.available()];
            getAnnotationStream.read(by);
            String abc = getString(clientCodepage, by);

            byte[] by1 = abc.getBytes("UTF-8");
            ByteArrayInputStream buf = new ByteArrayInputStream(by1) ;
            InputStream is = buf;

            //transform
            
            transformer = factory.newTransformer(new StreamSource(xslReader));
            transformer.transform(new StreamSource(is),new StreamResult(os));

            is.close();
            ByteArrayOutputStream bos = (ByteArrayOutputStream) os;
            byte[] bo = bos.toByteArray();
            iniData = new String(bo, "UTF-8");
        } catch (Exception ex) {
            throw new AmsException(
                "problem transforming annotations.",
                ex ); //chains the exception
        }
        return iniData;
    }

    /**
     * Get the Annotation Xsl.
     * We do this every time just so it can change dynamically if necessary.
     * Look at caching if necessary for performance.
     *
     * We return the string rather than a stream because we want to evaluate
     * the content to see if it is valid or not before going on to transformation.
     *
     * @param request
     * @throws AmsException if there is a problem
     * @return the xsl in string form.  Returns null if there is a problem.
     */
    private String getAnnotationXsl(HttpServletRequest request)
            throws AmsException {
        String xslContent = null; //default for error getting this

        //cquinton 12/14/2011 Rel 7.1 ir#snul121443365 start
        boolean getContentSuccess = false;
        String path = request.getContextPath() + "/xsl/Annot.xsl";

        // use the producerRoot property
        // producerRoot is the local host/port, so is appropriate for xsl retrieval usage
        String producerRoot = null;
        try {
            PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle
                .getBundle("TradePortal");
            producerRoot = tpProperties.getString("producerRoot");
        } catch (Exception ex) { //just move on
            producerRoot = null;
            LOG.info("GetImageAnnotationsServlet: " +
                "producerRoot property not found.");
        }
        if ( !InstrumentServices.isBlank( producerRoot ) ) {
            try {
                URL xslUrl = new URL(producerRoot + path);
                xslContent = getURLContent( xslUrl );

                //for special cases we parse the response to see we didn't actually get content correctly
                // this is going to be specific to what weblogic returns!!!!
                // for the URL class there does not appear to be any way to check the http response code
                if ( xslContent != null && xslContent.indexOf( "400 Bad Request" ) >= 0 ) {
                    LOG.info("GetImageAnnotationsServlet: " +
                        "problem getting annotations xsl at producerRoot: 400 Bad Request." +
                        " producerRoot=" + producerRoot +
                        " path=" + path +
                        " content=" + xslContent);
                } else {
                    getContentSuccess = true;
                    LOG.debug("GetImageAnnotationsServlet: " +
                        "Successfully loaded annotations xsl at producerRoot. " +
                        " producerRoot=" + producerRoot +
                        " path=" + path );
                }
            } catch( Exception ex ) {
                LOG.info("GetImageAnnotationsServlet: " +
                    "problem getting annotations xsl at producerRoot." +
                    " producerRoot=" + producerRoot +
                    " path=" + path );
                ex.printStackTrace();
            }
        }

        //if necessary, try the local context
        if ( !getContentSuccess ) {
            String scheme = request.getScheme();
            String localName = request.getLocalName();
            int localPort = request.getLocalPort();
            //String path = request.getContextPath() + "/xsl/Annot.xsl";
            //boolean getContentSuccess = false;
            try {
                URL xslUrl = new URL(scheme, localName, localPort, path);
                xslContent = getURLContent( xslUrl );

                //for special cases we parse the response to see we didn't actually get content correctly
                // this is going to be specific to what weblogic returns!!!!
                // for the URL class there does not appear to be any way to check the http response code
                if ( xslContent != null && xslContent.indexOf( "400 Bad Request" ) >= 0 ) {
                    LOG.info("GetImageAnnotationsServlet: " +
                        "problem getting annotations xsl on local context: 400 Bad Request." +
                        " scheme=" + scheme +
                        " localName=" + localName +
                        " localPort=" + localPort +
                        " path=" + path +
                        " content=" + xslContent);
                } else {
                    getContentSuccess = true;
                    LOG.debug("GetImageAnnotationsServlet: " +
                        "Successfully loaded annotations xsl on local context. " +
                        " scheme=" + scheme +
                        " localName=" + localName +
                        " localPort=" + localPort +
                        " path=" + path );
                }
            } catch( Exception ex ) {
                LOG.info("GetImageAnnotationsServlet: " +
                    "problem getting annotations xsl on local context." +
                    " scheme=" + scheme +
                    " localName=" + localName +
                    " localPort=" + localPort +
                    " path=" + path );
                ex.printStackTrace();
            }
        }

        /* no need to attempt the request server context
        //if an error, try the request server context (this is similar to code in
        // filenet sample file viewer code
        if ( !getContentSuccess ) {
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();
            try {
                URL xslUrl = new URL(scheme, serverName, serverPort, path);
                xslContent = getURLContent( xslUrl );

                //for special cases we parse the response to see we didn't actually get content correctly
                // this is going to be specific to what weblogic returns!!!!
                // for the URL class there does not appear to be any way to check the http response code
                if ( xslContent != null && xslContent.indexOf( "400 Bad Request" ) >= 0 ) {
                    LOG.info("GetImageAnnotationsServlet: " +
                        "problem getting annotations xsl on request context: 400 Bad Request." +
                        " scheme=" + scheme +
                        " serverName=" + serverName +
                        " serverPort=" + serverPort +
                        " path=" + path +
                        " content=" + xslContent);
                } else {
                    getContentSuccess = true;
                    LOG.debug("GetImageAnnotationsServlet: " +
                        "Successfully loaded annotations xsl on request context. " +
                        " scheme=" + scheme +
                        " serverName=" + serverName +
                        " serverPort=" + serverPort +
                        " path=" + path );
                }
            } catch( Exception ex ) {
                LOG.info("GetImageAnnotationsServlet: " +
                    "problem getting annotations xsl on request context." +
                    " scheme=" + scheme +
                    " serverName=" + serverName +
                    " serverPort=" + serverPort +
                    " path=" + path );
                ex.printStackTrace();
            }
        }
        */
        //cquinton 12/14/2011 Rel 7.1 ir#snul121443365 end

        //if still an error, throw exception so it drops out
        if ( !getContentSuccess ) {
            throw new AmsException(
                "problem getting annotations xsl." );
        }

        return xslContent;
    }

    /**
     * Get content from a URL.
     *
     * @param url
     * @return url content as a string
     * @throws IOException if any problem occurs
     */
    private String getURLContent(URL url)
            throws IOException {
        String urlContent = "";

        InputStream is = url.openStream();
        try {
            BufferedReader reader =
                new BufferedReader(
                    new InputStreamReader( is ) );
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line=reader.readLine()) != null) {
                sb.append( line+"\n" );
            }
            urlContent = sb.toString();
        } finally {
            if ( is!=null ) {
                try {
                    is.close();
                } catch ( Exception ex ) {
                    LOG.info("GetImageAnnotationsServlet: problem closing url stream. " +
                        " protocol=" + url.getProtocol() +
                        " host=" + url.getHost() +
                        " port=" + url.getPort() +
                        " path=" + url.getPath() );
                }
            }
        }
        return urlContent;
    }

    /**
     * This method converts the byte array in to a string specific to the clientCodepage.
     *
     * @param clientCodepage is used for byte array to String translation.
     * @param textByteData is the byte array to be converted to String.
     * @return java.lang.String
     */
    private String getString(String clientCodepage, byte[] textByteData)
    {
        String lDefaultString = null;

        try
        {
            lDefaultString = new String(textByteData, clientCodepage);
        }
        catch (UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return lDefaultString;
    }
}
