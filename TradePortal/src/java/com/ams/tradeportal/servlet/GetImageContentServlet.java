package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//IValavala Adding CR 742 related jars
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.UUID;

import javax.crypto.SecretKey;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.Interaction;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SOAPElement;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBody;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.Base64;
import com.ams.tradeportal.common.ImagingIdAndPassword;
import com.ams.tradeportal.common.ImagingServices;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.filenet.is.ra.cci.FN_IS_CciConnectionSpec;
import com.filenet.is.ra.cci.FN_IS_CciInteractionSpec;
import com.sun.media.imageio.plugins.tiff.BaselineTIFFTagSet;
import com.sun.media.imageio.plugins.tiff.TIFFDirectory;
import com.sun.media.imageio.plugins.tiff.TIFFField;
import com.sun.media.imageio.plugins.tiff.TIFFTag;

/**
 * The GetImageContentServlet class is used by the Daeja viewONE java applet
 * to pull down image content returned by the IS resource adapter
 * to the browser.
 *
 * PPX-166 5/27/2011 - VeniceBridge Replacement
 * Release 7.1.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */
public class GetImageContentServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(GetImageContentServlet.class);

    public static final int DEFAULT_GET_IMAGE_MAX_ATTEMPTS = 3;

    //codes to indicate result of getImage() method
    private static final int GET_IMAGE_SUCCESS = 0;
    private static final int GET_IMAGE_VIEW_ERROR = 1;
    private static final int GET_IMAGE_HASH_ERROR = 2;
    
    //CR 742
    private static final String STATUS_PENDING = "P";
    private static final String STATUS_COMPLETE = "C";
    private static final String STATUS_ERROR = "E";
   

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doTheWork(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException {
        long startTime = System.currentTimeMillis();
    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}


        
        PropertyResourceBundle servletProperties = (PropertyResourceBundle)
            PropertyResourceBundle.getBundle("TradePortal");

        //cquinton 8/16/2011 Rel 7.1.0 ppx240 - allow for different error messages
        boolean success = false;
		String errorResource = "";

        //cquinton 11/3/2011 Rel 7.1 handle situations where
        // session is not passed through.
        // generally this means we can't get error messages from resource manager
		//	W Zhu 8/22/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
        HttpSession session = request.getSession(false); //don't create a new one, return null if none
        ResourceManager resourceManager = null;
        SecretKey secretKey = null;
        String user_oid = "";
        String user_id = "";
        if (session!=null) {
        	LOG.info("GetImageContentServlet: session==null");
            resourceManager = (ResourceManager) session.getAttribute("resMgr");
       	   SessionWebBean userSession = (SessionWebBean)session.getAttribute("userSession");
      	   secretKey = userSession.getSecretKey();
      	   //IValavala CR 742
      	   user_oid = userSession.getUserOid();
      	   user_id = userSession.getUserId();
        }
      

        //cquinton 7/21/2011 Re. 7.1.0 ppx240 - capture hash from parms
		// differentiate between encrypted/non-encrypted parms
		String encryptedDocId =request.getParameter(TradePortalConstants.DOCUMENT_ID);
		String docId = EncryptDecrypt.decryptStringUsingTripleDes( encryptedDocId, secretKey );
        String encryptedImageHash = request.getParameter("hash");
		String imageHash = EncryptDecrypt.decryptStringUsingTripleDes( encryptedImageHash, secretKey );	  
		//IValavala CR 742 March 4 2013. Get imageSource type
		String encryptedsourceType = request.getParameter("source_type");
		
		String imageSourceType = "";
		if  (!StringFunction.isBlank(encryptedsourceType)){
			imageSourceType = EncryptDecrypt.decryptStringUsingTripleDes( encryptedsourceType, secretKey );
		}
		//cquinton 11/9/2011 Rel 7.1 - move fromWhere var here from below
		// so we can use it in extended context
        String fromWhere = request.getParameter("fromWhere");
      


        //cquinton 11/15/2011 Rel 7.1 bkul101931721
        // added retry logic for situations where imaging fails initially
        String pageNumber = request.getParameter(TradePortalConstants.PAGE_NUMBER);

        int maxNumberOfAttempts = DEFAULT_GET_IMAGE_MAX_ATTEMPTS;
        try {
            String tmp = servletProperties.getString("getImageMaxNumberOfAttempts");
            maxNumberOfAttempts = Integer.parseInt(tmp);
        } catch (Exception ex) {
            //do nothing just attempt once
            maxNumberOfAttempts = DEFAULT_GET_IMAGE_MAX_ATTEMPTS;
        }
        //
        //if external image call getExternalImage. CR 742 IValavala
        if (imageSourceType.equals(TradePortalConstants.EXTERNAL_IMAGE_TYPE)){
        		int attemptReturn = getExternalImage(docId, request, secretKey, response, user_oid, user_id);
        		if ( attemptReturn == GET_IMAGE_SUCCESS ) {
        			success = true;
        		}
        		else{
        			errorResource = "DocumentView.ImageViewError";
        		}
        }
        else{
        	for (int attempt = 0; attempt < maxNumberOfAttempts; attempt++) {
        		int attemptReturn = getImage(
        				docId, pageNumber, imageHash, fromWhere,
        				attempt+1, startTime, //pass in some logging info
        				response );
        		if ( attemptReturn == GET_IMAGE_SUCCESS ) {
        			success = true;
        			break;
        		}
        		if ( attempt==0 ) {
        			//save the first attempt error resource
        			// this is what we pass back to user
        			if ( attemptReturn == GET_IMAGE_HASH_ERROR ) {
        				errorResource = "DocumentView.ImageHashError";
        			} else { //GET_IMAGE_VIEW_ERROR
        				errorResource = "DocumentView.ImageViewError";
        			}
        		}
        	}
        	//cquinton 11/15/2011 Rel 7.1 end
        }

		
        if ( !success ) {
		    //cquinton 11/9/2011 Rel 7.1 - format differently if
		    // in image viewer or on view pdf page
            if ( "ViewPDF".equalsIgnoreCase( fromWhere ) ) {
                response.setContentType("text/html; charset=UTF-8");
            } else {
                response.setContentType("text/plain; charset=UTF-8");
            }
	    	ServletOutputStream sos = response.getOutputStream();
			//cquinton 9/15/2011 Rel 7.1 ppx240 - write appropriate error message
	    	String errorMessage = "";
	    	if ( resourceManager!=null ) {
			    errorMessage = "                     " +
			        resourceManager.getText( errorResource, TradePortalConstants.TEXT_BUNDLE) +
			        "                     ";
	    	} else { //default it - the session was not available
                errorMessage = "                     " +
                    "The image you are attempting to view is not available.  Please contact your bank for assistance." +
                    "                     ";
	    	}
			sos.write(errorMessage.getBytes());
	        sos.flush();
	        sos.close();
		}
		
    }

    
    //IValavala CR 742. Add new method getExternalImage

	private int getExternalImage(String docId, HttpServletRequest request,
			SecretKey secretKey, HttpServletResponse response, String user_oid,
			String userID) {
		String imgEndpoint = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("imgurl"), secretKey);
		String imgUserID = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("imgUsr"), secretKey);
		String imgPassword = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("imgPwd"), secretKey);
		String customerID = EncryptDecrypt.decryptStringUsingTripleDes(	request.getParameter("customer_id"), secretKey);
		String transaction_id = EncryptDecrypt.decryptStringUsingTripleDes(	request.getParameter("tran_id"), secretKey);
		//Generate and assign the values for serviceResponseHistoryOid, messageId
		UUID uniqueID = UUID.randomUUID();
		String uUID = uniqueID.toString();
		
		String serviceResponseHistoryOid = null;
		try {
			serviceResponseHistoryOid = Long.toString(ObjectIDCache.getInstance(AmsConstants.BANKQUERY).generateObjectID(false));
			
			long startTime = System.currentTimeMillis();
			Service service = new Service();
			Call call = (Call) service.createCall();

			call.setTargetEndpointAddress(new java.net.URL(imgEndpoint));
			// create a SOAP request Envelope
			SOAPEnvelope envelope = new SOAPEnvelope();
			// Set the Header Info
			// Set the Header Info
			if (! (StringFunction.isBlank(imgUserID) || StringFunction.isBlank(imgPassword) )){
			
			//IValavala CR 742.
			call.setUsername(imgUserID);
			call.setPassword(imgPassword);

			}
			
			// set Body Info
			String nameSpace1 = getNameSpaceFromSettings("WEBSERVICE_NAMESPACE_v1");
			String nameSpace1Prefix = "v1";
			if (StringFunction.isBlank(nameSpace1)) nameSpace1 = "urn:thisNamespace";
			envelope.addNamespaceDeclaration(nameSpace1Prefix, nameSpace1);
			
			
			SOAPBodyElement body = new SOAPBodyElement(nameSpace1,	"imageRequest");

			body.setPrefix(nameSpace1Prefix);
			body.removeNamespaceDeclaration(nameSpace1Prefix);
			
			String nameSpace2 = getNameSpaceFromSettings("WEBSERVICE_NAMESPACE_v11");
			String nameSpace2Prefix = "v11";
			if (StringFunction.isBlank(nameSpace2)) nameSpace2 = "urn:thisNamespace";
			envelope.addNamespaceDeclaration(nameSpace2Prefix, nameSpace2);
			//Build Request context
			
			SOAPBodyElement reqContext = new SOAPBodyElement(nameSpace2,	"requestContext");
			reqContext.setPrefix(nameSpace2Prefix);
			reqContext.removeNamespaceDeclaration(nameSpace2Prefix);
			body.addChildElement(reqContext);
			
			//requestUuid
			SOAPElement requestUuid = reqContext.addChildElement("requestUuid");
			
			
			
			requestUuid.addTextNode(uUID);
			
			//requestor
			SOAPElement requestor = reqContext.addChildElement("requestor");
			SOAPElement requestorAppName = requestor.addChildElement("requestorAppName");
			
			requestorAppName.addTextNode("TPL");
			SOAPElement requestorAppHost = requestor.addChildElement("requestorAppHost");
			requestorAppHost.addTextNode("");
			
			//intermediary
			
			SOAPElement intermediary = reqContext.addChildElement("intermediary");
			intermediary.addChildElement("requestorAppName");
			intermediary.addChildElement("requestorAppHost");

			//originator

			SOAPElement originator = reqContext.addChildElement("originator");
			SOAPElement orequestorAppName = originator.addChildElement("requestorAppName");
			orequestorAppName.addTextNode("TPL");
			originator.addChildElement("requestorAppHost");

			//requestDt
			SOAPElement requestDt = reqContext.addChildElement("requestDt");
			String datetime  = new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime()).toString();
			String date = datetime.substring(0, 10);
			String time = datetime.substring(11,datetime.length());
			datetime = date + "T" + time;
			requestDt.addTextNode(datetime);
			//requestHeader
			SOAPElement requestHeader = reqContext.addChildElement("requestHeader");
			requestHeader.addChildElement("key");
			requestHeader.addChildElement("value");

			//End requestHeader
			
			//End build request context
			
			SOAPElement bodyNode = body.addChildElement("customerID","");
			bodyNode.addTextNode(customerID);
			bodyNode = body.addChildElement("imageID","");
			bodyNode.addTextNode(docId);
			bodyNode = body.addChildElement("userID","");
			bodyNode.addTextNode(userID);


			envelope.addBodyElement(body);

			// Make an entry in service response history.
			createServiceResponseHistory(envelope.toString(),user_oid, transaction_id,uUID, serviceResponseHistoryOid);
			// Instantiate a SOAP response envelope
			SOAPEnvelope returnEnvelope = new SOAPEnvelope();
			// send the SOAP request to the service
			returnEnvelope = call.invoke(envelope);
			// Now interrogate the Response (return envelope)
			// and obtain Status, ImageFormat and ImageContent				
			SOAPBody returnBody = (SOAPBody) returnEnvelope.getBody();
			Iterator it = returnBody.getChildElements();
	        int count = 0;
	        String status="";
	        String ImageCntbase64="";
	        while (it.hasNext()) {
	            Object o = it.next();
	            SOAPElement bodyElement = (SOAPElement)o;
	            	Iterator it1 = bodyElement.getChildElements();
	            	String childName;
	            	 while (it1.hasNext()) {
	            		 SOAPElement childElement = (SOAPElement)(it1.next());
	            		 childName = childElement.getElementName().getLocalName();
	            		 if (childName.equals("status")){
	            			 Iterator it2 = childElement.getChildElements();
	            			 while (it2.hasNext()){
	            				 SOAPElement childElementstatus = (SOAPElement)(it2.next());
	    	            		 childName = childElementstatus.getElementName().getLocalName();
	    	            		 if (childName.equals("statusCode")) status = childElementstatus.getValue();
	            			 }
	            		 }
	            		 if (childName.equals("imageContent")) ImageCntbase64 = childElement.getValue();	            		 
	            	 }            
	            count++;
	        }
			
			String msgStatus = STATUS_ERROR;
			if ("0".equalsIgnoreCase(status)) {
				msgStatus = STATUS_COMPLETE;
			}
			// Update service response history.
			updateServiceResponseHistory(returnEnvelope.toString(), msgStatus, serviceResponseHistoryOid);
			if ("0".equalsIgnoreCase(status)) {
				byte[] imageData = Base64.decode(ImageCntbase64.getBytes());
				response.setContentType("application/pdf");
				
				response.setContentLength(imageData.length);
										
				ServletOutputStream sos = response.getOutputStream();
				sos.write(imageData);
				sos.flush();
				sos.close();
				long stopTime = System.currentTimeMillis();
				LOG.info("Doc id " + docId + " served as PDF in "
						+ (stopTime - startTime) + " ms." + " ContentType="
						+ response.getContentType());
				return GET_IMAGE_SUCCESS;

				// ---code snippet---
			} else {
				System.out
						.println("GetImageContentServlet.getExternalImage: WebServiceRequest failed to retrieve image from: "
								+ imgEndpoint + "Doc id " + docId);
				return GET_IMAGE_VIEW_ERROR;
			}

		} catch (Exception re) {
			System.out
					.println("GetImageContentServlet.getExternalImage: WebServiceRequest failed to retrieve image from: "
							+ imgEndpoint + "Doc id " + docId);
			re.printStackTrace();
			// Update service response history.
			updateServiceResponseHistory(
					"GetImageContentServlet.getExternalImage: WebServiceRequest failed to retrieve image from: "+ imgEndpoint+ " Doc id "+ docId+ ":"
							+ re.toString(), STATUS_ERROR, serviceResponseHistoryOid);
			return GET_IMAGE_VIEW_ERROR;
		}

	}

	//cquinton 11/15/2011 Rel 7.1 ir bkul101931721
    // created independent method to get the image to allow for retry logic
    /**
     * Attempt to get the image.
     * If successful, sets the image content on the http response, including setting content type.
     * If error, writes the specific error to the log and returns an error indicator.
     * To simply this method, it returns the following enumeration types:
     *   0 - success
     *   1 - view error
     *   2 - hash error
     */
    private int getImage(String docId, String pageNumber, String imageHash, String fromWhere,
            int attempt, long startTime,
            HttpServletResponse response) {
        int returnCode = GET_IMAGE_VIEW_ERROR; //default
        //cquinton 11/29/2011 Rel 7.1 close the connection
        javax.resource.cci.Connection connection = null;
        try {
            PropertyResourceBundle servletProperties = (PropertyResourceBundle)
                PropertyResourceBundle.getBundle("TradePortal");
            Context context = new InitialContext();
            javax.resource.cci.ConnectionFactory connectionFactory = (ConnectionFactory)
                context.lookup(servletProperties.getString(TradePortalConstants.ISRA_JNDI));

            ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
            FN_IS_CciConnectionSpec connectionSpec = new FN_IS_CciConnectionSpec(idandpass.getId(),idandpass.getPassword(),0);
            //cquinton 11/29/2011 Rel 7.1 close the connection
            connection = connectionFactory.getConnection(connectionSpec);

            if (connection != null)
            {
                Interaction interaction = connection.createInteraction();
                FN_IS_CciInteractionSpec interactionSpec = new FN_IS_CciInteractionSpec();
                RecordFactory recordFactory = connectionFactory.getRecordFactory();


                interactionSpec.setFunctionName("GetDocumentContent");

                MappedRecord inputRecord = recordFactory.createMappedRecord("DocContentRequest");
                inputRecord.put(TradePortalConstants.DOCUMENT_ID, Long.valueOf(docId));
                //cquinton 11/15/2011 Rel 7.1 pass in pageNumber from method args
                inputRecord.put(TradePortalConstants.PAGE_NUMBER, Integer.valueOf(pageNumber));

                MappedRecord outputRecord = (MappedRecord) interaction.execute(interactionSpec, inputRecord);

                String fileName = (String) outputRecord.get("file_name");
                String mimeType = (String) outputRecord.get("mime_type");
                InputStream inputStream = (InputStream)outputRecord.get("stream");
                LOG.info("************************* mimeType ->" + mimeType);

                //cquinton 11/9/2011 Rel 7.1 - move fromWhere var above
                
                
                if ( "ViewPDF".equalsIgnoreCase( fromWhere ) && "image/tiff".equalsIgnoreCase( mimeType ))
                {
                    //cquinton 7/21/2011 Rel 7.1.0 ppx240
                    //although we deal with image io below, we first need to
                    // validate the image hash against the raw input stream.
                    //so we read data here and then create a new stream from that
                    int availableBytes = inputStream.available();
                    byte[] data = new byte[availableBytes] ;
                    inputStream.read(data);
                    inputStream.close();

                    //after reading the image and before writing out, compare the hash
                    if ( validateImageHash( data, imageHash ) ) {
                        //cquinton 1/19/2012 Rel 8.0 start
                        //log image io cache directory
                        File cacheDirFile = ImageIO.getCacheDirectory();
                        if ( cacheDirFile != null ) {
                            String cacheDir = cacheDirFile.getPath();
                            LOG.info("ImageIO cache dir:" + cacheDir);
                        } else {
                            String tmpDir = System.getProperty("java.io.tmpdir"); 
                            LOG.info("ImageIO default cache dir:" + tmpDir);
                        }
                        //cquinton 1/19/2012 Rel 8.0 end
                        
                        ByteArrayInputStream bStream = new ByteArrayInputStream(data);
                        ImageInputStream iis = ImageIO.createImageInputStream(bStream);

                        //use the first available tif reader
                        Iterator readers = ImageIO.getImageReadersByFormatName("tif");
                        ImageReader imgRdr = (ImageReader)readers.next();
                        imgRdr.setInput(iis);

                        //cquinton 10/4/2011 Rel 7.1 move tif to pdf conversion to separate method
                        PDDocument doc = getTifAsPdf( imgRdr );

                        response.setContentType("application/pdf");
                        //IR 23373 Start- change file extension from .tif to .pdf
                        if(StringFunction.isNotBlank(fileName)){
                        	int lastDot = fileName.indexOf(".");
                        	if (lastDot != -1) {
                        		fileName = fileName.substring(0, lastDot) + ".pdf";
                        	} else {
                        		fileName = fileName + ".pdf";
                        		
                        	}
                        	fileName = fileName.replaceAll(".tmp", "");
                        }
                        //IR 23373 End
                        response.encodeURL(fileName);
                        response.addHeader("Content-disposition", "filename=" + response.encodeURL(fileName));
                        ServletOutputStream sos = response.getOutputStream();
                        doc.save(sos);
                        doc.close();
                        sos.flush();
                        sos.close();

                        iis.flush();
                        iis.close();
                        imgRdr.dispose();

                        long stopTime = System.currentTimeMillis();
                        LOG.info("Doc id " + docId + " served as PDF in " + (stopTime - startTime) + " ms." +
                            //cquinton 11/9/2011 Rel 7.1 - add content type for debugging if necessary
                            " ContentType=" + response.getContentType() +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_SUCCESS;
                    } else {
                        //hash comparison failed!
                        LOG.info("GetImageContentServlet: Hash comparison failure. " +
                            "Doc id " + docId +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_HASH_ERROR;
                    }
                }
                else
                {
                    //cquinton 7/21/2011 Rel 7.1.0 ppx240

                    //get data from the stream
                    int availableBytes = inputStream.available();
                    byte[] data = new byte[availableBytes] ;
                    inputStream.read(data);
                    inputStream.close();

                    //after reading the image and before writing out, compare the hash
                    if ( validateImageHash( data, imageHash ) ) {
                        //setup the response
                        response.setContentType(mimeType);
                        //cquinton 11/9/2011 Rel 7.1 - fudge the mimetype to pdf
                        if ( "ViewPDF".equalsIgnoreCase( fromWhere ) &&
                             "application/octet-stream".equals( mimeType) ) {
                            response.setContentType("application/pdf");
                            LOG.info("Attempting to view pdf, but mimetype is application/octet-stream. " +
                                "Explicity setting application/pdf");
                        }
                        response.encodeURL(fileName);
                        response.setContentLength(availableBytes);
                        response.addHeader("Content-disposition", "filename=" + response.encodeURL(fileName) + ";");

                        ServletOutputStream sos = response.getOutputStream();

                        sos.write(data);
                        sos.flush();
                        sos.close();

                        long stopTime = System.currentTimeMillis();
                        LOG.info("Doc id " + docId + " served in " + (stopTime - startTime) + " ms." +
                            //cquinton 11/9/2011 Rel 7.1 - add content type for debugging if necessary
                            " ContentType=" + response.getContentType() +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_SUCCESS;
                    } else {
                        //hash comparison failed!
                        LOG.info("GetImageContentServlet: Hash comparison failure. " +
                            "Doc id " + docId +
                            " Attempt=" + attempt );
                        returnCode = GET_IMAGE_HASH_ERROR;
                    }
                }
            }
            else {
                LOG.info("GetImageContentServlet: Connection null. " +
                    "Doc id " + docId +
                    " Attempt=" + attempt );
                returnCode = GET_IMAGE_VIEW_ERROR;
            }
        }
        catch(ResourceException re){
            //cquinton 11/2/2011 Rel 7.1 start - catch special condition of record not found.
            // print out an error, but no need to do a stack trace...
            if ( "FN_IS_RA_10363".equalsIgnoreCase( re.getErrorCode() ) ) {
                LOG.info("GetImageContentServlet: Document ID " +
                    docId + " not found!" +
                    " Attempt=" + attempt +
                    " " + re.getMessage() );
            } else {
                LOG.info("GetImageContentServlet: Resource Exception: " +
                    "Doc id " + docId +
                    " Attempt=" + attempt + " " +
                    " " + re.getMessage() );
                re.printStackTrace();
            }
            //cquinton 11/2/2011 Rel 7.1 end
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        catch(NamingException ne){
            LOG.info("GetImageContentServlet: Naming Exception: " +
                "Doc id " + docId +
                " Attempt=" + attempt + " " +
                " " + ne.getMessage() );
            ne.printStackTrace();
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        catch(Exception e){
            LOG.info("GetImageContentServlet: Exception: " +
                "Doc id " + docId +
                " Attempt=" + attempt + " " +
                " " + e.getMessage() );
            e.printStackTrace();
            returnCode = GET_IMAGE_VIEW_ERROR;
        }
        //cquinton 11/29/2011 Rel 7.1 close the connection
        finally {
            if ( connection!=null ) {
                try {
                    connection.close();
                    connection=null;
                }
                catch(Exception e){
                    LOG.info("GetImageContentServlet: Problem closing connection: " +
                        " " + e.toString() );
                    connection=null;
                }
            }
        }

        return returnCode;
    }



    //cquinton 8/16/2011 Rel 7.1.0 ppx240 start
	/**
	 * Validate the image hash.
	 *
	 * @param data - the image as a byte array - the byte array
	 *               length must be the actual length of the image
	 * @param compareHash
	 * @return
	 */
    private boolean validateImageHash( byte[] data, String compareHash ) {
        boolean isValid = false;

        if ( TradePortalConstants.NO_HASH.equals(compareHash) ) {
            //this is a special value to signify null hash value pulled from db
            //we don't use a blank hash value because its sent as a parm
            // and if we allowed that a request without a 'hash' parm
            // would be allowed to view images - its a simple security mechanism
            //a null hash value in the database means the image was stored
            // pre hash implementation, so allow it
            isValid = true;
        } else {
            if ( compareHash != null && compareHash.length() > 0 ) {
                //attempt to compare
                //use image length as the secret key
                String hashKey = String.valueOf(data.length);
                try {
                    byte[] keyBytes = hashKey.getBytes( AmsConstants.UTF8_ENCODING );
                    //the hash result will be exactly 128bits becuase its md5
                    //this is 16 bytes
                    byte[] hashValueBytes =
                        EncryptDecrypt.createHmacMD5Hash( data, keyBytes );
                    //the following converts the hash bytes as a hex string
                    //it doubles the length using the left shift operator
                    //because each byte is represented by 2 characters -
                    // which means that calculated hash is always 32 characters long
                    String calculatedHash = EncryptDecrypt.bytesToHexString( hashValueBytes );

                    if (compareHash.equals(calculatedHash)) { //note calculatedHash could be null!
                        //all is good
                        isValid = true;
                    }
                } catch ( Exception ex ) {
                    System.err.println("GetImageContentServlet: Problem validating image hash: " + ex.toString() );
                    ex.printStackTrace();
                }
            }
        }
        return isValid;
    }
    //cquinton 8/16/2011 Rel 7.1.0 ppx240 end

    //cquinton 10/4/2011 Rel 7.1.0 move tif to pdf conversion to separate method start
    private PDDocument getTifAsPdf(ImageReader imgRdr)
            throws IOException {
        PDDocument doc = new PDDocument();
        Integer pageCount = imgRdr.getNumImages(true);
        LOG.info("PageCount is [" + pageCount.toString() + "]");

        //some literals for pdf formatting
        int pdfResDpi = 72;
        int pdfResDpc = (int) (72 * 2.54);

        for (int i = 0; i < pageCount; i++) {
            BufferedImage bufImg = imgRdr.read(i);
            int imgWidth = imgRdr.getWidth(i);
            int imgHeight = imgRdr.getHeight(i);

            //get image metadata in order to calculate the correct
            // PDRectangle size for the image
            IIOMetadata imgMetadata = imgRdr.getImageMetadata(i);
            TIFFDirectory dir = TIFFDirectory.createFromMetadata(imgMetadata);
            BaselineTIFFTagSet base = BaselineTIFFTagSet.getInstance();
            //ResolutionUnit = 1-None/2-Inch/3-Centimeter
            int resUnit = dir.getTIFFField(296).getAsInt(0);
            //XResolution example: 300/1
            int xRes = dir.getTIFFField(282).getAsInt(0);
            //XResolution example: 300/1
            int yRes = dir.getTIFFField(283).getAsInt(0);

            //if rows per strip is greater than the height means
            // we have more than one strip, which is not supported by pdf (or pdfbox)
            if (imgHeight > dir.getTIFFField(278).getAsInt(0)) {
                //so manipulate the metadata to force a single strip
                dir.removeTIFFField(278);   // RowsPerStrip
                TIFFTag tagRowsPerStrip = base.getTag(BaselineTIFFTagSet.TAG_ROWS_PER_STRIP) ;
                TIFFField fieldRowsPerStrip = new TIFFField(tagRowsPerStrip, TIFFTag.TIFF_SHORT, 1, new char[]{(char)imgHeight});
                dir.addTIFFField(fieldRowsPerStrip);
            }
            //default the x/y resolution to the other if it seems wrong
            if (xRes == 1 && yRes > 1) xRes = yRes;
            if (yRes == 1 && xRes > 1) yRes = xRes;

            float xPixelRatio, yPixelRatio;
            if (resUnit == 2) { // Inches
                xPixelRatio = xRes/pdfResDpi;
                yPixelRatio = yRes/pdfResDpi;
            }
            else if (resUnit == 3) { // Centimeters
                xPixelRatio = xRes/pdfResDpc;
                yPixelRatio = yRes/pdfResDpc;
            }
            else { // No Resolution Units
                xPixelRatio = 1;
                yPixelRatio = 1;
            }

            //get the first available tif image writer
            Iterator writers = ImageIO.getImageWritersByFormatName("tif");
            ImageWriter imgWrtr = (ImageWriter)writers.next();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
            imgWrtr.setOutput(ios);
            //write the image with possibly modified metadata
            IIOImage iioImage = new IIOImage(bufImg, null, imgMetadata);
            imgWrtr.write(imgMetadata, iioImage, null);          
            if(ios!=null){
            	ios.flush();
                ios.close();	
            }
            
            //convert to pdf via pdfbox
            PDRectangle pdRect = new PDRectangle(imgWidth / xPixelRatio, imgHeight / yPixelRatio);
            PDPage page = new PDPage(pdRect);
            doc.addPage(page);

            org.apache.pdfbox.io.RandomAccessBuffer rab = new org.apache.pdfbox.io.RandomAccessBuffer();
            rab.write(baos.toByteArray(), 0, baos.size());

            PDXObjectImage ximage = new PDCcitt(doc, rab);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);
            contentStream.drawXObject(ximage, 0, 0, imgWidth/xPixelRatio, imgHeight/yPixelRatio);
            contentStream.close();

            imgWrtr.dispose();
        }

        return doc;
    }
    //cquinton 10/4/2011 Rel 7.1.0 end
    
    //IValavala CR742 Rel 8.2
    private void updateServiceResponseHistory(String responseMsg, String msgStatus, String serviceResponseHistoryOid){
    	//MessageID will not be used.

    	String UPDATE_SQL = "update SERVICE_RESPONSE_HISTORY set time_received=?, status = ?, response_msg=? where service_response_hist_oid=?";
    	try(Connection con = DatabaseQueryBean.connect(true);
			PreparedStatement pStmt =  con.prepareStatement(UPDATE_SQL)) {
			
			pStmt.setTimestamp(1, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); 
			pStmt.setString(2,msgStatus);
			pStmt.setString(3,responseMsg);
			pStmt.setString(4, serviceResponseHistoryOid);
			int rowCount = pStmt.executeUpdate();
			

		} catch (AmsException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }
    
    private void createServiceResponseHistory(String requestMsg,  String user_oid, String transaction_id, String uUID, String serviceResponseHistoryOid){
		 String CREATE_SQL = "insert into SERVICE_RESPONSE_HISTORY (service_response_hist_oid, time_created, message_id, a_user_oid, A_TRANSACTION_OID, msg_type, request_msg, status, uuid)values(?,?,?,?,?,?,?,?,?)";
			try(Connection con = DatabaseQueryBean.connect(true);
				PreparedStatement pStmt =  con.prepareStatement(CREATE_SQL)) {
				
				pStmt.setString(1, serviceResponseHistoryOid);
				pStmt.setTimestamp(2, new java.sql.Timestamp(GMTUtility.getGMTDateTime(true,false).getTime())); //time created     // NSX  IR# NTUL121466003 Rel. 7.1.0  12/12/11
				pStmt.setString(3, "");
				pStmt.setString(4, user_oid);
				pStmt.setString(5, transaction_id);
				pStmt.setString(6, "IMAGEREQUEST");
				pStmt.setString(7, requestMsg);
				pStmt.setString(8, STATUS_PENDING);
				pStmt.setString(9, uUID);
				
				int rowCount = pStmt.executeUpdate();
				
			} catch (AmsException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	
    }
   

    private String getNameSpaceFromSettings(String nameSpace) throws  AmsException{
		StringBuffer configSettingQuery = new StringBuffer();
		configSettingQuery.append("SELECT SETTING_VALUE FROM CONFIG_SETTING ");
		configSettingQuery.append(" WHERE SETTING_NAME = ? AND CLASS_NAME = ? " );
		DocumentHandler xmlDoc = DatabaseQueryBean.getXmlResultSet(configSettingQuery.toString(), false, nameSpace, "EXTERNAL_IMAGE_REQUEST");
		String nameSpaceValue = xmlDoc.getAttribute("/ResultSetRecord(0)/SETTING_VALUE");
		return nameSpaceValue;
    }
}
