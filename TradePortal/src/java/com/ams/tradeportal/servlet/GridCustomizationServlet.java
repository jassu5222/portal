package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ams.tradeportal.busobj.GridColumnCustomization;
import com.ams.tradeportal.busobj.GridCustomization;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.html.DataGridFactory;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.FormManager;

/**
 * Handles ajax call for grid customization actions.
 */
public class GridCustomizationServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(GridCustomizationServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doTheWork(request, response);
    }

    /**
     * This method implements the standard servlet API for GET and POST
     * requests. It handles uploading of files to the Trade Portal.
     * 
     * @param javax
     *            .servlet.http.HttpServletRequest request - the Http servlet
     *            request object
     * @param javax
     *            .servlet.http.HttpServletResponse response - the Http servlet
     *            response object
     * @exception javax.servlet.ServletException
     * @exception java.io.IOException
     * @return void
     * @throws AmsException
     */

    public void doTheWork(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    
        
        String output = "<response>";
        boolean errors = false;
        String errorMessage = "";

        //get the user's resource manager from the session 
        HttpSession session = request.getSession(false); //don't create a new one, return null if none
        ResourceManager resMgr = null;
        SessionWebBean userSession = null;
        FormManager formMgr = null;
        if (session!=null) {
            resMgr = (ResourceManager) session.getAttribute("resMgr");
            userSession = (SessionWebBean) session.getAttribute("userSession");
            formMgr = (FormManager) session.getAttribute("formMgr");
        } 
        else {
            throw new ServletException("Could not obtain session.");
        }
        if (resMgr==null) {
            //this would only happen if the calling servlet did not set the resource manager bean
            throw new ServletException("Could not obtain resource manager.");
        }
        if (userSession==null) {
            //this would only happen if the calling servlet did not set the user session bean
            throw new ServletException("Could not obtain user session bean.");
        }
        if (formMgr==null) {
            //this would only happen if the calling servlet did not set the form manager bean
            throw new ServletException("Could not obtain form manager.");
        }
        
        String userOid = userSession.getUserOid();

        String action = request.getParameter("action");
        String actionOutput = "";
        if ( "save".equals(action) ) {
            try {
                saveGridCustomization(userOid, request);
                //note there is no action output here
            } 
            catch (Exception ex) {
                LOG.error("Problem saving grid customization. " , ex);
                errors = true;
                errorMessage = "Problem saving grid customization: " + ex.toString();
            }
        } 
        else if ( "reset".equals(action) ) {
            try {
                actionOutput = resetGridCustomization(userOid, request, response, 
                    resMgr, userSession, formMgr);
            }
            catch (Exception ex) {
                LOG.error("Problem resetting grid customization. " , ex);
                errors = true;
                errorMessage = "Problem resetting grid customization: " + ex.toString();
            }
        } 
        else {
            LOG.info("Invalid action parameter: {}" , action);
            errors = true;
            errorMessage = "Invalid action";
        } 

        //todo: check for no errors!
        if ( !errors ) {
            output += "<responseCode>SUCCESS</responseCode>";
            output += actionOutput;
        }
        else {
            output += "<responseCode>ERROR</responseCode>";
            output += "<errorMessage>" + errorMessage + "</errorMessage>";
        }
        output += "</response>";
        
        response.setContentType("text/xml");

        PrintWriter pw = response.getWriter();
      //Validation to check Cross Site Scripting	  
		if(output != null){
			output = StringFunction.xssCharsToHtml(output);
		  }
        pw.write(output);
        pw.flush();
    }

    /**
     * Save the grid customization data.
     * 
     * @param userOid
     * @param request
     * @param response
     * @return
     */
    private void saveGridCustomization(String userOid, HttpServletRequest request)
            throws RemoteException, AmsException {

        String gridId = request.getParameter("gridId");
        
        String gridCustOid = getGridCustomizationOid(userOid, gridId);
        if ( gridCustOid == null ) {
            insertGridCustomization(userOid, gridId, request);
        }
        else { 
            updateGridCustomization(gridCustOid, request);
        }
    }

    private void insertGridCustomization(String userOid, String gridId,
            HttpServletRequest request)
            throws RemoteException, AmsException {

        String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

        GridCustomization gridCust = (GridCustomization) 
            EJBObjectFactory.createClientEJB(serverLocation,"GridCustomization");
        gridCust.newObject();
        
        // Set object attributes equal to input arguments and save
        gridCust.setAttribute("user_oid", userOid);
        gridCust.setAttribute("grid_id", gridId);
        String gridName = request.getParameter("gridName");
        gridCust.setAttribute("grid_name", gridName);

        addGridColumns( gridCust, request );
        
        int saveResult = gridCust.save();
    }
    
    private void updateGridCustomization(String gridCustOid,
            HttpServletRequest request)
            throws RemoteException, AmsException {

        String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
        GridCustomization gridCust = (GridCustomization) 
            EJBObjectFactory.createClientEJB(serverLocation,"GridCustomization");
        gridCust.getData(Long.parseLong(gridCustOid));        

        //remove the old columns stuff....
        gridCust.deleteComponent("GridColumnCustomizationList");
        
        addGridColumns( gridCust, request );
        
        int saveResult = gridCust.save();
    }
    
    private void addGridColumns(GridCustomization gridCust, HttpServletRequest request)
            throws RemoteException, AmsException {
        int colIdx = 0;
        String columnId = request.getParameter("columnId"+colIdx);
        while (columnId!=null) {
            String width = request.getParameter("width"+colIdx);
            
            long gridColOid = gridCust.newComponent("GridColumnCustomizationList");
            GridColumnCustomization gridColumn = (GridColumnCustomization)
                gridCust.getComponentHandle("GridColumnCustomizationList", gridColOid);

            gridColumn.setAttribute("column_id", columnId);
            //display order is 1 based
            gridColumn.setAttribute("display_order", ""+(colIdx+1)); 
            gridColumn.setAttribute("width", width);

            colIdx++;
            
            columnId = request.getParameter("columnId"+colIdx);
        }
    }

    /**
     * 
     * @param userOid
     * @param request
     * @param response
     * @return - string output to be evaluated by client.  for reset this is the 
     *           reset grid layout
     */
    private String resetGridCustomization(String userOid, 
            HttpServletRequest request, HttpServletResponse response,
            ResourceManager resMgr, SessionWebBean userSession, FormManager formMgr)
            throws RemoteException, AmsException {
        String actionOutput = "<gridLayout>";
        
        String gridId = request.getParameter("gridId");

        String gridCustOid = getGridCustomizationOid(userOid, gridId);
        
        if ( gridCustOid!=null ) {
            String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
            GridCustomization gridCust = (GridCustomization) 
                EJBObjectFactory.createClientEJB(serverLocation,"GridCustomization");
            gridCust.getData(Long.parseLong(gridCustOid));
            gridCust.delete();
        }
        
        //now pull in the default grid layout and return
        String gridName = request.getParameter("gridName");
        DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
        String gridLayout = dgFactory.createGridLayout(gridId, gridName);
        actionOutput += gridLayout;
        
        actionOutput += "</gridLayout>";
        
        return actionOutput;
    }

    /**
     * Returns the grid customization oid for the given user/gridId
     * or null if not found.
     * 
     * @param userOid
     * @param gridId
     * @return
     */
    private String getGridCustomizationOid(String userOid, String gridId)
            throws AmsException {
        String gridCustOid = null;
      //jgadela - 07/15/2014 - Rel 9.1 IR#T36000026319 - SQL Injection fix
        String sql = "select grid_cust_oid from grid_customization where a_user_oid = ? and grid_id = ?";
        DocumentHandler doc = DatabaseQueryBean.getXmlResultSet(sql, false, userOid, gridId);
        
        if (doc != null) {
           Vector gridCustList = doc.getFragments("/ResultSetRecord");
           if ( gridCustList.size()>0 ) { //assume 1
               DocumentHandler gridCustDoc = (DocumentHandler) gridCustList.get(0);
               gridCustOid = gridCustDoc.getAttribute("/GRID_CUST_OID");
           }
        }

        return gridCustOid;
    }
    
}
