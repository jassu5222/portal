package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.crypto.SecretKey;
import javax.ejb.EJBObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.ams.tradeportal.busobj.InvoiceFileUpload;
import com.ams.tradeportal.busobj.InvoiceLineItemDetail;
import com.ams.tradeportal.busobj.InvoicesSummaryData;
import com.ams.tradeportal.busobj.util.InstrumentServices;
import com.ams.tradeportal.busobj.webbean.InvoiceDefinitionWebBean;
import com.ams.tradeportal.busobj.webbean.SessionWebBean;
import com.ams.tradeportal.common.Debug;
import com.ams.tradeportal.common.InvoiceFileDetails;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.ClientServerDataBridge;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.MediatorServices;
import com.amsinc.ecsg.frame.NavigationManager;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.EncryptDecrypt;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.web.BeanManager;
import com.amsinc.ecsg.web.FormManager;
// Manohar - CR 597 added

/**
 * The InvoiceFileUploadServlet class is used when a user
 * uploads a Invoice data file to the Trade Portal for processing.
 *
 * Shilpa R CR-710  - Invoice Management
 * Release 8.0.0.0
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
 */

/**
 *
 * Invoice File Format - Invoice file can be formatted as comma, tab, semi-colon
 * or pipe-delimited
 */

public class InvoiceFileUploadServlet extends HttpServlet {
private static final Logger LOG = LoggerFactory.getLogger(InvoiceFileUploadServlet.class);

	private static final int BOM_SIZE = 3;
	private static final long MAX_FILE_SIZE = Long
			.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
					"uploadFileMaxSize", "23068672")); // 22MB
	private static final String MAX_FILE_SIZE_IN_MB = getMaxFileSizeInMB();
	private static final long MAX_FILE_INVOICES = Long
			.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
					"uploadFileMaxInvoices", "10000"));
	private static final long MAX_FILE_INV_LINE_ITEMS = Long
	.parseLong(TradePortalConstants.getPropertyValue("TradePortal",
			"uploadINVFileMaxLineItems", "600")); // Nar IR MMUM052582934

	


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doTheWork(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doTheWork(request, response);
	}

	/**
	 * This method implements the standard servlet API for GET and POST
	 * requests. It handles uploading of files to the Trade Portal.
	 *
	 * @param javax
	 *            .servlet.http.HttpServletRequest request - the Http servlet
	 *            request object
	 * @param javax
	 *            .servlet.http.HttpServletResponse response - the Http servlet
	 *            response object
	 * @exception javax.servlet.ServletException
	 * @exception java.io.IOException
	 * @return void
	 * @throws AmsException
	 */

	public void doTheWork(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

    	//T36000005819 W Zhu 9/24/12 Do not allow direct access to the servlet.
    	if (NavigationManager.bookmarkedAccessDenied(request, response, this)) {
    		return;
    	}
                
    
        
		// Retrieve the form manager and resource manager from the session so
		// that
		// we can issue errors or create an invoice upload record if necessary
		String fileName = null;
		HttpSession session = request.getSession(true);
		ResourceManager resourceManager = (ResourceManager) session
				.getAttribute("resMgr");
		FormManager formManager = (FormManager) session.getAttribute("formMgr");
		SessionWebBean userSession = (SessionWebBean) session
				.getAttribute("userSession");
		
		MediatorServices mediatorServices = new MediatorServices();
		try {
			

			mediatorServices.getErrorManager().setLocaleName(
					userSession.getUserLocale());

			if (!ServletFileUpload.isMultipartContent(request)) {
				throw new RuntimeException(
						"Request does not have multipart....");
			}

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// factory.setSizeThreshold(100000);

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(MAX_FILE_SIZE);

			// Parse the request
			LOG.debug("Before parsing request..");
			List items = upload.parseRequest(request);
			LOG.debug("After parsing request..");

			
			String invPayDataOption = ""; //SHR CR708 Rel8.1.1
			String invUploadInstrOption = ""; // DK CR-709 Rel8.2
			String invUploadloanType = ""; // DK CR-709 Rel8.2
			String invItemsGroupingOption ="";
			// Process the uploaded items
			try {
				//	W Zhu 8/22/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
				SecretKey secretKey = userSession.getSecretKey();
				
				Iterator iter = items.iterator();
				boolean a = false;
				boolean b = false;
				FileItem invFileItem = null;
				String invDefOid = null;
			    String formDataNi="";				
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (!item.isFormField()) {
						fileName = FilenameUtils.getName(item.getName());
						invFileItem = item;
						a = true;

					} else {
						String name = item.getFieldName();
						String value = item.getString();
						if (name.startsWith("invoiceDefinitionName")
								&& null != value && !value.equals("")) {
							 
							value=EncryptDecrypt.decryptStringUsingTripleDes(value, secretKey);
							
							invDefOid = value;
							b = true;
						}//SHR CR708 Rel8.1.1 Begin
						else if (name.startsWith("InvPayUploadDataOption")
								&& null != value && !value.equals("")) {
							invPayDataOption = value;

						}
						//SHR CR708 Rel8.1.1 End
						// DK CR-709 Rel8.2 Starts
						else if (name.startsWith("InvUploadInstrOption")
								&& null != value && !value.equals("")) {
							invUploadInstrOption = value;
						}
						else if (name.startsWith("loanType")
								&& null != value && !value.equals("")) {
							invUploadloanType = value;
						}
						else if (name.startsWith("InvItemsGroupingOption")
								&& null != value && !value.equals("")) {
							invItemsGroupingOption = value;
						}
						// DK CR-709 Rel8.2 Ends
						else if (name.startsWith("ni") && StringFunction.isNotBlank(value)) {
							formDataNi = value;
						}
					}
					}
				
					//csrf check
					//compare form data ni to the one in hashtable in formManager
					//
				    if (formManager == null) {
						throw new RuntimeException("formManager is NULL");				    	
				    }else{
				      if (StringFunction.isNotBlank(formDataNi)){
				        formManager.getServletInvocation (formDataNi,request); 
				      }else{
				    	  throw new RuntimeException("form ni value is missing");
				      }
				    }				

					if (a && b) {
						//SHR CR708 Rel8.1.1 Change
						// DK CR-709 Rel8.2
						processUploadedFile(invDefOid, invFileItem,invPayDataOption, invUploadInstrOption,invUploadloanType,
								resourceManager, userSession, mediatorServices, invItemsGroupingOption, request);
						a = false;
						b = false;
					}



			} catch (AmsException e) {
				// TODO Auto-generated catch block
				System.out
						.println("InvoiceFileUploadServlet: Error occured during upload, file:'"
								+ fileName + "' Reason:" + e.getMessage());
				e.printStackTrace();

			}

			

			try {
				handleError(mediatorServices, formManager);
			} catch (AmsException e) {
				// TODO Auto-generated catch block
				
				throw new ServletException(e);
			}
			return;

		} catch (SizeLimitExceededException ex) {
			try {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.INVOICE_FILE_SIZE_EXCEEDED,
						MAX_FILE_SIZE_IN_MB + "");
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out
					.println("InvoiceFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + ex.getMessage());
			ex.printStackTrace();
			throw new ServletException(ex);
		} catch (Exception e) {
			try {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ERR_SAVING_INVOICEUPLOAD_FILE);
				handleError(mediatorServices, formManager);
			} catch (AmsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out
					.println("InvoiceFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + e.getMessage());
			e.printStackTrace();

			throw new ServletException(e);
		}
	}

	protected void handleError(MediatorServices mediatorServices,
			FormManager formManager) throws AmsException, ServletException,
			IOException {

		LOG.debug("Inside hanldeError..");
		DocumentHandler xmlDoc = formManager.getFromDocCache("default.doc");

		mediatorServices.addErrorInfo();
		xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
		xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
		xmlDoc.setAttribute("/In/maxErrorSeverity", String.valueOf(mediatorServices.getErrorManager().getMaxErrorSeverity()));
		formManager.storeInDocCache("default.doc", xmlDoc);

	}

	public void processUploadedFile(String invDefOid, FileItem item,String invPayDataOption,
			String invUploadInstrOption, String invUploadloanType,
			ResourceManager resourceManager, SessionWebBean userSession,
			MediatorServices mediatorServices, String invItemsGroupingOption, HttpServletRequest request) throws AmsException, IOException {

		InvoiceFileUpload invoiceFile = null;
		File file = null;
		String fileName = FilenameUtils.getName(item.getName());
		//RKAZI REL 8.2 CR 709 - START
		
		String                   subsidiaryAccessOrgOid = null;
		
		String                   invDataFilePath         = fileName;
		
	    String invDataOption           = invPayDataOption;
	    String invoiceUploadInstrOption = invUploadInstrOption;
	    DocumentHandler          invUploadParametersDoc  = null;
	      FormManager              formManager            = null;
	      HttpSession              session = request.getSession(true);

	      formManager     = (FormManager)     session.getAttribute("formMgr");
	      String                   invUploadDefinition     = invDefOid;
	      BeanManager	 beanMgr = (BeanManager) session.getAttribute("beanMgr");
	  //	SecretKey secretKey = ((SessionWebBean)request.getSession(false).getAttribute("userSession")).getSecretKey();


		//RKAZI REL 8.2 CR 709 END
		
		
		 
		/*
		 * if (fileName.length() > AbstractInvoiceFileDetail.MAX_FILE_NAME_SIZE)
		 * { mediatorServices.getErrorManager().issueError(TradePortalConstants.
		 * ERR_CAT_1, TradePortalConstants.PAYMENT_FILENAME_SIZE_EXCEEDED);
		 * return; }
		 */
		try {

			invoiceFile = createInvoiceFileUpload();
			file = getTempFile(invoiceFile);
			invoiceFile.setAttribute("invoice_file_name", fileName);
			invoiceFile.setAttribute("user_oid", userSession.getUserOid());
			invoiceFile.setAttribute("owner_org_oid",
					userSession.getOwnerOrgOid());
			invoiceFile.setAttribute("validation_status",
					TradePortalConstants.PYMT_UPLOAD_VALIDATION_PENDING);
			invoiceFile.setAttribute("system_name", "MIDDLEWARE");
			invoiceFile.setAttribute("server_name", userSession.getServerName());
			
		           
            if(StringFunction.isBlank(invItemsGroupingOption)){
            	invItemsGroupingOption = TradePortalConstants.USE_ONLY_FILE_PO_LINE_ITEMS;
            }
            // If user is using subsidiary access and is able to see their own reference data while doing so, include the org OID here
            if(userSession.showOrgDataUnderSubAccess())
                subsidiaryAccessOrgOid = userSession.getSavedUserSession().getOwnerOrgOid();

            // If the PO data option specified by the user is to group the PO items based on LC creation
            // rules, then get the grouping option that was selected as well
             
        	if (StringFunction.isBlank(invDataOption)) {
			
                issueINVUploadError(formManager, TradePortalConstants.INV_DATA_REQD, invUploadDefinition,
              		  invDataOption, invItemsGroupingOption, invoiceUploadInstrOption,userSession.getUserLocale(),mediatorServices, fileName, invUploadloanType);

        		return;
			}
        	
            
            if (invDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS))
            {
               
               // TLE - 07/29/07 - CR-375 - Add Begin
               invoiceUploadInstrOption = invUploadInstrOption;

               if (invoiceUploadInstrOption == null || invoiceUploadInstrOption.equals("") || invItemsGroupingOption == null || invItemsGroupingOption.equals("")) {
                  issueINVUploadError(formManager, TradePortalConstants.AUTO_UPLOAD_INV_INSTR_TYPE_MISSING, invUploadDefinition,
                		  invDataOption, invItemsGroupingOption, invoiceUploadInstrOption,userSession.getUserLocale(),mediatorServices, fileName, invUploadloanType);
                   return;
               }
               // TLE - 07/29/07 - CR-375 - Add End
            }

            // Retrieve the sequence number for this PO upload
           String  invUploadSequenceNumber = invoiceFile.getAttribute("invoice_file_upload_oid");

            // Retrieve all necessary data for the PO Data parsing background process to run after
            // the upload has finished
            invUploadParametersDoc = getINVUploadParametersDoc(userSession, invUploadSequenceNumber, invUploadDefinition,
                                                             invDataOption, invItemsGroupingOption, invoiceUploadInstrOption ,invDataFilePath, subsidiaryAccessOrgOid, invUploadloanType);

            
            // DK CR-709 Rel8.2 Ends
            invoiceFile.setAttribute("invoice_upload_parameters",invUploadParametersDoc.toString());
			//SHR CR708 Rel8.1.1 End
			if(writetoDisk(invDefOid, item, file, invoiceFile, resourceManager,
					userSession, mediatorServices,beanMgr)){

			if (invoiceFile.save() != 1) {
				throw new RuntimeException("error saving InvoiceFileUpload");
			}
			}

		} catch (Exception e) {
			System.out
					.println("InvoiceFileUploadServlet: Error occured during upload, file:'"
							+ fileName + "' Reason:" + e.getMessage());
			e.printStackTrace();
			cleanup(file);
			if (mediatorServices.getErrorManager().getIssuedErrorsList().size() == 0) {
				mediatorServices.getErrorManager().issueError(
						TradePortalConstants.ERR_CAT_1,
						TradePortalConstants.ERR_SAVING_INVOICEUPLOAD_FILE);
			}
		} finally {
			if (invoiceFile != null) {
				try {
					invoiceFile.remove();
				} catch (Exception ex) {
				}
			}
		}
	}
	   /**
	    * This method issues an error message using the error code passed in. It
	    * also stores appropriate error info and any data previously selected by
	    * the user in the XML doc currently in the doc cache so that the errors
	    * will apear in the page that's returned to.
	    *

	 * @param mediatorServices 
	 * @param fileName 
	 * @param invUploadloanType 
	    * @param      com.amsinc.ecsg.web.FormManager formManager - the form manager object for the user's session
	    * @param      java.lang.String errorCode - the unique code of the error to issue
	    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
	    * @param      java.lang.String poDataOption - the PO data option that was selected
	    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
	    * @param      java.lang.String localeName - locale used to generate the error
	    * @exception  com.amsinc.ecsg.frame.AmsException
	    * @return     void
	    */
	   private void issueINVUploadError(FormManager formManager, String errorCode, String poUploadDefinition,
	                                   String poDataOption, String poItemsGroupingOption, String poUploadInstrOption,String localeName, MediatorServices mediatorServices, String fileName, String invUploadloanType) throws AmsException
	   {
	      
	      DocumentHandler    xmlDoc           = null;

	      xmlDoc = formManager.getFromDocCache();


	      mediatorServices.getErrorManager().setLocaleName(localeName);
	      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, errorCode);


	      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	      xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);

	      xmlDoc.setAttribute("/In/InvPayUploadDataOption", poDataOption);
	      xmlDoc.setAttribute("/In/PaymentDataFilePath", fileName);
	      xmlDoc.setAttribute("/In/loanType", invUploadloanType);
	      
	      // If we're going back to the same page that made the PO upload request, return all
	      // data selected by the user on the page
	      if (poUploadDefinition != null)
	      {
	         xmlDoc.setAttribute("/In/invoiceDefinitionName", poUploadDefinition);

	      }

	      if (poDataOption.equals(TradePortalConstants.GROUP_PO_LINE_ITEMS))
	      {
	         xmlDoc.setAttribute("/In/InvItemsGroupingOption", poItemsGroupingOption);


	         xmlDoc.setAttribute("/In/InvUploadInstrOption", poUploadInstrOption);

	      }

	      formManager.storeInDocCache("default.doc", xmlDoc);
	   }	
	   /**
	    * This method saves PO and user data to an XML document for a PO parsing
	    * background process to pick up after the PO upload has finished.
	    *

	    * @param      com.ams.tradeportal.busobj.webbean.SessionWebBean userSession - the current user's Trade Portal session
	    * @param      java.lang.String poUploadSequenceNumber - the upload sequence number for the current PO upload
	    * @param      java.lang.String poUploadDefinition - the oid of the PO upload definition if one was selected
	    * @param      java.lang.String poDataOption - the PO data option that was selected
	    * @param      java.lang.String poItemsGroupingOption - the PO items grouping option that was selected
	    * @param      java.lang.String poUploadInstrOption- the instrument type (LC, ATP, etc.) option that was selected
	    * @param      java.lang.String poDataFilePath - the full path of the PO Data file if one was selected
	    * @exception  com.amsinc.ecsg.frame.AmsException
	    * @return     com.amsinc.ecsg.util.DocumentHandler - an XML document containing all necessary PO data
	    *                                                    for the PO parsing background process to function
	    */
	   private DocumentHandler getINVUploadParametersDoc(SessionWebBean userSession, String poUploadSequenceNumber,
	                                                    String poUploadDefinition, String poDataOption,
	                                                    String poItemsGroupingOption, String poUploadInstrOption, 
	                                                    String poDataFilePath, String subsidiaryAccessOrgOid,
	                                                    String invUploadloanType)
	                                                    throws AmsException
	   {
	      DocumentHandler   poUploadParametersDoc = null;

	      // Set up the PO upload parameters doc for the background process to pick up
	      poUploadParametersDoc = new DocumentHandler();
	      poUploadParametersDoc.setAttribute("/In/InvoiceDataUploadInfo/uploadDefinitionOid", poUploadDefinition);
	      poUploadParametersDoc.setAttribute("../uploadSequenceNumber",                  poUploadSequenceNumber);
	      poUploadParametersDoc.setAttribute("../InvPayDataOption",                          poDataOption);
	      poUploadParametersDoc.setAttribute("../SecretKey",                          userSession.getSecretKey().toString());
	      poUploadParametersDoc.setAttribute("../loanType",                          invUploadloanType);
	      
	      
	      

	      if(subsidiaryAccessOrgOid != null)
	         poUploadParametersDoc.setAttribute("../subsidiaryAccessOrgOid",                subsidiaryAccessOrgOid);


	      if (poItemsGroupingOption != null)
	      {
	         poUploadParametersDoc.setAttribute("../InvItemsGroupingOption", poItemsGroupingOption);
	      }

	      if (poUploadInstrOption!= null)
	      {
	         poUploadParametersDoc.setAttribute("../InvUploadInstrOption", poUploadInstrOption);
	      }


	      poUploadParametersDoc.setAttribute("/In/User/userOid",    userSession.getUserOid());
	      poUploadParametersDoc.setAttribute("../timeZone",         userSession.getTimeZone());
	      poUploadParametersDoc.setAttribute("../locale",           userSession.getUserLocale());
	      poUploadParametersDoc.setAttribute("../securityRights",   userSession.getSecurityRights());
	      poUploadParametersDoc.setAttribute("../baseCurrencyCode", userSession.getBaseCurrencyCode());
	      poUploadParametersDoc.setAttribute("../clientBankOid",                         userSession.getClientBankOid());
	      poUploadParametersDoc.setAttribute("../corporateOrgOid",                        userSession.getOwnerOrgOid());
	      if(subsidiaryAccessOrgOid != null)
		         poUploadParametersDoc.setAttribute("../subsidiaryAccessOrgOid",                subsidiaryAccessOrgOid);
	      poUploadParametersDoc.setAttribute("../poDataFilePath",                        poDataFilePath);
	      return poUploadParametersDoc;
	   }


	protected InvoiceFileUpload createInvoiceFileUpload() throws AmsException,
			RemoteException {
		LOG.debug("Creating InvoiceFileUplaod..");
		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");
		InvoiceFileUpload invoicefile = (InvoiceFileUpload) EJBObjectFactory
				.createClientEJB(serverLocation, "InvoiceFileUpload");
		invoicefile.newObject();
		invoicefile.setAttribute("creation_timestamp",
				DateTimeUtility.getGMTDateTime(true, false));

		return invoicefile;
	}

	protected void removeEJBReference(EJBObject objRef) {
		try {
			if (objRef != null) {
				objRef.remove();
				objRef = null;
			}
		} catch (Exception e) {
			LOG.info("while removing EJB reference ...'" + "' Reason:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * This method returns the encoding type by checking the Byte Order Marker
	 * 
	 * @param byte[] - byte array
	 * @return String
	 * @throws IOException
	 */
	private String getEncoding(PushbackInputStream is) throws IOException {
		String encoding = "ASCII";
		int read = 0;
		byte bom[] = new byte[BOM_SIZE];

		is.read(bom, 0, bom.length);

		if ((bom[0] == (byte) 0xFF) && (bom[1] == (byte) 0xFE)) {
			encoding = "UTF-16LE";
			read = 2;
		} else if ((bom[0] == (byte) 0xFE) && (bom[1] == (byte) 0xFF)) {
			encoding = "UTF-16BE";
			read = 2;
		} else if ((bom[0] == (byte) 0xEF) && (bom[1] == (byte) 0xBB)
				&& (bom[2] == (byte) 0xBF)) {
			encoding = "UTF-8";
			read = 3;
		}
		is.unread(bom, read, BOM_SIZE - read);
		return encoding;
	}

	/**
	 * This method returns the Uploaded Invoice file
	 * 
	 * @param InvoiceFileUpload
	 *            -
	 * @return File
	 * @throws RemoteException
	 * @throws AMSException
	 * 
	 */
	protected File getTempFile(InvoiceFileUpload invoiceFile)
			throws RemoteException, AmsException {
		LOG.debug("Getting temporary file..");
		File file = null;
		String fileName = invoiceFile.getAttribute("invoice_file_upload_oid");
		String folder = TradePortalConstants.getPropertyValue(
				"AgentConfiguration", "uploadFolder", "c:/temp/");
		String fname = folder + fileName + InvoiceFileDetails.FILE_EXTENSION;

		file = new File(fname);

		LOG.debug("Getting temporary file. Filename: "
				+ file.getAbsolutePath());

		return file;
	}

	/**
	 * This method validates the uploaded file and writes to temporary folder
	 * 
	 * @param item
	 * @param file
	 * @param invoicefile
	 * @param resourceManager
	 * @param userSession
	 * @param mediatorServices
	 * @return
	 * @throws Exception
	 */
	public boolean writetoDisk(String invDefOid, FileItem item, File file,
			InvoiceFileUpload invoiceFileUpload,
			ResourceManager resourceManager, SessionWebBean userSession,
			MediatorServices mediatorServices,BeanManager beanMgr) throws Exception {

		boolean success = true;

		BufferedWriter fs = null;
		BufferedReader reader = null;
		int lineIndex = 0;
		try {


			InputStream is = item.getInputStream();
			PushbackInputStream pis = new PushbackInputStream(is, BOM_SIZE);

			InputStreamReader isr = new InputStreamReader(pis, getEncoding(pis));
			reader = new BufferedReader(isr);

			fs = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file), AmsConstants.UTF8_ENCODING));

			String sLine = "";

			String serverLocation = JPylonProperties.getInstance().getString(
					"serverLocation");
			ClientServerDataBridge csdb = resourceManager.getCSDB();
			//InvoiceDefinition invoiceDefinition = (InvoiceDefinition) EJBObjectFactory
				//	.createClientEJB(serverLocation, "InvoiceDefinition",
					//		Long.parseLong(invDefOid), csdb);
			
			InvoiceDefinitionWebBean	invoiceDefinition =beanMgr.createBean(InvoiceDefinitionWebBean.class, "InvoiceDefinition",csdb);
			invoiceDefinition.getById(invDefOid);
			invoiceFileUpload.setAttribute("invoice_file_definition_name",
					invoiceDefinition.getAttributeWithoutHtmlChars("name"));
			
			String delimiterChar = invoiceDefinition.getAttributeWithoutHtmlChars("delimiter_char");
			LOG.debug("Processing lines..");
			String[] record = null;
			sLine = reader.readLine();
			HashMap map = new HashMap();
			InvoiceFileDetails.getInvoiceDefFieldCount(invoiceDefinition, mediatorServices, map);
           // Nar IR-NNUM022937252 03/24/2012 Begin - if included column header is checked in invoice definition,
		   //then ignore first line in invoice file while processing.
			String includeColumHeader = invoiceDefinition.getAttributeWithoutHtmlChars("include_column_headers");
			if(TradePortalConstants.INDICATOR_YES.equals(includeColumHeader)){
				sLine = reader.readLine();
				//if header is included and sLine is blank, it indicates that 
				//the input file only has a header or next line after header is empty
				//so return false for validation - no need to validate empty files
				if(StringFunction.isBlank(sLine)){
					success = false;
				}
			}
           // Nar IR-NNUM022937252 03/24/2012 End

			long invCount = 0;
			int lineItemCountPerInvoice = 0; // Nar IR MMUM052582934
			String invIdVar = "";

			while ((reader != null) && (sLine != null)) {
				HashMap details = new HashMap();

				if (InstrumentServices.isBlankOrWhiteSpace(sLine)) {
					sLine = reader.readLine();
					continue;
				} else {
					lineIndex++;
					record = InvoiceFileDetails.parseInvoiceLineItem(delimiterChar,
							sLine, 100);

					if (record == null) {
						mediatorServices
								.getErrorManager()
								.issueError(
										TradePortalConstants.ERR_CAT_1,
										TradePortalConstants.FILE_FORMAT_NOT_VALID_DELIMITED);
						return false;
					}
					boolean isValidFieldCount =InvoiceFileDetails.validateINVLineItem(record,mediatorServices,map);
					if (!isValidFieldCount) {
						return false;
					}
					//get the invoice id
					String invId = InvoiceFileDetails.getUniqueID(
							record, mediatorServices, invoiceDefinition,lineIndex,"inv_summary_order","invoice_id");

					if(StringFunction.isNotBlank(invId)){
                       if( !invId.equals(invIdVar)){
						     invCount++;
							 invIdVar=invId;
							 lineItemCountPerInvoice = 1; //Nar IR MMUM052582934 if new invoice, then reset invoice line item
					    }
                        // Nar IR MMUM052582934 Begin
					    // if invoice id is same as previous, then increment only invoice line item
					   else{
						     lineItemCountPerInvoice++;
					   }
                       // Nar IR MMUM052582934 End
					}
                     if (invCount > MAX_FILE_INVOICES){
						mediatorServices
						.getErrorManager()
						.issueError(
								TradePortalConstants.ERR_CAT_1,
								TradePortalConstants.INVOCIE_FILE_UPLOAD_EXCEEDED);
						return false;
					}
					// Nar IR MMUM052582934 Beign
					if (lineItemCountPerInvoice > MAX_FILE_INV_LINE_ITEMS){
						mediatorServices.getErrorManager().issueError(
								TradePortalConstants.ERR_CAT_1,TradePortalConstants.INV_FILE_LINE_ITEMS_EXCEEDED,String.valueOf(MAX_FILE_INV_LINE_ITEMS));
						return false;
					}
                    // Nar IR MMUM052582934 End
					fs.write(sLine);
					fs.newLine();
					sLine = reader.readLine();
				}

			}

		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} finally {
			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					// ignore
				}

			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
		return success;
	}

	protected static InvoicesSummaryData createInvoiceSummaryData()
			throws AmsException, RemoteException {
		LOG.debug("Creating InvoicesSummaryData..");
		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");
		InvoicesSummaryData file = (InvoicesSummaryData) EJBObjectFactory
				.createClientEJB(serverLocation, "InvoicesSummaryData");
		file.newObject();
		return file;
	}

	protected static InvoiceLineItemDetail createInvoiceLineItemDetail()
			throws AmsException, RemoteException {
		LOG.debug("Creating InvoicesSummaryData..");
		String serverLocation = JPylonProperties.getInstance().getString(
				"serverLocation");
		InvoiceLineItemDetail file = (InvoiceLineItemDetail) EJBObjectFactory
				.createClientEJB(serverLocation, "InvoiceLineItemDetail");
		file.newObject();
		return file;
	}





	protected static String getMaxFileSizeInMB() {
		float f = (float) (MAX_FILE_SIZE / (1024.0 * 1024.0));
		BigDecimal dec = new BigDecimal(f);
		dec = dec.setScale(1, BigDecimal.ROUND_DOWN);
		return dec.toString();

	}


	/**
	 * Performs cleanup
	 *
	 * @param file
	 *
	 * @return
	 */
	protected void cleanup(File file) {
		LOG.debug("Inside cleanup..");
		// delete file
		if (file != null && !file.delete()) {

			LOG.info("Error: Could not delete file: "
					+ file.getAbsoluteFile());
		}

	}
}
