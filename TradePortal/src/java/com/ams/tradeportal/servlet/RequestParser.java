package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

import javax.servlet.http.*;
import javax.servlet.ServletInputStream;
heebfvghuerburebvurebuvbhyrvu1234567890987654321	23456789098765432
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.util.StringFunction;

public class RequestParser
{
private static final Logger LOG = LoggerFactory.getLogger(RequestParser.class);
    Hashtable   parsedParams  = new Hashtable();
    String      separator     = "";
    int         contentLength = 0;

    /**
    * Default constructor for RequestParser
    *
    *     Copyright  � 2001                         
    *     American Management Systems, Incorporated 
    *     All rights reserved 
    *

    */
    public RequestParser()
    {

    }

    /**
    * This method parses the names and values of the page parameters, and any file content and 
    * file names as well, from an HttpServletRequest that has an enctype of "multipart/form-data".  
    * A hashtable of parameters is returned.  The keys that are in the hashtable for every request 
    * are:
    * <br>fileName - the name of the file, without path information.
    * <br>contentType - the content type of the file.
    * <br>fileContent - the contents of the uploaded file, as a ByteArrayOutputString
    * <p>
    * The page parameters are stored as keys with the corresponding value stored as a String object.
    *

    * @param      javax.servlet.http.HttpServletRequest request - the request to parse the file and parameters from
    * @exception  java.io.IOException
    * @return     java.util.Hashtable - hashtable containing all form data, including file contents
    */
    public Hashtable parseRequest(HttpServletRequest request) throws IOException
    {
        ServletInputStream   in         = null;
        Enumeration          enumer       = null;
        String               paramValue = null;
        String               paramName  = null;
        String               current    = null;
        String               line       = null;

        // First, retrieve the input stream and content length
        in            = request.getInputStream();
        contentLength = request.getContentLength();

        // Read the separator - it's the first line in the request.
        separator = readLineToString(in).trim();

        // Loop through all the lines in the file, processing each line according to it's 
        // content. Processing depends on whether the line contains page parameters and 
        // values, or file content.
        line = readLineToString(in);        
        while(line != null)
        {
            // If the line contains the filename, it will contain a "filename" string.
            if ((line.indexOf("form-data")>0) && (line.indexOf("filename") > 0))
            {
                LOG.debug("RequestParser.parseRequest: process file name");

                // Parse out the file name and content type
                parseFileData(line);

                // Read the next line and add the file content to the list of parameters.
                parseContentType(readLineToString(in));

                // Read the next line - will be blank
                line = readLineToString(in);

                // Read all of the file content, and add it to the parameters
                parseFileContent(in);
            }
            // Else if the line is a normal parameter, put it and its value in the hashtable
            else if ((line.indexOf("form-data") > 0) && (line.indexOf("name") > 0))
            {
               LOG.debug("RequestParser.parseRequest: process parameter");

               paramName = parseParameterName(line);

               // Read the next line - it will be blank
               line = readLineToString(in);

               // Read the next line - this will be the value of the parameter
               paramValue = readLineToString(in);

               // Add the parameter to the parameters hashtable
               parsedParams.put(paramName.trim(), paramValue.trim());
            }

           // All other lines- blank lines, and separator lines are skipped.
           // Read the next line
           LOG.debug("RequestParser.parseRequest: reading next line *****");

           line = readLineToString(in);
        }
        
        LOG.debug("RequestParser.parseRequest: done with request");

        enumer = parsedParams.keys();

        LOG.debug("RequestParser - parsed parameters:");

        while (enumer.hasMoreElements())
        {
            current = (String) enumer.nextElement();

            if (current.equalsIgnoreCase("filecontent"))
            {
                LOG.debug("param: {} : {} " , current, ((ByteArrayOutputStream) parsedParams.get(current)));
            }
            else
            {
                LOG.debug("param: {}   value: {}"  ,current, (String) parsedParams.get(current));
            }
        }

        return parsedParams;
    }

    /**
    * This method parses out the file name from the content String and adds it to the 
    * Hashtable of request parameters. The path is also removed from the fileName since 
    * the path refers to the directory on the end user's machine where the file was 
    * uploaded from, and not the location on the server where the file will be saved.
    *

    * @param   java.lang.String rawString - the string to parse
    * @return  void
    **/
    private void parseFileData(String rawString)
    {
        String   fileNameIndicator = null;
        String   fileName          = null;
        int      lastPathIndicator = 0;
        int      fileNamePosition  = 0;
        int      endQuote          = 0;

        // Parse out the file name from the request by looking for the "filename=" string. The 
        // name of the file will appear as follows: filename="myFile.txt". 
        fileNameIndicator = "filename=\"";
        fileNamePosition  = rawString.indexOf(fileNameIndicator);
        endQuote          = rawString.indexOf("\"", fileNamePosition + fileNameIndicator.length());
        fileName          = rawString.substring(fileNamePosition + fileNameIndicator.length(), endQuote);

        // Add the file path to the parameters in case we need it later
        parsedParams.put("filePath", fileName.trim().replace(' ', '_'));

        // Remove the path by only taking the string after the last file separator.
        lastPathIndicator = fileName.lastIndexOf("\\");
        if (lastPathIndicator <= 0)
        {
            lastPathIndicator = fileName.lastIndexOf("/");
        }

        fileName = fileName.substring(lastPathIndicator + 1, fileName.length());

        // Add the filename to the parameters
        LOG.debug("RequestParser.parseFileData: file name: {}" , fileName);
        
        parsedParams.put("fileName", fileName.trim().replace(' ', '_'));
    }

    /**
    * This method parses the name of a parameter from the line from the HttpRequest. 
    *

    * @param   java.lang.String nameString - the string to parse the parameter name from
    * @return  java.lang.String - the parameter name
    **/
    private String parseParameterName(String nameString)
    {
        String   parameterNameIndicator = null;
        String   paramName              = null;
        int      indexQuote             = 0;
        int      nameStart              = 0;

        // In the request, parameters are in the format name="parameter Name" parameter Value;
        parameterNameIndicator = "name=\"";

        // Retrieve the position of the end of the "name=\"" string in the paramString.
        nameStart = nameString.indexOf(parameterNameIndicator) + parameterNameIndicator.length();

        // Retrieve the name of the parameter by reading from the end of the parameterNamePosition
        // to the second quote.
        indexQuote = nameString.indexOf("\"", nameStart);
        paramName  = nameString.substring(nameStart, indexQuote);

        LOG.debug("RequestParser.parseParameterName: param: {}" , paramName);

        return paramName;
    }

    /**
    * This method parses out the content type from the rawString parameter,
    * and adds the content type to the parameter hashtable.
    *

    * @param   String rawString - the string to parse
    * @return  void
    **/
    private void parseContentType(String rawString)
    {
        String   contentIndex = null;
        String   contentType  = null;
        int      typePosition = 0;

        // Parse out the content type from the string
        contentIndex = "Content-Type:";
        typePosition = rawString.indexOf(contentIndex) + contentIndex.length() + 1;
        contentType  = rawString.substring(typePosition);

        // Add the contentType to the list of parameters
        LOG.debug("RequestParser.parseContentType: content Type: {}" , contentType);

        parsedParams.put("contentType", contentType.trim());
    }

    /**
    * This method parses the file content from a servlet request. This method
    * loops through the request, readin every line from the request until the
    * separator is reached. The file content is stored in a ByteArrayOutputStream, 
    * and added to the parameters.
    *

    * @param      javax.servlet.ServletInputStream in - the servlet input stream
    * @exception  java.io.IOException
    * @return     void
    */
    private void parseFileContent(ServletInputStream in) throws IOException
    {
        ByteArrayOutputStream   bos  = new ByteArrayOutputStream();
        byte[]                  line = null;

        line = readLine(in);
        
        // NSX CR-486 05/05/2001 --  Begin
        String encoding=getEncoding(line);
        int	lineStart =getBOMLength(encoding); // skip BOM characters
        	
        // NSX CR-486 05/05/2001 --  End
        
        while (line != null && !isSeparator(line))
        {
            LOG.trace("RequestParser.parseFileContent: content line: {}" , line);

            bos.write(line, lineStart, line.length-lineStart);
            lineStart=0; // reset to beginning of line
            line = readLine(in);
        }

    	parsedParams.put("fileEncoding", encoding);        // NSX CR-486 05/05/2010 - End
        parsedParams.put("fileContent", bos);
    }
    
    // NSX CR-486 05/05/2001 --  Begin
    
    /**
     * This method returns Byte Order Marker size for given encoding
     *
     * @param      String - encoding
     * @return     int
     */ 
    private int getBOMLength(String encoding) {
    	int _BOM_size = 0;
    	
    	if (StringFunction.isBlank(encoding)) return _BOM_size;

    	if (encoding.startsWith("UTF-16")) {
    		_BOM_size = 2;         
    	}
    	else if ("UTF-8".equals(encoding)) {
    		_BOM_size = 3;   
    	}

    	return _BOM_size;
    }
           
    /**
     * This method returns the encoding type by checking the Byte Order Marker
     *
     * @param      byte[] - byte array
     * @return     String
     */ 
    private String getEncoding(byte[] line)  {
    	String encoding = "";

    	if (line!=null) {
    		if (line.length >=2 && (line[0] == (byte)0xFF) && (line[1] == (byte)0xFE )) {
    			encoding = "UTF-16LE";         
    		}
    		else if (line.length >=2 && (line[0] == (byte)0xFE) && (line[1] == (byte)0xFF )) {
    			encoding = "UTF-16BE";         
    		}
    		else if (line.length >=3 && (line[0] == (byte)0xEF) && (line[1] == (byte)0xBB ) && (line[2] == (byte)0xBF )) {
    			encoding = "UTF-8";         
    		}
    	}
    	return encoding;
    }
    
    
    /**
     * This method returns the content of file as String using the file encoding.
     *
     * @return     String
     */ 
    public String getFileContentAsString() throws UnsupportedEncodingException {
    	String  content = null;
    	String  fileEncoding = (String)parsedParams.get("fileEncoding");
        
        if (StringFunction.isBlank(fileEncoding)) {
        	content = ((ByteArrayOutputStream)parsedParams.get("fileContent")).toString();
        }
        else {
        	content = ((ByteArrayOutputStream)parsedParams.get("fileContent")).toString(fileEncoding);
        	// seems like last encoded character is for '\n'. It is causing problem because, the file content is broken by
        	// line ("\r\n"), remove it for now
        	// Todo - there should be better way to do this.
        	content = content.substring(0, content.length()-1);   
        }
        return 	content;
    }
 // NSX CR-486 05/05/2001 --  End
    
    /**
    * This method reads a line from the input stream, and returns it as a byte array.
    * Null is returned if a line can not be read from the input stream.
    *

    * @param   javax.servlet.ServletInputStream in - the InputStream to parse
    * @return  byte[] - a line of bytes from the input stream (or null if the InputStream can't be read)
    */
    public byte[] readLine(ServletInputStream in)
    {
        ByteArrayOutputStream   bos         = new ByteArrayOutputStream();
        byte[]                  buffer      = new byte[contentLength];
        int                     contentRead = 0;

        try
        {  
            // Read a line, until the EOL or maxLength is reached.
            contentRead = in.readLine(buffer, 0, contentLength);

            if(contentRead != -1)
            {
                // Write to the bos, removing the end of line characters
                bos.write(buffer, 0, contentRead);

                buffer = bos.toByteArray();

                LOG.debug("RequestParser.readLine(): read line: {}" , bos);

                return buffer;
            }
            else
            {
                LOG.debug("RequestParser.readLine: reached end of request, return null");

                return null;
            }
        } 
        catch(Exception e)
        {
            return null;
        }          
    }

    /**
    * This method reads a line from the input stream, and returns it as a String object.
    * Null is returned if a line can not be read from the input stream.
    *

    * @param   javax.servlet.ServletInputStream in - the InputStream to parse
    * @return  java.lang.String - a line from the input stream in String format 
    *                           (or null if the InputStream can't be read)
    */
    public String readLineToString(ServletInputStream in)
    {
       byte[]   b = null;

       b = readLine(in);
       if (b != null)
       {
          return new String(b);
       }
       else
       {
          return null;
       }
    }

    /**
    * This method returns true if the file separator is contained in the byte array parameter. False
    * is returned otherwise. 
    *

    * @param   byte[] - the byte[] to evaluate. 
    * @return  boolean - indicates whether or not the file separator is contained in the byte array parameter
    *                    (true  - the file sepaator is contained in the byte array
    *                     false - the file sepaator is not contained in the byte array)
    */
    public boolean isSeparator(byte[] b)
    {
        String   temp = null;

        temp = new String(b);
        return temp.indexOf(separator) >= 0;
    }
}
