package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**

 * @date 03/12/2014
 * Release 9.0 - T36000026074
 *This Filter will be used for applying application wide modifications
 *to the HTTP Response/Requests
 *The modifications here are specific to meeting some security requirements
 *such as disabling caching of dynamic content by the browsers
 */
public class SecurityFilter implements Filter {
private static final Logger LOG = LoggerFactory.getLogger(SecurityFilter.class);

	public SecurityFilter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        //AiA - Modify the response header by adding the "no-cache" directive
    	//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    	//MEer Rel 9.1 IR-T36000034131 Modify Cache Control response header with no-store, max-age and must-revalidate directives 
    	// to prevent browser caching in all Portal supported browsers.
    	response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); //prevents caching at the proxy 
        chain.doFilter(req, res);		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
