package com.ams.tradeportal.servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.html.*;
import com.ams.tradeportal.busobj.PmtTermsTenorDtl;
import com.ams.tradeportal.busobj.webbean.*;
import com.ams.tradeportal.common.*;
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.rmi.RemoteException;
import java.text.*;

import com.bo.wibean.*;
import com.ams.tradeportal.busobj.util.*;

/**
 * The TradePortalDownloadData class is used when a user wishes to download
 * Outgoing Standby Guarantee data.  This class is mainly meant to break up the
 * code from the TradePortalDownloadServlet class.  This is meant to help isolate
 * the code specific to the report being downloaded.
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 */

public class TradePortalDownloadData
{
private static final Logger LOG = LoggerFactory.getLogger(TradePortalDownloadData.class);

   public static void downloadOutgoingStandbyGuaranteeData( HttpServletRequest request, HttpServletResponse response )
							throws ServletException, IOException, AmsException
   {
      ServletOutputStream   out               = null;
      InstrumentWebBean     instrument        = null;
      TermsPartyWebBean     termsParty        = null;
      ResourceManager       resourceManager   = null;
      TermsWebBean          terms             = null;
      StringBuffer          defaultFileName   = null;
      StringBuffer          downloadData      = null;
      BeanManager           beanManager       = null;
      HttpSession           session           = null;
      boolean               isValidTermsParty = true;
      String                paymentTermsType  = null;
      String                confirmationType  = null;
      String                bankChargesType   = null;
      String                consignmentType   = null;
      String                resourceLocale    = null;
      String                markedFreight     = null;
      String                termsPartyOid     = null;
      String                currencyCode      = null;

      // First we need to retrieve the resource manager from the session so that we can retrieve
      // internationalized field name strings
      session         = request.getSession(true);
      resourceManager = (ResourceManager) session.getAttribute("resMgr");
      beanManager     = (BeanManager)     session.getAttribute("beanMgr");

      // Retrieve the http servlet output stream
      out = response.getOutputStream();

      // Retrieve the locale from resource manager; this should be currently set to the user's locale
      resourceLocale = resourceManager.getResourceLocale();

      // Next, retrieve all web beans in the session pertaining to the Import LC instrument currently being viewed
      instrument   = (InstrumentWebBean)  beanManager.getBean("Instrument");
      terms        = (TermsWebBean)       beanManager.getBean("Terms");

      // Set the default file name to use for the Outgoing Standby data file to be downloaded
      defaultFileName = new StringBuffer();
      defaultFileName.append(resourceManager.getText("SLCIssue.DownloadFileNamePrefix",
                                                     TradePortalConstants.TEXT_BUNDLE));
      defaultFileName.append(instrument.getAttribute("complete_instrument_id"));
      defaultFileName.append(resourceManager.getText("SLCIssue.DownloadFileNameSuffix",
                                                     TradePortalConstants.TEXT_BUNDLE));

      // Set the http servlet response content type and content disposition  (i.e., default filename)
      setResponseContent(response, TradePortalConstants.DOWNLOAD_TXT_CONTENT_TYPE, defaultFileName.toString().replace('/', '_'));

      // Retrieve the instrument number of the Import LC instrument currently being viewed
      appendData( downloadData,  resourceManager.getText("SLCIssue.StandbyLCNumber", TradePortalConstants.TEXT_BUNDLE),
                  instrument.getAttribute("complete_instrument_id"), true, out);

  // Retrieve the Beneficiary terms party from the database
      termsPartyOid = terms.getAttribute("c_FirstTermsParty");
      termsParty = beanManager.createBean(TermsPartyWebBean.class, "TermsParty");

      if (!InstrumentServices.isBlank(termsPartyOid))
      {
         termsParty.setAttribute("terms_party_oid", termsPartyOid);
         termsParty.getDataFromAppServer();

         isValidTermsParty = true;
      }

      // Start retrieving all saved Beneficiary data of the Import LC being viewed
      appendSectionHeader( downloadData, resourceManager.getText("SLCIssue.BeneficiaryPlain",
                           TradePortalConstants.TEXT_BUNDLE), true, out);
      appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);

  // Retrieve the Corresponding Bank terms party from the database
      termsPartyOid = terms.getAttribute("c_ThirdTermsParty");

      if (!InstrumentServices.isBlank(termsPartyOid))
      {
         termsParty.setAttribute("terms_party_oid", termsPartyOid);
         termsParty.getDataFromAppServer();

         isValidTermsParty = true;
      }
      else
      {
         isValidTermsParty = false;
      }

      // Start retrieving all saved Corresponding Bank data of the Standby LC being viewed
      appendSectionHeader( downloadData, resourceManager.getText("SLCIssue.CorrespondentBankPlain", TradePortalConstants.TEXT_BUNDLE),
                           true, out);
      appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);


 // Retrieve the Applicant terms party from the database
      termsPartyOid = terms.getAttribute("c_SecondTermsParty");

      if (!InstrumentServices.isBlank(termsPartyOid))
      {
         termsParty.setAttribute("terms_party_oid", termsPartyOid);
         termsParty.getDataFromAppServer();

         isValidTermsParty = true;
      }
      else
      {
         isValidTermsParty = false;
      }

      // Start retrieving all saved Applicant data of the Standby LC being viewed
      appendSectionHeader( downloadData, resourceManager.getText("SLCIssue.ApplicantPlain", TradePortalConstants.TEXT_BUNDLE),
                           true, out);
      appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);


      // Retrieve the Applicant's characteristic type, reference number, the Import LC transaction's expiry date,
      // currency, amount, amount tolerance, payment terms, and bank charges
      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.PurposeType", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");
      if( InstrumentServices.isNotBlank(terms.getAttribute("characteristic_type")) )
      {
         downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARACTERISTIC_TYPE,
                                                                            terms.getAttribute("characteristic_type"),
                                                                            resourceLocale));
      }
      flushData( out, downloadData, true);

      appendData( downloadData,  resourceManager.getText("SLCIssue.YourRefNumber", TradePortalConstants.TEXT_BUNDLE),
                  terms.getAttribute("reference_number"), true, out);

      appendData( downloadData,  resourceManager.getText("ImportDLCIssue.ExpiryDate", TradePortalConstants.TEXT_BUNDLE),
                  TPDateTimeUtility.formatDate(terms.getAttribute("expiry_date"), TPDateTimeUtility.LONG,
                                                       resourceLocale), true, out);

      // Retrieve the currency code of the transaction
      currencyCode = terms.getAttribute("amount_currency_code");

      appendData( downloadData,  resourceManager.getText("transaction.Currency", TradePortalConstants.TEXT_BUNDLE),
                  currencyCode, true, out);

      appendData( downloadData,  resourceManager.getText("transaction.Amount", TradePortalConstants.TEXT_BUNDLE),
                  TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), currencyCode,
                                                             resourceLocale), true, out);

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.AmountTolerance", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");
      downloadData.append(resourceManager.getText("transaction.Plus", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(terms.getAttribute("amt_tolerance_pos"));
      downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" ");
      downloadData.append(resourceManager.getText("transaction.Minus", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(terms.getAttribute("amt_tolerance_neg"));
      downloadData.append(resourceManager.getText("transaction.Percent", TradePortalConstants.TEXT_BUNDLE));
      flushData( out, downloadData, true);

      // Retrieve the transaction's payment terms type
      paymentTermsType = terms.getAttribute("pmt_terms_type");

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.PaymentTerms", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");
      flushData( out, downloadData, false);
//      if (paymentTermsType.equals(TradePortalConstants.PMT_SIGHT))
//      {
//         downloadData.append(resourceManager.getText("SLCIssue.Sight", TradePortalConstants.TEXT_BUNDLE));
//      }
//      else if (paymentTermsType.equals(TradePortalConstants.PMT_OTHER))
//      {
//         downloadData.append(resourceManager.getText("SLCIssue.OtherCondLink", TradePortalConstants.TEXT_BUNDLE));
//      }
      
      downloadData = new StringBuffer();
      setPaymentTenorTerms(downloadData, terms, resourceLocale, out);
      downloadData = new StringBuffer("");
      flushData( out, downloadData, false);

      // Retrieve the transaction's bank charges type
      bankChargesType = terms.getAttribute("bank_charges_type");

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("ImportDLCIssue.BankCharges", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");

      if (bankChargesType.equals(TradePortalConstants.CHARGE_OUR_ACCT))
      {
         downloadData.append(resourceManager.getText("SLCIssue.AllOurAccount", TradePortalConstants.TEXT_BUNDLE));
      }
      else if (bankChargesType.equals(TradePortalConstants.CHARGE_BEN_FOR_OUTSIDE))
      {
         downloadData.append(resourceManager.getText("SLCIssue.AllOutside", TradePortalConstants.TEXT_BUNDLE));
      }
      else if (bankChargesType.equals(TradePortalConstants.CHARGE_OTHER))
      {
         downloadData.append(resourceManager.getText("SLCIssue.OtherCondLink", TradePortalConstants.TEXT_BUNDLE));
      }

      flushData( out, downloadData, true);
      out.println("");

 // Documents Required section

      appendSectionHeader( downloadData, resourceManager.getText("SLCIssue.DocumentsReqShort", TradePortalConstants.TEXT_BUNDLE),
                           true, out);

      appendData( downloadData,  resourceManager.getText("SLCIssue.DocumentList", TradePortalConstants.TEXT_BUNDLE),
                  terms.getAttribute("addl_doc_text"), true, out);

      out.println("");

 // Other Conditions section

      appendSectionHeader( downloadData, resourceManager.getText("SLCIssue.OtherConditionsShort", TradePortalConstants.TEXT_BUNDLE),
                           true, out);

      // Retrieve the Confirmation Type
      confirmationType = terms.getAttribute("confirmation_type");

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.Confirmations", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");

   //SURESHL T36000034384 REL 9.3 31/3/2015  <START>     

      if (confirmationType.equals(TradePortalConstants.CONFIRM_NOT_REQD))
      {
         downloadData.append(resourceManager.getText("SLCIssue.CorrBankConfirmationNotRequired", TradePortalConstants.TEXT_BUNDLE));
         downloadData.append(" ");
      }
      else if (confirmationType.equals(TradePortalConstants.BANK_TO_ADD))
      {
         downloadData.append(resourceManager.getText("SLCIssue.CorrBankAddConfirmation", TradePortalConstants.TEXT_BUNDLE));
         downloadData.append(" ");
       
      }
      else if (confirmationType.equals(TradePortalConstants.BANK_MAY_ADD))
      {
         downloadData.append(resourceManager.getText("SLCIssue.CorrBankMayAddConfirmation", TradePortalConstants.TEXT_BUNDLE));
         downloadData.append(" ");
         
      }
  //SURESHL T36000034384 REL 9.3 31/3/2015  <END>     

      flushData( out, downloadData, false);

      downloadData = new StringBuffer();
//      downloadData.append(resourceManager.getText("SLCIssue.AutoExtendShort", TradePortalConstants.TEXT_BUNDLE));
//      downloadData.append(" : ");
//
//      if (terms.getAttribute("auto_extend").equals(TradePortalConstants.INDICATOR_YES))
//      {
//         downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE));
//      }else
//      {
//            downloadData.append(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE));
//      }
//      flushData( out, downloadData, false);

  	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("ucp_version_details_ind"))){
		  downloadData = new StringBuffer();
		  downloadData.append(resourceManager.getText("common.ICCPublications", TradePortalConstants.TEXT_BUNDLE));
		  downloadData.append(" - ");
		  flushData( out, downloadData, true);
		  
		  downloadData = new StringBuffer();
		  downloadData.append(resourceManager.getText("common.ICCApplicableRules", TradePortalConstants.TEXT_BUNDLE));
		  downloadData.append(" : ");
		  downloadData.append(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE));
		  flushData( out, downloadData, true);
		  
		  downloadData = new StringBuffer();
		  downloadData.append(resourceManager.getText("common.ICCVersion", TradePortalConstants.TEXT_BUNDLE));
		  downloadData.append(" : ");
		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));
		  flushData( out, downloadData, true);
		  
		  if (StringFunction.isNotBlank(terms.getAttribute("ucp_details"))){
			  downloadData = new StringBuffer();
			  downloadData.append(resourceManager.getText("common.ICCDetails", TradePortalConstants.TEXT_BUNDLE));
			  downloadData.append(" : ");
			  downloadData.append(terms.getAttribute("ucp_details"));
			  flushData( out, downloadData, true);
		  }
	  }
	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End 
	  
	  appendData( downloadData,  resourceManager.getText("SLCIssue.AdditionalConditions", TradePortalConstants.TEXT_BUNDLE),
              terms.getAttribute("additional_conditions"), true, out);
  
	  downloadData = new StringBuffer();
	  setAutoExtTerms(downloadData, terms, resourceManager, out);
   }




   public static void downloadGuaranteeData( HttpServletRequest request, HttpServletResponse response )
							throws ServletException, IOException, AmsException
   {

        ServletOutputStream   out               = null;
        InstrumentWebBean     instrument        = null;
        TermsPartyWebBean     termsParty        = null;
        ResourceManager       resourceManager   = null;
        TermsWebBean          terms             = null;
        BeanManager           beanManager       = null;
        HttpSession           session           = null;
        StringBuffer          defaultFileName   = null;
        StringBuffer          downloadData      = null;
        boolean               isValidTermsParty = true;
        String                resourceLocale    = null;
        String                termsPartyOid     = null;
        String                currencyCode      = null;

        // First we need to retrieve the resource manager from the session so that we can retrieve
        // internationalized field name strings
        session         = request.getSession(true);
        resourceManager = (ResourceManager) session.getAttribute("resMgr");
        beanManager     = (BeanManager)     session.getAttribute("beanMgr");

        // Retrieve the http servlet output stream
        out = response.getOutputStream();

        // Retrieve the locale from resource manager; this should be currently set to the user's locale
        resourceLocale = resourceManager.getResourceLocale();

        // Next, retrieve all web beans in the session pertaining to the Guarantee instrument currently being viewed
        instrument   = (InstrumentWebBean)  beanManager.getBean("Instrument");
        terms        = (TermsWebBean)       beanManager.getBean("Terms");

        // Set the default file name to use for the Outgoing Standby data file to be downloaded
        defaultFileName = new StringBuffer();
        defaultFileName.append(resourceManager.getText("GuaranteeIssue.DownloadFileNamePrefix",
                                                       TradePortalConstants.TEXT_BUNDLE));
        defaultFileName.append(instrument.getAttribute("complete_instrument_id"));
        defaultFileName.append(resourceManager.getText("GuaranteeIssue.DownloadFileNameSuffix",
                                                       TradePortalConstants.TEXT_BUNDLE));

        // Set the http servlet response content type and content disposition  (i.e., default filename)
        setResponseContent(response, TradePortalConstants.DOWNLOAD_TXT_CONTENT_TYPE, defaultFileName.toString().replace('/', '_'));

        // Retrieve the instrument number of the Guarantee instrument currently being viewed
        appendData( downloadData,  resourceManager.getText("GuaranteeIssue.GuaranteeNumber", TradePortalConstants.TEXT_BUNDLE),
                    instrument.getAttribute("complete_instrument_id"), true, out);

        // Beneficiary
        termsPartyOid = terms.getAttribute("c_FirstTermsParty");
        termsParty = beanManager.createBean(TermsPartyWebBean.class,  "TermsParty");

        if (!InstrumentServices.isBlank(termsPartyOid))
        {
            termsParty.setAttribute("terms_party_oid", termsPartyOid);
            termsParty.getDataFromAppServer();

            isValidTermsParty = true;
        }

        appendSectionHeader( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.BeneName",
                TradePortalConstants.TEXT_BUNDLE)), true, out);
        appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);

        // Applicant
        termsPartyOid = terms.getAttribute("c_SecondTermsParty");

        if (!InstrumentServices.isBlank(termsPartyOid))
        {
            termsParty.setAttribute("terms_party_oid", termsPartyOid);
            termsParty.getDataFromAppServer();

            isValidTermsParty = true;
        }
        else
        {
            isValidTermsParty = false;
        }

        appendSectionHeader( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ApplicantName",
                           TradePortalConstants.TEXT_BUNDLE)), true, out);
        appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);


        downloadData = new StringBuffer();

        // Reference Number
        appendData( downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("SLCIssue.YourRefNumber", TradePortalConstants.TEXT_BUNDLE)),
                    terms.getAttribute("reference_number"), true, out);

        // Currency
        currencyCode = terms.getAttribute("amount_currency_code");

        appendData( downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("transaction.Currency", TradePortalConstants.TEXT_BUNDLE)),
                    currencyCode, true, out);

        // Amount
        appendData( downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("transaction.Amount", TradePortalConstants.TEXT_BUNDLE)),
                    TPCurrencyUtility.getDisplayAmount(terms.getAttribute("amount"), currencyCode,
                                                             resourceLocale), true, out);

        // Type
        downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.PurposeType", TradePortalConstants.TEXT_BUNDLE)));
        downloadData.append(" : ");
        if( InstrumentServices.isNotBlank(terms.getAttribute("characteristic_type")) )
        {
            downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARACTERISTIC_TYPE,
                                                                              terms.getAttribute("characteristic_type"),
                                                                              resourceLocale));
        }
        flushData( out, downloadData, true);

        // Issuing Bank
        String issuingBank = terms.getAttribute("guarantee_issue_type");
        downloadData = new StringBuffer();
        if (issuingBank.equals(TradePortalConstants.GUAR_ISSUE_APPLICANTS_BANK))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ApplicantsBank", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.GteeIssue", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (issuingBank.equals(TradePortalConstants.GUAR_ISSUE_OVERSEAS_BANK))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ApplicantsBank", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.OverseasBank1", TradePortalConstants.TEXT_BUNDLE)), true, out);

            // Overseas Validity Date
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ValidityDateOverseasBank", TradePortalConstants.TEXT_BUNDLE)),
                       TPDateTimeUtility.formatDate(terms.getAttribute("overseas_validity_date"), TPDateTimeUtility.LONG, resourceLocale),
                       true, out);
        }

        // Oversees Bank
        termsPartyOid = terms.getAttribute("c_ThirdTermsParty");

        if (!InstrumentServices.isBlank(termsPartyOid))
        {
            termsParty.setAttribute("terms_party_oid", termsPartyOid);
            termsParty.getDataFromAppServer();

            isValidTermsParty = true;
        }
        else
        {
            isValidTermsParty = false;
        }

        appendSectionHeader( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.OseasBankName",
                           TradePortalConstants.TEXT_BUNDLE)), true, out);
        appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);

        // Validity
        appendSectionHeader( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Validity", TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Valid From
        String validFrom = terms.getAttribute("guar_valid_from_date_type");
        downloadData = new StringBuffer();
        if (validFrom.equals(TradePortalConstants.DATE_OF_ISSUE))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ValidFrom", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DateofIssue", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (validFrom.equals(TradePortalConstants.OTHER_VALID_FROM_DATE))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ValidFrom", TradePortalConstants.TEXT_BUNDLE)),
                       TPDateTimeUtility.formatDate(terms.getAttribute("guar_valid_from_date"), TPDateTimeUtility.LONG, resourceLocale),
                       true, out);
        }

        // Valid To
        String validTo = terms.getAttribute("guar_expiry_date_type");
        downloadData = new StringBuffer();
        if (validTo.equals(TradePortalConstants.VALIDITY_DATE))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ValidTo", TradePortalConstants.TEXT_BUNDLE)),
                       TPDateTimeUtility.formatDate(terms.getAttribute("expiry_date"), TPDateTimeUtility.LONG, resourceLocale),
                       true, out);
        }
        else if (validTo.equals(TradePortalConstants.OTHER_EXPIRY_DATE))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.ValidTo", TradePortalConstants.TEXT_BUNDLE)),
                       resourceManager.getText("GuaranteeIssue.Other1", TradePortalConstants.TEXT_BUNDLE), true, out);

        }

        // Delivery Instructions
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliveryInstructions",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Deliver To
        String deliverTo = terms.getAttribute("guar_deliver_to");
        downloadData = new StringBuffer();
        if (deliverTo.equals(TradePortalConstants.DELIVER_TO_APPLICANT))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverTo", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Applicant", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverTo.equals(TradePortalConstants.DELIVER_TO_BENEFICIARY))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverTo", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Beneficiary", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverTo.equals(TradePortalConstants.DELIVER_TO_AGENT))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverTo", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Agent1", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverTo.equals(TradePortalConstants.DELIVER_TO_OTHER))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverTo", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Other1", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }

        // Deliver By
        String deliverBy = terms.getAttribute("guar_deliver_by");
        downloadData = new StringBuffer();
        if (deliverBy.equals(TradePortalConstants.DELIVER_BY_TELEX))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverBy", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.TelexSwift", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverBy.equals(TradePortalConstants.DELIVER_BY_REG_MAIL))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverBy", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.RegMail", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverBy.equals(TradePortalConstants.DELIVER_BY_MAIL))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverBy", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Mail", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }
        else if (deliverBy.equals(TradePortalConstants.DELIVER_BY_COURIER))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DeliverBy", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.Courier", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }

        // Agent
        termsPartyOid = terms.getAttribute("c_FourthTermsParty");

        if (!InstrumentServices.isBlank(termsPartyOid))
        {
            termsParty.setAttribute("terms_party_oid", termsPartyOid);
            termsParty.getDataFromAppServer();

            isValidTermsParty = true;
        }
        else
        {
            isValidTermsParty = false;
        }

        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AgentsName",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        appendTermsParty(downloadData, termsParty, resourceManager, resourceLocale, isValidTermsParty, out);

        // Accept Instruction From This Party
        downloadData = new StringBuffer();
        if (terms.getAttribute("guar_accept_instr").equals(TradePortalConstants.INDICATOR_YES))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AcceptInst", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }else
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AcceptInst", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }

        // Advise Issuance
        downloadData = new StringBuffer();
        if (terms.getAttribute("guar_advise_issuance").equals(TradePortalConstants.INDICATOR_YES))
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AdviseIssuance", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }else
        {
            appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AdviseIssuance", TradePortalConstants.TEXT_BUNDLE)),
            		StringFunction.asciiToUnicode(resourceManager.getText("common.No", TradePortalConstants.TEXT_BUNDLE)), true, out);
        }

        // Detail of Tender
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DetailsofTender",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("tender_order_contract_details"));
        flushData( out, downloadData, true);

        appendSectionHeader( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("SLCIssue.OtherConditionsShort", TradePortalConstants.TEXT_BUNDLE)),
                true, out);

        downloadData = new StringBuffer();
        setAutoExtTerms(downloadData, terms, resourceManager, out);
       // flushData( out, downloadData, true);

      	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - Begin
  	  if (TradePortalConstants.INDICATOR_YES.equals(terms.getAttribute("ucp_version_details_ind"))){
  		  downloadData = new StringBuffer();
  		  downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("common.ICCPublications", TradePortalConstants.TEXT_BUNDLE)));
  		  downloadData.append(" - ");
  		  flushData( out, downloadData, true);
  		  
  		  downloadData = new StringBuffer();
  		  downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("common.ICCApplicableRules", TradePortalConstants.TEXT_BUNDLE)));
  		  downloadData.append(" : ");
  		  downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("common.Yes", TradePortalConstants.TEXT_BUNDLE)));
  		  flushData( out, downloadData, true);
  		  
  		  downloadData = new StringBuffer();
  		  downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("common.ICCVersion", TradePortalConstants.TEXT_BUNDLE)));
  		  downloadData.append(" : ");
  		  downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr("ICC_GUIDELINES", terms.getAttribute("ucp_version")));
  		  flushData( out, downloadData, true);
  		  
  		  if (StringFunction.isNotBlank(terms.getAttribute("ucp_details"))){
  			  downloadData = new StringBuffer();
  			  downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("common.ICCDetails", TradePortalConstants.TEXT_BUNDLE)));
  			  downloadData.append(" : ");
  			  downloadData.append(terms.getAttribute("ucp_details"));
  			  flushData( out, downloadData, true);
  		  }
  	  }
  	//Sandeep - Rel-8.3 CR-752 Dev 06/11/2013 - End 
  	  
        // Additional Info SPENKE IR-T36000002852 
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AdditionalInf",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Customer Text
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.CustomerText",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("guar_customer_text"));
        flushData( out, downloadData, true);

        // Bank Standard Wording
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.BankStandardWording",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("guar_bank_standard_text"));
        flushData( out, downloadData, true);

        //INSTRUCTIONS TO BANK
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.InstructionstoBank",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Bank Instructions
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.OtherInstructions",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("special_bank_instructions"));
        flushData( out, downloadData, true);

        // Settlement Instructions
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.SettlementInstructions",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Brach Code
        appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.BranchCode", TradePortalConstants.TEXT_BUNDLE)),
                   terms.getAttribute("branch_code"), true, out);

        // Debit - Our Account Number
        appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DebitAccount", TradePortalConstants.TEXT_BUNDLE)),
                   terms.getAttribute("settlement_our_account_num"), true, out);

        // Debit - Foreign Ccy Account
        appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DebitFCA", TradePortalConstants.TEXT_BUNDLE)),
                   terms.getAttribute("settlement_foreign_acct_num"), true, out);

        // Currency of Account
        currencyCode = terms.getAttribute("settlement_foreign_acct_curr");

        appendData( downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.CcyofAccount", TradePortalConstants.TEXT_BUNDLE)),
                    currencyCode, true, out);


        // Commissions & Charges
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.SettlementInstructions",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);

        // Debit - Our Account Number
        appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DebitAccount", TradePortalConstants.TEXT_BUNDLE)),
                   terms.getAttribute("coms_chrgs_our_account_num"), true, out);

        // Debit - Foreign Ccy Account
        appendData(downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.DebitFCA", TradePortalConstants.TEXT_BUNDLE)),
                   terms.getAttribute("coms_chrgs_foreign_acct_num"), true, out);

        // Currency of Account
        currencyCode = terms.getAttribute("coms_chrgs_foreign_acct_curr");

        appendData( downloadData,  StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.CcyofAccount", TradePortalConstants.TEXT_BUNDLE)),
                    currencyCode, true, out);

        // Additional Text for Settlement or Commissions & Charges
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.AdditionalSettlementCommissionsChargesText",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("coms_chrgs_other_text"));
        flushData( out, downloadData, true);

        // Internal Instructions
        appendSectionHeader(downloadData, StringFunction.asciiToUnicode(resourceManager.getText("GuaranteeIssue.InternalInstructions",
                            TradePortalConstants.TEXT_BUNDLE)), true, out);
        downloadData = new StringBuffer();
        downloadData.append(terms.getAttribute("internal_instructions"));
        flushData( out, downloadData, true);
   }





   /**
    * This method sets the Http response's content type to a Trade Portal-specific
    * application type so that a Save file dialog will be displayed, and it sets the
    * content disposition so that a default file name can be used.
    *

    * @param   response        - the Http servlet response object
    * @param   contentType     - the Trade Portal specific content type for the
    *                            file being downloaded
    * @param   defaultFileName - the default name of the file to be downloaded
    * @return  void
    */
   public static void setResponseContent(HttpServletResponse response, String contentType, String defaultFileName)
   {
      StringBuffer   contentDisposition = new StringBuffer();

      // Set the content type to a TradePortal-specific application type so that it can be saved as a local file
      response.setContentType(contentType);

      contentDisposition.append("attachment;filename=");
      contentDisposition.append(defaultFileName);

      // Set the content disposition so that the filename uses a unique naming convention
      response.setHeader("Content-Disposition", contentDisposition.toString());
   }






   /**
    * This method is meant to consolidate 2 lines of code into 1.  Basically all that
    * is going on here, we are making it easier to send the data out to the file and
    * insert an additional carriage return as needed.
    *

    * @param    ServletOutputStream out     - object to write to a file with.
    * @param    StringBuffer downloadData   - object that contains the data that needs to be written.
    * @param    boolean blankLine           - Do you want to insert a balnk line after the data is written?
    */
   private static void flushData( ServletOutputStream out, StringBuffer downloadData, boolean blankLine)
                  throws IOException
   {
	  //IR-GAUI051582080 Krishna 05/19/2008 Begin
	  //Commented the following line and added a line below that to convert xssHtml codes back to
	  //original characters.
	  //out.println(downloadData.toString());
      out.println(StringFunction.xssHtmlToChars(downloadData.toString()));
      //IR-GAUI051582080 Krishna 05/19/2008 End

      if( blankLine ) out.println("");
   }






   /**
    * This method helps reuse code that gets called repeatedly.  The goal is to isolate the following
    * code in 1 location:

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("ImportDLCIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");
      if (isValidTermsParty)
      {
         downloadData.append(termsParty.getAttribute("address_postal_code"));
      }
      flushData( out, downloadData, true);

    *
    *

    * @param    StringBuffer downloadData   - object that contains the data that needs to be written.
    * @param    String colHeader            - The caption header for the current row.
    * @param    String delimiter            - seperater between the header and the display data (ie a ':' -or- '-'
    * @param    String displayData          - the display data is usually a webbeans attribute value.
    * @param    boolean isValidTermsParty   - boolean to determine whether or not to display the attribute
    *                                         (Prevents NullPointerException).
    * @param    boolean blankLine           - Do you want to insert a balnk line after the data is written?
    * @param    ServletOutputStream out     - object to write to a file with.
    */
   private static void appendTermsPartyAttribute( StringBuffer downloadData, String colHeader,
                                                  String delimiter,          String displayData,
                                                  boolean isValidTermsParty, boolean blankLine,
                                                  ServletOutputStream out)
                  throws ServletException, IOException, AmsException
   {
        downloadData = new StringBuffer();
        downloadData.append( colHeader );
        downloadData.append( delimiter );

        if (isValidTermsParty)
        {
            downloadData.append( displayData );
        }
        flushData( out, downloadData, blankLine);
   }





   /**
    * This method output all the address information for a terms party
    *

    * @param    StringBuffer downloadData   - object that contains the data that needs to be written.
    * @param    boolean isValidTermsParty   - boolean to determine whether or not to display the attribute
    *                                         (Prevents NullPointerException).
    * @param    ServletOutputStream out     - object to write to a file with.
    */
   private static void appendTermsParty(StringBuffer downloadData, TermsPartyWebBean termsParty,
                                                 ResourceManager resourceManager, String resourceLocale,
                                                 boolean isValidTermsParty, ServletOutputStream out)
                  throws ServletException, IOException, AmsException
   {
      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.Name", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("name"), isValidTermsParty, false, out);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.PhoneNumber", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("phone_number"), isValidTermsParty, false, out);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.AddressLine1", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("address_line_1"), isValidTermsParty, false, out);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.AddressLine2", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("address_line_2"), isValidTermsParty, false, out);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.City", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("address_city"), isValidTermsParty, false, out);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.ProvinceState", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("address_state_province"), isValidTermsParty, false, out);

      downloadData = new StringBuffer();
      downloadData.append(StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.Country", TradePortalConstants.TEXT_BUNDLE)));
      downloadData.append(" : ");
      if (isValidTermsParty && (!InstrumentServices.isBlank(termsParty.getAttribute("address_country"))))
      {
         downloadData.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.COUNTRY,
                                                                           termsParty.getAttribute("address_country"),
                                                                           resourceLocale));
      }
      flushData( out, downloadData, false);

      appendTermsPartyAttribute( downloadData, StringFunction.asciiToUnicode(resourceManager.getText("ImportDLCIssue.PostalCode", TradePortalConstants.TEXT_BUNDLE)),
                                 " : ", termsParty.getAttribute("address_postal_code"), isValidTermsParty, true, out);
   }






   /**
    * This method helps isollate the common code written to display a section header with in the
    * outgoing download file information.  The following illustrates the code being isolated here:

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.ApplicantPlain", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" - ");
      flushData( out, downloadData, true);

    *

    * @param    StringBuffer downloadData   - object that contains the data that needs to be written.
    * @param    String colHeader            - The caption header for the current row.
    * @param    boolean blankLine           - Do you want to insert a balnk line after the data is written?
    * @param    ServletOutputStream out     - object to write to a file with.
    */
   private static void appendSectionHeader( StringBuffer downloadData,  String colHeader,
                                            boolean blankLine,          ServletOutputStream out)
                  throws ServletException, IOException, AmsException
   {
        downloadData = new StringBuffer();
        downloadData.append( colHeader );
        downloadData.append(" - ");
        flushData( out, downloadData, blankLine);
   }






   /**
    * This method helps isollate the common code written to display general body data with in the
    * outgoing download file information.  The following illustrates the code being isolated here:

      downloadData = new StringBuffer();
      downloadData.append(resourceManager.getText("SLCIssue.AdditionalConditions", TradePortalConstants.TEXT_BUNDLE));
      downloadData.append(" : ");
      downloadData.append(terms.getAttribute("additional_conditions"));

      flushData( out, downloadData, true);

    *

    * @param    StringBuffer downloadData   - object that contains the data that needs to be written.
    * @param    String colHeader            - The caption header for the current row.
    * @param    String displayData          - the display data is usually a webbeans attribute value.
    * @param    boolean blankLine           - Do you want to insert a balnk line after the data is written?
    * @param    ServletOutputStream out     - object to write to a file with.
    */
   private static void appendData( StringBuffer downloadData,  String colHeader,  String displayData,
                                   boolean blankLine,          ServletOutputStream out)
                  throws ServletException, IOException, AmsException
   {
        downloadData = new StringBuffer();
        downloadData.append( colHeader );
        downloadData.append(" : ");
        downloadData.append( displayData );

        flushData( out, downloadData, blankLine);
   }

   protected static void setPaymentTenorTerms(StringBuffer downloadData, TermsWebBean terms, String resourceLocale, ServletOutputStream out) {
		try {
			ReferenceDataManager refDataMgr = ReferenceDataManager.getRefDataMgr();
			String percentAmountRadio 			= terms.getAttribute("percent_amount_dis_ind");
			String currencyCode 				= terms.getAttribute("amount_currency_code");
			String termsAmount 					= terms.getAttribute("amount");
			String termsPmtType 				= terms.getAttribute("payment_type");
			String specialTenorTermsText			= terms.getAttribute("special_tenor_text");
			SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			if ( TradePortalConstants.SPECIAL.equals(termsPmtType)){
				if (StringFunction.isNotBlank(specialTenorTermsText))
					downloadData.append(specialTenorTermsText);
				
				return;
			}
			List<AmsEntityWebBean> pmtTermsTenorDtlList = terms.getChildren("PmtTermsTenorDtl","pmt_terms_tenor_dtl_oid","p_terms_oid"); 
			for(AmsEntityWebBean pmtTermsTenorDtl : pmtTermsTenorDtlList) {          
				
				String tenorType = pmtTermsTenorDtl.getAttribute("tenor_type");
				String daysAfterType = pmtTermsTenorDtl.getAttribute("days_after_type");
				String maturityDate = pmtTermsTenorDtl.getAttribute("maturity_date");
				String numberOfDaysAfter = pmtTermsTenorDtl.getAttribute("num_days_after");
				String tenorAmount = 	pmtTermsTenorDtl.getAttribute("amount");
				String tenorPercent = 	pmtTermsTenorDtl.getAttribute("percent");
				
				String tempAmountStr = "";
				if(percentAmountRadio.equalsIgnoreCase("A")){
					tempAmountStr =  TPCurrencyUtility.getDisplayAmount(tenorAmount, currencyCode, resourceLocale);
					
				}else if(percentAmountRadio.equalsIgnoreCase("P")){
			    	BigDecimal finPCTValue = BigDecimal.ZERO;
			    	if ((tenorPercent != null)&&(!tenorPercent.equals(""))) {
			    		finPCTValue = (new BigDecimal(tenorPercent)).divide(new BigDecimal(100.00));
			    	}
			    	if (finPCTValue.compareTo(BigDecimal.ZERO) != 0){
			    		BigDecimal tempAmount = BigDecimal.ZERO;
			    		BigDecimal tenorAmountTmp = new BigDecimal(termsAmount == null ? "0.00" : termsAmount) ;
			    		tempAmount = finPCTValue.multiply(tenorAmountTmp);
						String trnxCurrencyCode = terms.getAttribute("copy_of_currency_code");
						int numberOfCurrencyPlaces = 2;

						if (trnxCurrencyCode != null && !trnxCurrencyCode.equals("")) {
							numberOfCurrencyPlaces = Integer.parseInt(ReferenceDataManager
									.getRefDataMgr().getAdditionalValue(
											TradePortalConstants.CURRENCY_CODE,
											trnxCurrencyCode));
						}
						tempAmount = tempAmount.setScale(numberOfCurrencyPlaces,
								BigDecimal.ROUND_HALF_UP);
						tempAmountStr =  TPCurrencyUtility.getDisplayAmount(tempAmount.toString(), currencyCode, resourceLocale);
			    	}

				}
				downloadData.append(currencyCode);
				downloadData.append(" ");
				downloadData.append(tempAmountStr);
				if (TradePortalConstants.PMT_SIGHT.equals(tenorType)){
					downloadData.append(" at ");
				}else{
					downloadData.append(" by ");
				}
				downloadData.append(refDataMgr.getDescr(TradePortalConstants.TENOR, tenorType, resourceLocale ));
				downloadData.append(" ");
				if (StringFunction.isNotBlank(daysAfterType)){
					downloadData.append(numberOfDaysAfter);
					downloadData.append(" days after ");
					downloadData.append(refDataMgr.getDescr(TradePortalConstants.PMT_TERMS_DAYS_AFTER, daysAfterType, resourceLocale ));
				}
				if (StringFunction.isNotBlank(maturityDate)){
					downloadData.append(" at fixed maturity. Maturity date of ");
					String date = pmtTermsTenorDtl.getAttribute("maturity_date");
					if(StringFunction.isNotBlank(date)){
						date = isoDateFormat.format(pmtTermsTenorDtl.getAttribute("maturity_date"));
						date = TPDateTimeUtility.formatDate(date, TPDateTimeUtility.SHORT, resourceLocale);
					}
					downloadData.append(date);
					
				}
				flushData(out, downloadData, false);
			}
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (AmsException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static void setAutoExtTerms(StringBuffer downloadData, TermsWebBean terms, com.amsinc.ecsg.util.ResourceManager resourceManager, ServletOutputStream out) throws ServletException, IOException {
		
			try {
				String autoExtInd 			= terms.getAttribute("auto_extension_ind");
				
				if (TradePortalConstants.INDICATOR_YES.equals(autoExtInd)){
					
					String maxAutoExtAllowed	= terms.getAttribute("max_auto_extension_allowed");
					String autoExtPeriod 		= terms.getAttribute("auto_extension_period");
					String autoExtDays 			= terms.getAttribute("auto_extension_days");					
					String notifyBeneDays 		= terms.getAttribute("notify_bene_days");
					String finalExpiryDate 		= terms.getAttribute("final_expiry_date");
					if(StringFunction.isNotBlank(finalExpiryDate)){
						finalExpiryDate = TPDateTimeUtility.formatDate(finalExpiryDate, TPDateTimeUtility.LONG, resourceManager.getResourceLocale());
					}
					
					appendSectionHeader( downloadData, resourceManager.getText("ImportDLCIssue.AutoExtensionTerms",
	                           TradePortalConstants.TEXT_BUNDLE), true, out);
				    appendData( downloadData,  resourceManager.getText("ImportDLCIssue.AutoExtension", TradePortalConstants.TEXT_BUNDLE),
				    		 "Yes", false, out);

				    appendData( downloadData,  resourceManager.getText("ImportDLCIssue.MaxNumber", TradePortalConstants.TEXT_BUNDLE),
				    		maxAutoExtAllowed, false, out);

				    appendData( downloadData,  resourceManager.getText("ImportDLCIssue.ExtensionPeriod", TradePortalConstants.TEXT_BUNDLE),
				    		ReferenceDataManager.getRefDataMgr().getDescr(
									TradePortalConstants.AUTO_EXTEND_PERIOD_TYPE, autoExtPeriod, resourceManager.getResourceLocale()), false, out);

				    appendData( downloadData,  resourceManager.getText("ImportDLCIssue.NumOfDays", TradePortalConstants.TEXT_BUNDLE),
				    		autoExtDays, false, out);
				    
				    appendData( downloadData,  resourceManager.getText("TermsBeanAlias.final_expiry_date", TradePortalConstants.TEXT_BUNDLE),
				    		finalExpiryDate, false, out);
				    
				    appendData( downloadData,  resourceManager.getText("TermsBeanAlias.notify_bene_days", TradePortalConstants.TEXT_BUNDLE),
				    		notifyBeneDays, true, out);

				    				    
				}
				
			} catch (AmsException e) {
				e.printStackTrace();
			}
				
		
	}
   
}