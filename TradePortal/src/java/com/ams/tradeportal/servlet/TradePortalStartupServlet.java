package com.ams.tradeportal.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ams.tradeportal.busobj.util.ServerCleanup;
import com.ams.tradeportal.common.TPDateTimeUtility;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.StringFunction;

import weblogic.logging.NonCatalogLogger;

/**
 * The TradePortalStartupServlet's only function is to initialize the Trade Portal infrastructure. This servlet must be set to be
 * initialized on startup of the application server. Most app servers provide startup class functionality. However, this requires
 * that the startup class be in the system classpath instead of the classpath of the EAR file. Running the startup() method when
 * this servlet is initialized requires nothing to be in the system classpath, but still runs everything prior to the application
 * server starting.
 *
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 *
 */
public class TradePortalStartupServlet extends HttpServlet {
	private static final Logger LOG = LoggerFactory.getLogger(TradePortalStartupServlet.class);

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing since the servlet is never called directly
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing since the servlet is never called directly
	}

	public void doTheWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing since the servlet is never called directly
	}

	/**
	 * Actually does the work of initializing the Trade Portal infrastructure and other cleanup tasks.
	 */
	public void init() throws ServletException {
		try {
			PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

			String serverLocation = JPylonProperties.getInstance().getString("serverLocation");

			// Determine if strings sent to standard output should be logged in the WebLogic log file
			// Examine the writeStandardOutputToLog setting in TradePortal.properties
			// The default is to send standard out to the log. Therefore,
			// map standard out to the log if setting does not exist or if it is not 'N'
			boolean logStdOut = false;
			String writeStandardOutputToLog = null;

			try {
				writeStandardOutputToLog = portalProperties.getString("writeStandardOutputToLog");

				if (writeStandardOutputToLog.equalsIgnoreCase("N")) {
					logStdOut = false;
				} else {
					logStdOut = true;
				}
			} catch (Exception e) {
				// turn logging on if no setting specified
				logStdOut = true;
			}

			if (logStdOut) {
				// Set standard out to be an instance of our inner class (defined below)
				System.setOut(new WeblogicLogPrintStream());
				System.setErr(new WeblogicLogPrintStream());
			}

			// Set the default timezone to UTC (GMT)
			// This is so that date objects will be expressed in GMT
			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

			LOG.info("***********************************************************");
			LOG.info("\nStarting TradePortal for server {}\n\n", serverLocation);

			if (serverLocation == null) {
				LOG.info("TradePortal.startup: No server location was specified for the EJB server");
			}

			// First create and initialize the server deployment object
			ServerDeploy serverDeploy = (ServerDeploy) EJBObjectFactory.createClientEJB(serverLocation, "ServerDeploy");

			serverDeploy.init(serverLocation);
			serverDeploy.remove();

			// Second, perform a server cleanup.
			ServerCleanup.cleanUp();

			// Lastly, print out the current GMT date/time and the method for
			// obtaining that time.
			Date gmtDateTime = GMTUtility.getGMTDateTime(true);
			String formattedGMT = TPDateTimeUtility.formatDateTime(gmtDateTime, "en_US");

			LOG.info("\n\n");
			LOG.info("   ********************************************************");
			LOG.info("   * The current GMT date/time is: {}", formattedGMT);

			JPylonProperties jPylonProperties = JPylonProperties.getInstance();

			String property = jPylonProperties.getString("overrideDate");
			if (StringFunction.isNotBlank(property) && property.equalsIgnoreCase("y")) {
				LOG.info("   *   (obtained from the overrideDate.xml file )");
			} else if (jPylonProperties.getString("getDateTimeFromDatabase").equals("y")) {
				LOG.info("   *   (obtained from the database)");
			} else {
				LOG.info("   *   (obtained from the server)");
			}
			LOG.info("   ********************************************************");
			LOG.info("\n\nTradePortal Server startup complete.\n");
			LOG.info("***********************************************************");
		} catch (Exception e) {
			LOG.error("Could not complete initialization of TradePortalStartupServlet", e);
		}
	}

	/**
	 * Inner class used to capture strings being sent to system.out and instead write them to the Weblogic log. Weblogic's log will
	 * also output to the console if it is set up to do so.
	 */
	public static class WeblogicLogPrintStream extends PrintStream {
		// Weblogic logger instance to which all strings are sent
		private NonCatalogLogger weblogicLogger;

		/**
		 * Constructor - need to have this to extend PrintStream
		 *
		 * @param OutputStream
		 * 
		 */
		public WeblogicLogPrintStream(OutputStream os) {
			super(os);
		}

		/**
		 * Constructor - need to have this to extend PrintStream
		 *
		 * @param OutputStream
		 * 
		 */
		public WeblogicLogPrintStream(OutputStream os, boolean b) {
			super(os, b);
		}

		/**
		 * Constructor. Grabs instance of weblogic's logger class and holds onto it for later use
		 */
		public WeblogicLogPrintStream() {
			super(System.out);
			this.weblogicLogger = new NonCatalogLogger("TradePortal");
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param Object
		 *            - the string to be written
		 */
		public void print(Object o) {

			weblogicLogger.info(o.toString());
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param Object
		 *            - the string to be written
		 */
		public void println(Object o) {

			weblogicLogger.info(o.toString());
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(String s) {

			weblogicLogger.info(s);
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(String s) {

			weblogicLogger.info(s);
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param boolean
		 *            - the string to be written
		 */
		public void print(boolean x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param boolean
		 *            - the string to be written
		 */
		public void println(boolean x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param char
		 *            - the string to be written
		 */
		public void print(char x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param char
		 *            - the string to be written
		 */
		public void println(char x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(int x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(int x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(long x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(long x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(float x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(float x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(double x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(double x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of print method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void print(char[] x) {

			weblogicLogger.info(String.valueOf(x));
		}

		/**
		 * Override of println method. Instead of writing to stdout, sends the string to weblogic's logger.
		 *
		 * @param String
		 *            - the string to be written
		 */
		public void println(char[] x) {

			weblogicLogger.info(String.valueOf(x));
		}

	}
}
