
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the Account Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AccountTester
{
private static final Logger LOG = LoggerFactory.getLogger(AccountTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      Account testObject = null;

      /* Test the basic functionaility of the Account object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class Account");
         testObject = (Account)EJBObjectFactory.createClientEJB("t3://localhost:7001","Account");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute account_number to 'TEST'");
         testObject.setAttribute("account_number", "TEST");
   
         LOG.info("Tester: Setting attribute currency to 'O'");
         testObject.setAttribute("currency", "O");
   
         LOG.info("Tester: Setting attribute owner_object_type to 'O'");
         testObject.setAttribute("owner_object_type", "O");
   
         LOG.info("Tester: Setting attribute account_name to 'TEST'");
         testObject.setAttribute("account_name", "TEST");
   
         LOG.info("Tester: Setting attribute bank_country_code to 'O'");
         testObject.setAttribute("bank_country_code", "O");
   
         LOG.info("Tester: Setting attribute source_system_branch to 'TEST'");
         testObject.setAttribute("source_system_branch", "TEST");
   
         LOG.info("Tester: Setting attribute account_type to 'TEST'");
         testObject.setAttribute("account_type", "TEST");
   
         LOG.info("Tester: Setting attribute available_for_debit to 'Y'");
         testObject.setAttribute("available_for_debit", "Y");
         
         /* Vasavi CR-509 12/10/2009  BEGIN */
         LOG.info("Tester: Setting attribute available_for_direct_debit to 'Y'");
         testObject.setAttribute("available_for_direct_debit", "Y");
         /* Vasavi CR-509 12/10/2009  END */
         
         LOG.info("Tester: Setting attribute available_for_loan_request to 'Y'");
         testObject.setAttribute("available_for_loan_request", "Y");
   
         LOG.info("Tester: Setting attribute available_for_domestic_pymts to 'Y'");
         testObject.setAttribute("available_for_domestic_pymts", "Y");
   
         LOG.info("Tester: Setting attribute pending_transaction_balance to '5.2'");
         testObject.setAttribute("pending_transaction_balance", "5.2");
   
         LOG.info("Tester: Setting attribute current_balance to '5.2'");
         testObject.setAttribute("current_balance", "5.2");
   
         LOG.info("Tester: Setting attribute last_retrieved_from_bank_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("last_retrieved_from_bank_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute interest_rate to '5.2'");
         testObject.setAttribute("interest_rate", "5.2");
   
         LOG.info("Tester: Setting attribute available_funds to '5.2'");
         testObject.setAttribute("available_funds", "5.2");
   
         LOG.info("Tester: Setting attribute status_code to 'TEST'");
         testObject.setAttribute("status_code", "TEST");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) {
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
