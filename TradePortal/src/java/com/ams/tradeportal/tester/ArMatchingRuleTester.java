
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the ArMatchingRule Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ArMatchingRuleTester
{
private static final Logger LOG = LoggerFactory.getLogger(ArMatchingRuleTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      ArMatchingRule testObject = null;

      /* Test the basic functionaility of the ArMatchingRule object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class ArMatchingRule");
         testObject = (ArMatchingRule)EJBObjectFactory.createClientEJB("t3://localhost:7001","ArMatchingRule");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute buyer_id to 'TEST'");
         testObject.setAttribute("buyer_id", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_name to 'TEST'");
         testObject.setAttribute("buyer_name", "TEST");
   
         LOG.info("Tester: Setting attribute tolerance_percent_indicator to 'Y'");
         testObject.setAttribute("tolerance_percent_indicator", "Y");
   
         LOG.info("Tester: Setting attribute tolerance_percent_plus to '5.2'");
         testObject.setAttribute("tolerance_percent_plus", "5.2");
   
         LOG.info("Tester: Setting attribute tolerance_percent_minus to '5.2'");
         testObject.setAttribute("tolerance_percent_minus", "5.2");
   
         LOG.info("Tester: Setting attribute tolerance_amount_indicator to 'Y'");
         testObject.setAttribute("tolerance_amount_indicator", "Y");
   
         LOG.info("Tester: Setting attribute tolerance_amount_plus to '5.2'");
         testObject.setAttribute("tolerance_amount_plus", "5.2");
   
         LOG.info("Tester: Setting attribute tolerance_amount_minus to '5.2'");
         testObject.setAttribute("tolerance_amount_minus", "5.2");
   
         LOG.info("Tester: Setting attribute no_tolerance_indicator to 'Y'");
         testObject.setAttribute("no_tolerance_indicator", "Y");
   
         LOG.info("Tester: Setting attribute partial_pay_to_portal_indicator to 'Y'");
         testObject.setAttribute("partial_pay_to_portal_indicator", "Y");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
