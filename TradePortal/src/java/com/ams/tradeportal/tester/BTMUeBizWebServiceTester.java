package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ams.tradeportal.BTMUeBizWebService.*;
import java.net.UnknownHostException;
import javax.xml.bind.JAXBException;

/**
 *
 * @author weian.zhu
 */
public class BTMUeBizWebServiceTester  {
private static final Logger LOG = LoggerFactory.getLogger(BTMUeBizWebServiceTester.class);
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, JAXBException  {
        if (args.length < 1) {
            LOG.info("usage: java -cp BTMUeBizWebServiceTest.jar com.ams.tradeportal.tester.BTMUeBizWebServiceTester service_url=... [signed_data_field=...] [signature=...] [customer_id=...] [user_id=...] [ignore_cert=Y/N]");
            return;
        }

        com.ams.tradeportal.BTMUeBizWebService.Test test = new com.ams.tradeportal.BTMUeBizWebService.Test();
        for (int i = 0; i < args.length; i++) {
            String[] parms = args[i].split("=");
            if ("signed_data_field".equals(parms[0])) {
                test.signedDataField = parms[1];
            }
            else if ("signature".contains(parms[0])){
                test.signature = parms[1];
            }
            else if ("service_url".equals(parms[0])){
                test.serviceURL = parms[1];
            }
            else if ("customer_id".equals(parms[0])){
                test.customerID = parms[1];
            }
            else if ("user_id".equals(parms[0])) {
                test.userID = parms[1];
            }
            else if ("ignore_cert".equals(parms[0])) {
                test.ignoreCert = parms[1];
            }
            
        }
        test.testBTMUeBizWebService();
    }
    
    

}
