
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the CorporateOrganization Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CorporateOrganizationTester
{
private static final Logger LOG = LoggerFactory.getLogger(CorporateOrganizationTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      CorporateOrganization testObject = null;

      /* Test the basic functionaility of the CorporateOrganization object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class CorporateOrganization");
         testObject = (CorporateOrganization)EJBObjectFactory.createClientEJB("t3://localhost:7001","CorporateOrganization");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute activation_status to 'O'");
         testObject.setAttribute("activation_status", "O");
   
         LOG.info("Tester: Setting attribute date_deactivated to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_deactivated", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting association default_user_oid to ''");
         testObject.setAttribute("default_user_oid", "");
   
         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");
   
         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");
   
         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");
   
         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");
   
         LOG.info("Tester: Setting attribute address_postal_code to 'TEST'");
         testObject.setAttribute("address_postal_code", "TEST");
   
         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");
   
         LOG.info("Tester: Setting attribute allow_airway_bill to 'Y'");
         testObject.setAttribute("allow_airway_bill", "Y");
   
         LOG.info("Tester: Setting attribute allow_auto_lc_create to 'Y'");
         testObject.setAttribute("allow_auto_lc_create", "Y");
   
         LOG.info("Tester: Setting attribute allow_create_bank_parties to 'Y'");
         testObject.setAttribute("allow_create_bank_parties", "Y");
   
         LOG.info("Tester: Setting attribute allow_export_collection to 'Y'");
         testObject.setAttribute("allow_export_collection", "Y");
         //Vasavi CR 524 03/31/2010 Begin
         LOG.info("Tester: Setting attribute allow_new_export_collection to 'Y'");
         testObject.setAttribute("allow_new_export_collection", "Y");
         //Vasavi CR 524 03/31/2010 End
         LOG.info("Tester: Setting attribute allow_export_LC to 'Y'");
         testObject.setAttribute("allow_export_LC", "Y");
   
         LOG.info("Tester: Setting attribute allow_funds_transfer to 'Y'");
         testObject.setAttribute("allow_funds_transfer", "Y");
   
         LOG.info("Tester: Setting attribute allow_guarantee to 'Y'");
         testObject.setAttribute("allow_guarantee", "Y");
   
         LOG.info("Tester: Setting attribute allow_import_DLC to 'Y'");
         testObject.setAttribute("allow_import_DLC", "Y");
   
         LOG.info("Tester: Setting attribute allow_loan_request to 'Y'");
         testObject.setAttribute("allow_loan_request", "Y");
   
         LOG.info("Tester: Setting attribute allow_manual_po_entry to 'Y'");
         testObject.setAttribute("allow_manual_po_entry", "Y");
   
         LOG.info("Tester: Setting attribute allow_request_advise to 'Y'");
         testObject.setAttribute("allow_request_advise", "Y");
   
         LOG.info("Tester: Setting attribute allow_shipping_guar to 'Y'");
         testObject.setAttribute("allow_shipping_guar", "Y");
   
         LOG.info("Tester: Setting attribute allow_SLC to 'Y'");
         testObject.setAttribute("allow_SLC", "Y");
   
         LOG.info("Tester: Setting attribute auth_user_different_ind to 'Y'");
         testObject.setAttribute("auth_user_different_ind", "Y");
   
         LOG.info("Tester: Setting attribute authentication_method to 'O'");
         testObject.setAttribute("authentication_method", "O");
   
         LOG.info("Tester: Setting attribute base_currency_code to 'O'");
         testObject.setAttribute("base_currency_code", "O");
   
         LOG.info("Tester: Setting attribute corporate_org_type_code to 'O'");
         testObject.setAttribute("corporate_org_type_code", "O");
   
         LOG.info("Tester: Setting attribute creation_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("creation_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute dnld_adv_terms_format to 'O'");
         testObject.setAttribute("dnld_adv_terms_format", "O");
   
         LOG.info("Tester: Setting attribute doc_prep_accountname to 'TEST'");
         testObject.setAttribute("doc_prep_accountname", "TEST");
   
         LOG.info("Tester: Setting attribute doc_prep_domainname to 'TEST'");
         testObject.setAttribute("doc_prep_domainname", "TEST");
   
         LOG.info("Tester: Setting attribute doc_prep_ind to 'Y'");
         testObject.setAttribute("doc_prep_ind", "Y");
   
         LOG.info("Tester: Setting attribute dual_auth_airway_bill to 'O'");
         testObject.setAttribute("dual_auth_airway_bill", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_disc_response to 'O'");
         testObject.setAttribute("dual_auth_disc_response", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_export_coll to 'O'");
         testObject.setAttribute("dual_auth_export_coll", "O");
         //Vasavi CR 524 03/31/2010 Begin
         LOG.info("Tester: Setting attribute dual_auth_new_export_coll to 'O'");
         testObject.setAttribute("dual_auth_new_export_coll", "O");
         //Vasavi CR 524 03/31/2010 End
         LOG.info("Tester: Setting attribute dual_auth_export_LC to 'O'");
         testObject.setAttribute("dual_auth_export_LC", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_funds_transfer to 'O'");
         testObject.setAttribute("dual_auth_funds_transfer", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_guarantee to 'O'");
         testObject.setAttribute("dual_auth_guarantee", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_import_DLC to 'O'");
         testObject.setAttribute("dual_auth_import_DLC", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_loan_request to 'O'");
         testObject.setAttribute("dual_auth_loan_request", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_request_advise to 'O'");
         testObject.setAttribute("dual_auth_request_advise", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_shipping_guar to 'O'");
         testObject.setAttribute("dual_auth_shipping_guar", "O");
   
         LOG.info("Tester: Setting attribute dual_auth_SLC to 'O'");
         testObject.setAttribute("dual_auth_SLC", "O");
   
         LOG.info("Tester: Setting attribute email_language to 'O'");
         testObject.setAttribute("email_language", "O");
   
         LOG.info("Tester: Setting attribute email_receiver to 'TEST'");
         testObject.setAttribute("email_receiver", "TEST");
   
         LOG.info("Tester: Setting attribute force_certs_for_user_maint to 'Y'");
         testObject.setAttribute("force_certs_for_user_maint", "Y");
   
         LOG.info("Tester: Setting attribute last_email_date to '02/28/2000'");
         testObject.setAttribute("last_email_date", "02/28/2000");
   
         LOG.info("Tester: Setting attribute Proponix_customer_id to 'TEST'");
         testObject.setAttribute("Proponix_customer_id", "TEST");
   
         LOG.info("Tester: Setting attribute route_within_hierarchy to 'O'");
         testObject.setAttribute("route_within_hierarchy", "O");
   
         LOG.info("Tester: Setting attribute standbys_use_either_form to 'Y'");
         testObject.setAttribute("standbys_use_either_form", "Y");
   
         LOG.info("Tester: Setting attribute standbys_use_guar_form_only to 'Y'");
         testObject.setAttribute("standbys_use_guar_form_only", "Y");
   
         LOG.info("Tester: Setting attribute timeout_auto_save_ind to 'Y'");
         testObject.setAttribute("timeout_auto_save_ind", "Y");
   
         LOG.info("Tester: Setting attribute allow_atp_manual_po_entry to 'Y'");
         testObject.setAttribute("allow_atp_manual_po_entry", "Y");
   
         LOG.info("Tester: Setting attribute allow_approval_to_pay to 'Y'");
         testObject.setAttribute("allow_approval_to_pay", "Y");
   
         LOG.info("Tester: Setting attribute dual_auth_approval_to_pay to 'O'");
         testObject.setAttribute("dual_auth_approval_to_pay", "O");
   
         LOG.info("Tester: Setting attribute allow_auto_atp_create to 'Y'");
         testObject.setAttribute("allow_auto_atp_create", "Y");
   
         LOG.info("Tester: Setting attribute allow_arm to 'Y'");
         testObject.setAttribute("allow_arm", "Y");
   
         LOG.info("Tester: Setting attribute dual_auth_arm to 'O'");
         testObject.setAttribute("dual_auth_arm", "O");
         
         /* Vasavi CR-509 12/10/2009 Begin */
         LOG.info("Tester: Setting attribute allow_direct_debit to 'Y'");
         testObject.setAttribute("allow_direct_debit", "Y");
   
         LOG.info("Tester: Setting attribute dual_auth_direct_debit to 'O'");
         testObject.setAttribute("dual_auth_direct_debit", "O");
         /* Vasavi CR-509 12/10/2009 End */
         LOG.info("Tester: Setting attribute enforce_daily_limit_transfer to 'Y'");
         testObject.setAttribute("enforce_daily_limit_transfer", "Y");
   
         LOG.info("Tester: Setting attribute transfer_daily_limit_amt to '5.2'");
         testObject.setAttribute("transfer_daily_limit_amt", "5.2");
   
         LOG.info("Tester: Setting attribute enforce_daily_limit_dmstc_pymt to 'Y'");
         testObject.setAttribute("enforce_daily_limit_dmstc_pymt", "Y");
   
         LOG.info("Tester: Setting attribute domestic_pymt_daily_limit_amt to '5.2'");
         testObject.setAttribute("domestic_pymt_daily_limit_amt", "5.2");
   
         LOG.info("Tester: Setting attribute enforce_daily_limit_intl_pymt to 'Y'");
         testObject.setAttribute("enforce_daily_limit_intl_pymt", "Y");
   
         LOG.info("Tester: Setting attribute intl_pymt_daily_limit_amt to '5.2'");
         testObject.setAttribute("intl_pymt_daily_limit_amt", "5.2");
   
         LOG.info("Tester: Setting attribute timezone_for_daily_limit to 'O'");
         testObject.setAttribute("timezone_for_daily_limit", "O");
   
         LOG.info("Tester: Setting attribute allow_xfer_btwn_accts to 'Y'");
         testObject.setAttribute("allow_xfer_btwn_accts", "Y");
   
         LOG.info("Tester: Setting attribute dual_xfer_btwn_accts to 'O'");
         testObject.setAttribute("dual_xfer_btwn_accts", "O");
   
         LOG.info("Tester: Setting attribute allow_domestic_payments to 'Y'");
         testObject.setAttribute("allow_domestic_payments", "Y");
   
         LOG.info("Tester: Setting attribute dual_domestic_payments to 'O'");
         testObject.setAttribute("dual_domestic_payments", "O");
   
         LOG.info("Tester: Setting attribute allow_pymt_instrument_auth to 'Y'");
         testObject.setAttribute("allow_pymt_instrument_auth", "Y");
   
         LOG.info("Tester: Setting attribute allow_panel_auth_for_pymts to 'Y'");
         testObject.setAttribute("allow_panel_auth_for_pymts", "Y");
   
         LOG.info("Tester: Setting attribute allow_xfer_btwn_accts_panel to 'Y'");
         testObject.setAttribute("allow_xfer_btwn_accts_panel", "Y");
   
         LOG.info("Tester: Setting attribute allow_domestic_payments_panel to 'Y'");
         testObject.setAttribute("allow_domestic_payments_panel", "Y");
   
         LOG.info("Tester: Setting attribute allow_funds_transfer_panel to 'Y'");
         testObject.setAttribute("allow_funds_transfer_panel", "Y");
   
         LOG.info("Tester: Setting attribute doc_prep_url to 'TEST'");
         testObject.setAttribute("doc_prep_url", "TEST");
   
         LOG.info("Tester: Setting attribute reporting_type to 'TEST'");
         testObject.setAttribute("reporting_type", "TEST");
   
         LOG.info("Tester: Setting attribute cust_daily_balance_amt_ftrq to '5.2'");
         testObject.setAttribute("cust_daily_balance_amt_ftrq", "5.2");
   
         LOG.info("Tester: Setting attribute cust_daily_balance_amt_ftdp to '5.2'");
         testObject.setAttribute("cust_daily_balance_amt_ftdp", "5.2");
   
         LOG.info("Tester: Setting attribute cust_daily_balance_amt_ftba to '5.2'");
         testObject.setAttribute("cust_daily_balance_amt_ftba", "5.2");
   
         LOG.info("Tester: Setting attribute cust_daily_balance_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("cust_daily_balance_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting association client_bank_oid to ''");
         testObject.setAttribute("client_bank_oid", "");
   
         LOG.info("Tester: Setting association bank_org_group_oid to ''");
         testObject.setAttribute("bank_org_group_oid", "");
   
         LOG.info("Tester: Setting association first_op_bank_org to ''");
         testObject.setAttribute("first_op_bank_org", "");
   
         LOG.info("Tester: Setting association second_op_bank_org to ''");
         testObject.setAttribute("second_op_bank_org", "");
   
         LOG.info("Tester: Setting association third_op_bank_org to ''");
         testObject.setAttribute("third_op_bank_org", "");
   
         LOG.info("Tester: Setting association fourth_op_bank_org to ''");
         testObject.setAttribute("fourth_op_bank_org", "");
   
         LOG.info("Tester: Setting association notify_rule_oid to ''");
         testObject.setAttribute("notify_rule_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
