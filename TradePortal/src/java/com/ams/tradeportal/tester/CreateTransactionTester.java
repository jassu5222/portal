package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;
import com.ams.tradeportal.mediator.*;

/*
 * Client application to test the Transaction Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CreateTransactionTester
{
private static final Logger LOG = LoggerFactory.getLogger(CreateTransactionTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeMediatorTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


 protected static void executeMediatorTest() throws RemoteException, AmsException, RemoveException
   {
         CreateTransactionMediator mediator = null;


      /* Test the basic functionaility of a transaction mediator.*/
      try{
         /*Output the new phrase to xml*/
         DocumentHandler xmlDoc = new DocumentHandler("<DocRoot><In><selectMode>N</selectMode><startPage>/transactions/TransactionsHome.jsp</startPage><cancelAction>cleanUpDoc</cancelAction><prevStepAction>goToInstrumentSearch</prevStepAction><userOid>6000011</userOid><securityRights>292843187796241809398</securityRights><clientBankOid>2000001</clientBankOid><ownerOrg>4000001</ownerOrg><titleKey>InstSearch.HeadingNewTransStep2</titleKey><newTransStep1><radioButtonOuter>CREATE_NEW_INSTRUMENT</radioButtonOuter><radioButtonInner>Instr</radioButtonInner><bankBranch>10000001</bankBranch><instrumentType></instrumentType></newTransStep1><mode>CREATE_NEW_INSTRUMENT</mode><bankBranch>10000001</bankBranch><copyType>Instr</copyType><nextStepForm>NewTransInstrSearchForm</nextStepForm><instrumentOid>12020/30</instrumentOid></In></DocRoot>", false);
         LOG.info(xmlDoc.toString());
        
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of CreateTransactionMediator");
         mediator = (CreateTransactionMediator)EJBObjectFactory.createClientEJB("t3://localhost:7001","CreateTransactionMediator");
         
         LOG.info("Tester: Calling CreateTransactionMediator.performService");
         ClientServerDataBridge csdb = new ClientServerDataBridge();
         csdb.setLocaleName("en_US");
         DocumentHandler result = mediator.performService(xmlDoc, csdb);
         LOG.info("XML RETURNED:\n" + result.toString());


      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }
      catch(Exception e) 
      {
         e.printStackTrace();
      }
   }
        

}
