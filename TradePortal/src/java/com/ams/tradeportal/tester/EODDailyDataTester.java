
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the EODDailyData Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class EODDailyDataTester
{
private static final Logger LOG = LoggerFactory.getLogger(EODDailyDataTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      EODDailyData testObject = null;

      /* Test the basic functionaility of the EODDailyData object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class EODDailyData");
         testObject = (EODDailyData)EJBObjectFactory.createClientEJB("t3://localhost:7001","EODDailyData");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute balance_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("balance_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute source to 'TEST'");
         testObject.setAttribute("source", "TEST");
   
         LOG.info("Tester: Setting attribute bank_routing_id to 'TEST'");
         testObject.setAttribute("bank_routing_id", "TEST");
   
         LOG.info("Tester: Setting attribute statement_number to '7'");
         testObject.setAttribute("statement_number", "7");
   
         LOG.info("Tester: Setting attribute opening_balance to '5.2'");
         testObject.setAttribute("opening_balance", "5.2");
   
         LOG.info("Tester: Setting attribute closing_balance to '5.2'");
         testObject.setAttribute("closing_balance", "5.2");
   
         LOG.info("Tester: Setting attribute total_debit_movement to '5.2'");
         testObject.setAttribute("total_debit_movement", "5.2");
   
         LOG.info("Tester: Setting attribute number_of_debits to '7'");
         testObject.setAttribute("number_of_debits", "7");
   
         LOG.info("Tester: Setting attribute total_credit_movement to '5.2'");
         testObject.setAttribute("total_credit_movement", "5.2");
   
         LOG.info("Tester: Setting attribute number_of_credits to '7'");
         testObject.setAttribute("number_of_credits", "7");
   
         LOG.info("Tester: Setting attribute debit_interest_rate to '5.2'");
         testObject.setAttribute("debit_interest_rate", "5.2");
   
         LOG.info("Tester: Setting attribute credit_interest_rate to '5.2'");
         testObject.setAttribute("credit_interest_rate", "5.2");
   
         LOG.info("Tester: Setting attribute overdraft_limit to '5.2'");
         testObject.setAttribute("overdraft_limit", "5.2");
   
         LOG.info("Tester: Setting attribute debit_interest_accrued to '5.2'");
         testObject.setAttribute("debit_interest_accrued", "5.2");
   
         LOG.info("Tester: Setting attribute credit_interest_accrued to '5.2'");
         testObject.setAttribute("credit_interest_accrued", "5.2");
   
         LOG.info("Tester: Setting attribute fid_accrued to '5.2'");
         testObject.setAttribute("fid_accrued", "5.2");
   
         LOG.info("Tester: Setting attribute badt_accrued to '5.2'");
         testObject.setAttribute("badt_accrued", "5.2");
   
         LOG.info("Tester: Setting attribute next_posting_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("next_posting_date", "02/28/2000 12:13:54");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
