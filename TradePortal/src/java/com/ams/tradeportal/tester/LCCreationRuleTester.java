package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the LCCreationRule Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class LCCreationRuleTester
{
private static final Logger LOG = LoggerFactory.getLogger(LCCreationRuleTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      LCCreationRule testObject = null;

      /* Test the basic functionaility of the LCCreationRule object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class LCCreationRule");
         testObject = (LCCreationRule)EJBObjectFactory.createClientEJB("t3://localhost:7001","LCCreationRule");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute description to 'TEST'");
         testObject.setAttribute("description", "TEST");
   
         LOG.info("Tester: Setting attribute creation_date_time to '02/28/2000 12:13:54'");
         testObject.setAttribute("creation_date_time", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute maximum_amount to '5.2'");
         testObject.setAttribute("maximum_amount", "5.2");
   
         LOG.info("Tester: Setting attribute min_last_shipment_date to '7'");
         testObject.setAttribute("min_last_shipment_date", "7");
   
         LOG.info("Tester: Setting attribute max_last_shipment_date to '7'");
         testObject.setAttribute("max_last_shipment_date", "7");

         LOG.info("Tester: Setting attribute instrument_type_code to 'O'");
         testObject.setAttribute("instrument_type_code", "O");
   
         LOG.info("Tester: Setting attribute pregenerated_sql to 'TEST'");
         testObject.setAttribute("pregenerated_sql", "TEST");
   
         LOG.info("Tester: Setting attribute criterion1_data to 'O'");
         testObject.setAttribute("criterion1_data", "O");
   
         LOG.info("Tester: Setting attribute criterion1_value to 'TEST'");
         testObject.setAttribute("criterion1_value", "TEST");
   
         LOG.info("Tester: Setting attribute criterion2_data to 'O'");
         testObject.setAttribute("criterion2_data", "O");
   
         LOG.info("Tester: Setting attribute criterion2_value to 'TEST'");
         testObject.setAttribute("criterion2_value", "TEST");
   
         LOG.info("Tester: Setting attribute criterion3_data to 'O'");
         testObject.setAttribute("criterion3_data", "O");
   
         LOG.info("Tester: Setting attribute criterion3_value to 'TEST'");
         testObject.setAttribute("criterion3_value", "TEST");
   
         LOG.info("Tester: Setting attribute criterion4_data to 'O'");
         testObject.setAttribute("criterion4_data", "O");
   
         LOG.info("Tester: Setting attribute criterion4_value to 'TEST'");
         testObject.setAttribute("criterion4_value", "TEST");
   
         LOG.info("Tester: Setting attribute criterion5_data to 'O'");
         testObject.setAttribute("criterion5_data", "O");
   
         LOG.info("Tester: Setting attribute criterion5_value to 'TEST'");
         testObject.setAttribute("criterion5_value", "TEST");
   
         LOG.info("Tester: Setting attribute criterion6_data to 'O'");
         testObject.setAttribute("criterion6_data", "O");
   
         LOG.info("Tester: Setting attribute criterion6_value to 'TEST'");
         testObject.setAttribute("criterion6_value", "TEST");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting association template_oid to ''");
         testObject.setAttribute("template_oid", "");
   
         LOG.info("Tester: Setting association op_bank_org_oid to ''");
         testObject.setAttribute("op_bank_org_oid", "");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   
         LOG.info("Tester: Setting association po_upload_def_oid to ''");
         testObject.setAttribute("po_upload_def_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
