


package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the OperationalBankOrganization Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
public class OperationalBankOrganizationTester
{
private static final Logger LOG = LoggerFactory.getLogger(OperationalBankOrganizationTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}

      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}

      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      OperationalBankOrganization testObject = null;

      /* Test the basic functionaility of the OperationalBankOrganization object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class OperationalBankOrganization");
         testObject = (OperationalBankOrganization)EJBObjectFactory.createClientEJB("t3://localhost:7001","OperationalBankOrganization");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");

         LOG.info("Tester: Setting attribute activation_status to 'O'");
         testObject.setAttribute("activation_status", "O");

         LOG.info("Tester: Setting attribute date_deactivated to '02/28/2000 12:13:54'");
         testObject.setAttribute("date_deactivated", "02/28/2000 12:13:54");

         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");

         LOG.info("Tester: Setting attribute imp_DLC_prefix to 'TEST'");
         testObject.setAttribute("imp_DLC_prefix", "TEST");

         LOG.info("Tester: Setting attribute imp_DLC_suffix to 'TEST'");
         testObject.setAttribute("imp_DLC_suffix", "TEST");

         LOG.info("Tester: Setting attribute incoming_SLC_prefix to 'TEST'");
         testObject.setAttribute("incoming_SLC_prefix", "TEST");

         LOG.info("Tester: Setting attribute incoming_SLC_suffix to 'TEST'");
         testObject.setAttribute("incoming_SLC_suffix", "TEST");

         LOG.info("Tester: Setting attribute airway_bill_prefix to 'TEST'");
         testObject.setAttribute("airway_bill_prefix", "TEST");

         LOG.info("Tester: Setting attribute airway_bill_suffix to 'TEST'");
         testObject.setAttribute("airway_bill_suffix", "TEST");

         LOG.info("Tester: Setting attribute steamship_guar_prefix to 'TEST'");
         testObject.setAttribute("steamship_guar_prefix", "TEST");

         LOG.info("Tester: Setting attribute steamship_guar_suffix to 'TEST'");
         testObject.setAttribute("steamship_guar_suffix", "TEST");
         //Vasavi CR 524 03/31/2010 Begin
         LOG.info("Tester: Setting attribute new_exp_coll_prefix to 'TEST'");
         testObject.setAttribute("new_exp_coll_prefix", "TEST");

         LOG.info("Tester: Setting attribute new_exp_coll_suffix to 'TEST'");
         testObject.setAttribute("new_exp_coll_suffix", "TEST");
         //Vasavi CR 524 03/31/2010 End
         LOG.info("Tester: Setting attribute dir_snd_coll_prefix to 'TEST'");
         testObject.setAttribute("dir_snd_coll_prefix", "TEST");

         LOG.info("Tester: Setting attribute dir_snd_coll_suffix to 'TEST'");
         testObject.setAttribute("dir_snd_coll_suffix", "TEST");

         LOG.info("Tester: Setting attribute guar_prefix to 'TEST'");
         testObject.setAttribute("guar_prefix", "TEST");

         LOG.info("Tester: Setting attribute guar_suffix to 'TEST'");
         testObject.setAttribute("guar_suffix", "TEST");

         LOG.info("Tester: Setting attribute Proponix_id to 'TEST'");
         testObject.setAttribute("Proponix_id", "TEST");

         LOG.info("Tester: Setting attribute export_LC_prefix to 'TEST'");
         testObject.setAttribute("export_LC_prefix", "TEST");

         LOG.info("Tester: Setting attribute export_LC_suffix to 'TEST'");
         testObject.setAttribute("export_LC_suffix", "TEST");

         LOG.info("Tester: Setting attribute address_city to 'TEST'");
         testObject.setAttribute("address_city", "TEST");

         LOG.info("Tester: Setting attribute address_country to 'O'");
         testObject.setAttribute("address_country", "O");

         LOG.info("Tester: Setting attribute address_line_1 to 'TEST'");
         testObject.setAttribute("address_line_1", "TEST");

         LOG.info("Tester: Setting attribute address_line_2 to 'TEST'");
         testObject.setAttribute("address_line_2", "TEST");

         LOG.info("Tester: Setting attribute address_postal_code to 'TEST'");
         testObject.setAttribute("address_postal_code", "TEST");

         LOG.info("Tester: Setting attribute address_state_province to 'TEST'");
         testObject.setAttribute("address_state_province", "TEST");

         LOG.info("Tester: Setting attribute fax_1 to 'TEST'");
         testObject.setAttribute("fax_1", "TEST");

         LOG.info("Tester: Setting attribute telex_1 to 'TEST'");
         testObject.setAttribute("telex_1", "TEST");

         LOG.info("Tester: Setting attribute telex_answer_back_1 to 'TEST'");
         testObject.setAttribute("telex_answer_back_1", "TEST");

         LOG.info("Tester: Setting attribute fax_2 to 'TEST'");
         testObject.setAttribute("fax_2", "TEST");

         LOG.info("Tester: Setting attribute swift_address_part1 to 'TEST'");
         testObject.setAttribute("swift_address_part1", "TEST");

         LOG.info("Tester: Setting attribute swift_address_part2 to 'TEST'");
         testObject.setAttribute("swift_address_part2", "TEST");

         LOG.info("Tester: Setting attribute phone_number to 'TEST'");
         testObject.setAttribute("phone_number", "TEST");

         LOG.info("Tester: Setting attribute loan_req_prefix to 'TEST'");
         testObject.setAttribute("loan_req_prefix", "TEST");

         LOG.info("Tester: Setting attribute loan_req_suffix to 'TEST'");
         testObject.setAttribute("loan_req_suffix", "TEST");

         LOG.info("Tester: Setting attribute funds_transfer_prefix to 'TEST'");
         testObject.setAttribute("funds_transfer_prefix", "TEST");

         LOG.info("Tester: Setting attribute funds_transfer_suffix to 'TEST'");
         testObject.setAttribute("funds_transfer_suffix", "TEST");

         LOG.info("Tester: Setting attribute request_advise_prefix to 'TEST'");
         testObject.setAttribute("request_advise_prefix", "TEST");

         LOG.info("Tester: Setting attribute request_advise_suffix to 'TEST'");
         testObject.setAttribute("request_advise_suffix", "TEST");

         LOG.info("Tester: Setting attribute approval_to_pay_prefix to 'TEST'");
         testObject.setAttribute("approval_to_pay_prefix", "TEST");

         LOG.info("Tester: Setting attribute approval_to_pay_suffix to 'TEST'");
         testObject.setAttribute("approval_to_pay_suffix", "TEST");

         LOG.info("Tester: Setting attribute transfer_btw_acct_prefix to 'TEST'");
         testObject.setAttribute("transfer_btw_acct_prefix", "TEST");

         LOG.info("Tester: Setting attribute transfer_btw_acct_suffix to 'TEST'");
         testObject.setAttribute("transfer_btw_acct_suffix", "TEST");

         LOG.info("Tester: Setting attribute domestic_payment_prefix to 'TEST'");
         testObject.setAttribute("domestic_payment_prefix", "TEST");

         LOG.info("Tester: Setting attribute domestic_payment_suffix to 'TEST'");
         testObject.setAttribute("domestic_payment_suffix", "TEST");

         LOG.info("Tester: Setting association bank_org_group_oid to ''");
         testObject.setAttribute("bank_org_group_oid", "");

         LOG.info("Tester: Setting association OTL_PurchaseOrder to ''");
         testObject.setAttribute("OTL_PurchaseOrder", "");
		
	//Pratiksha CR-509 DDI 12/09/2009 Begin

	 LOG.info("Tester: Setting attribute direct_debit_instruction_prefix to 'TEST'");
         testObject.setAttribute("direct_debit_prefix", "TEST");

         LOG.info("Tester: Setting attribute direct_debit_instruction_suffix to 'TEST'");
         testObject.setAttribute("direct_debit_suffix", "TEST");

	//Pratiksha CR-509 DDI 12/09/2009 End


         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete");

      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
