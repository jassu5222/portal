
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the POLineItem Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class POLineItemTester
{
private static final Logger LOG = LoggerFactory.getLogger(POLineItemTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      POLineItem testObject = null;

      /* Test the basic functionaility of the POLineItem object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class POLineItem");
         testObject = (POLineItem)EJBObjectFactory.createClientEJB("t3://localhost:7001","POLineItem");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute auto_added_to_amend_ind to 'Y'");
         testObject.setAttribute("auto_added_to_amend_ind", "Y");
   
         LOG.info("Tester: Setting attribute po_num to 'TEST'");
         testObject.setAttribute("po_num", "TEST");
   
         LOG.info("Tester: Setting attribute item_num to 'TEST'");
         testObject.setAttribute("item_num", "TEST");
   
         LOG.info("Tester: Setting attribute ben_name to 'TEST'");
         testObject.setAttribute("ben_name", "TEST");
   
         LOG.info("Tester: Setting attribute currency to 'TEST'");
         testObject.setAttribute("currency", "TEST");
   
         LOG.info("Tester: Setting attribute amount to '5.2'");
         testObject.setAttribute("amount", "5.2");
   
         LOG.info("Tester: Setting attribute last_ship_dt to '02/28/2000 12:13:54'");
         testObject.setAttribute("last_ship_dt", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute po_text to 'TEST'");
         testObject.setAttribute("po_text", "TEST");
   
         LOG.info("Tester: Setting attribute other1 to 'TEST'");
         testObject.setAttribute("other1", "TEST");
   
         LOG.info("Tester: Setting attribute other2 to 'TEST'");
         testObject.setAttribute("other2", "TEST");
   
         LOG.info("Tester: Setting attribute other3 to 'TEST'");
         testObject.setAttribute("other3", "TEST");
   
         LOG.info("Tester: Setting attribute other4 to 'TEST'");
         testObject.setAttribute("other4", "TEST");
   
         LOG.info("Tester: Setting attribute other5 to 'TEST'");
         testObject.setAttribute("other5", "TEST");
   
         LOG.info("Tester: Setting attribute other6 to 'TEST'");
         testObject.setAttribute("other6", "TEST");
   
         LOG.info("Tester: Setting attribute other7 to 'TEST'");
         testObject.setAttribute("other7", "TEST");
   
         LOG.info("Tester: Setting attribute other8 to 'TEST'");
         testObject.setAttribute("other8", "TEST");
   
         LOG.info("Tester: Setting attribute other9 to 'TEST'");
         testObject.setAttribute("other9", "TEST");
   
         LOG.info("Tester: Setting attribute other10 to 'TEST'");
         testObject.setAttribute("other10", "TEST");
   
         LOG.info("Tester: Setting attribute other11 to 'TEST'");
         testObject.setAttribute("other11", "TEST");
   
         LOG.info("Tester: Setting attribute other12 to 'TEST'");
         testObject.setAttribute("other12", "TEST");
   
         LOG.info("Tester: Setting attribute other13 to 'TEST'");
         testObject.setAttribute("other13", "TEST");
   
         LOG.info("Tester: Setting attribute other14 to 'TEST'");
         testObject.setAttribute("other14", "TEST");
   
         LOG.info("Tester: Setting attribute other15 to 'TEST'");
         testObject.setAttribute("other15", "TEST");
   
         LOG.info("Tester: Setting attribute other16 to 'TEST'");
         testObject.setAttribute("other16", "TEST");
   
         LOG.info("Tester: Setting attribute other17 to 'TEST'");
         testObject.setAttribute("other17", "TEST");
   
         LOG.info("Tester: Setting attribute other18 to 'TEST'");
         testObject.setAttribute("other18", "TEST");
   
         LOG.info("Tester: Setting attribute other19 to 'TEST'");
         testObject.setAttribute("other19", "TEST");
   
         LOG.info("Tester: Setting attribute other20 to 'TEST'");
         testObject.setAttribute("other20", "TEST");
   
         LOG.info("Tester: Setting attribute other21 to 'TEST'");
         testObject.setAttribute("other21", "TEST");
   
         LOG.info("Tester: Setting attribute other22 to 'TEST'");
         testObject.setAttribute("other22", "TEST");
   
         LOG.info("Tester: Setting attribute other23 to 'TEST'");
         testObject.setAttribute("other23", "TEST");
   
         LOG.info("Tester: Setting attribute other24 to 'TEST'");
         testObject.setAttribute("other24", "TEST");
   
         LOG.info("Tester: Setting attribute upload_sequence_num to '7'");
         testObject.setAttribute("upload_sequence_num", "7");
   
         LOG.info("Tester: Setting attribute previous_po_line_item_oid to '7'");
         testObject.setAttribute("previous_po_line_item_oid", "7");
   
         LOG.info("Tester: Setting attribute source_type to 'O'");
         testObject.setAttribute("source_type", "O");
   
         LOG.info("Tester: Setting attribute creation_date_time to '02/28/2000 12:13:54'");
         testObject.setAttribute("creation_date_time", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting association source_upload_definition_oid to ''");
         testObject.setAttribute("source_upload_definition_oid", "");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   
         LOG.info("Tester: Setting association assigned_to_trans_oid to ''");
         testObject.setAttribute("assigned_to_trans_oid", "");
   
         LOG.info("Tester: Setting association active_for_instrument to ''");
         testObject.setAttribute("active_for_instrument", "");
   
         LOG.info("Tester: Setting association shipment_oid to ''");
         testObject.setAttribute("shipment_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
