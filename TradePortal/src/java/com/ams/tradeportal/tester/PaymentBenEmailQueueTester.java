
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.ejb.RemoveException;

import com.ams.tradeportal.busobj.PaymentBenEmailQueue;
import com.ams.tradeportal.common.TradePortalConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.IssuedError;
import com.amsinc.ecsg.frame.ServerDeploy;
import com.amsinc.ecsg.util.ExecutionTimer;


/*
 * Client application to test the PaymentBenEmailQueue Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentBenEmailQueueTester
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentBenEmailQueueTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }

   
   protected static void executeTest() throws RemoteException, AmsException, RemoveException{
	   executeInsertTest();
	   executeCustomTest();
   }
   
   
   protected static void executeCustomTest()throws RemoteException, AmsException, RemoveException{
	   PaymentBenEmailQueue testObject = null;
	   try{
		   testObject = (PaymentBenEmailQueue)EJBObjectFactory.createClientEJB("t3://localhost:7001","PaymentBenEmailQueue");
		   LOG.info("Tester: Invoking createNewPaymentBeneficiaryEmailQueueRecord");
		   testObject.createNewPaymentBeneficiaryEmailQueueRecord("1601703", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);
		   LOG.info("Tester: Calling save() method");
		   testObject.save();
		   LOG.info("Tester: Save complete"); 		   
	   }catch(Exception e){
		   e.printStackTrace();
	   }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for (IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
	   
   }

   protected static void executeInsertTest() throws RemoteException, AmsException, RemoveException
   {
	   PaymentBenEmailQueue testObject = null;

      /* Test the basic functionaility of the PaymentBenEmailQueue object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class PaymentBenEmailQueue");
         testObject = (PaymentBenEmailQueue)EJBObjectFactory.createClientEJB("t3://localhost:7001","PaymentBenEmailQueue");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
         String dateString = sdf.format(new Date());
         
         LOG.info("Tester: Setting attribute creation_timestamp to current date/time");
         testObject.setAttribute("creation_timestamp", dateString);         

         LOG.info("Tester: Setting attribute status to 'PENDING'");
         testObject.setAttribute("status", TradePortalConstants.PYMT_BEN_EMAIL_PENDING);        
         
         LOG.info("Tester: Setting attribute agent_id to TEST_1");
         testObject.setAttribute("agent_id", "TEST_1");     
         
         LOG.info("Tester: Setting transaction_oid to '1601703'");
         testObject.setAttribute("transaction_oid", "1601703");        

         LOG.info("Tester: Setting attribute required_status to 'COMPLETE'");
         testObject.setAttribute("required_status", TradePortalConstants.BENEFICIARY_PAYMENT_STATUS_COMPLETE);        
         

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
