
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

import java.rmi.*;
import java.util.*;
import java.math.*;
import java.sql.*;
import java.io.*;

import javax.ejb.*;
import java.nio.CharBuffer;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;

import com.amsinc.ecsg.frame.AmsConstants;
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.ObjectIDCache;
import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.GMTUtility;
import com.amsinc.ecsg.util.JPylonProperties;



/*
 * Client application to test the PaymentUploadAgent by creating a payment file in 
 * the PAYMENT_FILE_UPLOAD table, which triggers the PaymentUploadAgent to process the payments.
 * It requires the following input arguments:
 *      int timesToExecute
 * 		String transactionOid
 *		String ownerOrgOid 
 *		String userOid 
 *		String fileName of the payments file
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PaymentUploadAgentTester
{
private static final Logger LOG = LoggerFactory.getLogger(PaymentUploadAgentTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }
      

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest(argv);}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest(String[] argv) throws RemoteException, AmsException, RemoveException
   {
		String transactionOid = (String)argv[1];
		String ownerOrgOid = (String)argv[2];
		String userOid = (String)argv[3];
		String fileName = (String)argv[4];
     	StringBuffer paymentFile = readPaymentFile(fileName);
		
		String sqlInsert = "insert into PAYMENT_FILE_UPLOAD (PAYMENT_FILE_UPLOAD_OID, TRANSACTION_OID, P_OWNER_ORG_ID, USER_OID, CREATION_TIMESTAMP, " +
				"STATUS, PAYMENT_DATA_FILE) VALUES ( ?,?,?,?" + 
				",  TO_Date( '09/01/2010 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 'S', " +" empty_clob())";
		LOG.info("Insert payment file sql = " + sqlInsert.toString());
		
		String sqlSelect = "select payment_data_file from payment_file_upload where payment_file_upload_oid = ? for update";
		String sqlUpdate = "update payment_file_upload set payment_data_file = ? where payment_file_upload_oid = ?";

      try{
			Connection con = DatabaseQueryBean.connect(false);
			PreparedStatement pStmt = con.prepareStatement(sqlInsert);
			pStmt = con.prepareStatement(sqlUpdate);
			pStmt.setString(1, transactionOid);
			pStmt.setString(2, transactionOid);
			pStmt.setString(3, ownerOrgOid);
			pStmt.setString(4, userOid);
			int rowCount = pStmt.executeUpdate();
			
			pStmt = con.prepareStatement(sqlSelect);
			pStmt.setString(1, transactionOid);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				Clob paymentFileClob = rs.getClob(1);
				paymentFileClob.setString(1, paymentFile.toString());
				pStmt = con.prepareStatement(sqlUpdate);
				pStmt.setClob(1, paymentFileClob);
				pStmt.setString(2, transactionOid);
				rowCount = pStmt.executeUpdate();
			}
			
			
			rs.close();
			pStmt.close();
			con.close();
			//DatabaseQueryBean.disconnect();

		} catch (AmsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

   }
   protected static StringBuffer readPaymentFile(String fileName)
   { 
	   StringBuffer sb = new StringBuffer();
	   BufferedReader reader = null; 

       try {
    	   File file = new File(fileName);
    	   //File file = new File("100 CBFT Records Payment File - Updated.TXT");
    	  // File file = new File("Mid Heavy 110 -NEW txt.txt");
    	   reader = new BufferedReader(new FileReader(file));   
    	   String text = null; 
              
    	   // repeat until all lines is read      
    	   while ((text = reader.readLine()) != null)           
    	   {                
    	       sb.append(text).append("&&");
    	   }

       } catch (IOException e) {
    	   e.printStackTrace(); 
       }finally {
          try  {               
              if (reader != null) {                    
               reader.close();              
              }           
       } catch (IOException e)            
       {        
           e.printStackTrace();            
       } 
      
       } 
   
       // show file contents here 
       LOG.info(sb.toString()); 
	   return sb;

   }
}
