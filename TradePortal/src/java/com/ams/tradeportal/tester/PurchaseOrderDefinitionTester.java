
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the PurchaseOrderDefinition Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderDefinitionTester
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderDefinitionTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      PurchaseOrderDefinition testObject = null;

      /* Test the basic functionaility of the PurchaseOrderDefinition object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class PurchaseOrderDefinition");
         testObject = (PurchaseOrderDefinition)EJBObjectFactory.createClientEJB("t3://localhost:7001","PurchaseOrderDefinition");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute name to 'TEST'");
         testObject.setAttribute("name", "TEST");
   
         LOG.info("Tester: Setting attribute description to 'TEST'");
         testObject.setAttribute("description", "TEST");
   
         LOG.info("Tester: Setting attribute delimiter_char to 'O'");
         testObject.setAttribute("delimiter_char", "O");
   
         LOG.info("Tester: Setting attribute date_format to 'O'");
         testObject.setAttribute("date_format", "O");
   
         LOG.info("Tester: Setting attribute default_flag to 'Y'");
         testObject.setAttribute("default_flag", "Y");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute incoterm_req to 'Y'");
         testObject.setAttribute("incoterm_req", "Y");
   
         LOG.info("Tester: Setting attribute incoterm_loc_req to 'Y'");
         testObject.setAttribute("incoterm_loc_req", "Y");
   
         LOG.info("Tester: Setting attribute partial_ship_allowed_req to 'Y'");
         testObject.setAttribute("partial_ship_allowed_req", "Y");
   
         LOG.info("Tester: Setting attribute goods_desc_req to 'Y'");
         testObject.setAttribute("goods_desc_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def1_label to 'TEST'");
         testObject.setAttribute("buyer_users_def1_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def2_label to 'TEST'");
         testObject.setAttribute("buyer_users_def2_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def3_label to 'TEST'");
         testObject.setAttribute("buyer_users_def3_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def4_label to 'TEST'");
         testObject.setAttribute("buyer_users_def4_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def5_label to 'TEST'");
         testObject.setAttribute("buyer_users_def5_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def6_label to 'TEST'");
         testObject.setAttribute("buyer_users_def6_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def7_label to 'TEST'");
         testObject.setAttribute("buyer_users_def7_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def8_label to 'TEST'");
         testObject.setAttribute("buyer_users_def8_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def9_label to 'TEST'");
         testObject.setAttribute("buyer_users_def9_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def10_label to 'TEST'");
         testObject.setAttribute("buyer_users_def10_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def1_label to 'TEST'");
         testObject.setAttribute("seller_users_def1_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def2_label to 'TEST'");
         testObject.setAttribute("seller_users_def2_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def3_label to 'TEST'");
         testObject.setAttribute("seller_users_def3_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def4_label to 'TEST'");
         testObject.setAttribute("seller_users_def4_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def5_label to 'TEST'");
         testObject.setAttribute("seller_users_def5_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def6_label to 'TEST'");
         testObject.setAttribute("seller_users_def6_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def7_label to 'TEST'");
         testObject.setAttribute("seller_users_def7_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def8_label to 'TEST'");
         testObject.setAttribute("seller_users_def8_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def9_label to 'TEST'");
         testObject.setAttribute("seller_users_def9_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_users_def10_label to 'TEST'");
         testObject.setAttribute("seller_users_def10_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_users_def1_req to 'Y'");
         testObject.setAttribute("buyer_users_def1_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def2_req to 'Y'");
         testObject.setAttribute("buyer_users_def2_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def3_req to 'Y'");
         testObject.setAttribute("buyer_users_def3_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def4_req to 'Y'");
         testObject.setAttribute("buyer_users_def4_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def5_req to 'Y'");
         testObject.setAttribute("buyer_users_def5_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def6_req to 'Y'");
         testObject.setAttribute("buyer_users_def6_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def7_req to 'Y'");
         testObject.setAttribute("buyer_users_def7_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def8_req to 'Y'");
         testObject.setAttribute("buyer_users_def8_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def9_req to 'Y'");
         testObject.setAttribute("buyer_users_def9_req", "Y");
   
         LOG.info("Tester: Setting attribute buyer_users_def10_req to 'Y'");
         testObject.setAttribute("buyer_users_def10_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def1_req to 'Y'");
         testObject.setAttribute("seller_users_def1_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def2_req to 'Y'");
         testObject.setAttribute("seller_users_def2_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def3_req to 'Y'");
         testObject.setAttribute("seller_users_def3_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def4_req to 'Y'");
         testObject.setAttribute("seller_users_def4_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def5_req to 'Y'");
         testObject.setAttribute("seller_users_def5_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def6_req to 'Y'");
         testObject.setAttribute("seller_users_def6_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def7_req to 'Y'");
         testObject.setAttribute("seller_users_def7_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def8_req to 'Y'");
         testObject.setAttribute("seller_users_def8_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def9_req to 'Y'");
         testObject.setAttribute("seller_users_def9_req", "Y");
   
         LOG.info("Tester: Setting attribute seller_users_def10_req to 'Y'");
         testObject.setAttribute("seller_users_def10_req", "Y");
   
         LOG.info("Tester: Setting attribute line_item_detail_provided to 'Y'");
         testObject.setAttribute("line_item_detail_provided", "Y");
   
         LOG.info("Tester: Setting attribute line_item_num_req to 'Y'");
         testObject.setAttribute("line_item_num_req", "Y");
   
         LOG.info("Tester: Setting attribute unit_price_req to 'Y'");
         testObject.setAttribute("unit_price_req", "Y");
   
         LOG.info("Tester: Setting attribute unit_of_measure_req to 'Y'");
         testObject.setAttribute("unit_of_measure_req", "Y");
   
         LOG.info("Tester: Setting attribute quantity_req to 'Y'");
         testObject.setAttribute("quantity_req", "Y");
   
         LOG.info("Tester: Setting attribute quantity_var_plus_req to 'Y'");
         testObject.setAttribute("quantity_var_plus_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud1_req to 'Y'");
         testObject.setAttribute("prod_chars_ud1_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud2_req to 'Y'");
         testObject.setAttribute("prod_chars_ud2_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud3_req to 'Y'");
         testObject.setAttribute("prod_chars_ud3_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud4_req to 'Y'");
         testObject.setAttribute("prod_chars_ud4_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud5_req to 'Y'");
         testObject.setAttribute("prod_chars_ud5_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud6_req to 'Y'");
         testObject.setAttribute("prod_chars_ud6_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud7_req to 'Y'");
         testObject.setAttribute("prod_chars_ud7_req", "Y");
   
         LOG.info("Tester: Setting attribute prod_chars_ud1_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud1_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud2_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud2_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud3_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud3_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud4_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud4_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud5_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud5_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud6_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud6_label", "TEST");
   
         LOG.info("Tester: Setting attribute prod_chars_ud7_label to 'TEST'");
         testObject.setAttribute("prod_chars_ud7_label", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field1 to 'TEST'");
         testObject.setAttribute("po_data_field1", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field2 to 'TEST'");
         testObject.setAttribute("po_data_field2", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field3 to 'TEST'");
         testObject.setAttribute("po_data_field3", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field4 to 'TEST'");
         testObject.setAttribute("po_data_field4", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field5 to 'TEST'");
         testObject.setAttribute("po_data_field5", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field6 to 'TEST'");
         testObject.setAttribute("po_data_field6", "TEST");
   
         LOG.info("Tester: Setting attribute po_data_field7 to 'TEST'");
         testObject.setAttribute("po_data_field7", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field1 to 'TEST'");
         testObject.setAttribute("po_line_item_field1", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field2 to 'TEST'");
         testObject.setAttribute("po_line_item_field2", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field3 to 'TEST'");
         testObject.setAttribute("po_line_item_field3", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field4 to 'TEST'");
         testObject.setAttribute("po_line_item_field4", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field5 to 'TEST'");
         testObject.setAttribute("po_line_item_field5", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field6 to 'TEST'");
         testObject.setAttribute("po_line_item_field6", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field7 to 'TEST'");
         testObject.setAttribute("po_line_item_field7", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field8 to 'TEST'");
         testObject.setAttribute("po_line_item_field8", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field9 to 'TEST'");
         testObject.setAttribute("po_line_item_field9", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field10 to 'TEST'");
         testObject.setAttribute("po_line_item_field10", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field11 to 'TEST'");
         testObject.setAttribute("po_line_item_field11", "TEST");
   
         LOG.info("Tester: Setting attribute po_line_item_field12 to 'TEST'");
         testObject.setAttribute("po_line_item_field12", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field1 to 'TEST'");
         testObject.setAttribute("po_summary_field1", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field2 to 'TEST'");
         testObject.setAttribute("po_summary_field2", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field3 to 'TEST'");
         testObject.setAttribute("po_summary_field3", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field4 to 'TEST'");
         testObject.setAttribute("po_summary_field4", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field5 to 'TEST'");
         testObject.setAttribute("po_summary_field5", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field6 to 'TEST'");
         testObject.setAttribute("po_summary_field6", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field7 to 'TEST'");
         testObject.setAttribute("po_summary_field7", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field8 to 'TEST'");
         testObject.setAttribute("po_summary_field8", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field9 to 'TEST'");
         testObject.setAttribute("po_summary_field9", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field10 to 'TEST'");
         testObject.setAttribute("po_summary_field10", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field11 to 'TEST'");
         testObject.setAttribute("po_summary_field11", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field12 to 'TEST'");
         testObject.setAttribute("po_summary_field12", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field13 to 'TEST'");
         testObject.setAttribute("po_summary_field13", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field14 to 'TEST'");
         testObject.setAttribute("po_summary_field14", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field15 to 'TEST'");
         testObject.setAttribute("po_summary_field15", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field16 to 'TEST'");
         testObject.setAttribute("po_summary_field16", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field17 to 'TEST'");
         testObject.setAttribute("po_summary_field17", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field18 to 'TEST'");
         testObject.setAttribute("po_summary_field18", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field19 to 'TEST'");
         testObject.setAttribute("po_summary_field19", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field20 to 'TEST'");
         testObject.setAttribute("po_summary_field20", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field21 to 'TEST'");
         testObject.setAttribute("po_summary_field21", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field22 to 'TEST'");
         testObject.setAttribute("po_summary_field22", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field23 to 'TEST'");
         testObject.setAttribute("po_summary_field23", "TEST");
   
         LOG.info("Tester: Setting attribute po_summary_field24 to 'TEST'");
         testObject.setAttribute("po_summary_field24", "TEST");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
