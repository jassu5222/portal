
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the PurchaseOrderFileUpload Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderFileUploadTester
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderFileUploadTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      PurchaseOrderFileUpload testObject = null;

      /* Test the basic functionaility of the PurchaseOrderFileUpload object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class PurchaseOrderFileUpload");
         testObject = (PurchaseOrderFileUpload)EJBObjectFactory.createClientEJB("t3://localhost:7001","PurchaseOrderFileUpload");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute po_file_name to 'TEST'");
         testObject.setAttribute("po_file_name", "TEST");
   
         LOG.info("Tester: Setting attribute validation_status to 'O'");
         testObject.setAttribute("validation_status", "O");
   
         LOG.info("Tester: Setting attribute number_of_po_uploaded to '7'");
         testObject.setAttribute("number_of_po_uploaded", "7");
   
         LOG.info("Tester: Setting attribute number_of_po_failed to '7'");
         testObject.setAttribute("number_of_po_failed", "7");
   
         LOG.info("Tester: Setting attribute creation_timestamp to '02/28/2000 12:13:54'");
         testObject.setAttribute("creation_timestamp", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute completion_timestamp to '02/28/2000 12:13:54'");
         testObject.setAttribute("completion_timestamp", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute agent_id to 'TEST'");
         testObject.setAttribute("agent_id", "TEST");
   
         LOG.info("Tester: Setting attribute deleted_ind to 'Y'");
         testObject.setAttribute("deleted_ind", "Y");
   
         LOG.info("Tester: Setting attribute error_msg to 'TEST'");
         testObject.setAttribute("error_msg", "TEST");
   
         LOG.info("Tester: Setting attribute validation_seconds to '7'");
         testObject.setAttribute("validation_seconds", "7");
   
         LOG.info("Tester: Setting attribute currency to 'TEST'");
         testObject.setAttribute("currency", "TEST");
   
         LOG.info("Tester: Setting attribute amount to '5.2'");
         testObject.setAttribute("amount", "5.2");
   
         LOG.info("Tester: Setting attribute po_upload_parameters to 'TEST'");
         testObject.setAttribute("po_upload_parameters", "TEST");
   
         LOG.info("Tester: Setting attribute a_upload_definition_oid to '7'");
         testObject.setAttribute("a_upload_definition_oid", "7");
   
         LOG.info("Tester: Setting attribute po_result_parameters to 'TEST'");
         testObject.setAttribute("po_result_parameters", "TEST");
   
         LOG.info("Tester: Setting association user_oid to ''");
         testObject.setAttribute("user_oid", "");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
