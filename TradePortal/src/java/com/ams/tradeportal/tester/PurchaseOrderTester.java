
  

package com.ams.tradeportal.tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;
import java.rmi.*;
import java.util.*;
import java.math.*;
import javax.ejb.*;
import com.ams.tradeportal.common.*;
import com.ams.tradeportal.busobj.*;


/*
 * Client application to test the PurchaseOrder Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PurchaseOrderTester
{
private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderTester.class);

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      LOG.info("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         LOG.info("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      LOG.info("Tester: End of Test");
      LOG.info("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      PurchaseOrder testObject = null;

      /* Test the basic functionaility of the PurchaseOrder object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         LOG.info("Tester: Creating instance of class PurchaseOrder");
         testObject = (PurchaseOrder)EJBObjectFactory.createClientEJB("t3://localhost:7001","PurchaseOrder");
         LOG.info("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */
         LOG.info("Tester: Setting attribute purchase_order_num to 'TEST'");
         testObject.setAttribute("purchase_order_num", "TEST");
   
         LOG.info("Tester: Setting attribute purchase_order_type to 'TEST'");
         testObject.setAttribute("purchase_order_type", "TEST");
   
         LOG.info("Tester: Setting attribute opt_lock to 'TEST'");
         testObject.setAttribute("opt_lock", "TEST");
   
         LOG.info("Tester: Setting attribute issue_date to '02/28/2000'");
         testObject.setAttribute("issue_date", "02/28/2000");
   
         LOG.info("Tester: Setting attribute latest_shipment_date to '02/28/2000 12:13:54'");
         testObject.setAttribute("latest_shipment_date", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute currency to 'TEST'");
         testObject.setAttribute("currency", "TEST");
   
         LOG.info("Tester: Setting attribute amount to '5.2'");
         testObject.setAttribute("amount", "5.2");
   
         LOG.info("Tester: Setting attribute seller_name to 'TEST'");
         testObject.setAttribute("seller_name", "TEST");
   
         LOG.info("Tester: Setting attribute incoterm to 'O'");
         testObject.setAttribute("incoterm", "O");
   
         LOG.info("Tester: Setting attribute incoterm_location to 'TEST'");
         testObject.setAttribute("incoterm_location", "TEST");
   
         LOG.info("Tester: Setting attribute partial_shipment_allowed to 'Y'");
         testObject.setAttribute("partial_shipment_allowed", "Y");
   
         LOG.info("Tester: Setting attribute buyer_user_def1_label to 'TEST'");
         testObject.setAttribute("buyer_user_def1_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def2_label to 'TEST'");
         testObject.setAttribute("buyer_user_def2_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def3_label to 'TEST'");
         testObject.setAttribute("buyer_user_def3_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def4_label to 'TEST'");
         testObject.setAttribute("buyer_user_def4_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def5_label to 'TEST'");
         testObject.setAttribute("buyer_user_def5_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def6_label to 'TEST'");
         testObject.setAttribute("buyer_user_def6_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def7_label to 'TEST'");
         testObject.setAttribute("buyer_user_def7_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def8_label to 'TEST'");
         testObject.setAttribute("buyer_user_def8_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def9_label to 'TEST'");
         testObject.setAttribute("buyer_user_def9_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def10_label to 'TEST'");
         testObject.setAttribute("buyer_user_def10_label", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def1_value to 'TEST'");
         testObject.setAttribute("buyer_user_def1_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def2_value to 'TEST'");
         testObject.setAttribute("buyer_user_def2_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def3_value to 'TEST'");
         testObject.setAttribute("buyer_user_def3_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def4_value to 'TEST'");
         testObject.setAttribute("buyer_user_def4_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def5_value to 'TEST'");
         testObject.setAttribute("buyer_user_def5_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def6_value to 'TEST'");
         testObject.setAttribute("buyer_user_def6_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def7_value to 'TEST'");
         testObject.setAttribute("buyer_user_def7_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def8_value to 'TEST'");
         testObject.setAttribute("buyer_user_def8_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def9_value to 'TEST'");
         testObject.setAttribute("buyer_user_def9_value", "TEST");
   
         LOG.info("Tester: Setting attribute buyer_user_def10_value to 'TEST'");
         testObject.setAttribute("buyer_user_def10_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def1_label to 'TEST'");
         testObject.setAttribute("seller_user_def1_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def2_label to 'TEST'");
         testObject.setAttribute("seller_user_def2_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def3_label to 'TEST'");
         testObject.setAttribute("seller_user_def3_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def4_label to 'TEST'");
         testObject.setAttribute("seller_user_def4_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def5_label to 'TEST'");
         testObject.setAttribute("seller_user_def5_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def6_label to 'TEST'");
         testObject.setAttribute("seller_user_def6_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def7_label to 'TEST'");
         testObject.setAttribute("seller_user_def7_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def8_label to 'TEST'");
         testObject.setAttribute("seller_user_def8_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def9_label to 'TEST'");
         testObject.setAttribute("seller_user_def9_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def10_label to 'TEST'");
         testObject.setAttribute("seller_user_def10_label", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def1_value to 'TEST'");
         testObject.setAttribute("seller_user_def1_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def2_value to 'TEST'");
         testObject.setAttribute("seller_user_def2_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def3_value to 'TEST'");
         testObject.setAttribute("seller_user_def3_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def4_value to 'TEST'");
         testObject.setAttribute("seller_user_def4_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def5_value to 'TEST'");
         testObject.setAttribute("seller_user_def5_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def6_value to 'TEST'");
         testObject.setAttribute("seller_user_def6_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def7_value to 'TEST'");
         testObject.setAttribute("seller_user_def7_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def8_value to 'TEST'");
         testObject.setAttribute("seller_user_def8_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def9_value to 'TEST'");
         testObject.setAttribute("seller_user_def9_value", "TEST");
   
         LOG.info("Tester: Setting attribute seller_user_def10_value to 'TEST'");
         testObject.setAttribute("seller_user_def10_value", "TEST");
   
         LOG.info("Tester: Setting attribute goods_description to 'TEST'");
         testObject.setAttribute("goods_description", "TEST");
   
         LOG.info("Tester: Setting attribute status to 'O'");
         testObject.setAttribute("status", "O");
   
         LOG.info("Tester: Setting attribute creation_date_time to '02/28/2000 12:13:54'");
         testObject.setAttribute("creation_date_time", "02/28/2000 12:13:54");
   
         LOG.info("Tester: Setting attribute deleted_ind to 'Y'");
         testObject.setAttribute("deleted_ind", "Y");
   
         LOG.info("Tester: Setting attribute amend_seq_no to '7'");
         testObject.setAttribute("amend_seq_no", "7");
   
         LOG.info("Tester: Setting association transaction_oid to ''");
         testObject.setAttribute("transaction_oid", "");
   
         LOG.info("Tester: Setting association owner_org_oid to ''");
         testObject.setAttribute("owner_org_oid", "");
   
         LOG.info("Tester: Setting association upload_definition_oid to ''");
         testObject.setAttribute("upload_definition_oid", "");
   
         LOG.info("Tester: Setting association instrument_oid to ''");
         testObject.setAttribute("instrument_oid", "");
   
         LOG.info("Tester: Setting association shipment_oid to ''");
         testObject.setAttribute("shipment_oid", "");
   

         /* Save the object to the database */
         LOG.info("Tester: Calling save() method");
         testObject.save();
         LOG.info("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         List<IssuedError> errors = testObject.getIssuedErrors();
         for(IssuedError errorObject : errors) 
         {
            
            LOG.info(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
