<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.webbean.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<%


  //cquinton 2/28/2013 ir#14364 if there is no active session, we can't redirect the
  // user to the home page.  instead display the navigation error message, with 
  // same general content, but tell the user to login again instead.
  // becuase user is not logged in and branding has been lost,
  // do not display the header or anything that requires branding
  boolean activeSession = false;
  HttpSession theSession = request.getSession(false);
  SessionWebBean mySession = (SessionWebBean)theSession.getAttribute("userSession");
  if ( mySession!=null ) {
    String activeSessionOid = mySession.getActiveUserSessionOid();
    if ( activeSessionOid!=null  ) {
      activeSession = true;
    }
  }
  if ( activeSession ) {
    // This will cause this page to be display for 5 seconds
    // and then automatically forwarded to the page that the user last came from
    // Note that we write the refresh header here before writing the page content!
    // setting the header after page content makes redirect not work in some browsers
    formMgr.setCurrPage("TradePortalHome");
    String nextPhysicalPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request);
    response.setHeader("Refresh", "5; URL=".concat(request.getContextPath()+nextPhysicalPage));
%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeTimeoutForward" value="false" />
</jsp:include>

<%
  } //end if ( activeSession )
  else {
%>
<!DOCTYPE html>
<html>
<body>
<%
  }
%>

<div class="pageContent">

<%
  if ( activeSession ) {
%>
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="AutoReroute.YouAreBeingRedirected"/>
  </jsp:include>
<%
  }
  else {
%>
  <jsp:include page="/common/PageHeader.jsp">
    <jsp:param name="titleKey" value="AutoReroute.NavigationError"/>
  </jsp:include>
<%
  }
%>

  <div class="formContentNoSidebar padded">
 
    <div class="formItem">
  
      <br/>
      <div class="instruction formItem"><%= resMgr.getText("AutoReroute.TextPart1", TradePortalConstants.TEXT_BUNDLE) %></div>
      <div class="instruction formItem"><%= resMgr.getText("AutoReroute.TextPart2", TradePortalConstants.TEXT_BUNDLE) %></div>
<%
  if ( activeSession) {
%>
      <div class="instruction formItem"><%= resMgr.getText("AutoReroute.TextPart3", TradePortalConstants.TEXT_BUNDLE) %> <%= formMgr.getLinkAsHref("goToTradePortalHome", resMgr.getText("AutoReroute.TextPart4", TradePortalConstants.TEXT_BUNDLE), response) %></div>
<%
  } else {
%>
      <br/>
      <div class="instruction formItem"><%= resMgr.getText("AutoReroute.LoginAgain", TradePortalConstants.TEXT_BUNDLE) %></div>
<%
  }
%>
  
    </div>

  </div>

</div>


</body>
</html>
