<%--
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*" %>

<%
   // Main entry point to the Trade Portal when the user is authenticating
   // themselves using a certificate.   Basically, it forwards the user on
   // to TradePortalLogon.jsp, which is the central entry point to the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that certificates are being used

   // There must be separate entry points for certificates and passwords so 
   // that Apache web server can be configured to require a certificate for the
   // certificates page and not require one for the password page.
   // IIS and Weblogic does not support this, so instead, users should be directed
   // to go to different ports (one with SSL, one without) depending on the
   // authentication method

   // Get the physical URL of the central logon page
   String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   String organization = request.getParameter("organization");
   String branding     = request.getParameter("branding");
   String locale       = request.getParameter("locale");
   String certData     = request.getParameter("identification");
   String date         = request.getParameter("time");
%>

<%
   // Clear out any previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   session.invalidate();
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
     formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

  <jsp:forward page='<%= url %>'>
       <jsp:param name="auth"         value="<%= TradePortalConstants.AUTH_CERTIFICATE %>" />
       <jsp:param name="organization" value="<%= organization %>" />
       <jsp:param name="branding"     value="<%= branding %>" />
       <jsp:param name="locale"       value="<%= locale %>" />
       <jsp:param name="info"         value="<%= certData %>" />
       <jsp:param name="date"         value="<%= date %>" />
  </jsp:forward>
