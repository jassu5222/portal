<%--
 * This JSP submits a form containing two encrypted values to a URL specified in a
 * configuration file.
 *
 *     Copyright   2003
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.busobj.webbean.*, java.security.*, com.ams.tradeportal.busobj.util.*, com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<%-- CR-433 Krishna Begin 05/25/2008 --%>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
<%-- CR-433 Krishna End 05/25/2008 --%>

<html>
 <head>
  <%
    try
     {
      // Set up the web beans to retrieve data for this user's org and their user record
      UserWebBean user = beanMgr.createBean(UserWebBean.class, "User");

      CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
      ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
      clientBank.getById(userSession.getClientBankOid());                   	       

      user.getById(userSession.getUserOid());
      org.getById(user.getAttribute("owner_org_oid"));

      // Get values for the parameters
      String username = user.getAttribute("doc_prep_username");
      if (username==null) username = "";
      String accountName = org.getAttribute("doc_prep_accountname");
      if (accountName==null) accountName = "";
      String domainName = org.getAttribute("doc_prep_domainname");
      if (domainName==null) domainName = "";
      String userOid = userSession.getUserOid();
      String currentGMTTime = DateTimeUtility.getGMTDateTime(true, false); //MM/dd/yyyy HH:mm:ss, ignore override date to ensure testing with TradeBeam

 // DFEIGE - 04/18/07 - IR#DGUH041857162 - Begin - Remove line '.append("<ppxuserid>").append(userOid).append("</ppxuserid>")' from plainTextMessage stringBuffer.
      // Construct the plain text message to be signed
      // StringBuffer stringBuffer = new StringBuffer();
      // stringBuffer.append("<sso_data><event>GetDashBoard</event>")
                 // .append("<userid>").append(username).append("</userid>")
                 // .append("<companyid>").append(accountName).append("</companyid>")
                 // .append("<domain>").append(domainName).append("</domain>")
                 // .append("<timestamp>").append(currentGMTTime).append(" GMT</timestamp>")
                 // .append("<ppxuserid>").append(userOid).append("</ppxuserid>")
                 // .append("</sso_data>");
      // String plainTextMessage = stringBuffer.toString();

      // Construct the plain text message to be signed
	   StringBuffer stringBuffer = new StringBuffer();
	   stringBuffer.append("<sso_data><event>GetDashBoard</event>")
	               .append("<userid>").append(username).append("</userid>")
	               .append("<companyid>").append(accountName).append("</companyid>")
	               .append("<domain>").append(domainName).append("</domain>")
	               .append("<timestamp>").append(currentGMTTime).append(" GMT</timestamp>")
	               .append("</sso_data>");
      String plainTextMessage = stringBuffer.toString();

// DFEIGE - 04/18/07 - IR#DGUH041857162 - End - Remove line '.append("<ppxuserid>").append(userOid).append("</ppxuserid>")' from plainTextMessage stringBuffer.
      //CR-433 Krishna 05/13/2008 Begin
      String physicalPage = null;
      //Retrieve Corp ServicesURL configured by the Corporate customer
      String corpServicesURL = org.getAttribute("doc_prep_url");
      //Incase Corp ServicesURL is not configured by the Corporate customer then retrieve it from the Customer's Bank
      if (InstrumentServices.isBlank(corpServicesURL)) 
      corpServicesURL = clientBank.getAttribute("doc_prep_url");
	  //If Corp ServicesURL is configured Neigther  by the Corporate customer nor by Customer's Bank
          // then throw an error to the user
          if (InstrumentServices.isBlank(corpServicesURL)) 
	  {   
           MediatorServices mediatorServices = new MediatorServices();
           mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
           mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.CORP_SERVICES_URL_NOT_ESTD);
           mediatorServices.addErrorInfo();
           DocumentHandler defaultDoc = new DocumentHandler();
           defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	   defaultDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
           formMgr.storeInDocCache("default.doc", defaultDoc); 
		   physicalPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request);
		%>
		<jsp:forward page='<%= physicalPage %>'/>
		<%
         }
	  //Commented out the following code in order not to read URL from TradePortal.properties file
         // Get the URL from TradePortal.properties
         /*
	  PropertyResourceBundle portalProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle ("TradePortal");
          String url = portalProperties.getString("docPrepUrl");
         */
      //CR-433 Krishna 05/13/2008 End
      // Get the portal private key
      PrivateKey portalPrivateKey = StoredCertificateData.getPortalPrivateKeyFromFile();

      // Encrypt the username and GlobalTrade org ID
      String signedMessage;
      if(!plainTextMessage.equals(""))
         signedMessage = EncryptDecrypt.createDigitialSignature(plainTextMessage, portalPrivateKey, false);
      else
         signedMessage = "";

      // Sample code to validate signature
	  //boolean validSignature = EncryptDecrypt.verifyDigitalSignature(plainTextMessage, signedMessage, StoredCertificateData.getPortalPublicKeyFromFile());
  %>

 </head>
 <% // Set up the HTML so that it will submit the form when it is loaded. %>
 <body onload="document.forms[0].submit()">

    <% // Submit the form to the URL specified in the file using the POST method 
       //CR-433 Krishna 05/13/2008 instead of the variable 'url', corpServicesURL is being used.
    %>
    <form method="POST" action="<%= corpServicesURL %>" >
          <% // Pass along the values as username and organization parameters %>
          <input type="hidden" name="singleSignOnData"     value="<%=plainTextMessage%>" />
          <input type="hidden" name="singleSignOnSignature"     value="<%=signedMessage%>" />
          <input type="hidden" name="singleSignOnSource" value="TradePortal" />
    </form>
 </body>
  <%
     }
    catch(Exception e)
     {
        out.print("There was a problem launching the doc prep application");
        e.printStackTrace();
     }
  %>
</html>