<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, com.amsinc.ecsg.util.*" %>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>
<div Class="pageContent">
<div class="secondaryNav">
                    <jsp:include page="/common/PageHeader.jsp">
                    <jsp:param name="titleKey" value="ExceptionPage.Heading"/>
                     </jsp:include>
</div>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>

<div Class="formContentNoSidebar">
<br>&nbsp;<br>
		<div Class="formItem">
       <%= resMgr.getText("ExceptionPage.SystemEncounteredError", TradePortalConstants.TEXT_BUNDLE) %><br>&nbsp;<br>
       <%= resMgr.getText("ExceptionPage.Click", TradePortalConstants.TEXT_BUNDLE) %>
	  <%= formMgr.getLinkAsHref("goToTradePortalHome", resMgr.getText("ExceptionPage.Here", TradePortalConstants.TEXT_BUNDLE), response) %>
        <%= resMgr.getText("ExceptionPage.ToGoToHomePage", TradePortalConstants.TEXT_BUNDLE) %></div>
 
<%
Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");

if(throwable != null)
	throwable.printStackTrace();
%>
<br>&nbsp;<br>
</div>
  </div>
</div>


<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
