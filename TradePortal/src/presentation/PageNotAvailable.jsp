<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.html.*,com.amsinc.ecsg.frame.*,
        com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*,
	  com.ams.tradeportal.common.*" %>

<%
 /* This page is displayed whenever navigation manager cannot resolve the
     page that the user wants to go to.   If the user bookmarks a page, logs out,
     and then attempts to access the bookmark, this page will be displayed. */
     //Ravindra - 21/01/2011 - CR-541 - Start
     //Ravindra - 03/31/2011 - CUUL032965287 - Start
    /* DocumentHandler              logoutDoc                = new DocumentHandler();
   DocumentHandler              xmlDoc                   = formMgr.getFromDocCache();
   if(xmlDoc!=null){
	     String authMethod = xmlDoc.getAttribute("/In/auth");
		 if (TradePortalConstants.AUTH_AES256.equals(authMethod)){
		 	String orgId               = request.getParameter("organization");
		 	String user=xmlDoc.getAttribute("/In/User/ssoId");
		     // 601 is CMA specific status code to notate "Trade Portal times out before the client bank portal"
			   response.setStatus(HttpResponseUtility.getResponseCode( orgId, user, HttpResponseUtility.SESSION_TIMEOUT ));
		   }
	}*/
	   //Ravindra - 03/31/2011 - CUUL032965287 - End
	 //Ravindra - 21/01/2011 - CR-541 - End
 %>


<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
    formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>


<HTML>
<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
<br>&nbsp;&nbsp;
<FONT FACE="Arial" SIZE="2">You must first log in to access the application.   This message will appear if 
your session has timed out or if you attempt to access the application through a bookmarked link.
</FONT>
</HTML>