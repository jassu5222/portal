<%--
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*" %>

<%
   // Main entry point to the Trade Portal when the user is authenticating
   // themselves using a password.   Basically, it forwards the user on
   // to TradePortalLogon.jsp, which is the central entry point to the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that a password is being used

   // There must be separate entry points for certificates and passwords so 
   // that Apache web server can be configured to require a certificate for the
   // certificates page and not require one for the password page.
   // IIS and Weblogic does not support this, so instead, users should be directed
   // to go to different ports (one with SSL, one without) depending on the
   // authentication method

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   // [START] JOUK031033813 - Fishnet audit XSS and blind SQL injection
   //String organization = request.getParameter("organization");
   //String branding     = request.getParameter("branding");
   //String locale       = request.getParameter("locale");
   String organization = StringFunction.xssCharsToHtml(request.getParameter("organization"));
   String branding     = StringFunction.xssCharsToHtml(request.getParameter("branding"));
   String locale       = StringFunction.xssCharsToHtml(request.getParameter("locale"));

   // Check the whitelist for each parameter
   //boolean passOrgWhitelistCheck = false;
   boolean passBrandingWhitelistCheck = false;
   boolean passLocaleWhitelistCheck = false;
   StringBuffer sqlQuery = new StringBuffer();
   Vector queryResults = null;
   
   // organization whitelist
   //if((organization != null)
   //	&& (organization.trim().length() > 0))
   //{
   //	sqlQuery.setLength(0);
   //	sqlQuery.append("select distinct(otl_id) as ORGANIZATION_WHITELIST from client_bank");
   //	queryResults = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false)
   //				.getFragments("/ResultSetRecord");
   //	for(int i=0; i<queryResults.size();i++) {
   //		if(organization.equalsIgnoreCase(
   //			((DocumentHandler)queryResults.elementAt(i)).getAttribute("/ORGANIZATION_WHITELIST")))
   //		{
   //			Debug.debug("[PasswordLogon.jsp] "
   //				+"Found whitelist match for organization("+organization+") after "+(i+1)+" of "+queryResults.size()+" possible tries");
	//			passOrgWhitelistCheck = true;
	//			break;
   //		}
   //		Debug.debug("[PasswordLogon.jsp] "
   //			+"organization whitelist check try #"+(i+1));
   //	}
   //}
   //else
   //{
   //	passOrgWhitelistCheck = true;
   //}

	// Log an error to the log if the organization parameter is not on the whitelist
   //if(passOrgWhitelistCheck == false)
   //{
   //	System.err.println("[PasswordLogon.jsp] "
   //		+"!!!ERROR!!! "
   //		+"Invalid organization parameter ("+organization+") passed from "
   //		+"Remote Address = "+request.getRemoteAddr()+"; "
   //		+"Remote Host = "+request.getRemoteHost());
   //}

   // branding whitelist
   if((branding != null)
   	&& (branding.trim().length() > 0))
   {
   	sqlQuery.setLength(0);
   	sqlQuery.append("select distinct(branding_directory) as BRANDING_WHITELIST ");
   	sqlQuery.append("from (select branding_directory from bank_organization_group ");
   	sqlQuery.append("union select branding_directory from client_bank)");
   	
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    Object sqlParams[] = null ;
   	queryResults = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams)
   				.getFragments("/ResultSetRecord");
   	for(int i=0; i<queryResults.size();i++) {
   		if(branding.equalsIgnoreCase(
   			((DocumentHandler)queryResults.elementAt(i)).getAttribute("/BRANDING_WHITELIST")))
   		{
   			Debug.debug("[PasswordLogon.jsp] "
   				+"Found whitelist match for branding("+branding+") after "+(i+1)+" of "+queryResults.size()+" possible tries");
				passBrandingWhitelistCheck = true;
				break;
   		}
   		Debug.debug("[PasswordLogon.jsp] "
   			+"branding whitelist check try #"+(i+1));
   	}
   }
   else
   {
   	passBrandingWhitelistCheck = true;
   }
   
	// Log an error to the log if the branding parameter is not on the whitelist
   if(passBrandingWhitelistCheck == false)
   {
   	System.err.println("[PasswordLogon.jsp] "
   		+"!!!ERROR!!! "
   		+"Invalid branding parameter ("+branding+") passed from "
   		+"Remote Address = "+request.getRemoteAddr()+"; "
   		+"Remote Host = "+request.getRemoteHost());
   }
   
   // locale whitelist
   if((locale != null)
   	&& (locale.trim().length() >0))
   {
   	sqlQuery.setLength(0);
   	sqlQuery.append("select distinct(locale) as LOCALE_WHITELIST from users");
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    Object sqlParams[] = null ;
   	queryResults = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams)
   				.getFragments("/ResultSetRecord");
   	for(int i=0; i<queryResults.size();i++) {
   		if(locale.equalsIgnoreCase(
   			((DocumentHandler)queryResults.elementAt(i)).getAttribute("/LOCALE_WHITELIST")))
   		{
   			Debug.debug("[PasswordLogon.jsp] "
   				+"Found whitelist match for locale("+locale+") after "+(i+1)+" of "+queryResults.size()+" possible tries");
				passLocaleWhitelistCheck = true;
				break;
   		}
   		Debug.debug("[PasswordLogon.jsp] "
   			+"locale whitelist check try #"+(i+1));
   	}
   }
   else
   {
   	passLocaleWhitelistCheck = true;
   }
   
	// Log an error to the log if the locale parameter is not on the whitelist
   if(passLocaleWhitelistCheck == false)
   {
   	System.err.println("[PasswordLogon.jsp] "
   		+"!!!ERROR!!! "
   		+"Invalid locale parameter ("+locale+") passed from "
   		+"Remote Address = "+request.getRemoteAddr()+"; "
   		+"Remote Host = "+request.getRemoteHost());
   }
   // [END] JOUK031033813 - Fishnet audit XSS and blind SQL injection

   // Clear out any previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   session.invalidate();
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
     formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<%
   // Get the physical URL of the central logon page
   String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);
%>

<%
  // [START] JOUK031033813 - Fishnet audit XSS and blind SQL injection
  //if (request.getHeader("Referer") == null)
  if ((request.getHeader("Referer") == null)
  //		|| (!passOrgWhitelistCheck)
  		|| (!passBrandingWhitelistCheck)
  		|| (!passLocaleWhitelistCheck))
  // [END] JOUK031033813 - Fishnet audit XSS and blind SQL injection
  {
    url = NavigationManager.getNavMan().getPhysicalPage("PageNotAvailable", request)  ;
    System.err.println("PasswordLogon security checks failed.  Forwarding to PageNotAvailable." +
      " RefererHeader=" + ((request.getHeader("Referer")==null)?"null":"populated") +
      " passBrandingWhitelistCheck=" + passBrandingWhitelistCheck +
      " passLocaleWhitelistCheck=" + passBrandingWhitelistCheck );
%>
<jsp:forward page='<%= url %>'>
</jsp:forward>
<%
  } else {
    System.out.println("PasswordLogon security checks passed.  Forwarding to TradePortalLogon.");
%>
<jsp:forward page='<%= url %>'>
     <jsp:param name="auth"         value="<%= TradePortalConstants.AUTH_PASSWORD %>" />
     <jsp:param name="organization" value="<%= organization %>" />
     <jsp:param name="branding"     value="<%= branding %>" />
     <jsp:param name="locale"       value="<%= locale %>" />
</jsp:forward>
<%
  }
%>
