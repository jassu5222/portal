<%--
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*" %>

<%
   // Main entry point to the Trade Portal when the user is authenticating
   // themselves using single sign-on.   Basically, it forwards the user on
   // to TradePortalLogon.jsp, which is the central entry point to the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that single sign-on is being used

   // The single sign-on is based on SiteMinder Affiliate Agent.
   // There must be separate entry points for single sign-on and the other 
   // login methods so that Apache web server can block the single sign-on page if 
   // the SiteMinder Affiliate Agent is not installed.  

   // Get the physical URL of the central logon page
   String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   String organization = request.getParameter("organization");
   String branding     = request.getParameter("branding");
   String locale       = request.getParameter("locale");
   String ssoData     = request.getHeader("HTTP_SSO_LOGIN_ID"); 
   // HTTP_CAAS_AUTHLEVEL is the original login method for ANZ.  
   // 5 = User ID/Password
   // 40 = VASCO
   // 50 = TNC/Gemalto smart card
   String authLevel    = request.getHeader("HTTP_CAAS_AUTHLEVEL");  
%>

<%
   // Clear out any previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   session.invalidate();
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
     formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

  <jsp:forward page='<%= url %>'>
       <jsp:param name="auth"         value="<%= TradePortalConstants.AUTH_SSO %>" />
       <jsp:param name="organization" value="<%= organization %>" />
       <jsp:param name="branding"     value="<%= branding %>" />
       <jsp:param name="locale"       value="<%= locale %>" />
       <jsp:param name="info"         value="<%= ssoData %>" />
       <jsp:param name="HTTP_CAAS_AUTHLEVEL" value="<%=authLevel%>" />
  </jsp:forward>
