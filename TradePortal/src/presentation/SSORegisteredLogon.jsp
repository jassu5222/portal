<%--
 *
 *     Copyright  � 2002
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*" %>

<%
   // Main entry point to the Trade Portal when the user is authenticating
   // themselves using single sign-on by registration.  Basically, it forwards
   // the user to TradePortalLogon.jsp, which is the central entry point to
   // the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that single sign-on by registration is being used

   // The single sign-on is based on SAML 2.0 using the portal WebLogic Server
   // as a SAML2 Service Provider.
   // There must be separate entry points for single sign-on and the other 
   // login methods so that Apache web server can block the single sign-on
   // page if the SAML2 Service Provider is not configured.

   // Get the physical URL of the central logon page
   String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   String organization = request.getParameter("organization");
   String branding     = request.getParameter("branding");
   String locale       = request.getParameter("locale");
   String ssoData      = request.getRemoteUser();
%>

<%
   // Clear out any previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   session.invalidate();
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
     formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

  <jsp:forward page='<%= url %>'>
       <jsp:param name="auth"         value="<%= TradePortalConstants.AUTH_REGISTERED_SSO %>" />
       <jsp:param name="organization" value="<%= organization %>" />
       <jsp:param name="branding"     value="<%= branding %>" />
       <jsp:param name="locale"       value="<%= locale %>" />
       <jsp:param name="info"         value="<%= ssoData %>" />
  </jsp:forward>
