<%--
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, 
                 java.util.*, weblogic.servlet.security.*" %>

<%
   // Main entry point to the Trade Portal when the user is registering for SSO.
   // Basically, it forwards the user on to TradePortalLogon.jsp, which is the
   // central entry point to the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that SSO Registration is being used

   // There must be separate entry points for certificates and passwords so 
   // that Apache web server can be configured to require a certificate for the
   // certificates page and not require one for the password page.
   // IIS and Weblogic does not support this, so instead, users should be directed
   // to go to different ports (one with SSL, one without) depending on the
   // authentication method

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   String organization = StringFunction.xssCharsToHtml(request.getParameter("organization"));
   String branding     = StringFunction.xssCharsToHtml(request.getParameter("branding"));
   String locale       = StringFunction.xssCharsToHtml(request.getParameter("locale"));

   // Check the whitelist for each parameter
   boolean passBrandingWhitelistCheck = false;
   boolean passLocaleWhitelistCheck = false;
   StringBuffer sqlQuery = new StringBuffer();
   Vector queryResults = null;

   // branding whitelist
   if(!InstrumentServices.isBlankOrWhiteSpace(branding)) {
   	sqlQuery.setLength(0);
   	sqlQuery.append("select distinct(branding_directory) as BRANDING_WHITELIST ");
   	sqlQuery.append("from (select branding_directory from bank_organization_group ");
   	sqlQuery.append("union select branding_directory from client_bank)");
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
   Object sqlParams[] = null ;
   	queryResults = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams)
   				.getFragments("/ResultSetRecord");
   	for(int i=0; i<queryResults.size();i++) {
   		if(branding.equalsIgnoreCase(
   			((DocumentHandler)queryResults.elementAt(i)).getAttribute("/BRANDING_WHITELIST"))) {
   			Debug.debug("[SSORegistrationPasswordLogon.jsp] "
   				+"Found whitelist match for branding("+branding+") after "+(i+1)+" of "+queryResults.size()+" possible tries");
				passBrandingWhitelistCheck = true;
				break;
   		}
   		Debug.debug("[SSORegistrationPasswordLogon.jsp] "
   			+"branding whitelist check try #"+(i+1));
   	}
   }
   else {
   	passBrandingWhitelistCheck = true;
   }
   
	// Log an error to the log if the branding parameter is not on the whitelist
   if(passBrandingWhitelistCheck == false) {
   	System.err.println("[SSORegistrationPasswordLogon.jsp] "
   		+"!!!ERROR!!! "
   		+"Invalid branding parameter ("+branding+") passed from "
   		+"Remote Address = "+request.getRemoteAddr()+"; "
   		+"Remote Host = "+request.getRemoteHost());
   }
   
   // locale whitelist
   if(!InstrumentServices.isBlankOrWhiteSpace(locale)) {
   	sqlQuery.setLength(0);
   	sqlQuery.append("select distinct(locale) as LOCALE_WHITELIST from users");
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    Object sqlParams[] = null ;
   	queryResults = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams)
   				.getFragments("/ResultSetRecord");
   	for(int i=0; i<queryResults.size();i++) {
   		if(locale.equalsIgnoreCase(
   			((DocumentHandler)queryResults.elementAt(i)).getAttribute("/LOCALE_WHITELIST"))) {
   			Debug.debug("[SSORegistrationPasswordLogon.jsp] "
   				+"Found whitelist match for locale("+locale+") after "+(i+1)+" of "+queryResults.size()+" possible tries");
				passLocaleWhitelistCheck = true;
				break;
   		}
   		Debug.debug("[SSORegistrationPasswordLogon.jsp] "
   			+"locale whitelist check try #"+(i+1));
   	}
   }
   else {
   	passLocaleWhitelistCheck = true;
   }
   
	// Log an error to the log if the locale parameter is not on the whitelist
   if(passLocaleWhitelistCheck == false) {
   	System.err.println("[SSORegistrationPasswordLogon.jsp] "
   		+"!!!ERROR!!! "
   		+"Invalid locale parameter ("+locale+") passed from "
   		+"Remote Address = "+request.getRemoteAddr()+"; "
   		+"Remote Host = "+request.getRemoteHost());
   }

   // Clear out any SAML state and previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   ServletAuthentication.invalidateAll(request);
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
     formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<%
  // Get the physical URL of the central logon page
  String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);

  if ((request.getHeader("Referer") == null)
  		|| (!passBrandingWhitelistCheck)
  		|| (!passLocaleWhitelistCheck)) {
     url = NavigationManager.getNavMan().getPhysicalPage("PageNotAvailable", request)  ;
%>
<jsp:forward page='<%= url %>'/>
<%
  } else {
%>
<jsp:forward page='<%= url %>'>
     <jsp:param name="auth"         value="<%= TradePortalConstants.AUTH_SSO_REGISTRATION %>" />
     <jsp:param name="organization" value="<%= organization %>" />
     <jsp:param name="branding"     value="<%= branding %>" />
     <jsp:param name="locale"       value="<%= locale %>" />
</jsp:forward>
<%
  }
%>