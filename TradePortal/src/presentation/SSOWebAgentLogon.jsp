<%--
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.frame.*, com.ams.tradeportal.mediator.*,
	  com.ams.tradeportal.common.*, com.ams.tradeportal.html.*, java.util.*" %>

<%
   // Main entry point to the Trade Portal when the user is authenticating
   // themselves using single sign-on.   Basically, it forwards the user on
   // to TradePortalLogon.jsp, which is the central entry point to the portal.
   // Note the 'auth' parameter that is passed to TradePortalLogon to
   // indicate that single sign-on is being used

   // The single sign-on is based on SiteMinder Web Agent.
   // There must be separate entry points for single sign-on and the other 
   // login methods so that Apache web server can block the single sign-on page if 
   // the SiteMinder Web Agent is not installed.

   //cquinton 4/29/2013 Rel PR ir#16473
   //SiteMinder12 upgrade - ANZ security team has decided to only use SSPWebAgentLogic 
   //for SSO and Smardcards with SM12 upgrade. We've updated this page to page to accept SSO and 
   //Smartcards based on the Authentication level passed in the HTTP header.

   
   // Get the physical URL of the central logon page
   String url = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);

   // Get various parameters from the URL.
   // Errors are given on TradePortalLogon if organization and branding do not exist
   String orgId        = request.getParameter("organization");
   String branding     = StringFunction.xssCharsToHtml(request.getParameter("branding")); //T36000004579 #3;
   String locale       = request.getParameter("locale");
   
   // According to design, the policy server supposedly is sending HTTP_SM_USERDN and HTTP_CAAS_AUTHLEVEL 
   // but somehow the "HTTP_" is not sent through.
   String userDN       = request.getHeader("SM_USERDN"); 
   String authLevel    = request.getHeader("CAAS_AUTHLEVEL");
   PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");	
   
   //SM12 upgrade begin
   String authLevelSmartcard  = null;
   String authLevelSSO  = null;
   //SM12 upgrade end
   
   // Allow overriding SM_USERDN & CAAS_AUTHLEVEL for internal troubleshooting
   String tmp = request.getParameter("xSM_USERDN");
   if (tmp!=null && !tmp.equals("")) {
	   userDN = tmp;
   }
   
   tmp = request.getParameter("xCAAS_AUTHLEVEL");
   if (tmp!=null && !tmp.equals("")) {
	   authLevel = tmp;
   }
      
   // For dev testing, comment the 2 lines above and uncomment the 2 lines below
   //String userDN       = request.getParameter("SM_USERDN");
   //String authLevel    = request.getParameter("CAAS_AUTHLEVEL");
   Debug.debug("SSOWebAgentLogon.jsp HTTP Headers: "); 
   Enumeration headerNames = request.getHeaderNames();
   while(headerNames.hasMoreElements()) {
	   String headerName = (String)headerNames.nextElement();
	   String headerValue = request.getHeader(headerName);
	   Debug.debug("SSOWebAgentLogon.jsp HTTP Header: " + headerName + " = " + headerValue); // Leave for troubleshooting
   }

   // Clear out any previously active sessions
   // This ensures that all data is cleared out and cleaned up from old sessions
   session.invalidate();
   session = request.getSession(true);
%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
<%
   formMgr.init(request.getSession(true), "AmsServlet");
%>
</jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%
  // Check for an organization ID and branding directory - these are mandatory
  if ((orgId == null) || ("".equals(orgId))) {
       out.print("An organisation ID must be specified");
       return;
  }
  else {
       // Make it so that the organization ID is not case sensitive
       orgId = orgId.toUpperCase();
  }
  
  if (branding==null || "".equals(branding)) 
  {
	  branding = "null"; //SSO login does not really require branding parameter.  Set a dummy value here so that TradePortalLogon.jsp does not throw error.  Or we can update the TradePortalLogon.jsp logic.
  }

  // Check for optional locale
  if ((locale == null) || ("".equals(locale))) {
       // Default to en if no locale is received
       locale = "en";
       Debug.debug("SSOWebAgentLogon.jsp: No locale specified in SSOWebAgentLogon... defaulting to en");
  }

  //SM12 upgrade 
  try {
	  authLevelSmartcard = tpProperties.getString("CAAS_AUTH_LEVEL_SMARTCARD");
	
  } catch ( Exception ex ) {
      // Issue an error indicating that the authlevel is not valid.
       System.out.println("SSOWebAgentLogon.jsp: The CAAS_AUTH_LEVEL_SMARTCARD is not defined in TradePortal.Proeprties");

       // Use mediatorServices to accumulate errors
       MediatorServices mediatorServices = new MediatorServices();
       mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
             TradePortalConstants.CERTIFICATE_NOT_VALID);
       mediatorServices.addErrorInfo();

       // Add these errors to the default.doc XML so that it can be
       // accessed by the error page
       DocumentHandler defaultDoc = new DocumentHandler();
       defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
       formMgr.storeInDocCache("default.doc", defaultDoc);

       // Forward the user to the error page, passing along parameter
       url = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
  }
  
  //SM12 Upgrade
  try {
	  authLevelSSO = tpProperties.getString("CAAS_AUTHLEVEL_HARDWARE_PINPAD_TOKEN_OTP");
	
  } catch ( Exception ex ) {
      // Issue an error indicating that the authlevel is not valid.
       System.out.println("SSOWebAgentLogon.jsp: The CAAS_AUTHLEVEL_HARDWARE_PINPAD_TOKEN_OTP is not defined in TradePortal.Proeprties");

       // Use mediatorServices to accumulate errors
       MediatorServices mediatorServices = new MediatorServices();
       mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
             TradePortalConstants.CERTIFICATE_NOT_VALID);
       mediatorServices.addErrorInfo();

       // Add these errors to the default.doc XML so that it can be
       // accessed by the error page
       DocumentHandler defaultDoc = new DocumentHandler();
       defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
       formMgr.storeInDocCache("default.doc", defaultDoc);

       // Forward the user to the error page, passing along parameter
       url = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
  }
  
      
   System.out.println("SSOWebAgentLogon.jsp: CAAS_AUTHLEVEL header ="+authLevel);
  //Update with SM12 to handle SSO and Smartcard 
   // W Zhu 11/12/2015 Rel 9.4 CR-1018 allow CAAS_AUTHLEVEL_PASSWORD, i.e. the user logging in with password to bank portal.
  if (!authLevelSSO.equals(authLevel) && !authLevelSmartcard.equals(authLevel) && !TradePortalConstants.CAAS_AUTHLEVEL_PASSWORD.equals(authLevel)) {
       // Issue an error indicating that the authlevel is not valid.
       System.out.println("SSOWebAgentLogon.jsp: The CAAS_AUTHLEVEL header ("+authLevel+") was not provided or is not valid.  Turn on debug to see all headers.");

       // Use mediatorServices to accumulate errors
       MediatorServices mediatorServices = new MediatorServices();
       mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
       mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
             TradePortalConstants.CERTIFICATE_NOT_VALID);
       mediatorServices.addErrorInfo();

       // Add these errors to the default.doc XML so that it can be
       // accessed by the error page
       DocumentHandler defaultDoc = new DocumentHandler();
       defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
       formMgr.storeInDocCache("default.doc", defaultDoc);

       // Forward the user to the error page, passing along parameter
       url = NavigationManager.getNavMan().getPhysicalPage("AuthenticationErrors", request);
  }

  // Extract the SSO ID from the User Distinguished Name
  String ssoId;
  try {
	   String cnPrefix = "CN=";
	   String cnPostfix = ",";

	   int cnStartIndex = userDN.toUpperCase().indexOf(cnPrefix);
	   int cnEndIndex = userDN.indexOf(cnPostfix, cnStartIndex);

	   ssoId = userDN.substring(cnStartIndex + cnPrefix.length(), cnEndIndex);
  }
  catch (Exception e) {
       System.out.println("SSOWebAgentLogon.jsp: The SM_USERDN header ("+userDN+") was not provided or did not contain the CN property.  Turn on debug to see all headers.");
	   ssoId = "";
  }

  // Temporary code to allow skipping SSO logout.
  String ssoLogout = "Y";
  
  try {
	  tmp = tpProperties.getString("WebAgent_SSO_Logout");
	  ssoLogout = tmp;
  } catch ( Exception ex ) {
      //do nothing, use default value
  }
  
  if ("Y".equals(ssoLogout)) {
	  userSession.setUsingSiteMinderWebAgentSSO(true);
  }
%>

<%
  String authMethod = TradePortalConstants.AUTH_SSO;
  // W Zhu 11/30/2015 Rel 9.4.0.1 T36000044771 Use SSO for auth method.  Will check certificate_id field in LoginMediaterBean.
  //if (authLevelSmartcard.equals(authLevel)) {
  //  authMethod = TradePortalConstants.AUTH_CERTIFICATE_SSO;
  //}
%>      

<jsp:forward page='<%= url %>'>

   <jsp:param name="auth"         value="<%= authMethod %>" />
   <jsp:param name="info"         value="<%= ssoId %>" />
   <jsp:param name="organization" value="<%= orgId %>" />
   <jsp:param name="branding"     value="<%= branding %>" />
   <jsp:param name="locale"       value="<%= locale %>" />
   <jsp:param name="HTTP_CAAS_AUTHLEVEL" value="<%=authLevel%>" />

</jsp:forward>
