<%--
*******************************************************************************
                        AddInvoiceDetail 

  Description:
     This page is used to display the detail for an Invoice.
  It refers common fragment used to display the Invoice detail

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,
	com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.Hashtable, com.ams.util.*, java.util.*"%>

<%
  String closeLink = null;//"goToAddInvoices";
  String myPage = request.getParameter("myPage");
  if(myPage!=null && InstrumentType.LOAN_RQST.equals(myPage)){
	   closeLink = "goToAddTransInvoices";
  }
  else{
	  closeLink = "goToAddInvoices";
  }
  

%>
<%@ include file="/invoicemanagement/fragments/UploadInvoiceDetail.frag"%>
