<%--
*******************************************************************************
                            Add PO Line Items Page

  Description:  The Add PO Line Items page is used to present the user with a 
                list of PO line items from which he/she can select and add to 
                the transaction that is currently being edited. This page can 
                only be accessed in edit mode and only by organizations that 
                support purchase order processing and by users having the 
                necessary security right to do the same. Currently, this page 
                is accessible only to Import LC - Issue and Import LC - Amend
                transaction types.

                This page contains a listview of PO line items, each having 
                its own checkbox. The actual PO line items that appear in this 
                listview depend on whether or not the transaction currently 
                being edited has PO line items assigned to it. If PO line 
                items *are* currently assigned to it, the po upload definition
                oid, beneficiary name, and currency for the assigned PO line 
                items are passed into the page. The listview will then display
                only those unassigned PO line items that match all three of the 
                fields that were passed in. If the transaction currently 
                doesn't have any PO line items assigned to it, the listview 
                will display all unassigned PO line items belonging to the 
                user's organization.  

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,
                 com.ams.tradeportal.html.*, com.ams.util.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridLayout = null,
		   gridHtml = null,
		   dataViewJava	= null;


    TransactionWebBean              transaction         = null;
   DocumentHandler                 xmlDoc              = null;
   Hashtable                       secureParms         = null;
   boolean                         hasPOLineItems      = false;
   String                          uploadDefinitionOid = null;
   String                          userSecurityType    = null;
   String                          beneficiaryName     = null;
   String                          transactionOid      = null;
   String                          currency            = null;
   String                          link                = null;
   String                          shipmentOid         = null;
   String                          sourceType          = null;
   int                             numberOfRows        = 0;
   String poNumber = "";
   String lineItemNumber = "";
   String beneName = "";
 
   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();

   // Get the current transaction web bean so that we can pass its oid to the Add PO Line Items mediator
   transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionOid = transaction.getAttribute("transaction_oid");

   xmlDoc = formMgr.getFromDocCache();

   // If there are PO line items currently assigned to the transaction, then the PO upload definition
   // oid, beneficiary name, and currency will be passed into this page in the XML; otherwise,
   // check to see if they were passed back into the page from the Add PO Line Items mediator.
   if (!StringFunction.isBlank(xmlDoc.getAttribute("/In/SearchForPO/uploadDefinitionOid")))
   {
      uploadDefinitionOid = xmlDoc.getAttribute("/In/SearchForPO/uploadDefinitionOid");
      beneficiaryName     = xmlDoc.getAttribute("/In/SearchForPO/beneficiaryName");
      currency            = xmlDoc.getAttribute("/In/SearchForPO/currency");
      shipmentOid         = xmlDoc.getAttribute("/In/SearchForPO/shipment_oid");

      if(!ConvenienceServices.isBlank(uploadDefinitionOid))
           hasPOLineItems      = true;
   }
   else
   {
      // If we're coming back to this page as a result of the transaction previously having
      // PO line items, get the PO upload definition oid, beneficiary name, and currency from 
      // the /In section of the xml doc; otherwise, try to get it from the /Out section (in 
      // the case where the transaction did *not* previously have PO line items assigned to it).
      uploadDefinitionOid = xmlDoc.getAttribute("/In/Transaction/uploadDefinitionOid");
      shipmentOid = xmlDoc.getAttribute("/In/Terms/ShipmentTermsList/shipment_oid");

      if (xmlDoc.getAttribute("/Out/Transaction") != null)
      {
         uploadDefinitionOid = xmlDoc.getAttribute("/Out/Transaction/uploadDefinitionOid");
         beneficiaryName     = xmlDoc.getAttribute("/Out/Transaction/beneficiaryName");
         currency            = xmlDoc.getAttribute("/Out/Transaction/currency");
         hasPOLineItems      = true;
      }
   }

  // If this is a new shipment, the shipment OID will be in the out section of the XML
  if( (shipmentOid == null) || shipmentOid.equals("0") || shipmentOid.equals(""))
   {
      String numShipmentsString = xmlDoc.getAttribute("/In/numberOfShipments");

      int numShipments = 1;

      if(numShipmentsString != null)
         numShipments = Integer.parseInt(numShipmentsString);

      ShipmentTermsWebBean shipmentTerms	 = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
      TermsWebBean terms = (TermsWebBean) beanMgr.getBean("Terms");

      shipmentTerms.loadShipmentTerms(terms.getAttribute("terms_oid"), numShipments);

      shipmentOid = shipmentTerms.getAttribute("shipment_oid");
      xmlDoc.setAttribute("/In/SearchForPO/shipment_oid", shipmentOid);
      xmlDoc.setAttribute("/In/Terms/ShipmentTermsList/shipment_oid", shipmentOid);
   }
   StringBuffer groupingOptions              = new StringBuffer();
   String       selectedOption               = "";
   StringBuffer optionsExtraTags             = new StringBuffer();
   String		multiplePOLineItems 		 = TradePortalConstants.INDICATOR_NO;   
   
   selectedOption = request.getParameter("selectPOListType");
   
   if (selectedOption == null) {
      selectedOption = (String) session.getAttribute("selectPOListType");
   }
   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER.equals(selectedOption) || selectedOption == null) {
      groupingOptions.append(" selected ");
	 }
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("AddPOLineItems.ShowPOAndLineItem", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");

   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
      groupingOptions.append(" selected ");
	}
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("AddPOLineItems.ShowPONumber", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");

  
   session.setAttribute("selectPOListType", selectedOption);
   
   // Set a bunch of secure parameters necessary for the Add PO Line Items mediator
   secureParms = new Hashtable();
   secureParms.put("transactionOid", transactionOid);
   secureParms.put("shipmentOid", shipmentOid);
   secureParms.put("hasPOLineItems", String.valueOf(hasPOLineItems));
	//IRT36000016949 
	
   String poCurr ="";
	String poBen ="";
   if (hasPOLineItems)
   {
      
      poCurr=currency;
      secureParms.put("uploadDefinitionOid", uploadDefinitionOid);
      secureParms.put("currency",            currency);
      if(StringFunction.isNotBlank(xmlDoc.getAttribute("/In/SearchForPO/beneficiaryName"))){
    	  poBen= xmlDoc.getAttribute("/In/SearchForPO/beneficiaryName");
    		secureParms.put("beneficiaryName",     poBen);
    	}    	
    }
   
   	  
   if (!hasPOLineItems)currency = xmlDoc.getAttribute("/In/SearchForPO/currency");
   
    
%>


<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

	<div class="pageMainNoSidebar">     
		<div class="pageContent"> 
			<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="AddPOLineItems.Header"/>
				<jsp:param name="helpUrl"  value="customer/add_po_items.htm" />
  			</jsp:include>
  			
  			<jsp:include page="/common/ErrorSection.jsp" />
			<form id="AddPOLineItemsForm" name="AddPOLineItemsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
                <div class="formContentNoSidebar">
					<%= formMgr.getFormInstanceAsInputField("AddPOLineItemsForm", secureParms) %>
					<input type=hidden name="buttonName"     value="">
					<input type=hidden name="hasPOLineItems" value="<%=hasPOLineItems%>">
					<%=widgetFactory.createSearchSelectField("selectPOListType", "AddPOLineItems.Show", "", groupingOptions.toString(), " style=\"width:11em;\" onChange=\"shufflePODataView()\"")%>
					<div class="subHeaderDivider"></div>
					</br>
					<div class="gridSearch">
						<div class="searchHeader">
							<span class="searchHeaderCriteria">
								<i><%=widgetFactory.createSubLabel("AddPOLineItems.NoteFilterText")%></i>
							</span>
							
							<span class="searchHeaderActions">
								<jsp:include page="/common/gridShowCount.jsp">
		        					<jsp:param name="gridId" value="addPOLineItemsDataGridId" />
		      					</jsp:include>
		      				</span>
		      				<div style="clear: both;"></div>
				</div>
				</div>
		      				
				</br>		
					
				<div class="gridSearch">
					<div class="searchHeader">
					<span class="searchHeaderCriteria">
						<%=widgetFactory.createSearchTextField("PONumber","AddPOLineItems.PONumber", "14")%>
						<%=widgetFactory.createSearchTextField("LineItemNumber","AddPOLineItems.LineItemNumber","14")%>
						<%=widgetFactory.createSearchTextField("BeneName","AddPOLineItems.BeneficiaryName","35", " class=\"char20\"")%>
					</span>						
					<span class="searchHeaderActions">
					
						<button data-dojo-type="dijit.form.Button" type="button" id="SearchButton">
							<%=resMgr.getText("common.FilterText", TradePortalConstants.TEXT_BUNDLE)%>
						    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  			filterPOs();return false;
	      					</script>
						</button>
						<%=widgetFactory.createHoverHelp("SearchButton", "SearchHoverText")%>
					</span>
					<div style="clear: both;"></div>
				</div>
				</div>
					
					<%-- Leelavathi IR#T36000016913 30/07/2013 Begin  --%>
					<%
				     if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
				        multiplePOLineItems = TradePortalConstants.INDICATOR_YES;
					 }
				  %>
				  <%-- Leelavathi IR#T36000016913 30/07/2013 End  --%>
					
 					<input type=hidden name="multiplePOLineItems" value="<%=multiplePOLineItems%>">
					
					<%
						if (TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER.equals(selectedOption) || selectedOption == null) {
	                    	gridLayout = dgFactory.createGridLayout("addPOLineItemsDataGridId", "AddPOLineItemsDataGrid");
	                    	gridHtml = dgFactory.createDataGrid("addPOLineItemsDataGridId","AddPOLineItemsDataGrid",null);
	                    	dataViewJava = "AddPOLineItemsDataView";
						}
						if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
	                    	gridLayout = dgFactory.createGridLayout("addPOLineItemsDataGridId", "AddPODataGrid");
	                    	gridHtml = dgFactory.createDataGrid("addPOLineItemsDataGridId","AddPODataGrid",null);
	                    	dataViewJava = "AddPODataView";
						}	                    	
                    %>
						<%=gridHtml%>
						                              
				</div><%-- end of formContentNoSidebar div --%>  
  			</form>
  		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>
	
	<jsp:include page="/common/Footer.jsp">
	    <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    </jsp:include>
    
   
    <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;	
		
		var uploadDefinitionOid = <%=StringFunction.xssCharsToHtml(uploadDefinitionOid)%>;
		var beneficiaryName = "<%=poBen%>";
		var dataView = "<%=EncryptDecrypt.encryptStringUsingTripleDes(dataViewJava,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		if(uploadDefinitionOid == null || uploadDefinitionOid == "null") uploadDefinitionOid = "";
		if(beneficiaryName == null || beneficiaryName == "null") beneficiaryName = "";
		
		var initSearchParms="uploadDefinitionOid="+uploadDefinitionOid;
		   	initSearchParms+="&currency=<%=StringFunction.xssCharsToHtml(poCurr)%>";
			initSearchParms+="&beneficiaryName="+beneficiaryName;
			initSearchParms+="&sourceType=<%=TradePortalConstants.PO_SOURCE_UPLOAD%>";
			initSearchParms+="&hasPOLineItems=<%=hasPOLineItems%>";
			initSearchParms+="&encrypted=true";
		
		console.log("initSearchParms="+initSearchParms);
		console.log("dataViewJava-"+dataView);
		createDataGrid("addPOLineItemsDataGridId", dataView,gridLayout, initSearchParms);
		
		function filterPOs(){
			require(["dojo/dom","dojo/domReady!"],
			        function(dom){
			console.log("Inside filterPOs()");
				var benName = dom.byId("BeneName").value;
		
			var poNumber = dom.byId("PONumber").value;
			var lineItemNumber = dom.byId("LineItemNumber").value;
			var uploadDefinitionOid = <%=StringFunction.xssCharsToHtml(uploadDefinitionOid)%>;
			
			if(uploadDefinitionOid == null || uploadDefinitionOid == "null") uploadDefinitionOid = "";
			
			var searchParms="uploadDefinitionOid=<%=StringFunction.xssCharsToHtml(uploadDefinitionOid)%>";
			searchParms+="&currency=<%=StringFunction.xssCharsToHtml(poCurr)%>";
			searchParms+="&sourceType=<%=TradePortalConstants.PO_SOURCE_UPLOAD%>";
			searchParms+="&hasPOLineItems=<%=hasPOLineItems%>";
			searchParms+="&encrypted=true";
			<%-- IR T36000016949 adding beneficiaryName in search params --%>
			searchParms+="&beneficiaryName="+beneficiaryName;
			searchParms+="&beneName="+benName;
			searchParms+="&poNumber="+poNumber;
			searchParms+="&itemNumber="+lineItemNumber;
			
			console.log("searchParms="+searchParms);
			searchDataGrid("addPOLineItemsDataGridId", "<%=dataViewJava%>", searchParms);
			
			});
		}
		
		function addPOLineItems(){
			var rowKeys = getSelectedGridRowKeys("addPOLineItemsDataGridId");
			
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			setButtonPressed('<%=TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS%>', 0); 
			submitFormWithParms("AddPOLineItemsForm", '<%=TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS%>', "checkbox", rowKeys);
		}
		
		function closePOLineItems(){
			var URL = '<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", response)%>';
			document.location.href  = URL;
	 	}
		function shufflePODataView(){
			var selectedValue;
			require(["dijit/registry"],
			        function(registry) { 
						selectedValue = registry.byId("selectPOListType").value;
			});
			 var URL;
			 if(selectedValue == "<%=TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER%>"){
				 URL = '<%=formMgr.getLinkAsUrl("goToAddPOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER +"&beneficiaryName="+poBen
				 +"&uploadDefinitionOid="+uploadDefinitionOid+"&currency="+poCurr+"&sourceType="+TradePortalConstants.PO_SOURCE_UPLOAD+"&hasPOLineItems="+hasPOLineItems %>';
			 }else if(selectedValue == "<%=TradePortalConstants.BY_PO_NUMBER%>"){
				 URL = '<%=formMgr.getLinkAsUrl("goToAddPOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_NUMBER+"&beneficiaryName="+poBen
				 +"&uploadDefinitionOid="+uploadDefinitionOid+"&currency="+poCurr+"&sourceType="+TradePortalConstants.PO_SOURCE_UPLOAD+"&hasPOLineItems="+hasPOLineItems %>';
			 }
			 document.location.href  = URL;
		}
		
		require(["dojo/aspect","dijit/registry","dojo/ready"], function(aspect,registry,ready){
			ready(function(){
				var bgrid=registry.byId("addPOLineItemsDataGridId");<%-- Add your grid id instead mailmessagedatagridid --%>
				aspect.after(bgrid, "_onFetchComplete", function(){
					if(!bgrid.store._numRows){
						registry.byId("AddPOLineItems_AddSelectedPOLineItemsText").set('disabled',true);<%-- Add your button id to byId --%>
					}

				});
			});
		});
	</script>
</body>
</html>
<jsp:include page="/common/gridShowCountFooter.jsp">
	<jsp:param name="gridId" value="addPOLineItemsDataGridId" />
</jsp:include>
<%
   DocumentHandler xml = formMgr.getFromDocCache();

   xml.removeComponent("/In/POLineItemList");
   xml.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", xml);
%>
