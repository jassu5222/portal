<%--
*******************************************************************************
                        Upload Direct Debit Data File Page

  Description:  The Upload Debit Direct Data File page is used for uploading
                a direct debit data file from the user's computer to the
                Trade Portal with multiple payees for a Domestic Payment 
                Transaction. When the file is uploaded successfully, 
                it will create one Direct Debit Instruction that will/can 
                have multiple Payers

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   
   // Set the focus to the Purchase Order File Location field
   String onLoad = "document.forms[0].DirectDebitDataFilePath.focus();";
   String headerTitle=  	headerTitle=resMgr.getText("LocateUploadDirectDebitFile.FileUploadTitle", TradePortalConstants.TEXT_BUNDLE);
%>


<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>

<%--
   Note: The following form MUST have the 'enctype' attribute specified as "multipart/form-data"
   in order for the contents of the file selected by the user to be uploaded. It also must go 
   through the Direct Debit File Upload Servlet, as this servlet handles the 
   requests and forwards the user to the appropriate page afterwards.
--%>
	<div class="pageMain">
		<div class="pageContent">
			<form id="DirectDebitFileUploadDataForm" name="DirectDebitFileUploadDataForm" method="POST" data-dojo-type="dijit.form.Form"
			 	enctype="multipart/form-data" action="<%= response.encodeURL(request.getContextPath()+ "/transactions/DirectDebitFileUploadServlet.jsp") %>">

  				<div class="formArea">
  				<jsp:include page="/common/PageHeader.jsp">
                     <jsp:param name="titleKey" value="<%=headerTitle%>" />
                 </jsp:include>
	<jsp:include page="/common/ErrorSection.jsp" /> 

  <input type=hidden name="buttonName" value=""> 

 
					<div class="formContent">
					<br>
					<div class="formItem">
          					<%= resMgr.getText("LocateUploadDirectDebitFile.ToUploadDataNote", TradePortalConstants.TEXT_BUNDLE) %> 
        				</div>
  						<%= widgetFactory.createFileField("DirectDebitDataFilePath", "LocateUploadDirectDebitFile.DirectDebitDataFileLocation","class='char40'","", false) %>

      	  				<div id="buttonsDiv" class="formItem">
							<button data-dojo-type="dijit.form.Button"  name=<%=TradePortalConstants.BUTTON_UPLOAD_FILE%> id="UploadButton" type="button" data-dojo-props="" >
								<%=resMgr.getText("common.UploadText", TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									if(document.getElementById('DirectDebitDataFilePath').value != "" ) {
										setButtonPressed('<%=TradePortalConstants.BUTTON_UPLOAD_FILE%>', '0');
										hideButtons();
										document.forms[0].submit();
									}
									else {
										alert("<%=resMgr.getTextEscapedJS("LocateUploadDirectDebitFile.SelectFile", TradePortalConstants.TEXT_BUNDLE)%>");
									}
	 							</script>
							</button>
							<%=widgetFactory.createHoverHelp("UploadButton","UploadHoverText") %>
						
                 
             
	              			<button data-dojo-type="dijit.form.Button"  name="CancelButton" id="CancelButton" type="button" data-dojo-props="" >
								<%=resMgr.getText("common.CancelText", TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								openURL("<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", response)%>");
	 						</script>
							</button>
							<%=widgetFactory.createHoverHelp("CancelButton","common.Cancel") %>
						</div>
						<br>
						<div id="UploadMessage" style="display: none;">
						    <p >
							   <font class="availItemSubHeader">
							      <%=resMgr.getText("DocumentImageFileUpload.UploadingMessage", TradePortalConstants.TEXT_BUNDLE)%>
							   </font>
							</p>
					 	</div>
					</div> <%--closes formContent area--%>
				</div> <%--closes formArea--%>
  </form>
		</div> <%--closes pageContent area--%>
	</div> <%--closes pageMain area--%>
	<jsp:include page="/common/Footer.jsp">
	  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	</jsp:include>
</body>
</html>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<script LANGUAGE="JavaScript">
<%-- SETUP FOR BLINKING TEXT --%>
	var bName = navigator.appName;
	var bVer = parseInt(navigator.appVersion);
	var NS4 = (bName == "Netscape" && bVer >= 4);
	var IE4 = (bName == "Microsoft Internet Explorer" && bVer >= 4);
	var NS3 = (bName == "Netscape" && bVer < 4);
	var IE3 = (bName == "Microsoft Internet Explorer" && bVer < 4);
	var blink_speed=100;
	var i=0;

 	if (NS4 || IE4) {
		if (navigator.appName == "Netscape") {
			layerStyleRef="layer.";
			layerRef="document.layers";
			styleSwitch="";
		} else {
			layerStyleRef="layer.style.";
			layerRef="document.all";
			styleSwitch=".style";
		}
	}

<%-- BLINK FUNCTION --%>
	function Blink(layerName){
		if (NS4 || IE4) {
			if(i%2==0) {
				eval(layerRef+'["'+layerName+'"]'+
				styleSwitch+'.visibility="visible"');
			} else {
 				eval(layerRef+'["'+layerName+'"]'+
				styleSwitch+'.visibility="hidden"');
			}
		}
 		if(i<1) {
			i++;
		} else {
			i--
		}
 		setTimeout("Blink('"+layerName+"')",blink_speed);
	}

<%-- HIDE BUTTONS, SHOW AND BLINK TEXT --%>
     function hideButtons() {

		document.getElementById('buttonsDiv').style.display = 'none';
		document.getElementById('UploadMessage').style.display = 'block';
	 }

function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
    	}
    }
	 document.location.href  = URL;
	 return false;
}
</script>
