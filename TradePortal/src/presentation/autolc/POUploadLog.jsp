<%--
*******************************************************************************
                        Purchase Order Upload Log Page

  Description:
     This page displays the (latest) upload log for a given user's corporate
  org.  It supports the ability to refresh the page when the user clicks the 
  refresh button.

  (Future enhancements include the ability to display the log records in 
  reverse order (i.e. so that refreshing shows the last line at the top 
  rather than requiring the user to scroll to the bottom).  Also, adding the
  ability to display prior logs.
*******************************************************************************
--%>

	<%--
	 *
	 *     Copyright   2001                         
	 *     American Management Systems, Incorporated 
	 *     All rights reserved
	--%>	
	<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
	                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
	                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	                 com.ams.tradeportal.html.*,java.util.*" %>
	
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
	</jsp:useBean>
	
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
	   scope="session">
	</jsp:useBean>


	<%-- ************** Data retrieval page setup begins here ****************  --%>

	<%
		WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	
	  // showInReverse is for a potential future enhancement to support displaying 
	  // the log records in revers order (i.e., last line first).
	  boolean showInReverse = false;
	
	  String logSequence = "";
	  
	  DocumentHandler resultDoc = null;
	  DocumentHandler logDoc = null;
	  Vector          logRecords = null;
	  int             numRecords = 0;
	
	  // Retrieve the log records for the given corporate org.  We only choose the
	  // records in the org for the last upload log.  (There can be more than one,
	  // however, we don't give the option currently to show anything other than
	  // the latest.)
	
	
	  try {
	      String sql = "select text  from auto_lc_create_log where a_corp_org_oid = ?  and sequence_number = "
	      				+"    (select max(sequence_number) from auto_lc_create_log  where a_corp_org_oid = ? "
	      				+") order by auto_lc_create_log_oid ";
	      if (showInReverse) sql+=" DESC";
	      Debug.debug(sql.toString());
	
	      resultDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, userSession.getOwnerOrgOid(), userSession.getOwnerOrgOid() );
	      if (resultDoc != null) {
	          logRecords = resultDoc.getFragments("/ResultSetRecord");
	          numRecords = logRecords.size();
	      } else {
	          numRecords = 0;
	      }
	
	  } catch (Exception e) {
	      e.printStackTrace();
	  } finally {
	  }  // try/catch/finally block
	
	%>


<%-- ********************* HTML for page begins here *********************  --%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" 
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  
             value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
	
<jsp:include page="/common/ButtonPrep.jsp" />

<%
  String instrType = resMgr.getText("POLineItemDetail.PurchaseOrders", TradePortalConstants.TEXT_BUNDLE),
    pageType = resMgr.getText("POUploadLog.UploadLog", TradePortalConstants.TEXT_BUNDLE);
%>

<div class="pageMain">
  <div class="pageContent" >
			
<%
  String pageTitleKey = "SecondaryNavigation.PurchaseOrders";
  String helpUrl = "customer/po_upload_log.htm";
%>
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="SecondaryNavigation.PurchaseOrders.UploadLog" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>
			
    <div class="subHeaderDivider"></div>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <span><%=instrType%></span>
        <span>&nbsp;-&nbsp;</span>
        <span><%=pageType%></span>
      </span>
      <span class="pageSubHeader-right">
	<span class="pageSubHeaderRefresh" id="refresh">
      	  <a href="<%=formMgr.getLinkAsUrl("goToViewPOUploadLog", response)%>"></a>
    	</span>
		<%=widgetFactory.createHoverHelp("refresh", "RefreshImageLinkHoverText") %>
        <%--cquinton 10/31/2012 ir#7015 change return link to close button when no close button present.
            note this does NOT include an icon as in the sidebar--%>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToUploadsHome", response) %>';
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>


			<form name="POUploadLog" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
		  		<div class="formContentNoSidebar">	
		  			<jsp:include page="/common/ErrorSection.jsp" />
		  			
		  			<input type=hidden value="" name=buttonName>
		  		
					<%
					    for (int i=0; i < numRecords; i++) {
					       logDoc = (DocumentHandler) logRecords.elementAt(i);
					%>
	
					<div class="formItem">
						<%=logDoc.getAttribute("/TEXT")%><br/>
					</div>
				
					<%    
					    }
					%>
				</div>
			</form>
		</div>
	</div>
</body>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

	<%
	  // Finally, reset the cached document to eliminate carryover of 
	  // information to the next visit of this page.
	  formMgr.storeInDocCache("default.doc", new DocumentHandler());
	
	%>
</html>
