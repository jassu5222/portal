<!DOCTYPE HTML>
<%--
*******************************************************************************
                        Purchase Order Line Item Detail

  Description:
     This page is used to display the detail for a purchase order line item.
  Although it is modeled after the standard ref data pages, this is a readonly
  page.  It is only used to display data and can never update anything.

*******************************************************************************
--%>

<%--
 *		Dev Owner: Sandeep
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,java.text.SimpleDateFormat,java.util.Date,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
    		 
  boolean isReadOnly = true;

  String oid = "";

  boolean showDelete = true;
  boolean showSave   = false;

  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;

  DocumentHandler doc;

  POUtility poUtility = new POUtility();
  Hashtable fields = null;

  POLineItemWebBean lineItem = beanMgr.createBean(POLineItemWebBean.class, "POLineItem");

  POUploadDefinitionWebBean uploadDefn = beanMgr.createBean(POUploadDefinitionWebBean.class,  "POUploadDefinition");

  String loginLocale;

%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();

  // Get the document from the cache.  We'll use it to repopulate the screen 
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();

  Debug.debug("doc from cache is " + doc.toString());

  if (doc.getDocumentNode("/In/POLineItem") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item)

     oid = request.getParameter("po_line_item_oid");
     if (oid != null) {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
     }

     if (oid == null) {
       // Should never happen as we can only come to this page for an existing
       // line item.
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");

     if (maxError.compareTo(
                  String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
        // We've returned from a delete that was successful
        // We shouldn't be here.  Forward to the TransactionsHome page.
        formMgr.setCurrPage("TransactionsHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("TransactionsHome", request);

%>
        <jsp:forward page='<%=physicalPage%>' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        // Use oid from input doc.
        oid = doc.getAttribute("/In/POLineItem/po_line_item_oid");
     }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Display error.

     getDataFromDoc = false;

     lineItem.setAttribute("po_line_item_oid", oid);
     lineItem.getDataFromAppServer();

     if (lineItem.getAttribute("po_line_item_oid").equals("")) {
       oid = "0";
     }
  } 

  if (getDataFromDoc) {
     // Populate the line item bean with the input doc.
     try {
        lineItem.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get PO LineItem attributes "
             + " for oid: " + oid + " " + e.toString());
     }
  }

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " oid: " + oid);

%>

<%
  // From the upload definition (for this po line item) we need to create a
  // hashtable that translates the internal field names to the external (user
  // defined field names).  In other words, rather than displaying "other1",
  // we display what ever name the user has assigned to the other1 field.
  String definitionOid = lineItem.getAttribute("source_upload_definition_oid");

  try {
    uploadDefn.setAttribute("po_upload_definition_oid", definitionOid);
    uploadDefn.getDataFromAppServer();

    fields = poUtility.loadFieldNames(uploadDefn);
  } catch (Exception e) {
    e.printStackTrace();
  }

%>
<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/ButtonPrep.jsp" />

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%
  String instrType = resMgr.getText("POStructureLineItemDetail.PurchaseOrders", TradePortalConstants.TEXT_BUNDLE),
    poItem = resMgr.getText("POLineItemDetail.POItem", TradePortalConstants.TEXT_BUNDLE);
%>
	
<div class="pageMain">
  <div class="pageContent">
<%
  String pageTitleKey = "SecondaryNavigation.PurchaseOrders",
    helpUrl = "customer/po_line_detail.htm";
%>
			
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="SecondaryNavigation.PurchaseOrders.POItems" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>

    <div class="subHeaderDivider"></div>
			
    <%--cquinton 11/1/2012 ir#7015 replace return link with close button--%>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <span><%=instrType%></span>
        <span>&nbsp;-&nbsp;</span>
        <span><%=lineItem.getAttribute("po_num")%></span>
        <%if(lineItem.getAttribute("item_num")!= null && !("").equals(lineItem.getAttribute("item_num"))) 
        	{
        %>
        	<span>&nbsp;-&nbsp;</span>
        	<span><%=lineItem.getAttribute("item_num")%></span>
        <%} %>
      </span>
					
      <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button"  name="DeleteButton" id="DeleteButton" type="submit" data-dojo-props="iconClass:'delete'" class="pageSubHeaderButton">
          <%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			var confirmDelete = confirm("<%=resMgr.getText("POLineItemDetail.ConfirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE)%>");
            
            if(confirmDelete==true){                        
                  setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
                  document.forms[0].submit();
            }
            return false;
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("DeleteButton","DeletHoverText") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              '<%= formMgr.getLinkAsUrl("goToUploadsHome", response) %>';
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton","ReturnPreviousHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>

			<form name="POLineItemDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
			  <input type=hidden value="" name=buttonName>
			  		<div class="formContentNoSidebar">	
			  			<jsp:include page="/common/ErrorSection.jsp" />
						<%
							  // Store values such as the userid, his security rights, and his org in a secure
							  // hashtable for the form.  The mediator needs this information.
							  Hashtable secureParms = new Hashtable();
							  secureParms.put("UserOid", userSession.getUserOid());
							
							  secureParms.put("po_line_item_oid", oid);
						%>
			
						<%= formMgr.getFormInstanceAsInputField("POLineItemDetailForm", secureParms) %>
							<%
							//This is done to display date based on Locale.
							Locale objLocale = new Locale(loginLocale.substring(0, 2), loginLocale.substring(3, 5));

							SimpleDateFormat sdf=new SimpleDateFormat("dd MMM yyyy hh:mm:ss",objLocale);
							SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S",objLocale);
							Date date= isoDateFormatter.parse(lineItem.getAttribute("creation_date_time"));
							String subHdr = resMgr.getText("POLineItemDetail.POItemDtls", TradePortalConstants.TEXT_BUNDLE) + " - " +
										resMgr.getText("POLineItemDetail.DateTime", TradePortalConstants.TEXT_BUNDLE) + ": " +sdf.format(date);
							
							%>
							<%= widgetFactory.createWideSubsectionHeader(subHdr,true,false,false,"") %>
							
							<table class="poContent">
				  				<tr>
				  					<td width="25%" > 
				        				<%= widgetFactory.createTextField((String)fields.get("po_num"),(String)fields.get("po_num"),lineItem.getAttribute("po_num"),"8",true) %>
				      				</td>
				      				<td width="25%" >
				      					<%= widgetFactory.createTextField((String)fields.get("currency"),(String)fields.get("currency"),lineItem.getAttribute("currency"),"8",true) %>
				      				</td>
				      				<td width="25%" >
				      				<%String displayAmount = TPCurrencyUtility.getDisplayAmount(lineItem.getAttribute("amount"),lineItem.getAttribute("currency"), loginLocale); %>
				      					<%= widgetFactory.createTextField((String)fields.get("amount"),(String)fields.get("amount"),displayAmount,"8",true) %>
				      				</td>
				      				<td width="25%"  >
				      					<%= widgetFactory.createTextField((String)fields.get("ben_name"),(String)fields.get("ben_name"),lineItem.getAttribute("ben_name"),"8",true) %>
				      				</td>
				  				</tr>  				
				
				  							
								
								<tr>
								<%
								    // Now, for each of the 24 other fields only print the label and value
								    // if the value is non-blank.
								    int colCount=0;
									if (StringFunction.isNotBlank(lineItem.getAttribute("item_num"))) {
										colCount++;
									%>
				  					<td> 
				  						<%= widgetFactory.createTextField((String)fields.get("item_num"),(String)fields.get("item_num"),lineItem.getAttribute("item_num"),"8",true) %>
				      				</td>
				      				<% }
									if (StringFunction.isNotBlank(lineItem.getAttribute("last_ship_dt"))) {
										colCount++;
									%>
					      				<td>
					      					<%= widgetFactory.createDateField((String)fields.get("last_ship_dt"),(String)fields.get("last_ship_dt"), lineItem.getAttribute("last_ship_dt"), true)%>
					      				</td>
						      		<% }
									
								    for (int i=1; i<=24; i++) {
								      String attributeName = "other" + i;
								      String fieldName = "_field_name";
								      
								      String attribute = (String)fields.get(attributeName + fieldName);
								      String value = lineItem.getAttribute(attributeName);
								      String fieldTypeAttributeName = "other" + i + "_datatype";
									  String fieldTypeValue = uploadDefn.getAttribute(fieldTypeAttributeName);
								      if (StringFunction.isNotBlank(value)) {
								    	  colCount++;
								%>
						        	<td> 
						        	
						        	<%if(fieldTypeValue.equals("DATE")){ %>
						        	<%
						        		value = TPDateTimeUtility.convertJPylonDateToISODate(value);
										value= value + " 00:00:00.0"; //Pavani-This is done for proper date conversion. 
										//Since the attribute is not registered as DateTimeAttribute while saving, this workaround is required to acheive correct display value.
									%>
						            	<%= widgetFactory.createDateField(attribute,attribute,value, true)%>
						            <%}else{ %>
						            	<%= widgetFactory.createTextField(attribute,attribute,value,"8",true) %>
						            <%} %>
						          	</td>	        
								
								<%
								      
								      
								      if(colCount%4==0){
								%>
										</tr><tr>
								<%
								      }//end if - colCount
								      }//end if value
								    }// end for
								%> 
								</tr>
								<tr>
								<% if (StringFunction.isNotBlank(lineItem.getAttribute("po_text"))) {
										colCount++;
					  				%>
					      				<td colspan="2" >
					      					<%= widgetFactory.createTextArea((String)fields.get("po_text"), (String)fields.get("po_text"), lineItem.getAttribute("po_text"), isReadOnly,false, false, "style=\"width:400px;height:60px;\"", "", "") %>
					      				</td>
						      		<%} %>
						      	</tr>	
						      	</table>
					</div><%--closing formContentNoSidebar --%>
			</form>
		</div>
	</div>
</body>

<%
  // Finally, reset the cached document to eliminate carryover of 
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
