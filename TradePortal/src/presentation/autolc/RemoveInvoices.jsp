<%--
*******************************************************************************
                            Remove Invoice Page

   Description:  The Remove Invoice  page is used to present the user with a 
                list of Invoices assigned to the transaction. User will
                be able to Unassign the invoices from the transaction

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;
	TransactionWebBean transaction = null;
	DocumentHandler xmlDoc = null;
	StringBuffer dynamicWhereClause = null;
	Hashtable secureParms = null;
	boolean hasInvoices = false;
	String uploadInvDefOid = "";
	String userSecurityType = null;
	String sellerName = "";
	String validDate= null;
	String transactionOid = null;
	String userOrgOid = null;
	String currency = "";
	String link = null;
	String termsOid = null;
	String sourceType = null;
	int numberOfRows = 0;

	String poNumber = "";
	String lineItemNumber = "";
	boolean fromAddInvoice = false;
	String transactionType    = "";
	// Get the user's security type and organization oid
	userSecurityType = userSession.getSecurityType();
	userOrgOid = userSession.getOwnerOrgOid();
	String dPattern = (userSession.getDatePattern()).toLowerCase();
	link = formMgr.getLinkAsUrl("goToInstrumentNavigator", response);
	// Get the current transaction web bean so that we can pass its oid to the Add Invoice  mediator
	transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionType = transaction.getAttribute("transaction_type_code");
   transactionOid  = transaction.getAttribute("transaction_oid");

    xmlDoc = formMgr.getFromDocCache();
   termsOid = xmlDoc
	.getAttribute("/In/SearchForPO/termsOid");
    currency      = xmlDoc.getAttribute("/In/SearchForPO/invCurrency");

   if ((termsOid == null) || termsOid.equals("0")
			|| termsOid.equals("")) {
		TermsWebBean terms = (TermsWebBean) beanMgr.getBean("Terms");

		termsOid = terms.getAttribute("terms_oid");
		xmlDoc.setAttribute("/In/SearchForPO/termsOid",
				termsOid);
		xmlDoc.setAttribute("/In/Terms/termsOid",
				termsOid);
	}
	// Set a bunch of secure parameters necessary for the Add Invoice mediator
	 secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("transactionOid", transactionOid);
   secureParms.put("termsOid", termsOid);
%>


<%-- ********************* HTML for page begins here *********************  --%>
<jsp:include page="/common/Header.jsp">
			   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   
			</jsp:include>
<div class="pageMainNoSidebar">
		<div class="pageContent">
		<jsp:include page="/common/PageHeader.jsp">
                     <jsp:param name="titleKey" value="AtpInvoiceSearch.AssignInvoices" />
                 </jsp:include>
<form name="RemoveInvoicesForm" method="POST" action="<%=formMgr.getSubmitAction(response)%>">

  				

 <jsp:include page="/common/ErrorSection.jsp" /> 
<div class="formContentNoSidebar">
<%=formMgr.getFormInstanceAsInputField("RemoveInvoicesForm", secureParms)%> 
<input type=hidden
	name="buttonName" value=""> <input type=hidden
	name="hasInvoices" value="<%=hasInvoices%>">

<%@ include file="/transactions/fragments/ViewInvoicesSearch.frag"%>

<%
gridLayout = dgFactory.createGridLayout("addInvoiceDataDataGridId", "RemoveInvoicesDataGrid");
gridHtml = dgFactory.createDataGrid("addInvoiceDataDataGridId","RemoveInvoicesDataGrid",null);
%>
<%=gridHtml%>
</div>

</form>
</div>
</div>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  
</jsp:include>
    
 <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;
								
		var initSearchParms="userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
			initSearchParms+="&transactionTypeCode=<%=transaction.getAttribute("transaction_type_code")%>";
			initSearchParms+="&hasInvoices=<%=hasInvoices%>";
			initSearchParms+="&uploadDefinitionOid=<%=uploadInvDefOid%>";
			initSearchParms+="&currency=<%=currency%>";
			initSearchParms+="&beneficiaryName=<%=sellerName%>";
			initSearchParms+="&validDate=<%=validDate%>";
			initSearchParms+="&tranOid=<%=transaction.getAttribute("transaction_oid")%>";
			initSearchParms+="&fromAddInv=N";
		
	
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("RemoveInvoicesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("addInvoiceDataDataGridId", viewName,gridLayout, initSearchParms);
		
		function filterPOs(){
			 require(["dojo/dom"],
				      function(dom){
			console.log("Inside filterPOs()");
			
			var benName = dom.byId("BeneName").value;
			var invID = dom.byId("InvID").value;
			var dateTo = dom.byId("DateTo").value;
			var dateFrom = dom.byId("DateFrom").value;
			 var currency=(dijit.byId("Currency").value).toUpperCase();
			var amountFrom = dom.byId("AmountFrom").value;
			var amountTo = dom.byId("AmountTo").value;
			var searchParms= "userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
			searchParms+="&sellerName="+benName;
			searchParms+="&InvID="+invID;
			searchParms+="&DateTo="+dateTo;
			searchParms+="&DateFrom="+dateFrom;
			searchParms+="&Currency="+currency;
			searchParms+="&AmountFrom="+amountFrom;
			searchParms+="&AmountTo="+amountTo;
			var tempDatePattern = encodeURI('<%=dPattern%>');
	 		searchParms=searchParms+"&dPattern="+tempDatePattern;
			console.log("searchParms="+searchParms);

			searchDataGrid("addInvoiceDataDataGridId", "RemoveInvoicesDataView", searchParms);
		});
		}
		function closeStructurePODataItems(){
			openURL("<%=link %>");
		}
		function openURL(URL){
            if (isActive =='Y') {
            	if (URL != '' && URL.indexOf("javascript:") == -1) {
	                var cTime = (new Date()).getTime();
	                URL = URL + "&cTime=" + cTime;
	                URL = URL + "&prevPage=" + context;
            	}
            }
			 document.location.href  = URL;
			 return false;
		}
		
		function removeInvoiceData(){
			var rowKeys = getSelectedGridRowKeys("addInvoiceDataDataGridId"),
			    buttonName = '<%=TradePortalConstants.REMOVE_PO%>',
			    formName = 'RemoveInvoicesForm';
			    var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getText("CashMgmtFutureValue.PopupMessage", 
	            TradePortalConstants.TEXT_BUNDLE)) %>";
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			if (!confirm(confirmMessage)) 
			{
				 return;
			}
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		}
	
	</script>   
	
</body>
</html>

<%
DocumentHandler xml = formMgr.getFromDocCache();
xml.removeComponent("/Error");

xml.removeComponent("/In/POLineItemList");
// Finally, reset the cached document to eliminate carryover of
// information to the next visit of this page.
formMgr.storeInDocCache("default.doc", xml);
%>
