<%--
*******************************************************************************
                            Remove PO Line Items Page

  Description:  The Remove PO Line Items page is used to present the user with 
                a list of PO line items assigned to the transaction that is 
                currently being edited from which he/she can select to remove
                from the transaction. This page can only be accessed in edit 
                mode and only by organizations that support purchase order 
                processing and by users having the necessary security right to 
                do the same. Currently, this page is accessible only to Import 
                LC - Issue and Import LC - Amend transaction types.

                This page contains a listview of PO line items, each having 
                its own checkbox. Once a user selects one or more PO line items
                and presses the Remove PO Line Items button, the selected PO
                line items are removed from the transaction and reset to 
                unassigned.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridLayout = null,
	   gridHtml = null,
	   dataViewJava	= null;



   TransactionWebBean              transaction        = null;
   StringBuffer                    dynamicWhereClause = null;
   Hashtable                       secureParms        = null;
   String                          userSecurityType   = null;
   String                          transactionType    = null;
   String                          transactionOid     = null;
   String                          userOrgOid         = null;
   String                          link               = null;
   String                          shipmentOid         = null;
   int                             numberOfRows       = 0;


   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();
   userOrgOid       = userSession.getOwnerOrgOid();

   // Get the transaction oid from the current transaction web bean
   transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionType = transaction.getAttribute("transaction_type_code");
   transactionOid  = transaction.getAttribute("transaction_oid");

   DocumentHandler xmlDoc = formMgr.getFromDocCache();
   shipmentOid         = xmlDoc.getAttribute("/In/SearchForPO/shipment_oid");

   StringBuffer groupingOptions              = new StringBuffer();
   String       selectedOption               = "";
   StringBuffer optionsExtraTags             = new StringBuffer();
   String		multiplePOLineItems 		 = TradePortalConstants.INDICATOR_NO;   
   
   selectedOption = request.getParameter("selectPOListType");
   if (selectedOption == null) {
      selectedOption = (String) session.getAttribute("selectPOListType");
   }
   											
   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER.equals(selectedOption) || selectedOption == null) {
      groupingOptions.append(" selected ");
	 }
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("RemovePOLineItems.ShowPOAndLineItem", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");

   groupingOptions.append("<option value='");
   groupingOptions.append(TradePortalConstants.BY_PO_NUMBER);
   groupingOptions.append("' ");
   if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
      groupingOptions.append(" selected ");
	}
   groupingOptions.append(">");
   groupingOptions.append(resMgr.getText("RemovePOLineItems.ShowPONumber", TradePortalConstants.TEXT_BUNDLE));
   groupingOptions.append("</option>");

   session.setAttribute("selectPOListType", selectedOption);
   
   // Set a bunch of secure parameters necessary for the Remove PO Line Items mediator
   secureParms = new Hashtable();
   secureParms.put("transactionOid", transactionOid);
   secureParms.put("shipmentOid", shipmentOid);
%>

<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMainNoSidebar">     
		<div class="pageContent"> 
			<jsp:include page="/common/PageHeader.jsp">
				<jsp:param name="titleKey" value="RemovePOLineItems.Header"/>
				<jsp:param name="helpUrl"  value="customer/remove_po_items.htm" />
  			</jsp:include>
  			
  			<jsp:include page="/common/ErrorSection.jsp" />
			<form id="RemovePOLineItemsForm" name="RemovePOLineItemsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
                <div class="formContentNoSidebar">
	
						<%= formMgr.getFormInstanceAsInputField("RemovePOLineItemsForm", secureParms) %>
						  <input type=hidden value="" name=buttonName>

						<div class="gridSearch">
						<div class="searchHeader">
							<span class="searchHeaderCriteria">
								<%=widgetFactory.createSearchSelectField("selectPOListType", "RemovePOLineItems.Show", "", groupingOptions.toString(), " style=\"width:11em;\" onChange=\"shufflePODataView()\"")%>
							</span>
							
							<span class="searchHeaderActions">
								<jsp:include page="/common/gridShowCount.jsp">
		        					<jsp:param name="gridId" value="removePOLineItemsDataGridId" />
		      					</jsp:include>
		      				</span>
		      				<div style="clear: both;"></div>
						</div>
						</div>
    
  <%
     if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
        multiplePOLineItems = TradePortalConstants.INDICATOR_YES;
	 }
  %>
  <input type=hidden name="multiplePOLineItems" value="<%=multiplePOLineItems%>">
  <input type=hidden name="deletePOLineItems" value="<%=TradePortalConstants.INDICATOR_NO%>">
  
    <%
						if (TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER.equals(selectedOption) || selectedOption == null) {
	                    	gridLayout = dgFactory.createGridLayout("removePOLineItemsDataGridId", "RemovePOLineItemsDataGrid");
	                    	gridHtml = dgFactory.createDataGrid("removePOLineItemsDataGridId","RemovePOLineItemsDataGrid",null);
	                    	dataViewJava = "RemovePOLineItemsDataView";
						}
						if (TradePortalConstants.BY_PO_NUMBER.equals(selectedOption)) {
	                    	gridLayout = dgFactory.createGridLayout("removePOLineItemsDataGridId", "RemovePODataGrid");
	                    	gridHtml = dgFactory.createDataGrid("removePOLineItemsDataGridId","RemovePODataGrid",null);
	                    	dataViewJava = "RemovePODataView";
						}	                    	
                    %>
						<%=gridHtml%>    
			  
   </div><%-- end of formContentNoSidebar div --%>  
  			</form>
  		</div><%-- end of pageContent div --%>
	</div><%-- end of pageMainNoSidebar div --%>
	
	<jsp:include page="/common/Footer.jsp">
	    <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
	    <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
    </jsp:include>

<script LANGUAGE="JavaScript">
var gridLayout = <%= gridLayout %>;	

var dataView = "<%=EncryptDecrypt.encryptStringUsingTripleDes(dataViewJava,userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>


var initSearchParms="userOrgOid=<%=userOrgOid%>";
	initSearchParms+="&transactionOid=<%=transactionOid%>";
	initSearchParms+="&shipmentOid="+<%=shipmentOid%>;
	initSearchParms+="&sourceType=<%=TradePortalConstants.PO_SOURCE_UPLOAD%>";
	initSearchParms+="&transactionType=<%=transactionType%>";
	
console.log("initSearchParms="+initSearchParms);
console.log("dataViewJava-"+dataView);
createDataGrid("removePOLineItemsDataGridId", dataView,gridLayout, initSearchParms);


function removePOLineItems(){
	var rowKeys = getSelectedGridRowKeys("removePOLineItemsDataGridId");
	
	if(rowKeys == ""){
		alert("Please select record(s).");
		return;
	}
	setButtonPressed('RemovePOLineItems', 0); 
	submitFormWithParms("RemovePOLineItemsForm", 'RemovePOLineItems', "checkbox", rowKeys);
}

function deletePOLineItems(){
	var rowKeys = getSelectedGridRowKeys("removePOLineItemsDataGridId");
	
	if(rowKeys == ""){
		alert("Please select record(s).");
		return;
	}
	if(confirmDelete()){
		setButtonPressed('DeletePOLineItems', 0); 
		submitFormWithParms("RemovePOLineItemsForm", 'DeletePOLineItems', "checkbox", rowKeys);
	}else{
		return;
	}	
}

function closePOLineItems(){
	var URL = '<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", response)%>';
	document.location.href  = URL;
		<%-- return false; --%>
}

function confirmDelete() {
    <%--
    // Display a popup asking if the user wants to delete.  If OK is pressed,
    // true is returned.  If Cancel is pressed, false it returned
    --%>
	var msg = "<%=resMgr.getText("confirm.delete",TradePortalConstants.TEXT_BUNDLE)%>"
    if (!confirm(msg))
       return false;
    else {
	   document.forms[0].deletePOLineItems.value = "<%=TradePortalConstants.INDICATOR_YES%>";
       return true;
	}
  }
  
function shufflePODataView(){
	var selectedValue;
	require(["dijit/registry"],
	        function(registry) { 
				selectedValue = registry.byId("selectPOListType").value;
	});
	 var URL;
	 if(selectedValue == "<%=TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER%>"){
		 URL = '<%=formMgr.getLinkAsUrl("goToRemovePOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_AND_LINEITEM_NUMBER %>';
	 }else if(selectedValue == "<%=TradePortalConstants.BY_PO_NUMBER%>"){
		 URL = '<%=formMgr.getLinkAsUrl("goToRemovePOLineItems", response)+"&selectPOListType="+TradePortalConstants.BY_PO_NUMBER%>';
	 }
	 document.location.href  = URL;
}

require(["dojo/aspect","dijit/registry","dojo/ready"], function(aspect,registry,ready){
	ready(function(){
		var bgrid=registry.byId("removePOLineItemsDataGridId");<%-- Add your grid id instead mailmessagedatagridid --%>
		aspect.after(bgrid, "_onFetchComplete", function(){
			if(!bgrid.store._numRows){
				registry.byId("RemovePOLineItems_RemoveSelectedPOLineItemsText").set('disabled',true);<%-- Add your button id to byId --%>
				registry.byId("RemovePOLineItems_RemoveAndDeleteSelectedPOLineItemsText").set('disabled',true);<%-- Add your button id to byId --%>
			}

		});
	});
});
  
</script>

</body>
</html>
<jsp:include page="/common/gridShowCountFooter.jsp">
	<jsp:param name="gridId" value="removePOLineItemsDataGridId" />
</jsp:include>
<%
   DocumentHandler xml = formMgr.getFromDocCache();

   xml.removeComponent("/In/POLineItemList");
   xml.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", xml);
%>
