<%--
*******************************************************************************
                        Remove Structured PO Detail

  Description:
     This page is used to display the detail for a purchase order line item.
   It refers common fragment used to display the PO detail

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>



<%
  String closeLink = "goToRemoveStructuredPO";
%>
<%@ include file="/autolc/PurchaseOrderStructuredDetail.frag"%>
