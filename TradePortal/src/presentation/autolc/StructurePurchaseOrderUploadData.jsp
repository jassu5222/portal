<%--
*******************************************************************************
                        Upload Purchase Order Data File Page

  Description:  The Upload Purchase Order Data File page is used for uploading
                a purchase order data file from the user's computer to the
                Trade Portal and starting the process of creating appropriate 
                PO line items and Import LC Issue/Amend transactions (according 
                to options selected by the user in this page). The page will 
                display an informational message indicating whether any current
                PO data files are being processed for the user's organization.
                If the user presses the Upload button while another data file 
                is still being processed, an error will be issued and the user
                will be returned to the Transactions Home page. Otherwise, the
                PO data file and all options made by the user will be saved off
                for a background process to collect, parse, and group as 
                necessary (i.e., using the AutoLCAgent and 
                AutoLCCreateMediator).

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   MediatorServices   mediatorServices         = null;
   DocumentHandler    poUploadDefinitionDoc    = null;
   DocumentHandler    xmlDoc                   = null;
   StringBuffer       dropdownOptions          = new StringBuffer();
   StringBuffer       searchOptions            = new StringBuffer();
   Hashtable          secureParms              = new Hashtable();
   boolean            defaultRadioButtonValue1 = true;
   boolean            defaultRadioButtonValue2 = false;
   boolean            defaultRadioButtonValueA = false;
   boolean            defaultRadioButtonValueB = false;
   boolean            defaultRadioButtonValueC = false;
   boolean            defaultRadioButtonValueD = false;  
   Vector             poUploadDefinitionList   = null;
   String             poItemsGroupingOption    = null;
   String             poUploadDefinitionOid    = null;
   String             poDataOption             = null;
   String             userOrgOid               = null;
   String             hasErrors                = null;
   String             newLink                  = null;
   String             onLoad                   = null;
   String             poUploadInstrType        = null;
   boolean            readOnlyATP              = false;
   boolean            readOnlyDLC              = false;

   String             userSecurityRights        = null;
   userSecurityRights  = userSession.getSecurityRights();
   String             userOrgAutoATPCreateIndicator = null;
   String             userOrgAutoLCCreateIndicator  = null;
   String             userOid = null;
   DocumentHandler    hierarchyDoc = null;
   userOrgAutoATPCreateIndicator = userSession.getOrgAutoATPCreateIndicator();
   userOrgAutoLCCreateIndicator = userSession.getOrgAutoLCCreateIndicator();
   String            selectedWorkflow   = "";
   final String ALL_WORK = "common.allWork";
   final String MY_WORK = "common.myWork";
   int               totalOrganizations = 0;
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.UploadCenter");
   //cquinton 1/18/2013 add page to page flow
   userSession.addPage("goToStructureUploadPOData",request);

   boolean canProcessPOForDLC = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.IMPORT_DLC);
   boolean canProcessPOForATP = SecurityAccess.canProcessPurchaseOrder(userSecurityRights, InstrumentType.APPROVAL_TO_PAY);
   String            userDefaultWipView = userSession.getDefaultWipView();
   
   // Replaced canProcessPOForATP and canProcessPOForDLC
   if ((userOrgAutoATPCreateIndicator != null && !userOrgAutoATPCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) || !canProcessPOForATP) {
      readOnlyATP = true;
   }
   
   if ((userOrgAutoLCCreateIndicator != null && !userOrgAutoLCCreateIndicator.equals(TradePortalConstants.INDICATOR_YES)) || !canProcessPOForDLC) {
      readOnlyDLC = true;
   }

						
   // Retrieve the user's org oid
   userOrgOid = userSession.getOwnerOrgOid();
   userOid    = userSession.getUserOid();
   
   CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   corpOrg.getById(userOrgOid);
   
   String path = corpOrg.getAttribute("restricted_po_upload_dir");
   if(path!=null){
      path = path.replaceAll("\\\\", "\\\\\\\\");
   }
   else{
      path ="";
   }
   String newPath = null;
  
   // This is the query used for populating the PO Upload Definition dropdown list;
   // it retrieves all PO Upload Definitions that belong to the corporate customer 
   // user's org that are used for uploading or for both uploading and manual entry
   List<Object> sqlParams = new ArrayList<Object>();
   StringBuilder sqlQuery = new StringBuilder("select PURCHASE_ORDER_DEFINITION_OID, NAME  from PURCHASE_ORDER_DEFINITION  where a_owner_org_oid in (?");
   sqlParams.add(userSession.getOwnerOrgOid());

   // Also include PO definitions from the user's actual organization if using subsidiary access
   if(userSession.showOrgDataUnderSubAccess())
    {
      sqlQuery.append(",?");
      sqlParams.add(userSession.getSavedUserSession().getOwnerOrgOid());
    }

   sqlQuery.append(") order by ");
   sqlQuery.append(resMgr.localizeOrderBy("name"));

   poUploadDefinitionDoc  = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);

   if(poUploadDefinitionDoc == null)
      poUploadDefinitionDoc = new DocumentHandler();

   // Get the current xml doc from the doc cache and check for errors
   xmlDoc = formMgr.getFromDocCache();

   hasErrors = xmlDoc.getAttribute("/In/HasErrors");
 
      // Default the PO Upload Definition dropdown to be the default definition for this corporate org
      String defaultDefinitionSql = "select PURCHASE_ORDER_DEFINITION_OID from PURCHASE_ORDER_DEFINITION where default_flag = ? and a_owner_org_oid = ? ";
      DocumentHandler results = DatabaseQueryBean.getXmlResultSet(defaultDefinitionSql, false, new Object[]{"Y",userSession.getOwnerOrgOid()});
     

      if(results != null)
       {
          poUploadDefinitionOid = results.getAttribute("/ResultSetRecord(0)/PURCHASE_ORDER_DEFINITION_OID");
       }
 
   // If the user's org currently has other Purchase Orders being uploaded, issue an 
   // informational message indicating this to the user; do this by checking the active_po_upload 
   // table for active PO's that belong to the user's org.
   String whereClause = "a_corp_org_oid = ?";
   Debug.debug("whereClause=="+whereClause.toString());

   if (DatabaseQueryBean.getCount("active_po_upload_oid", "active_po_upload", whereClause, false, new Object[]{userOrgOid}) != 0)
   {
      mediatorServices = new MediatorServices();
      mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, 
                                                    TradePortalConstants.AUTOLC_PROCESS_UPLOADING);
      mediatorServices.addErrorInfo();

      xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());

      formMgr.storeInDocCache("default.doc", xmlDoc);
   }
   
   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.
   String sqlSearchQuery = "select organization_oid, name from corporate_org where activation_status = ? start with organization_oid = ?"
   +" connect by prior organization_oid = p_parent_corp_org_oid  order by "+resMgr.localizeOrderBy("name");
   
   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlSearchQuery.toString(), false, TradePortalConstants.ACTIVE, userOrgOid);
   
   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
   totalOrganizations = orgListDoc.size();
   
   selectedWorkflow = request.getParameter("workflow");
 
   if (selectedWorkflow == null)
   {
      selectedWorkflow = (String) session.getAttribute("workflow");

      if (selectedWorkflow == null)
      {
         if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG))
         {
            selectedWorkflow = userOrgOid;
         }
         else if (userDefaultWipView.equals(TradePortalConstants.WIP_VIEW_MY_ORG_CHILDREN))
         {
            selectedWorkflow = ALL_WORK;
         }
         else
         {
            selectedWorkflow = MY_WORK;
         }
         selectedWorkflow = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
      }
   }

   session.setAttribute("workflow", selectedWorkflow);

   selectedWorkflow = EncryptDecrypt.decryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());

   // Set the focus to the Purchase Order File Location field
   onLoad = "document.forms[0].PurchaseOrderDataFilePath.focus();";
   
   secureParms.put("SecurityRights",   userSecurityRights);
   secureParms.put("UserOid",          userOid);
   secureParms.put("corporateOrgOid",  userOrgOid);
   secureParms.put("timeZone",         userSession.getTimeZone());
   secureParms.put("userLocale",       userSession.getUserLocale());
   secureParms.put("clientBankOid",    userSession.getClientBankOid());
   secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode());
   secureParms.put("ownerOrgOid", userSession.getOwnerOrgOid());

%>

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ********************* HTML for page begins here *********************  --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="<%=onLoad%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="SecondaryNavigation.PurchaseOrders" />
      <jsp:param name="item1Key" value="LocateUploadPO.Header" />
      <jsp:param name="helpUrl"  value="customer/upload_po_file.htm" />
    </jsp:include>
    <div class="subHeaderDivider"></div>
    <%--cquinton 11/1/2012 ir#7015 change return link for close button--%>
    <div class="pageSubHeader">
      <span class="pageSubHeaderItem">
        <%=resMgr.getText("LocateUploadPO.Header", TradePortalConstants.TEXT_BUNDLE)%>
      </span>
      <span class="pageSubHeader-right">
        <button data-dojo-type="dijit.form.Button" type="button" class="pageSubHeaderButton" id="Upload">
          <%=resMgr.getText("common.UploadFileText", TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            if (restrictAttach(document.forms[0],'<%= path %>')) {
              document.forms[0].submit();
            }
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("Upload", "LocateUploadPO.UploadFile") %>
        <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
          <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            document.location.href =
              "<%= formMgr.getLinkAsUrl("goToUploadsHome", response) %>";
          </script>
        </button> 
        <%=widgetFactory.createHoverHelp("CloseButton", "CloseHoverText") %>
      </span>
      <div style="clear:both;"></div>
    </div>
    
<%--
   Note: The following form MUST have the 'enctype' attribute specified as "multipart/form-data"
   in order for the contents of the file selected by the user to be uploaded with the other
   form parameter values (i.e., hidden field values, radio button values, etc.). It also must go 
   through the Purchase Order File Upload Servlet, as this servlet handles all file PO upload 
   requests and forwards the user to the appropriate page afterwards.
--%>
<form name="StructuredPOUploadsForm" id="StructuredPOUploadsForm" method="POST" data-dojo-type="dijit.form.Form"
      action="<%= formMgr.getSubmitAction(response) %>"> 
    <input type=hidden value="" name=buttonName>
<div class="formAreaNoSidebar">
  <jsp:include page="/common/ErrorSection.jsp" />     
 <div class="formContentNoSidebar">	
 		  <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step1") %>
  		  <%= widgetFactory.createFileField("PaymentDataFilePath", "","class='char40'","", false) %>
  		  <%=widgetFactory.createHoverHelp("PaymentDataFilePath","LocateUploadPO.Step1") %>
  		  
  		  
          <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step2") %>
         <div class="formItem">  
         <%=widgetFactory.createRadioButtonField("POUploadDataOption","TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS","LocateUploadPO.MakePOStructureAvailable",TradePortalConstants.MANUALLY_ADD_PO_LINE_ITEMS,defaultRadioButtonValue1, false," labelClass=\"formWidth\" onClick=\"setPOItemsGroupingOption()\"","") %>                                    
  	     <br> 
         <%=widgetFactory.createRadioButtonField("POUploadDataOption","TradePortalConstants.GROUP_PO_LINE_ITEMS","LocateUploadPO.GroupUploadedPOStructureItems",TradePortalConstants.GROUP_PO_LINE_ITEMS,defaultRadioButtonValue2, false," labelClass=\"formWidth\" onClick=\"setPOItemsGroupingOption()\"","") %>                                    
         <br>
  
  
  <div class="formItem"> 
    <div class="formItem"> 
    <%  
    //Show only if user has the security right to opt for Import LC for PO file upload.
    if(!readOnlyDLC)
    {
    %>
    
       
        <%=widgetFactory.createRadioButtonField("POUploadInstrOption","TradePortalConstants.AUTO_LC_PO_UPLOAD","LocateUploadPO.ImportLC",TradePortalConstants.AUTO_LC_PO_UPLOAD,defaultRadioButtonValueC, readOnlyDLC,"onClick=\"setPODataOption()\"","") %>                                     
     
    <%--Krishna IR-AGUI031880087(Visibilty Of LC/ATP option in The PO FileUpload Page)--%>   
    <%  
    //Put empty spaces only if next option ATP security right for PO file upload is there for the User.
    if(!readOnlyATP)
    {
    %>
    &nbsp;
    <%
        }//if(!readOnlyATP)
     } //if(!readOnlyDLC)
    //Show only if user has the security right to opt for Approval to Pay for PO file upload. 
    if(!readOnlyATP)
    {
    %>
    
        <%=widgetFactory.createRadioButtonField("POUploadInstrOption","TradePortalConstants.AUTO_ATP_PO_UPLOAD","LocateUploadPO.ApprovalToPay",TradePortalConstants.AUTO_ATP_PO_UPLOAD,defaultRadioButtonValueD, readOnlyATP,"onClick=\"setPODataOption()\"","") %>                                    
      
    <%--Krishna IR-AGUI031880087(Visibilty Of LC/ATP option in The PO FileUpload Page) End--%> 
    <%
     } //if(!readOnlyATP)
    %>
 
      </div>  
 
 		<%=widgetFactory.createCheckboxField("POUploadGroupingOption", "LocateUploadPO.IncludePOStructureDataInTheFile",defaultRadioButtonValueA, false, false, " labelClass=\"formWidth\"  onClick=\"setPODataOption()\"", "", "" ) %>
 
    </div> 
    </div> 
        <%= widgetFactory.createWideSubsectionHeader("LocateUploadPO.Step3") %>
          <%
             if (poUploadDefinitionOid == null)
             {
                poUploadDefinitionOid = "";
             }

             dropdownOptions.append(Dropdown.createSortedOptions(poUploadDefinitionDoc, "PURCHASE_ORDER_DEFINITION_OID", 
                                                                "NAME", poUploadDefinitionOid, userSession.getSecretKey(), resMgr.getResourceLocale()));

             String defaultText = resMgr.getText("LocateUploadPO.SelectUploadDefinition", TradePortalConstants.TEXT_BUNDLE);
         %>
         <%=widgetFactory.createSelectField("PODefinitionName", "",defaultText, dropdownOptions.toString(), false) %>
         

     <div class="searchDivider"></div>

    <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
        StringBuffer prependText = new StringBuffer();
		String defaultValue1 = "";
		String defaultText1  = "";
        prependText.append(resMgr.getText("PaymentFileUpload.WorkFor", 
                                           TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");
        if(!userSession.hasSavedUserSession())
	     { 
	      defaultValue1 = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
	       defaultText1 = MY_WORK;
	       
	       searchOptions.append("<option value=\"");
	       searchOptions.append(defaultValue1);
	       searchOptions.append("\"");
	       
	       if (selectedWorkflow.equals(defaultText1))
	       {
	    	   searchOptions.append(" selected");
	       }
	
	       searchOptions.append(">");
	       searchOptions.append(resMgr.getText( defaultText1, TradePortalConstants.TEXT_BUNDLE));
	       searchOptions.append("</option>");
	     }

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, 
                                     SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           searchOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              searchOptions.append("<option value=\"");
              searchOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              searchOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 searchOptions.append(" selected");
              }

              searchOptions.append(">");
              searchOptions.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              searchOptions.append("</option>");
           }
        }
        else
        {
           searchOptions.append("<option value=\"");
           searchOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           searchOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              searchOptions.append(" selected");
           }

           searchOptions.append(">");
           searchOptions.append(prependText.toString());
           searchOptions.append(userSession.getOrganizationName());
           searchOptions.append("</option>");
        }

        

        

   
     
%>
	<div class="formItem">
	<%=widgetFactory.createSearchSelectField("Workflow","PaymentFileUpload.Show","",searchOptions.toString(),
	 "onChange='searchPOFileUploads();'") %>
</div>
    </span>
    <span class="searchHeaderActions">
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="POFileUploadGridID" />
      </jsp:include>

      <span id="POUploadRefresh" class="searchHeaderRefresh"></span>
      <%=widgetFactory.createHoverHelp("POUploadRefresh", "RefreshImageLinkHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>
  
   <%
      DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr, response);
      String gridHtml = dgFactory.createDataGrid("POFileUploadGridID","POFileUploadDataGrid",null);
      String gridLayout = dgFactory.createGridLayout("POFileUploadGridID", "POFileUploadDataGrid");
   %>
   <%=gridHtml%>
     </div>
  </div>
  <%= formMgr.getFormInstanceAsInputField("StructuredPOUploadsForm", secureParms) %>
  </form>
  </div>
  </div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<script language="JavaScript">

var blink_speed=200;
var i=0;

function restrictAttach(form,path) {

var allowUpload = true; 

var file = form.PaymentDataFilePath.value;

if (!file) {
	<%--  BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
	<%--  jgadela Rel 8.3  added escapeQuotesforJS to fix quotes error in the french text --%>
	alert('<%=StringFunction.escapeQuotesforJS(StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.NoPOFileError",
					TradePortalConstants.TEXT_BUNDLE)))%>');
	return false;
}

var filedef = form.PODefinitionName[0].value;

if (!filedef || filedef == "<Select PO Definition>") {
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.NoPOFileDefError",
					TradePortalConstants.TEXT_BUNDLE))%>');
	return false;
}


if (path){
 var delimiter = "\\";
	delimiter = (file.lastIndexOf("//") != -1)?"//":"\\";
	delimiter = (file.lastIndexOf("/") != -1)?"/":"\\";
			
 if (file.lastIndexOf(delimiter) != -1){
 var slashIndex = file.lastIndexOf(delimiter) ;
 file = file.substring(0,slashIndex);
 newPath = file;

    <%-- if path is not same as restricted path the show up the error --%>
   if(newPath != path){
	<%--  BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
	alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.RestrictFilePathErrorText",
					TradePortalConstants.TEXT_BUNDLE))%>');
     return false;
	 }
	}
  }

<%-- Purchase Order Data validation --%>

if (document.forms[0].POUploadDataOption[1].checked == true) {
 var instrOptionlist = document.getElementsByName("POUploadInstrOption");
 var insOptionChecked =false;
 for(var i = 0; i < instrOptionlist.length; i++) {
  if (instrOptionlist[i].type == 'radio' && instrOptionlist[i].checked == true) {
	   insOptionChecked = true;
	   break;
	 }
 }
 if (!insOptionChecked) {
	<%--  jgadela Rel 8.3  added escapeQuotesforJS to fix quotes error in the french text --%>
	  alert('<%=StringFunction.escapeQuotesforJS(StringFunction.asciiToUnicode(resMgr.getText("StructuredPOUpload.instrTypeMissing",TradePortalConstants.TEXT_BUNDLE)))%>');
	  return false;
 }                  

}


 if (allowUpload) {
	   form.action ="<%=response.encodeURL(request.getContextPath()
					+ "/transactions/POStructuredFileUploadServlet.jsp")%>";
	   form.enctype="multipart/form-data";
	   form.encoding="multipart/form-data";	  
	   return true;
 }
 else {
     <%--  BSL IR NRUL080338698 03/28/2012 - Convert numeric character references to Unicode --%>
     alert('<%=StringFunction.asciiToUnicode(resMgr.getText(
					"StructuredPOUpload.ValidFileExtionsionText",
					TradePortalConstants.TEXT_BUNDLE))%>');
     return false;
 }

}

<%--get grid layout from the data grid factory--%>
var gridLayout = <%= gridLayout %>;
<%--set the initial search parms--%>

var initSearchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>&selectedWorkflow=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(selectedWorkflow))%>";
console.log ('initParams='+initSearchParms);
var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("POFileUploadDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
<%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
var POFileUploadId = 
  createDataGrid("POFileUploadGridID", viewName,
                  gridLayout, initSearchParms,'-2');

function searchPOFileUploads(){
	 
	  require(["dojo/dom"],
		      function(dom){
		 var searchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>";
		 var selectField = dijit.byId('Workflow');
		 var val = selectField.attr('value');
		  if (val == '') {
		 	val = selectField.attr('displayedValue');
		 	displayedValue = 'false';
		 	
		 } else {
		 searchParms=searchParms+"&displayedValue=true";
		 }
		 <%-- Naveen. IR-T36000011218(ANZ737). Calling encodeURIComponent is not required as its being done in datagrid.js. Hence commented below line --%>
		 searchParms=searchParms+"&selectedWorkflow="+val;		
		        searchDataGrid("POFileUploadGridID", "POFileUploadDataView",
		                       searchParms);
		      });

}
require(["dojo/query", "dojo/on", "dojo/domReady!"],
	      function(query, on){
	    query('#POUploadRefresh').on("click", function() {
	    	searchPOFileUploads();
	    });
	  }); 
	  
function deleteSelectedPOFiles(){
	console.log("inside deleteSelectedPOFiles()");
	var formName = 'StructuredPOUploadsForm',
		buttonName = '<%=TradePortalConstants.BUTTON_DELETE_PO_UPLOAD%>',
		rowKeys = getSelectedGridRowKeys("POFileUploadGridID");

	if(rowKeys == ""){
		alert("Please pick record(s) and click 'Delete'");
		return;
	}
	
	console.log("buttonName="+buttonName);
	console.log("rowKeys="+rowKeys);

	submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
}
<%--
// This function unchecks the PO Items grouping option if the second PO data 
// option is selected and checkes the first PO Items grouping option only if 
// the user is changing to the first PO data option (does nothing if it's 
// already selected.
--%>
function setPOItemsGroupingOption()
{
	require(["dijit/registry"],
	  function(registry) {
		   var radio0 = registry.getEnclosingWidget(document.forms[0].POUploadInstrOption[0]);
		   var radio1 = registry.getEnclosingWidget(document.forms[0].POUploadInstrOption[1]);
		   if (document.forms[0].POUploadDataOption[0].checked == true)
		   {
			  document.forms[0].POUploadDataOption[1].checked = false;
			  var instrOptionlist = document.getElementsByName("POUploadInstrOption");
			  if(instrOptionlist.length == 1){
				  registry.getEnclosingWidget(document.forms[0].POUploadInstrOption).set('checked',false);
			  }else if(instrOptionlist.length == 2){
				  radio0.set('checked', false);
				  radio1.set('checked', false);
			  }
		      dijit.byId("POUploadGroupingOption").set('checked',false);
		   }
		   else
		   {
		      if (document.forms[0].POUploadDataOption[1].checked == true) 
		      {
		            document.forms[0].POUploadDataOption[0].checked = false;
		       }
		   }
	});
}

<%--  This function checks the first PO data option if it hasn't already been  --%>
<%--  selected. This function gets called when the user selects either of the  --%>
<%--  PO Items grouping options. --%>
function setPODataOption()
{
   
   var ch = dijit.getEnclosingWidget(document.forms[0].POUploadDataOption[1]);
   if (document.forms[0].POUploadDataOption[1].checked == false)
   {
	  document.forms[0].POUploadDataOption[1].checked = true;	  
	  ch.set('checked',true);
   }
   if (document.forms[0].POUploadGroupingOption.checked == true) {
   dijit.byId("POUploadGroupingOption").set("value",'<%= TradePortalConstants.INCLUDE_ALL_PO_LINE_ITEMS %>');
   }
}




</script>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="POFileUploadGridID" />
</jsp:include>

</body>
</html>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
