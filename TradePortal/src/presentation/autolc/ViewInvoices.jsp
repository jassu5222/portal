<%--
*******************************************************************************
                            View Invoice Page

  Description:  The View Invoice page is used to present the user with a 
                list of Invoices assigned to the transaction



*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,
	com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
	com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.Hashtable,com.ams.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DGridFactory dgFactory = new DGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;
	TransactionWebBean transaction = null;
	DocumentHandler xmlDoc = null;
	StringBuffer dynamicWhereClause = null;
	Hashtable secureParms = null;
	boolean hasInvoices = false;
	String uploadInvDefOid = "";
	String userSecurityType = null;
	String sellerName = "";
	String validDate= null;
	String transactionOid = null;
	String userOrgOid = null;
	String currency = "";
	String link = null;
	String termsOid = null;
	String sourceType = null;
	int numberOfRows = 0;

	String poNumber = "";
	String lineItemNumber = "";
	boolean fromAddInvoice = false;

	// Get the user's security type and organization oid
	userSecurityType = userSession.getSecurityType();
	userOrgOid = userSession.getOwnerOrgOid();
	String dPattern = (userSession.getDatePattern()).toLowerCase();

	// Get the current transaction web bean so that we can pass its oid to the Add Invoice  mediator
	transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

	transactionOid = transaction.getAttribute("transaction_oid");
	link = formMgr.getLinkAsUrl("goToInstrumentNavigator", response);
	xmlDoc = formMgr.getFromDocCache();

	// If there are Invoice  currently assigned to the transaction, then the Invoice upload definition
	// oid, beneficiary name, and currency will be passed into this page in the XML; otherwise,
	// check to see if they were passed back into the page from the Add Invoice  mediator.
	if (!InstrumentServices
			.isBlank(xmlDoc
					.getAttribute("/In/SearchForPO/uploadInvDefOid"))) {
		uploadInvDefOid = xmlDoc
				.getAttribute("/In/SearchForPO/uploadInvDefOid");
		sellerName = xmlDoc
				.getAttribute("/In/SearchForPO/sellerName");
		currency = xmlDoc
				.getAttribute("/In/SearchForPO/invCurrency");
		termsOid = xmlDoc
				.getAttribute("/In/SearchForPO/termsOid");
		validDate = xmlDoc.getAttribute("/In/Transaction/validDate");
		validDate =TPDateTimeUtility.convertToDBDate(validDate,
				TradePortalConstants.PO_FORMATTED_US_DATE);
		if (!ConvenienceServices.isBlank(uploadInvDefOid))
			hasInvoices = true;
	}else {
		// If we're coming back to this page as a result of the transaction previously having
		// Invoice , get the Invoice upload definition oid, beneficiary name, and currency from 
		// the /In section of the xml doc; otherwise, try to get it from the /Out section (in 
		// the case where the transaction did *not* previously have Invoice  assigned to it).
		uploadInvDefOid = xmlDoc
				.getAttribute("/In/Transaction/uploadDefinitionOid");
		termsOid = xmlDoc
				.getAttribute("/In/Terms/termsOid");

		if (xmlDoc.getAttribute("/Out/Transaction") != null) {
			uploadInvDefOid = xmlDoc
					.getAttribute("/Out/Transaction/uploadDefinitionOid");
			sellerName = xmlDoc
					.getAttribute("/Out/Transaction/beneficiaryName");
			currency = xmlDoc.getAttribute("/Out/Transaction/currency");
			validDate = xmlDoc.getAttribute("/Out/Transaction/validDate");
			hasInvoices = true;
		}
		
		if (!ConvenienceServices.isBlank(uploadInvDefOid))
			hasInvoices = true;
	}
	if (!InstrumentServices
			.isBlank(currency)) {
	xmlDoc.setAttribute("/In/SearchForPO/invCurrency",
			currency);
	}
	if ((termsOid == null) || termsOid.equals("0")
			|| termsOid.equals("")) {
		TermsWebBean terms = (TermsWebBean) beanMgr.getBean("Terms");

		termsOid = terms.getAttribute("terms_oid");
		xmlDoc.setAttribute("/In/SearchForPO/termsOid",
				termsOid);
		xmlDoc.setAttribute("/In/Terms/termsOid",
				termsOid);
	}
	
	//SHR IR T36000005856 Start
	if(InstrumentServices.isBlank(validDate) && xmlDoc
			.getAttribute("/In/SearchForPO/dueDate") !=null){
		validDate = xmlDoc
		.getAttribute("/In/SearchForPO/dueDate"); 
		validDate =TPDateTimeUtility.convertToDBDate(validDate,
				"yyyy-MM-dd HH:mm:ss.S"); 
		
	}
	//SHR IR T36000005856 End
	xmlDoc.setAttribute("/In/SearchForPO/uploadInvDefOid",
			uploadInvDefOid);
	xmlDoc.setAttribute("/In/SearchForPO/sellerName",
			sellerName);
	String listViewXML = "AddInvoicesListView.xml";
	String newSearchTrans = request.getParameter("NewSearch");
	// Set a bunch of secure parameters necessary for the Add Invoice mediator
	secureParms = new Hashtable();
	secureParms.put("transactionOid", transactionOid);
	secureParms.put("termsOid", termsOid);
	secureParms.put("UserOid", userSession.getUserOid());
	secureParms.put("hasInvoices", String.valueOf(hasInvoices));

	if (hasInvoices) {
		secureParms.put("uploadDefinitionOid", uploadInvDefOid);
		secureParms.put("currency", currency);
		secureParms.put("beneficiaryName", sellerName);
	}

%>


<%-- ********************* HTML for page begins here *********************  --%>
<jsp:include page="/common/Header.jsp">
			   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
			   
			</jsp:include>
<div class="pageMainNoSidebar">
		<div class="pageContent">
		<jsp:include page="/common/PageHeader.jsp">
                     <jsp:param name="titleKey" value="AtpInvoiceSearch.AssignInvoices" />
                 </jsp:include>
<form name="AddInvoicesForm" method="POST" action="<%=formMgr.getSubmitAction(response)%>">

  				

 <jsp:include page="/common/ErrorSection.jsp" /> 
<div class="formContentNoSidebar">
<%=formMgr.getFormInstanceAsInputField("AddInvoicesForm", secureParms)%> 
<input type=hidden
	name="buttonName" value=""> <input type=hidden
	name="hasInvoices" value="<%=hasInvoices%>">

<%@ include file="/transactions/fragments/ViewInvoicesSearch.frag"%>

<%
gridLayout = dgFactory.createGridLayout("addInvoiceDataDataGridId", "ViewInvoicesDataGrid");
gridHtml = dgFactory.createDataGrid("addInvoiceDataDataGridId","ViewInvoicesDataGrid",null);
%>
<%=gridHtml%>
</div>

</form>
</div>
</div>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  
</jsp:include>
    
 <script type='text/javascript'>
 require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
		var gridLayout = <%= gridLayout %>;
								
		var initSearchParms="userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
			initSearchParms+="&transactionTypeCode=<%=transaction.getAttribute("transaction_type_code")%>";
			initSearchParms+="&hasInvoices=<%=hasInvoices%>";
			initSearchParms+="&uploadDefinitionOid=<%=uploadInvDefOid%>";
			initSearchParms+="&currency=<%=currency%>";
			initSearchParms+="&beneficiaryName=<%=sellerName%>";
			initSearchParms+="&validDate=<%=validDate%>";
			initSearchParms+="&tranOid=<%=transaction.getAttribute("transaction_oid")%>";
			initSearchParms+="&fromAddInv=N";
		
	
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ViewInvoicesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		var invoiceGrid =  onDemandGrid.createOnDemandGrid("addInvoiceDataDataGridId", viewName,gridLayout, initSearchParms,'0'); 
});
		
		function filterPOs(){
			 require(["dojo/dom", "t360/OnDemandGrid", "dojo/dom-construct"],
				      function(dom, onDemandGrid, domConstruct){
			console.log("Inside filterPOs()");
			
			var benName = dom.byId("BeneName").value;
			var invID = dom.byId("InvID").value;
			var dateTo = dom.byId("DateTo").value;
			var dateFrom = dom.byId("DateFrom").value;
			 var currency=(dijit.byId("Currency").value).toUpperCase();
			var amountFrom = dom.byId("AmountFrom").value;
			var amountTo = dom.byId("AmountTo").value;
			var searchParms= "userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";
			searchParms+="&sellerName="+benName;
			searchParms+="&InvID="+invID;
			searchParms+="&DateTo="+dateTo;
			searchParms+="&DateFrom="+dateFrom;
			searchParms+="&Currency="+currency;
			searchParms+="&AmountFrom="+amountFrom;
			searchParms+="&AmountTo="+amountTo;
			var tempDatePattern = encodeURI('<%=dPattern%>');
	 		searchParms=searchParms+"&dPattern="+tempDatePattern;
			console.log("searchParms="+searchParms);

			onDemandGrid.searchDataGrid("addInvoiceDataDataGridId", "ViewInvoicesDataView", searchParms);
			 });
			 }
		
		function addInvoiceData(){
			var rowKeys = getSelectedGridRowKeys("addInvoiceDataDataGridId"),
			    buttonName = '<%=TradePortalConstants.BUTTON_ADD_PO_LINE_ITEMS%>',
			    formName = 'AddInvoicesForm';
			
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
		}
		
		function closeStructurePODataItems(){
			openURL("<%=link %>");
		}
		function openURL(URL){
            if (isActive =='Y') {
            	if (URL != '' && URL.indexOf("javascript:") == -1) {
            		var cTime = (new Date()).getTime();
	                URL = URL + "&cTime=" + cTime;
    	            URL = URL + "&prevPage=" + context;
            	}
            }
			 document.location.href  = URL;
			 return false;
		}
	
	</script>   
	
<jsp:include page="/common/dGridShowCountFooter.jsp">
  <jsp:param name="gridId" value="addInvoiceDataDataGridId" />
</jsp:include>
</body>
</html>

<%
DocumentHandler xml = formMgr.getFromDocCache();
xml.removeComponent("/Error");

xml.removeComponent("/In/POLineItemList");
// Finally, reset the cached document to eliminate carryover of
// information to the next visit of this page.
formMgr.storeInDocCache("default.doc", xml);
%>
