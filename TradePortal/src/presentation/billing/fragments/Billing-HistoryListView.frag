<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Billing Home History Tab

  Description:
    Contains HTML to create the History tab for the Transactions Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Billing-HistoryListView.jsp" %>
*******************************************************************************
--%> 


<%-- add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">
<%
  // use constant instead of translated value for ALL
  final String ALL_ORGS = "AuthorizedTransactions.AllOrganizations";

  //When the Instrument History tab is selected, display
  // Organizations dropdown or the single organization.
  if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) 
       && (totalOrganizations > 1)) {

    dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc,"ORGANIZATION_OID", "NAME",selectedOrg, userSession.getSecretKey()));
    dropdownOptions.append("<option value=\"");
    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGS, userSession.getSecretKey()));
    dropdownOptions.append("\"");

    if (selectedOrg.equals(ALL_ORGS)) {
       dropdownOptions.append(" selected");
    }

    dropdownOptions.append(">");
    dropdownOptions.append(resMgr.getText(ALL_ORGS, TradePortalConstants.TEXT_BUNDLE));
    dropdownOptions.append("</option>");
    
%>
      <%=widgetFactory.createSearchSelectField("Organization","InstrumentHistory.Show","", dropdownOptions.toString(), 
           "onChange='local.searchInstruments();'")%>
<%
  }
	
  StringBuffer instrumentStatus = new StringBuffer();
  instrumentStatus.append(widgetFactory.createInlineLabel("", "InstrumentHistory.Status"));
  instrumentStatus.append(widgetFactory.createCheckboxField("InstrStatusTypeActive", "common.StatusActive", true, 
    false, false,"onClick='local.searchInstruments();'","","none"));
  instrumentStatus.append(widgetFactory.createCheckboxField("InstrStatusTypeInactive", "common.StatusInactive", false, 
    false,false,"onClick='local.searchInstruments();'","","none"));
  widgetFactory.wrapSearchItem(instrumentStatus,"");
%>	
	
      <%= instrumentStatus %>
      <div style="clear:both;"></div>
    </span>
    <span class="searchHeaderActions">
	

      <%-- include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="billingInstrSearchGridId" />
      </jsp:include>

    </span>
    <div style="clear:both;"></div>
  </div>
  
  <div class="searchDivider"></div>
	
<%    
  String linkName = "Basic";
  String paramStr = "&current2ndNav="+current2ndNav+"&SearchType=";
  if (searchType == null) {
    searchType = TradePortalConstants.SIMPLE;
    paramStr=paramStr+TradePortalConstants.ADVANCED;
  } else if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkName = "Basic";
    paramStr=paramStr+TradePortalConstants.SIMPLE;
  } else if (searchType.equals(TradePortalConstants.SIMPLE)) {
    linkName = "Advanced";
    paramStr=paramStr+TradePortalConstants.ADVANCED;
  }
	
  String linkHref = formMgr.getLinkAsUrl("goToTradeTransactionsHome", paramStr, response);
%>
  	
  <%--include both advanced and basic, set the non-used one to display:none--%>
  <%@ include file="/billing/fragments/BillingSearch-AdvancedFilter.frag" %>
  <%@ include file="/billing/fragments/BillingSearch-BasicFilter.frag" %>

  <input type=hidden name=SearchType value=<%=searchType%>>

</div>
  
<%
  dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  gridHtml = dgFactory.createDataGrid("billingInstrSearchGridId","BillingInstrHistoryDataGrid",null);
%> 
<%= gridHtml %>

