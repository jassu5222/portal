<%--
*******************************************************************************
                  Billing Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
  
  IMPORTANT NOTE: This page may currently come from the following:
  
  Create Transaction Step 1 Page: for an existing instrument or copying an
  			instrument
  Export LC Issue Transfer: Search Instrument button
  MessageHome: Search Instrument
  
  Each originating page should contain the following code:
  
    <input type="hidden" name="NewSearch" value="Y">
  
  - this will allow a new search criteria to be created.

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<div id="advancedInstrumentFilter" style="display:none;"> <%--default advanced to non-display--%>
  <div class="searchDetail">
  
  <%=widgetFactory.createSearchDateField("startDateFrom","InstSearch.StartDateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
		<%=widgetFactory.createSearchDateField("startDateTo","InstSearch.To", " class='char8' ", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
		
		
		<%=widgetFactory.createSearchDateField("closeDateFrom","Billing.ClosrDateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    	<%=widgetFactory.createSearchDateField("closeDateTo","InstSearch.To", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    
    
    <span class="searchActions">
      <button id="advanceSearchId" class="gridSearchButton" data-dojo-type="dijit.form.Button" type="button" >
        <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          local.searchInstruments("Advanced");return false;
        </script>
      </button>

      <%-- <a class="searchTypeToggle" href="<%=linkHref%>" ><%=linkName%></a>--%>
      <div id="basic"><a class="searchTypeToggle" href="javascript:local.shuffleFilter('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a></div>
    </span>
    
     <%=widgetFactory.createHoverHelp("advanceSearchId","SearchHoverText") %>
     <%=widgetFactory.createHoverHelp("basic","BasicSearchHypertextLinkHoverText") %>
			
    	<div style="clear:both"></div>
</div>
<div>
&nbsp;
</div>
</div>
