<%--
*******************************************************************************
                               Cash Management Tab

  Description:  
     
   
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="java.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 
<%
  
    String userSecurityRights = userSession.getSecurityRights();
    String userOrgOid         = userSession.getOwnerOrgOid();

    // Construct the Corporate Organization
    CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    corpOrg.getById(userOrgOid);
    
    // Get Corporate Organization rights.  allow_funds_transfer equates to allowing InternationalPayments.
    boolean allowPymtInstrumentAuth = corpOrg.getAttribute("allow_pymt_instrument_auth").equals(TradePortalConstants.INDICATOR_YES);
        
    boolean allowXferBtwnAccts = false;
    boolean allowDomesticPayments = false;
    boolean allowInternationalPayments = false;
    
    request.setAttribute("panelEnabled", TradePortalConstants.INDICATOR_NO);	    //IAZ CM-451 03/16/09 Add
    if(corpOrg.getAttribute("allow_panel_auth_for_pymts").equals(TradePortalConstants.INDICATOR_YES)){
      allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts_panel").equals(TradePortalConstants.INDICATOR_YES);   
      allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments_panel").equals(TradePortalConstants.INDICATOR_YES);   
      allowInternationalPayments = corpOrg.getAttribute("allow_funds_transfer_panel").equals(TradePortalConstants.INDICATOR_YES);
      request.setAttribute("panelEnabled", TradePortalConstants.INDICATOR_YES);	    //IAZ CM-451 03/16/09 Add
	  //out.println("CBar: " + (String)request.getAttribute("panelEnabled"));    	    	
      
    } else {
      allowXferBtwnAccts = corpOrg.getAttribute("allow_xfer_btwn_accts").equals(TradePortalConstants.INDICATOR_YES);
      allowDomesticPayments = corpOrg.getAttribute("allow_domestic_payments").equals(TradePortalConstants.INDICATOR_YES);     
      allowInternationalPayments = corpOrg.getAttribute("allow_funds_transfer").equals(TradePortalConstants.INDICATOR_YES);    
    }

    // Get permissions from the Security Profile and merge permissions with the Corporate Organization rights.
    boolean canSeeXferBtwnAccts         = ( SecurityAccess.hasRights(userSecurityRights, SecurityAccess.TRANSFER_CREATE_MODIFY) && allowXferBtwnAccts );
    boolean canSeeDomesticPayments      = ( SecurityAccess.hasRights(userSecurityRights, SecurityAccess.DOMESTIC_CREATE_MODIFY) && allowDomesticPayments);
    boolean canSeeInternationalPayments = ( SecurityAccess.hasRights(userSecurityRights, SecurityAccess.FUNDS_XFER_CREATE_MODIFY) && allowInternationalPayments);
    

    // Check Instrument capabilities if only international payments is selected.
    // Do not display the tab unless the Cash Management toolbar is available
/* ctq - comment this out, this page is unused
    if(canSeeInternationalPayments && !(canSeeXferBtwnAccts || canSeeDomesticPayments)){
        //TRUDDEN IR NLUJ042266006 Add Begin  
        String toolbarVisbility = corpOrg.tabVisible();
        if ((TradePortalConstants.ONLY_INSTRUMENT_INT_TAB_VISIBLE.equals(toolbarVisbility)) ||
          (TradePortalConstants.INSTRUMENT_CASH_TAB_VISIBLE.equals(toolbarVisbility) &&
           SecurityAccess.canViewTradeInstrument(userSecurityRights))) {
             //don't display cashMgt tab, because the Cash Management toolbar is not available
             canSeeInternationalPayments = false;
        }
        //TRUDDEN IR NLUJ042266006 End
    }
*/

    boolean displayCashManagementTab = allowPymtInstrumentAuth && (canSeeXferBtwnAccts || canSeeDomesticPayments || canSeeInternationalPayments);
    
    // IAZ CM-451 03/16/09 Begin
    // Check if panel enabled for either this corp org or parent corp org.
    // This will be used to determine if authorize/authenticate are to be allowed for subsidiary if this is a subsidiary access
    if (TradePortalConstants.INDICATOR_NO.equals((String)request.getAttribute("panelEnabled")))
    {
		String parentCorpOrgOid = corpOrg.getAttribute("parent_corp_org_oid");
    	if (InstrumentServices.isNotBlank(parentCorpOrgOid))
    	{
	    	CorporateOrganizationWebBean parentCorpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
    	    parentCorpOrg.getById(parentCorpOrgOid);
    	    if ((parentCorpOrg != null)&&(TradePortalConstants.INDICATOR_YES.equals(parentCorpOrg.getAttribute("allow_panel_auth_for_pymts"))))
    	    {
		      request.setAttribute("panelEnabled", TradePortalConstants.INDICATOR_YES);
		      //out.println("CMBar: " + (String)request.getAttribute("panelEnabled"));    	    	
    	    }
    	}
   	}
   	//IAZ CM-451 03/16/09 End
    	
%>
 
<%
  if(displayCashManagementTab){  
    
    // Initialize form variables
    String formName = "CashMgmtTabForm";
    Hashtable secureParms = new Hashtable();  
    secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
    secureParms.put("userOid",userSession.getUserOid());
    secureParms.put("securityRights",userSecurityRights); 
    secureParms.put("clientBankOid",userSession.getClientBankOid()); 
    secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
    
 %>

<input type=hidden name="bankBranch" value='<%=corpOrg.getAttribute("first_op_bank_org")%>'>
<%-- input type=hidden name="userOid" value='<%=userSession.getUserOid()%>' --%>
<%-- input type=hidden name="securityRights" value="<%=userSecurityRights%>" --%>
<%-- input type=hidden name="clientBankOid" value="<%=userSession.getClientBankOid()%>" --%>
<%-- input type=hidden name="ownerOrg" value="<%=userSession.getOwnerOrgOid()%>" --%>


<%-- form method="post" name="<%=formName%>" action="<%=formMgr.getSubmitAction(response)%>" --%>
<%-- %= formMgr.getFormInstanceAsInputField(formName, secureParms) % --%> 
<input type=hidden name="instrumentType">

<%
    String acctBalLink        = formMgr.getLinkAsUrl("goToAccountsHome", response) + "&amp;current2ndNav="  + TradePortalConstants.NAV__ACCT_BALANCE;    
    String reportsLink        = formMgr.getLinkAsUrl("goToReportsHomeViaIntermediatePage", response);
%>

<div id="CashManagementTabDiv" class=ColorBeige style="width: 130px">
  <p class="ControlLabel"><%=resMgr.getText("Accounts.Click", TradePortalConstants.TEXT_BUNDLE)%> </p>
  <table width="100%" cellpadding="4">
    <tr>
      <td class="Links" style="height: 30px;"><a href="<%=acctBalLink%>"><%=resMgr.getText("Accounts.ACCOUNT_BALANCE", TradePortalConstants.TEXT_BUNDLE)%></a></td>
    </tr>
<% if(canSeeXferBtwnAccts){ %>      
    <tr>
      <td class="Links" style="height: 30px;">
        <a href='javascript:submitFormForNewTransaction("<%=InstrumentType.XFER_BET_ACCTS%>")'><%=resMgr.getText("Accounts.TRF_BTW_ACCOUNT", TradePortalConstants.TEXT_BUNDLE)%></a>
      </td>
    </tr>
<% } %>     
<% if(canSeeDomesticPayments){ %>  
    <tr>
      <td class="Links" style="height: 30px;">
        <a href='javascript:submitFormForNewTransaction("<%=InstrumentType.DOMESTIC_PMT%>")'><%=resMgr.getText("Accounts.DTC_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%></a>
      </td>
    </tr>  
<% } %>     
<% if(canSeeInternationalPayments){ %>  
    <tr>
      <td class="Links" style="height: 30px;">
        <a href='javascript:submitFormForNewTransaction("<%=InstrumentType.FUNDS_XFER%>")'><%=resMgr.getText("Accounts.INTL_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%></a>
      </td>
    </tr>
<% } %>  
<% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.ACCESS_REPORTS_AREA)){ 
     if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT) || SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_OR_MAINTAIN_CUSTOM_REPORT)){%>
    <tr>
      <td class="Links" style="height: 30px;"><a href="<%=reportsLink%>"><%=resMgr.getText("Accounts.REPORTS", TradePortalConstants.TEXT_BUNDLE)%></a></td>
    </tr>  
<% } }%>  
  </table>

</div>
<%
  }
%>  

<script language="javascript">
  function submitFormForNewTransaction(instType){
    document.forms[0].instrumentType.value=instType;
    setButtonPressed('AddNewTrans', 0)
    <%-- window.alert(document.forms[0].instrumentType.value) --%>
    <%-- window.alert(document.forms[0].name.value) --%>
    document.forms[0].submit();
  }
</script>
