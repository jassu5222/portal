<%--
*****************************************************************************************************
                  Cash Management Transaction Search Advanced Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Advanced Filter fields for the 
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  
 
  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

********************************************************************************************************
--%>
  <input type="hidden" name="NewSearch" value="Y">

  <div id="advancePmtFilter" class="searchDetail">
   <div>
    <div class="searchCriteria">
		<%options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
		<%-- changed event to window.event to all the fields in order for enter key to work in Firefox --%>    
		<%=widgetFactory.createSearchSelectField("advInstrumentType","CashMgmtTransactionSearch.TransactionType"," ", options, " class='char20' onKeydown='filterpaymentInquiryOnEnter(window.event,\"advInstrumentType\", \"Advanced\");' ")%>
		<% options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale);%>
     
	    <%=widgetFactory.createSearchSelectField("Currency","CashMgmtTransactionSearch.Currency"," ", options, " class='char25' onKeydown='filterpaymentInquiryOnEnter(window.event,\"Currency\", \"Advanced\");' ")%>
		<%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
    </div>
    <div class="searchActions">
      <button id="searchButtonAdvPmt" data-dojo-type="dijit.form.Button" type="button" class="SearchButton">
        <%= resMgr.getText("common.searchButton",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          local.paymentInquiryAdvanceSearch();return false;
        </script>
      </button>
	  <%=widgetFactory.createHoverHelp("searchButtonAdvPmt","SearchHoverText") %>
	   <br>
      <a id="searchLinkAdvPmt" class="searchTypeToggle" href="javascript:local.shuffleFilterPmtSearch('Basic');"><%=resMgr.getText("ExistingDirectDebitSearch.Basic", TradePortalConstants.TEXT_BUNDLE)%></a>
			
    </div>
    <%=widgetFactory.createHoverHelp("searchButtonAdvPmt","SearchHoverText") %>
    	<%=widgetFactory.createHoverHelp("searchLinkAdvPmt","common.BasicSearchHypertextLink") %>
    <div style="clear:both"></div>
  </div>

	
  <div class="searchDetail lastSearchDetail">
      <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
      <div class="searchCriteria">
		<%=widgetFactory.createSearchAmountField("AmountFrom", "ExistingDirectDebitSearch.AmountFrom","","class='char15'  onKeydown='filterpaymentInquiryOnEnter(window.event,\"AmountFrom\", \"Advanced\");' class='char6 inline'")%>
		<%=widgetFactory.createSearchAmountField("AmountTo", "CashMgmtTransactionSearch.To","","class='char15'  onKeydown='filterpaymentInquiryOnEnter(window.event,\"AmountTo\", \"Advanced\");' class='char6 inline'")%>
		<%-- DK CR-886 Rel8.4 10/15/2013 starts --%>		
    	<%=widgetFactory.createSearchDateField("PayDateFrom","CashMgmtTransactionSearch.PayDateFrom", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    	<%=widgetFactory.createSearchDateField("PayDateTo","CashMgmtTransactionSearch.PayDateTo", " class='char8'", "constraints:{datePattern:'"+datePattern+"'},placeHolder:'"+datePatternDisplay+"'")%>
    	<%-- DK CR-886 Rel8.4 10/15/2013 ends --%>
		<%=widgetFactory.createSearchTextField("OtherParty","CashMgmtTransactionSearch.OtherParty","30", "class='char20' onKeydown='filterpaymentInquiryOnEnter(window.event,\"OtherParty\", \"Advanced\");'")%>			
      </div> 
      <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>
    
    <div style="clear:both"></div>
    <%	
    CorporateOrganization corpOrgObj = (CorporateOrganization)EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "CorporateOrganization");
    corpOrgObj.getData(Long.valueOf(userOrgOid).longValue());
    ComponentList externalBankComponenet = (ComponentList) corpOrgObj.getComponentHandle("ExternalBankList");
    int extBankTotal = externalBankComponenet.getObjectCount();

    BusinessObject externalBankBusinessObj = null;
    String externalBankBranches = "";
    String externalBankOid = null;
    DocumentHandler extBankDoc = null;
    java.util.List sqlParamsLst = new java.util.ArrayList();

    for(int i=0; i<extBankTotal; i++){
    	
    	externalBankComponenet.scrollToObjectByIndex(i);
    	externalBankBusinessObj = externalBankComponenet.getBusinessObject();
    	externalBankOid = externalBankBusinessObj.getAttribute("op_bank_org_oid");		
    	  
    	      if (!InstrumentServices.isBlank(externalBankOid)) {    	     
    	          if ( externalBankBranches.length()>0 ) {    	        
    	        	  externalBankBranches += ",";    	        	  
    	          }	         
    	          externalBankBranches += "?";    	
    	          sqlParamsLst.add(externalBankOid);           
    	      }
    	      else {
    	          break; //if a blank is found there will be no more
    	           
    	      }		
    }
    //Build query to get all external bank's 
    try {
        StringBuilder extBanksql = new StringBuilder();
        extBanksql.append("select ORGANIZATION_OID, NAME");
        extBanksql.append(" from OPERATIONAL_BANK_ORG");
        extBanksql.append(" where ORGANIZATION_OID in (").append(externalBankBranches).append(")");
        extBanksql.append(" order by name");
            
		if(StringFunction.isNotBlank(externalBankBranches)){
        	extBankDoc = DatabaseQueryBean.getXmlResultSet(extBanksql.toString(), true, sqlParamsLst);
        }
        

      } catch (AmsException e) {
        //todo: should throw an error
        Debug.debug("InstSearch-Advance:Exception in Getting External Bank");
        e.printStackTrace();
      }

    
    options = ListBox.createOptionList(extBankDoc, "ORGANIZATION_OID", "NAME", "", null);
  out.print(widgetFactory.createSearchSelectField( "ExternalBankDropDownId", "CashMgmtTransactionSearch.BankName", " ", options, ""));
  
  %>
    <div class="searchCriteria">
    
    </div>
  </div>
  
 </div>
 
<div style="clear:both"></div>
