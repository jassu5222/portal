<%--
*******************************************************************************
                               Account Balance Page

  Description:  **ToDo**
  
  

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<table>
<tr>
<td class="ListHeaderText">

                              <%=resMgr.getText("Accounts.Click", TradePortalConstants.TEXT_BUNDLE)%> 

</td>
</tr>
<tr>
<td height=30>
&nbsp;
</td>
</tr>
<tr>
<td height=40 valign= center>
  <%
                         if (cashSelection.equals(TradePortalConstants.ACCT_BALANCE))
                         {
                      %>
                            
                            <u class="ListHeaderText">
                              <%=resMgr.getText("Accounts.ACCOUNT_BALANCE", TradePortalConstants.TEXT_BUNDLE)%> 
                            </u>
                      <%
                         }
                         else
                         {
                            
                            newLink = new StringBuffer();
                            newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
                            newLink.append("&cashSelection=" + TradePortalConstants.ACCT_BALANCE);
                      %>
                            
                            <a href="<%= newLink.toString() %>"> 
                              <%=resMgr.getText("Accounts.ACCOUNT_BALANCE", TradePortalConstants.TEXT_BUNDLE)%> 
                            </a>
                      <%
                         }
                      %>
  


</td>
</tr>
<tr>
<td height=40 valign= center>
  <%
                         if (cashSelection.equals(TradePortalConstants.TRF_BTW_ACCOUNT))
                         {
                      %>
                            
                             <u class="ListHeaderText">
                              <%=resMgr.getText("Accounts.TRF_BTW_ACCOUNT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </u>
                      <%
                         }
                         else
                         {
                            newLink = new StringBuffer();
                            newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
                            newLink.append("&cashSelection=" + TradePortalConstants.TRF_BTW_ACCOUNT);
                      %>
                            
                            <a href="<%= newLink.toString() %>"> 
                              <%=resMgr.getText("Accounts.TRF_BTW_ACCOUNT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </a>
                      <%
                         }
                      %>
</td>
</tr>
<tr>
<td height=40 valign= center>
<%
                         if (cashSelection.equals(TradePortalConstants.DTC_PAYMENT))
                         {
                      %>
                            
                             <u class="ListHeaderText">
                              <%=resMgr.getText("Accounts.DTC_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </u>
                      <%
                         }
                         else
                         {
                            newLink = new StringBuffer();
                            newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
                            newLink.append("&cashSelection=" + TradePortalConstants.DTC_PAYMENT);
                      %>
                            
                            <a href="<%= newLink.toString() %>"> 
                              <%=resMgr.getText("Accounts.DTC_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </a>
                      <%
                         }
                      %>
</td>
</tr>
<tr>
<td height=40 valign= center>
<%
                         if (cashSelection.equals(TradePortalConstants.INTL_PAYMENT))
                         {
                      %>
                            
                             <u class="ListHeaderText">
                              <%=resMgr.getText("Accounts.INTL_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </u>
                      <%
                         }
                         else
                         {
                            newLink = new StringBuffer();
                            newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
                            newLink.append("&cashSelection=" + TradePortalConstants.INTL_PAYMENT);
                      %>
                            
                            <a href="<%= newLink.toString() %>"> 
                              <%=resMgr.getText("Accounts.INTL_PAYMENT", TradePortalConstants.TEXT_BUNDLE)%> 
                            </a>
                      <%
                         }
                      %>
</td>
</tr>
<tr>
<td height=40 valign= center>
<%
                         if (cashSelection.equals(TradePortalConstants.REPORTS))
                         {
                      %>
                            
                             <u class="ListHeaderText">
                              <%=resMgr.getText("Accounts.REPORTS", TradePortalConstants.TEXT_BUNDLE)%> 
                            </u>
                      <%
                         }
                         else
                         {
                            newLink = new StringBuffer();
                            newLink.append(formMgr.getLinkAsUrl("goToCashManagement", response));
                            newLink.append("&cashSelection=" + TradePortalConstants.REPORTS);
                      %>
                            
                            <a href="<%= newLink.toString() %>"> 
                              <%=resMgr.getText("Accounts.REPORTS", TradePortalConstants.TEXT_BUNDLE)%> 
                            </a>
                      <%
                         }
                      %>
</td>
</tr>
</table>