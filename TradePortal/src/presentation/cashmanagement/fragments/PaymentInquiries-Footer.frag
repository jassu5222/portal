<%--
*******************************************************************************
  Cash Management Payment Inquiries Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  //this gridLayout is unused for now
  //see hardcoding below
  //String gridLayout = dgFactory.createGridLayout("paymentInquiriesID", "PaymentInquiriesDataGrid");

%>

<script type="text/javascript">

  var local = {};

  var instrument="";

  var instrType;

  function dialogQuickView(rowKey){
	 var dialogDiv = document.createElement("div");
	 dialogDiv.setAttribute('id',"quickviewdialogdivid");
	 document.forms[0].appendChild(dialogDiv);
	 
	 var title="";
	 
	require(["t360/dialog"], function(dialog ) {
		
       dialog.open('quickviewdialogdivid',title ,
                   'TransactionQuickViewDialog.jsp',
                   ['rowKey','DailogId'],[rowKey,'quickviewdialogdivid'], <%-- parameters --%>
                   'select', quickViewCallBack);
     });
  }

  var quickViewFormatter=function(columnValues, idx, level) {
	var gridLink="";
	
	if(level == 0){
		
		if ( columnValues.length >= 2 ) {
		      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
		    }
		    else if ( columnValues.length == 1 ) {
		      <%-- no url parameters, just return the display --%>
		      gridLink = columnValues[0];
		    }
		    return gridLink;
      
 }else if(level ==1){
	  	console.log(columnValues[3]);
	  	if(columnValues[3]=='PROCESSED_BY_BANK'){
	  		if(columnValues[2]){
	  			return "<a href='javascript:dialogQuickView("+columnValues[2]+");'>Quick View</a> "+columnValues[0]+ " ";
	  		}else{
	  			return columnValues[0];	
	  		}
	    
	  	}else{
				return columnValues[0];
		  	}
   
    }
  }
  var formatGridLinkChild=function(columnValues, idx, level){
    var gridLink="";
	
	if(level == 0){
		return columnValues[0];
      
    }else if(level ==1){
	  if ( columnValues.length >= 2 ) {
	      gridLink = "<a href='" + columnValues[1] + "'>" + columnValues[0] + "</a>";
	    }
	    else if ( columnValues.length == 1 ) {
	      <%-- no url parameters, just return the display --%>
	      gridLink = columnValues[0];
	    }
	    return gridLink;
	    }
  }

  <%-- function to set the title after ajax call of dialog value[2]- dialogId, value[0]-complete instrument id, value[1]-transaction type --%>
  function quickViewCallBack(key,value){
	dijit.byId(value[2]).set('title','Transaction Quick View - '+value[0]+' - '+value[1]);
	<%-- commented as this dialog gets minimize automatically  --%>
	<%-- dijit.byId(value[2]).set('style','width:600px'); --%>
	
  }

  var gridLayout=[{name:'<%=resMgr.getTextEscapedJS("AllTransactions.InstrumentID",TradePortalConstants.TEXT_BUNDLE)%>', field:"InstrumentId",fields:["InstrumentId","InstrumentId_linkUrl","transaction_oid","STATUSFORQVIEW","Type"], formatter:quickViewFormatter, width:"125px"} ,
    {name:'<%=resMgr.getTextEscapedJS("AllTransactions.InstrumentType",TradePortalConstants.TEXT_BUNDLE)%>', field:"Type", fields:["Type","Type_linkUrl"], formatter:formatGridLinkChild, width:"150px"} ,
    {name:'<%=resMgr.getTextEscapedJS("CashMgmtTransactionHistory.CCY",TradePortalConstants.TEXT_BUNDLE)%>', field:"CCY", width:"40px"} ,
    {name:'<%=resMgr.getTextEscapedJS("CashMgmtTransactionHistory.Amount",TradePortalConstants.TEXT_BUNDLE)%>', field:"Amount", width:"100px", cellClasses:"gridColumnRight"} ,
    {name:'<%=resMgr.getTextEscapedJS("CashMgmtTransactionHistory.Status",TradePortalConstants.TEXT_BUNDLE)%>', field:"Status", width:"55px"} ,
    {name:'<%=resMgr.getTextEscapedJS("CashMgmtTransactionHistory.PrimaryRefNo",TradePortalConstants.TEXT_BUNDLE)%>', field:"PrimaryReference", width:"150px"},
    {name:'<%=resMgr.getTextEscapedJS("CashMgmtTransactionHistory.Party",TradePortalConstants.TEXT_BUNDLE)%>', field:"Party", width:"150px"} ];

  var selectedOrg="";
  var instrStatusType = "";
  <%-- userLocale is not used in dataView --%>
 <%-- var init='loginLocale=<%=loginLocale%>'; --%>
 var init="";
  var userOrgOid = '<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>';

  var instrumentId = "";
  var instrumentType = "";
  var refNum = "";
  var currency = "";
  var amountFrom = "";
  var amountTo = "";
  var otherParty = "";
  var externalBankOid = ""; <%-- Kyriba CR 268 --%>
  <%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
  var payDateFrom = "";
  var payDateTo = "";
  <%--  DK CR-886 Rel8.4 10/15/2013 ends --%>

  var savedSort = savedSearchQueryData["sort"];
   searchType = savedSearchQueryData["searchType"];
   instrStatusType = savedSearchQueryData["instrStatusType"];
   selectedOrg = savedSearchQueryData["selectedOrg"];
  
  function initializeInqDataGrid(){
  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"], 
      function(dom, registry, t360grid) {
	  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg)
    selectedOrg = checkString( registry.byId('Organization') );	
	  else{
		  registry.byId("Organization").set('value',selectedOrg);
	  }
	  if("" == instrStatusType || null == instrStatusType || "undefined" == instrStatusType)
   	 instrStatusType = checkString( registry.byId('instrStatusType') );
	  else
		  registry.byId("instrStatusType").set('value',instrStatusType);
	  
	  if("" == savedSort || null == savedSort || "undefined" == savedSort)
		  savedSort = '0';
    if("" == selectedOrg || null == selectedOrg){
      selectedOrg = userOrgOid;
    }
    if("" == searchType || null == searchType || "undefined" == searchType){
    	searchType = registry.byId('SearchType');
      }
    if('A'==searchType){
    	 amountFrom = savedSearchQueryData["amountFrom"];
    	 amountTo = savedSearchQueryData["amountTo"];
    	 otherParty = savedSearchQueryData["otherParty"];
    	 instrumentType = savedSearchQueryData["instrumentType"];
    	 currency = savedSearchQueryData["currency"];
    	 <%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
    	 payDateFrom = savedSearchQueryData["payDateFrom"];
    	 payDateTo = savedSearchQueryData["payDateTo"];
    	 <%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
    	if("" == currency || null == currency || "undefined" == currency)
    		currency =checkString( dijit.byId("Currency") );
    	else
    		registry.byId("Currency").set('value',currency);
    	
    	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
    		instrumentType = checkString( dijit.byId("advInstrumentType") );
    	else
    		registry.byId("advInstrumentType").set('value',instrumentType);
    	
    	 if("" == amountFrom || null == amountFrom || "undefined" == amountFrom)
    		 amountFrom = checkString( dijit.byId("AmountFrom") );
    	 else
    		 dom.byId("AmountFrom").value=amountFrom;
    	 
    	 if("" == amountTo || null == amountTo || "undefined" == amountTo)
    		 amountTo = checkString( dijit.byId("AmountTo") );
    	 else
    		 dom.byId("AmountTo").value=amountTo;
    	 
    	 if("" == otherParty || null == otherParty || "undefined" == otherParty)
    		 otherParty = checkString( dijit.byId("OtherParty") );
    	 else
    		dom.byId("OtherParty").value=otherParty;
    	 
    	<%--  DK CR-886 Rel8.4 10/15/2013 starts   --%>
    	if("" == payDateFrom || null == payDateFrom || "undefined" == payDateFrom)
    		payDateFrom = dom.byId("PayDateFrom").value;   	
    	 else
    		 registry.byId("PayDateFrom").set('displayedValue',payDateFrom);
    	 
    	 if("" == payDateTo || null == payDateTo || "undefined" == payDateTo)
    		 payDateTo = dom.byId("PayDateTo").value;  
    	 else
    		 registry.byId("PayDateTo").set('displayedValue',payDateTo);
    	<%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
     	 
         dom.byId("advancePmtFilter").style.display='block';
         dom.byId("basicPmtFilter").style.display='none';
    	 <%--  DK CR-886 Rel8.4 10/15/2013 update --%>
    	 var tempDatePattern = encodeURI('<%=datePattern%>');
         init = "currency="+currency+"&instrumentType="+instrumentType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&otherParty="+otherParty+
         "&payDateFrom="+payDateFrom+"&payDateTo="+payDateTo+"&dPattern="+tempDatePattern;
    }
    else{
    	 refNum = savedSearchQueryData["refNum"];	
    	 instrumentType = savedSearchQueryData["instrumentType"];
    	 instrumentId = savedSearchQueryData["instrumentId"];
    	if("" == instrumentId || null == instrumentId || "undefined" == instrumentId)
    		instrumentId = checkString( dijit.byId("InstrumentId") );
    	if("" == instrumentType || null == instrumentType || "undefined" == instrumentType)
    		instrumentType = checkString( dijit.byId("InstrumentType") );
    	else{
    		registry.byId("InstrumentType").set('value',instrumentType);
  	  	}
    	 if("" == refNum || null == refNum || "undefined" == refNum)
    		 refNum = checkString( dijit.byId("RefNum") );
    	 dom.byId("InstrumentId").value=instrumentId;
         dom.byId("RefNum").value=refNum;
    	 
         dom.byId("advancePmtFilter").style.display='none';
         dom.byId("basicPmtFilter").style.display='block';
    	init = "instrumentId="+instrumentId+"&instrumentType="+instrumentType+"&refNum="+refNum;
    }
		
    console.log("Inside initParms"+selectedOrg+" - "+instrStatusType);
    var initSearchParms = init+"&selectedOrg="+selectedOrg+"&instrStatusType="+instrStatusType+"&searchType="+searchType+"&confInd=<%=confInd%>&searchCondition=<%=searchCondition%>";
    console.log("initSearchParms--"+initSearchParms);
    <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
   var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentInquiriesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var paymentInquiriesID = t360grid.createLazyTreeDataGrid("paymentInquiriesID", viewName, gridLayout, initSearchParms,savedSort);
  
    <%-- Called when Organization/Status dropdown option changes --%>
    local.paymentInquirySearch = function(){

      var filterType = "Basic";
      if (dom.byId("advancePmtFilter").style.display == 'block') {
        filterType = "Advanced";
      }
      local.searchpaymentInquiryHistory(filterType);
    };

    local.searchpaymentInquiryHistory = function(filterType) {
      if(filterType=="Basic") 
        local.paymentInquiryBasicSearch();
      else 
        local.paymentInquiryAdvanceSearch();
    };
	
    <%-- Basic Search --%>
    local.paymentInquiryBasicSearch = function(){

      instrumentId = checkString( dijit.byId("InstrumentId") );
      instrumentType = checkString( dijit.byId("InstrumentType") );
      refNum = checkString( dijit.byId("RefNum") );
      
      selectedOrg = checkString( dijit.byId('Organization') );	
      instrStatusType = checkString( dijit.byId('instrStatusType') );
  	  
      <%--cquinton 4/9/2013 Rel PR ir#15704
          comment out defaulting org to user org, display nothing
      if("" == selectedOrg || null == selectedOrg){
        selectedOrg = userOrgOid;
      }
      --%>

      var searchParms = "selectedOrg="+selectedOrg+"&instrStatusType="+instrStatusType+"&instrumentId="+
        instrumentId+"&instrumentType="+instrumentType+"&refNum="+refNum+"&searchType=S"+"&confInd=<%=confInd%>";
      console.log('searchParms='+searchParms); 
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentInquiriesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      t360grid.searchLazyTreeDataGrid("paymentInquiriesID", viewName,
        gridLayout, searchParms);	
    };

    <%-- Advance Search --%>
    local.paymentInquiryAdvanceSearch = function(){

      instrumentType = checkString( dijit.byId("advInstrumentType") );
      currency = checkString( dijit.byId("Currency") );
      amountFrom = checkString( dijit.byId("AmountFrom") );
      amountTo = checkString( dijit.byId("AmountTo") );
      otherParty = checkString( dijit.byId("OtherParty") );
      externalBankOid = checkString( dijit.byId("ExternalBankDropDownId") );<%-- Kyriba CR 268 --%>
      <%--  DK CR-886 Rel8.4 10/15/2013 starts --%>
      <%--   DK IR T36000024022 Rel8.4 01/10/2014 starts --%>
 	  payDateFrom = dom.byId("PayDateFrom").value ;
 	  payDateTo =  dom.byId("PayDateTo").value ;
 	<%--   DK IR T36000024022 Rel8.4 01/10/2014 ends --%>
 	  <%--  DK CR-886 Rel8.4 10/15/2013 ends --%>
	 <%--  KMehta IR-T36000028449 Rel 9300 on 16-Jun-2015 Add - Begin --%>
      <%--  if (isNaN(amountFrom))        	amountFrom = "";
      if (isNaN(amountTo))           	amountTo = "";  --%>
 	 var regex = new RegExp(',', 'g'); <%-- Created reg exp to find comma globally --%>
     if(amountFrom!=null){
       amountFrom = amountFrom.replace(regex, '');		    	
     }	
     if(amountTo!=null){
       amountTo = amountTo.replace(regex, '');		    	
     }
     <%--  KMehta IR-T36000028449 Rel 9300 on 16-Jun-2015 Add - End --%>
      selectedOrg = checkString( dijit.byId('Organization') );	
      instrStatusType = checkString( dijit.byId('instrStatusType') );
  	  
      <%--cquinton 4/9/2013 Rel PR ir#15704
          comment out defaulting org to user org, display nothing
        if("" == selectedOrg || null == selectedOrg){
        selectedOrg = userOrgOid;
      }
      --%>
      <%-- DK IR T36000024022 Rel8.4 01/10/2014 - need to pass datePattern as param for search --%>
      var tempDatePattern = encodeURI('<%=datePattern%>'); 
      var searchParms = "selectedOrg="+selectedOrg+"&instrStatusType="+instrStatusType+"&currency="+
        currency+"&instrumentType="+instrumentType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+"&otherParty="+otherParty+"&searchType=A"+
        "&confInd=<%=confInd%>"+"&payDateFrom="+payDateFrom+"&payDateTo="+payDateTo+"&dPattern="+tempDatePattern+"&externalBankOid="+externalBankOid;;
      console.log('searchParms='+searchParms); 
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentInquiriesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      t360grid.searchLazyTreeDataGrid("paymentInquiriesID", viewName,
        gridLayout, searchParms);	
    };

    local.shuffleFilterPmtSearch = function(linkValue){
      console.log("inside shuffleFilter(): "+linkValue);
  		
      if(linkValue=='Advance'){
        dom.byId("advancePmtFilter").style.display='block';
        dom.byId("basicPmtFilter").style.display='none';
  	  				
        <%-- clearing Basic Filter --%>
        dom.byId("InstrumentId").value='';
        dom.byId("InstrumentType").value='';
        dom.byId("RefNum").value='';
        document.getElementById("SearchType").value = "A";
      }
      if(linkValue=='Basic'){
    	  dom.byId("advancePmtFilter").style.display='none';
    	  dom.byId("basicPmtFilter").style.display='block';
  	  				
        <%-- clearing Advance Filter --%>
        dom.byId("advInstrumentType").value='';
        dom.byId("Currency").value='';
        dom.byId("OtherParty").value='';
        dom.byId("AmountFrom").value='';
        dom.byId("AmountTo").value='';
        dom.byId("externalBankOid").value=''; <%-- Kyriba CR 268 --%>
         <%--  DK IR T36000024022 Rel8.4 01/10/2014 starts --%>
        dom.byId("PayDateFrom").value='';
        dom.byId("PayDateTo").value='';
        <%--  DK IR T36000024022 Rel8.4 01/10/2014 ends --%>
        document.getElementById("SearchType").value = "S";
        <%-- /this.resetDates(); --%>
      }
    };

  });
  
  }<%-- func end --%>
  
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  Start --%>
  <%-- Modified the function in order for enter key to work in Firefox --%>
  function filterpaymentInquiryOnEnter(evt, fieldId, filterType){
  	require(["dojo/on","dijit/registry"],function(on, registry) {
  		on(registry.byId(fieldId), "keypress", function(evt) {
	if(evt && evt.keyCode==13){
		local.searchpaymentInquiryHistory(filterType);
		}
  	 });
   });		
  }
  <%-- Kiran  05/07/2013  Rel8.2 IR-T36000014861  End --%>

<%-- Check for null validation .. --%>

function checkString(inquiryStr){
	if(null == inquiryStr)
		return "";
	else
		return inquiryStr.attr('value');
}

function openCopySelectedDialogHelper(callBackFunction){
    require(["t360/dialog"], function(dialog) {
      var selectedRow = getSelectedGridRowKeys("paymentInquiriesID");
      if(selectedRow.length == 0){
    	  alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("CopySelected.PromptMessage4",TradePortalConstants.TEXT_BUNDLE)) %>');
      }else if(selectedRow.length >1){
    	  alert('<%=resMgr.getTextEscapedJS("CopySelected.PromptMessage5",TradePortalConstants.TEXT_BUNDLE) %>');
      }else {
    	  dialog.open('copySelectedInstrument', '<%=resMgr.getTextEscapedJS("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>',
                  'copySelectedInstrumentDialog.jsp',
                  "instrumentType", instrType, <%-- parameters --%>
                  'select', callBackFunction);
      }	  
    });
}

function copySelectedToInstrument(bankBranchOid, copyType, templateName, templateGroup, flagExpressFixed){
	var rowKeys ;
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		      <%--get array of rowkeys from the grid--%>
		      rowKeys = getSelectedGridRowKeys("paymentInquiriesID");
	 });
	if(copyType == 'I'){ 
		if ( document.getElementsByName('NewInstrumentForm1').length > 0 ) {
		    var theForm = document.getElementsByName('NewInstrumentForm1')[0];
		    theForm.bankBranch.value=bankBranchOid;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.submit();
		  }
	}	
	if(copyType == 'T'){ 
		if ( document.getElementsByName('NewTemplateForm1').length > 0 ) {
			var flag = flagExpressFixed.split('/');
			var expressFlag = flag[0];
			var fixedFlag = flag[1];
			var theForm = document.getElementsByName('NewTemplateForm1')[0];
		    theForm.name.value=templateName;
		    theForm.mode.value="<%=TradePortalConstants.NEW_TEMPLATE%>";
		    theForm.CopyType.value="<%=TradePortalConstants.FROM_INSTR%>";
		    theForm.fixedFlag.value=fixedFlag;
		    theForm.expressFlag.value=expressFlag;
		    theForm.PaymentTemplGrp.value=templateGroup;
		    theForm.copyInstrumentOid.value=rowKeys;
		    theForm.validationState.value = "";
		    theForm.submit();
		  }
	}
}

require(["dijit/registry","t360/dialog","dojo/on", "t360/datagrid", "dojo/ready","dojo/_base/array"], 
		function(registry,dialog,on,datagrid, ready, baseArray) {
		ready(function(){
			initializeInqDataGrid();
		var myGrid = registry.byId("paymentInquiriesID");
		
		myGrid.on("SelectionChanged", function(){
		var items = this.selection.getSelected();
		baseArray.forEach(items, function(item){
			instrType = item.i.InstrumentTypeCode;
		});
		});
		
		myGrid.onCanSelect=function(idx){
			var selectedItem = this.getItem(idx).i;
			var isChild = (selectedItem.CHILDREN == "false");
			if (!isChild)  {
				if (selectedItem.TPSGen == "T") 	{
					registry.byId("CopySelected").setAttribute("disabled",true);
				} else {
					registry.byId("CopySelected").setAttribute("disabled",false);
				}		
			}
		
		   return (!isChild);
		};
		});	
});
</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="paymentInquiriesID" />
</jsp:include>
