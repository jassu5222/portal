<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
  Payment Pending Transactions page

  Description:
    Contains HTML to create the Pending page for payment Transactions 

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="PaymentPendingTransactions.jsp" %>
*******************************************************************************
--%> 

<%--cquinton 8/30/2012 add grid search header--%>
<div class="gridSearch">
  <div class="searchHeader">
    <span class="searchHeaderCriteria">

<%
        StringBuffer prependText = new StringBuffer();
        prependText.append(resMgr.getText("PendingTransactions.WorkFor", TradePortalConstants.TEXT_BUNDLE));
        prependText.append(" ");

        // If the user has rights to view child organization work, retrieve the 
        // list of dropdown options from the query listview results doc (containing
        // an option for each child org) otherwise, simply use the user's org 
        // option to the dropdown list (in addition to the default 'My Work' option).

        if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK))
        {
           dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                               "ORGANIZATION_OID", "NAME", 
                                                               prependText.toString(),
                                                               selectedWorkflow, userSession.getSecretKey()));

           // Only display the 'All Work' option if the user's org has child organizations
           if (totalOrganizations > 1)
           {
              dropdownOptions.append("<option value=\"");
              dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
              dropdownOptions.append("\"");

              if (selectedWorkflow.equals(ALL_WORK))
              {
                 dropdownOptions.append(" selected");
              }

              dropdownOptions.append(">");
              dropdownOptions.append(resMgr.getText(ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
              dropdownOptions.append("</option>");
           }
        }
        else
        {
           dropdownOptions.append("<option value=\"");
           dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
           dropdownOptions.append("\"");

           if (selectedWorkflow.equals(userOrgOid))
           {
              dropdownOptions.append(" selected");
           }

           dropdownOptions.append(">");
           dropdownOptions.append(prependText.toString());
           dropdownOptions.append(userSession.getOrganizationName());
           dropdownOptions.append("</option>");
        }

        extraTags.append("onchange=\"search();\" ");

        String defaultValue = "";
        String defaultText  = "";

        if(!userSession.hasSavedUserSession())
         {
           defaultValue = EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey());
           defaultText = MY_WORK;
		   
		   StringBuffer sb = new StringBuffer();
		   sb.append("<option value=\"");
           sb.append(defaultValue);
           sb.append("\">");
           sb.append(resMgr.getText(defaultText, TradePortalConstants.TEXT_BUNDLE));
           sb.append("</option>");
		   dropdownOptions.insert(0,sb.toString());
		   
         }


%>
      <%= widgetFactory.createSearchSelectField("Workflow","PendingTransactions.Show",
            "", dropdownOptions.toString(), "onChange='searchPending();'") %>


<%
         newLink.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
         newLink.append("&current2ndNav=");
         newLink.append(TradePortalConstants.NAV__PENDING_TRANSACTIONS);
         newLink.append("&workflow=");
         newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey()));
%>
        
  
<%
        // Build the dropdown options (hardcoded in ALL, STARTED, READY, PARTIALLY_AUTH, AUTH_FAILED), re-selecting the
        // most recently selected option.

        dropdownOptions = Dropdown.createCashMgmtTransStatusOptions(resMgr,selectedStatus); //Narayan-SRUL051855063-05/23/11

        
        extraTags = new StringBuffer();

//        extraTags.append("onchange=\"location='");
//        extraTags.append(formMgr.getLinkAsUrl("goToPaymentTransactionsHome", response));
//        extraTags.append("&amp;current2ndNav=");
//        extraTags.append(TradePortalConstants.NAV__PENDING_TRANSACTIONS);
//        extraTags.append("&amp;transStatusType='+this.options[this.selectedIndex].value\"");
        extraTags.append("onchange=\"search();\" ");
        
        //ctq - get this to work
        //out.print(InputField.createSelectFieldWithID("transStatusType", "", "",
        //out.print(InputField.createSelectField("transStatusType", "", "",
        //                                       dropdownOptions.toString(), 
        //                                       "ListText", false, extraTags.toString()));

%>
      <%= widgetFactory.createSearchSelectField("transStatusType", "InstrumentHistory.Status", 
            "", dropdownOptions.toString(), "onChange='searchPending();'") %>
     
    </span>
    <span class="searchHeaderActions">
      <%--cquinton 10/8/2012 include gridShowCounts --%>
      <jsp:include page="/common/gridShowCount.jsp">
        <jsp:param name="gridId" value="pmtPendGrid" />
      </jsp:include>

      <span id="pmtPendRefresh" class="searchHeaderRefresh"></span>
      <span id="pmtPendGridEdit" class="searchHeaderEdit"></span>
	  <%=widgetFactory.createHoverHelp("pmtPendRefresh","RefreshImageLinkHoverText") %> 
	  <%=widgetFactory.createHoverHelp("pmtPendGridEdit","CustomizeListImageLinkHoverText") %>  


    </span>
    <div style="clear:both;"></div>
  </div>
</div>

       


  
  <% 
    //-- CR-640/581 Rel 7.1.0 09/22/11  --Begin-- //
	BankOrganizationGroupWebBean bankOrgGrp  = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
	bankOrgGrp.getById(corpOrg.getAttribute("bank_org_group_oid"));
	if (TradePortalConstants.INDICATOR_YES.equals(bankOrgGrp.getAttribute("fx_online_avail_ind")) && userSession.hasAccessToLiveMarketRate()) {
		
		if	(SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.DOM_PMT, TransactionType.ISSUE) ||
			SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.XFER_BET_ACCTS, TransactionType.ISSUE) ||
			SecurityAccess.canAuthorizeInstrument(userSecurityRights, InstrumentType.FUNDS_XFER, TransactionType.ISSUE)) {
		    String loginSecurityType = null;
            boolean subsidiaryAccessPanelAuth = false;
		
            if (userSession.hasSavedUserSession()) {
                loginSecurityType = userSession.getSavedUserSessionSecurityType();
                subsidiaryAccessPanelAuth=TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")); 
            } else {
                loginSecurityType = userSession.getSecurityType();
            }
			
            if (TradePortalConstants.NON_ADMIN.equals(loginSecurityType) &&
                !subsidiaryAccessPanelAuth) {
		
  %> 
  
    <script LANGUAGE="JavaScript">  
   
   function getMarketRate(){  

            require(["t360/dialog","t360/datagrid"], function(dialog, grid){
				var errSec = document.getElementById('_errorSection');
				errSec.innerHTML = "";
				var rowKeys = grid.getSelectedGridRowKeys("pmtPendGrid");
			
			dialog.open('MarketRate', '<%=resMgr.getText("MarketRate.dialogTitle",TradePortalConstants.TEXT_BUNDLE)%>',
			'marketRateDialog.jsp',
			  ['selectedInstrument','form'],[rowKeys[0],'CashMgmt-TransactionForm'],    
			'select', null);
			});
		}
	</script>		
  <%
        }
     }
  }

  //-- CR-640/581 Rel 7.1.0 09/22/11  --End-- // 
  %> 
  
<%
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, beanMgr,response);   
String gridHtml = dgFactory.createDataGrid("pmtPendGrid","PaymentPendingTransactionsDataGrid",null);
%>
 <%=gridHtml %> 


<%--the route messages dialog--%>
<div id="routeDialog" style="display:none;"></div>
<div id="MarketRate" ></div>

<script type='text/javascript'>
<%--
//When the user drop down box has been clicked, 
//set the first RouteToUser to 'Y' and the second to 'N' for the radio buttons
function pickUser(selectUser) {
    document.RouteTransactionSelection.RouteToUser[0].checked = selectUser;
    document.RouteTransactionSelection.RouteToUser[1].checked = !selectUser;
  }
--%>

</script>
