<%--
*******************************************************************************
  Payment Pending Transactions Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
  String gridLayout = dgFactory.createGridLayout("pmtPendGrid", "PaymentPendingTransactionsDataGrid");
%>



<script type="text/javascript">

var gridLayout = <%=gridLayout%>;
var savedSort = savedSearchQueryData["sort"];
var selectedOrg = savedSearchQueryData["selectedWorkflow"];
var selectedStatus = savedSearchQueryData["selectedStatus"];
 <%--set the initial search parms--%>
<% 
//encrypt selectedOrgOid for the initial, just so it is the same on subsequent
//search - note that the value in the dropdown is encrypted

String encryptedOrgOid = EncryptDecrypt.encryptStringUsingTripleDes(selectedWorkflow, userSession.getSecretKey());
%>

<%-- IR 16481 start --%>
function initializePenDataGrid(){
require(["dojo/dom", "dojo/query", "dijit/registry", "t360/datagrid"], 
	      function(dom, query, registry, t360grid) {
		  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		 	<%--  selectedOrg = ( registry.byId('Workflow') == null ?"":registry.byId('Workflow').attr('value'));	 --%>
			selectedOrg= '<%=encryptedOrgOid%>';
		 	 }
		  	else{
		  		registry.byId("Workflow").set('value',selectedOrg);		  
		  		}
		  if("" == selectedOrg || null == selectedOrg || "undefined" == selectedOrg){
		      selectedOrg = userOrgOid;
		    }
			if("" == savedSort || null == savedSort || "undefined" == savedSort)
	  			savedSort = 'InstrumentID';
  			if("" == selectedStatus || null == selectedStatus || "undefined" == selectedStatus){
  				selectedStatus = dom.byId("transStatusType").value;
	 		}
  			else{
  				registry.byId("transStatusType").set('value',selectedStatus);
  				
  		  	}

var initSearchParms="selectedStatus="+selectedStatus+"&selectedWorkflow="+selectedOrg+"&userOid=<%=userOid%>&userOrgOid=<%=userOrgOid%>&confInd=<%=confInd%>";
console.log("initSearchParms"+initSearchParms);
var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PaymentPendingTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
  <%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
  var pmtPendGrid = t360grid.createDataGrid("pmtPendGrid",viewName,gridLayout, initSearchParms,savedSort);
  console.log("initSearchParms"+initSearchParms);
});
}
  function searchPending() {
	  
	   
	        var selectedWork = dijit.byId("Workflow").attr('value');
	        var selectedStatus = dijit.byId("transStatusType").attr('value');
	        
	        var searchParms = "userOrgOid=<%=userOrgOid%>&userOid=<%=userOid%>&selectedWorkflow=" + selectedWork + "&selectedStatus=" +selectedStatus+ "&confInd=<%=confInd%>"; 
	        searchDataGrid("pmtPendGrid", "PaymentPendingTransactionsDataView",
	                       searchParms);
	     
	  }
  
  
  function routePaymentTransactions(){
	  openRouteDialog('routeItemDialog', routeItemDialogTitle, 
				'pmtPendGrid', 
				'<%=TradePortalConstants.INDICATOR_YES%>', 
				'<%=TradePortalConstants.FROM_CM%>','') ;
	  }
  
 
    
  function authorizePaymentTransactions(){
	  <%
	  //IR T36000014931 REL ER 8.1.0.4 BEGIN
	  if (InstrumentServices.isNotBlank(certAuthURL)){
	    	//String authLink = "javascript:openReauthenticationWindow("+ "'" + certAuthURL + "','Authorize')";
		  String authLink = "javascript:openReauthenticationWindowForList("+ "'" + certAuthURL + "','"+formName+"','"+TradePortalConstants.BUTTON_AUTHORIZE+"','Transaction','pmtPendGrid')";
	  %>
	  openURL("<%=authLink%>");
	  <%}else{
	  //IR T36000014931 REL ER 8.1.0.4 END
	  %>
	  var formName = '<%=formName%>';
	    var buttonName = '<%=TradePortalConstants.BUTTON_AUTHORIZE%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("pmtPendGrid");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
	   <%
	   //IR T36000014931 REL ER 8.1.0.4 BEGIN
	    } 
	    //IR T36000014931 REL ER 8.1.0.4 END
	    %>
  }
  
  <%-- IR T36000014931 REL ER 8.1.0.4 BEGIN --%>
  function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
	    document.location.href  = URL;
		return false;
}
  <%-- IR T36000014931 REL ER 8.1.0.4 END --%>
  
  function deletePaymentTransactions(){
	  var formName = '<%=formName%>';
	    var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';

	    <%--get array of rowkeys from the grid--%>
	    var rowKeys = getSelectedGridRowKeys("pmtPendGrid");

	    <%--submit the form with rowkeys
	        becuase rowKeys is an array, the parameter name for
	        each will be checkbox + an iteration number -
	        i.e. checkbox0, checkbox1, checkbox2, etc --%>
	    submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
  }

  <%--  require(["dojo/ready"], function(ready){
           ready(function(){
        	   initParms();
        	   console.log ('initParams='+initSearchParms);
           });
       });  --%>

  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/popup", "dijit/registry","dojo/ready", "dojo/domReady!"],
      function(query, on, t360grid, t360popup, registry,ready){
	  ready(function() {
		  initializePenDataGrid();
	   var f =  registry.byId('PaymentPendingTransactions_Authorize');
	   if (f) f.set('disabled',true);
	   
	   f =  registry.byId('PaymentPendingTransactions_Route');
	   if (f) f.set('disabled',true);
	   
	   f = registry.byId('PaymentPendingTransactions_Delete');
	   if (f) f.set('disabled',true);
	
    var pmtPendGrid = registry.byId('pmtPendGrid');
	 on(pmtPendGrid,"selectionChanged", function() {
	   if (registry.byId('PaymentPendingTransactions_Authorize')){
        t360grid.enableFooterItemOnSelectionGreaterThan(pmtPendGrid,"PaymentPendingTransactions_Authorize",0);
	   }
	   if (registry.byId('PaymentPendingTransactions_Route')){
	  t360grid.enableFooterItemOnSelectionGreaterThan(pmtPendGrid,"PaymentPendingTransactions_Route",0);
	  }
	  if (registry.byId('PaymentPendingTransactions_Delete')){
	  t360grid.enableFooterItemOnSelectionGreaterThan(pmtPendGrid,"PaymentPendingTransactions_Delete",0);
	  }
      });

    query('#pmtPendRefresh').on("click", function() {
      searchPending();
    });
    query('#pmtPendGridEdit').on("click", function() {
      var columns = t360grid.getColumnsForCustomization('pmtPendGrid');
      var parmNames = [ "gridId", "gridName", "columns" ];
      var parmVals = [ "pmtPendGrid", "PaymentPendingTransactionsDataGrid", columns ];
      t360popup.open(
        'pmtPendGridEdit', 'gridCustomizationPopup.jsp',
        parmNames, parmVals,
        null, null);  <%-- callbacks --%>
    });
	  });
  });

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="pmtPendGrid" />
</jsp:include>
