<%--
 *  Everything associatd with admin menus that should be placed in the footer area.
 *  This includes dialogs that are solely associated with the menu.
 *  Note that the dialogs themselves are defined
 *  at \presentation\dialog.  Behavior below is specific to use from menus.
 *  
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*, java.util.*;" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%-- Hide dialog divs  --%>
<%--<div style="display:none;">--%>
<div>
  <div id="templateSearchDialog" ></div>
  <div id="instrumentSearchDialog" ></div>
  <div id="bankBranchSelectorDialog" ></div>
  <div id="copyDirectDebitDialog" ></div>
  <div id="copyPaymentsDialog" ></div>
  <div id="otherConditionsDialog" ></div>
  <div id="deleteAllConfirmationDialog" ></div>

<%  //include a hidden form for new instrument submission
    // this is used for all of the new instrument types available from the menu
    
    Hashtable newInstrSecParms = new Hashtable();
   if(userSession.getUserOid()!=null)
    	newInstrSecParms.put("UserOid", userSession.getUserOid());
   if(userSession.getSecurityRights()!=null)
    newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
   if( userSession.getClientBankOid()!=null)
    newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
   if(userSession.getOwnerOrgOid()!=null)
    newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
    
    
%>
  <form method="post" name="NewInstrumentForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" value="NEW_INSTRUMENT"/>
    <input type="hidden" name="copyType" value="Blank"/>
    <input type="hidden" name="bankBranch" />
    <input type="hidden" name="instrumentType" />
    <input type="hidden" name="transactionType" />
    <input type="hidden" name="copyInstrumentOid" />
    <%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
  </form>
 
 <%
	Hashtable newTempSecParms = new Hashtable();
 if(userSession.getUserOid()!=null)
	newTempSecParms.put("userOid", userSession.getUserOid());
 if(userSession.getSecurityRights()!=null)
	newTempSecParms.put("securityRights", userSession.getSecurityRights());
 if(userSession.getClientBankOid()!=null)
	newTempSecParms.put("clientBankOid", userSession.getClientBankOid());
 if(userSession.getOwnerOrgOid()!=null)
	newTempSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
 if(userSession.getOwnershipLevel()!=null)
	newTempSecParms.put("ownerLevel", userSession.getOwnershipLevel());
%>
	<form method="post" name="NewTemplateForm1" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="name" />
	<input type="hidden" name="bankBranch" />
	<input type="hidden" name="transactionType"/>
	<input type="hidden" name="InstrumentType" />
	<input type="hidden" name="copyInstrumentOid" />
	<input type="hidden" name="mode"  />
	<input type="hidden" name="CopyType" />
	<input type="hidden" name="expressFlag"  />
	<input type="hidden" name="fixedFlag" />
	<input type="hidden" name="PaymentTemplGrp" />
	<input type="hidden" name="validationState" />
	
	<%= formMgr.getFormInstanceAsInputField("NewTemplateForm",newTempSecParms) %>
	</form> 
</div>

<%
  //create some javascript variables that can be used by the menu event handlers events
  //
  //  bankBranchArray - array of operational bank org oids associated with the user's corp org
  //  templateSearchDialogTitle - localized dialog title
  //  instrumentSearchDialogTitle - localized dialog title
  //  bankBranchSelectorDialogTitle - localized dialog title
  //  transferExportLCDialogTitle - localized dialog title
  //

  String menuOwnerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean menuCorpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  menuCorpOrg.getById(menuOwnerOrg);

  String[] menuOpBankOid = {menuCorpOrg.getAttribute("first_op_bank_org"),
                            menuCorpOrg.getAttribute("second_op_bank_org"),
                            menuCorpOrg.getAttribute("third_op_bank_org"),
                            menuCorpOrg.getAttribute("fourth_op_bank_org")};
  String menuBankBranches = "";
  for (int bbaIdx=0; bbaIdx<4; bbaIdx++)
  {
      if (!InstrumentServices.isBlank(menuOpBankOid[bbaIdx])) {
          if ( menuBankBranches.length()>0 ) {
              menuBankBranches += ",";
          }
          menuBankBranches += "'" + menuOpBankOid[bbaIdx] + "'";
      }
      else {
          break; //if a blank is found there will be no more
      }
  }

  //dialog titles - get them from text resource
  String templateSearchDialogTitle = resMgr.getText("templateSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String instrumentSearchDialogTitle = resMgr.getText("instrumentSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String bankBranchSelectorDialogTitle = resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String copyDirectDebitDialogTitle = resMgr.getText("NewInstrumentsMenu.ExistingDirectDebitSearch.DirectDebitSearch", TradePortalConstants.TEXT_BUNDLE);
  String copyPaymentsDialogTitle = resMgr.getText("NewInstrumentsMenu.ExistingPaymentSearch.PaymentSearch", TradePortalConstants.TEXT_BUNDLE);
  String otherConditionsDialogTitle = resMgr.getText("common.OtherConditionsDialogTitle", TradePortalConstants.TEXT_BUNDLE);
  String deleteAllConfirmationDialogTitle = resMgr.getText("Notifications.DeleteAll", TradePortalConstants.TEXT_BUNDLE);
%>
<script>

  var bankBranchArray = [ <%=menuBankBranches%> ];
  var templateSearchDialogTitle = '<%=templateSearchDialogTitle%>';
  var instrumentSearchDialogTitle = '<%=instrumentSearchDialogTitle%>';
  var bankBranchSelectorDialogTitle = '<%=bankBranchSelectorDialogTitle%>';
  var copyDirectDebitDialogTitle = '<%=copyDirectDebitDialogTitle%>';
  var copyPaymentsDialogTitle = '<%=copyPaymentsDialogTitle%>';
  var otherConditionsDialogTitle = '<%=otherConditionsDialogTitle%>';
  var deleteAllConfirmationDialogTitle = '<%=deleteAllConfirmationDialogTitle%>';
</script>
