<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<%
    Debug.debug("***START********************DOC CLEAN UP**************************START***");

    HttpSession theSession    = request.getSession(false);

    String forwardPage = (String) theSession.getAttribute("startPage");

    Debug.debug("Resetting session indicator inNewTransactionArea to 'N'");
    theSession.removeAttribute("inNewTransactionArea");
    theSession.removeAttribute("startPage");

    Debug.debug("forwardPage " + forwardPage);
    String physicalPage = NavigationManager.getNavMan().getPhysicalPage(forwardPage, request);
    Debug.debug("Leaving DocCleanUp.  Going to this physical page -> " + physicalPage);
%>
    <jsp:forward page='<%= physicalPage %>' />
