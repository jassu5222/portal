
<!DOCTYPE HTML>
<%--
*******************************************************************************
                              Document View Page

  Description:
    This jsp is designed to retrieve an image from the database based on 2
  passed in parameters via the Repository Name and the Image Id.  This is 
  accomplished by calling an applet.  
*******************************************************************************
--%>   

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,javax.resource.cci.*,javax.naming.*,
		 javax.resource.ResourceException,com.filenet.is.ra.cci.*, java.sql.SQLException,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.mediator.util.*,
                 com.ams.tradeportal.common.cache.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"         scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"              scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"              scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"   scope="session"></jsp:useBean>

<%
  //cquinton 9/15/2011 Rel 7.1 ppx240 differentiate between encrypted/non encrypted parms
  String encryptedImageId;
  String imageId;
  String formType;
  String transactionOid;
  String instrumentOid;
  String instrumentID;
  String instrumentType;
  String transactionType	= "";
  String instrumentDisplay	= "";

  //Variables used to pass parameters in the url - encrypted.
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String urlParm;
  String urlParm2		= "";
  String urlParm3;
  String            helpSensitiveLink     = null;
  String docContentURL = "";
  String annotationsURL ="";
  
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);	
  //Begin IValavala 5/28/13. IValavala T36000016319. Applying the below. Comment out other returnAction and use
  //returnaction instead of urlParams for close button.
  //cquinton add page to page flow so back button works correctly
  userSession.addPage("goToDocumentView", request);
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString();
  returnParms+="&returning=true";
 //End IValavala 5/28/13. IValavala T36000016319. Applying the below.

  boolean isFromMessages = !InstrumentServices.isBlank( request.getParameter("fromMessages") );
  String messageSubject		= "";
  
  //IValavala Rel 8.2 CR 742.
  boolean isFromPayrem = false;
  //Commenting out the below temporarily
  isFromPayrem = !InstrumentServices.isBlank( request.getParameter("fromPayrem") );
   

  //cquinton 9/15/2011 Rel 7.1 ppx240 differentiate between encrypted/non encrypted parms
  encryptedImageId = request.getParameter("image_id");
  
  imageId = EncryptDecrypt.decryptStringUsingTripleDes( encryptedImageId, userSession.getSecretKey() );
  //out.println("image id is: " + imageId);
  
  
  transactionOid = request.getParameter("tran_id");
  
  //System.out.println("transaction oid is: " +   transactionOid);
  //out.println("from Payrem is : " +   request.getParameter("fromPayrem"));
  transactionOid = EncryptDecrypt.decryptStringUsingTripleDes( transactionOid, userSession.getSecretKey() );
  

  formType = request.getParameter("formType");
  formType = EncryptDecrypt.decryptStringUsingTripleDes( formType, userSession.getSecretKey() );
  
  if( isFromMessages ) {

	String encryptedMessageOid = request.getParameter("mailMessageOid");
	
	/*<PDastidar BOUF081272868 07-Feb-2006>Start*/
	String origMessageId = EncryptDecrypt.decryptStringUsingTripleDes(encryptedMessageOid, userSession.getSecretKey());
	//Validation to check Cross Site Scripting
	if(encryptedMessageOid != null){
		encryptedMessageOid = StringFunction.xssCharsToHtml(encryptedMessageOid);
	}	
	//	Build the Sql to get a list of Document Images from the Database.
	DocumentHandler docImageList = null;		//used to display a list of document Image 
	Vector listviewVector = null;
	String imageIdFromMail = null;
	boolean isValid = false;
	try{
	  if( InstrumentServices.isNotBlank( origMessageId ) ) {
		String sql =  "select image_id from document_image where mail_message_oid = ? ";
		
		docImageList = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{origMessageId});
		if( docImageList != null )
		 {
			Debug.debug("*** Returned Resultset is: " + docImageList.toString() );
			listviewVector = docImageList.getFragments("/ResultSetRecord/");
			int size = listviewVector.size();
			for(int i= 0; i<size; i++)
			{
				imageIdFromMail = ((DocumentHandler)listviewVector.get(i)).getAttribute("/IMAGE_ID");
				if( InstrumentServices.isNotBlank( imageIdFromMail )  
						&& InstrumentServices.isNotBlank( imageId ) 
						&& imageId.equals(imageIdFromMail) )
				{
					isValid = true;
					break;
				}
			}
		 }
	  }
	}catch(Exception e){
	  Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
		  e.printStackTrace();
	}

	if(isValid)
	{
	/*<PDastidar BOUF081272868 07-Feb-2006>End*/

		messageSubject = request.getParameter("messageSubject");
		
		urlParm = "&mailMessageOid=" + encryptedMessageOid;
		urlParm3 = "&mailMessageOid=" + encryptedMessageOid;
		//IValavala 5/28/13. IValavala T36000016319. Return action being dervied from previous page.
		//returnAction = "goToMailMessageDetail";
	/*<PDastidar BOUF081272868 07-Feb-2006>Start*/
	}
	else
	{
		%>
		<jsp:forward page='/AutoReroute.jsp'></jsp:forward>
		<%
	}
	/*<PDastidar BOUF081272868 07-Feb-2006>End*/
	
  }
  //IValavala Rel 8.2 CR 742
  else if(isFromPayrem){
	  Debug.debug(" This is Payrem Image: ");
	  String encryptedPayremOid = request.getParameter("tran_id");
	  urlParm = "&pay_remit_oid=" + encryptedPayremOid;
		urlParm3 = "&pay_remit_oid=" + encryptedPayremOid;
		//IValavala 5/28/13. IValavala T36000016319. Return action being dervied from previous page.
		//returnAction = "goToPaymentRemittance";
  }
  else{
  	TransactionWebBean transaction  = (TransactionWebBean)
                                      beanMgr.getBean("Transaction");
  	InstrumentWebBean instrument    = (InstrumentWebBean)
                                      beanMgr.getBean("Instrument");

    if (InstrumentServices.isNotBlank(transactionOid)) {
	    Debug.debug(" Retrieving.... Tranaction from DB with transaction_oid= " + transactionOid);
		transaction = beanMgr.createBean(TransactionWebBean.class,  "Transaction");
        transaction.getById(transactionOid);

		Debug.debug(" Retrieving.... Instrument from DB with instrument_oid= " + transaction.getAttribute("instrument_oid"));
		instrument = beanMgr.createBean(InstrumentWebBean.class,  "Instrument");
        instrument.getById(transaction.getAttribute("instrument_oid"));

    }	
  	transactionOid    = transaction.getAttribute("transaction_oid");
  	instrumentOid     = instrument.getAttribute("instrument_oid");
  	instrumentID      = instrument.getAttribute("complete_instrument_id"); 
	
	

  	instrumentType    = instrument.getAttribute("instrument_type_code"); 
  	instrumentType    = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE,
						                 	instrumentType);
  	transactionType   = transaction.getAttribute("transaction_type_code");
  	transactionType   = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,
						                 	transactionType);

  	instrumentDisplay = instrumentID + "    " + instrumentType;

	/*<PDastidar BOUF081272868 07-Feb-2006>Start*/
	//	Build the Sql to get a list of Document Images from the Database.
	DocumentHandler docImageList = null;		//used to display a list of document Image 
	Vector listviewVector = null;
	String imageIdFromTrans = null;
	boolean isValid = false;
	try{
	  if( InstrumentServices.isNotBlank( transactionOid ) ) {
		String sql = "select image_id from document_image where p_transaction_oid = ? ";
		
		docImageList = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{transactionOid});
		 if( docImageList != null )
		 {
			Debug.debug("*** Returned Resultset is: " + docImageList.toString() );
			listviewVector = docImageList.getFragments("/ResultSetRecord/");
			int size = listviewVector.size();
			for(int i= 0; i<size; i++)
			{
				imageIdFromTrans = ((DocumentHandler)listviewVector.get(i)).getAttribute("/IMAGE_ID");
				//if multiple images are attached
				if( InstrumentServices.isNotBlank( imageIdFromTrans )  
						&& InstrumentServices.isNotBlank( imageId ) 
						&& imageId.equals(imageIdFromTrans) )
				{
					isValid = true;
					break;
				}
			}
		 }
	  }
	}catch(Exception e){
	  Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
		  e.printStackTrace();
	}
	if(isValid)
	{
	/*<PDastidar BOUF081272868 07-Feb-2006>End */

  		// Store the instrument oid in the session in case the user clicks on the Instrument ID
	  	// breadcrumb link
  		session.setAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID, instrument.getAttribute("instrument_oid"));

	  	encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes( transactionOid, userSession.getSecretKey() ); 		//Transaction_Oid
  		encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( instrumentOid, userSession.getSecretKey() );		//Instrument_Oid
	  	encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( "goToInstrumentSummary", userSession.getSecretKey() );	

	  	urlParm = "&oid=" + encryptVal1 + "&instrument_oid=" + encryptVal2 + "&returnAction=" + encryptVal3;
  		urlParm2 = "&oid=" + encryptVal1 + "&instrument_oid=" + encryptVal2 + "&returnAction=" + 
                      encryptVal3;
	  	urlParm3 = "&oid=" + encryptVal1 + "&instrument_oid=" + encryptVal2 + "&returnAction=" + encryptVal3;
	  //IValavala 5/28/13. T36000016319. Return action being dervied from previous page.
	  	//returnAction = "goToInstrumentNavigator";
	/*<PDastidar BOUF081272868 07-Feb-2006>Start */
	}
	else
	{
		%>
		<jsp:forward page='/AutoReroute.jsp'></jsp:forward>
		<%
	}
	/*<PDastidar BOUF081272868 07-Feb-2006>End */
  }

%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />
<div class="pageMain">
  <div class="pageContent">
    <form id="DocumentView" name="DocumentView" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

      <%--cquinton 12/18/2012 ir#6711 fix menu and page header issues--%>
      <div class="pageSubHeader">
        <span class="pageSubHeaderItem">
<%
  if( isFromMessages ) {
%>
          <a href="<%=formMgr.getLinkAsUrl("goToMailMessageDetail", urlParm, response) %>">
            <%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(messageSubject))%>
          </a>
<%
  }
  else {
%>
          <a href="<%=formMgr.getLinkAsUrl("goToInstrumentCloseNavigator", urlParm, response) %>" >
            <%=instrumentDisplay%>
          </a>
<%
  } 
%> 
          <span>&nbsp;</span>
<%
  if( isFromMessages ) {
%>
          <%=StringFunction.xssCharsToHtml(formType)%>
<%
  }
  else{
%>
          <a href="<%=formMgr.getLinkAsUrl("goToInstrumentNavigator", urlParm2, response ) %>" >
            <%=transactionType%>
          </a> 
          &nbsp;&gt;&nbsp;<%=StringFunction.xssCharsToHtml(formType)%>
<%
  } 
%>
        </span>
				       
        <span class="pageSubHeader-right">
          <button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" class="pageSubHeaderButton">
            <%=resMgr.getText("common.CloseText", TradePortalConstants.TEXT_BUNDLE)%>
            <%//IValavala 5/28/13.  T36000016319. Use Returnaction and returnParms being dervied from previous page instead of urlParams%>
            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
              openURL("<%=formMgr.getLinkAsUrl( returnAction, returnParms, response )%>");        					
            </script>
          </button> 
<% //Adding Online help file call based on passed in info for the fileName and anchor...
   //This is backwards compatible so as not to affect everyone else that hasn't done this yet.					    
  helpSensitiveLink = OnlineHelp.createContextSensitiveLink( "/customer/doc_view.htm", "", resMgr, userSession);
%> 								     
          <span class="pageSubHeaderHelp" id="pageHelp">
            <%=helpSensitiveLink%>
          </span>					     
          <%= widgetFactory.createHoverHelp("CloseButton","CloseHoverText")     %> 
          <%= widgetFactory.createHoverHelp("pageHelp","HelpImageLinkHoverText") %> 
        </span>
        <div style="clear:both;"></div>
      </div>

      <div class="formContentNoSidebar">
       <%-- NARAYAN IR-T36000013473  Commenting VeniceBridge code--%>
       <%-- 
				<%
				    //cquinton 11/2/2011 Rel 7.1 start default to use ISRA, but if property is set use VeniceBridge
				    PropertyResourceBundle tpProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
				    boolean useISRA = true;
				    try {
				        String imageImpl = tpProperties.getString("imageImpl");
				        if ( "VeniceBridge".equalsIgnoreCase( imageImpl ) ) {
				            useISRA = false;
				        }
				    } catch ( Exception ex ) {
				        //do nothing, already default to useISRA=true
				    }
				
				    if ( useISRA ) {
				        //cquinton 11/2/2011 Rel 7.1 end
		--%>	
		            <%
				        // use  request.getParameter("image_id"); which is still encrypted to pass to servlet and decrypt there
                        //  code for servlet EncryptDecrypt.decryptStringUsingTripleDes( imageId, userSession.getSecretKey() );
				
				        //cquinton 7/21/2011 Rel 7.1.0 ppx240 - pass along encrypted image hash
				        String encryptedImageHash = request.getParameter("hash");








				
				        //cquinton 11/3/2011 Rel 7.1
				        //the applet does not include session, so explicitly add it
				        String sessionId = request.getRequestedSessionId();
				     



				        //Rel 9.2 XSS - End
				        //out.println("sessionId="+sessionId);
				        boolean fromCookie = request.isRequestedSessionIdFromCookie();
				        //out.println("fromCookie="+fromCookie);
				        boolean fromURL = request.isRequestedSessionIdFromURL();
				        //out.println("fromURL="+fromURL);
				        boolean sessionIsValid = request.isRequestedSessionIdValid();
				        //out.println("isValid="+sessionIsValid);
				        
				        //cquinton 11/27/2011 Rel 7.1.0 note: this is just a comment to note that the below relative URLS
				        //for image content and annotations are necessary approach becuase of firewall
				        //between apache and weblogic in prod/non-prod envs.  cannot go directly to weblogic url
				        //for example to bypass apache
				        docContentURL = request.getContextPath() + 
				            "/GetImageContentServlet?doc_id=" + StringFunction.escapeQuotesforJS(encryptedImageId)  
				            + "&page_number=1&hash=" + StringFunction.escapeQuotesforJS(encryptedImageHash);
				        //IValavala CR 742. Add source type
				        if(isFromPayrem){
				        	String en_sourceType = request.getParameter("source_type");
				        	//Rel 9.2 XSS : CID 11222 - Start
				        	if(null != en_sourceType){
				        		en_sourceType = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(en_sourceType));
				        	}
				        	////Rel 9.2 XSS - End
				        	//out.println("source type:" + en_sourceType);
				        	//Debug.debug(isFromPayrem + " source type:" + en_sourceType);
				        	Cache cbCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
				        	DocumentHandler CBCResult = (DocumentHandler)cbCache.get(userSession.getClientBankOid());
				        	String imageURL = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_ENDPOINT");
				        	String imageUser = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_USERID");
				        	String imagePasswd = CBCResult.getAttribute("/ResultSetRecord(0)/IMAGE_SVC_PWORD");
				        	//Encrypt imageURL, user and passwd
				        	imageURL = EncryptDecrypt.encryptStringUsingTripleDes( imageURL, userSession.getSecretKey() );
				        	imageUser = EncryptDecrypt.encryptStringUsingTripleDes( imageUser, userSession.getSecretKey() );
				        	imagePasswd = EncryptDecrypt.encryptStringUsingTripleDes( imagePasswd, userSession.getSecretKey() );
				        	docContentURL = docContentURL + "&source_type=" + StringFunction.escapeQuotesforJS(en_sourceType) + "&imgurl=" + imageURL + "&imgUsr=" + imageUser + "&imgPwd=" + imagePasswd;
				        	Debug.debug(" docContentURL:" + docContentURL);

				        }
				        annotationsURL = request.getContextPath() + 
				            "/GetImageAnnotationsServlet?doc_id=" + StringFunction.escapeQuotesforJS(encryptedImageId) + "&page_number=-1";
				        //cquinton 11/3/2011 Rel 7.1
				        //the applet does not include session data on the urls, so explicitly add it
				        if ( sessionId!=null && sessionIsValid ) {
				            docContentURL += "&JSESSIONID="+ StringFunction.escapeQuotesforJS(sessionId);
				            annotationsURL += "&JSESSIONID="+StringFunction.escapeQuotesforJS(sessionId);
				        }
				        //for debugging if necessary
				        //out.println("scheme:" + request.getScheme());
				        //out.println("serverName:" + request.getServerName());
				        //out.println("serverPort:" + request.getServerPort());
				        //out.println("docContentURL:" + docContentURL);
				        //out.println("annotationsURL:" + annotationsURL);
				        //cquinton 10/24/2011 Rel 7.1.0 ir#caul092946520 end
				%>
				<%-- Load the applet through java script at bottom as  it required DOJO lib which included in footer --%>
	         <div id="appletContent">
				
			 </div>	
			 <%-- NARAYAN IR-T36000013473  Commenting VeniceBridge code--%>
			 <%-- 
				<%  //cquinton 11/2/2011 Rel 7.1 start - if property is set use VeniceBridge
				    } else {  
				        //use venice bridge
				        ImagingIdAndPassword idandpass = ImagingServices.getNextIdAndPassword();
				        String userName = idandpass.getId();
				        String password = idandpass.getPassword();
				        boolean imageSuccess = false;
				        com.venetica.vbr.client.Repository vbrRepository = null;
				        com.venetica.vbr.client.Content vbrContent = null;
				       
				        HttpSession mySession = request.getSession(false);                                    
				        TradePortalSessionListener tpsl = (TradePortalSessionListener)mySession.getAttribute("TimeoutListener");
				        com.venetica.vbr.client.User vbrUser = (com.venetica.vbr.client.User)mySession.getAttribute("vbr-user");
				        if (vbrUser == null)
				        {
				            try {
				                vbrUser = new com.venetica.vbr.client.User();       
				                vbrUser.initialize();            
				                mySession.setAttribute("vbr-user",vbrUser);
				                tpsl.vbrUser = vbrUser;
				            } catch (Exception e) { 
				                imageSuccess = false;
				                System.out.println("Connection to Venice Bridge Failed");
				                e.printStackTrace(); 
				            }
				        }                
				        vbrRepository = (com.venetica.vbr.client.Repository)mySession.getAttribute("vbr-repository");
				        try {
				            if (vbrRepository == null)
				            {
				                vbrRepository = vbrUser.getRepositoryByID("PanagonIS");
				                vbrRepository.logon(userName,password,"");
				                mySession.setAttribute("vbr-repository",vbrRepository);
				                tpsl.vbrRepository = vbrRepository ;
				            }
				            vbrContent = vbrRepository.getContent(imageId,null);
				            mySession.setAttribute("vbr-content",vbrContent);
				            imageSuccess = true;
				        } catch (Exception e) { 
				            imageSuccess = false;
				            System.out.println("Retrieving Venice Bridge Image Failed");
				            e.printStackTrace(); 
				        }
				
				        if (imageSuccess) {
				%>
				    <applet archive="/portal/venicebridge/tp_viewer_applet.jar" code="com.venetica.vbr.viewer.client.TradePortalViewerApplet.class" width="100%" height="100%">
				       <param name="displayCachedContent" value="true">
				       <param name="cabbase" VALUE="/portal/venicebridge/tp_viewer_applet.cab">   <hr>
				       <hr>
				       Java is not supported by your browser.
				       <hr>
				    </applet>
				<%      }
				        else if(isFromPayrem){ //IValavala added for CR 742. Display different message.
				        	%>
							 &nbsp;&nbsp;&nbsp;&nbsp;<p>
							    <span class="ControlLabel"><%=resMgr.getText("DocumentView.ExternalImageViewError", TradePortalConstants.TEXT_BUNDLE)%></span>
							 </p>
							<%
				        }
				        else {
				%>
				 &nbsp;&nbsp;&nbsp;&nbsp;<p>
				    <span class="ControlLabel"><%=resMgr.getText("DocumentView.ImageViewError", TradePortalConstants.TEXT_BUNDLE)%></span>
				 </p>
				<%
				        } //end !imageSuccess
				    } //end !useISRA
				    //cquinton 11/2/2011 Rel 7.1 end				    
				%>
				--%>
				</div><%-- formContent --%>
			</form>
		</div>><%-- pageMain --%>
	</div>><%-- pageContent --%>

	<jsp:include page="/common/Footer.jsp">
	  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_NO%>" />
	  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
	</jsp:include>

<script language="JavaScript">
		require(["dojo/has", "dojo/ready","dojo/_base/sniff"], function(has,ready){
				var content="";	
				ready(function(){
				if(!has("chrome")){
					  content='<APPLET CODEBASE="/portal/viewONE/FNJavaV1Files" ARCHIVE = "/portal/viewONE/FNJavaV1Files/ji.jar" CODE = "ji.applet.jiApplet.class" NAME = "ViewONE" WIDTH = "100%" HEIGHT = "500px">'
						        + '<PARAM NAME="cabbase" VALUE="ji.cab">'
						        + '<PARAM NAME="filename" value="<%=docContentURL%>">'
						        + '<PARAM NAME="annotationFile" value="<%=annotationsURL%>">'
						        + '<PARAM NAME="filenet" value="true">'
						        + '<PARAM NAME="filenetSystem" value="0">'
						        + '<PARAM NAME="annotationEncoding" VALUE ="UTF8">'
						        + '<PARAM NAME="annotationUnits" value="inches">'
						        + '<PARAM NAME="annotationColorMask" value="0bgr">'
						        + '<PARAM NAME="annotationNoteColor" value="255,255,0">'
						        + '<PARAM NAME="annotationNoteSize" value="0.3,0.3">'
						        + '<PARAM NAME="AnnotationDefaultLineColor1" value="freehand: 0, 0, 255">'
						        + '<PARAM NAME="AnnotationDefaultLineColor2" value="arrow: 255, 0, 255">'
						        + '<PARAM NAME="annotationHideButtons" value="show">'
						        + '<PARAM NAME="FileButtonOpen" value="false">'
						        + '<PARAM NAME="FileButtonClose" value="false">'
						        + '<PARAM NAME="FileButtonSave" value="false">'
						        + '<PARAM NAME="scale" value="ftow">'
						        + '<PARAM NAME="infoOptions" value="false">'
						        + '<PARAM NAME="FileMenus" value="false">'
						        + '<PARAM NAME="newWindowButtons" value="false">'
						        + '<PARAM NAME="fileKeys" value="false">'
						        + '<hr><hr>'
							    + 'Java is not supported by your browser.'
							    + '<hr>'												     
						        + '</APPLET>';
				     
					  
				  }else{				
					  content='<embed name="viewONE" width="100%" height="500px" pluginspage="http://java.sun.com/products/plugin/1.5/plugin-install.html"'
			                     + 'type="application/x-java-applet;version=1.5"'
			                     + 'archive="ji.jar"'
			                     + 'codebase="/portal/viewONE/FNJavaV1Files/"'
			                     + 'code="ji.applet.jiApplet.class"'
			                     + 'filename="<%=docContentURL%>"'
			                     + 'annotationFile="<%=annotationsURL%>"'
			                     + 'filenet="true"'
			                     + 'annotationEncoding="UTF8"'
			                     + 'annotationUnits="inches"'
			                     + 'annotationColorMask="0bgr"'
			                     + 'annotationNoteColor="255,255,0"'
			                     + 'annotationNoteSize="0.3,0.3"'
			                     + 'AnnotationDefaultLineColor1="freehand: 0, 0, 255"'
			                     + 'AnnotationDefaultLineColor2="arrow: 255, 0, 255"'
			                     + 'annotationHideButtons="show"'
			                     + 'FileButtonOpen="false"'
			                     + 'FileButtonClose="false"'
			                     + 'FileButtonSave="false"'
			                     + 'scale="ftow"'
			                     + 'infoOptions="false"'
			                     + 'FileMenus="false">'
			                     + '</embed>';
				   }  
				   document.getElementById("appletContent").innerHTML=content;
				});
			});				  				  
</script>

<script language="JavaScript">
 
 function openURL(URL){
     if (isActive =='Y') {
    	 if (URL != '' && URL.indexOf("javascript:") == -1) {
	         var cTime = (new Date()).getTime();
	         URL = URL + "&cTime=" + cTime;
	         URL = URL + "&prevPage=" + context;
    	 }
     }
	 document.location.href  = URL;
	 return false;
 }

</script>
 
</body>
</html>
