<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
		 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
		 com.ams.tradeportal.busobj.util.*,java.text.*, java.util.Date,java.util.*, com.amsinc.ecsg.frame.PerfStatConfigManager"%>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
  //cquinton 12/20/2012 before doing anything check for force password change flag to see if we need to forward
  // the user to the change password page
  String checkChangePassword = request.getParameter("checkChangePassword");
  //by default always check, but allow ChangePassword, Logout, other special pages to bypass
  if ( !TradePortalConstants.INDICATOR_NO.equals(checkChangePassword) ) {
    if ( userSession.getForceChangePassword()) {
      boolean forceChangePassword = true;
      //first check to see if the user actually did change their password
      //this would be invoked on TradePortalHome for example after the password is actually changed
      String checkPasswordUserOid = userSession.getUserOid();
      if ( !InstrumentServices.isBlank(checkPasswordUserOid) ) {
        UserWebBean checkPasswordUser = null;
        checkPasswordUser = beanMgr.createBean(UserWebBean.class, "User");
        checkPasswordUser.getById(checkPasswordUserOid);
        //Note: if there is a problem pulling in the user, the below
        // still forces the user to the change password page.

        if ( "new".equals(userSession.getChangePasswordReason() ) ) {
          //do opposite new user check as in login
          if(!TradePortalConstants.INDICATOR_YES.equals(checkPasswordUser.getAttribute("new_user_ind"))) {
            //user is no longer new, reset user session flag
            userSession.setForceChangePassword(false);
            forceChangePassword = false;
          }
        }
        else { //password expired
          //do opposite password expired check as in login
          if (!checkPasswordUser.isPasswordExpired()) {
            //password is no longer expired, reset user session flag
            userSession.setForceChangePassword(false);
            forceChangePassword = false;
          }
        }
      }

      if ( forceChangePassword ) {
        String forwardPage = NavigationManager.getNavMan().getPhysicalPage("ChangePassword", request);

        // Add URL parameters to force the user to change their password
        forwardPage = forwardPage + "?force=true&reason=" + userSession.getChangePasswordReason();
%>
         <jsp:forward page='<%= forwardPage %>' />
<%
      }
    }
  }

  //ctq: following 2 flags are no longer valid
  //error section should be included individually
  //includeNavBarFlag is replaced by minimalHeaderFlag, which by default is false.
  //this is done to automatically include menu on all pages
  //String         includeErrorSectionFlag  = null;
  //String         includeNavBarFlag        = null;
  String         minimalHeaderFlag        = null;
  //ctq: additionalOnLoad is no longer a valid parameter.
  // pages should use dojo/domReady! instead (or dojo/ready if appropriate)
  //String         additionalOnLoad         = "";
  String         userSecurityType         = null;
  String         brandingDirectory        = null;
  String         currentPrimaryNavigation = null;
  String         autoSaveFlag             = null;
  String         templateFlag             = null;
  String         autoSaveFormNumber       = null;
  String         timeoutURL               = null;
  String         timeoutAutoSaveInd       = null;

  // Retrieve all parameters passed to this jsp; these are used for including or
  // excluding other jsps
  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 new option to not display header at all
  //although even if header is not displayed, html header with branding is still necessary for rest of page
  String displayHeader = request.getParameter("displayHeader");
  minimalHeaderFlag       = request.getParameter("minimalHeaderFlag");
  autoSaveFlag            = request.getParameter("autoSaveFlag");  // Whether to perform auto-save when time-out
  templateFlag            = request.getParameter("templateFlag");  // Whether to auto-save a template when time-out
  autoSaveFormNumber      = request.getParameter("autoSaveFormNumber");  // The index number of the form being auto-saved, defaults to 0
  if (autoSaveFormNumber==null) autoSaveFormNumber = "0";

  // Retrieve the user's branding directory to obtain the appropriate css stylesheet
  if (request.getParameter("brandingDirectory") != null) {
    brandingDirectory = StringFunction.xssCharsToHtml(request.getParameter("brandingDirectory"));
  }
  else {
    brandingDirectory = userSession.getBrandingDirectory();

    if (brandingDirectory == null || brandingDirectory.equals("")) {
      brandingDirectory = TradePortalConstants.DEFAULT_BRANDING;
    }
  }
  //explicit mappings to default brand
  // todo: put in properties files
  if ( "blueyellow".equals(brandingDirectory) ) {
      brandingDirectory = TradePortalConstants.DEFAULT_BRANDING;
  }
  //for anz we do a translation
  if ( "anztransac".equals(brandingDirectory) ) {
    brandingDirectory = "anz";
  }


  //cquinton 8/28/2012 Rel portal refresh start
  //calculate textual branding heading.
  // the stylesheet will specify whether to display or not
  // If a branded header is available, it will exist as a text resource entry
  // with the key Header.PageHeading.xxx, where xxx is the branding directory (case sensitive)
  String brandedHeadingTextResourceKey = "Header.PageHeading."+brandingDirectory;
  String headingText;
  try {
    // Attempt to get the branded heading
    headingText = resMgr.getResource(TradePortalConstants.TEXT_BUNDLE).getString(brandedHeadingTextResourceKey);
  }
  catch (MissingResourceException e) {
    // If none exists, use the default
    headingText = resMgr.getText("Header.PageHeading", TradePortalConstants.TEXT_BUNDLE);
  }

  // Set the current tab in the user's session
  currentPrimaryNavigation = userSession.getCurrentPrimaryNavigation();

  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  userSecurityType = userSession.getSecurityType();

  // When a timeout uccurs, the userSession bean loses its data.  Default the
  // user type to "" to prevent a null pointer
  if (userSecurityType == null) {
    userSecurityType = "";
  }

  // Update the performance statistic object with the client bank code
  // Since all actions will run the code contained in the header,
  // this is where the client bank code is set.
  PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

  if(perfStat != null) {
    perfStat.setOrganizationCode(userSession.getClientBankCode());
  }

  // Get the version number from the TradePortalVersion.properties file.  If we
  // cannot determine the version, assign the unknown version value to it.
  String version = resMgr.getText("version", "TradePortalVersion");
  if (version.equals("version")) {
    version = resMgr.getText("Footer.UnknownVersion",
                             TradePortalConstants.TEXT_BUNDLE);
  }
  
  //M Eerupula 03/21/2013 Rel 8.2 T36000014955 Below line is commented as the version is encrypted based on User session's secret key which results in different URLs on logon and homepage
  // W Zhu 1/16/2013 Rel 8.1 T36000006705 #335 encrypt version number
  // version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
  
  //M Eerupula 03/21/2013 Rel 8.2 T36000014955 version number is encrypted consistently using Base64 to fix the URL difference for cache busting, means the version is same on all included pages 
  
  //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 Starts.  
  version = EncryptDecrypt.bytesToBase64String(version.getBytes());// RKAZI Rel 9.1 08/29/2014 Dojo Minificatio - Start - we need to use version from TradePortalVersion.properties to make caching work properly. 
 	//version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey()); // using userSesion.getSecretKey() will create unique version key 
																								 // for every user login effectively making caching not be used at all. So commneted this out. 
  	version = version.substring(0, 4);
  //RPasupulati geting first 4 char's from encripted version String IR no T36000017311 ENDs.
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  //cquinton 1/4/2013 get some values for i18n
  String headerLang = null;
  Locale headerLocaleObj = resMgr.getResourceLocaleValue();
  if ( headerLocaleObj!=null ) {
    headerLang = headerLocaleObj.getLanguage();
  }
  String headerLocale = resMgr.getResourceLocale();
  headerLocale = StringFunction.xssCharsToHtml(headerLocale);
  String clientBankCode = userSession.getClientBankCode();
  String pageName = formMgr.getNavigateToPage();
  String isSLAMonitoringOn = userSession.getIsSLAMonitoringOn();
  
  String isActive = "N";
  String cTime = null;
  if (TradePortalConstants.INDICATOR_YES.equals(isSLAMonitoringOn)){
  	isActive = PerfStatConfigManager.getPerfStatConfigMgr(formMgr.getServerLocation()).getPerfStatConfig(pageName, clientBankCode, "N");
  	cTime = request.getParameter("cTime");
  }
%>

<%--the html 5 doc type - causes browsers to use standard mode rather than quirky mode.--%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10" />
  <%-- the stylesheet - note dependencies are included as imports in this --%>
  <%-- we use the version on the stylesheet url as a cache busting mechanism --%>
  <link rel="stylesheet" type="text/css" href="/portal/themes/<%=brandingDirectory %>/portalstyle.css?<%=version%>" />

  <%-- cquinton 1/2/2013 - use jsp comment rather than xml comment to remove from html
  //START - rkumar - 12/05/2012 - FIX FOR DOM 8 EXCEPTION
  /*
     DO NOT DELETE THE EMPTY <SCRIPT></SCRIPT> TAG BELOW. PLEASE READ THE FOLLOWING FOR THE REASON.

     FAILING TO DO SO WILL MAKE DATAGRID FAIL ACROSS TRADE PORTAL APPLICATION

     WHEN DOJO LOADER IS TRYING TO INSERT SCRIPT ELEMENT INTO THE PAGE WITH DATAGRID,
     DOM 8 EXCEPTION WAS THROWN AT RANDOM IN IE AND CHROME. THIS IS DUE TO THE FACT THAT
     THE ELEMENT BEFORE THE CURRENT NODE WAS NOT EXISTED IN DOCUMENT. THIS IS A WIERED ERROR.
     AFTER CALL WITH SITEPEN ON DEC 04, 2012 (BRIAN ARNOLD), THE SOLUTION SUGGESTED WAS TO INSERT
     AN EMPTY <SCRIPT></SCRIPT> TAG IN THE PAGE HEADER, SO THAT DOJO LOADER WILL FIND ATLEAST THIS
     SCRIPT ELEMENT AS ELEMENT BEFORE AND DON'T FAIL
  */
  --%>
  <script>
  		var  isActive = '<%=isActive%>';
  		var context = '<%=formMgr.getNavigateToPage()%>';
  	  <% if (TradePortalConstants.INDICATOR_YES.equals(isSLAMonitoringOn)){  		  
  			//XSS Rel 9.2 Coverity CID 11532,11220,11216,11212 start
			String xssTransOid = null; 
			xssTransOid = request.getParameter("transOid");
			/*if(null != xssTransOid){
			 xssTransOid = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssTransOid));
			}*/
			String xssPrevPage = null; 
			xssPrevPage = request.getParameter("prevPage");
			/*if(null != xssPrevPage){
				xssPrevPage = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssPrevPage));
			}*/
			String xssCTime = null; 
			String nullValue = null;
			xssCTime = request.getParameter("cTime");
			/*if(null != xssCTime){
				xssCTime = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssCTime));
			}*/
			String xssNavId = null; 
			xssNavId = request.getParameter("ni");
			/*if(null != xssNavId){
				xssNavId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssNavId));
			}*/
			//XSS rel 9.2 End
  	  
  	  %>
		  var creationTimeStamp = '<%=DateTimeUtility.formatDate(GMTUtility.getGMTDateTime(), "dd-MMM-yyyy HH:mm:ss")%>';
		  var timeStarted = (new Date()).getTime();
		  var gridsToLoad = 0;
		  var gridsLoaded = 0;
		  var gridTimes = {};
		<%-- XSS Rel 9.2 Coverity CID 11532,11220,11216,11212 --%>
		 <%--MEer Rel 9.3 XSS CID-11611, 11851, 11853 --%>
		  var navId = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssNavId))%>';		
		  var transOid = '<%=(StringFunction.isBlank(xssTransOid)?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssTransOid)))%>';
	      var cStartTime = '<%=(StringFunction.isBlank(xssCTime) ?TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssCTime)))%>';
	      var prevPage = '<%=(StringFunction.isBlank(xssPrevPage) ? TradePortalConstants.BLANK:StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssPrevPage)))%>';
      <%}%>
      </script>
<%-- script src="<%=userSession.getdojoJsPath()%>/t360/slaMonitor.js?<%= version %>"></script--%>
  <%--
  //DO NOT DELETE THE EMPTY <SCRIPT></SCRIPT> TAG ABOVE. PLEASE READ THE COMMENT ABOVE
  //END - rkumar - 12/05/2012 - FIX FOR DOM 8 EXCEPTION
  --%>
</head>

<%--note: no onLoad - use dojo domReady! instead!!!--%>
<%--cquinton 1/4/2013 add lang and locale classes to provide those contexts to css.
    This allows the ability of css to use language or locale specific images or other modifications
    as necessary.  This would be done by using the lang or locale classes as a selector, for example:
      .lang_fr .headerBrandName {}
      .locale_fr_CA .headerBrandName {}
--%>
<body class='claro<%=(headerLang!=null)?" lang_"+headerLang:""%><%=(headerLocale!=null)?" locale_"+headerLocale:""%>'>

<%
  //cquinton 2/14/2013 if expandedPage, don't display the header at all!
  String expandedPage = request.getParameter("expandedPage");

  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 start
  // do not include header if flag so indicates. note that default is to yes display
  if (!TradePortalConstants.INDICATOR_NO.equals(displayHeader)) {
%>
<div class="pageHeader"<%=("Y".equals(expandedPage))?" style=\"display: none;\"":""%>>
  <div class="pageContent">
<% //style of header is different if basic things don't exist or personalization stuff isnt supposed to display.
   if ( userSecurityType!=null && userSecurityType.length()>0 &&
        !TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(minimalHeaderFlag)) {
%>
    <div class="headerContent">
<%
   } else { //some basics are missing or no personalization. this applies for login page, detail pages, etc.
%>
    <div class="headerContentMinimal">
<%
   }
%>

      <div class="headerLogo"></div>
      <div class="headerTitle">
        <%--cquinton 8/28/2012 Rel portal refresh start
            add span for textual branding.  stylesheet should specify whether visible or not --%>
        <div class="headerBrandName"><span><%=headingText%></span></div>
        <%--cquinton 8/28/2012 Rel portal refresh end --%>
<%
   if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(minimalHeaderFlag)) {
      // If the user is accessing the Trade Portal via customer access or subsidiary access
      // place the name of the corporate customer they are accessing in the header
      String parentSubTitle = "";
	  String subsidiarySubTitle = "";
      if (userSession.hasSavedUserSession()) {
        parentSubTitle = userSession.getSavedUserSession().getOrganizationName();
		if (userSession.isUsingSubsidiaryAccess()) {
			subsidiarySubTitle += resMgr.getText("Header.SubsidiaryPrefix", TradePortalConstants.TEXT_BUNDLE) + " ";
		} else {
			subsidiarySubTitle += resMgr.getText("Header.CustomerPrefix", TradePortalConstants.TEXT_BUNDLE) + " ";
		}
        subsidiarySubTitle += userSession.getOrganizationName();
      }
      else {
        parentSubTitle = userSession.getOrganizationName();
      }

      //cquinton 12/31/2012 resolve issues with subtitle centering.
      // previously this only worked in non-ie browsers
      //note use of '...CenteringParent' and '...CenteringChild' classes
      // which use table in css to achieve vertical centering
      // for more details see the default css classes
%>
        <div class="headerSubTitle">
          <div class="headerSubTitleCenteringParent">
            <div class="headerSubTitleCenteringChild">
              <div class="parentSubTitle">
                <span><%= StringFunction.xssCharsToHtml(parentSubTitle) %></span>
              </div>
              <div style="clear:both;"></div>
<%
      if (userSession.hasSavedUserSession()) {
%>
              <div class="subsidiarySubTitle">
                <span><%= StringFunction.xssCharsToHtml(subsidiarySubTitle) %></span>
              </div>
<%
      }
%>
            </div>
          </div>
        </div>

<%
   }
%>
      </div>

<%
   if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(minimalHeaderFlag)) {
       String homeLink = formMgr.getLinkAsUrl("goToTradePortalHome", response);
       String helpLink;
       if (TradePortalConstants.ADMIN.equals(userSecurityType)) {
           helpLink = OnlineHelp.getURLForHelp("admin/admin_help.htm", resMgr);
       }
       else {
           helpLink = OnlineHelp.getURLForHelp("/customer/customer_help.htm", resMgr);
       }
       String logoutLink = formMgr.getLinkAsUrl("logout", response);
       String userLink;
       if (TradePortalConstants.ADMIN.equals(userSecurityType)) {
    	   userLink = formMgr.getLinkAsUrl("goToAdminUserPreferences", response);
       }
       else {
    	   userLink = formMgr.getLinkAsUrl("goToUserPreferences", response);
       }
      // userLink = formMgr.getLinkAsUrl("goToUserPreferences", response);
%>
      <div class="headerBehavior">
        <div class="headerActions">
          <%-- cquinton 1/4/2013 Home text needs to come from resource bundle --%>
          <span class="headerHome"><a href="<%=homeLink%>">
             <%= resMgr.getText("Header.Home", TradePortalConstants.TEXT_BUNDLE) %>
          </a></span>
          <span class="headerActionDivider"></span>
<%
       //header messages and notifications - only retrieve data and format this if the user is a corporate user
       //cquinton 8/9/2012 Rel portal refresh start
       //only display main/notifications for non-admin users
       if ( !TradePortalConstants.ADMIN.equals(userSecurityType) &&
            SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.ACCESS_MESSAGES_AREA) ) {
           //cquinton 8/9/2012 Rel portal refresh end
           //cquinton 4/9/2013 Rel PR ir#15704 pass in text key
           String messagesUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__MAIL +
               "&currentFolder=" + TradePortalConstants.INBOX_FOLDER +
               "&currentMailOption=" +
                   EncryptDecrypt.encryptStringUsingTripleDes("Mail.MeAndUnassigned", userSession.getSecretKey());
           String messagesLink = formMgr.getLinkAsUrl("goToMessagesHome", messagesUrlParm, response);
%>
          <span class="headerMessages"><a href="<%=messagesLink%>">
<%
           int unreadMessagesCount = 0;
           int totalMessagesCount = 0;
           StringBuffer msgWhere = new StringBuffer();
           java.util.List<Object> sqlParamsLst = new java.util.ArrayList();

           // If the user is accessing on behalf of a subsidiary, do not show the message for this user.
           // Only show the messages for the susidiary organization.
           if (!userSession.hasSavedUserSession())  //!isCustomerSubsidiaryAccess
           {
               msgWhere.append("(a_assigned_to_user_oid = ?  or ");
               sqlParamsLst.add(userSession.getUserOid());
           }
           msgWhere.append("(a_assigned_to_corp_org_oid = ? ");
           msgWhere.append(" and a_assigned_to_user_oid is null)");
           sqlParamsLst.add(userSession.getOwnerOrgOid());
           if (!userSession.hasSavedUserSession())  //!isCustomerSubsidiaryAccess
           {
               msgWhere.append(")");
           }
           msgWhere.append( " AND " );
           msgWhere.append( "message_status in (?,?,?) ");
           sqlParamsLst.add(TradePortalConstants.REC);
           sqlParamsLst.add(TradePortalConstants.REC_ASSIGNED);
           sqlParamsLst.add(TradePortalConstants.SENT_ASSIGNED);

           String totalMsgsWhere = msgWhere.toString();

           msgWhere.append( " AND " );
           msgWhere.append( "unread_flag = '" );
           msgWhere.append( TradePortalConstants.INDICATOR_YES );
           msgWhere.append( "'" );

           String unreadMsgsWhere = msgWhere.toString();

           unreadMessagesCount = DatabaseQueryBean.getCount( "message_oid", "mail_message",unreadMsgsWhere , false, sqlParamsLst );
           totalMessagesCount = DatabaseQueryBean.getCount( "message_oid", "mail_message",totalMsgsWhere , false, sqlParamsLst );
          
           if ( unreadMessagesCount > 0 ) {
%>
<%=unreadMessagesCount%>
<%
           }
%>
(<%=totalMessagesCount%>)
	  </a></span>
          <span class="headerActionDivider"></span>


<%
           String notificationsUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__NOTIFICATIONS;
           String notificationsLink = formMgr.getLinkAsUrl("goToMessagesHome", notificationsUrlParm, response);
%>
          <span class="headerNotifications"><a href="<%=notificationsLink%>">
<%
       int unreadNotifCount = 0;
       int totalNotifCount = 0;
       String notifWhere =  "t.p_instrument_oid = i.instrument_oid AND t.show_on_notifications_tab = ? AND ((i.a_corp_org_oid = ? ) AND (t.transaction_status in (?,?,?,?) OR rejection_indicator is not null))";

       String totalNotifWhere = notifWhere.toString();
       List<Object> sqlParams = new ArrayList<Object>();
       sqlParams.add(TradePortalConstants.INDICATOR_YES);
       sqlParams.add(userSession.getOwnerOrgOid());
       sqlParams.add(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK);
       sqlParams.add(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK);
       sqlParams.add(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT);
       sqlParams.add(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT);

       totalNotifCount = DatabaseQueryBean.getCount( "t.transaction_oid", "transaction t, instrument i", totalNotifWhere, false, sqlParams);

       //cquinton 1/2/2013 remove unread notification count as it is failing and
       // is not a valid query anyway - no distinction between read and unread notifications
       //below commented out line is for testing header sizing with large number of notifications
       //totalNotifCount = 99999;
       //totalNotifCount = 99;
%>
            (<%=totalNotifCount%>)
          </a></span>
          <span class="headerActionDivider"></span>
<%
     }  
		//Rpasupulati CR 1029 Starts
		
		if ( TradePortalConstants.ADMIN.equals(userSecurityType) && userSession.isEnableAdminUpdateInd() && 
				(SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_DELETE) || 
						  SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_CREATE_REPLY) || 
						  SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_SEND_TO_CUST) || 
						  SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_ATTACH_DOC) || 
						  SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_DELETE_DOC)
						  )
				) {
            //cquinton 8/9/2012 Rel portal refresh end
            //cquinton 4/9/2013 Rel PR ir#15704 pass in text key
            String messagesUrlParm = "&current2ndNav=" + TradePortalConstants.NAV__MAIL +
                "&currentFolder=" + TradePortalConstants.INBOX_FOLDER +
                "&currentMailOption=" +
                    EncryptDecrypt.encryptStringUsingTripleDes("Mail.MeAndUnassigned", userSession.getSecretKey());
            String messagesLink = formMgr.getLinkAsUrl("goToBankMessagesHome", messagesUrlParm, response);
 %>
           <span class="headerMessages"><a href="<%=messagesLink%>">
 <%
            int unreadMessagesCount = 0;
            int totalMessagesCount = 0;
            StringBuffer msgWhere = new StringBuffer();
            java.util.List<Object> sqlParamsLst = new java.util.ArrayList();
            
            msgWhere.append(" bank_mail_message.a_assigned_to_corp_org_oid = corporate_org.organization_oid ");	
            msgWhere.append(" AND corporate_org.cust_is_not_intg_tps = ? ");
            msgWhere.append(" AND corporate_org.a_bank_org_group_oid = bank_organization_group.organization_oid ");
            sqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
            if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
             
     		   msgWhere.append(" and bank_organization_group.organization_oid not in ( ");
     		   msgWhere.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
     		   sqlParamsLst.add(userSession.getBankGrpRestrictRuleOid());
     		   msgWhere.append(") ");
                
         }
           if ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ) {
        	   msgWhere.append(" AND corporate_org.a_client_bank_oid = ? ");
        	   sqlParamsLst.add(userSession.getOwnerOrgOid());
           } else {
        	   msgWhere.append( " AND corporate_org.a_bank_org_group_oid = ? ");
        	   sqlParamsLst.add(userSession.getOwnerOrgOid());
        	        	   
           }
           
         
            msgWhere.append( " AND " );
            msgWhere.append( " message_status in (?,?,?) ");
            sqlParamsLst.add(TradePortalConstants.REC);
            sqlParamsLst.add(TradePortalConstants.REC_ASSIGNED);
            sqlParamsLst.add(TradePortalConstants.SENT_ASSIGNED);
            
            totalMessagesCount = DatabaseQueryBean.getCount( "bank_mail_message_oid", "bank_mail_message, corporate_org, bank_organization_group", msgWhere.toString() , false, sqlParamsLst );

            msgWhere.append( " AND " );
            msgWhere.append( " unread_flag = ? " );
            sqlParamsLst.add(TradePortalConstants.INDICATOR_YES);

            unreadMessagesCount = DatabaseQueryBean.getCount( "bank_mail_message_oid", "bank_mail_message, corporate_org,bank_organization_group", msgWhere.toString() , false, sqlParamsLst );
            
           
            if ( unreadMessagesCount > 0 ) {
 %>
 <%=unreadMessagesCount%>
 <%
            }
 %>
 (<%=totalMessagesCount%>)
 	  </a></span>
           <span class="headerActionDivider"></span>

 <%
      }
     //RPasupulati CR 1029 Ends
%>
          <span class="headerHelp"><a href="<%=helpLink%>">
             <%= resMgr.getText("common.Help", TradePortalConstants.TEXT_BUNDLE) %>
          </a></span>
          <span class="headerLogout"><a href="<%=logoutLink%>">
             <%= resMgr.getText("Header.Logout", TradePortalConstants.TEXT_BUNDLE) %>
          </a></span>
        </div>
        <div style="clear:both;"></div>

        <span class="headerUserName"><a href="<%=userLink%>"><%=StringFunction.xssCharsToHtml(userSession.getUserFullName())%></a></span>
        <div style="clear:both;"></div>
	
        <%-- CR 821 - Rel 8.3 START --%>
		<%
		String fromHomePage = request.getParameter("fromHomePage");		
		if(fromHomePage != null && ("Y").equals(fromHomePage)) {
			Date date = null;
			String at = resMgr.getText("Header.At", TradePortalConstants.TEXT_BUNDLE);
			SimpleDateFormat sdf=new SimpleDateFormat("dd MMMM yyyy '"+at+"' hh:mm:ss z",resMgr.getLocaleByLogicalName(userSession.getUserLocale()));
			sdf.setTimeZone(TimeZone.getTimeZone(userSession.getTimeZone()));
			if (StringFunction.isNotBlank(userSession.getLastLoginTimestamp())){
				date = DateTimeUtility.convertStringDateTimeToDate(userSession.getLastLoginTimestamp());
			%>
			
			<span class="headerLastLogin"><%=resMgr.getText("Header.LastSignInText", TradePortalConstants.TEXT_BUNDLE)%> <%= sdf.format(date).toString()%> </span>
		<%} 
		}%>
		<div style="clear:both;"></div>
		<%-- CR 821 - Rel 8.3 END --%>
      </div>


      <%-- do no support this yet
      <div class="headerSearch">
        <%= widgetFactory.createSearchTextField("Header.instrumentId", "Instrument ID", "15") %>
        <div class="headerSearchButton">Search</div>
      </div>
      --%>

<%
   }
%>


    </div>
    <div style="clear:both;"></div>

  </div>
</div>

<%
  // pageHeaderNavigation section
  if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(minimalHeaderFlag)) {
    //cquinton 2/14/2013 if expandedPage, don't display the header at all!
%>
<div class="pageHeaderNavigation"<%=("Y".equals(expandedPage))?" style=\"display: none;\"":""%>>
  <%--cquinton 1/29/2013 add a div that is not in the flow for background--%>
  <div class="pageHeaderNavigationBackground"></div>
  <div class="pageContent">
<%
      if (userSecurityType.equals(TradePortalConstants.ADMIN)) {
%>
         <jsp:include page="/common/AdminNavigationBar.jsp" />
<%
      }
      else {
        String newRecentTransactionOid = request.getParameter("newRecentTransactionOid");
        if (newRecentTransactionOid!=null) {
          String newRecentInstrumentId = request.getParameter("newRecentInstrumentId");
          String newRecentTransactionType = request.getParameter("newRecentTransactionType");
          String newRecentTransactionStatus = request.getParameter("newRecentTransactionStatus");
%>
         <jsp:include page="/common/NavigationBar.jsp" >
           <jsp:param name="newRecentTransactionOid" value="<%=newRecentTransactionOid%>" />
           <jsp:param name="newRecentInstrumentId"  value="<%=newRecentInstrumentId%>" />
           <jsp:param name="newRecentTransactionType" value="<%=newRecentTransactionType%>" />
           <jsp:param name="newRecentTransactionStatus" value="<%=newRecentTransactionStatus%>" />
         </jsp:include>
<%
        }
        else {
%>
         <jsp:include page="/common/NavigationBar.jsp" />
<%
        }
      }
   }
   else {
%>
<div class="pageHeaderNavigationMinimal">
  <%--cquinton 1/29/2013 add a div that is not in the flow for background--%>
  <div class="pageHeaderNavigationBackground"></div>
  <div class="pageContent">
    <div class="headerNavigationContentMinimal"></div>
<%
   }
%>
  </div>
</div>

<%
  }
  //cquinton 5/1/2013 Rel 8.1.0.6 ir#16256 end
%>