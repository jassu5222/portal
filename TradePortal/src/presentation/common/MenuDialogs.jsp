<%--
 *  Common place for the dialogs that are displayed from the menu.
 *  Note that the dialogs themselves are defined
 *  at \presentation\dialog.  Behavior below is specific to use from menus.
 *  
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%-- Hide dialog divs  --%>
<%--<div style="display:none;">--%>
<div>

  <div id="templateSearchDialog" ></div>
  <div id="instrumentSearchDialog" ></div>
  <div id="bankBranchSelectorDialog" ></div>
  <div id="transferExportLCDialog" ></div>

<%  //include a hidden form for new instrument submission
    // this is used for all of the new instrument types available from the menu
    Hashtable newInstrSecParms = new Hashtable();
    newInstrSecParms.put("UserOid", userSession.getUserOid());
    newInstrSecParms.put("SecurityRights", userSession.getSecurityRights());
    newInstrSecParms.put("clientBankOid", userSession.getClientBankOid());
    newInstrSecParms.put("ownerOrg", userSession.getOwnerOrgOid());
%>
  <form method="post" name="NewInstrumentForm" action="<%=formMgr.getSubmitAction(response)%>">
    <input type="hidden" name="mode" value="NEW_INSTRUMENT"/>
    <input type="hidden" name="copyType" value="Blank"/>
    <input type="hidden" name="bankBranch" />
    <input type="hidden" name="instrumentType" />
    <input type="hidden" name="transactionType" />
    <input type="hidden" name="copyInstrumentOid" />
    <%= formMgr.getFormInstanceAsInputField("NewInstrumentForm",newInstrSecParms) %>
  </form>
</div>

<%-- load javascript associated with these dialogs --%>
<%--<script src="<%=userSession.getdojoJsPath()%>/dialog/menuDialogs.js" type="text/javascript"></script>--%>
<%-- load javascript associated with menus --%>
<%--<script src="<%=userSession.getdojoJsPath()%>/menu.js" type="text/javascript"></script>--%>

<%
  //dialog titles - get them from text resource
  String templateSearchDialogTitle = resMgr.getText("templateSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String instrumentSearchDialogTitle = resMgr.getText("instrumentSearchDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String bankBranchSelectorDialogTitle = resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String transferExportLCDialogTitle = resMgr.getText("transferExportLCDialog.title", TradePortalConstants.TEXT_BUNDLE); 

  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String tradeSearchGridLayout = dgFactory.createGridLayout("tradeSearchGrid", "TradeSearchDataGrid");
%>
<script type='text/javascript'>

  <%-- define open dialog helper functions that pull in the dialog title --%>
  <%--  text resource --%>
  function openTemplateSearchDialogHelper(selectCallback) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    require(["t360/dialog"], function(dialog ) {
      dialog.open('templateSearchDialog', '<%=templateSearchDialogTitle%>',
                  'templateSearchDialog.jsp',
                  null, null, <%-- parameters --%>
                  'select', selectCallback);
    });
  }
  function openInstrumentSearchDialogHelper(selectCallback) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    require(["t360/dialog"], function(dialog ) {
      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
                  'instrumentSearchDialog.jsp',
                  null, null, <%-- parameters --%>
                  'select', selectCallback);

      var gridLayout = <%= tradeSearchGridLayout %>;
      var initTradeSearchParms = "none=blank";
      var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
      createDataGrid("tradeSearchGrid", viewName,
                     gridLayout, initTradeSearchParms);
    });
  }
  function openBankBranchSelectorDialogHelper(selectCallback) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    require(["t360/dialog"], function(dialog ) {
      dialog.open('bankBranchSelectorDialog', '<%=bankBranchSelectorDialogTitle%>',
                  'bankBranchSelectorDialog.jsp',
                  null, null, <%-- parameters --%>
                  'select', selectCallback);
    });
  }
  function openTransferExportLCDialogHelper(selectCallback) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    require(["t360/dialog"], function(dialog ) {
      dialog.open('transferExportLCDialog', '<%=transferExportLCDialogTitle%>',
                  'transferExportLCDialog.jsp',
                  null, null, <%-- parameters --%>
                  'select', selectCallback);
    });
  }

  <%--provide a search function that collects the necessary search parameters--%>
  function searchTrade() {
    require(["dojo/dom"],
      function(dom){
        var tradeSearchParms = "none=blank"; 
        searchDataGrid("tradeSearchGrid", "TradeSearchDataView",
                       tradeSearchParms);
      });
  }

</script>
