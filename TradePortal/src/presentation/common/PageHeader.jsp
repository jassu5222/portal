<%--
*******************************************************************************
  PageHeader

  The page header.
  Generic layout for a page header that does not include any complex
  secondary navigation stuff.  Allows a title, up to 3 items, and a help url.
  If anything more complex is necessary, just provide layout directly on your 
  page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*,java.net.URLEncoder" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
    String titleKey = StringFunction.xssCharsToHtml(request.getParameter("titleKey"));
    //KMehta - 16 Sep 2014 - Rel9.1 IR-T36000032300 - Change  
    String linkAction = StringFunction.xssCharsToHtml(request.getParameter("linkAction"));
    String titleKeyCamel = StringFunction.xssCharsToHtml(request.getParameter("titleKeyCamel")); //Komal ANZ Issue No.359
    String item1Key = StringFunction.xssCharsToHtml(request.getParameter("item1Key"));
    String item2Key = StringFunction.xssCharsToHtml(request.getParameter("item2Key"));
    String item3Key = StringFunction.xssCharsToHtml(request.getParameter("item3Key"));
    String refreshUrl = StringFunction.xssCharsToHtml(request.getParameter("refreshUrl"));
    String editUrl = StringFunction.xssCharsToHtml(request.getParameter("editUrl"));
    String helpUrl =  StringFunction.xssCharsToHtml(request.getParameter("helpUrl"));
    String isIgnore = request.getParameter("ignoreValidation");
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
%>


<div class="secondaryNav">
<%-- Komal ANZ Issue No.359 BEGINS --%>
<%//Validation to check Cross Site Scripting
  if ( titleKey != null ) {
	  if(!"Y".equals(isIgnore))
	  titleKey = StringFunction.xssCharsToHtml(titleKey);
	  //KMehta - 16 Sep 2014 - Rel9.1 IR-T36000032300 - Change  - Begin
	  if(linkAction != null){
%>

	 <span class="secondaryNavTitle"><a href="<%=linkAction%>">
	 	<%= resMgr.getText(titleKey, TradePortalConstants.TEXT_BUNDLE) %>
	 </a>
	 </span>
<% }else { %>
  <span class="secondaryNavTitle">
    <%=resMgr.getText(titleKey,
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>
<%}     //KMehta - 16 Sep 2014 - Rel9.1 IR-T36000032300 - Change  - Begin
	   }
%>

<%//Validation to check Cross Site Scripting
  if ( titleKeyCamel != null ) {
	  titleKeyCamel = StringFunction.xssCharsToHtml(titleKeyCamel);
%>
  <span class="secondaryNavTitleCamel">
    <%=resMgr.getText(titleKeyCamel,
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>
<%
  }
%>
<%-- Komal ANZ Issue No.359 ENDS  --%>
<%
  if ( item1Key != null ) {
%>
  <span class="secondaryNavItem">
    <%=resMgr.getText(item1Key,
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>
<%
  }
%>

<%
  if ( item2Key != null ) {
%>
  <span class="secondaryNavItem">
    <%=resMgr.getText(item2Key,
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>
<%
  }
%>

<%
  if ( item3Key != null ) {
%>
  <span class="secondaryNavItem">
    <%=resMgr.getText(item3Key,
                      TradePortalConstants.TEXT_BUNDLE)%>
  </span>
<%
  }
%>

<%
  if ( helpUrl != null || editUrl != null || refreshUrl != null ) {
%>
  <span class="secondaryNav-right">
<%
    //if there is a refresh in the header, we are just reloading the page
    if ( refreshUrl != null ) {
%>
    <span class="secondaryNavRefresh" id="secondaryNavRefresh">
      <a href="<%=refreshUrl%>"></a>
    </span>
<%
      //cquinton 12/7/2012 - sometimes we want the hover help to be about the page
      if ( "true".equals(request.getParameter("refreshPage")) ) {
%>
    <%=widgetFactory.createHoverHelp("secondaryNavRefresh","RefreshPageHoverText") %> 
<%
      }
      else {
%>
    <%=widgetFactory.createHoverHelp("secondaryNavRefresh","RefreshImageLinkHoverText") %> 
<%
      }
    }
%>
<%
    if ( editUrl != null ) {
      //note: hoverhelp for this should be specified by the page as it will be page specific
%>
    <span class="secondaryNavEdit" id="secondaryNavEdit">
      <a href="<%=editUrl%>"></a>
    </span>
<%
    }
%>

<% //note those on the right are entered in backwards order due to float style
    if ( helpUrl != null ) {
      String helpSensitiveLink =
        OnlineHelp.createContextSensitiveLink(helpUrl,  resMgr, userSession);
%>
    <span class="secondaryNavHelp" id="secondaryNavHelp">
        <%=helpSensitiveLink%>
    </span>
    <%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %> 
<%
    }
%>

  </span>
<%
  }
%>



  <div style="clear:both;"></div>

</div>
