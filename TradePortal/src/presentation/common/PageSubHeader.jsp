<%--
*******************************************************************************
  PageSubHeader

  The page header.
  Generic layout for a page sub header that does not include any complex
  secondary navigation stuff.  Allows a title, up to 3 items, and a help url.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
    scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%
  String titleKey = request.getParameter("titleKey");
  //cquinton 10/30/2012 ir#7015 - add title parameter when key lookup is not needed
  String title = request.getParameter("title");
  String titleBankInstIDKey  = request.getParameter("titleBankInstIDKey");
  //String refreshUrl = request.getParameter("refreshUrl");
  //String editUrl = request.getParameter("editUrl");
  String returnUrl = request.getParameter("returnUrl");
%>

<div class="subHeaderDivider"></div>
<div class="transactionSubHeader">

  <%--cquinton 9/7/2012 added pageSubHeaderItem
      so it floats left for ie7--%>
  <span class="pageSubHeaderItem">
<%//Validation to check Cross Site Scripting
  if (StringFunction.isNotBlank(titleKey)) {
	  titleKey = StringFunction.xssCharsToHtml(titleKey);
%>
    <%=resMgr.getText(titleKey,
                      TradePortalConstants.TEXT_BUNDLE)%>
<%
  }//Validation to check Cross Site Scripting
  else if (StringFunction.isNotBlank(title)) {
	  title = StringFunction.xssCharsToHtml(title);
%>
    <%= title %>
<% } %>
</span>

<% 
if (StringFunction.isNotBlank(titleBankInstIDKey)) {
	titleBankInstIDKey = StringFunction.xssCharsToHtml(titleBankInstIDKey);
%>
<span class="pageSubHeaderTitlePane-right">
<%=resMgr.getText(titleBankInstIDKey,   TradePortalConstants.TEXT_BUNDLE)%>
</span>
<%} %>

<%--cquinton 10/29/2012 ir#7015 remove the return link
<%
    if ( returnUrl != null ) {
%>
    <span class="pageSubHeader-right">
      <span class="pageSubHeaderReturn">
        <a href="<%=returnUrl%>">
          <%=resMgr.getText("PageSubHeader.Return",
                            TradePortalConstants.TEXT_BUNDLE)%>
        </a>
      </span>
    </span>

<%
    }
%>
--%>
  <div style="clear:both;"></div>
</div>
