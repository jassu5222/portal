<%--
*******************************************************************************
                             Phrase Lookup Button

  Description:
    This JSP creates HTML for the phrase lookup button.  You must include
  PhraseLookupPrep.jsp for this to work.  It will create an href image that
  acts like a submit button name PhraseLookup.

  Include this file with the following line:
        <jsp:include page="/common/PartySearchButton.jsp">
           <jsp:param name="imageName" value='someUniqueName' />
        </jsp:include>

  Parameters:
    imageName - a unique name that is assigned to the image.  This allows 
                multiple phrase lookup buttons to handle rollover correctly.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.common.*, com.amsinc.ecsg.util.StringFunction" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<%
  String source = "/portal/images/AddIcon_off.gif";
  String sourceRO = "/portal/images/AddIcon_ro.gif";
  String imageName = request.getParameter("imageName");


  if ((imageName == null) || imageName.equals("")) imageName = "phraseLookup";
%>
    <a href="javascript:document.forms[0].submit()" name="PhraseLookup"
       onMouseOut="changeImage('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(imageName))%>', '<%=source%>')"
       onMouseOver="changeImage('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(imageName))%>', '<%=sourceRO%>')"
       onClick="setButtonPressed('<%=TradePortalConstants.BUTTON_PHRASELOOKUP%>',0);">
       <img border="0" name="<%=StringFunction.xssCharsToHtml(imageName)%>" width=15 height=15
            src="<%=source%>"
            alt='<%=resMgr.getText("common.PhraseLookupText",
                                   TradePortalConstants.TEXT_BUNDLE)%>'></a>
