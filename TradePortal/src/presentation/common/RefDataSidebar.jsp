<%--
*
*     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
	<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,java.util.*"%>
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%--cquinton 10/10/2012 ir#6053 move sectionLinks here rather than in template so we can put
    titlepane with internationalized title--%>
<%
	boolean savedSectionValue =userSession.getSavedSectionValue();

%>
<div data-dojo-type="dijit.TitlePane" class="sectionLinksSection" id="sectionLinksSection" open ='<%=savedSectionValue%>'  title='<%=resMgr.getText("Sidebar.SectionShortcuts",TradePortalConstants.TEXT_BUNDLE)%>'>
  <script type="dojo/connect" data-dojo-event="toggle" data-dojo-args="evt">callAjaxToUpdateSection();return false;</script>
  <ol id="navContainer" class="sectionLinks"></ol>
</div>

<%--cquinton 9/13/2012 Rel portal refresh start
    add titlepane to sidebar actions--%>
<%
  boolean showLinks = false;
  boolean showTips = true;
  boolean showTop = false;
  WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
  
  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("true"))
      showLinks = true;
        
  if(request.getParameter("showTips")!=null)
    if(request.getParameter("showTips").equals("false"))
      showTips = false;
  
  if(request.getParameter("showTop")!=null)
	    if(request.getParameter("showTop").equals("true"))
	      showTop = true;
%>
<%= widgetFactory.createSidebarQuickLinks(showLinks, showTips, showLinks || showTop, userSession) %>
<%--PR IR-T36000006128 Rakesh Begin--%>
<%--cquinton 9/13/2012 Rel portal refresh end--%>

<%

/******************************************************************************************
* THIS JSP WILL BUILD SAVE, DELETE, CLOSE, AND HELP BUTTONS IN THAT ORDER.
*
* SPECIAL NOTES:
*      ButtonPrep.jsp must be included in the jsp from which this is called for this jsp
*          to work properly.  And it must be included prior to the point at which this jsp
*          is called
*      This jsp must be included INSIDE an HTML table cell
*
* REQUIRED PARAMETERS:
*      boolean showSave
*      boolean showDelete
*      boolean showClose
*      boolean showHelp
*      String  cancelAction
*      String  helpLink
******************************************************************************************/

    boolean showSave   = false;
    boolean showSaveClose   = false;
    boolean showSaveAs  = false;
    boolean showDelete = false;
    boolean showClose  = true;
    boolean showHelp   = false;
	boolean showSaveAsDraft = false;
	boolean showPreview = false;
	boolean showDeactivate = false;
	String saveAsDialogPageName = null;
	String saveAsDialogId = null;
	boolean showCreateCreationRule = false;
	//jgadela rel8.3 CR501 [START] 
	boolean showApprove  = false;
	boolean showReject  = false;
	//jgadela rel8.3 CR501 [END] 
	
    
    if("true".equals(request.getParameter("showSaveButton")))
        showSave = true;
    String saveOnClick = request.getParameter("saveOnClick");
    if("true".equals(request.getParameter("showSaveCloseButton")))
      showSaveClose = true;
    String saveCloseOnClick = request.getParameter("saveCloseOnClick");
    if("true".equals(request.getParameter("showSaveAsButton")))
      showSaveAs = true;
    if("true".equals(request.getParameter("showDeleteButton")))
        showDelete = true;
    if("false".equals(request.getParameter("showCloseButton")))
        showClose = false;
    if("true".equals(request.getParameter("showHelpButton")))
        showHelp = true;
    if("true".equals(request.getParameter("showSaveAsDraftButton")))
    	showSaveAsDraft = true;
    if("true".equals(request.getParameter("showPreviewButton")))
    	showPreview = true;
    if("true".equals(request.getParameter("showDeactivateButton")))
    	showDeactivate = true;
    if("true".equals(request.getParameter("showCreateCreationRule")))
    	showCreateCreationRule = true;

    String helpLink = request.getParameter("helpLink");    
    
	saveAsDialogPageName = request.getParameter("saveAsDialogPageName");
    
    saveAsDialogId = request.getParameter("saveAsDialogId"); 
    
  //jgadela rel8.3 CR501 [START] 
    if("true".equals(request.getParameter("showApproveButton")))
        showApprove = true;
    if("true".equals(request.getParameter("showRejectButton")))
        showReject = true;
  //jgadela rel8.3 CR501 [END] 
		  
	//Rel 9.2 XSS CID 11211, 11224 Start
	//Rel 9.3 XSS CID-11211
	/*if(null != saveAsDialogPageName){
		saveAsDialogPageName = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(saveAsDialogPageName));
    }
	if(null != saveAsDialogId){
		saveAsDialogId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(saveAsDialogId));
    }*/
	//Rel 9.2 XSS End
    
%>

<div class="saveTime"> 
	<% 
		String button ="";
		String saveTime = "";
		String errors = request.getParameter("error");
		button = request.getParameter("buttonPressed");
		
		if(errors !=null && button !=null){
			if(button.equals("Verify") || button.equals("SaveTrans")){
				if(errors.contains("I001") || errors.contains("I002")) {
					saveTime = TPDateTimeUtility.getCurrentTimeForRequestTimeZone(userSession.getTimeZone());
	%>
					<%=resMgr.getText("Sidebar.SavedTime",TradePortalConstants.TEXT_BUNDLE)%> <%=saveTime.toLowerCase()%>
	<%
				}
			}
		}
	%>
	</div>
<ul class="sidebarButtons">


<% //jgadela rel8.3 CR501 [START]  %>
<% 
   if (showApprove) {
%>
  <li>
    <button data-dojo-type="dijit.form.Button"  name="ApproveButton" id="ApproveButton" type="button" data-dojo-props="iconClass:'authorize'" >
      <%=resMgr.getText("PendingRefdata.Approve.Button",TradePortalConstants.TEXT_BUNDLE)%>
<%
   // if (!"none".equals(saveCloseOnClick)) {
%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_APPROVE_PENDING_REF_DATA%>', '0');
        document.forms[0].submit();
      </script>
<%
   // }
%>
    </button> 
  </li>   
  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
<%
  }
  
   if (showReject) {
%>
  <li>
    <button data-dojo-type="dijit.form.Button"  name="RejectButton" id="RejectButton" type="button" data-dojo-props="iconClass:'reject'" >
      <%=resMgr.getText("PendingRefdata.Reject.Button",TradePortalConstants.TEXT_BUNDLE)%>
<%
   // if (!"none".equals(saveCloseOnClick)) {
%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_REJECT_PENDING_REF_DATA%>', '0');
        document.forms[0].submit();
      </script>
<%
   // }
%>
    </button> 
  </li>   
  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
<%
  }
%>
<% //jgadela rel8.3 CR501 [END]  %>
  
 



<%
  if (showSave) {
%>
  <li>
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="SaveButton" id="SaveButton" type="button" data-dojo-props="iconClass:'save'" >
      <%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
<%--cquinton 10/8/2012 ir#5988 do not include save script if saveOnClick = "none" --%>
<%
    if (!"none".equals(saveOnClick)) {
%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        //setButtonPressed('<%=TradePortalConstants.BUTTON_SAVETRANS%>', '0');
        //document.forms[0].submit();
      </script>
<%
    }
%>
    </button> 
  
  </li> 
  <%=widgetFactory.createHoverHelp("SaveButton","SaveHoverText") %>   
<%
  }
  if (showSaveAsDraft) {
%>
  <li>    
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="SaveDraftButton" id="SaveDraftButton" type="button" data-dojo-props="iconClass:'save'" >
      <%=resMgr.getText("common.SaveDraftText",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_SAVEASDRAFT%>', '0');
        document.forms[0].submit();
      </script>
    </button> 
  </li>   
<%
  }
  if (showPreview) {
%>
  <li>
    <%-- Preview should open a popup. This should not submit the page--%>
    <button data-dojo-type="dijit.form.Button"  name="PreviewButton" id="PreviewButton" type="button" data-dojo-props="iconClass:'preview'" >
      <%=resMgr.getText("common.PreviewText",TradePortalConstants.TEXT_BUNDLE)%>
    </button> 
  </li>		
<%  
  } 
  if (showSaveClose) {
%>
  <li>
    <%--cquinton 10/8/2012 ir#5988 rename button to match footer event handler --%>
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="SaveCloseButton" id="SaveCloseButton" type="button" data-dojo-props="iconClass:'saveclose'" >
      <%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
<%--cquinton 10/8/2012 ir#5988 do not include save script if saveCloseOnClick = "none" --%>
<%
    if (!"none".equals(saveCloseOnClick)) {
%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_SAVEANDCLOSE%>', '0');
        document.forms[0].submit();
      </script>
<%
    }
%>
    </button> 
  </li>   
  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
<%
  }
  if (showSaveAs) {
    String saveAsTitle = resMgr.getText("SecurityProfileDetail.SaveAsTitle",TradePortalConstants.TEXT_BUNDLE);
%>
  <li>    
  <button data-dojo-type="dijit.form.Button" name="SaveAs" id="SaveAs" data-dojo-props="iconClass:'saveAs'" type="button">
    <%=resMgr.getText("common.SaveAsText",TradePortalConstants.TEXT_BUNDLE)%>
    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
      openSaveAsDialog('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(saveAsDialogId)) %>', '<%= saveAsTitle %>','<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(saveAsDialogPageName)) %>');
    </script>
 
  </button>
  </li>  
   <%=widgetFactory.createHoverHelp("SaveAs","SaveAsHoverText") %>   
<%
  }
  
  if (showDelete) {
%>
  <li>   
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="DeleteButton" id="DeleteButton" type="button" data-dojo-props="iconClass:'delete'" >
      <%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		<%-- IR T36000048558 Rel9.5 05/09/2016 --%>
        var confirmDelete = confirm("<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("common.confirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE))%>");
            
            if(confirmDelete==true){                        
                  setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
                  document.forms[0].submit();
            }
		return false;  
      </script>
    </button> 
  </li>  
   <%=widgetFactory.createHoverHelp("DeleteButton","DeletHoverText") %>    
<%
  }
  if (showDeactivate) { 
%>
   <li>
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="DeactivateButton" id="DeactivateButton" type="button" data-dojo-props="iconClass:'delete'" >
      <%=resMgr.getText("common.DeactiveText", TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_DEACTIVATE%>', '0');
        document.forms[0].submit();
      </script>
    </button> 
  </li>
<%
  }

  if (showCreateCreationRule) {
%>
  <li>                    
    <%--cquinton 12/6/2012 make button type 'button' so ie does not double submit.--%>
    <button data-dojo-type="dijit.form.Button"  name="CreateATPCreationRule" id="CreateATPCreationRule" type="button" >
      <%=resMgr.getText("POUploadDefinitionDetail.CreateCreationRule",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('CreateATPCreationRule', '0');
        document.forms[0].submit();
      </script>
    </button> 
  </li>
  <%=widgetFactory.createHoverHelp("CreateATPCreationRule","CreateATPCreationRuleHoverText") %>
<%
  }

  Hashtable secureParmsClose = new Hashtable(); 
  if(request.getParameter("pay_remit_oid") != null){
  	secureParmsClose.put("pay_remit_oid",request.getParameter("pay_remit_oid"));
  }
  //cquinton 8/28/2012 always show close button
  String cancelAction = request.getParameter("cancelAction");
  String closeLink = "";
  if ( cancelAction == null || cancelAction.equals("") || "null".equals(cancelAction))  { //default it if necessary
    cancelAction = "goToRefDataHome";
  }
  if(secureParmsClose != null){
	  closeLink=formMgr.getLinkAsUrl(cancelAction, response);
  }else{
	  closeLink=formMgr.getLinkAsUrl(cancelAction, response, secureParmsClose);
  }
 
  //SHR add this condition so that showClose is default to true. If not needed,then pass
  //showCloseButton = false from jsp
  if (showClose) {
	  
%>
  <li>
    <button data-dojo-type="dijit.form.Button"  name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
      <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        openURL("<%=closeLink%>");
      </script>
    </button>
     </li>        
     <%
  }
  %>            
  <%--cquinton 8/28/2012 Rel portal refresh end--%> 
<%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
</ul>
 <input type="hidden" name="cTime" id="cTime" />
 <input type="hidden" name="prevPage" id="prevPage" />
 <input type="hidden" name="autoSaved" id="autoSaved" />
<input type="hidden" name="templateAutoSaved" id="templateAutoSaved" />
 
<script type="text/javascript">
 
 function openURL(URL){
     if (isActive =='Y') {
    	 if (URL != '' && URL.indexOf("javascript:") == -1) {
	         var cTime = (new Date()).getTime();
	         URL = URL + "&cTime=" + cTime;
	         URL = URL + "&prevPage=" + context;
    	 }
     }
   document.location.href  = URL;
   return false;
 }
</script>