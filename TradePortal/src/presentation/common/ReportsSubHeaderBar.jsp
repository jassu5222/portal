<%--
*
*     Copyright   2012                         
 *     CGI Information Systems, Incorporated 
 *     All rights reserved
--%>
<%-- @ Developed by komal.mehta --%>
<%@ page 
      import="com.amsinc.ecsg.web.*, com.ams.tradeportal.common.cache.*, com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*, com.ams.tradeportal.html.*"%>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session"></jsp:useBean>
<jsp:useBean id="userSession"
      class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session"></jsp:useBean>

<%
String PartySearchAddressTitle =resMgr.getText("PartySearch.TabHeading",TradePortalConstants.TEXT_BUNDLE);
String pageTitleKey = null;
String returnLink = null;
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String button =  "";
//button = request.getParameter("buttonPressed");
String linkEditURL = "";
String linkSaveAsURL = "";
String linkDeleteURL = "";
String editNavPage = ""; 
String saveAsNavPage = ""; 
String deleteNavPage = ""; 
String sToken = null;
String repName = ""; 
String reportDesc = ""; 
String reportTab = "";
String editReportMode = "";
StringBuffer newLink = null;
String navPage = request.getParameter("navPage");
boolean showExpandCollapse  = false;
boolean showSaveAs  = false;
boolean showEdit  = false;    
boolean showDelete = false;

if("true".equals(request.getParameter("showExpandCollapse")))
  showExpandCollapse = true;
if("true".equals(request.getParameter("showSaveAsButton")))
  showSaveAs = true;
if("true".equals(request.getParameter("showEditButton")))
    showEdit = true;
if("true".equals(request.getParameter("showDeleteButton")))
    showDelete = true;  

String expandedPage = request.getParameter("expandedPage");

//XSS Rel 9.2 Coverity CID 11259,11329,11229,11228 Start
if(null != request.getParameter("pageTitleKey")){
	pageTitleKey = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("pageTitleKey")));
}

if( null != request.getParameter("sToken") ){
	sToken = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("sToken")));
}
/*if( null != request.getParameter("returnLink") ){
	returnLink = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("returnLink")));
}*/
returnLink = request.getParameter("returnLink");
//XSS Rel 9.2 End

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;
Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>


<%--cquinton 2/14/2013 move dividers to main page--%>

<%--cquinton 2/14/2013 format per standard page subheader--%>      
<div class="pageSubHeader">

  <span class="pageSubHeaderItem">
    <%=pageTitleKey%>   
  </span>

  <span class="pageSubHeader-right">
<% //12-19-2013  - Rel 8.4 CR-854 [BEGIN]- Added the reporting lang option -  Modified the button lables and hover text%>
<%   
  if (showExpandCollapse) { 
%>
    <button data-dojo-type="dijit.form.Button" name="CollapseButton" id="CollapseButton" type="button" 
            class="pageSubHeaderButton"<%=(!"Y".equals(expandedPage))?" style=\"display:none;\"":""%>> <%--starts out hidden--%>
      <%=resMgr.getText("Reports.Collapse",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.collapsePage();
      </script>
    </button>
    <%= widgetFactory.createHoverHelp("CollapseButton","Reports.Collapse", reportingPageLabelLan)%>

    <button data-dojo-type="dijit.form.Button" name="ExpandButton" id="ExpandButton" type="button" 
            class="pageSubHeaderButton"<%=("Y".equals(expandedPage))?" style=\"display:none;\"":""%>>
      <%=resMgr.getText("Reports.Expand",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.expandPage();
      </script>
    </button>
    <%= widgetFactory.createHoverHelp("ExpandButton", "Reports.Expand", reportingPageLabelLan)%>
<%
  }
  if (showSaveAs) { 
            
            //Save as Button
            saveAsNavPage = request.getParameter("saveAsNavPage");
            repName = request.getParameter("repName"); 
            reportDesc = request.getParameter("reportDesc"); 
                       
      %>
      
    <%--cquinton 2/14/2013 remove icon--%>      
    <button data-dojo-type="dijit.form.Button" type="button"
            name="SaveAsButton" id="SaveAsButton" 
            class="pageSubHeaderButton" onClick="routeMail();return false;">
      <%=resMgr.getText("Reports.SaveasButtonText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%> 
    </button>
    <%= widgetFactory.createHoverHelp("SaveAsButton","SaveAsHoverText", reportingPageLabelLan)%>                        
      
      <% }if (showEdit) {  %> 
                  <%
			       //XSS Rel 9.2 Coverity CID 11495,11402,11297 start
					String xssEditReportMode = null; 
                  	String xssCurrent2ndNav = null; 
                  	String xssEditNavPage = null; 
                  	xssEditReportMode = request.getParameter("editReportMode");
					 
					xssCurrent2ndNav = request.getParameter("current2ndNav");
					 
					xssEditNavPage = request.getParameter("editNavPage");
				 
					//XSS rel 9.2 End 
                  //Edit reports Button
                  newLink = new StringBuffer();
                //XSS Rel 9.2 Coverity CID 11495,11402,11297
                  newLink.append(StringFunction.escapeQuotesforJS(xssEditNavPage));
                  newLink.append("&storageToken=" + sToken);                
                  newLink.append("&reportMode=" + StringFunction.escapeQuotesforJS(xssEditReportMode));
                  newLink.append("&current2ndNav=" + StringFunction.escapeQuotesforJS(xssCurrent2ndNav));
                  linkEditURL = newLink.toString();
                  %>
    <%--cquinton 2/14/2013 remove icon--%>      
    <button data-dojo-type="dijit.form.Button"  name="EditButton" id="EditButton" type="button"
            class="pageSubHeaderButton"> 
      <%=resMgr.getText("Reports.EditButtonText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>  
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        openButtonURL("<%=linkEditURL%>");                                      
      </script>
    </button>
    <%= widgetFactory.createHoverHelp("EditButton","EditHoverText", reportingPageLabelLan)%> 
    
      <% } if (showDelete) { %>                       
                  <%
                  //XSS Rel 9.2 Coverity CID 11468,11348 start
					String xssDeleteNavPage = null; 
                  	String xssReportTab = null;
                  	xssDeleteNavPage = request.getParameter("deleteNavPage");
 					xssReportTab = request.getParameter("reportTab");
                   //Delet Button
                  newLink = new StringBuffer();
                //XSS Rel 9.2 Coverity CID 11468,11348
                  newLink.append(StringFunction.escapeQuotesforJS(xssDeleteNavPage));
                  newLink.append("&storageToken=" + sToken);
                  newLink.append("&reportTab=" + StringFunction.escapeQuotesforJS(xssReportTab));
                  linkDeleteURL = newLink.toString();
                  %> 
    <%--cquinton 2/14/2013 remove icon--%>      
    <button data-dojo-type="dijit.form.Button"  name="DeleteButton" id="DeleteButton" type="button"
            class="pageSubHeaderButton"> 
            <%=resMgr.getText("Reports.DeleteButtonText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>      
                <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                        if (confirmDelete()) openButtonURL("<%=linkDeleteURL%>");                                
                  </script>
              </button>
              <%= widgetFactory.createHoverHelp("DeleteButton", "DeleteHoverText" , reportingPageLabelLan)%> 
             
      <%  }  %>
    
    <%--cquinton 2/14/2013 remove icon--%>      
    <button data-dojo-type="dijit.form.Button"  name="CloseButton" id="CloseButton" type="button"
            class="pageSubHeaderButton"> 
      <%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        openURL("<%= StringFunction.escapeQuotesforJS(returnLink)%>");
      </script>
    </button>
    <%= widgetFactory.createHoverHelp("CloseButton","CloseHoverText", reportingPageLabelLan)%> 

  </span>
  <div style="clear:both;"></div>
</div>    

<div id="ReportDialog" ></div>  

<%--cquinton 2/14/2013 move dividers to main page--%>
<%
//XSS Rel 9.2 Coverity CID 11215, 11218  start
if(null != repName){
	repName = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(repName));
}
if(null != reportDesc){
	reportDesc = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(reportDesc));
}
//XSS End
%>

<script type="text/javascript">

function routeMail(){
      <%-- var keys = new Array(); --%>
      <%-- keys[0] = 'OrderingPartyName'; --%>
      <%-- keys[1] = 'OrderingPartyAddress1'; --%>
      <%-- keys[2] = 'OrderingPartyAddress2'; --%>
      <%-- keys[3] = 'OrderingPartyAddress3'; --%>
      <%-- keys[4] = 'OrderingPartyAddress4'; --%>
      <%-- keys[5] = 'OrderingPartyCountry'; --%>
      <%-- itemid = 'OrderingPartyName,OrderingPartyAddress1,OrderingPartyAddress2,OrderingPartyAddress3,OrderingPartyAddress4,OrderingPartyCountry'; --%>
      
      require(["t360/dialog"], function(dialog){
    	<%-- 01-06-014 - Rel 8.4 CR-854 T36000023777 - Dialog title label based on reporting language --%>
      dialog.open('ReportDialog', '<%=resMgr.getText("ReportDialog.Title",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan)%>',
      'EditReport_Step3.jsp',
      ['storageToken','reportMode','editReportName','reportDesc',],['<%=sToken%>','TradePortalConstants.REPORT_COPY_STANDARD','<%= StringFunction.escapeQuotesforJS(repName)%>','<%= StringFunction.escapeQuotesforJS(reportDesc)%>'],
      'select', this.reportsCallback);
      });
      dijit.byId("ReportDialog").set('style',"width:700px");
}

function reportsCallback(reportName, reportDesc,reportTypeStandard, standardCategory, customCategory ){
      console.log(reportName +"-" +reportDesc+"-" +reportTypeStandard+"-" + standardCategory+"-" +customCategory );
      if ( document.getElementsByName('saveReport').length > 0 ) {
    	  <%
          //XSS Rel 9.2 Coverity CID 11233 start
			String xssSelectedCategory = null;          	
    	  	xssSelectedCategory = request.getParameter("selectedCategory");
	
			%>
          var theForm = document.getElementsByName('saveReport')[0];
          theForm.ReportName.value = reportName;
          theForm.ReportDesc.value = reportDesc;
          theForm.StandardReport.value = reportTypeStandard;
          theForm.StandardCategory.value = standardCategory;
          theForm.selectedCategory.value = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(xssSelectedCategory))%>';
          theForm.CustomCategory.value = customCategory;
          console.log(theForm);
          theForm.submit();
      }
      }
      
      

function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
    	}
    }
       document.location.href  = URL;
       return false;
}  

 function openButtonURL(url){
       
       urlLinkWin = window.open(url, '_self', '');
       return false;
 }
 


 function confirmDelete() 
 {
       
      <%--  Display a popup asking if the user wants to delete.  If OK is pressed, --%>
      <%--  true is returned.  If Cancel is pressed, false it returned. --%>
      var msg = "<%=resMgr.getText("confirm.delete",TradePortalConstants.TEXT_BUNDLE)%>"
      if (!confirm(msg))
      {
            formSubmitted = false;
            return false;
       }
      return true;
  }
  
</script>
