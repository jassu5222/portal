<%--
*
*     Copyright   2012                         
 *     CGI Information Systems, Incorporated 
 *     All rights reserved
--%>
<%-- @ Developed by komal.mehta --%>
<%@ page
      import="com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*,com.ams.tradeportal.busobj.webbean.*,
      com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.html.*,java.net.URLEncoder"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session">
</jsp:useBean>

<%
WidgetFactory widgetFactory = new WidgetFactory(resMgr);

String button =  "";
//button = request.getParameter("buttonPressed");
StringBuffer        newLink                 = new StringBuffer();
String                        viewPDFLink                   = "";
String                        viewHTMLLink                  = "";
String                        sToken                            = "";
String                        viewExcelLink                 = "";
String                        viewCSVLink             = "";
sToken = request.getParameter("sToken"); 
String                        divID                         = request.getParameter("divID");
String                        csvReport                           = request.getParameter("csvReport");

//Rel 9.2 XSS CID 11413
if(null != sToken){
	sToken = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(sToken));
}

//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan1 = null;
String cbReportingLangOptionInd1 = null;

Cache CBCache1 = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult1 = (DocumentHandler)CBCache1.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd1 = CBCResult1.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd1)){
	reportingPageLabelLan1 = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan1 = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>     
<%
     //View PDF
     newLink = new StringBuffer();
     newLink.append("/portal/reports/viewPDF.jsp?");
     newLink.append("&storageToken=" + StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(sToken)));
     viewPDFLink = newLink.toString();
     //Validation to check Cross Site Scripting
     if(viewPDFLink != null){
    	 viewPDFLink = StringFunction.xssCharsToHtml(viewPDFLink);
     }
%>  
<%
     //View HTML
     newLink = new StringBuffer();
     newLink.append("/portal/reports/viewHTML.jsp?");
     newLink.append("&storageToken=" + StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(sToken)));
     viewHTMLLink = newLink.toString();
%>      
<%
      //View CSV
      // DK IR T36000024861 Rel8.4 03/03/2014 
     newLink = new StringBuffer();
    newLink.append("/portal/reports/viewCSV.jsp?");
    newLink.append("&storageToken=" + sToken);
         //Download report button
    //newLink = new StringBuffer();
   // newLink.append(request.getContextPath());
    // We use this weird formatting (/.jsp) because there is a problem with
    // Netscape 7 that adds the .jsp extension on to the end of the file name
    // However, we still need the request to get forwarded to the app server
    // by the webserver, so it must fit *.jsp.   Having it as /.jsp makes
    // it so that the file extension is incorrect (even in NS7) and also that
    // the request gets forwarded by Apache or IIS over to WebLogic
    // W Zhu 5/17/2013 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
    
    //newLink.append("/common/TradePortalDownloadServlet?");
    //newLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
    //newLink.append("=");
    //newLink.append(TradePortalConstants.REPORT_DOWNLOAD);
    //newLink.append("&storageToken=" + sToken);
    viewCSVLink = newLink.toString();
%>
<%
    //View Excel
    newLink = new StringBuffer();
    newLink.append("/portal/reports/viewEXCEL.jsp?");
    newLink.append("&storageToken=" + StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(sToken)));
    viewExcelLink = newLink.toString();
%>

<%--cquinton 2/14/2013 use standard sub header classes--%>
<div class="pageSubHeader-right" style="margin-top:3px">
             <span class="csv" id="csv" >
               <%--cquinton 11/24/2014 use a link rather than javascript to avoid an issue with IE not functioning after using this.--%>
               <a HREF="<%=viewCSVLink%>" target="_blank"></a>
             </span>
            
             <span class="pdf" id="pdf" >
               <%--cquinton 11/17/2014 use a link rather than javascript to avoid an issue with IE not functioning after using this.
                   This approach is better anyway as it works like imaging viewing of pdfs and allows user to open as many as they like.
                   _blank causes the pdf to open in new tab/window.--%>
               <a href="<%=viewPDFLink%>" target="_blank"></a>
             </span> 
             <span class="xls"  id="xls">
               <%--cquinton 11/24/2014 use a link rather than javascript to avoid an issue with IE not functioning after using this.--%>
               <a HREF="<%=viewExcelLink%>" target="_blank"></a>
             </span>
</div>

<script language="JavaScript"> 

function openURL(URL){
    if (isActive =='Y') {
    	if (URL != '' && URL.indexOf("javascript:") == -1) {
	    	var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
    	} 
    }
    document.location.href  = URL;
    return false;
}

<%--cquinton 11/24/2014 remove javascript to open csv/excel for viewing --%>

</script>

<%--cquinton 11/17/2014 remove javascript to open pdf for viewing --%>

<% //12-19-2013  - Rel 8.4 CR-854 [BEGIN]- Added the reporting lang option -  Modified the button lables and hover text%>
<%=widgetFactory.createHoverHelp("csv","CSVHoverText", reportingPageLabelLan1) %>
<%=widgetFactory.createHoverHelp("pdf","PDFHoverText", reportingPageLabelLan1) %>
<%=widgetFactory.createHoverHelp("xls","ExcelHoverText", reportingPageLabelLan1) %>
