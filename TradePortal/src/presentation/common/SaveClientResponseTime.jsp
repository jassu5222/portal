<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*, com.amsinc.ecsg.util.PerformanceStatistic, java.text.SimpleDateFormat" %>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<%

	String startTime = request.getParameter("startTime");
	startTime = (startTime == null || "NaN".equals(startTime)) ? "0" : startTime;
	String stopTime = request.getParameter("endTime");
	stopTime = (stopTime == null || "NaN".equals(stopTime)) ? "0" : stopTime;
	try {
		Long.valueOf(startTime);
	}catch(Exception e){
		startTime = "0";
	}
	try {
		Long.valueOf(stopTime);
	}catch(Exception e){
		stopTime = "0";
	}
	Debug.debug("SaveClientRespoinseTime.jsp : StartTime  --> " + startTime);
	Debug.debug("SaveClientRespoinseTime.jsp : StopTime  --> " + stopTime);
	Debug.debug("SaveClientRespoinseTime.jsp : Long StopTime  --> " + Long.valueOf(stopTime));
	Debug.debug("SaveClientRespoinseTime.jsp : Long StartTime  --> " + Long.valueOf(startTime));
	Debug.debug("SaveClientRespoinseTime.jsp : Long Elapsed  --> " +(Long.valueOf(stopTime) - Long.valueOf(startTime))/1000.00);
	String elapsedTime = String.valueOf(((Long.valueOf(stopTime) - Long.valueOf(startTime))/1000.00));
	String actionType=request.getParameter("acionType");
	actionType = actionType == null ? "": actionType;
	String action = request.getParameter("action");
	action = action == null ? "": action; 
	String contextName = request.getParameter("contextName");
	contextName = StringFunction.isBlank(contextName) || "null".equals(contextName)? "" : contextName;
	Debug.debug("SaveClientRespoinseTime.jsp : Context Name ---> " + contextName); 
	String clientBank =userSession.getClientBankCode();
	String creationTimeStamp = request.getParameter("creationTimeStamp");
	creationTimeStamp = creationTimeStamp == null ? "" : creationTimeStamp;
	String activeUserSessionOid = userSession.getActiveUserSessionOid();
	System.out.println(userSession.getClientBankCode());
	String gridName = request.getParameter("gridName");
	gridName = StringFunction.isBlank(gridName)  || "null".equals(gridName)? "" : gridName;
	String userId = userSession.getUserId();
	String userOid = userSession.getUserOid();
	String serverName = userSession.getServerName();
	String systemName = "PORTAL";
	Debug.debug("SaveClientRespoinseTime.jsp : Navigation ID ---> " + request.getParameter("navId"));
	String navId = request.getParameter("navId");
	navId = StringFunction.isBlank(navId)  || "null".equals(navId)? "" : navId;
	String timeType=request.getParameter("timeType");
	timeType = timeType == null ? "": timeType;
	Debug.debug("SaveClientRespoinseTime.jsp : Time Type ---> "+ timeType);
	if (timeType.length() > 1) timeType = timeType.substring(0,1);
	String prevPage=request.getParameter("prevPage");
	prevPage = prevPage == null ? "": prevPage;
	String transOid=request.getParameter("transOid");
	transOid = transOid == null ? "": transOid;
	String toPage = contextName;
	if (contextName.indexOf(".") != -1){
		String [] tempArr = contextName.split("\\.");
		toPage = tempArr[0];
	}
	StringBuilder sqlBuf = null;
	StringBuffer desiBank = null;
	String jsonFmt = "";
// 	if (StringFunction.isNotBlank(gridName) ){
// 		if (StringFunction.isNotBlank(contextName))
// 			contextName= contextName + "." + gridName;
// 		else 
// 			contextName = gridName;
// 	}

	
		PerformanceStatistic perfStat = new PerformanceStatistic ();
		perfStat.setAction(action);
		perfStat.setContext(contextName);
		perfStat.setaObjectOid(transOid);
		perfStat.setFromPage(prevPage);
		perfStat.setToPage(toPage);
		perfStat.setTimeType(timeType);
		perfStat.setUserOid(userOid);
		perfStat.setOrganizationCode(clientBank);
		perfStat.setNavId(navId);
		perfStat.setServerName(serverName);
		perfStat.setTimerStart(new Date (Long.valueOf(startTime).longValue()));
		perfStat.setTimerStop(new Date (Long.valueOf(stopTime).longValue()));
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH24:mm:SS");
        perfStat.setTimerStopGMTTimeStamp(new Date(creationTimeStamp));
        perfStat.setActionType(actionType);

        if (PerformanceTimerCache.collectTimerData() && !("0".equals(startTime) || "0".equals(stopTime))){
			PerformanceTimerCache.add(perfStat);
		}
		


%>

