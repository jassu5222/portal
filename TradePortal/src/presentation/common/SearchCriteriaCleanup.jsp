<%--for the ajax include--%>

<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*,com.ams.tradeportal.common.cache.*"%>
				 
<jsp:useBean  id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%

String  searchCriteriaSec = request.getParameter("searchCriteriaSection");
if (searchCriteriaSec!=null && !searchCriteriaSec.equals(userSession.getSavedSearchCriteriaSection())) {
    userSession.clearSavedSearchQueryData(); //clear the criteria hash map
    userSession.setSavedSearchCriteriaSection(searchCriteriaSec);
}

%>
