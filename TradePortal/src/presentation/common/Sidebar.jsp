<%--
*******************************************************************************
  Sidebar

  Description:
     The sidebar content.  The Save, Save&Close, Route, Verify,
  Edit, Authorise, and Close buttons may display.  Most buttons display
  conditionally, although Close button is always available.

  Parameters:
     None.  The assumption is that 'instrument' and 'transaction' are webbeans
  available to this JSP
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, 
com.ams.tradeportal.html.*,com.ams.tradeportal.common.cache.*,java.util.Vector"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
	boolean savedSectionValue =userSession.getSavedSectionValue();

%>
<%--cquinton 10/10/2012 ir#6053 move sectionLinks here rather than in template so we can put
    titlepane with internationalized title--%>
<div data-dojo-type="dijit.TitlePane" class="sectionLinksSection" id="sectionLinksSection" open ='<%=savedSectionValue%>' title='<%=resMgr.getText("Sidebar.SectionShortcuts",TradePortalConstants.TEXT_BUNDLE)%>'>
  <script type="dojo/connect" data-dojo-event="toggle" data-dojo-args="evt">callAjaxToUpdateSection();return false;</script>
  <ol id="navContainer" class="sectionLinks"></ol>
</div>

<%--cquinton 9/13/2012 Rel portal refresh start
    add titlepane to sidebar actions--%>
<%
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();	
  TransactionWebBean transaction = (TransactionWebBean) beanMgr.getBean("Transaction");
  String transactionType = transaction.getAttribute("transaction_type_code");
  //out.print("transactionType: "+transactionType);
  InstrumentWebBean instrument = (InstrumentWebBean) beanMgr.getBean("Instrument");
  String instrumentType = instrument.getAttribute("instrument_type_code");
  //out.print("instrumentType: "+instrumentType);
  
  
  OperationalBankOrganizationWebBean opBankOrg = beanMgr.createBean(OperationalBankOrganizationWebBean.class, "OperationalBankOrganization");
  opBankOrg.getById(instrument.getAttribute("op_bank_org_oid"));
 
  
  BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class, "BankOrganizationGroup");
  bankOrganizationGroup.getById(opBankOrg.getAttribute("bank_org_group_oid"));
  String selectedPDFType = "";


  
  
  boolean isExpanded = false;
  if(InstrumentType.GUARANTEE.equals(instrumentType)){
	  selectedPDFType =  bankOrganizationGroup.getAttribute("gua_pdf_type");
  }else if(InstrumentType.STANDBY_LC.equals(instrumentType)){
	  selectedPDFType = bankOrganizationGroup.getAttribute("slc_pdf_type");
  }else if(InstrumentType.IMPORT_DLC.equals(instrumentType)){
	  selectedPDFType = bankOrganizationGroup.getAttribute("imp_dlc_pdf_type");
  }
  
  if(TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(selectedPDFType))
	  isExpanded = true; 
  
  
  
  //cquinton 1/18/2013 remove old close action behavior
  String newTransaction = null;
  
  
  if(request.getParameter("isNewTransaction")!=null){
	  newTransaction=request.getParameter("isNewTransaction");
  }
  
  
  boolean showLinks = false;
  boolean showTips = true;
  boolean showTop = false;
  WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
  
  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("true"))
      showLinks = true;
        
  if(request.getParameter("showTips")!=null)
    if(request.getParameter("showTips").equals("false"))
      showTips = false;
  
  if(request.getParameter("showTop")!=null)
	    if(request.getParameter("showTop").equals("true"))
	      showTop = true;
  
//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - Begin
  boolean pmtBenPanelAuthInd = false;
  if(request.getParameter("pmtBenPanelAuthInd")!=null)
	    if(request.getParameter("pmtBenPanelAuthInd").equals("true"))
	    	pmtBenPanelAuthInd = true;
//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - End  
%>
<% 

HttpSession theSession    = request.getSession(false);
if (!TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(newTransaction)) {
	theSession.setAttribute("isFromTransDetailPage", TradePortalConstants.INDICATOR_YES);
}
%>
<%= widgetFactory.createSidebarQuickLinks(showLinks, showTips, showLinks || showTop, userSession) %>
<%--PR IR-T36000006128 Rakesh Begin--%>

<%--cquinton 9/13/2012 Rel portal refresh end--%>


	<%--the route messages dialog--%>

	<div id="routeDialog" style="display:none;"></div>
		<div class="saveTime">
		<%
			String button ="";
			String saveTime = "";
			String errors = request.getParameter("error");
			button = request.getParameter("buttonPressed");

			if(errors !=null && button !=null){
				if(button.equals("Verify") || button.equals("SaveTrans")){
					if(errors.contains("I002") || errors.contains("I001")) {
						saveTime = TPDateTimeUtility.getCurrentTimeForRequestTimeZone(userSession.getTimeZone());

		%>
						<%=resMgr.getText("Sidebar.SavedTime",TradePortalConstants.TEXT_BUNDLE)%> <%=saveTime.toLowerCase()%>
		<%
					}
				}
			}
		%>
		</div>

		<ul class="sidebarButtons">
		<%

			String formName = (String) request.getParameter("formName");
			//InstrumentWebBean  instrument  = (InstrumentWebBean) beanMgr.getBean("Instrument");
			TemplateWebBean    template    = (TemplateWebBean) beanMgr.getBean("Template");

			// This array of string is used when creating the LC Issue Application Form links.
		    String linkArgs[] = {"","","",""};
			String linkArg0 = "";
			String labelApplicationForm = "";

			boolean showSave = false;
			boolean showSaveClose = false;
			boolean showRoute = false;
			boolean saveBeforeRoute = false;
			boolean showVerify = false;
			boolean showVerifyFX = false; //VShah - CR564
			boolean showEdit = false;
			boolean showAuthorize = false;
			boolean showProxyAuthorize = false; //AAlubala CR-711 Rel 8.0 10/14/2011
			boolean showReCertificate = false; //IAZ 01/03/09
			boolean showAuthorizeDelete = false;
			boolean showUploadPayment = false;
			boolean canCreateMod = false;
			boolean canRoute = false;
			boolean canAuthorize = false;
			boolean canProxyAuthorize = false; //AAlubala CR-711 Rel 8.0 10/14/2011
			boolean isEditable = false;
			boolean isReadOnly = false;
			boolean canShowUploadPayment = false;
			boolean showUploadDirectDebit = false;
			boolean canShowUploadDirectDebit = false;
			boolean showCopyInstrument = false;
			boolean isTemplate = false;
			boolean showDelete = false;
			boolean showViewTerms = false;
			boolean showApplnForm = true;
			boolean showBillOfExchangeForm = true;
			boolean showRequestMarketRate= false;
			boolean isVascoOTP = false;
			
			//SSikhakolli Rel-8.3 CR-821 Dev 05/28/2013 - Start
			boolean showSendForAuth= false;
			boolean showSendForRepair  = false;
			boolean canSendForAuth = false;
			boolean canSendForRepair  = false;
			//SSikhakolli Rel-8.3 CR-821 Dev 05/28/2013 - Start
			boolean showPrintSettleInstrs = false;
			
			//SSikhakolli - Rel-8.3 NBS UAT IR#T36000022268 on 10/29/2013 - Begin
			//Adding display condition for 'Collection Schedule and Bill of Exchange' buttons
			boolean isPortalOriginatedTran = StringFunction.isNotBlank(transaction.getAttribute("c_CustomerEnteredTerms"));
			//SSikhakolli - Rel-8.3 NBS UAT IR#T36000022268 on 10/29/2013 - End
			
			StringBuffer      sqlQuery   = new StringBuffer(); //Vshah - IR#DWUM043037276

			StringBuffer extraTags;
			StringBuffer downloadLink = new StringBuffer();
			String userRights = userSession.getSecurityRights();
			String loginLocale = userSession.getUserLocale();

			String encryptVal1;
			String encryptVal2;
			String encryptVal3;
			String rowKey;
			String urlParm;
			String instrumentOidUrlParam = "";
			String transactionOid;
			String transactionStatus;
			String instrumentIdRouteDisplay;
			String instrumentTypeRouteDisplay;
			String transactionStatusRouteDisplay;

			instrumentIdRouteDisplay = instrument.getAttribute("complete_instrument_id");
			instrumentTypeRouteDisplay = refData.getDescr(TradePortalConstants.INSTRUMENT_TYPE,
						                 instrument.getAttribute("instrument_type_code"),
						                 loginLocale);
			transactionStatusRouteDisplay = refData.getDescr(TradePortalConstants.TRANSACTION_STATUS,
											transaction.getAttribute("transaction_status"),
							                 loginLocale);
			transactionStatus = transaction.getAttribute("transaction_status");

			transactionOid = transaction.getAttribute("transaction_oid");
			instrumentOidUrlParam  = instrument.getAttribute("instrument_oid");
			encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey());
			encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes(instrumentOidUrlParam, userSession.getSecretKey());
			encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes("true", userSession.getSecretKey());
			showApplnForm = Boolean.valueOf(request.getParameter("showApplnForm") != null ? request.getParameter("showApplnForm") : "true");
			showBillOfExchangeForm = Boolean.valueOf(request.getParameter("showBillOfExchangeForm") != null  ? request.getParameter("showBillOfExchangeForm") : "true");
			
			
			rowKey = transactionOid+"/"+instrumentOidUrlParam+"/"+transactionType;
		  	urlParm = "&oid=" + encryptVal1 + "&instrumentOid=" + encryptVal2 + "&isFromTransTermsFlag=" + encryptVal3;
		  	if(request.getParameter("showViewTerms") != null){
		  		if(request.getParameter("showViewTerms").equals("true")){
		  			showViewTerms = true;
		  		}
		  	}
		  	
		    String corpOrgOid = "";

		    CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		    if (isTemplate){ 
		  	     corpOrgOid = template.getAttribute("owner_org_oid");
		    } else{
		  	     corpOrgOid = instrument.getAttribute("corp_org_oid"); 
		    }
		    if(StringFunction.isBlank(corpOrgOid)){
		  	    corpOrgOid = userSession.getOwnerOrgOid();
		    }

		    corpOrg.getById(corpOrgOid);		  	
		  	
		  	
		  	// Get the ownership level of the user.  If this user is within customer access or subsidiary access,
			// the true ownership level is store on the savedUserSession.
			String userSecurityType = null;
			boolean adminCustAccess = false;
			if (userSession.hasSavedUserSession()) {
				userSecurityType = userSession.getSavedUserSessionSecurityType();

				if (userSecurityType.equals(TradePortalConstants.ADMIN))
					adminCustAccess = true;

				// Note that panelEnabled and parentCorpOrgOid attributes are stored to the request object by the CashMgmt.jsp
				// that reads the corresponding values from the CorpOrgWebBean it creates.
				else {
					// This is not an admin access so this is subsidiary access
					// Check if subsidiary itself has panel authorization enabled. if so, disable authroize as it is disallowed in
					// the subsidiary access mode.
					if (TradePortalConstants.INDICATOR_YES.equals((String) request.getAttribute("panelEnabled"))) {
						adminCustAccess = true;
					}
				}
			}
			//Rel 9.3.5 - CR 1029 Start
			boolean bankTransUpdateFlag = false;
			
			boolean showBankInProgessMenu = false;						
			boolean showBankAppliedMenu = false;
			boolean showBankApprovedMenu = false;
			boolean showBankRepairMenu = false;
			boolean showBankInstLockedMenu = false;
			
			boolean showBankApplyUpdates = false;
			boolean showBankApprove = false;
			boolean showBankSendForRepair = false;
			boolean showBankEdit = false;
			boolean showBankAttachDeleteDoc = false;
			
			boolean isSameUser = false;
			
			if(null != request.getParameter("bankTransUpdateMenu")){
				if(TradePortalConstants.BANK_IN_PROGRESS.equalsIgnoreCase(request.getParameter("bankTransUpdateMenu"))){
					showBankInProgessMenu = true;
				}else if(TradePortalConstants.BANK_APPLIED.equalsIgnoreCase(request.getParameter("bankTransUpdateMenu"))){
					showBankAppliedMenu = true;
				}else if(TradePortalConstants.BANK_APPROVED.equalsIgnoreCase(request.getParameter("bankTransUpdateMenu"))){
					showBankApprovedMenu = true;
				}else if(TradePortalConstants.BANK_REPAIR.equalsIgnoreCase(request.getParameter("bankTransUpdateMenu"))){
					showBankRepairMenu = true;
				}else if(TradePortalConstants.BANK_INSTRUMENT_LOCKED.equalsIgnoreCase(request.getParameter("bankTransUpdateMenu"))){
					showBankInstLockedMenu = true;
				}
			}
			
			//get the isSameUser attribute, check if its true.  If true, then its same user logged in.
			if(null != request.getParameter("isSameUser")){
				isSameUser = Boolean.valueOf(request.getParameter("isSameUser"));
			}
			
			if(showBankInProgessMenu){
				showSave = true;
				showSaveClose = true;
				showBankApplyUpdates = true;	
				bankTransUpdateFlag = true;
				showBankAttachDeleteDoc = true;
			}else if(showBankAppliedMenu){
				showSave = false;
				showSaveClose = false;
				showBankApplyUpdates = false;	
				//Processor cant view approve/repair button
				if(isSameUser){
					showBankApprove = false;
					showBankSendForRepair = false;
				}else{
					showBankApprove = true;
					showBankSendForRepair = true;
				}
				
				bankTransUpdateFlag = true;
			}else if(showBankApprovedMenu){
				showSave = false;
				showSaveClose = false;
				showBankApplyUpdates = false;				
				showBankApprove = false;
				showBankSendForRepair = false;	
				
				bankTransUpdateFlag = true;
			}else if(showBankRepairMenu){
				showSave = false;
				showSaveClose = false;
				showBankApplyUpdates = false;				
				showBankApprove = false;
				showBankSendForRepair = false;	
				/*User who sends this transaction to repair, is not allowed to edit the same transaction.*/
				if(isSameUser){
					showBankEdit = false;
				}else{
					showBankEdit = true;
				}
				
				bankTransUpdateFlag = true;
			}else if(showBankInstLockedMenu){
				showSave = false;
				showSaveClose = false;
				showBankApplyUpdates = false;				
				showBankApprove = false;
				showBankSendForRepair = false;	
				bankTransUpdateFlag = true;
			}
			//Rel 9.3.5 - CR 1029 End

			//String instrumentType = instrument.getAttribute("instrument_type_code");
			String SlcGuaranteeForm = transaction.getAttribute("standby_using_guarantee_form");

			String bankReleasedTerms = transaction.getAttribute("c_BankReleasedTerms");
			boolean hasBankReleasedTerms = InstrumentServices.isNotBlank(bankReleasedTerms);

			isReadOnly = Boolean.valueOf(request.getParameter("isReadOnly"));
			isTemplate = Boolean.valueOf(request.getParameter("isTemplate"));

	
			Debug.debug("transStatus " + transactionStatus);
			Debug.debug("instType " + instrumentType);

			canCreateMod = SecurityAccess.canCreateModInstrument(userRights, instrumentType, transactionType);
			if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ 
				canCreateMod = SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.CC_GUA_CREATE_MODIFY);
			}
			canRoute = SecurityAccess.canRouteInstrument(userRights, instrumentType, transactionType);
			if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ 
				canRoute = SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.CC_GUA_ROUTE);
			}
			canAuthorize = SecurityAccess.canAuthorizeInstrument(userRights, instrumentType, transactionType);
			if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ 
				canAuthorize = SecurityAccess.hasRights(userSession.getSecurityRights(),SecurityAccess.CC_GUA_CONVERT);
			}
			
			  //AAlubala - CR-711 Rel8.0 - 10/14/2011 - Proxy Authorize check - start
			  canProxyAuthorize = SecurityAccess.canProxyAuthorizeInstrument(userRights, instrumentType, transactionType);
			  //CR-711 - end

			canShowUploadDirectDebit = SecurityAccess.canProcessUploadDirectDebitFile(userRights, instrumentType);

			isEditable = InstrumentServices.isEditableTransStatus(transactionStatus);

			if (isEditable && canCreateMod && !isReadOnly)
				showSave = true;

			if (isEditable && canCreateMod && !isReadOnly)
				showSaveClose = true;

			if (isEditable && canRoute) {
				showRoute = true;
				saveBeforeRoute = true;
			}

			/* KMehta IR-T36000041634 Rel 9500 on 19-Jan-2016 Change  - Begin*/
			if (!isEditable && canRoute && (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
				|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)				
				|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN) //IAZ 01/03/09
				|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED))) {

				showRoute = true;
			}
			/* KMehta IR-T36000041634 Rel 9500 on 19-Jan-2016 Change  - End*/

			if (isEditable && canCreateMod && !isReadOnly) {
				showVerify = true;
			}
			if (canCreateMod) {
				showCopyInstrument = true;
			}

			if (isEditable && canCreateMod && !isReadOnly) {
				if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_VERIFIED_PENDING_FX))
					showVerifyFX = true;
			}

			if (!isEditable && canCreateMod	&& (userSession.getOwnerOrgOid().equals(instrument.getAttribute("corp_org_oid")))) { //IAZ IR-LKUK052747265 Add
				if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FVD_AUTHORIZED) //VS CR 609 11/23/10
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN) //IAZ 01/03/09
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REPAIR)// NAR Rel8.3 CR 821 05/29/2013
					|| transactionStatus.equals(TradePortalConstants.STATUS_READY_TO_CHECK))//Rel 8.3 IR T36000020368

					showEdit = true;
			}

			// Do not display Authorize button if this is an admin user in customer access
			if (!isEditable && canAuthorize && !adminCustAccess) {
				if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED)
					|| transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE))
					showAuthorize = true;

				//cr498 change certNewLink to certAuthURL to support multiple button actions
				String certAuthURL = (String) request.getParameter("certAuthURL");
				if (InstrumentServices.isNotBlank(certAuthURL))
					showReCertificate = true;

				//When the user has the Authorization rights as well as the rights to delete
				//Discrepancy/ATP Approval Notices from the Mail Message Inbox and the associated Mail message notice is
				//not yet deleted then display the "Authorize&Delete" Button
				if ( ((transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE) || transactionType.equals(TransactionType.DISCREPANCY))
						&& (SecurityAccess.hasRights(userRights, SecurityAccess.DELETE_DISCREPANCY_MSG))) || 
						(TransactionType.SIR.equals(transactionType) && SecurityAccess.hasRights(userRights, SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION)) ) {
					//jgadela R92 - SQL INJECTION FIX
					String msgStatusQuery = "select message_status from mail_message where response_transaction_oid = ? ";
					String messageStatus = "";
					Vector veFrags = null;
					DocumentHandler resultsDocXML = DatabaseQueryBean.getXmlResultSet(msgStatusQuery.toString(), false, new Object[]{transaction.getAttribute("transaction_oid")});
					//Fetch the existing records
					if (resultsDocXML != null)
						veFrags = resultsDocXML.getFragments("/ResultSetRecord");
					if (veFrags != null) {
						messageStatus = resultsDocXML.getAttribute("/ResultSetRecord(0)/MESSAGE_STATUS");
					}
					//If the user can authorize as well the associated Mail message to this transaction is not deleted
					//then display the Authorize&Delete Button
					if ((showAuthorize && !messageStatus.equals(TradePortalConstants.REC_DELETED))) {
						showAuthorizeDelete = true;
					}
				}
			}
			
			//Sandeep Rel-8.3 CR-821 Dev 06/05/2013 - Start
			//TODO: need to implement conditions for "canSendForAuth && canSendForRepair" based on user security 
			 // jgadela - CR-821 IR T36000019687. - Added parameter - transactionType
			canSendForAuth = SecurityAccess.hasCheckerAccess(userRights, instrumentType, transactionType);
			canSendForRepair = SecurityAccess.hasRepairAccess(userRights, instrumentType, transactionType);
			//visibility condition for Send for Authorization button
			if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_CHECK) && 
					!isEditable && canSendForAuth){
			   showSendForAuth = true;
			   showSendForRepair = true;
			}else			
			//visibility condition for Send for Repair button
			if (!isEditable && canSendForRepair &&
			   (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE) ||
			    transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED) ||
			    transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED))) {
			   showSendForRepair = true;
			}
			//Sandeep Rel-8.3 CR-821 Dev 06/05/2013 - End
			
			//AAlubala - IR#DNUM062245488 -  This will check if a user is  set to use OTP Vasco type
			  QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
			  queryListView.setSQL("select auth2fa_subtype from users where user_oid = ?", new Object[]{userSession.getUserOid()});
			  queryListView.getRecords();

			  DocumentHandler result = queryListView.getXmlResultSet();
			  //String tokenType = result.getAttribute("/ResultSetRecord(0)/TOKEN_TYPE");
			  String authSubtype = result.getAttribute("/ResultSetRecord(0)/AUTH2FA_SUBTYPE");
			  if (TradePortalConstants.AUTH_2FA_SUBTYPE_OTP.equals(authSubtype)) isVascoOTP = true;

			  //  //AAlubala - IR#DNUM062245488 -  This will check if a user is  set to use OTP Vasco type


			  //AAlubala - CR-711 Rel8.0 - The proxy authorize - 10/14/2011 - START --
			  // Do not display Proxy Authorize button if this is an admin user in customer access

			 ////AAlubala - IR#DNUM062245488 -  This will check if a user is NOT set to use OTP Vasco type
			  if (!isEditable && canProxyAuthorize && !adminCustAccess && !isVascoOTP) {
			    if (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE)
			     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED)
			     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_REQ_AUTHENTICTN)
				 || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED)
			     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED) //Vshah - IR#MMUM061784253 - Rel8.0
			     || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE) ) //Vshah - IR#MMUM061784253 - Rel8.0
				 {
				 	//Vshah - IR#MMUM060640963 - Rel8.0 - 06/06/2012 - <BEGIN>
				 	//Offline authorise button should appear only for transactions
				 	//that are flagged for token authorization at the Bank ASP level.
				 	Cache reCertCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
					DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());
					String requireTranAuth = CBCResult.getAttribute("/ResultSetRecord(0)/REQUIRE_TRAN_AUTH");
					boolean requireAuth = InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, instrumentType+"_"+transactionType);
				 	if(requireAuth)
				 	//Vshah - IR#MMUM060640963 - Rel8.0 - 06/06/2012 - <END>
				 	   showProxyAuthorize = true;
			      }

			    String certAuthURL = (String)request.getParameter("certAuthURL");
			    if (InstrumentServices.isNotBlank(certAuthURL))
			       showReCertificate = true;
			  }
			  //CR-711 - END

			if (isEditable && canShowUploadPayment) {
				showUploadPayment = true;
			}

			if (isEditable && canShowUploadDirectDebit) {
				showUploadDirectDebit = true;
			}
			if((!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_STARTED)) &&
		    (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_DELETED)) &&
		    (!transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK))){
				System.out.println("Inside Sidebar.jsp For instrumentType.."+instrumentType+" and transaction type:"+transaction.getAttribute("transaction_type_code")+"  showPDF  flag is ["+ showApplnForm+"]");
		    	if(instrumentType.equals(InstrumentType.IMPORT_DLC)){	
		    		
		    		if(!isExpanded){
						linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_DLC_AMEND_APPLICATION : TradePortalConstants.PDF_DLC_ISSUE_APPLICATION;
						linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("ImportLCAmend.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("ImportLCIssue.title",TradePortalConstants.TEXT_BUNDLE,"en_US");  		
		    		}else{
						linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_DLC_AMEND_APPLICATION : TradePortalConstants.PDF_DLC_ISSUE_EXPANDED_APPLICATION;
						linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("ImportLCAmendx.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("ImportLCIssuex.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    		}				
						
						if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
							labelApplicationForm = "ImportDLCIssue.IssueApplicationFormMulti"; 
						}else{
							labelApplicationForm = "ImportDLCIssue.IssueApplicationFormButton"; 
						}				
					showBillOfExchangeForm = false;
		    	}else if(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)){		    		
		    			linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_ATP_AMEND_APPLICATION : TradePortalConstants.PDF_ATP_ISSUE_APPLICATION;					
						if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
								labelApplicationForm = "ApprovalToPayIssue.IssueApplicationFormMulti";
							}else{
								labelApplicationForm = "ApprovalToPayIssue.IssueApplicationFormButton";
							}
					showBillOfExchangeForm = false;
		    	}else if(instrumentType.equals(InstrumentType.REQUEST_ADVISE)){    
						linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_RQA_AMEND_APPLICATION : TradePortalConstants.PDF_RQA_ISSUE_APPLICATION;					
						if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
							labelApplicationForm = "RequestAdviseIssue.IssueApplicationFormMulti";
						}else{
							labelApplicationForm = "RequestAdviseIssue.IssueApplicationFormButton";
						} 		
					showBillOfExchangeForm = false;
		    	}else if(instrumentType.equals(InstrumentType.STANDBY_LC)){
		    		if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_YES)){
		    			if(!isExpanded){
							//linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_SLC_AMEND_APPLICATION : TradePortalConstants.PDF_SLC_ISSUE_APPLICATION_D;
							linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingSLCA.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingSLCD.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    			}else{ 
		    				//linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_SLC_AMEND_APPLICATION : TradePortalConstants.PDF_SLC_ISSUE_EXPANDED_APPLICATION_D;	
		    				linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingSLCAx.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingSLCDx.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    			}
		    				labelApplicationForm = "SLCIssue.IssueApplicationFormMulti";	    				
						showBillOfExchangeForm = false;
	    			}else{	
	    				if(!isExpanded){
							linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingSLCA.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingSLCS.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
	    				}else{
							//linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_SLC_AMEND_APPLICATION : TradePortalConstants.PDF_SLC_ISSUE_EXPANDED_APPLICATION_S;
							linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingSLCAx.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingSLCSx.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
	    				}
							labelApplicationForm = "SLCIssue.IssueApplicationFormMulti";
						showBillOfExchangeForm = false;
	    			}
		    		// jgadela 06/18.2014 IR T36000026067 - Corrected the if condition
		    	}else if(instrumentType.equals(InstrumentType.GUARANTEE) || TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("standby_using_guarantee_form")) ){
		    			if(transaction.getAttribute("standby_using_guarantee_form").equals(TradePortalConstants.INDICATOR_YES)){	
		    				if(!isExpanded){		
		    					if(TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))){
		    						//linkArg0 = TradePortalConstants.PDF_SLC_AMEND_APPLICATION;//resMgr.getText("OutgoingSLCA.title",TradePortalConstants.TEXT_BUNDLE) = Outgoing Standby LC Amend
		    						linkArg0 = resMgr.getText("OutgoingGuaranteeA.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    					}else{
		    						//linkArg0 = TradePortalConstants.PDF_SLC_ISSUE_APPLICATION_D;//resMgr.getText("OutgoingSLCD.title",TradePortalConstants.TEXT_BUNDLE) = Outgoing Standby LC Issue D
		    						linkArg0 = resMgr.getText("OutgoingGuaranteeS.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    					}
		    					
		    				}else{		    					
		    					//linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_SLC_AMEND_APPLICATION : TradePortalConstants.PDF_SLC_ISSUE_EXPANDED_APPLICATION_D;
		    					linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingGuaranteeAx.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingGuaranteeSx.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    				}
		    					labelApplicationForm = "SLCIssue.IssueApplicationFormMulti";
							showBillOfExchangeForm = false;
		    			}else{		
		    				//System.out.println("transaction- standby_using_guarantee_form="+transaction.getAttribute("standby_using_guarantee_form"));
		    				if(!isExpanded){	
		    					linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? resMgr.getText("OutgoingGuaranteeA.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingGuaranteeS.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    				}else{
								//linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))? TradePortalConstants.PDF_SLC_AMEND_APPLICATION : TradePortalConstants.PDF_SLC_ISSUE_EXPANDED_APPLICATION_D;
								
								linkArg0 = TransactionType.AMEND.equals(transaction.getAttribute("transaction_type_code"))?  resMgr.getText("OutgoingGuaranteeAx.title",TradePortalConstants.TEXT_BUNDLE,"en_US") : resMgr.getText("OutgoingGuaranteeSx.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
		    				}
							labelApplicationForm = "GuaranteeIssue.IssueApplicationFormMulti";
							showBillOfExchangeForm = false;
		    			}
		    	}else if(instrumentType.equals(InstrumentType.NEW_EXPORT_COL)){   		
					linkArg0 = TradePortalConstants.PDF_NEW_EXPORT_COLLECTION;
					labelApplicationForm = "ExportCollectionIssue.ExportCollection"; 
					showBillOfExchangeForm = showBillOfExchangeForm && true;
				}else if(instrumentType.equals(InstrumentType.EXPORT_COL)){								
					showBillOfExchangeForm = showBillOfExchangeForm && true;
					if (showApplnForm || (!showApplnForm && transaction.getAttribute("transaction_status").equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK))) { 					  
						linkArg0 = TradePortalConstants.PDF_EXPORT_COLLECTION;
					  	labelApplicationForm = "ExportCollectionIssue.ExportCollection";					  
					   	showBillOfExchangeForm = true;
					  	 showApplnForm = true;
					   //Srinivasu_D IR#T36000015900 Rel8.2 06/09/2013 - Added below two buttons
					    if(transaction.getAttribute("transaction_type_code").equals(TransactionType.PAYMENT)){
						  showBillOfExchangeForm = false;
						  showApplnForm = false;
						}
					 }
				}else if(InstrumentType.AIR_WAYBILL.equals(instrumentType)){
						//linkArg0 = TradePortalConstants.PDF_AIRWAY_BILL_REL_APPLICATION;
						linkArg0 = resMgr.getText("AirwaybillRelease.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
						labelApplicationForm = "AirwaybillRelease.ReleaseApplicationFormButton"; 		
						showBillOfExchangeForm = false;
		    	}
				else if(InstrumentType.SHIP_GUAR.equals(instrumentType)){									
						//linkArg0 = TradePortalConstants.PDF_SHIPPING_GUAR_ISS_APPLICATION;
						linkArg0 = resMgr.getText("ShippingGuarnteeIssue.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
						labelApplicationForm = "ShiipingGuarnteeIssue.IssueApplicationFormButton"; 			
						showBillOfExchangeForm = false;
		    	}else if(instrumentType.equals(InstrumentType.LOAN_RQST )){
		    		//only for finance inv
		    		  String importIndicator = instrument.getAttribute("import_indicator");
		    		  boolean isReceivables = TradePortalConstants.INDICATOR_X.equals(importIndicator);
		    		  if(isReceivables){
							//linkArg0 = TradePortalConstants.PDF_LRQ_INVFINANCE_ISS_APPLICATION;
							linkArg0 =resMgr.getText("InvoiceFinanceLRQIssue.title",TradePortalConstants.TEXT_BUNDLE,"en_US");
							labelApplicationForm = "InvoiceFinanceLRQIssue.IssueApplicationFormButton"; 		
							showBillOfExchangeForm = false;		    			  
		    		  }else {
							showApplnForm = false;
							showBillOfExchangeForm = false;
					  }

		    	}else {
					showApplnForm = false;
					showBillOfExchangeForm = false;
				}
			} else {
				showApplnForm = false;
				showBillOfExchangeForm = false;
			}
			
			long instrumentOid = Long.valueOf(instrument.getAttribute("instrument_oid")).longValue();
			long userOid = Long.valueOf(userSession.getUserOid()).longValue();

			// If it isn't already locked by the user, don't allow them to do anything.
			// (We only lock the instrument if it is editable.)
			if (isEditable && !LockingManager.isLocked(instrumentOid, userOid, false)) {
				Debug.debug("User doesn't have a lock so can't do anything");
				showSave = false;
				showSaveClose = false;
				showRoute = false;
				showVerify = false;
				showEdit = false;
				showAuthorize = false;
				showUploadPayment = false;
				showUploadDirectDebit = false;
			}

			// TLE - 06/27/07 - IR-NNUH062163493 - Add Begin
			// Added code to check if we need to display the 'Delete a Document' button

			boolean showDeleteAttachDocButton = false;
			//jgadela R92 - SQL INJECTION FIX
			String query  = "select doc_image_oid, image_id, doc_name from document_image where p_transaction_oid = ? and form_type = ?  and mail_message_oid is null ";





			Debug.debug("Query is : " + query);
			DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false,transaction.getAttribute("transaction_oid"), TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

			if (dbQuerydoc != null) {
				if (dbQuerydoc.getFragmentsList("/ResultSetRecord/").size() > 0) {
					showDeleteAttachDocButton = true;
				}
			}
		%>

		<%
		// DOWNLOAD SAVED DATA Button
			boolean showDownLoad = true;
			downloadLink = new StringBuffer();
			downloadLink.append(request.getContextPath());
                         // W Zhu 10/25/12 Rel 8.1 KJUM101652104 T36000006922 remove /.jsp
			downloadLink.append("/common/TradePortalDownloadServlet?");
			downloadLink.append(TradePortalConstants.DOWNLOAD_REQUEST_TYPE);
			downloadLink.append("=");
			boolean isDownloadLink = false;
			if ((instrumentType.equals(InstrumentType.IMPORT_DLC)) && (transactionType.equals(TransactionType.ISSUE))) {
				downloadLink.append(TradePortalConstants.DOWNLOAD_IMPORT_LC_ISSUE_DATA);
				isDownloadLink=true;
			}else if ((instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) && (transactionType.equals(TransactionType.ISSUE))) {
				downloadLink.append(TradePortalConstants.DOWNLOAD_ATP_ISSUE_DATA);
				isDownloadLink=true;
			}else if ((instrumentType.equals(InstrumentType.REQUEST_ADVISE)) && (transactionType.equals(TransactionType.ISSUE))) {
				downloadLink.append(TradePortalConstants.DOWNLOAD_IMPORT_LC_ISSUE_DATA);
				isDownloadLink=true;
			}else if ((instrumentType.equals(InstrumentType.STANDBY_LC)) && (transactionType.equals(TransactionType.ISSUE)) && (!SlcGuaranteeForm.equals(TradePortalConstants.INDICATOR_YES))) {
				downloadLink.append(TradePortalConstants.DOWNLOAD_OUTGOING_STANDBY);
				isDownloadLink=true;
			}else if ((instrumentType.equals(InstrumentType.GUARANTEE)) && transactionType.equals(TransactionType.ISSUE)	|| (instrumentType.equals(InstrumentType.STANDBY_LC)
							&& transactionType.equals(TransactionType.ISSUE) && SlcGuaranteeForm.equals(TradePortalConstants.INDICATOR_YES))) {
				downloadLink.append(TradePortalConstants.DOWNLOAD_GUARANTEE_ISSUE_DATA);
				isDownloadLink=true;
			}else{
				//showDownLoad = false;
				isDownloadLink=false;
			}
			//
			//AiA IR#T36000018027 - 06/17/2013
			//AiA IR#T36000018549 - 06/24/2013 - Apply to ATP as well
            //For Import L/C or ATP Transactions that are processed by bank, on the transaction summary page, 
            //there should be View Terms as Entered button, clicking on which should 
            //then display the Download Saved Data button
			//if((instrumentType.equals(InstrumentType.IMPORT_DLC))||(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY))){
				//set it to false by default and only show if the conditions below are met
				showDownLoad = false;
				//For Import L/C or ATP instruments that are started, ready to authorize & authorized in the portal, 
				//The Download Saved Data should appear
				//
				//TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE
				//TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK
				//TradePortalConstants.TRANS_STATUS_STARTED
				//TradePortalConstants.TRANS_STATUS_AUTHORIZED
				//
				//When the Terms link is clicked, a "isFromTransTermsFlag" param is set
				//so, we use this to determine if the link is clicked and add it as a condition for displaying the
				//saved data button as per the requirement
				boolean viewTermsEnteredClicked = false;
				  if( request.getParameter("isFromTransTermsFlag") != null ){

						String transTermsFlag = EncryptDecrypt.decryptStringUsingTripleDes( request.getParameter("isFromTransTermsFlag"), userSession.getSecretKey() );

					        if ((StringFunction.isNotBlank(transTermsFlag)) && (transTermsFlag.equals("true"))){
					        	viewTermsEnteredClicked = true;
					        }
				  
					  }				
				//IR-T36000031921-adding partially authorized status,ready to check status and removing the started status condition.		
				//IR-T36000031567- viewTermsEnteredClicked only for processed by bank status
				if(((transactionStatus.equals(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE) ||
				   transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED) ||
				     transactionStatus.equals(TradePortalConstants.STATUS_READY_TO_CHECK) || 
				     transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK) ||
					  transactionStatus.equals(TradePortalConstants.TRANS_STATUS_AUTHORIZED)) && isDownloadLink))
				{
					showDownLoad = true;
				}
				//IR-T36000031921-adding started status condition.
				//IR T36000018690 -Do not display the download button for IMP_LC AMEND activity
				if (transactionType.equalsIgnoreCase(TransactionType.DISCREPANCY) || transactionStatus.equals(TradePortalConstants.TRANS_STATUS_STARTED) || transactionType.equalsIgnoreCase(TransactionType.AMEND)){					
					showDownLoad = false;
				}
				//IR#T36000018738 
				//ATP Issue creation page, do not show download button
				//
				if((instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) && transactionType.equals(TransactionType.ISSUE)){
					if(transactionStatus.equals(TradePortalConstants.TRANS_STATUS_STARTED))
						showDownLoad = false;
				}

		//}
			//AiA
			//IR T36000018529 06/26/2013 - Do not display the download button for discrepancy
			//set to false
			/* Moved as part of IR T36000018691 below
			if (transactionType.equalsIgnoreCase(TransactionType.DISCREPANCY)){					
				showDownLoad = false;
			}	*/
			//END IR T36000018529 
			//
			//END - IR#T36000018027
			
			if (isTemplate && !isReadOnly){
				showSave = true;
				showSaveClose = true;
				showVerify = false;
				showRoute = false;
				showDownLoad = false;
				showCopyInstrument = false;
				showDelete = true;
			}
			/*Added below conditions, so that Copy Instrument,LC Application buttons are not shown for the Transaction- Discrepancy Response
			  and Copy Instrument,ATP Application buttons are not shown for the Transaction- Approval To Pay Response	*/
			//IR T36000018691 -Do not display the download button for Discrepancy and ATP Response
			//IR T36000043797 -Do not display the download button for SIM and SIR
			if (transactionType.equalsIgnoreCase(TransactionType.DISCREPANCY) 
					|| transactionType.equals(TransactionType.APPROVAL_TO_PAY_RESPONSE)
					|| transactionType.equalsIgnoreCase(TransactionType.SIM)
					|| transactionType.equalsIgnoreCase(TransactionType.SIR)){
				showCopyInstrument=false;
				showApplnForm = false;				
				showDownLoad = false;
			}
			if(transactionType.equalsIgnoreCase(TransactionType.AMEND) || transactionType.equalsIgnoreCase(TransactionType.TRACER) ){
				showCopyInstrument=false;
				
				if(transactionType.equalsIgnoreCase(TransactionType.TRACER)){
					showBillOfExchangeForm = false;
				}
			}
			
			//SSikhakollo - Rel-9.4 CR-818 - Print Function
			if(transactionStatus.equalsIgnoreCase(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE) || 
						transactionStatus.equalsIgnoreCase(TradePortalConstants.TRANS_STATUS_PARTIALLY_AUTHORIZED) || 
						transactionStatus.equalsIgnoreCase(TradePortalConstants.TRANS_STATUS_READY_TO_CHECK) || 
						transactionStatus.equalsIgnoreCase(TradePortalConstants.TRANS_STATUS_AUTHORIZED) || 
						transactionStatus.equalsIgnoreCase(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK)){
				
				if(transactionType.equalsIgnoreCase(TransactionType.DISCREPANCY)
						 && SettlementInstrUtility.isSettleInstrIncludeInDCR(userSession.getOwnerOrgOid())){
					//For Instrument Type RQA (Request to Advise), IMP_DLC(Import LC), EXP_DLC (Export LC), SLC (Outgoing Standby LC), INC_SLC (Incoming Standby LC)
					linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_DISC_RES;	
					showPrintSettleInstrs = true;
				}else if(transactionType.equalsIgnoreCase(TransactionType.SIM)){
					if(instrumentType.equalsIgnoreCase(InstrumentType.DOCUMENTARY_BA)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_BNK_ACC;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.DEFERRED_PAY)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_DIFF_PAY;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.IMPORT_COL)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_INW_COL;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.LOAN_RQST)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_LOAN_REQ;
					}
					showPrintSettleInstrs = true;
				}else if(transactionType.equalsIgnoreCase(TransactionType.SIR)){
					if(instrumentType.equalsIgnoreCase(InstrumentType.DOCUMENTARY_BA)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_BNK_ACC;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.DEFERRED_PAY)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_DIFF_PAY;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.IMPORT_DLC)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_IMP_LC;
					}
					if(instrumentType.equalsIgnoreCase(InstrumentType.LOAN_RQST)){
						linkArg0 = TradePortalConstants.PDF_SETTLE_INSTR_LOAN_REQ;
					}
					showPrintSettleInstrs = true;
				}
			}
			
			//Sandeep PR-IR#T36000011914-BMO#91 02/19/2013 - Start
			//Copy Instrument button should not show when Instrument Type Code is Approval to Pay 'ATP' or Export LC 'EXP_DLC'
			if(instrumentType.equalsIgnoreCase(InstrumentType.EXPORT_DLC) || instrumentType.equalsIgnoreCase(InstrumentType.APPROVAL_TO_PAY))
				showCopyInstrument=false;
			//Sandeep PR-IR#T36000011914-BMO#91 02/19/2013 - End
			
			if (showSave) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraSaveTags"));
		%>
			<button data-dojo-type="dijit.form.Button"  name="SaveButton" id="SaveButton" type="button" data-dojo-props="iconClass:'save'" >
				<%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			 	</script>
			</button>

			  <%=widgetFactory.createHoverHelp("SaveButton","SaveHoverText") %>

		<%
			}

			if (showSaveClose) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraSaveCloseTags"));
		%>
			<button data-dojo-type="dijit.form.Button"  name="SaveCloseButton" id="SaveCloseButton" type="button" data-dojo-props="iconClass:'saveclose'" >
				<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			 	</script>
			</button>
			  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %>
		<%
			}
			if (showDelete) {
			  //cquinton 2/11/2013 change buttons to type button to avoid double action
		%>
			  <button data-dojo-type="dijit.form.Button"  name="DeleteButton" id="DeleteButton" type="button" data-dojo-props="iconClass:'delete'" >
				         <%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
				        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				          var confirmDelete = confirm("<%=StringFunction.escapeQuotesforJS(resMgr.getText("common.confirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE))%>");
            
            				if(confirmDelete==true){                        
                  				setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
                  				document.forms[0].submit();
            				}
							return false; 
				        </script>
				    </button>
				    <%=widgetFactory.createHoverHelp("DeleteButton","DeletHoverText") %>
			<%
			}

			if (showVerifyFX) {
			  extraTags = new StringBuffer();
			  extraTags.append(request.getParameter("extraVerifyTags"));
			  
		%>
			<button data-dojo-type="dijit.form.Button"  name="VerifyFXButton" id="VerifyFXButton" type="button" data-dojo-props="iconClass:'verify'" >
			    <%=resMgr.getText("common.VerifyFXText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_VERIFY_FX%>', '0');
						document.forms[0].submit();
	 			</script>
			</button>
			<%=widgetFactory.createHoverHelp("VerifyFXButton","VerifyFXHoverText") %>
		<%
			}
			
			//Rel 9.3.5 - CR 1029 Start
			if ( (showBankApplyUpdates) && (SecurityAccess.hasRights(userRights,
					SecurityAccess.BANK_TRANS_APPLY_UPDATE)) ) {
				
		%>
			<button data-dojo-type="dijit.form.Button"  name="ApplyUpdatesButton" id="ApplyUpdatesButton" type="button" data-dojo-props="iconClass:'verify'" >
			     <%=resMgr.getText("common.ApplyUpdates",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">

						setButtonPressed('<%=TradePortalConstants.BUTTON_APPLY_UPDATES%>', '0');
						document.forms[0].submit();
						
	 			</script>
			</button>
			<%=widgetFactory.createHoverHelp("ApplyUpdatesButton","VerifyHoverText") %>
		<%
			}
			
			if ( (showBankApprove) && (SecurityAccess.hasRights(userRights,
					SecurityAccess.BANK_TRANS_APPROVE)) ) {
				
				%>
					<button data-dojo-type="dijit.form.Button"  name="ApproveButton" id="ApproveButton" type="button" data-dojo-props="iconClass:'verify'" >
					    <%=resMgr.getText("common.Approve",TradePortalConstants.TEXT_BUNDLE)%>
					    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">

								setButtonPressed('<%=TradePortalConstants.BUTTON_APPROVE%>', '0');
								document.forms[0].submit();
								
			 			</script>
					</button>
					<%=widgetFactory.createHoverHelp("ApproveButton","VerifyHoverText") %>
				<%
					}
				if ( (showBankSendForRepair) && (SecurityAccess.hasRights(userRights,
						SecurityAccess.BANK_TRANS_SEND_FOR_REPAIR)) ) {
				
				%>
					<button data-dojo-type="dijit.form.Button"  name="SendForRepairButton" id="SendForRepairButton" type="button" data-dojo-props="iconClass:'route'" >
					     <%=resMgr.getText("common.SendForRepair",TradePortalConstants.TEXT_BUNDLE)%>
					    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">

								setButtonPressed('<%=TradePortalConstants.BUTTON_BANK_SEND_FOR_REPAIR%>', '0');
								document.forms[0].submit();
								
			 			</script>
					</button>
					<%=widgetFactory.createHoverHelp("SendForRepairButton","VerifyHoverText") %>
				<%
					}
				if (showBankEdit) {
					
					%>
						<button data-dojo-type="dijit.form.Button"  name="BankEditButton" id="BankEditButton" type="button" data-dojo-props="iconClass:'edit'" >
						    <%=resMgr.getText("common.EditDataText",TradePortalConstants.TEXT_BUNDLE)%>
						    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">

									setButtonPressed('<%=TradePortalConstants.BUTTON_BANK_EDIT%>', '0');
									document.forms[0].submit();
									
				 			</script>
						</button>
						<%=widgetFactory.createHoverHelp("BankEditButton","VerifyHoverText") %>
					<%
						}
				if ( (showBankAttachDeleteDoc) && (SecurityAccess.hasRights(userRights, SecurityAccess.BANK_TRANS_ATTACH_DOC))) {
					session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
					String bankTransUpdateSql = "select bank_transaction_update_oid from bank_transaction_update where a_transaction_oid = ? ";
					DocumentHandler resultsBankDocXML = DatabaseQueryBean.getXmlResultSet(bankTransUpdateSql, false, new Object[]{transaction.getAttribute("transaction_oid")});
					String bankTransUpdateOid = null;
					if (resultsBankDocXML != null) {
							bankTransUpdateOid = resultsBankDocXML.getAttribute("/ResultSetRecord(0)/BANK_TRANSACTION_UPDATE_OID");
					}
					session.setAttribute("documentImageFileUploadOid", bankTransUpdateOid);

					extraTags = new StringBuffer();
				  
			%>
				<button data-dojo-type="dijit.form.Button"  name="DocumentImageFileUploadData" id="DocumentImageFileUploadData" type="button" data-dojo-props="iconClass:'attachDocument'" >
				    <%=resMgr.getText("common.AttachADocumentText",TradePortalConstants.TEXT_BUNDLE)%>
				    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							setButtonPressed('<%=TradePortalConstants.BUTTON_BANK_ATTACH_DOCUMENTS%>', '0');
							document.forms[0].submit();
		 			</script>
				</button>
				  <%=widgetFactory.createHoverHelp("DocumentImageFileUploadData","AttachDocumentHoverText") %>
			<%
				}
				
				if ( (showBankAttachDeleteDoc) && (SecurityAccess.hasRights(userRights, SecurityAccess.BANK_TRANS_DELETE_DOC))) {		
					String bankTransIDSql = "select bank_transaction_update_oid from bank_transaction_update where a_transaction_oid = ? ";
					DocumentHandler resultDocXML = DatabaseQueryBean.getXmlResultSet(bankTransIDSql, false, new Object[]{transaction.getAttribute("transaction_oid")});
					String bankTransOid = null;
					if (resultDocXML != null) {
							bankTransOid = resultDocXML.getAttribute("/ResultSetRecord(0)/BANK_TRANSACTION_UPDATE_OID");
					}
					String docQuery  = "select doc_image_oid, image_id, doc_name from document_image where p_transaction_oid = ? and form_type = ?  and mail_message_oid is null ";					
			       
					DocumentHandler imageDoc = DatabaseQueryBean.getXmlResultSet(docQuery.toString(), false, bankTransOid, TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

					if (imageDoc != null) {
						if (imageDoc.getFragmentsList("/ResultSetRecord/").size() > 0) 
							showDeleteAttachDocButton = true;				
					}
				}	
				
				if (showBankAttachDeleteDoc && showDeleteAttachDocButton) {

			%>
					<button data-dojo-type="dijit.form.Button"  name="DeleteADocumentButton" id="DeleteADocumentButton" type="button" data-dojo-props="iconClass:'deleteDocument'" >
				    	<%=resMgr.getText("common.DeleteADocumentText",TradePortalConstants.TEXT_BUNDLE)%>
				    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>', '0');
							if(confirmDocumentDelete()){
								document.forms[0].submit();
							}else{
								return false;
							}
		 				</script>
				  	</button>
				  <%=widgetFactory.createHoverHelp("DeleteADocumentButton","DeleteDocumentHoverText") %>
			<%
					
				}
		//Rel 9.3.5 - CR 1029 End
			if (showVerify) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraVerifyTags"));
				
		%>
			<button data-dojo-type="dijit.form.Button"  name="VerifyButton" id="VerifyButton" type="button" data-dojo-props="iconClass:'verify'" >
			    <%=resMgr.getText("common.VerifyText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
<%--
AiA IR#T36000018156 - Button to submit form when pressed. 06/13/2013
--%>
		<%-- commneted as this causes issue during verify when the instrument has errors. We use SidebarFooter.jsp instead.
						setButtonPressed('<%=TradePortalConstants.BUTTON_VERIFY%>', '0');
						document.forms[0].submit();
						--%>
	 			</script>
			</button>
			<%=widgetFactory.createHoverHelp("VerifyButton","VerifyHoverText") %>
		<%
			}
			if (showRoute) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraRouteTags"));
				String submitValue = saveBeforeRoute ? TradePortalConstants.BUTTON_SAVEROUTETRANS
						: TradePortalConstants.BUTTON_ROUTE;
			  
		%>
			<button data-dojo-type="dijit.form.Button"  name="RouteButton" id="RouteButton" type="button" data-dojo-props="iconClass:'route'" >
			    <%=resMgr.getText("common.RouteText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openRouteDialog('routeItemDialog', routeItemDialogTitle,'',
									'<%=TradePortalConstants.INDICATOR_NO%>','<%=TradePortalConstants.FROM_TRADE%>','<%=rowKey%>',
									'<%=StringFunction.escapeQuotesforJS(instrumentIdRouteDisplay)%>', "<%=StringFunction.escapeQuotesforJS(instrumentTypeRouteDisplay)%>", '<%=StringFunction.escapeQuotesforJS(transactionStatusRouteDisplay)%>') ;
					return false;
	 			</script>
			</button>
			<%=widgetFactory.createHoverHelp("RouteButton","RouteHoverText") %>
		<%
			}
			if ((isEditable) && (!isReadOnly) && (!isTemplate) && (SecurityAccess.hasRights(userRights, SecurityAccess.ATTACH_DOC_TRANSACTION))) {
				session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
				session.setAttribute("documentImageFileUploadOid", transaction.getAttribute("transaction_oid"));

				extraTags = new StringBuffer();
			  
		%>
			<button data-dojo-type="dijit.form.Button"  name="DocumentImageFileUploadData" id="DocumentImageFileUploadData" type="button" data-dojo-props="iconClass:'attachDocument'" >
			    <%=resMgr.getText("common.AttachADocumentText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('AttachDocuments', '0');
						document.forms[0].submit();
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("DocumentImageFileUploadData","AttachDocumentHoverText") %>
		<%
			}
			if(showDownLoad && !bankTransUpdateFlag){
		%>
		<button data-dojo-type="dijit.form.Button"  name="DownLoadSavedData" id="DownLoadSavedData" type="button" data-dojo-props="iconClass:'download'" >
	    	<%=resMgr.getText("common.DownLoadSavedDataText",TradePortalConstants.TEXT_BUNDLE)%>
	    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=downloadLink.toString()%>");
	 		</script>
	  	</button>
	  	<%=widgetFactory.createHoverHelp("DownLoadSavedData","DownloadSavedHoverText") %>
		<%	}
			if(showCopyInstrument && TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){
		%>
		<button data-dojo-type="dijit.form.Button"  name="CopySelected" id="CopySelected" type="button" data-dojo-props="iconClass:'copy'">
			<%=resMgr.getText("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["t360/common"], function(common){
					common.openCopyConvertedSelectedDialogHelper('', '<%=TradePortalConstants.INDICATOR_NO%>', '<%=encryptVal2%>', '<%=instrumentType%>');
				   });
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("CopySelected","CopySelectedHoverText") %>
		<div id="copySelectedInstrument"></div>
		<% }  else if(showCopyInstrument){ %>

		<button data-dojo-type="dijit.form.Button"  name="CopySelected" id="CopySelected" type="button" data-dojo-props="iconClass:'copy'">
			<%=resMgr.getText("CopySelected.Title",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["t360/common"], function(common){
					common.openCopySelectedDialogHelper('', '<%=TradePortalConstants.INDICATOR_NO%>', '<%=encryptVal2%>', '<%=instrumentType%>');
				   });
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("CopySelected","CopySelectedHoverText") %>
		<div id="copySelectedInstrument"></div>
		<%} %>
		<%
			if (showUploadDirectDebit) {   				// This is the link for the Upload Direct Debit Data button
		%>
			<button data-dojo-type="dijit.form.Button"  name="UploadDirectDebitFile" id="UploadDirectDebitFile" type="button" data-dojo-props="iconClass:'upload'" >
			    <%=resMgr.getText("common.UploadDirectDebitFileText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openURL("<%=formMgr.getLinkAsUrl("goToUploadDirectDebitFile", response)%>");
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("UploadDirectDebitFile","AttachDDHoverText") %>
		<%
			}

			if (showUploadPayment) {
				// This is the link for the Upload PO Data button
				StringBuffer newLink = new StringBuffer("");
				newLink.append(formMgr.getLinkAsUrl("goToUploadPaymentFile", response));
		%>
			<button data-dojo-type="dijit.form.Button"  name="UploadPaymentFile" id="UploadPaymentFile" type="button" data-dojo-props="iconClass:'attachDocument'" >
			    <%=resMgr.getText("common.UploadPaymentFileText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openURL("<%=java.net.URLEncoder.encode(newLink.toString())%>");
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("UploadPaymentFile","AttachHoverText") %>
		<%
			}
			 if ((isEditable)
					&& (!isReadOnly)
					&& (SecurityAccess.hasRights(userRights,
							SecurityAccess.DELETE_DOC_TRANSACTION))) {

				 if (showDeleteAttachDocButton) {
				   //cquinton 2/11/2013 change buttons to type button to avoid double action
		%>
				<button data-dojo-type="dijit.form.Button"  name="DeleteADocumentButton" id="DeleteADocumentButton" type="button" data-dojo-props="iconClass:'deleteDocument'" >
			    	<%=resMgr.getText("common.DeleteADocumentText",TradePortalConstants.TEXT_BUNDLE)%>
			    	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>', '0');
						if(confirmDocumentDelete()){
							document.forms[0].submit();
						}else{
							return false;
						}
	 				</script>
			  	</button>
			  <%=widgetFactory.createHoverHelp("DeleteADocumentButton","DeleteDocumentHoverText") %>
		<%
				}
			}

			if (showEdit) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraEditTags"));
				
		%>
			<button data-dojo-type="dijit.form.Button"  name="EditButton" id="EditButton" type="button" data-dojo-props="iconClass:'edit'" >
			    <%=resMgr.getText("common.EditDataText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_EDIT%>', '0');
						document.forms[0].submit();
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("EditButton","EditHoverText") %>
		<%
			}
			
			//MEerupula Rel8.3 IR-T36000019060 -- Moved Send For Repair button as per IR
			  if(showSendForRepair){
		%>
		        <div id="SendForRepairDialog"></div>
				<button data-dojo-type="dijit.form.Button"  name="SendForRepair" id="SendForRepair" type="button" data-dojo-props="iconClass:'route'" >
				    <%=resMgr.getText("common.SendForRepair",TradePortalConstants.TEXT_BUNDLE)%>
				    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		              require(["t360/dialog"], function(dialog) {		 
				       var dialogName = '<%=StringFunction.escapeQuotesforJS(resMgr.getText("CheckFlowTransaction.SendForRepair", TradePortalConstants.TEXT_BUNDLE))%>';	 
				       dialog.open('SendForRepairDialog', dialogName, 'SendForRepairDialog.jsp', null,null, //parameters
			                null, null);
			  });;
					</script>
				</button>
				<%=widgetFactory.createHoverHelp("SendForRepair","common.SendForRepair") %>
		<%
			  }
			
			if (showAuthorize) {
				
				if (!showReCertificate) {
					extraTags = new StringBuffer();
					extraTags.append(request.getParameter("extraAuthorizeTags"));
					
					if (TradePortalConstants.INDICATOR_YES.equals(transaction.getAttribute("converted_transaction_ind"))){ 
		%>
			<button data-dojo-type="dijit.form.Button"  name="ConvertButton" id="ConvertButton" type="button" data-dojo-props="iconClass:'authorize'" >
			    <%=resMgr.getText("common.ConvertText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_CONVERT%>', '0');
						document.forms[0].submit();
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("ConvertButton","ConvertHoverText") %>
		<%}else{%>
			<button data-dojo-type="dojox.form.BusyButton"  name="AuthorizeButton" id="AuthorizeButton" type="button" data-dojo-props="iconClass:'authorize', busyLabel:'<%=StringFunction.escapeQuotesforJS(resMgr.getText("BusyLabel.Authorize",TradePortalConstants.TEXT_BUNDLE))%>', useIcon:false " >
			    <%=resMgr.getText("common.AuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_AUTHORIZE%>', '0');
						document.forms[0].submit();
	 			</script>
			</button>
			  <%=widgetFactory.createHoverHelp("AuthorizeButton","AuthorizeHoverText") %>
		<%  }
				} else {
						String certAuthURL = (String) request.getParameter("certAuthURL");
						//Rel 9.2 XSS CID 11397
						if(null != certAuthURL){
							// Nar IR-T36000037621 XSS escape should not be done on URL.
							certAuthURL = StringFunction.escapeQuotesforJS(certAuthURL);
						}
						String authLink = "javascript:openReauthenticationWindow("
								+ "'" + certAuthURL + "','Authorize')";
		%>
			<button data-dojo-type="dijit.form.Button"  name="Authorize" id="Authorize" type="button" data-dojo-props="iconClass:'authorize'" >
			    <%=resMgr.getText("common.AuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						openURL("<%=authLink%>");
				</script>
			</button>
			  <%=widgetFactory.createHoverHelp("Authorize","AuthorizeHoverText") %>
		<%
				}
				//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - Begin
				if(InstrumentType.DOMESTIC_PMT.equalsIgnoreCase(instrumentType) 
						&& StringFunction.isNotBlank(transaction.getAttribute("panel_auth_group_oid"))
						&& pmtBenPanelAuthInd
						)
				{//Sandeep - Rel 8.3 System Test IR# T36000020739 09/16/2013 - End
		%>
			
			<%-- CR 857 Prateep Start --%>
			
			<button data-dojo-type="dijit.form.Button"  name="ViewPanelAuthorizers" id="ViewPanelAuthorizers" type="button" data-dojo-props="iconClass:'view'" >
			    <%=resMgr.getText("common.ViewPanelAuthorizers",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						var title = "<%=StringFunction.escapeQuotesforJS(resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Title", TradePortalConstants.TEXT_BUNDLE))%>"+"<%=StringFunction.escapeQuotesforJS(instrument.getAttribute("complete_instrument_id"))%>";
						var rowKeyVal = "<%=transactionOid%>"+"/"+"<%=instrumentOidUrlParam%>" ;
						opendialogViewPanelAuthsFromSidebar(title,rowKeyVal,"TradeCash","false");
				</script>
			</button>
			  <%=widgetFactory.createHoverHelp("ViewPanelAuthorizers","ViewPanelAuthorizersHoverText") %>
			 <%-- CR 857 Prateep End --%>
			
			<%
				}
			}
			
			//MEerupula Rel8.3 IR-T36000019060 -- Moved Send For Authorisation button as per IR
			  if(showSendForAuth){
		    %>
		<button data-dojo-type="dijit.form.Button"  name="SendForAuth" id="SendForAuth" type="button" data-dojo-props="iconClass:'authorize'" >
		    <%=resMgr.getText("common.SendForAuth",TradePortalConstants.TEXT_BUNDLE)%>
		    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				setButtonPressed('<%=TradePortalConstants.BUTTON_SEND_FOR_AUTH%>', 0);
				document.forms[0].submit();
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("SendForAuth","common.SendForAuth") %>
		<%		  
			  }
			  
			if (showAuthorizeDelete) {
				extraTags = new StringBuffer();
				extraTags.append(request.getParameter("extraAuthorizeTags"));
				System.out.println("extraTags:" + extraTags.toString());

				//cr498 add reauth link to Authorize & Delete
			if (!showReCertificate) {
				//cquinton 2/11/2013 change buttons to type button to avoid double action
		%>
			<button data-dojo-type="dijit.form.Button"  name="AuthorizeAndDeleteButton" id="AuthorizeAndDeleteButton" type="button" data-dojo-props="iconClass:'authorizeDelete'" >
			    <%=resMgr.getText("common.AuthorizeDeleteText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						setButtonPressed('<%=TradePortalConstants.BUTTON_AUTHORIZEDELETE%>', 0);
						if(confirmAuthorizeDelete()){
							document.forms[0].submit();
						}else{
							return false;
						}
				</script>
			</button>
			  <%=widgetFactory.createHoverHelp("AuthorizeAndDeleteButton","AuthorizeDeleteHoverText") %>
		<%
			} else {
				String certAuthURL = (String) request
						.getParameter("certAuthURL");
				String authAndDeleteLink = "javascript:openReauthenticationWindow("
						+ "'" + certAuthURL + "','AuthorizeAndDelete')"; // Narayan IR-CUUK113047292
		%>
			<button data-dojo-type="dijit.form.Button"  name="AuthorizeAndDeleteButton" id="AuthorizeAndDeleteButton" type="button" data-dojo-props="iconClass:'authorizeDelete'" >
			    <%=resMgr.getText("common.AuthorizeDeleteText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						if(confirmAuthorizeDelete()){
							openURL("<%=authAndDeleteLink%>");
						}else{
							return false;
						}
						
				</script>
			</button>
			  <%=widgetFactory.createHoverHelp("AuthorizeAndDeleteButton","AuthorizeDeleteHoverText") %>
		<%
				}
			}

	        if (showProxyAuthorize) {
	        	if (!showReCertificate) {

	          		extraTags = new StringBuffer();
	          		extraTags.append(request.getParameter("extraAuthorizeTags"));
	%>
					<%--
					//No need to show proxy button if recertification is not required
	          		--%>
	<%
				}
	        	else {
	        	  String certAuthURL = (String)request.getParameter("certAuthURL");
	                  String authLink = "javascript:openReauthenticationWindow(" +
	                      "'" + certAuthURL + "','ProxyAuthorize')";
	%>

	          <button data-dojo-type="dijit.form.Button"  name="ProxyAuthorize" id="ProxyAuthorize" type="button" data-dojo-props="iconClass:'authorize'" >
			    <%=resMgr.getText("common.ProxyAuthorizeText",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						openURL("<%=authLink%>");
				</script>
			</button>
			  <%=widgetFactory.createHoverHelp("ProxyAuthorize","AuthorizeHoverText") %>

	<%
				}
	        }



	//CR-711 Rel8.0 - END
	// DK IR T36000022581 Rel9.0 04/30/2014 - changed icon class to preview
	  if(showViewTerms){
	  %>
	  <button data-dojo-type="dijit.form.Button" name="ViewTermsButton" id="ViewTermsButton" type="button" data-dojo-props="iconClass:'preview'" >
				<%=resMgr.getText("common.ViewTermsAsEnteredText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=formMgr.getLinkAsUrl( "goToInstrumentNavigator", urlParm, response )%>");
        </script>
	 </button>
	<%}
		//SSikhakolli - Rel-8.3 NBS UAT IR#T36000022268 on 10/29/2013 - Adding display condition for 'Collection Schedule and Bill of Exchange' buttons
	  if(showApplnForm && isPortalOriginatedTran && !bankTransUpdateFlag){

		  linkArgs[0] = linkArg0;
		  //linkArgs[0] = "ExpandedType";
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);
          linkArgs[2] = loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
%>
             <button data-dojo-type="dijit.form.Button" name="IssueApplicationForm" id="IssueApplicationForm" type="button" data-dojo-props="iconClass:'attachDocument'" >
            <%-- Leelavathi IR#T36000017435 Rel8.2 05/24/2013 Begin --%>
			<%=(TransactionType.AMEND.equals(transactionType)
			?(InstrumentType.EXPORT_COL.equals(instrumentType)? 
					resMgr.getText("DirectSendCollection.AmendmentScheduleForDCO",TradePortalConstants.TEXT_BUNDLE) 
					:resMgr.getText("DirectSendCollection.AmendmentSchedule",TradePortalConstants.TEXT_BUNDLE)
					)
					:(TransactionType.TRACER.equals(transactionType)
					?resMgr.getText("ExportCollectionTracer.Tracer",TradePortalConstants.TEXT_BUNDLE)
					:resMgr.getText(labelApplicationForm,TradePortalConstants.TEXT_BUNDLE)
					)
					)%>
					<%-- Leelavathi IR#T36000017435 Rel8.2 05/24/2013 End --%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openDoc('<%=formMgr.getDocumentLinkAsURLInFrame (resMgr.getText(labelApplicationForm,TradePortalConstants.TEXT_BUNDLE),linkArg0, linkArgs,response) %>');

		</script>
	     </button>

	<%}
		//SSikhakolli - Rel-9.4 CR-818 - Adding Print Function (Print Settlemetn Instruction Details) button
		if(showPrintSettleInstrs){
			linkArgs[0] = linkArg0;
			linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);
		    linkArgs[2] = loginLocale;
		    linkArgs[3] = userSession.getBrandingDirectory();
		    labelApplicationForm = "SettlementInstruction.PrintSettlInstrDtls";
	%>
			<button data-dojo-type="dijit.form.Button" name="PrintSettlInstrDtls" id="PrintSettlInstrDtls" type="button" data-dojo-props="iconClass:'attachDocument'" >
				<%=resMgr.getText(labelApplicationForm,TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					openDoc('<%=formMgr.getDocumentLinkAsURLInFrame (resMgr.getText(labelApplicationForm,TradePortalConstants.TEXT_BUNDLE),linkArg0, linkArgs,response) %>');
				</script>
	     	</button>

		<%}

		 if ((TradePortalConstants.TRANS_STATUS_FX_THRESH_EXCEEDED.equals(transactionStatus) ||
			    TradePortalConstants.TRANS_STATUS_AUTH_PEND_MARKET_RATE.equals(transactionStatus)) && userSession.hasAccessToLiveMarketRate()) {

		   isEditable = InstrumentServices.isEditableTransStatus(transactionStatus);
       	    canAuthorize = SecurityAccess.canAuthorizeInstrument(userSession.getSecurityRights(), instrumentType, transactionType);
       	    adminCustAccess = false;
	        boolean canRequestRate = false;
	           //Vshah - Rel7.1 - IR#DAUL122659077 - 01/05/2012 - <BEGIN>
	           //Comment out the below line and re-written the logic to populate "canRequestRate"
	           //Based on the FX Online functionality of the Bank Group associated to the OP Org of the Debit Account.
	           DocumentHandler fxOnlineResultDoc = null;
	           if (fxOnlineResultDoc != null) {
	    		    canRequestRate = (TradePortalConstants.INDICATOR_YES.equals(fxOnlineResultDoc.getAttribute("/ResultSetRecord(0)/FX_ONLINE_AVAIL_IND")))  ? true : false ;
	    	   }
	     
	           //Vshah - Rel7.1 - IR#DAUL122659077 - 01/05/2012 - <END>


       if (userSession.hasSavedUserSession())       {
			if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSessionSecurityType()))
				adminCustAccess = true;
			else if (TradePortalConstants.INDICATOR_YES.equals((String)request.getAttribute("panelEnabled")))
				adminCustAccess = true;

 	}
	       if (!isEditable && canAuthorize && !adminCustAccess) {
		  showRequestMarketRate= true;

		%>
        <button data-dojo-type="dijit.form.Button"  name="GetRate" id="GetRate" type="button" data-dojo-props="iconClass:'getMarketRate'" >
			    <%=resMgr.getText("TransferBetweenAccounts.RequestMarketRate",TradePortalConstants.TEXT_BUNDLE)%>
			    <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					getMarketRate(); return false;
	 			</script>
			</button>

			<%=widgetFactory.createHoverHelp("GetRate","RequestRateHoverText") %>

 <%
     }
   }
		//SSikhakolli - Rel-8.3 NBS UAT IR#T36000022268 on 10/29/2013 - Adding display condition for 'Collection Schedule and Bill of Exchange' buttons
	  if(showBillOfExchangeForm && isPortalOriginatedTran){
		  linkArgs[0] = TradePortalConstants.PDF_BILL_OF_EXCHANGES;
          linkArgs[1] = EncryptDecrypt.encryptStringUsingTripleDes(transactionOid);
          linkArgs[2] = loginLocale;
          linkArgs[3] = userSession.getBrandingDirectory();
%>
         <button data-dojo-type="dijit.form.Button" name="BillOfExchangeForm" id="BillOfExchangeForm" type="button" data-dojo-props="iconClass:'attachDocument'" >
					<%=resMgr.getText("ExportCollectionIssue.BillOfExchanges",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	          openDoc('<%=formMgr.getDocumentLinkAsURLInFrame (resMgr.getText("ExportCollectionIssue.BillOfExchanges",TradePortalConstants.TEXT_BUNDLE),TradePortalConstants.PDF_BILL_OF_EXCHANGES, linkArgs,response) %>');
	        </script>
		 </button>
	
<%
  }
  //cquinton 1/18/2013 remove old close action logic
  //cquinton 1/18/2013 return to close navigator
  String closeLink = formMgr.getLinkAsUrl("goToInstrumentCloseNavigator","&returning=true",response);
  // Always show close button
%>
			<button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
				<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=closeLink%>");
        </script>
			</button>
      <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
      <%--cquinton 8/21/2012 Rel portal refresh end--%>

	</ul>

	<%
		//VS RVU062382912 Adding New parameter value for authorizing transactions
		if (userSession.getSavedUserSession() == null) {
	%>

	<input type="hidden" name="AuthorizeAtParentViewOrSubs" value="Y"/>

	<%
		}
	%>

<input type="hidden" name="cTime" id="cTime" />
<input type="hidden" name="prevPage" id="prevPage" />
<input type="hidden" name="autoSaved" id="autoSaved" />
<input type="hidden" name="templateAutoSaved" id="templateAutoSaved" />

	<script language="JavaScript">
	 <%--
	 // This function is used to display a popup message confirming
	 // that the user wishes to delete the selected items in the
	 // listview.--%>
		function openURL(URL){
		    if (isActive =='Y') {
		    	if (URL != '' && URL.indexOf("javascript:") == -1) {
			        var cTime = (new Date()).getTime();
			        URL = URL + "&cTime=" + cTime;
			        URL = URL + "&prevPage=" + context;
		    	}
		    }
	 		document.location.href  = URL;
	 		return false;
 		}
		function openDoc(URL){
	 		window.open(URL);
	 		return false;
 		}

		function confirmDocumentDelete(){
			<%-- Changes done for PortalRefresh IR - T36000004278 - Pavani Mitnala --%>
			<%-- Changes are required as the existing logic does not work for the dojo checkboxes. --%>
			var returnValue;
			require(["dijit/registry"],
				function(registry){
		    		var confirmMessage = '<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("PendingTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE))%>';
					
				    <%-- Find any checked checkbox and flip a flag if one is found --%>
				    var isAtLeastOneDocumentChecked = Boolean.FALSE;
				    if (document.forms[0].AttachedDocument != null) {
				    	if (document.forms[0].AttachedDocument.length != null) {
			          for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
			        	<%-- Jyothikumari.G - 22/03/2013 - Rel 8200 IR-T36000014692  - Start --%>
		                <%-- Un Commented the bellow line because it was Unable to delete attached documents when 2 or more docs are attached to instrument. --%>
			        	  <%-- var widgetID = document.forms[0].AttachedDocument[i].value; --%>
			        	 <%-- Jyothikumari.G - 22/03/2013 - Rel 8200 IR-T36000014692  - End --%>
			        	 
				        	  if(document.forms[0].AttachedDocument[i].checked){
				             	isAtLeastOneDocumentChecked = true;
				             }					     
				          }
				       }
				       else if (registry.byId("AttachedDocument1").checked==true) {
				          isAtLeastOneDocumentChecked = true;
				       }
				       
				    }
				    <%-- Setting the values--%>
				    if (isAtLeastOneDocumentChecked==true) {
					       if (confirm(confirmMessage)==true) {
					       <%-- Updated the below if block as per the MessagesSidebar.frag as the Attachment deletion is working fine in Messages- Start --%>
				    	   if (document.forms[0].AttachedDocument != null) {
				    		     if (document.forms[0].AttachedDocument.length != null) {
					                for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
					                	<%-- Jyothikumari.G - 22/03/2013 - Rel 8200 IR-T36000014692  - Start --%>
					                	<%-- modified the bellow code because it was Unable to delete attached documents when 2 or more docs are attached to instrument. --%>
					                	<%-- var widgetID = document.forms[0].AttachedDocument[i].value; --%>
					                	
					                	  if(document.forms[0].AttachedDocument[i].checked){
							        		  document.forms[0].AttachedDocument[i].value=document.forms[0].AttachedDocument[i].value;
							        	<%-- Jyothikumari.G - 22/03/2013 - Rel 8200 IR-T36000014692  - End --%>
							        	}else{
							        		document.forms[0].AttachedDocument[i].value="";
							        	}					                	  
					                }
					             }
					             else if (registry.byId("AttachedDocument1").checked==true) {
					                document.forms[0].AttachedDocument.value = registry.byId("AttachedDocument1").value;
					             }
					          }
				    	 <%-- Updated the below if block as per the MessagesSidebar.frag as the Attachment deletion is working fine in Messages- End --%>
					    	   returnValue = true;
				    }else{
				    	returnValue = false;
				    }
			}else {
				<% if(showBankAttachDeleteDoc){%>
				alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("common.AttachedDocumentNotSelected", TradePortalConstants.TEXT_BUNDLE))%>');
				<%}else{%>
					alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE))%>');
				<%}%>				
				returnValue = false;
			}

			});
			if (returnValue==true) {
				formSubmitted = true;
			    return true;
			}else{
				formSubmitted = false;
			    return false;
			}
		}
</script>