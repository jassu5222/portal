<%--
*******************************************************************************
                             TransactionButtonBar

  Description:
     Creates the transaction button bar.  The Save, Save&Close, Route, Verify,
  Edit, Authorise, and Close buttons may display.  All of these buttons display
  conditionally.

  Parameters:
     None.  The assumption is that 'instrument' and 'transaction' are webbeans
  available to this JSP
*******************************************************************************
--%>


<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*, com.ams.tradeportal.html.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<script>
  require(["dijit/registry", "dojo/on", "t360/common", "dojo/ready"],
      function(registry, on, common, ready) {
    ready(function() {
    	dojo.query("input[type='button']").forEach(function(node){
			registry.getEnclosingWidget(node).on( "click", function(evt){
				console.log(evt);
				var cTime = evt.timeStamp;
				document.getElementById("cTime").value= (new Date(cTime)).getTime();
				document.getElementById("cTime").text= (new Date(cTime)).getTime();
				document.getElementById("prevPage").value= context;
				document.getElementById("prevPage").text= context;
				});
	  	});
      <%--register event handlers--%>
      <%--todo: only execute when necessary--%>
      <%--todo: use event propagation to reduce registry associations--%>
      var myButton = registry.byId("SaveButton");
      if (myButton) {
        myButton.on("click", function() {
        	<%--- Suresh_L  02/06/2014 Rel8.4  IR-T36000013975 -start  ---%>       
         if(  common.setButtonPressed('SaveTrans','0')==true)
          {
          	<%--  DK IR T36000025854 Rel8.4 03/06/2014 starts - disable button --%>
          	registry.byId("SaveButton").set('disabled',true);
          	document.forms[0].submit();
          }
          else{
        	  registry.byId("SaveButton").set('disabled',false);
        	 <%--  DK IR T36000025854 Rel8.4 03/06/2014 ends --%>
          }
          
          <%--- Suresh_L  02/06/2014 Rel8.4  IR-T36000013975 -end  ---%>     
          
        });
      }
      var myButton = registry.byId("SaveCloseButton");
      if (myButton) {
        myButton.on("click", function() {
          common.setButtonPressed('SaveCloseTrans','0');
          document.forms[0].submit();
        });
      }
      var myButton = registry.byId("VerifyButton");
      if (myButton) {
        myButton.on("click", function() {
       <%--- Suresh_L  02/06/2014 Rel8.4  IR-T36000013975 -start  ---%> 
        if(  common.setButtonPressed('Verify','0')==true)
        {
         <%--  DK IR T36000025854 Rel8.4 03/06/2014 starts --%>
        	registry.byId("VerifyButton").set('disabled',true);
        	document.forms[0].submit();
        }
        else{
        	registry.byId("VerifyButton").set('disabled',false);
        <%--  DK IR T36000025854 Rel8.4 03/06/2014 ends --%>
        }
        <%--- Suresh_L  02/06/2014 Rel8.4  IR-T36000013975 -end  ---%>    
        });
      }
    });
  });
</script>
