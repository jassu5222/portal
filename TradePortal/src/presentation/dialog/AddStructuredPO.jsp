<%--
*******************************************************************************
                            Add Structured PO Page

  Description:  The Add PO  page is used to present the user with a 
                list of PO  from which he/she can select and add to 
                the transaction that is currently being edited. This page can 
                only be accessed in edit mode and only by organizations that 
                support Structured purchase order processing and by users having the 
                necessary security right to do the same. Currently, this page 
                is accessible only to Import LC - Issue , Import LC - Amend
                and ATP transaction types.

                This page contains a listview of PO , each having 
                its own checkbox. The actual PO  that appear in this 
                listview depend on whether or not the transaction currently 
                being edited has PO  assigned to it. If PO  
                *are* currently assigned to it, the po upload definition
                oid, beneficiary name, and currency for the assigned PO  
                are passed into the page. The listview will then display
                only those unassigned PO  that match all three of the 
                fields that were passed in. If the transaction currently 
                doesn't have any PO  assigned to it, the listview 
                will display all unassigned PO  belonging to the 
                user's organization.  

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%


    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;
	TransactionWebBean transaction = null;
	DocumentHandler xmlDoc = null;
	StringBuffer dynamicWhereClause = null;
	Hashtable secureParms = null;
	boolean hasStructuredPOs = false;
	String uploadDefinitionOid = null;
	String userSecurityType = null;
	String beneName = null;
	String transactionOid = null;
	String userOrgOid = null;
	String currency = null;
	String link = null;
	String shipmentOid = null;
	String sourceType = null;
	int numberOfRows = 0;

	String poNumber = "";
	String lineItemNumber = "";
	boolean fromAddStrcuPO = true;
	
	String form_name = "";
	String ins_type = request.getParameter("ins_type");

	if ("ATP".equals(ins_type)) {
		form_name = "TransactionATP";
	} else if ("IMPORTLC".equals(ins_type)) {
		form_name = "TransactionIMP_DLC";
	} 
	
	
	
	// Get the user's security type and organization oid
	userSecurityType = userSession.getSecurityType();
	userOrgOid = userSession.getOwnerOrgOid();
	

	// Get the current transaction web bean so that we can pass its oid to the Add PO  mediator
	transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

	transactionOid = transaction.getAttribute("transaction_oid");

	xmlDoc = formMgr.getFromDocCache();

	// If there are PO  currently assigned to the transaction, then the PO upload definition
	// oid, beneficiary name, and currency will be passed into this page in the XML; otherwise,
	// check to see if they were passed back into the page from the Add PO  mediator.
	if (!InstrumentServices
			.isBlank(xmlDoc
					.getAttribute("/In/SearchForPO/uploadDefinitionOid"))) {
		uploadDefinitionOid = xmlDoc
				.getAttribute("/In/SearchForPO/uploadDefinitionOid");
		beneName = xmlDoc
				.getAttribute("/In/SearchForPO/beneficiaryName");
		currency = xmlDoc
				.getAttribute("/In/SearchForPO/currency");
		shipmentOid = xmlDoc
				.getAttribute("/In/SearchForPO/shipment_oid");

		if (!ConvenienceServices.isBlank(uploadDefinitionOid))
			hasStructuredPOs = true;
	} else {
		// If we're coming back to this page as a result of the transaction previously having
		// PO , get the PO upload definition oid, beneficiary name, and currency from 
		// the /In section of the xml doc; otherwise, try to get it from the /Out section (in 
		// the case where the transaction did *not* previously have PO  assigned to it).
		uploadDefinitionOid = xmlDoc
				.getAttribute("/In/Transaction/uploadDefinitionOid");
		shipmentOid = xmlDoc
				.getAttribute("/In/Terms/ShipmentTermsList/shipment_oid");

		if (xmlDoc.getAttribute("/Out/Transaction") != null) {
			uploadDefinitionOid = xmlDoc
					.getAttribute("/Out/Transaction/uploadDefinitionOid");
			beneName = xmlDoc
					.getAttribute("/Out/Transaction/beneficiaryName");
			currency = xmlDoc.getAttribute("/Out/Transaction/currency");
			hasStructuredPOs = true;
		}
	}
	xmlDoc.setAttribute("/In/SearchForPO/uploadDefinitionOid",
			uploadDefinitionOid);
	xmlDoc.setAttribute("/In/SearchForPO/beneficiaryName",
			beneName);
	xmlDoc.setAttribute("/In/SearchForPO/currency",
			currency);
	// If this is a new shipment, the shipment OID will be in the out section of the XML
	if ((shipmentOid == null) || shipmentOid.equals("0")
			|| shipmentOid.equals("")) {
		String numShipmentsString = xmlDoc
				.getAttribute("/In/numberOfShipments");

		int numShipments = 1;

		if (numShipmentsString != null)
			numShipments = Integer.parseInt(numShipmentsString);

		ShipmentTermsWebBean shipmentTerms = beanMgr.createBean(ShipmentTermsWebBean.class, "ShipmentTerms");
		TermsWebBean terms = (TermsWebBean) beanMgr.getBean("Terms");

		shipmentTerms.loadShipmentTerms(
				terms.getAttribute("terms_oid"), numShipments);

		shipmentOid = shipmentTerms.getAttribute("shipment_oid");
		xmlDoc.setAttribute("/In/SearchForPO/shipment_oid",
				shipmentOid);
		xmlDoc.setAttribute("/In/Terms/ShipmentTermsList/shipment_oid",
				shipmentOid);
	}


	if (!hasStructuredPOs) {
		currency = xmlDoc.getAttribute("/In/SearchForPO/currency");
	} 
	//IR T36000017820 -Get PO Currency 
	currency = StringFunction.xssCharsToHtml(request.getParameter("poLineItemCurrency"));
	beneName = request.getParameter("poLineItemBeneficiaryName");
	uploadDefinitionOid = StringFunction.xssCharsToHtml(request.getParameter("poLineItemUploadDefinitionOid"));
	if(StringFunction.isNotBlank(uploadDefinitionOid) || StringFunction.isNotBlank(beneName) ){
		hasStructuredPOs = true;
	}
	
	// Set a bunch of secure parameters necessary for the Add PO mediator
	secureParms = new Hashtable();
	secureParms.put("transactionOid", transactionOid);
	secureParms.put("shipmentOid", shipmentOid);
	secureParms.put("UserOid", userSession.getUserOid());
	secureParms.put("hasStructuredPOs", String.valueOf(hasStructuredPOs));

	if (hasStructuredPOs) {
		secureParms.put("uploadDefinitionOid", StringFunction.xssCharsToHtml(uploadDefinitionOid));
		secureParms.put("currency", StringFunction.xssCharsToHtml(currency));
	}
	secureParms.put("beneficiaryName", StringFunction.xssCharsToHtml(beneName));
	 
%>


<%-- ********************* HTML for page begins here *********************  --%>


 <jsp:include page="/common/ErrorSection.jsp" /> 
<div class="POPopUpContent">
<%=formMgr.getFormInstanceAsInputField("AddStructuredPOForm", secureParms)%> 

<input type=hidden name="buttonName" value=""> 
<input type=hidden name="hasPOLineItems" value="<%=hasStructuredPOs%>">

<%@ include file="/transactions/fragments/StructuredPOSearch.frag"%>

<%
gridLayout = dgFactory.createGridLayout("addStructurePODataDataGridId", "AddStructurePODataGrid");
gridHtml = dgFactory.createDataGrid("addStructurePODataDataGridId","AddStructurePODataGrid",null);
%>
<%=gridHtml%>
</div>



    
 <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;
								
		var initSearchParms="userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";			
			initSearchParms+="&transactionTypeCode=<%=transaction.getAttribute("transaction_type_code")%>";
			initSearchParms+="&hasStructuredPOs=<%=hasStructuredPOs%>";
			initSearchParms+="&uploadDefinitionOid=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uploadDefinitionOid))%>";
			initSearchParms+="&currency=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(currency))%>";
			initSearchParms+="&beneName=<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(beneName))%>";
			
		
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AddStructurePODataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("addStructurePODataDataGridId", viewName,gridLayout, initSearchParms);
		
		function filterPOs(){
			require(["dojo/dom","dojo/domReady!"],
			        function(dom){
			
			
			var benName = dom.byId("BeneName").value;
			var poNumber = dom.byId("PONum").value;
															
			<%-- SSikhakolli - Rel 9.0 IR# T36000029375 06/19/2014 - Begin - Passing 'poNumber' in searchParms --%>
			var searchParms= initSearchParms;
			searchParms+="&beneficiaryName="+benName+"&poNumber="+poNumber;
			<%-- SSikhakolli - Rel 9.0 IR# T36000029375 06/19/2014 - End --%>
			

			searchDataGrid("addStructurePODataDataGridId", "AddStructurePODataView", searchParms);
			});
		}
		
		function addStructurePOData(){
		  require(["t360/common"], function(common) {
			var rowKeys = getSelectedGridRowKeys("addStructurePODataDataGridId"),
			    buttonName = 'AddStructuredPOButton',
			    formName = '<%=form_name%>';
			
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			
			var myForm = document.getElementsByName(formName)[0];
			common.addParmToForm(myForm, 'hasStructuredPOs', '<%=hasStructuredPOs%>');
			<%--  hideDialog("AddStructurePODialog");  --%> <%-- IR T36000045428 Commenting Hide Dialog as we are submiting form. --%>
			common.submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			
		  });
	
		}
		
		function closeStructurePODataItems(){
			require(["dijit/registry"],
					 function(registry) {																		
						hideDialog("AddStructurePODialog");
			});

		}

		var dialogName=dijit.byId("AddStructurePODialog");
		var dialogCloseButton=dialogName.closeButtonNode;
		dialogCloseButton.setAttribute("id","AddStructurePODialogCloseButton");		
	</script>   
	<%=widgetFactory.createHoverHelp("AddStructurePODialogCloseButton", "common.Cancel") %>	
</body>
</html>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
