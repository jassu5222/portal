<%--
 *  Bank Branch Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,
                 net.sf.json.*, net.sf.json.xml.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 



  //read in parameters
  String addSubAccDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
  String corporateOrgOid = StringFunction.xssCharsToHtml(request.getParameter("corporateOrgOid"));
  QueryListView queryListView = null;
  DocumentHandler acctList = new DocumentHandler();

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);

  String ownerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(ownerOrg);
  String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                        corpOrg.getAttribute("second_op_bank_org"),
                        corpOrg.getAttribute("third_op_bank_org"),
                        corpOrg.getAttribute("fourth_op_bank_org")};

  StringBuffer custNameListSql = new StringBuffer();
	custNameListSql.append("select ORGANIZATION_OID, NAME from CORPORATE_ORG");
	custNameListSql.append(" connect by prior ORGANIZATION_OID = P_PARENT_CORP_ORG_OID");
	custNameListSql.append(" start with P_PARENT_CORP_ORG_OID =? ");
	
  Debug.debug(custNameListSql.toString());
  //BSL IR HHUL070671455 07/07/11 End
		
  DocumentHandler custNameListHandler = DatabaseQueryBean
    .getXmlResultSet(custNameListSql.toString(), false,new Object[]{corporateOrgOid});
  String custNameList = ListBox.createOptionList(custNameListHandler,
    "ORGANIZATION_OID", "NAME", "", null);

  //out.println(getJson(acctList));
  
%>
<%--<div class="formContent">--%>

<div id="addSubAccDialogContent" class="dialogContent fullwidth">

  <div class="searchDetail">
    <span class="searchCriteria">
      <%= widgetFactory.createSearchSelectField( "custName1", "AddSubsidiaryAccount.criteria.Subsidiary", resMgr.getText("common.all", TradePortalConstants.TEXT_BUNDLE), custNameList,"")%>
      <%= widgetFactory.createSearchTextField( "accName", "AddSubsidiaryAccount.criteria.AccountName", "35")%>
    </span>
    <span class="searchActions">
      <button id="addSubAcctSearchButton" data-dojo-type="dijit.form.Button" type="button">
        <%=resMgr.getText("common.search", TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          addSubDialog.searchAccounts();return false;
        </script>
      </button>
      <%--cquinton 11/13/2012 add hover help--%>
      <%=widgetFactory.createHoverHelp("addSubAcctSearchButton","SearchHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>

<%
  String gridHtml = dgFactory.createDataGrid("addSubGrid","SubsidiaryAccountsDataGrid",null);
%>
  <%= gridHtml %>

  <%=widgetFactory.createHoverHelp("AddSubsidiaryAccount_AddAccounts","CorpCustSubsidiaryAccounts.AddAccounts") %>
  <%=widgetFactory.createHoverHelp("AddSubsidiaryAccount_cancel","common.cancel") %>

</div>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>

  var addSubDialog;

  require(["dijit/registry", "t360/datagrid", "dojo/domReady!"],
      function( registry, t360grid ) {
 
    var initSearchParmsAddSub = "corporateOrgOid=<%=StringFunction.escapeQuotesforJS(corporateOrgOid)%>";
<%
  String addSubGridLayout = dgFactory.createGridLayout("addSubGrid", "SubsidiaryAccountsDataGrid");
%>
    var gridLayout = <%= addSubGridLayout %>;
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("SubsidiaryAccountsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    <%--create a local memory grid--%>
    var myGrid = t360grid.createDataGrid("addSubGrid", viewName,gridLayout, initSearchParmsAddSub);
    myGrid.set('autoHeight',10);

    addSubDialog = {
      searchAccounts: function(){
        var searchParms="corporateOrgOid=<%=StringFunction.escapeQuotesforJS(corporateOrgOid)%>";
        var customerName = registry.byId("custName1").value;
        if ( customerName && customerName != null && customerName.length>0 ) {
          searchParms+="&custName="+customerName;
        }
        var accountName = registry.byId("accName").value;
        if ( accountName && accountName != null && accountName.length>0 ) {
          searchParms+="&accountName="+accountName;
        }
        t360grid.searchDataGrid("addSubGrid", "SubsidiaryAccountsDataView", searchParms);
      }
    };
  });
  
  function selectSA() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!", "dojo/_base/array"],
      function(registry, query, on, dialog, ready, baseArray) {
        var myGrid = registry.byId("addSubGrid");
        var items = myGrid.selection.getSelected();
   	
        <%-- baseArray.forEach(items, function(item){ --%>
        
        <%-- }); --%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(addSubAccDialogId))%>', 'selectAddSub', items);
        dialog.hide('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(addSubAccDialogId))%>');
    });
  }
 
  function closeSADialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(addSubAccDialogId)%>');
    });
  }

  
  
</script>
<%!
public String getJson(DocumentHandler xmlDoc) 
throws Exception {
String xmlResultStr = null;
JSON json = null;

if(xmlDoc == null)
	return null;

xmlResultStr = xmlDoc.toString();

XMLSerializer xmlSerializer = new XMLSerializer();   
json = xmlSerializer.read( xmlResultStr );        

return "{\"numRows\":"+json.size()+",\"items\":"+json.toString()+"}";
}
%>
