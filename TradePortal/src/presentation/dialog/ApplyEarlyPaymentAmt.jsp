<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
    com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*" %>
    

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
        
   DocumentHandler  doc = formMgr.getFromDocCache();  
   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   String selectedCount = request.getParameter("selectedCount");
  
 //read in parameters
   String dialogId = request.getParameter("dialogId");
 
    if(StringFunction.isBlank(request.getParameter("selectedCount"))){
       MediatorServices medService = new MediatorServices();
       medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
       medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
               TradePortalConstants.NO_ITEM_SELECTED,
               resMgr.getText("UploadInvoices.ApplyEarlyPaymentAmount", TradePortalConstants.TEXT_BUNDLE));
        medService.addErrorInfo();
        doc.addComponent("/Error", medService.getErrorDoc());
        formMgr.storeInDocCache("default.doc", doc);
    }
%>

<div id="applyPaymentAmountContent" class="dialogContent">
<form id="ApplyPaymentAmount" method="post" name="ApplyPaymentAmountForm" data-dojo-type="dijit.form.Form">
  <div class="formItem">
    <%= widgetFactory.createAmountField( "payment_amount", "UploadInvoices.EarlyPaymentAmt", "", "", false, false, false, 
    		"class='char15' maxLength='19'", "regExp:'[0-9]{0,13}([.][0-9]{0,5})?'", "") %>
  </div>
   <div class="formItem">
    <button data-dojo-type="dijit.form.Button"  name="modify" id="modify" type="button">
        <%=resMgr.getText("UploadInvoices.ApplyEarlyPaymentAmount",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            applyPaymentAmount();                       
        </script>
    </button> 
 
    <button data-dojo-type="dijit.form.Button"  name="cancel" id="cancel" type="button">
        <%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            cancelOne();                        
        </script>
    </button>     
  </div>
 </form>
</div>

<script type='text/javascript'>

function applyPaymentAmount() {
    require(["dijit/registry", "t360/dialog","dojo/dom","dojo/domReady!" ],
            function(registry, dialog,dom ) {  
     var paymentAmount = dom.byId("payment_amount").value;
     dialog.doCallback('<%=StringFunction.escapeQuotesforJS(dialogId)%>', 'select', paymentAmount);
    });
}

function closeDialog()
{
    hideDialog('<%=StringFunction.escapeQuotesforJS(dialogId)%>');
}

function cancelOne(){    
    require(["t360/dialog"], function(dialog) {
    dialog.hide('<%=StringFunction.escapeQuotesforJS(dialogId)%>');
});
}
 
</script>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>

