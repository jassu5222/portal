<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*,java.util.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%

		
   String invoiceDateDay = null;
   String invoiceDateMonth = null;
   String invoiceDateYear = null; 
   String payment_date = null;
   DocumentHandler  doc = formMgr.getFromDocCache();  
   String globalNavigationTab = userSession.getCurrentPrimaryNavigation();
   String formName = "ApplyPaymentDateToSelectionForm";
   //BSL CR710 02/16/2012 Rel8.0 BEGIN - update for grouping
   String fromInvoiceGroupDetail = (String)session.getAttribute("fromInvoiceGroupDetail");
   String currentTab = (String)session.getAttribute("currentInvoiceMgmtTab");
   if (!TradePortalConstants.INDICATOR_YES.equals(fromInvoiceGroupDetail) &&
		   TradePortalConstants.INV_GROUP_TAB.equals(currentTab)) {
      formName = "ApplyPaymentDateToGroupSelectionOnDetailForm";
   }
   else if("PG".equals(request.getParameter("fromPage"))){
	   formName = "PayableInvoiceGroupForm";
   }
   else if("PL".equals(request.getParameter("fromPage"))){
	   formName = "PayableInvoiceDetail-PayableDeleteSelectedInvoice";
   }
   //BSL CR710 02/16/2012 Rel8.0 END
   String cancelLink=formMgr.getLinkAsUrl("goToInvoiceManagement", response); 
   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   Hashtable secureParms        = new Hashtable();
   secureParms.put("SecurityRights", userSession.getSecurityRights());
   secureParms.put("UserOid",        userSession.getUserOid());
   secureParms.put("ownerOrg",       userSession.getOwnerOrgOid());
   String selectedCount = StringFunction.xssCharsToHtml(request.getParameter("selectedCount"));
   String dialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
   String  selectedStatus   = "";
   String  loginLocale      = null;
   Vector  codesToExclude   = null;
   codesToExclude = new Vector();
  // codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_DELETED);
 //  codesToExclude.add(TradePortalConstants.UPLOAD_INV_STATUS_AUTH_FAILED);
	//Srinivasu_D CR-709 Rel8.2 02/02/2013  start
   loginLocale=userSession.getUserLocale();
	CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
	corpOrg.getById(userSession.getOwnerOrgOid());
   String loanAllow = corpOrg.getAttribute("allow_loan_request");  
   String atpAllow = corpOrg.getAttribute("allow_approval_to_pay");
   //Rel9.0 CR 913 START
   String payablesAllow = TradePortalConstants.INDICATOR_NO;
   if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("pay_invoice_utilised_ind")) ) {
	   payablesAllow = TradePortalConstants.INDICATOR_YES;
   }
   //Rel9.0 CR 913 END
   String receivablesAllow = TradePortalConstants.INDICATOR_NO;
   if(TradePortalConstants.INDICATOR_YES.equals(corpOrg.getAttribute("rec_invoice_utilised_ind")) ) {
	   receivablesAllow = TradePortalConstants.INDICATOR_YES;
   }
   Debug.debug("loanALlow:"+loanAllow+"\t atpAllow:"+atpAllow+"\t payablesAllow:"+payablesAllow+"\t receivablesAllow:"+receivablesAllow);
   

   StringBuffer instTypeOptions = null;
   if("PG".equals(request.getParameter("fromPage")) || "PL".equals(request.getParameter("fromPage"))){
	   if(TradePortalConstants.INDICATOR_NO.equals(loanAllow)){
		   codesToExclude.add(InstrumentType.LOAN_RQST);
	}
	if(TradePortalConstants.INDICATOR_NO.equals(atpAllow)){
		   codesToExclude.add(InstrumentType.APPROVAL_TO_PAY);
	}
	//Rel9.0 CR 913 START
	if(TradePortalConstants.INDICATOR_NO.equals(payablesAllow)){
        codesToExclude.add(TradePortalConstants.INV_LINKED_INSTR_PYB);
 	}
	//Rel9.0 CR 913 END
	if(TradePortalConstants.INDICATOR_NO.equals(receivablesAllow)){
        codesToExclude.add(TradePortalConstants.INV_LINKED_INSTR_REC);
 	}
	   instTypeOptions = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_REQ_PAY_INSTR_TYPE",
               selectedStatus, loginLocale, codesToExclude));
	}
   else{
	   if(TradePortalConstants.INDICATOR_NO.equals(loanAllow)){
		   codesToExclude.add(InstrumentType.LOAN_RQST);
	}
	   if(TradePortalConstants.INDICATOR_NO.equals(receivablesAllow)){
	        codesToExclude.add(TradePortalConstants.INV_LINKED_INSTR_REC);
	 	}
	 instTypeOptions = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_REQ_INSTRUMENT_TYPE",
                                             selectedStatus, loginLocale, codesToExclude));
   }
	StringBuffer insttBlank = new StringBuffer();	
		    insttBlank.append("<option value='").append("").append("'>");
            insttBlank.append("");
            insttBlank.append("</option>");
			insttBlank.append(instTypeOptions);
			StringBuffer loanTypeDropdown = null;
			codesToExclude = InvoiceUtility.getLoanTypesNotAllowed(userSession.getOwnerOrgOid());
			if("PG".equals(request.getParameter("fromPage")) || "PL".equals(request.getParameter("fromPage"))){
				codesToExclude.add(TradePortalConstants.EXPORT_FIN);
				codesToExclude.add(TradePortalConstants.INV_FIN_REC);
				loanTypeDropdown = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_TYPE",
                        selectedStatus, loginLocale, codesToExclude));
			}
			else{
				codesToExclude.add(TradePortalConstants.INV_FIN_BR);
				codesToExclude.add(TradePortalConstants.INV_FIN_BB);
	 	 		loanTypeDropdown = new StringBuffer(Dropdown.createSortedRefDataOptions("LOAN_TYPE",
                                             selectedStatus, loginLocale, codesToExclude));
			}
	 StringBuffer loanBlank = new StringBuffer();	
		    loanBlank.append("<option value='").append("").append("'>");
            loanBlank.append("");
            loanBlank.append("</option>");
		loanBlank.append(loanTypeDropdown);
	//Srinivasu_D CR-709 Rel8.2 02/02/2013  End  
	
	String buttonPressed = "ListAssignInstrType";
	if("PL".equals(request.getParameter("fromPage"))){
		buttonPressed = "InvAssignInstrType";
	}
		
%>





<%-- check for security --%>


<div id="assignPaymentDateContent" class="invoiceDialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

  <div class="formItem">
   <table border='0'>
	<tr><td nowrap>
     <%=resMgr.getText("UploadInvoiceAction.InstrumentType",TradePortalConstants.TEXT_BUNDLE) %>
	 </td><td></td><td>
	<%
	 out.print(widgetFactory.createSelectField("instrumentType", "", "",
                                               insttBlank.toString(), false,
                                               false, false, "onChange='hideLoanTypeDiv();'", "", ""));   
	%>
	</td>
	<%-- Srinivasu_D CR-709 Rel8.2 02/02/2013  start --%>
	<td></td>
	
	<td nowrap>
	<div id='LoanTypeDiv' style="display:block">
	<table><tr><td>
	<%if(TradePortalConstants.INDICATOR_YES.equals(loanAllow)){ %>
	<%=resMgr.getText("UploadInvoiceAction.LoanType",TradePortalConstants.TEXT_BUNDLE) %>
	
	</td>
	<td>
	<%
	 out.print(widgetFactory.createSelectField("loanType", "",  "", loanBlank.toString(), false,
                                               false, false, "", "", ""));   
	}
	%>
	</td>
	</div>
	</tr></table>
	</td>
	</tr>
	</table>
	</div>
	
  
  <br><br><br>

    <div class="formItem">
	
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("UploadInvoiceAction.Continue",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			assignInstrumentType();     					
		</script>
	</button> 

	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button">
		<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			closeDialog();     					
		</script>
	</button> 

	
 </form>
</div>

<script type='text/javascript'>

function assignInstrumentType() {
require(["dijit/registry", "t360/dialog"],
        function(registry, dialog ) { 
			var instType = registry.byId("instrumentType");		
			var loanType = registry.byId("loanType");
			if(instType == ""){			
				return false;	
				registry.byId("instrumentType").focus();
			}
			<%--  if(instType == "LRQ" && loanType == ""){			
				return false;	
				registry.byId("loanType").focus();
			}  --%>
			if(instType == "REC" && instType == "ATP")
				loanType="";
	     dialog.doCallback('<%=StringFunction.escapeQuotesforJS(dialogId)%>', 'select',instType,loanType);
		
		});
}

function closeDialog()
{
    hideDialog("InvoiceGroupListDialogID");
}



function hideLoanTypeDiv(){
	  require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom) { 
		 	var instType = registry.byId("instrumentType");		
		<%-- IR 27063- hide loanType div for PYB invoices --%>
			if(instType == "<%=TradePortalConstants.INV_LINKED_INSTR_PYB%>"  || 
					instType == "<%=InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT%>" || instType == "<%=InstrumentType.APPROVAL_TO_PAY%>"){				
				dom.byId("LoanTypeDiv").style.display='none';
				if(registry.byId("loanType")){
				registry.byId("loanType").value='';
				}
			}else{				
				dom.byId("LoanTypeDiv").style.display='block';
				registry.byId("loanType").focus();
			}
		 
	    });
	}	
 	<%--  Srinivasu_D CR-709 Rel8.2 02/02/2013 End --%>
</script>
</div>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>
