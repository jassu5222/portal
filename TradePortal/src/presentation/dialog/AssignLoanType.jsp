<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.html.*,java.util.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%

		
	String invoiceDateDay = null;
	String invoiceDateMonth = null;
	String invoiceDateYear = null;
	String payment_date = null;
	DocumentHandler doc = formMgr.getFromDocCache();
	String globalNavigationTab = userSession
			.getCurrentPrimaryNavigation();
	//BSL CR710 02/16/2012 Rel8.0 BEGIN - update for grouping
	String fromInvoiceGroupDetail = (String) session
			.getAttribute("fromInvoiceGroupDetail");
	String currentTab = (String) session
			.getAttribute("currentInvoiceMgmtTab");
	String formName = "ApplyPaymentDateToSelectionForm";

	if (!"GD".equals(fromInvoiceGroupDetail)
			&& TradePortalConstants.INV_GROUP_TAB.equals(currentTab)) {
		formName = "ApplyPaymentDateToGroupSelectionOnDetailForm";
	} else {
		formName = "ApplyPaymentDateToSelectionForm";
	}

	//BSL CR710 02/16/2012 Rel8.0 END
	String cancelLink = formMgr.getLinkAsUrl("goToInvoiceManagement",
			response);
	WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
	Hashtable secureParms = new Hashtable();
	secureParms.put("SecurityRights", userSession.getSecurityRights());
	secureParms.put("UserOid", userSession.getUserOid());
	secureParms.put("ownerOrg", userSession.getOwnerOrgOid());
	String selectedCount = StringFunction.xssCharsToHtml(request.getParameter("selectedCount"));
	String dialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
	if (InstrumentServices.isBlank(request
			.getParameter("selectedCount"))) {
		MediatorServices medService = new MediatorServices();
		medService.getErrorManager().setLocaleName(
				resMgr.getResourceLocale());
		medService.getErrorManager().issueError(
				TradePortalConstants.ERR_CAT_1,
				TradePortalConstants.NO_ITEM_SELECTED,
				resMgr.getText("UploadInvoices.AssignLoanType",
						TradePortalConstants.TEXT_BUNDLE));
		medService.addErrorInfo();
		doc.addComponent("/Error", medService.getErrorDoc());
		formMgr.storeInDocCache("default.doc", doc);
	}

	String selectedStatus = "";
	String loginLocale = null;
	Vector codesToExclude = null;
	codesToExclude = new Vector();
	codesToExclude = InvoiceUtility.getLoanTypesNotAllowed(userSession.getOwnerOrgOid());
	loginLocale = userSession.getUserLocale();
	StringBuffer loanTypeDropdown = new StringBuffer(
			Dropdown.createSortedRefDataOptions("LOAN_TYPE",
					selectedStatus, loginLocale, codesToExclude));
	if ("PLRG".equals(request.getParameter("fromPage"))) {
		codesToExclude.add(TradePortalConstants.EXPORT_FIN);
		codesToExclude.add(TradePortalConstants.INV_FIN_REC);
		loanTypeDropdown = new StringBuffer(
				Dropdown.createSortedRefDataOptions("LOAN_TYPE",
						selectedStatus, loginLocale, codesToExclude));
	} else {
		codesToExclude.add(TradePortalConstants.INV_FIN_BR);
		codesToExclude.add(TradePortalConstants.INV_FIN_BB);
		loanTypeDropdown = new StringBuffer(
				Dropdown.createSortedRefDataOptions("LOAN_TYPE",
						selectedStatus, loginLocale, codesToExclude));
	}
	StringBuffer loanBlank = new StringBuffer();	
		    loanBlank.append("<option value='").append("").append("'>");
            loanBlank.append("");
            loanBlank.append("</option>");
            loanBlank.append(loanTypeDropdown);
%>





<%-- check for security --%>


<div id="assignPaymentDateContent" class="invoiceDialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <%=formMgr.getFormInstanceAsInputField(formName, secureParms)%>

  <div class="formItem">
   <table border='0'>
   <tr>
	
	<td nowrap>
	<%=resMgr.getText("UploadInvoiceAction.LoanTypeReq",TradePortalConstants.TEXT_BUNDLE)%>
	</td>
	</tr>
	</td></tr>
	</table>	
	<br>
	<table border='0'>
	<tr>
	<td width='20px'></td>
	<td nowrap>
	<%=resMgr.getText("UploadInvoiceAction.LoanType",TradePortalConstants.TEXT_BUNDLE)%>
	</td>
	<td>
	<%
		out.print(widgetFactory.createSelectField("loanType", "", "",loanBlank.toString(), false, false, 
				false, "", "",""));
	%>
	</td>
	</tr>
	</table>
	
	</div>
  
  <br><br><br>

    <div class="formItem">
	
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("UploadInvoiceAction.Continue",
					TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			assignLoanType();     					
		</script>
	</button> 

	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button">
		<%=resMgr.getText("common.CloseText",
					TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			closeDialog();     					
		</script>
	</button> 

	
 </form>
</div>

<script type='text/javascript'>

function assignLoanType() {
require(["dijit/registry", "t360/dialog"],
        function(registry, dialog ) {    		
	 var userLoanType = registry.byId("loanType").value;
	 dialog.doCallback('<%=StringFunction.escapeQuotesforJS(dialogId)%>', 'select',userLoanType);

    });
}

function closeDialog()
{
    hideDialog("InvoiceGroupListDialogID");
}

 
</script>
</div>
</body>
</html>

<%
	/**********
	 * CLEAN UP
	 **********/

	// Finally, reset the /Error portion of the cached document 
	// to eliminate carryover of errors from one page to another
	// The rest of the data is not eliminated since it will be
	// use by the Route mediator and other transaction pages

	doc = formMgr.getFromDocCache();
	doc.setAttribute("/Error", "");

	formMgr.storeInDocCache("default.doc", doc);
%>

