<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*,java.util.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%

		
   String invoiceDateDay = null;
   String invoiceDateMonth = null;
   String invoiceDateYear = null; 
   String payment_date = null;
   DocumentHandler  doc = formMgr.getFromDocCache();  
   String globalNavigationTab = userSession.getCurrentPrimaryNavigation();
   String formName = "ApplyPaymentDateToSelectionOnDetailForm";
   //BSL CR710 02/16/2012 Rel8.0 BEGIN - update for grouping
   String fromInvoiceGroupDetail = (String)session.getAttribute("fromInvoiceGroupDetail");
   String currentTab = (String)session.getAttribute("currentInvoiceMgmtTab");
  if ("NGL".equals(fromInvoiceGroupDetail)){
	  formName = "ApplyPaymentDateToSelectionForm";
  }
  else if ("GD".equals(fromInvoiceGroupDetail)){
	  formName = "ApplyPaymentDateToSelectionOnDetailForm";
  }
  else if ("GL".equals(fromInvoiceGroupDetail)){
      formName = "ApplyPaymentDateToGroupSelectionOnDetailForm";
   }
   //BSL CR710 02/16/2012 Rel8.0 END
   String cancelLink=formMgr.getLinkAsUrl("goToInvoiceManagement", response); 
   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   Hashtable secureParms        = new Hashtable();
   secureParms.put("SecurityRights", userSession.getSecurityRights());
   secureParms.put("UserOid",        userSession.getUserOid());
   secureParms.put("ownerOrg",        userSession.getOwnerOrgOid());
   String selectedCount = StringFunction.xssCharsToHtml(request.getParameter("selectedCount"));
 //read in parameters
   String dialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
   if(InstrumentServices.isBlank(request.getParameter("selectedCount"))){
	   MediatorServices medService = new MediatorServices();
	   medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	   medService.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
			   TradePortalConstants.NO_ITEM_SELECTED,
			   resMgr.getText("UploadInvoiceAction.ApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE));
		medService.addErrorInfo();
		doc.addComponent("/Error", medService.getErrorDoc());
		formMgr.storeInDocCache("default.doc", doc);
	}
   String datePattern = userSession.getDatePattern();
   String dateWidgetOptions = "constraints:{datePattern:'"+datePattern+"'},placeHolder:'" + datePattern + "'"; 
%>

<%-- check for security --%>


<div id="assignPaymentDateContent" class="dialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
  <div class="formItem">
    <%= widgetFactory.createDateField("payment_date", "InvoiceUploadRequest.payment_date", "", false, false, false, "class='char10'",dateWidgetOptions, "")%>
  </div>
    <div class="formItem">
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("UploadInvoiceAction.UploadInvoiceApplyPaymentDateText",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			applyPaymentDate();     					
		</script>
	</button> 
 
	<button data-dojo-type="dijit.form.Button"  name="cancel" id="cancel" type="button">
		<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			cancelOne();     					
		</script>
	</button> 
 	  
	<%-- a href="javascript:closeDialog()"><%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %></a> --%>
 </form>
</div>

<script type='text/javascript'>

function applyPaymentDate() {
require(["dijit/registry", "t360/dialog","dojo/dom","dojo/domReady!" ],
        function(registry, dialog,dom ) {    		
	var payDate = dom.byId("payment_date").value;
	 dialog.doCallback('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(dialogId))%>', 'select',payDate);

    });
}

function closeDialog()
{
    hideDialog('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(dialogId))%>');
}
function cancelOne(){
    
    require(["t360/dialog"], 
                function(dialog) {
    dialog.hide('<%=StringFunction.escapeQuotesforJS(dialogId)%>');

});
    }
 
</script>
</div>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>

