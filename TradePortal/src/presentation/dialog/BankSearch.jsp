<%--
/******************************************************************************
* BankSearch.jsp
*
* Filtering:
* This page originally displays a listview containing all possible Bank/Branch Code and branch address for a given bank,
* but user may also choose to narrow down the results by searching by City, Province, Bank Name, or Branch Name 
 * which will filter the existing results and link that redisplays the page with the filter text passed as a url parameter.
* JavaScript is used to dynamically build that link.
*
* Selecting:
* JavaScript is used to detect which radio button is selected, and the value
* of that button is passed as a url parameter to page from which the search
* page was called.
******************************************************************************/
--%>
<%--
*
*     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                                                                com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                                                                com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
  Debug.debug("***START********************BANK*SEARCH**************************START***");


WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String returnAction     = null;
String onLoad           = "document.FilterForm.bankName.focus();";
String city                                = null;
String province                      = null;
String bankName                                 = null;
String branchName             = null;
String bankBranchCode   = null; //Vasavi CR 573 05/19/10 Add
String accCountry                 = "";
String bankType                   = "";
String itemid                                      = "";
String sectionName                        = "";
String accountOid 					= ""; 
String beneCountry					= ""; 

String paymentMethodCode    = "";
String paymentMethodType    = "";

StringBuffer where                          = new StringBuffer();
StringBuffer searchCriteria           = new StringBuffer();
StringBuffer sql                                                  = new StringBuffer();
StringBuffer countrySQL                                               = new StringBuffer();

Hashtable secureParams = new Hashtable();

//returnAction                  = doc.getAttribute("/In/BankSearchInfo/ReturnAction");
paymentMethodCode                  = StringFunction.xssCharsToHtml(request.getParameter("paymentMethodCode"));
//out.println("paymentMethodCode="+paymentMethodCode);
bankType                            = StringFunction.xssCharsToHtml(request.getParameter("bankType"));
//out.println("bankType="+bankType);
sectionName                     = StringFunction.xssCharsToHtml(request.getParameter("section"));
itemid                                   = StringFunction.xssCharsToHtml(request.getParameter("itemid"));
accountOid = StringFunction.xssCharsToHtml(request.getParameter("account_oid"));
beneCountry = StringFunction.xssCharsToHtml(request.getParameter("beneCountry"));
bankBranchCode = StringFunction.xssCharsToHtml(request.getParameter("bankBranchCode"));
sql.append("SELECT ADDL_VALUE FROM REFDATA WHERE TABLE_TYPE = ? "); 
sql.append(" AND CODE = ? ");

//Nar Rel9000 IR T36000026319 - SQL FIX
Object sqlParams[] = new Object[2];
sqlParams[0] = TradePortalConstants.PMT_METHOD;
sqlParams[1] = paymentMethodCode;

DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);
if (result != null)
                paymentMethodType = result.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");

if (accountOid != null && !accountOid.equals("")) {
	AccountWebBean account = beanMgr.createBean(AccountWebBean.class, "Account");
	//if (InstrumentType.DIRECT_DEBIT_INSTRUCTION.equals(paymentMethodCode)) {
                // out.println("account_oid="+StringFunction.xssCharsToHtml(request.getParameter("account_oid")));
                account.setAttribute("account_oid", StringFunction.xssCharsToHtml(accountOid));
                account.getDataFromAppServer();
                //out.println("bank_country_code="+account.getAttribute("bank_country_code"));
	//}

    
    countrySQL.append("SELECT ADDL_VALUE FROM REFDATA WHERE CODE = ? ");
    countrySQL.append(" AND TABLE_TYPE = ? ");
            	
    //Nar Rel9000 IR T36000026319 - SQL FIX
     Object sqlRefParam[] = new Object[2];
     sqlRefParam[0] = account.getAttribute("bank_country_code");
     sqlRefParam[1] = TradePortalConstants.COUNTRY_ISO3116_3;
            	   
     DocumentHandler countryDoc = DatabaseQueryBean.getXmlResultSet(countrySQL.toString(), false, sqlRefParam);
    
	if (countryDoc != null) {
                //out.println("countryDoc="+countryDoc.toString());
                accCountry = countryDoc.getAttribute("/ResultSetRecord(0)/ADDL_VALUE");   
	}
}
else {	
	accCountry = beneCountry;
}

%>

<div id="ExistingDirectDebitsSearchDialogContent" class="dialogContent fullwidth">      
<div>
<%= widgetFactory.createLabel("", "BankSearch.BankName", false, false, false, "inline") %>
<%= widgetFactory.createTextField("BankName", "", bankName, "120", false, false, false, "onKeydown='Javascript: filterBankSearchOnEnter(event,\"BankName\");' width=12 ", "", "inline") %>

<%= widgetFactory.createLabel("", "BankSearch.BankBranchCode", false, false, false, "inline") %>
<%= widgetFactory.createTextField("BankBranchCode", "", bankBranchCode, "16", false, false, false, "onKeydown='Javascript: filterBankSearchOnEnter(event,\"BankBranchCode\");' width=10 ", "", "inline") %>

 <%= widgetFactory.createLabel("", "BankSearch.BranchName", false, false, false, "inline") %>
<%= widgetFactory.createTextField("BranchName", "", branchName, "70", false, false, false, "onKeydown='Javascript: filterBankSearchOnEnter(event,\"BranchName\");' width=10 ", "", "inline") %>    
                                <div align="right">
				 <span class="searchActions">
               <button data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction" id="BanKSearchButton">
                                                <%=resMgr.getText("BankSearch.Filter",TradePortalConstants.TEXT_BUNDLE)%>
                                <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                                                searchBank();return false;
                </script>
                                </button>
                                </span>
        </div>    
       <div style="clear:both;"></div>
        <%=widgetFactory.createHoverHelp("BanKSearchButton", "SearchHoverText") %>
</div>
<div>
                <%= widgetFactory.createLabel("", "BankSearch.City", false, false, false, "inline") %>
                <%= widgetFactory.createTextField("City", "", city, "35", false, false, false, "onKeydown='Javascript: filterBankSearchOnEnter(event,\"City\");' width=10 ", "", "inline") %>
                
                 <%= widgetFactory.createLabel("", "BankSearch.Province", false, false, false, "inline") %>     
                 <%= widgetFactory.createTextField("Province", "", province, "16", false, false, false, "onKeydown='Javascript: filterBankSearchOnEnter(event,\"Province\");' width=10 ", "", "inline") %>

                <div style="clear:both;"></div>
                </div>

 <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("bankSearchGrid","BankSearchDataGrid",null);
  String gridLayout = dgFactory.createGridLayout("bankSearchGrid", "BankSearchDataGrid");  

%>
<div id="bankSearchGridDisplay">
<%= gridHtml %>
</div>
</div>
<script type="text/javascript">

  <%--get grid layout from the data grid factory--%>
  var gridLayout = <%= gridLayout %>;
  var isCreateCalled = false;
  <%-- Srinivasu_D IR#T36000018356 Rel8.2 06/19/2013 - BankBranch Code parms attached to Search --%>
  var initParm = "BankBranchCode=<%=StringFunction.escapeQuotesforJS(bankBranchCode)%>&bankType=<%=StringFunction.escapeQuotesforJS(bankType)%>&paymentMethodCode=<%=StringFunction.escapeQuotesforJS(paymentMethodCode)%>" +                "&accCountry=<%=StringFunction.escapeQuotesforJS(accCountry)%>" ;
                console.log("initSearchParms="+initParm);
  var searchParms;
  <%-- var div = document.getElementById('bankSearchGridDisplay') ; --%>
  <%-- div.hidden = true; --%>
  
  
   require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
	   var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BankSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>	  
	   var grid =   t360grid.createDataGrid("bankSearchGrid", viewName,gridLayout, initParm);
	   grid.set('autoHeight',10); 
  });
  
  
  function createBankGrid() {
	searchDataGrid("bankSearchGrid", "BankSearchDataView", searchParms);
	<%-- div.hidden = false; --%>
  }
  
  
  <%--provide a search function that collects the necessary search parameters--%>


  function chooseBank() {
                  require(["dojo/ready","dijit/registry","dojo/_base/array","dojo/query","dojo/domReady!","dojo/on","t360/dialog"], 

                                                                function(ready,registry,baseArray,query,dom, on, dialog) {
                                                console.log('chooseBank invoked');
                                <%--             ready(function(){ --%>
                                                console.log('inside ready');
                                                <%-- var myGrid = registry.byId("bankSearchGrid"); --%>
                                                
                                                items = registry.byId("bankSearchGrid").selection.getSelected();
                                                console.log('items='+items);
                                                if (items.length == 0){
                                                                alert('No Bank/Branch selected');
                                                                return;
                                                } else {
                                                var itemsObj = String("<%=StringFunction.escapeQuotesforJS(itemid)%>");
                                                var itemsArr = itemsObj.split(',');
                                                console.log('itemsObj='+itemsObj);

                                                <%--  get the data of each column of selected grid and set the data to fields in the form --%>
                                                var arr = [];
                                                <%-- "rowKey","Country","PaymentMethod","BankBranchCode","BankName","BranchName","BranchAdd1","BranchAdd2","City","Province"} --%>
                                                arr[0] = items[0].i.rowKey;
                                                arr[1] = items[0].i.Country;
                                                arr[2] = items[0].i.PaymentMethod;
                                                arr[3] = items[0].i.BankBranchCode;
                                                arr[4] = items[0].i.BankName;
                                                arr[5] = items[0].i.BranchName;
                                                arr[6] = items[0].i.BranchAdd1;
                                                arr[7] = items[0].i.BranchAdd2;
                                                arr[8] = items[0].i.City;
                                                arr[9] = items[0].i.Province;

                                                matchPartyPatternsByPageName(itemsArr, arr, section);
                                                dialog.hide('BankSearchDialog');
                                                }
                                                });
  }

  function filterBankSearchOnEnter(evt, fieldID){
  	<%--  require(["dojo/on","dijit/registry"],function(on, registry) {
  	    on(registry.byId(fieldID), "keypress", function(event) {
  	        if (event && event.keyCode == 13) {
  	        	dojo.stopEvent(event);
  	        	searchBank();
  	        }
  	    });
  	}); end require --%>
  	if(evt && evt.keyCode == 13){
  		searchBank();
  	}
  }<%-- end of filterBankSearchOnEnter() --%>

  
  function searchBank() {
      require(["dojo/dom"],
        function(dom){
    	  searchParms= initParm;
                 console.log('isCreateCalled='+isCreateCalled)
                                  
          var bankName = dom.byId("BankName").value;
          var bankBranchCode = dom.byId("BankBranchCode").value;
          var branchName = dom.byId("BranchName").value;
          var city = dom.byId("City").value;
          var province = dom.byId("Province").value;
          var performSearch = false;
          
          if (bankName != ""){
                  performSearch = true;
                  searchParms=searchParms+"&BankName="+bankName;}
          if (bankBranchCode != ""){
                  performSearch = true;
                  searchParms=searchParms+"&BankBranchCode="+bankBranchCode;}
          if (branchName != ""){
                  performSearch = true;
                  searchParms=searchParms+"&BranchName="+branchName;}
          if (city != ""){
                  performSearch = true;
                  searchParms=searchParms+"&City="+city;}
          if (province != ""){
                  performSearch = true;
                  searchParms=searchParms+"&Province="+province;}
         console.log('performSearch='+performSearch);
          if (performSearch == true){
          console.log("searchParams= "+searchParms);
          if (isCreateCalled == false) {
                                  this.createBankGrid();
                                  isCreateCalled = true;
                                  } else {
                                                  searchDataGrid("bankSearchGrid", "BankSearchDataView",
                          searchParms);
                                          <%-- div.hidden = false; --%>
                                  }
          }
        });
    }

  function cancelBankPopup() {
                  require(["t360/dialog"], function(dialog) {
                                  dialog.hide('BankSearchDialog');
                  });
                  
  }
  var dialogName=dijit.byId("BankSearchDialog");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","BankSearchDialogCloseButton");
  
  
</script>
<%=widgetFactory.createHoverHelp("BankSearchDialogCloseButton", "common.Cancel") %>
