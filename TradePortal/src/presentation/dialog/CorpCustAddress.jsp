<%--
**********************************************************************************
  Corporate Customer Address

  Description:
     Dialog for adding/updating addresses.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.common.cache.Cache,com.ams.tradeportal.common.cache.TPCacheManager" %> 

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%


  AddressWebBean address = null;
  boolean isReadOnly = false;

  String SeqNo = null;
  String Name = null;
  String AddressLine1 = null;
  String AddressLine2 = null;
  String StateProvince = null;
  String City = null;
  String PostalCode = null;
  String Country = null;
  String userDefinedField1 = null;
  String userDefinedField2 = null;
  String userDefinedField3 = null;
  String userDefinedField4 = null;
  boolean isUserDefinedFieldEnable = false;

  String dialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
  String mode = StringFunction.xssCharsToHtml(request.getParameter("mode"));
  String rowKey = null;
  String divId = null;

  String ownershipLevel = userSession.getOwnershipLevel();
  String userSecurityRights = userSession.getSecurityRights();
  String userLocale         = userSession.getUserLocale();
  

  if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
    isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_CB);
  }
  else {
    isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_CORP_BY_BG);
  }
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  String adminUserDualCtrlReqdInd = CBCResult.getAttribute("/ResultSetRecord(0)/CORP_ORG_DUAL_CTRL_REQD_IND");
  String utilizedAddlField = CBCResult.getAttribute("/ResultSetRecord(0)/UTILIZE_ADDITIONAL_BANK_FIELDS");
  if (TradePortalConstants.INDICATOR_YES.equals(utilizedAddlField)) {
	   isUserDefinedFieldEnable = true;
  }

  //rowKey=EncryptDecrypt.decryptStringUsingTripleDes(StringFunction.xssCharsToHtml(request.getParameter("rowKey")), userSession.getSecretKey());
  if ( "update".equals(mode) ) {
    rowKey=StringFunction.xssCharsToHtml(request.getParameter("rowKey"));
    divId=StringFunction.xssCharsToHtml(request.getParameter("divId"));
    SeqNo=StringFunction.xssCharsToHtml(request.getParameter("SeqNo"));
    Name=StringFunction.xssCharsToHtml(request.getParameter("Name"));
    AddressLine1=StringFunction.xssCharsToHtml(request.getParameter("AddressLine1"));
    AddressLine2=StringFunction.xssCharsToHtml(request.getParameter("AddressLine2"));
    City=StringFunction.xssCharsToHtml(request.getParameter("City"));
    StateProvince=StringFunction.xssCharsToHtml(request.getParameter("StateProvince"));
    PostalCode=StringFunction.xssCharsToHtml(request.getParameter("PostalCode"));
    Country=StringFunction.xssCharsToHtml(request.getParameter("Country"));
    if ( isUserDefinedFieldEnable ) { 
    	userDefinedField1=StringFunction.xssCharsToHtml(request.getParameter("userDefinedField1"));
    	userDefinedField2=StringFunction.xssCharsToHtml(request.getParameter("userDefinedField2"));
    	userDefinedField3=StringFunction.xssCharsToHtml(request.getParameter("userDefinedField3"));
    	userDefinedField4=StringFunction.xssCharsToHtml(request.getParameter("userDefinedField4"));
    }
  }
 
  // Register an address web bean
  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.AddressWebBean", "Address");
  address = (AddressWebBean) beanMgr.getBean("Address");
  if ( "update".equals(mode) ) {
    address.setAttribute("name",Name);
    address.setAttribute("address_line_1",AddressLine1);
    address.setAttribute("address_line_2",AddressLine2);
    address.setAttribute("city",City);
    address.setAttribute("country",Country);
    address.setAttribute("state_province",StateProvince);
    address.setAttribute("postal_code",PostalCode);
    address.setAttribute("address_seq_num",SeqNo);
    if ( isUserDefinedFieldEnable ) { 
    	address.setAttribute("user_defined_field_1",userDefinedField1);
    	address.setAttribute("user_defined_field_2",userDefinedField2);
    	address.setAttribute("user_defined_field_3",userDefinedField3);
    	address.setAttribute("user_defined_field_4",userDefinedField4);
    }
    
  }

  WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
%>

<%-- ********************* HTML for page begins here *********************  --%>


<div class="dialogContent midwidth">
  <form id="AddressForm" data-dojo-type="dijit.form.Form" name="AddressForm">
    <input type=hidden value="" name=buttonName>
    <div class="formContent">
  
      <div class="columnLeft">
	          
        <%= widgetFactory.createTextField( "SecName", "CorpCust.AddressName", Name, "35", isReadOnly,true) %>
        <%= widgetFactory.createTextField( "SecAddressLine1", "CorpCust.AddressLine1", AddressLine1 , "35", isReadOnly,true) %>                             
        <%= widgetFactory.createTextField( "SecAddressLine2", "CorpCust.AddressLine2", AddressLine2 , "35", isReadOnly) %>
        <div style="clear: both;"></div>                              

      </div>
   
      <div class="columnRight">         
        <%= widgetFactory.createTextField( "SecCity", "CorpCust.City", City, "23", isReadOnly,true) %> 
	           	
        <%= widgetFactory.createTextField( "SecStateProvince", "CorpCust.ProvinceState", StateProvince , "8", isReadOnly,false,false,"","","inline") %>
        <%= widgetFactory.createTextField( "SecPostalCode", "CorpCust.PostalCode", PostalCode, "8", isReadOnly,false,false,"","","inline") %>
	           		
        <%= widgetFactory.createNumberField( "SecAddressSeqNum", "CorpCust.Sequence", SeqNo, "4", isReadOnly, true, false,"","","inline") %>
        <div style="clear:both;"></div>
	      	  	
<%
  //only create the default text if country is not yet selected
  String defaultText = "";
  String countryWidgetProps = "placeHolder:'" +
    resMgr.getText("CorpCust.SelectACountry", TradePortalConstants.TEXT_BUNDLE) + "'";
  if ( Country==null || Country.length()<=0 ) {
    countryWidgetProps += ", value:''"; //set initial value to blank
  }

  String dropdownOptions = Dropdown.createSortedRefDataOptions("COUNTRY", 
    Country, userLocale);
%>

        <%= widgetFactory.createSelectField( "SecCountry", "CorpCust.Country", defaultText , 
              dropdownOptions, isReadOnly,true,false,"class='char25'",countryWidgetProps,"")%>

      </div>
      <div style="clear:both;"></div>
       <%-- Nar Rel9500 CR-1132 01/27/2016 Add- Begin --%>       
        <% if ( isUserDefinedFieldEnable ) { %>
            <%=widgetFactory.createWideSubsectionHeader("CorpCust.PortalOnlyAdditionalFields")%>
            <div class="columnLeft">
                <%= widgetFactory.createTextField( "SecUserDefinedField1", "CorpCust.UserDefinedField1", userDefinedField1, "20", isReadOnly, false, false, "class='char20'","","")%> 
                <%= widgetFactory.createTextField( "SecUserDefinedField2", "CorpCust.UserDefinedField2", userDefinedField2, "20", isReadOnly, false, false, "class='char20'","","")%>   
            </div>           
             <div class="columnRight">
                <%= widgetFactory.createTextField( "SecUserDefinedField3", "CorpCust.UserDefinedField3", userDefinedField3, "20", isReadOnly, false, false, "class='char20'","","")%>  
                <%= widgetFactory.createTextField( "SecUserDefinedField4", "CorpCust.UserDefinedField4", userDefinedField4, "20", isReadOnly, false, false, "class='char20'","","")%>  
             </div>           
             <div style="clear: both"></div>
         <% } %>
      <%-- Nar Rel9500 CR-1132 01/27/2016 Add- End --%>          
      <div class="dialogFooter">
        <span class="dialogFooter-right">
          <button data-dojo-type="dijit.form.Button" type="button" id="SaveAddress" name="SaveAddress" onclick="saveAddress();">
            <%=resMgr.getText("CorpCust.SaveAddress",TradePortalConstants.TEXT_BUNDLE)%>
          </button>  
          <%=widgetFactory.createHoverHelp("SaveAddress","CorpCustAddress.Save") %>
          <button data-dojo-type="dijit.form.Button" type="button" id="CancelSaveAddress" name="CancelSaveAddress" onclick="closeAddressDialog();">
            <%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE)%>
          </button>  
          <%=widgetFactory.createHoverHelp("CancelSaveAddress","CorpCustAddress.Cancel") %>
        </span>
        <div style="clear:both;"></div>
      </div>
    </div>

  </form>
</div> 

<%
  // Unregister the Corporate Org web bean that was used in this page
  beanMgr.unregisterBean("Address");

  // Reset the cached document 
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<script type="text/javascript">
var reqFields = false;
  function saveAddress() {
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/ready"],
        function(registry, query, on, dialog,ready ) {
    <%--   ready(function() { --%>
        <%-- var formNode=registry.byId("AddressForm"); --%>
        <%-- registry.findWidgets(formNode) --%>
        <%-- todo:Replace this code with form findwidgets array code --%>
	    	
        var secSeqNo=registry.byId("SecAddressSeqNum").value;
        var secName=registry.byId("SecName").value;
        var secAddressLine1=registry.byId("SecAddressLine1").value;
        var secAddressLine2=registry.byId("SecAddressLine2").value;
        var secStateProvince=registry.byId("SecStateProvince").value;
        var secPostalCode=registry.byId("SecPostalCode").value;
        var secCountry=registry.byId("SecCountry").value;
        var secCity=registry.byId("SecCity").value;
        if(secName == null || secName=="" || secAddressLine1 == null || secAddressLine1=="" || secCountry == null || secCountry=="" || secCity == null || secCity =="" || secSeqNo == null || secSeqNo ==""){
        	reqFields= true;
        	alert('<%=resMgr.getText(
					"AllTransaction.ReqFields",
					TradePortalConstants.TEXT_BUNDLE)%>');
        	return false;
        }
        else{
        	reqFields= false;
        }
        
        var SecUserDefinedField1 = null;
        var SecUserDefinedField2 = null;
        var SecUserDefinedField3 = null;
        var SecUserDefinedField4 = null;        
        <% if ( isUserDefinedFieldEnable ) { %>
             SecUserDefinedField1=registry.byId("SecUserDefinedField1").value;
             SecUserDefinedField2=registry.byId("SecUserDefinedField2").value;
             SecUserDefinedField3=registry.byId("SecUserDefinedField3").value;
             SecUserDefinedField4=registry.byId("SecUserDefinedField4").value;
        <% } %>

<%
  if ("update".equals(mode)) {      
%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(dialogId)%>', 'updateAddress',
          ['SecAddressOid','SecAddressLine1','SecAddressLine2','SecCountry','SecPostalCode','SecAddressSeqNum','SecStateProvince','SecName','SecCity','SecOwnershipLevel','divId','SecUserDefinedField1','SecUserDefinedField2','SecUserDefinedField3','SecUserDefinedField4'],
          ['<%=StringFunction.escapeQuotesforJS(rowKey)%>',secAddressLine1,secAddressLine2,secCountry,secPostalCode,secSeqNo,secStateProvince,secName,secCity,'<%=ownershipLevel%>','<%=StringFunction.escapeQuotesforJS(divId)%>',SecUserDefinedField1,SecUserDefinedField2,SecUserDefinedField3,SecUserDefinedField4]);
<%
  }
  else {
%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(dialogId)%>', 'addAddress',
          ['SecAddressOid','SecAddressLine1','SecAddressLine2','SecCountry','SecPostalCode','SecAddressSeqNum','SecStateProvince','SecName','SecCity','SecOwnershipLevel','SecUserDefinedField1','SecUserDefinedField2','SecUserDefinedField3','SecUserDefinedField4'],
          [null,secAddressLine1,secAddressLine2,secCountry,secPostalCode,secSeqNo,secStateProvince,secName,secCity,'<%=ownershipLevel%>',SecUserDefinedField1,SecUserDefinedField2,SecUserDefinedField3,SecUserDefinedField4]);
<%
  }
%>
 <%-- }); --%>
    });
if(!reqFields)
	closeAddressDialog();  
  }

  function closeAddressDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(dialogId)%>');
    });
  }

</script>
