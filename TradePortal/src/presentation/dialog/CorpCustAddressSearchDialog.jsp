<%--
 *  CorpCustomerAddress Search Dialog content
 *  
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%


String corpCustAddressSearchDialogId = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(request.getParameter("dialogId")));
String corp_org_oid = StringFunction.xssCharsToHtml(request.getParameter("corp_org_oid"));
String gridHtml="";
String gridLayout = "";
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,response); 
%>

<%-- ********************* HTML for page begins here *********************  --%>
<div id="corpCustAddressSearchDialogContent" class="dialogContent fullwidth">


<%  
  gridHtml = dgFactory.createDataGrid("corpCustAddressSearchDialogID","CorpCustAddressSearchDataGrid",null);
  gridLayout = dgFactory.createGridLayout("CorpCustAddressSearchDataGrid");
%>
<%= gridHtml %>
 

</div>
<script type='text/javascript'>
  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
    var gridLayout = <%=gridLayout%>;   
    var initSearchParms = "corp_org_oid=<%=StringFunction.escapeQuotesforJS(corp_org_oid)%>";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("CorpCustAddressSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    t360grid.createDataGrid("corpCustAddressSearchDialogID", viewName,gridLayout, initSearchParms);
  });
</script>
<script type='text/javascript'>
  function chooseSelectedAddress(){
	  require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		  			var addressOid = getSelectedGridRowKeys("corpCustAddressSearchDialogID");
		  			if(getSelectedGridRowKeys("corpCustAddressSearchDialogID").length == 0){
		  				alert('<%=resMgr.getText("AddressSearch.SelectBlankWarning",TradePortalConstants.TEXT_BUNDLE)%>');
		  			}else{
		  				dialog.doCallback('<%=corpCustAddressSearchDialogId%>', 'setAddress',addressOid,"");
		  			}
		  			 
	  });
  }
  
  function choosePrimaryAddress(){
	  require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {
		  			var addressOid = getSelectedGridRowKeys("corpCustAddressSearchDialogID");
		  			dialog.doCallback('<%=corpCustAddressSearchDialogId%>', 'setAddress',addressOid,"PRIMARY"); 
	  });
  }
  
   function closeCCDialog() {
    require(["t360/dialog"],
        function(dialog ) {
      dialog.hide('<%=corpCustAddressSearchDialogId%>');
    });
  }
</script>
