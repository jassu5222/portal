<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,com.amsinc.ecsg.html.*,com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>



<%


  String userOid = userSession.getUserOid();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  Hashtable<String, String> secureParams = new Hashtable<String, String>();
  secureParams.put("user_oid", userOid);
  secureParams.put("login_oid", userOid);
  secureParams.put("login_rights", userSession.getSecurityRights());
  secureParams.put("ownership_level", userSession.getOwnershipLevel());
  secureParams.put("ownership_type",TradePortalConstants.OWNER_TYPE_NON_ADMIN);
  secureParams.put("owner_oid", userSession.getOwnerOrgOid());
  secureParams.put("favoriteType", TradePortalConstants.FAV_TYPE_TASK);

  String focusField = "";

  //get the user's Favorite task
  //if no favorite task saved, then go with the default

  //determine current user preferences
  List<String> favoriteOids = new ArrayList<String>();
  List<String> favoriteIds = new ArrayList<String>();
  List<String> favoriteOptLocks = new ArrayList<String>();
  
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
  String favSql = " select favorite_task_cust_oid, favorite_id, opt_lock from favorite_task_customization "
  	+"where a_user_oid = ?  and favorite_type = ? order by display_order"; 
	//Srinivasu_D T36000034729 Rel9.1 11/19/2014 - sql injection order issue
	List paramList = new ArrayList();
	paramList.add(userOid);
	paramList.add(TradePortalConstants.FAV_TYPE_TASK);

  DocumentHandler favListDoc = DatabaseQueryBean.getXmlResultSet(
    favSql.toString(), false, paramList);
  if (favListDoc != null) {
    Vector favList = favListDoc.getFragments("/ResultSetRecord");
    for (int i = 0; i < favList.size(); i++) {
      DocumentHandler favDoc = (DocumentHandler) favList.get(i);      
      String favoriteOidAttr = favDoc
        .getAttribute("/FAVORITE_TASK_CUST_OID");
      favoriteOids.add(favoriteOidAttr);
      String favoriteIdAttr = favDoc.getAttribute("/FAVORITE_ID");
      favoriteIds.add(favoriteIdAttr);
      String optLockAttr = favDoc.getAttribute("/OPT_LOCK");
      favoriteOptLocks.add(optLockAttr);
    }
  }

  //cquinton 12/11/2012 just by entereing this dialog, we clear the favorite tasks in the user session
  // so the menu will be regenerated on page transition
  userSession.setFavoriteTasks(null);

  // a variable to determine whether section should be displayed
  boolean displayTask = false;

  //cquinton 12/11/2012 - this dialog requires some javascript to execute to finish rendering
  // extend the 'Loading...' message during that time. dialogLoading class is removed when complete.
%>

<div id="favTaskDialogMoreLoading" class='dijitContentPaneLoading'>
  <span class='dijitInline dijitIconLoading'></span>
  <%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE)%>
</div>
<div id="favTaskDialogContent" class="dialogContent itemSelectWidth dialogLoading">

  <form name="Favorite-Task-Customization" id="Favorite-Task-Customization" method="POST" 
        data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
    <input type=hidden value="Save" name="buttonName" />

    <span class="customizationColumn1">
      <div class="instruction">
        <%=resMgr.getText("FavoriteTask.Step1", TradePortalConstants.TEXT_BUNDLE)%>
      </div>  
      <div class="customizationItemsHeader">
        <%=resMgr.getText("FavoriteTask.Task", TradePortalConstants.TEXT_BUNDLE)%>
      </div>


<%  
  //***************************************************    MESSAGES SECTION  ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
%>
 
      <div data-dojo-type="dijit.TitlePane" id="MessageTasks" class="availItemsSection"
           title='<%=resMgr.getText("FavoriteTask.Messages", TradePortalConstants.TEXT_BUNDLE)%>'>
                 
        <div id="<%=TradePortalConstants.FAV_TASK__CREATE_MAIL%>_avail" class="availItem">
          <span class="availItemSelect">               
<%
  displayTask = false;
  if (favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_MAIL)) {
    displayTask = true;
  }
%>
            <%=widgetFactory.createCheckboxField(
                 TradePortalConstants.FAV_TASK__CREATE_MAIL + "_checkbox",
                 "", displayTask, false, false, "", "htmlOnly", "none")%>
          </span>
          <span class="availItemDesc">
            <%=resMgr.getText("FavoriteTask.CreateMailMessage",
                 TradePortalConstants.TEXT_BUNDLE)%>
          </span>
        </div>

        <div id="<%=TradePortalConstants.FAV_TASK__VIEW_NOTIFS%>_avail" class="availItem">
          <span class="availItemSelect">
<%
  displayTask = false;
  if (favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_NOTIFS)) {
    displayTask = true;
  }
%>
          <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_NOTIFS + "_checkbox", 
            "", displayTask, false, false, "", "htmlOnly", "none")%>
        </span>
        <span class="availItemDesc">
          <%=resMgr.getText("FavoriteTask.ViewNotification",
               TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>
    </div>

<%
  }
  //***************************************************   NEW INSTRUMENTS SECTION    ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INSTRUMENTS ) ) {
%>
            
    <div data-dojo-type="dijit.TitlePane" id="NewInstrumentTasks" class="availItemsSection"
         title='<%=resMgr.getText("FavoriteTask.NewInstruments", TradePortalConstants.TEXT_BUNDLE)%>'>
              
<%
    //#################################    NEW INSTRUMENTS SECTION- Trade Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRADE_INSTRUMENTS ) ) {
%>
      <div class="availItemSubHeader">
        <span id="box1" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box1', 'instrument_trade_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.NewInstruments.Trade", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

      <div id="instrument_trade_subSection" style="display:none;"> <%--start out closed--%>
              <div id="<%=TradePortalConstants.FAV_TASK__COPY_TRADE%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__COPY_TRADE)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__COPY_TRADE
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CopyFromExistingTrade",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div> 
              
<%
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSFER_EXPORT_COLL ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.TransferExportLC",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>     
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_AIR_WAYBILL ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateAirWaybill",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_APPROVAL_TO_PAY ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateApprovalToPay",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>   
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_SEND_COLL ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateDirectSendCollection",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>          

<%
      }
      //cquinton 2/25/2013 add new export collection
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_EXPORT_COLL ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateExportCollection",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>          
                    
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DLC ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateImportLC",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>  
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_LOAN_RQST ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateLoanRequest",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>         
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_GUAR ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateOutgoingGuarantee",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>    
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC_DET ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateOutgoingStandbyLCDetailed",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>      
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateOutgoingStandbyLCSimple",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>  
                         
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_REQUEST_ADVISE ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateRequestToAdvise",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>  
              
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SHIP_GUAR ) ) {
%>
              <div id="<%=TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR%>_avail" class="availItem">
                  <span class="availItemSelect">
          <%
            displayTask = false;
            if (favoriteIds
                .contains(TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR)) {
              displayTask = true;
            }
          %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateShippingGuarantee",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
              </div>                          
<%
      }
      //cquinton 2/25/2013 display int payment in trade if its the only cash management
      // instrument.  Note: this must be mutually exclusive of int payment in payment area!
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INT_PAYMENT_TRADE ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__CREATE_INT_PMT%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__CREATE_INT_PMT)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_INT_PMT
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateInternationalPayment",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
<%
      }
%>
            </div>   
<%
    }

    //##################################    NEW INSTRUMENTS SECTION- Payment Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box2" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box2', 'instrument_payment_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">
          <%=resMgr.getText("FavoriteTask.NewInstruments.Payment", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

      <div id= "instrument_payment_subSection" style="display:none;"> <%--start out closed--%>
                <div id="<%=TradePortalConstants.FAV_TASK__COPY_PAYMENT%>_avail" class="availItem">
                  <span class="availItemSelect">               
  <%
                   displayTask = false;
                   if (favoriteIds
                       .contains(TradePortalConstants.FAV_TASK__COPY_PAYMENT)) {
                     displayTask = true;
                   }
                 %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__COPY_PAYMENT
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CopyFromExistingPayment",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
<%
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRANSFER_BTW_ACCTS ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateTransferBetweenAccounts",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DOMESTIC_PAYMENT ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__CREATE_PMT%>_avail" class="availItem">
                  <span class="availItemSelect">               
            <%
                     displayTask = false;
                     if (favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_PMT)) {
                       displayTask = true;
                     }
                   %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_PMT + "_checkbox",
            "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreatePayment",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
<%
      }
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INT_PAYMENT ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__CREATE_INT_PMT%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__CREATE_INT_PMT)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__CREATE_INT_PMT
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.CreateInternationalPayment",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
<%
      }
%>
               </div> 
<%
    }

    //##################################    NEW INSTRUMENTS SECTION- Direct Debit Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box3" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box3', 'instrument_directDebit_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.NewInstruments.DirectDebits", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

        <div id="instrument_directDebit_subSection" style="display:none;"> <%--start out closed--%>
                  <div id="<%=TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT%>_avail" class="availItem">
                      <span class="availItemSelect">               
              <%
                       displayTask = false;
                       if (favoriteIds
                           .contains(TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT)) {
                         displayTask = true;
                       }
                     %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.CopyFromExistingDirectDebit",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                  </div>  
                  
<%
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DDI ) ) {
%>
                  <div id="<%=TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT%>_avail" class="availItem">
                      <span class="availItemSelect">               
              <%
                       displayTask = false;
                       if (favoriteIds
                           .contains(TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT)) {
                         displayTask = true;
                       }
                     %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.CreateDirectDebit",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                  </div>                        
<%
      }
%>
        </div>  
<%
    }
%>
      </div>            
<%
  }

  //***************************************************   TRANSACTIONS SECTION  ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
%>
       <div data-dojo-type="dijit.TitlePane" id="TransactionTasks" class="availItemsSection"
             title='<%=resMgr.getText("FavoriteTask.Transactions",
          TradePortalConstants.TEXT_BUNDLE)%>'>

<%
    //##################################    TRANSACTIONS SECTION- Trade Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box4" class="expandSectionDialog">
          <a href="#" onclick="toggleSubSection('box4', 'transaction_trade_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.Transactions.Trade", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

            <div id="transaction_trade_subSection" style="display:none;"> <%--start out closed--%>
                  <div id="<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS%>_avail" class="availItem">
                    <span class="availItemSelect">
              <%
                displayTask = false;
                if (favoriteIds
                    .contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS)) {
                  displayTask = true;
                }
              %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS
                  + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.ViewPendingTradeTransactions",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>
                  <div id="<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS%>_avail" class="availItem">
                    <span class="availItemSelect">               
            <%
                     displayTask = false;
                     if (favoriteIds
                         .contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS)) {
                       displayTask = true;
                     }
                   %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS
                  + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.ViewAuthorizedTradeTransactions",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>
                  <div id="<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY%>_avail" class="availItem">
                    <span class="availItemSelect">
              <%
                displayTask = false;
                if (favoriteIds
                    .contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY)) {
                  displayTask = true;
                }
              %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY
                  + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.ViewTradeHistory",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>
                  <div id="<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT%>_avail" class="availItem">
                    <span class="availItemSelect">
              <%
                displayTask = false;
                if (favoriteIds
                    .contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT)) {
                  displayTask = true;
                }
              %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT + "_checkbox",
              "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.CreateAmendment",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>
                  
                  <div id="<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER%>_avail" class="availItem">
                    <span class="availItemSelect">
              <%
                displayTask = false;
                if (favoriteIds
                    .contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER)) {
                  displayTask = true;
                }
              %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER + "_checkbox",
              "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.CreateTracer",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>  
                  
                  <div id="<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT%>_avail" class="availItem">
                    <span class="availItemSelect">
              <%
                displayTask = false;
                if (favoriteIds
                    .contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT)) {
                  displayTask = true;
                }
              %>
                      <%=widgetFactory.createCheckboxField(
              TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT + "_checkbox",
              "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                    </span>
                    <span class="availItemDesc">
                      <%=resMgr.getText("FavoriteTask.CreateAssignment",
              TradePortalConstants.TEXT_BUNDLE)%>
                    </span>
                  </div>                                        
                 </div> 
             
<%
    }

    //##################################    TRANSACTIONS SECTION- Payment Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box5" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box5', 'transaction_payment_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.Transactions.Payment", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

              <div id="transaction_payment_subSection" style="display:none;"> <%--start out closed--%>
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewPendingPaymentTransactions",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewFutureValuePaymentTransactions",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>              
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewAuthorizedPaymentTransactions",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>  
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewPaymentHistory",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>                           
               </div> 
         
<%
    }

    //##################################    TRANSACTIONS SECTION- Direct Debit Sub section     ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DIRECT_DEBIT_TRANSACTIONS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box6" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box6', 'transaction_directDebit_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.Transactions.DirectDebits", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

              <div id="transaction_directDebit_subSection" style="display:none;"> <%--start out closed--%>
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewPendingDirectDebitTransactions",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>  
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewDirectDebitHistory",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>                                        
               </div> 
          
<%
    }

    //##################################  TRANSACTIONS SECTION- Receivables Sub Section   ####################################
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
%>

      <div class="availItemSubHeader">
        <span id="box7" class="expandSectionDialog">          
          <a href="#" onclick="toggleSubSection('box7', 'transaction_receivables_subSection')"></a>
        </span>
        <span class="availItemSubHeaderTitle">             
          <%=resMgr.getText("FavoriteTask.Transactions.Receivables", TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>

              <div id="transaction_receivables_subSection" style="display:none;"> <%--start out closed--%>
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewReceivablesMatching",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewInvoices",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div> 
                    
                    <div id="<%=TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY%>_avail" class="availItem">
                      <span class="availItemSelect">
                <%
                  displayTask = false;
                  if (favoriteIds
                      .contains(TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY)) {
                    displayTask = true;
                  }
                %>
                        <%=widgetFactory.createCheckboxField(
                TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY
                    + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                      </span>
                      <span class="availItemDesc">
                        <%=resMgr.getText("FavoriteTask.ViewReceivablesHistory",
                TradePortalConstants.TEXT_BUNDLE)%>
                      </span>
                    </div>                                          
               </div>   
<%
    }
%>
           </div>                
<%
  }

  //***************************************************   ACCOUNTS SECTION  ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNTS ) ) {
%>

    <div data-dojo-type="dijit.TitlePane" id="AccountTasks" class="availItemsSection"
         title='<%=resMgr.getText("FavoriteTask.Accounts", TradePortalConstants.TEXT_BUNDLE)%>'>   
<%
    //cquinton 2/25/2013 add display account balances security
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNT_BALANCES ) ) {
%>
      <div id="<%=TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES%>_avail" class="availItem">
        <span class="availItemSelect">
<%
  displayTask = false;
  if (favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES)) {
    displayTask = true;
  }
%>
          <%=widgetFactory.createCheckboxField(
               TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES + "_checkbox", 
               "", displayTask, false, false,
            "", "htmlOnly", "none")%>
        </span>
        <span class="availItemDesc">
          <%=resMgr.getText("FavoriteTask.ViewAccountBalances",
               TradePortalConstants.TEXT_BUNDLE)%>
        </span>
      </div>   
<%
    }
%>
    </div>  
<%
  }

  //***************************************************   REFERENCE DATA SECTION  ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_REFDATA ) ) {
%>

         <div data-dojo-type="dijit.TitlePane" id="RefDataTasks" class="availItemsSection"
            title='<%=resMgr.getText("FavoriteTask.RefData",
                          TradePortalConstants.TEXT_BUNDLE)%>'>   
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PARTY ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PARTIES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_PARTIES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_PARTIES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewParties",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>   
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PHRASE ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PHRASES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_PHRASES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_PHRASES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewPhrases",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>   
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_USERS ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_USERS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_USERS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_USERS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewUsers",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>     
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_THRESH_GRP ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewThresholdGroups",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>        
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_SEC_PROF ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewSecurityProfiles",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>        
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
        condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewPODefinitions",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>                                                      
  
<%
    }
    //cquinton 2/25/2013 add invoice definition
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INV_UPLOAD_DEFN ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewInvDefinitions",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_CREATE_RULE ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewCreationRules",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLES_MATCH_RULES ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewReceivablesMatchingRules",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_WORK_GROUPS ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewWorkGroups",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>       
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTH_GRP ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewPanelAuthorizationGroups",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>   
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TEMPLATE_GRP ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewTemplateGroups",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>  
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEMPLATE ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_TEMPLATES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_TEMPLATES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_TEMPLATES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewTemplates",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>  
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_FX_RATE ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__VIEW_FX_RATES%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__VIEW_FX_RATES)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__VIEW_FX_RATES
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.ViewForeignExchangeRates",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>                                                                         
<%
    }
%>
       </div>          
<%
  }

  //***************************************************   UPLOAD CENTER SECTION  ****************************************************
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_UPLOAD_CENTER ) ) {
%>

         <div data-dojo-type="dijit.TitlePane" id="UploadCenterTasks" class="availItemsSection"
            title='<%=resMgr.getText("FavoriteTask.UploadCenter",
                          TradePortalConstants.TEXT_BUNDLE)%>'> 
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
        condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.PurchaseOrderUpload",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
                </div>  
                
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_MANAGEMENT ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.InvoiceFileUpload",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
               </div>                                                                    
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_FILE_UPLOAD ) ) {
%>
                <div id="<%=TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE%>_avail" class="availItem">
                  <span class="availItemSelect">
            <%
              displayTask = false;
              if (favoriteIds
                  .contains(TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE)) {
                displayTask = true;
              }
            %>
                    <%=widgetFactory.createCheckboxField(
            TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE
                + "_checkbox", "", displayTask, false, false,
            "", "htmlOnly", "none")%>
                  </span>
                  <span class="availItemDesc">
                    <%=resMgr.getText("FavoriteTask.PaymentFileUpload",
            TradePortalConstants.TEXT_BUNDLE)%>
                  </span>
               </div>                                                                    
<%
    }
%>
      </div>                                                     
<%
  }
%>
                             
    </span>        
    <span class="customizationColumnDivider"></span>
    <span class="customizationColumn2">
      <div class="instruction">
        <%=resMgr.getText("FavoriteTask.Step2",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>
      <div class="customizationItemsHeader">
        <%=resMgr.getText("FavoriteTask.SelectedTasks",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>
  
      <div data-dojo-type="dijit.TitlePane" id="SelectedTaskItems" class="selectedItemsSection" 
           title='<%=resMgr.getText("FavoriteTask.Order",
                                    TradePortalConstants.TEXT_BUNDLE)%>'>
        <%--a dom element that is direct parent of individual selected items, so we can easily access--%>
        <div id="favselectedItems">
<%
  String displayKey = "";
  int selectedItemIdx = 0;
  int i = 0;
  for (i = 0; i < favoriteIds.size(); i++) {
    String favId = favoriteIds.get(i);
    //cquinton 2/25/2013 add security on the list of selected favorites
    if ( FavoriteUtility.shouldDisplayTask( favId, condDisplay ) ) {
      displayKey = FavoriteUtility.getTaskDisplayKey(favId);

      //only display if we recognize (this filters out
      // old items and bad data)
      if ( displayKey != null && displayKey.length()>0 ) {

        String favOid = "0"; //default to new
        String favOptLock = "";
        if (favoriteIds.size() == favoriteOids.size()) { //these are not default sections
          favOid = favoriteOids.get(i);
          favOptLock = favoriteOptLocks.get(i);
        }
%>
        <%--use the sectionId as the main id for easy access on select/unselect--%>
        <div class="selectedItem">
        <%-- VishalS Cross Site Scripting encoding for favOid --%>
          <input id="favOid<%=selectedItemIdx%>" name="favOid<%=selectedItemIdx%>" type="hidden" value="<%=StringFunction.xssCharsToHtml(favOid)%>" />
          <span class="selectedItemOrder">
            <%--cquinton 12/11/2012 use a differnet id as dashboard prefs uses the same. note retain the "name" though for submission--%>
            <%--cquinton 12/20/2012 just do html here, do dijit creation later--%>
            <input id="fav_order<%=selectedItemIdx%>" name="fav_order<%=selectedItemIdx%>"
                   maxLength="3" class="selectedItemOrderTextBox" value="<%=selectedItemIdx+1%>"  />                   
          </span>
          <%-- VishalS Cross Site Scripting encoding for favId --%>
          <input id="favId<%=selectedItemIdx%>" name="favId<%=selectedItemIdx%>" class="selectedItemId" type="hidden" value="<%=StringFunction.xssCharsToHtml(favId)%>" />
          <span id="favTaskDesc<%=selectedItemIdx%>" class="selectedItemDesc">                    
            <%=resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE)%>
          </span>
          <%-- VishalS Cross Site Scripting encoding for favOptLock --%>
          <input id="favOptLock<%=selectedItemIdx%>" name="favOptLock<%=selectedItemIdx%>" type="hidden" value="<%=StringFunction.xssCharsToHtml(favOptLock)%>" />
          <span class="deleteSelectedItem"></span>
          <div style="clear:both;"></div>
        </div>                              
                               
<%
        //set focus field to first order item display order, if there is one
        if (selectedItemIdx == 0) {
          focusField = "fav_order0";
        }
        selectedItemIdx++;
      } //if ( displayKey != null && displayKey.length()>0 )
    } //if ( FavoriteUtility.shouldDisplay( favId, condDisplay ) )
  } //for
%>
        </div>
        <div class="selectedItemsFooter">
          <div class="selectedItemsTotalCount">
            <span>
              <%=resMgr.getText("FavoriteTask.totalCount",
                   TradePortalConstants.TEXT_BUNDLE)%>&nbsp;
            </span>
            <span id="favselectedItems_totalCount"><%=selectedItemIdx%></span>
          </div>
          <div class="clear:both;"></div>
        </div>
      </div>
      <button id="FavoriteTasksDialogUpdateButton" type="button">
        <%=resMgr.getText("common.update", TradePortalConstants.TEXT_BUNDLE)%>
      </button>            
      <%=widgetFactory.createHoverHelp("FavoriteTasksDialogUpdateButton","FavoriteTask.Update") %>
      <br/><br/>
      <div class="instruction">
        <%=resMgr.getText("FavoriteTask.Instruction",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>       
      <div align="right">  
        <button id="FavoriteTasksDialogSaveButton" type="button">
          <%=resMgr.getText("common.save", TradePortalConstants.TEXT_BUNDLE)%>
        </button>         
        <%=widgetFactory.createHoverHelp("FavoriteTasksDialogSaveButton","SaveHoverText") %>
        <button id="FavoriteTasksDialogCancelButton" type="button">
          <%=resMgr.getText("common.cancel", TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("FavoriteTasksDialogCancelButton","CloseHoverText") %>
      </div>
    </span>  
    <%=formMgr.getFormInstanceAsInputField(
         "Favorite-Task-Customization", secureParams)%>
  </form>
</div>

<%
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
   
<script type="text/javascript">

  <%-- define a local variable for functions --%>
  var favTaskDialog = {
    <%-- a property that keep track of the last order field changed so that it sorts at the top --%>
    lastOrderChanged: undefined
  };

  <%-- define the set of field ids - this is used for adding/removing rows and updating row order --%>
  <%-- the array of field ids must be: 0=oid, 1=id, 2=order input, 3+ other fields --%>
  var selectedItemFieldIds = [ "favOid", "favId", "fav_order", "favTaskDesc", "favOptLock" ];

  require(["dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "dijit/form/CheckBox",
         "dijit/form/Button",
         "t360/common", "t360/dialog",
         "t360/itemselect",
         "dojo/ready", 
         "dojo/NodeList-traverse",
         "dojo/domReady!"],
      function(dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             CheckBox,
             Button,
             common, dialog,
             itemSelect,
             ready ) {

    <%-- create widgets dynamically.  we do this way --%>
    <%--  rather than declaratively because the dialog is not parsed correctly the 2nd+ time it opened. --%>
    <%-- this should be better for performance as well --%>
    var myWidget = new Button({
      id: "FavoriteTasksDialogUpdateButton",
      onClick: function() { itemSelect.updateOrder("favselectedItems", selectedItemFieldIds, favTaskDialog.lastOrderChanged); }
    }, "FavoriteTasksDialogUpdateButton");
    myWidget = new Button({
      id: "FavoriteTasksDialogSaveButton",
      onClick: function() { registry.byId("Favorite-Task-Customization").submit(); }
    }, "FavoriteTasksDialogSaveButton");
    myWidget = new Button({
      id: "FavoriteTasksDialogCancelButton",
      onClick: function() { favTaskDialog.close() }
    }, "FavoriteTasksDialogCancelButton");

<%
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_MAIL)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_MAIL%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_NOTIFS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_NOTIFS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRADE_INSTRUMENTS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__COPY_TRADE)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__COPY_TRADE%>_checkbox");
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSFER_EXPORT_COLL ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__TRANSFER_EXPORT_LC%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_AIR_WAYBILL ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_AIRWAY_BILL%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_APPROVAL_TO_PAY ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_APPROVAL_TO_PAY%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_SEND_COLL ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_DIRECT_SEND_COLL%>_checkbox");
<%
    }
    //cquinton 2/25/2013 add new export collection
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_EXPORT_COLL ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_EXPORT_COLL%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DLC ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_IMPORT_LC%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_LOAN_RQST ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_LOAN_REQUEST%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_GUAR ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_OUTGOING_GUAR%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC_DET ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_DETAILED%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SLC ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_OUT_SLC_SIMPLE%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_REQUEST_ADVISE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_REQUEST_TO_ADVISE%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_SHIP_GUAR ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_SHIPPING_GUAR%>_checkbox");
<%
    }
    //cquinton 2/25/2013
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INT_PAYMENT_TRADE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_INT_PMT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_INT_PMT%>_checkbox");
<%
    }
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__COPY_PAYMENT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__COPY_PAYMENT%>_checkbox");
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_TRANSFER_BTW_ACCTS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_TRNSFR_BTN_ACCT%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DOMESTIC_PAYMENT ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_PMT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_PMT%>_checkbox");
<%
    }
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_INT_PAYMENT ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_INT_PMT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_INT_PMT%>_checkbox");
<%
    }
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DIRECT_DEBIT_INSTRUMENTS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__COPY_DIRECT_DEBIT%>_checkbox");
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_DDI ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__CREATE_DIRECT_DEBIT%>_checkbox");
<%
    }
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRADE_TRANSACTIONS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_PEND_TRANS%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_AUTH_TRANS%>_checkbox");      
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_TRADE_HISTORY%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_AMENDMENT%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_TRACER%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__TRADE_CREATE_ASSIGNMENT%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TRANSACTIONS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PMT_PEND_TRANS%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PMT_FUTURE_TRANS%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PMT_AUTH_TRANS%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PMT_HISTORY%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DIRECT_DEBIT_TRANSACTIONS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_DD_PEND_TRANS%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_DD_HISTORY%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_RCV_MATCHING%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_RCV_INVOICES%>_checkbox");
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_RCV_HISTORY%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNT_BALANCES ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_ACCT_BALANCES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PARTY ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PARTIES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PARTIES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PHRASE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PHRASES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PHRASES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_USERS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_USERS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_USERS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_THRESH_GRP ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_THRESHOLDGROUPS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_SEC_PROF ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_SECURITYPROFILES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
      condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PODEFINITIONS%>_checkbox");
<%
  }
  //cquinton 2/25/2013
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INV_UPLOAD_DEFN ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_INV_DEFINITIONS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_CREATE_RULE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_CREATIONRULES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLES_MATCH_RULES ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_REC_MATCH_RULES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_WORK_GROUPS ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_WORKGROUPS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTH_GRP ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_PANEL_AUTH_GRPS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_TEMPLATE_GRP ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_TEMPLATE_GRPS%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEMPLATE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_TEMPLATES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_TEMPLATES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_FX_RATE ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__VIEW_FX_RATES)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__VIEW_FX_RATES%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TEXT_PURCHASE_ORDER ) ||
      condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STRUCTURE_PURCHASE_ORDER ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__UPLOAD_PURCHASE_ORDER%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_MANAGEMENT ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__UPLOAD_INVOICE_FILE%>_checkbox");
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PAYMENT_FILE_UPLOAD ) ) {
%>
    myWidget = new CheckBox({
      checked: <%=favoriteIds.contains(TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE)?"true":"false"%>,
      onChange: function(isChecked) { favTaskDialog.onAvailItemChange(this, isChecked); }
    }, "<%=TradePortalConstants.FAV_TASK__UPLOAD_PAYMENT_FILE%>_checkbox");
<%
  }
%>

    <%-- create a dijit for each order text box --%>
    var favSelectedItems = dom.byId("favselectedItems");
    if ( favSelectedItems ) {
      query('.selectedItemOrderTextBox', favSelectedItems).forEach( function(entry) {
        var orderTextBox = new dijit.form.TextBox({ 
          id: entry.id,
          name: entry.name,
          value: entry.value,
          intermediateChanges: true, <%-- after each keystroke! ie8 does fire normal on button click --%>
          onChange: function() {
            favTaskDialog.lastOrderChanged = entry.id;
          }
        }, entry.id);
        domClass.add(orderTextBox.domNode,"selectedItemOrderTextBox");
      });
    }
  
    <%-- selectItem is page specific, so we do it here rather than commonly in itemselect --%>
    favTaskDialog.selectItem = function(checkboxWidget, selectedItemsId) {      
      <%-- cquinton 12/14/2012 to simplify, set total count node after all is done --%>
      <%-- itemSelect.increaseTotalCount(selectedItemsId);   --%>

      <%-- get selectItemId --%>
      var checkboxId = checkboxWidget.id;
      var idx = checkboxId.indexOf("_checkbox");
      var addSelectItemId = checkboxId.substring(0,idx);
      <%-- get the text to add --%>
      var description = "";
      <%-- only expect one but we use query for simplicity --%>
      query("#"+addSelectItemId+"_avail").children(".availItemDesc").forEach( function(entry, i) {
        description = entry.innerHTML;
      });
      var selectItems = dom.byId(selectedItemsId); <%-- get the selectedItem parent --%>
  
      <%-- get index for next item --%>
      <%--  the fields must be sequential, zero based for submit to work --%>
      var nextIdx = itemSelect.getSelectedItemCount(selectedItemsId);
      
      <%-- get the next order # in sequence --%>
      var nextOrderValue = itemSelect.getMaxOrderValue(selectedItemsId) + 1;
  
      <%-- add the selected item --%>
      var newSelectItem = domConstruct.create("div", null, selectItems);
      domClass.add(newSelectItem,"selectedItem");
      var newOid = domConstruct.create("input", 
        { id:"favOid"+nextIdx, name:"favOid"+nextIdx, type:"hidden", value:"0" }, 
        newSelectItem);
      var newSelectItemOrder = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemOrder,"selectedItemOrder");
      var newOrderInput = domConstruct.create("input",
        {id:"fav_order"+nextIdx, name:"fav_order"+nextIdx}, 
        newSelectItemOrder);
      var newOrderTextBox = 
        new dijit.form.TextBox({ 
          name:"fav_order"+nextIdx, 
          value: nextOrderValue,
          intermediateChanges: true, <%-- after each keystroke! ie8 does fire normal on button click --%>
          onChange: function() { 
            favTaskDialog.lastOrderChanged = "fav_order"+nextIdx;
          }
      }, newOrderInput);
      domClass.add(newOrderTextBox.domNode,"selectedItemOrderTextBox");
      var newSelectItemId = domConstruct.create("input", 
        { id:"favId"+nextIdx, name:"favId"+nextIdx, type:"hidden", value:addSelectItemId }, 
        newSelectItem);
      domClass.add(newSelectItemId,"selectedItemId");
      var newSelectItemDesc = domConstruct.create("span", 
        {id:"favTaskDesc"+nextIdx, innerHTML:description}, 
        newSelectItem);
      domClass.add(newSelectItemDesc,"selectedItemDesc");
      var newOptLock = domConstruct.create("input", 
        { id:"favOptLock"+nextIdx, name:"favOptLock"+nextIdx, type:"hidden", value:"" }, 
        newSelectItem);
      var newSelectItemDel = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemDel,"deleteSelectedItem");
      var newSelectItemClear = domConstruct.create("div", null, newSelectItem);
      domStyle.set(newSelectItemClear, "clear", "both");        

      <%-- cquinton 12/14/2012 to simplify, set total count node after all is done --%>
      itemSelect.setTotalCount(selectedItemsId);
    };

    favTaskDialog.onAvailItemChange = function(checkBoxWidget, checkValue) {
      if ( checkValue ) {
        <%-- do not allow more than 10 selected items --%>
        var tCount = itemSelect.getSelectedItemCount("favselectedItems");
        if ( tCount >= 10 ) {
          alert('A maximum of 10 task items may be selected.');
          checkBoxWidget.set("checked",false);
        }
        else {
          favTaskDialog.selectItem(checkBoxWidget, "favselectedItems");
        }
      }
      else {
        itemSelect.unselectItem(checkBoxWidget, "favselectedItems", selectedItemFieldIds);
      }
    };

    favTaskDialog.close = function(){
      dialog.hide('favTaskDialog');
    };

    <%-- delete selected item event handler --%>
    <%-- cquinton 9/13/2012 ir#t36000004200 --%>
    <%-- use event delegation to get all interaction on any --%>
    <%--  delete selected item within selectedItems --%>
    <%-- this removes need to add/remove event handles as selected items are --%>
    <%--  added/removed --%>
    query('#favselectedItems').on(".deleteSelectedItem:click", function(evt) {
      itemSelect.deleteEventHandler(evt.target);
    });

    <%-- cquinton 12/20/2012 event delegation on change does not fire for ie7,8 --%>
    <%--  add individual event handler instead, for now. --%>
    <%-- put an onchange event handler on the order fields so we can keep track of --%>
    <%-- which one was touched last --%>
    <%-- query('#favselectedItems').on(".selectedItemOrderTextBox:change", function(evt) { --%>
    <%--   favTaskDialog.lastOrderChanged = evt.target.id; --%>
    <%-- }); --%>

    <%-- now load the stuff that requires dojo to be loaded --%>
    ready(function() {    
      var focusFieldId = '<%=focusField%>';
      if ( focusFieldId ) {
        <%-- var focusField = registry.byId(focusFieldId);    //NAVEEN- registry.byId()- Function not working --%>
        var focusField = document.getElementById(focusFieldId);
        focusField.focus();
      }                 
            
    
  
    });

    <%-- dialog is complete, actually display it now --%>
    domConstruct.destroy("favTaskDialogMoreLoading");
    domClass.remove("favTaskDialogContent", 'dialogLoading');
    registry.byId("favTaskDialog")._position();
  }); 


  function toggleSubSection(toggleElementId, subSectionId) {  
    var toggleElement = document.getElementById(toggleElementId);
    var subSection = document.getElementById(subSectionId);    
    var toggleClass = toggleElement.className;

    if(toggleClass == 'expandSectionDialog'){      
      toggleElement.className = 'collapseSectionDialog';
      subSection.style.display = '';
    }
    else {      
      toggleElement.className = 'expandSectionDialog';
      subSection.style.display = 'none';      
    }    
  }

</script>
