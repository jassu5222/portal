<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,java.util.*,java.beans.Beans,com.businessobjects.rebean.wi.*,
                        com.crystaldecisions.sdk.framework.*,
                 com.crystaldecisions.sdk.exception.*,com.ams.tradeportal.busobj.util.*, 
                 java.text.*, com.ams.tradeportal.busobj.util.*, com.crystaldecisions.sdk.occa.infostore.IInfoStore, net.sf.json.*, net.sf.json.xml.*
                 ,com.ams.tradeportal.dataview.*,com.ams.tradeportal.common.cache.Cache,com.ams.tradeportal.common.cache.TPCacheManager" %>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%--commented by Kajal Bhatt
we require EnterpriseSession instead of WISession
<%WISession webiSession = null; %>--%>

<%
Debug.debug("***START*********************EDIT*REPORT*STEP3***************************START***"); 


String saveAsDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
DocumentHandler     standardDocHdr              = null;
DocumentHandler     customDocHdr                = null;
StringBuffer        standardDropdownOptions     = new StringBuffer();
StringBuffer        customDropdownOptions       = new StringBuffer();
String              storageToken                = null;
StringBuffer        newLink                     = new StringBuffer();
boolean             standardReportSelected      = false;
boolean             standardReportOptionsSelected      = false;
boolean             publishSelected             = false;
DocumentHandler     xmlDoc                      = null;
String              hasErrors                   = null;
String              reportName                  = null;
String              repName                     = null;
String              reportDesc                  = null;
String              reportType                  = null;
String              reportMode                  = null;
String              editReportName              = null;
//commented by kajal bhatt
//WIDocument          webiDocument                = null;
DocumentInstance    webiDocument                = null;
String              getOwnershipLevel           = null;
MediatorServices    mediatorServices            = null;
String              physicalPage                = null;
String helpUrl=null;


WidgetFactory widgetFactory = new WidgetFactory (resMgr);

Debug.debug("REPORT MODE IN EDIT REPORT STEP3##########################"+reportMode+"#####################");

storageToken     = StringFunction.xssCharsToHtml(request.getParameter("storageToken"));

reportMode       = StringFunction.xssCharsToHtml(request.getParameter("reportMode"));
editReportName   = StringFunction.xssCharsToHtml(request.getParameter("editReportName"));


reportDesc = StringFunction.xssCharsToHtml(request.getParameter("reportDesc"));
reportType=StringFunction.xssCharsToHtml(request.getParameter("reportType"));


//rbhaduri - 29th Jul 09 - PPX-051 - Add Begin 
if (editReportName == null || editReportName.equals("null")){
    editReportName = "";
}

if (reportDesc == null || reportDesc.equals("null")){
    reportDesc = "";
}

//rbhaduri - 29th Jul 09 - PPX-051 - Add End

//Get ownership level
getOwnershipLevel = userSession.getOwnershipLevel();

Debug.debug("Ownership level in EditReport_Step3  ########"+getOwnershipLevel);

//Process for Copy this report mode
if (reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD))
{
   reportName = resMgr.getText("Reports.CopyOf", TradePortalConstants.TEXT_BUNDLE);
   
}

%>

<%@ include file="/reports/fragments/wistartpage.frag" %>

<%

if (storageToken != null)
{
    //Retrieve report from repository using storage token
    webiDocument = reportEngine.getDocumentFromStorageToken(storageToken);
    repName = webiDocument.getProperties().getProperty(PropertiesType.NAME);
    reportDesc = webiDocument.getProperties().getProperty(PropertiesType.DESCRIPTION); 
    
    
     //rbhaduri - 29th Jul 09 - PPX-051 
    if (reportDesc == null || reportDesc.equals("null")){
        reportDesc = "";
    }
}
else
{
     //Catch all exception
     System.out.println("storagrToken is null in Edit Report Step 3 ######################");
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%
}


if( boSession != null )
{
    //commented by kajal bhatt, we have to change the method parameter of RepUtility.getCategories()
      //Retrieve Standard and Custom category list from the repository
    //12/15/2013 Rel 8.4 CR-854 [BEGIN]- Added parameter for ReportingLanguage option
    standardDocHdr = RepUtility.getCategories(boSession, TradePortalConstants.STANDARD_REPORTS,beanMgr, userSession.getUserOid(),userSession.getReportingLanguage());
    customDocHdr = RepUtility.getCategories(boSession, TradePortalConstants.CUSTOM_REPORTS, beanMgr, userSession.getUserOid(),userSession.getReportingLanguage());
    //12/15/2013 Rel 8.4 CR-854 [END]- Added parameter for ReportingLanguage option
}
else
{
     //Catch all exception
     System.out.println("webiSession is null in Edit Report Step 3 ######################");
     xmlDoc = formMgr.getFromDocCache();
     mediatorServices = new MediatorServices();
     mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
     mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
     mediatorServices.addErrorInfo();
     xmlDoc.setComponent("/Error",         mediatorServices.getErrorDoc());
     xmlDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
     formMgr.storeInDocCache("default.doc", xmlDoc);
     physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
     
     %>
     <jsp:forward page='<%= physicalPage %>' />
     <%   
}

//Append report name with "Copy of" if report mode is REPORT_COPY_CUSTOM or 
//REPORT_COPY_STANDARD
Debug.debug("REPORT NAME in edit report step 3###################"+reportName);
if (reportName != null)
{
    repName = reportName+repName;
}

Debug.debug("REPORT NAME in edit report step 3###################"+repName);











Debug.debug("REPORT NAME in edit report step 3###################"+repName);
//THIS SECTION DETERMINES IF REPORTING IS AVAILABLE IN THIS ENVIRONMENT
//IF IT IS NOT, THEN JUST DISPLAY A MESSAGE AND NOTHING ELSE
boolean reportingAvailable = false;

PropertyResourceBundle portalProperties = null; 
//Narayan IR -AHUL061457053  06/18/2011 Begin
String   securityRights     = null;
boolean isStdMaintainRights = false;
boolean isCusMaintainRights = false; 
boolean isStdViewRights     = false;
boolean isCusViewRights     = false; 


IEnterpriseSession boSessionLocal = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
String current2ndNav = null;

securityRights = userSession.getSecurityRights();
//Narayan IR -AHUL061457053  06/18/2011 End 

try
{
portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
String reportsModuleAvailable = portalProperties.getString("reportsModuleAvailable");

if(reportsModuleAvailable.equalsIgnoreCase(TradePortalConstants.INDICATOR_YES))
   reportingAvailable = true;
}
catch(MissingResourceException mre)
{
// This will happen if the reportsModuleAvailable setting is not in the config file
reportingAvailable = false;
}

String              reportTab                   = null;
StringBuffer        reportTabLink               = null;
StringBuffer        dropdownOptions             = new StringBuffer();
StringBuffer        extraTags                   = new StringBuffer();
String              value1                      = null;
String              value2                      = null;
String              selectedCategory            = null;
String              selectedCategorySQL         = null;
DocumentHandler     dochdr                      = null;
///String              reportType                  = null;
String              userName                    = null;
String              userPass                    = null;
//String                    physicalPage                      = null;
String              report                      = null;
//MediatorServices    mediatorServices            = null; 
//DocumentHandler     xmlDoc                      = null; 
boolean                   reportLoginInd              = false;
//StringBuffer        newLink                     = new StringBuffer();
IEnterpriseSession  boSessionR                   = null;  //added by kajal bhatt on 25/06/07
String            cmsName                       = null;
String            cmsAuth                       = null;     

try 
{ 
userSession.setCurrentPrimaryNavigation("NavigationBar.Reports"); 
//added by kajal bhatt on 25/06/07
//starts here
try
{
       boSessionR = (IEnterpriseSession) request.getSession().getAttribute("CE_ENTERPRISESESSION");
   //out.println("boSession="+request.getSession().getAttribute("CE_ENTERPRISESESSION"));
}
catch (Exception e)
{
   e.printStackTrace();
}
//ends here
//Determine the suffix to place in front of the organization OID
String reportingUserSuffix = userSession.getReportingUserSuffix();

if(reportingUserSuffix == null)
{
//rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify Begin - M And T Bank Change - get reporting 
//suffix based on the level of the user logged in. For ASP level user get the get the suffix from global organization table. But for all other levels, get
//the suffix from the client bank table
String userOwnerShipLevel = null;
userOwnerShipLevel = userSession.getOwnershipLevel();
String reportingUserSuffixSql = null;
//jgadela R91 IR T36000026319 - SQL INJECTION FIX
String sqlParam = null;
	if (userOwnerShipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
    { 			
			   //jgadela R91 IR T36000026319 - SQL INJECTION FIX
               // Get the reporting user suffix from the Global Org to which this org belongs
               reportingUserSuffixSql = "select reporting_user_suffix" 
                        + " from global_organization"
                        + " where organization_oid = ? ";
               sqlParam = userSession.getGlobalOrgOid();
               
    } else{     //jgadela R91 IR T36000026319 - SQL INJECTION FIX 
               String clientBankOID = userSession.getClientBankOid();
     
               // Get the reporting user suffix from the Client Bank of the logged in user
               reportingUserSuffixSql = "select reporting_user_suffix" 
                        + " from client_bank"
                        + " where organization_oid = ? " ;
               sqlParam = clientBankOID;
   }  //rbhaduri - 30th Nov 09 - IR KAUJ112336153 Modify End - M And T Bank Change 
	DocumentHandler result = DatabaseQueryBean.getXmlResultSet(reportingUserSuffixSql, false, new Object[]{sqlParam});
	reportingUserSuffix = result.getAttribute("/ResultSetRecord(0)/REPORTING_USER_SUFFIX");    
	// Put it on the session so that later accesses of this page don't have to look it up  
	userSession.setReportingUserSuffix(reportingUserSuffix);
}
//rbhaduri - 30th Nov 09 - IR KAUJ112336153 - added system out
//Place the reporting user suffix on to the beginning of the OID The suffix is used to distinguish between organizations being processed by different ASPs that might be on the same reports server.  
userName = userSession.getOwnerOrgOid()+reportingUserSuffix;
//get password from the property file
userPass = userSession.getOwnerOrgOid() + reportingUserSuffix.toLowerCase(); 
//initialize config file
//ConfigFileReader.init(application.getRealPath("WEB-INF"));
try //try-2
{
   reportLoginInd = userSession.getReportLoginInd(); 
   //modified by Kajal Bhatt on 25/06/07 starts here
   if((boSessionR == null) || (reportLoginInd == false)) //if-3
   {
         // Attempt logon. Create an Enterprise session manager object.
         ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
         
         // Log on to BusinessObjects Enterprise
        cmsName = portalProperties.getString("CMSName");
        cmsAuth = portalProperties.getString("Authentication");   
        boSessionR = sessionMgr.logon(userName, userPass, cmsName, cmsAuth);
         
         //put the enterprisesession in to session
         request.getSession().setAttribute("CE_ENTERPRISESESSION", boSessionR);
         
         //get the reportengines object and store it into session
         ReportEngines reportEngines1 = null;
         reportEngines1 = (ReportEngines)boSessionR.getService("ReportEngines");
         // request.getSession().setAttribute("ReportEngines", reportEngines1);
         //ends here
         //get the default storage token and store it into session
         String strToken = boSessionR.getLogonTokenMgr().getDefaultToken();
         userSession.setReportLoginInd(true); // Nar IR-T36000031433 Rel 9.2.0.0
   }//end of if-3
} //end of try-2
catch (REException rex) //catch-2
{
   rex.printStackTrace();
   //Catch all exceptions
   Debug.debug("*** REException in Favorate Reports Home>>: "+rex.getErrorCode()+" "+rex.getMessage());
   
   xmlDoc = formMgr.getFromDocCache();
   mediatorServices = new MediatorServices();
   mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
   mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
   mediatorServices.addErrorInfo();
   xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
   xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
   formMgr.storeInDocCache("default.doc", xmlDoc);
   physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
}//end of catch-2
catch(SDKException sdk)
{
   sdk.printStackTrace();
               StringTokenizer stk = new StringTokenizer(sdk.toString(),"OCA_Abuse");
               Debug.debug("*** <<SDKException in Favorate Reports Home>>: " + sdk.getMessage());
   //BO User is not setup in CMS
   if( stk.hasMoreTokens())
   {
         Debug.debug("*** <<BO user is not serup in CMS>>: " + userName);
         xmlDoc = formMgr.getFromDocCache();
         mediatorServices = new MediatorServices();
         mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.REPORT_USER_NOT_SETUP);
         mediatorServices.addErrorInfo();
         xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
         xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
         formMgr.storeInDocCache("default.doc", xmlDoc);
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
               }
               else
               {
           //Catch all exceptions
         xmlDoc = formMgr.getFromDocCache();
         mediatorServices = new MediatorServices();
         mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
         mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1, TradePortalConstants.WEBI_SESSION_ERROR);
         mediatorServices.addErrorInfo();
         xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
         xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
         formMgr.storeInDocCache("default.doc", xmlDoc);
         physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request);
   }
}
}
catch (Exception e) 
{ 
//In this block, catch exceptions from anywhere during the process of attempting to connect
//to the reports server

Debug.debug("*** Exception occured while in ReportsHome.jsp while attempting to connect to reports server: "); 
e.printStackTrace();
xmlDoc = formMgr.getFromDocCache();
mediatorServices = new MediatorServices();
mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());        
mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,       TradePortalConstants.WEBI_SERVER_ERROR);
mediatorServices.addErrorInfo();
xmlDoc.setComponent("/Error", mediatorServices.getErrorDoc());
xmlDoc.setAttribute("/In/HasErrors", TradePortalConstants.INDICATOR_YES);
formMgr.storeInDocCache("default.doc", xmlDoc);
physicalPage = NavigationManager.getNavMan().getPhysicalPage("ReportsErrors", request); 

} 
   isStdMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_STANDARD_REPORT);
   isStdViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_STANDARD_REPORT);
   isCusMaintainRights   = SecurityAccess.hasRights(securityRights, SecurityAccess.MAINTAIN_CUSTOM_REPORT);
   isCusViewRights       = SecurityAccess.hasRights(securityRights, SecurityAccess.VIEW_CUSTOM_REPORT);  
   
   ReportsDataView reportsDataView = new ReportsDataView();
   reportsDataView.setUserSession(userSession);
   DocumentHandler  xmlDocStd = null;
   DocumentHandler  xmlDocCus = null; 
   HashMap<String,String> parameterMap = new HashMap<String,String>();
   HashMap<String,String> parameterMapCus = new HashMap<String,String>();
   
         //If reportTab is null set the default reports tab to Standatd Reports
   if(isStdMaintainRights || isStdViewRights){
         String    selectedCategorySQLStd         = null;
         reportTab = TradePortalConstants.STANDARD_REPORTS;
         reportType = TradePortalConstants.STANDARD_REPORTS;
         selectedCategory = TradePortalConstants.REPORT_ALL_CAT;//request.getParameter("standardCategory");
         report = TradePortalConstants.NEW_STANDARD_REPORTS;
         //02/14/2014 jgadela R8.4 IR T36000024863
         //selectedCategorySQLStd = RepUtility.getUserCategorySQLWithoutQuote(selectedCategory, beanMgr, userSession.getUserOid(),userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType());
         selectedCategorySQLStd = RepUtility.getUserCategorySQL(selectedCategory, beanMgr, userSession.getUserOid(),userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType());
         parameterMap.put("reportType", reportType);
         parameterMap.put("userName", userName);
         parameterMap.put("userPass", userPass);
         parameterMap.put("cmsName", portalProperties.getString("CMSName"));
         parameterMap.put("cmsAuth", portalProperties.getString("Authentication"));
         parameterMap.put("categories", selectedCategorySQLStd);
         xmlDocStd = reportsDataView.getDataInXmlFormat(parameterMap);
   }
   if(isCusMaintainRights || isCusViewRights){
         String    selectedCategorySQLCus         = null;
         reportTab = TradePortalConstants.CUSTOM_REPORTS;
         reportType = TradePortalConstants.CUSTOM_REPORTS;
         selectedCategory = TradePortalConstants.REPORT_ALL_CAT;//request.getParameter("standardCategory");
         //selectedCategorySQLCus = RepUtility.getUserCategorySQL(selectedCategory, beanMgr, userSession.getUserOid(),userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType());
         parameterMapCus.put("reportType", reportType);
         parameterMapCus.put("userName", userName);
         parameterMapCus.put("userPass", userPass);
         parameterMapCus.put("cmsName", portalProperties.getString("CMSName"));
         parameterMapCus.put("cmsAuth", portalProperties.getString("Authentication"));
         parameterMapCus.put("categories", selectedCategorySQLCus);
         xmlDocCus = reportsDataView.getDataInXmlFormat(parameterMapCus);
   }
   String jsonStd = getJsonData(xmlDocStd);
   String jsonCust = getJsonData(xmlDocCus);
   
//out.println(getJsonData(xmlDocStd)) ;



//Process for Save as Standard Report mode
//commented by kajal bhatt
/* In XIR2 there is no way to detach the category, the only way is while saving
the document pass the category as null*/
/*if (reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
    reportMode.equals(TradePortalConstants.REPORT_COPY_CUSTOM))
{
    String [] categories;
    if ( (categories = webiDocument.getCategoryList()) != null )

    for (int i=0;i<categories.length;i++)
          webiDocument.detachCategory(categories[i]);
}*/
xmlDoc = formMgr.getFromDocCache();
hasErrors = xmlDoc.getAttribute("/In/HasErrors");

if ((hasErrors != null) && (hasErrors.equals(TradePortalConstants.INDICATOR_YES)))
{
    editReportName = xmlDoc.getAttribute("/In/ReportName");
    reportDesc = xmlDoc.getAttribute("/In/ReportDesc");
    reportType = xmlDoc.getAttribute("/In/ReportType");
    //String standardReport  = xmlDoc.getAttribute("/In/ReportType");
    
      //rbhaduri - 29th Jul 09 - PPX-051 
    if (reportDesc == null || reportDesc.equals("null")){
        reportDesc = "";
    }
        
}
String selectedCategoryHome = request.getParameter("selectedCategory");
//jgadela R 8.4 CR-854 T36000024797   -[START] Reporting page labels. 
String reportingPageLabelLan = null;
String cbReportingLangOptionInd = null;

Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
if (!StringFunction.isBlank(userSession.getClientBankOid())){
	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 
}

if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){
	reportingPageLabelLan = userSession.getReportingLanguage();
}else{
	reportingPageLabelLan = userSession.getUserLocale();
}
//jgadela R 8.4 CR-854 T36000024797   -[END]
%>



<%--*******************************HTML for page begins here **************--%>


<div class="pageMainNoSidebar">     
<div class="errorsAndwarnings"  id="errorsAndwarnings" style="display:none; background-color:#FFFCEF" >
<div class="errorText" >

<%= resMgr.getText("Reports.errorText", TradePortalConstants.TEXT_BUNDLE)%>

</div>
</div>

<div class="pageContent">
<div class="dialogContent fullwidth">
       <input type="hidden" name="ReportName"    value="<%= editReportName %>" />  
       
     <%//12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting report lables
		String nameLable =  resMgr.getText("Reports.ReportName",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan); 
		String reportDescLable =  resMgr.getText("Reports.Desc",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
		String reportTypeLable =  resMgr.getText("Reports.ReportType",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);%> 
     <%=widgetFactory.createTextField("ReportName",nameLable,repName,"35",false,true,false,"","","")%>    
     <%=widgetFactory.createTextField("ReportDesc",reportDescLable,reportDesc,"35",false,true,false,"","","")%>
     <%= widgetFactory.createLabel("ReportType",reportTypeLable,false,true,false,"") %>  
            
   <%//12-19-2013 Rel 8.4 CR-854 [END]%>
   <div class="formItemWithIndent3" >
   
  <%
  if (!reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM))
  {
  %>

 
            <%
            if (reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) ||
                reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
            {
            %>
                <input type="hidden" name="StandardReport" value="Y" />
                 <%=widgetFactory.createRadioButtonField("StandardReport","StandardReportId","","Y",standardReportSelected,false,"onChagne=\"disableDivs()\"","") %>
                &nbsp;                                                                                                                                
            <%
            }
            else
            {
            	//12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting report lables
        		String stdRepLable =  resMgr.getText("Reports.StandardReportsCategory",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
            %>
       		<%=widgetFactory.createRadioButtonField("StandardReport","StandardReportId",stdRepLable,"Y",standardReportSelected,false,"onChange=\"disableDivs()\"","") %>
      <%} %>
      
      
      <div class="formItemWithIndent3" >
         <%
            //Narayan CR-603 06/27/2011 while editing reprot include All Category only if report category indicator is off.
              String includeAllCategories = TradePortalConstants.INDICATOR_YES;
              // UserWebBean thisUser = beanMgr.createBean(UserWebBean.class,"User");      
              // thisUser.setAttribute("user_oid", userSession.getUserOid());
              // thisUser.getDataFromAppServer();  
              //String clientBankOid = thisUser.getAttribute("client_bank_oid");         
               
               if (!StringFunction.isBlank(userSession.getClientBankOid())) {
                      ///Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
                      //DocumentHandler CBCResult = (DocumentHandler)CBCache.get(clientBankOid);
                      includeAllCategories = CBCResult.getAttribute("/ResultSetRecord(0)/REPORT_CATEGORIES_IND");
                }
                    
            // W Zhu CR-603 4/27/2011 Consider user categories.
                    //standardDropdownOptions.append(Dropdown.getUnsortedOptions(standardDocHdr, "CATEGORY", "CATEGORY", "", null));
                              //Ravindra - Rel7000 - IR# RUUL070741274  - 07/11/2011 - Start
                              //ANZ -> CR -603 -Report Categories  -  Admin user should see only  the category associated with the Corporate Customer.
                              //Ravindra - Rel7000 - IR# DVUL070864037  - 07/12/2011 - Start
                    //jgadela Rel 8.4 CR-854 [BEGIN]- Modified the lacale param to pass reporting lang option
                    standardDropdownOptions.append(RepUtility.getUserCategoryOptions(standardDocHdr, "", userSession.getReportingLanguage(), beanMgr, userSession.getUserOid(),includeAllCategories,userSession.hasSavedUserSession(),userSession.getOwnerOrgOid(),userSession.getSecurityType()));
                  //Ravindra - Rel7000 - IR# DVUL070864037  - 07/12/2011 - End
                              //Ravindra - Rel7000 - IR# RUUL070741274  - 07/11/2011 - End
              
                    //12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting report cat lables
        			String stdCatLable =  resMgr.getText("Reports.StandardCategory",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);
                    if (reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) &&
                        reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
                       /*  out.print(InputField.createSelectField("StandardCategory", "", "", standardDropdownOptions.toString(), "ListText", false, ""));
                    */
                    out.println(widgetFactory.createSelectField("StandardCategory",stdCatLable,"", standardDropdownOptions.toString(), false,false,false,"DISABLED","","")); 
                    
                    else
                     /*    out.print(InputField.createSelectField("StandardCategory", "", "", standardDropdownOptions.toString(), "ListText", false, "onClick=\"pickUser(true)\"")); */
                   
                    out.println(widgetFactory.createSelectField("StandardCategory",stdCatLable,"", standardDropdownOptions.toString(), false,false,false,"DISABLED","",""));   
                   %> 
   <%
    //Display selective publishing options only for BOG and Bank users
    if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK) ||
        getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
    {
   %>
 
      <label  style="margin-left:18px">   <%=resMgr.getText("Reports.PublishText", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %></label>
          
           </br>
    <span style="margin-left:10px" >     
             
    <%=   widgetFactory.createRadioButtonField("Publish","public", "","Y",publishSelected,false,"DISABLED","") %>                  
                        <%
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK))
                        {    //12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting report cat lables
                        %>
                            <%=resMgr.getText("Reports.ClientBankPublishAll", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
                        <%
                        }
                                
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
                        {
                        %>
                            <%=resMgr.getText("Reports.BOGPublishAll", TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %></p>
                        <%
                        }
                        %>
                         </span>
                 <br/>
   
    <span style="margin-left:10px" >
        <%=   widgetFactory.createRadioButtonField("Publish","private","","N",publishSelected,false,"DISABLED","") %>                         
                        <%
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BANK))
                        {
                        %>
                            <%=resMgr.getText("Reports.ClientBankSelect", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
                                    
                        if (getOwnershipLevel.equals(TradePortalConstants.OWNER_BOG))
                        {
                        %>
                            <%=resMgr.getText("Reports.BOGSelect", TradePortalConstants.TEXT_BUNDLE) %></p>
                        <%
                        }
        } //end if
        } //end if
  %>
 </span>
 
  
</div>      
  <div style="clear:both"></div>
  <%
  if (!reportMode.equals(TradePortalConstants.REPORT_COPY_STANDARD) &&
      !reportMode.equals(TradePortalConstants.REPORT_EDIT_STANDARD))
  {        
                        
            customDropdownOptions.append(Dropdown.getUnsortedOptions(customDocHdr, "CATEGORY", "CATEGORY", "", null));
             
           String S=customDropdownOptions.toString();
                          
       
                    if (reportMode.equals(TradePortalConstants.REPORT_EDIT_CUSTOM))
                    {
                    %>
                        <input type="hidden" name="StandardReport" value="C" />
                        &nbsp;
                    <%
                    }
                    else
                    {
     %>
                          <%-- <%=InputField.createRadioButtonField("StandardReport", "N", "", standardReportSelected, "ListText", "", false)%> ----%> 
             		<%//12-19-2013 Rel 8.4 CR-854 [BEGIN]- getting report cat lables
        			String custReLable =  resMgr.getText("Reports.CustomReportsCategory",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan);%>             
                  <%= widgetFactory.createRadioButtonField("StandardReport","CustomReportsId",custReLable,"C",standardReportSelected,false,"onChange=\"disableDivs()\"","") %>
                                                                                                      
                     
                      <%
                      }}
   %>
  
   </div>
  <span>&nbsp;</span>
  <span>&nbsp;</span>
  
   <button data-dojo-type="dijit.form.Button" type="button" name="SaveButton"  id="SaveButton">
  		<% //12-19-2013 Rel 8.4 CR-854 [BEGIN]- lables change%>
       <%=resMgr.getText("Reports.SaveText",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %> 
       <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                                    saveAsReport();                                 
             </script>
             
            </button>
 <%=widgetFactory.createHoverHelp("SaveButton","SaveHoverText", reportingPageLabelLan) %>  
      
       <button data-dojo-type="dijit.form.Button" type="button" name="CancelButton"  id="CancelButton">
       <% //12-19-2013 Rel 8.4 CR-854 [BEGIN]- lables change%>
      <%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE, reportingPageLabelLan) %>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                                    closeDialog();                                  
             </script>
      
      </button>
        <%=widgetFactory.createHoverHelp("CancelButton","common.Cancel", reportingPageLabelLan) %>  
  
  
  
 
</div>

</div></div>



<script language="javascript"> 

function disableDivs(){
      require(["dijit/registry","t360/dialog"], function(registry,dialog){
            
            
            
            
      if (registry.byId("StandardReportId").checked){
            dijit.byId('StandardCategory').set('disabled',false);
            if(dijit.byId('public')!= null){
                  dijit.byId('public').set('disabled',false);
            }
            if(dijit.byId('private')!= null){
                  dijit.byId('private').set('disabled',false);
            }
      }else if(!registry.byId("StandardReportId").checked){
            dijit.byId('StandardCategory').set('disabled',true);
            if(dijit.byId('public')!= null){
                  dijit.byId('public').set('disabled',true);
            }
            if(dijit.byId('private')!= null){
                  dijit.byId('private').set('disabled',true);
            }
      }
      });
}




function saveAsReport(){
      var jsonS = "[<%=jsonStd%>]";
      var jsonCust = "[<%=jsonCust%>]";
      
      
      var jsonObj = null;
      
      require(["dijit/registry","t360/dialog"], function(registry,dialog){
            var reportName = registry.byId("ReportName").value;
            var reportDesc = registry.byId("ReportDesc").value;
            var standardCategory = registry.byId("StandardCategory").value;
            
            var reportTypeStandard;
            if(registry.byId("StandardReportId").checked){
                  reportTypeStandard = 'Y';
                  document.getElementById("current2ndNav1").value="S";
                  jsonObj = jsonS;
            }else if(registry.byId("CustomReportsId").checked){
                  reportTypeStandard = 'S';
                  document.getElementById("current2ndNav1").value="C";
                  jsonObj = jsonCust;
            }
            
            if (reportName == null || reportName == ""){
                  alert('Enter name'); 
                  return false;
            }
            if (reportDesc == null || reportDesc == ""){
                  alert('Enter Desc');
                  return false;
            }
            
            if(!registry.byId("StandardReportId").checked && !registry.byId("CustomReportsId").checked){
                  alert("Select Report Type");
                  return false;
            }
            var editreportName='<%=StringFunction.escapeQuotesforJS(editReportName)%>';
            
     
            var arryOb = jsonObj.split('%split%');
        for(var i = 0; i < arryOb.length; i++){
                  if(reportName==arryOb[i]){
                    document.getElementById('errorsAndwarnings').style.display = "block";
                    return false;
                  }
            }
            dialog.doCallback('<%=StringFunction.escapeQuotesforJS(saveAsDialogId)%>', 'select',reportName, reportDesc,reportTypeStandard, standardCategory);

      });
      
}

function closeDialog()
{
    hideDialog('<%=StringFunction.escapeQuotesforJS(saveAsDialogId)%>');
}

var dialogName=dijit.byId('ReportDialog');
var dialogCloseButton=dialogName.closeButtonNode;
dialogCloseButton.setAttribute("id","ReportDialogIdClose");



</script>



<script LANGUAGE="JavaScript">



  function pickUser(selectUser) {
    <%--  When the user drop down box has been clicked, --%>
    <%--  set the first StandardReport to 'Y' and the second to 'N' for the radio buttons --%>
    <%-- var size=document.saveReport.StandardReports.length; --%>
    
    document.saveReport.StandardReport[0].checked = selectUser;
    document.saveReport.StandardReport[1].checked = !selectUser;    
    
   
  }
</script>

<script LANGUAGE="JavaScript">
  function pickCat(selectUser) {
    <%--  When the user drop down box has been clicked, --%>
    <%--  set the first StandardReport to 'Y' and the second to 'N' for the radio buttons --%>
    <%-- var size=document.saveReport.StandardReports.length; --%>
    document.saveReport.StandardReport.checked = selectUser;
       
  }
</script>
 <%= widgetFactory.createHoverHelp("ReportDialogIdClose","common.Cancel")%>
 
<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

<%
Debug.debug("***END*********************EDIT*REPORT*STEP3***************************END***"); 
%>



<%!
String getJsonData(DocumentHandler xmlDoc)
{
   StringBuilder data = new StringBuilder();
   if(xmlDoc == null){
               return data.toString();
   }
   Vector<DocumentHandler> aVector =  xmlDoc.getFragments("/ResultSetRecord");
   
   int numItems = aVector.size();
   
   for (int iLoop=0;iLoop<numItems;iLoop++) {
             DocumentHandler repDoc = (DocumentHandler) aVector.elementAt(iLoop);
             String name = repDoc.getAttribute("/NAME");
         //cquinton 11/29/2012 apply xssCharsToHtml, which encodes html chars and single quote char
       //  data.append("{ ");
         data.append(name);
         //data.append(" }");
         if ( iLoop < numItems-1) {
           data.append("%split%");
         }
    }
   
   return StringFunction.xssCharsToHtml(data.toString());
}

%>
