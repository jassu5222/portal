<%--Kyriba CR 268 
 *  Bank Branch Selector (external / operational) Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.html.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, 
                 com.ams.tradeportal.busobj.*, com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%	


   //T36000021349- Display client bank name as radio button label 
    String clientBankOid = userSession.getClientBankOid();
    
    ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
    clientBank.getById(clientBankOid);
    String clientBankName = clientBank.getAttribute("name");
   // T36000021349-end
   
  //read in parameters
  String bankBranchSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
  String isNonCC = StringFunction.xssCharsToHtml(request.getParameter("nonCC"));
  if(isNonCC == null || isNonCC == ""){
	isNonCC = TradePortalConstants.INDICATOR_NO;
  }
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
  java.util.List<Object> sqlParamsLst = new java.util.ArrayList();
  String ownerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(ownerOrg);
  String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                        corpOrg.getAttribute("second_op_bank_org"),
                        corpOrg.getAttribute("third_op_bank_org"),
                        corpOrg.getAttribute("fourth_op_bank_org")};

  //do a local query to get names for operational bank orgs
  //could do this as dataview, but this is simpler - we don't expect this to 
  //change on this dialog via search.
  DocumentHandler oboListDoc = null;
  StringBuilder in = new StringBuilder("?");
  sqlParamsLst.add(opBankOid[0]);
  Vector opBankDocVec = new Vector();
  for (int oboCount=1; oboCount<4; oboCount++) {
    if (InstrumentServices.isBlank(opBankOid[oboCount])) {
      break;
    }
    else {
        in.append(", ?");
        sqlParamsLst.add(opBankOid[oboCount]);
    }
  }
  try {
	    String sql = "select ORGANIZATION_OID, NAME from OPERATIONAL_BANK_ORG where ORGANIZATION_OID in ("+in+") order by name";
		Debug.debug(" op org sql:"+sql.toString());
		if(StringFunction.isNotBlank(in.toString())){
	   		 oboListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParamsLst);
	    if(null != oboListDoc){
	    	opBankDocVec = oboListDoc.getFragments("/ResultSetRecord");
	    }
		}
  } catch (AmsException e) {
    //todo: should throw an error
    Debug.debug("queryListView threw an exception");
    e.printStackTrace();
  }
%>

<%

/*Kyriba CR 268
Get all External Bank's for this particular corporate customer.
*/
String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
ClientServerDataBridge csdb = resMgr.getCSDB();
long longOldCorpOrgOid = Long.parseLong(ownerOrg.trim());

CorporateOrganization transOriginal = (CorporateOrganization)EJBObjectFactory.createClientEJB(serverLocation,
				   "CorporateOrganization", longOldCorpOrgOid, csdb);
ComponentList externalBankComponenet = (ComponentList) transOriginal.getComponentHandle("ExternalBankList");
int extBankTotal = externalBankComponenet.getObjectCount();

BusinessObject externalBankBusinessObj = null;
String externalBankBranches = "";
String externalBankOid = null;
DocumentHandler extBankDoc = null;
sqlParamsLst = new java.util.ArrayList();
for(int i=0; i<extBankTotal; i++){
	
	externalBankComponenet.scrollToObjectByIndex(i);
	externalBankBusinessObj = externalBankComponenet.getBusinessObject();
	externalBankOid = externalBankBusinessObj.getAttribute("op_bank_org_oid");		
	  
	      if (!InstrumentServices.isBlank(externalBankOid)) {
	          if ( externalBankBranches.length()>0 ) {
	        	  externalBankBranches += ",";
	          }
	          externalBankBranches += "?";
	          sqlParamsLst.add(externalBankOid);
	      }
	      else {
	          break; //if a blank is found there will be no more
	      }		
}
//Build query to get all external bank's 
try {
	
    StringBuilder extBanksql = new StringBuilder();
    extBanksql.append("select ORGANIZATION_OID, NAME");
    extBanksql.append(" from OPERATIONAL_BANK_ORG");
    extBanksql.append(" where ORGANIZATION_OID in (").append(externalBankBranches).append(")");
    extBanksql.append(" order by name");
	Debug.debug(" external bank sql:"+extBanksql.toString());
	if(StringFunction.isNotBlank(externalBankBranches)){
    extBankDoc = DatabaseQueryBean.getXmlResultSet(extBanksql.toString(), true, sqlParamsLst);
    }

  } catch (AmsException e) {
    //todo: should throw an error
    Debug.debug("Dialog:Exception in Getting External Bank");
    e.printStackTrace();
  }

String dropdownOptions = null; %>

<div id="bankBranchSelectorDialogContent" class="dialogContent">

  <div class="instruction">
    <%= resMgr.getText("bankBranchSelectorDialog.instructions", TradePortalConstants.TEXT_BUNDLE) %>
  </div>
  <%
	dropdownOptions = Dropdown.createSortedOptions(oboListDoc, "ORGANIZATION_OID", "NAME", "", null, resMgr.getResourceLocale());
	//dropdownOptions = "<option value=\"\"> </option>"+dropdownOptions;
	

  
  if(null != opBankDocVec && opBankDocVec.size() > 0) {
  		if(extBankTotal == 0 ){%>
  	<div class="formItemWithIndent4">
  	<%
  			out.print(widgetFactory.createSelectField( "OperationalBankDropDown", "", " ", dropdownOptions, 
  				  false, false, false, "onChange=setBankOid(this)","","none"));
  	%></div>
  	<%}else{%>
  	
  
	  <div>
	  <%=widgetFactory.createRadioButtonField("BankSearchRadio","OperationalBankOrgRadio",
						clientBankName,"OperationalBankOrgRadio", false,  false, "onClick=\"showBankDropDown('OperationalBankOrgRadio')\"","") %>	
	  </div>
	  <div id="operationalbankorg_div_id" class="formItemWithIndent4">
	  <%
	 	out.print(widgetFactory.createSelectField( "OperationalBankDropDown", "", " ", dropdownOptions, 
	    false, false, false, "onChange=setBankOid(this)","","none"));
	  %>
	  </div>
  	<%} 
  	}
  
  dropdownOptions = "";
  dropdownOptions = Dropdown.createSortedOptions(extBankDoc, "ORGANIZATION_OID", "NAME", "", null, resMgr.getResourceLocale());
  //dropdownOptions = "<option value=\"\"> </option>"+dropdownOptions;
  
  if(extBankTotal > 0){
	  if(null == opBankDocVec || opBankDocVec.size() == 0) {%>
		  <div class="formItemWithIndent4">
		 <%  out.print(widgetFactory.createSelectField( "ExternalBankDropDown", "", " ", dropdownOptions, 
		    false, false, false, "onChange=setBankOid(this)","","none"));
		 %>
		 </div>
	  <%} else {%>
	  <div>
	   <%=widgetFactory.createRadioButtonField("BankSearchRadio","ExternalBankRadio",
						"NewInstrument.Dialog.Bank","ExternalBankRadio", false,  false, "onClick=\"showBankDropDown('ExternalBankRadio')\"","") %>
	  </div>
	  <div id="externalbank_div_id" class="formItemWithIndent4">
	  <%
	 
	  out.print(widgetFactory.createSelectField( "ExternalBankDropDown", "", " ", dropdownOptions, 
	    false, false, false, "onChange=setBankOid(this)","","none"));
	  %>
	  </div>
  <%}
	  }
  //This is used to show the footer buttons select & cancel
  gridHtml = dgFactory.createDataGrid("bbGrid","BankBranchSelectorDataGrid",null);
%>
  <%= gridHtml %>
    <% if(TradePortalConstants.INDICATOR_NO.equals(isNonCC)) { %>

 <%= widgetFactory.createTextField( "manualInstrumentId", "ConvertBankBranch.Header2","", "16", false, true, false, "","","instruction")%>

<% } %>

</div>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>

var selectedBankOid = "";
  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
	  <%-- When dialog loads hide the drop downs --%>
	  hideBankDropDown();
  });

  <%-- on select perform execute the select callback function --%>
  function selectBB() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry","dojo/dom", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, digit,query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      var rowKeys = selectedBankOid;
	  var manualInstId = null;
	   var isNonCCVal = "<%=StringFunction.escapeQuotesforJS(isNonCC)%>";
	    
		if(isNonCCVal == "<%=TradePortalConstants.INDICATOR_NO%>"){
		  var selectField = dijit.byId('manualInstrumentId');
		  manualInstId = selectField.attr('value');
		}
	<%--    var manualInstId = registry.byId("manualInstrumentId").value;	 --%>
	  <%--  var externalBankOid = registry.byId("externalBankOid").value;	 --%>
	   if((isNonCCVal == "<%=TradePortalConstants.INDICATOR_NO%>") && (manualInstId == null || manualInstId == "")){
		    alert("Instrument Id is required for Converted instruments.");
			return;
	   }
	   var filter = /^[A-Za-z0-9]+$/;
	   if((isNonCCVal == "<%=TradePortalConstants.INDICATOR_NO%>") && !filter.test(manualInstId)){
		    alert("Instrument Id must be an alpha-numeric value");
			return;
	   }

      if (rowKeys && rowKeys.length > 1 ) {
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>', 'select',rowKeys,manualInstId);
      }else{
    	  alert("Please select the Bank/Branch or press 'Cancel'");
			return;
      }
    });
  }

  function closeBBDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>');
    });
  }
 
	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","bankBranchSelectorDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
	
	<%-- Based upon the radio button selection, show the appropriate dropdown --%>
	function showBankDropDown(temp){
		 
		if('OperationalBankOrgRadio' == temp){
			document.getElementById("operationalbankorg_div_id").style.display = 'block' ;
			document.getElementById("externalbank_div_id").style.display = 'none' ;
			dijit.byId('OperationalBankDropDown').attr('displayValue','');
		}else if('ExternalBankRadio'== temp){
			document.getElementById("operationalbankorg_div_id").style.display = 'none' ;
			document.getElementById("externalbank_div_id").style.display = 'block' ;
			dijit.byId('ExternalBankDropDown').attr('displayValue','');
		}
	}
	<%-- Hide the dropdown when dialog loads. --%>
	function hideBankDropDown(){
        <%--  R8.4 T36000026116 smanohar, check to make sure that dialog does exist in the page --%>

		if (document.getElementById("operationalbankorg_div_id"))
			document.getElementById("operationalbankorg_div_id").style.display = 'none' ;
		if (document.getElementById("externalbank_div_id"))
			document.getElementById("externalbank_div_id").style.display = 'none' ;
 	}
	<%-- Set the selected external/oper bank oid to an common var --%>
	function setBankOid(thisObj){
		if(thisObj){
			selectedBankOid = thisObj.value;
		}
	}
	
</script>
	<%=widgetFactory.createHoverHelp("bankBranchSelectorDialogCloseLink", "common.Cancel") %>