<%--
*******************************************************************************
                              Mail Message Page

  Description:  
    This page is designed to display the dynamic Mail Message page.  The page 
  is dynamic because it can be displayed in one of 9 different views.  In some 
  views the buttons (like 'Reply to Bank'), the button is a submit button,
  in other views the button is a link.  Pay close attention to that, because
  it will affect whether or not a mediator is called.  For example, the Route
  Button sometimes will call the RouteDeleteMediator and other times it will
  call the MailMessage Mediator.  Generally there are 2 modes the Mail Message 
  Mediator is called in =>
  Message Mode:  'C' = 'Create' a new message.
  		 'U' = 'Update' an existing message.
  This informs the mediator whether or not we are updating a preExisting message.
  
    This Particular jsp is creating an ejb instead of creating a webBean.  
  This was intentional because the un_read flag needs to be updated prior to
  leaving the page when a message is read for the first time.  The only way to 
  accomplish this is to use an ejb rather than a webBean.

  The basic views this page be displayed in are as follows:
 
  View				Condition				Read Only Status (in general)
  ---------------------------------------------------------------------------------------------------
  New Mail Message		Message oid passed in is null		False
				isReply flag == false
				isEditFlage == false

  Draft Mail Message		Message oid was passed in		False
				MessageStatus = 'DRAFT'
				isEdit = 'true' OR Message is assigned to you
					OR Message is assgined to no-One && is assigned to your corporate Org

  Mail Message Rec From Bank	Message oid was passed in		True
				Message was assigned to someone else (&& you have viewing rights)
				
  Mail Message Sent To Bank	Message oid was passed in		True
				Message Status is 'SENT_TO_BANK'
				isReply flag is not true

  MailMessage Rec From Bank	Message oid was passed in 		True
				Message Status is 'REC'
				Discrepancy Flag is set to 'N' or ''

  Discrepancy Notice Rec/Bank	Message oid was passed in		True
  'Not yet Started'		Message Status is 'REC'
				Discrepancy Flag is set to 'Y'
				Response Transaction oid is null

  Discrepancy Notice Rec/Bank	Message oid was passed in		True
  'Started'			Message Status is 'REC'
				Discrepancy Flag is set to 'Y'.
				Response Transaction oid is defined with a value

  New Response from Mail Msg	Message oid passed in is null		False
				isReply(url parm) is = 'true'
				Related Inst was passed in via url Parm
				subject was passed in via url Parm
				
  Response to Mail Message	Message oid passed in is null		False
				message isReply is defined as a reply (on object)
				Related Inst was passed in via url Parm
				subject was passed in via url Parm

  Draft Response to Message	Message oid passed in is null		False
				message status is 'DRAFT'
				isReply(url parm) is = 'true'
				Related Inst was passed in via url Parm
				subject was passed in via url Parm
*******************************************************************************
--%>

<%--
 *		Dev Owner: Sandeep
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.html.*, java.text.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"    	 scope="session"></jsp:useBean>

<%


	String newMailMessageDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   Debug.debug("***START******************** Mail Message Page *********************START***");

  //URL Parameter variables
  String 	reply 			= StringFunction.xssCharsToHtml(request.getParameter("isReply"));		//Reply button pressed
  String 	edit  			= StringFunction.xssCharsToHtml(request.getParameter("isEdit"));		//Edit button pressed
  String 	relatedInstID		= StringFunction.xssCharsToHtml(request.getParameter("relatedInstID"));
  String 	subject			= StringFunction.xssCharsToHtml(request.getParameter("subject"));
  String 	mailMessageOid 		= StringFunction.xssCharsToHtml(request.getParameter("mailMessageOid"));
  String	sequenceNumber 		= StringFunction.xssCharsToHtml(request.getParameter("sequenceNumber"));
  String instrumentSearchDialogTitle=resMgr.getText("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE);
  //cquinton 1/18/2013 remove old close action behabior


  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String	userOid			= userSession.getUserOid();
  String	corpOrgOid		= userSession.getOwnerOrgOid();
  String	clientBankOid		= userSession.getClientBankOid();
  String	goToMailMessageDetail	= "goToMailMessageDetail";
  Long		messageOid		= null;
  String 	messageMode		= TradePortalConstants.MESSAGE_CREATE;		
  //CR 375-D Krishna ATP-Response
  //String	instrumentOid;
  //String 	instrumentType;
  String	instrumentOid="";
  String 	instrumentType="";
  String 	instrumentTypeCode="";
  //CR 375-D Krishna ATP-Response
  String 	transactionStatus	= "";
  String	completeInstId		= "";
  String	displayInstrumentID	= "";
  int		stringLength		= 0;
  boolean       displayDiscrepancyReplyToBankButton = false;
  //CR-419 Krishna Begin
  TermsPartyWebBean    counterParty  = null;
  String    counterPartyOid          = null;
  String    counterPartyName         = null;
  //CR-419 Krishna End
  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParms		= new Hashtable();
  ClientServerDataBridge csdb 		= resMgr.getCSDB();
       
  String 	serverLocation          = JPylonProperties.getInstance().getString("serverLocation");

  MailMessage 	message;		//Business object - needed to do this since we needed to be able 
					//to update the un_read flag in the db with-in the jsp.
  InstrumentWebBean instrument		= beanMgr.createBean(InstrumentWebBean.class,  "Instrument");
  UserWebBean 	routedByUser		= beanMgr.createBean(UserWebBean.class, "User");
  TransactionWebBean transaction	= beanMgr.createBean(TransactionWebBean.class, "Transaction");

  //Message attribute related variables

  String	messageStatus		= "";	
  String	messageText		= "";
  //CR-419 Krishna Begin
  String	discrepancyText		     = "";
  String	presentationDate	     = "";
  String	poInvoiceDiscrepancyText     = ""; 
  //CR-419 Krishna End	
  String	discrepancyFlag		= "";	
  String	responseTransOid	= "";	
  String 	date;
  String 	amount;				//used on discrepancy pages
  String	currencyCode;			//used on discrepancy pages

  DocumentHandler docImageList = null;		//used to display a list of document Image 
						//links if the handler is non null.
  DocumentHandler myDoc;
  Vector listviewVector = null;			//list of returned records formatted for easy looping.

  //Protect against null values passed in from calling jsp(s) regarding Page Parameters

  if( relatedInstID == null ) 	relatedInstID 	= "";
  if( subject == null )  	subject		= "";
  if( mailMessageOid == null )  mailMessageOid	= "";

  //If the Message Oid was not passed in through the the URL - look for it
  //in the session since the create Transaction process may have put it there
  //during a reply to bank.  If it's there, we dont need to decrypt it - but 
  //we'll want to remove it from the session.

  if( InstrumentServices.isBlank(mailMessageOid) ){
	mailMessageOid = (String) session.getAttribute("mailMessageOid");

	if( InstrumentServices.isBlank( mailMessageOid ) ) {
		mailMessageOid	= defaultDoc.getAttribute("/In/MailMessage/message_oid");

		if( InstrumentServices.isBlank( mailMessageOid ) ){
			mailMessageOid	= defaultDoc.getAttribute("/In/mailMessageOid");

			if( InstrumentServices.isBlank( mailMessageOid ) )
		    				mailMessageOid = "";
		}
	}else{
		HttpSession theSession    = request.getSession(false);
		theSession.removeAttribute("mailMessageOid");
	}
		
  }else{
	mailMessageOid = EncryptDecrypt.decryptStringUsingTripleDes( mailMessageOid , userSession.getSecretKey());
  }

  //Build the Message object...
  if( InstrumentServices.isNotBlank( mailMessageOid ) )
  {
	messageOid 	 = new Long(mailMessageOid);

	message 	 = (MailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                	"MailMessage", messageOid.longValue(), csdb);

	responseTransOid = message.getAttribute("response_transaction_oid");
	discrepancyFlag  = message.getAttribute("discrepancy_flag");
  	messageStatus 	 = message.getAttribute("message_status");
	subject 	 = message.getAttribute("message_subject");
	messageText	 = message.getAttribute("message_text");
    //CR-419 Krishna Begin
    discrepancyText	         = message.getAttribute("discrepancy_text");
    presentationDate	     = message.getAttribute("presentation_date");
    poInvoiceDiscrepancyText = message.getAttribute("po_invoice_discrepancy_text"); 
    //CR-419 Krishna End
	sequenceNumber	 = message.getAttribute("sequence_number");
	messageMode	 = TradePortalConstants.MESSAGE_UPDATE;

  }else{
  	message 	 = (MailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                			  "MailMessage", csdb);
	message.newObject();
	messageOid	 = new Long( message.getAttribute("message_oid") );
	
  }

  //Need to populate the amount and CurrencyCode and set up for display.
  amount = message.getAttribute("discrepancy_amount");
  currencyCode = message.getAttribute("discrepancy_currency_code");

  if (InstrumentServices.isNotBlank(amount) && InstrumentServices.isNotBlank(currencyCode) )  
	amount = TPCurrencyUtility.getDisplayAmount(amount, currencyCode, loginLocale);

  //Build the Sql to get a list of Document Images from the Database.

  try{
  	if( InstrumentServices.isNotBlank( mailMessageOid ) ) {
  	    // W Zhu 7/26/07 EEUH0725555228 add image_format
  	    // [START] IR-EEUH072555228 - jkok - Added doc_name
  	    // cquinton 7/21/2011 Rel 7.1.0 ppx240 - add hash
  	    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
  	    String sql = "select doc_image_oid, form_type, image_id, image_format, doc_name, hash "
  	    // [END] IR-EEUH072555228 - jkok
  	    + "from document_image where mail_message_oid = ? and form_type <> ?"; //  IR-YRUH032953136 - jkok
	    Debug.debug("*** Sql == " + sql.toString() );
  	    docImageList = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, mailMessageOid, TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);
	    
	   if( docImageList != null ) {
	    	Debug.debug("*** Returned Resultset is: " + docImageList.toString() );

 	    	listviewVector = docImageList.getFragments("/ResultSetRecord/");
	   }
	}
  }catch(Exception e){
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
     	e.printStackTrace();
  }



  //boolean flags defined to help determine what should be displayed on the page (for each page)
  boolean 	isMessageOidNull	= InstrumentServices.isBlank(mailMessageOid);

  boolean	isNewReply;

  // Set the edit flag based on either a variable in the URL or
  // a value that was submitted last time in a hidden field
  // Having the edit flag set overrides some other read-only rules
  if(reply == null) 
   {
	if(defaultDoc.getAttribute("/In/isNewReply") != null)
		isNewReply=true;
	else
		isNewReply=false;
   }
  else
   {
	isNewReply = reply.equals("true");
   }


  boolean	isEdit;

  // Set the edit flag based on either a variable in the URL or
  // a value that was submitted last time in a hidden field
  // Having the edit flag set overrides some other read-only rules
  if(edit == null) 
   {
	if(defaultDoc.getAttribute("/In/isEdit") != null)
		isEdit=true;
	else
		isEdit=false;
   }
  else
   {
	isEdit = edit.equals("true");
   }

  boolean 	isCurrentUser		= message.getAttribute("assigned_to_user_oid").equals(userOid);
  boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.CREATE_MESSAGE );
  boolean	hasSendRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.SEND_MESSAGE );
  boolean	hasRouteRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.ROUTE_MESSAGE);
  boolean	hasDeleteRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.DELETE_MESSAGE);
  boolean   hasDelDocMsg            = SecurityAccess.hasRights(loginRights, SecurityAccess.DELETE_DOC_MESSAGE);
  boolean	hasEditDiscrepRights  	= SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_CREATE_MODIFY);
  boolean	hasRouteDiscrepRights	= SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_ROUTE);
  boolean 	hasDeleteDiscrepRights  = SecurityAccess.hasRights(loginRights,SecurityAccess.DISCREPANCY_DELETE);
  boolean	isUnAssigned		= InstrumentServices.isBlank(message.getAttribute("assigned_to_user_oid"));
  boolean 	isRouted		= InstrumentServices.isNotBlank(message.getAttribute("last_routed_by_user_oid"));
  boolean	isUsersOrgOid;
  boolean	isDiscrepancy;
  boolean	isStatus		= InstrumentServices.isBlank( messageStatus );
  boolean 	messageIsReply;	
  boolean	isMsgFromBank;								    //message.Message_Source_Type
  boolean	isResponse		= InstrumentServices.isNotBlank( responseTransOid );  //Response from -Bank-
  boolean	isRelatedInst		= InstrumentServices.isBlank( relatedInstID );	    //is : Related Inst passed in
  boolean	isReadOnly		= true;
  boolean       messageCanSendToBank    = false;

  MediatorServices mediatorServices	= null;						    //Populate Error section
  String	messageSourceType	= message.getAttribute("message_source_type");
  String	headerPrefix		= "";
  String	headerSuffix		= "";
  String	headerSuffixDate		= "";
					//Either equals: "Close", "Cancel New Repsonse", or "Cancel New Message"
  String 	cancelFlag		= "common.CloseText";	
  String	cancelImg		= "common.CloseImg";
  String	cancelWidth		= "";

  //Since these variables will be used repetitively, do a check for null values.
  // if the variables are null, we default them here to a value.
  secureParms.put("last_entry_user_oid", message.getAttribute("last_entry_user_oid") );

  //If the message object has been flagged as deleted, then don't allow the user to edit 
  // the message.  Rather populate an error in the xml doc and store in the doc cache so
  // that when the header.jsp is included, it will be able to get the data from the cache
  // to populate the screen with the info.
  // This could happen if the message was updated by another user prior to this user pressing save.

  if( message.isDeleted() ) {
	Debug.debug("***** Message is deleted - Can't Edit it ****** ");
	isEdit = false;
	mediatorServices = new MediatorServices();
        mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
	mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
      mediatorServices.addErrorInfo();

      defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
      formMgr.storeInDocCache("default.doc", defaultDoc);
	}
 
  //Default value- 
  instrumentOid = message.getAttribute("related_instrument_oid");

  //If we are coming from the instrument Summary page, then the instrumentOid
  //was placed onto the session there.

  //cquinton 1/18/2013 remove old close action behabior

  if (defaultDoc.getDocumentNode("/In/InstrumentSearchInfo/InstrumentData") != null) {

     // We have returned from the instrument search page.  Based on the returned 
     // data, retrieve the complete instrument ID of the selected instrument to 
     // populate the Related Instrument ID in the General section
     String instrumentData = defaultDoc.getAttribute("/In/InstrumentSearchInfo/InstrumentData");

     instrumentOid  = InstrumentServices.parseForOid(instrumentData);
     completeInstId = InstrumentServices.parseForInstrumentId(instrumentData);
  }

  if( InstrumentServices.isNotBlank( instrumentOid ) ) {
	
	instrument.setAttribute("instrument_oid", instrumentOid );
  	instrument.getDataFromAppServer();
	instrumentType = instrument.getAttribute("instrument_type_code");
	instrumentTypeCode=instrumentType; //rkrishna CR 375-D ATP 08/22/2007

	instrumentType = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.INSTRUMENT_TYPE, instrumentType);

		
	completeInstId = instrument.getAttribute("complete_instrument_id");

	if( InstrumentServices.isNotBlank( message.getAttribute("sequence_number") ) ) {

		sequenceNumber = message.getAttribute("sequence_number");
		displayInstrumentID = completeInstId + "-" + sequenceNumber;

	}else{
		displayInstrumentID = completeInstId;
	}
  }else if ( isNewReply && InstrumentServices.isNotBlank(relatedInstID) ) {
	if( InstrumentServices.isNotBlank(sequenceNumber)) {
		displayInstrumentID = relatedInstID + "-" + sequenceNumber;
	}else{
		displayInstrumentID = relatedInstID;
	}
	completeInstId = relatedInstID;
  }
   //CR-419 Krishna Begin
   // Retrieve the name of the counter party associated with the instrument
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!InstrumentServices.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

      counterParty.getById(counterPartyOid);

      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }
   //CR-419 Krishna End

//If the subject is not defined from a EJB nor the Url Parameter - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/message_subject") != null )
  {
	subject = defaultDoc.getAttribute("/In/MailMessage/message_subject");
  }

   //CR-419 Krishna Begin
   //If the  Presentation Date is not defined from a EJB - Check the Xml Doc
   //Maybe it was returned from a page call that came back to this page from earlier.
    if( defaultDoc.getAttribute("/In/MailMessage/presentation_date") != null )
   {
	presentationDate = defaultDoc.getAttribute("/In/MailMessage/presentation_date");
   }

 //If the  DiscrepancyText is not defined from a EJB - Check the Xml Doc
 //Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/discrepancy_text") != null )
   {
	discrepancyText = defaultDoc.getAttribute("/In/MailMessage/discrepancy_text");
   }
  //If the  POInvoiceDiscrepancyText is not defined from a EJB - Check the Xml Doc
  //Maybe it was returned from a page call that came back to this page from earlier.
  if( defaultDoc.getAttribute("/In/MailMessage/po_invoice_discrepancy_text") != null )
   {
	poInvoiceDiscrepancyText = defaultDoc.getAttribute("/In/MailMessage/po_invoice_discrepancy_text");
   }
  //CR-419 Krishna End

//If the Message Text is not defined from a EJB - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( defaultDoc.getAttribute("/In/MailMessage/message_text") != null )
   {
	messageText = defaultDoc.getAttribute("/In/MailMessage/message_text");
   }

//If the Instrument ID is not defined from a EJB nor the Url Parameter - Check the Xml Doc
//Maybe it was returned from a page call that came back to this page from earlier.

  if( InstrumentServices.isBlank(completeInstId) &&
      (defaultDoc.getAttribute("/In/Instrument/complete_instrument_id") != null)) {
		Debug.debug("************** Made it HERE 5 *************");
	completeInstId = defaultDoc.getAttribute("/In/Instrument/complete_instrument_id");
	displayInstrumentID = completeInstId;
  }


  if( InstrumentServices.isBlank(message.getAttribute("is_reply")) ||
      message.getAttribute("is_reply").equals(TradePortalConstants.INDICATOR_NO ) ) {
				messageIsReply = false;
  }else{			messageIsReply = true;
				messageMode = TradePortalConstants.MESSAGE_UPDATE;
  }

  if( InstrumentServices.isBlank(discrepancyFlag) ||
      discrepancyFlag.equals(TradePortalConstants.INDICATOR_NO) )
				isDiscrepancy = false;
  else				isDiscrepancy = true;

  if( InstrumentServices.isBlank(messageSourceType) ||
      messageSourceType.equals(TradePortalConstants.PORTAL) )
				isMsgFromBank = false;
  else				isMsgFromBank = true;

  if( InstrumentServices.isBlank(message.getAttribute("assigned_to_corp_org_oid")) ||
      !corpOrgOid.equals(message.getAttribute("assigned_to_corp_org_oid")) )
				isUsersOrgOid = false;
  else				isUsersOrgOid = true;


  //We need to determine if the page needs to display the text as readOnly or as editable data.
  //Consequently there are a set of rules with which to determine this...

  //If the Message originated in OTL -OR- The Message is a Discrepancy -OR- 
  //User doesn't have right to edit/create messages set Page into ReadOnly mode
  //*** Then the read only mode is already defaulted to true ***

									//If the message is assigned to the 
									//current user -OR- is unassigned and 
									//is owned by the users corporation.
  if(messageSourceType.equals( TradePortalConstants.BANK ) ||
     !SecurityAccess.hasRights(loginRights,SecurityAccess.CREATE_MESSAGE) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {		

	 isReadOnly = true;						//If the message is assigned to the 
									//current user -OR- is unassigned and 
									//is owned by the users corporation.
  }else if( isCurrentUser || (isUnAssigned && isUsersOrgOid) || 
	    isEdit || isMessageOidNull) {		
									
	 isReadOnly = false;
  }

  //IR - LRUH011547224 - BEGIN
  //Below conditions will determine whether to display "Send To Bank" button or not
  if(messageSourceType.equals( TradePortalConstants.BANK ) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_BANK_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {		

	 	messageCanSendToBank = false;						
	 
  }else if( isCurrentUser || (isUnAssigned && isUsersOrgOid) || 
	    isEdit || isMessageOidNull) {		
									
	 	messageCanSendToBank = true;
  }
  //IR - LRUH011547224 - END


  if( isRouted ) {
	routedByUser.setAttribute("user_oid", message.getAttribute("last_routed_by_user_oid"));
	routedByUser.getDataFromAppServer();
  }


  if( isResponse ) {
	transaction.setAttribute("transaction_oid", responseTransOid);
	transaction.getDataFromAppServer();
	transactionStatus = transaction.getAttribute("transaction_status");
  }

  // PLUI070959610 begin
  if ( isDiscrepancy ) {
      if( !isResponse || (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_DELETED) )) {
               displayDiscrepancyReplyToBankButton = true;
      }
  }
  // PLUI070959610 end

  //Check to see if errors were returned in the document, if so - 
  //Populate the variables with values from the xml document and NOT from the EJB.


  if( InstrumentServices.isBlank( sequenceNumber ) )
	sequenceNumber = "";

  //Set up the secure parameters....
	//This set of parameters is set up for the Transaction creation (Reply to Bank)
	//The textfields have a higher order of precedence than the secureParms does in 
	//the formMgr.getFormInstanceAsInputField call.  So if a field is updated - or
	// NOT in read only mode than the value from that field will be what is passed
	//to the mediator if both the input field and the secureParm share the same 
	//mapping in the Mapfile...
  secureParms.put( "instrumentOid", instrumentOid + "/");
  secureParms.put( "mode", TradePortalConstants.EXISTING_INSTR );
  secureParms.put( "clientBankOid", clientBankOid );
  secureParms.put( "userOid", userOid );
  secureParms.put( "ownerOrg", corpOrgOid );
  secureParms.put( "securityRights", loginRights );
  secureParms.put( "mailMessageOid", mailMessageOid );
  secureParms.put( "startPage", "MailMessageDetail" );
  //rkrishna CR 375-D ATP 07/26/2007 Begin
  if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY))
  secureParms.put( "transactionType", TransactionType.APPROVAL_TO_PAY_RESPONSE );
  else
  //rkrishna CR 375-D ATP 07/26/2007 End
  secureParms.put( "transactionType", TransactionType.DISCREPANCY );
  secureParms.put( "validationState", TradePortalConstants.VALIDATE_NOTHING );
  secureParms.put( "MessageMode", messageMode);
  secureParms.put( "timeZone", loginTimezone);
  secureParms.put( "user_oid", userOid);
  secureParms.put( "message_oid", mailMessageOid);
  secureParms.put( "complete_instrument_id", completeInstId);
  secureParms.put( "sequence_number", sequenceNumber);

  //Document Image link related Variables.
  
  String 	urlParm;
  String	attribute;
  String	displayText;
  String	encryptedMailMessageOid;
  String	encryptVal2;
  String	encryptVal3;

  //cquinton 9/15/2011 Rel 7.1 ppx240
  encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid , userSession.getSecretKey());
  secureParms.put("MessageOid", encryptedMailMessageOid);
  secureParms.put("fromMessages", TradePortalConstants.INDICATOR_YES);
  secureParms.put("SecurityRights", loginRights);
  secureParms.put("UserOid", userOid);



  if( isNewReply ) {
  	 secureParms.put( "is_reply", TradePortalConstants.INDICATOR_YES);
  	 secureParms.put( "assigned_to_corp_org_oid", message.getAttribute("assigned_to_corp_org_oid"));
  }else{ 
	 secureParms.put( "is_reply", TradePortalConstants.INDICATOR_NO);
	 secureParms.put( "assigned_to_corp_org_oid", corpOrgOid);
  }
  


  userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
%>

<%-- ********************************** HTML STARTS HERE **************************************** --%>

<jsp:include page="/common/ButtonPrep.jsp" />


<script language="JavaScript">

   function confirmDocumentDelete()
   {
      var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", 
                                               TradePortalConstants.TEXT_BUNDLE) %>";
      
      <%-- Find any checked checkbox and flip a flag if one is found --%>
      var isAtLeastOneDocumentChecked = Boolean.FALSE;      
      if (document.forms[0].AttachedDocument != null) {
         if (document.forms[0].AttachedDocument.length != null) {
            for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
               if (document.forms[0].AttachedDocument[i].checked==true) {
                  isAtLeastOneDocumentChecked = true;
               }
            }
         }
         else if (document.forms[0].AttachedDocument.checked==true) {
            isAtLeastOneDocumentChecked = true;
         }
      }
      
      if (isAtLeastOneDocumentChecked==true) {
         if (confirm(confirmMessage)==false)
         {
            <%-- Uncheck any checked checkboxes --%>
            if (document.forms[0].AttachedDocument != null) {
               if (document.forms[0].AttachedDocument.length != null) {
                  for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
                     if (document.forms[0].AttachedDocument[i].checked==true) {
                     document.forms[0].AttachedDocument[i].checked = false;
                     }
                  }
               }
               else if (document.forms[0].AttachedDocument.checked==true) {
                  document.forms[0].AttachedDocument.checked = false;
               }
            }            
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = false;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return false;
         }
         else
         {
            <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
            formSubmitted = true;
            <%-- [END] IR-YRUH032953136 - jkok --%>
            return true;
         }
      }
      else {
         <%-- [BEGIN] IR-YVUH032339608 - jkok --%>
         alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE) %>");
         <%-- [END] IR-YVUH032339608 - jkok --%>
         <%-- [BEGIN] IR-YRUH032953136 - jkok --%>
         formSubmitted = false;
         <%-- [END] IR-YRUH032953136 - jkok --%>
         return false;
      }
  }


</script>

	<div class="dialogContent">
	
				<div> 	
					<%-- This control is set up to display the complete Instrument ID WITH the sequence number. --%>
					<%-- But since the seqeunce number is a seperate piece of data, it is NOT included in the   --%>
					<%-- complete instrument id which we send in secureParms that is also mapped to the same    --%>
					<%-- mapfile entry as this control.							    							--%>	
					
					<%
						if( messageStatus.equals(TradePortalConstants.SENT_TO_BANK) || (isMessageOidNull && !isNewReply)){	
							if( isNewReply ){
				        		String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
				            	if(!subject.startsWith(re)){
				   		    		subject = re + "  " + subject;
				           		}
				            	if(subject.length() > 60){
				                	subject = subject.substring(0, 56) + "...";
				            	}
		
								// Add to secure parms since the field will be read only
								secureParms.put("message_subject", subject);
							}
				      		if( messageIsReply || isReadOnly ){
								// Add to secure parms since the field will be read only
								secureParms.put("message_subject", subject);
				      		}
					%>
						<%= widgetFactory.createTextField("message_subject", "Message.Subject", StringFunction.xssCharsToHtml(subject),
							"80", isReadOnly || isNewReply || messageIsReply, !isReadOnly, false, "class='char40'", "", "") %>
					<%	
						}
					
						 String instruButton = "";
                         String ItemID1="complete_instrument_id";
                         String section1="InstrumentId";
			 //Rel9.0 Adding instrument type 'PYB' to the instrument search dialog
                         instruButton = widgetFactory.createInstrumentSearchButton(ItemID1,section1,isReadOnly,TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES, false);
                         String instrId = "";
						if( isReadOnly ) 
							instrId = "Message.RelatedToInstrumentID";
						if( !isReadOnly ) 
							instrId = "Message.ReferenceAnInstrumentID";
					%>
					<%= widgetFactory.createTextField("complete_instrument_id", instrId, displayInstrumentID, 
						"19", (isReadOnly || isNewReply || messageIsReply), false, false, "", "", "inline",instruButton) %>
						
					<% secureParms.put( "complete_instrument_id", completeInstId); %>
					
					<%-- Search Instruments button --%>
					<% if( !isReadOnly && !isNewReply && !messageIsReply ) { %>		
 		     			<input type="hidden" name="NewSearch" value="Y">

						<% // Send flag indicating that user is able to view messages of their children
			             	if(SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_CHILD_ORG_MESSAGES )){
						%>
			 		     	<input type="hidden" name="ShowChildInstruments" value="<%=TradePortalConstants.INDICATOR_YES%>">
						<%}%>
						
					<%}%>
		
					<%-- Discrepancy Amount --%>
					<%if( isDiscrepancy ) { %>
						<%= widgetFactory.createTextField("PresentationAmount", "MailMessage.PresentationAmount", currencyCode + "   " + amount, 
							"35", isReadOnly, false, false, "", "", "inline") %>
					<%}%>
					
					<%-- Routed by name --%>
					<% if( isReadOnly && isRouted ) { %>
						<%= widgetFactory.createTextField("RoutedToYouBy", "Message.RoutedToYouBy", routedByUser.getAttribute("first_name") + "  " + routedByUser.getAttribute("last_name"), 
							"35", true, false, false, "", "", "inline") %>
					<% } if( (isDiscrepancy && isRouted) || (!isReadOnly && isRouted ) ) { %> 
						<%= widgetFactory.createTextField("RoutedToYouBy", "Message.RoutedToYouBy", routedByUser.getAttribute("first_name") + "  " + routedByUser.getAttribute("last_name"), 
							"35", true, false, false, "", "", "inline") %>
					<% } %>
					<%if( isDiscrepancy ) { %>
						<%= widgetFactory.createTextField("OtherParty", "MailMessage.OtherParty", counterPartyName, 
							"35", isReadOnly, false, false, "", "", "inline") %>
					<% } %>
					<div style="clear: both;"></div>
				</div>
				<div>
					<% if( isDiscrepancy ) { %>
						<%= widgetFactory.createTextField("PresentationNumber", "MailMessage.PresentationNumber", sequenceNumber, 
							"35", isReadOnly, false, false, "", "", "inline") %>
			
<%
							 //As "PresentationDate" is a newly added attribute empty check is being done here to avoid exceptions
							 //for the existing messages in the system.     
							 if(InstrumentServices.isNotBlank(presentationDate)){
							  Date presentationDateObj = new Date(presentationDate.substring(0,10));
							  presentationDate = TPDateTimeUtility.formatDate(presentationDateObj , loginLocale);
							 }
%>				
						<%= widgetFactory.createTextField("PresentationDate", "MailMessage.PresentationDate", presentationDate, 
							"35", isReadOnly, false, false, "", "", "inline") %>
					<% } %>
					<div style="clear: both;"></div>
				</div>
	
<%
			     /***************************
			      * Document Image Links
			      **************************/   
			      Debug.debug("*** Document Image Links ***");
%>
			    
				<% if( (docImageList != null) && (listviewVector != null) ) { %>
					<br/>
					<%= widgetFactory.createLabel("", "Message.RelatedDocuments", false, false, false, "") %>
<%
					  	int liTotalDocs = listviewVector.size();
					  	int liDocCounter = liTotalDocs;
					  	String  displayTextPDF = "";
						encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
						for(int y=0; y < ((liTotalDocs+2)/3); y++) {
			    			for (int x=0; x < 3 ; x++) {
			      				if (liDocCounter==0) break;
			      				
			       	 			myDoc       = ((DocumentHandler)listviewVector.elementAt((y*3)+x));
						        String imageHash = myDoc.getAttribute("/HASH");
						        String encryptedImageHash = "";
			        
						        if ( imageHash != null && imageHash.length()>0 ) {
			          				encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
			        			} else {
							    	//send something rather than nothing, which ensures we know we are not allowing
							        // attempts to view images when hash parm is just forgotten
							        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
							    }
			
						        // Display doc conditional on the format
						        String imageFormat = myDoc.getAttribute("/IMAGE_FORMAT");
						        
						        if (imageFormat.equals(TradePortalConstants.PDF_IMAGE)) {
									//Use correct display text depending on IMAGE_FORMAT
									displayText = myDoc.getAttribute("/DOC_NAME");
									
									//add hash parm
									urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
%>
									<div class="formItem">
										<%= widgetFactory.createSubLabel(displayText+" ") %>
							       		<a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'>Image</a>
							       		<%= widgetFactory.createSubLabel(" | ") %>
<%
        						}else {
									//Use correct display text depending on IMAGE_FORMAT
									displayText = getDisplayText( myDoc );
          
									//add hash parm
									urlParm = buildURLParameters( listviewVector, myDoc, encryptedMailMessageOid, subject, displayText, encryptedImageHash, userSession );
%>
									<div class="formItem">
										<%= widgetFactory.createSubLabel(displayText+" ") %>
							        	<a href="<%= formMgr.getLinkAsUrl("goToDocumentView", urlParm, response)   %>">Image</a>
							        	<%= widgetFactory.createSubLabel(" | ") %>
<%
				          			displayTextPDF = resMgr.getText("Message.getImage", TradePortalConstants.TEXT_BUNDLE);
							        //add imageHash parm
			          				urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
%>
        						<a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'><%= displayTextPDF %></a>
        					</div> 
<%			
						}//end else 
      					liDocCounter--;
    				}//end inner for "x"
    			
    				if (liDocCounter==0) {
      				for (int z=liTotalDocs % 3; z > 0; z--) {}      
  					}
    			}//end outer for "y"
			} //Closes off If we have associated documents	
%>

<%
		     /***************************
		      * Subject
		      **************************/                 
		      Debug.debug("*** Subject ***");
			/* If this message is a reply to an earlier message, then we need to append a 'Re : ' to
			   the front the subject.  And if in doing this the subject exceeds 60 characters in length
		 	   then we need to truncate the excess off and add an ellipsis
			*/

			if( isNewReply ){
        		String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
            	if(!subject.startsWith(re)){
   		    		subject = re + "  " + subject;
           		}
            	if(subject.length() > 60){
                	subject = subject.substring(0, 56) + "...";
            	}

				// Add to secure parms since the field will be read only
				secureParms.put("message_subject", subject);
			}
      		if( messageIsReply || isReadOnly ){
				// Add to secure parms since the field will be read only
				secureParms.put("message_subject", subject);
      		}
			
      		if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) && !(isMessageOidNull && !isNewReply)){
			// Display subject in read only if page is in read only mode, reply is being created, or message has been saved and is a reply 
		%>
			<%= widgetFactory.createTextField("message_subject", "Message.Subject", subject, 
				"100", isReadOnly || isNewReply || messageIsReply, !isReadOnly, false, "style='width:420px;'", "", "") %> 
		<%			
      		}
		     /***************************************
		      * Message Text : Multi Line Text Area
		      **************************************/                 
		      Debug.debug("*** Message Text : Multi Line Text Area ***");
			
			// Sandeep IR -T36000003310  07/17/2012 Start 
		%>
			<%@ include file="/messages/fragments/Messages-Documents.frag" %>
			
			<%= widgetFactory.createTextArea("message_text", "Message.Message", messageText, isReadOnly, false, false, "maxlength='4000' rows='12'", "", "") %>
		<%
			//Sandeep IR -T36000003310  07/17/2012 End
			
		     /***************************************
		      * Message Text : Multi Line DiscrepancyText Area
		      **************************************/                 
		      Debug.debug("*** Message Text : Multi Line DiscrepancyText Area Area ***");
    
    		if( isDiscrepancy ) { 
		%>
				<%= widgetFactory.createTextArea("discrepancy_text", "MailMessage.ExamDiscrepancyText", discrepancyText, isReadOnly) %>
		<%
		     /***************************************
		      * Message Text : Multi Line POInvoiceDiscrepancyText Area
		      **************************************/                 
		      Debug.debug("*** Message Text :Multi Line POInvoiceDiscrepancyText Area ***");
		%>
				<%= widgetFactory.createTextArea("po_invoice_discrepancy_text", "MailMessage.POInvoiceDiscrepancyText", poInvoiceDiscrepancyText, isReadOnly) %>
<% 			}//end of discrepancy 
     
			/***************************************
		      * Discrepancy Response Status
		      **************************************/                 
      		Debug.debug("*** Discrepancy Response Status ***");

			String respText = "";
			if( isDiscrepancy ) {
	    		if (instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY)) { 
	    			respText = "Message.ATPResponseStatus";
	    		} else {
	    			respText = "Message.DiscrepancyResponseStatus";
		  		}

				//If this discrepancy does not have a related Response Transaction Oid -OR-
				//the message is flagged as deeted then display the fact that the message is
				//not in a started state, otherwise we display the message status.
		
				//PLUI070959610 Move the setting of displayDiscrepancyReplyToBankButton to the top
     			if (displayDiscrepancyReplyToBankButton) {
     				String respTextStatus = resMgr.getText("Message.NotYetStarted", TradePortalConstants.TEXT_BUNDLE);	
%>
					<%= widgetFactory.createTextField(respText, respText, respTextStatus, "100", true) %>
<%				}else{ %>
					<%= widgetFactory.createTextField(respText, respText, 
						ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_STATUS, 
						transactionStatus,loginLocale), "100", true) %>	
<% 				}
			}  // closes if isDiscrepancy
%>			
			
				
<%	    
			secureParms.put("opt_lock", message.getAttribute("opt_lock"));
	
		    // Put the isEdit flag into the input document so that after
		    // the edit button is pressed subsequent pages (until final submit)
		    // will also be in edit mode
		    if(isEdit)
				secureParms.put("isEdit", "true");
			if(isNewReply)
	            secureParms.put("isNewReply", "true");
%>
		
		
	
	
	<%@ include file="/dialog/fragments/NewMailMessageButtons.frag" %>
	
	
</div> <%--closes dialogContent area--%>
<div id="instrumentSearchDialog"></div>

<%
   Debug.debug("Default Doc data is: " + defaultDoc.toString() );

   if( (isCurrentUser || (isUnAssigned && isUsersOrgOid )) &&
       !(message.getAttribute("unread_flag")).equals(TradePortalConstants.INDICATOR_NO) ) {

	message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
   	message.save();						    //This may increment opt_lock
  }								    //This will ensure the mediator
   								    //has the right numeric value.
  message.remove();

   Debug.debug("***END******************** Mail Message Page *********************END***");
%>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   defaultDoc.removeAllChildren("/");
   formMgr.storeInDocCache("default.doc", defaultDoc);
%>
<%
Hashtable routeMessagesSecParms = new Hashtable();
routeMessagesSecParms.put("UserOid", userSession.getUserOid());
routeMessagesSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

<form method="post" name="RouteMessagesForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="RouteUserOid" />
<input type="hidden" name="RouteCorporateOrgOid" />
<input type="hidden" name="RouteToUser"/>
<input type="hidden" name="RouteToOrg" />
<input type="hidden" name="multipleOrgs" />
<input type="hidden" name="routeAction"  />
<input type="hidden" name="fromListView" />

<%= formMgr.getFormInstanceAsInputField("RouteMessagesForm",routeMessagesSecParms) %>
</form>

<div id="routeDialog" style="display:none;"></div>
<%!
   // Various methods are declared here (in alphabetical order).

   // This method helps build the url parameters when creating the document 
   // image links in the 'for' loop.  This just localizes code that is used 
   // twice in that same loop (for multiple table column reasons).

   // Parameter List : 
   //	Vector		listviewVector		-List of document image(s) from the Db.
   //	DocumentHandler	myDoc			-handle to the actual data for each image.
   //	int		x			-Counter from loop.
   //	String		encryptedMailMessageOid	-Contains previously encrypted MailMessageOid.
 
   //cquinton 7/21/2011 Rel 7.1.0 ppx240 - add imageHash
   public String buildURLParameters (Vector listviewVector, DocumentHandler myDoc, String encryptedMailMessageOid, 
                                     String subject, String displayText, String encryptedImageHash, SessionWebBean userSession ) {

      String urlParm = "";

      String imageId = myDoc.getAttribute("/IMAGE_ID");
      String encryptedImageId = EncryptDecrypt.encryptStringUsingTripleDes( imageId , userSession.getSecretKey());
       
      String encryptedDisplayText = EncryptDecrypt.encryptStringUsingTripleDes( displayText, userSession.getSecretKey() );

      urlParm    = "&image_id=" + encryptedImageId + 
                   "&hash=" + encryptedImageHash +
                   "&formType=" + encryptedDisplayText +
                   "&fromMessages=Y" + "&messageSubject=" + subject +
                   "&mailMessageOid=" + encryptedMailMessageOid;

      return urlParm;
   }


   //This method only focuses on building a display value of the formtype.  The returned
   //result will be used as the link text.

   public String getDisplayText (DocumentHandler myDoc) {
	String attribute;
	String displayText = "";
	
	try {
	   attribute   = myDoc.getAttribute("/FORM_TYPE");
	   displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.FORM_TYPE,
						                 	attribute);
	}catch(AmsException e) {		//This should never happen
		Debug.debug("*********** Call to Refdata Mgr failed to get description for a documemt image. **********");
		Debug.debug("***** The problematic FormType is : " + myDoc.getAttribute("/FORM_TYPE") );
	}

	return displayText;
   }
%>

<script type="text/javascript">

function clickButton(buttonName){
	 require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
		        function(registry, query, on, dialog ) {		 
		 var instrumentID = registry.byId("complete_instrument_id").value;
		 var messageSubject = registry.byId("message_subject").value;
		 var messageText = registry.byId("message_text").value;
		 dialog.doCallback('<%=StringFunction.escapeQuotesforJS(newMailMessageDialogId)%>', 'select',buttonName,instrumentID,messageSubject,messageText);
	 });
}

function openURL(URL){
	 document.location.href  = URL;
	 return false;
}
<%--UAT IR T36000006223 --%>
function closeMMDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(newMailMessageDialogId)%>');
    });
  }
  function SearchInstrument(identifierStr, sectionName , InstrumentType) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%> 
    itemid = String(identifierStr);
      section = String(sectionName);
      IntType=String(InstrumentType);
      
              
    require(["t360/dialog"], function(dialog ) {
      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>',
                  'instrumentSearchDialogID.jsp',
                  ['itemid','section','IntType'], [itemid,section,IntType], <%-- parameters --%>
                  'select', null);
     
    });
  }
  

  var xhReq;
  function routeMail(){
	  require(["dijit/registry"],
			    function(registry ) {
		  		<%-- XHR request will give a call to the Dummy JSP and perform the save of the message --%>
	  				if (!xhReq) xhReq = getXmlHttpRequest();
	  				 	var instrumentID = registry.byId("complete_instrument_id").value;
	  				 	var messageSubject = registry.byId("message_subject").value;
	  				 	var messageText = registry.byId("message_text").value;
	  				 	if(messageSubject == "" || messageSubject == null){
	  				 		alert("<%=resMgr.getText("Message.Warning1", TradePortalConstants.TEXT_BUNDLE)%>");
	  				 	}else{
			  				var req = "subject=" + messageSubject + "&text="+messageText+"&complete_instrument_id="+instrumentID;
			  				
			  				xhReq.open("GET", '/portal/transactions/PerformSave.jsp?'+req, true);
			  				
			  				xhReq.onreadystatechange = checkStatus;
			  				xhReq.send(null);
	  				 	}
	  			  });}

  <%-- Fuction used to create XHR request --%>
  function getXmlHttpRequest()
  {
    var xmlHttpObj;
    if (window.XMLHttpRequest) {
      xmlHttpObj = new XMLHttpRequest();
    }
    else {
      try {
        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
          xmlHttpObj = false;
        }
      }
    }
    console.log(xmlHttpObj);
    return xmlHttpObj;
  }

<%-- Based on the result of the XHR request, if its success- open the Route dialog, or submit the form.   --%>
function checkStatus(){
	require(["dijit/registry"], function(registry){
	 if (xhReq.readyState == 4 && xhReq.status == 200) {
		var response_Value = JSON.parse(xhReq.responseText);
		
    	var message_oid = response_Value.oid;
    	var ifInsert = response_Value.flag;
    	
    	
    	
    	if(ifInsert == 0) {
    		
    		var buttonName = '<%=TradePortalConstants.BUTTON_ROUTE%>';
  			<%String dialogName = resMgr.getText("routeDialog.title", TradePortalConstants.TEXT_BUNDLE);%>
  			var dialogName = '<%=dialogName%>';    	 	
    	 	var messageSubject = registry.byId("message_subject").value;
	    	require(["t360/dialog"], function(dialog) {
		 	      dialog.open('routeDialog', dialogName,
			                      'routeDialog.jsp',
			                      ['fromListView','fromWhere','rowKey','gridDisplayValue1'], ['<%=TradePortalConstants.INDICATOR_NO%>','<%=TradePortalConstants.FROM_MESSAGES%>',message_oid,messageSubject], <%-- parameters --%>
			                      'select', this.routeSelectedItems);
			  });
	 	}
    	else {
    		require(["t360/common"], function(common) {
    			if ( document.getElementsByName('MessagesDetailPopupForm').length > 0 ) {
    				
    		 		var instrumentID = registry.byId("complete_instrument_id").value;
    				var messageSubject = registry.byId("message_subject").value;
    				var messageText = registry.byId("message_text").value;
    				var theForm = document.getElementsByName('MessagesDetailPopupForm')[0];
    				theForm.message_subject.value = messageSubject;
    				theForm.message_text.value = messageText;
    				theForm.complete_instrument_id.value = instrumentID;
    				theForm.buttonName.value = buttonName;
    				theForm.is_reply.value='N';
    				common.setButtonPressed(buttonName,0);
    				
    				theForm.submit();
    			}	
    		});    		
    	}
      } 
	});
  }  
  
 
  function routeSelectedItems(routeToUser, routeUserOid, routeCorporateOrgOid, multipleOrgs, rowKey){
	
	
		 if ( document.getElementsByName('RouteMessagesForm').length > 0 ) {
		        var theForm = document.getElementsByName('RouteMessagesForm')[0];
		        theForm.RouteUserOid.value=routeUserOid;
		        if(routeCorporateOrgOid != ""){
		        	theForm.RouteCorporateOrgOid.value=routeCorporateOrgOid;
		        }
		        theForm.RouteToUser.value=routeToUser;
		        theForm.multipleOrgs.value=multipleOrgs;
		        theForm.routeAction.value='<%=TradePortalConstants.INDICATOR_YES%>';
		        theForm.fromListView.value='<%=TradePortalConstants.INDICATOR_YES%>';
		        
		        var buttonName = 'RouteAndSave';
		   	    submitFormWithParms("RouteMessagesForm", buttonName, "checkbox", rowKey);
		        
		 }
	} 
</script>
