<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.html.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	int tLoop; 
	boolean isReadOnly = false;
	boolean isTemplate = false;
	  
	final String[] instrumentCatagory = {
			InstrumentType.AIR_WAYBILL, InstrumentType.APPROVAL_TO_PAY, InstrumentType.BILLING,
			InstrumentType.CLEAN_BA, InstrumentType.NEW_EXPORT_COL, InstrumentType.EXPORT_BANKER_ACCEPTANCE, 
			InstrumentType.EXPORT_COL, InstrumentType.EXPORT_DEFERRED_PAYMENT, InstrumentType.EXPORT_DLC, 
			InstrumentType.INDEMNITY, InstrumentType.EXPORT_TRADE_ACCEPTANCE, InstrumentType.IMPORT_BANKER_ACCEPTANCE,
			InstrumentType.IMPORT_COL, InstrumentType.IMPORT_DEFERRED_PAYMENT, InstrumentType.IMPORT_DLC, 
			InstrumentType.IMPORT_TRADE_ACCEPTANCE, InstrumentType.INCOMING_GUA, InstrumentType.INCOMING_SLC,
			InstrumentType.FUNDS_XFER, InstrumentType.LOAN_RQST, InstrumentType.GUARANTEE, 
			InstrumentType.STANDBY_LC, InstrumentType.PAYABLES_MGMT, InstrumentType.DOMESTIC_PMT, 
			InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, InstrumentType.REFINANCE_BA, 
			InstrumentType.REQUEST_ADVISE, InstrumentType.SHIP_GUAR, InstrumentType.SUPPLIER_PORTAL,
			InstrumentType.XFER_BET_ACCTS, InstrumentType.MESSAGES, InstrumentType.SUPP_PORT_INST_TYPE, 
			InstrumentType.HTWOH_INV_INST_TYPE};
	  
	  final String [] AIR_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "PAY", "REA", "REL"};
	  final String [] ATP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA"};//1 EXT
	  final String [] BIL_TransArray = {"ARU", "ADJ", "BCH", "BCA", "PCS", "PCE", "DEA", "BNW","REA", "RED", "RVV"};
	  final String [] CBA_TransArray = {"ARU", "ADJ", "BUY", "CHG", "PCS", "PCE", "CUS", "LIQ"};//3 BUY PCS PCE
	  
	  final String [] EXP_OCO_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
	  final String [] EXP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  final String [] EXP_COL_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
	  final String [] EXP_DFP_TransArray = { "ARU", "ADJ", "BUY",  "CHG", "CUS", "LIQ"};
	  
	  final String [] EXP_DLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] LOI_TransArray = {"ARU", "ADJ", "AMD", "CHG", "CRE", "DEA", "EXP", "PAY", "REA"};//2 DEA EXT
	  final String [] EXP_TAC_TransArray = { "ARU", "ADJ",  "BUY",  "CHG", "CUS", "LIQ"};
	  final String [] IMP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"}; 
	  
	  final String [] IMC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "COL", "NAC", "PAY", "REA"};//DEA
	  final String [] IMP_DFP_TransArray = {"ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  final String [] IMP_DLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] IMP_TAC_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  
	  final String [] INC_GUA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] INC_SLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] FTRQ_TransArray = {"ARU", "ADJ", "ISS"};
	  final String [] LRQ_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
	  
	  final String [] GUA_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] SLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] PYB_TransArray = {"CHP", "APM", "PPY"};
	  final String [] FTDP_TransArray = {"ARU", "ADJ", "ISS"};
	  
	  final String [] RFN_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
	  final String [] REC_TransArray = {"PAR", "CHM", "ARM"};
	  final String [] RBA_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "CUS", "LIQ"};
	  final String [] RQA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV", "TRN", "PYT"};
	  
	  final String [] SHP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "ISS", "PAY", "REA"};
	  final String [] SUPPLIER_PORTAL_TransArray = {"ISS", "LIQ"};
	  final String [] FTB_ATransArray = {"ARU", "ADJ", "ISS"};
	  final String [] MESSAGES_TransArray = {"DISCR", "MAIL", "MATCH", "SETTLEMENT"};
	  
	  final String [] LRQ_INV_TransArray = {"LRQ_INV"};
	  final String [] HTOH_TransArray = {"PIN", "PCN", "RIN"};

	  final String [] SUPPLIER_PORTAL_Lable_TransArray = {"SP_CNF", "SP_LF"};
	  final String [][] instrTransArrays = {
			  	 AIR_TransArray, ATP_TransArray, BIL_TransArray, 
			  	 CBA_TransArray, EXP_OCO_TransArray, EXP_DBA_TransArray, 
			  	 EXP_COL_TransArray, EXP_DFP_TransArray, EXP_DLC_TransArray, 
			  	 LOI_TransArray, EXP_TAC_TransArray, IMP_DBA_TransArray, 
	             IMC_TransArray, IMP_DFP_TransArray, IMP_DLC_TransArray, 
	             IMP_TAC_TransArray, INC_GUA_TransArray, INC_SLC_TransArray, 
	             FTRQ_TransArray, LRQ_TransArray, GUA_TransArray, 
	             SLC_TransArray, PYB_TransArray, FTDP_TransArray, 
	             RFN_TransArray, REC_TransArray, RBA_TransArray, 
	             RQA_TransArray, SHP_TransArray, SUPPLIER_PORTAL_TransArray, 
	             FTB_ATransArray, MESSAGES_TransArray, LRQ_INV_TransArray, 
	             HTOH_TransArray};
	  
  //read in parameters
  	String sectionNum = request.getParameter("sectionNum");
  	String uiType = request.getParameter("uiType");
  	String criteriaStartCount = request.getParameter("criteriaStartCount");
  	String notificationRuleDetailDialogId = request.getParameter("dialogId");
    int sNum = Integer.valueOf(sectionNum);
    String [] transArray = instrTransArrays[sNum-1];
    String notifyEmail;
    int notifEmailIndex = 0;
    int rowIndex = 0;
    int spIndex = 0;
    int DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
    
    String notifyRuleUsersSql = "select USER_OID, EMAIL_ADDR from users where p_owner_org_oid = ? and email_addr is not null";
    DocumentHandler notifyRuleUsersList = DatabaseQueryBean.getXmlResultSet(notifyRuleUsersSql, false, userSession.getOwnerOrgOid());
 	
 	String userEmailsOptions = "<option  value=\"\"></option>";
 	userEmailsOptions += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "EMAIL_ADDR", "", null);
%>

	<div id="NotificationRuleDetailDialog" class="dialogContent midwidth">
		<%=widgetFactory.createNote("NotificationRuleDetail.TransDialogNote")%> 
	<%
		if(uiType.equals("ALL")){
			for (tLoop = 0; tLoop < transArray.length; tLoop++){
				if(InstrumentType.SUPPLIER_PORTAL.equals(instrumentCatagory[sNum-1])){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+SUPPLIER_PORTAL_Lable_TransArray[spIndex])%>
	<%
				spIndex++;
				}else{
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+transArray[tLoop])%>
	<%
				}
	%>
				<table>
					<tr>
						<td width="20%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
						</td>
						<td width="18%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
						</td>
						<td width="15%">
						
						</td>
						<td width="37%">
						
						</td>
					</tr>
					<tr>
						<td width="20%" style="vertical-align: top;">
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transNotifySettingAlways_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transNotifySettingNone_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transNotifySettingChrgsDocsOnly_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
						</td>
						
						<td width="18%" style="vertical-align: top;">
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingAlways_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingNone_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingChrgsDocsOnly_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
						</td>
						<td width="15%" style="vertical-align: top;">

						</td>
						<td width="37%" align="left" style="vertical-align: top;">
							<button data-dojo-type="dijit.form.Button"  name="ClearAllTrans_<%=instrumentCatagory[sNum-1]%>_<%=transArray[tLoop]%>" id="ClearAllTrans_<%=instrumentCatagory[sNum-1]%>_<%=transArray[tLoop]%>" type="button">
								<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								clearTransCategorySection(<%=tLoop%>, '<%=instrumentCatagory[sNum-1]%>', '<%=transArray[tLoop]%>');
							</script>
							</button>
						</td>
					</tr>
				</table>
	<%
			}
		}//if uiType is empty
		else if(uiType.equals("MESSAGES") || uiType.equals("OTHERS")){
			for (tLoop = 0; tLoop < transArray.length; tLoop++){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+transArray[tLoop])%>
				<table>
					<tr>
						<td width="30%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
						</td>
						<td width="8%">
							&nbsp;
						</td>
						<td width="35%">
					
						</td>
						<td width="27%">
						
						</td>
					</tr>
					<tr>
						<td width="30%" style="vertical-align: top;">
						<%
							if(uiType.equals("MESSAGES")){
						%>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingAlways_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS, false, isReadOnly)%>                         
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingNone_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE, false, isReadOnly)%>
						<%
							} else if(uiType.equals("OTHERS")){
						%>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingAlways_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.Yes",TradePortalConstants.NOTIF_RULE_YES, false, isReadOnly)%>                         
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"transEmailSettingNone_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop],
									"NotificationRuleDetail.No",TradePortalConstants.NOTIF_RULE_NONE, false, isReadOnly)%>
						
							<br><br>
							<%=widgetFactory.createLabel("", "NotificationRuleDetail.EmailInterval", false, false, false, "inline") %>
							<%= widgetFactory.createTextField("transNotifyEmailInterval_"+instrumentCatagory[sNum-1]+"_"+transArray[tLoop], "", "", "5", 
									isReadOnly,false,false,"","regExp:'^[1-9][0-9]*0$',invalidMessage:'"+resMgr.getText("NotificationRuleDetail.InvalidNotificationEmailFreqValue",TradePortalConstants.TEXT_BUNDLE)+"'","none") %>
							<br>
						<%
							}
						%>
						</td>
						
						<td width="8%" style="vertical-align: top;">
							&nbsp;
						</td>
						<td width="35%" style="vertical-align: top;">
						</td>
						<td width="28%" style="vertical-align: top;">
							<button data-dojo-type="dijit.form.Button"  name="ClearAllTrans_<%=instrumentCatagory[sNum-1]%>_<%=transArray[tLoop]%>" id="ClearAllTrans_<%=instrumentCatagory[sNum-1]%>_<%=transArray[tLoop]%>" type="button">
								<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									clearTransCategorySection(<%=tLoop%>, '<%=instrumentCatagory[sNum-1]%>', '<%=transArray[tLoop]%>');
								</script>
							</button>
						</td>
					</tr>
				</table>
	<%
			}
		}
	%>
		<div class='formItem'>
			<button data-dojo-type="dijit.form.Button"  name="SaveCloseButtonDialog<%=sNum-1%>" id="SaveCloseButtonDialog<%=sNum-1%>" type="button">
				<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					updateTransNotifySettings();
			 	</script>
			</button>
			<button data-dojo-type="dijit.form.Button"  name="CancelDialog<%=sNum-1%>" id="CancelDialog<%=sNum-1%>" type="button">
				<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					closeTransNotifySettingDialog();    					
				</script>
			</button> 
		</div>
	</div><%-- end dialogid div --%>
	
<script type='text/javascript'>
var instrArray = ['AIR', 'ATP', 'BIL', 'CBA', 'EXP_OCO', 'EXP_DBA', 'EXP_COL', 'EXP_DFP', 'EXP_DLC', 
                  'LOI', 'EXP_TAC', 'IMP_DBA', 'IMC', 'IMP_DFP', 'IMP_DLC', 'IMP_TAC', 'INC_GUA', 'INC_SLC', 
                  'FTRQ', 'LRQ', 'GUA', 'SLC', 'PYB', 'FTDP', 'RFN', 'REC', 'RBA', 
                  'RQA', 'SHP', 'SUPPLIER_PORTAL', 'FTBA', 'MESSAGES', 'LRQ_INV', 'H2H_INV_CRN'];

var AIR_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "PAY", "REA", "REL"];
var ATP_TransArray = ["ARU", "ADJ", "AMD", "CHG", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA"];//1 EXT
var BIL_TransArray = ["ARU", "ADJ", "BCH", "BCA", "PCS", "PCE", "DEA", "BNW","REA", "RED", "RVV"];
var CBA_TransArray = ["ARU", "ADJ", "BUY", "CHG", "PCS", "PCE", "CUS", "LIQ"];//3 BUY PCS PCE

var EXP_OCO_TransArray = ["ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"];
var EXP_DBA_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];
var EXP_COL_TransArray = ["ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"];
var EXP_DFP_TransArray = [ "ARU", "ADJ", "BUY",  "CHG", "CUS", "LIQ"];

var EXP_DLC_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
var LOI_TransArray = ["ARU", "ADJ", "AMD", "CHG", "CRE", "DEA", "EXP", "PAY", "REA"];//2 DEA EXT
var EXP_TAC_TransArray = [ "ARU", "ADJ",  "BUY",  "CHG", "CUS", "LIQ"];
var IMP_DBA_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"]; 

var IMC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "COL", "NAC", "PAY", "REA"];//DEA
var IMP_DFP_TransArray = ["ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];
var IMP_DLC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
var IMP_TAC_TransArray = [ "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"];

var INC_GUA_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
var INC_SLC_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"];
var FTRQ_TransArray = ["ARU", "ADJ", "ISS"];
var LRQ_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"];

var GUA_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
var SLC_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"];
var PYB_TransArray = ["CHP", "APM", "PPY"];
var FTDP_TransArray = ["ARU", "ADJ", "ISS"];

var RFN_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"];
var REC_TransArray = ["PAR", "CHM", "ARM"];
var RBA_TransArray = ["ARU", "ADJ", "CHG", "PCS", "PCE", "CUS", "LIQ"];
var RQA_TransArray = ["ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV", "TRN", "PYT"];

var SHP_TransArray = ["ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "ISS", "PAY", "REA"];
var SUPPLIER_PORTAL_TransArray = ["ISS", "LIQ"];
var FTB_ATransArray = ["ARU", "ADJ", "ISS"];
var MESSAGES_TransArray = ["DISCR", "MAIL", "MATCH", "SETTLEMENT"];

var LRQ_INV_TransArray = ["LRQ_INV"];
var HTOH_TransArray = ["PIN", "PCN", "RIN"];

  var instrTransArrays = [
		  	 AIR_TransArray, ATP_TransArray, BIL_TransArray, 
		  	 CBA_TransArray, EXP_OCO_TransArray, EXP_DBA_TransArray, 
		  	 EXP_COL_TransArray, EXP_DFP_TransArray, EXP_DLC_TransArray, 
		  	 LOI_TransArray, EXP_TAC_TransArray, IMP_DBA_TransArray, 
             IMC_TransArray, IMP_DFP_TransArray, IMP_DLC_TransArray, 
             IMP_TAC_TransArray, INC_GUA_TransArray, INC_SLC_TransArray, 
             FTRQ_TransArray, LRQ_TransArray, GUA_TransArray, 
             SLC_TransArray, PYB_TransArray, FTDP_TransArray, 
             RFN_TransArray, REC_TransArray, RBA_TransArray, 
             RQA_TransArray, SHP_TransArray, SUPPLIER_PORTAL_TransArray, 
             FTB_ATransArray, MESSAGES_TransArray, LRQ_INV_TransArray, 
             HTOH_TransArray];
	
	function clearTransCategorySection(sectionNum, instrType, transType) {
		var uiType = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uiType))%>';
		require(["dijit/registry"],function(registry) {
			if(uiType == 'ALL'){
				registry.byId('transNotifySettingAlways_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transNotifySettingNone_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transType).setChecked(false);
			}else if(uiType == 'MESSAGES'){
				registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
			}else if(uiType == 'OTHERS'){
				registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
				registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
				registry.byId("transNotifyEmailInterval_"+instrType+'_'+transType).setValue('');
			}
		});
  	}

	function updateTransNotifySettings(){
		require(["dijit/registry", "t360/dialog"],function(registry, dialog) {
			var secNum = <%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(sectionNum))%>; 
			var uiType = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uiType))%>';
			var instrType = instrArray[secNum-1];
			var transArray = instrTransArrays[secNum-1];
			var criteriaStartCount = <%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(criteriaStartCount))%>;
			var notifSetting;
			var emailSetting;
			var transNotifyEmailFreq;
			
			for(i=0; i<transArray.length; i++){
				notifSetting = '';
				emailSetting = '';
				userEmail = '';
				transNotifyEmailFreq = '';
				
				if(uiType == 'ALL'){	
					if (registry.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).checked){
						notifSetting = registry.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).checked){
			    		notifSetting = registry.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).checked){
			    		notifSetting = registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).value;
			    	}
			    	
			    	if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).value;
			    	}
			    	
			    	dojo.byId("send_notif_setting"+criteriaStartCount).value = notifSetting;
	    			dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}else if(uiType == 'MESSAGES'){
					if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}
					
					dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}else if(uiType == 'OTHERS'){	
					if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}
					
					transNotifyEmailFreq = registry.byId("transNotifyEmailInterval_"+instrType+'_'+transArray[i]).value;
	    			dojo.byId("notify_email_freq"+criteriaStartCount).value = transNotifyEmailFreq;
					dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}
		    	
    			criteriaStartCount++;
			}
			
			dialog.close('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>');
			document.getElementById('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>').hidden = true;
		});
 	}
  	
	function closeTransNotifySettingDialog(){
		require(["dijit/registry", "t360/dialog"],function(registry, dialog) {
			dialog.close('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>');
			document.getElementById('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>').hidden = true;
		});
	}
</script>
