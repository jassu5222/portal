<%--
*******************************************************************************
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%
	
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;	
	String panelLevel = request.getParameter("panelLevel");
	String paramOid1 = request.getParameter("paramOid1");
	String paramOid2 = request.getParameter("paramOid2");
	String fromPage = request.getParameter("fromPage");
%>

<%-- ********************* HTML for page begins here *********************  --%>
<div class="formContent" style="padding:10px;height:300px;width:400px">


<input type=hidden name="buttonName" value="">  
<%
gridLayout = dgFactory.createGridLayout("notifyUsersDataGridId", "NotifyPendingPanelAuthUsersDataGrid");
gridHtml = dgFactory.createDataGrid("notifyUsersDataGridId","NotifyPendingPanelAuthUsersDataGrid",null);
%>
<%=gridHtml%>
</div>



    
 <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;								
		var initSearchParms= "panelLevel=<%=StringFunction.escapeQuotesforJS(panelLevel)%>";
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("NotifyPendingPanelAuthUsersDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("notifyUsersDataGridId", viewName,gridLayout, initSearchParms);
		
		require(["dijit/registry","dojo/ready", "dojo/domReady!"],
		function(registry, ready ) {
			var myGrid = registry.byId("notifyUsersDataGridId");
	    	myGrid.set('autoHeight',10);
		});
		
		function sendEmail(){
		  require(["t360/common"], function(common) {
			var rowKeys = getSelectedGridRowKeys("notifyUsersDataGridId"),
			buttonName = '<%=TradePortalConstants.BUTTON_NOTIFY_PANEL_AUTH_USER%>',
			fromPage = '<%=StringFunction.escapeQuotesforJS(fromPage)%>';
			var formName, myForm, formNum;
			if(fromPage == 'TradeCash'){
				formName = "Transaction-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].transaction_oid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
				document.forms[formNum].instrument_oid.value='<%=StringFunction.escapeQuotesforJS(paramOid2)%>';
			}else if(fromPage == 'RM'){
				formName = "Receivables-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].payRemitOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == 'INV_GROUP'){
				formName = "InvoiceGroup-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceGroupOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == 'INV_SUMMARY'){
				formName = "InvoiceSummary-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceSummaryOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == '<%=TradePortalConstants.SUPPLIER_PORTAL%>'){
				formName = "InvoiceOffersGroup-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceOfferGroupOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == 'PAY_INV'){
				formName = "Payables-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceSummaryOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == 'PAY_INV_GROUP'){
				formName = "InvoiceGroup-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceGroupOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}else if(fromPage == 'PAY_INV_SUMMARY'){
				formName = "PayInvoiceSummary-NotifyPanelUserForm";
				myForm = document.getElementsByName(formName)[0];
				formNum = common.getFormNumber(formName);
				document.forms[formNum].invoiceSummaryOid.value='<%=StringFunction.escapeQuotesforJS(paramOid1)%>';
			}

			hideDialog("NotifyPendingPanelAuthEmailDialog");
			common.submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			
		  });
	
		}
		
		function CancelNotify(){
			require(["dijit/registry"],
					 function(registry) {																		
						hideDialog("NotifyPendingPanelAuthEmailDialog");
			});

		}
	
	</script>   
	<%=widgetFactory.createHoverHelp("AddStructurePODialogCloseButton", "common.Cancel") %>	
</body>
</html>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

