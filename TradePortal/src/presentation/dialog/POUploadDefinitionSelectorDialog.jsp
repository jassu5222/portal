<%--
 *  PO Definition Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Sandee Sikhakolli
 *	   Copyright   2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
	<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
	                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

	<% 
	    
	    CorporateOrganizationWebBean corporateOrgRef = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
        corporateOrgRef.setAttribute("organization_oid", userSession.getOwnerOrgOid());
        corporateOrgRef.getDataFromAppServer();
		String uploadFormat =  corporateOrgRef.getAttribute("po_upload_format");


		//read in parameters
		String poUploadDefnSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
		
		WidgetFactory widgetFactory = new WidgetFactory(resMgr);
		DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
		String gridHtml = "";
		
		StringBuilder sql = new StringBuilder();
		QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
		List<Object> sqlParamsPo = new ArrayList();
		if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
		  sql.append("select po_upload_definition_oid, name ");
		  sql.append(" from po_upload_definition");
		  sql.append(" where a_owner_org_oid in ( ?"  );
		  sqlParamsPo.add(userSession.getOwnerOrgOid());
  		     // Also include PO defs from the user's actual organization if using subsidiary access
  		  if(userSession.showOrgDataUnderSubAccess()){
      		sql.append(",?");
      		sqlParamsPo.add(userSession.getSavedUserSession().getOwnerOrgOid());
   		  }

		sql.append(") and definition_type in (?,?)");
		sql.append(" order by ");
		sql.append(resMgr.localizeOrderBy("name"));
		sqlParamsPo.add(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD);
		sqlParamsPo.add(TradePortalConstants.PO_DEFINITION_TYPE_BOTH);
		}else if(TradePortalConstants.PO_UPLOAD_STRUCTURED.equals(uploadFormat)){
			 // Build a list of PO Structured Definitions for the user's org.
          sql.append("select purchase_order_definition_oid, name ");
          sql.append(" from purchase_order_definition");
          sql.append(" where a_owner_org_oid in (?" );
		  sqlParamsPo.add(userSession.getOwnerOrgOid());	
           // Also include PO defs from the user's actual organization if using subsidiary access
          if(userSession.showOrgDataUnderSubAccess())
           {
          sql.append(",?");
          sqlParamsPo.add(userSession.getSavedUserSession().getOwnerOrgOid());
           }
	      sql.append(")");
          sql.append(" order by ");
          sql.append(resMgr.localizeOrderBy("name"));
          Debug.debug(sql.toString());
	    }
		
		DocumentHandler poUplDefnListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true,sqlParamsPo);
		Vector<DocumentHandler> poUplDefnList = new Vector();
		
		if ( poUplDefnListDoc != null ){
			poUplDefnList = poUplDefnListDoc.getFragments("/ResultSetRecord");
		}
		
		if(poUplDefnList.size() != 0 && poUplDefnList.size() > 1){
	%>
	<br>
	&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("POUploadDefnSelector.PODefnInstruction")%>
		<div id="poUploadDefnSelectorDialogContent" class="dialogContent">
		
		<%
	  		gridHtml = dgFactory.createDataGrid("poUploadDefnGrid","POUploadDefinitionSelectorDataGrid",null);
		%>
	  		<%= gridHtml %>
	  		
	  		
  		</div>
  		
  		<%--the following script loads the datagrid.  note that this dialog is a DialogSimple to allow scripts to execute after it loads on the page--%>
		<script type='text/javascript'>
  			require(["t360/datagrid", "dojo/domReady!"],function( t360grid ) {
		<%
  				String poUploadDefnGridLayout = dgFactory.createGridLayout("poUploadDefnGrid", "POUploadDefinitionSelectorDataGrid");
		%>
    			var gridLayout = <%= poUploadDefnGridLayout %>;
    	<%
	    	 	//get the grid data
	    	  	//this reevaluates from the user session data rather than taking passed in bank branch
	    	  	//values for security
	    	  	StringBuffer poUplDefnGridData = new StringBuffer();
	    	
    	     if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
	    		for ( int poIdx = 0; poIdx<poUplDefnList.size(); poIdx++ ) {
					DocumentHandler poDoc = (DocumentHandler) poUplDefnList.get(poIdx);
					String poOid = poDoc.getAttribute("/PO_UPLOAD_DEFINITION_OID");
					poOid = EncryptDecrypt.encryptStringUsingTripleDes(poOid, userSession.getSecretKey());
					String poName = poDoc.getAttribute("/NAME");
					poUplDefnGridData.append("{ ");
					poUplDefnGridData.append("rowKey:'").append(poOid).append("'");
					poUplDefnGridData.append(", ");
					poUplDefnGridData.append("PODefinitionName:'").append(poName).append("'");
					poUplDefnGridData.append(" }");
	  	      
					if ( poIdx < poUplDefnList.size()-1) {
	  	        		poUplDefnGridData.append(", ");
	  	      		}
	  	    	}//end of for
    	     }else{
    	    	 for ( int poIdx = 0; poIdx<poUplDefnList.size(); poIdx++ ) {
 					DocumentHandler poDoc = (DocumentHandler) poUplDefnList.get(poIdx);
 					String poOid = poDoc.getAttribute("/PURCHASE_ORDER_DEFINITION_OID");
 					poOid = EncryptDecrypt.encryptStringUsingTripleDes(poOid, userSession.getSecretKey());
 					String poName = poDoc.getAttribute("/NAME");
 					poUplDefnGridData.append("{ ");
 					poUplDefnGridData.append("rowKey:'").append(poOid).append("'");
 					poUplDefnGridData.append(", ");
 					poUplDefnGridData.append("PODefinitionName:'").append(poName).append("'");
 					poUplDefnGridData.append(" }");
 	  	      
 					if ( poIdx < poUplDefnList.size()-1) {
 	  	        		poUplDefnGridData.append(", ");
 	  	      		}
 	  	    	}//end of for 
    	     }
    	%>
	    		var gridData = [ <%= poUplDefnGridData.toString() %> ];
				<%--create a local memory grid--%>
				t360grid.createMemoryDataGrid("poUploadDefnGrid", gridData, gridLayout);
	  		});
  		</script>
	<%
		}else if(poUplDefnList.size() == 0){
	%>
	<br>
			&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel( "POUploadDefnSelector.NoPODefnsInstruction")%>
			<br>
		<div id="poUploadDefnSelectorDialogContent" class="dialogContent">
			
			
			<div class="gridFooter">
				<button id="CreatePOUploadDefn" data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					<%= resMgr.getText("POUploadDefnSelector.CtearePODefinition", TradePortalConstants.TEXT_BUNDLE) %>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						openURL("<%=formMgr.getLinkAsUrl("selectPOUploadDefinition",response)%>");  
					</script>
				</button>
				<%=widgetFactory.createHoverHelp("CreatePOUploadDefn","NewHoverText") %>
				<button id="POUploadDefnSelector_cancel" data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					<%= resMgr.getText("POUploadDefnSelector.cancel", TradePortalConstants.TEXT_BUNDLE) %>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						closePODialog();
					</script>
				</button>
				<%=widgetFactory.createHoverHelp("POUploadDefnSelector_cancel","common.Cancel") %>
				<div style="clear:both;"></div>
			</div>
		</div>
	<%
		}
	%>

<script type='text/javascript'>
  <%-- on select perform execute the select callback function --%>
  function selectPO() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys("poUploadDefnGrid");
      if (rowKeys && rowKeys.length == 1 ) {
        <%-- bankBranchSelectorDialogSelect(rowKeys[0]); --%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(poUploadDefnSelectorDialogId)%>', 'select',rowKeys[0]);
      }else{
    	  alert('<%= resMgr.getText("POUploadDefnSelector.selectwarningmessage", TradePortalConstants.TEXT_BUNDLE) %>');
      }
    });
  }

  function closePODialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(poUploadDefnSelectorDialogId)%>');
    });
  }

  	function openURL(URL){
		document.location.href  = URL;
		return false;
	}

  	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(poUploadDefnSelectorDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","PODefinitionSelectorDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
</script>
	<%=widgetFactory.createHoverHelp("PODefinitionSelectorDialogCloseLink", "common.Cancel") %>
