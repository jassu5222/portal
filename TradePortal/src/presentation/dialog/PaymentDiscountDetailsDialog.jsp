<%--
 *
 *Srinivasu Doddapaneni - Rel9.3 CR-997 - Payment Discount Details Popup 04/07/2015
 *Includes computing totals on the fly as the user enters them
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,java.math.BigDecimal" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   boolean isNew = false;
   boolean isReadOnly = false;
   boolean  insertMode      = true;   
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showSaveClose = true;
   boolean showDelete = true;
   boolean showSaveAs = true;
   String oid =  "0";
   Object[] sqlParamsPayDisc = null;
   String invoiceId ="0";
   //AiA - 8/20
   String remitOid = "";
   String payMatchResultOid = StringFunction.xssCharsToHtml(request.getParameter("pay_match_result_oid"));
   String DailogId = StringFunction.xssCharsToHtml(request.getParameter("DailogId"));
    String autoInvID = StringFunction.xssCharsToHtml(request.getParameter("autoInvID"));
   String formName = "PayDiscountDetailForm";
 
   //System.out.println("payMatchResultOid:"+payMatchResultOid+"\t DailogId:"+DailogId+"\tautoInvID;"+autoInvID);
   if(StringFunction.isBlank(payMatchResultOid)){
	   payMatchResultOid = (String)session.getAttribute("currentPayMatchResultOid");
   }else{
   		session.setAttribute("currentPayMatchResultOid", payMatchResultOid);
   }
   int curDecimal = 2;
   String loginRights;  
   String loginLocale = userSession.getUserLocale();
   String currencyCode = userSession.getBaseCurrencyCode();
   String decimalPlaces = "";
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.MatchPayDedGlDtlsWebBean", "MatchPayDedGlDtls");
   MatchPayDedGlDtlsWebBean matchPayDedGlDtls = (MatchPayDedGlDtlsWebBean)beanMgr.getBean("MatchPayDedGlDtls");  
     
   
   Hashtable secureParams   = new Hashtable();

   DocumentHandler doc      = null;
   DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  
   String showErrorDialog = (String) session.getAttribute("showErrorDialog");
   
   //Get WidgetFactory instance..
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   String   refDataHome       = "goToRefDataHome";                //used for the close button and Breadcrumb link
   String buttonPressed ="";
   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = false;

   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
   Debug.debug("doc from cache is " + doc.toString());


   isNew = true;
    //invoiceId = null; 
    showSaveAs = true;   

    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String onLoad = "";

    //
	  if( InstrumentServices.isNotBlank(payMatchResultOid) ) {
	     insertMode = false;
	     payMatchResultOid = EncryptDecrypt.decryptStringUsingTripleDes( payMatchResultOid, userSession.getSecretKey() );
	     Debug.debug("***** matchPayDedGlDtls invoiceId == " + invoiceId);
	  }else {
	      invoiceId = defaultDoc.getAttribute("/Out/MatchPayDedGlDtls/invoice_reference_id");
	    
	      if( InstrumentServices.isNotBlank(invoiceId) ) {
	    	 // matchPayDedGlDtls.setAttribute("invoice_reference_id", invoiceId);
	    	//  matchPayDedGlDtls.getDataFromAppServer();
	      }
	      else {
	         invoiceId = "0";
	         payMatchResultOid = "0";
	         insertMode = true;
	      }
	
	  }    
//
//AiA - 8/20 - Use pay remit oid as the key
//Obtain the Pay remit oid


String theRemitPaySql = " SELECT PAY_MATCH_RESULT_OID, P_PAY_REMIT_OID, INVOICE_REFERENCE_ID  FROM PAY_MATCH_RESULT WHERE  PAY_MATCH_RESULT_OID = ? ";

//AiA
//out.print("<br>remit sql: "+theRemitPaySql);
DocumentHandler remitPayResultXML = DatabaseQueryBean.getXmlResultSet(theRemitPaySql, false, new Object[]{payMatchResultOid} );

if (remitPayResultXML != null){
	  remitOid = remitPayResultXML.getAttribute("/ResultSetRecord/P_PAY_REMIT_OID"); //AiA
	  //current invoice ref id
	  invoiceId = remitPayResultXML.getAttribute("/ResultSetRecord/INVOICE_REFERENCE_ID"); //AiA
	  if(StringFunction.isBlank(invoiceId)){
		  invoiceId = autoInvID;
	  }
}
//AiA - 8/20 - End
   
//System.out.println("invoiceID:"+invoiceId);
// Fetching decimal places for particular currency Vsarkary Rel 9100 IR-T36000031287 

decimalPlaces = ReferenceDataManager.getRefDataMgr().getAdditionalValue(TradePortalConstants.CURRENCY_CODE, currencyCode,loginLocale);
if(StringFunction.isNotBlank(decimalPlaces)){
	curDecimal = Integer.parseInt(decimalPlaces);
	
}

// End Vsarkary Rel 9100 IR-T36000031287 
    
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

    if (isReadOnly)
    {
        showSave = false;
        showDelete = false;
        showSaveClose = false;
        showSaveAs = false;
    }
    else if (isNew){
        showDelete = false;
        showSaveAs = false;
    }

%>

<%-- Body tag included as part of common header --%>
<%--
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>
--%>
<%--
<div class="pageMain">        <%-- Page Main Start --%>
  <div class="pageContent">   <%--  Page Content Start --%>
--%>
<%--
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Payment and Discount Details"/>
      <jsp:param name="helpUrl" value="customer/paydiscount_detail.htm"/>
    </jsp:include>
--%>
 
<% 
  String subHeaderTitle = "";

  subHeaderTitle = resMgr.getText("PaymentDiscountDetail.Subtitle",TradePortalConstants.TEXT_BUNDLE); 
  //subHeaderTitle = "Enter the details for up to eight (8) transactions in the fields below";
%>
<%--
    <jsp:include page="/common/PageSubHeader.jsp">
		  <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>
--%>


	<div class="subHeaderDivider"></div>
<div class="transactionSubHeader" style='padding: 14px 0px 10px 8px;'>
  <span class="pageSubHeaderItem">
	  <%= subHeaderTitle %>
  </span>
 </div><br>
<form name="PayDiscountDetailForm" id="PayDiscountDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
  <input type=hidden value="" name=saveAsFlag>
  
  <%--
<jsp:include page="/common/ErrorSection.jsp" />
--%>


<%



String options = "";

String transTypeOptions = "";

String invTotalAmount="";
String invOutstandingAmount = "";
String invDueDate ="";
String currency = "";
String invPayDate ="";
String buyerName = "";
String addLine1 = "";
String addLine2 = "";
String country = "";
StringBuffer applicantAddress =  new StringBuffer();
String theInvSql = " SELECT *  FROM INVOICE  WHERE  INVOICE_REFERENCE_ID = ? ";
DocumentHandler invResultXML = DatabaseQueryBean.getXmlResultSet(theInvSql, false, new Object[]{invoiceId});

if (invResultXML != null){
	  invTotalAmount = invResultXML.getAttribute("/ResultSetRecord/INVOICE_TOTAL_AMOUNT");
	  invOutstandingAmount = invResultXML.getAttribute("/ResultSetRecord/INVOICE_OUTSTANDING_AMOUNT");	 
	  currency = invResultXML.getAttribute("/ResultSetRecord/CURRENCY_CODE");
	  invTotalAmount = TPCurrencyUtility.getDisplayAmount(invTotalAmount, currency, loginLocale);	
	  invOutstandingAmount = TPCurrencyUtility.getDisplayAmount(invOutstandingAmount, currency, loginLocale);	
	  invDueDate=TPDateTimeUtility.formatDate(invResultXML.getAttribute("/ResultSetRecord/INVOICE_DUE_DATE"), TPDateTimeUtility.LONG, loginLocale);
	  invPayDate=TPDateTimeUtility.formatDate(invResultXML.getAttribute("/ResultSetRecord/INVOICE_PAYMENT_DATE"), TPDateTimeUtility.LONG, loginLocale);
	  buyerName = invResultXML.getAttribute("/ResultSetRecord/BUYER_PARTY_IDENTIFIER");
  	  addLine1 = invResultXML.getAttribute("/ResultSetRecord/BUYER_ADDRESS_LINE_1");
	  addLine2 = invResultXML.getAttribute("/ResultSetRecord/BUYER_ADDRESS_LINE_2");
	  country = invResultXML.getAttribute("/ResultSetRecord/BUYER_ADDRESS_COUNTRY");
	  applicantAddress.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+buyerName+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+addLine1+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+addLine2+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+country);
}

%>
<div id="payDetailsDialogContent">
 <%-- <div class="formContent">      --%>     <%--  Form Content Start --%>
<%--
 <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">	 --%>
  <div class="dijitNoTitlePaneContentOuterTST">
 <div class="dijitNoTitlePaneContentOuterTS">
 <table border='0' style='width:100%;'> <tr><td style='width:52%;vertical-align:top'>
 
	<table border='0'>
		<tr>
			<td><%= widgetFactory.createTextField("DataBuyerPaymentSource","PaymentDiscountDetail.InvoiceId", invoiceId, "30",
			true,false,false,"","","inline") %>
			</td>		
			<td>
			<%= widgetFactory.createTextField("DataBuyerValueDate","PaymentDiscountDetail.InvoiceTotalAmount",currency + " " +invTotalAmount, "30",
			true,false,false,"","","inline") %>
			
			</td>
		</tr>
		<tr>
			
			<td><%= widgetFactory.createTextField("DataBuyerTracerLine","PaymentDiscountDetail.InvoiceDueDate", invDueDate, "30",
			true,false,false,"","","inline") %></td>
	
			<td><%= widgetFactory.createTextField("DataBuyerTracerLine","PaymentDiscountDetail.OutstandingAmount", currency + " " +invOutstandingAmount, "30",
			true,false,false,"","","inline") %></td>
			
		
		</tr>
	</table>        
</td><td style='width:48%;vertical-align:top' bgcolor=''>
<table border='0' style='width:97%;'>

		<tr>
			<td style='vertical-align:top' bgcolor=''>
			<%= widgetFactory.createWideSubsectionHeader( "PaymentRemittance.Buyer") %>
	<%--		<%= widgetFactory.createSubsectionHeader( "PaymentRemittance.Buyer", false, false, false, "") %>--%>
			</td>
		</tr>
		<tr>
			<td>
			<%=	applicantAddress.toString()%>
			</td>
		</tr>

		
	</table>        

</td></tr></table>

<div style="clear:both;">&nbsp;</div>
	
		
		

<%
	//
	//Retrieve all Match Pay DED DTLS for the matching invoice
	//AiA start
	String theSql = " SELECT *  FROM MATCH_PAY_DED_GL_DTLS  WHERE  INVOICE_REFERENCE_ID = ?";
	DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(theSql, false, new Object[]{invoiceId});	
	DocumentHandler doc1 = null;
%>     
       
	<table>
<%
Vector matchPayFragments = new Vector();
String appliedAmount="";
String discComments="";
String discDesc="";
String transType="";
double totalPay = 0;
double totalOverPay = 0;
double totalDiscount = 0;
String transGlOptions="";
String discountOptions="";
String selectedGlOption="";
String selectedDiscOption="";
//Transaction Specific Codes
String discOptions="";
String payOptions="";
String overPayOptions="";

//Obtain ERP setting for customer
//
//select erp_transaction_rpt_reqd, corp_discount_code_type from corporate_org where organization_oid = userSession.getOwnerOrgOid()
String erpSetting = "";
String codeType = "";
String erpSql = "select erp_transaction_rpt_reqd, corp_discount_code_type from corporate_org  where organization_oid = ? ";
DocumentHandler erpDoc = DatabaseQueryBean.getXmlResultSet(erpSql.toString(), false, new Object[]{userSession.getOwnerOrgOid()});

if(erpDoc != null){
	erpSetting = erpDoc.getAttribute("/ResultSetRecord/ERP_TRANSACTION_RPT_REQD");///DocRoot/ResultSetRecord[1]/ERP_TRANSACTION_RPT_REQD[1]
	codeType = erpDoc.getAttribute("/ResultSetRecord/CORP_DISCOUNT_CODE_TYPE");
}

//GL CODES DROPDOWN sql

   StringBuilder sqlStmntStr = new StringBuilder();
   sqlStmntStr.append("select erp_gl_code, erp_gl_description, erp_gl_category, customer_erp_gl_code_oid, p_owner_oid");
   sqlStmntStr.append(" from customer_erp_gl_code"); 
   sqlStmntStr.append(" where deactivate_ind <> ? and p_owner_oid in (?) ");

   DocumentHandler glCodesDoc = DatabaseQueryBean.getXmlResultSet(sqlStmntStr.toString(), false, TradePortalConstants.INDICATOR_YES, userSession.getOwnerOrgOid());
   java.util.List<Object> sqlParamsLst = new java.util.ArrayList();
   StringBuffer sqlDisc = new StringBuffer();
   sqlDisc.append("select DISCOUNT_CODE, DISCOUNT_DESCRIPTION ");
   sqlDisc.append(" from customer_discount_code"); 
   sqlDisc.append(" where p_owner_oid in (?) and deactivate_ind ='"+TradePortalConstants.INDICATOR_NO+"'");
   sqlParamsLst.add(userSession.getOwnerOrgOid());
   //Add bank codes only if user is set to use both bank and customer defined codes
   //IR T36000016764
   if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(erpSetting) && TradePortalConstants.BANK_AND_CUSTOMER_ERP_CODES.equalsIgnoreCase(codeType)){
	   sqlDisc.append(" union ");
	   sqlDisc.append(" select code as DISCOUNT_CODE, descr as DISCOUNT_DESCRIPTION from bankrefdata");
	   sqlDisc.append(" where table_type ='DISCOUNT_CODE' and client_bank_id = ? ");
	   sqlParamsLst.add(userSession.getLoginOrganizationId());
   }
   
   DocumentHandler discCodesDoc = DatabaseQueryBean.getXmlResultSet(sqlDisc.toString(), false, sqlParamsLst);
  
if (resultXML != null){
	  matchPayFragments = resultXML.getFragments("/ResultSetRecord");
}else{
}

	  int numItems = matchPayFragments.size();
	  //out.print("pay match fragments: "+numItems);
	  for (int i = 0; i < 8; i++) {
		  //IR 19455 - if error then diplay error msg on the same page
		  if(numItems>i && doc.getFragments("/Error/errorlist/error")!=null && doc.getFragments("/Error/errorlist/error").size()==0){
				doc1 = (DocumentHandler) matchPayFragments.elementAt(i);
				appliedAmount = doc1.getAttribute("/APPLIED_AMOUNT");
				discComments = doc1.getAttribute("/DISCOUNT_COMMENTS");
				transType = doc1.getAttribute("/TRANSACTION_TYPE");
				selectedGlOption = doc1.getAttribute("/GENERAL_LEDGER_CODE");
				selectedDiscOption = doc1.getAttribute("/DISCOUNT_CODE");
				oid = doc1.getAttribute("/MATCH_PAY_DED_GL_DTLS_OID");
				discDesc = doc1.getAttribute("/DISCOUNT_DESCRIPTION");
		  	}else{
				appliedAmount = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/applied_amount");
				discComments = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/discount_comments");
				transType = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/transaction_type");
				selectedGlOption = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/general_ledger_code");
				selectedDiscOption = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/discount_code");
				if(StringFunction.isNotBlank(doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/match_pay_ded_gl_dtls_oid")))
					oid = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/match_pay_ded_gl_dtls_oid");
				else
					oid="0";
				discDesc = doc.getAttribute("/In/MatchPayDedGlDtls("+i+")/discount_description");	  		
		  	}
		  	
		  	transTypeOptions = Dropdown.createGLCategoryOptions(resMgr, transType).toString();		  	

		  	discOptions=Dropdown.createMultiTextOptions(glCodesDoc,"ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION","","ERP_GL_CATEGORY",TradePortalConstants.ERP_DISCOUNT, resMgr.getResourceLocale());
		  	payOptions=Dropdown.createMultiTextOptions(glCodesDoc,"ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION","","ERP_GL_CATEGORY",TradePortalConstants.ERP_PAYMENT, resMgr.getResourceLocale());
		  	overPayOptions=Dropdown.createMultiTextOptions(glCodesDoc,"ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION","","ERP_GL_CATEGORY",TradePortalConstants.ERP_OVERPAYMENT, resMgr.getResourceLocale());
		    //	
		    //transGlOptions = Dropdown.createMultiTextOptions(glCodesDoc,"ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION",selectedGlOption, resMgr.getResourceLocale());
		  	transGlOptions = Dropdown.createMultiTextOptions(glCodesDoc,"ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION",selectedGlOption,"ERP_GL_CATEGORY",transType, resMgr.getResourceLocale());
		  	discountOptions = Dropdown.createMultiTextOptions(discCodesDoc,"DISCOUNT_CODE","DISCOUNT_CODE,DISCOUNT_DESCRIPTION",selectedDiscOption, resMgr.getResourceLocale());
		  	
		  //Start summing each transaction type totals
		  // Use BigDecimal instead Long
			
			BigDecimal enteredAmt = BigDecimal.ZERO;
		  	if(StringFunction.isNotBlank(appliedAmount)){
			try{
		  		enteredAmt= new BigDecimal(appliedAmount);
		  	}catch(NumberFormatException nfe){
				Debug.debug("nfe caught:"+nfe);
			}
		  	if(TradePortalConstants.ERP_PAYMENT.equalsIgnoreCase(transType)){
		  	//	if(StringFunction.isNotBlank(appliedAmount))
		  		totalPay += enteredAmt.doubleValue();
		  	}else if(TradePortalConstants.ERP_OVERPAYMENT.equalsIgnoreCase(transType)){
		  	//	if(StringFunction.isNotBlank(appliedAmount))
		  		totalOverPay += enteredAmt.doubleValue();
		  	}else if(TradePortalConstants.ERP_DISCOUNT.equalsIgnoreCase(transType)){
		  	//	if(StringFunction.isNotBlank(appliedAmount))
		  		totalDiscount += enteredAmt.doubleValue();
		  	}
			 appliedAmount = TPCurrencyUtility.getDisplayAmount(appliedAmount, currency, loginLocale);	
		  	}
		  	
%>	
	
		<tr>
			<td>
			<%=(i+1) %>
			</td> 
			<%
			//The first row will have the labels, other rows will not
			/* KMEhta added enableDisableComments() as a fix for IR T36000023161 on 8/1/2013 Start */ 
			// DK IR T36000027276 Rel9.0 06/16/2014 - Changed column width
			if(i==0){
			%>			
					<td>
					   <%= widgetFactory.createSelectField("transType"+i+"", "PaymentDiscountDetail.TransactionType", 
					    " ", transTypeOptions, isReadOnly, false, false,"onChange=\"enableDisableComments("+i+");sumAmount("+i+")\"style='width: 100px'", "", "") %>      
					</td> 
					<td>      
					  <%=widgetFactory.createTextField("appliedAmnt"+i+"","PaymentDiscountDetail.AppliedAmount",
							  appliedAmount ,"25",isReadOnly,false,false, "onChange=\"sumAmount("+i+")\"style='width: 80px'", "", "") %>
					</td>
					<td>
					  <%= widgetFactory.createSelectField("transGlCode"+i+"", "PaymentDiscountDetail.TransactionGlCode", 
					    " ", transGlOptions, isReadOnly, false, false,"style='width: 105px'", "", "") %>      
					</td> 
					<td>
					  <%= widgetFactory.createSelectField("discountCode"+i+"", "PaymentDiscountDetail.DiscountCode", 
					    " ", discountOptions, isReadOnly, false, false,"style='width: 100px'", "", "") %>      
					</td>  
					<td>         
					  <%=widgetFactory.createTextField("comments"+i+"","PaymentDiscountDetail.Comments",
					discComments ,"100",isReadOnly,false,false, "style='width: 170px'", "", "") %>  
					 </td>	
			<%}else{ %>   
					<td>
					   <%= widgetFactory.createSelectField("transType"+i+"", "", 
					    " ", transTypeOptions, isReadOnly, false, false,"onChange=\"enableDisableComments("+i+");sumAmount("+i+")\"style='width: 100px'", "", "") %>      
					</td> 
					<td>      
					  <%=widgetFactory.createTextField("appliedAmnt"+i+"","",
					appliedAmount ,"25",isReadOnly,false,false, "onChange=\"sumAmount("+i+")\"style='width: 80px'", "", "") %>
					</td>
					<td>
					  <%= widgetFactory.createSelectField("transGlCode"+i+"", "", 
					    " ", transGlOptions, isReadOnly, false, false,"style='width: 105px'", "", "") %>      
					</td> 
					<td>
					  <%= widgetFactory.createSelectField("discountCode"+i+"", "", 
					    " ", discountOptions, isReadOnly, false, false,"style='width: 100px'", "", "") %>      
					</td>  
					<td>         
					  <%=widgetFactory.createTextField("comments"+i+"","",
					discComments ,"100",isReadOnly,false,false, "style='width: 170px'", "", "") %>  
					 </td>				
			<%} %>                                             
		</tr>		
		<%--  KMEhta added enableDisableComments() as a fix for IR T36000023161 on 8/1/2013 End --%>
		<input type=hidden value="<%=oid %>" name="oid<%=+i%>">
		<input type=hidden value="<%=invoiceId %>" name="invoiceId<%=+i%>">
		<% 
		//IR T36000016039 - lookup discount description
		if(StringFunction.isNotBlank(selectedDiscOption) && StringFunction.isNotBlank(transType) && transType.equals(TradePortalConstants.ERP_DISCOUNT)){
			java.util.List<Object> sqlParamsLst1 = new java.util.ArrayList();
			   StringBuilder sqlDiscount = new StringBuilder();
			   sqlDiscount.append("select DISCOUNT_CODE, DISCOUNT_DESCRIPTION ");
			   sqlDiscount.append(" from customer_discount_code"); 
			   sqlDiscount.append(" where p_owner_oid in (?) and deactivate_ind = ? ");
			   sqlDiscount.append(" AND Discount_Code = ? ");
			   
			   sqlParamsLst1.add(userSession.getOwnerOrgOid());
			   sqlParamsLst1.add(TradePortalConstants.INDICATOR_NO);
			   sqlParamsLst1.add(selectedDiscOption);
			   //Add bank codes only if user is set to use both bank and customer defined codes
			   //IR T36000016764
			   if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(erpSetting) && TradePortalConstants.BANK_AND_CUSTOMER_ERP_CODES.equalsIgnoreCase(codeType)){
				   sqlDiscount.append(" union ");
				   sqlDiscount.append(" select code as DISCOUNT_CODE, descr as DISCOUNT_DESCRIPTION from bankrefdata");
				   sqlDiscount.append(" where table_type ='DISCOUNT_CODE' and client_bank_id = ? AND code = ? ");
				   sqlParamsLst1.add(userSession.getLoginOrganizationId());
				   sqlParamsLst1.add(selectedDiscOption);
			   }			
		    DocumentHandler discCodeDoc = DatabaseQueryBean.getXmlResultSet(sqlDiscount.toString(), false, sqlParamsLst1);
		    if (discCodeDoc != null){
			   discDesc = discCodeDoc.getAttribute("/ResultSetRecord/DISCOUNT_DESCRIPTION");
			} 
		}else{
			discDesc = "";
		}
		%>
		<input type=hidden value="<%=discDesc %>" name="discountDesc<%=+i%>">
<%
	  }
//}
%>		
		</table>

<div class="formItem readOnly" >
	<div>
	<label for="totalPayments">
	<%=resMgr.getText("PaymentDiscountDetail.TotalPayments",
                    TradePortalConstants.TEXT_BUNDLE)%>
	</label> 

	<label style="padding-left:40px;" for="totalOverPayments">
	<%=resMgr.getText("PaymentDiscountDetail.TotalOverPayments",
                    TradePortalConstants.TEXT_BUNDLE)%>
     </label>

	<label style="padding-left:22px;" for="totalDiscounts">
	<%=resMgr.getText("PaymentDiscountDetail.TotalDiscounts",
                    TradePortalConstants.TEXT_BUNDLE)%>
     </label>
                                        
	</div>
</div>
		
<div class="formItem inline readOnly" >
<input data-dojo-type="dijit.form.TextBox" name="totalPayments" id="totalPayments" maxLength="30" class="char6" value="<%=TPCurrencyUtility.getDecimalDisplayAmount(""+totalPay,curDecimal,loginLocale)%>" readonly="true">
</div>		
<div class="formItem inline readOnly" >
<input data-dojo-type="dijit.form.TextBox" name="totalOverPayments" id="totalOverPayments" maxLength="30" class="char6" value="<%=TPCurrencyUtility.getDecimalDisplayAmount(""+totalOverPay,curDecimal,loginLocale) %>" readonly="true">
</div> 
<div class="formItem inline readOnly" >
<input data-dojo-type="dijit.form.TextBox" name="totalDiscounts" id="totalDiscounts" maxLength="30" class="char6" value="<%=TPCurrencyUtility.getDecimalDisplayAmount(""+totalDiscount,curDecimal,loginLocale) %>" readonly="true">
</div>

<div>
 <%-- special div --%>
<div style="clear:both;">&nbsp;</div>
<%--
 </div>
 --%>


  <div class='sidebarButtons' style='background-color: #f0f0f0;margin-left: -7px;'>
	<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <button data-dojo-type="dijit.form.Button"  name="SaveAndClosePD" type="button" id="SaveAndClosePD" data-dojo-props="iconClass:'saveclose'" >
				<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				addFieldsToForm();			
				//setButtonPressed('SaveAndClosePD', '0');
					//document.forms[0].submit();
			 	</script>
			</button>
			<%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %>      
	
		  
        <button data-dojo-type="dijit.form.Button" name="Close" id="Close" type="button" data-dojo-props="iconClass:'close'" >
				<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
 				var r=confirm("Changes will not be saved");
				if (r==true)
 				{
					hideDialog();   	  
 				}
                					
        </script>
	</button> 
        <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
		<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>
 <%--
</div> --%>         <%--  Form Content End --%>
<%--formArea--%>
<%--
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PayDiscountDetailForm'">
 <%@ include file="./fragments/PaymentDiscountDetail_CommandBar.frag" %>                   
</div> 
--%>
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="<%=invOutstandingAmount%>" name="invTotalAmount">
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="<%=invPayDate%>" name="invPayDate">
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="<%=payMatchResultOid%>" name="remitOid">
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="N" name="payMatchErrorInd">
		
		<%--
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="0" name="payTotal" id="payTotal">
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="0" name="overPayTotal" id="overPayTotal">
		<input data-dojo-type="dijit.form.TextBox" type="hidden" value="0" name="discountTotal" id="discountTotal">
		 --%>

  <%= formMgr.getFormInstanceAsInputField("PayDiscountDetailForm", secureParams) %>
 <div id="saveAsDialogId" style="display: none;"></div>
 </div>
 
 </div> 
 <div style="clear:both;">&nbsp;</div>
</form>

<%--</div> --%>           <%--  Page Content End --%> 
<%--</div> --%>           <%--  Page Main End --%>
<%--
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
--%>
 

 
<%
  String focusField = "nameField";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script type='text/javascript'>

function hideDialog(){
    require(["t360/dialog"], 
                function(dialog) {
		
    dialog.close('PayDiscountDetailDialog');

});
    }

function openURL(URL){
		document.location.href  = URL;
		return false;
	}

 require(["dijit/registry", "dojo/ready", "dojo/domReady!"], 
    function(registry, ready ) {
      ready(function() {
		  
        var focusFieldId = '<%= focusField %>';
		
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if(focusField){ 
                  focusField.focus();
            }
        }
        
      	<%-- Rel-8.4 UB IR#T36000023161 on 12/06/2013 - Begin --%>
        for(var loop = 0; loop<8; loop++){
        	var transType = registry.byId("transType"+loop).value;
        	var commentsField = registry.byId("comments"+loop);
        	<%-- Rel-9.1 IR#T36000033996 on 11/03/20134 - For 'Payment' or 'Over-payment' transaction type, the 'Discount Code' should be greyed out and protected. --%>
        	var discountCodeField = registry.byId("discountCode"+loop);
        	
        	if(transType=="<%=TradePortalConstants.ERP_DISCOUNT%>"){
        		discountCodeField.setDisabled(false);
        		commentsField.setDisabled(false);
        	}else{
        		discountCodeField.setValue("");
        		commentsField.setValue("");
        		
        		discountCodeField.setDisabled(true);
        		commentsField.setDisabled(true);
        	}
        }
      	<%-- Rel-8.4 UB IR#T36000023161 on 12/06/2013 - End --%>
      });
	var errSize = "<%=error.size()%>";
	
	if(errSize>0){
	 require(["t360/dialog"], function(dialog) {
	
      dialog.open('PayDiscountDetailErrorDialog',"Error Details" ,
                  'PaymentDiscountDetailsErrorDialog.jsp',
                  ['error','dialogId'],[null,'PayDiscountDetailErrorDialog'], <%-- parameters --%>
                  null, null);
    });
	}
  });
  
	<%-- Rel-8.4 UB IR#T36000023161 on 12/06/2013 - Begin --%>
	function enableDisableComments(fieldIndex){
		require(["dijit/registry"], function(registry) {
			<%-- Rel-9.1 IR#T36000033996 on 11/03/20134 - For 'Payment' or 'Over-payment' transaction type, the 'Discount Code' should be greyed out and protected. --%>
			if(registry.byId("transType"+fieldIndex).value =="<%=TradePortalConstants.ERP_DISCOUNT%>"){
				registry.byId("comments"+fieldIndex).setDisabled(false);
				registry.byId("discountCode"+fieldIndex).setDisabled(false);
			}else{
				registry.byId("comments"+fieldIndex).setValue("");
				registry.byId("discountCode"+fieldIndex).setValue("");
				registry.byId("comments"+fieldIndex).setDisabled(true);
				registry.byId("discountCode"+fieldIndex).setDisabled(true);
			}
		});
	}
	<%-- Rel-8.4 UB IR#T36000023161 on 12/06/2013 - End --%>

  <%-- dojo.require("dijit.form.TextBox"); --%>
	function sumAmount(idx) {
		dojo.ready(function(){
			var myObj = dijit.byId("transType"+idx);
			var transTypeValue = ""+myObj;
		  
		  var payTotal=0;
		  var discountTotal=0;
		  var overPayTotal=0;
		  <%-- options --%>
		  var discOptions = '<%=discOptions%>';
		  var payOptions = '<%=payOptions%>';
		  var overPayOptions = '<%=overPayOptions%>';

		  <%-- browser display --%>
		  var totalPay = dijit.byId("totalPayments");
		  var totalDiscount = dijit.byId("totalDiscounts");
		  var totalOverPay = dijit.byId("totalOverPayments");
		
		   <% for(int jj=0; jj<8; jj++){%>
			  var transType<%=jj%> = dijit.byId("transType<%=jj%>");
			  var theAmount<%=jj%> = dijit.byId("appliedAmnt<%=jj%>");
			    
			  if(transType<%=jj%> =="<%=TradePortalConstants.ERP_PAYMENT%>"){
				  payTotal = (payTotal*1)+(theAmount<%=jj%>.get("value").replace(",","")*1);
			  }else if(transType<%=jj%> =="<%=TradePortalConstants.ERP_OVERPAYMENT%>"){
				  overPayTotal = (overPayTotal*1)+(theAmount<%=jj%>.get("value").replace(",","")*1);
			  }else if(transType<%=jj%> =="<%=TradePortalConstants.ERP_DISCOUNT%>"){
				  discountTotal = (discountTotal*1)+(theAmount<%=jj%>.get("value").replace(",","")*1);
			  }
				  
		  <% }%>
		  var xhReq;
		  var req;
			  var curDecimalPlaces = '<%=decimalPlaces%>';
			  xhReq = getXmlHttpRequest();
			  if(transTypeValue=='<%=TradePortalConstants.ERP_PAYMENT%>'){
				  req = "AppliedAmount=" +payTotal + "&curDecimalPlaces=" + curDecimalPlaces;
				  }else if(transTypeValue=='<%=TradePortalConstants.ERP_OVERPAYMENT%>'){
					  req = "AppliedAmount=" +overPayTotal + "&curDecimalPlaces=" + curDecimalPlaces;
				  }else if(transTypeValue=='<%=TradePortalConstants.ERP_DISCOUNT%>'){
					  req = "AppliedAmount=" +discountTotal + "&curDecimalPlaces=" + curDecimalPlaces;
				  }
			  xhReq = getXmlHttpRequest();
				xhReq.open("GET", '/portal/transactions/AmountFormatter.jsp?'+req, true);
				xhReq.onreadystatechange = updateAmount;
				xhReq.send(null);
				function getXmlHttpRequest()
				  {
				    var xmlHttpObj;
				    if (window.XMLHttpRequest) {
				      xmlHttpObj = new XMLHttpRequest();
				    }
				    else {
				      try {
				        xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
				      } catch (e) {
				        try {
				          xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
				        } catch (e) {
				          xmlHttpObj = false;
				        }
				      }
				    }
				    console.log(xmlHttpObj);
				    return xmlHttpObj;
				  }
				function updateAmount(){
	  				require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom){
	  					 if (xhReq.readyState == 4 && xhReq.status == 200) {	
		  						 var formattedAmt = xhReq.responseText;	
			  						if(transTypeValue=='<%=TradePortalConstants.ERP_PAYMENT%>'){
				  						totalPay.set("value",formattedAmt);
			  						  }else if(transTypeValue=='<%=TradePortalConstants.ERP_OVERPAYMENT%>'){
				  						totalOverPay.set("value",formattedAmt);
			  						  }else if(transTypeValue=='<%=TradePortalConstants.ERP_DISCOUNT%>'){
				  						totalDiscount.set("value",formattedAmt);
			  						  }
				  				  
	  			      } 
	  				});
	  			  }
 

		});
		var theid = idx;
		setTransGlCode(theid);
	}

	<%
	
	 String storeDisc = "", storePay = "", storeOverPay = "";
    try {
          StringBuilder acs = new StringBuilder();
          String[] arr = null;
         

          if (discOptions.length() > 0) {
                int count = 0;
                arr = discOptions.split("</option>");
                acs = new StringBuilder("data: [");
                //    out.println("array length ="+arr.length);
                for (count = 0; count < arr.length; count++) {
                      String str = arr[count];
                       int start = str.indexOf("=");
                       int end = str.indexOf(">");
                       String key = str.substring(start + 2, end - 1);
                       String val = str.substring(end + 1, str.length());
                       acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                      acs.append(",");
    
                }
                 acs.deleteCharAt(acs.length() - 1);
                 acs.append("]");
                 storeDisc = acs.toString();
           }

          if (payOptions.length() > 0) {
                int count = 0;
                arr = payOptions.split("</option>");
                acs = new StringBuilder("data: [");
                //    out.println("array length ="+arr.length);
                for (count = 0; count < arr.length; count++) {
                      String str = arr[count];
                      int start = str.indexOf("=");
                       int end = str.indexOf(">");
                       String key = str.substring(start + 2, end - 1);
                       String val = str.substring(end + 1, str.length());
                       acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                      acs.append(",");
                 }
                 acs.deleteCharAt(acs.length() - 1);
                 acs.append("]");
                 storePay = acs.toString();
           }

          if (overPayOptions.length() > 0) {
                arr = overPayOptions.split("</option>");
                acs = new StringBuilder("data: [");
                int count = 0;
                 for (count = 0; count < arr.length; count++) {
                      String str = arr[count];
                       int start = str.indexOf("=");
                       int end = str.indexOf(">");
                       String key = str.substring(start + 2, end - 1);
                       String val = str.substring(end + 1, str.length());
                       acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                      acs.append(",");
   
                }
                 acs.deleteCharAt(acs.length() - 1);
                 acs.append("]");
                 storeOverPay = acs.toString();
           }

    } catch (Exception ex) {
          ex.printStackTrace();
    }
%>
	function setTransGlCode(idx) {
		var myObj = dijit.byId("transType"+idx);
		var transacType = ""+myObj;
	       var tcStore = new dojo.store.Memory({
	    });
	      
	      if (transacType == '<%= TradePortalConstants.ERP_OVERPAYMENT%>'){	           
	            tcStore = new dojo.store.Memory({
	              <%=storeOverPay%>
	          });

	      } else if (transacType == '<%= TradePortalConstants.ERP_DISCOUNT%>'){	            
	            tcStore = new dojo.store.Memory({
	              <%=storeDisc%>
	          });	                  
	      } else if (transacType == '<%= TradePortalConstants.ERP_PAYMENT%>'){
	            tcStore = new dojo.store.Memory({
	              <%=storePay%>
	          });      
	      }
	var tc = dijit.byId("transGlCode"+idx);
	tc.attr('value','');
	tc.store = tcStore;
	
	}
	
	function addFieldsToForm(){

		  require(["t360/common"], function(common) {
		    buttonName = 'SaveAndCloseErp',
		    formName = 'PayDiscountDetailForm';				
			var myForm = document.getElementsByName(formName)[0];
			common.submitForm(formName,buttonName);
			
		  });
	}
	
</script>
<%
  }
%>


</body>

</html>

<%
  /**********
   * CLEAN UP
   **********/
if(TradePortalConstants.INDICATOR_YES.equals(showErrorDialog)){
   beanMgr.unregisterBean("MatchPayDedGlDtls");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
  
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
}
%>