<%--
*******************************************************************************
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%

	
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;	
	String transaction_oid = StringFunction.xssCharsToHtml(request.getParameter("transactionOid"));
	String panelAuthRangeOid = StringFunction.xssCharsToHtml(request.getParameter("panelAuthRangeOid"));
%>

<%-- ********************* HTML for page begins here *********************  --%>
<div class="formContent" style="padding:10px;height:300px;width:400px">


<input type=hidden name="buttonName" value="">  
<%
gridLayout = dgFactory.createGridLayout("viewBeneDataGridId", "PendingPanelAuthBeneficiariesDataGrid");
gridHtml = dgFactory.createDataGrid("viewBeneDataGridId","PendingPanelAuthBeneficiariesDataGrid",null);
%>
<%=gridHtml%>
</div>



    
 <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;								
		var initSearchParms= "transactionOid=<%=StringFunction.escapeQuotesforJS(transaction_oid)%>&panelAuthRangeOid=<%=StringFunction.escapeQuotesforJS(panelAuthRangeOid)%>";
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PendingPanelAuthBeneficiariesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("viewBeneDataGridId", viewName,gridLayout, initSearchParms);
		
		require(["dijit/registry","dojo/ready", "dojo/domReady!"],
		function(registry, ready ) {
			var myGrid = registry.byId("viewBeneDataGridId");
	    	myGrid.set('autoHeight',10);
		});
		
		
		
		function closeDialog(){
			require(["dijit/registry"],
					 function(registry) {																		
						hideDialog("PendingPanelAuthViewBeneficiaryDialog");
			});

		}
	
	</script>   
	<%=widgetFactory.createHoverHelp("AddStructurePODialogCloseButton", "common.Cancel") %>	
</body>
</html>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

