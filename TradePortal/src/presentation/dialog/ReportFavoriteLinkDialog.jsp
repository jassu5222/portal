<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,com.amsinc.ecsg.html.*,com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.frame.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>



<%

  String userOid = userSession.getUserOid();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  Hashtable<String, String> secureParams = new Hashtable<String, String>();
  secureParams.put("user_oid", userOid);
  secureParams.put("login_oid", userOid);
  secureParams.put("login_rights", userSession.getSecurityRights());
  secureParams.put("ownership_level", userSession.getOwnershipLevel());
  secureParams.put("ownership_type",TradePortalConstants.OWNER_TYPE_NON_ADMIN);
  secureParams.put("owner_oid", userSession.getOwnerOrgOid());
  secureParams.put("favoriteType", TradePortalConstants.FAV_TYPE_REPORT);

  String focusField = "";

  //get the user's Favorite reports
  //if no favorite saved, then go with the default

  //determine current user preferences
  List<String> favoriteOids = new ArrayList<String>();
  List<String> favoriteIds = new ArrayList<String>();
  List<String> favoriteAddlParms = new ArrayList<String>();
  List<String> favoriteOptLocks = new ArrayList<String>();
  StringBuilder favIdJsArraySB = new StringBuilder();
  favIdJsArraySB.append("[");
//jgadela R91 IR T36000026319 - SQL INJECTION FIX
  String favSql = "select favorite_task_cust_oid, favorite_id, addl_params, opt_lock from favorite_task_customization "
  						 +" where a_user_oid = ? and favorite_type = ? order by display_order";
  DocumentHandler favListDoc = DatabaseQueryBean.getXmlResultSet(favSql, false, userOid, TradePortalConstants.FAV_TYPE_REPORT);
  if (favListDoc != null) {
    Vector favList = favListDoc.getFragments("/ResultSetRecord");
    for (int i = 0; i < favList.size(); i++) {
      DocumentHandler favDoc = (DocumentHandler) favList.get(i);
      String favoriteOidAttr = favDoc
        .getAttribute("/FAVORITE_TASK_CUST_OID");
      favoriteOids.add(favoriteOidAttr);
      String favoriteIdAttr = favDoc.getAttribute("/FAVORITE_ID");
      favoriteIds.add(favoriteIdAttr);
      String favoriteAddlParmAttr = favDoc.getAttribute("/ADDL_PARAMS");
      //Added by dillip for defect #T36000010012
      if (favoriteAddlParmAttr.equals(null) || favoriteAddlParmAttr.equals("")){
    	  favoriteAddlParmAttr=favoriteIdAttr;
      }
      //Ended Here
      favoriteAddlParms.add(favoriteAddlParmAttr);
      String optLockAttr = favDoc.getAttribute("/OPT_LOCK");
      favoriteOptLocks.add(optLockAttr);
      favIdJsArraySB.append("'").append(favoriteIdAttr).append("'");
      if ( i<favList.size()-1) {
        favIdJsArraySB.append(",");
      }
    }
  }
  favIdJsArraySB.append("]");
  String favIdJsArray = favIdJsArraySB.toString();
  //no default


  //cquinton 12/11/2012 just by entering this dialog, we clear the favorite reports in the user session
  // so the menu will be regenerated on page transition
  userSession.setFavoriteReports(null);

  //cquinton 1/22/2013 get the user categories for standard reports
  // jgadela 01/22/2014 Rel 8.4 IR T36000023810 [START ]- Fixed the issue to get standard reports in Favorite dialog page
  StringBuffer userCategories = new StringBuffer();
  String categories = RepUtility.getUserCategorySQL(TradePortalConstants.REPORT_ALL_CAT,
  beanMgr, userSession.getUserOid(), userSession.hasSavedUserSession(), userSession.getOwnerOrgOid(), userSession.getSecurityType());
  String sqlQryCat = null;
  
  if(categories.contains(TradePortalConstants.REPORT_ALL_CAT))
  {
	  categories = "'"+TradePortalConstants.REPORT_ALL_CAT+"',"+categories;
  }
  //jgadela R91 IR T36000026319 - SQL INJECTION FIX
  
  List<Object> sqlParamsLst = new ArrayList<Object>();
  sqlParamsLst.add("REPORTING_CATEGORY");
  
  if(userSession.getReportingLanguage()!= null && userSession.getReportingLanguage().toLowerCase().contains("fr")){
		
		sqlQryCat = " select CODE, DESCR from refdata where table_type = ? and CODE in ("+SQLParamFilter.preparePlaceHolderStrForInClause(categories, sqlParamsLst)+") and locale_name = 'fr_CA' ";
  }else{
		sqlQryCat = " select CODE, DESCR from refdata where table_type = ?  and CODE in ("+SQLParamFilter.preparePlaceHolderStrForInClause(categories, sqlParamsLst)+") and locale_name is NULL";
  }
  DocumentHandler resultXML = DatabaseQueryBean.getXmlResultSet(sqlQryCat, false, sqlParamsLst);

  if(resultXML != null){   			    	   
     Vector totalCatg =  resultXML.getFragments("/ResultSetRecord");   
     int catgCount = totalCatg.size();
     for (int i = 0; i < catgCount; i++)
       {
	   String category    = resultXML.getAttribute("/ResultSetRecord(" + i + ")/DESCR");
	   userCategories.append(""+category+"");
         if ( i < catgCount-1) {
        	 userCategories.append(",");
         }                              
       }     
  }
 //jgadela 01/22/2014 Rel 8.4 IR T36000023810 [END]


  // a variable to determine whether section should be displayed
  boolean displayReport = false;

  //cquinton 12/11/2012 - this dialog requires some javascript to execute to finish rendering
  // extend the 'Loading...' message during that time. dialogLoading class is removed when complete.
%>
<div id="favReportDialogMoreLoading" class='dijitContentPaneLoading'>
  <span class='dijitInline dijitIconLoading'></span>
  <%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE)%>
</div>
<div id="favReportDialogContent" class="dialogContent itemSelectWidth dialogLoading">
  <%--cquinton 1/28/2013 add error section to this dialog--%>
  <div id="favReportErrorSection" class="errorSection">
  </div>

  <form name="Favorite-Report-Customization"  id="Favorite-Report-Customization" method="POST"
        data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
    <input type=hidden value="Save" name="buttonName" />

    <span class="customizationColumn1">
      <div class="instruction">
        <%=resMgr.getText("FavoriteReport.Step1",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>
      <div class="customizationItemsHeader">
        <%=resMgr.getText("FavoriteReport.AvailableReport",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>

<%
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_STANDARD_REPORTS ) ) {
%>
      <div data-dojo-type="dijit.TitlePane" id="StandardReportSection" class="availItemsSection"
           title='<%=resMgr.getText("FavoriteReport.StandardReports", TradePortalConstants.TEXT_BUNDLE)%>'>
        <%--include a loading image until data is loaded--%>
        <div id="standardReportsLoading">
          <span class='dijitInline dijitIconLoading'></span>
          <%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE)%>
        </div>
      </div>
<%
  }

  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_CUSTOM_REPORTS ) ) {
%>

      <div data-dojo-type="dijit.TitlePane" id="CustomReportSection" class="availItemsSection"
           title='<%=resMgr.getText("FavoriteReport.CustomReports", TradePortalConstants.TEXT_BUNDLE)%>'>
        <%--include a loading image until data is loaded--%>
        <div id="customReportsLoading">
          <span class='dijitInline dijitIconLoading'></span>
          <%=resMgr.getText("common.Loading", TradePortalConstants.TEXT_BUNDLE)%>
        </div>
      </div>
<%
  }
%>
    </span>
    <span class="customizationColumnDivider"></span>
    <span class="customizationColumn2">
      <div class="instruction">
        <%=resMgr.getText("FavoriteReport.Step2",
	     TradePortalConstants.TEXT_BUNDLE)%>
      </div>
      <div class="customizationItemsHeader">
        <%=resMgr.getText("FavoriteReport.SelectedReport",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>

      <div data-dojo-type="dijit.TitlePane" id="SelectedReportItems" class="selectedItemsSection"
           title='<%=resMgr.getText("FavoriteReport.Order",
                                    TradePortalConstants.TEXT_BUNDLE)%>'>
        <%--a dom element that is direct parent of individual selected items, so we can easily access--%>
        <div id="favselectedItems">
<%
  String displayKey = "";
  int selectedItemIdx = 0;
  int i = 0;
  for (i = 0; i < favoriteIds.size(); i++) {
    String favId = favoriteIds.get(i);

    if ( favId != null && favId.length()>0 ) {

      String favOid = "0"; //default to new
      String favAddlParms = "";
      String favOptLock = "";
      if (favoriteIds.size() == favoriteOids.size()) { //these are not default sections
        favOid = favoriteOids.get(i);
        favAddlParms = favoriteAddlParms.get(i);
        favOptLock = favoriteOptLocks.get(i);
      }
      //the report name is stored in the addlparms column, strip off the reporttype,
      // which trails after '|' delimiter
      StringTokenizer strTok = new StringTokenizer(favAddlParms,"|");
      String favDesc = "";
      if ( strTok.hasMoreTokens() ) {
        favDesc = strTok.nextToken();
      }
%>
        <%--use the sectionId as the main id for easy access on select/unselect--%>
        <div class="selectedItem">
          <input id="favOid<%=selectedItemIdx%>" name="favOid<%=selectedItemIdx%>" type="hidden" value="<%=favOid%>" />
          <span class="selectedFavItemOrder">
            <%--cquinton 12/11/2012 use a differnet id as dashboard prefs uses the same. note retain the "name" though for submission--%>
            <%--cquinton 12/20/2012 just do html here, do dijit creation later--%>
            <input id="fav_order<%=selectedItemIdx%>" name="fav_order<%=selectedItemIdx%>"
                   maxLength="3" class="selectedItemOrderTextBox" value="<%=selectedItemIdx+1%>"  />
          </span>
          <input id="favId<%=selectedItemIdx%>" name="favId<%=selectedItemIdx%>" class="selectedItemId" type="hidden" value="<%=favId%>" />
          <span id="favReportDesc<%=selectedItemIdx%>" class="selectedFavItemDesc">
            <%=favDesc%>
          </span>
          <input id="addlParams<%=selectedItemIdx%>" name="addlParams<%=selectedItemIdx%>" type="hidden" value="<%=favAddlParms%>" />
          <input id="favOptLock<%=selectedItemIdx%>" name="favOptLock<%=selectedItemIdx%>" type="hidden" value="<%=favOptLock%>" />
          <span class="deleteSelectedItem"></span>
          <div style="clear:both;"></div>
        </div>

<%
      //set focus field to first order item display order, if there is one
      if (selectedItemIdx == 0) {
        focusField = "fav_order0";
      }
      selectedItemIdx++;
    }
  }
%>
        </div>
        <div class="selectedItemsFooter">
          <div class="selectedItemsTotalCount">
            <span>
              <%=resMgr.getText("FavoriteReport.totalCount",
                   TradePortalConstants.TEXT_BUNDLE)%>&nbsp;
            </span>
            <span id="favselectedItems_totalCount"><%=favoriteIds.size()%></span>
          </div>
          <div class="clear:both;"></div>
        </div>
      </div>
      <button id="FavoriteReportsDialogUpdateButton" type="button">
        <%=resMgr.getText("common.update", TradePortalConstants.TEXT_BUNDLE)%>
      </button>
      <%=widgetFactory.createHoverHelp("FavoriteReportsDialogUpdateButton","FavoriteReport.Update") %>
      <br/><br/>
      <div class="instruction">
        <%=resMgr.getText("FavoriteReport.Instruction",
             TradePortalConstants.TEXT_BUNDLE)%>
      </div>
      <div align="right">
        <button id="FavoriteReportsDialogSaveButton" type="button">
          <%=resMgr.getText("common.save", TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("FavoriteReportsDialogSaveButton","SaveHoverText") %>
        <button id="FavoriteReportsDialogCancelButton" type="button">
          <%=resMgr.getText("common.cancel", TradePortalConstants.TEXT_BUNDLE)%>
        </button>
        <%=widgetFactory.createHoverHelp("FavoriteReportsDialogCancelButton","CloseHoverText") %>
      </div>
    </span>
    <%=formMgr.getFormInstanceAsInputField(
         "Favorite-Report-Customization", secureParams)%>
  </form>
</div>

    <%
    	formMgr.storeInDocCache("default.doc", new DocumentHandler());
    %>

<script type="text/javascript">

  <%-- define a local variable for functions --%>
  var favReportDialog = {
    <%-- a property that keep track of the last order field changed so that it sorts at the top --%>
    lastOrderChanged: undefined
  };

  <%-- define the set of field ids - this is used for adding/removing rows and updating row order --%>
  <%-- the array of field ids must be: 0=oid, 1=id, 2=order input, 3+ other fields --%>
	<%-- Srinivasu_D T36000034729 Rel9.1 11/19/2014 - addlParams added --%>
  var selectedItemFieldIds = [ "favOid", "favId", "fav_order", "favReportDesc", "addlParams","favOptLock" ];

  var favIds = <%= favIdJsArray %>;

  <%-- a variable to keep track of how many times we've gotten errors retrieving available reports --%>
  var availStandardReportErrors = 0;
  var availCustomReportErrors = 0;

  require(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class",
         "dojo/dom-style", "dojo/query",
         "dojo/_base/xhr",
         "dijit/registry", "dojo/on",
         "dijit/form/CheckBox",
         "dijit/form/Button",
         "t360/common", "t360/dialog",
         "t360/itemselect",
         "dojo/ready",
         "dojo/NodeList-traverse",
         "dojo/domReady!"],
      function(baseArray, dom,
             domConstruct, domClass,
             domStyle, query,
             xhr,
             registry, on,
             CheckBox,
             Button,
             common, dialog,
             itemSelect,
             ready ) {

    <%-- create widgets dynamically.  we do this way --%>
    <%--  rather than declaratively because the dialog is not parsed correctly the 2nd+ time it opened. --%>
    <%-- this should be better for performance as well --%>
    var myWidget = new Button({
      onClick: function() { itemSelect.updateOrder("favselectedItems", selectedItemFieldIds, favReportDialog.lastOrderChanged); }
    }, "FavoriteReportsDialogUpdateButton");
    myWidget = new Button({
      onClick: function() { registry.byId("Favorite-Report-Customization").submit(); }
    }, "FavoriteReportsDialogSaveButton");
    myWidget = new Button({
      onClick: function() { favReportDialog.close() }
    }, "FavoriteReportsDialogCancelButton");

    <%-- create a dijit for each order text box --%>
    var favSelectedItems = dom.byId("favselectedItems");
    if ( favSelectedItems ) {
      query('.selectedItemOrderTextBox', favSelectedItems).forEach( function(entry) {
        var orderTextBox = new dijit.form.TextBox({
          id: entry.id,
          name: entry.name,
          value: entry.value,
          intermediateChanges: true, <%-- after each keystroke! ie8 does fire normal on button click --%>
          onChange: function() {
            favReportDialog.lastOrderChanged = entry.id;
          }
        }, entry.id);
        domClass.add(orderTextBox.domNode,"selectedItemOrderTextBox");
      });
    }


    favReportDialog.createAvailItem = function(item, reportType, domNodeParent) {
      if ( item.DOCID && item.NAME ) {
        <%-- add the avail item --%>
        var newAvailItem = domConstruct.create("div", {id:item.DOCID+"_avail", name:item.NAME}, domNodeParent);
        domClass.add(newAvailItem,"availItem");
        var newSelect = domConstruct.create("span", null, newAvailItem);
        domClass.add(newSelect,"availItemSelect");
        var newCheckbox = domConstruct.create("input",
          { id:item.DOCID+"_checkbox", type:"checkbox" },
          newSelect);
        var newDesc = domConstruct.create("span", {innerHTML:item.NAME}, newAvailItem);
        domClass.add(newDesc,"availItemDesc");
        <%-- we put name in addlparms! along with reportType, delimited by "|" --%>
        var newAddlParms = domConstruct.create("input",
          { id:item.DOCID+"_addlParms", type:"hidden", value:item.NAME+"|"+reportType },
          newAvailItem);
        <%-- now add the checkbox widget --%>
        var checkIt = false;
        if ( baseArray.indexOf(favIds,item.DOCID)>=0 ) {
          checkIt = true;
        }
        new CheckBox({
          checked: checkIt,
          onChange: function(isChecked) { favReportDialog.onAvailItemChange(this, isChecked); }
        }, newCheckbox);
      }
      else {
        <%-- format of data returned is not right, raise an error --%>
        if ( "S" == reportType ) {
          availStandardReportErrors++;
        }
        else {
          availCustomReportErrors++;
        }
      }
    };

    <%-- now get the reports - from same data view the report pages use --%>
    <%--  but just get all of them - 1000 report limit set here --%>
    <%-- standard first --%>
    <%-- cquinton 3/8/2013 change to post to avoid bad css chars in url --%>
    <%-- Rel9.2 IR T36000032596 Encrypting dataViewName --%>
    xhr.post({
      <%-- cquinton 1/22/2013 pass in user categories --%>
      url: "/portal/getViewData?vName=<%=EncryptDecrypt.encryptStringUsingTripleDes("ReportsDataView",userSession.getSecretKey())%>",
      postData: "reportType=S&categories=<%= userCategories.toString() %>&start=0&count=1000&sort=NAME",
      load: function(data) {
        domConstruct.destroy("standardReportsLoading");
        var standardReportsPane = dom.byId("StandardReportSection_pane"); <%-- get the standard reports section --%>
        baseArray.forEach(data.items, function(item) {
          favReportDialog.createAvailItem(item, 'S', standardReportsPane);
        });
        if ( availStandardReportErrors>0 ) {
        
        }
      },
      handleAs: "json-comment-optional",
      error: function(err, ioargs) {
        <%-- cquinton 1/28/2013 display issues errors in error section, but no duplicates --%>
        if ( err.responseText && err.responseText.length>0 ) {
          common.addXmlErrorsToErrorSection('favReportErrorSection', err.responseText, false);
          domConstruct.destroy("standardReportsLoading");
        }
        else {
          alert('Error retrieving reports:'+err);
          domConstruct.destroy("standardReportsLoading");
        }
      }
    });
    <%-- custom reports second --%>
    <%-- cquinton 3/8/2013 change to post to avoid bad css chars in url --%>
    xhr.post({
      <%-- cquinton 1/22/2013 pass in user categories --%>
      url: "/portal/getViewData?vName=<%=EncryptDecrypt.encryptStringUsingTripleDes("ReportsDataView",userSession.getSecretKey())%>",
      postData: "reportType=C&categories=<%= userCategories %>&start=0&count=1000&sort=NAME",
      load: function(data) {
        domConstruct.destroy("customReportsLoading");
        var customReportsPane = dom.byId("CustomReportSection_pane"); <%-- get the custom reports section --%>
        baseArray.forEach(data.items, function(item) {
          favReportDialog.createAvailItem(item, 'C', customReportsPane);
        });
        if ( availCustomReportErrors>0 ) {
         
        }
      },
      handleAs: "json-comment-optional",
      error: function(err) {
        <%-- cquinton 1/28/2013 display issues errors in error section, but no duplicates --%>
        if ( err.responseText && err.responseText.length>0 ) {
          common.addXmlErrorsToErrorSection('favReportErrorSection', err.responseText, false);
          domConstruct.destroy("customReportsLoading");
        }
        else {
          alert('Error retrieving reports:'+err);
          domConstruct.destroy("customReportsLoading");
        }
      }
    });


    <%-- selectItem is page specific, so we do it here rather than commonly in itemselect --%>
    favReportDialog.selectItem = function(checkboxWidget, selectedItemsId) {
      <%-- cquinton 12/14/2012 to simplify, set total count node after all is done --%>
      <%-- itemSelect.increaseTotalCount(selectedItemsId); --%>

      <%-- get selectItemId --%>
      var checkboxId = checkboxWidget.id;
      var idx = checkboxId.indexOf("_checkbox");
      var addSelectItemId = checkboxId.substring(0,idx);
      <%-- get the text to add --%>
      var description = "";
      <%-- only expect one but we use query for simplicity --%>
      query("#"+addSelectItemId+"_avail").children(".availItemDesc").forEach( function(entry, i) {
        description = entry.innerHTML;
      });
      var addlParms = dom.byId(addSelectItemId+"_addlParms").value;

      var selectItems = dom.byId(selectedItemsId); <%-- get the selectedItem parent --%>

      <%-- get index for next item --%>
      <%--  the fields must be sequential, zero based for submit to work --%>
      var nextIdx = itemSelect.getSelectedItemCount(selectedItemsId);

      <%-- get the next order # in sequence --%>
      var nextOrderValue = itemSelect.getMaxOrderValue(selectedItemsId) + 1;

      <%-- add the selected item --%>
      var newSelectItem = domConstruct.create("div", null, selectItems);
      domClass.add(newSelectItem,"selectedItem");
      var newOid = domConstruct.create("input",
        { id:"favOid"+nextIdx, name:"favOid"+nextIdx, type:"hidden", value:"0" },
        newSelectItem);
      var newSelectItemOrder = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemOrder,"selectedFavItemOrder");
      var newOrderInput = domConstruct.create("input",
        {id:"fav_order"+nextIdx, name:"fav_order"+nextIdx},
        newSelectItemOrder);
      var newOrderTextBox =
        new dijit.form.TextBox({
          name:"fav_order"+nextIdx,
          value: nextOrderValue,
          intermediateChanges: true, <%-- after each keystroke! ie8 does fire normal on button click --%>
          onChange: function() {
            favReportDialog.lastOrderChanged = "fav_order"+nextIdx;
          }
      }, newOrderInput);
      domClass.add(newOrderTextBox.domNode,"selectedItemOrderTextBox");
      var newSelectItemId = domConstruct.create("input",
        { id:"favId"+nextIdx, name:"favId"+nextIdx, type:"hidden", value:addSelectItemId },
        newSelectItem);
      domClass.add(newSelectItemId,"selectedItemId");
      var newSelectItemDesc = domConstruct.create("span",
        {id:"favReportDesc"+nextIdx, innerHTML:description},
        newSelectItem);
      domClass.add(newSelectItemDesc,"selectedFavItemDesc");
      var newAddlParams = domConstruct.create("input",
        { id:"addlParams"+nextIdx, name:"addlParams"+nextIdx, type:"hidden", value:addlParms },
        newSelectItem);
      var newOptLock = domConstruct.create("input",
        { id:"favOptLock"+nextIdx, name:"favOptLock"+nextIdx, type:"hidden", value:"" },
        newSelectItem);
      var newSelectItemDel = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemDel,"deleteSelectedItem");
      var newSelectItemClear = domConstruct.create("div", null, newSelectItem);
      domStyle.set(newSelectItemClear, "clear", "both");

      <%-- cquinton 12/14/2012 to simplify, set total count node after all is done --%>
      itemSelect.setTotalCount(selectedItemsId);
    };

    favReportDialog.onAvailItemChange = function(checkBoxWidget, checkValue) {
      if ( checkValue ) {
        <%-- do not allow more than 10 selected items --%>
        var tCount = itemSelect.getSelectedItemCount("favselectedItems");
        if ( tCount >= 10 ) {
          alert('A maximum of 10 reports may be selected.');
          checkBoxWidget.set("checked",false);
        }
        else {
          favReportDialog.selectItem(checkBoxWidget, "favselectedItems");
        }
      }
      else {
        itemSelect.unselectItem(checkBoxWidget, "favselectedItems", selectedItemFieldIds);
      }
    };

    favReportDialog.close = function(){
      dialog.hide('favReportDialog');
    };

    <%-- delete selected item event handler --%>
    <%-- cquinton 9/13/2012 ir#t36000004200 --%>
    <%-- use event delegation to get all interaction on any --%>
    <%--  delete selected item within selectedItems --%>
    <%-- this removes need to add/remove event handles as selected items are --%>
    <%--  added/removed --%>
    query('#favselectedItems').on(".deleteSelectedItem:click", function(evt) {
      itemSelect.deleteEventHandler(evt.target);
    });

    <%-- cquinton 12/20/2012 event delegation on change does not fire for ie7,8 --%>
    <%--  add individual event handler instead, for now. --%>
    <%-- put an onchange event handler on the order fields so we can keep track of --%>
    <%-- which one was touched last --%>
    <%-- query('#favselectedItems').on(".selectedItemOrderTextBox:change", function(evt) { --%>
    <%--   favReportDialog.lastOrderChanged = evt.target.id; --%>
    <%-- }); --%>

    <%-- now load the stuff that requires dojo to be loaded --%>
    ready(function() {
      var focusFieldId = '<%=focusField%>';
      if ( focusFieldId ) {
        <%-- var focusField = registry.byId(focusFieldId);    //NAVEEN- registry.byId()- Function not working --%>
        var focusField = document.getElementById(focusFieldId);
        focusField.focus();
      }



    });

    <%-- dialog is complete, actually display it now --%>
    domConstruct.destroy("favReportDialogMoreLoading");
    domClass.remove("favReportDialogContent", 'dialogLoading');
    registry.byId("favReportDialog")._position();
  });

</script>
