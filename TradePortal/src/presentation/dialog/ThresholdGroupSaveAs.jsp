<%--
 *  Bank Branch Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>


<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<div class="dialogContent padded">
  <form name="thresholdSaveAsForm"> 
  
  
  <%= widgetFactory.createTextField( "newThresholdName", "ThresholdGroupDetail.ThresholdGroupName", "", "35", false, true, false,  "", "","" ) %>                          
 	   <div> 
 	   
 	   <table><tr> <td>
 	   <div class="formItem">
 	   <button data-dojo-type="dijit.form.Button" type="button" id="saveAs" data-dojo-props="iconClass:'saveAs'">
        		<%=resMgr.getText("SecurityProfileDetail.SaveAsTitle", TradePortalConstants.TEXT_BUNDLE)%>
         		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        		submitSaveAs();
	      		</script>
     		</button>
     		</div>
     		</td>
 	   <td>
 	    <div class="formItem">
 	     <button data-dojo-type="dijit.form.Button" id="thresholdCloseButton" type="button" onClick="hideDialog('thresholdGroupSaveAsDialogId')" data-dojo-props="iconClass:'close'" ><%= resMgr.getText("common.cancel", 
                             TradePortalConstants.TEXT_BUNDLE) %>
                         
                             </button>
 	  <%= widgetFactory.createHoverHelp("saveAs", "SaveAsHoverText")%>
 	  <%= widgetFactory.createHoverHelp("thresholdCloseButton", "CloseHoverText")%>
 	  <%= widgetFactory.createHoverHelp("thresholdGroupSaveAsDialogIdId", "CloseHoverText")%>
 	  
 	   </div>
 	   </td></tr> </table>
 	   
             
        </div>
 
  </form> 
  
</div>
