<%--
*******************************************************************************
                              Transaction Terms Detail Page

  Description: 
    This is the main driver for the Transaction Terms Detail page.  It handles data
  retrieval of terms and terms parties (or retrieval from the input document)
  and creates the html page to display all the data for the transaction.
  This Jsp will call 3 jsp(s) each of which represents a different html section
  of this document.  This Jsp is designed to be static in that no data is 
  submitted for storage.  The primary action here is to display data for read-
  only.  Of Note: the TermsSummary.jsp is also shared by the CurrentTerms.jsp.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   Debug.debug("***START******************** Transaction - Terms Detail **************************START***");

    /*********\
    * GLOBALS *
    \*********/
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    boolean isReadOnly         = true;   //is the page read only
    boolean showSave           = false;  //do we show the save button
    boolean showDelete         = false;  //do we show the delete button
    boolean showTemplateSave   = false;
    boolean showTemplateDelete = false;
    boolean isTemplate	       = false;
    boolean isFromCurrentTerms = false;  //****** should be defaulted to 'false'
	//String transaction_oid=EncryptDecrypt.decryptStringUsingTripleDes(StringFunction.xssCharsToHtml(request.getParameter("rowKey")), userSession.getSecretKey());
    String transaction_oid = "";
    
    boolean showTermsSummary   = false;

    String date 	       = "";   	 //universal variable to display a date.
    String currencyCode;
    String attribute;			 //generic attribute used to get data from db
    String displayText;			 //after formating the attribute - display the result

    String goToInstrumentNavigator = "goToInstrumentNavigator";
    String goToInstrumentCloseNavigator;
    String complete_instrument_id="";
    String transaction_type="";

  // Various oid and status info from transaction and instruments used in
  // several places.
  String instrumentOid = "";         
  //String transactionOid=EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("rowKey"), userSession.getSecretKey());
  String transactionOid=StringFunction.xssCharsToHtml(request.getParameter("rowKey"));
  String DailogId=StringFunction.xssCharsToHtml(request.getParameter("DailogId"));
  String instrumentType;
  String instrumentStatus;
  String transactionType; 
  String transactionStatus;
  String termsPartyOid;

  //Variables used to pass parameters in the url - encrypted.
  String encryptVal1;
  String encryptVal2;
  String encryptVal3;
  String urlParm;
  String links = "";

  //Variables used to build the list of Document Images and Listview data for the Charges Section.
  DocumentHandler dbQuerydoc = null;
  StringBuffer query = null;
  boolean showPDFLinks = false;
  Vector listviewVector;

  String loginLocale = userSession.getUserLocale(); 	//need this for the formatting of dates
  String loginRights = userSession.getSecurityRights();

  DocumentHandler myDoc;				//used in the Document / Charges loops
							//when scrolling through a list of data.
							
  String linkArgs[] = {"","","",""};//Vshah CR-452
  //Komal PR BMO Issue No 49 							
  if(null!=request.getParameter("rowKey")){
  	transaction_oid = StringFunction.xssCharsToHtml(request.getParameter("rowKey"));
  }
  // These are the beans used on the page.

  /* TransactionWebBean transaction  	  = (TransactionWebBean) beanMgr.getBean("Transaction");
  InstrumentWebBean instrument    	  = (InstrumentWebBean) beanMgr.getBean("Instrument");
  TermsWebBean terms              	  = null;
  TermsWebBean termsForLoan        	  = null; //PUUH062751147

  TermsPartyWebBean termsPartyApplicant   = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");
  TermsPartyWebBean termsPartyBeneficiary = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

  String customerEnteredTerms = transaction.getAttribute("c_CustomerEnteredTerms");
  boolean hasCustEnteredTerms = InstrumentServices.isNotBlank( customerEnteredTerms );

  String bankReleasedTerms	= transaction.getAttribute("c_BankReleasedTerms");
  boolean hasBankReleasedTerms  = InstrumentServices.isNotBlank( bankReleasedTerms );

  transactionOid = transaction.getAttribute("transaction_oid");
  instrumentOid  = instrument.getAttribute("instrument_oid");
  encryptVal1 = EncryptDecrypt.encryptStringUsingTripleDes( transactionOid , userSession.getSecretKey()); 	//Transaction_Oid
  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( instrumentOid , userSession.getSecretKey());	//Instrument_Oid
  encryptVal3 = EncryptDecrypt.encryptStringUsingTripleDes( "true" , userSession.getSecretKey());		//From Transaction-TermsDetails.jsp flag

  urlParm = "&oid=" + encryptVal1 + "&instrumentOid=" + encryptVal2 + "&isFromTransTermsFlag=" + encryptVal3; */

  Debug.debug("populating beans from database");
  // We will perform a retrieval from the database.
  // Instrument and Transaction were already retrieved.  Get
  // the rest of the data

  /* if( hasBankReleasedTerms )
  {
	terms = transaction.registerBankReleasedTerms();
	termsForLoan = terms; //PUUH062751147
  }else{
	Debug.debug("Error: transaction is processed by bank by doesn't have bank released terms");
	Debug.debug("   Forwarding to the standard customer entered terms page.");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("InstrumentNavigator", request); */

%>	
<% /* } */
  
  // Now load the termsPartyApplicant and termsPartyBeneficiary 
  // webbeans.
%>
  

<%
  /* transactionType   = transaction.getAttribute("transaction_type_code");
  transactionStatus = transaction.getAttribute("transaction_status");
/* 
/*   instrumentType    = instrument.getAttribute("instrument_type_code");
/*   instrumentStatus  = instrument.getAttribute("instrument_status"); */
  /*  boolean isARMInstrument = false;
   boolean isRFInstrument = false; //IR - RIUJ021980931 Vshah
  //Pramey - 11/22/2008 CR-434 ARM Add Begin
  	if (instrumentType.equals(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT))
       isARMInstrument = true;  */
	//Pramey - 11/22/2008 CR-434 ARM Add End 

 /*  //IR - RIUJ021980931 Vshah
    if (instrumentType.equals(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT)) 
       isRFInstrument = true;    

  Debug.debug("Instrument Type "    + instrumentType);
  Debug.debug("Instrument Status "  + instrumentStatus);
  Debug.debug("Transaction Type "   + transactionType);
  Debug.debug("Transaction Status " + transactionStatus); */


  // The Terms Summary section of this page only displays under certain
  // conditions.  1.  The instrument type must be IMP_DLC, EXP_DLC, SLC,
  // GUAR.  2.  The transaction is the original transaction OR the 
  // transaction type is Amend or Advise.

  /* showTermsSummary = false;

  if (InstrumentServices.isNotBlank(instrumentType)) {
    if ((instrumentType.equals(InstrumentType.IMPORT_DLC)) ||
	(instrumentType.equals(InstrumentType.EXPORT_DLC)) ||
	(instrumentType.equals(InstrumentType.LOAN_RQST)) ||
	(instrumentType.equals(InstrumentType.REQUEST_ADVISE)) ||
	(instrumentType.equals(InstrumentType.INCOMING_SLC)) ||
	(instrumentType.equals(InstrumentType.INCOMING_GUA)) ||
	(instrumentType.equals(InstrumentType.STANDBY_LC)) ||
	(instrumentType.equals(InstrumentType.GUARANTEE)) ||
	(isARMInstrument)||
        (isRFInstrument) || //IR - RIUJ021980931 Vshah
	(instrumentType.equals(InstrumentType.FUNDS_XFER)) || // pcutrone CR-375-D FTR 09/28/2007
	(instrumentType.equals(InstrumentType.APPROVAL_TO_PAY)) || //rkrishna CR 375-D ATP 08/22/2007
	(instrumentType.equals(InstrumentType.DOMESTIC_PMT)) || // Peter Ng IR INUJ012743663
	(instrumentType.equals(InstrumentType.XFER_BET_ACCTS))) { // Peter Ng PSUJ011957828 2/27/2009
      if (InstrumentServices.isNotBlank(transactionType)) {
         if ((transactionType.equals(TransactionType.AMEND)) ||
             (transactionType.equals(TransactionType.ADVISE))) {
           showTermsSummary = true;
         }
         else
         {
           String origTrans = instrument.getAttribute("original_transaction_oid");
           if (transactionOid.equals(origTrans)) {
             showTermsSummary = true;
           }
        }
      }
    }
  } */

  /* boolean isFundsXfer = false;
  boolean isLoanRqst = false;
 
  
  // Certain fields display based on instrument types of funds transfer or loan request.
  if (instrumentType.equals(InstrumentType.FUNDS_XFER))
       isFundsXfer = true;
  if (instrumentType.equals(InstrumentType.LOAN_RQST)) 
       isLoanRqst = true;
       
  //Vshah CR-452 Add Begin
  boolean showPDFLinks = false;
  if ( (instrumentType.equals(InstrumentType.EXPORT_COL)) &&
       (transactionType.equals(TransactionType.ISSUE) || transactionType.equals(TransactionType.AMEND)) &&
       (instrument.getAttribute("suppress_doc_link_ind").equals(TradePortalConstants.INDICATOR_YES)) &&
       (transactionStatus.equals(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK)) &&
       (hasCustEnteredTerms) )
		showPDFLinks = true;
  
  String showNavBar = TradePortalConstants.INDICATOR_NO;
  if (isTemplate)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  String onLoad = "";
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  String templateFlag = isTemplate ? TradePortalConstants.INDICATOR_YES : TradePortalConstants.INDICATOR_NO;


 String certAuthURL = "";
  //Vshah CR-452 Add End     */ 
	
%>

<%-- Body tag included as part of common header --%>
<%-- HTML Code Begins here			--%>
<div class="formContent" style="padding:10px">
	<table>
		<tr>
			<td><%=resMgr.getText("TransactionQuickView.Documents", 
                               TradePortalConstants.TEXT_BUNDLE)%></td>
			<td><%@ include file="fragments/Transaction-TermsDetails-Documents.frag" %></td>
		</tr>
		<tr>
			<td><%=resMgr.getText("TransactionQuickView.Charges", 
                               TradePortalConstants.TEXT_BUNDLE)%></td>
			<td>
				
				<%@ include file="fragments/Transaction-TermsDetails-Charges.frag" %>        
				
			</td>
		</tr>
	</table>
             
	 
	 
  
 </div>


<%
try {
	StringBuffer query1 = new StringBuffer();
	
	query1.append("select i.complete_instrument_id as INSTRUMENT_ID,t.transaction_type_code as TRANSACTION_TYPE_CODE  ");
	query1.append("from instrument i, transaction t ");
	query1.append("where t.p_instrument_oid=i.instrument_oid and t.transaction_oid= ?");
	
	DocumentHandler xmlDoc = new DocumentHandler();
	xmlDoc = DatabaseQueryBean.getXmlResultSet(query1.toString(), false, new Object[]{transactionOid});
	listviewVector=null;
   	listviewVector = xmlDoc.getFragments("/ResultSetRecord");
   	DocumentHandler resultRow = (DocumentHandler)listviewVector.elementAt(0);
   	
		complete_instrument_id=resultRow.getAttribute("/INSTRUMENT_ID");
		Debug.debug("complete_instrument_id" + complete_instrument_id );
		transaction_type=ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.TRANSACTION_TYPE,resultRow.getAttribute("/TRANSACTION_TYPE_CODE"),loginLocale);
		Debug.debug("transaction_type"+transaction_type);	
    
	
} catch(Exception e) {
	Debug.debug("******* Error in calling DatabaseQueryBean *******");
	e.printStackTrace();
}
%>
<script type="text/javascript">
require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/ready"],
	        function(registry, query, on, dialog,ready ) {
	    	ready(function() {
	    	  dialog.doCallback('<%=StringFunction.escapeQuotesforJS(DailogId)%>', 'select',
	        		['complete_instrument_id','transaction_type','DailogId'],
	        		['<%=complete_instrument_id%>','<%=transaction_type%>','<%=StringFunction.escapeQuotesforJS(DailogId)%>']);
	    	});
	    });
</script>