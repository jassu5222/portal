<%--
*******************************************************************************
  Reset User Account

  Description:
     Displays a page where a user's account can be reset after being locked.
     This page is accessed by first going to the Locked Out User listview.
     An encrypted OID must be passed to this page from that listview to determine
     which user's account to reset.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.ams.tradeportal.busobj.util.*,
                 com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"/>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"/>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%

UserWebBean user = beanMgr.createBean(UserWebBean.class, "User");
  // Get the document from the cache.  This is used to set the radio buttons back to the
  // way the user had previously set them if there was an error.
  DocumentHandler doc = formMgr.getFromDocCache();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
  //for hsm debugging comment out above and uncomment out below
  //boolean instanceHSMEnabled = true;

  boolean changingPasswordFlag;

  String changingPasswordFlagFromXml = doc.getAttribute("/In/changingPasswordFlag");
  if(!InstrumentServices.isBlank(changingPasswordFlagFromXml) &&
      changingPasswordFlagFromXml.equals("true")) {
    changingPasswordFlag = true;
  }
  else
  {
    // Default the radio button setting and the focus to "don't change password"
    // if not coming from an error situation
    changingPasswordFlag = false;
  }
  String stricterPasswordSetting="";    
  String minimumPasswordLength=""; // CR-482
  String suffix = "";
 
%>

<%-- Body tag included as part of common header --%>

<div class="formContent" style="padding:10px">
  <form name="ResetUserAccount" id="ResetUserAccount" method="post" action="<%=formMgr.getSubmitAction(response)%>">

<%
if (instanceHSMEnabled)
{
	  
	
%>
    
    <input type=hidden name="encryptedCurrentPassword" value="">
    <%-- [START] CR-543 --%>
    <input type=hidden name="protectedCurrentPassword" value="">
    <input type=hidden name="protectedNewPassword" value="">
    <input type=hidden name="encryptedRetypedNewPassword" value="">
    
    <%-- [END] CR-543 --%>
    <input type=hidden name="passwordValidationError" value="">
<%
} // if (instanceHSMEnabled)

  Hashtable addlParms = new Hashtable();

  // The OID of the user being reset must be sent to the mediator
  // It either comes from the XML if there were errors or from the URL parameters
  String userOID="";
  //cquinton 9/12/2012 Rel portal refresh ir#t36000004008 dialogId is always passed in,
  // no need for divId
  String dialogId=StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
 // String selection=StringFunction.xssCharsToHtml(request.getParameter("selection"));
  String selection=request.getParameter("selection");//MEer XSS fix CIDs-11577, 11578
  //cquinton 9/16/2012 Rel portal refresh ir#3412 start
  //new src parm that is used to help determine how to return when
  // the form submits
  String source=StringFunction.xssCharsToHtml(request.getParameter("src"));
  //cquinton 9/16/2012 Rel portal refresh ir#3412 end

  if(doc.getAttribute("/In/userOID") != null)
    userOID = doc.getAttribute("/In/userOID");
  else if(selection !=null)
    userOID = EncryptDecrypt.decryptStringUsingTripleDes(selection, userSession.getSecretKey());
  
  if(StringFunction.isNotBlank(userOID)){
	  userOID = StringFunction.xssCharsToHtml(userOID);	  
  }

  // Pass the user OID to the user
  addlParms.put("userOID", userOID);

  // Flag to indicate that this user should be reset
  addlParms.put("resettingUserFlag", "true");

  // If the password is being changed, force the user to change it again
  // once they log in
  addlParms.put("force", "true");

  // If the password is being changed, indicate that there is no validation
  // that the user entered their current password
  addlParms.put("checkCurrentPasswordFlag", "false");

  //cquinton 9/16/2012 Rel portal refresh ir#3412 start
  if ("TradePortalHome".equals(source)) {
%>
  <%= formMgr.getFormInstanceAsInputField("HomeUnlockUserAccount", addlParms) %>
<%
  } else {
%>
  <%= formMgr.getFormInstanceAsInputField("UnlockUserAccount", addlParms) %>
<%
  }
  //cquinton 9/16/2012 Rel portal refresh ir#3412 end
  if (instanceHSMEnabled)
{
    user.getById(userOID); 
    

    if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_GLOBAL)) {
      stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
      // [START] CR-482
      minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
      // [END] CR-482
    }
    else {
      ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,  "ClientBank");
      String organizationOid = user.getAttribute("owner_org_oid");
      if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE)){
    	  CorporateOrganizationWebBean corporateOrganization = beanMgr.createBean(CorporateOrganizationWebBean.class,  "CorporateOrganization");
    	  corporateOrganization.getById(organizationOid);
    	  organizationOid =  corporateOrganization.getAttribute("client_bank_oid");
      }
      if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_BOG)){
    	  BankOrganizationGroupWebBean bankOrganizationGroup = beanMgr.createBean(BankOrganizationGroupWebBean.class,  "BankOrganizationGroup");
    	  bankOrganizationGroup.getById(organizationOid);
    	  organizationOid =  bankOrganizationGroup.getAttribute("client_bank_oid");
      }
    	  
      clientBank.getById(organizationOid);

      // [START] CR-482 Updated indenting and added minimumPasswordLength
      if (user.getAttribute("ownership_level").equals(TradePortalConstants.OWNER_CORPORATE)) {
        stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
        minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
      }
      else {
        stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
        minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
      }
      // [END] CR-482
    }
	
    if (stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
      suffix = "StricterRules";
    }

%>
<input type=hidden name="currentPassword" value="<%=user.getAttribute("password")%>)">
<%} %>		
    <div class='formItem'>
      <%=widgetFactory.createRadioButtonField("changingPasswordFlagGroup", "1", "ResetUserAccount.ResetOnlyLoginCount", "", true, false, "onchange='setchangingPasswordFlag()'", "") %> <br />
      <%=widgetFactory.createRadioButtonField("changingPasswordFlagGroup", "2", "ResetUserAccount.ResetCountAndPassword", "", false, false, "onchange='setchangingPasswordFlag()'", "") %> <br />
      <div class='formItemWithIndent4'>
        <%=widgetFactory.createNote("ResetUserAccount.userToSpecify") %>
      </div>
      <div class="formItemWithIndent3">
        <%=widgetFactory.createTextField("newPassword", "ResetUserAccount.NewPassword", "", "32", false, false, false, "class='char15' type='password' onClick=ChangeRadioButton();", "", "inline") %>
        <%=widgetFactory.createTextField("retypePassword", "ResetUserAccount.RetypeNewPassword", "", "32", false, false, false, "class='char15' type='password' onClick=ChangeRadioButton();", "", "inline") %>
      </div>
      <%-- 08-Oct-12 dillip T36000006114 --%>
      <div style="clear:both"></div>
    </div>
    <div class='formItem' style="margin-left:29px;">
      <button data-dojo-type="dijit.form.Button" type="button" id="Reset_Locked_Out_User" name="Reset_Locked_Out_User"><%=resMgr.getText("ResetUserAccount.Reset_Locked_Out_User", TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          unLockUser();
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("Reset_Locked_Out_User", "AdminRefdataUnlockUser.ResetButton") %>	 
      <%-- Ended(T36000006114) --%>
				
      <button data-dojo-type="dijit.form.Button"  name="Close" id="Reset_Locked_Out_User_Cancel" type="button">
        <%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          resetUserDialog.cancel();
        </script>
      </button> 
      <%=widgetFactory.createHoverHelp("Reset_Locked_Out_User_Cancel", "AdminRefdataUnlockUser.Cancel") %> 
      <%=widgetFactory.createHoverHelp("adminUserDialogCloseButton", "AdminRefdataUnlockUser.Cancel") %>

    </div>
    <input type="hidden" name="changingPasswordFlag" id="changingPasswordFlag" value="false">
  </form>
</div>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<%
  // [START] CR-482
  // Include javascript functions for encryption
  if (instanceHSMEnabled)
  {
%>
<script src="js/crypto/pidcrypt.js"></script>
<script src="js/crypto/pidcrypt_util.js"></script>
<script src="js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="js/crypto/sha256.js"></script>
<script src="js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%> 
<%
  } // if (instanceHSMEnabled)
%>
<script type="text/javascript">
<%
  if (instanceHSMEnabled) {
       
%>
  function encryptHsmFields() {
    var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
    var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
    <%-- for debugging hsm in dev comment out above and comment in below --%>
    <%-- var public_key = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvmMiU1irZ6aejIm0B+cKL9foNO+iDdftWwFS0eUYmu8A4IJWxoq66hgJ1+z84TuKrkHYhWGYz1iK1sxsf5CgbN86UumXX99c3KhfP3YJrgHzohvpU0yt/HAlSLa7JgnuSTNYcQiaGW/y1lkuS8bzSw2Hb/yeXUI1yt7KhwTPjE9hJnh5Jk7YERebRXP7MCOUxWJBuEjidmYIRjRkqzJ3FDEG1WByLjco5cmMaWZYdz9GUUEF3lIW7/Zb0kt40B2Ct0b3sIPhH5g6UF+nCzXxzBy5FOMMdUxh23re2ob88Ec+9AfYcD3qL9HS9IQ3ztAHTPZbhcwj8++8b9sXrfo/+QIDAQAB\n-----END PUBLIC KEY-----'; --%>
    <%-- var iv = 'zhXaSTr+/981U4TPJSu/Yg=='; --%>

    var protector = new TPProtect();
      
    <%-- Ensure that the new password and retyped password match --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%= TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: document.ResetUserAccount.newPassword.value, retypePassword: document.ResetUserAccount.retypePassword.value});
    }
      
    <%-- Ensure that the new password is not the same as the user_identifier --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ResetUserAccount.newPassword.value, sameAsString: "<%=StringFunction.escapeQuotesforJS(user.getAttribute("user_identifier")) %>"});
    }

    <%-- Ensure that the new password is not the same as the first_name --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ResetUserAccount.newPassword.value, sameAsString: "<%= StringFunction.escapeQuotesforJS(user.getAttribute("first_name")) %>"});
    }
      
    <%-- Ensure that the new password is not the same as the last_name --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ResetUserAccount.newPassword.value, sameAsString: "<%= StringFunction.escapeQuotesforJS(user.getAttribute("last_name")) %>"});
    }

    <%-- Ensure that the new password is not the same as the login_id --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.ResetUserAccount.newPassword.value, sameAsString: "<%= StringFunction.escapeQuotesforJS(user.getAttribute("login_id")) %>"});
    }

    <%-- Ensure that the new password does not start or end with a space --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%= TradePortalConstants.PASSWORD_SPACE %>", newPassword: document.ResetUserAccount.newPassword.value});
    }

    <%-- Ensure that the new password is the required length --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
    	 <%-- MEer T36000035658 To fix CID-11576 XSS issue --%>
      document.ResetUserAccount.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%= TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: document.ResetUserAccount.newPassword.value, reqLength: "<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(minimumPasswordLength)) %>"});
    }

    <%-- Ensure that the new password meets the consecutive characters requirements --%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%= TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: document.ResetUserAccount.newPassword.value});
    }

<%
    if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
%>
    if (document.ResetUserAccount.passwordValidationError.value == "") {
      document.ResetUserAccount.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%= TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: document.ResetUserAccount.newPassword.value});
    }
<%
    }
%>

    <%-- Encrypt two versions of the current password, clear-text and SHA-256 hashed --%>
    if (document.ResetUserAccount.currentPassword) {
      document.ResetUserAccount.encryptedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ResetUserAccount.currentPassword.value});          
      <%-- [START] CR-543 --%>
      var currentPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.ResetUserAccount.currentPassword.value, challenge: iv});
      document.ResetUserAccount.protectedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: currentPasswordMac});                    
    }
    <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
    if (document.ResetUserAccount.retypePassword) {
        document.ResetUserAccount.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ResetUserAccount.retypePassword.value});  
     }    
    
    <%-- Encrypt the MAC of the new password --%>
    if (document.ResetUserAccount.newPassword) {
       	
        var newPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.ResetUserAccount.newPassword.value, challenge: iv});
        var combinedNewPassword = newPasswordMac + '|' + document.ResetUserAccount.newPassword.value;
        document.ResetUserAccount.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: combinedNewPassword});  

    }          
    <%-- [END] CR-543 --%>
    <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- END --%>  
    <%-- Blank out the password fields --%>
    if (document.ResetUserAccount.currentPassword) {
      document.ResetUserAccount.currentPassword.value = "";
    }
    if (document.ResetUserAccount.newPassword) {
      document.ResetUserAccount.newPassword.value = "";
    }
    if(document.ResetUserAccount.retypePassword) {
      document.ResetUserAccount.retypePassword.value = "";
    }
  } <%--  function encryptHsmFields() --%>
<%
  } // if (instanceHSMEnabled)
%>



  var resetUserDialog = {};

  require(["dojo/on", "dojo/dom","t360/dialog"],
      function(on, dom, dialog) {

    resetUserDialog.cancel = function() {
      dialog.hide('<%= StringFunction.escapeQuotesforJS(dialogId) %>');
    }

  });

  function setchangingPasswordFlag(){

    if(dijit.byId("1").checked){
      document.getElementById("changingPasswordFlag").value=false;
    }
    if(dijit.byId("2").checked){
      document.getElementById("changingPasswordFlag").value=true;
    }
  }

  function unLockUser(){
    var unLockForm=document.getElementById("ResetUserAccount");
    setButtonPressed('<%=TradePortalConstants.BUTTON_RESET_ACCOUNT%>', '0');
    <%
    if (instanceHSMEnabled) {
  %>
          encryptHsmFields();
  <%
    } // if (instanceHSMEnabled)
  %>
    
    unLockForm.submit();
  }
  function ChangeRadioButton(){
    dijit.byId("1").set('checked',false);
    dijit.byId("2").set('checked',true);
  }
</script>
