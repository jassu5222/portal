<%--
*******************************************************************************
ViewPanelAuthorisationsDialog.jsp

This will display the new "View Authorisations" screen for the transaction.
From this screen, the user can view each panel rule and which authorisation has been received 
as well as which authorisations are still required. 
If user then clicks on alias/panel level hyperlink on the "View Authorisations" Screen, 
the system will display a pop up screen with a list of all the users assigned to that panel level/alias.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

    /*********\
    * GLOBALS *
    \*********/
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    String authHistory = null;
    Hashtable refData = new Hashtable();
    HashMap<Integer, PanelAuthorizationRuleObject> panelRuleMap = null;
    String panelRangeMinAmount = "";
    String panelRangeMaxAmount = "";
    String baseCurrency = "";
    String currencyBasis = "";
    int panelApproversLen = 0;
    int noOfAuthRemaining = 0;
    int panelApproversSize1 = 0;
    int panelApproversSize2 = 0;
    String paramOid1, paramOid2 = "";
    String awaitingPanelApprovers = null;
    String authReceived = null;
    List<PanelRule> panelRuleList = new ArrayList<PanelRule>();
    Map panelAppSummaryMap = null;
    PanelAuthUtility panelUtilObj = new PanelAuthUtility();
    String fromPage = null;
    String rowKey = null;
    String decryptedRowKey = null;
    String isEncrypted = null;
    boolean isBulkPayment = false;
    Map<Double , PanelRange> panelRangeCollectionMap = null;
    PanelRange panelRangeObj = new PanelRange();
    Panel panelObj = null;
    String viewBeneButtonName = null;
    String notifyPanelDialogTitle = resMgr.getText("Home.PanelAuthTransactions.PanelLevel", TradePortalConstants.TEXT_BUNDLE);
    String viewBeneDialogTitle = resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.ViewBeneDialog.Title", TradePortalConstants.TEXT_BUNDLE);
    PanelAuthorizationRangeObject panelAuthRange[] = null; 
    PanelAuthorizationRuleObject panelAuthRule = null;
    int panelRangesSize = 0;
    boolean fromPanelAuthDialog = false; //MEer Rel 8.3 IR-23030
	
    if(request.getParameter("insType")!=null){
    	fromPage = request.getParameter("insType").toString();
    }
    if(request.getParameter("rowkey")!=null){
    	rowKey = request.getParameter("rowkey").toString();
    }
    if(request.getParameter("isEncrypted")!=null)
    {
    	isEncrypted = request.getParameter("isEncrypted").toString();
    }else{
    	isEncrypted = "true";	
    }
    
    if(rowKey!=null){
    	if(isEncrypted.equals("true")){
    		decryptedRowKey = EncryptDecrypt.decryptStringUsingTripleDes(rowKey, userSession.getSecretKey());
    	}else{
    		decryptedRowKey = rowKey;
    	}
   }
    if(fromPage.equals("TradeCash")){
    	paramOid1 = decryptedRowKey.split("/")[0];//transaction_oid
    	paramOid2 = decryptedRowKey.split("/")[1];//instrument_oid
    	fromPanelAuthDialog = true;//MEer Rel 8.3 IR-23030
    	panelObj = panelUtilObj.populatePanelDetails(fromPage,paramOid1,paramOid2,userSession.getUserOid(),fromPanelAuthDialog);
    }else{
    	paramOid1 = decryptedRowKey.split("/")[0];
    	panelObj = panelUtilObj.populatePanelDetails(fromPage,paramOid1,userSession.getUserOid());
    }
    
    if(panelObj != null){
    	
    
	  	//Fetch Panel Aliases Map from CorporateOrganizationBean
		panelUtilObj.getPanelLevelAliases(userSession.getOwnerOrgOid(),userSession.getUserLocale());
	  	
		//Fetch refData values for Approver Sequence
		try {
			refData = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr("PANEL_APPROVE_SEQUENCE", userSession.getUserLocale());
		} catch (AmsException e) {
			Debug.debug("WidgetFactory createSortedOptions[] - Error in fetching RefData Values"); 
		} 
	    
		baseCurrency = panelUtilObj.getCurrency(); 
		currencyBasis = panelUtilObj.getCurrencyBasis();
		isBulkPayment = panelUtilObj.isBulkPayment();
		authHistory = panelUtilObj.getAuthHistory();
		panelAppSummaryMap = panelUtilObj.getPanelAppSummaryMap();
		
		panelRangeCollectionMap = panelObj.getPanelRangeSortedMap();
		if(panelRangeCollectionMap != null){
			panelRangesSize = panelRangeCollectionMap.size();
		}else{
			panelRangesSize = 0;
		}
		panelAuthRange = new PanelAuthorizationRangeObject[panelRangesSize];
		if(!isBulkPayment && panelRangesSize!=0){
			panelRangesSize=1; //Always only one Panel Range if it is not a Bulk Payment
		}
		Iterator iter = null;
		if(panelRangeCollectionMap != null){
			iter = panelRangeCollectionMap.keySet().iterator();
		}
		
	    
	    for (int cntLoop = 0; cntLoop<panelRangesSize; cntLoop++ ) {
			panelAuthRange[cntLoop] = new PanelAuthorizationRangeObject();
			Double rangeMinAmount = Double.parseDouble(iter.next().toString() );	 	
			panelRangeObj =  panelRangeCollectionMap.get(rangeMinAmount);
			
			//Get the list of Panel Rules for the Panel Range
		    if(panelRangeObj!= null){
		    	panelRuleList = panelRangeObj.getPanelRuleList(); 
		    }
			
		    /* Getting values for Panel range min and max amount starts*/
			  if(panelRangeObj!=null){
			       if(!InstrumentServices.isBlank(panelRangeObj.getPanelRangeMinAmt())) {
			             panelRangeMinAmount = TPCurrencyUtility.getDisplayAmount(panelRangeObj.getPanelRangeMinAmt(), baseCurrency, userSession.getUserLocale());
			       }
			       if(!InstrumentServices.isBlank(panelRangeObj.getPanelRangeMaxAmt())) {
			             panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmount(panelRangeObj.getPanelRangeMaxAmt(), baseCurrency, userSession.getUserLocale());
			       }
			  }
		    
			panelAuthRange[cntLoop].setPanel_auth_range_oid(panelRangeObj.getPanelRangeOid());
		    panelAuthRange[cntLoop].setPanel_auth_range_min_amount(panelRangeMinAmount);
		    panelAuthRange[cntLoop].setPanel_auth_range_max_amount(panelRangeMaxAmount); 
		    panelAuthRange[cntLoop].setPanel_auth_range_base_currency(baseCurrency);
		    panelAuthRange[cntLoop].setPanel_auth_range_currency_basis(currencyBasis);
		    
		    panelRuleMap = new HashMap<Integer, PanelAuthorizationRuleObject>();
		    
		    if(panelRangeObj!=null){ 
		  		  PanelRule panelRuleObj = null;
		  		  String appValue = null;
		  		  awaitingPanelApprovers = null;
		  		    for(int jLoop=0;jLoop<panelRuleList.size();jLoop++){
			  	      panelRuleObj = (PanelRule)panelRuleList.get(jLoop);
			  	      panelAuthRule = new PanelAuthorizationRuleObject();
			      		System.out.println("Print"+panelRuleObj);
			      	  if(panelRuleObj!=null){
			      		  	if(StringFunction.isBlank(authHistory)) {
			      				authHistory = "";
			      			}
			      		  awaitingPanelApprovers = panelRuleObj.getAllAwaitingPanelApprovers(authHistory);
			      		  panelApproversLen = awaitingPanelApprovers.length();
			      		//MEerupula Rel 8.3 IR-20194 To get received approvals for each rule use 'authHistoryPerRule' from PanelRule object
			      		  String receivedAuthorisations = panelRuleObj.getAuthHistoryPerRule().toString();
			      		  String totalApprovers = panelRuleObj.getPanelApprovers(); 
			      		  authReceived = "";
			      		  if(receivedAuthorisations !="" && (panelApproversLen != totalApprovers.length())){
				      		  authReceived = "{"
				                  + "'identifier': 'id',"
				                  + "'label' : 'name',"
				                  + "'items' : [";
				                for(int i=0;i<receivedAuthorisations .length();i++){
				                	authReceived = authReceived + "{'id' : '"+panelUtilObj.getAliasName(receivedAuthorisations.substring(i,i+1))+"_"+i+"',"; //IR T36000020390 Rel 8.3 - Appending 'i' to make sure the id is unique
					      			authReceived = authReceived + "'name' : '"+panelUtilObj.getAliasName(receivedAuthorisations.substring(i,i+1))+"'}";
					      			if(i<receivedAuthorisations.length()-1){
					      				authReceived = authReceived+",";
					      			}
					      		  }
				              authReceived = authReceived+  "]"
				              + "}";
			      		  }
			              if(panelApproversLen == totalApprovers.length()){
			      		  	  noOfAuthRemaining = totalApprovers.length();
			              }else{
			            	  noOfAuthRemaining = totalApprovers.length() - receivedAuthorisations.length();
			              }
			      		  panelAuthRule.setTotalNoOfAuthorizersRemaining(noOfAuthRemaining);
			      		  panelAuthRule.setApproversHistory(authReceived);
			      		  panelAuthRule.setApprover_sequence(refData.get(panelRuleObj.getSequence()).toString()); 
			      		  List pList = new ArrayList(); 
			          	  for(int k=0;k<panelApproversLen;k++){
			          		  appValue = awaitingPanelApprovers.substring(k,k+1).toString();
			          		  pList.add(appValue);
			          	 }
			          	  
			          	  for(int k=0;k<(20-panelApproversLen);k++){
			          		  pList.add("");
			          	  }
			          	panelAuthRule.setApprover_1((pList.get(0)!=null)?pList.get(0).toString():"");
			          	panelAuthRule.setApprover_2((pList.get(1)!=null)?pList.get(1).toString():""); 
			          	panelAuthRule.setApprover_3((pList.get(2)!=null)?pList.get(2).toString():""); 
			          	panelAuthRule.setApprover_4((pList.get(3)!=null)?pList.get(3).toString():""); 
			          	panelAuthRule.setApprover_5((pList.get(4)!=null)?pList.get(4).toString():""); 
			          	panelAuthRule.setApprover_6((pList.get(5)!=null)?pList.get(5).toString():""); 
			          	panelAuthRule.setApprover_7((pList.get(6)!=null)?pList.get(6).toString():""); 
			          	panelAuthRule.setApprover_8((pList.get(7)!=null)?pList.get(7).toString():""); 
			          	panelAuthRule.setApprover_9((pList.get(8)!=null)?pList.get(8).toString():""); 
			          	panelAuthRule.setApprover_10((pList.get(9)!=null)?pList.get(9).toString():""); 
			          	panelAuthRule.setApprover_11((pList.get(10)!=null)?pList.get(10).toString():"");
			          	panelAuthRule.setApprover_12((pList.get(11)!=null)?pList.get(11).toString():""); 
			          	panelAuthRule.setApprover_13((pList.get(12)!=null)?pList.get(12).toString():""); 
			          	panelAuthRule.setApprover_14((pList.get(13)!=null)?pList.get(13).toString():""); 
			          	panelAuthRule.setApprover_15((pList.get(14)!=null)?pList.get(14).toString():""); 
			          	panelAuthRule.setApprover_16((pList.get(15)!=null)?pList.get(15).toString():""); 
			          	panelAuthRule.setApprover_17((pList.get(16)!=null)?pList.get(16).toString():""); 
			          	panelAuthRule.setApprover_18((pList.get(17)!=null)?pList.get(17).toString():""); 
			          	panelAuthRule.setApprover_19((pList.get(18)!=null)?pList.get(18).toString():""); 
			          	panelAuthRule.setApprover_20((pList.get(19)!=null)?pList.get(19).toString():""); 
			          	panelRuleMap.put(jLoop,panelAuthRule);
			          } 
			      	}
		  		   
		  	  }
		    panelAuthRange[cntLoop].setPanelAuthRuleMap(panelRuleMap); 
		}
		panelRuleMap = null;
		panelAuthRule = null;
		
		if(panelAppSummaryMap.size()>5){
			panelApproversSize1 = 5;
			panelApproversSize2 = panelAppSummaryMap.size()-5;
		}else{
			panelApproversSize1 = panelAppSummaryMap.size();
		}
    }
	
		
		
%>
<%--HTML Code to display the contents starts here --%>

<div class="viewPendingPanelDiv"> 
<%if(isBulkPayment && panelAppSummaryMap.size()>0){ %>
		<%=widgetFactory.createSectionHeader("p1", "Home.PanelAuthTransactions.ViewAuthorisationsDialog.Section1") %>
		<%=widgetFactory.createNote("Home.PanelAuthTransactions.ViewAuthorisationsDialog.SummaryText") %>
		<div class="formContent" style="padding:10px;">
		<div class = "columnLeftNoBorder">
		<table cellspacing="0" cellpadding="0" class="formDocumentsTable" style="width:75%">
			<thead>
			<tr>
				<th>
					<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.PanelApprover", TradePortalConstants.TEXT_BUNDLE) %>
				</th>
			</tr>
			</thead>
			<%  String appName = "";
				Iterator iter1 = panelAppSummaryMap.keySet().iterator();
          		for (int app = 0; app<panelApproversSize1;app++ ) {
          			appName =(iter1.next()).toString();
          		%>
          		<tr>
				<td>
					<a href="javascript:openNotifyDialog('<%=appName%>','<%=panelUtilObj.getAliasName(appName).toString() %>');"><%=panelUtilObj.getAliasName(appName).toString() %></a>
				</td>
			</tr>
			<%} %>
		</table>
		</div>
		<%if(panelApproversSize2>0) {%>
		<div class = "columnRightNoBorder">
		<table cellspacing="0" cellpadding="0" class="formDocumentsTable" style="width:75%">
			<thead>
			<tr>
				<th>
					<%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.PanelApprover", TradePortalConstants.TEXT_BUNDLE) %>
				</th>
			</tr>
			</thead>
			<%for(int app=0;app<panelApproversSize2;app++) {
				appName =(iter1.next()).toString();%>
			<tr>
				<td>
					<a href="javascript:openNotifyDialog('<%=appName%>','<%=panelUtilObj.getAliasName(appName).toString() %>');"><%=panelUtilObj.getAliasName(appName).toString() %></a>
				</td>
			</tr>
			<%} %>
		</table>
		</div>
		<%} %>
		<div style="clear:both;"></div>
		</div>
		</div>
<%} %>
<%
	if(panelObj == null || panelRangesSize == 0){ %>
	 	<%=widgetFactory.createNote("Panel Group Reference Data for this transaction has changed. Please verify transaction again to update panel information.") %>
 	<%}else{
		  if(isBulkPayment){ %>
			<%=widgetFactory.createSectionHeader("p2", "Home.PanelAuthTransactions.ViewAuthorisationsDialog.Section2") %>
		<%}else{ %>
			<%=widgetFactory.createSectionHeader("p2", "Home.PanelAuthTransactions.ViewAuthorisationsDialog.Section") %>
		<%} 
	 }%>

<%
	for (int i=0;i<panelRangesSize;i++ ) 
	{
%>
	
<div class="viewPendingPanelRangesDiv">

	<div id="panelRangeDiv">
	
                        	<div >
	                        	<table cellspacing="0" cellpadding="0" class="formDocumentsTable">
	                        		<thead>
	                                <tr>
	                                	<th colspan="24" class="headingLeft">
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.PmntAmtRange", TradePortalConstants.TEXT_BUNDLE) %>
	                                        <%=widgetFactory.createAmountField("panel_range_MinAmt_"+i,"",panelAuthRange[i].getPanel_auth_range_min_amount().toString(), "", false, false, false,"class='char10' READONLY", "", "none")%>
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.to", TradePortalConstants.TEXT_BUNDLE) %>
	                                        <%=widgetFactory.createAmountField("panel_range_MaxAmt_"+i,"",panelAuthRange[i].getPanel_auth_range_max_amount().toString(), "", false, false, false,"class='char10' READONLY", "", "none")%>
	                                        <b></b>
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.currbasis", TradePortalConstants.TEXT_BUNDLE) %>
	                                        <%=widgetFactory.createSubLabel(panelAuthRange[i].getPanel_auth_range_base_currency()+" ["+ReferenceDataManager.getRefDataMgr().getDescr("CURRENCY_BASIS",panelAuthRange[i].getPanel_auth_range_currency_basis(), userSession.getUserLocale())+"]") %>
	                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                        <%if(isBulkPayment){ 
	                                        	viewBeneButtonName = "ViewBene_"+i;
	                                        %>
	                                        
		                                        <button data-dojo-type="dijit.form.Button" type="button" name="<%=viewBeneButtonName%>"  id="<%=viewBeneButtonName%>">
		                                        <%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.ViewBeneficiaries", TradePortalConstants.TEXT_BUNDLE) %>
		 											<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
														      openViewBene('<%=panelAuthRange[i].getPanel_auth_range_oid()%>','<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(paramOid1))%>','<%=viewBeneDialogTitle%>'); 					
	 	 											</script>
												</button>
											<%} %>
	                                    </th>
	                                </tr>
	                               <tr>
	                               		<th class="headingLeft">
	                               		&nbsp;
	                                    </th>
	                                	<th >
	                                        <%=resMgr.getText("Home.PanelAuthTransactions.ViewAuthorisationsDialog.Sequence", TradePortalConstants.TEXT_BUNDLE) %>
	                                    </th>
	                                    <th >
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.NumberOfAuthorisations", TradePortalConstants.TEXT_BUNDLE) %>
	                                    </th>
	                                    <th class="headingLeft">
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.ApprovalsReceived", TradePortalConstants.TEXT_BUNDLE) %>
	                                    </th>
	                                    <%for(int appLoop=1;appLoop<=20;appLoop++){ %>
	                                    <th class="headingLeft" nowrap>
	                                        <%=resMgr.getText("PanelAuthorizationGroupDetail.approver", TradePortalConstants.TEXT_BUNDLE) %>
	                                    </th>
	                                    <%} %>
	                                </tr>
	                              	</thead>
	                              	<%
	                              	panelRuleMap = new HashMap<Integer, PanelAuthorizationRuleObject> ();
	                              	panelRuleMap = panelAuthRange[i].getPanelAuthRuleMap();
	                              	int iLoop = 0;
	                              	for (Iterator iter1 = panelRuleMap.keySet().iterator(); iter1.hasNext(); ) {
	                              		panelAuthRule = new PanelAuthorizationRuleObject();
	                              		iter1.next();
	                              		panelAuthRule = (PanelAuthorizationRuleObject)panelRuleMap.get(iLoop); 
	                              	
	                              	%>
	                              	<tr>
	                              	<td><%=iLoop+1 %></td>
	                              	<td nowrap>
	                              		<%=widgetFactory.createSubLabel(panelAuthRule.getApprover_sequence()) %>
	                              	</td>
	                              	<td>
	                              		<%=widgetFactory.createTextField("NoOfAuth"+"_"+i+"_"+iLoop,"",String.valueOf(panelAuthRule.getTotalNoOfAuthorizersRemaining()), "25", true,false, false, "class='char10'", "", "none")%>
	                              	</td>
	                              	<%if(StringFunction.isNotBlank(panelAuthRule.getApproversHistory())) {%>
		                              	<td>
		                              	<%String divID ="PdivID"+"_"+i+"_"+iLoop;	%> 
		                              	<div id="<%=divID %>"></div>
		                              	
	 									</td>
	 								<%}else{ %>
	 									<td nowrap>&nbsp;</td>
	 								<%} %>	
 									<%if(StringFunction.isBlank(panelAuthRule.getApprover_1())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_1()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_1()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_1()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_2())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_2()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_2()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_2()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_3())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_3()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_3()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_3()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_4())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_4()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_4()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_4()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_5())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_5()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_5()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_5()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_6())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_6()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_6()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_6()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_7())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_7()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_7()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_7()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_8())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_8()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_8()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_8()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_9())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_9()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_9()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_9()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_10())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_10()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_10()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_10()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_11())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_11()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_11()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_11()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_12())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_12()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_12()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_12()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_13())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_13()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_13()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_13()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_14())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_14()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_14()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_14()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_15())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_15()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_15()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_15()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_16())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>	
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_16()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_16()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_16()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_17())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_17()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_17()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_17()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_18())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_18()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_18()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_18()).toString() %></a></td>
	                              	<%} %>
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_19())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_19()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_19()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_19()).toString() %></a></td>
	                              	<%} %>	
	                              	
	                              	<%if(StringFunction.isBlank(panelAuthRule.getApprover_20())){ %>
 										<td nowrap>&nbsp;</td>
 									<%}else{ %>
	                              		<td nowrap><a href="javascript:openNotifyDialog('<%=panelAuthRule.getApprover_20()%>','<%=panelUtilObj.getAliasName(panelAuthRule.getApprover_20()).toString() %>');"><%=panelUtilObj.getAliasName(panelAuthRule.getApprover_20()).toString() %></a></td>
	                              	<%} %>	
	                              	</tr>
	                              	<%iLoop++;
	                              	} %>
	                        	</table>
                        	</div>
             
	 </div> 
 </div>
 <%}%>
  </div>
</div>
	<%-- Sandeep - Rel 8.3 IR# T36000020184 08/26/2013 - Begin --%>
	<div class="gridFooter">
		<button id="PendingPanel_Cancel" data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
			<%=resMgr.getText("common.CancelText",TradePortalConstants.TEXT_BUNDLE)%>
    		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				closePendingPanalDialog();
    		</script>
  		</button>
  		<%=widgetFactory.createHoverHelp("PendingPanel_Cancel","common.Cancel") %>
		<div style="clear:both;"></div>
	</div>
	<%-- Sandeep - Rel 8.3 IR# T36000020184 08/26/2013 - End --%>
<script type="text/javascript">
	function openNotifyDialog(panelApprover, panelAlias){
		openNotify(panelApprover,'<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(paramOid1)) %>','<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(paramOid2)) %>','<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(fromPage))%>','<%=notifyPanelDialogTitle %>'+' '+ panelAlias); 
	}

	require([
         "dojo/data/ItemFileWriteStore",
         "dojo/store/Memory",
         "dijit/Tree",                    
         "dijit/tree/ForestStoreModel"  
         ], function( ItemFileWriteStore,Memory, Tree, ForestStoreModel) {
				var json;
				var j;
			  <% 
			  if(panelObj!=null){
				  for (int ik=0;ik<panelRangesSize;ik++ ) {
					  panelRuleMap = new HashMap<Integer, PanelAuthorizationRuleObject> ();
	                	panelRuleMap = panelAuthRange[ik].getPanelAuthRuleMap();
	                	int iLoop = 0;
	                	for (Iterator iter1 = panelRuleMap.keySet().iterator(); iter1.hasNext(); ) {
	                		panelAuthRule = new PanelAuthorizationRuleObject();
	                		iter1.next();
	                		panelAuthRule = (PanelAuthorizationRuleObject)panelRuleMap.get(iLoop); 
	                	
						if(StringFunction.isNotBlank(panelAuthRule.getApproversHistory()))
						{
					    %>
					  		  json = <%=panelAuthRule.getApproversHistory()%>;
					  		  var store = new ItemFileWriteStore( { data: json});
					  		  var model = new ForestStoreModel({ store : store, 
						                query : { 
						                        "id" : "*" 
						                }, 
						                rootLabel : "Approvers", 
						                childrenAttrs : [ "children" ] 
						        	}); 
				              j="<%=ik+"_"+iLoop%>";
				              var tree = new Tree( { model: model, id: "PdivID_"+j, autoExpand:false });
				              tree.placeAt( document.getElementById("PdivID_"+j) );
				              tree.startup();
					  <%}  
						iLoop++;
					  }
				}
			}%>	
          });
	
	<%-- Sandeep - Rel 8.3 IR# T36000020184 08/26/2013 - Begin --%>
	function closePendingPanalDialog(){
		require(["t360/dialog"], function(dialog) {
			dialog.hide('viewPanelAuthsdialogdivid');
		});
	}
	<%-- Sandeep - Rel 8.3 IR# T36000020184 08/26/2013 - End	 --%>
</script>