<%--
  Grid Customization Popup - this displays when the user clicks the edit icon
  on a grid.  It allows the user to customize their grid display.
  It is implemented as a popup rather than a dialog.

 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.Map, com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, 
  com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
  com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, 
  com.ams.tradeportal.dataview.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 

  //read in parameters
  String gridId = StringFunction.xssCharsToHtml(request.getParameter("gridId"));
 
 
 
  String gridName = StringFunction.xssCharsToHtml(request.getParameter("gridName"));
 
  //columns is comma delimited list of current columns in the grid
  // note that this is without prefix!
  String columns = StringFunction.xssCharsToHtml(request.getParameter("columns"));

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, response);
  //get the metadata for the grid
  DataGridManager.DataGridMetadata gridMetadata = DataGridManager.getDataGridMetadata(gridName);
%>

<div class="popupContent">
  <div id="saveGridCust" class="gridCustAction">
    <%= resMgr.getText("gridCustomizationPopup.save", TradePortalConstants.TEXT_BUNDLE) %>
  </div>
  <div id="resetGridCust" class="gridCustAction">
    <%= resMgr.getText("gridCustomizationPopup.reset", TradePortalConstants.TEXT_BUNDLE) %>
  </div>
  <div class="gridCustDivider"></div>
  <div class="gridCustColumnHeader">
    <%= resMgr.getText("gridCustomizationPopup.addRemoveColumns", TradePortalConstants.TEXT_BUNDLE) %>
  </div>
<%
  Set currentColumnSet = new HashSet();
  int colIdx = 0;
  // 11/20/2012 - rajendra - This is the total column count which is defined in the respective data view XML file
  int totalColumns = 0;

  // list all columns in the currently displayed grid
  if ( columns != null ) {
    StringTokenizer strTok = new StringTokenizer(columns, ",");
    while ( strTok.hasMoreTokens() ) {
      String columnId = StringFunction.xssCharsToHtml(strTok.nextToken());

      //determine if column is currently hidden
      //this also gets actual name of columns if modified
      boolean hiddenColumn = false;
      int hiddenIdx = columnId.indexOf(":off");
      if ( hiddenIdx >= 0 ) {
        hiddenColumn = true;
        columnId = columnId.substring(0, hiddenIdx);
      }

      //cquinton 8/30/2012 - only display if the metadata indicates the column is visible
      boolean display = false;
      for (int i=0; i<gridMetadata.columns.length; i++) {
        if (columnId.equals(gridMetadata.columns[i].columnKey) &&
            !gridMetadata.columns[i].isHidden) {
          display = true;
          break;
        }
      }

      if ( display ) {
        String displayColumnText =
        	StringFunction.xssCharsToHtml(dGridFactory.getColumnDisplayText( gridMetadata.resourcePrefix, columnId ));
%>

  <div class="gridCustColumn">
    <%--checked checkbox.
        note we use onChange explicitly here because could not get 
        external event handler to work in the popup due to dojo/ready timing issues--%>
    <%--cquinton 8/31/2012 add _gc to the id so it is doesn't conflict with some pages--%>
    <input id="<%=columnId%>_gc" name="<%=columnId%>" class="gridCustColumnCheckbox" type="checkbox" 
           <%=hiddenColumn?"":"checked"%> data-dojo-type="dijit.form.CheckBox"
           onChange="gridCustPopup.columnChange(this)" />
    <label for="<%=columnId%>_gc"><%=displayColumnText%></label>
  </div>
<%

        currentColumnSet.add(columnId);
        colIdx++;
      }
      // 11/20/2012 - rajendra - Increment total column count.
      totalColumns++;
    }
  }

  //now read the set of all columns from the grid xml
  // and append those columns not currently display in order
  for (int i=0; i<gridMetadata.columns.length; i++) {
    //cquinton 8/30/2012 - only display if the metadata indicates the column is visible
    if (!gridMetadata.columns[i].isHidden) {
      if (!currentColumnSet.contains(gridMetadata.columns[i].columnKey)) {
	    if (isVisible(gridMetadata.columns[i].visibilityCondition, userSession,beanMgr,resMgr)) {
        String columnId = StringFunction.xssCharsToHtml(gridMetadata.columns[i].columnKey);
        String displayColumnText =
        	StringFunction.xssCharsToHtml(dGridFactory.getColumnDisplayText( gridMetadata.resourcePrefix, columnId ));
%>
  <div class="gridCustColumn">
    <%--non checked checkbox.
        note we use onChange explicitly here because could not get 
        external event handler to work in the popup due to dojo/ready timing issues--%>
    <%--cquinton 8/31/2012 add _gc to the id so it is doesn't conflict with some pages--%>
    <input id="<%=columnId%>_gc" name="<%=columnId%>" class="gridCustColumnCheckbox" type="checkbox" 
           value="on" data-dojo-type="dijit.form.CheckBox"
           onChange="gridCustPopup.columnChange(this)" />
    <label for="<%=columnId%>_gc"><%=displayColumnText%></label>
  </div>
<%
        colIdx++;
      }
	  }
    }
  }
%>

</div>

<%!
    public boolean isVisible(String clazz, SessionWebBean userSession, BeanManager beanMgr, ResourceManager resMgr) {
    	boolean visible=false;
    	try {
            Class vClass = Class.forName(clazz);
            AbstractVisibilityCondition vCondition = (AbstractVisibilityCondition) vClass.newInstance();
            	visible = vCondition.execute(userSession,beanMgr,resMgr);
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    	return visible;
    }
%>


<script>
  var gridCustPopup;

  require(["dojo/query", "dojo/on", "t360/datagrid", "t360/OnDemandGrid", "dijit/registry", "t360/popup", "dojo/domReady!"],
      function(query, on, t360grid, onDemandGrid, registry, popup ) {

    gridCustPopup = {
      columnChange: function(checkBoxWidget) {
        var checkvalue = checkBoxWidget.get('value');
        var columnId = checkBoxWidget.name; <%-- note we do not use id --%>
        if ( checkvalue ) {
        	onDemandGrid.showColumn('<%=StringFunction.escapeQuotesforJS(gridId)%>',columnId);
        }
        else {
        	onDemandGrid.hideColumn('<%=StringFunction.escapeQuotesforJS(gridId)%>',columnId);
        	
        	<%--  11/20/2012 - rajendra - Get the list of columns to be customized from the datagrid. --%>
        	var hiddenColumnCount = onDemandGrid.getColumnsForCustomization('<%= StringFunction.escapeQuotesforJS(gridId)%>').match(/:off/g).length;

			<%--  <START - 11/20/2012 - rajendra> - Check whether hidden column count is equal to total columns in the datagrid. If 'YES', then show an alert to user --%>
			<%--  and show back the last column choosen for hiding. --%>
        	if(<%=totalColumns%> - hiddenColumnCount <= 0 ){
        		alert("At least one column should be selected.");
        		onDemandGrid.showColumn('<%=StringFunction.escapeQuotesforJS(gridId)%>',columnId);
        	}

        }
      }
    };

    <%--register event handlers--%>

    <%--action event handlers--%>
    query('#saveGridCust').on("click", function() {
      onDemandGrid.saveGridLayout('<%=StringFunction.escapeQuotesforJS(gridId)%>','<%=StringFunction.escapeQuotesforJS(gridName)%>');
      <%-- close all popup content --%>
      <%-- todo: use onExecute instead --%>
      query('.popupContent').forEach(function(p) {
        var myPopupRef = registry.getEnclosingWidget(p);
        popup.close(myPopupRef);
      });
    });

    query('#resetGridCust').on("click", function() {
    	<%-- Rel 9.2 XSS CID 11281, 11432 --%>
      onDemandGrid.resetGridLayout('<%=StringFunction.escapeQuotesforJS(gridId)%>','<%=StringFunction.escapeQuotesforJS(gridName)%>');
      <%-- close all popup content --%>
      <%-- todo: use onExecute instead --%>
      query('.popupContent').forEach(function(p) {
        var myPopupRef = registry.getEnclosingWidget(p);
        popup.close(myPopupRef);
      });
    });
  });
</script>